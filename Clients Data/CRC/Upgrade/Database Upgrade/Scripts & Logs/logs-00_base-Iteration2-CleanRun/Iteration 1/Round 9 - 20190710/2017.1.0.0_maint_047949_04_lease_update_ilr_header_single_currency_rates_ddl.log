15:49:49 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2017.1.0.0_maint_047949_04_lease_update_ilr_header_single_currency_rates_ddl.sql
15:49:49 hopplant> /*
15:49:49 hopplant> ||============================================================================
15:49:49 hopplant> || Application: PowerPlan
15:49:49 hopplant> || File Name:   maint_047949_04_lease_update_ilr_header_single_currency_rates_ddl.sql
15:49:49 hopplant> ||============================================================================
15:49:49 hopplant> || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
15:49:49 hopplant> ||============================================================================
15:49:49 hopplant> || Version	 Date	    Revised By	     Reason for Change
15:49:49 hopplant> || ---------- ---------- ---------------- ------------------------------------
15:49:49 hopplant> || 2017.1.0.0 05/15/2017 Jared Watkins    update ILR header view to use a faster currency rate retrieve
15:49:49 hopplant> ||============================================================================
15:49:49 hopplant> */
15:49:49 hopplant> 
15:49:49 hopplant> CREATE OR REPLACE VIEW V_LS_ILR_HEADER_FX_VW AS
15:49:49   2  WITH
15:49:49   3  cur AS (
15:49:49   4  	SELECT /*+ materialize */ ls_cur_type, currency_id, currency_display_symbol, iso_code, contract_approval_rate
15:49:49   5  	FROM (
15:49:49   6  	  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
15:49:49   7  	    contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 contract_approval_rate
15:49:49   8  	  FROM currency contract_cur
15:49:49   9  	  UNION
15:49:49  10  	  SELECT 2, company_cur.currency_id,
15:49:49  11  	    company_cur.currency_display_symbol, company_cur.iso_code, NULL
15:49:49  12  	  FROM currency company_cur
15:49:49  13  	)
15:49:49  14  ),
15:49:49  15  cr AS (
15:49:49  16  	SELECT /*+ materialize */ exchange_date, currency_from, currency_to, rate
15:49:49  17  	FROM currency_rate_default a
15:49:49  18  	WHERE exchange_date = (select max(exchange_date)
15:49:49  19  			       from currency_rate_default b
15:49:49  20  			       where to_char(a.exchange_date, 'yyyymm') = to_char(b.exchange_date, 'yyyymm')
15:49:49  21  			       and a.currency_from = b.currency_from and a.currency_to = b.currency_to)
15:49:49  22  ),
15:49:49  23  calc_rate AS (
15:49:49  24  	SELECT /*+ materialize */ company_id, contract_currency_id, company_currency_id,
15:49:49  25  	  accounting_month, exchange_date, rate
15:49:49  26  	FROM ls_lease_calculated_date_rates a
15:49:49  27  	WHERE accounting_month = (select max(accounting_month)
15:49:49  28  				  from ls_lease_calculated_date_rates b
15:49:49  29  				  where a.company_id = b.company_id
15:49:49  30  				  and a.contract_currency_id = b.contract_currency_id
15:49:49  31  				  and a.company_currency_id = b.company_currency_id
15:49:49  32  				  group by company_id, contract_currency_id, company_currency_id)
15:49:49  33  )
15:49:49  34  SELECT /*+ no_merge */
15:49:49  35  	ilr.ilr_id,
15:49:49  36  	ilr.ilr_number,
15:49:49  37  	ilr.current_revision,
15:49:49  38  	liasob.revision,
15:49:49  39  	liasob.set_of_books_id,
15:49:49  40  	cur.ls_cur_type,
15:49:49  41  	lio.in_service_exchange_rate,
15:49:49  42  	cr.rate,
15:49:49  43  	calc_rate.rate calc_rate,
15:49:49  44  	cur.currency_display_symbol,
15:49:49  45  	cur.iso_code,
15:49:49  46  	lease.contract_currency_id,
15:49:49  47  	cur.currency_id,
15:49:49  48  	cr.exchange_date,
15:49:49  49  	calc_rate.exchange_date calc_date,
15:49:49  50  	ilr.lease_id,
15:49:49  51  	ilr.company_id,
15:49:49  52  	liasob.internal_rate_return,
15:49:49  53  	liasob.is_om,
15:49:49  54  	lio.purchase_option_amt * nvl(calc_rate.rate, cr.rate) purchase_option_amt,
15:49:49  55  	lio.termination_amt * nvl(calc_rate.rate, cr.rate) termination_amt,
15:49:49  56  	liasob.net_present_value * nvl(calc_rate.rate, cr.rate) net_present_value,
15:49:49  57  	liasob.capital_cost * nvl(nvl(cur.contract_approval_rate, lio.in_service_exchange_rate), cr.rate) capital_cost,
15:49:49  58  	liasob.current_lease_cost * nvl(nvl(cur.contract_approval_rate, lio.in_service_exchange_rate), cr.rate) current_lease_cost
15:49:49  59  FROM ls_ilr ilr
15:49:49  60  INNER JOIN ls_ilr_amounts_set_of_books liasob
15:49:49  61  ON ilr.ilr_id = liasob.ilr_id
15:49:49  62  INNER JOIN ls_ilr_options lio
15:49:49  63  ON lio.ilr_id = liasob.ilr_id
15:49:49  64  AND lio.revision = liasob.revision
15:49:49  65  INNER JOIN ls_lease lease
15:49:49  66  ON ilr.lease_id = lease.lease_id
15:49:49  67  INNER JOIN currency_schema cs
15:49:49  68  	ON ilr.company_id = cs.company_id
15:49:49  69  INNER JOIN cur
15:49:49  70  	ON (cur.ls_cur_type = 1 AND cur.currency_id = lease.contract_currency_id)
15:49:49  71  	OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
15:49:49  72  INNER JOIN cr
15:49:49  73  	ON cur.currency_id = cr.currency_to
15:49:49  74  	AND lease.contract_currency_id = cr.currency_from
15:49:49  75  	AND cr.exchange_date < Add_Months(SYSDATE, 1)
15:49:49  76  LEFT OUTER JOIN calc_rate
15:49:49  77  	ON calc_rate.company_id = ilr.company_id
15:49:49  78  	AND calc_rate.contract_currency_id = lease.contract_currency_id
15:49:49  79  	AND calc_rate.company_currency_id = cur.currency_id
15:49:49  80  WHERE cr.exchange_date = (
15:49:49  81  	SELECT Max(exchange_date)
15:49:49  82  	FROM cr cr2
15:49:49  83  	WHERE cr.currency_from = cr2.currency_from
15:49:49  84  	AND cr.currency_to = cr2.currency_to
15:49:49  85  	AND cr2.exchange_date < Add_Months(SYSDATE, 1)
15:49:49  86  )
15:49:49  87  AND cs.currency_type_id = 1;

View created.

15:49:49 hopplant> 
15:49:49 hopplant> --****************************************************
15:49:49 hopplant> --Log the run of the script to local PP_SCHEMA_LOG
15:49:49 hopplant> --****************************************************
15:49:49 hopplant> INSERT INTO PP_SCHEMA_CHANGE_LOG
15:49:49   2  	  (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
15:49:49   3  	  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
15:49:49   4  VALUES
15:49:49   5  	  (3497, 0, 2017, 1, 0, 0, 47949, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047949_04_lease_update_ilr_header_single_currency_rates_ddl.sql', 1,
15:49:49   6  	  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
15:49:49   7  	  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

15:49:49 hopplant> COMMIT;

Commit complete.

15:49:49 hopplant> SPOOL OFF
