16:14:30 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2017.3.0.0_maint_050179_lessee_01_fcst_option_ddl.sql
16:14:30 hopplant> /*
16:14:30 hopplant> ||============================================================================
16:14:30 hopplant> || Application: PowerPlan
16:14:30 hopplant> || File Name: maint_050179_lessee_01_fcst_option_ddl.sql
16:14:30 hopplant> ||============================================================================
16:14:30 hopplant> || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
16:14:30 hopplant> ||============================================================================
16:14:30 hopplant> || Version	 Date	    Revised By	   Reason for Change
16:14:30 hopplant> || ---------- ---------- -------------- ----------------------------------------
16:14:30 hopplant> || 2017.3.0.0 03/06/2018 Shane "C" Ward Add new column for Earnings Acct to Lessee Forecasting (Modified Retrospective)
16:14:30 hopplant> ||										   Also add way to link Forecast ILR's around to in-service ones
16:14:30 hopplant> ||============================================================================
16:14:30 hopplant> */
16:14:30 hopplant> 
16:14:30 hopplant> ALTER TABLE ls_forecast_version ADD earnings_gl_acct_id NUMBER(22,0) NULL;

Table altered.

16:14:30 hopplant> 
16:14:30 hopplant> ALTER TABLE ls_forecast_version
16:14:30   2  	ADD CONSTRAINT ls_forecast_version_acct_fk FOREIGN KEY (
16:14:30   3  	  earnings_gl_acct_id
16:14:30   4  	) REFERENCES gl_account (
16:14:30   5  	  gl_account_id
16:14:30   6  	);

Table altered.

16:14:30 hopplant> 
16:14:30 hopplant> CREATE TABLE LS_ASSET_FCST_EARNINGS
16:14:30   2  	(
16:14:30   3  	   ls_asset_id	   NUMBER(22, 0),
16:14:30   4  	   revision	   NUMBER(22, 0),
16:14:30   5  	   set_of_books_id NUMBER(22, 0),
16:14:30   6  	   month	   DATE,
16:14:30   7  	   earnings_amount NUMBER(22, 2),
16:14:30   8  	   user_id	   VARCHAR2(18) NULL,
16:14:30   9  	   time_stamp	   DATE NULL
16:14:30  10  	);

Table created.

16:14:30 hopplant> 
16:14:30 hopplant> COMMENT ON COLUMN ls_asset_fcst_earnings.ls_asset_id IS 'System-assigned identifier of a particular leased asset.
16:14:30   2  The internal leased asset id within PowerPlan.';

Comment created.

16:14:30 hopplant> COMMENT ON COLUMN ls_asset_fcst_earnings.revision IS 'The revision.';

Comment created.

16:14:30 hopplant> COMMENT ON COLUMN ls_asset_fcst_earnings.set_of_books_id IS 'The internal set of books id within PowerPlant.';

Comment created.

16:14:30 hopplant> COMMENT ON COLUMN ls_asset_fcst_earnings.month IS 'The month being processed.';

Comment created.

16:14:30 hopplant> COMMENT ON COLUMN ls_asset_fcst_earnings.earnings_amount IS 'The earnings amount of the Asset at Conversion.';

Comment created.

16:14:30 hopplant> COMMENT ON COLUMN ls_asset_fcst_earnings.user_id IS 'Standard system-assigned user id used for audit purposes.';

Comment created.

16:14:30 hopplant> COMMENT ON COLUMN ls_asset_fcst_earnings.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

Comment created.

16:14:30 hopplant> 
16:14:30 hopplant> alter table ls_forecast_ilr_master add source_revision number(22,0);

Table altered.

16:14:30 hopplant> alter table ls_forecast_ilr_master add in_service_revision number(22,0);

Table altered.

16:14:30 hopplant> 
16:14:30 hopplant> COMMENT on column ls_forecast_ilr_master.source_revision is 'Current revision of the ILR that the forecast is based off. To be used for reference in transition.';

Comment created.

16:14:30 hopplant> COMMENT on column ls_forecast_ilr_master.in_service_revision is 'Revision number of the ILR that the forecast version puts into-service. To be used for reference in transition.';

Comment created.

16:14:30 hopplant> 
16:14:30 hopplant> --****************************************************
16:14:30 hopplant> --Log the run of the script to local PP_SCHEMA_LOG
16:14:30 hopplant> --****************************************************
16:14:30 hopplant> INSERT INTO PP_SCHEMA_CHANGE_LOG
16:14:30   2  	      (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
16:14:30   3  	      SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
16:14:30   4  VALUES
16:14:30   5  	      (4164, 0, 2017, 3, 0, 0, 50179, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050179_lessee_01_fcst_option_ddl.sql', 1,
16:14:30   6  	      SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
16:14:30   7  	      SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

16:14:30 hopplant> COMMIT;

Commit complete.

16:14:30 hopplant> SPOOL OFF
