07:13:19 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2018.2.0.0_maint_046763_taxrpr_02_companies_dml.sql
07:13:19 hopplant> /*
07:13:19 hopplant> ||============================================================================
07:13:19 hopplant> || Application: PowerPlan
07:13:19 hopplant> || File Name: maint_046763_taxrpr_02_companies_dml.sql.sql
07:13:19 hopplant> ||============================================================================
07:13:19 hopplant> || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
07:13:19 hopplant> ||============================================================================
07:13:19 hopplant> || Version	 Date	    Revised By	   Reason for Change
07:13:19 hopplant> || ---------- ---------- -------------- --------------------------------------
07:13:19 hopplant> || 2018.2.0.0 10/10/2017 Eric Berger    DML multi-batch reporting.
07:13:19 hopplant> ||============================================================================
07:13:19 hopplant> */
07:13:19 hopplant> 
07:13:19 hopplant> --if there are batches for the company, turn on the flag
07:13:19 hopplant> UPDATE company_setup o
07:13:19   2  	 SET is_tax_repairs_company = 1
07:13:19   3   WHERE EXISTS (SELECT company_id
07:13:19   4  		FROM repair_batch_control i
07:13:19   5  	       WHERE o.company_id = i.company_id);

0 rows updated.

07:13:20 hopplant> 
07:13:20 hopplant> --update dynamic filters to only show the in-scope companies in tax repairs
07:13:20 hopplant> INSERT INTO pp_dynamic_filter
07:13:20   2  	(filter_id, label, input_type, sqls_column_expression, dw, dw_id,
07:13:20   3  	 dw_description, dw_id_datatype)
07:13:20   4  	SELECT 286, 'Company', 'dw',
07:13:20   5  	       'company.company_id', 'dw_rpr_company_filter', 'company_id',
07:13:20   6  	       'description', 'N'
07:13:20   7  	  FROM dual
07:13:20   8  	 WHERE NOT EXISTS
07:13:20   9  	 (SELECT 1 FROM pp_dynamic_filter WHERE dw = 'dw_rpr_company_filter');

1 row created.

07:13:20 hopplant> 
07:13:20 hopplant> UPDATE pp_dynamic_filter_mapping
07:13:20   2  	 SET filter_id =
07:13:20   3  	     (SELECT Max(filter_id)
07:13:20   4  		FROM pp_dynamic_filter
07:13:20   5  	       WHERE dw = 'dw_rpr_company_filter')
07:13:20   6   WHERE pp_report_filter_id BETWEEN 30 AND 38
07:13:20   7  	 AND filter_id IN (3, 24, 49, 51);

9 rows updated.

07:13:20 hopplant> 
07:13:20 hopplant> --create the menu item for the company configuration window
07:13:20 hopplant> INSERT INTO ppbase_workspace
07:13:20   2  	(MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP,
07:13:20   3  	 OBJECT_TYPE_ID)
07:13:20   4  	SELECT 'REPAIRS', 'menu_wksp_tax_yr', 'Tax Year Configuration',
07:13:20   5  	       'uo_rpr_config_wksp_tax_yr', 'Tax Year Configuration', 1
07:13:20   6  	  FROM dual
07:13:20   7  	 WHERE NOT EXISTS
07:13:20   8  	 (SELECT 1
07:13:20   9  		  FROM ppbase_workspace
07:13:20  10  		 WHERE Lower(MODULE) = 'repairs'
07:13:20  11  		   AND Lower(workspace_identifier) = 'menu_wksp_tax_yr');

1 row created.

07:13:20 hopplant> 
07:13:20 hopplant> --create the menu item for the company configuration window
07:13:20 hopplant> INSERT INTO ppbase_workspace
07:13:20   2  	(MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP,
07:13:20   3  	 OBJECT_TYPE_ID)
07:13:20   4  	SELECT 'REPAIRS', 'menu_wksp_tax_rpr_companies', 'Company Configuration',
07:13:20   5  	       'uo_rpr_config_wksp_company', 'Repair Company Configuration', 1
07:13:20   6  	  FROM dual
07:13:20   7  	 WHERE NOT EXISTS
07:13:20   8  	 (SELECT 1
07:13:20   9  		  FROM ppbase_workspace
07:13:20  10  		 WHERE LOWER(MODULE) = 'repairs'
07:13:20  11  		   AND LOWER(workspace_identifier) = 'menu_wksp_tax_rpr_companies');

1 row created.

07:13:20 hopplant> 
07:13:20 hopplant> --fix the tr menu structure
07:13:20 hopplant> MERGE INTO ppbase_menu_items a
07:13:20   2  USING (SELECT 'REPAIRS' AS MODULE,
07:13:20   3  		    'menu_wksp_tax_exp_tests' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
07:13:20   4  		    4 AS ITEM_ORDER, 'Repair Tests' AS LABEL,
07:13:20   5  		    'Repair Test Configuration' AS MINIHELP,
07:13:20   6  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20   7  		    'menu_wksp_tax_exp_tests' AS WORKSPACE_IDENTIFIER,
07:13:20   8  		    1 AS ENABLE_YN
07:13:20   9  	       FROM DUAL
07:13:20  10  	     UNION ALL
07:13:20  11  	     SELECT 'REPAIRS' AS MODULE, 'menu_wksp_import' AS MENU_IDENTIFIER,
07:13:20  12  		    2 AS MENU_LEVEL, 12 AS ITEM_ORDER, 'Import' AS LABEL,
07:13:20  13  		    'Import Center' AS MINIHELP,
07:13:20  14  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20  15  		    'menu_wksp_import' AS WORKSPACE_IDENTIFIER, 1 AS ENABLE_YN
07:13:20  16  	       FROM DUAL
07:13:20  17  	     UNION ALL
07:13:20  18  	     SELECT 'REPAIRS' AS MODULE,
07:13:20  19  		    'menu_wksp_book_summary' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
07:13:20  20  		    2 AS ITEM_ORDER, 'Book Summary' AS LABEL,
07:13:20  21  		    'Repair Book Summary Configuration' AS MINIHELP,
07:13:20  22  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20  23  		    'menu_wksp_book_summary' AS WORKSPACE_IDENTIFIER,
07:13:20  24  		    1 AS ENABLE_YN
07:13:20  25  	       FROM DUAL
07:13:20  26  	     UNION ALL
07:13:20  27  	     SELECT 'REPAIRS' AS MODULE,
07:13:20  28  		    'menu_wksp_repair_schema' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
07:13:20  29  		    3 AS ITEM_ORDER, 'Repair Schema' AS LABEL,
07:13:20  30  		    'Repair Schema Configuration' AS MINIHELP,
07:13:20  31  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20  32  		    'menu_wksp_repair_schema' AS WORKSPACE_IDENTIFIER,
07:13:20  33  		    1 AS ENABLE_YN
07:13:20  34  	       FROM DUAL
07:13:20  35  	     UNION ALL
07:13:20  36  	     SELECT 'REPAIRS' AS MODULE,
07:13:20  37  		    'menu_wksp_tax_locations' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
07:13:20  38  		    5 AS ITEM_ORDER, 'Repair Locations' AS LABEL,
07:13:20  39  		    'Repair Location Configuration' AS MINIHELP,
07:13:20  40  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20  41  		    'menu_wksp_tax_locations' AS WORKSPACE_IDENTIFIER,
07:13:20  42  		    1 AS ENABLE_YN
07:13:20  43  	       FROM DUAL
07:13:20  44  	     UNION ALL
07:13:20  45  	     SELECT 'REPAIRS' AS MODULE, 'menu_wksp_tax_status' AS MENU_IDENTIFIER,
07:13:20  46  		    2 AS MENU_LEVEL, 8 AS ITEM_ORDER, 'Tax Status (Gen)' AS LABEL,
07:13:20  47  		    'Tax Status Configuration' AS MINIHELP,
07:13:20  48  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20  49  		    'menu_wksp_tax_status' AS WORKSPACE_IDENTIFIER, 1 AS ENABLE_YN
07:13:20  50  	       FROM DUAL
07:13:20  51  	     UNION ALL
07:13:20  52  	     SELECT 'REPAIRS' AS MODULE, 'menu_wksp_range_test' AS MENU_IDENTIFIER,
07:13:20  53  		    2 AS MENU_LEVEL, 9 AS ITEM_ORDER, 'Range Test (Gen)' AS LABEL,
07:13:20  54  		    'Range Test Configuration' AS MINIHELP,
07:13:20  55  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20  56  		    'menu_wksp_range_test' AS WORKSPACE_IDENTIFIER, 1 AS ENABLE_YN
07:13:20  57  	       FROM DUAL
07:13:20  58  	     UNION ALL
07:13:20  59  	     SELECT 'REPAIRS' AS MODULE,
07:13:20  60  		    'menu_wksp_tax_loc_rollups' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
07:13:20  61  		    10 AS ITEM_ORDER, 'Repair Loc Rollups (Alloc)' AS LABEL,
07:13:20  62  		    'Repair Location Rollup Configuration for Blanket Allocations' AS MINIHELP,
07:13:20  63  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20  64  		    'menu_wksp_tax_loc_rollups' AS WORKSPACE_IDENTIFIER,
07:13:20  65  		    1 AS ENABLE_YN
07:13:20  66  	       FROM DUAL
07:13:20  67  	     UNION ALL
07:13:20  68  	     SELECT 'REPAIRS' AS MODULE, 'menu_wksp_tax_units' AS MENU_IDENTIFIER,
07:13:20  69  		    2 AS MENU_LEVEL, 6 AS ITEM_ORDER,
07:13:20  70  		    'Tax Units of Property' AS LABEL,
07:13:20  71  		    'Tax Units of Property Configuration' AS MINIHELP,
07:13:20  72  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20  73  		    'menu_wksp_tax_units' AS WORKSPACE_IDENTIFIER, 1 AS ENABLE_YN
07:13:20  74  	       FROM DUAL
07:13:20  75  	     UNION ALL
07:13:20  76  	     SELECT 'REPAIRS' AS MODULE,
07:13:20  77  		    'menu_wksp_tax_thresholds' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
07:13:20  78  		    7 AS ITEM_ORDER, 'Tax Thresholds' AS LABEL,
07:13:20  79  		    'Tax Threshold Configuration' AS MINIHELP,
07:13:20  80  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20  81  		    'menu_wksp_tax_thresholds' AS WORKSPACE_IDENTIFIER,
07:13:20  82  		    1 AS ENABLE_YN
07:13:20  83  	       FROM DUAL
07:13:20  84  	     UNION ALL
07:13:20  85  	     SELECT 'REPAIRS' AS MODULE,
07:13:20  86  		    'menu_wksp_system_options' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
07:13:20  87  		    13 AS ITEM_ORDER, 'System Options' AS LABEL,
07:13:20  88  		    'System Options' AS MINIHELP,
07:13:20  89  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20  90  		    'menu_wksp_system_options' AS WORKSPACE_IDENTIFIER,
07:13:20  91  		    1 AS ENABLE_YN
07:13:20  92  	       FROM DUAL
07:13:20  93  	     UNION ALL
07:13:20  94  	     SELECT 'REPAIRS' AS MODULE, 'menu_wksp_tax_yr' AS MENU_IDENTIFIER,
07:13:20  95  		    2 AS MENU_LEVEL, 11 AS ITEM_ORDER,
07:13:20  96  		    'Tax Year Configuration' AS LABEL,
07:13:20  97  		    'Tax Year Configuration' AS MINIHELP,
07:13:20  98  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20  99  		    'menu_wksp_tax_yr' AS WORKSPACE_IDENTIFIER, 1 AS ENABLE_YN
07:13:20 100  	       FROM DUAL
07:13:20 101  	     UNION ALL
07:13:20 102  	     SELECT 'REPAIRS' AS MODULE,
07:13:20 103  		    'menu_wksp_tax_rpr_companies' AS MENU_IDENTIFIER,
07:13:20 104  		    2 AS MENU_LEVEL, 1 AS ITEM_ORDER,
07:13:20 105  		    'Company Configuration' AS LABEL,
07:13:20 106  		    'Repair Company Configuration' AS MINIHELP,
07:13:20 107  		    'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
07:13:20 108  		    'menu_wksp_tax_rpr_companies' AS WORKSPACE_IDENTIFIER,
07:13:20 109  		    1 AS ENABLE_YN
07:13:20 110  	       FROM DUAL) b
07:13:20 111  ON (a.MODULE = b.MODULE AND a.MENU_IDENTIFIER = b.MENU_IDENTIFIER)
07:13:20 112  WHEN NOT MATCHED THEN
07:13:20 113  	INSERT
07:13:20 114  	  (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
07:13:20 115  	   PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
07:13:20 116  	VALUES
07:13:20 117  	  (b.MODULE, b.MENU_IDENTIFIER, b.MENU_LEVEL, b.ITEM_ORDER, b.LABEL,
07:13:20 118  	   b.MINIHELP, b.PARENT_MENU_IDENTIFIER, b.WORKSPACE_IDENTIFIER,
07:13:20 119  	   b.ENABLE_YN)
07:13:20 120  WHEN MATCHED THEN
07:13:20 121  	UPDATE
07:13:20 122  	   SET a.MENU_LEVEL		= b.MENU_LEVEL,
07:13:20 123  	       a.ITEM_ORDER		= b.ITEM_ORDER,
07:13:20 124  	       a.LABEL			= b.LABEL,
07:13:20 125  	       a.MINIHELP		= b.MINIHELP,
07:13:20 126  	       a.PARENT_MENU_IDENTIFIER = b.PARENT_MENU_IDENTIFIER,
07:13:20 127  	       a.WORKSPACE_IDENTIFIER	= b.WORKSPACE_IDENTIFIER,
07:13:20 128  	       a.ENABLE_YN		= b.ENABLE_YN;

13 rows merged.

07:13:20 hopplant> 
07:13:20 hopplant> --updating the tax repairs import templates so only in scope companies appear
07:13:20 hopplant> INSERT INTO pp_import_lookup
07:13:20   2  	(IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL,
07:13:20   3  	 IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME,
07:13:20   4  	 LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL,
07:13:20   5  	 DERIVED_AUTOCREATE_YN)
07:13:20   6  	SELECT 1122,
07:13:20   7  	       'Company.Description',
07:13:20   8  	       'The passed in value corresponds to the Company: Description field.  Translate to the Company ID using the Description column on the Company table.',
07:13:20   9  	       'company_id',
07:13:20  10  	       '( select co.company_id from company co where upper( trim( <importfield> ) ) = upper( trim( co.description ) ) )',
07:13:20  11  	       0, 'company', 'description', '',
07:13:20  12  	       'select co.description from company_setup cs, company co where cs.company_id = co.company_id and cs.is_tax_repairs_company = 1 union select ''All Companies'' from dual',
07:13:20  13  	       NULL
07:13:20  14  	  FROM dual
07:13:20  15  	 WHERE NOT EXISTS
07:13:20  16  	 (SELECT 1
07:13:20  17  		  FROM pp_import_lookup
07:13:20  18  		 WHERE LOOKUP_VALUES_ALTERNATE_SQL =
07:13:20  19  		       'select co.description from company_setup cs, company co where cs.company_id = co.company_id and cs.is_tax_repairs_company = 1 union select ''All Companies'' from dual');

1 row created.

07:13:20 hopplant> 
07:13:20 hopplant> UPDATE pp_import_template_fields
07:13:20   2  	 SET import_lookup_id =
07:13:20   3  	     1122
07:13:20   4   WHERE (import_template_id, column_name) IN
07:13:20   5  	     (SELECT pf.import_template_id, pf.column_name
07:13:20   6  		FROM pp_import_type_subsystem ts, pp_import_type it,
07:13:20   7  		     pp_import_subsystem ps, pp_import_template pt,
07:13:20   8  		     pp_import_template_fields pf
07:13:20   9  	       WHERE it.import_type_id = ts.import_type_id
07:13:20  10  		 AND ps.import_subsystem_id = ts.import_subsystem_id
07:13:20  11  		 AND pt.import_type_id = it.import_type_id
07:13:20  12  		 AND pf.import_template_id = pt.import_template_id
07:13:20  13  		 AND Lower(pf.column_name) = 'company_id'
07:13:20  14  		 AND Lower(ps.description) = 'tax repairs');

2 rows updated.

07:13:20 hopplant> 
07:13:20 hopplant> update pp_import_lookup x
07:13:20   2  	 set x.lookup_sql = '( select co.company_id from (select company.company_id, company.description from company union select-1, ''ALL COMPANIES'' FROM DUAL) co where upper( trim( <importfield> ) ) = upper( trim( co.description ) ) )'
07:13:20   3   where x.import_lookup_id =
07:13:20   4  	     1122;

1 row updated.

07:13:20 hopplant> 
07:13:20 hopplant> --****************************************************
07:13:20 hopplant> --Log the run of the script to local PP_SCHEMA_LOG
07:13:20 hopplant> --****************************************************
07:13:20 hopplant> INSERT INTO PP_SCHEMA_CHANGE_LOG
07:13:20   2  	      (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
07:13:20   3  	      SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
07:13:20   4  VALUES
07:13:20   5  	      (13723, 0, 2018, 2, 0, 0, 46763, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_046763_taxrpr_02_companies_dml.sql', 1,
07:13:20   6  	      SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
07:13:20   7  	      SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

07:13:20 hopplant> COMMIT;

Commit complete.

07:13:20 hopplant> SPOOL OFF
