14:38:51 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2017.1.0.0_maint_047281_09_lease_default_multicurrency_values_dml.sql
14:38:51 hopplant> /*
14:38:51 hopplant> ||============================================================================
14:38:51 hopplant> || Application: PowerPlan
14:38:51 hopplant> || File Name:   maint_047281_09_lease_default_multicurrency_values_dml.sql
14:38:51 hopplant> ||============================================================================
14:38:51 hopplant> || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
14:38:51 hopplant> ||============================================================================
14:38:51 hopplant> || Version	 Date	    Revised By	     Reason for Change
14:38:51 hopplant> || ---------- ---------- ---------------- ------------------------------------
14:38:51 hopplant> || 2017.1.0.0 04/24/2017 Jared Watkins    default all of the currency-related values necessary
14:38:51 hopplant> ||					     for Lease Multicurrency to function on a new or existing install
14:38:51 hopplant> ||============================================================================
14:38:51 hopplant> */
14:38:51 hopplant> 
14:38:51 hopplant> declare
14:38:51   2  	PPCMSG varchar2(10) := 'PPC-MSG> ';
14:38:51   3  	PPCERR varchar2(10) := 'PPC-ERR> ';
14:38:51   4  	PPCSQL varchar2(10) := 'PPC-SQL> ';
14:38:51   5  	PPCORA varchar2(10) := 'PPC-ORA' || '-> ';
14:38:51   6  
14:38:51   7  	LL_BAD_COUNT number;
14:38:51   8  	-- change value of LB_SKIP_CHECK to TRUE if you determine there is data that is OK but will always
14:38:51   9  	-- show up as an exception.
14:38:51  10  	LB_SKIP_CHECK boolean := false;
14:38:51  11  begin
14:38:51  12  	DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
14:38:51  13  
14:38:51  14  	if not LB_SKIP_CHECK then
14:38:51  15  	  --check here for invalid existing currencies and throw an exception
14:38:51  16  	  select count(*)
14:38:51  17  	    into LL_BAD_COUNT
14:38:51  18  	  from currency
14:38:51  19  	  where (currency_id = 1
14:38:51  20  	    and ((description <> 'USD' and description <> 'United States Dollar') or currency_display_symbol <> '$')
14:38:51  21  	  )
14:38:51  22  	  or (currency_id = 2
14:38:51  23  	    and ((description <> 'EUR' and description <> 'Euro Member Countries') or currency_display_symbol <> '�')
14:38:51  24  	  )
14:38:51  25  	  or (currency_id = 3
14:38:51  26  	    and ((description <> 'MXN' and description <> 'Mexican Peso') or currency_display_symbol <> '$')
14:38:51  27  	  )
14:38:51  28  	  or (currency_id = 4
14:38:51  29  	    and ((description <> 'GBP' and description <> 'United Kingdom Pound') or currency_display_symbol <> '�')
14:38:51  30  	  )
14:38:51  31  	  or (currency_id = 5
14:38:51  32  	    and ((description <> 'CAD' and description <> 'Canadian Dollar') or currency_display_symbol <> '$')
14:38:51  33  	  );
14:38:51  34  
14:38:51  35  	  if LL_BAD_COUNT > 0 then
14:38:51  36  	    DBMS_OUTPUT.PUT_LINE(PPCORA || 'ORA' ||
14:38:51  37  				'-02000 - Contact PPC support for help.  Read message below.');
14:38:51  38  	    DBMS_OUTPUT.PUT_LINE(PPCMSG || '  Please edit the script and read the comments at ');
14:38:51  39  	    DBMS_OUTPUT.PUT_LINE(PPCMSG || '  the bottom to see how to evaluate the data issues.');
14:38:51  40  	    DBMS_OUTPUT.PUT_LINE(PPCMSG || '  To skip checking this in the future.');
14:38:51  41  	    DBMS_OUTPUT.PUT_LINE(PPCMSG || '  change this line: LB_SKIP_CHECK boolean := false;');
14:38:51  42  	    DBMS_OUTPUT.PUT_LINE(PPCMSG || '  to:		LB_SKIP_CHECK boolean := true;');
14:38:51  43  
14:38:51  44  	    RAISE_APPLICATION_ERROR(-20000, 'Currency data issue, contact PPC support for help.');
14:38:51  45  	  else
14:38:51  46  	    DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Currency data is OK.');
14:38:51  47  	  end if;
14:38:51  48  	else
14:38:51  49  	  DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Currency check not performed.');
14:38:51  50  	end if;
14:38:51  51  
14:38:51  52  	--default the CURRENCY table
14:38:51  53  	merge into currency a
14:38:51  54  	  using (select 1 currency_id, 'United States Dollar' description, 1 currency_display_factor,
14:38:51  55  			'$' currency_display_symbol, 'USD' iso_code from dual
14:38:51  56  		    union
14:38:51  57  		 select 2 currency_id, 'Euro Member Countries', 1, '�', 'EUR' from dual
14:38:51  58  		    union
14:38:51  59  		 select 3 currency_id, 'Mexican Peso', 1, '$', 'MXN' from dual
14:38:51  60  		    union
14:38:51  61  		 select 4 currency_id, 'United Kingdom Pound', 1, '�', 'GBP' from dual
14:38:51  62  		    union
14:38:51  63  		 select 5 currency_id, 'Canadian Dollar', 1, '$', 'CAD' from dual) b
14:38:51  64  	  on (a.currency_id = b.currency_id)
14:38:51  65  	  when matched then update set a.description = b.description, a.currency_display_symbol = b.currency_display_symbol, a.iso_code = b.iso_code
14:38:51  66  	  when not matched then insert (currency_id, description, currency_display_factor, currency_display_symbol, iso_code)
14:38:51  67  	    values(b.currency_id, b.description, b.currency_display_factor, b.currency_display_symbol, b.iso_code);
14:38:51  68  
14:38:51  69  	--default CURRENCY_SCHEMA with all the company currencies (set to USD) - for companies without an 'Actuals' row already
14:38:51  70  	INSERT INTO currency_schema(currency_schema_id, company_id, currency_type_id, currency_id)
14:38:51  71  	SELECT max_cs_id + ROWNUM, comp.company_id, 1, 1
14:38:51  72  	FROM company_setup comp, (SELECT MAX(currency_schema_id) max_cs_id FROM currency_schema)
14:38:51  73  	WHERE NOT EXISTS (SELECT 1 FROM currency_schema cs2 WHERE cs2.company_id = comp.company_id AND currency_type_id = 1);
14:38:51  74  
14:38:51  75  	--default all of the MLA contract currencies to USD for MLAs with no current contract currency
14:38:51  76  	UPDATE ls_lease
14:38:51  77  	SET contract_currency_id = 1
14:38:51  78  	WHERE contract_currency_id IS NULL;
14:38:51  79  
14:38:51  80  	--default all of the lease asset contract currencies to the MLA/ILR currency, or USD if not associated with an MLA/ILR
14:38:51  81  	UPDATE ls_asset la
14:38:51  82  	SET contract_currency_id =
14:38:51  83  		  (SELECT contract_currency_id
14:38:51  84  		    FROM ls_lease ll
14:38:51  85  		    INNER JOIN ls_ilr li
14:38:51  86  		    ON ll.lease_id = li.lease_id
14:38:51  87  		    WHERE li.ilr_id = la.ilr_id)
14:38:51  88  	WHERE ilr_id IS NOT NULL;
14:38:51  89  
14:38:51  90  	UPDATE ls_asset
14:38:51  91  	SET contract_currency_id = 1
14:38:51  92  	WHERE contract_currency_id IS NULL;
14:38:51  93  
14:38:51  94  end;
14:38:51  95  /
PPC-ORA-> ORA-02000 - Contact PPC support for help.  Read message below.        
PPC-MSG>   Please edit the script and read the comments at                      
PPC-MSG>   the bottom to see how to evaluate the data issues.                   
PPC-MSG>   To skip checking this in the future.                                 
PPC-MSG>   change this line: LB_SKIP_CHECK boolean := false;                    
PPC-MSG>   to:               LB_SKIP_CHECK boolean := true;                     
declare
*
ERROR at line 1:
ORA-20000: Currency data issue, contact PPC support for help. 
ORA-06512: at line 44 


