14:38:49 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2017.1.0.0_maint_047500_lease_mc_ls_payment_line_view_ddl.sql
14:38:49 hopplant> /*
14:38:49 hopplant> ||============================================================================
14:38:49 hopplant> || Application: PowerPlant
14:38:49 hopplant> || File Name:   maint_047500_lease_mc_ls_payment_line_view_ddl.sql
14:38:49 hopplant> ||============================================================================
14:38:49 hopplant> || Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
14:38:49 hopplant> ||============================================================================
14:38:49 hopplant> || Version	  Date	     Revised By     Reason for Change
14:38:49 hopplant> || --------	  ---------- -------------- ----------------------------------------
14:38:49 hopplant> || 2017.1.0.0  04/21/2017 Anand R	    PP-47500 Create a view to display currency conversion
14:38:49 hopplant> ||============================================================================
14:38:49 hopplant> */
14:38:49 hopplant> 
14:38:49 hopplant> create view v_ls_payment_line_fx as
14:38:49   2  WITH
14:38:49   3  cur AS (
14:38:49   4  	  SELECT /*materialize*/ 1 ls_cur_type, contract_cur.currency_id AS currency_id, contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code
14:38:49   5  	  FROM currency contract_cur
14:38:49   6  	  UNION
14:38:49   7  	  SELECT /*materialize*/ 2, company_cur.currency_id, company_cur.currency_display_symbol, company_cur.iso_code
14:38:49   8  	  FROM currency company_cur
14:38:49   9  	  ),
14:38:49  10  	cr AS (
14:38:49  11  	  SELECT /*materialize*/ exchange_date, currency_from, currency_to, rate from currency_rate
14:38:49  12  	  union
14:38:49  13  	  select /*materialize*/ to_date('19000101' , 'yyyymmdd') exchange_date, currency_from.currency_id currency_from, currency_to.currency_id currency_to,
14:38:49  14  		 decode( currency_from.currency_id - currency_to.currency_id, 0, 1, 0) rate
14:38:49  15  	  from currency currency_from, currency currency_to
14:38:49  16  	)
14:38:49  17  
14:38:49 hopplant> SELECT lpl.payment_id,
14:38:49   2  	     lpl.payment_line_number,
14:38:49   3  	     lpl.payment_type_id,
14:38:49   4  	     lpl.ls_asset_id,
14:38:49   5  	     Round(lpl.amount * cr.rate, 2) amount,
14:38:49   6  	     lpl.gl_posting_mo_yr,
14:38:49   7  	     lpl.description,
14:38:49   8  	     lpl.set_of_books_id,
14:38:49   9  	     Round(lpl.adjustment_amount * cr.rate, 2) adjustment_amount,
14:38:49  10  	     lease.contract_currency_id,
14:38:49  11  	     cs.currency_id company_currency_id,
14:38:49  12  	     cur.ls_cur_type AS ls_cur_type,
14:38:49  13  	     cr.exchange_date,
14:38:49  14  	     --cur.currency_id,
14:38:49  15  	     cr.rate,
14:38:49  16  	     cur.iso_code,
14:38:49  17  	     cur.currency_display_symbol
14:38:49  18  FROM   ls_payment_line lpl
14:38:49  19  INNER JOIN ls_payment_hdr lph
14:38:49  20  	    ON lph.payment_id = lpl.payment_id
14:38:49  21  INNER JOIN ls_lease lease
14:38:49  22  	    ON lph.lease_id = lease.lease_id
14:38:49  23  INNER JOIN currency_schema cs
14:38:49  24  	    ON lph.company_id = cs.company_id
14:38:49  25  INNER JOIN cur
14:38:49  26  	    ON (
14:38:49  27  		(cur.ls_cur_type = 1 AND cur.currency_id = lease.contract_currency_id)
14:38:49  28  		OR
14:38:49  29  		(cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
14:38:49  30  	       )
14:38:49  31  INNER JOIN cr
14:38:49  32  	    ON cr.currency_from =  lease.contract_currency_id
14:38:49  33  	    AND cr.currency_to =  cur.currency_id
14:38:49  34  WHERE cr.exchange_date =	( SELECT /*materialize*/ max(c.exchange_date) from cr c
14:38:49  35  				  WHERE c.currency_from = cr.currency_from
14:38:49  36  				  AND c.currency_to = cr.currency_to
14:38:49  37  				  AND c.exchange_date <= add_months(lph.gl_posting_mo_yr, 1)
14:38:49  38  				)
14:38:49  39  
14:38:49 hopplant> 
14:38:49 hopplant> --****************************************************
14:38:49 hopplant> --Log the run of the script to local PP_SCHEMA_LOG
14:38:49 hopplant> --****************************************************
14:38:49 hopplant> INSERT INTO PP_SCHEMA_CHANGE_LOG
14:38:49   2  	      (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
14:38:49   3  	      SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
14:38:49   4  VALUES
14:38:49   5  	      (3446, 0, 2017, 1, 0, 0, 47500, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047500_lease_mc_ls_payment_line_view_ddl.sql', 1,
14:38:49   6  	      SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
14:38:49   7  	      SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

14:38:49 hopplant> COMMIT;

Commit complete.

14:38:49 hopplant> SPOOL OFF
