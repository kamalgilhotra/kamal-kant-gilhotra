12:12:44 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2015.1.0.0_maint_041083_sys_tblgrp_clean.sql
12:12:44 hopplant>  /*
12:12:44 hopplant>  ||============================================================================
12:12:44 hopplant>  || Application: PowerPlan
12:12:44 hopplant>  || File Name: maint_041083_sys_tblgrp_clean.sql
12:12:44 hopplant>  ||============================================================================
12:12:44 hopplant>  || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
12:12:44 hopplant>  ||============================================================================
12:12:44 hopplant>  || Version	  Date	     Revised By     Reason for Change
12:12:44 hopplant>  || ---------- ---------- -------------- ----------------------------------------
12:12:44 hopplant>  || 2015.1.0.0 11/06/2014 A Scott	    some earlier maintenance scripts inserted
12:12:44 hopplant>  ||					    into pp table groups for "system" when setting
12:12:44 hopplant>  ||					    up for table maint.  This should not have been
12:12:44 hopplant>  ||					    done.  If a client does not use table security,
12:12:44 hopplant>  ||					    having a single row in the table turns it on.
12:12:44 hopplant>  ||============================================================================
12:12:44 hopplant>  */
12:12:44 hopplant> 
12:12:44 hopplant> SET SERVEROUTPUT ON
12:12:44 hopplant> 
12:12:44 hopplant> ----check for count of distinct "groups" in pp table groups.
12:12:44 hopplant> ----if there's only one distinct group in pp table groups.
12:12:44 hopplant> ----  if that group is "system"
12:12:44 hopplant> ----     if there are less than 10 rows of data in pp_table_groups THEN
12:12:44 hopplant> ----        delete from pp_table_groups;
12:12:44 hopplant> ----     end if
12:12:44 hopplant> ----  end if
12:12:44 hopplant> ----end if
12:12:44 hopplant> ----So do nothing if any of the above if statements are not true.
12:12:44 hopplant> declare
12:12:44   2  	 COUNT_GROUPS number(22, 0);
12:12:44   3  	 GROUP_NAME   varchar2(35);
12:12:44   4  	 COUNT_ROWS   number(22, 0);
12:12:44   5  	 THRESHOLD    number(22, 0) := 10;
12:12:44   6  	 ROWS_DELETED number(22, 0);
12:12:44   7  begin
12:12:44   8  
12:12:44   9  	 select count(distinct GROUPS) into COUNT_GROUPS from PP_TABLE_GROUPS;
12:12:44  10  
12:12:44  11  	 DBMS_OUTPUT.PUT_LINE(COUNT_GROUPS || ' distinct group(s) used in pp table groups.');
12:12:44  12  
12:12:44  13  	 if COUNT_GROUPS = 1 then
12:12:44  14  
12:12:44  15  	    select LOWER(trim(GROUPS)) into GROUP_NAME from PP_TABLE_GROUPS where ROWNUM = 1;
12:12:44  16  
12:12:44  17  	    DBMS_OUTPUT.PUT_LINE('The only group in use is "' || GROUP_NAME || '".');
12:12:44  18  
12:12:44  19  	    if GROUP_NAME = 'system' then
12:12:44  20  
12:12:44  21  	       select count(*) into COUNT_ROWS from PP_TABLE_GROUPS;
12:12:44  22  
12:12:44  23  	       DBMS_OUTPUT.PUT_LINE('There are ' || COUNT_ROWS || ' rows in pp table groups.');
12:12:44  24  
12:12:44  25  	       if COUNT_ROWS <= THRESHOLD then
12:12:44  26  		  DBMS_OUTPUT.PUT_LINE('This is less than or equal to the threshold of 10.');
12:12:44  27  		  DBMS_OUTPUT.PUT_LINE('This means that previous scripts erroneously inserted into pp table groups ');
12:12:44  28  		  DBMS_OUTPUT.PUT_LINE('when table security was not in use. This is being corrected now.');
12:12:44  29  
12:12:44  30  		  delete from PP_TABLE_GROUPS;
12:12:44  31  
12:12:44  32  		  ROWS_DELETED := sql%rowcount;
12:12:44  33  		  DBMS_OUTPUT.PUT_LINE(ROWS_DELETED || ' rows were deleted from pp table groups.');
12:12:44  34  
12:12:44  35  	       end if;
12:12:44  36  
12:12:44  37  	    end if;
12:12:44  38  
12:12:44  39  	 end if;
12:12:44  40  
12:12:44  41  exception
12:12:44  42  	 when others then
12:12:44  43  	    DBMS_OUTPUT.PUT_LINE('Error discovered in checks used to determine in pp table groups is not in used and should be cleared.');
12:12:44  44  end;
12:12:44  45  /
2 distinct group(s) used in pp table groups.                                    

PL/SQL procedure successfully completed.

12:12:44 hopplant> 
12:12:44 hopplant> 
12:12:44 hopplant> 
12:12:44 hopplant> --**************************
12:12:44 hopplant> -- Log the run of the script
12:12:44 hopplant> --**************************
12:12:44 hopplant> 
12:12:44 hopplant> insert into PP_SCHEMA_CHANGE_LOG
12:12:44   2  	 (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
12:12:44   3  	  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
12:12:44   4  values
12:12:44   5  	 (2008, 0, 2015, 1, 0, 0, 41083, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041083_sys_tblgrp_clean.sql', 1,
12:12:44   6  	  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
12:12:44   7  	  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

12:12:44 hopplant> commit;

Commit complete.

12:12:44 hopplant> SPOOL OFF
