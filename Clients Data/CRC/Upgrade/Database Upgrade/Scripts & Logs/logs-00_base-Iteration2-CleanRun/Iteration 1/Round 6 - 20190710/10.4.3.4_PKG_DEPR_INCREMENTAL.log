11:59:41 hopplant> @&&PP_SCRIPT_PATH.packages_released\10.4.3.4_PKG_DEPR_INCREMENTAL.sql
11:59:41 hopplant> /*
11:59:41 hopplant> ||============================================================================
11:59:41 hopplant> || Application: PowerPlant
11:59:41 hopplant> || File Name:   maint_037397_depr_PKG_DEPR_INCREMENTAL.sql
11:59:41 hopplant> ||============================================================================
11:59:41 hopplant> || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
11:59:41 hopplant> ||============================================================================
11:59:41 hopplant> || Version  Date	  Revised By	   Reason for Change
11:59:41 hopplant> || -------- ---------- ---------------- --------------------------------------
11:59:41 hopplant> || 10.4.2.0 03/31/2014 Charlie Shilling
11:59:41 hopplant> ||============================================================================
11:59:41 hopplant> */
11:59:41 hopplant> 
11:59:41 hopplant> create or replace package PKG_DEPR_INCREMENTAL
11:59:41   2  /*
11:59:41   3  ||============================================================================
11:59:41   4  || Application: PowerPlant
11:59:41   5  || Object Name: PKG_DEPR_INCREMENTAL
11:59:41   6  || Description: Loads and handles the results for Incremental Depreciation
11:59:41   7  ||============================================================================
11:59:41   8  || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
11:59:41   9  ||============================================================================
11:59:41  10  || Version Date	    Revised By	   Reason for Change
11:59:41  11  || ------- ---------- -------------- -----------------------------------------
11:59:41  12  || 1.0	 02/10/2014 Brandon Beck     Create
11:59:41  13  || 2.0	 3/27/2015  Anand R	   PP-43314 - Add NVL around rwip allocation
11:59:41  14  ||============================================================================
11:59:41  15  */
11:59:41  16   as
11:59:41  17  	      /*
11:59:41  18  	      *       This procedure stages data from fcst_depr_ledger_fp into the staging table for depreciation calculation
11:59:41  19  	      *
11:59:41  20  	      */
11:59:41  21  	 procedure P_DEPRSTAGE
11:59:41  22  	 (
11:59:41  23  		      A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
11:59:41  24  		      A_FP_IDS PKG_PP_COMMON.NUM_TABTYPE, A_REVISIONS PKG_PP_COMMON.NUM_TABTYPE,
11:59:41  25  		      A_MONTHS PKG_PP_COMMON.DATE_TABTYPE
11:59:41  26  	      );
11:59:41  27  
11:59:41  28  	      /*
11:59:41  29  	      *       Handles the results of the incremental depreciation calculation
11:59:41  30  	      */
11:59:41  31  	      procedure P_HANDLERESULTS;
11:59:41  32  end PKG_DEPR_INCREMENTAL;
11:59:41  33  /

Package created.

11:59:42 hopplant> 
11:59:42 hopplant> 
11:59:42 hopplant> create or replace package body PKG_DEPR_INCREMENTAL as
11:59:42   2  	 --**************************************************************
11:59:42   3  	 --	  TYPE DECLARATIONS
11:59:42   4  	 --**************************************************************
11:59:42   5  
11:59:42   6  	 --**************************************************************
11:59:42   7  	 --	  VARIABLES
11:59:42   8  	 --**************************************************************
11:59:42   9  	 G_VERSION_ID	   FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%type;
11:59:42  10  	 G_MONTHS	   PKG_PP_COMMON.DATE_TABTYPE;
11:59:42  11  	 G_RTN		 NUMBER;
11:59:42  12  
11:59:42  13  	 --**************************************************************
11:59:42  14  	 --	  PROCEDURES
11:59:42  15  	 --**************************************************************
11:59:42  16  
11:59:42  17  	 --**************************************************************************
11:59:42  18  	 --			       P_DEPRSTAGE
11:59:42  19  	 --**************************************************************************
11:59:42  20  	 --
11:59:42  21  	 --  Loads the global temp table for calculating depreciation forecast.
11:59:42  22  	 -- THE Load is based on fcst depr ledger for a version id
11:59:42  23  	 -- AND a single month
11:59:42  24  	 --
11:59:42  25  	      procedure P_DEPRSTAGE(
11:59:42  26  		      A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
11:59:42  27  		      A_FP_IDS PKG_PP_COMMON.NUM_TABTYPE, A_REVISIONS PKG_PP_COMMON.NUM_TABTYPE,
11:59:42  28  		      A_MONTHS PKG_PP_COMMON.DATE_TABTYPE
11:59:42  29  	      ) is
11:59:42  30  	      my_first_month date;
11:59:42  31  	      my_last_month date;
11:59:42  32  	 begin
11:59:42  33  		      PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_INCREMENTAL.P_DEPRSTAGE');
11:59:42  34  
11:59:42  35  		      my_first_month := a_months( a_months.FIRST );
11:59:42  36  		      my_last_month := a_months( a_months.LAST );
11:59:42  37  
11:59:42  38  		      FORALL indx in indices of A_FP_IDS
11:59:42  39  			      insert into DEPR_CALC_STG
11:59:42  40  			      (
11:59:42  41  				      DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
11:59:42  42  				      DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, SMOOTH_CURVE,
11:59:42  43  				      DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD, COMPANY_ID, SUBLEDGER_TYPE_ID,
11:59:42  44  				      DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV, MID_PERIOD_METHOD,
11:59:42  45  				      RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE,
11:59:42  46  				      END_OF_LIFE, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE,
11:59:42  47  				      COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT,
11:59:42  48  				      SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE, DEPR_LEDGER_STATUS,
11:59:42  49  				      BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE,
11:59:42  50  				      RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
11:59:42  51  				      RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR, BEGIN_BALANCE,
11:59:42  52  				      ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE,
11:59:42  53  				      END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
11:59:42  54  				      DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS,
11:59:42  55  				      SALVAGE_CASH, RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT,
11:59:42  56  				      GAIN_LOSS, VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE,
11:59:42  57  				      CURRENT_NET_SALVAGE_AMORT, CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG,
11:59:42  58  				      IMPAIRMENT_RESERVE_ACT, IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION,
11:59:42  59  				      COR_BEG_RESERVE, COR_EXPENSE, COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN,
11:59:42  60  				      COR_RES_TRAN_OUT, COR_RES_ADJUST, COR_END_RESERVE, COST_OF_REMOVAL_RATE,
11:59:42  61  				      COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL, RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS,
11:59:42  62  				      RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST,
11:59:42  63  				      SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT,
11:59:42  64  				      RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER,
11:59:42  65  				      IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT,
11:59:42  66  				      ORIG_DEPR_EXPENSE, ORIG_DEPR_EXP_ALLOC_ADJUST, ORIG_SALV_EXPENSE,
11:59:42  67  				      ORIG_SALV_EXP_ALLOC_ADJUST, ORIG_COR_EXP_ALLOC_ADJUST, ORIG_COR_EXPENSE,
11:59:42  68  				      ORIG_BEGIN_RESERVE, ORIG_COR_BEG_RESERVE, impairment_asset_activity_salv, impairment_asset_begin_balance,
11:59:42  69  				      FCST_DEPR_VERSION_ID, curve_trueup_adj, CURVE_TRUEUP_ADJ_SALV, CURVE_TRUEUP_ADJ_COR,
11:59:42  70  				      SPREAD_FACTOR_ID, FUNDING_WO_ID, REVISION
11:59:42  71  			      )
11:59:42  72  			      select DL.FCST_DEPR_GROUP_ID, --	depr_group_id			NULL,
11:59:42  73  				      DL.SET_OF_BOOKS_ID, --  set_of_books_id		   NUMBER(22,0)   NULL,
11:59:42  74  				      DL.GL_POST_MO_YR, --  gl_post_mo_yr		 DATE		NULL,
11:59:42  75  				      DL.GL_POST_MO_YR,   --  calc_month		   DATE 	  NULL,
11:59:42  76  				      1, --  depr_calc_status		  NUMBER(2,0)	 NULL,
11:59:42  77  				      '', --  depr_calc_message 	   VARCHAR2(2000) NULL,
11:59:42  78  				      LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
11:59:42  79  															       DGV.COMPANY_ID),
11:59:42  80  						 'no')), --  trf_in_est_adds		  VARCHAR2(35)	 NULL,
11:59:42  81  				      LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
11:59:42  82  															       DGV.COMPANY_ID),
11:59:42  83  						 'no')), --  include_rwip_in_net	  VARCHAR2(35)	 NULL,
11:59:42  84  				      LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('DEPR - Smooth Curve True-up',
11:59:42  85  															       DGV.COMPANY_ID),
11:59:42  86  						 '0')), -- smooth_curve
11:59:42  87  				      0, --  dnsa_cor_bal		  NUMBER(22,2)	 NULL,
11:59:42  88  				      0, --  dnsa_salv_bal		  NUMBER(22,2)	 NULL,
11:59:42  89  				      0, --  cor_ytd			  NUMBER(22,2)	 NULL,
11:59:42  90  				      0, --  salv_ytd			  NUMBER(22,2)	 NULL,
11:59:42  91  				      DGV.COMPANY_ID, --  company_id		       NUMBER(22,0)   NULL,
11:59:42  92  				      DGV.SUBLEDGER_TYPE_ID, --  subledger_type_id	      NUMBER(22,0)   NULL,
11:59:42  93  				      DGV.DESCRIPTION, --  description			VARCHAR2(254)  NULL,
11:59:42  94  				      DGV.FCST_DEPR_METHOD_ID, --  depr_method_id		NUMBER(22,0)   NULL,
11:59:42  95  				      DGV.MID_PERIOD_CONV, --  mid_period_conv		    NUMBER(22,2)   NULL,
11:59:42  96  				      lower(trim(DGV.MID_PERIOD_METHOD)), --  mid_period_method 	   VARCHAR2(35)   NULL,
11:59:42  97  				      DMR.RATE, --  rate			 NUMBER(22,8)	NULL,
11:59:42  98  				      DMR.NET_GROSS, --  net_gross		      NUMBER(22,0)   NULL,
11:59:42  99  				      NVL(DMR.OVER_DEPR_CHECK, 0), --  over_depr_check		    NUMBER(22,0)   NULL,
11:59:42 100  				      DMR.NET_SALVAGE_PCT, --  net_salvage_pct		    NUMBER(22,8)   NULL,
11:59:42 101  				      NVL(DMR.RATE_USED_CODE, 0), --  rate_used_code		   NUMBER(22,0)   NULL,
11:59:42 102  				      DMR.END_OF_LIFE, --  end_of_life			NUMBER(22,0)   NULL,
11:59:42 103  				      NVL(DMR.EXPECTED_AVERAGE_LIFE, 0), --  expected_average_life	  NUMBER(22,0)	 NULL,
11:59:42 104  				      NVL(DMR.RESERVE_RATIO_ID, 0), --	reserve_ratio_id	     NUMBER(22,0)   NULL,
11:59:42 105  				      DMR.COST_OF_REMOVAL_RATE, --  dmr_cost_of_removal_rate	 NUMBER(22,8)	NULL,
11:59:42 106  				      DMR.COST_OF_REMOVAL_PCT, --  cost_of_removal_pct		NUMBER(22,8)   NULL,
11:59:42 107  				      DMR.SALVAGE_RATE, --  dmr_salvage_rate		 NUMBER(22,8)	NULL,
11:59:42 108  				      DMR.INTEREST_RATE, --  interest_rate		  NUMBER(22,8)	 NULL,
11:59:42 109  				      NVL(DMR.AMORTIZABLE_LIFE, 0), --	amortizable_life	     NUMBER(22,0)   NULL,
11:59:42 110  				      DECODE(LOWER(NVL(DMR.COR_TREATMENT, 'no')), 'no', 0, 1), --  cor_treatment		NUMBER(1,0)    NULL,
11:59:42 111  				      DECODE(LOWER(NVL(DMR.SALVAGE_TREATMENT, 'no')), 'no', 0, 1), --  salvage_treatment	    NUMBER(1,0)    NULL,
11:59:42 112  				      NVL(DMR.NET_SALVAGE_AMORT_LIFE, 0), --  net_salvage_amort_life	   NUMBER(22,0)   NULL,
11:59:42 113  				      DMR.ALLOCATION_PROCEDURE, --  allocation_procedure	 VARCHAR2(5)	NULL,
11:59:42 114  				      DL.DEPR_LEDGER_STATUS, --  depr_ledger_status	      NUMBER(22,0)   NULL,
11:59:42 115  				      nvl(DL.BEGIN_RESERVE,0), --  begin_reserve		NUMBER(22,2)   NULL,
11:59:42 116  				      0 AS END_RESERVE, --  end_reserve 		 NUMBER(22,2)	NULL,
11:59:42 117  				      DL.RESERVE_BAL_PROVISION, --  reserve_bal_provision	 NUMBER(22,2)	NULL,
11:59:42 118  				      DL.RESERVE_BAL_COR, --  reserve_bal_cor		   NUMBER(22,2)   NULL,
11:59:42 119  				      DL.SALVAGE_BALANCE, --  salvage_balance		   NUMBER(22,2)   NULL,
11:59:42 120  				      DL.RESERVE_BAL_ADJUST, --  reserve_bal_adjust	      NUMBER(22,2)   NULL,
11:59:42 121  				      DL.RESERVE_BAL_RETIREMENTS, --  reserve_bal_retirements	   NUMBER(22,2)   NULL,
11:59:42 122  				      DL.RESERVE_BAL_TRAN_IN, --  reserve_bal_tran_in	       NUMBER(22,2)   NULL,
11:59:42 123  				      DL.RESERVE_BAL_TRAN_OUT, --  reserve_bal_tran_out 	NUMBER(22,2)   NULL,
11:59:42 124  				      DL.RESERVE_BAL_OTHER_CREDITS, --	reserve_bal_other_credits    NUMBER(22,2)   NULL,
11:59:42 125  				      DL.RESERVE_BAL_GAIN_LOSS, --  reserve_bal_gain_loss	 NUMBER(22,2)	NULL,
11:59:42 126  				      DL.RESERVE_ALLOC_FACTOR, --  reserve_alloc_factor 	NUMBER(22,2)   NULL,
11:59:42 127  				      DL.BEGIN_BALANCE, --  begin_balance		 NUMBER(22,2)	NULL,
11:59:42 128  				      DL.ADDITIONS, --	additions
11:59:42 129  				      DL.RETIREMENTS, --  retirements		       NUMBER(22,2)   NULL,
11:59:42 130  				      DL.TRANSFERS_IN, --  transfers_in 		NUMBER(22,2)   NULL,
11:59:42 131  				      DL.TRANSFERS_OUT, --  transfers_out		 NUMBER(22,2)	NULL,
11:59:42 132  				      DL.ADJUSTMENTS, --  adjustments		       NUMBER(22,2)   NULL,
11:59:42 133  				      0 AS DEPRECIATION_BASE, --  depreciation_base	       NUMBER(22,2)
11:59:42 134  				      0 AS END_BALANCE, --  end_balance 		 NUMBER(22,2)	NULL
11:59:42 135  				      decode(DL.DEPR_LEDGER_STATUS, 1, DL.DEPRECIATION_RATE, DMR.RATE), --  depreciation_rate		 NUMBER(22,8)	NULL,
11:59:42 136  				      0 AS DEPRECIATION_EXPENSE, --  depreciation_expense	  NUMBER(22,2)	 NULL,
11:59:42 137  				      DL.DEPR_EXP_ADJUST, --  depr_exp_adjust		   NUMBER(22,2)   NULL,
11:59:42 138  				      0 AS DEPR_EXP_ALLOC_ADJUST, --  depr_exp_alloc_adjust	   NUMBER(22,2)   NULL,
11:59:42 139  				      DL.COST_OF_REMOVAL, --  cost_of_removal		   NUMBER(22,2)   NULL,
11:59:42 140  				      DL.RESERVE_RETIREMENTS, --  reserve_retirements	       NUMBER(22,2)   NULL,
11:59:42 141  				      DL.SALVAGE_RETURNS, --  salvage_returns		   NUMBER(22,2)   NULL,
11:59:42 142  				      DL.SALVAGE_CASH, --  salvage_cash 		NUMBER(22,2)   NULL,
11:59:42 143  				      DL.RESERVE_CREDITS, --  reserve_credits		   NUMBER(22,2)   NULL,
11:59:42 144  				      DL.RESERVE_ADJUSTMENTS, --  reserve_adjustments	       NUMBER(22,2)   NULL,
11:59:42 145  				      DL.RESERVE_TRAN_IN, --  reserve_tran_in		   NUMBER(22,2)   NULL,
11:59:42 146  				      DL.RESERVE_TRAN_OUT, --  reserve_tran_out 	    NUMBER(22,2)   NULL,
11:59:42 147  				      DL.GAIN_LOSS, --	gain_loss		     NUMBER(22,2)   NULL,
11:59:42 148  				      DL.VINTAGE_NET_SALVAGE_AMORT, --	vintage_net_salvage_amort    NUMBER(22,2)   NULL,
11:59:42 149  				      DL.VINTAGE_NET_SALVAGE_RESERVE, --  vintage_net_salvage_reserve  NUMBER(22,2)   NULL,
11:59:42 150  				      DL.CURRENT_NET_SALVAGE_AMORT, --	current_net_salvage_amort    NUMBER(22,2)   NULL,
11:59:42 151  				      DL.CURRENT_NET_SALVAGE_RESERVE, --  current_net_salvage_reserve  NUMBER(22,2)   NULL,
11:59:42 152  				      DL.IMPAIRMENT_RESERVE_BEG, --  impairment_reserve_beg	  NUMBER(22,2)	 NULL,
11:59:42 153  				      DL.IMPAIRMENT_RESERVE_ACT, --  impairment_reserve_act	  NUMBER(22,2)	 NULL,
11:59:42 154  				      DL.IMPAIRMENT_RESERVE_END, --  impairment_reserve_end	  NUMBER(22,2)	 NULL,
11:59:42 155  				      DL.EST_ANN_NET_ADDS, --  est_ann_net_adds 	    NUMBER(22,2)   NULL,
11:59:42 156  				      NVL(DL.RWIP_ALLOCATION, 0), --  rwip_allocation		   NUMBER(22,2)   NULL,
11:59:42 157  				      DL.COR_BEG_RESERVE, --  cor_beg_reserve		   NUMBER(22,2)   NULL,
11:59:42 158  				      0 AS COR_EXPENSE, --  cor_expense 		 NUMBER(22,2)	NULL,
11:59:42 159  				      NVL(DL.COR_EXP_ADJUST,0), --  cor_exp_adjust		 NUMBER(22,2)	NULL,
11:59:42 160  				      0 AS COR_EXP_ALLOC_ADJUST, --  cor_exp_alloc_adjust	  NUMBER(22,2)	 NULL,
11:59:42 161  				      DL.COR_RES_TRAN_IN, --  cor_res_tran_in		   NUMBER(22,2)   NULL,
11:59:42 162  				      DL.COR_RES_TRAN_OUT, --  cor_res_tran_out 	    NUMBER(22,2)   NULL,
11:59:42 163  				      DL.COR_RES_ADJUST, --  cor_res_adjust		  NUMBER(22,2)	 NULL,
11:59:42 164  				      0 AS COR_END_RESERVE, --	cor_end_reserve 	     NUMBER(22,2)   NULL,
11:59:42 165  				      DL.COST_OF_REMOVAL_RATE, --  cost_of_removal_rate 	NUMBER(22,8)   NULL,
11:59:42 166  				      DL.COST_OF_REMOVAL_BASE, --  cost_of_removal_base 	NUMBER(22,2)   NULL,
11:59:42 167  				      nvl(DL.RWIP_COST_OF_REMOVAL,0), --  rwip_cost_of_removal	       NUMBER(22,2)   NULL,
11:59:42 168  				      nvl(DL.RWIP_SALVAGE_CASH,0), --  rwip_salvage_cash	    NUMBER(22,2)   NULL,
11:59:42 169  				      nvl(DL.RWIP_SALVAGE_RETURNS,0), --  rwip_salvage_returns	       NUMBER(22,2)   NULL,
11:59:42 170  				      nvl(DL.RWIP_RESERVE_CREDITS,0), --  rwip_reserve_credits	       NUMBER(22,2)   NULL,
11:59:42 171  				      nvl(DL.SALVAGE_RATE,0), --  salvage_rate		       NUMBER(22,8)   NULL,
11:59:42 172  				      nvl(SALVAGE_BASE,0), --  salvage_base		    NUMBER(22,2)   NULL,
11:59:42 173  				      nvl(SALVAGE_EXPENSE,0), --  salvage_expense	       NUMBER(22,2)   NULL,
11:59:42 174  				      nvl(DL.SALVAGE_EXP_ADJUST,0), --	salvage_exp_adjust	     NUMBER(22,2)   NULL,
11:59:42 175  				      nvl(SALVAGE_EXP_ALLOC_ADJUST,0), --  salvage_exp_alloc_adjust	NUMBER(22,2)   NULL,
11:59:42 176  				      nvl(DL.RESERVE_BAL_SALVAGE_EXP,0), --  reserve_bal_salvage_exp	  NUMBER(22,2)	 NULL,
11:59:42 177  				      nvl(DL.RESERVE_BLENDING_ADJUSTMENT,0), --  reserve_blending_adjustment  NUMBER(22,2)   NULL,
11:59:42 178  				      nvl(DL.RESERVE_BLENDING_TRANSFER,0), --  reserve_blending_transfer    NUMBER(22,2)   NULL,
11:59:42 179  				      nvl(DL.COR_BLENDING_ADJUSTMENT,0), --  cor_blending_adjustment	  NUMBER(22,2)	 NULL,
11:59:42 180  				      nvl(DL.COR_BLENDING_TRANSFER,0), --  cor_blending_transfer	NUMBER(22,2)   NULL,
11:59:42 181  				      nvl(DL.IMPAIRMENT_ASSET_AMOUNT,0), --  impairment_asset_amount	  NUMBER(22,2)	 NULL,
11:59:42 182  				      nvl(DL.IMPAIRMENT_EXPENSE_AMOUNT,0), --  impairment_expense_amount    NUMBER(22,2)   NULL,
11:59:42 183  				      nvl(DL.RESERVE_BAL_IMPAIRMENT,0), --  reserve_bal_impairment	 NUMBER(22,2)	NUll,
11:59:42 184  				      0, --ORIG_DEPR_EXPENSE
11:59:42 185  				      0, --  orig_depr_exp_alloc_adjust   NUMBER(22,2)	 NULL,
11:59:42 186  				      0, --  orig_salv_expense		  NUMBER(22,2)	 NULL,
11:59:42 187  				      0, --  orig_salv_exp_alloc_adjust   NUMBER(22,2)	 NULL,
11:59:42 188  				      0, --  orig_cor_exp_alloc_adjust	  NUMBER(22,2)	 NULL,
11:59:42 189  				      0, --  orig_cor_expense		  NUMBER(22,2)	 NULL
11:59:42 190  				      nvl(DL.BEGIN_RESERVE,0), --orig_begin_reserve	    NUMBER(22,2) DEFAULT 0  NULL,
11:59:42 191  				      DL.COR_BEG_RESERVE, --  orig_cor_begin_reserve	 NUMBER(22,2) DEFAULT 0 NULL
11:59:42 192  				      dl.impairment_asset_activity_salv, dl.impairment_asset_begin_balance,
11:59:42 193  				      A_VERSION_ID,
11:59:42 194  				      0 as curve_trueup_adj,
11:59:42 195  				      0 as CURVE_TRUEUP_ADJ_SALV,
11:59:42 196  				      0 as CURVE_TRUEUP_ADJ_COR,
11:59:42 197  				      DGV.FACTOR_ID,
11:59:42 198  				      DL.FUNDING_WO_ID, DL.REVISION
11:59:42 199  			      from FCST_DEPR_METHOD_RATES DMR, FCST_DEPR_LEDGER_FP DL, FCST_DEPR_GROUP_VERSION DGV
11:59:42 200  			 where DMR.FCST_DEPR_METHOD_ID = DGV.FCST_DEPR_METHOD_ID
11:59:42 201  			       and DMR.FCST_DEPR_VERSION_ID = DGV.FCST_DEPR_VERSION_ID
11:59:42 202  			       and DMR.EFFECTIVE_DATE =
11:59:42 203  				      (select max(DMR1.EFFECTIVE_DATE)
11:59:42 204  					from FCST_DEPR_METHOD_RATES DMR1
11:59:42 205  				       where DMR.FCST_DEPR_METHOD_ID = DMR1.FCST_DEPR_METHOD_ID
11:59:42 206  					 and DMR.FCST_DEPR_VERSION_ID = DMR1.FCST_DEPR_VERSION_ID
11:59:42 207  					 and DMR.SET_OF_BOOKS_ID = DMR1.SET_OF_BOOKS_ID
11:59:42 208  					 and DMR1.EFFECTIVE_DATE <= DL.GL_POST_MO_YR)
11:59:42 209  			       and DL.FCST_DEPR_GROUP_ID = DGV.FCST_DEPR_GROUP_ID
11:59:42 210  			       and DL.FCST_DEPR_VERSION_ID = DGV.FCST_DEPR_VERSION_ID
11:59:42 211  			       and DL.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
11:59:42 212  			       and DGV.FCST_DEPR_VERSION_ID = A_VERSION_ID
11:59:42 213  			       and DL.FUNDING_WO_ID = a_fp_ids(indx)
11:59:42 214  			       and DL.REVISION = a_revisions(indx)
11:59:42 215  			       and dl.gl_post_mo_yr between my_first_month and my_last_month
11:59:42 216  			       --Either we don't ignore inactive depr groups, or the depr group is active
11:59:42 217  			       and (
11:59:42 218  				      LOWER(trim(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Ignore Inactive Depr Groups',DGV.COMPANY_ID),'no'))) = 'no'
11:59:42 219  				      or NVL(DGV.STATUS_ID, 1) = 1 or DL.END_BALANCE <> 0 or DL.END_RESERVE <> 0 or
11:59:42 220  				      DL.COR_END_RESERVE <> 0 or DL.BEGIN_BALANCE <> 0 or DL.BEGIN_RESERVE <> 0 or
11:59:42 221  				      DL.COR_BEG_RESERVE <> 0
11:59:42 222  			       )
11:59:42 223  			       ;
11:59:42 224  
11:59:42 225  		      G_RTN := analyze_table('DEPR_CALC_STG',100);
11:59:42 226  
11:59:42 227  		      PKG_PP_ERROR.REMOVE_MODULE_NAME;
11:59:42 228  	      exception
11:59:42 229  		      when others then
11:59:42 230  			      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
11:59:42 231  	 end P_DEPRSTAGE;
11:59:42 232  
11:59:42 233  	      procedure P_HANDLERESULTS
11:59:42 234  	      is
11:59:42 235  	      begin
11:59:42 236  		      PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_INCREMENTAL.P_HANDLERESULTS');
11:59:42 237  
11:59:42 238  		      update fcst_depr_ledger_fp d set (
11:59:42 239  			      begin_reserve, end_reserve, begin_balance, end_balance,
11:59:42 240  			      depreciation_base, depreciation_rate, depreciation_expense, depr_exp_alloc_adjust,
11:59:42 241  			      salvage_base, salvage_rate, salvage_expense, salvage_exp_alloc_adjust, salvage_balance,
11:59:42 242  			      cost_of_removal_base, cost_of_removal_rate, cor_expense, cor_exp_alloc_adjust, cor_beg_reserve, cor_end_reserve,
11:59:42 243  			      impairment_reserve_end, reserve_bal_impairment,  reserve_bal_provision,
11:59:42 244  			      reserve_bal_cor,	reserve_bal_adjust, reserve_bal_retirements,
11:59:42 245  			      reserve_bal_tran_in, reserve_bal_tran_out, reserve_bal_gain_loss,
11:59:42 246  			      reserve_bal_salvage_exp, est_ann_net_adds,
11:59:42 247  			      cor_blending_adjustment, cor_blending_transfer,
11:59:42 248  			      impairment_asset_amount, impairment_expense_amount,
11:59:42 249  			      reserve_blending_adjustment, reserve_blending_transfer,
11:59:42 250  			      rwip_cost_of_removal, rwip_reserve_credits, rwip_salvage_cash, rwip_salvage_returns,
11:59:42 251  			      salvage_exp_adjust
11:59:42 252  		       ) =
11:59:42 253  		      (  select s.begin_reserve, s.end_reserve, s.begin_balance, s.end_balance,
11:59:42 254  			      s.depreciation_base, s.depreciation_rate, s.depreciation_expense, (s.depr_exp_alloc_adjust + s.over_depr_adj + s.retro_depr_adj + s.curve_trueup_adj),
11:59:42 255  			      s.salvage_base, s.salvage_rate, s.salvage_expense, (s.salvage_exp_alloc_adjust + s.over_depr_adj_salv + s.retro_salv_adj + s.curve_trueup_adj_salv), s.salvage_balance,
11:59:42 256  			      s.cost_of_removal_base, s.cost_of_removal_rate, s.cor_expense,
11:59:42 257  			      (s.cor_exp_alloc_adjust + s.over_depr_Adj_cor + s.retro_cor_adj + s.curve_trueup_adj_Cor), s.cor_beg_reserve, s.cor_end_reserve,
11:59:42 258  			      s.impairment_reserve_end, s.reserve_bal_impairment,  s.reserve_bal_provision,
11:59:42 259  			      s.reserve_bal_cor,  s.reserve_bal_adjust, s.reserve_bal_retirements,
11:59:42 260  			      s.reserve_bal_tran_in, s.reserve_bal_tran_out, s.reserve_bal_gain_loss,
11:59:42 261  			      s.reserve_bal_salvage_exp, s.est_ann_net_adds,
11:59:42 262  			      s.cor_blending_adjustment, s.cor_blending_transfer,
11:59:42 263  			      s.impairment_asset_amount, s.impairment_expense_amount,
11:59:42 264  			      s.reserve_blending_adjustment, s.reserve_blending_transfer,
11:59:42 265  			      s.rwip_cost_of_removal, s.rwip_reserve_credits, s.rwip_salvage_cash, s.rwip_salvage_returns,
11:59:42 266  			      s.salvage_exp_adjust
11:59:42 267  			      from depr_calc_stg s
11:59:42 268  			      where d.fcst_depr_group_id = s.depr_group_id
11:59:42 269  			      and d.set_of_books_id = s.set_of_books_id
11:59:42 270  			      and d.gl_post_mo_yr = s.gl_post_mo_yr
11:59:42 271  			      and d.fcst_depr_version_id = s.fcst_depr_version_id
11:59:42 272  			      and s.depr_calc_status = 9
11:59:42 273  		      )
11:59:42 274  		      where exists (
11:59:42 275  			      select 1
11:59:42 276  			      from depr_calc_stg s
11:59:42 277  			      where d.fcst_depr_group_id = s.depr_group_id
11:59:42 278  			      and d.set_of_books_id = s.set_of_books_id
11:59:42 279  			      and d.gl_post_mo_yr = s.gl_post_mo_yr
11:59:42 280  			      and d.fcst_depr_version_id = s.fcst_depr_version_id
11:59:42 281  			      and d.funding_wo_id = s.funding_wo_id
11:59:42 282  			      and d.revision = s.revision
11:59:42 283  			      and s.depr_calc_status = 9
11:59:42 284  		      );
11:59:42 285  
11:59:42 286  		      PKG_PP_ERROR.REMOVE_MODULE_NAME;
11:59:42 287  	      exception
11:59:42 288  		      when others then
11:59:42 289  			      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
11:59:42 290  	 end P_HANDLERESULTS;
11:59:42 291  
11:59:42 292  end PKG_DEPR_INCREMENTAL;
11:59:42 293  /

Package body created.

11:59:42 hopplant> 
11:59:42 hopplant> --**************************
11:59:42 hopplant> -- Log the run of the script
11:59:42 hopplant> --**************************
11:59:42 hopplant> 
11:59:42 hopplant> insert into PP_SCHEMA_CHANGE_LOG
11:59:42   2  	 (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
11:59:42   3  	  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
11:59:42   4  values
11:59:42   5  	 (2465, 0, 10, 4, 3, 4, 0, 'C:\PlasticWKS\PowerPlant\sql\packages', '10.4.3.4_PKG_DEPR_INCREMENTAL.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

11:59:42 hopplant> commit;

Commit complete.

11:59:42 hopplant> SPOOL OFF
