21:44:23 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2017.1.0.0_maint_047549_04_lease_update_ilr_header_view_calculated_rates_ddl.sql
21:44:23 hopplant> /*
21:44:23 hopplant> ||============================================================================
21:44:23 hopplant> || Application: PowerPlan
21:44:23 hopplant> || File Name:   maint_047549_04_lease_update_ilr_header_view_calculated_rates_ddl.sql
21:44:23 hopplant> ||============================================================================
21:44:23 hopplant> || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
21:44:23 hopplant> ||============================================================================
21:44:23 hopplant> || Version	 Date	    Revised By	     Reason for Change
21:44:23 hopplant> || ---------- ---------- ---------------- ------------------------------------
21:44:23 hopplant> || 2017.1.0.0 05/05/2017 Jared Watkins    update the multicurrency view for ILR header fields
21:44:23 hopplant> ||					     to use the latest calculated rate if it exists
21:44:23 hopplant> ||============================================================================
21:44:23 hopplant> */
21:44:23 hopplant> 
21:44:23 hopplant> CREATE OR REPLACE VIEW v_ls_ilr_header_fx_vw AS
21:44:23   2  WITH
21:44:23   3  cur AS (
21:44:23   4  	SELECT /*+ materialize */ ls_cur_type, currency_id, currency_display_symbol, iso_code
21:44:23   5  	FROM (
21:44:23   6  	  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id, contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code
21:44:23   7  	  FROM currency contract_cur
21:44:23   8  	  UNION
21:44:23   9  	  SELECT 2, company_cur.currency_id, company_cur.currency_display_symbol, company_cur.iso_code
21:44:23  10  	  FROM currency company_cur
21:44:23  11  	)
21:44:23  12  ),
21:44:23  13  cr AS (
21:44:23  14  	SELECT /*+ materialize */ exchange_date, currency_from, currency_to, rate
21:44:23  15  	FROM currency_rate_default
21:44:23  16  ),
21:44:23  17  calc_rate AS (
21:44:23  18  	SELECT /*+ materialize */ company_id, contract_currency_id, company_currency_id,
21:44:23  19  	  accounting_month, exchange_date, rate
21:44:23  20  	FROM ls_lease_calculated_date_rates
21:44:23  21  )
21:44:23  22  SELECT /*+ no_merge */
21:44:23  23  	ilr.ilr_id,
21:44:23  24  	ilr.ilr_number,
21:44:23  25  	ilr.current_revision,
21:44:23  26  	liasob.revision,
21:44:23  27  	liasob.set_of_books_id,
21:44:23  28  	cur.ls_cur_type,
21:44:23  29  	lio.in_service_exchange_rate,
21:44:23  30  	cr.rate,
21:44:23  31  	calc_rate.rate calc_rate,
21:44:23  32  	cur.currency_display_symbol,
21:44:23  33  	cur.iso_code,
21:44:23  34  	lease.contract_currency_id,
21:44:23  35  	cur.currency_id,
21:44:23  36  	cr.exchange_date,
21:44:23  37  	calc_rate.exchange_date calc_date,
21:44:23  38  	ilr.lease_id,
21:44:23  39  	ilr.company_id,
21:44:23  40  	liasob.internal_rate_return,
21:44:23  41  	liasob.is_om,
21:44:23  42  	lio.purchase_option_amt * nvl(calc_rate.rate, cr.rate) purchase_option_amt,
21:44:23  43  	lio.termination_amt * nvl(calc_rate.rate, cr.rate) termination_amt,
21:44:23  44  	liasob.net_present_value * nvl(calc_rate.rate, cr.rate) net_present_value,
21:44:23  45  	liasob.capital_cost * nvl(lio.in_service_exchange_rate, cr.rate) capital_cost,
21:44:23  46  	liasob.current_lease_cost * nvl(lio.in_service_exchange_rate, cr.rate) current_lease_cost
21:44:23  47  FROM ls_ilr ilr
21:44:23  48  INNER JOIN ls_ilr_amounts_set_of_books liasob
21:44:23  49  ON ilr.ilr_id = liasob.ilr_id
21:44:23  50  INNER JOIN ls_ilr_options lio
21:44:23  51  ON lio.ilr_id = liasob.ilr_id
21:44:23  52  AND lio.revision = liasob.revision
21:44:23  53  INNER JOIN ls_lease lease
21:44:23  54  ON ilr.lease_id = lease.lease_id
21:44:23  55  INNER JOIN currency_schema cs
21:44:23  56  	ON ilr.company_id = cs.company_id
21:44:23  57  INNER JOIN cur
21:44:23  58  	ON (cur.ls_cur_type = 1 AND cur.currency_id = lease.contract_currency_id)
21:44:23  59  	OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
21:44:23  60  INNER JOIN cr
21:44:23  61  	ON cur.currency_id = cr.currency_to
21:44:23  62  	AND lease.contract_currency_id = cr.currency_from
21:44:23  63  LEFT OUTER JOIN calc_rate
21:44:23  64  	ON calc_rate.company_id = ilr.company_id
21:44:23  65  	AND calc_rate.contract_currency_id = lease.contract_currency_id
21:44:23  66  	AND calc_rate.company_currency_id = cur.currency_id
21:44:23  67  WHERE cr.exchange_date = (
21:44:23  68  	SELECT Max(exchange_date)
21:44:23  69  	FROM cr cr2
21:44:23  70  	WHERE cr.currency_from = cr2.currency_from
21:44:23  71  	AND cr.currency_to = cr2.currency_to
21:44:23  72  	AND cr2.exchange_date < Add_Months(SYSDATE, 1)
21:44:23  73  )
21:44:23  74  AND cs.currency_type_id = 1
21:44:23  75  ;

View created.

21:44:23 hopplant> 
21:44:23 hopplant> --****************************************************
21:44:23 hopplant> --Log the run of the script to local PP_SCHEMA_LOG
21:44:23 hopplant> --****************************************************
21:44:23 hopplant> INSERT INTO PP_SCHEMA_CHANGE_LOG
21:44:23   2  	      (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
21:44:23   3  	      SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
21:44:23   4  VALUES
21:44:23   5  	      (3480, 0, 2017, 1, 0, 0, 47549, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047549_04_lease_update_ilr_header_view_calculated_rates_ddl.sql', 1,
21:44:23   6  	      SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
21:44:23   7  	      SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

21:44:23 hopplant> COMMIT;

Commit complete.

21:44:23 hopplant> 
21:44:23 hopplant> 
21:44:23 hopplant> SPOOL OFF
