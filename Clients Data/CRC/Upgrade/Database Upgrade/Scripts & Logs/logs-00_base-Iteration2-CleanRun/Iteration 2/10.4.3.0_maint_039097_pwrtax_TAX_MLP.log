18:52:38 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\10.4.3.0_maint_039097_pwrtax_TAX_MLP.sql
18:52:38 hopplant> /*
18:52:38 hopplant> ||============================================================================
18:52:38 hopplant> || Application: PowerPlan
18:52:38 hopplant> || Object Name: maint_039097_pwrtax_TAX_MLP.sql
18:52:38 hopplant> ||============================================================================
18:52:38 hopplant> || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
18:52:38 hopplant> ||============================================================================
18:52:38 hopplant> || PP Version Version   Date       Revised By    Reason for Change
18:52:38 hopplant> || ---------- --------- ---------- ------------- --------------------------
18:52:38 hopplant> || 10.4.3.0	 1.01	   08/14/2014 Andrew Scott  Original Version : initially needed for
18:52:38 hopplant> ||						    faster K1 export generation and backfilling.
18:52:38 hopplant> ||============================================================================
18:52:38 hopplant> */
18:52:38 hopplant> 
18:52:38 hopplant> 
18:52:38 hopplant> create or replace package TAX_MLP as
18:52:38   2  	 /*
18:52:38   3  	 ||============================================================================
18:52:38   4  	 || Application: PowerPlan
18:52:38   5  	 || Object Name: TAX_MLP
18:52:38   6  	 || Description: General PLSQL Logic needed for MLP processes
18:52:38   7  	 ||============================================================================
18:52:38   8  	 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
18:52:38   9  	 ||============================================================================
18:52:38  10  	 ||
18:52:38  11  	 || PP Version	Version    Date 	  Revised By	 Reason for Change
18:52:38  12  	 || ----------	---------  -------------  -------------  -------------------------
18:52:38  13  	 || 10.4.3.0	1.01	   08/14/2014	  Andrew Scott	 Original Version : initially needed for
18:52:38  14  	 ||							 faster K1 export generation and backfilling.
18:52:38  15  	 ||============================================================================
18:52:38  16  	 */
18:52:38  17  
18:52:38  18  	 -- function to get the version of this package.
18:52:38  19  	 function F_GET_VERSION return varchar2;
18:52:38  20  
18:52:38  21  	 -- function to populate k1 export
18:52:38  22  	 function F_TAX_MLP_K1_GEN(A_VERSION_ID 	    number,
18:52:38  23  				   A_K1_EXPORT_RUN_ID	    number,
18:52:38  24  				   A_K1_EXPORT_DEFINTION_ID number) return varchar2;
18:52:38  25  
18:52:38  26  	 -- function to backfill k1 export id onto tax record control
18:52:38  27  	 function F_TAX_MLP_K1_TRC_BACKFILL(A_VERSION_ID       number,
18:52:38  28  					    A_K1_EXPORT_RUN_ID number) return varchar2;
18:52:38  29  
18:52:38  30  end TAX_MLP;
18:52:38  31  /

Package created.

18:52:38 hopplant> 
18:52:38 hopplant> create or replace package body TAX_MLP as
18:52:38   2  
18:52:38   3  	 --****************************************************************************************************
18:52:38   4  	 --		  VARIABLES
18:52:38   5  	 --****************************************************************************************************
18:52:38   6  	 L_PROCESS_ID number;
18:52:38   7  
18:52:38   8  	 --****************************************************************************************************
18:52:38   9  	 --		  Start Body
18:52:38  10  	 --****************************************************************************************************
18:52:38  11  
18:52:38  12  	 -- function to get the version of this package.
18:52:38  13  	 function F_GET_VERSION return varchar2 is
18:52:38  14  	 begin
18:52:38  15  	    return '1.01';
18:52:38  16  	 end F_GET_VERSION;
18:52:38  17  
18:52:38  18  	 --****************************************************************************************************
18:52:38  19  	 --		  PROCEDURES
18:52:38  20  	 --****************************************************************************************************
18:52:38  21  
18:52:38  22  	 --procedure grabs the process id for the online logs
18:52:38  23  	 procedure P_SETPROCESS is
18:52:38  24  	 begin
18:52:38  25  	    select PROCESS_ID
18:52:38  26  	      into L_PROCESS_ID
18:52:38  27  	      from PP_PROCESSES
18:52:38  28  	     where UPPER(DESCRIPTION) = UPPER('PowerTax - MLP K1 Export Generation');
18:52:38  29  	 exception
18:52:38  30  	    when others then
18:52:38  31  	       /* this catches all SQL errors, including no_data_found */
18:52:38  32  	       L_PROCESS_ID := -1;
18:52:38  33  	 end P_SETPROCESS;
18:52:38  34  
18:52:38  35  	 --****************************************************************************************************
18:52:38  36  	 --		   FUNCTIONS
18:52:38  37  	 --****************************************************************************************************
18:52:38  38  
18:52:38  39  	 --
18:52:38  40  	 -- F_TAX_MLP_K1_GEN : function to populate k1 export
18:52:38  41  	 --
18:52:38  42  	 function F_TAX_MLP_K1_GEN(A_VERSION_ID 	    number,
18:52:38  43  				   A_K1_EXPORT_RUN_ID	    number,
18:52:38  44  				   A_K1_EXPORT_DEFINTION_ID number) return varchar2 is
18:52:38  45  	    L_MSG		varchar2(2000);
18:52:38  46  	    L_PKG_VERSION	varchar2(2000);
18:52:38  47  	    L_PREFIX		varchar2(10);
18:52:38  48  	    SQLS		varchar2(2000);
18:52:38  49  	    L_LAST_TAX_EVENT_ID pls_integer;
18:52:38  50  	    L_NUM_ROWS		pls_integer;
18:52:38  51  	    L_FIELD_VALUE	varchar2(2000);
18:52:38  52  	    L_RTN_CODE		number(22, 0);
18:52:38  53  
18:52:38  54  	    L_COMPANY_FLD_VALUE   varchar2(2000);
18:52:38  55  	    L_VINTAGE_FLD_VALUE   varchar2(2000);
18:52:38  56  	    L_TAX_RECD_FLD_VALUE  varchar2(2000);
18:52:38  57  	    L_TAX_LAYER_FLD_VALUE varchar2(2000);
18:52:38  58  	    L_TAX_CRDT_FLD_VALUE  varchar2(2000);
18:52:38  59  	    L_TAX_RATE_FLD_VALUE  varchar2(2000);
18:52:38  60  
18:52:38  61  	    L_LAST_COMPANY_ID	    number;
18:52:38  62  	    L_LAST_VINTAGE_ID	    number;
18:52:38  63  	    L_LAST_IN_SERVICE_MONTH date;
18:52:38  64  	    L_LAST_TAX_LAYER_ID     number;
18:52:38  65  
18:52:38  66  	    type K1_EXP_TBL_TYPE is table of TAX_K1_EXPORT_RUN_RESULTS%rowtype;
18:52:38  67  	    K1_EXP_TBL K1_EXP_TBL_TYPE;
18:52:38  68  
18:52:38  69  	    type FIELD_MAPS_TYPE_REF_CUR is ref cursor;
18:52:38  70  	    FIELDS_MAP_REF_CUR FIELD_MAPS_TYPE_REF_CUR;
18:52:38  71  	    type FIELDS_REC_TYPE is record(
18:52:38  72  	       K1_FIELD_ID TAX_K1_EXPORT_FIELDS.K1_FIELD_ID%type,
18:52:38  73  	       DESCRIPTION TAX_K1_EXPORT_FIELDS.DESCRIPTION%type,
18:52:38  74  	       FIELD_SQL   TAX_K1_EXPORT_FIELDS.FIELD_SQL%type,
18:52:38  75  	       WHERE_SQL   TAX_K1_EXPORT_FIELDS.WHERE_SQL%type,
18:52:38  76  	       FROM_SQL    TAX_K1_EXPORT_FIELDS.FROM_SQL%type,
18:52:38  77  	       SEPARATOR   TAX_K1_EXPORT_DEF_FIELDS.SEPARATOR%type);
18:52:38  78  	    type FIELDS_TBL_TYPE is table of FIELDS_REC_TYPE;
18:52:38  79  	    FIELDS_TBL	     FIELDS_TBL_TYPE;
18:52:38  80  	    EMPTY_FIELDS_TBL FIELDS_TBL_TYPE;
18:52:38  81  
18:52:38  82  	    type K1_EXP_RES_REC_TYPE is record(
18:52:38  83  	       TAX_RECORD_ID	    TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID%type,
18:52:38  84  	       AUTOGEN_K1_EXPORT_ID TAX_K1_EXPORT_RUN_RESULTS.AUTOGEN_K1_EXPORT_ID%type,
18:52:38  85  	       FINAL_K1_EXPORT_ID   TAX_K1_EXPORT_RUN_RESULTS.FINAL_K1_EXPORT_ID%type);
18:52:38  86  	    type K1_EXP_RES_TBL_TYPE is table of K1_EXP_RES_REC_TYPE;
18:52:38  87  	    K1_EXP_RES_TBL K1_EXP_RES_TBL_TYPE;
18:52:38  88  
18:52:38  89  	 begin
18:52:38  90  
18:52:38  91  	    if L_PROCESS_ID = -1 or L_PROCESS_ID is null then
18:52:38  92  	       P_SETPROCESS();
18:52:38  93  	    end if;
18:52:38  94  
18:52:38  95  	    PKG_PP_LOG.P_START_LOG(L_PROCESS_ID);
18:52:38  96  
18:52:38  97  	    PKG_PP_LOG.P_WRITE_MESSAGE('Process started (PLSQL DB processing) ');
18:52:38  98  
18:52:38  99  	    L_PKG_VERSION := F_GET_VERSION();
18:52:38 100  	    PKG_PP_LOG.P_WRITE_MESSAGE('MLP Package version : ' || L_PKG_VERSION);
18:52:38 101  	    PKG_PP_LOG.P_WRITE_MESSAGE('Processing with the following variable definitions:');
18:52:38 102  	    PKG_PP_LOG.P_WRITE_MESSAGE('K1 Export Run ID : ' || A_K1_EXPORT_RUN_ID);
18:52:38 103  	    PKG_PP_LOG.P_WRITE_MESSAGE('K1 Export Definition ID : ' || A_K1_EXPORT_DEFINTION_ID);
18:52:38 104  	    PKG_PP_LOG.P_WRITE_MESSAGE('Version ID : ' || A_VERSION_ID);
18:52:38 105  	    PKG_PP_LOG.P_WRITE_MESSAGE('Beginning backfilling process...');
18:52:38 106  
18:52:38 107  	    ----START OF LOGIC
18:52:38 108  
18:52:38 109  	    ----we will not write to the log during the massive looping logic.
18:52:38 110  	    ----but, as actions are occuring, L_MSG will be updated.  This value is passed to the
18:52:38 111  	    ----exception which is displayed/logged, so it will help to pinpoint where an error is
18:52:38 112  	    ----encountered.
18:52:38 113  	    L_MSG := '';
18:52:38 114  
18:52:38 115  	    ---- turn off auditing.
18:52:38 116  	    L_RTN_CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
18:52:38 117  	    L_MSG      := 'Turn off auditing return code : ' || L_RTN_CODE;
18:52:38 118  	    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:38 119  
18:52:38 120  	    ----grab what we need from the K1 definition (used for all records being processed.)
18:52:38 121  	    L_MSG := 'Grabbing K1 definition parameters.';
18:52:38 122  	    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:38 123  	    select DEF.PREFIX
18:52:38 124  	      into L_PREFIX
18:52:38 125  	      from TAX_K1_EXPORT_DEFINITION DEF
18:52:38 126  	     where DEF.K1_EXPORT_DEFINITION_ID = A_K1_EXPORT_DEFINTION_ID;
18:52:38 127  
18:52:38 128  	    ----populate the table collection with the current table contents
18:52:39 129  	    L_MSG := 'Selecting run results into table variable.';
18:52:39 130  	    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 131  	    select * bulk collect
18:52:39 132  	      into K1_EXP_TBL
18:52:39 133  	      from TAX_K1_EXPORT_RUN_RESULTS
18:52:39 134  	     where K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
18:52:39 135  	     order by NVL(TAX_EVENT_ID, 999);
18:52:39 136  
18:52:39 137  	    ----loop through the result rows
18:52:39 138  	    L_MSG := 'Entering looping logic.';
18:52:39 139  	    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 140  	    for ROW_IDX in 1 .. K1_EXP_TBL.COUNT
18:52:39 141  	    loop
18:52:39 142  
18:52:39 143  	       ---- if the definition says the k1 export should be:
18:52:39 144  	       ---- vintage.year;tax_layer.description;company.description
18:52:39 145  	       ---- then the logic needs to go to update a the example record:
18:52:39 146  	       ---- field_value_1 = 2007
18:52:39 147  	       ---- field_value_2 = TechTerm XYZ blah blah
18:52:39 148  	       ---- field_value_3 = Yellow Jacket Power
18:52:39 149  
18:52:39 150  	       ---- the easiest way to do this will be to go into *another* loop for each of the fields
18:52:39 151  	       ---- make a reference cursor that gathers the fields needed.  ref cursors are a little slow,
18:52:39 152  	       ---- so if this row's tax event id is the same as the last row's tax event id, then it's safe
18:52:39 153  	       ---- to use the existing cursor's fields.
18:52:39 154  
18:52:39 155  	       if NVL(K1_EXP_TBL(ROW_IDX).TAX_EVENT_ID, 0) <> NVL(L_LAST_TAX_EVENT_ID, 0) or ROW_IDX = 1 then
18:52:39 156  		  ----populate the reference cursor of fields we build out.
18:52:39 157  		  L_MSG := 'Populating refence cursor of fields to use. Row: ' || ROW_IDX ||
18:52:39 158  			   '. Expecting tax event.';
18:52:39 159  		  open FIELDS_MAP_REF_CUR for
18:52:39 160  		     select FIELDS.K1_FIELD_ID,
18:52:39 161  			    FIELDS.DESCRIPTION,
18:52:39 162  			    FIELDS.FIELD_SQL,
18:52:39 163  			    FIELDS.WHERE_SQL,
18:52:39 164  			    FIELDS.FROM_SQL,
18:52:39 165  			    DEF_FIELDS.SEPARATOR
18:52:39 166  		       from TAX_K1_EXPORT_DEF_FIELDS DEF_FIELDS, TAX_K1_EXPORT_FIELDS FIELDS
18:52:39 167  		      where DEF_FIELDS.K1_FIELD_ID = FIELDS.K1_FIELD_ID
18:52:39 168  			and DEF_FIELDS.K1_EXPORT_DEFINITION_ID = A_K1_EXPORT_DEFINTION_ID
18:52:39 169  			and NVL(DEF_FIELDS.TAX_EVENT_ID, 99999) =
18:52:39 170  			    NVL(K1_EXP_TBL(ROW_IDX).TAX_EVENT_ID, 99999)
18:52:39 171  		      order by DEF_FIELDS.FIELD_ORDER;
18:52:39 172  
18:52:39 173  		  L_MSG      := 'Resetting fields table variable. Row: ' || ROW_IDX ||
18:52:39 174  				'. Expecting tax event.';
18:52:39 175  		  FIELDS_TBL := EMPTY_FIELDS_TBL;
18:52:39 176  		  L_MSG      := 'Fetching fields from cursor into table variable. Row: ' || ROW_IDX ||
18:52:39 177  				'. Expecting tax event.';
18:52:39 178  		  fetch FIELDS_MAP_REF_CUR bulk collect
18:52:39 179  		     into FIELDS_TBL;
18:52:39 180  		  close FIELDS_MAP_REF_CUR;
18:52:39 181  
18:52:39 182  		  ----is there anything in this collection?  If not, try the default (no tax event overrides were set up for this
18:52:39 183  		  ----tax event)
18:52:39 184  		  if FIELDS_TBL.COUNT = 0 then
18:52:39 185  
18:52:39 186  		     L_MSG := 'Populating refence cursor of fields to use. Row: ' || ROW_IDX ||
18:52:39 187  			      '. No tax event.';
18:52:39 188  		     open FIELDS_MAP_REF_CUR for
18:52:39 189  			select FIELDS.K1_FIELD_ID,
18:52:39 190  			       FIELDS.DESCRIPTION,
18:52:39 191  			       FIELDS.FIELD_SQL,
18:52:39 192  			       FIELDS.WHERE_SQL,
18:52:39 193  			       FIELDS.FROM_SQL,
18:52:39 194  			       DEF_FIELDS.SEPARATOR
18:52:39 195  			  from TAX_K1_EXPORT_DEF_FIELDS DEF_FIELDS, TAX_K1_EXPORT_FIELDS FIELDS
18:52:39 196  			 where DEF_FIELDS.K1_FIELD_ID = FIELDS.K1_FIELD_ID
18:52:39 197  			   and DEF_FIELDS.K1_EXPORT_DEFINITION_ID = A_K1_EXPORT_DEFINTION_ID
18:52:39 198  			   and DEF_FIELDS.TAX_EVENT_ID is null
18:52:39 199  			 order by DEF_FIELDS.FIELD_ORDER;
18:52:39 200  
18:52:39 201  		     L_MSG	:= 'Resetting fields table variable. Row: ' || ROW_IDX ||
18:52:39 202  				   '. No tax event.';
18:52:39 203  		     FIELDS_TBL := EMPTY_FIELDS_TBL;
18:52:39 204  		     L_MSG	:= 'Fetching fields from cursor into table variable. Row: ' || ROW_IDX ||
18:52:39 205  				   '. No tax event.';
18:52:39 206  		     fetch FIELDS_MAP_REF_CUR bulk collect
18:52:39 207  			into FIELDS_TBL;
18:52:39 208  		     close FIELDS_MAP_REF_CUR;
18:52:39 209  
18:52:39 210  		  end if; --if original field count = 0
18:52:39 211  
18:52:39 212  	       end if; --current tax event does not match last tax event
18:52:39 213  
18:52:39 214  	       ----if the fields_tbl is still empty, then there are no fields at all linked to this definition.
18:52:39 215  	       if FIELDS_TBL.COUNT = 0 then
18:52:39 216  		  L_MSG := SUBSTR('Error! There are no fields assigned to this K1 Export Definition.'||
18:52:39 217  			' Please do this in the K1 Export Definition Maintenance window and try again.'||
18:52:39 218  			' Exiting. ',1,2000);
18:52:39 219  		  return L_MSG;
18:52:39 220  	       end if;
18:52:39 221  
18:52:39 222  	       ----loop through fields
18:52:39 223  	       for FIELD_IDX in 1 .. FIELDS_TBL.COUNT
18:52:39 224  	       loop
18:52:39 225  
18:52:39 226  		  ---- what is the field's "from sql"?	This tells us what table we need to be pulling
18:52:39 227  		  ---- from.  current available values are:
18:52:39 228  		  ----	   company_setup
18:52:39 229  		  ----	   vintage
18:52:39 230  		  ----	   tax_record_control
18:52:39 231  		  ----	   tax_layer
18:52:39 232  		  ----	   tax_credit
18:52:39 233  		  ----	   tax_rate_control
18:52:39 234  
18:52:39 235  		  case UPPER(trim(FIELDS_TBL(FIELD_IDX).FROM_SQL))
18:52:39 236  		     when 'COMPANY_SETUP' then
18:52:39 237  
18:52:39 238  			begin
18:52:39 239  
18:52:39 240  			   L_MSG := 'Building company value. Row: ' || ROW_IDX || '. Field:' ||
18:52:39 241  				    FIELD_IDX || '.';
18:52:39 242  			   SQLS  := 'SELECT ' || trim(FIELDS_TBL(FIELD_IDX).FIELD_SQL) || ' ' ||
18:52:39 243  				    'FROM COMPANY_SETUP ' || 'WHERE COMPANY_SETUP.COMPANY_ID = ' || K1_EXP_TBL(ROW_IDX)
18:52:39 244  				   .COMPANY_ID;
18:52:39 245  			   L_MSG := 'Executing company value SQL. SQLS: ' || SQLS || '. Row: ' ||
18:52:39 246  				    ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 247  			   execute immediate SQLS
18:52:39 248  			      into L_COMPANY_FLD_VALUE;
18:52:39 249  
18:52:39 250  			exception
18:52:39 251  			   when NO_DATA_FOUND then
18:52:39 252  			      L_MSG		  := 'No company value found. This is okay. Row: ' ||
18:52:39 253  						     ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 254  			      L_COMPANY_FLD_VALUE := '';
18:52:39 255  			   when others then
18:52:39 256  			      L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
18:52:39 257  			      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 258  			      return L_MSG;
18:52:39 259  			end;
18:52:39 260  
18:52:39 261  			L_FIELD_VALUE	  := L_COMPANY_FLD_VALUE;
18:52:39 262  			L_LAST_COMPANY_ID := K1_EXP_TBL(ROW_IDX).COMPANY_ID;
18:52:39 263  
18:52:39 264  		     when 'VINTAGE' then
18:52:39 265  
18:52:39 266  			begin
18:52:39 267  
18:52:39 268  			   L_MSG := 'Building vintage value. Row: ' || ROW_IDX || '. Field:' ||
18:52:39 269  				    FIELD_IDX || '.';
18:52:39 270  			   SQLS  := 'SELECT ' || trim(FIELDS_TBL(FIELD_IDX).FIELD_SQL) || ' ' ||
18:52:39 271  				    'FROM VINTAGE ' || 'WHERE VINTAGE.VINTAGE_ID = ' || K1_EXP_TBL(ROW_IDX)
18:52:39 272  				   .VINTAGE_ID;
18:52:39 273  
18:52:39 274  			   L_MSG := 'Executing vintage value SQL. SQLS: ' || SQLS || '. Row: ' ||
18:52:39 275  				    ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 276  			   execute immediate SQLS
18:52:39 277  			      into L_VINTAGE_FLD_VALUE;
18:52:39 278  
18:52:39 279  			exception
18:52:39 280  			   when NO_DATA_FOUND then
18:52:39 281  			      L_MSG		  := 'No vintage value found. This is okay. Row: ' ||
18:52:39 282  						     ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 283  			      L_VINTAGE_FLD_VALUE := '';
18:52:39 284  			   when others then
18:52:39 285  			      L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
18:52:39 286  			      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 287  			      return L_MSG;
18:52:39 288  			end;
18:52:39 289  
18:52:39 290  			L_FIELD_VALUE	  := L_VINTAGE_FLD_VALUE;
18:52:39 291  			L_LAST_VINTAGE_ID := K1_EXP_TBL(ROW_IDX).VINTAGE_ID;
18:52:39 292  
18:52:39 293  		     when 'TAX_RECORD_CONTROL' then
18:52:39 294  
18:52:39 295  			begin
18:52:39 296  
18:52:39 297  			   L_MSG := 'Building tax record control value. Row: ' || ROW_IDX ||
18:52:39 298  				    '. Field:' || FIELD_IDX || '.';
18:52:39 299  			   SQLS  := 'SELECT TRIM(' || trim(FIELDS_TBL(FIELD_IDX).FIELD_SQL) || ') ' ||
18:52:39 300  				    'FROM TAX_RECORD_CONTROL ' ||
18:52:39 301  				    'WHERE TAX_RECORD_CONTROL.TAX_RECORD_ID = ' || K1_EXP_TBL(ROW_IDX)
18:52:39 302  				   .TAX_RECORD_ID;
18:52:39 303  
18:52:39 304  			   L_MSG := 'Executing tax record control value SQL. SQLS: ' || SQLS ||
18:52:39 305  				    '. Row: ' || ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 306  			   execute immediate SQLS
18:52:39 307  			      into L_TAX_RECD_FLD_VALUE;
18:52:39 308  
18:52:39 309  			exception
18:52:39 310  			   when NO_DATA_FOUND then
18:52:39 311  			      L_MSG		   := 'No tax record control value found. This is okay. Row: ' ||
18:52:39 312  						      ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 313  			      L_TAX_RECD_FLD_VALUE := '';
18:52:39 314  			   when others then
18:52:39 315  			      L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
18:52:39 316  			      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 317  			      return L_MSG;
18:52:39 318  			end;
18:52:39 319  
18:52:39 320  			L_FIELD_VALUE		:= L_TAX_RECD_FLD_VALUE;
18:52:39 321  			L_LAST_IN_SERVICE_MONTH := K1_EXP_TBL(ROW_IDX).IN_SERVICE_MONTH;
18:52:39 322  
18:52:39 323  		     when 'TAX_LAYER' then
18:52:39 324  
18:52:39 325  			begin
18:52:39 326  
18:52:39 327  			   L_MSG := 'Building tax layer value. Row: ' || ROW_IDX || '. Field:' ||
18:52:39 328  				    FIELD_IDX || '.';
18:52:39 329  			   SQLS  := 'SELECT ' || trim(FIELDS_TBL(FIELD_IDX).FIELD_SQL) || ' ' ||
18:52:39 330  				    'FROM TAX_LAYER ' ||
18:52:39 331  				    'WHERE NVL(TAX_LAYER.TAX_LAYER_ID,-999) = ' || NVL(K1_EXP_TBL(ROW_IDX)
18:52:39 332  				   .TAX_LAYER_ID,-999);
18:52:39 333  
18:52:39 334  			   L_MSG := 'Executing tax layer value SQL. SQLS: ' || SQLS || '. Row: ' ||
18:52:39 335  				    ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 336  			   execute immediate SQLS
18:52:39 337  			      into L_TAX_LAYER_FLD_VALUE;
18:52:39 338  
18:52:39 339  			exception
18:52:39 340  			   when NO_DATA_FOUND then
18:52:39 341  			      L_MSG		    := 'No tax layer value found. This is okay. Row: ' ||
18:52:39 342  						       ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 343  			      L_TAX_LAYER_FLD_VALUE := '';
18:52:39 344  			   when others then
18:52:39 345  			      L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
18:52:39 346  			      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 347  			      return L_MSG;
18:52:39 348  			end;
18:52:39 349  
18:52:39 350  			L_FIELD_VALUE	    := L_TAX_LAYER_FLD_VALUE;
18:52:39 351  			L_LAST_TAX_LAYER_ID := K1_EXP_TBL(ROW_IDX).TAX_LAYER_ID;
18:52:39 352  
18:52:39 353  		     when 'TAX_CREDIT' then
18:52:39 354  
18:52:39 355  			begin
18:52:39 356  
18:52:39 357  			   L_MSG := 'Building tax credit value. Row: ' || ROW_IDX || '. Field:' ||
18:52:39 358  				    FIELD_IDX || '.';
18:52:39 359  			   SQLS  := 'SELECT ' || trim(FIELDS_TBL(FIELD_IDX).FIELD_SQL) || ' ' ||
18:52:39 360  				    'FROM TAX_DEPRECIATION, TAX_BOOK, TAX_CREDIT ' ||
18:52:39 361  				    'WHERE TAX_DEPRECIATION.TAX_RECORD_ID = ' || K1_EXP_TBL(ROW_IDX)
18:52:39 362  				   .TAX_RECORD_ID || ' ' || 'AND TAX_DEPRECIATION.TAX_YEAR = ' || K1_EXP_TBL(ROW_IDX)
18:52:39 363  				   .TAX_YEAR || ' ' ||
18:52:39 364  				    'AND TAX_DEPRECIATION.TAX_BOOK_ID = TAX_BOOK.TAX_BOOK_ID ';
18:52:39 365  
18:52:39 366  			   if not (trim(FIELDS_TBL(FIELD_IDX).WHERE_SQL) is null) then
18:52:39 367  			      SQLS := SQLS || ' AND ' || FIELDS_TBL(FIELD_IDX).WHERE_SQL || ' ';
18:52:39 368  			   end if;
18:52:39 369  
18:52:39 370  			   SQLS := SQLS ||
18:52:39 371  				   ' AND TAX_DEPRECIATION.TAX_CREDIT_ID = TAX_CREDIT.TAX_CREDIT_ID';
18:52:39 372  
18:52:39 373  			   L_MSG := 'Executing tax credit value SQL. SQLS: ' || SQLS || '. Row: ' ||
18:52:39 374  				    ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 375  			   execute immediate SQLS
18:52:39 376  			      into L_TAX_CRDT_FLD_VALUE;
18:52:39 377  
18:52:39 378  			exception
18:52:39 379  			   when NO_DATA_FOUND then
18:52:39 380  			      L_MSG		   := 'No tax credit value found. This is okay. Row: ' ||
18:52:39 381  						      ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 382  			      L_TAX_CRDT_FLD_VALUE := '';
18:52:39 383  			   when others then
18:52:39 384  			      L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
18:52:39 385  			      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 386  			      return L_MSG;
18:52:39 387  			end;
18:52:39 388  
18:52:39 389  			L_FIELD_VALUE := L_TAX_CRDT_FLD_VALUE;
18:52:39 390  
18:52:39 391  		     when 'TAX_RATE_CONTROL' then
18:52:39 392  
18:52:39 393  			begin
18:52:39 394  
18:52:39 395  			   L_MSG := 'Building tax rate value. Row: ' || ROW_IDX || '. Field:' ||
18:52:39 396  				    FIELD_IDX || '.';
18:52:39 397  			   SQLS  := 'SELECT ' || trim(FIELDS_TBL(FIELD_IDX).FIELD_SQL) || ' ' ||
18:52:39 398  				    'FROM TAX_DEPRECIATION, TAX_BOOK, TAX_RATE_CONTROL ' ||
18:52:39 399  				    'WHERE TAX_DEPRECIATION.TAX_RECORD_ID = ' || K1_EXP_TBL(ROW_IDX)
18:52:39 400  				   .TAX_RECORD_ID || ' ' || 'AND TAX_DEPRECIATION.TAX_YEAR = ' || K1_EXP_TBL(ROW_IDX)
18:52:39 401  				   .TAX_YEAR || ' ' ||
18:52:39 402  				    'AND TAX_DEPRECIATION.TAX_BOOK_ID = TAX_BOOK.TAX_BOOK_ID ';
18:52:39 403  
18:52:39 404  			   if not (trim(FIELDS_TBL(FIELD_IDX).WHERE_SQL) is null) then
18:52:39 405  			      SQLS := SQLS || ' AND ' || FIELDS_TBL(FIELD_IDX).WHERE_SQL || ' ';
18:52:39 406  			   end if;
18:52:39 407  
18:52:39 408  			   SQLS := SQLS ||
18:52:39 409  				   ' AND TAX_DEPRECIATION.TAX_RATE_ID = TAX_RATE_CONTROL.TAX_RATE_ID';
18:52:39 410  
18:52:39 411  			   L_MSG := 'Executing tax rate value SQL. SQLS: ' || SQLS || '. Row: ' ||
18:52:39 412  				    ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 413  			   execute immediate SQLS
18:52:39 414  			      into L_TAX_RATE_FLD_VALUE;
18:52:39 415  
18:52:39 416  			exception
18:52:39 417  			   when NO_DATA_FOUND then
18:52:39 418  			      L_MSG		   := 'No tax rate value found. This is okay. Row: ' ||
18:52:39 419  						      ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 420  			      L_TAX_RATE_FLD_VALUE := '';
18:52:39 421  			   when others then
18:52:39 422  			      L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
18:52:39 423  			      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 424  			      return L_MSG;
18:52:39 425  			end;
18:52:39 426  
18:52:39 427  			L_FIELD_VALUE := L_TAX_RATE_FLD_VALUE;
18:52:39 428  
18:52:39 429  		  end case;
18:52:39 430  
18:52:39 431  		  ----append the separator value to the field if it's populated
18:52:39 432  		  if FIELDS_TBL(FIELD_IDX).SEPARATOR is not null then
18:52:39 433  		     L_FIELD_VALUE := L_FIELD_VALUE || FIELDS_TBL(FIELD_IDX).SEPARATOR;
18:52:39 434  		  end if;
18:52:39 435  
18:52:39 436  		  ----tried to do this dynamically, but it does not seem possible.
18:52:39 437  		  ----This case statement is fine.
18:52:39 438  		  L_MSG := 'Setting table variable Field Value: ' || L_FIELD_VALUE || '. Row: ' ||
18:52:39 439  			   ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 440  		  case FIELD_IDX
18:52:39 441  		     when 1 then
18:52:39 442  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_1 := L_PREFIX || L_FIELD_VALUE;
18:52:39 443  		     when 2 then
18:52:39 444  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_2 := L_FIELD_VALUE;
18:52:39 445  		     when 3 then
18:52:39 446  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_3 := L_FIELD_VALUE;
18:52:39 447  		     when 4 then
18:52:39 448  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_4 := L_FIELD_VALUE;
18:52:39 449  		     when 5 then
18:52:39 450  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_5 := L_FIELD_VALUE;
18:52:39 451  		     when 6 then
18:52:39 452  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_6 := L_FIELD_VALUE;
18:52:39 453  		     when 7 then
18:52:39 454  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_7 := L_FIELD_VALUE;
18:52:39 455  		     when 8 then
18:52:39 456  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_8 := L_FIELD_VALUE;
18:52:39 457  		     when 9 then
18:52:39 458  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_9 := L_FIELD_VALUE;
18:52:39 459  		     when 10 then
18:52:39 460  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_10 := L_FIELD_VALUE;
18:52:39 461  		     when 11 then
18:52:39 462  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_11 := L_FIELD_VALUE;
18:52:39 463  		     when 12 then
18:52:39 464  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_12 := L_FIELD_VALUE;
18:52:39 465  		     when 13 then
18:52:39 466  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_13 := L_FIELD_VALUE;
18:52:39 467  		     when 14 then
18:52:39 468  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_14 := L_FIELD_VALUE;
18:52:39 469  		     when 15 then
18:52:39 470  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_15 := L_FIELD_VALUE;
18:52:39 471  		     when 16 then
18:52:39 472  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_16 := L_FIELD_VALUE;
18:52:39 473  		     when 17 then
18:52:39 474  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_17 := L_FIELD_VALUE;
18:52:39 475  		     when 18 then
18:52:39 476  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_18 := L_FIELD_VALUE;
18:52:39 477  		     when 19 then
18:52:39 478  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_19 := L_FIELD_VALUE;
18:52:39 479  		     when 20 then
18:52:39 480  			K1_EXP_TBL(ROW_IDX).FIELD_VALUE_20 := L_FIELD_VALUE;
18:52:39 481  		     else
18:52:39 482  			L_MSG := SUBSTR('Error! The k1 export definition '||
18:52:39 483  			      'has more than 20 fields, which the process does not allow. '||
18:52:39 484  			      'Please reduce the number of fields for this definition and try again. '||
18:52:39 485  			      'Exiting. ',1,2000);
18:52:39 486  			return L_MSG;
18:52:39 487  		  end case;
18:52:39 488  
18:52:39 489  		  ----build out the final value
18:52:39 490  		  L_MSG := 'Setting table variable Final Value: ' || K1_EXP_TBL(ROW_IDX).FINAL_VALUE ||
18:52:39 491  			   L_FIELD_VALUE || '. Row: ' || ROW_IDX || '. Field:' || FIELD_IDX || '.';
18:52:39 492  		  if FIELD_IDX = 1 then
18:52:39 493  		     K1_EXP_TBL(ROW_IDX).FINAL_VALUE := L_PREFIX || L_FIELD_VALUE;
18:52:39 494  		  else
18:52:39 495  		     K1_EXP_TBL(ROW_IDX).FINAL_VALUE := K1_EXP_TBL(ROW_IDX).FINAL_VALUE || L_FIELD_VALUE;
18:52:39 496  		  end if;
18:52:39 497  
18:52:39 498  		  ----if we are on the last field in the row, and the field value is still null
18:52:39 499  		  ----then something is wrong.	bad template config most likely.  error out.
18:52:39 500  		  if FIELD_IDX = FIELDS_TBL.COUNT and K1_EXP_TBL(ROW_IDX).FINAL_VALUE IS NULL then
18:52:39 501  		     L_MSG := SUBSTR('Error! The final value for tax record id '||K1_EXP_TBL(ROW_IDX).TAX_RECORD_ID||
18:52:39 502  			   ' is entirely empty.  The k1 export definition '||
18:52:39 503  			   'is likely insufficient and needs to be reconfigured.  Exiting. ',1,2000);
18:52:39 504  		     return L_MSG;
18:52:39 505  		  end if;
18:52:39 506  
18:52:39 507  	       end loop; --fields within the row loop
18:52:39 508  
18:52:39 509  	       ----set the last tax event to be this row's tax event
18:52:39 510  	       L_MSG		   := 'Setting last tax event id. Row: ' || ROW_IDX || '.';
18:52:39 511  	       L_LAST_TAX_EVENT_ID := K1_EXP_TBL(ROW_IDX).TAX_EVENT_ID;
18:52:39 512  
18:52:39 513  	    end loop; --row loop
18:52:39 514  
18:52:39 515  	    L_MSG := 'Updating tax_k1_export_results with field values and final k1 export value. count :' ||
18:52:39 516  		     K1_EXP_TBL.COUNT;
18:52:39 517  	    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 518  	    ----update the actual table with everything populated into the table variable.
18:52:39 519  	    forall I in 1 .. K1_EXP_TBL.COUNT
18:52:39 520  	       update TAX_K1_EXPORT_RUN_RESULTS
18:52:39 521  		  set TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_1 = K1_EXP_TBL(I).FIELD_VALUE_1,
18:52:39 522  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_2 = K1_EXP_TBL(I).FIELD_VALUE_2,
18:52:39 523  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_3 = K1_EXP_TBL(I).FIELD_VALUE_3,
18:52:39 524  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_4 = K1_EXP_TBL(I).FIELD_VALUE_4,
18:52:39 525  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_5 = K1_EXP_TBL(I).FIELD_VALUE_5,
18:52:39 526  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_6 = K1_EXP_TBL(I).FIELD_VALUE_6,
18:52:39 527  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_7 = K1_EXP_TBL(I).FIELD_VALUE_7,
18:52:39 528  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_8 = K1_EXP_TBL(I).FIELD_VALUE_8,
18:52:39 529  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_9 = K1_EXP_TBL(I).FIELD_VALUE_9,
18:52:39 530  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_10 = K1_EXP_TBL(I).FIELD_VALUE_10,
18:52:39 531  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_11 = K1_EXP_TBL(I).FIELD_VALUE_11,
18:52:39 532  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_12 = K1_EXP_TBL(I).FIELD_VALUE_12,
18:52:39 533  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_13 = K1_EXP_TBL(I).FIELD_VALUE_13,
18:52:39 534  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_14 = K1_EXP_TBL(I).FIELD_VALUE_14,
18:52:39 535  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_15 = K1_EXP_TBL(I).FIELD_VALUE_15,
18:52:39 536  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_16 = K1_EXP_TBL(I).FIELD_VALUE_16,
18:52:39 537  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_17 = K1_EXP_TBL(I).FIELD_VALUE_17,
18:52:39 538  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_18 = K1_EXP_TBL(I).FIELD_VALUE_18,
18:52:39 539  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_19 = K1_EXP_TBL(I).FIELD_VALUE_19,
18:52:39 540  		      TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_20 = K1_EXP_TBL(I).FIELD_VALUE_20,
18:52:39 541  		      TAX_K1_EXPORT_RUN_RESULTS.FINAL_VALUE = K1_EXP_TBL(I).FINAL_VALUE
18:52:39 542  		where TAX_K1_EXPORT_RUN_RESULTS.K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
18:52:39 543  		  and TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID = K1_EXP_TBL(I).TAX_RECORD_ID;
18:52:39 544  
18:52:39 545  	    L_NUM_ROWS := K1_EXP_TBL.COUNT;
18:52:39 546  	    L_MSG      := L_NUM_ROWS || ' rows updated on tax_k1_export_results.';
18:52:39 547  	    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 548  
18:52:39 549  	    ----insert new rows into tax_k1_export
18:52:39 550  	    L_MSG := 'Inserting new rows into tax_k1_export.';
18:52:39 551  	    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 552  	    insert into TAX_K1_EXPORT
18:52:39 553  	       (K1_EXPORT_ID, DESCRIPTION)
18:52:39 554  	       select MAX_ID + ROWNUM, FINAL_VALUE
18:52:39 555  		 from (select distinct FINAL_VALUE
18:52:39 556  			 from TAX_K1_EXPORT_RUN_RESULTS
18:52:39 557  			where K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
18:52:39 558  			  and FINAL_VALUE not in (select DESCRIPTION from TAX_K1_EXPORT)),
18:52:39 559  		      (select NVL(max(K1_EXPORT_ID), 0) MAX_ID from TAX_K1_EXPORT);
18:52:39 560  
18:52:39 561  	    ----backfill k1 export ids to table.
18:52:39 562  	    L_MSG := 'Backfilling K1 export id onto tax_k1_export_results (pt1).';
18:52:39 563  	    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 564  	    select TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID,
18:52:39 565  		   TAX_K1_EXPORT.K1_EXPORT_ID AUTOGEN_K1_EXPORT_ID,
18:52:39 566  		   NVL(TAX_K1_EXPORT_RUN_RESULTS.EXISTING_K1_EXPORT_ID, TAX_K1_EXPORT.K1_EXPORT_ID) FINAL_K1_EXPORT_ID bulk collect
18:52:39 567  	      into K1_EXP_RES_TBL
18:52:39 568  	      from TAX_K1_EXPORT_RUN_RESULTS, TAX_K1_EXPORT
18:52:39 569  	     where TAX_K1_EXPORT_RUN_RESULTS.K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
18:52:39 570  	       and TAX_K1_EXPORT_RUN_RESULTS.FINAL_VALUE = TAX_K1_EXPORT.DESCRIPTION;
18:52:39 571  
18:52:39 572  	    L_MSG := 'Backfilling K1 export id onto tax_k1_export_results (pt2).';
18:52:39 573  	    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 574  	    forall I in K1_EXP_RES_TBL.FIRST .. K1_EXP_RES_TBL.LAST
18:52:39 575  	       update TAX_K1_EXPORT_RUN_RESULTS
18:52:39 576  		  set AUTOGEN_K1_EXPORT_ID = K1_EXP_RES_TBL(I).AUTOGEN_K1_EXPORT_ID,
18:52:39 577  		      FINAL_K1_EXPORT_ID = K1_EXP_RES_TBL(I).FINAL_K1_EXPORT_ID
18:52:39 578  		where TAX_K1_EXPORT_RUN_RESULTS.K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
18:52:39 579  		  and TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID = K1_EXP_RES_TBL(I).TAX_RECORD_ID;
18:52:39 580  	    L_NUM_ROWS := K1_EXP_RES_TBL.COUNT;
18:52:39 581  	    L_MSG      := L_NUM_ROWS || ' rows updated on tax_k1_export_results.';
18:52:39 582  	    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 583  
18:52:39 584  	    ----END OF LOGIC
18:52:39 585  	    PKG_PP_LOG.P_WRITE_MESSAGE('Finished...');
18:52:39 586  	    PKG_PP_LOG.P_END_LOG();
18:52:39 587  	    return 'OK';
18:52:39 588  
18:52:39 589  	 exception
18:52:39 590  	    when others then
18:52:39 591  	       L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,
18:52:39 592  			       1,
18:52:39 593  			       2000);
18:52:39 594  	       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
18:52:39 595  	       PKG_PP_LOG.P_END_LOG();
18:52:39 596  	       return L_MSG;
18:52:39 597  	 end F_TAX_MLP_K1_GEN;
18:52:39 598  
18:52:39 599  	 --
18:52:39 600  	 -- F_TAX_MLP_K1_TRC_BACKFILL : simple function to update k1_export_id on tax record control to match
18:52:39 601  	 --    what was saved onto tax_k1_export_run_results
18:52:39 602  	 --
18:52:39 603  	 function F_TAX_MLP_K1_TRC_BACKFILL(A_VERSION_ID       number,
18:52:39 604  					    A_K1_EXPORT_RUN_ID number) return varchar2 is
18:52:39 605  	    L_MSG      varchar2(2000);
18:52:39 606  	    L_RTN_CODE number(22, 0);
18:52:39 607  
18:52:39 608  	    type K1_EXP_RES_REC_TYPE is record(
18:52:39 609  	       TAX_RECORD_ID	  TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID%type,
18:52:39 610  	       FINAL_K1_EXPORT_ID TAX_K1_EXPORT_RUN_RESULTS.FINAL_K1_EXPORT_ID%type);
18:52:39 611  	    type K1_EXP_RES_TBL_TYPE is table of K1_EXP_RES_REC_TYPE;
18:52:39 612  	    K1_EXP_RES_TBL K1_EXP_RES_TBL_TYPE;
18:52:39 613  
18:52:39 614  	 begin
18:52:39 615  
18:52:39 616  	    L_MSG := '';
18:52:39 617  
18:52:39 618  	    ---- turn off auditing.
18:52:39 619  	    L_RTN_CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
18:52:39 620  	    L_MSG      := 'Turn off auditing return code : ' || L_RTN_CODE;
18:52:39 621  
18:52:39 622  	    ----backfill final k1 export ids to tax record control.
18:52:39 623  	    L_MSG := 'Backfilling K1 export id onto tax record control (pt1).';
18:52:39 624  	    select TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID,
18:52:39 625  		   TAX_K1_EXPORT_RUN_RESULTS.FINAL_K1_EXPORT_ID bulk collect
18:52:39 626  	      into K1_EXP_RES_TBL
18:52:39 627  	      from TAX_K1_EXPORT_RUN_RESULTS, TAX_RECORD_CONTROL
18:52:39 628  	     where TAX_K1_EXPORT_RUN_RESULTS.K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
18:52:39 629  	       and TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID = TAX_RECORD_CONTROL.TAX_RECORD_ID
18:52:39 630  	       and TAX_RECORD_CONTROL.VERSION_ID = A_VERSION_ID
18:52:39 631  	       and NVL(TAX_K1_EXPORT_RUN_RESULTS.FINAL_K1_EXPORT_ID, 0) <>
18:52:39 632  		   NVL(TAX_RECORD_CONTROL.K1_EXPORT_ID, 0);
18:52:39 633  
18:52:39 634  	    L_MSG := 'Backfilling K1 export id onto tax_k1_export_results (pt2).';
18:52:39 635  	    forall I in K1_EXP_RES_TBL.FIRST .. K1_EXP_RES_TBL.LAST
18:52:39 636  	       update TAX_RECORD_CONTROL
18:52:39 637  		  set K1_EXPORT_ID = K1_EXP_RES_TBL(I).FINAL_K1_EXPORT_ID
18:52:39 638  		where TAX_RECORD_CONTROL.VERSION_ID = A_VERSION_ID
18:52:39 639  		  and TAX_RECORD_CONTROL.TAX_RECORD_ID = K1_EXP_RES_TBL(I).TAX_RECORD_ID;
18:52:39 640  
18:52:39 641  	    ----refresh the "existing" k1 export to be what was set as the final export id
18:52:39 642  	    L_MSG := 'Refreshing existing k1 export on run table to match final export.';
18:52:39 643  	    update TAX_K1_EXPORT_RUN_RESULTS
18:52:39 644  	       set EXISTING_K1_EXPORT_ID = FINAL_K1_EXPORT_ID
18:52:39 645  	     where TAX_K1_EXPORT_RUN_RESULTS.K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
18:52:39 646  	       and NVL(EXISTING_K1_EXPORT_ID, 0) <> NVL(FINAL_K1_EXPORT_ID, 0);
18:52:39 647  
18:52:39 648  	    return 'OK';
18:52:39 649  
18:52:39 650  	 exception
18:52:39 651  	    when others then
18:52:39 652  	       L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,
18:52:39 653  			       1,
18:52:39 654  			       2000);
18:52:39 655  	       return L_MSG;
18:52:39 656  	 end F_TAX_MLP_K1_TRC_BACKFILL;
18:52:39 657  
18:52:39 658  end TAX_MLP;
18:52:39 659  /

Package body created.

18:52:39 hopplant> 
18:52:39 hopplant> 
18:52:39 hopplant> --**************************
18:52:39 hopplant> -- Log the run of the script
18:52:39 hopplant> --**************************
18:52:39 hopplant> 
18:52:39 hopplant> insert into PP_SCHEMA_CHANGE_LOG
18:52:39   2  	 (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
18:52:39   3  	  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
18:52:39   4  values
18:52:39   5  	 (1358, 0, 10, 4, 3, 0, 39097, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039097_pwrtax_TAX_MLP.sql', 1,
18:52:39   6  	  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
18:52:39   7  	  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

18:52:39 hopplant> commit;

Commit complete.

18:52:39 hopplant> SPOOL OFF
