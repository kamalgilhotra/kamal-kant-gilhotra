21:45:46 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2017.1.0.0_maint_048370_lease_bal_sheet_span_query_add_multicurrency_dml.sql
21:45:46 hopplant> /*
21:45:46 hopplant> ||============================================================================================================================
21:45:46 hopplant> || Application: PowerPlan
21:45:46 hopplant> || File Name:   maint_048370_lease_bal_sheet_span_query_add_multicurrency_dml.sql
21:45:46 hopplant> ||============================================================================================================================
21:45:46 hopplant> || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
21:45:46 hopplant> ||============================================================================================================================
21:45:46 hopplant> || Version	 Date	    Revised By	     Reason for Change
21:45:46 hopplant> || ---------- ---------- ---------------- ------------------------------------------------------------------------------------
21:45:46 hopplant> || 2017.1.0.0 07/27/2017 Josh Sandler     Update Lease Balance Sheet Span by Asset query for Multicurrency in Company currency
21:45:46 hopplant> ||============================================================================================================================
21:45:46 hopplant> */
21:45:46 hopplant> 
21:45:46 hopplant> UPDATE pp_any_query_criteria_fields
21:45:46   2  SET column_order = column_order + 2
21:45:46   3  WHERE id = (SELECT id FROM pp_any_query_criteria WHERE description = 'Lease Balance Sheet Span by Asset')
21:45:46   4  AND column_order >= 20;

15 rows updated.

21:45:46 hopplant> 
21:45:46 hopplant> INSERT INTO pp_any_query_criteria_fields
21:45:46   2  	(id, detail_field, column_order, amount_field, include_in_select_criteria, column_header, column_width, column_type, quantity_field, hide_from_results, hide_from_filters)
21:45:46   3  VALUES
21:45:46   4  	((SELECT id FROM pp_any_query_criteria WHERE description = 'Lease Balance Sheet Span by Asset'), 'currency_display_symbol', 20, 0, 0, 'Currency Symbol', 300, 'VARCHAR2', 0, 1, 1)
21:45:46   5  ;

1 row created.

21:45:46 hopplant> 
21:45:46 hopplant> INSERT INTO pp_any_query_criteria_fields
21:45:46   2  	(id, detail_field, column_order, amount_field, include_in_select_criteria, column_header, column_width, column_type, quantity_field, hide_from_results, hide_from_filters)
21:45:46   3  VALUES
21:45:46   4  	((SELECT id FROM pp_any_query_criteria WHERE description = 'Lease Balance Sheet Span by Asset'), 'currency', 21, 0, 0, 'Currency', 300, 'VARCHAR2', 0, 0, 1)
21:45:46   5  ;

1 row created.

21:45:46 hopplant> 
21:45:46 hopplant> UPDATE pp_any_query_criteria
21:45:46   2  SET description = 'Lease Balance Sheet Span by Asset (Company Currency)',
21:45:46   3  SQL = 'with balance_sheet_detail as (
21:45:46   4  select la.ls_asset_id, la.leased_asset_number, to_char(las.month,''yyyymm'') as monthnum, la.description as asset_description,
21:45:46   5  	     la.company_id, co.description as company_description, ilr.ilr_id,
21:45:46   6  	     ilr.ilr_number, ilr.external_ilr, ll.lease_id, ll.lease_number, lct.description as lease_cap_type,
21:45:46   7  	     al.long_description as location, al.state_id as state, gl.external_account_code as account,
21:45:46   8  	     ua.utility_account_id, ua.description as utility_account_description, fc.description as func_class_description,
21:45:46   9  	     las.currency_display_symbol,
21:45:46  10  	     las.iso_code AS currency,
21:45:46  11  	     las.beg_capital_cost, las.end_capital_cost, las.beg_obligation, las.end_obligation,
21:45:46  12  	     las.beg_lt_obligation, las.end_lt_obligation,
21:45:46  13  	     case when las.month > depr_calc.month then las.begin_reserve else cprd.beg_reserve_month end as beg_reserve,
21:45:46  14  	     case when las.month > depr_calc.month then las.end_reserve else cprd.depr_reserve end as end_reserve,
21:45:46  15  	     nvl(cprd. net_adds_and_adjust,0) as additions, nvl(cprd.retirements,0) + nvl(gain_loss,0) as retirements, nvl(cprd.transfers_in,0) as transfers_in,
21:45:46  16  	     nvl(cprd.transfers_out,0) as transfers_out, nvl(cprd.reserve_trans_in,0) as reserve_trans_in, nvl(cprd.reserve_trans_out,0) as reserve_trans_out,
21:45:46  17  	     (las.end_capital_cost - las.end_obligation - case when las.month > depr_calc.month then las.end_reserve else cprd.depr_reserve end) as ferc_gaap_difference
21:45:46  18  from ls_asset la, ls_cpr_asset_map map, ls_ilr ilr, ls_lease ll, company co,
21:45:46  19  	   cpr_ledger cpr, utility_account ua, business_segment bs, func_class fc, gl_account gl,
21:45:46  20  	   v_ls_asset_schedule_fx_vw las, asset_location al,
21:45:46  21  	   ls_ilr_options ilro, ls_lease_cap_type lct,
21:45:46  22  	   (select m.ls_asset_id, c.*
21:45:46  23  	    from cpr_depr c, ls_cpr_asset_map m, cpr_ledger l
21:45:46  24  	    where c.asset_id = m.asset_id
21:45:46  25  	      and m.asset_id = l.asset_id
21:45:46  26  	      and to_char(c.gl_posting_mo_yr, ''yyyymm'') >= (select filter_value from pp_any_required_filter where upper(column_name)= ''START MONTHNUM'')
21:45:46  27  	      and to_char(c.gl_posting_mo_yr, ''yyyymm'') <= (select filter_value from pp_any_required_filter where upper(column_name)= ''END MONTHNUM'')
21:45:46  28  	      and l.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')) cprd,
21:45:46  29  	   (select company_id, max(gl_posting_mo_yr) month
21:45:46  30  	    from ls_process_control where depr_calc is not null
21:45:46  31  	      and company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
21:45:46  32  	    group by company_id) depr_calc
21:45:46  33  where la.ls_asset_id = map.ls_asset_id
21:45:46  34  	and map.asset_id = cpr.asset_id
21:45:46  35  	and la.ilr_id = ilr.ilr_id
21:45:46  36  	and ilr.lease_id = ll.lease_id
21:45:46  37  	and ilr.ilr_id = ilro.ilr_id
21:45:46  38  	and ilro.revision = ilr.current_revision
21:45:46  39  	and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
21:45:46  40  	and lct.is_om = 0
21:45:46  41  	and cpr.gl_account_id = gl.gl_account_id
21:45:46  42  	and la.utility_account_id = ua.utility_account_id
21:45:46  43  	and la.bus_segment_id = ua.bus_segment_id
21:45:46  44  	and ua.func_class_id = fc.func_class_id
21:45:46  45  	and la.bus_segment_id = bs.bus_segment_id
21:45:46  46  	and co.company_id = la.company_id
21:45:46  47  	and las.ls_asset_id = la.ls_asset_id
21:45:46  48  	and las.revision = la.approved_revision
21:45:46  49  	and depr_calc.company_id = co.company_id (+)
21:45:46  50  	and las.month = cprd.gl_posting_mo_yr (+)
21:45:46  51  	and las.ls_asset_id = cprd.ls_asset_id (+)
21:45:46  52  	and las.set_of_books_id = cprd.set_of_books_id (+)
21:45:46  53  	and las.ls_cur_type = 2
21:45:46  54  	and la.asset_location_id = al.asset_location_id
21:45:46  55  	and to_char(las.month, ''yyyymm'') >= (select filter_value from pp_any_required_filter where upper(column_name)= ''START MONTHNUM'')
21:45:46  56  	and to_char(las.month, ''yyyymm'') <= (select filter_value from pp_any_required_filter where upper(column_name)= ''END MONTHNUM'')
21:45:46  57  	and co.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID''))',
21:45:46  58  sql2 = 'select (select filter_value from pp_any_required_filter where upper(column_name)= ''START MONTHNUM'') as start_monthnum,
21:45:46  59  	     (select filter_value from pp_any_required_filter where upper(column_name)= ''END MONTHNUM'') as end_monthnum,
21:45:46  60  	     fm.LS_ASSET_ID, LEASED_ASSET_NUMBER, ASSET_DESCRIPTION, COMPANY_ID, COMPANY_DESCRIPTION, ILR_ID, ILR_NUMBER,
21:45:46  61  	     EXTERNAL_ILR, LEASE_ID, LEASE_NUMBER, LEASE_CAP_TYPE, LOCATION, STATE, ACCOUNT, UTILITY_ACCOUNT_ID, UTILITY_ACCOUNT_DESCRIPTION,
21:45:46  62  	     FUNC_CLASS_DESCRIPTION,
21:45:46  63  	     currency_display_symbol,
21:45:46  64  	     currency,
21:45:46  65  	     nvl(BEG_CAPITAL_COST,0) as BEG_CAPITAL_COST, nvl(END_CAPITAL_COST,0) as end_capital_cost, nvl(BEG_OBLIGATION,0) as beg_obligation,
21:45:46  66  	     nvl(END_OBLIGATION,0) as end_obligation, nvl(BEG_LT_OBLIGATION,0) as beg_lt_obligation, nvl(END_LT_OBLIGATION,0) as end_lt_obligation,
21:45:46  67  	     nvl(BEG_RESERVE,0) as beg_reserve, nvl(END_RESERVE,0) as end_reserve,
21:45:46  68  	     sum(additions) as additions, sum(RETIREMENTS) as retirements, sum(TRANSFERS_IN) as transfers_in, sum(TRANSFERS_OUT) as transfers_out,
21:45:46  69  	     sum(RESERVE_TRANS_IN) as reserve_trans_in, sum(RESERVE_TRANS_OUT) as reserve_trans_out, sum(FERC_GAAP_DIFFERENCE) as ferc_gaap_difference
21:45:46  70  from
21:45:46  71  (select monthnum, ls_asset_id, beg_capital_cost, beg_obligation, beg_lt_obligation, beg_reserve
21:45:46  72   from balance_sheet_detail
21:45:46  73   where monthnum = (select filter_value from pp_any_required_filter where upper(column_name)= ''START MONTHNUM'')) fm, /* first month */
21:45:46  74  (select monthnum, ls_asset_id, end_capital_cost, end_obligation, end_lt_obligation, end_reserve
21:45:46  75   from balance_sheet_detail
21:45:46  76   where monthnum = (select filter_value from pp_any_required_filter where upper(column_name)= ''END MONTHNUM'')) lm, /* last month */
21:45:46  77  (select LS_ASSET_ID, LEASED_ASSET_NUMBER, MONTHNUM, ASSET_DESCRIPTION, COMPANY_ID, COMPANY_DESCRIPTION, ILR_ID, ILR_NUMBER,
21:45:46  78  	      EXTERNAL_ILR, LEASE_ID, LEASE_NUMBER, LEASE_CAP_TYPE, LOCATION, STATE, ACCOUNT, UTILITY_ACCOUNT_ID, UTILITY_ACCOUNT_DESCRIPTION,
21:45:46  79  	      FUNC_CLASS_DESCRIPTION,
21:45:46  80  	      currency_display_symbol,
21:45:46  81  	      currency,
21:45:46  82  	      ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, RESERVE_TRANS_IN, RESERVE_TRANS_OUT, FERC_GAAP_DIFFERENCE
21:45:54  83   from balance_sheet_detail) am /* all months */
21:45:54  84  where 1=1
21:45:54  85  	and am.ls_asset_id = fm.ls_asset_id (+)
21:45:54  86  	and am.ls_asset_id = lm.ls_asset_id (+)
21:45:54  87  group by fm.monthnum, lm.monthnum, fm.LS_ASSET_ID, LEASED_ASSET_NUMBER, ASSET_DESCRIPTION, COMPANY_ID, COMPANY_DESCRIPTION, ILR_ID, ILR_NUMBER,
21:45:54  88  	     EXTERNAL_ILR, LEASE_ID, LEASE_NUMBER, LEASE_CAP_TYPE, LOCATION, STATE, ACCOUNT, UTILITY_ACCOUNT_ID, UTILITY_ACCOUNT_DESCRIPTION,
21:45:54  89  	     FUNC_CLASS_DESCRIPTION,
21:45:54  90  	     currency_display_symbol,
21:45:54  91  	     currency,
21:45:54  92  	     BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, BEG_RESERVE,
21:45:54  93  	     END_RESERVE
21:45:54  94  order by 1'
21:45:54  95  WHERE id = (SELECT id FROM pp_any_query_criteria WHERE description = 'Lease Balance Sheet Span by Asset');

1 row updated.

21:45:54 hopplant> 
21:45:54 hopplant> 
21:45:54 hopplant> --****************************************************
21:45:54 hopplant> --Log the run of the script to local PP_SCHEMA_LOG
21:45:54 hopplant> --****************************************************
21:45:54 hopplant> INSERT INTO PP_SCHEMA_CHANGE_LOG
21:45:54   2  	      (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
21:45:54   3  	      SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
21:45:54   4  VALUES
21:45:54   5  	      (3613, 0, 2017, 1, 0, 0, 48370, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048370_lease_bal_sheet_span_query_add_multicurrency_dml.sql', 1,
21:45:54   6  	      SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
21:45:54   7  	      SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

21:45:54 hopplant> COMMIT;

Commit complete.

21:45:54 hopplant> SPOOL OFF
