22:43:06 hopplant> @&&PP_SCRIPT_PATH.packages_released\2018.2.0.0_TAX_ACCRUAL_UTILITY_PKG.sql
22:43:06 hopplant> --||============================================================================
22:43:06 hopplant> --|| Application: PowerPlant
22:43:06 hopplant> --|| Object Name: tax_accrual_utility_pkg
22:43:06 hopplant> --|| Description: Contains several utilities used in processing PL/SQL for
22:43:06 hopplant> --|| 	      provision.
22:43:06 hopplant> --||============================================================================
22:43:06 hopplant> --|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
22:43:06 hopplant> --||============================================================================
22:43:06 hopplant> --|| Patch		Date	   Revised By	  Reason for Change
22:43:06 hopplant> --|| -------------	---------- -------------- ----------------------------------
22:43:06 hopplant> --|| ###PATCH(403)	08/04/2010 Blake Andrews  Initial Creation
22:43:06 hopplant> --||============================================================================
22:43:06 hopplant> 
22:43:06 hopplant> --Main Tax Accrual Oracle Package Declaration
22:43:06 hopplant> create or replace package pwrplant.tax_accrual_utility_pkg
22:43:06   2  as
22:43:06   3  	 G_PKG_VERSION varchar(35) := '2018.2.0.0';
22:43:06   4  	 -- Populates a table that stores the description of a particular processing step
22:43:06   5  	 -- along with the amount of time (in 1/100 sec) that has expired since the
22:43:06   6  	 -- debug process was started.
22:43:06   7  	 PROCEDURE p_pl_sql_debug(a_proc_name in varchar2, a_proc_step in varchar2);
22:43:06   8  
22:43:06   9  	 -- 1) Sets the session name that will be used when populating the debug table.  If the
22:43:06  10  	 --	  value in this parameter is a space, then the session name remains unchanged
22:43:06  11  	 -- 2) Resets the processing start time to 0
22:43:06  12  	 -- 3) Inserts a row into the debug table indicating the start of a new session.
22:43:06  13  	 PROCEDURE p_reset_session(a_session_name in varchar2);
22:43:06  14  
22:43:06  15  	 -- Clears the debug table of a particular session
22:43:06  16  	 -- If the session name parameter is a space, then the entire table is cleared out
22:43:06  17  	 PROCEDURE p_clear_debug(a_session_name in varchar2);
22:43:06  18  
22:43:06  19  end tax_accrual_utility_pkg;
22:43:06  20  /

Package created.

22:43:06 hopplant> 
22:43:06 hopplant> create or replace public synonym tax_accrual_utility_pkg for pwrplant.tax_accrual_utility_pkg;

Synonym created.

22:43:06 hopplant> grant execute on tax_accrual_utility_pkg to pwrplant_role_dev;

Grant succeeded.

22:43:06 hopplant> 
22:43:06 hopplant> -- ******************************************************************************************************** --
22:43:06 hopplant> --													       --
22:43:06 hopplant> --													       --
22:43:06 hopplant> --													       --
22:43:06 hopplant> -- PACKAGE BODY											       --
22:43:06 hopplant> --													       --
22:43:06 hopplant> --													       --
22:43:06 hopplant> --													       --
22:43:06 hopplant> -- ******************************************************************************************************** --
22:43:06 hopplant> create or replace package body tax_accrual_utility_pkg
22:43:06   2  AS
22:43:06   3  
22:43:06   4  	 g_start_time	number(22);
22:43:06   5  	 g_next_id	number(22);
22:43:06   6  	 g_session_name tax_accrual_plsql_debug.session_name%TYPE;
22:43:06   7  
22:43:06   8  	 --**************************************************************************************
22:43:06   9  	 --
22:43:06  10  	 -- p_pl_sql_debug_update
22:43:06  11  	 --
22:43:06  12  	 --**************************************************************************************
22:43:06  13  	 procedure p_pl_sql_debug_update(a_proc_name in varchar2, a_proc_step in varchar2, a_elapsed_time in long)
22:43:06  14  	 is
22:43:06  15  	    id	  number(22);
22:43:06  16  	    PRAGMA AUTONOMOUS_TRANSACTION;   -- This makes Oracle treat this as a standalone session, so the commit will not
22:43:06  17  					     --
22:43:06  18  	 begin
22:43:06  19  
22:43:06  20  	    g_next_id := g_next_id + 1;
22:43:06  21  
22:43:06  22  	    insert into tax_accrual_plsql_debug
22:43:06  23  	    (  id,
22:43:06  24  	       session_name,
22:43:06  25  	       proc_name,
22:43:06  26  	       proc_step,
22:43:06  27  	       elapsed_time
22:43:06  28  	    )
22:43:06  29  	    values
22:43:06  30  	    (  g_next_id,
22:43:06  31  	       g_session_name,
22:43:06  32  	       a_proc_name,
22:43:06  33  	       a_proc_step,
22:43:06  34  	       a_elapsed_time
22:43:06  35  	    );
22:43:06  36  	    commit;
22:43:06  37  	 end p_pl_sql_debug_update;
22:43:06  38  
22:43:06  39  	 --**************************************************************************************
22:43:06  40  	 --
22:43:06  41  	 -- p_pl_sql_debug
22:43:06  42  	 --
22:43:06  43  	 --**************************************************************************************
22:43:06  44  	 procedure p_pl_sql_debug(a_proc_name in varchar2, a_proc_step in varchar2)
22:43:06  45  	 is
22:43:06  46  	    proc_time number(22);
22:43:06  47  	 begin
22:43:06  48  	    if not tax_accrual_control_pkg.f_get_debug_status then
22:43:06  49  	       return;
22:43:06  50  	    end if;
22:43:06  51  
22:43:06  52  	    -- Calculate the total time since this debugger session began (in 1/100s of a second)
22:43:06  53  	    proc_time := dbms_utility.GET_TIME - g_start_time;
22:43:06  54  
22:43:06  55  	    p_pl_sql_debug_update(a_proc_name, a_proc_step, proc_time);
22:43:06  56  
22:43:06  57  	 end p_pl_sql_debug;
22:43:06  58  
22:43:06  59  	 --**************************************************************************************
22:43:06  60  	 --
22:43:06  61  	 -- p_clear_debug
22:43:06  62  	 --
22:43:06  63  	 --**************************************************************************************
22:43:06  64  	 procedure p_clear_debug(a_session_name in varchar2)
22:43:06  65  	 is
22:43:06  66  	    PRAGMA AUTONOMOUS_TRANSACTION;
22:43:06  67  	 begin
22:43:06  68  
22:43:06  69  	    if a_session_name <> ' ' then
22:43:06  70  	       delete from tax_accrual_plsql_debug
22:43:06  71  	       where session_name = a_session_name;
22:43:06  72  	    else
22:43:06  73  	       delete from tax_accrual_plsql_debug;
22:43:06  74  	    end if;
22:43:06  75  
22:43:06  76  	    commit;
22:43:06  77  
22:43:06  78  	    p_reset_session(' ');
22:43:06  79  
22:43:06  80  	 end p_clear_debug;
22:43:06  81  
22:43:06  82  	 --**************************************************************************************
22:43:06  83  	 --
22:43:06  84  	 -- p_reset_session
22:43:06  85  	 --
22:43:06  86  	 --**************************************************************************************
22:43:06  87  	 procedure p_reset_session(a_session_name in varchar2)
22:43:06  88  	 is
22:43:06  89  	 begin
22:43:06  90  	    if a_session_name <> ' ' then
22:43:06  91  	       g_session_name := a_session_name;
22:43:06  92  	    end if;
22:43:06  93  
22:43:06  94  	    select max(id)
22:43:06  95  	    into g_next_id
22:43:06  96  	    from tax_accrual_plsql_debug;
22:43:06  97  
22:43:06  98  	    if g_next_id is null then
22:43:06  99  	       g_next_id := 0;
22:43:06 100  	    end if;
22:43:06 101  
22:43:06 102  	    g_start_time := dbms_utility.GET_TIME;
22:43:06 103  	    p_pl_sql_debug_update('Debug Header', 'Begin debugging session ' || userenv('SESSIONID') || ' at ' || to_char(sysdate,'mm/dd/yyyy hh24:mi:ss'), 0);
22:43:06 104  
22:43:06 105  	 end p_reset_session;
22:43:06 106  
22:43:06 107  	 --**************************************************************************************
22:43:06 108  	 -- Initialization procedure (Called the first time this package is opened in a session)
22:43:06 109  	 -- 1) Set the start time so that the running time can be calculated
22:43:06 110  	 -- 2) Set the debug session name to the default (<username>:<sessionid>)
22:43:06 111  	 -- 3) Get the max ID from the debug table so that the ID column can be populated.
22:43:06 112  	 -- 4) Insert the debugger header record into the debug table.
22:43:06 113  	 --**************************************************************************************
22:43:06 114  	 begin
22:43:06 115  	    g_start_time := dbms_utility.GET_TIME;
22:43:06 116  
22:43:06 117  	    g_session_name := user || ':' || sys_context('USERENV','SESSIONID');
22:43:06 118  
22:43:06 119  	    select max(id)
22:43:06 120  	    into g_next_id
22:43:06 121  	    from tax_accrual_plsql_debug;
22:43:06 122  
22:43:06 123  	    if g_next_id is null then
22:43:06 124  	       g_next_id := 0;
22:43:06 125  	    end if;
22:43:06 126  
22:43:06 127  	    p_pl_sql_debug_update('Debug Header', 'Begin debugging session ' || userenv('SESSIONID') || ' at ' || to_char(sysdate,'mm/dd/yyyy hh24:mi:ss'), 0);
22:43:06 128  
22:43:06 129  end tax_accrual_utility_pkg;
22:43:06 130  --End of Package Body
22:43:06 131  /

Package body created.

22:43:06 hopplant> --**************************
22:43:06 hopplant> -- Log the run of the script
22:43:06 hopplant> --**************************
22:43:06 hopplant> insert into PP_SCHEMA_CHANGE_LOG
22:43:06   2  	 (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
22:43:06   3  	  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
22:43:06   4  values
22:43:06   5  	 (16313, 0, 2018, 2, 0, 0, 0, 'c:\plasticwks\powerplant\sql\packages',
22:43:06   6  	  'TAX_ACCRUAL_UTILITY_PKG.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

22:43:06 hopplant> commit;

Commit complete.

22:43:06 hopplant> SPOOL OFF
