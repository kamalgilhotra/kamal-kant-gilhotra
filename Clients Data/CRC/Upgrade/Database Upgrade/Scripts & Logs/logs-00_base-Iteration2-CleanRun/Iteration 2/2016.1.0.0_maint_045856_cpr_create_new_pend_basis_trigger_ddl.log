21:18:50 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2016.1.0.0_maint_045856_cpr_create_new_pend_basis_trigger_ddl.sql
21:18:50 hopplant> /*
21:18:50 hopplant> ||============================================================================
21:18:50 hopplant> || Application: PowerPlan
21:18:50 hopplant> || File Name:   maint_045856_cpr_create_new_pend_basis_trigger_ddl.sql
21:18:50 hopplant> ||============================================================================
21:18:50 hopplant> || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
21:18:50 hopplant> ||============================================================================
21:18:50 hopplant> || Version	 Date	    Revised By	     Reason for Change
21:18:50 hopplant> || ---------- ---------- ---------------- ------------------------------------
21:18:50 hopplant> || 2016.1.0.0 07/12/2016 Charlie Shilling prevent retirements from removing dollars from the retirement reversal basis bucket for tax repairs.
21:18:50 hopplant> ||============================================================================
21:18:50 hopplant> */
21:18:50 hopplant> CREATE OR REPLACE TRIGGER pend_basis_rpr_zero_ret_rev
21:18:50   2  	      BEFORE INSERT OR UPDATE ON pend_basis
21:18:50   3  	      FOR EACH ROW
21:18:50   4  DECLARE
21:18:50   5  	      l_activity_code	      pend_transaction.activity_code%TYPE;
21:18:50   6  	      l_ret_rev_basis	      book_summary.book_summary_id%TYPE;
21:18:50   7  	      l_ret_rev_found	      BOOLEAN := FALSE;
21:18:50   8  BEGIN
21:18:50   9  	      SELECT Trim(pt.activity_code)
21:18:50  10  	      INTO l_activity_code
21:18:50  11  	      FROM pend_transaction pt
21:18:50  12  	      WHERE pt.pend_trans_id = :new.pend_trans_id;
21:18:50  13  
21:18:50  14  	      --if this is a retirement transaction, make sure that the "Retirement Reversal" basis bucket is not retired from
21:18:50  15  	      IF l_activity_code IN ('MRET','URET','URGL','SALE','SAGL') THEN
21:18:50  16  		      BEGIN
21:18:50  17  			      SELECT book_summary_id
21:18:50  18  			      INTO l_ret_rev_basis
21:18:50  19  			      FROM book_summary
21:18:50  20  			      WHERE Lower(summary_name) = 'retirement reversal';
21:18:50  21  
21:18:50  22  			      l_ret_rev_found := TRUE;
21:18:50  23  		      EXCEPTION
21:18:50  24  			      WHEN No_Data_Found THEN
21:18:50  25  				      --this is OK
21:18:50  26  				      l_ret_rev_found := FALSE ;
21:18:50  27  			      WHEN Too_Many_Rows THEN
21:18:50  28  				      --this is not so OK.
21:18:50  29  				      Raise_Application_Error(-20000, 'There were multiple book summaries found with the summary name "Retirement Reversal"');
21:18:50  30  		      END;
21:18:50  31  
21:18:50  32  		      --only do special logic if we found a retirement reversal basis bucket
21:18:50  33  		      IF l_ret_rev_found THEN
21:18:50  34  			      --we know which basis bucket the retirement reversal is - make sure it is not being updated.
21:18:50  35  			      --This case statement is not ideal, but since we don't know which basis bucket is the retirement reversals bucket
21:18:50  36  			      --      and we can't do sql on the ":new" psuedovariable I thin this is the best work around.
21:18:50  37  			      --We did include an error message in the event that the book_summary_id is an invalid number since, as of
21:18:50  38  			      --      the writing of this trigger, we only have 70 basis buckets.
21:18:50  39  			      CASE l_ret_rev_basis
21:18:50  40  				      WHEN 1 THEN :new.basis_1 := 0;
21:18:50  41  				      WHEN 2 THEN :new.basis_2 := 0;
21:18:50  42  				      WHEN 3 THEN :new.basis_3 := 0;
21:18:50  43  				      WHEN 4 THEN :new.basis_4 := 0;
21:18:50  44  				      WHEN 5 THEN :new.basis_5 := 0;
21:18:50  45  				      WHEN 6 THEN :new.basis_6 := 0;
21:18:50  46  				      WHEN 7 THEN :new.basis_7 := 0;
21:18:50  47  				      WHEN 8 THEN :new.basis_8 := 0;
21:18:50  48  				      WHEN 9 THEN :new.basis_9 := 0;
21:18:50  49  				      WHEN 10 THEN :new.basis_10 := 0;
21:18:50  50  				      WHEN 11 THEN :new.basis_11 := 0;
21:18:50  51  				      WHEN 12 THEN :new.basis_12 := 0;
21:18:50  52  				      WHEN 13 THEN :new.basis_13 := 0;
21:18:50  53  				      WHEN 14 THEN :new.basis_14 := 0;
21:18:50  54  				      WHEN 15 THEN :new.basis_15 := 0;
21:18:50  55  				      WHEN 16 THEN :new.basis_16 := 0;
21:18:50  56  				      WHEN 17 THEN :new.basis_17 := 0;
21:18:50  57  				      WHEN 18 THEN :new.basis_18 := 0;
21:18:50  58  				      WHEN 19 THEN :new.basis_19 := 0;
21:18:50  59  				      WHEN 20 THEN :new.basis_20 := 0;
21:18:50  60  				      WHEN 21 THEN :new.basis_21 := 0;
21:18:50  61  				      WHEN 22 THEN :new.basis_22 := 0;
21:18:50  62  				      WHEN 23 THEN :new.basis_23 := 0;
21:18:50  63  				      WHEN 24 THEN :new.basis_24 := 0;
21:18:50  64  				      WHEN 25 THEN :new.basis_25 := 0;
21:18:50  65  				      WHEN 26 THEN :new.basis_26 := 0;
21:18:50  66  				      WHEN 27 THEN :new.basis_27 := 0;
21:18:50  67  				      WHEN 28 THEN :new.basis_28 := 0;
21:18:50  68  				      WHEN 29 THEN :new.basis_29 := 0;
21:18:50  69  				      WHEN 30 THEN :new.basis_30 := 0;
21:18:50  70  				      WHEN 31 THEN :new.basis_31 := 0;
21:18:50  71  				      WHEN 32 THEN :new.basis_32 := 0;
21:18:50  72  				      WHEN 33 THEN :new.basis_33 := 0;
21:18:50  73  				      WHEN 34 THEN :new.basis_34 := 0;
21:18:50  74  				      WHEN 35 THEN :new.basis_35 := 0;
21:18:50  75  				      WHEN 36 THEN :new.basis_36 := 0;
21:18:50  76  				      WHEN 37 THEN :new.basis_37 := 0;
21:18:50  77  				      WHEN 38 THEN :new.basis_38 := 0;
21:18:50  78  				      WHEN 39 THEN :new.basis_39 := 0;
21:18:50  79  				      WHEN 40 THEN :new.basis_40 := 0;
21:18:50  80  				      WHEN 41 THEN :new.basis_41 := 0;
21:18:50  81  				      WHEN 42 THEN :new.basis_42 := 0;
21:18:50  82  				      WHEN 43 THEN :new.basis_43 := 0;
21:18:50  83  				      WHEN 44 THEN :new.basis_44 := 0;
21:18:50  84  				      WHEN 45 THEN :new.basis_45 := 0;
21:18:50  85  				      WHEN 46 THEN :new.basis_46 := 0;
21:18:50  86  				      WHEN 47 THEN :new.basis_47 := 0;
21:18:50  87  				      WHEN 48 THEN :new.basis_48 := 0;
21:18:50  88  				      WHEN 49 THEN :new.basis_49 := 0;
21:18:50  89  				      WHEN 50 THEN :new.basis_50 := 0;
21:18:50  90  				      WHEN 51 THEN :new.basis_51 := 0;
21:18:50  91  				      WHEN 52 THEN :new.basis_52 := 0;
21:18:50  92  				      WHEN 53 THEN :new.basis_53 := 0;
21:18:50  93  				      WHEN 54 THEN :new.basis_54 := 0;
21:18:50  94  				      WHEN 55 THEN :new.basis_55 := 0;
21:18:50  95  				      WHEN 56 THEN :new.basis_56 := 0;
21:18:50  96  				      WHEN 57 THEN :new.basis_57 := 0;
21:18:50  97  				      WHEN 58 THEN :new.basis_58 := 0;
21:18:50  98  				      WHEN 59 THEN :new.basis_59 := 0;
21:18:50  99  				      WHEN 60 THEN :new.basis_60 := 0;
21:18:50 100  				      WHEN 61 THEN :new.basis_61 := 0;
21:18:50 101  				      WHEN 62 THEN :new.basis_62 := 0;
21:18:50 102  				      WHEN 63 THEN :new.basis_63 := 0;
21:18:50 103  				      WHEN 64 THEN :new.basis_64 := 0;
21:18:50 104  				      WHEN 65 THEN :new.basis_65 := 0;
21:18:50 105  				      WHEN 66 THEN :new.basis_66 := 0;
21:18:50 106  				      WHEN 67 THEN :new.basis_67 := 0;
21:18:50 107  				      WHEN 68 THEN :new.basis_68 := 0;
21:18:50 108  				      WHEN 69 THEN :new.basis_69 := 0;
21:18:50 109  				      WHEN 70 THEN :new.basis_70 := 0;
21:18:50 110  				      ELSE Raise_Application_Error(-20001, 'Invalid book_summary_id.');
21:18:50 111  			      END CASE;
21:18:50 112  		      END IF;
21:18:50 113  	      END IF;
21:18:50 114  END ;
21:18:50 115  /

Trigger created.

21:18:50 hopplant> 
21:18:50 hopplant> --****************************************************
21:18:50 hopplant> --Log the run of the script to local PP_SCHEMA_LOG
21:18:50 hopplant> --****************************************************
21:18:50 hopplant> INSERT INTO PP_SCHEMA_CHANGE_LOG
21:18:50   2  	      (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
21:18:50   3  	      SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
21:18:50   4  VALUES
21:18:50   5  	      (3239, 0, 2016, 1, 0, 0, 045856, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045856_cpr_create_new_pend_basis_trigger_ddl.sql', 1,
21:18:50   6  	      SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
21:18:50   7  	      SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

21:18:50 hopplant> COMMIT;

Commit complete.

21:18:50 hopplant> SPOOL OFF
