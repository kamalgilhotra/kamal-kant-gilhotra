18:21:28 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\10.4.2.4_maint_040121_depr_PKG_PP_DEPR_ACTIVITY.sql
18:21:28 hopplant> /*
18:21:28 hopplant> ||============================================================================
18:21:28 hopplant> || Application: PowerPlant
18:21:28 hopplant> || File Name:   maint_040121_depr_PKG_PP_DEPR_ACTIVITY.sql
18:21:28 hopplant> ||============================================================================
18:21:28 hopplant> || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
18:21:28 hopplant> ||============================================================================
18:21:28 hopplant> || Version  Date	  Revised By	   Reason for Change
18:21:28 hopplant> || -------- ---------- ---------------- --------------------------------------
18:21:28 hopplant> || 10.4.2.1 05/23/2014 Charlie Shilling maint 38309
18:21:28 hopplant> || 10.4.2.4 09/26/2014 Charlie Shilling maint 40121
18:21:28 hopplant> ||============================================================================
18:21:28 hopplant> */
18:21:28 hopplant> 
18:21:28 hopplant> create or replace package PKG_PP_DEPR_ACTIVITY
18:21:28   2  /*
18:21:28   3  ||============================================================================
18:21:28   4  || Application: PowerPlant
18:21:28   5  || Object Name: PKG_PP_DEPR_ACTIVITY
18:21:28   6  || Description: Depr Activity functions and procedures for PowerPlant application.
18:21:28   7  ||============================================================================
18:21:28   8  || Copyright (C) 2013 by PowerPlan, Inc. All Rights Reserved.
18:21:28   9  ||============================================================================
18:21:28  10  || Version Date	    Revised By	   Reason for Change
18:21:28  11  || ------- ---------- -------------- -----------------------------------------
18:21:28  12  || 1.0	 04/17/2013 Brandon Beck   initial creation
18:21:28  13  ||============================================================================
18:21:28  14  */
18:21:28  15   as
18:21:28  16  	 function F_RECURRINGACTIVITY(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
18:21:28  17  				      A_MONTHS	    PKG_PP_COMMON.DATE_TABTYPE,
18:21:28  18  				      A_MSG	    out varchar2) return number;
18:21:28  19  end PKG_PP_DEPR_ACTIVITY;
18:21:28  20  /

Package created.

18:21:28 hopplant> 
18:21:28 hopplant> 
18:21:28 hopplant> create or replace package body PKG_PP_DEPR_ACTIVITY as
18:21:28   2  	 --**************************************************************
18:21:28   3  	 --	  TYPE DECLARATIONS
18:21:28   4  	 --**************************************************************
18:21:28   5  
18:21:28   6  	 --**************************************************************
18:21:28   7  	 --	  VARIABLES
18:21:28   8  	 --**************************************************************
18:21:28   9  
18:21:28  10  	 --**************************************************************
18:21:28  11  	 --	  PROCEDURES
18:21:28  12  	 --**************************************************************
18:21:28  13  
18:21:28  14  	 --**************************************************************
18:21:28  15  	 --	  FUNCTIONS
18:21:28  16  	 --**************************************************************
18:21:28  17  	 -- removes recurring activity
18:21:28  18  	 function F_REMOVERECURRINGACTIVITY(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
18:21:28  19  					    A_MONTH	  date,
18:21:28  20  					    A_MSG	  out varchar2) return number is
18:21:28  21  
18:21:28  22  	 begin
18:21:28  23  	    pkg_pp_log.p_write_message( 'REMOVING recurring activity for the month' );
18:21:28  24  
18:21:28  25  	    FORALL i in INDICES OF a_company_ids
18:21:28  26  	    merge into depr_ledger a using
18:21:28  27  	    (
18:21:28  28  		      select depr_group_id, set_of_books_id, gl_post_mo_yr, sum(depr_exp_adjust) as depr_exp_adjust,
18:21:28  29  				      sum(cost_of_removal) as cost_of_removal, sum(salvage_cash) as salvage_cash,
18:21:28  30  				      sum(salvage_returns) as salvage_returns, sum(reserve_credits) as reserve_credits,
18:21:28  31  				      sum(reserve_tran_in) as reserve_tran_in, sum(reserve_tran_out) as reserve_tran_out,
18:21:28  32  				      sum(cor_res_tran_out) as cor_res_tran_out, sum(cor_res_tran_in) as cor_res_tran_in,
18:21:28  33  				      sum(cor_res_adjust) as cor_res_adjust, sum(cor_exp_adjust) as cor_exp_adjust,
18:21:28  34  				      sum(gain_loss) as gain_loss, sum(reserve_adjustments) as reserve_adjustments,
18:21:28  35  				      sum(impairment_reserve_act) as impairment_reserve_act, sum(salvage_exp_adjust) as salvage_exp_adjust
18:21:28  36  		      from (
18:21:28  37  			       select *
18:21:28  38  			       from
18:21:28  39  			       (
18:21:28  40  				      select da.depr_group_id, da.set_of_books_id, da.gl_post_mo_yr, da.description,
18:21:28  41  					 sum(da.amount) as amount
18:21:28  42  				      from depr_activity da, depr_group dg
18:21:28  43  				      where da.depr_group_id = dg.depr_group_id
18:21:28  44  				      and dg.company_id = a_company_ids(i)
18:21:28  45  				      and da.gl_post_mo_yr = a_month
18:21:28  46  				      and da.post_period_months is not null
18:21:28  47  				      group by da.depr_group_id, da.set_of_books_id, da.gl_post_mo_yr, da.description
18:21:28  48  			       ) b
18:21:28  49  			       model
18:21:28  50  			       partition by (b.depr_group_id, b.set_of_books_id, b.gl_post_mo_yr)
18:21:28  51  			       dimension by (b.description)
18:21:28  52  			       measures
18:21:28  53  			       (
18:21:28  54  				      amount,
18:21:28  55  				      0 as depr_exp_adjust, 0 as cost_of_removal, 0 as salvage_cash,
18:21:28  56  				      0 as salvage_returns, 0 as reserve_credits, 0 as reserve_tran_in,
18:21:28  57  				      0 as reserve_tran_out, 0 as cor_res_tran_out, 0 as cor_res_tran_in,
18:21:28  58  				      0 as cor_res_adjust, 0 as cor_exp_adjust, 0 as gain_loss,
18:21:28  59  				      0 as reserve_adjustments, 0 as impairment_reserve_act, 0 as salvage_exp_adjust
18:21:28  60  			       )
18:21:28  61  			       rules update
18:21:28  62  			       (
18:21:28  63  				      depr_exp_adjust['DEPR_EXP_ADJUST'] = amount['DEPR_EXP_ADJUST'],
18:21:28  64  				      cost_of_removal['COST_OF_REMOVAL'] = amount['COST_OF_REMOVAL'],
18:21:28  65  				      salvage_cash['SALVAGE_CASH'] = amount['SALVAGE_CASH'],
18:21:28  66  				      salvage_returns['SALVAGE_RETURNS'] = amount['SALVAGE_RETURNS'],
18:21:28  67  				      reserve_credits['RESERVE_CREDITS'] = amount['RESERVE_CREDITS'],
18:21:28  68  				      reserve_tran_in['RESERVE_TRANS_IN'] = amount['RESERVE_TRANS_IN'],
18:21:28  69  				      reserve_tran_out['RESERVE_TRANS_OUT'] = amount['RESERVE_TRANS_OUT'],
18:21:28  70  				      cor_res_tran_out['COST_OF_REMOVAL_TRANS_OUT'] = amount['COST_OF_REMOVAL_TRANS_OUT'],
18:21:28  71  				      cor_res_tran_in['COST_OF_REMOVAL_TRANS_IN'] = amount['COST_OF_REMOVAL_TRANS_IN'],
18:21:28  72  				      cor_res_adjust['COST_OF_REMOVAL_RES_ADJUST'] = amount['COST_OF_REMOVAL_RES_ADJUST'],
18:21:28  73  				      cor_exp_adjust['COST_OF_REMOVAL_EXP_ADJUST'] = amount['COST_OF_REMOVAL_EXP_ADJUST'],
18:21:28  74  				      gain_loss['GAIN_LOSS'] = amount['GAIN_LOSS'],
18:21:28  75  				      reserve_adjustments['RESERVE_ADJUSTMENT'] = amount['RESERVE_ADJUSTMENT'],
18:21:28  76  				      impairment_reserve_act['IMPAIRMENT_ACT'] = amount['IMPAIRMENT_ACT'],
18:21:28  77  				      salvage_exp_adjust['SALVAGE_EXP_ADJUST'] = amount['SALVAGE_EXP_ADJUST']
18:21:28  78  			       )
18:21:28  79  		      ) group by depr_group_id, set_of_books_id, gl_post_mo_yr
18:21:28  80  	    ) b
18:21:28  81  	    on
18:21:28  82  	    (
18:21:28  83  	       a.depr_group_id = b.depr_group_id
18:21:28  84  	       and a.set_of_books_id = b.set_of_books_id
18:21:28  85  	       and a.gl_post_mo_yr = b.gl_post_mo_yr
18:21:28  86  	    )
18:21:28  87  	    when matched then update
18:21:28  88  	    set
18:21:28  89  	       a.depr_exp_adjust = a.depr_exp_adjust - nvl(b.depr_exp_adjust, 0),
18:21:28  90  	       a.cost_of_removal = a.cost_of_removal - nvl(b.cost_of_removal, 0),
18:21:28  91  	       a.salvage_cash = a.salvage_cash - nvl(b.salvage_cash, 0),
18:21:28  92  	       a.salvage_returns = a.salvage_returns - nvl(b.salvage_returns, 0),
18:21:28  93  	       a.reserve_credits = a.reserve_credits - nvl(b.reserve_credits, 0),
18:21:28  94  	       a.reserve_tran_in = a.reserve_tran_in - nvl(b.reserve_tran_in, 0),
18:21:28  95  	       a.reserve_tran_out = a.reserve_tran_out - nvl(b.reserve_tran_out, 0),
18:21:28  96  	       a.cor_res_tran_out = a.cor_res_tran_out - nvl(b.cor_res_tran_out, 0),
18:21:28  97  	       a.cor_res_tran_in = a.cor_res_tran_in - nvl(b.cor_res_tran_in, 0),
18:21:28  98  	       a.cor_res_adjust = a.cor_res_adjust - nvl(b.cor_res_adjust, 0),
18:21:28  99  	       a.cor_exp_adjust = a.cor_exp_adjust - nvl(b.cor_exp_adjust, 0),
18:21:28 100  	       a.gain_loss = a.gain_loss - nvl(b.gain_loss, 0),
18:21:28 101  	       a.reserve_adjustments = a.reserve_adjustments - nvl(b.reserve_adjustments, 0),
18:21:28 102  	       a.impairment_reserve_act = a.impairment_reserve_act - nvl(b.impairment_reserve_act, 0),
18:21:28 103  	       a.salvage_exp_adjust = a.salvage_exp_adjust - nvl(b.salvage_exp_adjust, 0)
18:21:28 104  	    ;
18:21:28 105  
18:21:28 106  	    if sqlCode <> 0 then
18:21:28 107  	       a_msg := substr('ERROR Resetting Recurring Activity: '||sqlerrm, 1, 2000);
18:21:28 108  	       return -1;
18:21:28 109  	    end if;
18:21:28 110  
18:21:28 111  	    FORALL i in INDICES OF a_company_ids
18:21:28 112  	    delete
18:21:28 113  	    from depr_activity da
18:21:28 114  	    where depr_group_id in
18:21:28 115  	    (
18:21:28 116  	       select dg.depr_group_id
18:21:28 117  	       from depr_group dg
18:21:28 118  	       where dg.company_id = a_company_ids(i)
18:21:28 119  	    )
18:21:28 120  	    and da.gl_post_mo_yr = a_month
18:21:28 121  	    and da.post_period_months is not null
18:21:28 122  	    ;
18:21:28 123  
18:21:28 124  	    if sqlCode <> 0 then
18:21:28 125  	       a_msg := substr('ERROR Removing Recurring Activity: '||sqlerrm, 1, 2000);
18:21:28 126  	       return -1;
18:21:28 127  	    end if;
18:21:28 128  
18:21:28 129  	    return 1;
18:21:28 130  
18:21:28 131  	 exception when others then
18:21:28 132  	    /* this catches all SQL errors, including no_data_found */
18:21:28 133  	    a_msg := substr('ERROR Recurring Activity: '||sqlerrm, 1, 2000);
18:21:28 134  	    return -1;
18:21:28 135  	 end F_REMOVERECURRINGACTIVITY;
18:21:28 136  
18:21:28 137  	 function F_LOADRECURRINGACTIVITY(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
18:21:28 138  					  A_MONTH	date,
18:21:28 139  					  A_MSG 	out varchar2) return number is
18:21:28 140  
18:21:28 141  	 begin
18:21:28 142  	    pkg_pp_log.p_write_message( 'LOADING recurring activity for the month' );
18:21:28 143  	    -- insert into depr activity
18:21:28 144  	    FORALL i in INDICES OF a_company_ids
18:21:28 145  	    insert into depr_activity
18:21:28 146  	    (
18:21:28 147  	       depr_activity_id, depr_group_id, set_of_books_id, gl_post_mo_yr,
18:21:28 148  	       user_id, user_id1, user_id2, related_activity_id, depr_activity_code_id,
18:21:28 149  	       amount, description, long_description, gl_je_code, gl_post_flag,
18:21:28 150  	       credit_account, debit_account, post_period_months, asset_id, subledger_item_id
18:21:28 151  	    )
18:21:28 152  	    (
18:21:28 153  	       select
18:21:28 154  		  pwrplant1.nextval, da.depr_group_id, da.set_of_books_id, a_month,
18:21:28 155  		  da.user_id, da.user_id1, da.user_id2, da.related_activity_id, da.depr_activity_code_id,
18:21:28 156  		  da.amount, da.description, da.long_description, da.gl_je_code, da.gl_post_flag,
18:21:28 157  		  da.credit_account, da.debit_account, da.post_period_months, da.asset_id, da.subledger_item_id
18:21:28 158  	       from depr_activity_recurring da, depr_group dg
18:21:28 159  	       where da.depr_group_id = dg.depr_group_id
18:21:28 160  	       and dg.company_id = a_company_ids(i)
18:21:28 161  	       and da.gl_post_mo_yr <= a_month
18:21:28 162  	       and da.gl_post_mo_yr_end >= a_month
18:21:28 163  	       and mod(months_between(a_month, da.gl_post_mo_yr), da.post_period_months) = 0
18:21:28 164  	    );
18:21:28 165  	    if sqlCode <> 0 then
18:21:28 166  	       a_msg := substr('ERROR Inserting Recurring Activity: '||sqlerrm, 1, 2000);
18:21:28 167  	       return -1;
18:21:28 168  	    end if;
18:21:28 169  
18:21:28 170  	    -- summarize into depr ledger
18:21:28 171  	    FORALL i in INDICES OF a_company_ids
18:21:28 172  	    merge into depr_ledger a using
18:21:28 173  	    (
18:21:28 174  		      select depr_group_id, set_of_books_id, gl_post_mo_yr, sum(depr_exp_adjust) as depr_exp_adjust,
18:21:28 175  				      sum(cost_of_removal) as cost_of_removal, sum(salvage_cash) as salvage_cash,
18:21:28 176  				      sum(salvage_returns) as salvage_returns, sum(reserve_credits) as reserve_credits,
18:21:28 177  				      sum(reserve_tran_in) as reserve_tran_in, sum(reserve_tran_out) as reserve_tran_out,
18:21:28 178  				      sum(cor_res_tran_out) as cor_res_tran_out, sum(cor_res_tran_in) as cor_res_tran_in,
18:21:28 179  				      sum(cor_res_adjust) as cor_res_adjust, sum(cor_exp_adjust) as cor_exp_adjust,
18:21:28 180  				      sum(gain_loss) as gain_loss, sum(reserve_adjustments) as reserve_adjustments,
18:21:28 181  				      sum(impairment_reserve_act) as impairment_reserve_act, sum(salvage_exp_adjust) as salvage_exp_adjust
18:21:28 182  		      from (
18:21:28 183  		      select *
18:21:28 184  		      from
18:21:28 185  		      (
18:21:28 186  		      select da.depr_group_id, da.set_of_books_id, a_month as gl_post_mo_yr, da.description,
18:21:28 187  		      sum(da.amount) as amount
18:21:28 188  		      from depr_activity_recurring da, depr_group dg
18:21:28 189  		      where da.depr_group_id = dg.depr_group_id
18:21:28 190  		      and dg.company_id = a_company_ids(i)
18:21:28 191  		      and da.gl_post_mo_yr <= a_month
18:21:28 192  		      and da.gl_post_mo_yr_end >= a_month
18:21:28 193  		      and mod(months_between(a_month, da.gl_post_mo_yr), da.post_period_months) = 0
18:21:28 194  		      group by da.depr_group_id, da.set_of_books_id, a_month, da.description
18:21:28 195  		      ) b
18:21:28 196  		      model
18:21:28 197  		      partition by (b.depr_group_id, b.set_of_books_id, b.gl_post_mo_yr)
18:21:28 198  		      dimension by (b.description)
18:21:28 199  		      measures
18:21:28 200  		      (
18:21:28 201  		      amount,
18:21:28 202  		      0 as depr_exp_adjust, 0 as cost_of_removal, 0 as salvage_cash,
18:21:28 203  		      0 as salvage_returns, 0 as reserve_credits, 0 as reserve_tran_in,
18:21:28 204  		      0 as reserve_tran_out, 0 as cor_res_tran_out, 0 as cor_res_tran_in,
18:21:28 205  		      0 as cor_res_adjust, 0 as cor_exp_adjust, 0 as gain_loss,
18:21:28 206  		      0 as reserve_adjustments, 0 as impairment_reserve_act, 0 as salvage_exp_adjust
18:21:28 207  		      )
18:21:28 208  		      rules update
18:21:28 209  		      (
18:21:28 210  		      depr_exp_adjust['DEPR_EXP_ADJUST'] = amount['DEPR_EXP_ADJUST'],
18:21:28 211  		      cost_of_removal['COST_OF_REMOVAL'] = amount['COST_OF_REMOVAL'],
18:21:28 212  		      salvage_cash['SALVAGE_CASH'] = amount['SALVAGE_CASH'],
18:21:28 213  		      salvage_returns['SALVAGE_RETURNS'] = amount['SALVAGE_RETURNS'],
18:21:28 214  		      reserve_credits['RESERVE_CREDITS'] = amount['RESERVE_CREDITS'],
18:21:28 215  		      reserve_tran_in['RESERVE_TRANS_IN'] = amount['RESERVE_TRANS_IN'],
18:21:28 216  		      reserve_tran_out['RESERVE_TRANS_OUT'] = amount['RESERVE_TRANS_OUT'],
18:21:28 217  		      cor_res_tran_out['COST_OF_REMOVAL_TRANS_OUT'] = amount['COST_OF_REMOVAL_TRANS_OUT'],
18:21:28 218  		      cor_res_tran_in['COST_OF_REMOVAL_TRANS_IN'] = amount['COST_OF_REMOVAL_TRANS_IN'],
18:21:28 219  		      cor_res_adjust['COST_OF_REMOVAL_RES_ADJUST'] = amount['COST_OF_REMOVAL_RES_ADJUST'],
18:21:28 220  		      cor_exp_adjust['COST_OF_REMOVAL_EXP_ADJUST'] = amount['COST_OF_REMOVAL_EXP_ADJUST'],
18:21:28 221  		      gain_loss['GAIN_LOSS'] = amount['GAIN_LOSS'],
18:21:28 222  		      reserve_adjustments['RESERVE_ADJUSTMENT'] = amount['RESERVE_ADJUSTMENT'],
18:21:28 223  		      impairment_reserve_act['IMPAIRMENT_ACT'] = amount['IMPAIRMENT_ACT'],
18:21:28 224  		      salvage_exp_adjust['SALVAGE_EXP_ADJUST'] = amount['SALVAGE_EXP_ADJUST']
18:21:28 225  		      )
18:21:28 226  		      ) group by depr_group_id, set_of_books_id, gl_post_mo_yr
18:21:28 227  	    ) b
18:21:28 228  	    on
18:21:28 229  	    (
18:21:28 230  	       a.depr_group_id = b.depr_group_id
18:21:28 231  	       and a.set_of_books_id = b.set_of_books_id
18:21:28 232  	       and a.gl_post_mo_yr = b.gl_post_mo_yr
18:21:28 233  	    )
18:21:28 234  	    when matched then update
18:21:28 235  	    set
18:21:28 236  	       a.depr_exp_adjust = a.depr_exp_adjust + nvl(b.depr_exp_adjust, 0),
18:21:28 237  	       a.cost_of_removal = a.cost_of_removal + nvl(b.cost_of_removal, 0),
18:21:28 238  	       a.salvage_cash = a.salvage_cash + nvl(b.salvage_cash, 0),
18:21:28 239  	       a.salvage_returns = a.salvage_returns + nvl(b.salvage_returns, 0),
18:21:28 240  	       a.reserve_credits = a.reserve_credits + nvl(b.reserve_credits, 0),
18:21:28 241  	       a.reserve_tran_in = a.reserve_tran_in + nvl(b.reserve_tran_in, 0),
18:21:28 242  	       a.reserve_tran_out = a.reserve_tran_out + nvl(b.reserve_tran_out, 0),
18:21:28 243  	       a.cor_res_tran_out = a.cor_res_tran_out + nvl(b.cor_res_tran_out, 0),
18:21:28 244  	       a.cor_res_tran_in = a.cor_res_tran_in + nvl(b.cor_res_tran_in, 0),
18:21:28 245  	       a.cor_res_adjust = a.cor_res_adjust + nvl(b.cor_res_adjust, 0),
18:21:28 246  	       a.cor_exp_adjust = a.cor_exp_adjust + nvl(b.cor_exp_adjust, 0),
18:21:28 247  	       a.gain_loss = a.gain_loss + nvl(b.gain_loss, 0),
18:21:28 248  	       a.reserve_adjustments = a.reserve_adjustments + nvl(b.reserve_adjustments, 0),
18:21:28 249  	       a.impairment_reserve_act = a.impairment_reserve_act + nvl(b.impairment_reserve_act, 0),
18:21:28 250  	       a.salvage_exp_adjust = a.salvage_exp_adjust + nvl(b.salvage_exp_adjust, 0)
18:21:28 251  	    ;
18:21:28 252  	    if sqlCode <> 0 then
18:21:28 253  	       a_msg := substr('ERROR Loading Recurring Activity: '||sqlerrm, 1, 2000);
18:21:28 254  	       return -1;
18:21:28 255  	    end if;
18:21:28 256  
18:21:28 257  	    return 1;
18:21:28 258  
18:21:28 259  	 exception when others then
18:21:28 260  	    /* this catches all SQL errors, including no_data_found */
18:21:28 261  	    a_msg := substr('ERROR Recurring Activity: '||sqlerrm, 1, 2000);
18:21:28 262  	    return -1;
18:21:28 263  	 end f_loadRecurringActivity;
18:21:28 264  
18:21:28 265  	 -- Performs recurring activity
18:21:28 266  	 -- remove recurring activity
18:21:28 267  	 -- then load recurring activity
18:21:28 268  	 function F_RECURRINGACTIVITY(A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
18:21:28 269  				      A_MONTHS	    PKG_PP_COMMON.DATE_TABTYPE,
18:21:28 270  				      A_MSG	    out varchar2) return number is
18:21:28 271  
18:21:28 272  	    my_num number;
18:21:28 273  
18:21:28 274  	 begin
18:21:28 275  	    my_num := 1;
18:21:28 276  	    for i in 1 .. a_months.COUNT loop
18:21:28 277  	       if f_removeRecurringActivity(a_company_ids, a_months(i), a_msg) = -1 then
18:21:28 278  		  my_num := -1;
18:21:28 279  		  exit;
18:21:28 280  	       end if;
18:21:28 281  	       if f_loadRecurringActivity(a_company_ids, a_months(i), a_msg) = -1 then
18:21:28 282  		  my_num := -1;
18:21:28 283  		  exit;
18:21:28 284  	       end if;
18:21:28 285  	    end loop;
18:21:28 286  	    return my_num;
18:21:28 287  
18:21:28 288  	 exception when others then
18:21:28 289  	    /* this catches all SQL errors, including no_data_found */
18:21:28 290  	    a_msg := substr('ERROR loading recurring activity: '|| sqlerrm, 1, 2000);
18:21:28 291  	    return -1;
18:21:28 292  	 end F_RECURRINGACTIVITY;
18:21:28 293  
18:21:28 294  end PKG_PP_DEPR_ACTIVITY;
18:21:28 295  /

Package body created.

18:21:28 hopplant> 
18:21:28 hopplant> --**************************
18:21:28 hopplant> -- Log the run of the script
18:21:28 hopplant> --**************************
18:21:28 hopplant> 
18:21:28 hopplant> insert into PP_SCHEMA_CHANGE_LOG
18:21:28   2  	 (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
18:21:28   3  	  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
18:21:28   4  values
18:21:28   5  	 (1441, 0, 10, 4, 2, 4, 40121, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.4_maint_040121_depr_PKG_PP_DEPR_ACTIVITY.sql', 1,
18:21:28   6  	  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
18:21:28   7  	  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

18:21:28 hopplant> commit;

Commit complete.

18:21:28 hopplant> SPOOL OFF
