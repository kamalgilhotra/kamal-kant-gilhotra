22:55:12 hopplant> @&&PP_SCRIPT_PATH.packages_released\2018.2.1.0_TAX_ACCRUAL_UTILITY_PKG.sql
22:55:12 hopplant> --||============================================================================
22:55:12 hopplant> --|| Application: PowerPlant
22:55:12 hopplant> --|| Object Name: tax_accrual_utility_pkg
22:55:12 hopplant> --|| Description: Contains several utilities used in processing PL/SQL for
22:55:12 hopplant> --|| 	      provision.
22:55:12 hopplant> --||============================================================================
22:55:12 hopplant> --|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
22:55:12 hopplant> --||============================================================================
22:55:12 hopplant> --|| Patch		Date	   Revised By	  Reason for Change
22:55:12 hopplant> --|| -------------	---------- -------------- ----------------------------------
22:55:12 hopplant> --|| ###PATCH(403)	08/04/2010 Blake Andrews  Initial Creation
22:55:12 hopplant> --||============================================================================
22:55:12 hopplant> 
22:55:12 hopplant> --Main Tax Accrual Oracle Package Declaration
22:55:12 hopplant> create or replace package pwrplant.tax_accrual_utility_pkg
22:55:12   2  as
22:55:12   3  	 G_PKG_VERSION varchar(35) := '2018.2.1.0';
22:55:12   4  	 -- Populates a table that stores the description of a particular processing step
22:55:12   5  	 -- along with the amount of time (in 1/100 sec) that has expired since the
22:55:12   6  	 -- debug process was started.
22:55:12   7  	 PROCEDURE p_pl_sql_debug(a_proc_name in varchar2, a_proc_step in varchar2);
22:55:12   8  
22:55:12   9  	 -- 1) Sets the session name that will be used when populating the debug table.  If the
22:55:12  10  	 --	  value in this parameter is a space, then the session name remains unchanged
22:55:12  11  	 -- 2) Resets the processing start time to 0
22:55:12  12  	 -- 3) Inserts a row into the debug table indicating the start of a new session.
22:55:12  13  	 PROCEDURE p_reset_session(a_session_name in varchar2);
22:55:12  14  
22:55:12  15  	 -- Clears the debug table of a particular session
22:55:12  16  	 -- If the session name parameter is a space, then the entire table is cleared out
22:55:12  17  	 PROCEDURE p_clear_debug(a_session_name in varchar2);
22:55:12  18  
22:55:12  19  end tax_accrual_utility_pkg;
22:55:12  20  /

Package created.

22:55:12 hopplant> 
22:55:12 hopplant> create or replace public synonym tax_accrual_utility_pkg for pwrplant.tax_accrual_utility_pkg;

Synonym created.

22:55:12 hopplant> grant execute on tax_accrual_utility_pkg to pwrplant_role_dev;

Grant succeeded.

22:55:12 hopplant> 
22:55:12 hopplant> -- ******************************************************************************************************** --
22:55:12 hopplant> --													       --
22:55:12 hopplant> --													       --
22:55:12 hopplant> --													       --
22:55:12 hopplant> -- PACKAGE BODY											       --
22:55:12 hopplant> --													       --
22:55:12 hopplant> --													       --
22:55:12 hopplant> --													       --
22:55:12 hopplant> -- ******************************************************************************************************** --
22:55:12 hopplant> create or replace package body tax_accrual_utility_pkg
22:55:12   2  AS
22:55:12   3  
22:55:12   4  	 g_start_time	number(22);
22:55:12   5  	 g_next_id	number(22);
22:55:12   6  	 g_session_name tax_accrual_plsql_debug.session_name%TYPE;
22:55:12   7  
22:55:12   8  	 --**************************************************************************************
22:55:12   9  	 --
22:55:12  10  	 -- p_pl_sql_debug_update
22:55:12  11  	 --
22:55:12  12  	 --**************************************************************************************
22:55:12  13  	 procedure p_pl_sql_debug_update(a_proc_name in varchar2, a_proc_step in varchar2, a_elapsed_time in long)
22:55:12  14  	 is
22:55:12  15  	    id	  number(22);
22:55:12  16  	    PRAGMA AUTONOMOUS_TRANSACTION;   -- This makes Oracle treat this as a standalone session, so the commit will not
22:55:12  17  					     --
22:55:12  18  	 begin
22:55:12  19  
22:55:12  20  	    g_next_id := g_next_id + 1;
22:55:12  21  
22:55:12  22  	    insert into tax_accrual_plsql_debug
22:55:12  23  	    (  id,
22:55:12  24  	       session_name,
22:55:12  25  	       proc_name,
22:55:12  26  	       proc_step,
22:55:12  27  	       elapsed_time
22:55:12  28  	    )
22:55:12  29  	    values
22:55:12  30  	    (  g_next_id,
22:55:12  31  	       g_session_name,
22:55:12  32  	       a_proc_name,
22:55:12  33  	       a_proc_step,
22:55:12  34  	       a_elapsed_time
22:55:12  35  	    );
22:55:12  36  	    commit;
22:55:12  37  	 end p_pl_sql_debug_update;
22:55:12  38  
22:55:12  39  	 --**************************************************************************************
22:55:12  40  	 --
22:55:12  41  	 -- p_pl_sql_debug
22:55:12  42  	 --
22:55:12  43  	 --**************************************************************************************
22:55:12  44  	 procedure p_pl_sql_debug(a_proc_name in varchar2, a_proc_step in varchar2)
22:55:12  45  	 is
22:55:12  46  	    proc_time number(22);
22:55:12  47  	 begin
22:55:12  48  	    if not tax_accrual_control_pkg.f_get_debug_status then
22:55:12  49  	       return;
22:55:12  50  	    end if;
22:55:12  51  
22:55:12  52  	    -- Calculate the total time since this debugger session began (in 1/100s of a second)
22:55:12  53  	    proc_time := dbms_utility.GET_TIME - g_start_time;
22:55:12  54  
22:55:12  55  	    p_pl_sql_debug_update(a_proc_name, a_proc_step, proc_time);
22:55:12  56  
22:55:12  57  	 end p_pl_sql_debug;
22:55:12  58  
22:55:12  59  	 --**************************************************************************************
22:55:12  60  	 --
22:55:12  61  	 -- p_clear_debug
22:55:12  62  	 --
22:55:12  63  	 --**************************************************************************************
22:55:12  64  	 procedure p_clear_debug(a_session_name in varchar2)
22:55:12  65  	 is
22:55:12  66  	    PRAGMA AUTONOMOUS_TRANSACTION;
22:55:12  67  	 begin
22:55:12  68  
22:55:12  69  	    if a_session_name <> ' ' then
22:55:12  70  	       delete from tax_accrual_plsql_debug
22:55:12  71  	       where session_name = a_session_name;
22:55:12  72  	    else
22:55:12  73  	       delete from tax_accrual_plsql_debug;
22:55:12  74  	    end if;
22:55:12  75  
22:55:12  76  	    commit;
22:55:12  77  
22:55:12  78  	    p_reset_session(' ');
22:55:12  79  
22:55:12  80  	 end p_clear_debug;
22:55:12  81  
22:55:12  82  	 --**************************************************************************************
22:55:12  83  	 --
22:55:12  84  	 -- p_reset_session
22:55:12  85  	 --
22:55:12  86  	 --**************************************************************************************
22:55:12  87  	 procedure p_reset_session(a_session_name in varchar2)
22:55:12  88  	 is
22:55:12  89  	 begin
22:55:12  90  	    if a_session_name <> ' ' then
22:55:12  91  	       g_session_name := a_session_name;
22:55:12  92  	    end if;
22:55:12  93  
22:55:12  94  	    select max(id)
22:55:12  95  	    into g_next_id
22:55:12  96  	    from tax_accrual_plsql_debug;
22:55:12  97  
22:55:12  98  	    if g_next_id is null then
22:55:12  99  	       g_next_id := 0;
22:55:12 100  	    end if;
22:55:12 101  
22:55:12 102  	    g_start_time := dbms_utility.GET_TIME;
22:55:12 103  	    p_pl_sql_debug_update('Debug Header', 'Begin debugging session ' || userenv('SESSIONID') || ' at ' || to_char(sysdate,'mm/dd/yyyy hh24:mi:ss'), 0);
22:55:12 104  
22:55:12 105  	 end p_reset_session;
22:55:12 106  
22:55:12 107  	 --**************************************************************************************
22:55:12 108  	 -- Initialization procedure (Called the first time this package is opened in a session)
22:55:12 109  	 -- 1) Set the start time so that the running time can be calculated
22:55:12 110  	 -- 2) Set the debug session name to the default (<username>:<sessionid>)
22:55:12 111  	 -- 3) Get the max ID from the debug table so that the ID column can be populated.
22:55:12 112  	 -- 4) Insert the debugger header record into the debug table.
22:55:12 113  	 --**************************************************************************************
22:55:12 114  	 begin
22:55:12 115  	    g_start_time := dbms_utility.GET_TIME;
22:55:12 116  
22:55:12 117  	    g_session_name := user || ':' || sys_context('USERENV','SESSIONID');
22:55:12 118  
22:55:12 119  	    select max(id)
22:55:12 120  	    into g_next_id
22:55:12 121  	    from tax_accrual_plsql_debug;
22:55:12 122  
22:55:12 123  	    if g_next_id is null then
22:55:12 124  	       g_next_id := 0;
22:55:12 125  	    end if;
22:55:12 126  
22:55:12 127  	    p_pl_sql_debug_update('Debug Header', 'Begin debugging session ' || userenv('SESSIONID') || ' at ' || to_char(sysdate,'mm/dd/yyyy hh24:mi:ss'), 0);
22:55:12 128  
22:55:12 129  end tax_accrual_utility_pkg;
22:55:12 130  --End of Package Body
22:55:12 131  /

Package body created.

22:55:12 hopplant> --**************************
22:55:12 hopplant> -- Log the run of the script
22:55:12 hopplant> --**************************
22:55:12 hopplant> insert into PP_SCHEMA_CHANGE_LOG
22:55:12   2  	 (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
22:55:12   3  	  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
22:55:12   4  values
22:55:12   5  	 (18227, 0, 2018, 2, 1, 0, 0, 'C:\BitBucketRepos\Classic_PB\scripts\00_base\packages',
22:55:12   6  	  'TAX_ACCRUAL_UTILITY_PKG.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

22:55:12 hopplant> commit;

Commit complete.

22:55:12 hopplant> SPOOL OFF
