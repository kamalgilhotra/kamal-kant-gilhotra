@echo off
cls
Setlocal EnableDelayedExpansion
set "path0=%~dp0"
set "errors=%path0%errors.txt"

rem case-insensitive search for the string "ORA-" in all log files
rem in the current directory, piping the output to the errors.txt file
rem in the same directory

findstr /ip /c:"ORA-" *.log > errors.txt
findstr /ip /c:"SP2-" *.log >> errors.txt
findstr /ip /c:"Enter value for" *.log >> errors.txt
findstr /ip /v /c:"PPCERR varchar2(10) := 'PPC-ERR> '" *.log | findstr /ip /c:"PPC-ERR>"  >> errors.txt
findstr /ip /c:"maximum size" *.log >> errors.txt
findstr /ip /c:"Warning:" *.log >> errors.txt

set "cmd=findstr /R /N "^^" errors.txt | find /C ":""

for /f %%a in ('!cmd!') do set number=%%a
echo Possible Errors Found: %number% 

:exit
pause