21:45:46 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2017.1.0.0_maint_048037_lease_update_invoice_view_historic_rates_ddl.sql
21:45:46 hopplant> /*
21:45:46 hopplant> ||============================================================================
21:45:46 hopplant> || Application: PowerPlan
21:45:46 hopplant> || File Name: maint_048037_lease_update_invoice_view_historic_rates_ddl.sql
21:45:46 hopplant> ||============================================================================
21:45:46 hopplant> || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
21:45:46 hopplant> ||============================================================================
21:45:46 hopplant> || Version	Date	  Revised By	 Reason for Change
21:45:46 hopplant> || ---------- ---------- -------------- ----------------------------------------
21:45:46 hopplant> || 2017.1.0.0 07/26/2017 Jared Watkins  Update the v_ls_invoice_fx_vw to follow the same
21:45:46 hopplant> ||			 logic as the payments views since we only want to use a calculated rate and not an effective dated rate
21:45:46 hopplant> ||============================================================================
21:45:46 hopplant> */
21:45:46 hopplant> 
21:45:46 hopplant> CREATE OR REPLACE VIEW V_LS_INVOICE_FX_VW AS
21:45:46   2  WITH
21:45:46   3  cur AS (
21:45:46   4  	SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
21:45:46   5  	  contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
21:45:46   6  	FROM currency contract_cur
21:45:46   7  	UNION
21:45:46   8  	SELECT 2, company_cur.currency_id,
21:45:46   9  	  company_cur.currency_display_symbol, company_cur.iso_code, 0
21:45:46  10  	FROM currency company_cur
21:45:46  11  )
21:45:46  12  SELECT
21:45:46  13   invoice_id,
21:45:46  14   vendor_id,
21:45:46  15   invoice_status,
21:45:46  16   invoice_number,
21:45:46  17   inv.ls_asset_Id,
21:45:46  18   inv.ilr_id,
21:45:46  19   inv.ilr_Id tax_local_id,
21:45:46  20   gl_posting_Mo_yr,
21:45:46  21   invoice_amount * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_amount,
21:45:46  22   invoice_interest * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_interest,
21:45:46  23   invoice_executory * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_executory,
21:45:46  24   invoice_contingent * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_contingent,
21:45:46  25   lease.lease_id,
21:45:46  26   lease.lease_number,
21:45:46  27   inv.company_id,
21:45:46  28   cur.ls_cur_type ls_cur_type,
21:45:46  29   cr.exchange_date,
21:45:46  30   lease.contract_currency_id,
21:45:46  31   cur.currency_id display_currency_id,
21:45:46  32   cs.currency_id company_currency_id,
21:45:46  33   decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) rate,
21:45:46  34   cur.iso_code,
21:45:46  35   cur.currency_display_symbol
21:45:46  36   FROM ls_invoice inv
21:45:46  37   INNER JOIN currency_schema cs
21:45:46  38  	  ON inv.company_id = cs.company_id
21:45:46  39   INNER JOIN ls_lease lease
21:45:46  40  	  ON inv.lease_id = lease.lease_id
21:45:46  41   INNER JOIN cur
21:45:46  42  	  ON (cur.ls_cur_type = 1 AND
21:45:46  43  	     cur.currency_id = lease.contract_currency_id)
21:45:46  44  	  OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
21:45:46  45   LEFT OUTER JOIN ls_lease_calculated_date_rates cr
21:45:46  46  	  ON lease.contract_currency_id = cr.contract_currency_id
21:45:46  47  	 AND cur.currency_id = cr.company_currency_id
21:45:46  48  	 AND inv.company_id = cr.company_id
21:45:46  49  	 AND inv.gl_posting_mo_yr = cr.accounting_month
21:45:46  50   WHERE Nvl(cr.exchange_rate_type_id, 1) = 1;

View created.

21:45:46 hopplant> 
21:45:46 hopplant> 
21:45:46 hopplant>  --****************************************************
21:45:46 hopplant> --Log the run of the script to local PP_SCHEMA_LOG
21:45:46 hopplant> --****************************************************
21:45:46 hopplant> INSERT INTO PP_SCHEMA_CHANGE_LOG
21:45:46   2  	      (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
21:45:46   3  	      SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
21:45:46   4  VALUES
21:45:46   5  	      (3611, 0, 2017, 1, 0, 0, 48037, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048037_lease_update_invoice_view_historic_rates_ddl.sql', 1,
21:45:46   6  	      SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
21:45:46   7  	      SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

21:45:46 hopplant> COMMIT;

Commit complete.

21:45:46 hopplant> SPOOL OFF
