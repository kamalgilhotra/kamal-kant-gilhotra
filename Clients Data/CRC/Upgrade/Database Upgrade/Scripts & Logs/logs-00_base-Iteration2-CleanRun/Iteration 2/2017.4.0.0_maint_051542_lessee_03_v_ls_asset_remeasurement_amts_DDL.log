22:32:35 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2017.4.0.0_maint_051542_lessee_03_v_ls_asset_remeasurement_amts_DDL.sql
22:32:35 hopplant> /*
22:32:35 hopplant> ||=============================================================================
22:32:35 hopplant> || Application: PowerPlan
22:32:35 hopplant> || File Name:   maint_051542_lessee_03_v_ls_asset_remeasurement_amts_DDL.sql
22:32:35 hopplant> ||=============================================================================
22:32:35 hopplant> || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
22:32:35 hopplant> ||=============================================================================
22:32:35 hopplant> || Version	  Date	     Revised By       Reason for Change
22:32:35 hopplant> || ----------  ---------- ---------------- ------------------------------------
22:32:35 hopplant> || 2017.4.0.0  06/15/2018 David Conway     Off Balance Sheet Expense by Set of Books
22:32:35 hopplant> ||============================================================================
22:32:35 hopplant> */
22:32:35 hopplant> 
22:32:35 hopplant> CREATE OR REPLACE VIEW V_LS_ASSET_REMEASUREMENT_AMTS AS
22:32:35   2  SELECT stg.ilr_id, stg.revision, sch.ls_asset_id, sch.set_of_books_id, o.remeasurement_date, ilr.current_revision,
22:32:35   3  CASE
22:32:35   4  	WHEN sch.is_om = 1 AND stg.is_om = 0 THEN
22:32:35   5  	  1
22:32:35   6  	ELSE
22:32:35   7  	  0
22:32:35   8  END om_to_cap_indicator,
22:32:35   9  sch.end_obligation AS prior_month_end_obligation, sch.end_liability AS prior_month_end_liability,
22:32:35  10  sch.end_lt_obligation AS prior_month_end_lt_obligation, sch.end_lt_liability AS prior_month_end_lt_liability,
22:32:35  11  sch.end_deferred_rent AS prior_month_end_deferred_rent,
22:32:35  12  sch.end_prepaid_rent AS prior_month_end_prepaid_rent,
22:32:35  13  depr.end_reserve AS prior_month_end_depr_reserve,
22:32:35  14  sch.end_capital_cost - depr.end_reserve AS prior_month_end_nbv,
22:32:35  15  sch.end_capital_cost AS prior_month_end_capital_cost,
22:32:35  16  sch.end_net_rou_asset as prior_month_net_rou_asset,
22:32:35  17  sch.end_arrears_accrual AS prior_month_end_arrears_accr,
22:32:35  18  o.remeasurement_type as remeasurement_type,
22:32:35  19  o.partial_termination as partial_termination,
22:32:35  20  prior_opt.asset_quantity as original_asset_quantity,
22:32:35  21  o.asset_quantity AS current_asset_quantity,
22:32:35  22  1 is_remeasurement
22:32:35  23  FROM ls_ilr_stg stg
22:32:35  24  	JOIN ls_ilr ilr ON stg.ilr_id = ilr.ilr_id
22:32:35  25  	JOIN ls_ilr_options o ON stg.ilr_id = o.ilr_id AND stg.revision = o.revision
22:32:35  26  	JOIN ls_ilr_options prior_opt ON stg.ilr_id = prior_opt.ilr_id AND prior_opt.revision = ilr.current_revision
22:32:35  27  	JOIN ls_ilr_asset_map map ON map.ilr_id = stg.ilr_id AND map.revision = ilr.current_revision
22:32:35  28  	JOIN ls_asset_schedule sch ON map.ls_asset_id = sch.ls_asset_id AND sch.revision = ilr.current_revision AND sch.set_of_books_id = stg.set_of_books_id
22:32:35  29  	left OUTER JOIN ls_depr_forecast depr ON depr.ls_asset_id = map.ls_asset_id AND depr.revision = ilr.current_revision AND depr.set_of_books_id = stg.set_of_books_id
22:32:35  30  WHERE ilr.current_revision <> stg.revision
22:32:35  31  AND o.remeasurement_date IS NOT NULL
22:32:35  32  AND Trunc(o.remeasurement_date, 'month') = Add_Months(sch.MONTH,1)
22:32:35  33  AND (Trunc(o.remeasurement_date, 'month') = Add_Months(depr.MONTH,1) OR depr.MONTH IS NULL)
22:32:35  34  ;

View created.

22:32:35 hopplant> 
22:32:35 hopplant> --****************************************************
22:32:35 hopplant> --Log the run of the script to local PP_SCHEMA_LOG
22:32:35 hopplant> --****************************************************
22:32:35 hopplant> INSERT INTO PP_SCHEMA_CHANGE_LOG
22:32:35   2  	  (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
22:32:35   3  	  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
22:32:35   4  VALUES
22:32:35   5  	  (7464, 0, 2017, 4, 0, 0, 51542, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051542_lessee_03_v_ls_asset_remeasurement_amts_DDL.sql', 1,
22:32:35   6  	  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
22:32:35   7  	  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

22:32:35 hopplant> COMMIT;

Commit complete.

22:32:35 hopplant> SPOOL OFF
