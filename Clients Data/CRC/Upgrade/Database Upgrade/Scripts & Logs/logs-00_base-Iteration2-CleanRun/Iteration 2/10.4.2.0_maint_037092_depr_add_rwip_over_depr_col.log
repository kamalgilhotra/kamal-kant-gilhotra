18:20:08 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\10.4.2.0_maint_037092_depr_add_rwip_over_depr_col.sql
18:20:08 hopplant> SET SERVEROUTPUT ON
18:20:08 hopplant> /*
18:20:08 hopplant> ||============================================================================
18:20:08 hopplant> || Application: PowerPlant
18:20:08 hopplant> || File Name:   maint_037092_depr_add_rwip_over_depr_col.sql
18:20:08 hopplant> ||============================================================================
18:20:08 hopplant> || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
18:20:08 hopplant> ||============================================================================
18:20:08 hopplant> || Version  Date	  Revised By	   Reason for Change
18:20:08 hopplant> || -------- ---------- ---------------- --------------------------------------
18:20:08 hopplant> || 10.4.2.0 02/24/2014 Charlie Shilling maint-36045 - add rounding plug columns
18:20:08 hopplant> ||============================================================================
18:20:08 hopplant> */
18:20:08 hopplant> 
18:20:08 hopplant> declare
18:20:08   2  	 procedure ADD_COLUMN(V_TABLE_NAME varchar2,
18:20:08   3  			 V_COL_NAME   varchar2,
18:20:08   4  			 V_DATATYPE   varchar2,
18:20:08   5  			 V_COMMENT    varchar2) is
18:20:08   6  	 begin
18:20:08   7  	    begin
18:20:08   8  	       execute immediate 'alter table ' || V_TABLE_NAME || ' add ' || V_COL_NAME || ' ' ||
18:20:08   9  			     V_DATATYPE;
18:20:08  10  	       DBMS_OUTPUT.PUT_LINE('Sucessfully added column ' || V_COL_NAME || ' to table ' ||
18:20:08  11  			       V_TABLE_NAME || '.');
18:20:08  12  	    exception
18:20:08  13  	       when others then
18:20:08  14  		  if sqlcode = -1430 then
18:20:08  15  		     --1430 is "column being added already exists in table", so we are good here
18:20:08  16  		     DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' already exists on table ' ||
18:20:08  17  				     V_TABLE_NAME || '. No action necessasry.');
18:20:08  18  		  else
18:20:08  19  		     RAISE_APPLICATION_ERROR(-20001,
18:20:08  20  				       'Could not add column ' || V_COL_NAME || ' to ' ||
18:20:08  21  				       V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
18:20:08  22  		  end if;
18:20:08  23  	    end;
18:20:08  24  
18:20:08  25  	    begin
18:20:08  26  	       execute immediate 'comment on column ' || V_TABLE_NAME || '.' || V_COL_NAME || ' is ''' ||
18:20:08  27  			     V_COMMENT || '''';
18:20:08  28  	       DBMS_OUTPUT.PUT_LINE('  Sucessfully added the comment to column ' || V_COL_NAME ||
18:20:08  29  			       ' to table ' || V_TABLE_NAME || '.');
18:20:08  30  	    exception
18:20:08  31  	       when others then
18:20:08  32  		  RAISE_APPLICATION_ERROR(-20002,
18:20:08  33  				    'Could not add comment to column ' || V_COL_NAME || ' to ' ||
18:20:08  34  				    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
18:20:08  35  	    end;
18:20:08  36  	 end ADD_COLUMN;
18:20:08  37  begin
18:20:08  38  	 --add rwip_in_over_depr to normal staging tables and combined staging tables
18:20:08  39  	 ADD_COLUMN('depr_calc_stg',
18:20:08  40  		  'rwip_in_over_depr',
18:20:08  41  		  'number(1,0) default 0',
18:20:08  42  		  'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes');
18:20:08  43  	 ADD_COLUMN('depr_calc_stg_arc',
18:20:08  44  		  'rwip_in_over_depr',
18:20:08  45  		  'number(1,0) default 0',
18:20:08  46  		  'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes');
18:20:08  47  	 ADD_COLUMN('fcst_depr_calc_stg_arc',
18:20:08  48  		  'rwip_in_over_depr',
18:20:08  49  		  'number(1,0) default 0',
18:20:08  50  		  'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes');
18:20:08  51  	 ADD_COLUMN('depr_calc_combined_stg',
18:20:08  52  		  'rwip_in_over_depr',
18:20:08  53  		  'number(1,0) default 0',
18:20:08  54  		  'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes');
18:20:08  55  	 ADD_COLUMN('depr_calc_combined_arc',
18:20:08  56  		  'rwip_in_over_depr',
18:20:08  57  		  'number(1,0) default 0',
18:20:08  58  		  'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes');
18:20:08  59  	 ADD_COLUMN('fcst_depr_calc_combined_arc',
18:20:08  60  		  'rwip_in_over_depr',
18:20:08  61  		  'number(1,0) default 0',
18:20:08  62  		  'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes');
18:20:08  63  
18:20:08  64  	 --add rwip_allocation to combined stg
18:20:08  65  	 ADD_COLUMN('depr_calc_combined_stg',
18:20:08  66  		  'rwip_allocation',
18:20:08  67  		  'number(22,0) default 0',
18:20:08  68  		  'The amount of rwip allocation to this group.');
18:20:08  69  	 ADD_COLUMN('depr_calc_combined_arc',
18:20:08  70  		  'rwip_allocation',
18:20:08  71  		  'number(22,0) default 0',
18:20:08  72  		  'The amount of rwip allocation to this group.');
18:20:08  73  	 ADD_COLUMN('fcst_depr_calc_combined_arc',
18:20:08  74  		  'rwip_allocation',
18:20:08  75  		  'number(22,0) default 0',
18:20:08  76  		  'The amount of rwip allocation to this group.');
18:20:08  77  
18:20:08  78  	 --add rwip_cost_of_removal to combined stg
18:20:08  79  	 ADD_COLUMN('depr_calc_combined_stg',
18:20:08  80  		  'rwip_cost_of_removal',
18:20:08  81  		  'number(22,0) default 0',
18:20:08  82  		  'The amount of RWIP Allocation that is cost of removal');
18:20:08  83  	 ADD_COLUMN('depr_calc_combined_arc',
18:20:08  84  		  'rwip_cost_of_removal',
18:20:08  85  		  'number(22,0) default 0',
18:20:08  86  		  'The amount of RWIP Allocation that is cost of removal');
18:20:08  87  	 ADD_COLUMN('fcst_depr_calc_combined_arc',
18:20:08  88  		  'rwip_cost_of_removal',
18:20:08  89  		  'number(22,0) default 0',
18:20:08  90  		  'The amount of RWIP Allocation that is cost of removal');
18:20:08  91  
18:20:08  92  	 --add defaults to rounding_plug columns.
18:20:08  93  	 execute immediate 'alter table DEPR_CALC_COMBINED_STG MODIFY DEPR_ROUNDING_PLUG number(22, 2) default 0';
18:20:08  94  	 execute immediate 'alter table DEPR_CALC_COMBINED_ARC MODIFY DEPR_ROUNDING_PLUG number(22, 2) default 0';
18:20:08  95  	 execute immediate 'alter table FCST_DEPR_CALC_COMBINED_ARC MODIFY DEPR_ROUNDING_PLUG number(22, 2) default 0';
18:20:08  96  
18:20:08  97  	 execute immediate 'alter table DEPR_CALC_COMBINED_STG MODIFY COR_ROUNDING_PLUG number(22, 2) default 0';
18:20:08  98  	 execute immediate 'alter table DEPR_CALC_COMBINED_ARC MODIFY COR_ROUNDING_PLUG number(22, 2) default 0';
18:20:08  99  	 execute immediate 'alter table FCST_DEPR_CALC_COMBINED_ARC MODIFY COR_ROUNDING_PLUG number(22, 2) default 0';
18:20:08 100  end;
18:20:08 101  /
Sucessfully added column rwip_in_over_depr to table depr_calc_stg.              
Sucessfully added the comment to column rwip_in_over_depr to table              
depr_calc_stg.                                                                  
Sucessfully added column rwip_in_over_depr to table depr_calc_stg_arc.          
Sucessfully added the comment to column rwip_in_over_depr to table              
depr_calc_stg_arc.                                                              
Sucessfully added column rwip_in_over_depr to table fcst_depr_calc_stg_arc.     
Sucessfully added the comment to column rwip_in_over_depr to table              
fcst_depr_calc_stg_arc.                                                         
Sucessfully added column rwip_in_over_depr to table depr_calc_combined_stg.     
Sucessfully added the comment to column rwip_in_over_depr to table              
depr_calc_combined_stg.                                                         
Sucessfully added column rwip_in_over_depr to table depr_calc_combined_arc.     
Sucessfully added the comment to column rwip_in_over_depr to table              
depr_calc_combined_arc.                                                         
Sucessfully added column rwip_in_over_depr to table fcst_depr_calc_combined_arc.
Sucessfully added the comment to column rwip_in_over_depr to table              
fcst_depr_calc_combined_arc.                                                    
Sucessfully added column rwip_allocation to table depr_calc_combined_stg.       
Sucessfully added the comment to column rwip_allocation to table                
depr_calc_combined_stg.                                                         
Sucessfully added column rwip_allocation to table depr_calc_combined_arc.       
Sucessfully added the comment to column rwip_allocation to table                
depr_calc_combined_arc.                                                         
Sucessfully added column rwip_allocation to table fcst_depr_calc_combined_arc.  
Sucessfully added the comment to column rwip_allocation to table                
fcst_depr_calc_combined_arc.                                                    
Sucessfully added column rwip_cost_of_removal to table depr_calc_combined_stg.  
Sucessfully added the comment to column rwip_cost_of_removal to table           
depr_calc_combined_stg.                                                         
Sucessfully added column rwip_cost_of_removal to table depr_calc_combined_arc.  
Sucessfully added the comment to column rwip_cost_of_removal to table           
depr_calc_combined_arc.                                                         
Sucessfully added column rwip_cost_of_removal to table                          
fcst_depr_calc_combined_arc.                                                    
Sucessfully added the comment to column rwip_cost_of_removal to table           
fcst_depr_calc_combined_arc.                                                    

PL/SQL procedure successfully completed.

18:20:09 hopplant> 
18:20:09 hopplant> --**************************
18:20:09 hopplant> -- Log the run of the script
18:20:09 hopplant> --**************************
18:20:09 hopplant> 
18:20:09 hopplant> insert into PP_SCHEMA_CHANGE_LOG
18:20:09   2  	 (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
18:20:09   3  	  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
18:20:09   4  values
18:20:09   5  	 (1040, 0, 10, 4, 2, 0, 37092, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037092_depr_add_rwip_over_depr_col.sql', 1,
18:20:09   6  	  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
18:20:09   7  	  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

18:20:09 hopplant> commit;

Commit complete.

18:20:09 hopplant> SPOOL OFF
