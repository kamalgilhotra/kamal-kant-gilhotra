21:52:51 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2017.1.0.0_maint_048893_lessor_01_formula_components_import_ddl.sql
21:52:51 hopplant> /*
21:52:51 hopplant> ||============================================================================
21:52:51 hopplant> || Application: PowerPlan
21:52:51 hopplant> || File Name:   maint_048893_lessor_01_formula_components_import_ddl.sql
21:52:51 hopplant> ||============================================================================
21:52:51 hopplant> || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
21:52:51 hopplant> ||============================================================================
21:52:51 hopplant> || Version	 Date	    Revised By	     Reason for Change
21:52:51 hopplant> || ---------- ---------- ---------------- ------------------------------------
21:52:51 hopplant> || 2017.1.0.0 11/2/2017 Alex Healey	    create the staging table(s) needed for the Lessor Index/Rate import
21:52:51 hopplant> ||============================================================================
21:52:51 hopplant> */
21:52:51 hopplant> --create the staging table to hold the imported values
21:52:51 hopplant> CREATE TABLE LSR_IMPORT_VP_INDEX_COMP_AMTS
21:52:51   2  	(
21:52:51   3  	   import_run_id	   NUMBER(22, 0) NOT NULL,
21:52:51   4  	   line_id		   NUMBER(22, 0) NOT NULL,
21:52:51   5  	   error_message	   VARCHAR2(4000),
21:52:51   6  	   time_stamp		   DATE,
21:52:51   7  	   user_id		   VARCHAR2(18),
21:52:51   8  	   formula_component_id    NUMBER(22, 0),
21:52:51   9  	   formula_component_xlate VARCHAR2(254),
21:52:51  10  	   effective_date	   VARCHAR2(254),
21:52:51  11  	   amount		   VARCHAR2(254)
21:52:51  12  	);

Table created.

21:52:51 hopplant> 
21:52:51 hopplant> ALTER TABLE LSR_IMPORT_VP_INDEX_COMP_AMTS
21:52:51   2  	ADD CONSTRAINT lsr_import_vp_comp_amt_pk PRIMARY KEY (import_run_id, line_id) USING INDEX TABLESPACE pwrplant_idx;

Table altered.

21:52:51 hopplant> 
21:52:51 hopplant> ALTER TABLE LSR_IMPORT_VP_INDEX_COMP_AMTS
21:52:51   2  	ADD CONSTRAINT lsr_import_vp_comp_amt_fk1 FOREIGN KEY (import_run_id) REFERENCES PP_IMPORT_RUN (import_run_id);

Table altered.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON TABLE LSR_IMPORT_VP_INDEX_COMP_AMTS IS '(S) [06] The LSR Import VP Comp Amts table is an API table used to import Formula Component Amounts.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.line_id IS 'System-assigned line number for this import run.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.error_message IS 'Error messages resulting from data validation in the import process.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.user_id IS 'Standard system-assigned user id used for audit purposes.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.formula_component_id IS 'The internal Formula Component id within PowerPlant.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.formula_component_xlate IS 'Translation field for determining the Formula Component.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.effective_date IS 'The date upon which the amount for the Index/Rate becomes effective.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPORT_VP_INDEX_COMP_AMTS.amount IS 'The amount we are loading for the specific Formula Component.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> --create the archive staging table
21:52:51 hopplant> CREATE TABLE LSR_IMPRT_VP_INDX_CMP_AMTS_ARC
21:52:51   2  	(
21:52:51   3  	   import_run_id	   NUMBER(22, 0) NOT NULL,
21:52:51   4  	   line_id		   NUMBER(22, 0) NOT NULL,
21:52:51   5  	   time_stamp		   DATE,
21:52:51   6  	   user_id		   VARCHAR2(18),
21:52:51   7  	   formula_component_id    NUMBER(22, 0),
21:52:51   8  	   formula_component_xlate VARCHAR2(254),
21:52:51   9  	   effective_date	   VARCHAR2(254),
21:52:51  10  	   amount		   VARCHAR2(254)
21:52:51  11  	);

Table created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON TABLE LSR_IMPRT_VP_INDX_CMP_AMTS_ARC IS '(S) [06] The LSR Import VP Comp Amts Archive table holds records of previous Formula Component Amount imports.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.line_id IS 'System-assigned line number for this import run.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.user_id IS 'Standard system-assigned user id used for audit purposes.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.formula_component_id IS 'The internal Formula Component id within PowerPlant.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.formula_component_xlate IS 'Translation field for determining the Formula Component.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.effective_date IS 'The date upon which the amount for the Index/Rate becomes effective.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> COMMENT ON COLUMN LSR_IMPRT_VP_INDX_CMP_AMTS_ARC.amount IS 'The amount we are loading for the specific Formula Component.';

Comment created.

21:52:51 hopplant> 
21:52:51 hopplant> --****************************************************
21:52:51 hopplant> --Log the run of the script to local PP_SCHEMA_LOG
21:52:51 hopplant> --****************************************************
21:52:51 hopplant> INSERT INTO PP_SCHEMA_CHANGE_LOG
21:52:51   2  	  (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
21:52:51   3  	  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
21:52:51   4  VALUES
21:52:51   5  	  (3964, 0, 2017, 1, 0, 0, 48893, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048893_lessor_01_formula_components_import_ddl.sql', 1,
21:52:51   6  	  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
21:52:51   7  	  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

21:52:51 hopplant> COMMIT;

Commit complete.

21:52:51 hopplant> SPOOL OFF
