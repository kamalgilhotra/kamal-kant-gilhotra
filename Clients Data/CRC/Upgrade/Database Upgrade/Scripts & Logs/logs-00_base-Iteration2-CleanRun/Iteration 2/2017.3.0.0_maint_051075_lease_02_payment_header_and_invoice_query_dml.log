22:26:55 hopplant> @&&PP_SCRIPT_PATH.maint_scripts\2017.3.0.0_maint_051075_lease_02_payment_header_and_invoice_query_dml.sql
22:26:55 hopplant> /*
22:26:55 hopplant> ||============================================================================
22:26:55 hopplant> || Application: PowerPlan
22:26:55 hopplant> || Object Name: maint_051075_lease_02_payment_header_and_invoice_query_dml.sql
22:26:55 hopplant> || Description:
22:26:55 hopplant> ||============================================================================
22:26:55 hopplant> || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
22:26:55 hopplant> ||============================================================================
22:26:55 hopplant> || Version	 Date	     Revised By     Reason for Change
22:26:55 hopplant> || ---------- ----------  -------------- ----------------------------------------
22:26:55 hopplant> || 2017.3.0.0  05/07/2018 Anand R	    PP-51075 bug fix
22:26:55 hopplant> ||============================================================================
22:26:55 hopplant> */
22:26:55 hopplant> 
22:26:55 hopplant> DECLARE
22:26:55   2  	l_query_description VARCHAR2(254);
22:26:55   3  	l_query_sql CLOB;
22:26:55   4  	l_table_name VARCHAR2(35);
22:26:55   5  	l_subsystem VARCHAR2(60);
22:26:55   6  	l_query_type VARCHAR2(35);
22:26:55   7  	l_is_multicurrency number(1,0);
22:26:55   8  	l_id NUMBER;
22:26:55   9  	l_sql1 VARCHAR2(4000);
22:26:55  10  	l_sql2 VARCHAR2(4000);
22:26:55  11  	l_sql3 VARCHAR2(4000);
22:26:55  12  	l_sql4 VARCHAR2(4000);
22:26:55  13  	l_sql5 VARCHAR(4000);
22:26:55  14  	l_sql6 VARCHAR2(4000);
22:26:55  15  	l_offset NUMBER;
22:26:55  16  	l_cnt number;
22:26:55  17  BEGIN
22:26:55  18  
22:26:55  19  	l_query_description := 'Lease Payment Header and Invoice Detail';
22:26:55  20  	l_table_name := 'Dynamic View';
22:26:55  21  	l_subsystem := 'lessee';
22:26:55  22  	l_query_type := 'user';
22:26:55  23  	l_is_multicurrency := 1;
22:26:55  24  
22:26:55  25  	l_query_sql := 'WITH currency_selection AS (
22:26:55  26  SELECT lct.ls_currency_type_id, lct.description
22:26:55  27  FROM ls_lease_currency_type lct, pp_any_required_filter parf
22:26:55  28  where upper(trim(parf.column_name)) = ''CURRENCY TYPE''
22:26:55  29  AND  upper(trim(parf.filter_value)) =  upper(trim(lct.description))
22:26:55  30  )
22:26:55  31  select to_char(lph.gl_posting_mo_yr, ''yyyymm'') as monthnum,
22:26:55  32  	     lph.company_id,
22:26:55  33  		 (select s.description from set_of_books s where s.set_of_books_id = lpl.set_of_books_id)  as set_of_books,
22:26:55  34  	     lv.description as vendor,
22:26:55  35  	     aps.description as payment_status,
22:26:55  36  	     lpl.amount as calculated_amount,
22:26:55  37  	     lpl.adjustment_amount as adjustments,
22:26:55  38  	     lph.amount as total_payment,
22:26:55  39  	     inv.invoice_number,
22:26:55  40  	     inv.invoice_amount,
22:26:55  41  	     decode(map.in_tolerance, 1, ''Yes'', null, null, ''No'') as in_tolerance,
22:26:55  42  	     ll.lease_id,
22:26:55  43  	     ll.lease_number,
22:26:55  44  	     lct.description as lease_cap_type,
22:26:55  45  	     nvl(ilr.ilr_id, -1) as ilr_id,
22:26:55  46  	     nvl(ilr.ilr_number, ''N/A'') as ilr_number,
22:26:55  47  	     nvl(lfs.description, ''N/A'') as funding_status,
22:26:55  48  	     nvl(la.ls_asset_id, -1) as ls_asset_id,
22:26:55  49  	     nvl(la.leased_asset_number, ''N/A'') as leased_asset_number,
22:26:55  50  	     co.description as company_description,
22:26:55  51  	     InitCap(currency_selection.description) currency_type,
22:26:55  52  	     lph.iso_code currency,
22:26:55  53  	     lph.currency_display_symbol currency_symbol
22:26:55  54  	from v_ls_payment_hdr_fx lph
22:26:55  55  	     left OUTER JOIN ls_asset la ON (lph.ls_asset_id = la.ls_asset_id)
22:26:55  56  	     left JOIN ls_ilr ilr ON (lph.ilr_id = ilr.ilr_id)
22:26:55  57  	     JOIN ls_lease ll ON (lph.lease_id = ll.lease_id)
22:26:55  58  	     JOIN company co ON (co.company_id = lph.company_id)
22:26:55  59  	     JOIN ls_lease_cap_type lct ON (ll.lease_cap_type_id = lct.ls_lease_cap_type_id)
22:26:55  60  	     left OUTER JOIN ls_funding_status lfs ON (ilr.funding_status_id = lfs.funding_status_id)
22:26:55  61  	     JOIN ls_vendor lv ON (lph.vendor_id = lv.vendor_id)
22:26:55  62  	     JOIN approval_status aps ON (lph.payment_status_id = aps.approval_status_id)
22:26:55  63  	     left JOIN ls_invoice_payment_map map ON (lph.payment_id = map.payment_id)
22:26:55  64  	     left OUTER JOIN v_ls_invoice_fx_vw inv ON (map.invoice_id = inv.invoice_id)
22:26:55  65  	     JOIN (select payment_id,
22:26:55  66  		     sum(nvl(amount, 0)) as amount,
22:26:55  67  		     sum(nvl(adjustment_amount, 0)) as adjustment_amount,
22:26:55  68  		     ls_cur_type,
22:26:55  69  				 set_of_books_id
22:26:55  70  		from v_ls_payment_line_fx, currency_selection
22:26:55  71  	       where v_ls_payment_line_fx.ls_cur_type = currency_selection.ls_currency_type_id
22:26:55  72  	       AND to_char(gl_posting_mo_yr, ''yyyymm'') in
22:26:55  73  		     (select filter_value
22:26:55  74  			from pp_any_required_filter
22:26:55  75  		       where upper(trim(column_name)) = ''MONTHNUM'')	    group by payment_id, ls_cur_type, set_of_books_id) lpl ON (lpl.ls_cur_type = lph.ls_cur_type AND lpl.payment_id = lph.payment_id)
22:26:55  76  	     JOIN currency_selection ON (lpl.ls_cur_type = currency_selection.ls_currency_type_id AND inv.ls_cur_type = currency_selection.ls_currency_type_id)
22:26:55  77   where to_char(lph.gl_posting_mo_yr, ''yyyymm'') in
22:26:55  78  	     (select filter_value
22:26:55  79  		from pp_any_required_filter
22:26:55  80  	       where upper(trim(column_name)) = ''MONTHNUM'')
22:26:55  81  	 and to_char(lph.company_id) in
22:26:55  82  	     (select filter_value
22:26:55  83  		from pp_any_required_filter
22:26:55  84  	       where upper(trim(column_name)) = ''COMPANY ID'') 	 ';
22:26:55  85  
22:26:55  86  	/*****************************************************************************
22:26:55  87  	 BEGIN: DO NOT EDIT HERE!!
22:26:55  88  	*****************************************************************************/
22:26:55  89  
22:26:55  90  	SELECT COUNT(1) INTO l_cnt
22:26:55  91  	FROM pp_any_query_criteria
22:26:55  92  	where lower(trim(description)) = lower(trim(l_query_description));
22:26:55  93  
22:26:55  94  	IF l_cnt > 0 THEN
22:26:55  95  
22:26:55  96  	  SELECT ID INTO l_id
22:26:55  97  	  FROM pp_any_query_criteria
22:26:55  98  	  WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
22:26:55  99  
22:26:55 100  	  DELETE FROM pp_any_query_criteria_fields
22:26:55 101  	  where id = l_id;
22:26:55 102  
22:26:55 103  	  DELETE FROM pp_any_query_criteria
22:26:55 104  	  WHERE ID = l_id;
22:26:55 105  	ELSE
22:26:55 106  	  SELECT nvl(MAX(ID), 0) + 1 INTO l_id
22:26:55 107  	  FROM pp_any_query_criteria;
22:26:55 108  	END IF;
22:26:55 109  
22:26:55 110  	l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
22:26:55 111  
22:26:55 112  	l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
22:26:55 113  	l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
22:26:55 114  	l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
22:26:55 115  	l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
22:26:55 116  	l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
22:26:55 117  	l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
22:26:55 118  
22:26:55 119  	insert into pp_any_query_criteria(id,
22:26:55 120  					  source_id,
22:26:55 121  					  criteria_field,
22:26:55 122  					  table_name,
22:26:55 123  					  DESCRIPTION,
22:26:55 124  					  feeder_field,
22:26:55 125  					  subsystem,
22:26:55 126  					  query_type,
22:26:55 127  					  SQL,
22:26:55 128  					  sql2,
22:26:55 129  					  sql3,
22:26:55 130  					  sql4,
22:26:55 131  					  sql5,
22:26:55 132  					  sql6,
22:26:55 133  					  is_multicurrency)
22:26:55 134  	values (l_id,
22:26:55 135  		1001,
22:26:55 136  		'none',
22:26:55 137  		l_table_name,
22:26:55 138  		l_query_description,
22:26:55 139  		'none',
22:26:55 140  		l_subsystem,
22:26:55 141  		l_query_type,
22:26:55 142  		l_sql1,
22:26:55 143  		l_sql2,
22:26:55 144  		l_sql3,
22:26:55 145  		l_sql4,
22:26:55 146  		l_sql5,
22:26:55 147  		l_sql6,
22:26:55 148  		l_is_multicurrency);
22:26:55 149  
22:26:55 150  	/*****************************************************************************
22:26:55 151  	 END: DO NOT EDIT HERE!!
22:26:55 152  	*****************************************************************************/
22:26:55 153  
22:26:55 154  INSERT INTO pp_any_query_criteria_fields (ID,
22:26:55 155  						  detail_field,
22:26:55 156  						  column_order,
22:26:55 157  						  amount_field,
22:26:55 158  						  include_in_select_criteria,
22:26:55 159  						  default_value,
22:26:55 160  						  column_header,
22:26:55 161  						  column_width,
22:26:55 162  						  display_field,
22:26:55 163  						  display_table,
22:26:55 164  						  column_type,
22:26:55 165  						  quantity_field,
22:26:55 166  						  data_field,
22:26:55 167  						  required_filter,
22:26:55 168  						  required_one_mult,
22:26:55 169  						  sort_col,
22:26:55 170  						  hide_from_results,
22:26:55 171  						  hide_from_filters)SELECT  l_id,
22:26:55 172  	      'adjustments',
22:26:55 173  	      9,
22:26:55 174  	      1,
22:26:55 175  	      1,
22:26:55 176  	      '',
22:26:55 177  	      'Adjustments',
22:26:55 178  	      300,
22:26:55 179  	      '',
22:26:55 180  	      '',
22:26:55 181  	      'NUMBER',
22:26:55 182  	      0,
22:26:55 183  	      '',
22:26:55 184  	      null,
22:26:55 185  	      null,
22:26:55 186  	      '',
22:26:55 187  	      0,
22:26:55 188  	      0
22:26:55 189  
22:26:55 190  FROM dual
22:26:55 191  
22:26:55 192  UNION ALL
22:26:55 193  
22:26:55 194  SELECT  l_id,
22:26:55 195  	      'calculated_amount',
22:26:55 196  	      8,
22:26:55 197  	      1,
22:26:55 198  	      1,
22:26:55 199  	      '',
22:26:55 200  	      'Calculated Amount',
22:26:55 201  	      300,
22:26:55 202  	      '',
22:26:55 203  	      '',
22:26:55 204  	      'NUMBER',
22:26:55 205  	      0,
22:26:55 206  	      '',
22:26:55 207  	      null,
22:26:55 208  	      null,
22:26:55 209  	      '',
22:26:55 210  	      0,
22:26:55 211  	      0
22:26:55 212  
22:26:55 213  FROM dual
22:26:55 214  
22:26:55 215  UNION ALL
22:26:55 216  
22:26:55 217  SELECT l_id,
22:26:55 218  	  'set_of_books',
22:26:55 219  	  3,
22:26:55 220  	  0,
22:26:55 221  	  1,
22:26:55 222  	  '',
22:26:55 223  	  'Set of Books',
22:26:55 224  	  300,
22:26:55 225  	  'description',
22:26:55 226  	  'set_of_books',
22:26:55 227  	  'VARCHAR2',
22:26:55 228  	  0,
22:26:55 229  	  'description',
22:26:55 230  	  null,
22:26:55 231  	  null,
22:26:55 232  	  null,
22:26:55 233  	  0,
22:26:55 234  	  0
22:26:55 235  FROM DUAL
22:26:55 236  
22:26:55 237  UNION ALL
22:26:55 238  
22:26:55 239  SELECT  l_id,
22:26:55 240  	      'company_description',
22:26:55 241  	      4,
22:26:55 242  	      0,
22:26:55 243  	      1,
22:26:55 244  	      '',
22:26:55 245  	      'Company Description',
22:26:55 246  	      300,
22:26:55 247  	      'description',
22:26:55 248  	      '(select * from company where is_lease_company = 1)',
22:26:55 249  	      'VARCHAR2',
22:26:55 250  	      0,
22:26:55 251  	      'description',
22:26:55 252  	      null,
22:26:55 253  	      null,
22:26:55 254  	      'description',
22:26:55 255  	      0,
22:26:55 256  	      0
22:26:55 257  
22:26:55 258  FROM dual
22:26:55 259  
22:26:55 260  UNION ALL
22:26:55 261  
22:26:55 262  SELECT  l_id,
22:26:55 263  	      'company_id',
22:26:55 264  	      2,
22:26:55 265  	      0,
22:26:55 266  	      1,
22:26:55 267  	      '',
22:26:55 268  	      'Company Id',
22:26:55 269  	      300,
22:26:55 270  	      'description',
22:26:55 271  	      '(select * from company where is_lease_company = 1)',
22:26:55 272  	      'NUMBER',
22:26:55 273  	      0,
22:26:55 274  	      'company_id',
22:26:55 275  	      1,
22:26:55 276  	      2,
22:26:55 277  	      'description',
22:26:55 278  	      0,
22:26:55 279  	      0
22:26:55 280  
22:26:55 281  FROM dual
22:26:55 282  
22:26:55 283  UNION ALL
22:26:55 284  
22:26:55 285  SELECT  l_id,
22:26:55 286  	      'currency',
22:26:55 287  	      6,
22:26:55 288  	      0,
22:26:55 289  	      0,
22:26:55 290  	      '',
22:26:55 291  	      'Currency',
22:26:55 292  	      300,
22:26:55 293  	      '',
22:26:55 294  	      '',
22:26:55 295  	      'VARCHAR2',
22:26:55 296  	      0,
22:26:55 297  	      '',
22:26:55 298  	      null,
22:26:55 299  	      null,
22:26:55 300  	      '',
22:26:55 301  	      0,
22:26:55 302  	      1
22:26:55 303  
22:26:55 304  FROM dual
22:26:55 305  
22:26:55 306  UNION ALL
22:26:55 307  
22:26:55 308  SELECT  l_id,
22:26:55 309  	      'currency_symbol',
22:26:55 310  	      23,
22:26:55 311  	      0,
22:26:55 312  	      0,
22:26:55 313  	      '',
22:26:55 314  	      'Currency Symbol',
22:26:55 315  	      300,
22:26:55 316  	      '',
22:26:55 317  	      '',
22:26:55 318  	      'VARCHAR2',
22:26:55 319  	      0,
22:26:55 320  	      '',
22:26:55 321  	      null,
22:26:55 322  	      null,
22:26:55 323  	      '',
22:26:55 324  	      1,
22:26:55 325  	      1
22:26:55 326  
22:26:55 327  FROM dual
22:26:55 328  
22:26:55 329  UNION ALL
22:26:55 330  
22:26:55 331  SELECT  l_id,
22:26:55 332  	      'currency_type',
22:26:55 333  	      22,
22:26:55 334  	      0,
22:26:55 335  	      1,
22:26:55 336  	      '',
22:26:55 337  	      'Currency Type',
22:26:55 338  	      300,
22:26:55 339  	      'description',
22:26:55 340  	      'ls_lease_currency_type',
22:26:55 341  	      'VARCHAR2',
22:26:55 342  	      0,
22:26:55 343  	      'description',
22:26:55 344  	      1,
22:26:55 345  	      2,
22:26:55 346  	      '',
22:26:55 347  	      0,
22:26:55 348  	      0
22:26:55 349  
22:26:55 350  FROM dual
22:26:55 351  
22:26:55 352  UNION ALL
22:26:55 353  
22:26:55 354  SELECT  l_id,
22:26:55 355  	      'funding_status',
22:26:55 356  	      19,
22:26:55 357  	      0,
22:26:55 358  	      1,
22:26:55 359  	      '',
22:26:55 360  	      'Funding Status',
22:26:55 361  	      300,
22:26:55 362  	      'description',
22:26:55 363  	      'ls_funding_status',
22:26:55 364  	      'VARCHAR2',
22:26:55 365  	      0,
22:26:55 366  	      'description',
22:26:55 367  	      null,
22:26:55 368  	      null,
22:26:55 369  	      'description',
22:26:55 370  	      0,
22:26:55 371  	      0
22:26:55 372  
22:26:55 373  FROM dual
22:26:55 374  
22:26:55 375  UNION ALL
22:26:55 376  
22:26:55 377  SELECT  l_id,
22:26:55 378  	      'ilr_id',
22:26:55 379  	      17,
22:26:55 380  	      0,
22:26:55 381  	      1,
22:26:55 382  	      '',
22:26:55 383  	      'Ilr Id',
22:26:55 384  	      300,
22:26:55 385  	      'ilr_number',
22:26:55 386  	      'ls_ilr',
22:26:55 387  	      'NUMBER',
22:26:55 388  	      0,
22:26:55 389  	      'ilr_id',
22:26:55 390  	      null,
22:26:55 391  	      null,
22:26:55 392  	      'ilr_number',
22:26:55 393  	      0,
22:26:55 394  	      0
22:26:55 395  
22:26:55 396  FROM dual
22:26:55 397  
22:26:55 398  UNION ALL
22:26:55 399  
22:26:55 400  SELECT  l_id,
22:26:55 401  	      'ilr_number',
22:26:55 402  	      18,
22:26:55 403  	      0,
22:26:55 404  	      1,
22:26:55 405  	      '',
22:26:55 406  	      'Ilr Number',
22:26:55 407  	      300,
22:26:55 408  	      'ilr_number',
22:26:55 409  	      'ls_ilr',
22:26:55 410  	      'VARCHAR2',
22:26:55 411  	      0,
22:26:55 412  	      'ilr_number',
22:26:55 413  	      null,
22:26:55 414  	      null,
22:26:55 415  	      'ilr_number',
22:26:55 416  	      0,
22:26:55 417  	      0
22:26:55 418  
22:26:55 419  FROM dual
22:26:55 420  
22:26:55 421  UNION ALL
22:26:55 422  
22:26:55 423  SELECT  l_id,
22:26:55 424  	      'in_tolerance',
22:26:55 425  	      13,
22:26:55 426  	      0,
22:26:55 427  	      1,
22:26:55 428  	      '',
22:26:55 429  	      'In Tolerance',
22:26:55 430  	      300,
22:26:55 431  	      '',
22:26:55 432  	      '',
22:26:55 433  	      'VARCHAR2',
22:26:55 434  	      0,
22:26:55 435  	      '',
22:26:55 436  	      null,
22:26:55 437  	      null,
22:26:55 438  	      '',
22:26:55 439  	      0,
22:26:55 440  	      0
22:26:55 441  
22:26:55 442  FROM dual
22:26:55 443  
22:26:55 444  UNION ALL
22:26:55 445  
22:26:55 446  SELECT  l_id,
22:26:55 447  	      'invoice_amount',
22:26:55 448  	      12,
22:26:55 449  	      1,
22:26:55 450  	      1,
22:26:55 451  	      '',
22:26:55 452  	      'Invoice Amount',
22:26:55 453  	      300,
22:26:55 454  	      '',
22:26:55 455  	      '',
22:26:55 456  	      'NUMBER',
22:26:55 457  	      0,
22:26:55 458  	      '',
22:26:55 459  	      null,
22:26:55 460  	      null,
22:26:55 461  	      '',
22:26:55 462  	      0,
22:26:55 463  	      0
22:26:55 464  
22:26:55 465  FROM dual
22:26:55 466  
22:26:55 467  UNION ALL
22:26:55 468  
22:26:55 469  SELECT  l_id,
22:26:55 470  	      'invoice_number',
22:26:55 471  	      11,
22:26:55 472  	      0,
22:26:55 473  	      1,
22:26:55 474  	      '',
22:26:55 475  	      'Invoice Number',
22:26:55 476  	      300,
22:26:55 477  	      '',
22:26:55 478  	      '',
22:26:55 479  	      'VARCHAR2',
22:26:55 480  	      0,
22:26:55 481  	      '',
22:26:55 482  	      null,
22:26:55 483  	      null,
22:26:55 484  	      '',
22:26:55 485  	      0,
22:26:55 486  	      0
22:26:55 487  
22:26:55 488  FROM dual
22:26:55 489  
22:26:55 490  UNION ALL
22:26:55 491  
22:26:55 492  SELECT  l_id,
22:26:55 493  	      'lease_cap_type',
22:26:55 494  	      16,
22:26:55 495  	      0,
22:26:55 496  	      1,
22:26:55 497  	      '',
22:26:55 498  	      'Lease Cap Type',
22:26:55 499  	      300,
22:26:55 500  	      'description',
22:26:55 501  	      'ls_lease_cap_type',
22:26:55 502  	      'VARCHAR2',
22:26:55 503  	      0,
22:26:55 504  	      'description',
22:26:55 505  	      null,
22:26:55 506  	      null,
22:26:55 507  	      'description',
22:26:55 508  	      0,
22:26:55 509  	      0
22:26:55 510  
22:26:55 511  FROM dual
22:26:55 512  
22:26:55 513  UNION ALL
22:26:55 514  
22:26:55 515  SELECT  l_id,
22:26:55 516  	      'lease_id',
22:26:55 517  	      14,
22:26:55 518  	      0,
22:26:55 519  	      1,
22:26:55 520  	      '',
22:26:55 521  	      'Lease Id',
22:26:55 522  	      300,
22:26:55 523  	      'lease_number',
22:26:55 524  	      'ls_lease',
22:26:55 525  	      'NUMBER',
22:26:55 526  	      0,
22:26:55 527  	      'lease_id',
22:26:55 528  	      null,
22:26:55 529  	      null,
22:26:55 530  	      'lease_number',
22:26:55 531  	      0,
22:26:55 532  	      0
22:26:55 533  
22:26:55 534  FROM dual
22:26:55 535  
22:26:55 536  UNION ALL
22:26:55 537  
22:26:55 538  SELECT  l_id,
22:26:55 539  	      'lease_number',
22:26:55 540  	      15,
22:26:55 541  	      0,
22:26:55 542  	      1,
22:26:55 543  	      '',
22:26:55 544  	      'Lease Number',
22:26:55 545  	      300,
22:26:55 546  	      'lease_number',
22:26:55 547  	      'ls_lease',
22:26:55 548  	      'VARCHAR2',
22:26:55 549  	      0,
22:26:55 550  	      'lease_number',
22:26:55 551  	      null,
22:26:55 552  	      null,
22:26:55 553  	      'lease_number',
22:26:55 554  	      0,
22:26:55 555  	      0
22:26:55 556  
22:26:55 557  FROM dual
22:26:55 558  
22:26:55 559  UNION ALL
22:26:55 560  
22:26:55 561  SELECT  l_id,
22:26:55 562  	      'leased_asset_number',
22:26:55 563  	      21,
22:26:55 564  	      0,
22:26:55 565  	      1,
22:26:55 566  	      '',
22:26:55 567  	      'Leased Asset Number',
22:26:55 568  	      300,
22:26:55 569  	      'leased_asset_number',
22:26:55 570  	      'ls_asset',
22:26:55 571  	      'VARCHAR2',
22:26:55 572  	      0,
22:26:55 573  	      'leased_asset_number',
22:26:55 574  	      null,
22:26:55 575  	      null,
22:26:55 576  	      'leased_asset_number',
22:26:55 577  	      0,
22:26:55 578  	      0
22:26:55 579  
22:26:55 580  FROM dual
22:26:55 581  
22:26:55 582  UNION ALL
22:26:55 583  
22:26:55 584  SELECT  l_id,
22:26:55 585  	      'ls_asset_id',
22:26:55 586  	      20,
22:26:55 587  	      0,
22:26:55 588  	      1,
22:26:55 589  	      '',
22:26:55 590  	      'Ls Asset Id',
22:26:55 591  	      300,
22:26:55 592  	      'leased_asset_number',
22:26:55 593  	      'ls_asset',
22:26:55 594  	      'NUMBER',
22:26:55 595  	      0,
22:26:55 596  	      'ls_asset_id',
22:26:55 597  	      null,
22:26:55 598  	      null,
22:26:55 599  	      'leased_asset_number',
22:26:55 600  	      0,
22:26:55 601  	      0
22:26:55 602  
22:26:55 603  FROM dual
22:26:55 604  
22:26:55 605  UNION ALL
22:26:55 606  
22:26:55 607  SELECT  l_id,
22:26:55 608  	      'monthnum',
22:26:55 609  	      1,
22:26:55 610  	      0,
22:26:55 611  	      1,
22:26:55 612  	      '',
22:26:55 613  	      'Monthnum',
22:26:55 614  	      300,
22:26:55 615  	      'month_number',
22:26:55 616  	      '(select month_number from pp_calendar)',
22:26:55 617  	      'VARCHAR2',
22:26:55 618  	      0,
22:26:55 619  	      'month_number',
22:26:55 620  	      1,
22:26:55 621  	      2,
22:26:55 622  	      'the_sort',
22:26:55 623  	      0,
22:26:55 624  	      0
22:26:55 625  
22:26:55 626  FROM dual
22:26:55 627  
22:26:55 628  UNION ALL
22:26:55 629  
22:26:55 630  SELECT  l_id,
22:26:55 631  	      'payment_status',
22:26:55 632  	      7,
22:26:55 633  	      0,
22:26:55 634  	      1,
22:26:55 635  	      '',
22:26:55 636  	      'Payment Status',
22:26:55 637  	      300,
22:26:55 638  	      '',
22:26:55 639  	      '',
22:26:55 640  	      'VARCHAR2',
22:26:55 641  	      0,
22:26:55 642  	      '',
22:26:55 643  	      null,
22:26:55 644  	      null,
22:26:55 645  	      '',
22:26:55 646  	      0,
22:26:55 647  	      0
22:26:55 648  
22:26:55 649  FROM dual
22:26:55 650  
22:26:55 651  UNION ALL
22:26:55 652  
22:26:55 653  SELECT  l_id,
22:26:55 654  	      'total_payment',
22:26:55 655  	      10,
22:26:55 656  	      1,
22:26:55 657  	      1,
22:26:55 658  	      '',
22:26:55 659  	      'Total Payment',
22:26:55 660  	      300,
22:26:55 661  	      '',
22:26:55 662  	      '',
22:26:55 663  	      'NUMBER',
22:26:55 664  	      0,
22:26:55 665  	      '',
22:26:55 666  	      null,
22:26:55 667  	      null,
22:26:55 668  	      '',
22:26:55 669  	      0,
22:26:55 670  	      0
22:26:55 671  
22:26:55 672  FROM dual
22:26:55 673  
22:26:55 674  UNION ALL
22:26:55 675  
22:26:55 676  SELECT  l_id,
22:26:55 677  	      'vendor',
22:26:55 678  	      5,
22:26:55 679  	      0,
22:26:55 680  	      1,
22:26:55 681  	      '',
22:26:55 682  	      'Vendor',
22:26:55 683  	      300,
22:26:55 684  	      '',
22:26:55 685  	      '',
22:26:55 686  	      'VARCHAR2',
22:26:55 687  	      0,
22:26:55 688  	      '',
22:26:55 689  	      null,
22:26:55 690  	      null,
22:26:55 691  	      '',
22:26:55 692  	      0,
22:26:55 693  	      0
22:26:55 694  
22:26:55 695  FROM dual
22:26:55 696  ;
22:26:55 697  
22:26:55 698  END;
22:26:55 699  
22:26:55 700  /

PL/SQL procedure successfully completed.

22:26:55 hopplant> 
22:26:55 hopplant> 
22:26:55 hopplant> --****************************************************
22:26:55 hopplant> --Log the run of the script to local PP_SCHEMA_LOG
22:26:55 hopplant> --****************************************************
22:26:55 hopplant> INSERT INTO PP_SCHEMA_CHANGE_LOG
22:26:55   2  	  (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
22:26:55   3  	  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
22:26:55   4  VALUES
22:26:55   5  	  (5142, 0, 2017, 3, 0, 0, 51075, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051075_lease_02_payment_header_and_invoice_query_dml.sql', 1,
22:26:55   6  	  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
22:26:55   7  	  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

1 row created.

22:26:55 hopplant> COMMIT;

Commit complete.

22:26:55 hopplant> SPOOL OFF
