/*
||====================================================================================
|| Application: PowerPlant
|| File Name:   10.4.1.2_additional_10005_remove_escalate_reserve_yn.sql
||====================================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||====================================================================================
|| Version     Date             Revised By           Reason for Change
|| ----------  ---------------  -------------------  ---------------------------------
|| 10.4.1.2    07/10/2019       Kamal Kant Gilhotra  Add escalate_reserve_yn column to
||                                                   the PROPERTY_TAX_TYPE_DATA table
||====================================================================================
*/

alter table PROPERTY_TAX_TYPE_DATA drop column ESCALATE_RESERVE_YN;


--**************************
-- Log the run of the script
--**************************

insert into PP_UPDATE_FLEX
(COL_ID,
TYPE_NAME,
FLEXVCHAR1)
select NVL(MAX(COL_ID), 0) + 1,
'UPDATE_SCRIPTS',
'10.4.1.2_additional_10005_remove_escalate_reserve_yn.sql'
from PP_UPDATE_FLEX;

commit;