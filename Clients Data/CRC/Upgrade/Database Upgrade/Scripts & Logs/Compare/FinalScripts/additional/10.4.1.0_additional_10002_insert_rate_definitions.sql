/*
||====================================================================================
|| Application: PowerPlant
|| File Name:   10.4.1.2_additional_20001_add_escalate_reserve_yn.sql
||====================================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||====================================================================================
|| Version     Date             Revised By           Reason for Change
|| ----------  ---------------  -------------------  ---------------------------------
|| 10.4.1.2    07/10/2019       Kamal Kant Gilhotra  Add escalate_reserve_yn column to
||                                                   the PROPERTY_TAX_TYPE_DATA table
||====================================================================================
*/

-- pt_rate_definition
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 100, sysdate, user, '01%', '1% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 300, sysdate, user, '03%', '3% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 600, sysdate, user, '06%', '6% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 625, sysdate, user, '06.25%', '6.25% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 950, sysdate, user, '09.5%', '9.5% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 1000, sysdate, user, '10%', '10% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 1050, sysdate, user, '10.5%', '10.5% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 1350, sysdate, user, '13.5%', '13.5% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 1500, sysdate, user, '15%', '15% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 2000, sysdate, user, '20%', '20% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 2300, sysdate, user, '23%', '23% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 2500, sysdate, user, '25%', '25% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 2900, sysdate, user, '29%', '29% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 3000, sysdate, user, '30%', '30% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 3333, sysdate, user, '33.333333%', '33.333333% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 3500, sysdate, user, '35%', '10% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 4000, sysdate, user, '40%', '40% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 5000, sysdate, user, '50%', '50% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 6000, sysdate, user, '60%', '60% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 6666, sysdate, user, '66.666666%', '66.666666% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 7000, sysdate, user, '70%', '70% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 7500, sysdate, user, '75%', '75% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 8000, sysdate, user, '80%', '80% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );
insert into pwrplant.pt_rate_definition ( pt_rate_id, time_stamp, user_id, description, long_description, rate_type_id, limit_for_rate ) values ( 9000, sysdate, user, '90%', '90% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.', 1, null );

--**************************
-- Log the run of the script
--**************************

insert into PP_UPDATE_FLEX
(COL_ID,
TYPE_NAME,
FLEXVCHAR1)
select NVL(MAX(COL_ID), 0) + 1,
'UPDATE_SCRIPTS',
'10.4.1.0_additional_20002_insert_rate_definitions.sql'
from PP_UPDATE_FLEX;
commit;
