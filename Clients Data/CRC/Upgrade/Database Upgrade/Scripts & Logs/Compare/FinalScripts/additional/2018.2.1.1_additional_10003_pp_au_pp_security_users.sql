CREATE OR REPLACE TRIGGER 
PP_AU_PP_SECURITY_USERS AFTER 
 INSERT  OR  UPDATE  OR  DELETE  OF ADSI_PATH, APPWORKSPACE, CHANGE_DATE, COMPANY_ID, CONTRACT_ADM, DEFAULT_SOB, DEL_COUNT, DEPARTMENT_ID, DESCRIPTION, DIGIT_LEN, DIRECTORY, ENABLE_TOOLTIP, ENGINEER, EXT_USER_CODE, EXTERNAL_USER_FLAG, FIRST_NAME, GROUP_CONTROL, LANGUAGE_ID, LAST_NAME, LOCALE_ID, LOGON_COUNT, MAIL_ID, MY_POWERPLANT_BACK_COLOR, MY_POWERPLANT_CLOSE_ALL, MY_POWERPLANT_FONT, MY_POWERPLANT_FONT_SIZE, MY_POWERPLANT_OPEN, MY_POWERPLANT_TEXT_COLOR, MY_PP_POWERTAX_USER, OBJECTCLASS, ORG_USERS, OTHER, PHONE_NUMBER, PLANT_ACCOUNTANT, PROJ_MGR, STATUS, SUPERVISOR_USERS, TEST_WINDOW, USERS, WINDOW_RESIZE
 ON PP_SECURITY_USERS FOR EACH ROW 
DECLARE  
  prog varchar2(60);  
  osuser varchar2(60);  
  machine varchar2(60);  
  terminal varchar2(60);  
  old_lookup varchar2(250);  
  new_lookup varchar2(250);  
  pk_lookup varchar2(1500);  
  pk_temp varchar2(250);  
  window varchar2(60);  
  windowtitle varchar2(250);  
  comments varchar2(250);  
  trans varchar2(35);  
  ret number(22,0);  
BEGIN  
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then  
   return;  
end if; 
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window'); 
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),''); 
  trans        := nvl(sys_context('powerplant_ctx','process'     ),''); 
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),''); 
  prog         := nvl(sys_context('powerplant_ctx','program'     ),''); 
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),''); 
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),''); 
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),''); 
  comments     := substr( 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal), 1 , 250 );
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF; 
IF UPDATING THEN 
   IF :old.ADSI_PATH <> :new.ADSI_PATH OR 
     (:old.ADSI_PATH is null AND :new.ADSI_PATH is not null) OR 
     (:new.ADSI_PATH is null AND :old.ADSI_PATH is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'ADSI_PATH', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.ADSI_PATH, :new.ADSI_PATH, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.APPWORKSPACE <> :new.APPWORKSPACE OR 
     (:old.APPWORKSPACE is null AND :new.APPWORKSPACE is not null) OR 
     (:new.APPWORKSPACE is null AND :old.APPWORKSPACE is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'APPWORKSPACE', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.APPWORKSPACE, :new.APPWORKSPACE, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.CHANGE_DATE <> :new.CHANGE_DATE OR 
     (:old.CHANGE_DATE is null AND :new.CHANGE_DATE is not null) OR 
     (:new.CHANGE_DATE is null AND :old.CHANGE_DATE is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'CHANGE_DATE', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.CHANGE_DATE, :new.CHANGE_DATE, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.COMPANY_ID <> :new.COMPANY_ID OR 
     (:old.COMPANY_ID is null AND :new.COMPANY_ID is not null) OR 
     (:new.COMPANY_ID is null AND :old.COMPANY_ID is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'COMPANY_ID', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.COMPANY_ID, :new.COMPANY_ID, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.CONTRACT_ADM <> :new.CONTRACT_ADM OR 
     (:old.CONTRACT_ADM is null AND :new.CONTRACT_ADM is not null) OR 
     (:new.CONTRACT_ADM is null AND :old.CONTRACT_ADM is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'CONTRACT_ADM', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.CONTRACT_ADM, :new.CONTRACT_ADM, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.DEFAULT_SOB <> :new.DEFAULT_SOB OR 
     (:old.DEFAULT_SOB is null AND :new.DEFAULT_SOB is not null) OR 
     (:new.DEFAULT_SOB is null AND :old.DEFAULT_SOB is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DEFAULT_SOB', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.DEFAULT_SOB, :new.DEFAULT_SOB, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.DEL_COUNT <> :new.DEL_COUNT OR 
     (:old.DEL_COUNT is null AND :new.DEL_COUNT is not null) OR 
     (:new.DEL_COUNT is null AND :old.DEL_COUNT is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DEL_COUNT', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.DEL_COUNT, :new.DEL_COUNT, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.DEPARTMENT_ID <> :new.DEPARTMENT_ID OR 
     (:old.DEPARTMENT_ID is null AND :new.DEPARTMENT_ID is not null) OR 
     (:new.DEPARTMENT_ID is null AND :old.DEPARTMENT_ID is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DEPARTMENT_ID', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.DEPARTMENT_ID, :new.DEPARTMENT_ID, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.DESCRIPTION <> :new.DESCRIPTION OR 
     (:old.DESCRIPTION is null AND :new.DESCRIPTION is not null) OR 
     (:new.DESCRIPTION is null AND :old.DESCRIPTION is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DESCRIPTION', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.DESCRIPTION, :new.DESCRIPTION, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.DIGIT_LEN <> :new.DIGIT_LEN OR 
     (:old.DIGIT_LEN is null AND :new.DIGIT_LEN is not null) OR 
     (:new.DIGIT_LEN is null AND :old.DIGIT_LEN is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DIGIT_LEN', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.DIGIT_LEN, :new.DIGIT_LEN, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.DIRECTORY <> :new.DIRECTORY OR 
     (:old.DIRECTORY is null AND :new.DIRECTORY is not null) OR 
     (:new.DIRECTORY is null AND :old.DIRECTORY is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DIRECTORY', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.DIRECTORY, :new.DIRECTORY, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.ENABLE_TOOLTIP <> :new.ENABLE_TOOLTIP OR 
     (:old.ENABLE_TOOLTIP is null AND :new.ENABLE_TOOLTIP is not null) OR 
     (:new.ENABLE_TOOLTIP is null AND :old.ENABLE_TOOLTIP is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'ENABLE_TOOLTIP', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.ENABLE_TOOLTIP, :new.ENABLE_TOOLTIP, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.ENGINEER <> :new.ENGINEER OR 
     (:old.ENGINEER is null AND :new.ENGINEER is not null) OR 
     (:new.ENGINEER is null AND :old.ENGINEER is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'ENGINEER', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.ENGINEER, :new.ENGINEER, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.EXT_USER_CODE <> :new.EXT_USER_CODE OR 
     (:old.EXT_USER_CODE is null AND :new.EXT_USER_CODE is not null) OR 
     (:new.EXT_USER_CODE is null AND :old.EXT_USER_CODE is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'EXT_USER_CODE', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.EXT_USER_CODE, :new.EXT_USER_CODE, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.EXTERNAL_USER_FLAG <> :new.EXTERNAL_USER_FLAG OR 
     (:old.EXTERNAL_USER_FLAG is null AND :new.EXTERNAL_USER_FLAG is not null) OR 
     (:new.EXTERNAL_USER_FLAG is null AND :old.EXTERNAL_USER_FLAG is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'EXTERNAL_USER_FLAG', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.EXTERNAL_USER_FLAG, :new.EXTERNAL_USER_FLAG, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.FIRST_NAME <> :new.FIRST_NAME OR 
     (:old.FIRST_NAME is null AND :new.FIRST_NAME is not null) OR 
     (:new.FIRST_NAME is null AND :old.FIRST_NAME is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'FIRST_NAME', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.FIRST_NAME, :new.FIRST_NAME, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.GROUP_CONTROL <> :new.GROUP_CONTROL OR 
     (:old.GROUP_CONTROL is null AND :new.GROUP_CONTROL is not null) OR 
     (:new.GROUP_CONTROL is null AND :old.GROUP_CONTROL is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'GROUP_CONTROL', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.GROUP_CONTROL, :new.GROUP_CONTROL, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.LANGUAGE_ID <> :new.LANGUAGE_ID OR 
     (:old.LANGUAGE_ID is null AND :new.LANGUAGE_ID is not null) OR 
     (:new.LANGUAGE_ID is null AND :old.LANGUAGE_ID is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'LANGUAGE_ID', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.LANGUAGE_ID, :new.LANGUAGE_ID, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.LAST_NAME <> :new.LAST_NAME OR 
     (:old.LAST_NAME is null AND :new.LAST_NAME is not null) OR 
     (:new.LAST_NAME is null AND :old.LAST_NAME is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'LAST_NAME', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.LAST_NAME, :new.LAST_NAME, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.LOCALE_ID <> :new.LOCALE_ID OR 
     (:old.LOCALE_ID is null AND :new.LOCALE_ID is not null) OR 
     (:new.LOCALE_ID is null AND :old.LOCALE_ID is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'LOCALE_ID', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.LOCALE_ID, :new.LOCALE_ID, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.LOGON_COUNT <> :new.LOGON_COUNT OR 
     (:old.LOGON_COUNT is null AND :new.LOGON_COUNT is not null) OR 
     (:new.LOGON_COUNT is null AND :old.LOGON_COUNT is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'LOGON_COUNT', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.LOGON_COUNT, :new.LOGON_COUNT, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.MAIL_ID <> :new.MAIL_ID OR 
     (:old.MAIL_ID is null AND :new.MAIL_ID is not null) OR 
     (:new.MAIL_ID is null AND :old.MAIL_ID is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MAIL_ID', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.MAIL_ID, :new.MAIL_ID, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.MY_POWERPLANT_BACK_COLOR <> :new.MY_POWERPLANT_BACK_COLOR OR 
     (:old.MY_POWERPLANT_BACK_COLOR is null AND :new.MY_POWERPLANT_BACK_COLOR is not null) OR 
     (:new.MY_POWERPLANT_BACK_COLOR is null AND :old.MY_POWERPLANT_BACK_COLOR is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_BACK_COLOR', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.MY_POWERPLANT_BACK_COLOR, :new.MY_POWERPLANT_BACK_COLOR, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.MY_POWERPLANT_CLOSE_ALL <> :new.MY_POWERPLANT_CLOSE_ALL OR 
     (:old.MY_POWERPLANT_CLOSE_ALL is null AND :new.MY_POWERPLANT_CLOSE_ALL is not null) OR 
     (:new.MY_POWERPLANT_CLOSE_ALL is null AND :old.MY_POWERPLANT_CLOSE_ALL is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_CLOSE_ALL', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.MY_POWERPLANT_CLOSE_ALL, :new.MY_POWERPLANT_CLOSE_ALL, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.MY_POWERPLANT_FONT <> :new.MY_POWERPLANT_FONT OR 
     (:old.MY_POWERPLANT_FONT is null AND :new.MY_POWERPLANT_FONT is not null) OR 
     (:new.MY_POWERPLANT_FONT is null AND :old.MY_POWERPLANT_FONT is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_FONT', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.MY_POWERPLANT_FONT, :new.MY_POWERPLANT_FONT, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.MY_POWERPLANT_FONT_SIZE <> :new.MY_POWERPLANT_FONT_SIZE OR 
     (:old.MY_POWERPLANT_FONT_SIZE is null AND :new.MY_POWERPLANT_FONT_SIZE is not null) OR 
     (:new.MY_POWERPLANT_FONT_SIZE is null AND :old.MY_POWERPLANT_FONT_SIZE is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_FONT_SIZE', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.MY_POWERPLANT_FONT_SIZE, :new.MY_POWERPLANT_FONT_SIZE, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.MY_POWERPLANT_OPEN <> :new.MY_POWERPLANT_OPEN OR 
     (:old.MY_POWERPLANT_OPEN is null AND :new.MY_POWERPLANT_OPEN is not null) OR 
     (:new.MY_POWERPLANT_OPEN is null AND :old.MY_POWERPLANT_OPEN is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_OPEN', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.MY_POWERPLANT_OPEN, :new.MY_POWERPLANT_OPEN, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.MY_POWERPLANT_TEXT_COLOR <> :new.MY_POWERPLANT_TEXT_COLOR OR 
     (:old.MY_POWERPLANT_TEXT_COLOR is null AND :new.MY_POWERPLANT_TEXT_COLOR is not null) OR 
     (:new.MY_POWERPLANT_TEXT_COLOR is null AND :old.MY_POWERPLANT_TEXT_COLOR is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_TEXT_COLOR', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.MY_POWERPLANT_TEXT_COLOR, :new.MY_POWERPLANT_TEXT_COLOR, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.MY_PP_POWERTAX_USER <> :new.MY_PP_POWERTAX_USER OR 
     (:old.MY_PP_POWERTAX_USER is null AND :new.MY_PP_POWERTAX_USER is not null) OR 
     (:new.MY_PP_POWERTAX_USER is null AND :old.MY_PP_POWERTAX_USER is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_PP_POWERTAX_USER', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.MY_PP_POWERTAX_USER, :new.MY_PP_POWERTAX_USER, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.OBJECTCLASS <> :new.OBJECTCLASS OR 
     (:old.OBJECTCLASS is null AND :new.OBJECTCLASS is not null) OR 
     (:new.OBJECTCLASS is null AND :old.OBJECTCLASS is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'OBJECTCLASS', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.OBJECTCLASS, :new.OBJECTCLASS, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.ORG_USERS <> :new.ORG_USERS OR 
     (:old.ORG_USERS is null AND :new.ORG_USERS is not null) OR 
     (:new.ORG_USERS is null AND :old.ORG_USERS is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'ORG_USERS', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.ORG_USERS, :new.ORG_USERS, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.OTHER <> :new.OTHER OR 
     (:old.OTHER is null AND :new.OTHER is not null) OR 
     (:new.OTHER is null AND :old.OTHER is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'OTHER', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.OTHER, :new.OTHER, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.PHONE_NUMBER <> :new.PHONE_NUMBER OR 
     (:old.PHONE_NUMBER is null AND :new.PHONE_NUMBER is not null) OR 
     (:new.PHONE_NUMBER is null AND :old.PHONE_NUMBER is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'PHONE_NUMBER', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.PHONE_NUMBER, :new.PHONE_NUMBER, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.PLANT_ACCOUNTANT <> :new.PLANT_ACCOUNTANT OR 
     (:old.PLANT_ACCOUNTANT is null AND :new.PLANT_ACCOUNTANT is not null) OR 
     (:new.PLANT_ACCOUNTANT is null AND :old.PLANT_ACCOUNTANT is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'PLANT_ACCOUNTANT', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.PLANT_ACCOUNTANT, :new.PLANT_ACCOUNTANT, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.PROJ_MGR <> :new.PROJ_MGR OR 
     (:old.PROJ_MGR is null AND :new.PROJ_MGR is not null) OR 
     (:new.PROJ_MGR is null AND :old.PROJ_MGR is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'PROJ_MGR', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.PROJ_MGR, :new.PROJ_MGR, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.STATUS <> :new.STATUS OR 
     (:old.STATUS is null AND :new.STATUS is not null) OR 
     (:new.STATUS is null AND :old.STATUS is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'STATUS', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.STATUS, :new.STATUS, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.SUPERVISOR_USERS <> :new.SUPERVISOR_USERS OR 
     (:old.SUPERVISOR_USERS is null AND :new.SUPERVISOR_USERS is not null) OR 
     (:new.SUPERVISOR_USERS is null AND :old.SUPERVISOR_USERS is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'SUPERVISOR_USERS', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.SUPERVISOR_USERS, :new.SUPERVISOR_USERS, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.TEST_WINDOW <> :new.TEST_WINDOW OR 
     (:old.TEST_WINDOW is null AND :new.TEST_WINDOW is not null) OR 
     (:new.TEST_WINDOW is null AND :old.TEST_WINDOW is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'TEST_WINDOW', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.TEST_WINDOW, :new.TEST_WINDOW, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.USERS <> :new.USERS OR 
     (:old.USERS is null AND :new.USERS is not null) OR 
     (:new.USERS is null AND :old.USERS is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'USERS', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.USERS, :new.USERS, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
   IF :old.WINDOW_RESIZE <> :new.WINDOW_RESIZE OR 
     (:old.WINDOW_RESIZE is null AND :new.WINDOW_RESIZE is not null) OR 
     (:new.WINDOW_RESIZE is null AND :old.WINDOW_RESIZE is not null) THEN 
      
      
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, NEW_VALUE, OLD_DISPLAY, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'WINDOW_RESIZE', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :old.WINDOW_RESIZE, :new.WINDOW_RESIZE, 
         old_lookup, new_lookup, USER, SYSDATE, prog, 'U', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
   END IF; 
END IF; 
IF INSERTING THEN 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'ADSI_PATH', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.ADSI_PATH, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'APPWORKSPACE', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.APPWORKSPACE, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'CHANGE_DATE', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.CHANGE_DATE, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'COMPANY_ID', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.COMPANY_ID, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'CONTRACT_ADM', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.CONTRACT_ADM, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DEFAULT_SOB', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.DEFAULT_SOB, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DEL_COUNT', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.DEL_COUNT, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DEPARTMENT_ID', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.DEPARTMENT_ID, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DESCRIPTION', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.DESCRIPTION, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DIGIT_LEN', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.DIGIT_LEN, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DIRECTORY', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.DIRECTORY, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'ENABLE_TOOLTIP', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.ENABLE_TOOLTIP, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'ENGINEER', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.ENGINEER, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'EXT_USER_CODE', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.EXT_USER_CODE, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'EXTERNAL_USER_FLAG', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.EXTERNAL_USER_FLAG, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'FIRST_NAME', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.FIRST_NAME, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'GROUP_CONTROL', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.GROUP_CONTROL, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'LANGUAGE_ID', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.LANGUAGE_ID, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'LAST_NAME', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.LAST_NAME, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'LOCALE_ID', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.LOCALE_ID, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'LOGON_COUNT', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.LOGON_COUNT, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MAIL_ID', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.MAIL_ID, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_BACK_COLOR', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.MY_POWERPLANT_BACK_COLOR, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_CLOSE_ALL', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.MY_POWERPLANT_CLOSE_ALL, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_FONT', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.MY_POWERPLANT_FONT, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_FONT_SIZE', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.MY_POWERPLANT_FONT_SIZE, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_OPEN', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.MY_POWERPLANT_OPEN, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_TEXT_COLOR', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.MY_POWERPLANT_TEXT_COLOR, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_PP_POWERTAX_USER', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.MY_PP_POWERTAX_USER, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'OBJECTCLASS', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.OBJECTCLASS, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'ORG_USERS', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.ORG_USERS, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'OTHER', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.OTHER, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'PHONE_NUMBER', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.PHONE_NUMBER, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'PLANT_ACCOUNTANT', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.PLANT_ACCOUNTANT, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'PROJ_MGR', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.PROJ_MGR, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'STATUS', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.STATUS, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'SUPERVISOR_USERS', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.SUPERVISOR_USERS, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'TEST_WINDOW', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.TEST_WINDOW, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'USERS', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.USERS, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, NEW_VALUE, NEW_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'WINDOW_RESIZE', 
        ''|| 'USERS=' || :new.USERS||'; ', pk_lookup, :new.WINDOW_RESIZE, 
         new_lookup, USER, SYSDATE, prog, 'I', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
END IF; 
IF DELETING THEN 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'ADSI_PATH', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.ADSI_PATH, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'APPWORKSPACE', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.APPWORKSPACE, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'CHANGE_DATE', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.CHANGE_DATE, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'COMPANY_ID', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.COMPANY_ID, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'CONTRACT_ADM', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.CONTRACT_ADM, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DEFAULT_SOB', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.DEFAULT_SOB, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DEL_COUNT', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.DEL_COUNT, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DEPARTMENT_ID', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.DEPARTMENT_ID, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DESCRIPTION', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.DESCRIPTION, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DIGIT_LEN', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.DIGIT_LEN, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'DIRECTORY', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.DIRECTORY, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'ENABLE_TOOLTIP', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.ENABLE_TOOLTIP, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'ENGINEER', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.ENGINEER, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'EXT_USER_CODE', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.EXT_USER_CODE, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'EXTERNAL_USER_FLAG', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.EXTERNAL_USER_FLAG, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'FIRST_NAME', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.FIRST_NAME, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'GROUP_CONTROL', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.GROUP_CONTROL, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'LANGUAGE_ID', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.LANGUAGE_ID, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'LAST_NAME', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.LAST_NAME, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'LOCALE_ID', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.LOCALE_ID, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'LOGON_COUNT', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.LOGON_COUNT, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MAIL_ID', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.MAIL_ID, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_BACK_COLOR', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.MY_POWERPLANT_BACK_COLOR, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_CLOSE_ALL', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.MY_POWERPLANT_CLOSE_ALL, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_FONT', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.MY_POWERPLANT_FONT, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_FONT_SIZE', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.MY_POWERPLANT_FONT_SIZE, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_OPEN', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.MY_POWERPLANT_OPEN, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_POWERPLANT_TEXT_COLOR', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.MY_POWERPLANT_TEXT_COLOR, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'MY_PP_POWERTAX_USER', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.MY_PP_POWERTAX_USER, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'OBJECTCLASS', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.OBJECTCLASS, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'ORG_USERS', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.ORG_USERS, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'OTHER', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.OTHER, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'PHONE_NUMBER', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.PHONE_NUMBER, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'PLANT_ACCOUNTANT', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.PLANT_ACCOUNTANT, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'PROJ_MGR', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.PROJ_MGR, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'STATUS', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.STATUS, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'SUPERVISOR_USERS', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.SUPERVISOR_USERS, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'TEST_WINDOW', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.TEST_WINDOW, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'USERS', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.USERS, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
      INSERT INTO PP_AUDITS_SYSTEM_TRAIL 
        (SEQUENCE_KEY, TABLE_NAME, COLUMN_NAME, PRIMARY_KEY, PK_DISPLAY, OLD_VALUE, OLD_DISPLAY, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, 
         PROGRAM, ACTION, MONTH_NUMBER, COMMENTS, ACTIVE_WINDOW, WINDOW_TITLE, PROCESS) 
        VALUES 
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, 'PP_SECURITY_USERS', 'WINDOW_RESIZE', 
        ''|| 'USERS=' || :old.USERS||'; ', pk_lookup, :old.WINDOW_RESIZE, 
         old_lookup, USER, SYSDATE, prog, 'D', to_char(SYSDATE, 'yyyymm'), comments, window, windowtitle, trans); 
END IF; 
END; 
/
DELETE FROM PP_TABLE_AUDITS WHERE TABLE_NAME = 'PP_SECURITY_USERS';
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'ADSI_PATH', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'APPWORKSPACE', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'CHANGE_DATE', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'COMPANY_ID', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', 'company', 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'CONTRACT_ADM', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'DEFAULT_SOB', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'DEL_COUNT', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'DEPARTMENT_ID', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', 'department', 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'DESCRIPTION', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'DIGIT_LEN', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'DIRECTORY', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'ENABLE_TOOLTIP', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'ENGINEER', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'EXTERNAL_USER_FLAG', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'EXT_USER_CODE', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'FIRST_NAME', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'GROUP_CONTROL', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'LANGUAGE_ID', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', 'pp_languages', 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'LAST_NAME', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'LOCALE_ID', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'LOGON_COUNT', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'MAIL_ID', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'MY_POWERPLANT_BACK_COLOR', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'MY_POWERPLANT_CLOSE_ALL', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'MY_POWERPLANT_FONT', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'MY_POWERPLANT_FONT_SIZE', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'MY_POWERPLANT_OPEN', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'MY_POWERPLANT_TEXT_COLOR', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'MY_PP_POWERTAX_USER', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'OBJECTCLASS', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'ORG_USERS', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'OTHER', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'PHONE_NUMBER', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'PLANT_ACCOUNTANT', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'PROJ_MGR', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'STATUS', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'SUPERVISOR_USERS', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'TEST_WINDOW', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'USERS', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
INSERT INTO "PP_TABLE_AUDITS" ("TABLE_NAME", "COLUMN_NAME", "USER_ID", "TIMESTAMP", "TRIGGER_NAME", "ACTION", "DROPDOWN_NAME", "SUBSYSTEM", "PK_LOOKUP_SQL", "USE_PK_LOOKUP") VALUES ('PP_SECURITY_USERS', 'WINDOW_RESIZE', 'PWRPLANT', to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PP_AU_PP_SECURITY_USERS', 'IUD', null, 'SYSTEM', null, 0) ; 
DELETE FROM PP_TABLE_AUDITS_OBJ_ACTIONS WHERE TRIGGER_NAME = 'PP_AU_PP_SECURITY_USERS';
DELETE FROM PP_TABLE_AUDITS_PK_LOOKUP WHERE TABLE_NAME = 'PP_SECURITY_USERS';
INSERT INTO "PP_TABLE_AUDITS_PK_LOOKUP" ("TABLE_NAME", "COLUMN_NAME", "DISPLAY_TABLE_NAME", "DISPLAY_COLUMN_NAME", "CODE_COLUMN_NAME", "TIME_STAMP", "USER_ID") VALUES ('PP_SECURITY_USERS', 'USERS', null, null, null, to_date('2016-09-05 00:51:49', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ; 

--**************************
-- Log the run of the script
--**************************

insert into PP_UPDATE_FLEX
(COL_ID,
TYPE_NAME,
FLEXVCHAR1)
select NVL(MAX(COL_ID), 0) + 1,
'UPDATE_SCRIPTS',
'10.4.1.2_additional_20001_add_escalate_reserve_yn.sql'
from PP_UPDATE_FLEX;
commit;

