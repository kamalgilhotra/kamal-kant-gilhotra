/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047281_09_lease_default_multicurrency_values_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/24/2017 Jared Watkins    default all of the currency-related values necessary
||                                        for Lease Multicurrency to function on a new or existing install
||============================================================================
*/

declare
  PPCMSG varchar2(10) := 'PPC-MSG> ';
  PPCERR varchar2(10) := 'PPC-ERR> ';
  PPCSQL varchar2(10) := 'PPC-SQL> ';
  PPCORA varchar2(10) := 'PPC-ORA' || '-> ';

  LL_BAD_COUNT number;
  -- change value of LB_SKIP_CHECK to TRUE if you determine there is data that is OK but will always
  -- show up as an exception.
  LB_SKIP_CHECK boolean := true;
begin
  DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

  if not LB_SKIP_CHECK then
    --check here for invalid existing currencies and throw an exception
    select count(*)
      into LL_BAD_COUNT
    from currency
    where (currency_id = 1 
      and ((description <> 'USD' and description <> 'United States Dollar') or currency_display_symbol <> '$')
    )
    or (currency_id = 2 
      and ((description <> 'EUR' and description <> 'Euro Member Countries') or currency_display_symbol <> '�')
    )
    or (currency_id = 3 
      and ((description <> 'MXN' and description <> 'Mexican Peso') or currency_display_symbol <> '$')
    )
    or (currency_id = 4
      and ((description <> 'GBP' and description <> 'United Kingdom Pound') or currency_display_symbol <> '�')
    )
    or (currency_id = 5
      and ((description <> 'CAD' and description <> 'Canadian Dollar') or currency_display_symbol <> '$')
    );
    
    if LL_BAD_COUNT > 0 then
      DBMS_OUTPUT.PUT_LINE(PPCORA || 'ORA' ||
                          '-02000 - Contact PPC support for help.  Read message below.');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '  Please edit the script and read the comments at ');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '  the bottom to see how to evaluate the data issues.');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '  To skip checking this in the future.');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '  change this line: LB_SKIP_CHECK boolean := false;');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '  to:               LB_SKIP_CHECK boolean := true;');

      RAISE_APPLICATION_ERROR(-20000, 'Currency data issue, contact PPC support for help.');
    else
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Currency data is OK.');
    end if;
  else
    DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Currency check not performed.');
  end if;
  
  --default the CURRENCY table
  merge into currency a
    using (select 1 currency_id, 'United States Dollar' description, 1 currency_display_factor, 
                  '$' currency_display_symbol, 'USD' iso_code from dual
              union
           select 2 currency_id, 'Euro Member Countries', 1, '�', 'EUR' from dual
              union
           select 3 currency_id, 'Mexican Peso', 1, '$', 'MXN' from dual
              union
           select 4 currency_id, 'United Kingdom Pound', 1, '�', 'GBP' from dual
              union
           select 5 currency_id, 'Canadian Dollar', 1, '$', 'CAD' from dual) b
    on (a.currency_id = b.currency_id)
    when matched then update set a.description = b.description, a.currency_display_symbol = b.currency_display_symbol, a.iso_code = b.iso_code
    when not matched then insert (currency_id, description, currency_display_factor, currency_display_symbol, iso_code)
      values(b.currency_id, b.description, b.currency_display_factor, b.currency_display_symbol, b.iso_code);

  --default CURRENCY_SCHEMA with all the company currencies (set to USD) - for companies without an 'Actuals' row already
  INSERT INTO currency_schema(currency_schema_id, company_id, currency_type_id, currency_id)
  SELECT max_cs_id + ROWNUM, comp.company_id, 1, 1
  FROM company_setup comp, (SELECT MAX(currency_schema_id) max_cs_id FROM currency_schema)
  WHERE NOT EXISTS (SELECT 1 FROM currency_schema cs2 WHERE cs2.company_id = comp.company_id AND currency_type_id = 1);

  --default all of the MLA contract currencies to USD for MLAs with no current contract currency
  UPDATE ls_lease
  SET contract_currency_id = 1
  WHERE contract_currency_id IS NULL;

  --default all of the lease asset contract currencies to the MLA/ILR currency, or USD if not associated with an MLA/ILR
  UPDATE ls_asset la
  SET contract_currency_id =
            (SELECT contract_currency_id
              FROM ls_lease ll 
              INNER JOIN ls_ilr li 
              ON ll.lease_id = li.lease_id
              WHERE li.ilr_id = la.ilr_id)
  WHERE ilr_id IS NOT NULL;
                            
  UPDATE ls_asset 
  SET contract_currency_id = 1 
  WHERE contract_currency_id IS NULL;
  
end;
/
  
/*
As part of the new Multicurrency feature in Lease, we are now pre-delivering a list of currencies 
to the client. Since we need specific currencies that we support to be included in the CURRENCY table,
this script will check to see if the client has any currencies in this table and if they are different 
from the currencies we expect. If there are different currencies, an exception will be thrown and this 
script will error out, without defaulting any of the necessary Multicurrency data. If there are not any
issues or the currencies they have are the expected ones, the values will be updated to the expected
descriptions/ISO codes/currency symbols, and the other Multicurrency data will be defaulted as well. 

Expected values for the CURRENCY table can be found in the MERGE statement above.
*/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3452, 0, 2017, 1, 0, 0, 47281, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047281_09_lease_default_multicurrency_values_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

