/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_036380_proptax_mi3589_mods.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 09/25/2014 Anand Rajashekar    changes to MI-3589 returns
|| 10.4.3.0 10/28/2014 Andrew Scott        changed to work with non "MI" as state id.
||========================================================================================
*/

-- Add rolllup values
insert into PWRPLANT.PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE)
values
   (33001, 23, sysdate, user, '3589-1-G4 Assets', '3589-1-G4 Assets', '');

insert into PWRPLANT.PT_TYPE_ROLLUP_VALUES
   (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE)
values
   (33001, 24, sysdate, user, '3589-1-G5 Assets', '3589-1-G5 Assets', '');

--Add Reserve factors

insert into PWRPLANT.PT_RESERVE_FACTORS
   (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID)
values
   (33024, sysdate, user, 'MI-3589-1-G4 Assets', 'MI-3589-1-G4 Assets', 1, 2);

insert into PWRPLANT.PT_RESERVE_FACTORS
   (RESERVE_FACTOR_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RESERVE_FACTOR_TYPE_ID, STATUS_CODE_ID)
values
   (33025, sysdate, user, 'MI-3589-1-G5 Assets', 'MI-3589-1-G5 Assets', 1, 2);

--Add property type

insert into PWRPLANT.PROPERTY_TAX_TYPE_DATA
   (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD,
    RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID,
    INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, MARKET_VALUE_RATE_ID,
    LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID,
    KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID)
select 
   33036, sysdate, user, 'MI-3589-1-G4 Assets', DB_STATE_ID, 
   TO_DATE('01/01/2001', 'mm/dd/yyyy'), 'Inactive',
    1, 3, 33024, null, null, 0, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null
from D1_PT_CONV_STATE_XLATE
where trim(state_id) = 'MI';

insert into PWRPLANT.PROPERTY_TAX_TYPE_DATA
   (PROPERTY_TAX_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, STATE_ID, VALUATION_DATE, STATUS, METHOD, RESERVE_METHOD,
    RESERVE_FACTOR_ID, DEPRECIATION_FLOOR, DEPRECIATION_FLOOR_RECOVERY, ALLOCATION_TYPE, ALLOCATION_ID,
    INCREMENTAL_METHOD, VINTAGE_NONVINTAGE, VINTAGE_MONTH_YN, QUANTITY_INDICATOR, ESCALATED_VALUES, MARKET_VALUE_RATE_ID,
    LEVY_CLASS_ID, EXTERNAL_CODE, UNIT_COST_ID, QUANTITY_CONVERSION_ID, QUANTITY_FACTOR, PARCEL_TYPE_ID, SET_OF_BOOKS_ID,
    KEEP_VINTAGE_ON_LEDGER, DETAIL_MAP_ID)
select 
   33037, sysdate, user, 'MI-3589-1-G5 Assets', DB_STATE_ID, 
   TO_DATE('01/01/2002', 'mm/dd/yyyy'), 'Inactive',
    1, 3, 33025, null, null, 0, null, 1, 0, 0, 0, null, null, 1, '', null, null, null, 0, 1, null, null
from D1_PT_CONV_STATE_XLATE
where trim(state_id) = 'MI';

--Add rollup assign

insert into PWRPLANT.PT_TYPE_ROLLUP_ASSIGN
   (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID)
values
   (33036, 33001, 23, sysdate, user);

insert into PWRPLANT.PT_TYPE_ROLLUP_ASSIGN
   (PROPERTY_TAX_TYPE_ID, TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID)
values
   (33037, 33001, 24, sysdate, user);


----create the class code values from the property tax types (needed for tree assigns)

insert into CLASS_CODE_VALUES
   (CLASS_CODE_ID, value, EXT_CLASS_CODE_VALUE)
   select Q.CLASS_CODE_ID, P.DESCRIPTION, TO_CHAR(P.PROPERTY_TAX_TYPE_ID)
     from PROPERTY_TAX_TYPE_DATA P,
          (select CLASS_CODE_ID, DECODE(CPR_INDICATOR, 1, 1, 2) METHOD
             from CLASS_CODE
            where LOWER(DESCRIPTION) in ('property tax type', 'cwip property tax type', 'rwip property tax type')) Q
    where P.METHOD = Q.METHOD
      and not exists (select 1
             from CLASS_CODE_VALUES EXISTING_CCV
            where EXISTING_CCV.CLASS_CODE_ID = Q.CLASS_CODE_ID
              and EXISTING_CCV.VALUE = P.DESCRIPTION);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1461, 0, 10, 4, 3, 0, 36380, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_036380_proptax_mi3589_mods.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
