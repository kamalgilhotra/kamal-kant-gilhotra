/*
||============================================================================
|| Application: PowerPlan
|| File Name:   internal_049254_lessor_03_populate_formula_components_and_variable_payments_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/29/2017 Jared Watkins    Populate some values for the three types of Formula Components, as well as some Variable Payments (including formulas)
||============================================================================
*/
--I included commented-out check SQL throughout if you want to run it in steps and verify it all works
--This script also commits at the end, so you'll want to be sure you want this to go in before you run the whole thing
--Also, there are unique indices on descriptions of Formula Components and Variable Payments,
--  which is the only reason we can use those in where clauses and be sure we are getting the correct rows, so be careful using this same logic elsewhere
--I may come back to this and clean it up a bit (to be more fancy) so that we aren't relying on checking descriptions, but for now we'll have to make due

--create the formula components
insert into lsr_formula_component(formula_component_id, description, long_description, component_type_id, number_format_id)
select ls_formula_component_seq.nextval, 'IR1', 'IR1 (Added by script)', 1, 1 from dual;
insert into lsr_formula_component(formula_component_id, description, long_description, component_type_id, number_format_id)
select ls_formula_component_seq.nextval, 'VC1', 'VC1 (Added by script)', 2, 1 from dual;
insert into lsr_formula_component(formula_component_id, description, long_description, component_type_id, number_format_id)
select ls_formula_component_seq.nextval, 'VC2', 'VC2 (Added by script)', 2, 1 from dual;
insert into lsr_formula_component(formula_component_id, description, long_description, component_type_id, number_format_id)
select ls_formula_component_seq.nextval, 'HC1', 'HC1 (Added by script)', 3, 1 from dual;

--select * from lsr_formula_component where description in ('IR1','VC1','VC2','HC1') order by 1;

--add the calendars for the VCs
--could write this with PL/SQL to grab the sequence IDs and use those for these inserts
--probably a good idea going forward but I'm going to work with this as a static insert for now
insert into lsr_var_component_calendar(formula_component_id, load_month_number, payment_month_number)
select formula_component_id, 1, 1 from lsr_formula_component where description = 'VC1'
union
select formula_component_id, 2, 2 from lsr_formula_component where description = 'VC1'
union
select formula_component_id, 3, 3 from lsr_formula_component where description = 'VC1'
union
select formula_component_id, 4, 4 from lsr_formula_component where description = 'VC1'
union
select formula_component_id, 5, 5 from lsr_formula_component where description = 'VC1'
union
select formula_component_id, 6, 6 from lsr_formula_component where description = 'VC1'
union
select formula_component_id, 7, 7 from lsr_formula_component where description = 'VC1'
union
select formula_component_id, 8, 8 from lsr_formula_component where description = 'VC1'
union
select formula_component_id, 9, 9 from lsr_formula_component where description = 'VC1'
union
select formula_component_id, 10, 10 from lsr_formula_component where description = 'VC1'
union
select formula_component_id, 11, 11 from lsr_formula_component where description = 'VC1'
union
select formula_component_id, 12, 12 from lsr_formula_component where description = 'VC1'
union
select formula_component_id, 1, 1 from lsr_formula_component where description = 'VC2'
union
select formula_component_id, 2, 2 from lsr_formula_component where description = 'VC2'
union
select formula_component_id, 3, 3 from lsr_formula_component where description = 'VC2'
union
select formula_component_id, 4, 4 from lsr_formula_component where description = 'VC2'
union
select formula_component_id, 5, 5 from lsr_formula_component where description = 'VC2'
union
select formula_component_id, 6, 6 from lsr_formula_component where description = 'VC2'
union
select formula_component_id, 7, 7 from lsr_formula_component where description = 'VC2'
union
select formula_component_id, 8, 8 from lsr_formula_component where description = 'VC2'
union
select formula_component_id, 9, 9 from lsr_formula_component where description = 'VC2'
union
select formula_component_id, 10, 10 from lsr_formula_component where description = 'VC2'
union
select formula_component_id, 11, 11 from lsr_formula_component where description = 'VC2'
union
select formula_component_id, 12, 12 from lsr_formula_component where description = 'VC2';

--select b.description, a.* from lsr_var_component_calendar a
--inner join lsr_formula_component b
--on a.formula_component_id = b.formula_component_id
--where a.formula_component_id in (select formula_component_id from lsr_formula_component where description in ('VC1','VC2'))
--order by 2, 3;

--add the calendar rows for the HC
insert into lsr_hist_component_calendar(formula_component_id, calc_month_number, timing, payment_month_number)
select formula_component_id, 1, 1, 1 from lsr_formula_component where description = 'HC1'
union
select formula_component_id, 2, 1, 2 from lsr_formula_component where description = 'HC1'
union
select formula_component_id, 3, 1, 3 from lsr_formula_component where description = 'HC1'
union
select formula_component_id, 4, 1, 4 from lsr_formula_component where description = 'HC1'
union
select formula_component_id, 5, 1, 5 from lsr_formula_component where description = 'HC1'
union
select formula_component_id, 6, 1, 6 from lsr_formula_component where description = 'HC1'
union
select formula_component_id, 7, 1, 7 from lsr_formula_component where description = 'HC1'
union
select formula_component_id, 8, 1, 8 from lsr_formula_component where description = 'HC1'
union
select formula_component_id, 9, 1, 9 from lsr_formula_component where description = 'HC1'
union
select formula_component_id, 10, 1, 10 from lsr_formula_component where description = 'HC1'
union
select formula_component_id, 11, 1, 11 from lsr_formula_component where description = 'HC1'
union
select formula_component_id, 12, 1, 12 from lsr_formula_component where description = 'HC1';

--select b.description, a.* from lsr_hist_component_calendar a
--inner join lsr_formula_component b
--on a.formula_component_id = b.formula_component_id
--where a.formula_component_id = (select formula_component_id from lsr_formula_component where description = 'HC1')
--order by 3;

--add the VP header rows
insert into lsr_variable_payment(variable_payment_id, description, long_description, active_for_new_ilrs, preview_option)
select ls_variable_payment_seq.nextval, 'VP1', 'VP1 (Added by script)', 1, -1 from dual;
insert into lsr_variable_payment(variable_payment_id, description, long_description, active_for_new_ilrs, preview_option)
select ls_variable_payment_seq.nextval, 'VP2', 'VP2 (Added by script)', 1, -1 from dual;
insert into lsr_variable_payment(variable_payment_id, description, long_description, active_for_new_ilrs, preview_option)
select ls_variable_payment_seq.nextval, 'VP3', 'VP3 (Added by script)', 1, -1 from dual;
insert into lsr_variable_payment(variable_payment_id, description, long_description, active_for_new_ilrs, preview_option)
select ls_variable_payment_seq.nextval, 'VP4', 'VP4 (Added by script)', 1, -1 from dual;

--select * from lsr_variable_payment where description in ('VP1','VP2','VP3','VP4') order by 1;

--add the HC value (VP ID)
insert into lsr_historic_component_values(formula_component_id, variable_payment_id)
select formula_component_id, variable_payment_id from lsr_formula_component, lsr_variable_payment
where lsr_formula_component.description = 'HC1' and lsr_variable_payment.description = 'VP2';

--select a.formula_component_id, a.variable_payment_id, b.description, c.description from lsr_historic_component_values a
--inner join lsr_variable_payment b
--on a.variable_payment_id = b.variable_payment_id
--inner join lsr_formula_component c
--on a.formula_component_id = c.formula_component_id
--where a.formula_component_id = (select formula_component_id from lsr_formula_component where description = 'HC1')
--and a.variable_payment_id = (select variable_payment_id from lsr_variable_payment where description = 'VP2');

--add the VP/FC references
insert into lsr_variable_payment_component(variable_payment_id, formula_component_id)
select variable_payment_id, formula_component_id from lsr_variable_payment, lsr_formula_component
where lsr_variable_payment.description = 'VP1' and lsr_formula_component.description = 'IR1'
union
select variable_payment_id, formula_component_id from lsr_variable_payment, lsr_formula_component
where lsr_variable_payment.description = 'VP2' and lsr_formula_component.description = 'VC1'
union
select variable_payment_id, formula_component_id from lsr_variable_payment, lsr_formula_component
where lsr_variable_payment.description = 'VP3' and lsr_formula_component.description = 'HC1'
union
select variable_payment_id, formula_component_id from lsr_variable_payment, lsr_formula_component
where lsr_variable_payment.description = 'VP4' and lsr_formula_component.description = 'VC2';

--select a.variable_payment_id, a.formula_component_id, b.description, c.description from lsr_variable_payment_component a
--inner join lsr_variable_payment b
--on a.variable_payment_id = b.variable_payment_id
--inner join lsr_formula_component c
--on a.formula_component_id = c.formula_component_id
--where a.variable_payment_id in
--(select variable_payment_id from lsr_variable_payment where description in ('VP1','VP2','VP3','VP4'));

--update the VP formulas
--these formulas will likely need to be updated in the future once Andrew has finalised his pieces of the Lessor VP package
--in Lessee, IRCs send in the component ID, ILR ID, revision, initial amount flag, and month
--VCs send in the component ID, asset ID, and month
--HCs (asset-level) send in the component ID, asset ID, asset-level flag(1), set of books ID, and month
--HCs (ILR-level) send in the component ID, ILR ID, revision, asset-level flag (0), set of books ID, and month
update lsr_variable_payment set payment_formula = ' ''IR1'' + 5 ',
payment_formula_db = ' nvl(PKG_LESSOR_VAR_PAYMENTS.F_CALC_INDEX('|| (select formula_component_id from lsr_formula_component where description = 'IR1') ||',lio.ilr_id,lio.revision,b.initial_amt_flag,a.month),0) + 5 '
where description = 'VP1';
update lsr_variable_payment set payment_formula = ' ''VC1'' * 5 ',
payment_formula_db = ' nvl(PKG_LESSOR_VAR_PAYMENTS.F_CALC_VAR_COMPONENT('|| (select formula_component_id from lsr_formula_component where description = 'VC1') ||',a.lsr_asset_id,a.month),0) * 5 '
where description = 'VP2';
update lsr_variable_payment set payment_formula = ' ''HC1'' - 2 ',
payment_formula_db = ' nvl(PKG_LESSOR_VAR_PAYMENTS.F_CALC_HISTORIC('|| (select formula_component_id from lsr_formula_component where description = 'HC1') ||',a.lsr_asset_id,a.revision,1,a.set_of_books_id,a.month),0) - 2 '
where description = 'VP3';
update lsr_variable_payment set payment_formula = ' ''VC2'' / 4 ',
payment_formula_db = ' nvl(PKG_LESSOR_VAR_PAYMENTS.F_CALC_VAR_COMPONENT('|| (select formula_component_id from lsr_formula_component where description = 'VC2') ||',a.lsr_asset_id,a.month),0) / 4 '
where description = 'VP4';

--select description, payment_formula, payment_formula_db from lsr_variable_payment
--where description in ('VP1','VP2','VP3','VP4') order by variable_payment_id;

commit;