/*
||============================================================================
|| Application: PowerPlan
|| File Name:   internal_049481_lessor_01_remove_duplicate_description_fields_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/14/2017 Jared Watkins    remove any duplicate descriptions so we can add unique indices to all the Lessor tables
||============================================================================
*/
--internal script to run before adding the new indices
update lsr_cap_type set description = description || cap_type_id
where upper(trim(description)) in (select upper(trim(description)) from lsr_cap_type group by upper(trim(description)) having count(upper(trim(description))) > 1);

update lsr_fasb_cap_type set description = description || fasb_cap_type_id
where upper(trim(description)) in (select upper(trim(description)) from lsr_fasb_cap_type group by upper(trim(description)) having count(upper(trim(description))) > 1);

update lsr_ilr set ilr_number = ilr_number || ilr_id
where upper(trim(ilr_number)) in (select upper(trim(ilr_number)) from lsr_ilr group by upper(trim(ilr_number)) having count(upper(trim(ilr_number))) > 1);

update lsr_ilr_group set description = description || ilr_group_id
where upper(trim(description)) in (select upper(trim(description)) from lsr_ilr_group group by upper(trim(description)) having count(upper(trim(description))) > 1);

update lsr_lease set lease_number = lease_number || lease_id
where upper(trim(lease_number)) in (select upper(trim(lease_number)) from lsr_lease group by upper(trim(lease_number)) having count(upper(trim(lease_number))) > 1);

update lsr_lease_group set description = description || lease_group_id
where upper(trim(description)) in (select upper(trim(description)) from lsr_lease_group group by upper(trim(description)) having count(upper(trim(description))) > 1);

update lsr_lessee set description = description || lessee_id
where upper(trim(description)) in (select upper(trim(description)) from lsr_lessee group by upper(trim(description)) having count(upper(trim(description))) > 1);

update lsr_receivable_bucket_admin set bucket_name = bucket_name || bucket_number
where (upper(trim(bucket_name)), receivable_type) in (select upper(trim(bucket_name)), receivable_type from lsr_receivable_bucket_admin group by upper(trim(bucket_name)), receivable_type having count(*) > 1);