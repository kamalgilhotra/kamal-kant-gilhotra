/*
||============================================================================
|| Application: PowerPlan
|| File Name:   internal_049254_lessor_04_delete_formula_components_and_variable_payments_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/29/2017 Jared Watkins    Delete the components and variable payments created in the other internal script
||============================================================================
*/

--delete the association between the VPs and FCs
delete from lsr_variable_payment_component where variable_payment_id in
(select variable_payment_id from lsr_variable_payment where description in ('VP1','VP2','VP3','VP4'));

--delete the association from the HC to the VP it is based on
delete from lsr_historic_component_values where formula_component_id =
(select formula_component_id from lsr_formula_component where description = 'HC1');

--remove the associations to these VPs from any ILRs associated to them
update lsr_ilr_payment_term_var_pay set variable_payment_id = null
where variable_payment_id in (select variable_payment_id from lsr_variable_payment where description in ('VP1','VP2','VP3','VP4'));

--delete the variable payments
delete from lsr_variable_payment where description in ('VP1','VP2','VP3','VP4');

--delete the calendar rows for the HC
delete from lsr_hist_component_calendar where formula_component_id =
(select formula_component_id from lsr_formula_component where description = 'HC1');

--delete the calendar rows for the VCs
delete from lsr_var_component_calendar where formula_component_id in
(select formula_component_id from lsr_formula_component where description in ('VC1','VC2'));

--delete any amounts loaded for the VCs
delete from lsr_variable_component_values where formula_component_id in
(select formula_component_id from lsr_formula_component where description in ('VC1','VC2'));

--delete any amounts loaded for the IRC
delete from lsr_index_component_values where formula_component_id =
(select formula_component_id from lsr_formula_component where description = 'IR1');

--delete the formula components
delete from lsr_formula_component where formula_component_id in
(select formula_component_id from lsr_formula_component where description in ('IR1','VC1','VC2','HC1'));

commit;