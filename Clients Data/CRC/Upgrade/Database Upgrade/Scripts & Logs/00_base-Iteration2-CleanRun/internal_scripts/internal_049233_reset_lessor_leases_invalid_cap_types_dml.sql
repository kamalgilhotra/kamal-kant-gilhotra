/*
||============================================================================
|| Application: PowerPlan
|| File Name:   internal_049233_reset_lessor_leases_invalid_cap_types_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/11/2017 Andrew Hill      Reset cap types for lessor leases with invalid cap types
||============================================================================
*/

UPDATE lsr_lease
SET lease_cap_type_id = ( SELECT cap_type_id
                          FROM lsr_cap_type
                          WHERE rownum = 1)
WHERE lease_cap_type_id NOT IN (SELECT cap_type_id
                                FROM lsr_cap_type);

commit;