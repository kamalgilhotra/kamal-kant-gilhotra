/*
||============================================================================
|| Application: PowerPlan
|| File Name: internal_049858_lessor_00_init_lsr_mec_process_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/06/2017 Sarah Byers	 Initialize lsr_mec_config, lsr_process_control
||============================================================================
*/

insert into lsr_process_control (company_id, gl_posting_mo_yr)
select company_id, max(gl_posting_mo_yr)
  from ls_process_control
 group by company_id
minus
select company_id, gl_posting_mo_yr from lsr_process_control;

insert into lsr_mec_config (company_id, accruals, invoices, currency_gain_loss, auto_termination)
select company_id, 1, 1, 1, 0 from (
select company_id
  from lsr_process_control
minus
select company_id from lsr_mec_config);

commit;