/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048886_lessor_internal_add_new_book_summaries_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/21/2017 Jared Watkins    Insert the new Book Summary records for the Lessor cap types
||============================================================================
*/
--this shouldn't need to be approved
--add the new rows for Lessor
declare
  current_max_id number(22,0);
begin
  select max(book_summary_id) into current_max_id from book_summary;
  insert into book_summary(book_summary_id, summary_name, book_summary_type, currency_type_id, long_description)
  select current_max_id + 1, 'Lessor: O&M', 'LESSOR', 1, 'Lessor: O&M'
  from dual
  where not exists (select 1 from book_summary where summary_name = 'Lessor: O&M' and book_summary_type = 'LESSOR')
  union
  select current_max_id + 2, 'Lessor: Sales Type', 'LESSOR', 1, 'Lessor: Sales Type'
  from dual
  where not exists (select 1 from book_summary where summary_name = 'Lessor: Sales Type' and book_summary_type = 'LESSOR')
  union
  select current_max_id + 3, 'Lessor: Direct Finance', 'LESSOR', 1, 'Lessor: Direct Finance'
  from dual
  where not exists (select 1 from book_summary where summary_name = 'Lessor: Direct Finance' and book_summary_type = 'LESSOR');
end;
/

