/*
||============================================================================
|| Application: PowerPlan
|| File Name:   internal_049254_lessor_02_populate_lsr_distinct_asset_ilr_table_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/26/2017 Jared Watkins    Populate the new table that maintains the list of distinct asset/ILR combinations (ignoring revision)
||============================================================================
*/

insert into lsr_distinct_asset_ilr(ilr_id, asset_id)
select distinct ilr_id, lsr_asset_id from lsr_asset;
