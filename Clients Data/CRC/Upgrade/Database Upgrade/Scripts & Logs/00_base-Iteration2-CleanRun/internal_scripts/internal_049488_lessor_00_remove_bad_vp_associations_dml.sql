/*
||============================================================================
|| Application: PowerPlan
|| File Name: internal_049488_lessor_00_remove_bad_vp_associations_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 10/20/2017 Jared Watkins  Remove any existing VP associations in the
||                           LSR_ILR_PAYMENT_TERM_VAR_PAY table so we can add the FK to LSR_VARIABLE_PAYMENT
||============================================================================
*/

--remove bad data so we can apply the FK we need
update lsr_ilr_payment_term_var_pay set variable_payment_id = null where variable_payment_id is not null;

commit;