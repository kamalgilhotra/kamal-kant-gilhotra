/*
||============================================================================
|| Application: PowerPlan
|| File Name:   internal_048883_lessor_01_update_lsr_ilr_group_accounts_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/14/2017 JSisouphanh      Update accounts in ILR Group
||============================================================================
*/

UPDATE LSR_ILR_GROUP
SET	INT_ACCRUAL_ACCOUNT_ID 			= 43142000, 
	INT_EXPENSE_ACCOUNT_ID      	= 43142000,
	EXEC_ACCRUAL_ACCOUNT_ID  		= 43142000,
	EXEC_EXPENSE_ACCOUNT_ID   		= 43142000,
	CONT_ACCRUAL_ACCOUNT_ID   		= 43142000,
	CONT_EXPENSE_ACCOUNT_ID   		= 43142000,
	ST_RECEIVABLE_ACCOUNT_ID   		= 43142000,
	LT_RECEIVABLE_ACCOUNT_ID   		= 43142000,
	AR_ACCOUNT_ID   				= 43142000,
	UNGUARAN_RES_ACCOUNT_ID   		= 43142000,
	INT_UNGUARAN_RES_ACCOUNT_ID 	= 43142000,
	SELL_PROFIT_LOSS_ACCOUNT_ID 	= 43142000,
	INI_DIRECT_COST_ACCOUNT_ID  	= 43142000,
	PROP_PLANT_ACCOUNT_ID   		= 43142000,
	ST_DEFERRED_ACCOUNT_ID   		= 43142000,
	LT_DEFERRED_ACCOUNT_ID   		= 43142000,
	CURR_GAIN_LOSS_ACCT_ID    		= 43142000,
	CURR_GAIN_LOSS_OFFSET_ACCT_ID  	= 43142000
WHERE INT_ACCRUAL_ACCOUNT_ID IS NULL;
COMMIT;