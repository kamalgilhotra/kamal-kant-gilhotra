/*
This script inserts accounts from lsr_ilr_group table into lsr_ilr_account table for each ILR.
INTERNAL SCRIPT. DO NOT APPROVE THIS SCRIPT AND RUN IN MASTER OR MINION DB.
*/

INSERT INTO LSR_ILR_ACCOUNT
            (ilr_id,
             int_accrual_account_id,
             int_expense_account_id,
             exec_accrual_account_id,
             exec_expense_account_id,
             cont_accrual_account_id,
             cont_expense_account_id,
             st_receivable_account_id,
             lt_receivable_account_id,
             ar_account_id,
             unguaran_res_account_id,
             int_unguaran_res_account_id,
             sell_profit_loss_account_id,
             ini_direct_cost_account_id,
             prop_plant_account_id,
             st_deferred_account_id,
             lt_deferred_account_id,
             curr_gain_loss_acct_id,
             curr_gain_loss_offset_acct_id)
SELECT ds.ilr_id,
       int_accrual_account_id,
       int_expense_account_id,
       exec_accrual_account_id,
       exec_expense_account_id,
       cont_accrual_account_id,
       cont_expense_account_id,
       st_receivable_account_id,
       lt_receivable_account_id,
       ar_account_id,
       unguaran_res_account_id,
       int_unguaran_res_account_id,
       sell_profit_loss_account_id,
       ini_direct_cost_account_id,
       prop_plant_account_id,
       st_deferred_account_id,
       lt_deferred_account_id,
       curr_gain_loss_acct_id,
       curr_gain_loss_offset_acct_id
FROM   LSR_ILR_GROUP,
       ( SELECT ilr_id,
                ilr_group_id
         FROM   LSR_ILR a
         WHERE  NOT EXISTS
                    ( SELECT 1
                      FROM   LSR_ILR_ACCOUNT b
                      WHERE  a.ilr_id = b.ilr_id ) ) ds
WHERE  LSR_ILR_GROUP.ilr_group_id = ds.ilr_group_id;

COMMIT; 
