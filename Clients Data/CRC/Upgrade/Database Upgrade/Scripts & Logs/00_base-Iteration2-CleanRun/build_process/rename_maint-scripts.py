import os
import re
import io


goodFilename = re.compile(r"^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+_", flags=re.IGNORECASE)
ppVersion = re.compile(r"(insert into PP_SCHEMA_CHANGE_LOG[^(]*\([^(]*)\([\s\n]*([0-9]+)[\s\n]*,[\s\n]*([0-9]+)[\s\n]*,[\s\n]*([0-9]+)[\s\n]*,[\s\n]*([0-9]+)[\s\n]*,[\s\n]*([0-9]+)[\s\n]*,[\s\n]*([0-9]+)[\s\n]*,[\s\n]*([0-9]+)[\s\n]*,[\s\n]*'([^']+)'[\s\n]*,[\s\n]*'([^']+)'[\s\n]*,", flags=re.IGNORECASE | re.MULTILINE)


for x in os.listdir():
    if x[-4:].lower() == '.sql':
        rename = False
        newFilename = x
        newFileString = ''
        lineNum = 0
        foundPPSchema = False
        footer = ''
        versionFormat = ''
        try:
            with io.open(x, 'r', encoding='utf8') as f:
                content = f.read()
            versions = ppVersion.findall(content)
            if not versions:
                print(x)
            else:
                version = versions[-1]
                versionFormat = '{}.{}.{}.{}'.format(version[3], version[4], version[5], version[6])
                newFileString = version[9]
                if not version[9].startswith(versionFormat):
                    newFileString = '{}_{}'.format(versionFormat, newFileString)
                    # print('str, ',newFileString)
                if not goodFilename.match(x):
                    rename = True
                    newFilename = '{}_{}'.format(versionFormat, x)
                    # print('file, ',newFilename)
                if newFilename.lower() != newFileString.lower():
                    print('{}\n{}\ndo not match\n\n'.format(newFilename, newFileString))
            content_new = re.sub(ppVersion, r"\1(\2, \3, \4, \5, \6, \7, \8, '\9', '"+ newFileString +"',", content)
            with io.open(x, 'w', encoding='utf8') as f:
                f.write(content_new)
            if rename:
                os.rename(x, newFilename)
        except:
            print("error reading {} on line {}, please change format to UTF-8!".format(x,lineNum))
            continue
        
        