param([string]$InputFolder = '.',[string]$OutputFolder = 'parsed')
#Global
$Global:G_Build_Success=0
$DEBUG=0
# ******************************************************************************
# Start functions
# ******************************************************************************

# ******************************************************************************
# main
# ******************************************************************************

function main ()
{
	# make the output folder in case it doesn't exist
	md -Force $OutputFolder | Out-Null
	foreach ($logFile in Get-ChildItem $InputFolder *.log){
		Parse_PowerGen_Errors -LogFile $logFile.FullName -OutFolder $OutputFolder
	}
}


# ******************************************************************************
# log - Log to the screen - for debug purposes
# ******************************************************************************
function Log($String, $Color)
{
	if ($DEBUG -eq 1) {
   if ($null -eq $Color) {$Color = "white"}
      write-host $String -foregroundcolor $Color
	   #$String | out-file -Filepath $PS_LOG_FILE -append
   }
}

# ******************************************************************************
# print - Print to the screen
# ******************************************************************************
function Print($String, $Color)
{
   if ($null -eq $Color) {$Color = "white"}
      write-host $String -foregroundcolor $Color
	   #$String | out-file -Filepath $PS_LOG_FILE -append
}



# ******************************************************************************
# check_build_log
# ******************************************************************************
function Parse_PowerGen_Errors($LogFile, $OutFolder)
{
   Log "In Parse_PowerGen_Errors";
   $lines = get-content $LogFile;
   $errors = @();
   $nonTraditionalErrors = @();
   $buildFailureNonTraditionalErrors = 0;

   for ($i = 0; $i -lt $lines.length; $i++)
   {
	   if (([regex]::ismatch($lines[$i],"\): Error")) -or ([regex]::ismatch($lines[$i],"\): Warning")))
	   {
		Log " In first IF line: $i";
		Log $Lines[$i];
		   $Global:G_Build_Success = 0;

		   # IF THE LINE ABOVE ME ISN'T AN ERROR OR WARNING 
		   if ((-not [regex]::ismatch($lines[$i-1],"\): Error")) -and (-not [regex]::ismatch($lines[$i-1],"\): Warning")))
		   {
			log "In 2nd if"
			   $begin = 0;
			   $end = 0;
			
			   #LOOP BACKWARDS TO FIND .pbl ENTRY
			   for ($j = $i; $j -gt 0; $j--)
			   {
				
				   if ([regex]::ismatch($lines[$j],"([a-z]+)(.pbl)"))
				   {
   					$begin = $j;
	   				break;
		   		}
			   }
			
			   #LOOP FORWARDS TO FIND ALL ERRORS
			   for ($k = ($i+1); $k -lt $lines.length; $k++)
			   {
				   #if ((-not [regex]::ismatch($lines[$k],"\): Error")) -or (-not [regex]::ismatch($lines[$k],"\): Warning")))
				   if ([regex]::ismatch($lines[$k],"\): Warning"))
				   {
   					continue;
	   			}
		   		if (-not [regex]::ismatch($lines[$k],"\): Error"))
			   	{		
				   	$end += $k;
					   break;
				   }
			   }

			   # WRITE OUT ERROR LOG LINES
			   for ($l = $begin; $l -lt $end; $l++)
			   {
   				$errors += $lines[$l];
	   		}
			$errors += "                 From "+$LogFile
		   	$errors += "------------------------------";
		   }

	   }
	
	   if ([regex]::ismatch($lines[$i],"Could not open exported object file"))
	   {
		   $Global:G_Build_Success = 0;
		   $buildFailureNonTraditionalErrors = 1;
		
		   $nonTraditionalErrors += $lines[$i];
	   }
	   if ([regex]::ismatch($lines[$i],"Regeneration error"))
	   {
		   $Global:G_Build_Success = 0;
		   $buildFailureNonTraditionalErrors = 1;
		
		   $nonTraditionalErrors += $lines[$i];
	   }
	   if ([regex]::ismatch($lines[$i],"0 errors found"))
	   {
		   $Global:G_Build_Success = 1;
	   }
   } #End Loop
	$errors | ForEach-Object {Print -String $_  -Color "red"}
   # $errors | Out-File "parsed_errors.txt";
   # Log "$BUILD_LOGIC_PATH\working\errors.txt"
   Group_Errors -lines $errors -Folder $OutFolder;
}


# ******************************************************************************
# GroupErrors
# ******************************************************************************

function Group_Errors($lines, $Folder)
{
   Log "In Group_Errors";
   $output = @();

   # REMOVE OLD FILES FIRST
   # Remove-Item "$BUILD_LOGIC_PATH\pblEmails\*"

   $begin = 0;
   $end = 0;
   $pbl_name = "";

   # PARSE THOUGH TRADITIONAL ERRORS
   for ($i = 0; $i -lt $lines.length; $i++)
   {	
	   # FIND BEGINNING POINT
	   if ([regex]::ismatch($lines[$i],"([^\\]+)(.pbl)"))
	   {
   		$pbl_name =  [regex]::match($lines[$i],"([^\\]+)(.pbl)").groups[1];
	   	$begin = $i;
	   }
	
	   # FIND THE ENDING POINT
	   if ([regex]::ismatch($lines[$i],"------------------------------"))
	   {
		   $end = $i;
		   # WRITE OUT BLOCK TO pbl log file
		   for ($l = $begin; $l -lt $end; $l++)
		   {
   			Add-Content ($Folder + "\" + $pbl_name + ".log") $lines[$l];
			}
		   Add-Content ($Folder + "\" + $pbl_name + ".log") " ";
		   $begin = 0;
		   $end = 0;
		   $val = "";
		}
	}
}

main