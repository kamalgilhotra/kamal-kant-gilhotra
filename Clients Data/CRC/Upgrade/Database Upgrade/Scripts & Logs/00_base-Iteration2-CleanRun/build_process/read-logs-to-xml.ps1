Param($LogsPath = "", $XmlPath = "")
#Global
$Global:G_Build_Success = 0
$DEBUG = 0
# ******************************************************************************
# Start functions
# ******************************************************************************

# ******************************************************************************
# main
# ******************************************************************************

function main () {
    $currentwd = Get-Location
    Set-Location $LogsPath
    [xml]$Doc = New-Object System.Xml.XmlDocument
    $dec = $Doc.CreateXmlDeclaration("1.0", "UTF-8", $null)
    $root = $doc.CreateNode("element", "testsuite", $null)
    $root.SetAttribute("id", (Get-Date).ToString('yyyyMMdd_HHmmss'))
    #append to document
    $doc.AppendChild($dec) | Out-Null
    # make the output folder in case it doesn't exist
    foreach ($logFile in Get-ChildItem -Recurse *.log) {
        Parse_PowerGen_Errors -LogFile $logFile.FullName -Root $root
    }
    Set-Location $currentwd
    $doc.AppendChild($root) | Out-Null

    #save file
    Write-Host "Saving the XML document to $XmlPath" -ForegroundColor Green

    $utf8WithoutBom = New-Object System.Text.UTF8Encoding($false)
    $sw = New-Object System.IO.StreamWriter($XmlPath, $false, $utf8WithoutBom)

    $doc.save($sw)
    $sw.Close();

    Write-Host "Finished!" -ForegroundColor green
}


# ******************************************************************************
# log - Log to the screen - for debug purposes
# ******************************************************************************
function Log($String, $Color) {
    if ($DEBUG -eq 1) {
        if ($null -eq $Color) { $Color = "white" }
        Write-Host $String -foregroundcolor $Color
        #$String | out-file -Filepath $PS_LOG_FILE -append
    }
}

# ******************************************************************************
# print - Print to the screen
# ******************************************************************************
function Print($String, $Color) {
    if ($null -eq $Color) { $Color = "white" }
    Write-Host $String -foregroundcolor $Color
	   #$String | out-file -Filepath $PS_LOG_FILE -append
}



# ******************************************************************************
# check_build_log
# ******************************************************************************
function Parse_PowerGen_Errors($LogFile, $root) {
    Log "In Parse_PowerGen_Errors"
    $lines = Get-Content $LogFile
	$relpath = Get-Item $LogFile | Resolve-Path -Relative | Split-Path -Parent | ForEach-Object { $_ -replace "\.\\", '' }
	$filename = Split-Path $LogFile -Leaf
    $errors = @()
    $nonTraditionalErrors = @()
    #$buildFailureNonTraditionalErrors = 0
    $buildSuccess = 1

    for ($i = 0; $i -lt $lines.length; $i++) {
        if (([regex]::ismatch($lines[$i], "\): Error")) -or ([regex]::ismatch($lines[$i], "\): Warning"))) {
            Log " In first IF line: $i"
            Log $Lines[$i]
            #$Global:G_Build_Success = 0

            # IF THE LINE ABOVE ME ISN'T AN ERROR OR WARNING 
            if ((-not [regex]::ismatch($lines[$i - 1], "\): Error")) -and (-not [regex]::ismatch($lines[$i - 1], "\): Warning"))) {
                log "In 2nd if"
                $begin = 0
                $end = 0
			
                #LOOP BACKWARDS TO FIND .pbl ENTRY
                for ($j = $i; $j -gt 0; $j--) {
				
                    if ([regex]::ismatch($lines[$j], "([a-z]+)(.pbl)")) {
                        $begin = $j
                        break
                    }
                }
			
                #LOOP FORWARDS TO FIND ALL ERRORS
                for ($k = ($i + 1); $k -lt $lines.length; $k++) {
                    #if ((-not [regex]::ismatch($lines[$k],"\): Error")) -or (-not [regex]::ismatch($lines[$k],"\): Warning")))
                    if ([regex]::ismatch($lines[$k], "\): Warning")) {
                        continue
                    }
                    if (-not [regex]::ismatch($lines[$k], "\): Error")) {		
                        $end += $k
                        break
                    }
                }

                # WRITE OUT ERROR LOG LINES
                for ($l = $begin; $l -lt $end; $l++) {
                    $errors += ($lines[$l] -replace "[^\x09\x0A\x0D\x20-\xD7FF\xE000-\xFFFD\x10000\x10FFFF]", '')
                }
                #$errors += "                 From " + $LogFile
                $errors += "------------------------------"
                $buildSuccess = 0
            }

        }
	
        if ([regex]::ismatch($lines[$i], "Could not open exported object file")) {
            #$Global:G_Build_Success = 0
            #$buildFailureNonTraditionalErrors = 1
		
            $nonTraditionalErrors += $lines[$i]
        }
        if ([regex]::ismatch($lines[$i], "Regeneration error")) {
            #$Global:G_Build_Success = 0
            #$buildFailureNonTraditionalErrors = 1
		
            $nonTraditionalErrors += $lines[$i]
        }
        if ([regex]::ismatch($lines[$i], "0 errors found")) {
            #$Global:G_Build_Success = 1
        }
    } #End Loop
    #$errors | ForEach-Object { print -String $_  -Color "red" }
    # $errors | Out-File "parsed_errors.txt"
    # Log "$BUILD_LOGIC_PATH\working\errors.txt"
    #print -String ($relpath + "\" + $filename + " $buildSuccess")
    $case = $doc.CreateNode("element", "testcase", $null)
    $case.SetAttribute("id", $filename)
    $case.SetAttribute("name", $filename)
    $case.SetAttribute("file", $logFile)
    $case.SetAttribute("classname", $relpath)
    if ($buildSuccess -eq 0) {
        $error.clear()
        try{
        $errorLog = $doc.CreateElement("failure")
        $errorLog.InnerXml = ("Logfile: $LogFile`n" + ($errors -join "`n"))
        $case.AppendChild($errorLog) | Out-Null
        }
        catch {
            Write-Output $LogFile
            Write-Output $error
        }
    }
    else {
        $successLog = $doc.CreateElement("success")
        $case.AppendChild($successLog) | Out-Null
    }
   
    $root.AppendChild($case) | Out-Null
    #Group_Errors -lines $errors -root [ref]$root
}


# ******************************************************************************
# GroupErrors
# ******************************************************************************

function Group_Errors($lines, [ref]$root, $testclass) {
    Log "In Group_Errors"
    $output = @()

    # REMOVE OLD FILES FIRST
    # Remove-Item "$BUILD_LOGIC_PATH\pblEmails\*"

    $begin = 0
    $end = 0
    #$pbl_name = ""

    # PARSE THOUGH TRADITIONAL ERRORS
    for ($i = 0; $i -lt $lines.length; $i++) {	
        # FIND BEGINNING POINT
        if ([regex]::ismatch($lines[$i], "([^\\]+)(.pbl)")) {
            #$pbl_name =  [regex]::match($lines[$i],"([^\\]+)(.pbl)").groups[1]
            $begin = $i
        }
	
        # FIND THE ENDING POINT
        if ([regex]::ismatch($lines[$i], "------------------------------")) {
            $end = $i
            # WRITE OUT BLOCK TO pbl log file
            for ($l = $begin; $l -lt $end; $l++) {
                $output += $lines[$l]
            }
            $output += " "
            $begin = 0
            $end = 0
        }
    }
    return $output
}

main