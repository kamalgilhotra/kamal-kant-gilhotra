Param($LogsPath="",$XmlPath="")

[xml]$Doc = New-Object System.Xml.XmlDocument

#create declaration
$dec = $Doc.CreateXmlDeclaration("1.0","UTF-8",$null)
#append to document
$doc.AppendChild($dec) | Out-Null

#create root Node
$root = $doc.CreateNode("element","testsuites",$null)
$root.SetAttribute("id", (Get-Date).ToString('yyyyMMdd_HHmmss'))

$root.SetAttribute("tests", (Get-ChildItem $LogsPath -recurse | where-object { $_.FullName -imatch 'verbose' } | Where-Object {! $_.PSIsContainer}).Count)

$root.SetAttribute("failures", (Get-ChildItem $LogsPath -recurse | where-object { $_.FullName -inotmatch 'verbose' } | Where-Object {! $_.PSIsContainer}).Count)

foreach ($mainfolder in Get-ChildItem $LogsPath | where-object {$_.PSIsContainer}){
    Write-Host "Adding tests for $mainfolder" -ForegroundColor Green
    foreach ($taskfolder in Get-ChildItem $mainfolder.FullName | where-object {$_.PSIsContainer}) {
        Write-Host "    $taskfolder" -ForegroundColor Green
        $suite = $doc.CreateNode("element","testsuite",$null)
        $suite.SetAttribute("id", $mainfolder.Name + "/" + $taskfolder.Name)
        $suite.SetAttribute("name", $mainfolder.Name + "/" + $taskfolder.Name)
        $suite.SetAttribute("tests", (Get-ChildItem $taskfolder.FullName -recurse | where-object { $_.FullName -imatch 'verbose' } | Where-Object {! $_.PSIsContainer}).Count)
        $suite.SetAttribute("failures", (Get-ChildItem $taskfolder.FullName -recurse | where-object { $_.FullName -inotmatch 'verbose' } | Where-Object {! $_.PSIsContainer}).Count)
        foreach ($logFile in Get-ChildItem $taskfolder.FullName | where-object {! $_.PSIsContainer}) {
            Write-Host "        $logFile" -ForegroundColor Green
            $case = $doc.CreateNode("element","testcase",$null)
            $case.SetAttribute("id", $logFile.Name)
            $case.SetAttribute("name", $logFile.Name)
            $case.SetAttribute("file", $logFile.Fullname)
            $case.SetAttribute("classname", $taskfolder.Name)
            $errorLog = $doc.CreateElement("failure")
            $errorLog.InnerXml = ((Get-Content -Path $logFile.FullName)  -join "`n")
            $case.AppendChild($errorLog) | Out-Null
            $suite.AppendChild($case) | Out-Null
        }
        $root.AppendChild($suite) | Out-Null
    }
}

#add root to the document
$doc.AppendChild($root) | Out-Null

#save file
Write-Host "Saving the XML document to $XmlPath" -ForegroundColor Green

$utf8WithoutBom = New-Object System.Text.UTF8Encoding($false)
$sw = New-Object System.IO.StreamWriter($XmlPath, $false, $utf8WithoutBom)

$doc.save($sw)

Write-Host "Finished!" -ForegroundColor green