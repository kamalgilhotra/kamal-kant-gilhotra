param([string]$path,[string]$version)
Set-Location -literalPath ($path)
$version_commas = $version -replace '^([^.]+)\.([^.]+)\.([^.]+)\.([^.]+)(.+)?','$1,$2,$3,$4'
$srjFiles = Get-ChildItem . *.srj -rec
foreach ($file in $srjFiles)
{
    (Get-Content $file.PSPath) |
    Foreach-Object { $_ -replace '^(PVS|FVS):(.+)?$',"`$1:$version" -replace '^(PVN|FVN):(.+)?$',"`$1:$version_commas" } |
    Set-Content $file.PSPath -encoding utf8
}
