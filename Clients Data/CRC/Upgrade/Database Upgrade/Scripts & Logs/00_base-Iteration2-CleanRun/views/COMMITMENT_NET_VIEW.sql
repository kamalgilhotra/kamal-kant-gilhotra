
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."COMMITMENT_NET_VIEW" ("COMMITMENT_ID", "AMOUNT", "QUANTITY", "HOURS") AS 
  SELECT   commitments.charge_audit_id commitment_id,
      sum(commitments.amount) amount,
      sum(commitments.quantity) quantity,
      sum(commitments.hours) hours
 FROM    commitments
 GROUP BY   commitments.charge_audit_id
 
 
 
 
 
 ;