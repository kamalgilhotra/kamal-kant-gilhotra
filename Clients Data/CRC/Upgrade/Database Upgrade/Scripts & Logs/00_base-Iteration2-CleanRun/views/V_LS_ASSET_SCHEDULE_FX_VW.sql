CREATE OR REPLACE FORCE VIEW V_LS_ASSET_SCHEDULE_FX_VW AS
SELECT 
	las.ilr_id ilr_id,
	las.ls_asset_id ls_asset_id,
	las.revision revision,
	las.set_of_books_id set_of_books_id,
	las.month month,
	open_month.company_id company_id,
	open_month.open_month open_month,
	cur.ls_cur_type ls_cur_type,
	las.month exchange_date,
	Nvl(calc_rate.actual_prev_exchange_date, calc_rate.actual_exchange_date) prev_exchange_date,
	las.contract_currency_id contract_currency_id,
	cur.currency_id currency_id,
	cr.actual_rate rate,
	calc_rate.actual_rate calculated_rate,
	calc_rate.actual_prev_rate previous_calculated_rate,
	calc_rate.average_rate calculated_average_rate,
	calc_rate.average_prev_rate previous_calculated_avg_rate,
	decode(open_month.exchange_rate_type_id, 4, calc_rate.average_rate, calc_rate.actual_rate) average_rate,
	decode(open_month.exchange_rate_type_id, 4, cr.average_rate, cr.actual_rate) cr_average_rate,
	las.effective_in_svc_rate,
	original_in_svc_rate,
	cur.iso_code iso_code,
	cur.currency_display_symbol currency_display_symbol,
	las.residual_amount * nvl(calc_rate.actual_rate, cr.actual_rate) residual_amount,
	las.term_penalty * nvl(calc_rate.actual_rate, cr.actual_rate) term_penalty,
	las.bpo_price * nvl(calc_rate.actual_rate, cr.actual_rate) bpo_price,
	las.beg_capital_cost * case when las.is_om = 0 and las.month = las.first_month then
													nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
												else
													nvl(nvl(cur.contract_approval_rate, las.gross_weighted_avg_rate2), cr.current_rate)
												end beg_capital_cost,
	las.end_capital_cost * nvl(nvl(cur.contract_approval_rate,
																case
																	when las.month < nvl(las.remeasurement_date, las.month) then
																	 las.gross_weighted_avg_rate2
																	else
																	 las.gross_weighted_avg_rate
																end),
														cr.current_rate) end_capital_cost,
	las.beg_obligation * case when las.is_om = 0 and las.month = las.first_month then
												nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
											else
												nvl(calc_rate.actual_prev_rate, cr.actual_rate)
											end beg_obligation,
	las.end_obligation * nvl(calc_rate.actual_rate, cr.actual_rate) end_obligation,
	las.beg_lt_obligation * case when las.is_om = 0 and las.month = las.first_month then
													 nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
												 else
													 nvl(calc_rate.actual_prev_rate, cr.actual_rate)
												 end beg_lt_obligation,
	las.end_lt_obligation * nvl(calc_rate.actual_rate, cr.actual_rate) end_lt_obligation,
	las.principal_remeasurement * nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate) principal_remeasurement,
	las.beg_liability * case when las.is_om = 0 and las.month = las.first_month then
											 nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
										 else
											 nvl(calc_rate.actual_prev_rate, cr.actual_rate)
										 end beg_liability,
	las.end_liability * nvl(calc_rate.actual_rate, cr.actual_rate) end_liability,
	las.beg_lt_liability * case when las.is_om = 0 and las.month = las.first_month then
													nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
												else
													nvl(calc_rate.actual_prev_rate, cr.actual_rate)
												end beg_lt_liability,
	las.end_lt_liability * nvl(calc_rate.actual_rate, cr.actual_rate) end_lt_liability,
	las.liability_remeasurement * nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate) liability_remeasurement,
	las.interest_accrual * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) interest_accrual,
	las.principal_accrual * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) principal_accrual,
	las.interest_paid * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) interest_paid,
	las.principal_paid * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) principal_paid,
	las.executory_accrual1 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual1,
	las.executory_accrual2 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual2,
	las.executory_accrual3 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual3,
	las.executory_accrual4 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual4,
	las.executory_accrual5 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual5,
	las.executory_accrual6 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual6,
	las.executory_accrual7 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual7,
	las.executory_accrual8 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual8,
	las.executory_accrual9 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual9,
	las.executory_accrual10 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual10,
	las.executory_paid1 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid1,
	las.executory_paid2 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid2,
	las.executory_paid3 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid3,
	las.executory_paid4 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid4,
	las.executory_paid5 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid5,
	las.executory_paid6 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid6,
	las.executory_paid7 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid7,
	las.executory_paid8 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid8,
	las.executory_paid9 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid9,
	las.executory_paid10 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid10,
	las.contingent_accrual1 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual1,
	las.contingent_accrual2 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual2,
	las.contingent_accrual3 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual3,
	las.contingent_accrual4 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual4,
	las.contingent_accrual5 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual5,
	las.contingent_accrual6 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual6,
	las.contingent_accrual7 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual7,
	las.contingent_accrual8 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual8,
	las.contingent_accrual9 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual9,
	las.contingent_accrual10 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual10,
	las.contingent_paid1 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid1,
	las.contingent_paid2 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid2,
	las.contingent_paid3 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid3,
	las.contingent_paid4 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid4,
	las.contingent_paid5 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid5,
	las.contingent_paid6 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid6,
	las.contingent_paid7 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid7,
	las.contingent_paid8 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid8,
	las.contingent_paid9 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid9,
	las.contingent_paid10 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid10,
	las.current_lease_cost * nvl(nvl(cur.contract_approval_rate, original_in_svc_rate), cr.current_rate) current_lease_cost,
	las.beg_deferred_rent * nvl(calc_rate.actual_prev_rate, cr.actual_rate) beg_deferred_rent,
	las.deferred_rent * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) deferred_rent,
	las.end_deferred_rent * nvl(calc_rate.actual_rate, cr.actual_rate) end_deferred_rent,
	las.beg_st_deferred_rent * nvl(calc_rate.actual_prev_rate, cr.actual_rate) beg_st_deferred_rent,
	las.end_st_deferred_rent * nvl(calc_rate.actual_rate, cr.actual_rate) end_st_deferred_rent,
	las.initial_direct_cost * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) initial_direct_cost,
	las.incentive_amount *decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) incentive_amount,
	las.depr_expense * nvl(nvl(cur.contract_approval_rate,
														case
															when las.month < nvl(las.remeasurement_date, las.month) then
															 las.net_weighted_avg_rate2
															else
															 las.net_weighted_avg_rate
														end),
												cr.current_rate) depr_expense,
	las.begin_reserve * case when las.is_om = 0 and las.month = las.first_month then
											 nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
										 else
											 nvl(nvl(cur.contract_approval_rate, las.net_weighted_avg_rate2), cr.current_rate)
										 end begin_reserve,
	las.end_reserve * nvl(nvl(cur.contract_approval_rate,
													 case
														 when las.month < nvl(las.remeasurement_date, las.month) then
															las.net_weighted_avg_rate2
														 else
															las.net_weighted_avg_rate
													 end),
											 cr.current_rate) end_reserve,
	las.depr_exp_alloc_adjust * nvl(nvl(cur.contract_approval_rate,
																		 case
																			 when las.month < nvl(las.remeasurement_date, las.month) then
																				las.net_weighted_avg_rate2
																			 else
																				las.net_weighted_avg_rate
																		 end),
																 cr.current_rate) depr_exp_alloc_adjust,
	las.asset_description asset_description,
	las.leased_asset_number leased_asset_number,
	las.fmv * nvl(nvl(cur.contract_approval_rate, original_in_svc_rate), cr.current_rate) fmv,
	las.is_om is_om,
	las.approved_revision approved_revision,
	las.lease_cap_type_id lease_cap_type_id,
	las.ls_asset_status_id ls_asset_status_id,
	las.retirement_date retirement_date,
	las.beg_prepaid_rent * nvl(calc_rate.actual_prev_rate, cr.actual_rate) beg_prepaid_rent,
	las.prepay_amortization * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) prepay_amortization,
	las.prepaid_rent * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) prepaid_rent,
	las.end_prepaid_rent * nvl(calc_rate.actual_rate, cr.actual_rate ) end_prepaid_rent,
	nvl(las.beg_net_rou_asset, 0) * case when las.is_om = 0 and las.month = las.first_month then
																	 nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
																 else
																	 nvl(nvl(cur.contract_approval_rate, las.net_weighted_avg_rate2), cr.current_rate)
																 end beg_net_rou_asset,
	nvl(las.end_net_rou_asset, 0) * nvl(nvl(cur.contract_approval_rate,
																			 case
																				 when las.month < nvl(las.remeasurement_date, las.month) then
																					las.net_weighted_avg_rate2
																				 else
																					las.net_weighted_avg_rate
																			 end),
																	 cr.current_rate) end_net_rou_asset,
	las.actual_residual_amount * nvl(calc_rate.actual_rate, cr.actual_rate) actual_residual_amount,
	las.guaranteed_residual_amount * nvl(calc_rate.actual_rate, cr.actual_rate) guaranteed_residual_amount,
	(las.fmv * las.estimated_residual) * nvl(calc_rate.actual_rate, cr.actual_rate) estimated_residual,
	nvl(las.rou_asset_remeasurement, 0) * nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate) rou_asset_remeasurement,
	nvl(las.partial_term_gain_loss, 0) * nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate) partial_term_gain_loss,
	las.beg_arrears_accrual * nvl(calc_rate.actual_prev_rate, cr.actual_rate) beg_arrears_accrual,
	las.arrears_accrual * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) arrears_accrual,
	las.end_arrears_accrual * nvl(calc_rate.actual_rate, cr.actual_rate) end_arrears_accrual,
	las.additional_rou_asset * nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate) additional_rou_asset,
	nvl(las.executory_adjust, 0) * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_adjust,
	nvl(las.contingent_adjust, 0) * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_adjust,
	case
	when las.is_om = 1 then
	 0
	when las.contract_currency_id = cs.currency_id then
	 0
	else
	 (
		 ((las.end_liability * nvl(calc_rate.actual_rate, cr.actual_rate)) - (las.end_lt_liability * nvl(calc_rate.actual_rate, cr.actual_rate)))
		 - ((las.beg_liability * case when las.is_om = 0 and las.month = las.first_month then
											 nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
										 else
											 nvl(calc_rate.actual_prev_rate, cr.actual_rate)
										 end)
				- (las.beg_lt_liability * case when las.is_om = 0 and las.month = las.first_month then
											 nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
										 else
											 nvl(calc_rate.actual_prev_rate, cr.actual_rate)
										 end))
		 - (las.interest_accrual * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)))
		 + (las.interest_paid * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)))
		 + (las.principal_paid * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)))
		 - (las.obligation_reclass * nvl(calc_rate.actual_rate, cr.actual_rate))
		 - (las.st_obligation_remeasurement * nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate))
		 + (las.unaccrued_interest_reclass * nvl(calc_rate.actual_rate, cr.actual_rate))
		 + (las.unaccrued_interest_remeasure * nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate))
	 )
	end st_currency_gain_loss,
	case 
		when las.is_om = 1 then 0
		when las.contract_currency_id = cs.currency_id then 0
		else
	  (
       (las.end_lt_liability * nvl(calc_rate.actual_rate, cr.actual_rate))
       - (las.beg_lt_liability * case when las.is_om = 0 and las.month = las.first_month then
                                   nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
                                 else
                                   nvl(calc_rate.actual_prev_rate, cr.actual_rate)
                                 end)
       + ((-(las.lt_obligation_remeasurement + las.unaccrued_interest_remeasure) * nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)) + ((las.obligation_reclass - las.unaccrued_interest_reclass) * nvl(calc_rate.actual_rate, cr.actual_rate)))
      )
	end lt_currency_gain_loss,
	case
	when las.is_om = 1 then
	 0
	when las.contract_currency_id = cs.currency_id then
	 0
	else
		(
		 ((las.end_liability * nvl(calc_rate.actual_rate, cr.actual_rate)) - (las.end_lt_liability * nvl(calc_rate.actual_rate, cr.actual_rate)))
		 - ((las.beg_liability * case when las.is_om = 0 and las.month = las.first_month then
															 nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
														 else
															 nvl(calc_rate.actual_prev_rate, cr.actual_rate)
														 end)
				- (las.beg_lt_liability * case when las.is_om = 0 and las.month = las.first_month then
																		nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
																	else
																		nvl(calc_rate.actual_prev_rate, cr.actual_rate)
																	end))
		 - (las.interest_accrual * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)))
		 + (las.interest_paid * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)))
		 + (las.principal_paid * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)))
		 - (las.obligation_reclass * nvl(calc_rate.actual_rate, cr.actual_rate))
		 - (las.st_obligation_remeasurement * nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate))
		 + (las.unaccrued_interest_reclass * nvl(calc_rate.actual_rate, cr.actual_rate))
		 + (las.unaccrued_interest_remeasure * nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate))
	 ) +
     (
       (las.end_lt_liability * nvl(calc_rate.actual_rate, cr.actual_rate))
       - (las.beg_lt_liability * case when las.is_om = 0 and las.month = las.first_month then
                                   nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)
                                 else
                                   nvl(calc_rate.actual_prev_rate, cr.actual_rate)
                                 end)
       + ((-(las.lt_obligation_remeasurement + las.unaccrued_interest_remeasure) * nvl(nvl(cur.contract_approval_rate, las.effective_in_svc_rate), cr.current_rate)) + ((las.obligation_reclass - las.unaccrued_interest_reclass) * nvl(calc_rate.actual_rate, cr.actual_rate)))
     )
	end gain_loss_fx,
	las.obligation_reclass * nvl(calc_rate.actual_rate, cr.actual_rate) obligation_reclass,
	las.unaccrued_interest_reclass * nvl(calc_rate.actual_rate, cr.actual_rate) unaccrued_interest_reclass,
	las.begin_accum_impair * nvl(nvl(cur.contract_approval_rate, las.net_weighted_avg_rate2), cr.current_rate) begin_accum_impair,
	las.impairment_activity * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) impairment_activity,
	las.end_accum_impair * nvl(nvl(cur.contract_approval_rate,
										 case
											 when las.month < nvl(las.remeasurement_date, las.month) then
											las.net_weighted_avg_rate2
											 else
											las.net_weighted_avg_rate
										 end),
									 cr.current_rate) end_accum_impair
	FROM
	(
		SELECT
			la.ilr_id
			, las.ls_asset_id
			, las.revision
			, las.set_of_books_id
			, las.month
			, la.contract_currency_id
			, las.residual_amount
			, las.term_penalty
			, las.bpo_price
			, las.beg_capital_cost
			, las.end_capital_cost
			, las.beg_obligation
			, las.end_obligation
			, las.beg_lt_obligation
			, las.end_lt_obligation
			, las.principal_remeasurement
			, las.beg_liability
			, las.end_liability
			, las.beg_lt_liability
			, las.end_lt_liability
			, las.liability_remeasurement
			, las.interest_accrual
			, las.principal_accrual
			, las.interest_paid
			, las.principal_paid
			, las.executory_accrual1
			, las.executory_accrual2
			, las.executory_accrual3
			, las.executory_accrual4
			, las.executory_accrual5
			, las.executory_accrual6
			, las.executory_accrual7
			, las.executory_accrual8
			, las.executory_accrual9
			, las.executory_accrual10
			, las.executory_paid1
			, las.executory_paid2
			, las.executory_paid3
			, las.executory_paid4
			, las.executory_paid5
			, las.executory_paid6
			, las.executory_paid7
			, las.executory_paid8
			, las.executory_paid9
			, las.executory_paid10
			, las.contingent_accrual1
			, las.contingent_accrual2
			, las.contingent_accrual3
			, las.contingent_accrual4
			, las.contingent_accrual5
			, las.contingent_accrual6
			, las.contingent_accrual7
			, las.contingent_accrual8
			, las.contingent_accrual9
			, las.contingent_accrual10
			, las.contingent_paid1
			, las.contingent_paid2
			, las.contingent_paid3
			, las.contingent_paid4
			, las.contingent_paid5
			, las.contingent_paid6
			, las.contingent_paid7
			, las.contingent_paid8
			, las.contingent_paid9
			, las.contingent_paid10
			, las.current_lease_cost
			, las.beg_deferred_rent
			, las.deferred_rent
			, las.end_deferred_rent
			, las.beg_st_deferred_rent
			, las.end_st_deferred_rent
			, las.initial_direct_cost
			, las.incentive_amount
			, ldf.depr_expense
			, ldf.begin_reserve
			, ldf.end_reserve
			, ldf.depr_exp_alloc_adjust
			, la.company_id
			, opt.in_service_exchange_rate
			, la.description AS asset_description
			, la.leased_asset_number
			, la.fmv
			, las.is_om
			, la.approved_revision
			, opt.lease_cap_type_id
			, la.ls_asset_status_id
			, la.retirement_date
			, las.beg_prepaid_rent
			, las.prepay_amortization
			, las.prepaid_rent
			, las.end_prepaid_rent
			, las.beg_net_rou_asset
			, las.end_net_rou_asset
			, la.actual_residual_amount
			, la.guaranteed_residual_amount
			, la.estimated_residual
			, las.rou_asset_remeasurement
			, las.partial_term_gain_loss
			, las.beg_arrears_accrual
			, las.arrears_accrual
			, las.end_arrears_accrual
			, las.additional_rou_asset
			, las.executory_adjust
			, las.contingent_adjust
			, opt.in_service_exchange_rate AS effective_in_svc_rate	-- #JAS changed this to opt to remove the CTE
			, NVL(las.obligation_reclass, 0) obligation_reclass
			, NVL(las.st_obligation_remeasurement, 0) st_obligation_remeasurement
			, NVL(las.lt_obligation_remeasurement, 0) lt_obligation_remeasurement
			, NVL(las.unaccrued_interest_reclass, 0) unaccrued_interest_reclass
			, NVL(las.unaccrued_interest_remeasure, 0) unaccrued_interest_remeasure
			, NVL(las.begin_accum_impair,0) begin_accum_impair
			, NVL(las.impairment_activity,0) impairment_activity
			, NVL(las.end_accum_impair,0) end_accum_impair
			, orig_in_svc.in_service_exchange_rate original_in_svc_rate
			, opt.remeasurement_date remeasurement_date
			, all_war.gross_weighted_avg_rate
			, all_war.net_weighted_avg_rate
			, CASE
				WHEN las.month <= NVL(opt.remeasurement_date, las.month) THEN all_war.prior_gross_weighted_avg_rate
				ELSE all_war.gross_weighted_avg_rate
			END gross_weighted_avg_rate2
			, CASE
				WHEN las.month <= NVL(opt.remeasurement_date, las.month) THEN all_war.prior_net_weighted_avg_rate
				ELSE all_war.net_weighted_avg_rate
			END net_weighted_avg_rate2
			, TRUNC(las.month, 'MONTH') trunc_month
			, min_on_bs.first_month
		FROM ls_asset_schedule las
		JOIN ls_asset la
			ON las.ls_asset_id = la.ls_asset_id
		JOIN ls_ilr_options opt
			ON opt.ilr_id = la.ilr_id
			AND opt.revision = las.revision
		LEFT JOIN -- ##JAS changing all_war to a subquery from a CTE. Join based on a range of months, not between. Exclude records where effective_month = next_month meaning there was another revision in the same month. AS we are only looking at the month range, we don't need to join on revision.
		(
			SELECT
				war.ilr_id
				, opt.revision
				, war.set_of_books_id
				, war.effective_month
				, NVL(LEAD(war.effective_month) OVER (PARTITION BY war.ilr_id, war.set_of_books_id, opt.revision ORDER BY war.effective_month), TO_DATE('999912', 'YYYYMM')) next_effective_month
				, war.gross_weighted_avg_rate
				, NVL(LAG(war.gross_weighted_avg_rate) OVER (PARTITION BY war.ilr_id, war.set_of_books_id, opt.revision ORDER BY war.effective_month), war.gross_weighted_avg_rate) prior_gross_weighted_avg_rate
				, war.net_weighted_avg_rate
				, NVL(LAG(war.net_weighted_avg_rate) OVER (PARTITION BY war.ilr_id, war.set_of_books_id, opt.revision ORDER BY war.effective_month), war.net_weighted_avg_rate) prior_net_weighted_avg_rate
			FROM
			(
				SELECT
					ilr.ilr_id
					, lio.revision
					, sob.set_of_books_id
					, TRUNC(NVL(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month
					, ROW_NUMBER() OVER(PARTITION BY ilr.ilr_id, sob.set_of_books_id, TRUNC(NVL(lio.remeasurement_date, ilr.est_in_svc_date), 'month') ORDER BY DECODE(lia.approval_status_id, 3, 0, lia.approval_status_id), lia.approval_date DESC) rev_rnk
					, Nvl(war.gross_weighted_avg_rate, lio.in_service_exchange_rate) gross_weighted_avg_rate
					, Nvl(war.net_weighted_avg_rate, lio.in_service_exchange_rate) net_weighted_avg_rate
				FROM ls_ilr ilr
				JOIN ls_ilr_options lio
					ON lio.ilr_id = ilr.ilr_id
				CROSS JOIN set_of_books sob
				JOIN ls_ilr_approval lia
				ON lia.ilr_id = ilr.ilr_id
					AND lia.revision = lio.revision
				LEFT JOIN ls_ilr_weighted_avg_rates war
					ON war.ilr_id = ilr.ilr_id
					AND war.revision = lio.revision
					AND war.set_of_books_id = sob.set_of_books_id
				WHERE lio.revision > 0
					AND lia.approval_date IS NOT NULL
			) war
			JOIN ls_ilr_options opt -- #JAS based on FKs it looks like this is the best option to join to and get Revision back
				ON opt.ilr_id = war.ilr_id
			WHERE rev_rnk = 1
		) all_war
			ON all_war.ilr_id = la.ilr_id
			AND all_war.revision = las.revision
			AND all_war.set_of_books_id = las.set_of_books_id
			AND all_war.effective_month <= las.MONTH
			AND all_war.next_effective_month > las.MONTH
		LEFT JOIN ls_depr_forecast ldf ON
			las.ls_asset_id = ldf.ls_asset_id
			AND las.revision = ldf.revision
			AND las.set_of_books_id = ldf.set_of_books_id
			AND las.month = ldf.month
		LEFT JOIN
		(
			SELECT
				lio.ilr_id
				, lio.in_service_exchange_rate
			FROM ls_ilr_options lio
			WHERE (ilr_id, revision) IN
			(
				SELECT
					ilr_id
					, First_Value(revision) OVER (PARTITION BY ilr_id ORDER BY approval_date) first_rev
				FROM ls_ilr_approval
				WHERE approval_status_id NOT IN (1,4,5,7)
			)
		) orig_in_svc
			ON orig_in_svc.ilr_id = la.ilr_id
		LEFT JOIN -- ##JAS this subquery works much better if we force it to use the PK index. Move inside the LAS subquery
		(
			SELECT -- ##JAS removed index hint, after adding the new OM index the optimizer should use it by default
				ls_asset_id
				, revision
				, set_of_books_id
				, Min(month) first_month
			FROM ls_asset_schedule
			WHERE is_om = 0
			GROUP BY 
				ls_asset_id
				, revision
				, set_of_books_id
		) min_on_bs
			ON min_on_bs.ls_asset_id = las.ls_asset_id
			AND min_on_bs.revision = las.revision
			AND min_on_bs.set_of_books_id = las.set_of_books_id 
	) las
	JOIN currency_schema cs
		ON las.company_id = cs.company_id
		AND cs.currency_type_id = 1
	JOIN
	(
		SELECT 
			ls_currency_type_id AS ls_cur_type
			, currency_id
			, currency_display_symbol
			, iso_code
			, CASE ls_currency_type_id WHEN 1 THEN 1 ELSE NULL END AS contract_approval_rate
		FROM currency
		CROSS JOIN ls_lease_currency_type
	)	cur 
		ON cur.currency_id = CASE cur.ls_cur_type
			WHEN 1 THEN las.contract_currency_id
			WHEN 2 THEN cs.currency_id
		END
	JOIN
	(
		SELECT 
			ls_process_control.company_id
			, DECODE( lower(sc.control_value), 'yes', 4, 1) exchange_rate_type_id
			, Min(gl_posting_mo_yr) open_month
		FROM ls_process_control
		JOIN pp_system_control_companies sc 
			ON ls_process_control.company_id = sc.company_id
		WHERE ls_process_control.open_next IS NULL
			AND LOWER(TRIM(sc.control_name)) = 'lease mc: use average rates'
		GROUP BY 
			ls_process_control.company_id
			, DECODE(LOWER(sc.control_value), 'yes', 4, 1)
	) open_month
		ON las.company_id = open_month.company_id
	JOIN currency_rate_default_vw cr 
		ON cr.currency_from = las.contract_currency_id
		AND cr.currency_to = cur.currency_id
		AND trunc(cr.exchange_date, 'MONTH') <= las.trunc_month
		AND trunc(cr.next_date, 'MONTH') > las.trunc_month
	LEFT JOIN
	(
		SELECT * FROM
		(
			SELECT
				company_id
				, contract_currency_id
				, company_currency_id
				, exchange_rate_type_id
				, accounting_month
				, exchange_date
				, rate
				, Lag(exchange_date) OVER (PARTITION BY company_id, contract_currency_id, company_currency_id, exchange_rate_type_id ORDER BY accounting_month) prev_exchange_date
				, Lag(rate) OVER (PARTITION BY company_id, contract_currency_id, company_currency_id, exchange_rate_type_id ORDER BY accounting_month) prev_rate
			FROM
			(			
				SELECT company_id, contract_currency_id, company_currency_id, exchange_rate_type_id, accounting_month, exchange_date, rate 
				FROM ls_lease_calculated_date_rates
				UNION ALL
				SELECT company_id, contract_currency_id, company_currency_id, exchange_rate_type_id, Add_Months(Max(accounting_month),1) accounting_month, NULL exchange_date, NULL rate 
				FROM ls_lease_calculated_date_rates
				GROUP BY company_id, contract_currency_id, company_currency_id, exchange_rate_type_id
			)
		)
		PIVOT
		(
			Max(exchange_date) AS exchange_date
			, max(rate) AS rate
			, max(prev_exchange_date) AS prev_exchange_date
			, max(prev_rate) AS prev_rate
			FOR exchange_rate_type_id IN (1 AS Actual, 4 AS Average)
		)
	) calc_rate
		ON las.contract_currency_id = calc_rate.contract_currency_id
		AND cur.currency_id = calc_rate.company_currency_id
		AND las.company_id = calc_rate.company_id
		AND las.trunc_month = calc_rate.accounting_month;