
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_EST_MONTH_IS_EDITABLE_FY" ("WORK_ORDER_NUMBER", "WORK_ORDER_ID", "REVISION", "YEAR", "JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER", "REVISION_EDIT", "ACTUALS_MONTH_NUMBER") AS 
  select
   a.work_order_number, a.work_order_id, a.revision, to_number(a.year) year,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'01')),1,0,decode(sign(to_number(a.year||'01') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'01') - actuals_month_number),1,1,0))))))) january,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'02')),1,0,decode(sign(to_number(a.year||'02') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'02') - actuals_month_number),1,1,0))))))) february,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'03')),1,0,decode(sign(to_number(a.year||'03') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'03') - actuals_month_number),1,1,0))))))) march,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'04')),1,0,decode(sign(to_number(a.year||'04') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'04') - actuals_month_number),1,1,0))))))) april,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'05')),1,0,decode(sign(to_number(a.year||'05') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'05') - actuals_month_number),1,1,0))))))) may,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'06')),1,0,decode(sign(to_number(a.year||'06') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'06') - actuals_month_number),1,1,0))))))) june,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'07')),1,0,decode(sign(to_number(a.year||'07') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'07') - actuals_month_number),1,1,0))))))) july,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'08')),1,0,decode(sign(to_number(a.year||'08') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'08') - actuals_month_number),1,1,0))))))) august,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'09')),1,0,decode(sign(to_number(a.year||'09') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'09') - actuals_month_number),1,1,0))))))) september,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'10')),1,0,decode(sign(to_number(a.year||'10') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'10') - actuals_month_number),1,1,0))))))) october,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'11')),1,0,decode(sign(to_number(a.year||'11') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'11') - actuals_month_number),1,1,0))))))) november,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'12')),1,0,decode(sign(to_number(a.year||'12') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'12') - actuals_month_number),1,1,0))))))) december,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,1)))) revision_edit, actuals_month_number
from (
   select woc.funding_wo_indicator,
      woc.work_order_number,
      woa.work_order_id,
      woa.revision,
      pp.year,
      (select decode(count(*),0,0,1) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and fp.active = 1 and open_for_entry = 0) not_open_for_entry,
      (select decode(count(*),0,0,1) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and fp.active = 1 and bring_in_subs = 1) subs_only,
      (select fiscal_year * 100 + fiscal_month from pp_calendar where month_number = to_number(to_char(woa.est_start_date, 'yyyymm'))) start_month_number,
      (select fiscal_year * 100 + fiscal_month from pp_calendar where month_number = to_number(to_char(woa.est_complete_date, 'yyyymm'))) end_month_number,
      nvl((select fiscal_year * 100 + fiscal_month from pp_calendar where month_number = greatest(nvl(woa.actuals_month_number,0),(select nvl(max(nvl(actuals_month,0)),0) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and active = 1))),0) actuals_month_number,
      (
         select decode(woc.funding_wo_indicator,1,
            decode(lower(trim(nvl(max(control_value),'no'))),'no',
               decode(woa.approval_status_id,2,1,3,1,5,1,0),
               0),
            decode(woa.approval_status_id,2,1,3,1,5,1,0))
         from pp_system_control_companies sc
         where sc.company_id = woc.company_id
         and lower(trim(sc.control_name)) = lower(trim('FUNDPROJ - Edit Approved FP'))
      ) approval_status_id,
      (
         select decode(lower(trim(nvl(max(control_value),'yes'))),'yes',
            decode(woa.review_status,2,1,3,1,0),
            0)
         from pp_system_control_companies sc
         where sc.company_id = woc.company_id
         and lower(trim(sc.control_name)) = lower(trim('FP EST - Budget Review Locks Ests'))
      ) review_status_id
   from work_order_control woc, work_order_approval woa, pp_table_years pp, temp_work_order two
   where woc.work_order_id = woa.work_order_id
   and woc.work_order_id = two.work_order_id
   ) a
 
 ;