
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TEMPLATE_ASSET_DOL_VIEW" ("ASSET_ID", "SUBLEDGER_ITEM_ID", "SUBLEDGER_TYPE_ID", "ASSET_DOLLARS", "SALVAGE_DOLLARS", "GAIN_LOSS", "DEPR_RESERVE", "REMAINING_LIFE", "CURR_DEPR_EXPENSE") AS 
  SELECT DISTINCT "SUBLEDGER_TEMPLATE"."ASSET_ID",
                "SUBLEDGER_TEMPLATE"."SUBLEDGER_ITEM_ID",
                "SUBLEDGER_TEMPLATE"."SUBLEDGER_TYPE_ID",
                "TEMPLATE_DEPR"."ASSET_DOLLARS",
                "TEMPLATE_DEPR"."SALVAGE_DOLLARS",
                "TEMPLATE_DEPR"."GAIN_LOSS",
                "TEMPLATE_DEPR"."DEPR_RESERVE",
                "TEMPLATE_DEPR"."REMAINING_LIFE",
                "TEMPLATE_DEPR"."CURR_DEPR_EXPENSE"
  FROM "SUBLEDGER_TEMPLATE",
       "TEMPLATE_DEPR",
       cpr_ledger,
       (SELECT MAX(accounting_month) max_month, company_id
          FROM cpr_control
         WHERE cpr_closed IS NOT NULL
           AND company_id IN (SELECT company_id FROM company)
         GROUP BY company_id) cpr_mo
 WHERE "SUBLEDGER_TEMPLATE"."SUBLEDGER_ITEM_ID" = "TEMPLATE_DEPR"."SUBLEDGER_ITEM_ID"
   AND "SUBLEDGER_TEMPLATE"."ASSET_ID" = "TEMPLATE_DEPR"."ASSET_ID"
   AND "TEMPLATE_DEPR"."CPR_POSTING_MO_YR" = max_month
   AND subledger_template.asset_id = cpr_ledger.asset_id
UNION (SELECT DISTINCT "SUBLEDGER_TEMPLATE"."ASSET_ID",
                       "SUBLEDGER_TEMPLATE"."SUBLEDGER_ITEM_ID",
                       "SUBLEDGER_TEMPLATE"."SUBLEDGER_TYPE_ID",
                       0 "ASSET_DOLLARS",
                       0 "SALVAGE_DOLLARS",
                       0 "GAIN_LOSS",
                       0 "DEPR_RESERVE",
                       0 "REMAINING_LIFE",
                       0 "CURR_DEPR_EXPENSE"
         FROM "SUBLEDGER_TEMPLATE"
       MINUS (SELECT DISTINCT "SUBLEDGER_TEMPLATE"."ASSET_ID",
                             "SUBLEDGER_TEMPLATE"."SUBLEDGER_ITEM_ID",
                             "SUBLEDGER_TEMPLATE"."SUBLEDGER_TYPE_ID",
                             0 "ASSET_DOLLARS",
                             0 "SALVAGE_DOLLARS",
                             0 "GAIN_LOSS",
                             0 "DEPR_RESERVE",
                             0 "REMAINING_LIFE",
                             0 "CURR_DEPR_EXPENSE"
               FROM "SUBLEDGER_TEMPLATE",
                    "TEMPLATE_DEPR",
                    cpr_ledger,
                    (SELECT MAX(accounting_month) max_month, company_id
                       FROM cpr_control
                      WHERE cpr_closed IS NOT NULL
                        AND company_id IN (SELECT company_id FROM company)
                      GROUP BY company_id) cpr_mo
              WHERE "SUBLEDGER_TEMPLATE"."SUBLEDGER_ITEM_ID" = "TEMPLATE_DEPR"."SUBLEDGER_ITEM_ID"
                AND "SUBLEDGER_TEMPLATE"."ASSET_ID" = "TEMPLATE_DEPR"."ASSET_ID"
                AND "TEMPLATE_DEPR"."CPR_POSTING_MO_YR" = max_month
                AND subledger_template.asset_id = cpr_ledger.asset_id))
 
 
 
 
 
 ;