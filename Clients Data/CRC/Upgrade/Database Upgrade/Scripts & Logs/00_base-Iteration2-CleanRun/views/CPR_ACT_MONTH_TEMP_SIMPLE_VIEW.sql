
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CPR_ACT_MONTH_TEMP_SIMPLE_VIEW" ("ASSET_ID", "ACCUM_QUANTITY", "ACCUM_COST", "GL_POSTING_MO_YR") AS 
  SELECT "CPR_ACTIVITY"."ASSET_ID",
       SUM("CPR_ACTIVITY"."ACTIVITY_QUANTITY") accum_quantity,
       SUM("CPR_ACTIVITY"."ACTIVITY_COST") accum_cost,
       "CPR_ACTIVITY"."GL_POSTING_MO_YR"
  FROM "CPR_ACTIVITY"
 WHERE "CPR_ACTIVITY"."ASSET_ID" IN
       (SELECT "ASSET_ID"
          FROM "TEMP_ASSET"
         WHERE upper("TEMP_ASSET"."USER_ID") = USER
           AND "TEMP_ASSET"."SESSION_ID" = userenv('sessionid'))
   AND "CPR_ACTIVITY"."GL_POSTING_MO_YR" >=
       (SELECT "CPR_ACT_MONTH"."MONTH"
          FROM "CPR_ACT_MONTH"
         WHERE upper("CPR_ACT_MONTH"."USER_ID") = USER
           AND "CPR_ACT_MONTH"."SESSION_ID" = userenv('sessionid'))
 GROUP BY "CPR_ACTIVITY"."ASSET_ID",
          "CPR_ACTIVITY"."GL_POSTING_MO_YR"
 
 
 
 
 
 ;