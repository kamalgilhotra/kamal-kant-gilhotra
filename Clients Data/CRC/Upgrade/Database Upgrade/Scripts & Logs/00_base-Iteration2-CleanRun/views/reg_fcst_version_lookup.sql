
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."REG_FCST_VERSION_LOOKUP" ("VERSION_ID", "DESCRIPTION", "LONG_DESCRIPTION") AS 
  SELECT forecast_version_id version_id, description, long_description FROM reg_forecast_version
 
 ;