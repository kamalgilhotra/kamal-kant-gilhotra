
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CHARGE_SUMMARY" ("COMPANY_ID", "WORK_ORDER_ID", "EXPENDITURE_TYPE_ID", "MONTH", "MONTH_NUMBER", "CURRENT_CHARGES", "CLOSINGS", "TOTAL_COST", "IN_PROCESS_BALANCE") AS 
  select CC.COMPANY_ID,
       CC.WORK_ORDER_ID,
       CC.EXPENDITURE_TYPE_ID,
       CC.MONTH,
       TO_NUMBER(TO_CHAR(CC.MONTH, 'yyyymm')) MONTH_NUMBER,
       sum(CC.CURRENT_CHARGES) CURRENT_CHARGES,
       sum(CC.CLOSINGS) CLOSINGS,
       sum(CC.TOTAL_COST) TOTAL_COST,
       sum(case
              when WO_PROCESS_CONTROL.POWERPLANT_CLOSED is null then
               CC.CWIP_107_108
              else
               CC.WO_GL_107_108
           end) IN_PROCESS_BALANCE
  from WO_PROCESS_CONTROL,
       (select CWIP_CHARGE.COMPANY_ID,
               CWIP_CHARGE.WORK_ORDER_ID,
               CWIP_CHARGE.EXPENDITURE_TYPE_ID,
               WO_PROCESS_CONTROL.ACCOUNTING_MONTH month,
               sum(case
                      when CWIP_CHARGE.MONTH_NUMBER =
                           TO_NUMBER(TO_CHAR(WO_PROCESS_CONTROL.ACCOUNTING_MONTH, 'yyyymm')) then
                       AMOUNT
                      else
                       0
                   end) CURRENT_CHARGES,
               sum(case
                      when CWIP_CHARGE.MONTH_NUMBER <=
                           TO_NUMBER(TO_CHAR(WO_PROCESS_CONTROL.ACCOUNTING_MONTH, 'yyyymm')) then
                       AMOUNT
                      else
                       0
                   end) TOTAL_COST,
               sum(case
                      when CWIP_CHARGE.CLOSED_MONTH_NUMBER =
                           TO_NUMBER(TO_CHAR(WO_PROCESS_CONTROL.ACCOUNTING_MONTH, 'yyyymm')) then
                       AMOUNT
                      else
                       0
                   end) CLOSINGS,
               sum(case
                      when CWIP_CHARGE.GL_ACCOUNT_ID = WORK_ORDER_ACCOUNT.CWIP_GL_ACCOUNT or
                           CWIP_CHARGE.GL_ACCOUNT_ID = 1 or
                           CWIP_CHARGE.GL_ACCOUNT_ID = REMOVAL_GL_ACCOUNT or
                           CWIP_CHARGE.GL_ACCOUNT_ID = SALVAGE_GL_ACCOUNT then
                       AMOUNT
                      else
                       0
                   end) CWIP_107_108,
               0 WO_GL_107_108
          from CWIP_CHARGE, WO_PROCESS_CONTROL, WORK_ORDER_ACCOUNT
         where CWIP_CHARGE.COMPANY_ID = WO_PROCESS_CONTROL.COMPANY_ID
           and CWIP_CHARGE.WORK_ORDER_ID = WORK_ORDER_ACCOUNT.WORK_ORDER_ID
           and CWIP_CHARGE.CHARGE_TYPE_ID in
               (select CHARGE_TYPE_ID from CHARGE_TYPE where PROCESSING_TYPE_ID not in (1, 5))
           and CWIP_CHARGE.MONTH_NUMBER <=
               (select TO_NUMBER(TO_CHAR(max(ACCOUNTING_MONTH), 'yyyymm'))
                  from WO_PROCESS_CONTROL
                 where WO_PROCESS_CONTROL.COMPANY_ID = CWIP_CHARGE.COMPANY_ID)
         group by CWIP_CHARGE.COMPANY_ID,
                  CWIP_CHARGE.WORK_ORDER_ID,
                  CWIP_CHARGE.EXPENDITURE_TYPE_ID,
                  WO_PROCESS_CONTROL.ACCOUNTING_MONTH
        union
        select WO_GL_ACCOUNT_SUMMARY.COMPANY_ID,
               WO_GL_ACCOUNT_SUMMARY.WORK_ORDER_ID,
               1 EXPENDITURE_TYPE_ID,
               WO_PROCESS_CONTROL.ACCOUNTING_MONTH month,
               0 CURRENT_CHARGES,
               0 TOTAL_COST,
               0 CLOSINGS,
               0 CWIP_107_108,
               sum(BOOK_AMOUNT) WO_GL_107_108
          from WO_GL_ACCOUNT_SUMMARY, WO_PROCESS_CONTROL, WORK_ORDER_ACCOUNT
         where WO_GL_ACCOUNT_SUMMARY.COMPANY_ID = WO_PROCESS_CONTROL.COMPANY_ID
           and WO_GL_ACCOUNT_SUMMARY.WORK_ORDER_ID = WORK_ORDER_ACCOUNT.WORK_ORDER_ID
           and WO_GL_ACCOUNT_SUMMARY.MONTH_NUMBER =
               TO_NUMBER(TO_CHAR(WO_PROCESS_CONTROL.ACCOUNTING_MONTH, 'yyyymm'))
           and (WO_GL_ACCOUNT_SUMMARY.GL_ACCOUNT_ID = WORK_ORDER_ACCOUNT.CWIP_GL_ACCOUNT or
               WO_GL_ACCOUNT_SUMMARY.GL_ACCOUNT_ID = 1 or
               WO_GL_ACCOUNT_SUMMARY.GL_ACCOUNT_ID = REMOVAL_GL_ACCOUNT or
               WO_GL_ACCOUNT_SUMMARY.GL_ACCOUNT_ID = SALVAGE_GL_ACCOUNT)
         group by WO_GL_ACCOUNT_SUMMARY.COMPANY_ID,
                  WO_GL_ACCOUNT_SUMMARY.WORK_ORDER_ID,
                  WO_PROCESS_CONTROL.ACCOUNTING_MONTH) CC
 where CC.COMPANY_ID = WO_PROCESS_CONTROL.COMPANY_ID
   and CC.MONTH = WO_PROCESS_CONTROL.ACCOUNTING_MONTH
 group by CC.COMPANY_ID, WORK_ORDER_ID, EXPENDITURE_TYPE_ID, MONTH
 
 
 
 ;