
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_TOTAL_COST_W_FUND_VIEW" ("WORK_ORDER_ID", "TOTAL_COST", "IN_PROCESS_BALANCE") AS 
  SELECT work_order_control.WORK_ORDER_ID,          SUM(nvl(TOTAL_COST, 0)) "TOTAL_COST",           SUM(nvl(IN_PROCESS_BALANCE, 0)) "IN_PROCESS_BALANCE"             FROM work_order_control, CHARGE_SUMMARY           WHERE (MONTH = (select max(accounting_month) from wo_process_control           where powerplant_closed is null) or month is null) and           work_order_control.work_order_id = charge_summary.work_order_id (+) and  	          work_order_control.funding_wo_indicator <> 1            GROUP BY work_order_control.WORK_ORDER_ID            union               SELECT a.WORK_ORDER_ID,            sum(nvl(total_cost, 0)),           sum(nvl(in_process_balance, 0))          FROM CHARGE_SUMMARY,  WORK_ORDER_CONTROL a, WORK_ORDER_CONTROL b            WHERE ( a.work_order_id = b.funding_wo_id (+)) and           ( b.work_order_id = charge_summary.work_order_id (+)) and           ( ( a.FUNDING_WO_INDICATOR = 1 ) AND                (MONTH = (select max(accounting_month) from wo_process_control           where powerplant_closed is null) or month is null) )            GROUP BY a.WORK_ORDER_ID
 
 
 
 
 
 ;