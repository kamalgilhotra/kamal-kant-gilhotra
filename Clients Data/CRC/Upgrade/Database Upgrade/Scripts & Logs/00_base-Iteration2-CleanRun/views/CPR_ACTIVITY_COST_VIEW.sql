
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CPR_ACTIVITY_COST_VIEW" ("ASSET_ID", "ACTIVITY_COST") AS 
  SELECT "CPR_LEDGER"."ASSET_ID",
       SUM(nvl("CPR_ACTIVITY"."ACTIVITY_COST",
               0)) activity_cost
  FROM "REPORT_TIME",
       "CPR_LEDGER",
       "CPR_ACTIVITY"
 WHERE (to_number(to_char("CPR_ACTIVITY"."GL_POSTING_MO_YR",
                          'J')) >
       to_number(to_char("REPORT_TIME"."START_MONTH",
                          'J')))
   AND ("CPR_LEDGER"."ASSET_ID" = "CPR_ACTIVITY"."ASSET_ID")
   AND (upper("REPORT_TIME"."USER_ID") = USER)
   AND ("REPORT_TIME"."SESSION_ID" = userenv('sessionid'))
 GROUP BY "CPR_LEDGER"."ASSET_ID"
 
 
 
 
 
 ;