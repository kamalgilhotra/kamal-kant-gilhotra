
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_GL_ACCT_END_BAL_ANNUAL_VIEW" ("COMPANY_ID", "BUS_SEGMENT_ID", "MONTH_NUMBER", "GL_ACCOUNT_ID", "AMOUNT", "RETIREMENTS", "SALVAGE_CASH", "SALVAGE_CREDITS", "RESERVE_CREDITS", "BOOK_AMOUNT") AS 
  SELECT company_id, bus_segment_id, month_number, gl_account_id,     sum(  nvl(basis_1,0)+nvl(basis_2,0)+nvl(basis_3,0)+nvl(basis_4,0)+nvl(basis_5,0)+    nvl(basis_6,0)+nvl(basis_7,0)+nvl(basis_8,0)+nvl(basis_9,0)+nvl(basis_10,0)+    nvl(basis_11,0)+nvl(basis_12,0)+nvl(basis_13,0)+nvl(basis_14,0)+nvl(basis_15,0)+    nvl(basis_16,0)+nvl(basis_17,0)+nvl(basis_18,0)+nvl(basis_19,0)+nvl(basis_20,0)+    nvl(basis_21,0)+nvl(basis_22,0)+nvl(basis_23,0)) amount,    sum(nvl(retirements,0)) retirements,   sum(nvl(salvage_cash,0)) salvage_cash,   sum(nvl(salvage_returns,0)) salvage_credits,     sum(nvl(reserve_credits,0)) reserve_credits,   sum(nvl(book_amount,0)) book_amount     FROM wo_gl_account_summary, report_time  WHERE wo_gl_account_summary.month_number like (to_char(report_time.start_month,'yyyy') ||'%')  			and report_time.session_id= userenv('sessionid')  GROUP BY company_id, bus_segment_id, month_number, gl_account_id 
 
 
 
 
 
 ;