CREATE OR REPLACE VIEW v_ls_ilr_mc_options (
	   company,
       set_of_books,
       lease_group,
       ilr_id,
       Cancelable_Type,
       Termination_amt,
       Bargain_Purchase,
       purchase_opt_amt,
       Renew_opt,
       renewal_period_start,
       renewal_period_end,
       renewal_frequency,
       terms_per_renewal,
       amount_per_term,
	   renew_prob_id,
       Renew_notice_req,
       lease_cap_type,
	   rate,
	   ls_cur_type,
	   currency_id,
       currency_display_symbol,
       iso_code
)  AS 
WITH cur AS (
  SELECT ls_currency_type_id AS ls_cur_type,
    currency_id,
    currency_display_symbol,
    iso_code,
    CASE ls_currency_type_id
      WHEN 1 THEN 1
      ELSE NULL
    END AS contract_approval_rate
  FROM currency
  CROSS JOIN ls_lease_currency_type
)
SELECT   company.company_id as company,
       set_of_books.set_of_books_id as set_of_books,
       ls_lease_group.lease_group_id as lease_group,
       ls_ilr.ilr_id as ilr_id,
       ls_ilr_options.cancelable_type_id AS Cancelable_Type,
       Nvl(ls_ilr_options.termination_amt,0) * Nvl(cur.contract_approval_rate, cur_rate.rate) AS Termination_amt,
       ls_ilr_options.purchase_option_type_id AS Bargain_Purchase,
       Nvl(ls_ilr_options.purchase_option_amt,0) * Nvl(cur.contract_approval_rate, cur_rate.rate) AS purchase_opt_amt,
       ls_ilr_options.renewal_option_type_id AS Renew_opt,
       ls_ilr_renewal_options.renewal_start_date AS renewal_period_start,
       Add_Months(ls_ilr_renewal_options.renewal_start_date, (ls_ilr_renewal_options.number_of_terms * decode(ls_ilr_renewal_options.payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1))) AS renewal_period_end,
       ls_payment_freq.description as renewal_frequency,
       ls_ilr_renewal_options.number_of_terms AS terms_per_renewal,
       Nvl(ls_ilr_renewal_options.amount_per_term, 0 ) * Nvl(cur.contract_approval_rate, cur_rate.rate) AS amount_per_term,
       ls_ilr_renewal_options.ilr_renewal_probability_id as renew_prob_id,
       ls_ilr_renewal_options.notice_requirement_months AS Renew_notice_req,
       ls_lease_cap_type.ls_lease_cap_type_id as lease_cap_type,
	   Nvl(cur.contract_approval_rate, ls_ilr_options.in_service_exchange_rate) rate,
	   cur.ls_cur_type as ls_cur_type,
	   cur.currency_id as currency_id,
       cur.currency_display_symbol as currency_display_symbol,
       cur.iso_code as iso_code
 from  cur, ls_ilr, ls_lease, ls_fasb_cap_type_sob_map,
       ls_lease_cap_type, ls_ilr_options, ls_lease_group, 
       company, ls_ilr_group, set_of_books, ls_ilr_renewal_options,  
       ls_ilr_renewal_probability, ls_ilr_renewal, currency_schema cs, currency_rate_default_dense cur_rate,
       ls_payment_freq
 where  ls_ilr.company_id = company.company_id
   AND company.company_id = cs.company_id
   AND  ls_ilr.lease_id = ls_lease.lease_id
   AND ls_ilr.current_revision = ls_ilr_options.revision
   and ls_lease.lease_group_id = ls_lease_group.lease_group_id
   and ls_ilr_group.ilr_group_id = ls_ilr.ilr_group_id                         
   and ls_lease_cap_type.ls_lease_cap_type_id = ls_fasb_cap_type_sob_map.lease_cap_type_id
   and ls_ilr_options.lease_cap_type_id = ls_lease_cap_type.ls_lease_cap_type_id
   AND set_of_books.set_of_books_id = ls_fasb_cap_type_sob_map.set_of_books_id
   and ls_ilr_options.ilr_id = ls_ilr.ilr_id
   AND ls_ilr.ilr_id =  ls_ilr_renewal.ilr_id
   AND ls_ilr_renewal_probability.ilr_renewal_probability_id = ls_ilr_renewal_options.ilr_renewal_probability_id
   AND ls_ilr_renewal.ilr_renewal_id = ls_ilr_renewal_options.ilr_renewal_id
   AND ls_ilr_renewal.revision = ls_ilr.current_revision         
   and ls_fasb_cap_type_sob_map.fasb_cap_type_id in (1, 2, 4, 5)
   AND cur.currency_id =
       CASE cur.ls_cur_type
        WHEN 1 THEN ls_lease.contract_currency_id
        WHEN 2 THEN cs.currency_id
        ELSE NULL
      END
    AND cur_rate.exchange_date = ls_ilr_renewal_options.renewal_start_date
    AND cs.currency_id = cur_rate.currency_to
    AND ls_lease.contract_currency_id = cur_rate.currency_from
    AND cs.currency_type_id = 1
    and cur_rate.exchange_rate_type_id = 1
    and ls_ilr_renewal_options.payment_freq_id = ls_payment_freq.payment_freq_id;
