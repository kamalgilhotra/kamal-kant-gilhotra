
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."REG_HIST_TA_DEF_DRILL_INC_VIEW" ("M_ID", "M_ITEM", "TA_VERSION", "COMPANY", "OPER_IND", "TAXING_ENTITY", "REG_ACCOUNT", "YEAR", "GL_MONTH", "BEG_BALANCE", "END_BALANCE", "AMOUNT_EST", "AMOUNT_ACT", "AMOUNT_CALC", "AMOUNT_MANUAL", "AMOUNT_ADJ", "AMOUNT_ALLOC") AS 
  (
select m.m_id m_id,
		 t.description m_item,
		 v.description ta_version,
		 cs.description company,
		 o.description oper_ind,
		 e.description taxing_entity,
		 ram.description reg_account,
		 v.year year,
		 m.gl_month gl_month,
		 m.beg_balance beg_balance,
		 m.end_balance end_balance,
		 m.amount_est amount_est,
		 m.amount_act amount_act,
		 m.amount_calc amount_calc,
		 m.amount_manual amount_manual,
		 m.amount_adj amount_adj,
		 m.amount_alloc amount_alloc
  from company_setup cs,
		 tax_accrual_m_item m, tax_accrual_entity_include_act i,
		 tax_accrual_control c, tax_accrual_version v, tax_accrual_entity e, tax_accrual_m_master t, tax_accrual_oper_ind o,
		 reg_ta_component k, reg_acct_master ram,
		 reg_ta_comp_member_map w, reg_ta_family_member_map x, reg_ta_version_control y,
		 tax_accrual_gl_month_def g,
		 cr_dd_required_filter hv, cr_dd_required_filter fc, cr_dd_required_filter mn
 where c.company_id = cs.company_id
	and c.company_id = x.company_id
	and c.oper_ind = x.oper_ind
	and c.m_item_id = x.m_item_id
	and x.historic_version_id = y.historic_version_id
	and x.forecast_version_id = y.forecast_version_id
	and c.ta_version_id = y.ta_version_id
	and c.ta_version_id = v.ta_version_id
	and x.historic_version_id = w.historic_version_id
	and x.forecast_version_id = w.forecast_version_id
	and w.reg_component_id = nvl(ram.reg_component_id,0)
	and w.reg_family_id = nvl(ram.reg_family_id,0)
	and w.reg_member_id = nvl(ram.reg_member_id,0)
	and w.reg_member_id = x.reg_member_id
	and w.reg_component_id = k.reg_component_id
	and k.reg_component_id in (5, 6)
	and decode(k.federal, 1, 10, 20) = e.entity_juris_id
	and e.entity_id = x.entity_id
	and ram.reg_source_id = 6
	and m.m_id = c.m_id
	and c.entity_include_id = i.entity_include_id
	and i.entity_id = x.entity_id
	and c.m_item_id = t.m_item_id
	and c.oper_ind = o.oper_ind
	and c.ta_version_id = g.ta_version_id
	and m.gl_month = g.gl_month
	and to_char(v.year) = substr(mn.filter_string, 1, 4)
	and lpad(to_char(g.period), 2, '0') = substr(mn.filter_string, 5, 2)
	and upper(mn.column_name) = 'GL_MONTH'
	and cs.reg_company_id = to_number(fc.filter_string)
	and upper(fc.column_name) = 'REG_COMPANY_ID'
	and x.historic_version_id = to_number(hv.filter_string)
	and upper(hv.column_name) = 'HISTORIC_VERSION_ID'
	and x.forecast_version_id = 0
	and ram.reg_acct_id in (
		select to_number(filter_string) from cr_dd_required_filter
		 where upper(column_name) = 'REG_ACCT_ID')
)
 ;