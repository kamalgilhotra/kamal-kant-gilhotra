CREATE OR REPLACE FORCE VIEW V_LS_ILR_CLASS_TEST_FX_VW
 AS
WITH cur AS (
  SELECT ls_currency_type_id AS ls_cur_type,
    currency_id,
    currency_display_symbol,
    iso_code,
    CASE ls_currency_type_id
      WHEN 1
      THEN 1
      ELSE NULL
    END AS contract_approval_rate
  FROM currency
  CROSS JOIN ls_lease_currency_type
)
SELECT ls_ilr.ilr_id,
  ls_ilr_classification_test.revision,
  ls_ilr_classification_test.user_id,
  ls_ilr_classification_test.time_stamp,
  ls_ilr_classification_test.finance_tests,
  ls_ilr_classification_test.msg_finance_tests,
  ls_ilr_classification_test.economic_life,
  ls_ilr_classification_test.msg_economic_life,
  ls_ilr_classification_test.net_present_value,
  ls_ilr_classification_test.msg_net_present_value,
  ls_ilr_classification_test.transfer_of_ownership,
  ls_ilr_classification_test.msg_transfer_of_ownership,
  ls_ilr_classification_test.intent_to_purchase,
  ls_ilr_classification_test.msg_intent_to_purchase,
  ls_ilr_classification_test.specialized_asset,
  ls_ilr_classification_test.msg_specialized_asset,
  ls_ilr_classification_test.economic_life_percent,
  ls_ilr_classification_test.min_economic_life,
  ls_ilr_classification_test.max_economic_life,
  ls_ilr_classification_test.lease_term,
  ls_ilr_classification_test.fmv * NVL(cur.contract_approval_rate, cur_rate.rate) AS fmv,
  ls_ilr_classification_test.npv * NVL(cur.contract_approval_rate, cur_rate.rate) AS npv,
  ls_ilr_classification_test.npv_percent,
  NVL(cur.contract_approval_rate, cur_rate.rate) AS rate,
  cur.ls_cur_type,
  cur.currency_id,
  cur.currency_display_symbol,
  cur.iso_code
FROM cur,
  ls_ilr,
  ls_lease,
  ls_ilr_classification_test,
  currency_schema cs,
  currency_rate_default_dense cur_rate
WHERE ls_ilr.company_id               = cs.company_id
AND ls_ilr.lease_id                   = ls_lease.lease_id
AND ls_ilr.current_revision           = ls_ilr_classification_test.revision
AND ls_ilr_classification_test.ilr_id = ls_ilr.ilr_id
AND cur.currency_id                   =
  CASE cur.ls_cur_type
    WHEN 1
    THEN ls_lease.contract_currency_id
    WHEN 2
    THEN cs.currency_id
    ELSE NULL
  END
AND cur_rate.exchange_date        = to_date(TO_CHAR(sysdate,'YYYYMM'),'YYYYMM')
AND cs.currency_id                = cur_rate.currency_to
AND ls_lease.contract_currency_id = cur_rate.currency_from
AND cs.currency_type_id           = 1
and cur_rate.exchange_rate_type_id = 1;
