
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CR_EPE_ALL_TRANS_COA_VIEW" ("COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER", "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT", "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE", "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1", "FUTURE_USE_2", "AMOUNT", "QUANTITY", "INTERFACE_BATCH_ID", "MONTH_NUMBER", "GL_JOURNAL_CATEGORY", "DEBIT_CREDIT") AS 
  SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_accounts_payable_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_allocations_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_commitments_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_conversion_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_energy_expense_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_general_ledger_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_inter_company_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_inventory_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_journal_lines_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_otl_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_payroll_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_powerplant_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_purchasing_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_revenue_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_reversals_manual_sv
   UNION ALL
   SELECT "COMPANY", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER",
          "ACCOUNT", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT",
          "WORK_ORDER", "CHARGING_COST_CENTER", "CHARGING_EXPENSE_TYPE",
          "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1",
          "FUTURE_USE_2", amount, quantity, interface_batch_id, month_number,
          gl_journal_category,
          DECODE (dr_cr_id, -1, 'CREDIT', 'DEBIT') AS debit_credit
     FROM cr_customer_care_sv
 
 
 
 
 
 ;