
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_EST_APPR_REVISION_VIEW" ("WORK_ORDER_ID", "REVISION", "APPROVED", "CURRENT_REVISION", "MAX_REVISION") AS 
  select b.work_order_id, b.revision, nvl(a.count, 0) approved,
decode(c.current_revision, b.revision, 1, 0) current_revision,
decode(d.revision, b.revision, 1, 0) max_revision from
(select work_order_id, revision, count(*) count    from work_order_approval
where (authorizer1 is null or authorized_date1 is not null) and
(authorizer2 is null or authorized_date2 is not null) and
(authorizer3 is null or authorized_date3 is not null) and
(authorizer4 is null or authorized_date4 is not null) and
(authorizer5 is null or authorized_date5 is not null) and
(authorizer6 is null or authorized_date6 is not null) and
(authorizer7 is null or authorized_date7 is not null) and
(authorizer8 is null or authorized_date8 is not null) and
(authorizer9 is null or authorized_date9 is not null) and
(authorizer10 is null or authorized_date10 is not null)
group by work_order_id, revision ) a, work_order_approval b, work_order_control c,
(select max(revision) revision, work_order_id from work_order_approval
group by work_order_id) d where b.work_order_id = a.work_order_id (+) and
   b.revision = a.revision (+) and b.work_order_id = c.work_order_id and
   b.work_order_id = d.work_order_id
 
 
 
 
 
 ;