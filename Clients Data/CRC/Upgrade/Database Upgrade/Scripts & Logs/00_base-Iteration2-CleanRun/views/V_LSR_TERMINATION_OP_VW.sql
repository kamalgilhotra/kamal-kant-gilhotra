CREATE OR REPLACE VIEW V_LSR_TERMINATION_OP_VW (
  ilr_id,
  termination_date,
  rent_balance,
  deferred_costs,
  penalty_other_payment,
  income_gainloss,
  rent_balance_acct_id,
  deferred_costs_acct_id,
  penalty_other_payment_acct_id,
  income_gainloss_acct_id,
  termination_source,
  comments,
  company_id,
  ls_cur_type,
  contract_currency_id,
  display_currency_id,
  company_currency_id,
  rate,
  iso_code,
  currency_display_symbol,
  set_of_books_id
) AS
with cur AS (
  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
    contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
  FROM currency contract_cur
  UNION
  SELECT 2, company_cur.currency_id,
    company_cur.currency_display_symbol, company_cur.iso_code, 0
  FROM currency company_cur
)
SELECT
 lito.ilr_id,
 lito.termination_date,
 lito.rent_balance * decode(ls_cur_type, 2, nvl(lito.termination_exchange_rate, historic_rate), 1) rent_balance,
 lito.deferred_costs * decode(ls_cur_type, 2, nvl(lito.termination_exchange_rate, historic_rate), 1) deferred_costs,
 lito.penalty_other_payment * decode(ls_cur_type, 2, nvl(lito.termination_exchange_rate, historic_rate), 1) penalty_other_payment,
 lito.income_gainloss * decode(ls_cur_type, 2, nvl(lito.termination_exchange_rate, historic_rate), 1) income_gainloss,
 lito.rent_balance_acct_id rent_balance_acct_id,
 lito.deferred_costs_acct_id deferred_costs_acct_id,
 lito.penalty_other_payment_acct_id penalty_other_payment_acct_id,
 lito.income_gainloss_acct_id income_gainloss_acct_id,
 lito.termination_source termination_source,
 lito.comments comments,
 ilr.company_id company_id,
 cur.ls_cur_type ls_cur_type,
 lease.contract_currency_id,
 cur.currency_id display_currency_id,
 cs.currency_id company_currency_id,
 decode(ls_cur_type, 2, nvl(lito.termination_exchange_rate, historic_rate), 1) rate,
 cur.iso_code,
 cur.currency_display_symbol,
 lito.set_of_books_id
 FROM lsr_ilr_termination_op lito
 INNER JOIN lsr_ilr ilr
    ON ilr.ilr_id = lito.ilr_id
 INNER JOIN currency_schema cs
    ON ilr.company_id = cs.company_id
 INNER JOIN lsr_lease lease
    ON ilr.lease_id = lease.lease_id
 INNER JOIN cur
    ON (cur.ls_cur_type = 1 AND
       cur.currency_id = lease.contract_currency_id)
    OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
 WHERE cs.currency_type_id = 1;
 