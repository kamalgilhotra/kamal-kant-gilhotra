
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."ARO_SOB_VIEW" ("ARO_ID", "SET_OF_BOOKS_ID") AS 
  select A.ARO_ID, B.SET_OF_BOOKS_ID
  from ARO A, BOOKS_SOB_VIEW B
 where B.BOOK_SUMMARY_ID = NVL(A.BOOK_SUMMARY_OVERRIDE, (select max(BS.BOOK_SUMMARY_ID)
                                                          from BOOK_SUMMARY BS
                                                         where UPPER(trim(BOOK_SUMMARY_TYPE)) = 'ARO'))

 
 ;