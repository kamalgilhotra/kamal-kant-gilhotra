
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."V_PA_CONTROL_LOCN_GROUPBY" ("GROUPBY_NAME") AS 
  (
  SELECT GROUP_NAME AS GROUPBY_NAME FROM PWRPLANT.PA_CONTROL_LOCATIONS WHERE GROUPBY_SHOW = 1)
 
 ;