
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."REG_ALLOC_RESULTS_W_TAX" ("REG_CASE_ID", "REG_ACCT_ID", "REG_ACCT_TYPE", "SUB_ACCT_TYPE", "TAX_INDICATOR", "REG_ALLOC_CATEGORY_ID", "REG_ALLOC_TARGET_ID", "CASE_YEAR", "ANNUAL_REG_FACTOR", "STATISTICAL_VALUE", "ALLOCATOR", "REG_ALLOCATOR_ID", "EFFECTIVE_DATE", "ALLOC_AMOUNT", "PARENT_TARGET_ID", "ALLOC_LEVEL", "PER_BOOKS", "ADJ_AMOUNT") AS 
  (
SELECT x."REG_CASE_ID",x."REG_ACCT_ID",x."REG_ACCT_TYPE",x."SUB_ACCT_TYPE",x."TAX_INDICATOR",x."REG_ALLOC_CATEGORY_ID",x."REG_ALLOC_TARGET_ID",x."CASE_YEAR",x."ANNUAL_REG_FACTOR",x."STATISTICAL_VALUE",x."ALLOCATOR",x."REG_ALLOCATOR_ID",x."EFFECTIVE_DATE",x."ALLOC_AMOUNT",x."PARENT_TARGET_ID",x."ALLOC_LEVEL",
       x.alloc_amount - Nvl(aar.adj_amount, 0) per_books,
       aar.adj_amount adj_amount
  FROM (SELECT y.reg_case_id,
               m.reg_acct_id reg_acct_id,
               t.reg_acct_type_id reg_acct_type,
               s.sub_acct_type_id sub_acct_type,
               c.tax_control_id tax_indicator,
               ac.reg_alloc_category_id reg_alloc_category_id,
               x.reg_alloc_target_id reg_alloc_target_id,
               r.case_year case_year,
               f.annual_reg_factor,
               f.statistical_value,
               NVL(al.description, ' < none > ') allocator,
               al.reg_allocator_id,
               f.effective_date,
               r.amount alloc_amount,
               p.reg_alloc_target_id parent_target_id,
               lev.alloc_level alloc_level
          FROM reg_alloc_result r,
               reg_acct_type t,
               reg_sub_acct_type s,
               reg_tax_control c,
               reg_case_acct a,
               reg_acct_master m,
               reg_case_alloc_account b,
               reg_alloc_target x,
               reg_case y,
               reg_allocator al,
               reg_case_alloc_account p,
               reg_alloc_category ac,
               (SELECT reg_allocator_id,
                       reg_case_id,
                       reg_alloc_category_id,
                       reg_alloc_target_id,
                       effective_date,
                       annual_reg_factor,
                       statistical_value
                  FROM reg_case_alloc_factor af
                UNION
                SELECT reg_allocator_id,
                       reg_case_id,
                       -99,
                       reg_alloc_target_id,
                       test_yr_end,
                       annual_reg_factor,
                       statistical_value
                  FROM reg_case_allocator_dyn_factor daf
                 WHERE last_updated =
                       (SELECT Max(last_updated)
                          FROM reg_case_allocator_dyn_factor q
                         WHERE daf.reg_allocator_id = q.reg_allocator_id
                           AND daf.reg_case_id = q.reg_case_id
                           AND daf.reg_alloc_target_id = q.reg_alloc_target_id)) f,
               (SELECT c.reg_case_id,
                       c.reg_alloc_category_id,
                       f.reg_alloc_target_id,
                       c.step_order alloc_level
                  FROM reg_case_alloc_steps    s,
                       reg_case_alloc_steps    c,
                       reg_case_target_forward f
                 WHERE s.reg_case_id = c.reg_case_id
                   AND c.reg_case_id = f.reg_case_id
                   AND s.reg_alloc_category_id = f.reg_alloc_category_id
                   AND s.step_order + 1 = c.step_order
                union
                SELECT reg_case_id,
                       0           reg_alloc_target_id,
                       0           reg_alloc_category_id,
                       1           step_order
                  FROM reg_case) lev
         WHERE r.reg_case_id = a.reg_case_id
           AND r.reg_acct_id = a.reg_acct_id
           AND r.reg_case_id = b.reg_case_id
           AND r.reg_alloc_acct_id = b.reg_alloc_acct_id
           AND r.reg_acct_id = m.reg_acct_id
           AND b.reg_alloc_target_id = x.reg_alloc_target_id
           AND a.reg_acct_type_id = t.reg_acct_type_id
           AND a.reg_acct_type_id = s.reg_acct_type_id
           AND a.sub_acct_type_id = s.sub_acct_type_id
           AND s.tax_control_id = c.tax_control_id(+)
           AND r.reg_case_id = y.reg_case_id
           AND p.reg_allocator_id = al.reg_allocator_id(+)
           AND b.parent_reg_alloc_acct_id = p.reg_alloc_acct_id
           AND b.reg_case_id = p.reg_case_id
           AND p.reg_allocator_id = f.reg_allocator_id
           AND p.reg_case_id = f.reg_case_id
           AND ac.reg_alloc_category_id = r.reg_alloc_category_id
           AND b.reg_alloc_target_id = f.reg_alloc_target_id
           AND m.reg_acct_id <> 0
           AND lev.reg_alloc_category_id = ac.reg_alloc_category_id
           AND lev.reg_alloc_target_id = b.reg_alloc_target_id
           AND lev.reg_case_id = r.reg_case_id
           AND f.effective_date =
               (SELECT MAX(xx.effective_date)
                  FROM (SELECT reg_allocator_id,
                               reg_case_id,
                               reg_alloc_category_id,
                               reg_alloc_target_id,
                               effective_date,
                               annual_reg_factor,
                               statistical_value
                          FROM reg_case_alloc_factor af
                        UNION
                        SELECT reg_allocator_id,
                               reg_case_id,
                               -99,
                               reg_alloc_target_id,
                               test_yr_end,
                               annual_reg_factor,
                               statistical_value
                          FROM reg_case_allocator_dyn_factor daf) xx
                 WHERE xx.reg_case_id = r.reg_case_id
                   AND xx.reg_allocator_id = f.reg_allocator_id
                   AND xx.reg_alloc_category_id =
                       DECODE(f.reg_alloc_category_id,
                              -99,
                              xx.reg_alloc_category_id,
                              f.reg_alloc_category_id)
                   AND xx.reg_alloc_target_id = f.reg_alloc_target_id
                   AND xx.effective_date <= r.case_year)
        UNION
        SELECT s.reg_case_id,
               s.reg_acct_id,
               reg_acct_type_id reg_acct_type,
               sub_acct_type_id sub_acct_type,
               NULL tax_indicator,
               0 reg_alloc_category_id,
               0 reg_alloc_target_id,
               case_year,
               1 annual_reg_factor,
               NULL statistical_value,
               'Total Company' allocator,
               NULL reg_allocator_id,
               s.case_year effective_date,
               total_co_final_amount alloc_amount,
               NULL parent_target_id,
               1 alloc_level
          FROM reg_case_summary_ledger s, reg_case_acct a
         WHERE a.reg_case_id = s.reg_case_id
           AND a.reg_acct_id = s.reg_acct_id) x,
       (SELECT reg_case_id,
               case_year,
               reg_acct_id,
               reg_alloc_category_id,
               reg_alloc_target_Id,
               Sum(Nvl(alloc_amount, 0)) adj_amount
          FROM reg_case_adjust_alloc_result
         WHERE adjustment_id <> 0
         GROUP BY reg_case_id,
                  case_year,
                  reg_acct_id,
                  reg_alloc_category_id,
                  reg_alloc_target_Id) aar
 WHERE x.reg_case_id = aar.reg_case_id(+)
   AND x.case_year = aar.case_year(+)
   AND x.reg_acct_id = aar.reg_acct_id(+)
   AND x.reg_alloc_category_id = aar.reg_alloc_category_id(+)
   AND x.reg_alloc_target_id = aar.reg_alloc_target_id(+)
)
 ;