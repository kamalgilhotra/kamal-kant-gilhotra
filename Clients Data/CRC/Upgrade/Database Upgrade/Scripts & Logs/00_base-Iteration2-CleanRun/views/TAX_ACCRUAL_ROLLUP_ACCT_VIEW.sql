
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_ROLLUP_ACCT_VIEW" ("TAX_ACCRUAL_ROLLUP_ACCT_ID", "ROLLUP_DESCRIPTION", "TAX_ACCRUAL_ROLLUP_DTL_ACCT_ID", "ROLLUP_DETAIL_DESCRIPTION", "ACCOUNT") AS 
  select	ta_rollup.tax_accrual_rollup_acct_id,
			ta_rollup.description,
			ta_rollup_detail.tax_accrual_rollup_dtl_acct_id,
			ta_rollup_detail.description,
			acct_rollups.account
from	tax_accrual_rollup_acct ta_rollup,
		tax_accrual_rollup_dtl_acct ta_rollup_detail,
		tax_accrual_acct_rollups acct_rollups
where acct_rollups.tax_accrual_rollup_dtl_acct_id = ta_rollup_detail.tax_accrual_rollup_dtl_acct_id
	and ta_rollup_detail.tax_accrual_rollup_acct_id = ta_rollup.tax_accrual_rollup_acct_id
union all
(	select	ta_rollup.tax_accrual_rollup_acct_id,
				ta_rollup.description,
				0,
				'No Rollup',
				gl_account.external_account_code
	from	tax_accrual_rollup_acct ta_rollup,
			gl_account
	minus
	select	ta_rollup.tax_accrual_rollup_acct_id,
				ta_rollup.description,
				0,
				'No Rollup',
				acct_rollups.account
	from	tax_accrual_rollup_acct ta_rollup,
			tax_accrual_rollup_dtl_acct ta_rollup_detail,
			tax_accrual_acct_rollups acct_rollups
	where acct_rollups.tax_accrual_rollup_dtl_acct_id = ta_rollup_detail.tax_accrual_rollup_dtl_acct_id
		and ta_rollup_detail.tax_accrual_rollup_acct_id = ta_rollup.tax_accrual_rollup_acct_id
)
union all
select	-1000,
			'Account Type',
			gl_account.account_type_id,
			account_type.description,
			gl_account.external_account_code
from	gl_account,
		account_type
where gl_account.account_type_id = account_type.account_type_id
	and upper(account_type.description) like '%POWERTAX%'
union all
select	-2000,
			'Ferc Account',
			gl_account.ferc_sys_of_accts_id,
			ferc_sys_of_accts.description,
			gl_account.external_account_code
from 	gl_account,
		account_type,
		ferc_sys_of_accts
where gl_account.account_type_id = account_type.account_type_id
	and upper(account_type.description) like '%POWERTAX%'
	and ferc_sys_of_accts.ferc_sys_of_accts_id = gl_account.ferc_sys_of_accts_id

 
 
 
 
 ;