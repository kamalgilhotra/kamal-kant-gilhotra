
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."LS_TAX_BASIS_BUCKETS" ("CONT1", "CONT2", "CONT3", "CONT4", "CONT5", "CONT6", "CONT7", "CONT8", "CONT9", "CONT10", "EXEC1", "EXEC2", "EXEC3", "EXEC4", "EXEC5", "EXEC6", "EXEC7", "EXEC8", "EXEC9", "EXEC10") AS 
  select sum(CONT1)  as CONT1,
       sum(CONT2)  as CONT2,
       sum(CONT3)  as CONT3,
       sum(CONT4)  as CONT4,
       sum(CONT5)  as CONT5,
       sum(CONT6)  as CONT6,
       sum(CONT7)  as CONT7,
       sum(CONT8)  as CONT8,
       sum(CONT9)  as CONT9,
       sum(CONT10) as CONT10,
       sum(EXEC1)  as EXEC1,
       sum(EXEC2)  as EXEC2,
       sum(EXEC3)  as EXEC3,
       sum(EXEC4)  as EXEC4,
       sum(EXEC5)  as EXEC5,
       sum(EXEC6)  as EXEC6,
       sum(EXEC7)  as EXEC7,
       sum(EXEC8)  as EXEC8,
       sum(EXEC9)  as EXEC9,
       sum(EXEC10) as EXEC10
  from (select 2 - INCLUDE_IN_TAX_BASIS as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 1
        union
        select 0 as CONT1,
               2 - INCLUDE_IN_TAX_BASIS as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 2
        union
        select 0 as CONT1,
               0 as CONT2,
               2 - INCLUDE_IN_TAX_BASIS as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 3
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               2 - INCLUDE_IN_TAX_BASIS as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 4
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               2 - INCLUDE_IN_TAX_BASIS as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 5
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               2 - INCLUDE_IN_TAX_BASIS as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 6
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               2 - INCLUDE_IN_TAX_BASIS as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 7
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               2 - INCLUDE_IN_TAX_BASIS as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 8
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               2 - INCLUDE_IN_TAX_BASIS as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 9
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               2 - INCLUDE_IN_TAX_BASIS as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 10
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               2 - INCLUDE_IN_TAX_BASIS as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 1
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               2 - INCLUDE_IN_TAX_BASIS as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 2
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               2 - INCLUDE_IN_TAX_BASIS as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 3
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               2 - INCLUDE_IN_TAX_BASIS as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 4
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               2 - INCLUDE_IN_TAX_BASIS as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 5
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               2 - INCLUDE_IN_TAX_BASIS as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 6
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               2 - INCLUDE_IN_TAX_BASIS as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 7
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               2 - INCLUDE_IN_TAX_BASIS as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 8
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               2 - INCLUDE_IN_TAX_BASIS as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 9
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               2 - INCLUDE_IN_TAX_BASIS as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 10)
 
 ;