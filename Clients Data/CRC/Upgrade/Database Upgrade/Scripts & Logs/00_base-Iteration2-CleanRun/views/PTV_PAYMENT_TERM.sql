
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."PTV_PAYMENT_TERM" ("SCHEDULE_ID", "STATEMENT_YEAR_ID", "INSTALLMENT_ID", "PERIOD_ID", "DESCRIPTION", "INSTALLMENT_TYPE_ID", "PERIOD_TYPE_ID", "START_DATE", "END_DATE") AS 
  select		pt_schedule_period.schedule_id schedule_id,  				pt_schedule_period.statement_year_id statement_year_id,  				pt_schedule_period.installment_id installment_id,  				pt_schedule_period.period_id period_id,  				decode( pt_schedule_installment.installment_type_id,  								1, to_char( pt_schedule_installment.installment_number ) || ' - ' || pt_period_type.description,  								pt_installment_type.description  							) description,  				pt_schedule_installment.installment_type_id,  				pt_schedule_period.period_type_id,  				pt_schedule_period.start_date start_date,  				pt_schedule_period.end_date end_date  	from		pt_schedule_installment,  				pt_installment_type,  				pt_schedule_period,  				pt_period_type  	where	pt_schedule_installment.installment_type_id = pt_installment_type.installment_type_id  		and	pt_schedule_installment.schedule_id = pt_schedule_period.schedule_id  		and	pt_schedule_installment.statement_year_id = pt_schedule_period.statement_year_id  		and	pt_schedule_installment.installment_id = pt_schedule_period.installment_id  		and	pt_schedule_period.period_type_id = pt_period_type.period_type_id
 
 
 
 
 ;