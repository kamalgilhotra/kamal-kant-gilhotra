
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_ROLLUP_ACCT_JE_V" ("TAX_ACCRUAL_ROLLUP_ACCT_ID", "ROLLUP_DESCRIPTION", "TAX_ACCRUAL_ROLLUP_DTL_ACCT_ID", "ROLLUP_DETAIL_DESCRIPTION", "JE_ID") AS 
  select	distinct	acct_view.tax_accrual_rollup_acct_id,
						acct_view.rollup_description,
						acct_view.tax_accrual_rollup_dtl_acct_id,
						acct_view.rollup_detail_description,
						je.je_id
from	tax_accrual_rollup_acct_view acct_view,
		tax_accrual_je je
where acct_view.account = je.balance_sheet_acct
	or acct_view.account = je.debit_account_e
	or acct_view.account = je.credit_account_e
 
 
 
 
 ;