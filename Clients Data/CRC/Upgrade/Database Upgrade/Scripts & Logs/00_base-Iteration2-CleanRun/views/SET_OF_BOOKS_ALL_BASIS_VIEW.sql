
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."SET_OF_BOOKS_ALL_BASIS_VIEW" ("SET_OF_BOOKS_ID", "DESCRIPTION", "BOOK_SUMMARY_ID", "BASIS_STATUS") AS 
  select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_1_INDICATOR BASIS_STATUS
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where BS.BOOK_SUMMARY_ID = 1
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_2_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where BS.BOOK_SUMMARY_ID = 2
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_3_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where BS.BOOK_SUMMARY_ID = 3
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_4_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where BS.BOOK_SUMMARY_ID = 4
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_5_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where BS.BOOK_SUMMARY_ID = 5
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_6_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 6
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_7_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 7
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_8_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 8
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_9_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 9
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_10_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 10
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_11_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 11
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_12_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 12
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_13_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 13
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_14_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 14
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_15_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 15
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_16_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 16
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_17_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 17
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_18_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 18
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_19_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 19
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_20_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 20
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_21_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 21
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_22_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 22
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_23_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 23
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_24_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 24
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_25_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 25
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_26_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 26
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_27_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 27
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_28_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 28
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_29_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 29
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_30_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 30
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_31_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 31
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_32_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 32
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_33_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 33
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_34_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 34
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_35_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 35
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_36_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 36
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_37_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 37
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_38_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 38
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_39_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 39
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_40_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 40
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_41_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 41
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_42_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 42
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_43_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 43
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_44_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 44
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_45_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 45
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_46_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 46
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_47_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 47
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_48_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 48
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_49_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 49
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_50_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 50
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_51_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 51
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_52_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 52
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_53_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 53
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_54_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 54
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_55_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 55
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_56_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 56
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_57_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 57
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_58_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 58
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_59_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 59
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_60_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 60
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_61_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 61
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_62_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 62
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_63_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 63
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_64_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 64
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_65_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 65
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_66_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 66
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_67_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 67
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_68_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 68
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_69_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 69
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_70_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 70
 
 ;