
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_GL_DATA_CON_DTLS_V" ("GL_DATA_CONTROL_ID", "CR_PULL_FIELD_ID", "GL_ACCOUNT_TYPE_ID", "NOT_IND", "ACCOUNT_LOWER", "ACCOUNT_UPPER") AS 
  select	gl_data_control_id,
			cr_pull_field_id,
			gl_account_type_id,
			not_ind,
			account_lower,
			account_upper
from tax_accrual_gl_data_con_dtls
union all
(
	select	gl_data_control_id,
				cr_pull_field_id,
				1,
				0,
				null,
				null
	from	tax_accrual_gl_data_control control,
			tax_accrual_cr_pull_fields pull_fields
	where control.cr_pull_id = pull_fields.cr_pull_id
		and control.gl_data_control_id in
		(	select gl_data_control_id
			from tax_accrual_gl_data_con_dtls
		)
	minus
	select	gl_data_control_id,
				cr_pull_field_id,
				1,
				0,
				null,
				null
	from tax_accrual_gl_data_con_dtls
)

 
 
 
 
 ;