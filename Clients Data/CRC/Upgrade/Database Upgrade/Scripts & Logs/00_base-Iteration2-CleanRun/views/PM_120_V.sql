
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."PM_120_V" ("BUDGET_VERSION", "OWNING_CC", "PROJECT", "PROJECT_DESC", "ALLOCATION_TYPE", "BUDGET_ROLLUP_DESCR", "ATYPE", "SUBTOTAL3", "SUBTOTAL4", "Y1T", "AY1T", "Y2T", "AY2T", "Y3T", "AY3T", "Y4T", "AY4T", "Y5T", "AY5T", "Y6T", "AY6T", "Y7T", "AY7T", "Y8T", "AY8T", "Y9T", "AY9T", "Y10T", "AY10T", "TEN_YR_TOTAL", "ATEN_YR_TOTAL", "YR1", "YR2", "YR3", "YR4", "YR5", "YR6", "YR7", "YR8", "YR9", "YR10") AS 
  select budget_version, owning_Cc, project, project_desc, allocation_type, budget_rollup_descr,
decode(allocation_type, '1', 'PAYROLL', '2', 'PAYROLL', '3', 'PAYROLL', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '6','1','0') as "SUBTOTAL3",
decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '0') as "SUBTOTAL4",
sum(round(yr1_total)) y1t, sum(round(ayr1_total)) ay1t,
sum(round(yr2_total)) y2t, sum(round(ayr2_total)) ay2t,
sum(round(yr3_total)) y3t, sum(round(ayr3_total)) ay3t,
sum(round(yr4_total)) y4t, sum(round(ayr4_total)) ay4t,
sum(round(yr5_total)) y5t, sum(round(ayr5_total)) ay5t,
sum(round(yr6_total)) y6t, sum(round(ayr6_total)) ay6t,
sum(round(yr7_total)) y7t, sum(round(ayr7_total)) ay7t,
sum(round(yr8_total)) y8t, sum(round(ayr8_total)) ay8t,
sum(round(yr9_total)) y9t, sum(round(ayr9_total)) ay9t,
sum(round(yr10_total)) y10t, sum(round(ayr10_total)) ay10t,
sum(round(yr1_total)) +
sum(round(yr2_total)) +
sum(round(yr3_total)) +
sum(round(yr4_total)) +
sum(round(yr5_total)) +
sum(round(yr6_total)) +
sum(round(yr7_total)) +
sum(round(yr8_total)) +
sum(round(yr9_total)) +
sum(round(yr10_total)) ten_yr_total,
sum(round(ayr1_total))+
sum(round(ayr2_total))+
sum(round(ayr3_total))+
sum(round(ayr4_total))+
sum(round(ayr5_total))+
sum(round(ayr6_total))+
sum(round(ayr7_total))+
sum(round(ayr8_total))+
sum(round(ayr9_total))+
sum(round(ayr10_total)) aten_yr_total,
(select substr(yr.control_value,1,4)+0 yr1 from cr_alloc_system_control yr where control_id = 23) yr1,
(select substr(yr.control_value,1,4)+1 yr2 from cr_alloc_system_control yr where control_id = 23) yr2,
(select substr(yr.control_value,1,4)+2 yr3 from cr_alloc_system_control yr where control_id = 23) yr3,
(select substr(yr.control_value,1,4)+3 yr4 from cr_alloc_system_control yr where control_id = 23) yr4,
(select substr(yr.control_value,1,4)+4 yr5 from cr_alloc_system_control yr where control_id = 23) yr5,
(select substr(yr.control_value,1,4)+5 yr6 from cr_alloc_system_control yr where control_id = 23) yr6,
(select substr(yr.control_value,1,4)+6 yr7 from cr_alloc_system_control yr where control_id = 23) yr7,
(select substr(yr.control_value,1,4)+7 yr8 from cr_alloc_system_control yr where control_id = 23) yr8,
(select substr(yr.control_value,1,4)+8 yr9 from cr_alloc_system_control yr where control_id = 23) yr9,
(select substr(yr.control_value,1,4)+9 yr10 from cr_alloc_system_control yr where control_id = 23) yr10
from
(select bv.description budget_version,
woc.department_id owning_cc, woc.work_order_number project, woc.description project_desc,
etmv.allocation_type,etmv.budget_rollup_descr,
sum(est.total) yr1_total, sum(decode(etmv.allocation_type,'7',est.total,0)) ayr1_total,
0 yr2_total, 0 ayr2_total,
0 yr3_total, 0 ayr3_total,
0 yr4_total, 0 ayr4_total,
0 yr5_total, 0 ayr5_total,
0 yr6_total, 0 ayr6_total,
0 yr7_total, 0 ayr7_total,
0 yr8_total, 0 ayr8_total,
0 yr9_total, 0 ayr9_total,
0 yr10_total, 0 ayr10_total
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
     wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
group by est.year, bv.description,
woc.department_id, woc.work_order_number, woc.description, etmv.allocation_type, etmv.budget_rollup_descr
having sum(total) !=0
union
select bv.description budget_version,
woc.department_id owning_cc, woc.work_order_number project, woc.description project_desc,
etmv.allocation_type,etmv.budget_rollup_descr,
0 yr1_total, 0 ayr1_total,
sum(est.total) yr2_total, sum(decode(etmv.allocation_type,'7',est.total,0)) ayr2_total,
0 yr3_total, 0 ayr3_total,
0 yr4_total, 0 ayr4_total,
0 yr5_total, 0 ayr5_total,
0 yr6_total, 0 ayr6_total,
0 yr7_total, 0 ayr7_total,
0 yr8_total, 0 ayr8_total,
0 yr9_total, 0 ayr9_total,
0 yr10_total, 0 ayr10_total
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
     wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)+1
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
group by est.year, bv.description,
woc.department_id, woc.work_order_number, woc.description, etmv.allocation_type, etmv.budget_rollup_descr
having sum(total) !=0
union
select bv.description budget_version,
woc.department_id owning_cc, woc.work_order_number project, woc.description project_desc,
etmv.allocation_type,etmv.budget_rollup_descr,
0 yr1_total, 0 ayr1_total,
0 yr2_total, 0 ayr2_total,
sum(est.total) yr3_total, sum(decode(etmv.allocation_type,'7',est.total,0)) ayr3_total,
0 yr4_total, 0 ayr4_total,
0 yr5_total, 0 ayr5_total,
0 yr6_total, 0 ayr6_total,
0 yr7_total, 0 ayr7_total,
0 yr8_total, 0 ayr8_total,
0 yr9_total, 0 ayr9_total,
0 yr10_total, 0 ayr10_total
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
     wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)+2
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
group by est.year, bv.description,
woc.department_id, woc.work_order_number, woc.description, etmv.allocation_type, etmv.budget_rollup_descr
having sum(total) !=0
union
select bv.description budget_version,
woc.department_id owning_cc, woc.work_order_number project, woc.description project_desc,
etmv.allocation_type,etmv.budget_rollup_descr,
0 yr1_total, 0 ayr1_total,
0 yr2_total, 0 ayr2_total,
0 yr3_total, 0 ayr3_total,
sum(est.total) yr4_total, sum(decode(etmv.allocation_type,'7',est.total,0)) ayr4_total,
0 yr5_total, 0 ayr5_total,
0 yr6_total, 0 ayr6_total,
0 yr7_total, 0 ayr7_total,
0 yr8_total, 0 ayr8_total,
0 yr9_total, 0 ayr9_total,
0 yr10_total, 0 ayr10_total
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
     wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)+3
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
group by est.year, bv.description,
woc.department_id, woc.work_order_number, woc.description, etmv.allocation_type, etmv.budget_rollup_descr
having sum(total) !=0
union
select bv.description budget_version,
woc.department_id owning_cc, woc.work_order_number project, woc.description project_desc,
etmv.allocation_type,etmv.budget_rollup_descr,
0 yr1_total, 0 ayr1_total,
0 yr2_total, 0 ayr2_total,
0 yr3_total, 0 ayr3_total,
0 yr4_total, 0 ayr4_total,
sum(est.total) yr5_total, sum(decode(etmv.allocation_type,'7',est.total,0)) ayr5_total,
0 yr6_total, 0 ayr6_total,
0 yr7_total, 0 ayr7_total,
0 yr8_total, 0 ayr8_total,
0 yr9_total, 0 ayr9_total,
0 yr10_total, 0 ayr10_total
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
     wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)+4
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
group by est.year, bv.description,
woc.department_id, woc.work_order_number, woc.description, etmv.allocation_type, etmv.budget_rollup_descr
having sum(total) !=0
union
select bv.description budget_version,
woc.department_id owning_cc, woc.work_order_number project, woc.description project_desc,
etmv.allocation_type,etmv.budget_rollup_descr,
0 yr1_total, 0 ayr1_total,
0 yr2_total, 0 ayr2_total,
0 yr3_total, 0 ayr3_total,
0 yr4_total, 0 ayr4_total,
0 yr5_total, 0 ayr5_total,
sum(est.total) yr6_total, sum(decode(etmv.allocation_type,'7',est.total,0)) ayr6_total,
0 yr7_total, 0 ayr7_total,
0 yr8_total, 0 ayr8_total,
0 yr9_total, 0 ayr9_total,
0 yr10_total, 0 ayr10_total
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
     wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)+5
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
group by est.year, bv.description,
woc.department_id, woc.work_order_number, woc.description, etmv.allocation_type, etmv.budget_rollup_descr
having sum(total) !=0
union
select bv.description budget_version,
woc.department_id owning_cc, woc.work_order_number project, woc.description project_desc,
etmv.allocation_type,etmv.budget_rollup_descr,
0 yr1_total, 0 ayr1_total,
0 yr2_total, 0 ayr2_total,
0 yr3_total, 0 ayr3_total,
0 yr4_total, 0 ayr4_total,
0 yr5_total, 0 ayr5_total,
0 yr6_total, 0 ayr6_total,
sum(est.total) yr7_total, sum(decode(etmv.allocation_type,'7',est.total,0)) ayr7_total,
0 yr8_total, 0 ayr8_total,
0 yr9_total, 0 ayr9_total,
0 yr10_total, 0 ayr10_total
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
     wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)+6
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
group by est.year, bv.description,
woc.department_id, woc.work_order_number, woc.description, etmv.allocation_type, etmv.budget_rollup_descr
having sum(total) !=0
union
select bv.description budget_version,
woc.department_id owning_cc, woc.work_order_number project, woc.description project_desc,
etmv.allocation_type,etmv.budget_rollup_descr,
0 yr1_total, 0 ayr1_total,
0 yr2_total, 0 ayr2_total,
0 yr3_total, 0 ayr3_total,
0 yr4_total, 0 ayr4_total,
0 yr5_total, 0 ayr5_total,
0 yr6_total, 0 ayr6_total,
0 yr7_total, 0 ayr7_total,
sum(est.total) yr8_total, sum(decode(etmv.allocation_type,'7',est.total,0)) ayr8_total,
0 yr9_total, 0 ayr9_total,
0 yr10_total, 0 ayr10_total
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
     wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)+7
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
group by est.year, bv.description,
woc.department_id, woc.work_order_number, woc.description, etmv.allocation_type, etmv.budget_rollup_descr
having sum(total) !=0
union
select bv.description budget_version,
woc.department_id owning_cc, woc.work_order_number project, woc.description project_desc,
etmv.allocation_type,etmv.budget_rollup_descr,
0 yr1_total, 0 ayr1_total,
0 yr2_total, 0 ayr2_total,
0 yr3_total, 0 ayr3_total,
0 yr4_total, 0 ayr4_total,
0 yr5_total, 0 ayr5_total,
0 yr6_total, 0 ayr6_total,
0 yr7_total, 0 ayr7_total,
0 yr8_total, 0 ayr8_total,
sum(est.total) yr9_total, sum(decode(etmv.allocation_type,'7',est.total,0)) ayr9_total,
0 yr10_total, 0 ayr10_total
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
     wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)+8
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
group by est.year, bv.description,
woc.department_id, woc.work_order_number, woc.description, etmv.allocation_type, etmv.budget_rollup_descr
having sum(total) !=0
union
select bv.description budget_version,
woc.department_id owning_cc, woc.work_order_number project, woc.description project_desc,
etmv.allocation_type,etmv.budget_rollup_descr,
0 yr1_total, 0 ayr1_total,
0 yr2_total, 0 ayr2_total,
0 yr3_total, 0 ayr3_total,
0 yr4_total, 0 ayr4_total,
0 yr5_total, 0 ayr5_total,
0 yr6_total, 0 ayr6_total,
0 yr7_total, 0 ayr7_total,
0 yr8_total, 0 ayr8_total,
0 yr9_total, 0 ayr9_total,
sum(est.total) yr10_total, sum(decode(etmv.allocation_type,'7',est.total,0)) ayr10_total
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)+9
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
group by est.year, bv.description,
woc.department_id, woc.work_order_number, woc.description, etmv.allocation_type, etmv.budget_rollup_descr
having sum(total) !=0
)
group by budget_version, owning_Cc, project, project_desc, allocation_type, budget_rollup_descr
 
 
 
 
 
 ;