
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CURRENT_AFUDC_VIEW" ("AFUDC_TYPE_ID", "EFFECTIVE_DATE") AS 
  SELECT afudc_type_id,
       to_date(MAX(to_char(effective_date,
                           'J')),
               'J') "EFFECTIVE_DATE"
  FROM afudc_data,
       cpr_act_month
 WHERE effective_date <= cpr_act_month.MONTH
   AND (upper("CPR_ACT_MONTH"."USER_ID") = USER)
   AND ("CPR_ACT_MONTH"."SESSION_ID" = userenv('sessionid'))
 GROUP BY afudc_type_id
 
 
 
 
 
 ;