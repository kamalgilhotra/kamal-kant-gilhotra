
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_VERSION_EFF_DT_CO" ("TA_VERSION_ID", "COMPANY_ID", "GL_MONTH", "EFFECTIVE_DATE", "PROCESS_OPTION_ID", "PRIOR_TA_VERSION_ID", "PRIOR_GL_MONTH", "YTD_INCLUDE", "RTP_MONTH") AS 
  select	version.ta_version_id,
			ta_gl_month.company_id,
			ta_gl_month.gl_month,
			decode(	ta_gl_month.process_option_id,3,
						prior_ta_version.effective_date,
						ta_gl_month.effective_date
					),
			ta_gl_month.process_option_id,
			ta_gl_month.prior_ta_version_id,
			ta_gl_month.prior_gl_month,
			decode(	ta_gl_month.process_option_id,3,
						prior_ta_version.ytd_include,
						ta_gl_month.ytd_include
					),
			ta_gl_month.rtp_month
from	tax_accrual_version version,
		tax_accrual_process_info ta_gl_month,
		tax_accrual_gl_month_translate ta_gl_trans,
		tax_accrual_process_info prior_ta_version
where version.ta_version_id = ta_gl_month.ta_version_id
	and ta_gl_month.gl_month = ta_gl_trans.gl_month
	and ta_gl_month.prior_ta_version_id = prior_ta_version.ta_version_id(+)
	and ta_gl_month.gl_month = prior_ta_version.gl_month(+)

 
 
 
 
 ;