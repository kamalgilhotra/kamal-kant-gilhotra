
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_ENTITY_DEDUCT_VIEW" ("ENTITY_ID", "ENTITY_ID_DEDUCTIBLE", "EFFECTIVE_DATE", "M_ITEM_ID", "M_ITEM_ID_FT", "TAX_CREDIT", "COMPANY_ID", "DEDUCT_PERCENT") AS 
  select	deduct.entity_id,
			deduct.entity_id_deductible,
			deduct.effective_date,
			deduct.m_item_id,
			deduct.m_item_id_ft,
			deduct.tax_credit,
			exceptions.company_id,
			exceptions.deduct_percent
from		tax_accrual_entity_deduct deduct,
			tax_accrual_entity_deduct_exc exceptions
where	deduct.entity_id = exceptions.entity_id
	and deduct.entity_id_deductible = exceptions.entity_id_deductible
	and exceptions.effective_date =
	(	select max(inside.effective_date)
		from tax_accrual_entity_deduct_exc inside
		where inside.entity_id = exceptions.entity_id
			and inside.entity_id_deductible = exceptions.entity_id_deductible
			and inside.company_id = exceptions.company_id
			and inside.effective_date <= deduct.effective_date
	)
union all
(
	select	deduct.entity_id,
				deduct.entity_id_deductible,
				deduct.effective_date,
				deduct.m_item_id,
				deduct.m_item_id_ft,
				deduct.tax_credit,
				company.company_id,
				deduct.deduct_percent
	from		tax_accrual_entity_deduct deduct,
				company_setup company
	minus
	select	deduct.entity_id,
				deduct.entity_id_deductible,
				deduct.effective_date,
				deduct.m_item_id,
				deduct.m_item_id_ft,
				deduct.tax_credit,
				exceptions.company_id,
				deduct.deduct_percent
	from		tax_accrual_entity_deduct deduct,
				tax_accrual_entity_deduct_exc exceptions
	where	deduct.entity_id = exceptions.entity_id
		and deduct.entity_id_deductible = exceptions.entity_id_deductible
		and exceptions.effective_date =
		(	select max(inside.effective_date)
			from tax_accrual_entity_deduct_exc inside
			where inside.entity_id = exceptions.entity_id
				and inside.entity_id_deductible = exceptions.entity_id_deductible
				and inside.company_id = exceptions.company_id
				and inside.effective_date <= deduct.effective_date
		)
)
 
 
 
 
 ;