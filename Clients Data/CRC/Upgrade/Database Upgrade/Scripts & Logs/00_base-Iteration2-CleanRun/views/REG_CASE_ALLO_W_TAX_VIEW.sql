create or replace view reg_case_alloc_w_tax_view as
select k.case_name, k.reg_account, k.reg_acct_type, k.sub_acct_type, k.tax_indicator, k.allocation_category, k.allocation_target, k.case_year, k.amount, k.allocator,
f.annual_reg_factor, f.statistical_value, f.effective_date 
from (
select y.case_name case_name, m.description reg_account, t.description reg_acct_type, s.description sub_acct_type,
c.description tax_indicator, ac.description allocation_category, x.description allocation_target,
r.case_year case_year, r.amount amount, p.reg_allocator_id, p.reg_case_id, b.reg_alloc_target_id,
nvl(al.description,'<none>') allocator
from reg_alloc_result r, reg_acct_type t, reg_sub_acct_type s, reg_tax_control c, reg_case_acct a,
reg_acct_master m, reg_case_alloc_account b, reg_alloc_target x, reg_case y, reg_allocator al,
reg_case_alloc_account p, reg_alloc_category ac
where r.reg_case_id = a.reg_case_id
and r.reg_acct_id = a.reg_acct_id
and r.reg_case_id = b.reg_case_id
and r.reg_alloc_acct_id = b.reg_alloc_acct_id
and r.reg_acct_id = m.reg_acct_id
and b.reg_alloc_target_id = x.reg_alloc_target_id
and a.reg_acct_type_id = t.reg_acct_type_id
and a.reg_acct_type_id = s.reg_acct_type_id
and a.sub_acct_type_id = s.sub_acct_type_id
and s.tax_control_id = c.tax_control_id (+)
and r.reg_case_id = y.reg_case_id
AND p.reg_allocator_id = al.reg_allocator_id (+)
AND b.parent_reg_alloc_acct_id = p.reg_alloc_acct_id
AND b.reg_case_id = p.reg_case_id
AND ac.reg_alloc_category_id=r.reg_alloc_category_id
) k,
(SELECT reg_allocator_id, reg_case_id, reg_alloc_category_id, reg_alloc_target_id, effective_Date,
annual_reg_factor, statistical_value
FROM reg_case_alloc_factor af
UNION
SELECT reg_allocator_id, reg_case_id, -99, reg_alloc_target_id, test_yr_end, annual_reg_factor, statistical_value
FROM reg_case_allocator_dyn_factor daf
where (reg_case_id, reg_allocator_id, reg_alloc_target_id, last_updated) in (
select reg_case_id, reg_allocator_id, reg_alloc_target_id, max(last_updated)
from reg_case_allocator_dyn_factor
group by reg_case_id, reg_allocator_id, reg_alloc_target_id)
) f 
where k.reg_allocator_id = f.reg_allocator_id (+)
AND k.reg_case_id = f.reg_case_id (+)
AND k.reg_alloc_target_id = f.reg_alloc_target_id (+)
AND (f.effective_date is null or nvl(f.effective_date,190001) = (
SELECT Max(xx.effective_date) FROM (
SELECT reg_allocator_id, reg_case_id, reg_alloc_category_id, reg_alloc_target_id,
effective_Date, annual_reg_factor, statistical_value
FROM reg_case_alloc_factor af
union select null, null, null, null, 190001, null, null from dual
UNION
SELECT reg_allocator_id, reg_case_id, -99, reg_alloc_target_id, test_yr_end, annual_reg_factor, statistical_value
FROM reg_case_allocator_dyn_factor daf
where (reg_case_id, reg_allocator_id, reg_alloc_target_id, last_updated) in (
select reg_case_id, reg_allocator_id, reg_alloc_target_id, max(last_updated)
from reg_case_allocator_dyn_factor
group by reg_case_id, reg_allocator_id, reg_alloc_target_id)) xx
WHERE xx.reg_case_id=f.reg_case_id
AND xx.reg_allocator_id=f.reg_allocator_id
AND xx.reg_alloc_category_id = decode(f.reg_alloc_category_id, -99, xx.reg_alloc_category_id, f.reg_alloc_category_id)
AND xx.reg_alloc_target_id=f.reg_alloc_target_id AND xx.effective_date <= k.case_year ))
;