
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."BUDGET_VERSION_VIEW" ("COMPANY_ID", "WORKING_VERSION", "FORECAST_VERSION", "PREVIOUS_FORECAST_VERSION", "BUDGET_VERSION", "REVISED_BUDGET", "PROPOSED_VERSION", "CURRENT_YEAR", "ACTUALS_MONTH", "CURRENT_VERSION") AS 
  select COMPANY_ID,
       WORKING_VERSION,
       FORECAST_VERSION,
       PREVIOUS_FORECAST_VERSION,
       BUDGET_VERSION,
       NVL(REVISED_BUDGET, BUDGET_VERSION) REVISED_BUDGET,
       PROPOSED_VERSION,
       CURRENT_YEAR,
       ACTUALS_MONTH,
       CURRENT_VERSION
  from (select CS.COMPANY_ID COMPANY_ID,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) WORKING_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 2
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 2
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) FORECAST_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 3
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 3
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) PREVIOUS_FORECAST_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 4
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 4
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) BUDGET_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 5
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 5
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) REVISED_BUDGET,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 6
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 6
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) PROPOSED_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.CURRENT_YEAR)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.CURRENT_YEAR)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) CURRENT_YEAR,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.ACTUALS_MONTH)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.ACTUALS_MONTH)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) ACTUALS_MONTH,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV
                        where BV.CURRENT_VERSION = 1
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV
                        where BV.CURRENT_VERSION = 1
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) CURRENT_VERSION
          from COMPANY_SETUP CS, PP_SYSTEM_CONTROL_COMPANIES SC
         where SC.COMPANY_ID = CS.COMPANY_ID
           and UPPER(SC.CONTROL_NAME) = 'BV CURRENT VERSION BY COMPANY')
 ;