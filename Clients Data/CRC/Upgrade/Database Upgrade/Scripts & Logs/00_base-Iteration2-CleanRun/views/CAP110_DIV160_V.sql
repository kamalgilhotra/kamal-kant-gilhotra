
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CAP110_DIV160_V" ("PROJECT", "DESCRIPTION", "ALLOCATION_TYPE", "EXPENSE_TYPE", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "CC_TOTAL", "ATYPE", "SUBTOTAL3", "SUBTOTAL4", "BUDGET_VERSION", "AJAN", "AFEB", "AMAR", "AAPR", "AMAY", "AJUN", "AJUL", "AAUG", "ASEP", "AOCT", "ANOV", "ADEC", "ACC_TOTAL") AS 
  select s.project,
	   s.description,
       --s.description project,
       allocation_type,
         --min(upper(ft.description)) alloc_descr,
         budget_rollup_descr expense_type,
         round(sum(jan)) jan, round(sum(feb)) feb, round(sum(mar)) mar, round(sum(apr)) apr,
                             round(sum(may)) may, round(sum(jun)) jun, round(sum(jul)) jul, round(sum(aug)) aug,
                             round(sum(sep)) sep, round(sum(oct)) oct, round(sum(nov)) nov, round(sum(dec)) dec,          (round(sum(jan))+round(sum(feb))+round(sum(mar))+round(sum(apr))+round(sum(may))+round(sum(jun))+round(sum(jul))+round(sum(aug))+round(sum(sep))+round(sum(oct))+round(sum(nov))+round(sum(dec))) CC_TOTAL,
          decode(allocation_type, '1', 'PAYROLL', '2', 'PAYROLL', '3', 'PAYROLL', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
		 decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '6','1','0') as "SUBTOTAL3",
		 decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '0') as "SUBTOTAL4",
		 (select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') budget_version,
		 round(sum(ajan)) ajan, round(sum(afeb)) afeb, round(sum(amar)) amar, round(sum(aapr)) aapr,
                             round(sum(amay)) amay, round(sum(ajun)) ajun, round(sum(ajul)) ajul, round(sum(aaug)) aaug,
                             round(sum(asep)) asep, round(sum(aoct)) aoct, round(sum(anov)) anov, round(sum(adec)) adec,
(round(sum(ajan))+round(sum(afeb))+round(sum(amar))+round(sum(aapr))+round(sum(amay))+round(sum(ajun))+round(sum(ajul))+round(sum(aaug))+round(sum(asep))+round(sum(aoct))+round(sum(anov))+round(sum(adec))) ACC_TOTAL
from
(
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
sum(decode(substr(month_number,5,2),'01',amount,0)) jan,
sum(decode(substr(month_number,5,2),'02',amount,0)) feb,
sum(decode(substr(month_number,5,2),'03',amount,0)) mar,
sum(decode(substr(month_number,5,2),'04',amount,0)) apr,
sum(decode(substr(month_number,5,2),'05',amount,0)) may,
sum(decode(substr(month_number,5,2),'06',amount,0)) jun,
sum(decode(substr(month_number,5,2),'07',amount,0)) jul,
sum(decode(substr(month_number,5,2),'08',amount,0)) aug,
sum(decode(substr(month_number,5,2),'09',amount,0)) sep,
sum(decode(substr(month_number,5,2),'10',amount,0)) oct,
sum(decode(substr(month_number,5,2),'11',amount,0)) nov,
sum(decode(substr(month_number,5,2),'12',amount,0)) dec,
0 ajan,
0 afeb,
0 amar,
0 aapr,
0 amay,
0 ajun,
0 ajul,
0 aaug,
0 asep,
0 aoct,
0 anov,
0 adec
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 cr_epe_charging_cost_center_mv h,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
     (select min (case
            when control_id = 5
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY')
            end) year
            from cr_alloc_system_control) d
	 where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and g.owning_department = h.charging_cost_center
  and h.division = '160'
  and substr(a.month_number,1,4) = d.year
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and upper(trim(h.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,5,2)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 jan,
0 feb,
0 mar,
0 apr,
0 may,
0 jun,
0 jul,
0 aug,
0 sep,
0 oct,
0 nov,
0 dec,
sum(decode(substr(month_number,5,2),'01',amount,0)) ajan,
sum(decode(substr(month_number,5,2),'02',amount,0)) afeb,
sum(decode(substr(month_number,5,2),'03',amount,0)) amar,
sum(decode(substr(month_number,5,2),'04',amount,0)) aapr,
sum(decode(substr(month_number,5,2),'05',amount,0)) amay,
sum(decode(substr(month_number,5,2),'06',amount,0)) ajun,
sum(decode(substr(month_number,5,2),'07',amount,0)) ajul,
sum(decode(substr(month_number,5,2),'08',amount,0)) aaug,
sum(decode(substr(month_number,5,2),'09',amount,0)) asep,
sum(decode(substr(month_number,5,2),'10',amount,0)) aoct,
sum(decode(substr(month_number,5,2),'11',amount,0)) anov,
sum(decode(substr(month_number,5,2),'12',amount,0)) adec
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 cr_epe_charging_cost_center_mv h,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
     (select min (case
            when control_id = 5
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY')
            end) year
            from cr_alloc_system_control) d
	 where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and g.owning_department = h.charging_cost_center
  and h.division = '160'
  and substr(a.month_number,1,4) = d.year
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and upper(trim(h.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  and allocation_type = '7'
  group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,5,2)) s
		 WHERE  (exists (select 	1
						from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 cr_epe_charging_cost_center_mv h,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
     (select min (case
            when control_id = 5
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY')
            end) year
            from cr_alloc_system_control) d
	 where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and g.owning_department = h.charging_cost_center
  and h.division = '160'
  and substr(a.month_number,1,4) = d.year
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and upper(trim(h.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  and e.allocation_type <> 7
  and s.project = a.project   ))
		 GROUP BY s.project,
		 s.description,
         --s.description,
		 allocation_type,
		 budget_rollup_descr
 
 
 
 
 
 ;