
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."LESSEE_SCRIPTS_TO_RUN" ("MAINT_NUM") AS 
  select replace(jira.pkey, 'PP-', '') as maint_num      from nodeassociation@jira lessee, nodeassociation@jira v1041, customfieldvalue@jira c, jiraissue@jira jira      where lessee.sink_node_id = 10501    and lessee.association_type =  'IssueComponent'     and v1041.sink_node_id in (11802, 13000, 13100, 13003)  and v1041.association_type = 'IssueFixVersion'    and v1041.source_node_id = lessee.source_node_id      and c.customfield = 11001    and stringvalue = '12107'      and c.issue = lessee.source_node_id    and jira.id = c.issue     minus      select to_char(maint_num)    from pp_schema_change_log pp  
 
 ;