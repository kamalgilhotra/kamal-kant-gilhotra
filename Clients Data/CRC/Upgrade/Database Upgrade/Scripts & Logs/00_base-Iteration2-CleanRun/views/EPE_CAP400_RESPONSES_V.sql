
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."EPE_CAP400_RESPONSES_V" ("PURGE_COMMENT", "ID", "MONTH_NUMBER", "ANSWERED_BY", "COMMENTS", "COMMENT_DATE", "ASSIGNED_DATE", "CO_ROLLUP_DESCRIPTION", "DIV_DESCRIPTION", "BUDGETING_VALUE", "ACTUALS", "BUDGET", "VARIANCE", "VARIANCE_PERCENT") AS 
  SELECT distinct PURGE_COMMENT, ID,   MONTH_NUMBER, USERS,  COMMENTS,  COMMENT_DATE,  ASSIGNED_DATE,
       co_rollup_description, COL2, COL3, COL4, COL5, COL6, COL7
  FROM PP_USER_COMMENTS_ARCHIVE, CR_EPE_CHARGING_COST_CENTER_MV
  WHERE ALERT_LEVEL = 'CAP400'
    and col2 = div_description
    and col1 in (select distinct dvsn from xxepe_fin_rpt_deliveries_v where upper(deliver_to_user) = upper(user) union
                 select distinct dvsn from xxepe_fin_rpt_deliveries_v d, cr_distrib_users_groups g
                 where dvsn = decode(g.group_id,147,'130',149,'130',137,dvsn,133,dvsn) and upper(user) = upper(g.users))
order by ID, COMMENT_DATE DESC 
 
 
 
 
 
 ;