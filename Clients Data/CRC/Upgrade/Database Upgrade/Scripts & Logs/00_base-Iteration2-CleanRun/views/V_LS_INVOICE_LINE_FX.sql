
  CREATE OR REPLACE VIEW V_LS_INVOICE_LINE_FX as
  WITH cur
       AS (SELECT 1                                    ls_cur_type,
                  contract_cur.currency_id             AS currency_id,
                  contract_cur.currency_display_symbol currency_display_symbol,
                  contract_cur.iso_code                iso_code,
                  1                                    historic_rate
           FROM   currency contract_cur
           UNION
           SELECT 2,
                  company_cur.currency_id,
                  company_cur.currency_display_symbol,
                  company_cur.iso_code,
                  0
           FROM   currency company_cur)
  SELECT lil.invoice_id,
         lil.invoice_line_number,
         lil.payment_type_id,
         lil.ls_asset_id,
         lil.gl_posting_mo_yr,
         lil.amount * Decode(ls_cur_type, 2, nvl(Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg.rate, cr.rate), cur.historic_rate),
                                          1)  amount,
         lil.description,
         lil.notes,
         lease.contract_currency_id,
         cs.currency_id                       company_currency_id,
         cur.ls_cur_type                      AS ls_cur_type,
         nvl(Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg.exchange_date, cr.exchange_date), '01-Jan-1900') exchange_date,
         Decode(ls_cur_type, 2, nvl(Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg.rate, cr.rate), cur.historic_rate),
                             1)               rate,
         cur.iso_code,
         cur.currency_display_symbol
  FROM   ls_invoice_line lil
         inner join ls_invoice li
                 ON ( li.invoice_id = lil.invoice_id )
         inner join ls_lease lease
                 ON li.lease_id = lease.lease_id
         inner join currency_schema cs
                 ON li.company_id = cs.company_id
         inner join cur
                 ON ( ( cur.ls_cur_type = 1
                        AND cur.currency_id = lease.contract_currency_id )
                       OR ( cur.ls_cur_type = 2
                            AND cur.currency_id = cs.currency_id ) )
         left outer join ls_lease_calculated_date_rates cr
                      ON ( cr.company_id = li.company_id
                           AND cr.contract_currency_id =
                               lease.contract_currency_id
                           AND cr.company_currency_id = cs.currency_id
                           AND cr.accounting_month = lil.gl_posting_mo_yr 
                           AND Nvl(cr.exchange_rate_type_id, 1) = 1)
         left outer join ls_lease_calculated_date_rates cr_avg
                      ON ( cr_avg.company_id = li.company_id
                           AND cr_avg.contract_currency_id =
                               lease.contract_currency_id
                           AND cr_avg.company_currency_id = cs.currency_id
                           AND cr_avg.accounting_month = lil.gl_posting_mo_yr 
                           AND Nvl(cr_avg.exchange_rate_type_id, 4) = 4)
         inner join pp_system_control_companies sc
                      ON li.company_id = sc.company_id AND 
                         lower(trim(sc.control_name)) = 'lease mc: use average rates'
  WHERE cs.currency_type_id = 1;
