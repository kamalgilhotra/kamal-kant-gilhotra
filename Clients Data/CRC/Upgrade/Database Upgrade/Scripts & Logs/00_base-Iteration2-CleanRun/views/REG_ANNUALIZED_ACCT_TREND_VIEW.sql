
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."REG_ANNUALIZED_ACCT_TREND_VIEW" ("COMPANY", "ACCT_TYPE", "SUB_ACCT_TYPE", "REG_ACCT", "GL_MONTH", "MONTH_AMT", "ANNUALIZED_AMT", "HIST_OR_FCST", "HISTORIC_VERSION", "FORECAST_VERSION", "ACCT_TYPE_SORT", "SUB_ACCT_TYPE_SORT") AS 
  select COMPANY,
       ACCT_TYPE,
       SUB_ACCT_TYPE,
       REG_ACCT,
       GL_MONTH,
       MONTH_AMT,
       ANNUALIZED_AMT,
       HIST_OR_FCST,
       HISTORIC_VERSION,
       FORECAST_VERSION,
       ACCT_TYPE_SORT,
       SUB_ACCT_TYPE_SORT
  from (select RC.DESCRIPTION COMPANY,
               T.DESCRIPTION ACCT_TYPE,
               S.DESCRIPTION SUB_ACCT_TYPE,
               M.DESCRIPTION REG_ACCT,
               L.GL_MONTH GL_MONTH,
               L.ACT_AMOUNT MONTH_AMT,
               L.ANNUALIZED_AMT ANNUALIZED_AMT,
               'H' HIST_OR_FCST,
               V.LONG_DESCRIPTION HISTORIC_VERSION,
               '' FORECAST_VERSION,
               T.SORT_ORDER ACCT_TYPE_SORT,
               S.SORT_ORDER SUB_ACCT_TYPE_SORT
          from REG_COMPANY          RC,
               REG_ACCT_TYPE        T,
               REG_SUB_ACCT_TYPE    S,
               REG_ACCT_MASTER      M,
               REG_HISTORY_LEDGER   L,
               REG_HISTORIC_VERSION V
         where L.REG_COMPANY_ID = RC.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID
           and M.REG_ANNUALIZATION_ID in (1, 8, 2)
           and L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.HISTORIC_VERSION_ID <> 0
           and L.ANNUALIZED_AMT is not null
        union
        select RC.DESCRIPTION COMPANY,
               T.DESCRIPTION ACCT_TYPE,
               S.DESCRIPTION SUB_ACCT_TYPE,
               M.DESCRIPTION REG_ACCT,
               L.GL_MONTH GL_MONTH,
               L.FCST_AMOUNT MONTH_AMT,
               L.ANNUALIZED_AMT ANNUALIZED_AMT,
               'F' HIST_OR_FCST,
               '' HISTORIC_VERSION,
               V.DESCRIPTION FORECAST_VERSION,
               T.SORT_ORDER ACCT_TYPE_SORT,
               S.SORT_ORDER SUB_ACCT_TYPE_SORT
          from REG_COMPANY          RC,
               REG_ACCT_TYPE        T,
               REG_SUB_ACCT_TYPE    S,
               REG_ACCT_MASTER      M,
               REG_FORECAST_LEDGER  L,
               REG_FORECAST_VERSION V
         where L.REG_COMPANY_ID = RC.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID
           and M.REG_ANNUALIZATION_ID in (1, 8, 2)
           and L.FORECAST_VERSION_ID = V.FORECAST_VERSION_ID
           and L.FORECAST_VERSION_ID <> 0
           and L.ANNUALIZED_AMT is not null)
 order by COMPANY, ACCT_TYPE_SORT, SUB_ACCT_TYPE_SORT, REG_ACCT, GL_MONTH
 
 ;