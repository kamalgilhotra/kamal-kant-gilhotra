CREATE OR REPLACE VIEW V_LS_COMPANY_CURRENCY_FX AS
with lease_co as (
       select /*+ MATERIALIZE */ c.company_id, c.description, cs.currency_id
         from company c
         join currency_schema cs on c.company_id = cs.company_id
        where cs.currency_type_id = 1
     ),
     sc_bs as (
        select /*+ MATERIALIZE */ * 
          from pp_system_control_company
         where trim(lower(control_name)) = 'lease consolidated bs curr type'
     ),
     sc_is as (
        select /*+ MATERIALIZE */ * 
          from pp_system_control_company
         where trim(lower(control_name)) = 'lease consolidated is curr type'
     )
select c.company_id, c.description, cur.currency_id, cur.iso_code, cur.currency_display_symbol, 
       fx.month, fx.currency_from, fx.currency_to,
       fx.act_month_rate, fx.avg_month_rate, fx.act_now_rate,
       calc_rate.accounting_month, calc_rate.act_rate, calc_rate.avg_rate
  from lease_co c
  join currency cur on cur.currency_id = c.currency_id
  join ls_curr_fx_temp fx on fx.currency_to = cur.currency_id
  join (
          select sc_bs.company_id, lower(trim(sc_bs.control_value)) control_value
             from sc_bs
            where sc_bs.company_id <> -1
          union all
          select c.company_id, lower(trim(sc_bs.control_value)) control_value
            from sc_bs
           cross join (select company_id from lease_co
                       minus
                       select company_id from sc_bs b) c
           where sc_bs.company_id = -1
       ) bs_cv on bs_cv.company_id = c.company_id
  join (
          select sc_is.company_id, lower(trim(sc_is.control_value)) control_value
            from sc_is
           where sc_is.company_id <> -1
          union all
          select c.company_id, lower(trim(sc_is.control_value)) control_value
            from sc_is
           cross join (select company_id from lease_co
                       minus
                       select company_id from sc_is b) c
           where sc_is.company_id = -1
       ) is_cv on is_cv.company_id = c.company_id 
  left outer join (
          select cr1.company_id, cr1.contract_currency_id, cr1.company_currency_id, 
                 cr1.accounting_month, cr1.rate act_rate, nvl(cr4.rate, cr1.rate) avg_rate
           from ls_lease_calculated_date_rates cr1
           left outer join (
               select company_id, contract_currency_id, company_currency_id, accounting_month, rate
                 from ls_lease_calculated_date_rates
                where exchange_rate_type_id = 4
                          ) cr4 on cr1.company_id = cr4.company_id
                               and cr1.contract_currency_id = cr4.contract_currency_id
                               and cr1.company_currency_id = cr4.company_currency_id
                               and cr1.accounting_month = cr4.accounting_month
           where cr1.exchange_rate_type_id = 1   
                  ) calc_rate on calc_rate.company_id = c.company_id
                             and calc_rate.contract_currency_id = fx.currency_from
                             and calc_rate.company_currency_id = cur.currency_id
                             and trunc(calc_rate.accounting_month,'fmmonth') = fx.month ;
