
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."BI_EST_INITIAL_VERSION" ("BUDGET_ID", "BUDGET_VERSION_ID") AS 
  select b.budget_id,
   nvl(
      (  select max(ba.budget_version_id)
         from budget_amounts ba, budget_version_view bvv
         where b.company_id = bvv.company_id
         and b.budget_id = ba.budget_id
         and ba.budget_version_id = bvv.working_version
      ),
      nvl(
         (  select max(ba.budget_version_id)
            from budget_amounts ba, budget_version_view bvv
            where b.company_id = bvv.company_id
            and b.budget_id = ba.budget_id
            and ba.budget_version_id = bvv.current_version
         ),
         nvl(
            (  select max(ba.budget_version_id)
               from budget_amounts ba
               where ba.budget_id = b.budget_id
            ),
            0)))
   budget_version_id
from budget b
 
 ;