
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."PT_CALENDAR_APPEALS_VW" ("PROP_TAX_COMPANY_ID", "COMPANY_DESCR", "STATE_ID", "YEAR", "YEAR_DESCR", "DUE_DATE", "COMPLETION_DATE", "PARCEL_ID", "APPEAL_ID", "APPEAL_DESCRIPTION", "RESPONSIBLE_USER", "PRIORITY", "STATUS", "CALENDAR_TYPE") AS 
  select prop_tax_company_id, company_descr, state_id,
year, year_descr, due_date, completion_date,
parcel_id, appeal_id,
appeal_description, responsible_user,
case when appeal_status_id in (3,4) then 'Complete'
     else
      case when days_until_due <= crit then 'Critical'
           when crit < days_until_due and days_until_due <= warn then 'Warning'
           else 'Normal'
      end
end priority,
status,
'Appeal' calendar_type
from (
select distinct pt_parcel.prop_tax_company_id, pt_parcel.state_id, property_tax_year.year
, nvl(pt_parcel_appeal.due_date,sysdate) due_date
, pt_parcel_appeal.resolved_date completion_date
, pt_parcel_appeal.parcel_id
, pt_parcel_appeal.appeal_id
, pt_company.description company_descr
, property_tax_year.description year_descr
, pt_company.description ||': '|| trim(pt_parcel.state_id) ||': '|| property_tax_year.description appeal_description
, nvl(pt_parcel_appeal.assigned_to, nvl(pt_parcel.responsible_user,'Not Assigned')) responsible_user
, pt_parcel_appeal.appeal_status_id
, pt_parcel_appeal_status.description status
, nvl(trim(f_pp_system_control(-1, 'PT Calendar Warning Days Threshold')),21) warn
, nvl(trim(f_pp_system_control(-1, 'PT Calendar Critical Days Threshold')),7) crit
, trunc(nvl(pt_parcel_appeal.due_date,sysdate) - sysdate,0) days_until_due
from pt_parcel_appeal, property_tax_year, pt_parcel, pt_company, pt_parcel_appeal_status
where pt_parcel_appeal.assessment_year_id = property_tax_year.tax_year
and pt_parcel_appeal.parcel_id = pt_parcel.parcel_id
and pt_parcel.prop_tax_company_id = pt_company.prop_tax_company_id
and pt_parcel_appeal.appeal_status_id = pt_parcel_appeal_status.appeal_status_id
)
;