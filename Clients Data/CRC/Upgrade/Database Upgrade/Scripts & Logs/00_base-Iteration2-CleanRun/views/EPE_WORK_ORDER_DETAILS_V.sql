
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."EPE_WORK_ORDER_DETAILS_V" ("WO", "WO_DESCR", "WO_LONG_DESCR", "WO_COMPANY", "FP", "FP_DESCR", "OPERATING_SEGMENT", "BUSINESS_SEGMENT", "WO_STATUS", "WO_TYPE", "MAJOR_LOC", "MINOR_LOC", "GEOCODE", "EST_START_DATE", "EST_COMPLETE_DATE", "EST_IN_SERVICE_DATE", "LATE_CHG_WAIT_PERIOD", "IN_SERVICE_DATE", "COMPLETION_DATE", "INITIATION_DATE", "ALLOCATOR") AS 
  (
select wo.work_order_number wo, wo.description wo_descr, wo.long_description wo_long_descr,
c.gl_company_no wo_company, fp.work_order_number fp, fp.description fp_descr,
d.description operating_segment, d.external_bus_segment business_segment,
upper(e.description) wo_status, upper(f.description) wo_type, upper(g.description) major_loc,
upper(h.long_description) minor_loc, upper(h.grid_coordinate) geocode, wo.est_start_date,
wo.est_complete_date, wo.est_in_service_date, wo.late_chg_wait_period,
wo.in_service_date, wo.completion_date, i.initiation_date,
allocator."VALUE" allocator
from work_order_control fp, work_Order_control wo, company_setup c,
business_segment d, work_Order_status e, work_order_type f,
major_location g, asset_location h, work_order_initiator i,
work_order_class_code allocator
where fp.work_order_id = wo.funding_wo_id
and fp.funding_wo_indicator = 1
and wo.funding_wo_indicator = 0
and wo.company_id = c.company_id
and wo.bus_segment_id = d.bus_segment_id
and wo.wo_status_id = e.wo_status_id
and wo.work_order_type_id = f.work_order_type_id
and wo.major_location_id = g.major_location_id
and wo.asset_location_id = h.asset_location_id
and wo.work_order_id = i.work_order_id
and allocator.class_code_id (+) = 2
and wo.work_order_id = allocator.work_order_id (+))

 
 
 
 
 
 ;