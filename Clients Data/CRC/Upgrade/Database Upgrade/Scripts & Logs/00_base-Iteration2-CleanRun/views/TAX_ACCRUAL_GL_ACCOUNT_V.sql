
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_GL_ACCOUNT_V" ("EXTERNAL_ACCOUNT_CODE", "DESCRIPTION", "LONG_DESCRIPTION", "ACCOUNT_TYPE_ID", "ACCOUNT_TYPE_DESCRIPTION", "EXPENSE_IND") AS 
  select	a.external_account_code,
			a.description,
			a.long_description,
			a.account_type_id,
			b.description account_type_description,
			1 expense_ind
from	gl_account a,
		account_type b
where a.account_type_id = b.account_type_id
	and upper(b.description) like 'POWERTAX%EXPENSE%'
union all
select	a.external_account_code,
			a.description,
			a.long_description,
			a.account_type_id,
			b.description account_type_description,
			0 expense_ind
from	gl_account a,
		account_type b
where a.account_type_id = b.account_type_id
	and upper(b.description) not like 'POWERTAX%EXPENSE%'
 
 
 
 
 ;