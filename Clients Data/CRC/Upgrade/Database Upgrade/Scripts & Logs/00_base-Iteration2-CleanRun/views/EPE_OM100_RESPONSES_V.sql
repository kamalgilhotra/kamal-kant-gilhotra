
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."EPE_OM100_RESPONSES_V" ("PURGE_COMMENT", "ID", "MONTH_NUMBER", "ANSWERED_BY", "COMMENTS", "COMMENT_DATE", "ASSIGNED_DATE", "CO_ROLLUP_DESCRIPTION", "DIV_DESCRIPTION", "DEPT_DESCRIPTION", "CC_DESCRIPTION", "EXPENSE_TYPE", "ACTUALS", "BUDGET", "VARIANCE", "VARIANCE_PERCENT") AS 
  SELECT distinct PURGE_COMMENT, ID,   MONTH_NUMBER, USERS,  COMMENTS,  COMMENT_DATE,  ASSIGNED_DATE,
       co_rollup_description, DIV_DESCRIPTION, DEPT_DESCRIPTION, COL2, COL3, COL4, COL5, COL6, COL7
  FROM PP_USER_COMMENTS_ARCHIVE, CR_EPE_CHARGING_COST_CENTER_MV
  WHERE ALERT_LEVEL = 'OM100'
    and col2 = description
    and col1 in (select distinct cst_cntr from xxepe_fin_rpt_deliveries_v where dvsn in (
                 select distinct dvsn from xxepe_fin_rpt_deliveries_v d, cr_distrib_users_groups g
                 where dvsn = decode(g.group_id,147,'130',149,'130',137,dvsn,133,dvsn) and upper(user) = upper(g.users) union
                 select distinct dvsn from xxepe_fin_rpt_deliveries_v where upper(user) = upper(deliver_to_user)))
order by ID, COMMENT_DATE DESC
 
 
 
 
 
 ;