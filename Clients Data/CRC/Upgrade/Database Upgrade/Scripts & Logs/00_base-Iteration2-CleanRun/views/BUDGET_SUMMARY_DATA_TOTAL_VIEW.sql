
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."BUDGET_SUMMARY_DATA_TOTAL_VIEW" ("BUDGET_ID", "BUDGET_VERSION_ID", "APPROVED_DOLLARS", "HIST_ACTUALS", "CURRENT_YEAR_ACTUALS", "CURRENT_YEAR_DOLLARS", "YEAR2_DOLLARS", "YEAR3_DOLLARS", "YEAR4_DOLLARS", "YEAR5_DOLLARS", "YEAR6_DOLLARS", "YEAR7_DOLLARS", "YEAR8_DOLLARS", "YEAR9_DOLLARS", "YEAR10_DOLLARS", "YEAR11_DOLLARS", "YEAR12_DOLLARS", "YEAR13_DOLLARS", "YEAR14_DOLLARS", "YEAR15_DOLLARS", "FUTURE_DOLLARS") AS 
  select budget_id, budget_version_id,   			 sum(approved_dollars) approved_dollars,   			 sum(hist_actuals) hist_actuals,   			 sum(current_year_actuals) current_year_actuals,  			 sum(current_year_dollars) current_year_dollars,  			 sum(year2_dollars) year2_dollars, sum(year3_dollars) year3_dollars,   		    sum(year4_dollars) year4_dollars,sum(year5_dollars) year5_dollars,   			 sum(year6_dollars) year6_dollars, sum(year7_dollars) year7_dollars,  			 sum(year8_dollars) year8_dollars, sum(year9_dollars) year9_dollars,   			 sum(year10_dollars) year10_dollars,sum(year11_dollars) year11_dollars,  			 sum(year12_dollars) year12_dollars, sum(year13_dollars) year13_dollars,  			 sum(year14_dollars) year14_dollars, sum(year15_dollars) year15_dollars,   			 sum(future_dollars) future_dollars   	  from budget_summary_data group by budget_id, budget_version_id
 
 
 
 
 
 ;