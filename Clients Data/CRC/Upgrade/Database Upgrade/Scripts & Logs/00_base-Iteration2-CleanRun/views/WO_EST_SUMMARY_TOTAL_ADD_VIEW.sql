
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_EST_SUMMARY_TOTAL_ADD_VIEW" ("REVISION", "WORK_ORDER_ID", "BUDGET_VERSION_ID", "APPROVED_DOLLARS", "CURRENT_YEAR_DOLLARS", "YEAR2_DOLLARS", "YEAR3_DOLLARS", "YEAR4_DOLLARS", "YEAR5_DOLLARS", "YEAR6_DOLLARS", "YEAR7_DOLLARS", "YEAR8_DOLLARS", "YEAR9_DOLLARS", "YEAR10_DOLLARS", "YEAR11_DOLLARS", "FUTURE_DOLLARS") AS 
  (SELECT wo_est_monthly."REVISION",
       wo_est_monthly."WORK_ORDER_ID",
       budget_version_fund_proj.budget_version_id,
       sum("WO_EST_MONTHLY".total) APPROVED_DOLLARS,
       sum(decode(wo_est_monthly.year, budget_version.start_year, total, 0)) CURRENT_YEAR_DOLLARS,
       sum(decode(wo_est_monthly.year, budget_version.start_year + 1, total, 0)) YEAR2_DOLLARS,
       sum(decode(wo_est_monthly.year, budget_version.start_year + 2, total, 0)) YEAR3_DOLLARS,
       sum(decode(wo_est_monthly.year, budget_version.start_year + 3, total, 0)) YEAR4_DOLLARS,
       sum(decode(wo_est_monthly.year, budget_version.start_year + 4, total, 0)) YEAR5_DOLLARS,
       sum(decode(wo_est_monthly.year, budget_version.start_year + 5, total, 0)) YEAR6_DOLLARS,
       sum(decode(wo_est_monthly.year, budget_version.start_year + 6, total, 0)) YEAR7_DOLLARS,
       sum(decode(wo_est_monthly.year, budget_version.start_year + 7, total, 0)) YEAR8_DOLLARS,
       sum(decode(wo_est_monthly.year, budget_version.start_year + 8, total, 0)) YEAR9_DOLLARS,
       sum(decode(wo_est_monthly.year, budget_version.start_year + 9, total, 0)) YEAR10_DOLLARS,
       sum(decode(wo_est_monthly.year, budget_version.start_year + 10, total, 0)) YEAR11_DOLLARS,
       sum(decode(sign(wo_est_monthly.year - (budget_version.start_year + 10)), 1,
                  total, 0)) FUTURE_DOLLARS
  FROM "WO_EST_MONTHLY",
       budget_version,
       budget_version_fund_proj
 where wo_est_monthly.revision = budget_version_fund_proj.revision
   and budget_version_fund_proj.work_order_id = wo_est_monthly.work_order_id
   and budget_version_fund_proj.active = 1
   and budget_version.budget_version_id =
       budget_version_fund_proj.budget_version_id
   and wo_est_monthly.expenditure_type_id = 1
 group by "WO_EST_MONTHLY"."REVISION",
          "WO_EST_MONTHLY"."WORK_ORDER_ID",
          budget_version_fund_proj.budget_version_id)
 
 
 
 
 
 ;