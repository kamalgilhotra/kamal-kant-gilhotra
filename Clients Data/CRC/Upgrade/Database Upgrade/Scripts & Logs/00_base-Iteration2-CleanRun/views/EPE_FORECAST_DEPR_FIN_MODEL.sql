
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."EPE_FORECAST_DEPR_FIN_MODEL" ("FCST_DEPR_VERSION_ID", "YEAR", "ACCOUNT", "OP_SEG", "UNIT", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC") AS 
  SELECT Z.FCST_DEPR_VERSION_ID,
       Z.YEAR,
       Z.ACCOUNT,
       Z.OP_SEG,
       Z.UNIT,
       SUM(Z."1") AS JAN,
       SUM(Z."2") AS FEB,
       SUM(Z."3") AS MAR,
       SUM(Z."4") AS APR,
       SUM(Z."5") AS MAY,
       SUM(Z."6") AS JUN,
       SUM(Z."7") AS JUL,
       SUM(Z."8") AS AUG,
       SUM(Z."9") AS SEP,
       SUM(Z."10") AS OCT,
       SUM(Z."11") AS NOV,
       SUM(Z."12") AS DEC
  FROM (SELECT TO_NUMBER(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'YYYYMM')) AS MONTH_NUMBER,
               FCST_DEPR_LEDGER.FCST_DEPR_VERSION_ID,
               TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'YYYY') AS YEAR,
               'PlantDeprExp' AS ACCOUNT,
               CASE
                  WHEN FCST_DEPR_GROUP.BUS_SEGMENT_ID > 902 THEN
                   900
                  WHEN FCST_DEPR_GROUP.BUS_SEGMENT_ID = 902 THEN
                   901
                  ELSE
                   FCST_DEPR_GROUP.BUS_SEGMENT_ID
               END as OP_SEG,
               CASE
                  WHEN FCST_DEPR_GROUP.BUS_SEGMENT_ID = 900 AND
                       FCST_DEPR_GROUP.COMPANY_ID = 9 THEN
                   '9710'
                  WHEN FCST_DEPR_GROUP.BUS_SEGMENT_ID = 901 AND
                       FCST_DEPR_GROUP.COMPANY_ID = 9 THEN
                   '9810'
                  WHEN FCST_DEPR_GROUP.BUS_SEGMENT_ID > 902 AND
                       FCST_DEPR_GROUP.COMPANY_ID = 9 THEN
                   '9710'
                  ELSE
                   CR_EPE_CHARGING_COST_CENTER_MV.COST_CENTER
               END AS UNIT,
               DECODE(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'MM'), '01',
                      FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                      FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST, 0) AS "1",
               DECODE(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'MM'), '02',
                      FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                      FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST, 0) AS "2",
               DECODE(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'MM'), '03',
                      FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                      FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST, 0) AS "3",
               DECODE(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'MM'), '04',
                      FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                      FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST, 0) AS "4",
               DECODE(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'MM'), '05',
                      FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                      FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST, 0) AS "5",
               DECODE(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'MM'), '06',
                      FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                           FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST, 0) AS "6",
               DECODE(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'MM'), '07',
                      FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                           FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST, 0) AS "7",
               DECODE(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'MM'), '08',
                      FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                           FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST, 0) AS "8",
               DECODE(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'MM'), '09',
                      FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                           FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST, 0) AS "9",
               DECODE(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'MM'), '10',
                      FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                           FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST, 0) AS "10",
               DECODE(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'MM'), '11',
                      FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                           FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST, 0) AS "11",
               DECODE(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'MM'), '12',
                      FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                           FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST, 0) AS "12"
          FROM FCST_DEPR_GROUP,
               FCST_DEPR_LEDGER,
               CR_EPE_CHARGING_COST_CENTER_MV,
               COMPANY,
               BUSINESS_SEGMENT
         WHERE FCST_DEPR_GROUP.FCST_DEPR_GROUP_ID =
               FCST_DEPR_LEDGER.FCST_DEPR_GROUP_ID
           AND COMPANY.COMPANY_ID = FCST_DEPR_GROUP.COMPANY_ID
           AND BUSINESS_SEGMENT.BUS_SEGMENT_ID = FCST_DEPR_GROUP.BUS_SEGMENT_ID
           AND CR_EPE_CHARGING_COST_CENTER_MV.COMPANY = COMPANY.GL_COMPANY_NO
           AND CR_EPE_CHARGING_COST_CENTER_MV.BUSINESS_SEGMENT = BUSINESS_SEGMENT.EXTERNAL_BUS_SEGMENT
           AND UPPER(CR_EPE_CHARGING_COST_CENTER_MV.ELEMENT_TYPE) = 'BUDGET'
         HAVING SUM(FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE +
                   FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST) <> 0
         GROUP BY TO_NUMBER(TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'YYYYMM')),
                  FCST_DEPR_LEDGER.FCST_DEPR_VERSION_ID,
                  TO_CHAR(FCST_DEPR_LEDGER.GL_POST_MO_YR, 'YYYY'),
                  CASE
                     WHEN FCST_DEPR_GROUP.BUS_SEGMENT_ID > 902 THEN
                      900
                     WHEN FCST_DEPR_GROUP.BUS_SEGMENT_ID = 902 THEN
                      901
                     ELSE
                      FCST_DEPR_GROUP.BUS_SEGMENT_ID
                  END,
                  CASE
                     WHEN FCST_DEPR_GROUP.BUS_SEGMENT_ID = 900 AND
                          FCST_DEPR_GROUP.COMPANY_ID = 9 THEN
                      '9710'
                     WHEN FCST_DEPR_GROUP.BUS_SEGMENT_ID = 901 AND
                          FCST_DEPR_GROUP.COMPANY_ID = 9 THEN
                      '9810'
                     WHEN FCST_DEPR_GROUP.BUS_SEGMENT_ID > 902 AND
                          FCST_DEPR_GROUP.COMPANY_ID = 9 THEN
                      '9710'
                     ELSE
                      CR_EPE_CHARGING_COST_CENTER_MV.COST_CENTER
                  END,
                  FCST_DEPR_LEDGER.GL_POST_MO_YR,
                  FCST_DEPR_LEDGER.DEPRECIATION_EXPENSE,
                  FCST_DEPR_LEDGER.DEPR_EXP_ALLOC_ADJUST) Z
 GROUP BY Z.FCST_DEPR_VERSION_ID,
          Z.YEAR,
          Z.ACCOUNT,
          Z.OP_SEG,
          Z.UNIT
 ORDER BY Z.YEAR,
          Z.OP_SEG,
          Z.UNIT
 
 
 
 
 
 ;