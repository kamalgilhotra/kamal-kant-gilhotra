CREATE OR REPLACE VIEW v_lsr_ilr_rates_compounded AS
SELECT  lsr_ilr_rates.ilr_id,
        lsr_ilr_rates.revision,
		lsr_ilr_rates.set_of_books_id,
        lsr_ilr_rates.rate_type_id,
        lsr_ilr_rate_types.DESCRIPTION AS rate_type_description,
        pkg_financial_calcs.f_annual_to_implicit_rate(lsr_ilr_rates.rate,2) AS rate_implicit,
        rate AS annual_discount_rate
FROM lsr_ilr_rates
JOIN lsr_ilr_rate_types ON lsr_ilr_rates.rate_type_id = lsr_ilr_rate_types.rate_type_id;
