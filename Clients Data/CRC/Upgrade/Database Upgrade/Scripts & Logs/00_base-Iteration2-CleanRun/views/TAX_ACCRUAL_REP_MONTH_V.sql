
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_REP_MONTH_V" ("MONTH_LABEL_1", "MONTH_LABEL_2", "MONTH_LABEL_COMP", "MONTH_LABEL_SPAN", "YTD_START_MO", "YTD_END_MO", "VERSION_LABEL", "VERSION_LABEL_COMP", "VERSION_LABEL_SPAN") AS 
  select	max(month_label_1) month_label_1,
			max(month_label_2) month_label_2,
			max(month_label_comp) month_label_comp,
			max(month_label_span) month_label_span,
			max(ytd_start_mo) ytd_start_mo,
			max(ytd_end_mo) ytd_end_mo,
			max(version_label) version_label,
			max(version_label_comp) version_label_comp,
			max(version_label_span) version_label_span
from	(	select	description month_label_1,
						' ' month_label_2,
						' ' month_label_comp,
						' ' month_label_span,
						0 ytd_start_mo,
						0 ytd_end_mo,
						' ' version_label,
						' ' version_label_comp,
						' ' version_label_span
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'MONTH_LABEL_1'
			union all
			select	' ' month_label_1,
						description month_label_2,
						' ' month_label_comp,
						' ' month_label_span,
						0 ytd_start_mo,
						0 ytd_end_mo,
						' ' version_label,
						' ' version_label_comp,
						' ' version_label_span
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'MONTH_LABEL_2'
			union all
			select	' ' month_label_1,
						' ' month_label_2,
						description month_label_comp,
						' ' month_label_span,
						0 ytd_start_mo,
						0 ytd_end_mo,
						' ' version_label,
						' ' version_label_comp,
						' ' version_label_span
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'MONTH_LABEL_COMP'
			union all
			select	' ' month_label_1,
						' ' month_label_2,
						' ' month_label_comp,
						description month_label_span,
						0 ytd_start_mo,
						0 ytd_end_mo,
						' ' version_label,
						' ' version_label_comp,
						' ' version_label_span
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'MONTH_LABEL_SPAN'
			union all
			select	' ' month_label_1,
						' ' month_label_2,
						' ' month_label_comp,
						' ' month_label_span,
						id ytd_start_mo,
						0 ytd_end_mo,
						' ' version_label,
						' ' version_label_comp,
						' ' version_label_span
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'YTD_START_MO'
			union all
			select	' ' month_label_1,
						' ' month_label_2,
						' ' month_label_comp,
						' ' month_label_span,
						0 ytd_start_mo,
						id ytd_end_mo,
						' ' version_label,
						' ' version_label_comp,
						' ' version_label_span
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'YTD_END_MO'
			union all
			select	' ' month_label_1,
						' ' month_label_2,
						' ' month_label_comp,
						' ' month_label_span,
						0 ytd_start_mo,
						0 ytd_end_mo,
						description version_label,
						' ' version_label_comp,
						' ' version_label_span
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'VERSION_LABEL'
			union all
			select	' ' month_label_1,
						' ' month_label_2,
						' ' month_label_comp,
						' ' month_label_span,
						0 ytd_start_mo,
						0 ytd_end_mo,
						' ' version_label,
						description version_label_comp,
						' ' version_label_span
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'VERSION_LABEL_COMP'
			union all
			select	' ' month_label_1,
						' ' month_label_2,
						' ' month_label_comp,
						' ' month_label_span,
						0 ytd_start_mo,
						0 ytd_end_mo,
						' ' version_label,
						' ' version_label_comp,
						description version_label_span
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'VERSION_LABEL_SPAN'
		)
 
 
 
 
 ;