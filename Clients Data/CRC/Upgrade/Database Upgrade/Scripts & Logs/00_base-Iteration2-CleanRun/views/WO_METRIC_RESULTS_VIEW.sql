
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_METRIC_RESULTS_VIEW" ("METRIC", "METRIC_DETAIL", "MONTH", "GL_COMPANY_NO", "COUNT", "DOLLARS", "COLUMN1", "COLUMN2", "COLUMN3", "COLUMN4", "COLUMN5", "COLUMN6", "COMPANY_ID", "METRIC_ID") AS 
  SELECT c.description,
       d.description,
       a.MONTH,
       b.gl_company_no,
       a.COUNT,
       a.dollars,
       a.column1,
       a.column2,
       a.column3,
       a.column4,
       a.column5,
       a.column6,
       a.company_id,
       a.metric_id
  FROM pwrplant.wo_metric_results a,
       pwrplant.company           b,
       pwrplant.wo_metric         c,
       pwrplant.wo_metric_detail  d
 WHERE a.company_id = b.company_id
   AND a.metric_id = c.metric_id
   AND a.metric_id = d.metric_id
   AND a.metric_detail_id = d.metric_detail_id
 
 
 
 
 
 ;