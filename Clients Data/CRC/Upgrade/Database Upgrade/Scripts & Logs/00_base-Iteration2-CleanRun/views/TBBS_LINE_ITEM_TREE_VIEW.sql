
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TBBS_LINE_ITEM_TREE_VIEW" ("PP_TREE_TYPE_ID", "PARENT_ID", "NODE_ID", "DESCRIPTION", "SORT_ORDER", "MANDATORY_LEAF", "TREE_LEVEL") AS 
  SELECT pp_tree_type_id, to_number(parent_id) AS parent_id, child_id AS node_id, node_desc AS description, tree_order AS sort_order, DECODE(empty_node, 0, 1, 1, 0) AS
	mandatory_leaf, LEVEL AS tree_level
FROM
(SELECT A.pp_tree_type_id, A.empty_node, DECODE(A.parent_id, A.child_id, NULL, A.parent_id) AS parent_id, A.child_id, DECODE(A.empty_node, 0, tli.description, DECODE(
  A.parent_id, A.child_id, A.override_parent_description, b.override_parent_description ) ) node_desc, A.sort_order tree_order
FROM pp_tree a
LEFT OUTER JOIN
  ( SELECT DISTINCT pp_tree_type_id, parent_id, override_parent_description
  FROM pp_tree
  WHERE parent_id <> child_id
  ) b
ON b.pp_tree_type_id = a.pp_tree_type_id
AND b.parent_id      = A.child_id
AND b.parent_id     <> A.parent_id
LEFT JOIN tbbs_line_item tli
ON A.empty_node           = 0
AND A.child_id            = tli.line_item_id
WHERE pp_tree_category_id = 4
)
CONNECT BY PRIOR child_id = parent_id
AND PRIOR pp_tree_type_id   = pp_tree_type_id
START WITH parent_id     IS NULL
ORDER SIBLINGS BY tree_order
 ;