CREATE OR REPLACE FORCE VIEW v_ls_ilr_mc_purch_opt AS
WITH cur
      AS ( SELECT ls_currency_type_id AS ls_cur_type,
                  currency_id,
                  currency_display_symbol,
                  iso_code,
                  CASE ls_currency_type_id
                    WHEN 1 THEN 1
                    ELSE NULL
                  END                 AS contract_approval_rate
          FROM   CURRENCY
                  cross join LS_LEASE_CURRENCY_TYPE ),
      calc_rate
      AS (  SELECT Nvl(a.company_id, b.company_id) company_id,
                   Nvl(a.contract_currency_id, b.contract_currency_id) contract_currency_id,
                   Nvl(a.company_currency_id, b.company_currency_id) company_currency_id,
                   Nvl(a.accounting_month, Add_Months(b.accounting_month,1)) accounting_month,
                   nvl(a.exchange_rate_type_id, b.exchange_rate_type_id) exchange_rate_type_id,
                   Nvl(a.exchange_date, b.exchange_date) exchange_date,
                   a.rate,
                   b.rate prev_rate
            FROM ls_lease_calculated_date_rates a
              FULL OUTER JOIN ls_lease_calculated_date_rates b ON a.company_id = b.company_id
              AND a.contract_currency_id = b.contract_currency_id
              AND a.accounting_month = add_months(b.accounting_month,1)
              and b.exchange_rate_type_id = a.exchange_rate_type_id)
SELECT
        ilr.company_id                                                                                                  company_id,
        liasob.set_of_books_id                                                                                             set_of_books_id,
        lease.lease_group_id                                                                                            lease_group,
        ilr.ilr_id                                                                                                      ilr_id,
        po.purchase_date                                                                                               month,
        opt.lease_cap_type_id                                                                                         lease_cap_type,
        CUR.ls_cur_type                                                                                                 AS ls_cur_type,
        cr.exchange_date                                                                                                exchange_date,
        CALC_RATE.exchange_date                                                                                         prev_exchange_date,
        lease.contract_currency_id                                                                                      contract_currency_id,
        CUR.currency_id                                                                                                 display_currency_id,
        cr.rate                                                                                                         rate,
        CALC_RATE.rate                                                                                                  calculated_rate,
        CUR.iso_code                                                                                                    iso_code,
        CUR.currency_display_symbol                                                                                     currency_display_symbol,
        po.purchase_amt * Nvl( CALC_RATE.rate, cr.rate )                                                                purchase_option_amt,
        po.ilr_purchase_option_id                                                                                       ilr_purchase_option_id,
        po.ilr_purchase_probability_id                                                                                  ilr_purchase_probability_id,
        po.decision_notice                                                                                              decision_notice ,
        po.decision_date                                                                                                decision_date,
        po.purchase_date                                                                                                purchase_date,
        po.purchase_option_type                                                                                         purchase_option_type
        FROM
        LS_ILR ilr
        inner JOIN LS_ILR_OPTIONS opt
                ON ilr.ilr_id = opt.ilr_id
        inner JOIN LS_ILR_AMOUNTS_SET_OF_BOOKS liasob
                ON ilr.ilr_id = liasob.ilr_id AND ilr.current_revision = liasob.revision
        inner join LS_LEASE lease
                ON ilr.lease_id = lease.lease_id
        inner join CURRENCY_SCHEMA cs
                ON ilr.company_id = cs.company_id
        inner join cur
                ON CUR.currency_id = CASE CUR.ls_cur_type
                                      WHEN 1 THEN lease.contract_currency_id
                                      WHEN 2 THEN cs.currency_id
                                      ELSE NULL
                                    END
        inner join ls_ilr_purchase_options po
                ON ilr.ilr_id = po.ilr_id
                and opt.revision = po.revision
                and liasob.revision = po.revision
        inner join CURRENCY_RATE_DEFAULT_DENSE cr
                ON CUR.currency_id = cr.currency_to AND
                  lease.contract_currency_id = cr.currency_from AND
                  Trunc( cr.exchange_date, 'MONTH' ) = Trunc(po.purchase_date, 'MONTH' )
        left outer join calc_rate
                    ON lease.contract_currency_id = CALC_RATE.contract_currency_id AND
                        CUR.currency_id = CALC_RATE.company_currency_id AND
                        ilr.company_id = CALC_RATE.company_id AND
                        po.purchase_date = CALC_RATE.accounting_month
                        and calc_rate.exchange_rate_type_id = 1
        WHERE  cs.currency_type_id = 1 AND
               cr.exchange_rate_type_id = 1;