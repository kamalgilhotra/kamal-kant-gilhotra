
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CR_OM500_ALERTS_V" ("COMPANY_ROLLUP", "COMPANY_ROLLUP_DESC", "ACTUALSYTD", "BUDGETYTD", "VARIANCE", "VARPERCENT", "PROJECTED_ACTUALS", "PROJECTED_BUDGET", "VARIANCE2", "VARPERCENT2", "CURRENT_MONTH", "DELIVER_TO_USER") AS 
  SELECT      ffs.co,
            ffs.company||decode(ffs.co,'99',to_char(null),' ('||initcap(pp.last_name)||')') company,
			round(sum(actualsytd)) actualsytd,
			round(sum(budgetytd)) budgetytd,
			round(sum(actualsytd)) - round(sum(budgetytd)) Variance,					          round(case when round(sum(budgetytd)) = 0 then 100
else (round((sum(actualsytd)) - round(sum(budgetytd)))/round(sum(budgetytd)))*100
                              end,1) Varpercent,
			round(sum(projected_actuals)) projected_actuals,
			round(sum(projected_budget)) projected_budget,
			round(sum(projected_actuals)) - round(sum(projected_budget)) Variance2,
							  round(case when round(sum(projected_budget)) = 0 then 100
else ((round(sum(projected_actuals)) - round(sum(projected_budget)))/round(sum(projected_budget)))*100 end,1) Varpercent2,
	    (select min (case
            when control_id = 5
            then to_char(to_date(control_value, 'YYYYMM'), 'fmMonth YYYY')
            end)
            from cr_alloc_system_control) Current_Month, d.deliver_to_user
FROM pwrplant.xxepe_fin_rpt_deliveries d, pwrplant.xxepe_fin_rpt_deliveries d2, pwrplant.pp_security_users pp, (
select 	    c.company_rollup co,
            c.co_rollup_description company,
			sum (a.amount) actualsytd,
			0 budgetytd,
			0 projected_actuals,
			0 projected_budget
from 		cr_epe_charging_cost_center_mv c,
			cr_cost_repository_sv a,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d, cr_epe_account_mv e
where	a.charging_cost_center = c.charging_cost_center
            and a.account = e.account
			and a.month_number between d.start_month and d.end_month
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and e.acct_group between 'A' and 'E'
group by  c.company_rollup,c.co_rollup_description
union
select 	    c.company_rollup co,
            c.co_rollup_description company,
			0 actualsytd,
			sum(a.amount) budgetytd,
			0 projected_actuals,
			0 projected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,	cr_epe_account_mv e
where 	a.charging_cost_center = c.charging_cost_center
		    and a.ferc_account = e.account
			and a.budget_version = b.control_value
			and a.month_number between d.start_month and d.end_month
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and e.acct_group between 'A' and 'E'
			group	 by	c.company_rollup,
            c.co_rollup_description
union
select 	    c.company_rollup co,
            c.co_rollup_description company,
			0 actuals_amount,
			0 budget_amount,
			sum(a.amount) projected_actuals,
			0 projected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'REVISED BUDGET VERSION') b,
			(select min (case
                        when control_id = 5
                        then substr(control_value,1,4)
                        end
                        )as allyear
                        from cr_alloc_system_control) d,
			cr_epe_account_mv e
where 	a.charging_cost_center = c.charging_cost_center
            and a.ferc_account = e.account
			and a.budget_version = b.control_value
			and substr(a.month_number,1,4) = d.allyear
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and e.acct_group between 'A' and 'E'
group	 by	 c.company_rollup,
             c.co_rollup_description
union
select      c.company_rollup co,
            c.co_rollup_description company,
			0 actuals_amount,
			0 budget_amount,
			0 projected_actuals,
			sum(a.amount) projected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
                        when control_id = 5
                        then substr(control_value,1,4)
                        end
                        )as allyear
                        from cr_alloc_system_control) d,
			cr_epe_account_mv e
where 	a.charging_cost_center = c.charging_cost_center
		    and a.ferc_account = e.account
			and a.budget_version = b.control_value
			and substr(a.month_number,1,4) = d.allyear
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and e.acct_group between 'A' and 'E'
group	 by	 c.company_rollup,
             c.co_rollup_description) ffs
where ffs.co = d.company_rollup
  and d.report_level in ('A','B')
  and ffs.co = d2.company_rollup
  and d2.report_level in ('A','B')
  and d2.source = 'H'
  and d2.deliver_to_user = pp.users
GROUP BY  d.deliver_to_user, ffs.co,ffs.company||decode(ffs.co,'99',to_char(null),' ('||initcap(pp.last_name)||')')
 
 
 
 
 
 ;