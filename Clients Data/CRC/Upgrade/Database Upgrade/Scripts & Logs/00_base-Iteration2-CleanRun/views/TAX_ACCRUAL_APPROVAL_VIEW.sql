
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_APPROVAL_VIEW" ("COMPANY_ID", "APPROVAL_TYPE_ID", "DESCRIPTION", "AUTH_LEVEL", "AUTH_LEVEL_NAME", "USERS") AS 
  select 	company_id,
			approval_view.approval_type_id,
			approval_view.description,
			approval_view.auth_level,
			approval_view.auth_level_name,
			users
from	company_approval_auth_level,
		(	select 	approval_type_id,
						description,
						1 auth_level,
						auth_level1 auth_level_name
			from approval
			union
			select 	approval_type_id,
						description,
						2 auth_level,
						auth_level2
			from approval
			union
			select	approval_type_id,
						description,
						3 auth_level,
						auth_level3
			from approval
			union
			select 	approval_type_id,
						description,
						4 auth_level,
						auth_level4
			from approval
			union
			select 	approval_type_id,
						description,
						5 auth_level,
						auth_level5
			from approval
			union
			select	approval_type_id,
						description,
						6 auth_level,
						auth_level6
			from approval
			union
			select 	approval_type_id,
						description,
						7 auth_level,
						auth_level7
			from approval
			union
			select 	approval_type_id,
						description,
						8 auth_level,
						auth_level8
			from approval
			union
			select 	approval_type_id,
						description,
						9 auth_level,
						auth_level9
			from approval
			union
			select 	approval_type_id,
						description,
						10 auth_level,
						auth_level10
			from approval
		) approval_view
where approval_view.approval_type_id = company_approval_auth_level.approval_type_id
	and approval_view.auth_level = company_approval_auth_level.auth_level

 
 
 
 
 ;