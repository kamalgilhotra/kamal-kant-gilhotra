
CREATE OR REPLACE VIEW v_ls_asset_impairment_amts (
  ilr_id,
  revision,
  ls_asset_id,
  set_of_books_id,
  impairment_date,
  current_revision,
  is_impairment,
  prior_month_end_depr_reserve,
  prior_month_end_nbv,
  prior_month_end_capital_cost,
  prior_month_net_rou_asset,
  prior_month_end_accum_impair
) AS
SELECT ilr_id, revision, ls_asset_id, set_of_books_id, impairment_date,
       current_revision, is_impairment, prior_month_end_depr_reserve,
       prior_month_end_nbv, prior_month_end_capital_cost,
       prior_month_net_rou_asset, prior_month_end_accum_impair
  FROM (SELECT stg.ilr_id, stg.revision, sch.ls_asset_id, sch.set_of_books_id,
                o.impairment_date, ilr.current_revision,
                o.is_impairment is_impairment,
                depr.end_reserve AS prior_month_end_depr_reserve,
                sch.end_capital_cost - depr.end_reserve AS prior_month_end_nbv,
                sch.end_capital_cost AS prior_month_end_capital_cost,
                sch.end_net_rou_asset as prior_month_net_rou_asset,
                sch.end_accum_impair as prior_month_end_accum_impair
           FROM ls_ilr_stg stg
           JOIN ls_ilr ilr ON stg.ilr_id = ilr.ilr_id
           JOIN ls_ilr_options o ON stg.ilr_id = o.ilr_id
                                AND stg.revision = o.revision
           JOIN ls_ilr_options prior_opt ON stg.ilr_id = prior_opt.ilr_id
                                        AND prior_opt.revision =
                                            ilr.current_revision
           JOIN ls_ilr_asset_map map ON map.ilr_id = stg.ilr_id
                                    AND map.revision = ilr.current_revision
           JOIN ls_asset_schedule sch ON map.ls_asset_id = sch.ls_asset_id
                                     AND sch.revision = ilr.current_revision
                                     AND sch.set_of_books_id =
                                         stg.set_of_books_id
           left OUTER JOIN ls_depr_forecast depr ON depr.ls_asset_id =
                                                    map.ls_asset_id
                                                AND depr.revision =
                                                    ilr.current_revision
                                                AND depr.set_of_books_id =
                                                    stg.set_of_books_id
           JOIN ls_lease mla ON mla.lease_id = ilr.lease_id
          WHERE ilr.current_revision <> stg.revision
            AND o.is_impairment IS NOT NULL
            AND Trunc(o.impairment_date, 'month') = Add_Months(sch.MONTH, 1)
            AND (Trunc(o.impairment_date, 'month') =
                Add_Months(depr.MONTH, 1) OR depr.MONTH IS NULL))



