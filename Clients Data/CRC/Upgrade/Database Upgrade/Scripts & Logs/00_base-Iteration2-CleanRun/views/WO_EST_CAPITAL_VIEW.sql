
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_EST_CAPITAL_VIEW" ("WORK_ORDER_ID", "REVISION", "CAPITAL") AS 
  ( select work_order_id, revision, sum(capital) capital from ( select a.work_order_id,
nvl(a.revision, 1) revision,
nvl(sum(b.total), 0) capital from wo_est_first_view a, wo_est_monthly b
where a.work_order_id = b.work_order_id (+) and
                   a.revision = b.revision (+) and
                   b.expenditure_type_id (+) = 1 and
       b.est_chg_type_id (+) is null group by a.work_order_id, a.revision
union
 select a.work_order_id, a.revision, sum(b.total) from wo_est_first_view a,
wo_est_monthly b where a.work_order_id = b.work_order_id (+) and
a.revision = b.revision (+) and
b.expenditure_type_id (+) = 1 and
b.est_chg_type_id in
(select est_chg_type_id from estimate_charge_type
where processing_type_id = 4) group by a.work_order_id, a.revision)
group by work_order_id, revision )

 
 
 
 
 
 ;