
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CR_OM200_9499_V" ("DEPARTMENT", "DEPARTMENT_DESC", "CHARGING_COST_CENTER", "CHARGING_COST_CENTER_DESC", "ACTUALSYTD", "BUDGETYTD", "VARIANCE", "VARPERCENT", "PROJECTED_ACTUALS", "PROJECTED_BUDGET", "VARIANCE2", "VARPERCENT2", "CURRENT_MONTH") AS 
  SELECT	department,
			department_desc,
			charging_cost_center,
			charging_cost_center_desc,
			round(sum(actualsytd)) actualsytd,
			round(sum(budgetytd)) budgetytd,
			round(sum(actualsytd)) - round(sum(budgetytd)) Variance,
			    round(case when round(sum(budgetytd)) = 0
			            and round(sum(actualsytd)) = 0 then 0
						when round(sum(budgetytd)) = 0
			            and round(sum(actualsytd)) != 0 then 100 * sign(sum(actualsytd))
						else (round((sum(actualsytd)) - round(sum(budgetytd)))/round(sum(budgetytd)))*100
                              end,1) Varpercent,
			round(sum(projected_actuals)) projected_actuals,
			round(sum(projected_budget)) projected_budget,
			round(sum(projected_actuals)) - round(sum(projected_budget)) Variance2,
	             round(case when round(sum(projected_budget)) = 0
			            and round(sum(projected_actuals)) = 0 then 0
						when round(sum(projected_budget)) = 0
			            and round(sum(projected_actuals)) != 0 then 100 * sign(sum(projected_actuals))
						else (round((sum(projected_actuals)) - round(sum(projected_budget)))/round(sum(projected_budget)))*100
                              end,1) Varpercent2,
			(select min (case
            when control_id = 5
            then to_char(to_date(control_value, 'YYYYMM'), 'fmMonth YYYY')
            end)
            from cr_alloc_system_control) Current_Month
FROM (
select 			c.department department,
         		c.dept_description department_desc,
			c.charging_cost_center charging_cost_center,
			c.description charging_cost_center_desc,
			sum (a.amount) actualsytd,
			0 budgetytd,
			0 projected_actuals,
			0 projected_budget
from 		cr_epe_charging_cost_center_mv c,
			cr_cost_repository_sv a,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			cr_epe_account_mv e
where	a.charging_cost_center = c.charging_cost_center
            and a.account = e.account
			and a.month_number between d.start_month and d.end_month
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and e.acct_group between 'A' and 'E'
            and c.department = '9499'
group by	c.department,
			c.dept_description,
			c.charging_cost_center,
			c.description
union
select 		c.department department,
			c.dept_description department_desc,
			c.charging_cost_center charging_cost_center,
			c.description charging_cost_center_desc,
			0 actualsytd,
			sum(a.amount) budgetytd,
			0 projected_actuals,
			0 projected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			cr_epe_account_mv e
where 	a.charging_cost_center = c.charging_cost_center
		    and a.ferc_account = e.account
			and a.budget_version = b.control_value
			and a.month_number between d.start_month and d.end_month
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and e.acct_group between 'A' and 'E'
            and c.department = '9499'
group by	c.department,
			c.dept_description,
			c.charging_cost_center,
			c.description
union
select 		c.department department,
			c.dept_description department_desc,
			c.charging_cost_center charging_cost_center,
			c.description charging_cost_center_desc,
			0 actuals_amount,
			0 budget_amount,
			sum(a.amount) projected_actuals,
			0 projected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'REVISED BUDGET VERSION') b,
			(select min (case
                        when control_id = 5
                        then substr(control_value,1,4)
                        end
                        )as allyear
                        from cr_alloc_system_control) d,
			cr_epe_account_mv e
where 	a.charging_cost_center = c.charging_cost_center
            and a.ferc_account = e.account
			and a.budget_version = b.control_value
			and substr(a.month_number,1,4) = d.allyear
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and e.acct_group between 'A' and 'E'
            and c.department = '9499'
group by	c.department,
			c.dept_description,
			c.charging_cost_center,
			c.description
union
select 		c.department department,
			c.dept_description department_desc,
			c.charging_cost_center charging_cost_center,
			c.description charging_cost_center_desc,
			0 actuals_amount,
			0 budget_amount,
			0 projected_actuals,
			sum(a.amount) prjected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
                        when control_id = 5
                        then substr(control_value,1,4)
                        end
                        )as allyear
                        from cr_alloc_system_control) d,
			cr_epe_account_mv e
where 	a.charging_cost_center = c.charging_cost_center
		    and a.ferc_account = e.account
			and a.budget_version = b.control_value
			and substr(a.month_number,1,4) = d.allyear
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and e.acct_group between 'A' and 'E'
			and c.department = '9499'
group by 	c.department,
			c.dept_description,
			c.charging_cost_center,
			c.description
)
GROUP BY	department,
			department_desc,
			charging_cost_center,
			charging_cost_center_desc
HAVING abs(sum(actualsytd) + sum(budgetytd) + sum(projected_actuals) + sum(projected_budget)) > .49
 
 
 
 
 
 ;