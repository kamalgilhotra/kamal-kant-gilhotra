
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."UTILITY_ACCOUNT_CO" ("UTILITY_ACCOUNT_ID", "BUS_SEGMENT_ID", "TIME_STAMP", "USER_ID", "MORTALITY_CURVE_ID", "HW_TABLE_ID", "HW_LINE_NO", "FERC_PLT_ACCT_ID", "FUNC_CLASS_ID", "DESCRIPTION", "LONG_DESCRIPTION", "STATUS_CODE_ID", "EXTERNAL_ACCOUNT_CODE", "EXPECTED_LIFE", "HW_TABLE_LINE_ID") AS 
  (select a.utility_account_id,a.bus_segment_id,a.time_stamp,a.user_id,
a.mortality_curve_id,a.hw_table_id,a.hw_line_no,a.ferc_plt_acct_id,a.func_class_id,
a.description,a.long_description,a.status_code_id,a.external_account_code,a.expected_life,
a.hw_table_line_id from utility_account a
where a.bus_segment_id in (select distinct company_bus_segment_control.bus_segment_id
from  company, company_bus_segment_control
where company.company_id = company_bus_segment_control.company_id)
) 
 
 
 
 
 
 ;