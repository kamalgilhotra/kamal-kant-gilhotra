
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CPR_TEMP_ACT_VIEW" ("ASSET_ID", "BASIS_1", "BASIS_2", "BASIS_3", "BASIS_4", "BASIS_5", "BASIS_6", "BASIS_7", "BASIS_8", "BASIS_9", "BASIS_10", "BASIS_11", "BASIS_12", "BASIS_13", "BASIS_14", "BASIS_15", "BASIS_16", "BASIS_17", "BASIS_18", "BASIS_19", "BASIS_20", "BASIS_21", "BASIS_22", "BASIS_23", "BASIS_24", "BASIS_25", "BASIS_26", "BASIS_27", "BASIS_28", "BASIS_29", "BASIS_30", "BASIS_31", "BASIS_32", "BASIS_33", "BASIS_34", "BASIS_35", "BASIS_36", "BASIS_37", "BASIS_38", "BASIS_39", "BASIS_40", "BASIS_41", "BASIS_42", "BASIS_43", "BASIS_44", "BASIS_45", "BASIS_46", "BASIS_47", "BASIS_48", "BASIS_49", "BASIS_50", "BASIS_51", "BASIS_52", "BASIS_53", "BASIS_54", "BASIS_55", "BASIS_56", "BASIS_57", "BASIS_58", "BASIS_59", "BASIS_60", "BASIS_61", "BASIS_62", "BASIS_63", "BASIS_64", "BASIS_65", "BASIS_66", "BASIS_67", "BASIS_68", "BASIS_69", "BASIS_70") AS 
  select TEMP_ASSET.ASSET_ID,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_1,  0) BASIS_1,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_2,  0) BASIS_2,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_3,  0) BASIS_3,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_4,  0) BASIS_4,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_5,  0) BASIS_5,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_6,  0) BASIS_6,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_7,  0) BASIS_7,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_8,  0) BASIS_8,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_9,  0) BASIS_9,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_10, 0) BASIS_10,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_11, 0) BASIS_11,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_12, 0) BASIS_12,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_13, 0) BASIS_13,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_14, 0) BASIS_14,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_15, 0) BASIS_15,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_16, 0) BASIS_16,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_17, 0) BASIS_17,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_18, 0) BASIS_18,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_19, 0) BASIS_19,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_20, 0) BASIS_20,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_21, 0) BASIS_21,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_22, 0) BASIS_22,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_23, 0) BASIS_23,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_24, 0) BASIS_24,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_25, 0) BASIS_25,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_26, 0) BASIS_26,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_27, 0) BASIS_27,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_28, 0) BASIS_28,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_29, 0) BASIS_29,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_30, 0) BASIS_30,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_31, 0) BASIS_31,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_32, 0) BASIS_32,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_33, 0) BASIS_33,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_34, 0) BASIS_34,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_35, 0) BASIS_35,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_36, 0) BASIS_36,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_37, 0) BASIS_37,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_38, 0) BASIS_38,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_39, 0) BASIS_39,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_40, 0) BASIS_40,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_41, 0) BASIS_41,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_42, 0) BASIS_42,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_43, 0) BASIS_43,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_44, 0) BASIS_44,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_45, 0) BASIS_45,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_46, 0) BASIS_46,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_47, 0) BASIS_47,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_48, 0) BASIS_48,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_49, 0) BASIS_49,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_50, 0) BASIS_50,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_51, 0) BASIS_51,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_52, 0) BASIS_52,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_53, 0) BASIS_53,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_54, 0) BASIS_54,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_55, 0) BASIS_55,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_56, 0) BASIS_56,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_57, 0) BASIS_57,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_58, 0) BASIS_58,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_59, 0) BASIS_59,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_60, 0) BASIS_60,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_61, 0) BASIS_61,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_62, 0) BASIS_62,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_63, 0) BASIS_63,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_64, 0) BASIS_64,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_65, 0) BASIS_65,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_66, 0) BASIS_66,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_67, 0) BASIS_67,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_68, 0) BASIS_68,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_69, 0) BASIS_69,
       NVL(CPR_TEMP_ASSET_DEPR_VIEW.BASIS_70, 0) BASIS_70
  from TEMP_ASSET, CPR_ACT_MONTH, CPR_TEMP_ASSET_DEPR_VIEW
 where (TEMP_ASSET.ASSET_ID = CPR_TEMP_ASSET_DEPR_VIEW.ASSET_ID(+))
   and (UPPER(TEMP_ASSET.USER_ID) = UPPER(user))
   and (TEMP_ASSET.SESSION_ID = USERENV('sessionid'))
   and (UPPER(CPR_ACT_MONTH.USER_ID) = UPPER(TEMP_ASSET.USER_ID) and
       (CPR_ACT_MONTH.SESSION_ID = TEMP_ASSET.SESSION_ID))
 
 
 
 
 
 ;