
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."COMPANY_CONSOLIDATION" ("PP_TREE_CATEGORY_ID", "CONSOLIDATION_ID", "PARENT_ID", "COMPANY_ID", "DESCRIPTION") AS 
  SELECT 1           pp_tree_category_id,
       -1          pp_tree_type_id,
       company_id  parent_id,
       company_id  child_id,
       description description
  FROM company
UNION
SELECT DISTINCT pp_tree.pp_tree_category_id,
                pp_tree.pp_tree_type_id,
                pp_tree.child_id,
                pp_tree.child_id,
                nvl(cc1.override_description,
                    company.description) description
  FROM pp_tree,
       pp_tree_type,
       company,
       (SELECT pp_tree_category_id,
               pp_tree_type_id,
               parent_id,
               MAX(override_parent_description) override_description
          FROM pp_tree
         GROUP BY pp_tree_category_id,
                  pp_tree_type_id,
                  parent_id) cc1
 WHERE pp_tree.pp_tree_category_id = 1
   AND pp_tree.pp_tree_category_id = pp_tree_type.pp_tree_category_id
   AND pp_tree.pp_tree_type_id = pp_tree_type.pp_tree_type_id
   AND ((pp_tree.child_id = company.company_id AND pp_tree.empty_node = 0) OR
       (company.company_id IN (SELECT MIN(company_id)
                                  FROM company) AND
       pp_tree.empty_node = 1))
   AND (pp_tree.pp_tree_category_id, pp_tree.pp_tree_type_id,
        pp_tree.child_id) NOT IN
       (SELECT pp_tree_category_id,
               pp_tree_type_id,
               child_id
          FROM pp_tree
         WHERE child_id = parent_id)
   AND pp_tree.pp_tree_category_id = cc1.pp_tree_category_id(+)
   AND pp_tree.pp_tree_type_id = cc1.pp_tree_type_id(+)
   AND pp_tree.child_id = cc1.parent_id(+)
UNION
SELECT a.pp_tree_category_id,
       a.pp_tree_type_id,
       a.parent_id,
       a.child_id,
       nvl(a.override_parent_description,
           company.description) description
  FROM pp_tree a,
       pp_tree aa,
       pp_tree_type,
       company
 WHERE a.pp_tree_category_id = 1
   AND a.pp_tree_type_id = pp_tree_type.pp_tree_type_id
   AND a.parent_id = aa.child_id
   AND a.pp_tree_category_id = aa.pp_tree_category_id
   AND a.pp_tree_type_id = aa.pp_tree_type_id
   AND ((a.parent_id = company.company_id AND aa.empty_node = 0) OR
       (company.company_id IN (SELECT MIN(company_id)
                                  FROM company) AND aa.empty_node = 1))
UNION
SELECT a.pp_tree_category_id,
       a.pp_tree_type_id,
       a.parent_id,
       b.child_id,
       nvl(a.override_parent_description,
           company.description) description
  FROM pp_tree a,
       pp_tree aa,
       pp_tree b,
       pp_tree_type,
       company
 WHERE a.pp_tree_category_id = 1
   AND a.pp_tree_category_id = b.pp_tree_category_id
   AND a.child_id = b.parent_id
   AND a.pp_tree_type_id = b.pp_tree_type_id
   AND b.pp_tree_category_id = pp_tree_type.pp_tree_category_id
   AND b.pp_tree_type_id = pp_tree_type.pp_tree_type_id
   AND a.parent_id = aa.child_id
   AND a.pp_tree_category_id = aa.pp_tree_category_id
   AND a.pp_tree_type_id = aa.pp_tree_type_id
   AND ((a.parent_id = company.company_id AND aa.empty_node = 0) OR
       (company.company_id IN (SELECT MIN(company_id)
                                  FROM company) AND aa.empty_node = 1))
UNION
SELECT a.pp_tree_category_id,
       a.pp_tree_type_id,
       a.parent_id,
       c.child_id,
       nvl(a.override_parent_description,
           company.description) description
  FROM pp_tree a,
       pp_tree aa,
       pp_tree b,
       pp_tree c,
       pp_tree_type,
       company
 WHERE a.pp_tree_category_id = 1
   AND a.pp_tree_category_id = b.pp_tree_category_id
   AND a.child_id = b.parent_id
   AND a.pp_tree_type_id = b.pp_tree_type_id
   AND b.pp_tree_category_id = c.pp_tree_category_id
   AND b.child_id = c.parent_id
   AND b.pp_tree_type_id = c.pp_tree_type_id
   AND c.pp_tree_category_id = pp_tree_type.pp_tree_category_id
   AND c.pp_tree_type_id = pp_tree_type.pp_tree_type_id
   AND a.parent_id = aa.child_id
   AND a.pp_tree_category_id = aa.pp_tree_category_id
   AND a.pp_tree_type_id = aa.pp_tree_type_id
   AND ((a.parent_id = company.company_id AND aa.empty_node = 0) OR
       (company.company_id IN (SELECT MIN(company_id)
                                  FROM company) AND aa.empty_node = 1))
UNION
SELECT a.pp_tree_category_id,
       a.pp_tree_type_id,
       a.parent_id,
       d.child_id,
       nvl(a.override_parent_description,
           company.description) description
  FROM pp_tree a,
       pp_tree aa,
       pp_tree b,
       pp_tree c,
       pp_tree d,
       pp_tree_type,
       company
 WHERE a.pp_tree_category_id = 1
   AND a.pp_tree_category_id = b.pp_tree_category_id
   AND a.child_id = b.parent_id
   AND a.pp_tree_type_id = b.pp_tree_type_id
   AND b.pp_tree_category_id = c.pp_tree_category_id
   AND b.child_id = c.parent_id
   AND b.pp_tree_type_id = c.pp_tree_type_id
   AND c.pp_tree_category_id = d.pp_tree_category_id
   AND c.child_id = d.parent_id
   AND c.pp_tree_type_id = d.pp_tree_type_id
   AND d.pp_tree_category_id = pp_tree_type.pp_tree_category_id
   AND d.pp_tree_type_id = pp_tree_type.pp_tree_type_id
   AND a.parent_id = aa.child_id
   AND a.pp_tree_category_id = aa.pp_tree_category_id
   AND a.pp_tree_type_id = aa.pp_tree_type_id
   AND ((a.parent_id = company.company_id AND aa.empty_node = 0) OR
       (company.company_id IN (SELECT MIN(company_id)
                                  FROM company) AND aa.empty_node = 1))
UNION
SELECT a.pp_tree_category_id,
       a.pp_tree_type_id,
       a.parent_id,
       e.child_id,
       nvl(a.override_parent_description,
           company.description) description
  FROM pp_tree a,
       pp_tree aa,
       pp_tree b,
       pp_tree c,
       pp_tree d,
       pp_tree e,
       pp_tree_type,
       company
 WHERE a.pp_tree_category_id = 1
   AND a.pp_tree_category_id = b.pp_tree_category_id
   AND a.child_id = b.parent_id
   AND a.pp_tree_type_id = b.pp_tree_type_id
   AND b.pp_tree_category_id = c.pp_tree_category_id
   AND b.child_id = c.parent_id
   AND b.pp_tree_type_id = c.pp_tree_type_id
   AND c.pp_tree_category_id = d.pp_tree_category_id
   AND c.child_id = d.parent_id
   AND c.pp_tree_type_id = d.pp_tree_type_id
   AND d.pp_tree_category_id = e.pp_tree_category_id
   AND d.child_id = e.parent_id
   AND d.pp_tree_type_id = e.pp_tree_type_id
   AND e.pp_tree_category_id = pp_tree_type.pp_tree_category_id
   AND e.pp_tree_type_id = pp_tree_type.pp_tree_type_id
   AND a.parent_id = aa.child_id
   AND a.pp_tree_category_id = aa.pp_tree_category_id
   AND a.pp_tree_type_id = aa.pp_tree_type_id
   AND ((a.parent_id = company.company_id AND aa.empty_node = 0) OR
       (company.company_id IN (SELECT MIN(company_id)
                                  FROM company) AND aa.empty_node = 1))
UNION
SELECT a.pp_tree_category_id,
       a.pp_tree_type_id,
       a.parent_id,
       f.child_id,
       nvl(a.override_parent_description,
           company.description) description
  FROM pp_tree a,
       pp_tree aa,
       pp_tree b,
       pp_tree c,
       pp_tree d,
       pp_tree e,
       pp_tree f,
       pp_tree_type,
       company
 WHERE a.pp_tree_category_id = 1
   AND a.pp_tree_category_id = b.pp_tree_category_id
   AND a.child_id = b.parent_id
   AND a.pp_tree_type_id = b.pp_tree_type_id
   AND b.pp_tree_category_id = c.pp_tree_category_id
   AND b.child_id = c.parent_id
   AND b.pp_tree_type_id = c.pp_tree_type_id
   AND c.pp_tree_category_id = d.pp_tree_category_id
   AND c.child_id = d.parent_id
   AND c.pp_tree_type_id = d.pp_tree_type_id
   AND d.pp_tree_category_id = e.pp_tree_category_id
   AND d.child_id = e.parent_id
   AND d.pp_tree_type_id = e.pp_tree_type_id
   AND e.pp_tree_category_id = f.pp_tree_category_id
   AND e.child_id = f.parent_id
   AND e.pp_tree_type_id = f.pp_tree_type_id
   AND f.pp_tree_category_id = pp_tree_type.pp_tree_category_id
   AND f.pp_tree_type_id = pp_tree_type.pp_tree_type_id
   AND a.parent_id = aa.child_id
   AND a.pp_tree_category_id = aa.pp_tree_category_id
   AND a.pp_tree_type_id = aa.pp_tree_type_id
   AND ((a.parent_id = company.company_id AND aa.empty_node = 0) OR
       (company.company_id IN (SELECT MIN(company_id)
                                  FROM company) AND aa.empty_node = 1))
UNION
SELECT a.pp_tree_category_id,
       a.pp_tree_type_id,
       a.parent_id,
       g.child_id,
       nvl(a.override_parent_description,
           company.description) description
  FROM pp_tree a,
       pp_tree aa,
       pp_tree b,
       pp_tree c,
       pp_tree d,
       pp_tree e,
       pp_tree f,
       pp_tree g,
       pp_tree_type,
       company
 WHERE a.pp_tree_category_id = 1
   AND a.pp_tree_category_id = b.pp_tree_category_id
   AND a.child_id = b.parent_id
   AND a.pp_tree_type_id = b.pp_tree_type_id
   AND b.pp_tree_category_id = c.pp_tree_category_id
   AND b.child_id = c.parent_id
   AND b.pp_tree_type_id = c.pp_tree_type_id
   AND c.pp_tree_category_id = d.pp_tree_category_id
   AND c.child_id = d.parent_id
   AND c.pp_tree_type_id = d.pp_tree_type_id
   AND d.pp_tree_category_id = e.pp_tree_category_id
   AND d.child_id = e.parent_id
   AND d.pp_tree_type_id = e.pp_tree_type_id
   AND e.pp_tree_category_id = f.pp_tree_category_id
   AND e.child_id = f.parent_id
   AND e.pp_tree_type_id = f.pp_tree_type_id
   AND f.pp_tree_category_id = g.pp_tree_category_id
   AND f.child_id = g.parent_id
   AND f.pp_tree_type_id = g.pp_tree_type_id
   AND g.pp_tree_category_id = pp_tree_type.pp_tree_category_id
   AND g.pp_tree_type_id = pp_tree_type.pp_tree_type_id
   AND a.parent_id = aa.child_id
   AND a.pp_tree_category_id = aa.pp_tree_category_id
   AND a.pp_tree_type_id = aa.pp_tree_type_id
   AND ((a.parent_id = company.company_id AND aa.empty_node = 0) OR
       (company.company_id IN (SELECT MIN(company_id)
                                  FROM company) AND aa.empty_node = 1))
UNION
SELECT a.pp_tree_category_id,
       a.pp_tree_type_id,
       a.parent_id,
       h.child_id,
       nvl(a.override_parent_description,
           company.description) description
  FROM pp_tree a,
       pp_tree aa,
       pp_tree b,
       pp_tree c,
       pp_tree d,
       pp_tree e,
       pp_tree f,
       pp_tree g,
       pp_tree h,
       pp_tree_type,
       company
 WHERE a.pp_tree_category_id = 1
   AND a.pp_tree_category_id = b.pp_tree_category_id
   AND a.child_id = b.parent_id
   AND a.pp_tree_type_id = b.pp_tree_type_id
   AND b.pp_tree_category_id = c.pp_tree_category_id
   AND b.child_id = c.parent_id
   AND b.pp_tree_type_id = c.pp_tree_type_id
   AND c.pp_tree_category_id = d.pp_tree_category_id
   AND c.child_id = d.parent_id
   AND c.pp_tree_type_id = d.pp_tree_type_id
   AND d.pp_tree_category_id = e.pp_tree_category_id
   AND d.child_id = e.parent_id
   AND d.pp_tree_type_id = e.pp_tree_type_id
   AND e.pp_tree_category_id = f.pp_tree_category_id
   AND e.child_id = f.parent_id
   AND e.pp_tree_type_id = f.pp_tree_type_id
   AND f.pp_tree_category_id = g.pp_tree_category_id
   AND f.child_id = g.parent_id
   AND f.pp_tree_type_id = g.pp_tree_type_id
   AND g.pp_tree_category_id = h.pp_tree_category_id
   AND g.child_id = h.parent_id
   AND g.pp_tree_type_id = h.pp_tree_type_id
   AND h.pp_tree_category_id = pp_tree_type.pp_tree_category_id
   AND h.pp_tree_type_id = pp_tree_type.pp_tree_type_id
   AND a.parent_id = aa.child_id
   AND a.pp_tree_category_id = aa.pp_tree_category_id
   AND a.pp_tree_type_id = aa.pp_tree_type_id
   AND ((a.parent_id = company.company_id AND aa.empty_node = 0) OR
       (company.company_id IN (SELECT MIN(company_id)
                                  FROM company) AND aa.empty_node = 1))
UNION
SELECT a.pp_tree_category_id,
       a.pp_tree_type_id,
       a.parent_id,
       i.child_id,
       nvl(a.override_parent_description,
           company.description) description
  FROM pp_tree a,
       pp_tree aa,
       pp_tree b,
       pp_tree c,
       pp_tree d,
       pp_tree e,
       pp_tree f,
       pp_tree g,
       pp_tree h,
       pp_tree i,
       pp_tree_type,
       company
 WHERE a.pp_tree_category_id = 1
   AND a.pp_tree_category_id = b.pp_tree_category_id
   AND a.child_id = b.parent_id
   AND a.pp_tree_type_id = b.pp_tree_type_id
   AND b.pp_tree_category_id = c.pp_tree_category_id
   AND b.child_id = c.parent_id
   AND b.pp_tree_type_id = c.pp_tree_type_id
   AND c.pp_tree_category_id = d.pp_tree_category_id
   AND c.child_id = d.parent_id
   AND c.pp_tree_type_id = d.pp_tree_type_id
   AND d.pp_tree_category_id = e.pp_tree_category_id
   AND d.child_id = e.parent_id
   AND d.pp_tree_type_id = e.pp_tree_type_id
   AND e.pp_tree_category_id = f.pp_tree_category_id
   AND e.child_id = f.parent_id
   AND e.pp_tree_type_id = f.pp_tree_type_id
   AND f.pp_tree_category_id = g.pp_tree_category_id
   AND f.child_id = g.parent_id
   AND f.pp_tree_type_id = g.pp_tree_type_id
   AND g.pp_tree_category_id = h.pp_tree_category_id
   AND g.child_id = h.parent_id
   AND g.pp_tree_type_id = h.pp_tree_type_id
   AND h.child_id = i.parent_id
   AND h.pp_tree_type_id = i.pp_tree_type_id
   AND h.pp_tree_category_id = i.pp_tree_category_id
   AND i.pp_tree_category_id = pp_tree_type.pp_tree_category_id
   AND i.pp_tree_type_id = pp_tree_type.pp_tree_type_id
   AND a.parent_id = aa.child_id
   AND a.pp_tree_category_id = aa.pp_tree_category_id
   AND a.pp_tree_type_id = aa.pp_tree_type_id
   AND ((a.parent_id = company.company_id AND aa.empty_node = 0) OR
       (company.company_id IN (SELECT MIN(company_id)
                                  FROM company) AND aa.empty_node = 1))
UNION
SELECT a.pp_tree_category_id,
       a.pp_tree_type_id,
       a.parent_id,
       j.child_id,
       nvl(a.override_parent_description,
           company.description) description
  FROM pp_tree a,
       pp_tree aa,
       pp_tree b,
       pp_tree c,
       pp_tree d,
       pp_tree e,
       pp_tree f,
       pp_tree g,
       pp_tree h,
       pp_tree i,
       pp_tree j,
       pp_tree_type,
       company
 WHERE a.pp_tree_category_id = 1
   AND a.pp_tree_category_id = b.pp_tree_category_id
   AND a.child_id = b.parent_id
   AND a.pp_tree_type_id = b.pp_tree_type_id
   AND b.pp_tree_category_id = c.pp_tree_category_id
   AND b.child_id = c.parent_id
   AND b.pp_tree_type_id = c.pp_tree_type_id
   AND c.pp_tree_category_id = d.pp_tree_category_id
   AND c.child_id = d.parent_id
   AND c.pp_tree_type_id = d.pp_tree_type_id
   AND d.pp_tree_category_id = e.pp_tree_category_id
   AND d.child_id = e.parent_id
   AND d.pp_tree_type_id = e.pp_tree_type_id
   AND e.pp_tree_category_id = f.pp_tree_category_id
   AND e.child_id = f.parent_id
   AND e.pp_tree_type_id = f.pp_tree_type_id
   AND f.pp_tree_category_id = g.pp_tree_category_id
   AND f.child_id = g.parent_id
   AND f.pp_tree_type_id = g.pp_tree_type_id
   AND g.pp_tree_category_id = h.pp_tree_category_id
   AND g.child_id = h.parent_id
   AND g.pp_tree_type_id = h.pp_tree_type_id
   AND h.pp_tree_category_id = i.pp_tree_category_id
   AND h.child_id = i.parent_id
   AND h.pp_tree_type_id = i.pp_tree_type_id
   AND i.pp_tree_category_id = j.pp_tree_category_id
   AND i.child_id = j.parent_id
   AND i.pp_tree_type_id = j.pp_tree_type_id
   AND j.pp_tree_category_id = pp_tree_type.pp_tree_category_id
   AND j.pp_tree_type_id = pp_tree_type.pp_tree_type_id
   AND a.parent_id = aa.child_id
   AND a.pp_tree_category_id = aa.pp_tree_category_id
   AND a.pp_tree_type_id = aa.pp_tree_type_id AND ((a.parent_id = company.company_id AND aa.empty_node = 0) OR (company.company_id IN (SELECT MIN(company_id)
                                                                                                                                         FROM company) AND aa.empty_node = 1)
)
 
 
 
 
 
 ;