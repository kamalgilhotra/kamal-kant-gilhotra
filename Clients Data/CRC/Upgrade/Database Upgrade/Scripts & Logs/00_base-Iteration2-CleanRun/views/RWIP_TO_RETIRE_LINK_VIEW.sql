
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."RWIP_TO_RETIRE_LINK_VIEW" ("COMPANY_ID", "DESCRIPTION", "ASSET_ID", "MONTH_NUMBER", "WORK_ORDER_NUMBER", "ORIG_COST_RETIRE", "COST_OF_REMOVAL", "SALVAGE_CASH", "SALVAGE_RETURNS", "GAIN_LOSS", "MATCH") AS 
  with a as
 (select /*+ materialize */
   rt.pend_trans_id, rt.ldg_asset_id, rt.gl_posting_mo_yr,
   rt.work_order_number, ca.asset_activity_id
    from retire_transaction rt, cpr_activity ca
   where rt.pend_trans_id = ca.activity_status
     and rt.ldg_asset_id = ca.asset_id
     and rt.work_order_number = ca.work_order_number
     and rt.gl_posting_mo_yr = ca.gl_posting_mo_yr
     and ca.activity_cost <> 0
     and ca.activity_code in ('URET', 'URGL', 'SALE', 'SAGL')),
--retire txns with the same asset, acct. mo, wo number
--as those retire txns in a inclusive
b as
 (select /*+ materialize */
   rt.pend_trans_id, rt.gl_posting_mo_yr, rt.ldg_asset_id
    from retire_transaction rt
   where (rt.ldg_asset_id, rt.gl_posting_mo_yr, rt.work_order_number) in
         (select ldg_asset_id, gl_posting_mo_yr, work_order_number from a)),
--the retire txns not able to directly line
-- up with cpr activity on asset, wo number, gl post mo yr
c as
 (select /*+ materialize */
   pend_trans_id
    from retire_transaction
  minus
  select pend_trans_id from b),
-- the retire txns from c which can be brought back
-- to a prior gl posting mo yr, asset, wo number in cpr activity
d as
 (select /*+ materialize */
  distinct c.pend_trans_id, ca.gl_posting_mo_yr, ca.asset_id
    from c, retire_transaction rt, cpr_activity ca
   where c.pend_trans_id = rt.pend_trans_id
     and rt.work_order_number = ca.work_order_number
     and rt.ldg_asset_id = ca.asset_id
     and ca.gl_posting_mo_yr =
         (select max(q.gl_posting_mo_yr)
            from cpr_activity q
           where q.asset_id = rt.ldg_asset_id
             and q.gl_posting_mo_yr <= rt.gl_posting_mo_yr
             and q.activity_cost <> 0
             and q.work_order_number = rt.work_order_number
             and q.activity_code in ('URET', 'MRET', 'URGL', 'SALE', 'SAGL'))
     and ca.activity_code in ('URET', 'MRET', 'URGL', 'SALE', 'SAGL')),
-- the retire txns from c which cannot be brought back
-- to a prior gl posting mo yr, asset, wo number in cpr activity
e as
 (select /* + materialize */
  distinct c.pend_trans_id, rt.gl_posting_mo_yr, rt.ldg_asset_id
    from c, retire_transaction rt
   where c.pend_trans_id = rt.pend_trans_id
     and not exists
   (select 1
            from cpr_activity ca
           where ca.work_order_number = rt.work_order_number
             and ca.asset_id = rt.ldg_asset_id
             and ca.gl_posting_mo_yr <= rt.gl_posting_mo_yr
             and ca.activity_cost <> 0
             and ca.activity_code in ('URET', 'MRET', 'URGL', 'SALE', 'SAGL'))),
--original cost retirements by asset and month
ocr as
 (select /*+ materialize */
   asset_id, gl_posting_mo_yr, sum(activity_cost) orig_cost_retire,
   work_order_number
    from cpr_activity ca
   where ca.activity_code in ('URET', 'MRET', 'URGL', 'SALE', 'SAGL')
   group by asset_id, gl_posting_mo_yr, work_order_number),
--rwip view which groups together all retire txns
-- with their asset and gl post mo yr
rwip as
 (select /*+ materialize */
   d.asset_id, d.gl_posting_mo_yr, rt.cost_of_removal, rt.salvage_cash,
   rt.salvage_returns, rt.gain_loss, rt.work_order_number, 'd' match
    from d, retire_transaction rt
   where rt.pend_trans_id = d.pend_trans_id
  union all
  select e.ldg_asset_id, e.gl_posting_mo_yr, rt.cost_of_removal,
         rt.salvage_cash, rt.salvage_returns, rt.gain_loss,
         rt.work_order_number, 'e'
    from e, retire_transaction rt
   where rt.pend_trans_id = e.pend_trans_id
  union all
  select b.ldg_asset_id, b.gl_posting_mo_yr, rt.cost_of_removal,
         rt.salvage_cash, rt.salvage_returns, rt.gain_loss,
         rt.work_order_number, 'b'
    from b, retire_transaction rt
   where rt.pend_trans_id = b.pend_trans_id)
--Sum up RWIP by groupings of asset, gl post mo yr, orig cost retire, wo#
select cpr_ledger.company_id, depr_group.description, sub.asset_id,
       to_char(gl_posting_mo_yr, 'yyyymm') month_number,
       sub.work_order_number, orig_cost_retire,
       sum(cost_of_removal) cost_of_removal, sum(salvage_cash) salvage_cash,
       sum(salvage_returns) salvage_returns, sum(gain_loss) gain_loss, match
  from cpr_ledger, depr_group, company,
       (select ocr.asset_id, ocr.gl_posting_mo_yr, ocr.work_order_number,
                ocr.orig_cost_retire, rwip.cost_of_removal, rwip.salvage_cash,
                rwip.salvage_returns, rwip.gain_loss, rwip.match
           from rwip, ocr
          where ocr.asset_id = rwip.asset_id
            and ocr.gl_posting_mo_yr = rwip.gl_posting_mo_yr
            and ocr.work_order_number = rwip.work_order_number
         union all
         select ocr.asset_id, ocr.gl_posting_mo_yr, ocr.work_order_number,
                orig_cost_retire, 0 cost_of_removal, 0 salvage_cash,
                0 salvage_returns, 0 gain_loss, 'none'
           from ocr
          where not exists
          (select 1
                   from rwip
                  where rwip.asset_id = ocr.asset_id
                    and rwip.gl_posting_mo_yr = ocr.gl_posting_mo_yr
                    and rwip.work_order_number = ocr.work_order_number)) sub
 where sub.asset_id = cpr_ledger.asset_id
   and cpr_ledger.depr_group_id = depr_group.depr_group_id
   and cpr_ledger.company_id = company.company_id
 group by cpr_ledger.company_id, depr_group.description, sub.asset_id,
          to_char(gl_posting_mo_yr, 'yyyymm'), sub.work_order_number,
          orig_cost_retire, match
having orig_cost_retire <> 0 or sum (cost_of_removal) <> 0 or sum (salvage_cash) <> 0 or sum (salvage_returns) <> 0 or sum (gain_loss) <> 0
 
 
 ;