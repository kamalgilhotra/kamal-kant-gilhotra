
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CPR_LEDGER_MASS_AVAIL_TO_RET_V" ("PROPERTY_GROUP_ID", "RETIREMENT_UNIT_ID", "BUS_SEGMENT_ID", "COMPANY_ID", "UTILITY_ACCOUNT_ID", "GL_ACCOUNT_ID", "ASSET_LOCATION_ID", "SUB_ACCOUNT_ID", "ACCUM_QUANTITY") AS 
  select A.PROPERTY_GROUP_ID,
       A.RETIREMENT_UNIT_ID,
       A.BUS_SEGMENT_ID,
       A.COMPANY_ID,
       A.UTILITY_ACCOUNT_ID,
       A.GL_ACCOUNT_ID,
       A.ASSET_LOCATION_ID,
       A.SUB_ACCOUNT_ID,
       sum(A.ACCUM_QUANTITY) ACCUM_QUANTITY
  from (select A.PROPERTY_GROUP_ID,
               A.RETIREMENT_UNIT_ID,
               A.BUS_SEGMENT_ID,
               A.COMPANY_ID,
               A.UTILITY_ACCOUNT_ID,
               A.GL_ACCOUNT_ID,
               A.ASSET_LOCATION_ID,
               A.SUB_ACCOUNT_ID,
               A.ACCUM_QUANTITY
          from CPR_LEDGER A
         where LEDGER_STATUS < 100
        union all
        select UWO.PROPERTY_GROUP_ID,
               UWO.RETIREMENT_UNIT_ID,
               UWO.BUS_SEGMENT_ID,
               UWO.COMPANY_ID,
               UWO.UTILITY_ACCOUNT_ID,
               UWO.GL_ACCOUNT_ID,
               UWO.ASSET_LOCATION_ID,
               UWO.SUB_ACCOUNT_ID,
               -1 * CGC.QUANTITY
          from CHARGE_GROUP_CONTROL CGC, UNITIZED_WORK_ORDER UWO, CHARGE_TYPE CT
         where NVL(CGC.PEND_TRANSACTION, 0) = 0
           and CGC.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
           and CT.PROCESSING_TYPE_ID = 1
           and CGC.EXPENDITURE_TYPE_ID = 2
           and UWO.UNIT_ITEM_ID = CGC.UNIT_ITEM_ID
           and UWO.WORK_ORDER_ID = CGC.WORK_ORDER_ID
        union all
        select B.PROPERTY_GROUP_ID,
               B.RETIREMENT_UNIT_ID,
               B.BUS_SEGMENT_ID,
               B.COMPANY_ID,
               B.UTILITY_ACCOUNT_ID,
               B.GL_ACCOUNT_ID,
               B.ASSET_LOCATION_ID,
               B.SUB_ACCOUNT_ID,
               B.POSTING_QUANTITY
          from PEND_TRANSACTION B
         where B.FERC_ACTIVITY_CODE = 2) A
 where RETIREMENT_UNIT_ID not between 1 and 5
 group by A.PROPERTY_GROUP_ID,
          A.RETIREMENT_UNIT_ID,
          A.BUS_SEGMENT_ID,
          A.COMPANY_ID,
          A.UTILITY_ACCOUNT_ID,
          A.GL_ACCOUNT_ID,
          A.ASSET_LOCATION_ID,
          A.SUB_ACCOUNT_ID
 
 ;