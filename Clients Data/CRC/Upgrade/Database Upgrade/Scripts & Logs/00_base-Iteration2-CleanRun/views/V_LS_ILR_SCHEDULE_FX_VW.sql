CREATE OR REPLACE FORCE VIEW v_ls_ilr_schedule_fx_vw 
AS select 
   lis.ilr_id ilr_id,
   lis.ilr_number ilr_number,
   lis.lease_id lease_id,
   lis.lease_number lease_number,
   lis.current_revision current_revision,
   lis.revision revision,
   lis.set_of_books_id set_of_books_id,
   lis.month month,
   open_month.company_id company_id,
   open_month.open_month open_month,
   cur.ls_cur_type ls_cur_type,
   lis.month exchange_date,
   Nvl(calc_rate.actual_prev_exchange_date, calc_rate.actual_exchange_date) prev_exchange_date,
   lis.contract_currency_id contract_currency_id,
   cur.currency_id display_currency_id,
   cr.actual_rate rate,
   calc_rate.actual_rate calculated_rate,
   calc_rate.actual_prev_rate previous_calculated_rate,
   calc_rate.average_rate calculated_average_rate,
   calc_rate.average_prev_rate previous_calculated_avg_rate,
   decode(open_month.exchange_rate_type_id, 4, calc_rate.average_rate, calc_rate.actual_rate) average_rate,
   decode(open_month.exchange_rate_type_id, 4, cr.average_rate, cr.actual_rate) cr_average_rate,
   lis.effective_in_svc_rate,
   original_in_svc_rate,
   cur.iso_code iso_code,
   cur.currency_display_symbol currency_display_symbol,
   lis.purchase_option_amt * nvl(calc_rate.actual_rate, cr.actual_rate) purchase_option_amt,
   lis.termination_amt * nvl(calc_rate.actual_rate, cr.actual_rate) termination_amt,
   lis.net_present_value * nvl(calc_rate.actual_rate, cr.actual_rate) net_present_value,
   lis.capital_cost * nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate) capital_cost,
   lis.beg_capital_cost * case when lis.is_om = 0 and lis.month = lis.first_month then
                            nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                          else
                            nvl(nvl(cur.contract_approval_rate, lis.gross_weighted_avg_rate2), cr.current_rate)
                          end beg_capital_cost,
   lis.end_capital_cost * nvl(nvl(cur.contract_approval_rate,
                                  case
                                    when lis.month < nvl(lis.remeasurement_date, lis.month) then
                                     lis.gross_weighted_avg_rate2
                                    else
                                     lis.gross_weighted_avg_rate
                                  end),
                              cr.current_rate) end_capital_cost,
   lis.beg_obligation * case when lis.is_om = 0 and lis.month = lis.first_month then
                          nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                        else
                          nvl(calc_rate.actual_prev_rate, cr.actual_rate)
                        end beg_obligation,
   lis.end_obligation * nvl(calc_rate.actual_rate, cr.actual_rate) end_obligation,
   lis.beg_lt_obligation * case when lis.is_om = 0 and lis.month = lis.first_month then
                             nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                           else
                             nvl(calc_rate.actual_prev_rate, cr.actual_rate)
                           end beg_lt_obligation,
   lis.end_lt_obligation * nvl(calc_rate.actual_rate, cr.actual_rate) end_lt_obligation,
   lis.principal_remeasurement * nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate) principal_remeasurement,
   lis.beg_liability * case when lis.is_om = 0 and lis.month = lis.first_month then
                         nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                       else
                         nvl(calc_rate.actual_prev_rate, cr.actual_rate)
                       end beg_liability,
   lis.end_liability * nvl(calc_rate.actual_rate, cr.actual_rate) end_liability,
   lis.beg_lt_liability * case when lis.is_om = 0 and lis.month = lis.first_month then
                            nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                          else
                            nvl(calc_rate.actual_prev_rate, cr.actual_rate)
                          end beg_lt_liability,
   lis.end_lt_liability * nvl(calc_rate.actual_rate, cr.actual_rate) end_lt_liability,
   lis.liability_remeasurement * nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate) liability_remeasurement,
   lis.interest_accrual * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) interest_accrual,
   lis.principal_accrual * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) principal_accrual,
   lis.interest_paid * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) interest_paid,
   lis.principal_paid * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) principal_paid,
   lis.executory_accrual1 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual1,
   lis.executory_accrual2 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual2,
   lis.executory_accrual3 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual3,
   lis.executory_accrual4 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual4,
   lis.executory_accrual5 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual5,
   lis.executory_accrual6 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual6,
   lis.executory_accrual7 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual7,
   lis.executory_accrual8 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual8,
   lis.executory_accrual9 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual9,
   lis.executory_accrual10 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_accrual10,
   lis.executory_paid1 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid1,
   lis.executory_paid2 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid2,
   lis.executory_paid3 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid3,
   lis.executory_paid4 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid4,
   lis.executory_paid5 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid5,
   lis.executory_paid6 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid6,
   lis.executory_paid7 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid7,
   lis.executory_paid8 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid8,
   lis.executory_paid9 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid9,
   lis.executory_paid10 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_paid10,
   lis.contingent_accrual1 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual1,
   lis.contingent_accrual2 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual2,
   lis.contingent_accrual3 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual3,
   lis.contingent_accrual4 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual4,
   lis.contingent_accrual5 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual5,
   lis.contingent_accrual6 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual6,
   lis.contingent_accrual7 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual7,
   lis.contingent_accrual8 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual8,
   lis.contingent_accrual9 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual9,
   lis.contingent_accrual10 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_accrual10,
   lis.contingent_paid1 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid1,
   lis.contingent_paid2 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid2,
   lis.contingent_paid3 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid3,
   lis.contingent_paid4 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid4,
   lis.contingent_paid5 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid5,
   lis.contingent_paid6 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid6,
   lis.contingent_paid7 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid7,
   lis.contingent_paid8 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid8,
   lis.contingent_paid9 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid9,
   lis.contingent_paid10 * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_paid10,
   lis.current_lease_cost * nvl(nvl(cur.contract_approval_rate, original_in_svc_rate), cr.current_rate) current_lease_cost,
   lis.beg_deferred_rent * nvl(calc_rate.actual_prev_rate, cr.actual_rate) beg_deferred_rent,
   lis.deferred_rent * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) deferred_rent,
   lis.end_deferred_rent * nvl(calc_rate.actual_rate, cr.actual_rate) end_deferred_rent,
   lis.beg_st_deferred_rent * nvl(calc_rate.actual_prev_rate, cr.actual_rate) beg_st_deferred_rent,
   lis.end_st_deferred_rent * nvl(calc_rate.actual_rate, cr.actual_rate) end_st_deferred_rent,
   lis.initial_direct_cost * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) initial_direct_cost,
   lis.incentive_amount *decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) incentive_amount,
   lis.depr_expense * nvl(nvl(cur.contract_approval_rate,
                              case
                                when lis.month < nvl(lis.remeasurement_date, lis.month) then
                                 lis.net_weighted_avg_rate2
                                else
                                 lis.net_weighted_avg_rate
                              end),
                          cr.current_rate) depr_expense,
   lis.begin_reserve * case when lis.is_om = 0 and lis.month = lis.first_month then
                         nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                       else
                         nvl(nvl(cur.contract_approval_rate, lis.net_weighted_avg_rate2), cr.current_rate)
                       end begin_reserve,
   lis.end_reserve * nvl(nvl(cur.contract_approval_rate,
                             case
                               when lis.month < nvl(lis.remeasurement_date, lis.month) then
                                lis.net_weighted_avg_rate2
                               else
                                lis.net_weighted_avg_rate
                             end),
                         cr.current_rate) end_reserve,
   lis.depr_exp_alloc_adjust *
   nvl(nvl(cur.contract_approval_rate,
           case
             when lis.month < nvl(lis.remeasurement_date, lis.month) then
              lis.net_weighted_avg_rate2
             else
              lis.net_weighted_avg_rate
           end),
       cr.current_rate) depr_exp_alloc_adjust,
   lis.is_om is_om,
   lis.lease_cap_type_id lease_cap_type_id,
   lis.beg_prepaid_rent * nvl(calc_rate.actual_prev_rate, cr.actual_rate) beg_prepaid_rent,
   lis.prepay_amortization * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) prepay_amortization,
   lis.prepaid_rent * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) prepaid_rent,
   lis.end_prepaid_rent * nvl(calc_rate.actual_prev_rate, cr.actual_rate) end_prepaid_rent,
   nvl(lis.beg_net_rou_asset, 0) * case when lis.is_om = 0 and lis.month = lis.first_month then
                                     nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                                   else
                                     nvl(nvl(cur.contract_approval_rate, lis.net_weighted_avg_rate2), cr.current_rate)
                                   end beg_net_rou_asset,
   nvl(lis.end_net_rou_asset, 0) * nvl(nvl(cur.contract_approval_rate,
                                         case
                                           when lis.month < nvl(lis.remeasurement_date, lis.month) then
                                            lis.net_weighted_avg_rate2
                                           else
                                            lis.net_weighted_avg_rate
                                         end),
                                     cr.current_rate) end_net_rou_asset,
   nvl(lis.rou_asset_remeasurement, 0) * nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate) rou_asset_remeasurement,
   nvl(lis.partial_term_gain_loss, 0) * nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate) partial_term_gain_loss,
   lis.beg_arrears_accrual * nvl(calc_rate.actual_prev_rate, cr.actual_rate) beg_arrears_accrual,
   lis.arrears_accrual * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) arrears_accrual,
   lis.end_arrears_accrual * nvl(calc_rate.actual_rate, cr.actual_rate) end_arrears_accrual,
   lis.additional_rou_asset * nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate) additional_rou_asset,
   nvl(lis.executory_adjust, 0) * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) executory_adjust,
   nvl(lis.contingent_adjust, 0) * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) contingent_adjust,
   case
    when lis.is_om = 1 then
     0
    when lis.contract_currency_id = cs.currency_id then
     0
    else
     (
       ((lis.end_liability * nvl(calc_rate.actual_rate, cr.actual_rate)) - (lis.end_lt_liability * nvl(calc_rate.actual_rate, cr.actual_rate)))
       - ((lis.beg_liability * case when lis.is_om = 0 and lis.month = lis.first_month then
                                 nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                               else
                                 nvl(calc_rate.actual_prev_rate, cr.actual_rate)
                               end)
          - (lis.beg_lt_liability * case when lis.is_om = 0 and lis.month = lis.first_month then
                                      nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                                    else
                                      nvl(calc_rate.actual_prev_rate, cr.actual_rate)
                                    end))
       - (lis.interest_accrual * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)))
       + (lis.interest_paid * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)))
       + (lis.principal_paid * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)))
       - (lis.obligation_reclass * nvl(calc_rate.actual_rate, cr.actual_rate))
       - (lis.st_obligation_remeasurement * nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate))
       + (lis.unaccrued_interest_reclass * nvl(calc_rate.actual_rate, cr.actual_rate))
       + (lis.unaccrued_interest_remeasure * nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate))
     )
   end st_currency_gain_loss,
   case
    when lis.is_om = 1 then
     0
    when lis.contract_currency_id = cs.currency_id then
     0
    else
     (
       (lis.end_lt_liability * nvl(calc_rate.actual_rate, cr.actual_rate))
       - (lis.beg_lt_liability * case when lis.is_om = 0 and lis.month = lis.first_month then
                                   nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                                 else
                                   nvl(calc_rate.actual_prev_rate, cr.actual_rate)
                                 end)
       + ((-(lis.lt_obligation_remeasurement + lis.unaccrued_interest_remeasure) * nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)) + ((lis.obligation_reclass - lis.unaccrued_interest_reclass) * nvl(calc_rate.actual_rate, cr.actual_rate)))
     )
   end lt_currency_gain_loss,
   case
    when lis.is_om = 1 then
     0
    when lis.contract_currency_id = cs.currency_id then
     0
    else
      (
       ((lis.end_liability * nvl(calc_rate.actual_rate, cr.actual_rate)) - (lis.end_lt_liability * nvl(calc_rate.actual_rate, cr.actual_rate)))
       - ((lis.beg_liability * case when lis.is_om = 0 and lis.month = lis.first_month then
                                 nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                               else
                                 nvl(calc_rate.actual_prev_rate, cr.actual_rate)
                               end)
          - (lis.beg_lt_liability * case when lis.is_om = 0 and lis.month = lis.first_month then
                                      nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                                    else
                                      nvl(calc_rate.actual_prev_rate, cr.actual_rate)
                                    end))
       - (lis.interest_accrual * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)))
       + (lis.interest_paid * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)))
       + (lis.principal_paid * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)))
       - (lis.obligation_reclass * nvl(calc_rate.actual_rate, cr.actual_rate))
       - (lis.st_obligation_remeasurement * nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate))
       + (lis.unaccrued_interest_reclass * nvl(calc_rate.actual_rate, cr.actual_rate))
       + (lis.unaccrued_interest_remeasure * nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate))
     ) +
     (
       (lis.end_lt_liability * nvl(calc_rate.actual_rate, cr.actual_rate))
       - (lis.beg_lt_liability * case when lis.is_om = 0 and lis.month = lis.first_month then
                                   nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)
                                 else
                                   nvl(calc_rate.actual_prev_rate, cr.actual_rate)
                                 end)
       + ((-(lis.lt_obligation_remeasurement + lis.unaccrued_interest_remeasure) * nvl(nvl(cur.contract_approval_rate, lis.effective_in_svc_rate), cr.current_rate)) + ((lis.obligation_reclass - lis.unaccrued_interest_reclass) * nvl(calc_rate.actual_rate, cr.actual_rate)))
     )
   end gain_loss_fx,
   lis.obligation_reclass * nvl(calc_rate.actual_rate, cr.actual_rate) obligation_reclass,
   lis.unaccrued_interest_reclass * nvl(calc_rate.actual_rate, cr.actual_rate) unaccrued_interest_reclass,
   lis.begin_accum_impair * nvl(nvl(cur.contract_approval_rate, lis.net_weighted_avg_rate2), cr.current_rate) begin_accum_impair,
   lis.impairment_activity * decode(open_month.exchange_rate_type_id, 4, nvl(calc_rate.average_rate, cr.average_rate), nvl(calc_rate.actual_rate, cr.actual_rate)) impairment_activity,
   lis.end_accum_impair * nvl(nvl(cur.contract_approval_rate,
                     case
                       when lis.month < nvl(lis.remeasurement_date, lis.month) then
                      lis.net_weighted_avg_rate2
                       else
                      lis.net_weighted_avg_rate
                     end),
                   cr.current_rate) end_accum_impair,
   lis.escalation_month escalation_month,
   lis.escalation_pct escalation_pct
FROM 
(
    SELECT 
      lis.ilr_id
      , lis.revision
      , lis.set_of_books_id
      , lis.month
      , l.contract_currency_id
      , lis.residual_amount, lis.term_penalty, lis.bpo_price, lis.beg_capital_cost, lis.end_capital_cost
      , lis.beg_obligation, lis.end_obligation, lis.beg_lt_obligation, lis.end_lt_obligation, lis.principal_remeasurement
      , lis.beg_liability, lis.end_liability, lis.beg_lt_liability, lis.end_lt_liability, lis.liability_remeasurement
      , lis.interest_accrual, lis.principal_accrual, lis.interest_paid, lis.principal_paid
      , lis.executory_accrual1, lis.executory_accrual2, lis.executory_accrual3, lis.executory_accrual4, lis.executory_accrual5
      , lis.executory_accrual6, lis.executory_accrual7, lis.executory_accrual8, lis.executory_accrual9, lis.executory_accrual10
      , lis.executory_paid1, lis.executory_paid2, lis.executory_paid3, lis.executory_paid4, lis.executory_paid5
      , lis.executory_paid6, lis.executory_paid7, lis.executory_paid8, lis.executory_paid9, lis.executory_paid10
      , lis.contingent_accrual1, lis.contingent_accrual2, lis.contingent_accrual3, lis.contingent_accrual4, lis.contingent_accrual5
      , lis.contingent_accrual6, lis.contingent_accrual7, lis.contingent_accrual8, lis.contingent_accrual9, lis.contingent_accrual10
      , lis.contingent_paid1, lis.contingent_paid2, lis.contingent_paid3, lis.contingent_paid4, lis.contingent_paid5
      , lis.contingent_paid6, lis.contingent_paid7, lis.contingent_paid8, lis.contingent_paid9, lis.contingent_paid10
      , lis.current_lease_cost, lis.beg_deferred_rent, lis.deferred_rent, lis.end_deferred_rent, lis.beg_st_deferred_rent
      , lis.end_st_deferred_rent, lis.initial_direct_cost, lis.incentive_amount
      , ldf.depr_expense, ldf.begin_reserve, ldf.end_reserve, ldf.depr_exp_alloc_adjust
      , lis.beg_prepaid_rent, lis.prepay_amortization, lis.prepaid_rent
      , lis.end_prepaid_rent, lis.beg_net_rou_asset, lis.end_net_rou_asset
      , lis.rou_asset_remeasurement, lis.partial_term_gain_loss
      , lis.beg_arrears_accrual, lis.arrears_accrual, lis.end_arrears_accrual, lis.additional_rou_asset
      , lis.executory_adjust, lis.contingent_adjust
      , nvl(lis.obligation_reclass, 0) obligation_reclass
      , nvl(lis.st_obligation_remeasurement, 0) st_obligation_remeasurement
	  , nvl(lis.lt_obligation_remeasurement, 0) lt_obligation_remeasurement
      , nvl(lis.unaccrued_interest_reclass, 0) unaccrued_interest_reclass
      , nvl(lis.unaccrued_interest_remeasure, 0) unaccrued_interest_remeasure
      , NVL(lis.begin_accum_impair,0) begin_accum_impair
      , NVL(lis.impairment_activity,0) impairment_activity
      , NVL(lis.end_accum_impair,0) end_accum_impair
      , lis.is_om
	  , lis.escalation_month escalation_month
	  , lis.escalation_pct escalation_pct
      , ilr.lease_id
      , l.lease_number
      , ilr.company_id
      , ilr.ilr_number
      , ilr.current_revision
      , opt.in_service_exchange_rate
      , liasob.net_present_value
      , liasob.capital_cost
      , opt.lease_cap_type_id
      , opt.purchase_option_amt
      , opt.termination_amt
      , opt.in_service_exchange_rate AS effective_in_svc_rate 
      , orig_in_svc.in_service_exchange_rate as original_in_svc_rate
      , opt.remeasurement_date remeasurement_date
      , all_war.gross_weighted_avg_rate
      , all_war.net_weighted_avg_rate
      , case when lis.month <= nvl(opt.remeasurement_date, lis.month) then
        all_war.prior_gross_weighted_avg_rate
        else
        all_war.gross_weighted_avg_rate
        end gross_weighted_avg_rate2
      , case when lis.month <= nvl(opt.remeasurement_date, lis.month) then
        all_war.prior_net_weighted_avg_rate
        else
        all_war.net_weighted_avg_rate
        end net_weighted_avg_rate2
      , trunc(lis.month, 'MONTH') trunc_month
      , min_on_bs.first_month
    FROM ls_ilr_schedule lis
    JOIN ls_ilr ilr 
      ON lis.ilr_id = ilr.ilr_id
    JOIN ls_lease l 
      ON l.lease_id = ilr.lease_id
    JOIN ls_ilr_amounts_set_of_books liasob 
      ON lis.ilr_id = liasob.ilr_id
      AND lis.revision = liasob.revision
      AND lis.set_of_books_id = liasob.set_of_books_id
    JOIN ls_ilr_options opt
      ON opt.ilr_id = lis.ilr_id
      AND opt.revision = lis.revision
    LEFT JOIN 
    (
      SELECT
        war.ilr_id
        , opt.revision
        , war.set_of_books_id
        , war.effective_month
        , NVL(LEAD(war.effective_month) OVER (PARTITION BY war.ilr_id, war.set_of_books_id, opt.revision ORDER BY war.effective_month), TO_DATE('999912', 'YYYYMM')) next_effective_month
        , war.gross_weighted_avg_rate
        , NVL(LAG(war.gross_weighted_avg_rate) OVER (PARTITION BY war.ilr_id, war.set_of_books_id, opt.revision ORDER BY war.effective_month), war.gross_weighted_avg_rate) prior_gross_weighted_avg_rate
        , war.net_weighted_avg_rate
        , NVL(LAG(war.net_weighted_avg_rate) OVER (PARTITION BY war.ilr_id, war.set_of_books_id, opt.revision ORDER BY war.effective_month), war.net_weighted_avg_rate) prior_net_weighted_avg_rate
      FROM
      (
        SELECT
          ilr.ilr_id
          , lio.revision
          , sob.set_of_books_id
          , TRUNC(NVL(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month
          , ROW_NUMBER() OVER(PARTITION BY ilr.ilr_id, sob.set_of_books_id, TRUNC(NVL(lio.remeasurement_date, ilr.est_in_svc_date), 'month') ORDER BY DECODE(lia.approval_status_id, 3, 0, lia.approval_status_id), lia.approval_date DESC) rev_rnk
          , Nvl(war.gross_weighted_avg_rate, lio.in_service_exchange_rate) gross_weighted_avg_rate
          , Nvl(war.net_weighted_avg_rate, lio.in_service_exchange_rate) net_weighted_avg_rate
        FROM ls_ilr ilr
        JOIN ls_ilr_options lio
          ON lio.ilr_id = ilr.ilr_id
        CROSS JOIN set_of_books sob
        JOIN ls_ilr_approval lia
        ON lia.ilr_id = ilr.ilr_id
          AND lia.revision = lio.revision
        LEFT JOIN ls_ilr_weighted_avg_rates war
          ON war.ilr_id = ilr.ilr_id
          AND war.revision = lio.revision
          AND war.set_of_books_id = sob.set_of_books_id
        WHERE lio.revision > 0
          AND lia.approval_date IS NOT NULL
      ) war
      JOIN ls_ilr_options opt 
        ON opt.ilr_id = war.ilr_id
      WHERE rev_rnk = 1
    ) all_war
      ON all_war.ilr_id = lis.ilr_id
      AND all_war.revision = lis.revision
      AND all_war.set_of_books_id = lis.set_of_books_id
      AND all_war.effective_month <= lis.MONTH
      AND all_war.next_effective_month > lis.MONTH
    LEFT JOIN 
    (
      SELECT 
        la.ilr_id
        , ldf.revision
        , ldf.set_of_books_id
        , ldf.month
        , sum(ldf.depr_expense)         as depr_expense
        , sum(ldf.begin_reserve)         as begin_reserve
        , sum(ldf.end_reserve)           as end_reserve
        , sum(ldf.depr_exp_alloc_adjust) as depr_exp_alloc_adjust
      FROM ls_asset la
      JOIN ls_depr_forecast ldf
        ON ldf.ls_asset_id = la.ls_asset_id
      GROUP BY 
        la.ilr_id
        , ldf.revision
        , ldf.set_of_books_id
        , ldf.month
    ) ldf 
      ON lis.ilr_id = ldf.ilr_id
      AND lis.revision = ldf.revision
      AND lis.set_of_books_id = ldf.set_of_books_id
      AND lis.month = ldf.month
    LEFT JOIN
    (
      SELECT
        lio.ilr_id
        , lio.in_service_exchange_rate
      FROM ls_ilr_options lio
      WHERE (ilr_id, revision) IN
      (
        SELECT
          ilr_id
          , First_Value(revision) OVER (PARTITION BY ilr_id ORDER BY approval_date) first_rev
        FROM ls_ilr_approval
        WHERE approval_status_id NOT IN (1,4,5,7)
      )
    ) orig_in_svc
      ON orig_in_svc.ilr_id = lis.ilr_id
    LEFT JOIN 
    (
      SELECT 
        ilr_id
        , revision
        , set_of_books_id
        , Min(month) first_month
      FROM ls_ilr_schedule
      WHERE is_om = 0
      GROUP BY 
        ilr_id
        , revision
        , set_of_books_id
    ) min_on_bs
      ON min_on_bs.ilr_id = lis.ilr_id
      AND min_on_bs.revision = lis.revision
      AND min_on_bs.set_of_books_id = lis.set_of_books_id 
  ) lis
  JOIN currency_schema cs
    ON lis.company_id = cs.company_id
    AND cs.currency_type_id = 1
  JOIN
  (
    SELECT 
      ls_currency_type_id AS ls_cur_type
      , currency_id
      , currency_display_symbol
      , iso_code
      , CASE ls_currency_type_id WHEN 1 THEN 1 ELSE NULL END AS contract_approval_rate
    FROM currency
    CROSS JOIN ls_lease_currency_type
  ) cur 
    ON cur.currency_id = CASE cur.ls_cur_type
      WHEN 1 THEN lis.contract_currency_id
      WHEN 2 THEN cs.currency_id
    END
  JOIN
  (
    SELECT 
      ls_process_control.company_id
      , DECODE( lower(sc.control_value), 'yes', 4, 1) exchange_rate_type_id
      , Min(gl_posting_mo_yr) open_month
    FROM ls_process_control
    JOIN pp_system_control_companies sc 
      ON ls_process_control.company_id = sc.company_id
    WHERE ls_process_control.open_next IS NULL
      AND LOWER(TRIM(sc.control_name)) = 'lease mc: use average rates'
    GROUP BY 
      ls_process_control.company_id
      , DECODE(LOWER(sc.control_value), 'yes', 4, 1)
  ) open_month
    ON lis.company_id = open_month.company_id
  JOIN currency_rate_default_vw cr 
    ON cr.currency_from = lis.contract_currency_id
    AND cr.currency_to = cur.currency_id
    AND trunc(cr.exchange_date, 'MONTH') <= lis.trunc_month
    AND trunc(cr.next_date, 'MONTH') > lis.trunc_month
  LEFT JOIN
  (
    SELECT * FROM
    (
      SELECT
        company_id
        , contract_currency_id
        , company_currency_id
        , exchange_rate_type_id
        , accounting_month
        , exchange_date
        , rate
        , Lag(exchange_date) OVER (PARTITION BY company_id, contract_currency_id, company_currency_id, exchange_rate_type_id ORDER BY accounting_month) prev_exchange_date
        , Lag(rate) OVER (PARTITION BY company_id, contract_currency_id, company_currency_id, exchange_rate_type_id ORDER BY accounting_month) prev_rate
      FROM
      (     
        SELECT company_id, contract_currency_id, company_currency_id, exchange_rate_type_id, accounting_month, exchange_date, rate 
        FROM ls_lease_calculated_date_rates
        UNION ALL
        SELECT company_id, contract_currency_id, company_currency_id, exchange_rate_type_id, Add_Months(Max(accounting_month),1) accounting_month, NULL exchange_date, NULL rate 
        FROM ls_lease_calculated_date_rates
        GROUP BY company_id, contract_currency_id, company_currency_id, exchange_rate_type_id
      )
    )
    PIVOT
    (
      Max(exchange_date) AS exchange_date
      , max(rate) AS rate
      , max(prev_exchange_date) AS prev_exchange_date
      , max(prev_rate) AS prev_rate
      FOR exchange_rate_type_id IN (1 AS Actual, 4 AS Average)
    )
  ) calc_rate
    ON lis.contract_currency_id = calc_rate.contract_currency_id
    AND cur.currency_id = calc_rate.company_currency_id
    AND lis.company_id = calc_rate.company_id
    AND lis.trunc_month = calc_rate.accounting_month;