
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."COMPANY_CONSOL_VIEW" ("PP_TREE_CATEGORY_ID", "CONSOLIDATION_ID", "PARENT_ID", "COMPANY_ID", "DESCRIPTION", "SORT_ORDER", "PERCENT") AS 
  select PP_TREE_CATEGORY_ID,
       CONSOLIDATION_ID,
       PARENT_ID,
       MAIN_VIEW.COMPANY_ID,
       DESCRIPTION,
       SORT_ORDER,
       NVL(COMPANY_PERCENT.PERCENT, 1) PERCENT
  from (select B.PP_TREE_CATEGORY_ID,
               B.PP_TREE_TYPE_ID CONSOLIDATION_ID,
               B.MAIN_CHILD PARENT_ID,
               B.CHILD_ID COMPANY_ID,
               NVL(A.OVERRIDE_PARENT_DESCRIPTION, NVL(COMPANY.DESCRIPTION, 'NO DESCRIPTION')) DESCRIPTION,
               B.SORT_ORDER
          from (select distinct PP_TREE_TYPE_ID, PARENT_ID, OVERRIDE_PARENT_DESCRIPTION
                  from (select PP_TREE_CATEGORY_ID,
                               PP_TREE_TYPE_ID,
                               PARENT_ID,
                               CHILD_ID,
                               EMPTY_NODE,
                               OVERRIDE_PARENT_DESCRIPTION,
                               NVL(SORT_ORDER, 0) SORT_ORDER
                          from PP_TREE
                         where PP_TREE_CATEGORY_ID = 1)
                 where OVERRIDE_PARENT_DESCRIPTION is not null) A,
               COMPANY COMPANY,
               (select A.PP_TREE_CATEGORY_ID,
                       A.PP_TREE_TYPE_ID,
                       A.PARENT_ID,
                       A.CHILD_ID,
                       level                 LEVEL_NUM,
                       CONNECT_BY_ROOT       A.CHILD_ID MAIN_CHILD,
                       A.EMPTY_NODE,
                       CONNECT_BY_ROOT       A.SORT_ORDER SORT_ORDER
                  from (select PP_TREE_CATEGORY_ID,
                               PP_TREE_TYPE_ID,
                               PARENT_ID,
                               CHILD_ID,
                               EMPTY_NODE,
                               OVERRIDE_PARENT_DESCRIPTION,
                               NVL(SORT_ORDER, 0) SORT_ORDER
                          from PP_TREE
                         where PP_TREE_CATEGORY_ID = 1) A
                 start with A.CHILD_ID is not null
                connect by NOCYCLE prior A.CHILD_ID = A.PARENT_ID
                       and prior A.PP_TREE_TYPE_ID = A.PP_TREE_TYPE_ID) B,
               (select A.PP_TREE_CATEGORY_ID, A.PP_TREE_TYPE_ID, A.PARENT_ID, A.CHILD_ID, level LEVEL_NUM
                  from (select PP_TREE_CATEGORY_ID,
                               PP_TREE_TYPE_ID,
                               PARENT_ID,
                               CHILD_ID,
                               EMPTY_NODE,
                               OVERRIDE_PARENT_DESCRIPTION,
                               NVL(SORT_ORDER, 0) SORT_ORDER
                          from PP_TREE
                         where PP_TREE_CATEGORY_ID = 1) A
                 start with A.CHILD_ID = A.PARENT_ID
                connect by NOCYCLE prior A.CHILD_ID = A.PARENT_ID
                       and prior A.PP_TREE_TYPE_ID = A.PP_TREE_TYPE_ID) C
         where B.MAIN_CHILD = A.PARENT_ID(+)
           and B.PP_TREE_TYPE_ID = A.PP_TREE_TYPE_ID(+)
           and B.MAIN_CHILD = COMPANY.COMPANY_ID(+)
           and B.MAIN_CHILD = C.CHILD_ID
           and B.PP_TREE_TYPE_ID = C.PP_TREE_TYPE_ID
           and B.MAIN_CHILD in (select COMPANY_ID
                                  from COMPANY
                                union
                                select CHILD_ID
                                  from (select PP_TREE_CATEGORY_ID,
                                               PP_TREE_TYPE_ID,
                                               PARENT_ID,
                                               CHILD_ID,
                                               EMPTY_NODE,
                                               OVERRIDE_PARENT_DESCRIPTION,
                                               NVL(SORT_ORDER, 0) SORT_ORDER
                                          from PP_TREE
                                         where PP_TREE_CATEGORY_ID = 1) INSIDE
                                 where INSIDE.EMPTY_NODE = 1
                                   and B.PP_TREE_TYPE_ID = INSIDE.PP_TREE_TYPE_ID)
        union
        select 1 PP_TREE_CATEGORY_ID,
               -1 CONSOLIDATION_ID,
               COMPANY_ID PARENT_ID,
               COMPANY_ID COMPANY_ID,
               DESCRIPTION,
               0 SORT_ORDER
          from COMPANY COMPANY) MAIN_VIEW,
       COMPANY_PERCENT
 where MAIN_VIEW.COMPANY_ID = COMPANY_PERCENT.COMPANY_ID(+)
 
 ;