
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_SUMMARY_COMMITMENTS" ("WO_AMOUNT", "WORK_ORDER_ID", "COMPANY_ID", "CHARGE_TYPE_ID", "MONTH_NUMBER", "EXPENDITURE_TYPE_ID") AS 
  select sum(wo_amount), work_order_id, company_id, charge_type_id, month_number, expenditure_type_id
from( select sum(amount) wo_amount, expenditure_type_id, a.work_order_id work_order_id, a.company_id company_id,
nvl(min(b.charge_type_id),-1) charge_type_id, nvl(a.month_number, nvl(to_char(a.est_start_date,'yyyymm'),
nvl(to_char(a.charge_mo_yr,'yyyymm'), nvl(to_char(a.time_stamp,'yyyymm'),
to_char(sysdate,'yyyymm'))))) month_number
from PWRPLANT.commitments a, PWRPLANT.charge_type b
where a.est_chg_type_id = b.est_chg_type_id (+)
and (b.charge_type_id in (Select min(charge_Type_id)
from      PWRPLANT.charge_type ct where ct.est_chg_type_id = a.est_chg_type_id) or a.est_chg_type_id
not in(select est_chg_type_id from PWRPLANT.charge_type ct where ct.est_chg_type_id = a.est_chg_type_id))
group by a.month_number, a.est_start_date, a.charge_mo_yr,
a.time_stamp,a.work_order_id, a.est_chg_type_id, b.est_chg_type_id,expenditure_type_id , a.company_id
union select wo_amount, expenditure_type_id , work_order_id, company_id, charge_type_id, month_number
from PWRPLANT.wo_summary ) group by work_order_id, charge_type_id, month_number,expenditure_type_id, company_id
 
 
 
 
 
 ;