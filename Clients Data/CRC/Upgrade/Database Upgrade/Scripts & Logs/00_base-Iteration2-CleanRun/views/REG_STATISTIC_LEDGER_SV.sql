
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."REG_STATISTIC_LEDGER_SV" ("HISTORIC_LEDGER", "FORECAST_LEDGER", "REG_COMPANY", "STATISTIC", "MONTH_NUMBER", "STAT_VALUE", "UNIT_OF_MEASURE") AS 
  (
select h.long_description historic_ledger,
		 f.description forecast_ledger,
		 c.description reg_company,
	    s.description statistic,
		 l.month_number month_number,
		 l.stat_value stat_value,
		 s.unit_of_measure unit_of_measure
  from reg_statistic_ledger l,
		 reg_statistic s,
		 reg_historic_version h,
		 reg_forecast_version f,
		 (select distinct reg_company_id, description from reg_company_sv) c
 where l.historic_version_id = h.historic_version_id
	and l.forecast_version_id = f.forecast_version_id
	and l.reg_company_id = c.reg_company_id
	and l.reg_stat_id = s.reg_stat_id)
 ;