CREATE OR REPLACE VIEW v_lsr_termination_st_df_vw (
  ilr_id,
  termination_date,
  current_lease_receivable,
  future_payment,
  net_lease_receivable,
  unguaranteed_residual,
  net_investment,
  purchase_option_amount,
  termination_amount,
  other_future_payments,
  termination_source,
  comments,
  company_id,
  ls_cur_type,
  contract_currency_id,
  display_currency_id,
  company_currency_id,
  rate,
  iso_code,
  currency_display_symbol,
  set_of_books_id
) AS
with cur AS (
  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
    contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
  FROM currency contract_cur
  UNION
  SELECT 2, company_cur.currency_id,
    company_cur.currency_display_symbol, company_cur.iso_code, 0
  FROM currency company_cur
)
SELECT
 lit_stdf.ilr_id,
 lit_stdf.termination_date,
  lit_stdf.current_lease_receivable * decode(ls_cur_type, 2, nvl(lit_stdf.termination_exchange_rate, historic_rate), 1) current_lease_receivable,
  lit_stdf.future_payment * decode(ls_cur_type, 2, nvl(lit_stdf.termination_exchange_rate, historic_rate), 1) future_payment,
  lit_stdf.net_lease_receivable * decode(ls_cur_type, 2, nvl(lit_stdf.termination_exchange_rate, historic_rate), 1) net_lease_receivable,
  lit_stdf.unguaranteed_residual * decode(ls_cur_type, 2, nvl(lit_stdf.termination_exchange_rate, historic_rate), 1) unguaranteed_residual,
  lit_stdf.net_investment * decode(ls_cur_type, 2, nvl(lit_stdf.termination_exchange_rate, historic_rate), 1) net_investment,
  lit_stdf.purchase_option_amount * decode(ls_cur_type, 2, nvl(lit_stdf.termination_exchange_rate, historic_rate), 1) purchase_option_amount,
  lit_stdf.termination_amount * decode(ls_cur_type, 2, nvl(lit_stdf.termination_exchange_rate, historic_rate), 1) termination_amount,
  lit_stdf.other_future_payments * decode(ls_cur_type, 2, nvl(lit_stdf.termination_exchange_rate, historic_rate), 1) other_future_payments,
 lit_stdf.termination_source termination_source,
 lit_stdf.comments comments,
 ilr.company_id company_id,
 cur.ls_cur_type ls_cur_type,
 lease.contract_currency_id,
 cur.currency_id display_currency_id,
 cs.currency_id company_currency_id,
 decode(ls_cur_type, 2, nvl(lit_stdf.termination_exchange_rate, historic_rate), 1) rate,
 cur.iso_code,
 cur.currency_display_symbol,
 lit_stdf.set_of_books_id
 FROM lsr_ilr_termination_st_df lit_stdf
 INNER JOIN lsr_ilr ilr
    ON ilr.ilr_id = lit_stdf.ilr_id
 INNER JOIN currency_schema cs
    ON ilr.company_id = cs.company_id
 INNER JOIN lsr_lease lease
    ON ilr.lease_id = lease.lease_id
 INNER JOIN cur
    ON (cur.ls_cur_type = 1 AND
       cur.currency_id = lease.contract_currency_id)
    OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
 WHERE cs.currency_type_id = 1;
