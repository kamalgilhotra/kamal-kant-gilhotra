
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CR_BUDGET_DATA_ENTRY_V" ("ID", "BUDGET_VERSION", "COMPANY", "COMPANY_ROLLUP", "DIVISION", "DEPARTMENT", "BUSINESS_SEGMENT", "COMPANY_BUS_SEGMENT", "COST_CENTER", "FERC_ACCOUNT", "FERC_ACCOUNT_DESCR", "OPERATING_SEGMENT", "EXPENSE_TYPE", "PROJECT", "WORK_ORDER", "WORK_ORDER_DESCR", "CHARGING_COST_CENTER", "CHARGING_COST_CENTER_DESCR", "CHARGING_EXPENSE_TYPE", "CHARGING_EXP_TYPE_DESCR", "GAAP", "RAP", "ALLOCATOR", "GL_SOURCE", "FUTURE_USE_1", "FUTURE_USE_2", "YEAR", "TYPE", "PRIOR_YEAR_ACTUALS", "CURRENT_YEAR_ACTUALS", "CURRENT_APPROVED_BUDGET", "CURRENT_APPROVED_FORECAST", "TOTAL", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "CR_BUDGET_VERSION_ID", "TIME_STAMP", "USER_ID", "ALLOCATION_ID", "SPLIT_LABOR_ROW", "LINE_DESCRIPTION", "CR_TXN_TYPE", "CR_DERIVATION_RATE", "CR_DERIVATION_ID", "INTERFACE_BATCH_ID", "CONTROL_TOTAL", "CONSULTANT", "JUSTIFICATION") AS 
  (
SELECT a.id,
        cbv.budget_version,
        a.company company,
        b.co_rollup_description,
        b.div_description,
        b.dept_description,
        a.business_segment business_segment,
        a.company_bus_segment company_bus_segment,
        b.description,
        a.ferc_account||' '||c.description ferc_account,
        c.description ferc_account_descr,
        a.operating_segment operating_segment,
        a.expense_type expense_type,
        a.project project,
        a.work_order work_order,
        e.description work_order_descr,
        a.charging_cost_center charging_cost_center,
        b.description charging_cost_center_descr,
        a.charging_expense_type||' '||d.description charging_expense_type,
        d.description charging_exp_type_descr,
        a.gaap gaap,
        a.rap rap,
        a.allocator allocator,
        a.gl_source gl_source,
        a.future_use_1 future_use_1,
        a.future_use_2 future_use_2,
        a.year year,
        a.type type,
        a.prior_year_actuals prior_year_actuals,
        a.current_year_actuals current_year_actuals,
        a.current_approved_budget current_approved_budget,
        a.current_approved_forecast current_approved_forecast,
        a.total total,
        a.jan jan,
        a.feb feb,
        a.mar mar,
        a.apr apr,
        a.may may,
        a.jun jun,
        a.jul jul,
        a.aug aug,
        a.sep sep,
        a.oct oct,
        a.nov nov,
        a.dec dec,
        a.cr_budget_version_id cr_budget_version_id,
        a.time_stamp time_stamp,
        a.user_id user_id,
        a.allocation_id allocation_id,
        a.split_labor_row split_labor_row,
        a.line_description line_description,
        a.cr_txn_type cr_txn_type,
        a.cr_derivation_rate cr_derivation_rate,
        a.cr_derivation_id cr_derivation_id,
        a.interface_batch_id interface_batch_id,
        a.control_total control_total,
        con.consultant consultant,
        con.justification justification
    FROM cr_budget_data_entry a,
                cr_epe_charging_cost_center_mv b,
                cr_epe_account_mv c,
                cr_epe_charging_exp_type_mv d,
                cr_epe_work_order_mv e ,
                cr_budget_version cbv,
                cr_epe_om_budget_con_info_s con,
                cr_budget_version_security cbvs
            WHERE a.id = con.id (+)
                        and a.cr_budget_version_id = cbv.cr_budget_version_id
                        and upper(trim(cbv.budget_version)) = upper(trim(cbvs.budget_version))
                        and upper(trim(user)) = upper(trim(cbvs.users))
                        and a.charging_cost_center in (
SELECT budget_value
    FROM cr_budget_users_valid_values
            WHERE upper(trim(users)) = upper(trim(user)))
                        and a.charging_cost_center = b.charging_cost_center (+)
                        and b.element_type (+) = 'Actuals'
                        and not b.co_rollup_description is null
                        and not b.div_description is null
                        and not b.dept_description is null
                        and a.ferc_account = c.account (+)
                        and c.element_type (+) = 'Actuals'
                        and a.charging_expense_type = d.charging_expense_type (+)
                        and d.element_type (+) = 'Budget'
                        and a.work_order = e.work_order (+)
                        and e.element_type (+) = 'Actuals' ) 
 
 
 
 
 
 ;