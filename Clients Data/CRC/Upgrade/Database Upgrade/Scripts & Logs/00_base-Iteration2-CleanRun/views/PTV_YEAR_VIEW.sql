
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."PTV_YEAR_VIEW" ("TAX_YEAR", "PROP_TAX_COMPANY_ID", "STATE_ID", "YEAR", "VALUATION_YEAR") AS 
  select		property_tax_year.tax_year tax_year,  				all_data.prop_tax_company_id prop_tax_company_id,  				all_data.state_id state_id,  				nvl( pt_year_override.add_activity_year, 0 ) + property_tax_year.year year,  				nvl( pt_year_override.add_valuation_year, 0 ) + property_tax_year.valuation_year valuation_year  	from		pt_year_override,  				property_tax_year,  				(	select		pt_company.prop_tax_company_id prop_tax_company_id,  								state.state_id state_id  					from		pt_company,  								state				  				) all_data  	where	all_data.prop_tax_company_id = pt_year_override.prop_tax_company_id (+)  	and		all_data.state_id = pt_year_override.state_id (+)
 
 
 
 
 ;