
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CPR_ACT_MONTH_SIMPLE_VIEW" ("ASSET_ID", "START_MONTH", "ACCUM_QUANTITY", "ACCUM_COST") AS 
  SELECT "CPR_ACTIVITY"."ASSET_ID",
       "REPORT_TIME"."START_MONTH",
       SUM("CPR_ACTIVITY"."ACTIVITY_QUANTITY") accum_quantity,
       SUM("CPR_ACTIVITY"."ACTIVITY_COST") accum_cost
  FROM "REPORT_TIME",
       "CPR_ACTIVITY"
 WHERE to_number(to_char("CPR_ACTIVITY"."GL_POSTING_MO_YR",
                         'J')) >
       to_number(to_char("REPORT_TIME"."START_MONTH",
                         'J'))
   AND upper("REPORT_TIME"."USER_ID") = USER
   AND "REPORT_TIME"."SESSION_ID" = userenv('sessionid')
 GROUP BY "REPORT_TIME"."START_MONTH",
          "CPR_ACTIVITY"."ASSET_ID"
 
 
 
 
 
 ;