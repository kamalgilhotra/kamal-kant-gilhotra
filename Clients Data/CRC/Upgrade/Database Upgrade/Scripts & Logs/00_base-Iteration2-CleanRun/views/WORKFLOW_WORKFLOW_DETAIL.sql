
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WORKFLOW_WORKFLOW_DETAIL" ("WORKFLOW_ID", "ID", "WORKFLOW_RULE_ID", "WORKFLOW_RULE_DESC", "USERS", "DATE_APPROVED", "USER_APPROVED", "APPROVAL_STATUS_ID", "REQUIRED", "NOTES", "AUTHORITY_LIMIT", "NUM_APPROVERS", "NUM_REQUIRED", "SQL", "USE_LIMITS", "RULE_ORDER", "APPROVAL_AMOUNT", "MAX_REQUIRED_AUTHORITY", "ALLOW_EDIT", "ORIG_AUTHORITY_LIMIT", "IS_CUSTOM_LIMIT", "GROUP_APPROVAL", "SQL_USERS", "USERS_DISPLAY") AS 
  SELECT wfd.WORKFLOW_ID,
  wfd.ID,
  wfd.WORKFLOW_RULE_ID,
  wfd.WORKFLOW_RULE_DESC,
  wfd.USERS,
  wfd.DATE_APPROVED,
  wfd.USER_APPROVED,
  wfd.APPROVAL_STATUS_ID,
  wfd.REQUIRED,
  wfd.NOTES,
  wfd.AUTHORITY_LIMIT,
  wfd.NUM_APPROVERS,
  wfd.NUM_REQUIRED,
  wfd.SQL,
  wf.use_limits,
  wfd.RULE_ORDER,
  wf.approval_amount,
  wfmra.max_required_authority,
  nvl(allow_edit,1) allow_edit,
  orig_authority_limit,
  is_custom_limit,
  nvl(group_approval,0) group_approval,
  sql_users,
  nvl(trim(pp.last_name || nvl2(trim(pp.last_name),nvl2(trim(pp.first_name),', ',''),'')  || pp.first_name), pp.users) USERS_DISPLAY
FROM WORKFLOW_DETAIL wfd,
  workflow wf,
  workflow_max_required_auth wfmra,
  pp_security_users pp
where wfd.workflow_id = wf.workflow_id
and wf.workflow_id = wfmra.workflow_id
and upper(trim(wfd.users)) = upper(trim(pp.users))

 
 ;