
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_EST_SUMMARY_WO_CREDITS_V2" ("REVISION", "WORK_ORDER_ID", "HIST_ACTUALS", "APPROVED_DOLLARS", "CURRENT_YEAR_DOLLARS", "YEAR2_DOLLARS", "YEAR3_DOLLARS", "YEAR4_DOLLARS", "YEAR5_DOLLARS", "FUTURE_DOLLARS", "BUDGET_VERSION_ID") AS 
  select a.revision, a.work_order_id, sum(a.hist_actuals) hist_actuals, sum(total) approved_dollars,
sum(decode(year,bv.start_year,total,0)) current_year_dollars,
sum(decode(year,bv.start_year+1,total,0)) year2_dollars,
sum(decode(year,bv.start_year+2,total,0)) year3_dollars,
 sum(decode(year,bv.start_year+3,total,0)) year4_dollars,
sum(decode(year,bv.start_year+4,total,0)) year5_dollars,
sum(a.future_dollars) future_dollars, bv.budget_version_id
from wo_est_monthly a, estimate_charge_type b, budget_version_fund_proj bvfp, budget_version bv
where a.est_chg_type_id = b.est_chg_type_id and bvfp.active = 1 and
 bvfp.revision = a.revision and bvfp.work_order_id = a.work_order_id and
 bv.budget_version_id = bvfp.budget_version_id and        b.processing_type_id <> 6
group by a.revision, a.work_order_id,bv.budget_version_id 
 
 
 
 
 
 ;