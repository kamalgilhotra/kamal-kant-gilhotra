
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CPR_LDG_ZERO_ACTIVE_VIEW_TEST" ("ASSET_ID", "COMPANY_ID", "RETIREMENT_UNIT_ID", "UTILITY_ACCOUNT_ID", "BUS_SEGMENT_ID", "ASSET_LOCATION_ID") AS 
  SELECT cpr_ledger_post_test.asset_id,
       company_id,
       retirement_unit_id,
       utility_account_id,
       bus_segment_id,
       asset_location_id
FROM cpr_ledger_post_test, cpr_ldg_basis_post_test
WHERE cpr_ledger_post_test.asset_id = cpr_ldg_basis_post_test.asset_id
AND Abs(basis_1) + Abs(basis_2) + Abs(basis_3) + Abs(basis_4) + Abs(basis_5) + Abs(basis_6) +
	Abs(basis_7) + Abs(basis_8) + Abs(basis_9) + Abs(basis_10) + Abs(basis_11) +
	Abs(basis_12) + Abs(basis_13) + Abs(basis_14) + Abs(basis_15) + Abs(basis_16) +
	Abs(basis_17) + Abs(basis_18) + Abs(basis_19) + Abs(basis_20) + Abs(basis_21) +
	Abs(basis_22) + Abs(basis_23) + Abs(basis_24) + Abs(basis_25) + Abs(basis_26) +
	Abs(basis_27) + Abs(basis_28) + Abs(basis_29) + Abs(basis_30) + Abs(basis_31) +
	Abs(basis_32) + Abs(basis_33) + Abs(basis_34) + Abs(basis_35) + Abs(basis_36) +
	Abs(basis_37) + Abs(basis_38) + Abs(basis_39) + Abs(basis_40) + Abs(basis_41) +
	Abs(basis_42) + Abs(basis_43) + Abs(basis_44) + Abs(basis_45) + Abs(basis_46) +
	Abs(basis_47) + Abs(basis_48) + Abs(basis_49) + Abs(basis_50) + Abs(basis_51) +
	Abs(basis_52) + Abs(basis_53) + Abs(basis_54) + Abs(basis_55) + Abs(basis_56) +
	Abs(basis_57) + Abs(basis_58) + Abs(basis_59) + Abs(basis_60) + Abs(basis_61) +
	Abs(basis_62) + Abs(basis_63) + Abs(basis_64) + Abs(basis_65) + Abs(basis_66) +
	Abs(basis_67) + Abs(basis_68) + Abs(basis_69) + Abs(basis_70) > 0
 ;