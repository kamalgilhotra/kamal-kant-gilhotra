
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CR_OM500_V" ("CO", "COROLL", "Subtotal1", "Subtotal2", "COMPANY", "ACTUALSYTD", "DIAY", "BUDGETYTD", "DBY", "VARIANCE", "VARPERCENT", "PROJECTED_ACTUALS", "DPA", "PROJECTED_BUDGET", "DPB", "VARIANCE2", "VARPERCENT2", "CURRENT_MONTH") AS 
  SELECT      pq.co,
            decode(pq.co, '00','OP', '10', 'OT', '20', 'OT', '30', 'OT', '40', 'OT', '50', 'OT', '60', 'OT', '90', 'OT', '95', 'OT', '99', 'OT') as "COROLL",
			decode(pq.co, '00','1','0') as "Subtotal1",
			decode(pq.co, '10', '1', '20','1','30','1','40','1','50','1','60','1','90','1','95','1','99','1','0') as "Subtotal2",
     	    pq.company||decode(pq.co,'99',to_char(null),' ('||initcap(last_name)||')') company,
			round(sum(actualsytd)) actualsytd,
			round(sum(diay)) diay,
			round(sum(budgetytd)) budgetytd,
			round(sum(dby)) dby,
			round(sum(actualsytd)) - round(sum(budgetytd)) Variance,
					          round(case when round(sum(budgetytd)) = 0 then 100
							  	   else (round((sum(actualsytd)) - round(sum(budgetytd)))/round(sum(budgetytd)))*100
                              end,1) Varpercent,
			round(sum(projected_actuals)) projected_actuals,
			round(sum(dpa)) dpa,
			round(sum(projected_budget)) projected_budget,
			round(sum(dpb)) dpb,
			round(sum(projected_actuals)) - round(sum(projected_budget)) Variance2,
							  round(case when round(sum(projected_budget)) = 0 then 100
							  	   else ((round(sum(projected_actuals)) - round(sum(projected_budget)))/round(sum(projected_budget)))*100
							  end,1) Varpercent2,
	    (select min (case
            when control_id = 5
            then to_char(to_date(control_value, 'YYYYMM'), 'fmMonth YYYY')
            end)
            from cr_alloc_system_control) Current_Month
FROM xxepe_fin_rpt_deliveries cc, pp_security_users pp, (
select 	    c.company_rollup co,
            c.co_rollup_description company,
			sum (a.amount*decode(c.company_rollup,'00',-1,1)) actualsytd,
			sum (a.amount) diay,
			0 dby,
			0 budgetytd,
			0 projected_actuals,
			0 dpa,
			0 projected_budget,
			0 dpb
from 		cr_epe_charging_cost_center_mv c,
			cr_cost_repository_sv a,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			cr_epe_account_mv e
where	a.charging_cost_center = c.charging_cost_center
            and a.account = e.account
			and a.month_number between d.start_month and d.end_month
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and e.acct_group between 'A' and 'E'
group by  c.company_rollup,
          c.co_rollup_description
union
select 	    c.company_rollup co,
            c.co_rollup_description company,
			0 actualsytd,
			0 diay,
			sum(a.amount) dby,
			sum(a.amount*decode(c.company_rollup,'00',-1,1)) budgetytd,
			0 projected_actuals,
			0 dpa,
			0 projected_budget,
			0 dpb
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			cr_epe_account_mv e
where 	a.charging_cost_center = c.charging_cost_center
		    and a.ferc_account = e.account
			and a.budget_version = b.control_value
			and a.month_number between d.start_month and d.end_month
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and e.acct_group between 'A' and 'E'
group	 by	c.company_rollup,
            c.co_rollup_description
union
select 	    c.company_rollup co,
            c.co_rollup_description company,
			0 actuals_amount,
			0 diay,
			0 budget_amount,
			0 dby,
			sum(a.amount*decode(c.company_rollup,'00',-1,1)) projected_actuals,
			sum(a.amount) dpa,
			0 projected_budget,
			0 dpb
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'REVISED BUDGET VERSION') b,
			(select min (case
                        when control_id = 5
                        then substr(control_value,1,4)
                        end
                        )as allyear
                        from cr_alloc_system_control) d,
			cr_epe_account_mv e
where 	a.charging_cost_center = c.charging_cost_center
            and a.ferc_account = e.account
			and a.budget_version = b.control_value
			and substr(a.month_number,1,4) = d.allyear
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and e.acct_group between 'A' and 'E'
group	 by	 c.company_rollup,
             c.co_rollup_description
union
select      c.company_rollup co,
            c.co_rollup_description company,
			0 actuals_amount,
			0 diay,
			0 budget_amount,
			0 dby,
			0 projected_actuals,
			0 dpa,
			sum(a.amount*decode(c.company_rollup,'00',-1,1)) projected_budget,
			sum(a.amount) dpb
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
                        when control_id = 5
                        then substr(control_value,1,4)
                        end
                        )as allyear
                        from cr_alloc_system_control) d,
			cr_epe_account_mv e
where 	a.charging_cost_center = c.charging_cost_center
		    and a.ferc_account = e.account
			and a.budget_version = b.control_value
			and substr(a.month_number,1,4) = d.allyear
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and e.acct_group between 'A' and 'E'
group	 by	 c.company_rollup,
             c.co_rollup_description) pq
WHERE cc.COMPANY_ROLLUP = pq.co
  and cc.report_level in  ('A','B')
  and cc.deliver_to_user = pp.users
  and cc.source = 'H'
GROUP BY  pq.co, pq.company||decode(pq.co,'99',to_char(null),' ('||initcap(last_name)||')')
 
 
 
 
 
 ;