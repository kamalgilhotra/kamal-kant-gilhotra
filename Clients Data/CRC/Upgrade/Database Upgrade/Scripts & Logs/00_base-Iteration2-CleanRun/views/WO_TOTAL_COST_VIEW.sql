
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_TOTAL_COST_VIEW" ("WORK_ORDER_ID", "TOTAL_COST", "IN_PROCESS_BALANCE") AS 
  SELECT work_order_control.WORK_ORDER_ID,      SUM(nvl(TOTAL_COST, 0)) "TOTAL_COST",     SUM(nvl(IN_PROCESS_BALANCE, 0)) "IN_PROCESS_BALANCE"      FROM work_order_control, CHARGE_SUMMARY       WHERE MONTH = (select max(accounting_month)     from wo_process_control where wo_charge_collection is null)    and    	work_order_control.work_order_id = charge_summary.work_order_id (+)   and     work_order_control.funding_wo_indicator <> 1           GROUP BY work_order_control.WORK_ORDER_ID
 
 
 
 
 
 ;