
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."PEND_TRANS_WO_VIEW" ("PEND_TRANS_ID", "WORK_ORDER_NUMBER", "LENGTH") AS 
  SELECT pend_trans_id,
       rtrim(upper(work_order_control.work_order_number)) "WORK_ORDER_NUMBER",
       MAX(length(rtrim(work_order_control.work_order_number))) "LENGTH"
  FROM pend_transaction,
       work_order_control,
       report_time
 WHERE rtrim(upper(pend_transaction.work_order_number)) =
       rtrim(upper(work_order_control.work_order_number))
   AND to_char(pend_transaction.gl_posting_mo_yr,
               'MM/YYYY') = to_char(report_time.start_month,
                                    'MM/YYYY')
   AND upper(report_time.user_id) = USER
   AND (report_time.session_id = userenv('sessionid'))
 GROUP BY pend_trans_id,
          rtrim(upper(work_order_control.work_order_number))
 
 
 
 
 
 ;