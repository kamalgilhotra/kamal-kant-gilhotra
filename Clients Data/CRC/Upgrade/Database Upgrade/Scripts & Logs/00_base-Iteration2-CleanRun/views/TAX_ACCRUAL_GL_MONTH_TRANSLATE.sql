
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_GL_MONTH_TRANSLATE" ("GL_MONTH", "EXTERNAL_ACCOUNTING_DATE_DESCR", "CR_MONTH_NUMBER_OUT", "CR_MONTH_NUMBER_IN", "GL_PERIOD", "CR_YTD", "CALENDAR_MONTH", "YEAR_ADJUST") AS 
  select	period gl_month,
			'DAY_1_CURRENT_MONTH',
			accounting_period_out,
			accounting_period_in,
			accounting_period_out,
			0 cr_ytd,
			accounting_period_out,
			year_adjust
from tax_accrual_period
where ta_version_type_id = 1
 
 
 
 
 ;