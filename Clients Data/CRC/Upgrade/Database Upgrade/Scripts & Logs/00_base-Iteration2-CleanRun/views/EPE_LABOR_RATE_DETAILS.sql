
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."EPE_LABOR_RATE_DETAILS" ("COMPANY", "RATE_TYPE", "YEAR", "JAN_RATE", "FEB_RATE", "MAR_RATE", "APR_RATE", "MAY_RATE", "JUN_RATE", "JUL_RATE", "AUG_RATE", "SEP_RATE", "OCT_RATE", "NOV_RATE", "DEC_RATE") AS 
  select decode(rt.company_id, -1, 'All Companies', c.description) as company,
rt.description as rate_type, r.year, round(r.hrs_jan_rate, 2) as jan_rate,
round(r.hrs_feb_rate, 2) as feb_rate, round(r.hrs_mar_rate, 2) as mar_rate,
round(r.hrs_apr_rate, 2) as apr_rate, round(r.hrs_may_rate, 2) as may_rate,
round(r.hrs_jun_rate, 2) as jun_rate, round(r.hrs_jul_rate, 2) as jul_rate,
round(r.hrs_aug_rate, 2) as aug_rate, round(r.hrs_sep_rate, 2) as sep_rate,
round(r.hrs_oct_rate, 2) as oct_rate, round(r.hrs_nov_rate, 2) as nov_rate,
round(r.hrs_dec_rate, 2) as dec_rate
from rate_type rt, rate r, company c
where r.rate_type_id = rt.rate_type_id
and decode(rt.company_id, -1, -1, c.company_id) = rt.company_id
group by decode(rt.company_id, -1, 'All Companies', c.description),
rt.description, r.year, r.hrs_jan_rate, r.hrs_feb_rate, r.hrs_mar_rate,
r.hrs_apr_rate, r.hrs_may_rate, r.hrs_jun_rate, r.hrs_jul_rate,
r.hrs_aug_rate, r.hrs_sep_rate, r.hrs_oct_rate, r.hrs_nov_rate,
r.hrs_dec_rate
order by decode(rt.company_id, -1, 'All Companies', c.description),
rt.description, r.year
 
 
 
 
 
 ;