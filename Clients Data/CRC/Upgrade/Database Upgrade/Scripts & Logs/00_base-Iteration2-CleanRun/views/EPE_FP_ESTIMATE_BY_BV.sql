
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."EPE_FP_ESTIMATE_BY_BV" ("BUDGET_VERSION_ID", "WORK_ORDER", "YEAR", "EXPENDITURE_TYPE", "EST_CHG_TYPE", "DEPARTMENT", "BUDGET_PLANT_CLASS", "JOB_TASK", "JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER", "TOTAL", "HRS_JAN", "HRS_FEB", "HRS_MAR", "HRS_APR", "HRS_MAY", "HRS_JUN", "HRS_JUL", "HRS_AUG", "HRS_SEP", "HRS_OCT", "HRS_NOV", "HRS_DEC", "HRS_TOTAL", "QTY_JAN", "QTY_FEB", "QTY_MAR", "QTY_APR", "QTY_MAY", "QTY_JUN", "QTY_JUL", "QTY_AUG", "QTY_SEP", "QTY_OCT", "QTY_NOV", "QTY_DEC", "QTY_TOTAL", "FUTURE_DOLLARS", "HIST_ACTUALS", "COMPANY_ID") AS 
  select bvfp.budget_version_id, woc.work_order_number as work_order,
wem.year, et.description as expenditure_type, ect.description as est_chg_type,
d.description as department, bpc.description as budget_plant_class, jtl.description as job_task,
nvl(wem.january, 0) as january, nvl(wem.february, 0) as february, nvl(wem.march, 0) as march,
nvl(wem.april, 0) as april, nvl(wem.may, 0) as may, nvl(wem.june, 0) as june, nvl(wem.july, 0) as july,
nvl(wem.august, 0) as august, nvl(wem.september, 0) as september, nvl(wem.october, 0) as october,
nvl(wem.november, 0) as november, nvl(wem.december, 0) as december, nvl(wem.total, 0) as total,
nvl(wem.hrs_jan, 0) as hrs_jan, nvl(wem.hrs_feb, 0) as hrs_feb, nvl(wem.hrs_mar, 0) as hrs_mar,
nvl(wem.hrs_apr, 0) as hrs_apr, nvl(wem.hrs_may, 0) as hrs_may, nvl(wem.hrs_jun, 0) as hrs_jun,
nvl(wem.hrs_jul, 0) as hrs_jul, nvl(wem.hrs_aug, 0) as hrs_aug, nvl(wem.hrs_sep, 0) as hrs_sep,
nvl(wem.hrs_oct, 0) as hrs_oct, nvl(wem.hrs_nov, 0) as hrs_nov, nvl(wem.hrs_dec, 0) as hrs_dec,
nvl(wem.hrs_total, 0) as hrs_total, nvl(wem.qty_jan, 0) as qty_jan, nvl(wem.qty_feb, 0) as qty_feb,
nvl(wem.qty_mar, 0) as qty_mar, nvl(wem.qty_apr, 0) as qty_apr, nvl(wem.qty_may, 0) as qty_may,
nvl(wem.qty_jun, 0) as qty_jun, nvl(wem.qty_jul, 0) as qty_jul, nvl(wem.qty_aug, 0) as qty_aug,
nvl(wem.qty_sep, 0) as qty_sep, nvl(wem.qty_oct, 0) as qty_oct, nvl(wem.qty_nov, 0) as qty_nov,
nvl(wem.qty_dec, 0) as qty_dec, nvl(wem.qty_total, 0) as qty_total,
nvl(wem.future_dollars, 0) as future_dollars, nvl(wem.hist_actuals, 0) as hist_actuals,
c.company_id
from wo_est_monthly wem, budget_version_fund_proj bvfp, work_order_control woc,
expenditure_type et, estimate_charge_type ect, department d, budget_plant_class bpc,
job_task_list jtl, company c
where bvfp.work_order_id = wem.work_order_id
and bvfp.revision = wem.revision
and woc.work_order_id = wem.work_order_id
and et.expenditure_type_id = wem.expenditure_type_id
and ect.est_chg_type_id = wem.est_chg_type_id
and d.department_id(+) = wem.department_id
and bpc.budget_plant_class_id(+) = wem.utility_account_id
and jtl.job_task_id(+) = wem.job_task_id
and c.company_id = woc.company_id
and bvfp.active = 1
and woc.funding_wo_indicator = 1
and bvfp.budget_version_id in (select filter_value budget_version_id
                               from pp_any_required_filter
                               where upper(column_name) = 'BUDGET VERSION ID')
order by woc.work_order_number, wem.year, et.description, ect.description,
bpc.description, d.description,  jtl.description
 
 
 
 
 
 ;