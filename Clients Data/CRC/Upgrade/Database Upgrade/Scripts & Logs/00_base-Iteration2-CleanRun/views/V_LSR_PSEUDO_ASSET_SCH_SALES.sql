CREATE OR REPLACE VIEW V_LSR_PSEUDO_ASSET_SCH_SALES AS
SELECT lsr_asset_id,
      ilr_id,
     revision,
     set_of_books_id,
     month,
     principal_received * allocation AS principal_received,
     principal_accrued * allocation AS principal_accrued,
     beg_unguaranteed_residual * allocation AS beg_unguaranteed_residual,
     interest_unguaranteed_residual * allocation AS interest_unguaranteed_residual,
     ending_unguaranteed_residual * allocation AS ending_unguaranteed_residual,
     beg_net_investment * allocation AS beg_net_investment,
     interest_net_investment * allocation AS interest_net_investment,
     ending_net_investment * allocation AS ending_net_investment
FROM ( SELECT asset.lsr_asset_id,
        sch.ilr_id,
       sch.revision,
       sch.set_of_books_id,
       sch.month,
       sch.principal_received,
       sch.principal_accrued,
       sch.beg_unguaranteed_residual,
       sch.interest_unguaranteed_residual,
       sch.ending_unguaranteed_residual,
       sch.beg_net_investment,
       sch.interest_net_investment,
       sch.ending_net_investment,
       coalesce(
    RATIO_TO_REPORT(
      asset.fair_market_value
    ) OVER(PARTITION BY
      sch.ilr_id,
      sch.revision,
      sch.month,
      sch.set_of_books_id
    ),
    0
  ) AS allocation
FROM lsr_ilr_schedule_sales_direct sch
  JOIN lsr_asset asset ON sch.ilr_id = asset.ilr_id
  AND sch.revision = asset.revision
);