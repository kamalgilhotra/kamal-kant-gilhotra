
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."REG_CASE_COMPOSITION" ("REG_CASE_ID", "REG_ACCT_ID", "ACCOUNT_TYPE_ID", "SUB_ACCT_TYPE_ID", "GL_MONTH", "COVERAGE_YR_ENDED", "UNADJUSTED", "ADJUSTMENTS", "PER_BOOKS_ADJUSTED") AS 
  (
 select reg_case_id,
       reg_acct_id,
       account_type_id,
       sub_acct_type_id,
       gl_month,
       coverage_yr_ended,
       sum(unadj_amount) unadjusted,
       sum(adj_amount) adjustments,
       Sum(amount) per_books_adjusted
  from (SELECT c.reg_case_id,
               ca.reg_acct_id reg_acct_id,
               ca.reg_acct_type_id account_type_id,
               sa.sub_acct_type_id sub_acct_type_id,
               gl_month,
               coverage_yr_ended,
               nvl(act_amount,0) unadj_amount,
               nvl(adj_amount, 0) adj_amount,
               nvl(act_amount, 0) + nvl(adj_amount, 0) +
               nvl(recon_adj_amount, 0) amount
          FROM reg_history_ledger         hl,
               reg_historic_version       hv,
               reg_jurisdiction           j,
               reg_case                   c,
               reg_case_coverage_versions ccv,
               reg_case_acct              ca,
               reg_acct_master            ram,
               reg_sub_acct_type          sa,
               reg_acct_type              rat
         WHERE j.reg_company_id = hl.reg_company_id
           AND hl.historic_version_id = hv.historic_version_id
           AND j.reg_jurisdiction_id = c.reg_jurisdiction_id
           AND c.reg_case_id = ccv.reg_case_id
           AND hl.historic_version_id = ccv.hist_version_id
           AND ccv.fore_version_id = 0
           AND c.reg_case_id = ca.reg_case_id
           AND ca.reg_acct_id = hl.reg_acct_id
           AND ram.reg_acct_id = ca.reg_acct_id
           AND ca.sub_acct_type_id = sa.sub_acct_type_id
           AND ca.reg_acct_type_id = sa.reg_acct_type_id
           AND rat.reg_acct_type_id = sa.reg_acct_type_id
           AND gl_month BETWEEN CASE
                 WHEN ccv.start_date =
                      to_char(add_months(to_date(coverage_yr_ended, 'YYYYMM'),
                                         -11),
                              'YYYYMM') THEN
                  To_Number(To_Char(Add_Months(To_Date(ccv.start_date,
                                                       'YYYYMM'),
                                               -2),
                                    'YYYYMM'))
                 ELSE
                  ccv.start_date
               END AND ccv.end_date
        UNION
        SELECT c.reg_case_id,
               ca.reg_acct_id reg_acct_id,
               ca.reg_acct_type_id account_type_id,
               sa.sub_acct_type_id sub_acct_type_id,
               gl_month,
               coverage_yr_ended,
               nvl(fcst_amount,0) unadj_amount,
               nvl(adj_amount, 0) adj_amount,
               nvl(fcst_amount, 0) + nvl(adj_amount, 0) +
               nvl(recon_adj_amount, 0) amount
          FROM reg_forecast_ledger        fl,
               reg_forecast_version       fv,
               reg_jurisdiction           j,
               reg_case                   c,
               reg_case_coverage_versions ccv,
               reg_case_acct              ca,
               reg_acct_master            ram,
               reg_sub_acct_type          sa,
               reg_acct_type              rat
         WHERE j.reg_company_id = fl.reg_company_id
           AND fv.forecast_version_id = fl.forecast_version_id
           AND j.reg_jurisdiction_id = c.reg_jurisdiction_id
           AND c.reg_case_id = ccv.reg_case_id
           AND fl.forecast_version_id = ccv.fore_version_id
           AND c.reg_case_id = ca.reg_case_id
           AND ca.reg_acct_id = fl.reg_acct_id
           AND ram.reg_acct_id = ca.reg_acct_id
           AND ca.sub_acct_type_id = sa.sub_acct_type_id
           AND ca.reg_acct_type_id = sa.reg_acct_type_id
           AND rat.reg_acct_type_id = sa.reg_acct_type_id
           AND gl_month BETWEEN CASE
                 WHEN ccv.start_date =
                      to_char(add_months(to_date(coverage_yr_ended, 'YYYYMM'),
                                         -11),
                              'YYYYMM') THEN
                  To_Number(To_Char(Add_Months(To_Date(ccv.start_date,
                                                       'YYYYMM'),
                                               -2),
                                    'YYYYMM'))
                 ELSE
                  ccv.start_date
               END AND ccv.end_date)
 where substr(gl_month, 5, 2) <> 99
 GROUP BY reg_case_id,
          reg_acct_id,
          account_type_id,
          sub_acct_type_id,
          gl_month,
          coverage_yr_ended)
 ;