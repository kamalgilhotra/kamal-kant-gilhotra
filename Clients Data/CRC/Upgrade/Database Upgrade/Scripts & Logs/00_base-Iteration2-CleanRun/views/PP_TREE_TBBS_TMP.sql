
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."PP_TREE_TBBS_TMP" ("PP_TREE_CATEGORY_ID", "PP_TREE_TYPE_ID", "PARENT_ID", "CHILD_ID", "EMPTY_NODE", "TIME_STAMP", "USER_ID", "OVERRIDE_PARENT_DESCRIPTION", "SORT_ORDER") AS 
  SELECT  ptt.pp_tree_category_id, ptt.pp_tree_type_id, nvl(pti.parent_id + tli_max_id.id_offset, pti.node_id + tli_max_id.id_offset) AS parent_id,
	decode(pti.mandatory_leaf, 1, tli.line_item_id, pti.node_id + tli_max_id.id_offset) AS child_id, decode(pti.mandatory_leaf, 0, 1, 0) AS empty_node,
	pti.time_stamp, pti.user_id, decode(pti_parents.node_id, null, pti.DEFAULT_DESCRIPTION, pti_parents.default_description) as override_parent_description, pti.sort_order
FROM pp_tree_improved pti
JOIN pp_tree_type ptt ON pti.pp_tree_type_id = ptt.pp_tree_type_id
LEFT JOIN pp_tree_improved pti_parents ON pti.parent_id = pti_parents.node_id
LEFT JOIN tbbs_line_item_tree_relate tlitr ON pti.node_id = tlitr.node_id
LEFT JOIN tbbs_line_item tli ON tlitr.line_item_id = tli.line_item_id
CROSS JOIN (SELECT MAX(line_item_id) AS id_offset FROM tbbs_line_item) tli_max_id
 ;