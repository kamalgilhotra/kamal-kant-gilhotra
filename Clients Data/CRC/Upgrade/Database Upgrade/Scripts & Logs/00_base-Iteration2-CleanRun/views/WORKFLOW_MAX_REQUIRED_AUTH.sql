
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WORKFLOW_MAX_REQUIRED_AUTH" ("WORKFLOW_ID", "MAX_REQUIRED_AUTHORITY") AS 
  select WF.WORKFLOW_ID,
       (select NVL(LEAST(min(case
                             when (A.NUM_REQUIRED > 0 or (A.USERS is not null and A.REQUIRED = 1)) and
                                   A.AUTHORITY_LIMIT >= WF.APPROVAL_AMOUNT then
                                 A.AUTHORITY_LIMIT
                             else
                                 999999999999
                             end),
                         max(A.AUTHORITY_LIMIT)),
                   999999999999)
          from WORKFLOW_DETAIL A
         where A.WORKFLOW_ID = WF.WORKFLOW_ID) MAX_REQUIRED_AUTHORITY
  from WORKFLOW WF
 
 ;