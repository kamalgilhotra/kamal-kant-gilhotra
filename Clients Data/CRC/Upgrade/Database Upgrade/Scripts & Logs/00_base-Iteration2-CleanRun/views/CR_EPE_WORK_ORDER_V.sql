
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CR_EPE_WORK_ORDER_V" ("WORK_ORDER", "DESCRIPTION", "BUDGET_VALUE", "BUDGETING_VALUE", "ELEMENT_TYPE", "LABOR_FLAG", "VALIDATION_STATUS_ID", "EFFECTIVE_MONTH_NUMBER", "LONG_DESCRIPTION", "COMPANY", "OPERATING_SEGMENT", "BUSINESS_SEGMENT", "OWNING_DEPARTMENT", "PROJECT_MANAGER", "WO_STATUS", "WORK_ORDER_TYPE", "WORK_ORDER_TYPE_ID", "MAJOR_LOC", "MINOR_LOC", "GEO_CODE", "EST_START_DATE", "EST_COMPLETE_DATE", "EST_IN_SERVICE_DATE", "LATE_CHG_WAIT_PERIOD", "IN_SERVICE_DATE", "COMPLETION_DATE", "INITIATION_DATE", "ALLOCATOR") AS 
  with auth as
(SELECT work_order_id,
        max(authorized_date) as effmn
   FROM (SELECT work_order_id,
                 authorized_date1 as authorized_date
           FROM work_order_approval
          UNION
         SELECT work_order_id,
                authorized_date2
           FROM work_order_approval
          UNION
         SELECT work_order_id,
                authorized_date3
           FROM work_order_approval
          UNION
         SELECT work_order_id,
                authorized_date4
           FROM work_order_approval
          UNION
         SELECT work_order_id,
                authorized_date5
           FROM work_order_approval
          UNION
         SELECT work_order_id,
                authorized_date6
           FROM work_order_approval
          UNION
         SELECT work_order_id,
                authorized_date7
           FROM work_order_approval
          UNION
         SELECT work_order_id,
                authorized_date8
           FROM work_order_approval
          UNION
         SELECT work_order_id,
                authorized_date9
           FROM work_order_approval
          UNION
         SELECT work_order_id,
                authorized_date10
           FROM work_order_approval           )       group by work_order_id       )
SELECT       wo.work_order_number as work_order,
            wo.description as description,
            wo.work_order_number as budget_value,
            wo.work_order_number as budgeting_value,
            'Actuals' as element_type,
            null as labor_flag,
            decode(wo.wo_status_id,3,0,7,0,8,0,1) as validation_status_id,
            decode(wo.wo_status_id,3,to_number(to_char(woacct.afudc_stop_date,'yyyymm')),7,to_number(to_char(add_months(wo.close_date,1),'yyyymm')), 8,to_number(to_char(wo.close_date,'yyyymm')),to_number(to_char(auth.effmn,'yyyymm'))) as effective_month_number,
            wo.long_description as long_description,
            co.gl_company_no company,
            bus_seg.description operating_segment,
            bus_seg.external_bus_segment business_segment,
            d.external_department_code owning_department ,
            woi.proj_mgr project_manager,
            decode(substr(wo.work_order_number,1,5),'GP009','OPEN',upper(e.description)) wo_status,
            upper(f.description) wo_type,
            f.work_order_type_id wo_type_id,
            upper(g.description) major_loc,
            upper(h.long_description) minor_loc,
            upper(h.grid_coordinate) geocode,
            to_char(wo.est_start_date,'mm/dd/yyyy'),
            to_char(wo.est_complete_date,'mm/dd/yyyy'),
            to_char(wo.est_in_service_date,'mm/dd/yyyy'),
            wo.late_chg_wait_period,
            to_char(wo.in_service_date,'mm/dd/yyyy'),
            to_char(wo.completion_date,'mm/dd/yyyy'),
            to_char(i.initiation_date,'mm/dd/yyyy'),
            allocator.value allocator
            FROM       work_order_control wo,      work_order_account woacct,      work_order_initiator woi,
                    department d,                  auth,                              business_segment bus_seg,
                    work_order_status e,          work_order_type f,              major_location g,
                    asset_location h,              work_order_initiator i,          work_order_class_code allocator,
                    budget bud,                 company_setup co
            WHERE wo.funding_wo_indicator = 0                   and
                  wo.work_order_id = woacct.work_order_id       and
                  wo.work_order_id = auth.work_order_id         and
                  wo.work_order_id = woi.work_order_id          and
                  wo.department_id = d.department_id            and
                  wo.bus_segment_id = bus_seg.bus_segment_id    and
                  wo.wo_status_id = e.wo_status_id              and
                  wo.work_order_type_id = f.work_order_type_id  and
                  wo.major_location_id = g.major_location_id    and
                  wo.asset_location_id = h.asset_location_id    and
                  wo.work_order_id = i.work_order_id            and
                  allocator.class_code_id (+) = 2               and
                  wo.work_order_id = allocator.work_order_id(+) and
                  wo.budget_id = bud.budget_id                  and
                  wo.company_id = co.company_id
            UNION all
            SELECT       wo.work_order_number as work_order,
                        wo.description as description,
                        wo.work_order_number as budget_value,
                        wo.work_order_number as budgeting_value,
                        'Budget' as element_type,
                        null as labor_flag,
                        decode(wo.wo_status_id,3,0,7,0,8,0,1) as validation_status_id,
                        decode(wo.wo_status_id,3,to_number(to_char(woacct.afudc_stop_date,'yyyymm')),7,to_number(to_char(add_months(wo.close_date,1),'yyyymm')), 8,to_number(to_char(wo.close_date,'yyyymm')),to_number(to_char(auth.effmn,'yyyymm'))) as effective_month_number,
                        wo.long_description as long_description,
                        co.gl_company_no company,
                        bus_seg.description operating_segment,
                        bus_seg.external_bus_segment business_segment,
                        d.external_department_code owning_department ,
                        woi.proj_mgr project_manager,
                        upper(e.description) wo_status,
                        upper(f.description) wo_type,
                        f.work_order_type_id wo_type_id,
                        upper(g.description) major_loc,
                        upper(h.long_description) minor_loc,
                        upper(h.grid_coordinate) geocode,
                        to_char(wo.est_start_date,'mm/dd/yyyy'),
                        to_char(wo.est_complete_date,'mm/dd/yyyy'),
                        to_char(wo.est_in_service_date,'mm/dd/yyyy'),
                        wo.late_chg_wait_period,
                        to_char(wo.in_service_date,'mm/dd/yyyy'),
                        to_char(wo.completion_date,'mm/dd/yyyy'),
                        to_char(i.initiation_date,'mm/dd/yyyy'),
                        allocator.value allocator
                   FROM     work_order_control wo,      work_order_account woacct,      work_order_initiator woi,
                            department d,               auth,                           business_segment bus_seg,
                            work_order_status e,        work_order_type f,              major_location g,
                            asset_location h,           work_order_initiator i,         work_order_class_code allocator,
                            budget bud,                 company_setup co
        WHERE wo.funding_wo_indicator = 0              and
            wo.work_order_id = woacct.work_order_id    and
            woacct.work_order_grp_id = 7               and
            wo.work_order_id = auth.work_order_id      and
            wo.work_order_id = woi.work_order_id       and
            wo.department_id = d.department_id          and
            wo.bus_segment_id = bus_seg.bus_segment_id and
            wo.wo_status_id = e.wo_status_id           and
            wo.work_order_type_id = f.work_order_type_id    and
            wo.major_location_id = g.major_location_id      and
            wo.asset_location_id = h.asset_location_id      and
            wo.work_order_id = i.work_order_id              and
            allocator.class_code_id (+) = 2                 and
            wo.work_order_id = allocator.work_order_id (+)  and
            wo.budget_id = bud.budget_id              and
            wo.company_id = co.company_id
 
 
 
 
 
 ;