
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CURRENCY_DESCRIPTION_VIEW" ("COMPANY_ID", "FUNC_SYMBOL", "LOCAL_SYMBOL", "FUNC_CURR", "LOCAL_CURR") AS 
  (select company_id, curr_a.description func_symbol, 	  		nvl(curr_b.description,'N/A') local_symbol, func_curr, local_curr 			  from currency curr_a, currency curr_b, 		  (select company_id, sum(decode(currency_type_id,1, currency_schema.currency_id, 0)) func_curr,  sum(decode(currency_type_id,2, currency_schema.currency_id, 0)) local_curr 			  from currency_schema group by company_id) curr_schema 			  where 			curr_a.currency_id = curr_schema.func_curr and   curr_b.currency_id (+) = curr_schema.local_curr)
 
 
 
 
 
 ;