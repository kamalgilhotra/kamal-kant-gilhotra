
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_EST_INITIAL_REVISION" ("WORK_ORDER_ID", "REVISION") AS 
  select woc.work_order_id,
   case when woc.funding_wo_indicator = 1 then
      nvl(
         (  select max(bvfp.revision)
            from budget_version_fund_proj bvfp, budget_version_view bvv
            where woc.company_id = bvv.company_id
            and woc.work_order_id = bvfp.work_order_id
            and bvfp.active = 1
            and bvfp.budget_version_id = bvv.working_version
         ),
         nvl(
            (  select max(bvfp.revision)
               from budget_version_fund_proj bvfp, budget_version_view bvv
               where woc.company_id = bvv.company_id
               and woc.work_order_id = bvfp.work_order_id
               and bvfp.active = 1
               and bvfp.budget_version_id = bvv.current_version
            ),
            nvl(
               (  select max(woa.revision)
                  from work_order_approval woa
                  where woa.work_order_id = woc.work_order_id
               ),
               0)))
   else
      (  select max(woa.revision)
         from work_order_approval woa
         where woa.work_order_id = woc.work_order_id
      )
   end
   revision
from work_order_control woc
 
 ;