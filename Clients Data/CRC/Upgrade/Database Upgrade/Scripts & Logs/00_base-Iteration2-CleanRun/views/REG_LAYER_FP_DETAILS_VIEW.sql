
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."REG_LAYER_FP_DETAILS_VIEW" ("LAYER", "LONG_DESCRIPTION", "LAYER_GROUP", "FUNDING_PROJECT_NUMBER", "MONTH_YEAR", "LABEL", "ITEM", "ORIGINAL_REVISION", "ORIGINAL_AMOUNT", "UPDATED_REVISION", "UPDATED_AMOUNT", "DIFFERENCE", "WO_DESCRIPTION", "LABEL_SORT", "ITEM_SORT") AS 
  (
select layer, long_description, layer_group, funding_project_number, month_year, label, item,
		 original_revision, sum(original_amount) original_amount, updated_revision, sum(updated_amount) updated_amount,
		 sum(nvl(original_amount,0) - nvl(updated_amount,0)) difference,
		 wo_description, label_sort, item_sort
  from (
	select x.description layer,
			 x.long_description long_description,
			 nvl((select g.description from reg_incremental_group g where g.incremental_group_id = x.incremental_group_id),' ') layer_group,
			 w.work_order_number funding_project_number,
			 x.month_year month_year,
			 l.description label,
			 i.description item,
			 x.orig_revision original_revision,
			 x.orig_amount original_amount,
			 x.updated_revision updated_revision,
			 x.updated_amount updated_amount,
			 w.description wo_description,
			 l.sort_order label_sort,
			 i.sort_order item_sort
	  from incremental_fp_adj_label l, incremental_fp_adj_item i, work_order_control w,
			 (
				select a.description, a.long_description,
						 a.orig_work_order_id, a.orig_revision, a.updated_work_order_id, a.updated_revision,
						 d.label_id, d.item_id,
						 d.month_year month_year, d.amount orig_amount, 0 updated_amount, r.incremental_group_id incremental_group_id
				  from reg_incremental_adjustment a, incremental_fp_adj_data d, reg_incremental_adj_group r
				 where a.incremental_adj_type_id in (1, 4)
					and a.orig_work_order_id = d.work_order_id
					and a.orig_revision = d.revision
					and a.orig_budget_version_id = d.budget_version_id
					and a.orig_fcst_depr_version_id = d.fcst_depr_version_id
					and a.orig_version_id = d.version_id
					and a.incremental_adj_id = r.incremental_adj_id (+)
				union
				select a.description, a.long_description,
						 a.orig_work_order_id, a.orig_revision, a.updated_work_order_id, a.updated_revision,
						 d.label_id, d.item_id,
						 d.month_year month_year, 0 orig_amount, d.amount updated_amount, r.incremental_group_id
				  from reg_incremental_adjustment a, incremental_fp_adj_data d, reg_incremental_adj_group r
				 where a.incremental_adj_type_id in (1, 4)
					and a.updated_work_order_id = d.work_order_id
					and a.updated_revision = d.revision
					and a.updated_budget_version_id = d.budget_version_id
					and a.updated_fcst_depr_version_id = d.fcst_depr_version_id
					and a.updated_version_id = d.version_id
					and a.incremental_adj_id = r.incremental_adj_id (+)
			 ) x
	 where nvl(x.orig_work_order_id,x.updated_work_order_id) = nvl(x.updated_work_order_id,x.orig_work_order_id)
		and x.label_id = l.label_id
		and x.label_id = i.label_id
		and x.item_id = i.item_id
		and nvl(x.orig_work_order_id,x.updated_work_order_id) = w.work_order_id)
 group by layer, long_description, layer_group, funding_project_number, month_year, label, item,
		 	 original_revision, updated_revision, wo_description, label_sort, item_sort
)
 ;