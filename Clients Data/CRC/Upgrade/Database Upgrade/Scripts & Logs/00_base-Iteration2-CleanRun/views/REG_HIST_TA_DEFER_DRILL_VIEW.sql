
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."REG_HIST_TA_DEFER_DRILL_VIEW" ("TA_VERSION", "COMPANY", "M_ITEM", "TA_NORM_SCHEMA", "JE_TYPE", "OPER_IND", "TAXING_ENTITY", "GL_ACCOUNT", "REG_ACCOUNT", "YEAR", "GL_MONTH", "BEG_BALANCE", "PROVISION", "REVERSAL", "TRANSFER_IN", "TRANSFER_OUT", "ADJUSTMENT", "END_BALANCE", "OTHER", "PAYMENTS") AS 
  (
select v.description ta_version,
		 cs.description company,
		 m.description m_item,
		 n.description ta_norm_schema,
		 j.description je_type,
		 o.description oper_ind,
		 e.description taxing_entity,
		 l.gl_account gl_account,
		 ram.description reg_account,
		 v.year year,
		 l.gl_month gl_month,
		 l.beg_balance,
		 l.provision,
		 l.reversal,
		 l.transfer_in,
		 l.transfer_out,
		 l.adjustment,
		 l.end_balance,
		 l.other,
		 l.payments
  from company_setup cs,
		 tax_accrual_subledger l,
		 tax_accrual_control c, tax_accrual_version v, tax_accrual_entity e, tax_accrual_m_master m, tax_accrual_norm_schema n,
		 tax_accrual_je_type j, tax_accrual_oper_ind o,
		 reg_ta_component k, reg_acct_master ram,
		 reg_ta_comp_member_map w, reg_ta_family_member_map x, reg_ta_version_control y,
		 tax_accrual_gl_month_def g,
		 cr_dd_required_filter hv, cr_dd_required_filter fc, cr_dd_required_filter mn
 where c.company_id = cs.company_id
	and c.company_id = x.company_id
	and c.oper_ind = x.oper_ind
	and c.m_item_id = x.m_item_id
	and x.historic_version_id = y.historic_version_id
	and x.forecast_version_id = y.forecast_version_id
	and c.ta_version_id = y.ta_version_id
	and c.ta_version_id = v.ta_version_id
	and x.historic_version_id = w.historic_version_id
	and x.forecast_version_id = w.forecast_version_id
	and w.reg_component_id = nvl(ram.reg_component_id,0)
	and w.reg_family_id = nvl(ram.reg_family_id,0)
	and w.reg_member_id = nvl(ram.reg_member_id,0)
	and w.reg_member_id = x.reg_member_id
	and w.reg_component_id = k.reg_component_id
	and decode(k.federal, 1, 10, 20) = e.entity_juris_id
	and (l.je_type_id = decode(k.reg_component_id,1,2,2,2,3,2,4,2,7,3,8,3,9,3,10,3,-1)
		  or
		  l.je_type_id = decode(k.reg_component_id,1,4,2,4,3,4,4,4,7,4,8,4,9,4,10,4,-1))
	and e.entity_id = x.entity_id
	and ram.reg_source_id = 6
	and l.company_id = x.company_id
	and l.oper_ind = x.oper_ind
	and l.m_item_id = x.m_item_id
	and l.entity_id = x.entity_id
	and l.ta_version_id = y.ta_version_id
	and l.gl_account = x.gl_account
	and l.je_type_id = j.je_type_id
	and l.m_item_id = m.m_item_id
	and l.ta_norm_id = n.ta_norm_id
	and l.oper_ind = o.oper_ind
	and l.ta_version_id = g.ta_version_id
	and l.gl_month = g.gl_month
	and to_char(v.year) = substr(mn.filter_string, 1, 4)
	and lpad(to_char(g.period), 2, '0') = substr(mn.filter_string, 5, 2)
	and upper(mn.column_name) = 'GL_MONTH'
	and cs.reg_company_id = to_number(fc.filter_string)
	and upper(fc.column_name) = 'REG_COMPANY_ID'
	and x.historic_version_id = to_number(hv.filter_string)
	and upper(hv.column_name) = 'HISTORIC_VERSION_ID'
	and x.forecast_version_id = 0
	and ram.reg_acct_id in (
		select to_number(filter_string) from cr_dd_required_filter
		 where upper(column_name) = 'REG_ACCT_ID'))
 ;