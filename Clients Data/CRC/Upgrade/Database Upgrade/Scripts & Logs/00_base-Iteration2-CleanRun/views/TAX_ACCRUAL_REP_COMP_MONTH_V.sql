
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_REP_COMP_MONTH_V" ("CASE_1_MONTH_LABEL_1", "CASE_1_MONTH_LABEL_2", "CASE_2_MONTH_LABEL_1", "CASE_2_MONTH_LABEL_2", "CASE_1_YTD_START_MO", "CASE_1_YTD_END_MO", "CASE_2_YTD_START_MO", "CASE_2_YTD_END_MO") AS 
  select	max(case_1_month_label_1) case_1_month_label_1,
			max(case_1_month_label_2) case_1_month_label_2,
			max(case_2_month_label_1) case_2_month_label_1,
			max(case_2_month_label_2) case_2_month_label_2,
			max(case_1_ytd_start_mo) case_1_ytd_start_mo,
			max(case_1_ytd_end_mo) case_1_ytd_end_mo,
			max(case_2_ytd_start_mo) case_2_ytd_start_mo,
			max(case_2_ytd_end_mo) case_2_ytd_end_mo
from	(	select	description case_1_month_label_1,
						' ' case_1_month_label_2,
						' ' case_2_month_label_1,
						' ' case_2_month_label_2,
						0 case_1_ytd_start_mo,
						0 case_1_ytd_end_mo,
						0 case_2_ytd_start_mo,
						0 case_2_ytd_end_mo
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'CASE_1_MONTH_LABEL_1'
			union all
			select	' ' case_1_month_label_1,
						description case_1_month_label_2,
						' ' case_2_month_label_1,
						' ' case_2_month_label_2,
						0 case_1_ytd_start_mo,
						0 case_1_ytd_end_mo,
						0 case_2_ytd_start_mo,
						0 case_2_ytd_end_mo
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'CASE_1_MONTH_LABEL_2'
			union all
			select	' ' case_1_month_label_1,
						' ' case_1_month_label_2,
						description case_2_month_label_1,
						' ' case_2_month_label_2,
						0 case_1_ytd_start_mo,
						0 case_1_ytd_end_mo,
						0 case_2_ytd_start_mo,
						0 case_2_ytd_end_mo
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'CASE_2_MONTH_LABEL_1'
			union all
			select	' ' case_1_month_label_1,
						' ' case_1_month_label_2,
						' ' case_2_month_label_1,
						description case_2_month_label_2,
						0 case_1_ytd_start_mo,
						0 case_1_ytd_end_mo,
						0 case_2_ytd_start_mo,
						0 case_2_ytd_end_mo
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'CASE_2_MONTH_LABEL_2'
			union all
			select	' ' case_1_month_label_1,
						' ' case_1_month_label_2,
						' ' case_2_month_label_1,
						' ' case_2_month_label_2,
						id case_1_ytd_start_mo,
						0 case_1_ytd_end_mo,
						0 case_2_ytd_start_mo,
						0 case_2_ytd_end_mo
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'CASE_1_YTD_START_MO'
			union all
			select	' ' case_1_month_label_1,
						' ' case_1_month_label_2,
						' ' case_2_month_label_1,
						' ' case_2_month_label_2,
						0 case_1_ytd_start_mo,
						id case_1_ytd_end_mo,
						0 case_2_ytd_start_mo,
						0 case_2_ytd_end_mo
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'CASE_1_YTD_END_MO'
			union all
			select	' ' case_1_month_label_1,
						' ' case_1_month_label_2,
						' ' case_2_month_label_1,
						' ' case_2_month_label_2,
						0 case_1_ytd_start_mo,
						0 case_1_ytd_end_mo,
						id case_2_ytd_start_mo,
						0 case_2_ytd_end_mo
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'CASE_2_YTD_START_MO'
			union all
			select	' ' case_1_month_label_1,
						' ' case_1_month_label_2,
						' ' case_2_month_label_1,
						' ' case_2_month_label_2,
						0 case_1_ytd_start_mo,
						0 case_1_ytd_end_mo,
						0 case_2_ytd_start_mo,
						id case_2_ytd_end_mo
			from tax_accrual_rep_criteria_tmp
			where criteria_type = 'CASE_2_YTD_END_MO'
		)
 
 
 
 
 ;