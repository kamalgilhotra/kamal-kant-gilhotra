
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_M_ROLLUP_VIEW" ("TAX_ACCRUAL_ROLLUP_ID", "ROLLUP_DESCRIPTION", "TAX_ACCRUAL_ROLLUP_DETAIL_ID", "ROLLUP_DETAIL_DESCRIPTION", "HIDE_DETAIL", "IS_DISCRETE", "M_ITEM_ID") AS 
  select ROLLUP.TAX_ACCRUAL_ROLLUP_ID               TAX_ACCRUAL_ROLLUP_ID,
       ROLLUP.DESCRIPTION                         ROLLUP_DESCRIPTION,
       ROLLUP_DETAIL.TAX_ACCRUAL_ROLLUP_DETAIL_ID TAX_ACCRUAL_ROLLUP_DETAIL_ID,
       ROLLUP_DETAIL.DESCRIPTION                  ROLLUP_DETAIL_DESCRIPTION,
       ROLLUP_DETAIL.HIDE_DETAIL                  HIDE_DETAIL,
       ROLLUP_DETAIL.IS_DISCRETE                  IS_DISCRETE,
       M_ROLLUPS.M_ITEM_ID                        M_ITEM_ID
  from TAX_ACCRUAL_ROLLUP         rollup,
       TAX_ACCRUAL_ROLLUP_DETAIL  ROLLUP_DETAIL,
       TAX_ACCRUAL_M_ITEM_ROLLUPS M_ROLLUPS
 where ROLLUP.TAX_ACCRUAL_ROLLUP_ID = ROLLUP_DETAIL.TAX_ACCRUAL_ROLLUP_ID
   and ROLLUP_DETAIL.TAX_ACCRUAL_ROLLUP_DETAIL_ID = M_ROLLUPS.TAX_ACCRUAL_ROLLUP_DETAIL_ID
union all((select ROLLUP.TAX_ACCRUAL_ROLLUP_ID TAX_ACCRUAL_ROLLUP_ID,
                  ROLLUP.DESCRIPTION ROLLUP_DESCRIPTION,
                  0 TAX_ACCRUAL_ROLLUP_DETAIL_ID,
                  'No Rollup' ROLLUP_DETAIL_DESCRIPTION,
                  0 HIDE_DETAIL,
                  0 IS_DISCRETE,
                  M_MASTER.M_ITEM_ID M_ITEM_ID
             from TAX_ACCRUAL_ROLLUP rollup, TAX_ACCRUAL_M_MASTER M_MASTER
           union all
           select ROLLUP.TAX_ACCRUAL_ROLLUP_ID TAX_ACCRUAL_ROLLUP_ID,
                  ROLLUP.DESCRIPTION ROLLUP_DESCRIPTION,
                  0 TAX_ACCRUAL_ROLLUP_DETAIL_ID,
                  'No Rollup' ROLLUP_DETAIL_DESCRIPTION,
                  0 HIDE_DETAIL,
                  0 IS_DISCRETE,
                  -1 M_ITEM_ID
             from TAX_ACCRUAL_ROLLUP rollup)
minus
select ROLLUP.TAX_ACCRUAL_ROLLUP_ID TAX_ACCRUAL_ROLLUP_ID,
       ROLLUP.DESCRIPTION ROLLUP_DESCRIPTION,
       0 TAX_ACCRUAL_ROLLUP_DETAIL_ID,
       'No Rollup' ROLLUP_DETAIL_DESCRIPTION,
       0 HIDE_DETAIL,
       0 IS_DISCRETE,
       M_ROLLUPS.M_ITEM_ID M_ITEM_ID
  from TAX_ACCRUAL_ROLLUP         rollup,
       TAX_ACCRUAL_ROLLUP_DETAIL  ROLLUP_DETAIL,
       TAX_ACCRUAL_M_ITEM_ROLLUPS M_ROLLUPS
 where ROLLUP.TAX_ACCRUAL_ROLLUP_ID = ROLLUP_DETAIL.TAX_ACCRUAL_ROLLUP_ID
   and ROLLUP_DETAIL.TAX_ACCRUAL_ROLLUP_DETAIL_ID = M_ROLLUPS.TAX_ACCRUAL_ROLLUP_DETAIL_ID)
union all
select -1000 TAX_ACCRUAL_ROLLUP_ID,
       'M Type' ROLLUP_DESCRIPTION,
       M_MASTER.M_TYPE_ID TAX_ACCRUAL_ROLLUP_DETAIL_ID,
       M_TYPE.DESCRIPTION ROLLUP_DETAIL_DESCRIPTION,
       HIDE_DETAIL_ON_REPORT HIDE_DETAIL,
       IS_DISCRETE IS_DISCRETE,
       M_MASTER.M_ITEM_ID M_ITEM_ID
  from TAX_ACCRUAL_M_MASTER M_MASTER, TAX_ACCRUAL_M_TYPE M_TYPE
 where M_MASTER.M_TYPE_ID = M_TYPE.M_TYPE_ID
union all
select -1000 TAX_ACCRUAL_ROLLUP_ID,
       'M Type' ROLLUP_DESCRIPTION,
       -10 TAX_ACCRUAL_ROLLUP_DETAIL_ID,
       'No Rollup' ROLLUP_DETAIL_DESCRIPTION,
       HIDE_DETAIL_ON_REPORT HIDE_DETAIL,
       IS_DISCRETE IS_DISCRETE,
       -1 M_ITEM_ID
  from TAX_ACCRUAL_M_TYPE
 where M_TYPE_ID = 1
 
 ;