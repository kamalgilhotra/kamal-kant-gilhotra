
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."REG_CASE_NET_OPER_INCOME_WF_V" ("CASE_NAME", "LABEL", "AMOUNT", "SORT_ORDER", "REG_CASE_ID", "YEAR") AS 
  select X.CASE_NAME, X.LABEL, nvl(X.AMOUNT,0) AMOUNT, X.SORT_ORDER, X.REG_CASE_ID, X.YEAR
  from (select RC.CASE_NAME CASE_NAME,
               'System Per Books' LABEL,
               -1 * sum(DECODE(A.REG_ACCT_TYPE_ID, 5, L.TOTAL_CO_INCLUDED_AMOUNT, 0)) -
               sum(DECODE(A.REG_ACCT_TYPE_ID, 3, L.TOTAL_CO_INCLUDED_AMOUNT)) AMOUNT,
               1 SORT_ORDER,
               RC.REG_CASE_ID,
               L.CASE_YEAR YEAR
          from REG_CASE RC, REG_CASE_SUMMARY_LEDGER L, REG_CASE_ACCT A
         where RC.REG_CASE_ID = L.REG_CASE_ID
           and L.REG_CASE_ID = A.REG_CASE_ID
           and L.REG_ACCT_ID = A.REG_ACCT_ID
           and A.REG_ACCT_TYPE_ID in (3, 5, 11)
         group by RC.CASE_NAME, RC.REG_CASE_ID, L.CASE_YEAR
        union
        select RC.CASE_NAME,
               A.DESCRIPTION,
               -1 * sum(DECODE(T.REG_ACCT_TYPE_ID, 5, D.ADJUST_AMOUNT, 0)) -
               sum(DECODE(T.REG_ACCT_TYPE_ID, 3, D.ADJUST_AMOUNT)) -
               sum(DECODE(T.REG_ACCT_TYPE_ID, 11, DECODE(T.SUB_ACCT_TYPE_ID, 2, D.ADJUST_AMOUNT, 0))),
               2,
               RC.REG_CASE_ID,
               D.TEST_YR_ENDED YEAR
          from REG_CASE RC, REG_CASE_ADJUSTMENT A, REG_CASE_ADJUST_DETAIL D, REG_CASE_ACCT T
         where RC.REG_CASE_ID = A.REG_CASE_ID
           and A.REG_CASE_ID = D.REG_CASE_ID
           and A.ADJUSTMENT_ID = D.ADJUSTMENT_ID
           and D.REG_CASE_ID = T.REG_CASE_ID
           and D.REG_ACCT_ID = T.REG_ACCT_ID
           and T.REG_ACCT_TYPE_ID in (3, 5, 11)
         group by RC.CASE_NAME, A.DESCRIPTION, RC.REG_CASE_ID, D.TEST_YR_ENDED) X
union
select X.CASE_NAME, 'System Adjusted', sum(X.AMOUNT), 3, X.REG_CASE_ID, X.YEAR
  from (select RC.CASE_NAME CASE_NAME,
               'System Per Books' LABEL,
               -1 * sum(DECODE(A.REG_ACCT_TYPE_ID, 5, L.TOTAL_CO_INCLUDED_AMOUNT, 0)) -
               sum(DECODE(A.REG_ACCT_TYPE_ID, 3, L.TOTAL_CO_INCLUDED_AMOUNT)) AMOUNT,
               1 SORT_ORDER,
               RC.REG_CASE_ID,
               L.CASE_YEAR YEAR
          from REG_CASE RC, REG_CASE_SUMMARY_LEDGER L, REG_CASE_ACCT A
         where RC.REG_CASE_ID = L.REG_CASE_ID
           and L.REG_CASE_ID = A.REG_CASE_ID
           and L.REG_ACCT_ID = A.REG_ACCT_ID
           and A.REG_ACCT_TYPE_ID in (3, 5, 11)
         group by RC.CASE_NAME, RC.REG_CASE_ID, L.CASE_YEAR
        union
        select RC.CASE_NAME,
               A.DESCRIPTION,
               -1 * sum(DECODE(T.REG_ACCT_TYPE_ID, 5, D.ADJUST_AMOUNT, 0)) -
               sum(DECODE(T.REG_ACCT_TYPE_ID, 3, D.ADJUST_AMOUNT)) -
               sum(DECODE(T.REG_ACCT_TYPE_ID, 11, DECODE(T.SUB_ACCT_TYPE_ID, 2, D.ADJUST_AMOUNT, 0))),
               2,
               RC.REG_CASE_ID,
               D.TEST_YR_ENDED YEAR
          from REG_CASE RC, REG_CASE_ADJUSTMENT A, REG_CASE_ADJUST_DETAIL D, REG_CASE_ACCT T
         where RC.REG_CASE_ID = A.REG_CASE_ID
           and A.REG_CASE_ID = D.REG_CASE_ID
           and A.ADJUSTMENT_ID = D.ADJUSTMENT_ID
           and D.REG_CASE_ID = T.REG_CASE_ID
           and D.REG_ACCT_ID = T.REG_ACCT_ID
           and T.REG_ACCT_TYPE_ID in (3, 5, 11)
         group by RC.CASE_NAME, A.DESCRIPTION, RC.REG_CASE_ID, D.TEST_YR_ENDED) X
 group by X.CASE_NAME, X.REG_CASE_ID, X.YEAR
 order by CASE_NAME, SORT_ORDER, YEAR
 
 ;