
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."PT_CALENDAR_BILLS_VW" ("PROP_TAX_COMPANY_ID", "COMPANY_DESCR", "STATE_ID", "YEAR", "YEAR_DESCR", "DUE_DATE", "COMPLETION_DATE", "BILL_DESCRIPTION", "RESPONSIBLE_USER", "PRIORITY", "STATUS", "CALENDAR_TYPE") AS 
  select prop_tax_company_id, company_descr, state_id,
year, year_descr, due_date, completion_date,
bill_description, responsible_user,
case when is_paid = 1 then 'Complete'
     else
      case when days_until_due <= crit then 'Critical'
           when crit < days_until_due and days_until_due <= warn then 'Warning'
           else 'Normal'
      end
end priority,
status,
'Bill' calendar_type
from (
select distinct pt_statement_group.prop_tax_company_id, pt_statement_group.state_id, pt_statement_year.year
, nvl( pt_statement_installment.due_date, pt_schedule_installment.due_date ) due_date
, decode( payment_view.payment_id, null, 0, 1 ) is_paid
, payment_view.scheduled_pay_date completion_date
, pt_statement_group.statement_group_id
, pt_company.description company_descr
, pt_statement_year.description year_descr
, pt_statement_group.description bill_description
, nvl(pt_parcel.responsible_user,'Not Assigned') responsible_user
, pt_statement_statement_year.statement_status_id
, pt_statement_status.description status
, nvl(trim(f_pp_system_control(-1, 'PT Calendar Warning Days Threshold')),21) warn
, nvl(trim(f_pp_system_control(-1, 'PT Calendar Critical Days Threshold')),7) crit
, trunc(nvl(nvl( pt_statement_installment.due_date, pt_schedule_installment.due_date ) ,sysdate) - sysdate,0) days_until_due
from pt_statement_group, pt_statement_statement_year, pt_statement_year, pt_statement_status
, pt_statement_installment, pt_schedule_installment, pt_company
, pt_parcel, pt_statement_line
,( select  distinct  pt_statement_group_payment.schedule_id schedule_id,
              pt_statement_group_payment.statement_year_id statement_year_id,
              pt_statement_group_payment.installment_id installment_id,
              pt_statement_group_payment.scheduled_pay_date scheduled_pay_date,
              ptv_payment_term.description payment_term,
              pt_statement_group_payment.payment_id payment_id
        from    pt_statement_line_payment,
              pt_statement_group_payment,
              pt_payment_status,
              ptv_payment_term
        where pt_statement_line_payment.payment_id = pt_statement_group_payment.payment_id
          and pt_statement_group_payment.payment_status_id = pt_payment_status.payment_status_id
          and pt_payment_status.include_amount = 1
          and pt_statement_group_payment.schedule_id = ptv_payment_term.schedule_id
          and pt_statement_group_payment.statement_year_id = ptv_payment_term.statement_year_id
          and pt_statement_group_payment.installment_id = ptv_payment_term.installment_id
          and pt_statement_group_payment.period_id = ptv_payment_term.period_id
          and pt_statement_group_payment.separate_checks_payment_id is null
      ) payment_view
where pt_statement_group.statement_group_id = pt_statement_statement_year.statement_group_id
and pt_statement_statement_year.assessment_year_id = pt_statement_year.statement_year_id
and pt_statement_statement_year.statement_status_id = pt_statement_status.statement_status_id
and pt_statement_group.prop_tax_company_id = pt_company.prop_tax_company_id
and pt_statement_statement_year.statement_id = pt_statement_installment.statement_id
and pt_statement_statement_year.statement_year_id = pt_statement_installment.statement_year_id
and pt_statement_installment.schedule_id = pt_schedule_installment.schedule_id
and pt_statement_installment.statement_year_id = pt_schedule_installment.statement_year_id
and pt_statement_installment.installment_id = pt_schedule_installment.installment_id
and pt_statement_statement_year.statement_id = pt_statement_line.statement_id
and pt_statement_statement_year.statement_year_id = pt_statement_line.statement_year_id
and pt_statement_line.parcel_id = pt_parcel.parcel_id
and pt_statement_installment.schedule_id = payment_view.schedule_id (+)
and pt_statement_installment.statement_year_id = payment_view.statement_year_id (+)
and pt_statement_installment.installment_id = payment_view.installment_id (+)
)
;