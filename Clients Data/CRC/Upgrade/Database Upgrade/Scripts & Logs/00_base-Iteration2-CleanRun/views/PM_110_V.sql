
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."PM_110_V" ("YEAR", "BUDGET_VERSION", "OWNING_CC", "PROJECT", "PROJECT_DESC", "ALLOCATION_TYPE", "ATYPE", "SUBTOTAL3", "SUBTOTAL4", "BUDGET_ROLLUP_DESCR", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "YR_TOTAL", "AJAN", "AFEB", "AMAR", "AAPR", "AMAY", "AJUN", "AJUL", "AAUG", "ASEP", "AOCT", "ANOV", "ADEC", "AYR_TOTAL") AS 
  select year, budget_version,
owning_cc, project, project_desc,
allocation_type,
decode(allocation_type, '1', 'PAYROLL', '2', 'PAYROLL', '3', 'PAYROLL', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
         decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '6','1','0') as "SUBTOTAL3",
		 decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '0') as "SUBTOTAL4",
budget_rollup_descr,
sum(jan) jan, sum(feb) feb, sum(mar) mar, sum(apr) apr, sum(may) may, sum(jun) jun,
sum(jul) jul, sum(aug) aug, sum(sep) sep, sum(oct) oct, sum(nov) nov, sum(dec) dec, sum(yr_total) yr_total,
sum(ajan) ajan, sum(afeb) afeb, sum(amar) amar, sum(aapr) aapr, sum(amay) amay, sum(ajun) ajun,
sum(ajul) ajul, sum(aaug) aaug, sum(asep) asep, sum(aoct) aoct, sum(anov) anov, sum(adec) adec, sum(ayr_total) ayr_total
from (
select est.year, bv.description budget_version,
woc.department_id owning_cc, woc.work_order_number project, woc.description project_desc,
etmv.allocation_type,etmv.budget_rollup_descr,
round(sum(nvl(january,0))) jan, round(sum(nvl(february,0))) feb, round(sum(nvl(march,0))) mar,
round(sum(nvl(april,0))) apr,   round(sum(nvl(may,0))) may,      round(sum(nvl(june,0))) jun,
round(sum(nvl(july,0))) jul,    round(sum(nvl(august,0))) aug,    round(sum(nvl(september,0))) sep,
round(sum(nvl(october,0))) oct,  round(sum(nvl(november,0))) nov, round(sum(nvl(december,0))) dec,
round(sum(nvl(january,0)))+
round(sum(nvl(february,0)))+
round(sum(nvl(march,0)))+
round(sum(nvl(april,0)))+
round(sum(nvl(may,0)))+
round(sum(nvl(june,0)))+
round(sum(nvl(july,0)))+
round(sum(nvl(august,0)))+
round(sum(nvl(september,0)))+
round(sum(nvl(october,0)))+
round(sum(nvl(november,0)))+
round(sum(nvl(december,0))) yr_total,
0 ajan, 0 afeb, 0 amar, 0 aapr, 0 amay, 0 ajun,
0 ajul, 0 aaug, 0 asep, 0 aoct, 0 anov, 0 adec, 0 ayr_total
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
     wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
group by est.year, bv.description,
woc.department_id, woc.work_order_number, woc.description, etmv.allocation_type, etmv.budget_rollup_descr
having round(sum(nvl(january,0)))  +round(sum(nvl(february,0)))+round(sum(nvl(march,0)))   +round(sum(nvl(april,0)))+
       round(sum(nvl(may,0)))      +round(sum(nvl(june,0)))    +round(sum(nvl(july,0)))    +round(sum(nvl(august,0)))+
       round(sum(nvl(september,0)))+round(sum(nvl(october,0))) +round(sum(nvl(november,0)))+round(sum(nvl(december,0)))!=0
union
select est.year, bv.description budget_version,
woc.department_id owning_cc, woc.work_order_number project, woc.description project_desc,
etmv.allocation_type,
etmv.budget_rollup_descr xexp,
0 jan, 0 feb, 0 mar, 0 apr, 0 may, 0 jun,
0 jul, 0 aug, 0 sep, 0 oct, 0 nov, 0 dec, 0 yr_total,
round(sum(nvl(january,0))) ajan, round(sum(nvl(february,0))) afeb, round(sum(nvl(march,0))) amar, round(sum(nvl(april,0))) aapr,
round(sum(nvl(may,0))) amay, round(sum(nvl(june,0))) ajun, round(sum(nvl(july,0))) ajul, round(sum(nvl(august,0))) aaug,
round(sum(nvl(september,0))) asep, round(sum(nvl(october,0))) aoct,round(sum(nvl(november,0))) anov, round(sum(nvl(december,0))) adec,
round(sum(nvl(january,0)))+
round(sum(nvl(february,0)))+
round(sum(nvl(march,0)))+
round(sum(nvl(april,0)))+
round(sum(nvl(may,0)))+
round(sum(nvl(june,0)))+
round(sum(nvl(july,0)))+
round(sum(nvl(august,0)))+
round(sum(nvl(september,0)))+
round(sum(nvl(october,0)))+
round(sum(nvl(november,0)))+
round(sum(nvl(december,0))) ayr_total
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
     wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
  and etmv.allocation_type = '7'
group by est.year, bv.description,
woc.department_id, woc.work_order_number, woc.description, etmv.allocation_type, etmv.budget_rollup_descr
having round(sum(nvl(january,0)))  +round(sum(nvl(february,0)))+round(sum(nvl(march,0)))   +round(sum(nvl(april,0)))+
       round(sum(nvl(may,0)))      +round(sum(nvl(june,0)))    +round(sum(nvl(july,0)))    +round(sum(nvl(august,0)))+
       round(sum(nvl(september,0)))+round(sum(nvl(october,0))) +round(sum(nvl(november,0)))+round(sum(nvl(december,0)))!=0)
where project in (select woc.work_order_number
from cr_alloc_system_control sc, cr_alloc_system_control yr, budget_version bv, budget_version_fund_proj fp,
     wo_est_monthly est, work_order_control woc, estimate_charge_type ect, cr_epe_charging_exp_type_mv etmv
where sc.control_id = 6 and bv.current_version = 1
  and yr.control_id = 23 and est.year = substr(yr.control_value,1,4)
  and sc.control_value = bv.description
  and bv.budget_version_id = fp.budget_version_id
  and est.work_order_id = woc.work_order_id
  and est.work_order_id = fp.work_order_id
  and est.revision = fp.revision
  and est.est_chg_type_id = ect.est_chg_type_id
  and nvl(est.job_task_id,'107001') = '107001'
  and ect.external_est_chg_type = etmv.charging_expense_type
  and upper(trim(etmv.element_type)) = 'BUDGET'
  and etmv.allocation_type != '7'
  group by woc.work_order_number
  having
  round(sum(nvl(january,0)))+round(sum(nvl(february,0)))+round(sum(nvl(march,0)))+round(sum(nvl(april,0)))+
  round(sum(nvl(may,0)))+round(sum(nvl(june,0)))+round(sum(nvl(july,0)))+round(sum(nvl(august,0)))+
  round(sum(nvl(september,0)))+round(sum(nvl(october,0)))+round(sum(nvl(november,0)))+round(sum(nvl(december,0)))<>0)
group by year, budget_version, owning_cc, project, project_desc, allocation_type, budget_rollup_descr
order by owning_cc, project, allocation_type
 
 
 
 
 
 ;