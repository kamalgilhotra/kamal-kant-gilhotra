
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_EST_REMOVAL_VIEW" ("WORK_ORDER_ID", "REVISION", "REMOVAL") AS 
  (
select work_order_id, revision, sum(removal) removal
  from (select a.work_order_id, nvl(a.revision, 1) revision, sum(nvl(b.total, 0)) removal
          from wo_est_first_view a, wo_est_monthly b
         where a.work_order_id = b.work_order_id(+)
           and a.revision = b.revision(+)
           and b.expenditure_type_id(+) = 2
           and b.est_chg_type_id(+) is null
         group by a.work_order_id, a.revision
        union
        select a.work_order_id, a.revision, sum(b.total)
          from wo_est_first_view a, wo_est_monthly b
         where a.work_order_id = b.work_order_id(+)
           and a.revision = b.revision(+)
           and b.expenditure_type_id(+) = 2
           and b.est_chg_type_id in
               (select est_chg_type_id from estimate_charge_type where processing_type_id in (2, 4))
         group by a.work_order_id, a.revision)
 group by work_order_id, revision)
 
 
 
 
 
 ;