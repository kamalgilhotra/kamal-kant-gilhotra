
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_APPORTIONMENT_VIEW" ("COMPANY_ID", "ENTITY_ID", "EFFECTIVE_DATE", "COMPANY_DESCRIPTION", "ENTITY_DESCRIPTION", "APPORTIONMENT_PCT") AS 
  SELECT
	inside.company_id,
	inside.entity_id,
	inside.effective_date,
	comp.description company_description,
	ent.description entity_description,
	Nvl(Last_Value(app.apportionment_pct ignore nulls ) OVER (PARTITION BY inside.company_id, inside.entity_id ORDER BY inside.effective_date), 0) AS apportionment_pct
FROM
(
	SELECT app_co.company_id, app_co.entity_id, app_dt.effective_date
	FROM
	(
		SELECT DISTINCT company_id, entity_id
		FROM tax_accrual_apportionment
	) app_co
	INNER JOIN
	(
		SELECT DISTINCT company_id, effective_date
		FROM tax_accrual_apportionment
	) app_dt
		ON app_co.company_id = app_dt.company_id
) inside
inner join company_setup comp on inside.company_id = comp.company_id
inner join tax_accrual_entity ent on inside.entity_id = ent.entity_id
left OUTER JOIN tax_accrual_apportionment app
	ON app.company_id = inside.company_id AND app.entity_id = inside.entity_id AND app.effective_date = inside.effective_date
ORDER BY comp.description, inside.effective_date, inside.entity_id

 ;