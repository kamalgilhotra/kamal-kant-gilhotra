
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."V_PA_AGE_CHANGES" ("COMPANY", "COMPANY_ID", "BUS_SEGMENT", "FUNC_CLASS", "FERC_PLT_ACCT", "UTIL_ACCT", "PROCESS_YEAR", "AVG_AGE", "P_AVG_AGE", "ANNUALCHANGE", "L_AVG_AGE", "F_AVG_AGE", "PERIODCHANGE", "PERIODS") AS 
  (
select c.description as company, x.company_id, a.bus_segment, a.func_class, a.ferc_plt_acct, a.util_acct, x.process_year, avg_age, p_avg_age, avg_age-p_avg_age annualchange, l_avg_age, f_avg_age, l_avg_age-f_avg_age periodchange, periods from (
  select company_id, acct_key, process_year, avg_age
    , count(process_year) over (partition by company_id, acct_key) periods
    , lag(avg_age) over(partition by company_id, acct_key order by company_id, acct_key, process_year) p_avg_age
    , first_value(avg_age) over(partition by company_id, acct_key order by company_id, acct_key, process_year) f_avg_age
    , first_value(avg_age) over(partition by company_id, acct_key order by company_id, acct_key, process_year desc) l_avg_age
  from (
    select company_id, acct_key, process_year, round(case when sum(nvl(age_w_cst,0)) = 0 then 0 else sum(age_w_cst)/sum(w_cost) end,2) avg_age
    from PWRPLANT.pa_average_ages
    group by company_id, acct_key, process_year
    order by company_id, acct_key, process_year)) x
  join PWRPLANT.v_pa_company c on c.company_id = x.company_id
  join PWRPLANT.pa_accounts a on a.acct_key = x.acct_key)
 
 ;