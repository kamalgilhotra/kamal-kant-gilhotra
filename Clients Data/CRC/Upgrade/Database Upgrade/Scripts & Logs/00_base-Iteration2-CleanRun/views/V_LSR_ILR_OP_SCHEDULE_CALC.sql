CREATE OR REPLACE FORCE VIEW v_lsr_ilr_op_schedule_calc AS
SELECT  results.ilr_id,
        results.revision,
        results.set_of_books_id,
        cols.month,
        cols.interest_income_received,
        cols.interest_income_accrued,
        cols.interest_rental_recvd_spread,
        cols.begin_deferred_rev,
        cols.deferred_rev,
        cols.end_deferred_rev,
        cols.begin_receivable,
        cols.end_receivable,
        cols.begin_lt_receivable,
        cols.end_lt_receivable,
        cols.initial_direct_cost,
        cols.executory_accrual1,
        cols.executory_accrual2,
        cols.executory_accrual3,
        cols.executory_accrual4,
        cols.executory_accrual5,
        cols.executory_accrual6,
        cols.executory_accrual7,
        cols.executory_accrual8,
        cols.executory_accrual9,
        cols.executory_accrual10,
        cols.executory_paid1,
        cols.executory_paid2,
        cols.executory_paid3,
        cols.executory_paid4,
        cols.executory_paid5,
        cols.executory_paid6,
        cols.executory_paid7,
        cols.executory_paid8,
        cols.executory_paid9,
        cols.executory_paid10,
        cols.contingent_accrual1,
        cols.contingent_accrual2,
        cols.contingent_accrual3,
        cols.contingent_accrual4,
        cols.contingent_accrual5,
        cols.contingent_accrual6,
        cols.contingent_accrual7,
        cols.contingent_accrual8,
        cols.contingent_accrual9,
        cols.contingent_accrual10,
        cols.contingent_paid1,
        cols.contingent_paid2,
        cols.contingent_paid3,
        cols.contingent_paid4,
        cols.contingent_paid5,
        cols.contingent_paid6,
        cols.contingent_paid7,
        cols.contingent_paid8,
        cols.contingent_paid9,
        cols.contingent_paid10,
        t_lsr_ilr_schedule_all_rates( t_lsr_rates_implicit_in_lease(NULL, NULL), 
                                          t_lsr_rates_implicit_in_lease(NULL, NULL), 
                                          t_lsr_rates_implicit_in_lease(NULL, NULL)) as schedule_rates
FROM (SELECT  ilro.ilr_id,
              ilro.revision,
              fasb_sob.set_of_books_id,
              pkg_lessor_schedule.f_get_op_schedule(ilr_id,revision, fasb_sob.set_of_books_id) sch
      FROM lsr_ilr_options ilro
      JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
      JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
      WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION) ) = 'operating') results, TABLE ( results.sch ) (+) cols;