
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_CO_POST_VIEW" ("TA_VERSION_ID", "COMPANY_ID", "POST_TO_COMPANY_ID", "GL_MONTH") AS 
  select	ta_version_id,
			company_id,
			max(decode(post_to_company_id,0,company_id,post_to_company_id)) post_to_company_id,
			gl_month
from	(	select	co_post.ta_version_id,
						co_post.company_id,
						co_post.post_to_company_id,
						months.gl_month
			from	tax_accrual_company_posting co_post,
					tax_accrual_version_gl_month months
			where co_post.ta_version_id = months.ta_version_id
				and co_post.gl_month	=	(	select max(inside.gl_month)
													from tax_accrual_company_posting inside
													where inside.ta_version_id = co_post.ta_version_id
														and inside.company_id = co_post.company_id
														and inside.gl_month <= months.gl_month
												)
			union all
			select	ta_version_id,
						company_id,
						0,
						gl_month
			from	tax_accrual_version_gl_month,
					company_setup
		)
group by ta_version_id,
			company_id,
			gl_month
 
 
 
 
 ;