CREATE OR REPLACE VIEW V_LS_MONTHLY_TAX_FX_VW (
  LS_ASSET_ID, TAX_LOCAL_ID, GL_POSTING_MO_YR, SET_OF_BOOKS_ID, PAYMENT_AMOUNT, ACCRUAL_AMONT, SCHEDULE_MONTH, VENDOR_ID, 
  TAX_DISTRICT_ID, TAX_RATE, ADJUSTMENT_AMOUNT, TAX_BASE, ILR_ID, ILR_NUMBER, LEASE_ID, LEASE_NUMBER, MONTH, COMPANY_ID, 
  OPEN_MONTH, LS_CUR_TYPE, EXCHANGE_DATE, PREV_EXCHANGE_DATE, CONTRACT_CURRENCY_ID, DISPLAY_CURRENCY_ID, COMPANY_CURRENCY_ID, 
  RATE, CALCULATED_RATE, PREVIOUS_CALCULATED_RATE, ISO_CODE, CURRENCY_DISPLAY_SYMBOL) AS 
SELECT 
 lmt.ls_asset_id,
 lmt.tax_local_id,
 lmt.gl_posting_Mo_yr,
 set_of_books_id,
 lmt.payment_amount * Nvl(calc_rate.actual_rate, cr.actual_rate) payment_amount,
 lmt.accrual_amount * Nvl(calc_rate.actual_rate, cr.actual_rate) accrual_amount,
 lmt.schedule_month,
 lmt.vendor_id,
 lmt.tax_district_id,
 lmt.tax_rate                    tax_rate,
 lmt.adjustment_amount * Nvl(calc_rate.actual_rate, cr.actual_rate) adjustment_amount,
 lmt.tax_base * Nvl(calc_rate.actual_rate, cr.actual_rate) tax_base,
 ilr.ilr_id                  ilr_id,
 ilr.ilr_number,
 lease.lease_id,
 lease.lease_number,
 lmt.gl_posting_mo_yr        MONTH,
 open_month.company_id,
 open_month.open_month,
 cur.ls_cur_type             AS ls_cur_type,
 cr.exchange_date,
 calc_rate.actual_exchange_date     prev_exchange_date,
 lease.contract_currency_id,
 cur.currency_id             display_currency_id,
 cs.currency_id company_currency_id,
 cr.actual_rate rate,
 calc_rate.actual_rate              calculated_rate,
 calc_rate.actual_prev_rate         previous_calculated_rate,
 cur.iso_code,
 cur.currency_display_symbol
  FROM ls_monthly_tax lmt
 INNER JOIN ls_asset asst
    ON asst.ls_asset_id = lmt.ls_asset_id
 INNER JOIN ls_ilr ilr
    ON ilr.ilr_id = asst.ilr_id
   AND ilr.company_id = asst.company_id
 INNER JOIN ls_lease lease
    ON ilr.lease_id = lease.lease_id
 INNER JOIN currency_schema cs
    ON ilr.company_id = cs.company_id
 INNER JOIN (
              SELECT 
                ls_currency_type_id AS ls_cur_type
                , currency_id
                , currency_display_symbol
                , iso_code
                , CASE ls_currency_type_id WHEN 1 THEN 1 ELSE NULL END AS contract_approval_rate
              FROM currency
              CROSS JOIN ls_lease_currency_type
            ) cur 
    ON cur.currency_id = CASE cur.ls_cur_type
      WHEN 1 THEN lease.contract_currency_id
      WHEN 2 THEN cs.currency_id
    END
 INNER JOIN (
              SELECT 
                ls_process_control.company_id
                , Min(gl_posting_mo_yr) open_month
              FROM ls_process_control
              GROUP BY 
                ls_process_control.company_id
            ) open_month
    ON ilr.company_id = open_month.company_id
 INNER JOIN currency_rate_default_vw cr 
    ON cr.currency_from = lease.contract_currency_id
    AND cr.currency_to = cur.currency_id
    AND trunc(cr.exchange_date, 'MONTH') <= trunc(lmt.schedule_month,'MONTH')
    AND trunc(cr.next_date, 'MONTH') > trunc(lmt.schedule_month,'MONTH')
  LEFT JOIN
  (
    SELECT * FROM
    (
      SELECT
        company_id
        , contract_currency_id
        , company_currency_id
        , exchange_rate_type_id
        , accounting_month
        , exchange_date
        , rate
        , Lag(exchange_date) OVER (PARTITION BY company_id, contract_currency_id, company_currency_id, exchange_rate_type_id ORDER BY accounting_month) prev_exchange_date
        , Lag(rate) OVER (PARTITION BY company_id, contract_currency_id, company_currency_id, exchange_rate_type_id ORDER BY accounting_month) prev_rate
      FROM
      (     
        SELECT company_id, contract_currency_id, company_currency_id, exchange_rate_type_id, accounting_month, exchange_date, rate 
        FROM ls_lease_calculated_date_rates
        UNION ALL
        SELECT company_id, contract_currency_id, company_currency_id, exchange_rate_type_id, Add_Months(Max(accounting_month),1) accounting_month, NULL exchange_date, NULL rate 
        FROM ls_lease_calculated_date_rates
        GROUP BY company_id, contract_currency_id, company_currency_id, exchange_rate_type_id
      )
    )
    PIVOT
    (
      Max(exchange_date) AS exchange_date
      , max(rate) AS rate
      , max(prev_exchange_date) AS prev_exchange_date
      , max(prev_rate) AS prev_rate
      FOR exchange_rate_type_id IN (1 AS Actual)
    )
  ) calc_rate
    ON lease.contract_currency_id = calc_rate.contract_currency_id
    AND cur.currency_id = calc_rate.company_currency_id
    AND ilr.company_id = calc_rate.company_id
    AND lmt.schedule_month = calc_rate.accounting_month
 WHERE cs.currency_type_id = 1;
