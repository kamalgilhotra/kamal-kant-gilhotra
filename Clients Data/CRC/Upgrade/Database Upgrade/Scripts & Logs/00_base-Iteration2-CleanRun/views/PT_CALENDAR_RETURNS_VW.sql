
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."PT_CALENDAR_RETURNS_VW" ("PROP_TAX_COMPANY_ID", "COMPANY_DESCR", "STATE_ID", "YEAR", "YEAR_DESCR", "DUE_DATE", "COMPLETION_DATE", "RETURN_ID", "RETURN_DESCRIPTION", "RESPONSIBLE_USER", "PRIORITY", "STATUS", "CALENDAR_TYPE") AS 
  select prop_tax_company_id, company_descr, state_id,
year, year_descr, due_date, completion_date,
return_id, return_description, responsible_user,
case when completion_date is not null then 'Complete'
     else
      case when days_until_due <= crit then 'Critical'
           when crit < days_until_due and days_until_due <= warn then 'Warning'
           else 'Normal'
      end
end priority,
case when completion_date is not null then 'Complete'
     else 'Initiated'
end status,
'Return' calendar_type
from (
select distinct pt_process_return.prop_tax_company_id, pt_process_return.state_id, property_tax_year.year
, nvl(pt_process_return.extension_due_date, nvl(pt_process_return.due_date,sysdate)) due_date
, pt_process_return.completion_date
, pt_process_return.return_id
, pt_company.description company_descr
, property_tax_year.description year_descr
, pt_company.description ||': '|| trim(pt_process_return.state_id) ||': '|| property_tax_year.description return_description
, nvl(pt_parcel.responsible_user, 'Not Assigned') responsible_user
, nvl(trim(f_pp_system_control(-1, 'PT Calendar Warning Days Threshold')),21) warn
, nvl(trim(f_pp_system_control(-1, 'PT Calendar Critical Days Threshold')),7) crit
, trunc(nvl(pt_process_return.extension_due_date, nvl(pt_process_return.due_date,sysdate)) - sysdate,0) days_until_due
from pt_process_return, property_tax_year, pt_parcel, pt_company
where pt_process_return.tax_year = property_tax_year.tax_year
and pt_process_return.prop_tax_company_id = pt_parcel.prop_tax_company_id
and pt_process_return.state_id = pt_parcel.state_id
and pt_process_return.prop_tax_company_id = pt_company.prop_tax_company_id
)
;