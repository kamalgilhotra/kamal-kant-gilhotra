
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_GL_MONTH" ("GL_MONTH", "DESCRIPTION", "LONG_MONTH") AS 
  select	gl_month,
			month_description description,
			month_description long_month
from tax_accrual_gl_month_def
where ta_version_id = tax_accrual_control_pkg.f_get_ta_version_id()
union all
select	gl_month,
			max(month_description) description,
			max(month_description) long_month
from tax_accrual_gl_month_def
where gl_month in	(	select gl_month
							from tax_accrual_gl_month_def
							minus
							select gl_month
							from tax_accrual_gl_month_def
							where ta_version_id = tax_accrual_control_pkg.f_get_ta_version_id()
						)
group by gl_month
 
 
 
 
 ;