
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WORK_ORDER_APPROVAL_V" ("WORK_ORDER_ID", "REVISION", "APP_LEV", "AUTHORIZER", "AUTHORIZED_DATE", "REJECTED", "APPROVAL_STATUS_ID") AS 
  select WORK_ORDER_ID, REVISION, APP_LEV, AUTHORIZER, AUTHORIZED_DATE, REJECTED, APPROVAL_STATUS_ID
  from (select WORK_ORDER_ID,
               REVISION,
               1 APP_LEV,
               AUTHORIZER1 AUTHORIZER,
               AUTHORIZED_DATE1 AUTHORIZED_DATE,
               REJECTED,
               APPROVAL_STATUS_ID
          from WORK_ORDER_APPROVAL
         where AUTHORIZER1 is not null
        union
        select WORK_ORDER_ID,
               REVISION,
               2,
               AUTHORIZER2,
               AUTHORIZED_DATE2,
               REJECTED,
               APPROVAL_STATUS_ID
          from WORK_ORDER_APPROVAL
         where AUTHORIZER2 is not null
        union
        select WORK_ORDER_ID,
               REVISION,
               3,
               AUTHORIZER3,
               AUTHORIZED_DATE3,
               REJECTED,
               APPROVAL_STATUS_ID
          from WORK_ORDER_APPROVAL
         where AUTHORIZER3 is not null
        union
        select WORK_ORDER_ID,
               REVISION,
               4,
               AUTHORIZER4,
               AUTHORIZED_DATE4,
               REJECTED,
               APPROVAL_STATUS_ID
          from WORK_ORDER_APPROVAL
         where AUTHORIZER4 is not null
        union
        select WORK_ORDER_ID,
               REVISION,
               5,
               AUTHORIZER5,
               AUTHORIZED_DATE5,
               REJECTED,
               APPROVAL_STATUS_ID
          from WORK_ORDER_APPROVAL
         where AUTHORIZER5 is not null
        union
        select WORK_ORDER_ID,
               REVISION,
               6,
               AUTHORIZER6,
               AUTHORIZED_DATE6,
               REJECTED,
               APPROVAL_STATUS_ID
          from WORK_ORDER_APPROVAL
         where AUTHORIZER6 is not null
        union
        select WORK_ORDER_ID,
               REVISION,
               7,
               AUTHORIZER7,
               AUTHORIZED_DATE7,
               REJECTED,
               APPROVAL_STATUS_ID
          from WORK_ORDER_APPROVAL
         where AUTHORIZER7 is not null
        union
        select WORK_ORDER_ID,
               REVISION,
               8,
               AUTHORIZER8,
               AUTHORIZED_DATE8,
               REJECTED,
               APPROVAL_STATUS_ID
          from WORK_ORDER_APPROVAL
         where AUTHORIZER8 is not null
        union
        select WORK_ORDER_ID,
               REVISION,
               9,
               AUTHORIZER9,
               AUTHORIZED_DATE9,
               REJECTED,
               APPROVAL_STATUS_ID
          from WORK_ORDER_APPROVAL
         where AUTHORIZER9 is not null
        union
        select WORK_ORDER_ID,
               REVISION,
               10,
               AUTHORIZER10,
               AUTHORIZED_DATE10,
               REJECTED,
               APPROVAL_STATUS_ID
          from WORK_ORDER_APPROVAL
         where AUTHORIZER10 is not null)
 
 
 
 
 
 ;