
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."BUDGET_VERSION" ("BUDGET_VERSION_ID", "TIME_STAMP", "USER_ID", "DESCRIPTION", "LONG_DESCRIPTION", "LOCKED", "START_YEAR", "CURRENT_YEAR", "END_YEAR", "ACTUALS_MONTH", "DATE_FINALIZED", "CURRENT_VERSION", "OPEN_FOR_ENTRY", "BUDGET_ONLY", "EXTERNAL_BUDGET_VERSION", "BRING_IN_SUBS", "OUTLOOK_NUMBER", "LEVEL_ID", "REPORTING_CURRENCY_ID", "BUDGET_VERSION_TYPE_ID", "COMPANY_ID", "PROCESS_LEVEL") AS 
  SELECT
   bvc.BUDGET_VERSION_ID,
   bvc.TIME_STAMP,
   bvc.USER_ID,
   bvc.DESCRIPTION,
   bvc.LONG_DESCRIPTION,
   bvc.LOCKED,
   bvc.START_YEAR,
   bvc.CURRENT_YEAR,
   bvc.END_YEAR,
   bvc.ACTUALS_MONTH,
   bvc.DATE_FINALIZED,
   bvc.CURRENT_VERSION,
   nvl((select case when read_only = 1 then 0 else null end from budget_version_security a where a.budget_version_id = bvc.budget_version_id and lower(a.users) = lower(user)),bvc.OPEN_FOR_ENTRY) open_for_entry,
   bvc.BUDGET_ONLY,
   bvc.EXTERNAL_BUDGET_VERSION,
   bvc.BRING_IN_SUBS,
   bvc.OUTLOOK_NUMBER,
   bvc.LEVEL_ID,
   bvc.REPORTING_CURRENCY_ID,
   bvc.BUDGET_VERSION_TYPE_ID,
   bvc.COMPANY_ID,
   bvc.PROCESS_LEVEL
FROM BUDGET_VERSION_CONTROL bvc
where (
   bvc.budget_version_id in (
      select bvs.budget_version_id
      from budget_version_security bvs
      where lower(bvs.users) = lower(USER)
      )
   OR
   bvc.budget_version_id in (
      select bvsa.budget_version_id
      from budget_version_security_all bvsa
      )
   )
 ;