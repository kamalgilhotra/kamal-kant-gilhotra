
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_EST_JOBBING_VIEW2APP" ("WORK_ORDER_ID", "REVISION", "JOBBING") AS 
  select work_order_id, revision, sum(jobbing) jobbing   from
(select a.work_order_id, nvl(a.revision, 1) revision, nvl(sum(b.amount), 0) jobbing
from wo_est_first_view a, wo_estimate b            where a.work_order_id = b.work_order_id (+)
and a.revision = b.revision (+)            and b.expenditure_type_id (+) = 4
and b.est_chg_type_id (+) is null            group by a.work_order_id, a.revision
union           select a.work_order_id, nvl(a.revision, 1) revision, nvl(sum(b.amount), 0) jobbing
from wo_est_first_view a, wo_estimate b          where a.work_order_id = b.work_order_id (+)
and a.revision = b.revision (+)               and b.expenditure_type_id (+) = 4
and b.est_chg_type_id in                (select est_chg_type_id from estimate_charge_type
where processing_type_id = 4 and nvl(exclude_from_approval_amount,0) = 0)
group by a.work_order_id, a.revision)
group by work_order_id, revision

 
 
 
 
 
 ;