CREATE OR REPLACE VIEW V_LSR_ILR_MC_CALC_AMOUNTS AS
WITH cur AS (
  SELECT ls_currency_type_id AS ls_cur_type,
    currency_id,
    currency_display_symbol,
    iso_code,
    CASE ls_currency_type_id
      WHEN 1 THEN 1
      ELSE NULL
    END AS contract_approval_rate
  FROM currency
  CROSS JOIN ls_lease_currency_type
)
SELECT
lia.ilr_id,
lia.revision,
lia.set_of_books_id,
lia.npv_lease_payments * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS npv_lease_payments,
lia.npv_guaranteed_residual * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS npv_guaranteed_residual,
lia.npv_unguaranteed_residual * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS npv_unguaranteed_residual,
lia.selling_profit_loss * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS selling_profit_loss,
lia.beginning_lease_receivable * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS beginning_lease_receivable,
lia.beginning_net_investment * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS beginning_net_investment,
lia.cost_of_goods_sold * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS cost_of_goods_sold,
nvl(idc.amount, 0) * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS initial_direct_cost,
Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) rate,
cur.ls_cur_type,
cur.currency_id,
cur.currency_display_symbol,
cur.iso_code
FROM lsr_ilr_amounts lia
  INNER JOIN lsr_ilr ilr ON (lia.ilr_id = ilr.ilr_id)
  INNER JOIN lsr_lease lease ON (ilr.lease_id = lease.lease_id)
	INNER JOIN lsr_ilr_options lio ON (lia.ilr_id = lio.ilr_id AND lia.revision = lio.revision)
	INNER JOIN currency_schema cs
    ON ilr.company_id = cs.company_id
	INNER JOIN cur
    ON cur.currency_id =
      CASE cur.ls_cur_type
        WHEN 1 THEN lease.contract_currency_id
        WHEN 2 THEN cs.currency_id
        ELSE NULL
      END
  left outer join (select ilr_id, revision, sum(amount) amount 
                     from lsr_ilr_initial_direct_cost
                    group by ilr_id, revision) idc on idc.ilr_id = lia.ilr_id
                                                  and idc.revision = lia.revision
WHERE cs.currency_type_id = 1;