
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CR_BUDGET_DATA_LABOR2_V" ("ROW_ID", "ELEMENT_VALUE", "BUDGET_VERSION", "YEAR", "RESOURCE_ID", "RESOURCE_DESCRIPTION", "POSITION_DESCRIPTION", "START_DATE", "END_DATE", "HOURS", "BASE_RATE", "BASE_LABOR", "ATTRIBUTE_1", "ATTRIBUTE_2", "ATTRIBUTE_3", "ATTRIBUTE_4", "ATTRIBUTE_5", "ATTRIBUTE_6", "ATTRIBUTE_7", "ATTRIBUTE_8", "ATTRIBUTE_9", "ATTRIBUTE_10", "ATTRIBUTE_11", "ATTRIBUTE_12", "ATTRIBUTE_13", "ATTRIBUTE_14", "ATTRIBUTE_15", "ATTRIBUTE_16", "ATTRIBUTE_17", "ATTRIBUTE_18", "ATTRIBUTE_19", "ATTRIBUTE_20", "AMOUNT_1", "AMOUNT_2", "AMOUNT_3", "AMOUNT_4", "AMOUNT_5", "AMOUNT_6", "AMOUNT_7", "AMOUNT_8", "AMOUNT_9", "AMOUNT_10", "AMOUNT_11", "AMOUNT_12", "AMOUNT_13", "AMOUNT_14", "AMOUNT_15", "AMOUNT_16", "AMOUNT_17", "AMOUNT_18", "AMOUNT_19", "AMOUNT_20", "PERCENT_1", "PERCENT_2", "PERCENT_3", "PERCENT_4", "PERCENT_5", "PERCENT_6", "PERCENT_7", "PERCENT_8", "PERCENT_9", "PERCENT_10", "PERCENT_11", "PERCENT_12", "PERCENT_13", "PERCENT_14", "PERCENT_15", "PERCENT_16", "PERCENT_17", "PERCENT_18", "PERCENT_19", "PERCENT_20", "DATE_1", "DATE_2", "DATE_3", "DATE_4", "DATE_5", "DATE_6", "DATE_7", "DATE_8", "DATE_9", "DATE_10", "COMPUTED_1", "COMPUTED_2", "COMPUTED_3", "COMPUTED_4", "COMPUTED_5", "COMPUTED_6", "COMPUTED_7", "COMPUTED_8", "COMPUTED_9", "COMPUTED_10", "COMPUTED_11", "COMPUTED_12", "COMPUTED_13", "COMPUTED_14", "COMPUTED_15", "COMPUTED_16", "COMPUTED_17", "COMPUTED_18", "COMPUTED_19", "COMPUTED_20", "TOTAL", "COMPANY_ROLLUP", "DIVISION", "DEPARTMENT", "COST_CENTER") AS 
  (  select a.row_id row_id,  	 	    a.element_value element_value,  		b.budget_version budget_version,
    	  a.year year,  		        a.resource_id resource_id,  		    a.resource_description resource_description,
          a.position_description,	    a.start_date start_date,  		        a.end_date end_date,
          a.hours hours,  		        a.base_rate base_rate,  		        a.base_labor base_labor,
          a.attribute_1,                a.attribute_2,  		                a.attribute_3,
          a.attribute_4,                a.attribute_5,  		                a.attribute_6,
          a.attribute_7,                a.attribute_8,  		                a.attribute_9,
          a.attribute_10,               a.attribute_11,  		                a.attribute_12,
          a.attribute_13,  		        a.attribute_14,                 		a.attribute_15,
          a.attribute_16,  		        a.attribute_17,  		                a.attribute_18,
          a.attribute_19,  	        	a.attribute_20,
          a.amount_1,  		            a.amount_2,                       		a.amount_3,
          a.amount_4,             		a.amount_5,                      		a.amount_6,
          a.amount_7,  		            a.amount_8,  		                    a.amount_9,
          a.amount_10,  		        a.amount_11,  		                    a.amount_12,
          a.amount_13,           		a.amount_14,  		                    a.amount_15,
          a.amount_16,  		        a.amount_17,  		                    a.amount_18,
          a.amount_19,  		        a.amount_20,
          a.percent_1,  		        a.percent_2,                     		a.percent_3,
          a.percent_4,  		        a.percent_5,  		                    a.percent_6,
          a.percent_7,  		        a.percent_8,  		                    a.percent_9,
          a.percent_10,  		        a.percent_11,  		                    a.percent_12,
          a.percent_13,  		        a.percent_14,  		                    a.percent_15,
          a.percent_16,  		        a.percent_17,  		                    a.percent_18,
          a.percent_19,  		        a.percent_20,
          a.date_1,          		    a.date_2 date_2,  		                a.date_3,
          a.date_4,  		            a.date_5 date_5,  		                a.date_6,
          a.date_7,           		    a.date_8 date_8,                  		a.date_9,
          a.date_10,
          a.computed_1,                 a.computed_2,                     		a.computed_3,
          a.computed_4,  		        a.computed_5,  		                    a.computed_6,
          a.computed_7,  		        a.computed_8,  		                    a.computed_9,
          a.computed_10,  		        a.computed_11,  		                a.computed_12,
          a.computed_13,  		        a.computed_14,  		                a.computed_15,
          a.computed_16,  		        a.computed_17,  		                a.computed_18,
          a.computed_19,  		        a.computed_20,  		                a.total,
          cc.co_rollup_description,     cc.div_description,                     cc.dept_description, cc.description
FROM  cr_budget_data_labor2 a,                  cr_budget_version b,  			cr_budget_version_security cbvs,
      cr_epe_charging_cost_center_mv cc
WHERE trim(a.element_value) in
                (SELECT trim(budget_value)
                   FROM cr_budget_users_valid_values
                  WHERE trim(lower(users)) = trim(lower(user)))
            and a.cr_budget_version_id = b.cr_budget_version_id
  and upper(trim(b.budget_version)) = upper(trim(cbvs.budget_version))
  and upper(trim(user)) = upper(trim(cbvs.users))
  and a.element_value = cc.charging_cost_center(+)
  and cc.element_type(+) = 'Actuals'
  and cc.co_rollup_description is not null
  and cc.div_description is not null
  and cc.dept_description is not null)
 
 
 
 
 
 ;