CREATE OR REPLACE VIEW V_LSR_INVOICE_LINE_FX AS
WITH cur
     AS (SELECT 1                                    ls_cur_type,
                contract_cur.currency_id             AS currency_id,
                contract_cur.currency_display_symbol currency_display_symbol,
                contract_cur.iso_code                iso_code,
                1                                    historic_rate
         FROM   currency contract_cur
         UNION
         SELECT 2,
                company_cur.currency_id,
                company_cur.currency_display_symbol,
                company_cur.iso_code,
                0
         FROM   currency company_cur)
SELECT lil.invoice_id,
       lil.invoice_line_number,
       lil.invoice_type_id,
       lil.amount * Decode(ls_cur_type, 2, nvl(cr.rate, cur.historic_rate), 1)  amount,
       lil.adjustment_amount,
       lil.description,
       lil.notes,
       lease.contract_currency_id,
       cs.currency_id                       company_currency_id,
       cur.ls_cur_type                      AS ls_cur_type,
       Nvl(cr.exchange_date, '01-Jan-1900') exchange_date,
       Decode(ls_cur_type, 2, nvl(cr.rate, cur.historic_rate), 1)            rate,
       cur.iso_code,
       cur.currency_display_symbol,
   lil.set_of_books_id
FROM   lsr_invoice_line lil
       inner join lsr_invoice li
               ON ( li.invoice_id = lil.invoice_id )
       inner join lsr_ilr ilr
           ON li.ilr_id = ilr.ilr_id
        inner join pp_system_control_companies sc
              ON ilr.company_id = sc.company_id
       inner join lsr_lease lease
               ON ilr.lease_id = lease.lease_id
       inner join currency_schema cs
               ON ilr.company_id = cs.company_id
       inner join cur
               ON ( ( cur.ls_cur_type = 1
                      AND cur.currency_id = lease.contract_currency_id )
                     OR ( cur.ls_cur_type = 2
                          AND cur.currency_id = cs.currency_id ) )
       left outer join ls_lease_calculated_date_rates cr
              ON ( cr.company_id = ilr.company_id
                   AND cr.contract_currency_id =
                       lease.contract_currency_id
                   AND cr.company_currency_id = cs.currency_id
                   AND cr.accounting_month = li.gl_posting_mo_yr )
WHERE sc.CONTROL_NAME = 'Lease MC: Use Average Rates'
AND nvl(cr.EXCHANGE_RATE_TYPE_ID, DECODE(LOWER(sc.CONTROL_VALUE), 'yes', 4, 1)) = DECODE(LOWER(sc.CONTROL_VALUE), 'yes', 4, 1)
AND cs.currency_type_id = 1;