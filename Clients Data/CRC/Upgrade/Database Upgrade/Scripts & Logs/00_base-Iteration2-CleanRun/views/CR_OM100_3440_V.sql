
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CR_OM100_3440_V" ("CHARGING_COST_CENTER", "CHARGING_COST_CENTER_DESC", "ALLOCATION_TYPE", "A_TYPE", "SUBTOTAL3", "BUDGET_ROLLUP", "BUDGET_ROLLUP_DESCR", "ACTUALS_AMT", "BUDGET_AMT", "VARIANCE", "VARPERCENT", "ACTUALSYTD", "BUDGETYTD", "VARIANCEYTD", "VARPERCENT2", "PROJECTED_BUDGET", "UNEXPENDED_BUDGET", "CURRENT_MONTH") AS 
  SELECT		charging_cost_center,
			charging_cost_center_desc,
			allocation_type,
			decode(allocation_type, '1', 'PAYROLL', '2', 'PAYROLL', '3', 'PAYROLL', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
			decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '0') as "SUBTOTAL3",
			min(budget_rollup) budget_rollup,
			min(budget_rollup_descr) budget_rollup_descr,
			round(sum(actuals_amount)) actuals_amt,
			round(sum(budget_amount)) budget_amt,
			round(sum(actuals_amount)) - round(sum(budget_amount)) Variance,
			round(case when round(sum(budget_amount)) = 0
			            and round(sum(actuals_amount)) = 0 then 0
						when round(sum(budget_amount)) = 0
			            and round(sum(actuals_amount)) != 0 then 100 * sign(sum(actuals_amount))
						else (round((sum(actuals_amount)) - round(sum(budget_amount)))/round(sum(budget_amount)))*100
                              end,1) Varpercent,
			round(sum(actualsytd)) actualsytd,
			round(sum(budgetytd)) budgetytd,
			round(sum(actualsytd)) - round(sum(budgetytd)) VarianceYTD,
			round(case when round(sum(budgetytd)) = 0
			            and round(sum(actualsytd)) = 0 then 0
						when round(sum(budgetytd)) = 0
			            and round(sum(actualsytd)) != 0 then 100 * sign(sum(actualsytd))
						else (round((sum(actualsytd)) - round(sum(budgetytd)))/round(sum(budgetytd)))*100
                              end,1) Varpercent2,
			round(sum(projected_budget)) projected_budget,
			round(sum(projected_budget)) - round(sum(actualsytd)) Unexpended_Budget,
			(select min (case
            when control_id = 5
            then to_char(to_date(control_value, 'YYYYMM'), 'fmMonth YYYY')
            end)
            from cr_alloc_system_control) Current_Month
FROM
(select     c.charging_cost_center charging_cost_center,
            c.description charging_cost_center_desc,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            sum(a.amount) budget_amount,
            0 actualsytd,
            0 budgetytd,
            0 projected_budget
from         cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select control_value from cr_alloc_system_control
                   where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f
where     a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and a.month_number = d.control_value
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and c.charging_cost_center = '3440'
group     by    c.charging_cost_center,
            c.description,
            e.allocation_type,
            e.budget_rollup,
            e.budget_rollup_descr
union
select      c.charging_cost_center charging_cost_center,
            c.description charging_cost_center_desc,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            e.budget_rollup,
            e.budget_rollup_descr,
            sum(a.amount) actuals_amount,
            0 budget_amount,
            0 actualsytd,
            0 budgetytd,
            0 projected_budget
from         cr_epe_charging_cost_center_mv c,
            cr_cost_repository_sv a,
                        (select control_value from cr_alloc_system_control
                   where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f
where    a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.account = f.account
            and a.month_number = d.control_value
            and upper(trim(c.element_type)) = 'ACTUALS'
            and upper(trim(e.element_type)) = 'ACTUALS'
            and upper(trim(f.element_type)) = 'ACTUALS'
            and f.acct_group between 'A' and 'E'
            and c.charging_cost_center = '3440'
group by     c.charging_cost_center,
            c.description,
            e.allocation_type,
            e.budget_rollup,
            e.budget_rollup_descr
union
select      c.charging_cost_center charging_cost_center,
            c.description charging_cost_center_desc,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 budget_amount,
            sum (a.amount) actualsytd,
            0 budgetytd,
            0 projected_budget
from        cr_epe_charging_cost_center_mv c,
            cr_cost_repository_sv a,
            (select min (case
                         when control_id = 23
                         then control_value
                         end
                         ) as start_month,
                    min (case
                         when control_id = 24
                         then control_value
                         end
                         ) as end_month
              from cr_alloc_system_control
              )    d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f
where    a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.account = f.account
            and a.month_number between d.start_month and d.end_month
            and upper(trim(c.element_type)) = 'ACTUALS'
            and upper(trim(e.element_type)) = 'ACTUALS'
            and upper(trim(f.element_type)) = 'ACTUALS'
            and f.acct_group between 'A' and 'E'
            and c.charging_cost_center = '3440'
group     by    c.charging_cost_center,
            c.description,
            e.allocation_type,
            e.budget_rollup,
            e.budget_rollup_descr
union
select      c.charging_cost_center charging_cost_center,
            c.description charging_cost_center_desc,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 budget_amount,
            0 actualsytd,
            sum(a.amount) budgetytd,
            0 projected_budget
from        cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select min (case
                         when control_id = 23
                         then control_value
                         end
                         ) as start_month,
                    min (case
                         when control_id = 24
                         then control_value
                         end
                         ) as end_month
              from cr_alloc_system_control
              )    d,
             cr_epe_charging_exp_type_mv e,
             cr_epe_account_mv f
where     a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and a.month_number between d.start_month and d.end_month
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and c.charging_cost_center = '3440'
group     by    c.charging_cost_center,
            c.description,
            e.allocation_type,
            e.budget_rollup,
            e.budget_rollup_descr
union
select         c.charging_cost_center charging_cost_center,
            c.description charging_cost_center_desc,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 budget_amount,
            0 actualsytd,
            0 budgetytd,
            sum(a.amount) projected_budget
from        cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select min (case
                        when control_id = 5
                        then substr(control_value,1,4)
                        end
                        )as allyear
                        from cr_alloc_system_control) d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f
where     a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and substr(a.month_number,1,4) = d.allyear
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and c.charging_cost_center = '3440'
group     by c.charging_cost_center,
             c.description,
            e.allocation_type,
            e.budget_rollup,
            e.budget_rollup_descr)
WHERE allocation_type = 1
GROUP BY    charging_cost_center,
            charging_cost_center_desc,
            allocation_type
HAVING abs(sum(nvl(actuals_amount,0)) + sum(nvl(budget_amount,0)))>.49
   or abs(sum(nvl(actualsytd,0)) + sum(nvl(budgetytd,0))) > .49
   or abs(sum(projected_budget) - sum(actualsytd)) > .49
UNION
SELECT		charging_cost_center,
			charging_cost_center_desc,
			allocation_type,
			decode(allocation_type, '1', 'PAYROLL', '6', 'ALLOCATIONS', '3', 'PAYROLL', '4', 'LABOR RELATED', '5', 'NON LABOR', '2', 'PAYROLL') as "ALLOCATION TYPE",
			decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '0') as "SUBTOTAL3",
			budget_rollup,
			budget_rollup_descr,
			round(sum(actuals_amount)) actuals_amt,
			round(sum(budget_amount)) budget_amt,
			round(sum(actuals_amount)) - round(sum(budget_amount)) Variance,
			round(case when round(sum(budget_amount)) = 0
			            and round(sum(actuals_amount)) = 0 then 0
						when round(sum(budget_amount)) = 0
			            and round(sum(actuals_amount)) != 0 then 100 * sign(sum(actuals_amount))
						else (round((sum(actuals_amount)) - round(sum(budget_amount)))/round(sum(budget_amount)))*100
                              end,1) Varpercent,
			round(sum(actualsytd)) actualsytd,
			round(sum(budgetytd)) budgetytd,
			round(sum(actualsytd)) - round(sum(budgetytd)) VarianceYTD,
			round(case when round(sum(budgetytd)) = 0
			            and round(sum(actualsytd)) = 0 then 0
						when round(sum(budgetytd)) = 0
			            and round(sum(actualsytd)) != 0 then 100 * sign(sum(actualsytd))
						else (round((sum(actualsytd)) - round(sum(budgetytd)))/round(sum(budgetytd)))*100
                              end,1) Varpercent2,
			round(sum(projected_budget)) projected_budget,
			round(sum(projected_budget)) - round(sum(actualsytd)) Unexpended_Budget,
			(select min (case
            when control_id = 5
            then to_char(to_date(control_value, 'YYYYMM'), 'fmMonth YYYY')
            end)
            from cr_alloc_system_control) Current_Month
FROM (
select         c.charging_cost_center charging_cost_center,
            c.description charging_cost_center_desc,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            sum(a.amount) budget_amount,
            0 actualsytd,
            0 budgetytd,
            0 projected_budget
from         cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select control_value from cr_alloc_system_control
                   where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f
where     a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and a.month_number = d.control_value
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and c.charging_cost_center = '3440'
group     by    c.charging_cost_center,
            c.description,
            e.allocation_type,
            e.budget_rollup,
            e.budget_rollup_descr
union
select         c.charging_cost_center charging_cost_center,
            c.description charging_cost_center_desc,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            e.budget_rollup,
            e.budget_rollup_descr,
            sum(a.amount) actuals_amount,
            0 budget_amount,
            0 actualsytd,
            0 budgetytd,
            0 projected_budget
from         cr_epe_charging_cost_center_mv c,
            cr_cost_repository_sv a,
                        (select control_value from cr_alloc_system_control
                   where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f
where    a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.account = f.account
            and a.month_number = d.control_value
            and upper(trim(c.element_type)) = 'ACTUALS'
            and upper(trim(e.element_type)) = 'ACTUALS'
            and upper(trim(f.element_type)) = 'ACTUALS'
            and f.acct_group between 'A' and 'E'
            and c.charging_cost_center = '3440'
group by     c.charging_cost_center,
            c.description,
            e.allocation_type,
            e.budget_rollup,
            e.budget_rollup_descr
union
select         c.charging_cost_center charging_cost_center,
            c.description charging_cost_center_desc,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 budget_amount,
            sum (a.amount) actualsytd,
            0 budgetytd,
            0 projected_budget
from         cr_epe_charging_cost_center_mv c,
            cr_cost_repository_sv a,
            (select min (case
                         when control_id = 23
                         then control_value
                         end
                         ) as start_month,
                    min (case
                         when control_id = 24
                         then control_value
                         end
                         ) as end_month
              from cr_alloc_system_control
              )    d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f
where    a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.account = f.account
            and a.month_number between d.start_month and d.end_month
            and upper(trim(c.element_type)) = 'ACTUALS'
            and upper(trim(e.element_type)) = 'ACTUALS'
            and upper(trim(f.element_type)) = 'ACTUALS'
            and f.acct_group between 'A' and 'E'
            and c.charging_cost_center = '3440'
group     by    c.charging_cost_center,
            c.description,
            e.allocation_type,
            e.budget_rollup,
            e.budget_rollup_descr
union
select         c.charging_cost_center charging_cost_center,
            c.description charging_cost_center_desc,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 budget_amount,
            0 actualsytd,
            sum(a.amount) budgetytd,
            0 projected_budget
from         cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select min (case
                         when control_id = 23
                         then control_value
                         end
                         ) as start_month,
                    min (case
                         when control_id = 24
                         then control_value
                         end
                         ) as end_month
              from cr_alloc_system_control
              )    d,
             cr_epe_charging_exp_type_mv e,
             cr_epe_account_mv f
where     a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and a.month_number between d.start_month and d.end_month
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and c.charging_cost_center = '3440'
group     by    c.charging_cost_center,
            c.description,
            e.allocation_type,
            e.budget_rollup,
            e.budget_rollup_descr
union
select         c.charging_cost_center charging_cost_center,
            c.description charging_cost_center_desc,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 budget_amount,
            0 actualsytd,
            0 budgetytd,
            sum(a.amount) projected_budget
from         cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select min (case
                        when control_id = 5
                        then substr(control_value,1,4)
                        end
                        )as allyear
                        from cr_alloc_system_control) d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f
where     a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and substr(a.month_number,1,4) = d.allyear
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and c.charging_cost_center = '3440'
group	 by	c.charging_cost_center,
			c.description,
			e.allocation_type,
			e.budget_rollup,
			e.budget_rollup_descr)
WHERE allocation_type IN (2,3,4,5,6)
GROUP BY	charging_cost_center,
			charging_cost_center_desc,
			allocation_type,
			budget_rollup,
            budget_rollup_descr
HAVING abs(sum(nvl(actuals_amount,0)) + sum(nvl(budget_amount,0)))>.49
   or abs(sum(nvl(actualsytd,0)) + sum(nvl(budgetytd,0))) > .49
   or abs(sum(projected_budget) - sum(actualsytd)) > .49
order by 1,2,3
 
 
 
 
 
 ;