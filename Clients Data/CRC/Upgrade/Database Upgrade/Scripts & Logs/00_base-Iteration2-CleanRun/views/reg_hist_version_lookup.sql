
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."REG_HIST_VERSION_LOOKUP" ("VERSION_ID", "LONG_DESCRIPTION") AS 
  SELECT historic_version_id version_id, long_description FROM reg_historic_version
 
 ;