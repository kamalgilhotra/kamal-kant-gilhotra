CREATE OR REPLACE VIEW CR_BUDGET_DATA_VIEW AS
SELECT "ID",
       "PROVIDING_AREA",
       "TARGET_AREA",
       "RESOURCE_CODE",
       "TASK",
       "SID",
       "PROJECT_ITEM",
       "COST_CODE",
       "LOCATION_ID",
       "COMPANY",
       "FERC",
       "FUTURE_USE_1",
       "FUTURE_USE_2",
       "DR_CR_ID",
       "LEDGER_SIGN",
       "QUANTITY",
       "AMOUNT",
       "INCEPTION_TO_DATE_AMOUNT",
       "MONTH_NUMBER",
       "MONTH_PERIOD",
       "BUDGET_VERSION",
       "GL_JOURNAL_CATEGORY",
       "SOURCE_ID",
       "AMOUNT_TYPE",
       "DRILLDOWN_KEY",
       "GL_ID",
       "BATCH_ID",
       "CWIP_CHARGE_STATUS",
       "TIME_STAMP",
       "USER_ID",
       "ALLOCATION_ID",
       "TARGET_CREDIT",
       "REVERSAL_ID",
       "CROSS_CHARGE_COMPANY"
  FROM cr_budget_data
 WHERE (budget_version IN (SELECT budget_version
                             FROM cr_budget_version_security
                            WHERE users = lower(USER)) OR
       budget_version IN
       (SELECT budget_version
           FROM cr_budget_version_security_all));
