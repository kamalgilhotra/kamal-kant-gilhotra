CREATE OR REPLACE VIEW V_LSR_ILR_MASS_APP_KICKOUTS AS
SELECT DISTINCT ilr_id, revision, occurrence_id, time_stamp, message
FROM lsr_ilr_schedule_kickouts
UNION ALL
SELECT DISTINCT ilr_id, revision, occurrence_id, time_stamp, message
FROM lsr_ilr_approval_kickouts
UNION ALL
SELECT DISTINCT ilr_id, revision, NULL, time_stamp, message
FROM lsr_ilr_mass_app_val_kickouts;