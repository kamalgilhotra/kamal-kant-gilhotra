
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_EST_TOTAL_COST_VIEW" ("WORK_ORDER_ID", "EXPENDITURE_TYPE_ID", "TOTAL_COST", "IN_PROCESS_BALANCE", "CURRENT_CHARGES") AS 
  SELECT WORK_ORDER_ID,  EXPENDITURE_TYPE_ID,      		          SUM(TOTAL_COST) "TOTAL_COST",      	         SUM(IN_PROCESS_BALANCE) "IN_PROCESS_BALANCE",         		          SUM(CURRENT_CHARGES) "CURRENT_CHARGES"               FROM CHARGE_SUMMARY      WHERE MONTH = (select min(accounting_month) from wo_process_control                    where powerplant_closed is null)     	   GROUP BY WORK_ORDER_ID, EXPENDITURE_TYPE_ID
 
 
 
 
 
 ;