
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CAP120_V" ("PROJECT", "DESCRIPTION", "ALLOCATION_TYPE", "EXPENSE_TYPE", "YR1", "YR2", "YR3", "YR4", "YR5", "YR6", "YR7", "YR8", "YR9", "YR10", "YR_TOTAL", "ATYPE", "SUBTOTAL3", "SUBTOTAL4", "BUDGET_VERSION", "AYR1", "AYR2", "AYR3", "AYR4", "AYR5", "AYR6", "AYR7", "AYR8", "AYR9", "AYR10", "AYR_TOTAL", "Y1", "Y2", "Y3", "Y4", "Y5", "Y6", "Y7", "Y8", "Y9", "Y10") AS 
  select s.project,
	   s.description,
       allocation_type,
       budget_rollup_descr expense_type,
       round(sum(yr1)) yr1, round(sum(yr2)) yr2, round(sum(yr3)) yr3, round(sum(yr4)) yr4,
       round(sum(yr5)) yr5, round(sum(yr6)) yr6, round(sum(yr7)) yr7, round(sum(yr8)) yr8,
       round(sum(yr9)) yr9, round(sum(yr10)) yr10,
       (round(sum(yr1))+round(sum(yr2))+round(sum(yr3))+round(sum(yr4))+round(sum(yr5))+round(sum(yr6))+round(sum(yr7))+round(sum(yr8))+round(sum(yr9))+round(sum(yr10))) YR_TOTAL,
       decode(allocation_type, '1', 'PAYROLL', '2', 'PAYROLL', '3', 'PAYROLL', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
       decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '6','1','0') as "SUBTOTAL3",
	   decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '0') as "SUBTOTAL4",
	   (select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') budget_version,
		 round(sum(ayr1)) ayr1, round(sum(ayr2)) ayr2, round(sum(ayr3)) ayr3, round(sum(ayr4)) ayr4,
         round(sum(ayr5)) ayr5, round(sum(ayr6)) ayr6, round(sum(ayr7)) ayr7, round(sum(ayr8)) ayr8,
         round(sum(ayr9)) ayr9, round(sum(ayr10)) ayr10,
         (round(sum(ayr1))+round(sum(ayr2))+round(sum(ayr3))+round(sum(ayr4))+round(sum(ayr5))+round(sum(ayr6))+round(sum(ayr7))+round(sum(ayr8))+round(sum(ayr9))+round(sum(ayr10))) AYR_TOTAL,
		 (select min (case when control_id = 8 then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') end) from cr_alloc_system_control) y1,
		 (select min (case when control_id = 8 then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 1 end) from cr_alloc_system_control) y2,
		 (select min (case when control_id = 8 then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 2 end) from cr_alloc_system_control) y3,
		 (select min (case when control_id = 8 then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 3 end) from cr_alloc_system_control) y4,
		 (select min (case when control_id = 8 then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 4 end) from cr_alloc_system_control) y5,
		 (select min (case when control_id = 8 then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 5 end) from cr_alloc_system_control) y6,
		 (select min (case when control_id = 8 then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 6 end) from cr_alloc_system_control) y7,
		 (select min (case when control_id = 8 then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 7 end) from cr_alloc_system_control) y8,
		 (select min (case when control_id = 8 then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 8 end) from cr_alloc_system_control) y9,
		 (select min (case when control_id = 8 then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 9 end) from cr_alloc_system_control) y10
from
(
       select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
sum(a.amount) yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY')
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
    group by g.budget_value,
         g.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
sum(a.amount) yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 1
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
    group by g.budget_value,
         g.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
sum(a.amount) yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 2
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
    group by g.budget_value,
         g.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
sum(a.amount) yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
         cr_epe_charging_cost_center_mv h,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 3
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
sum(a.amount) yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 4
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
sum(a.amount) yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 5
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
sum(a.amount) yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 6
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
sum(a.amount) yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 7
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
sum(a.amount) yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 8
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
sum(a.amount) yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 9
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
		 union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
sum(a.amount) ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY')
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  and allocation_type = '7'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
sum(a.amount) ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 1
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  and allocation_type = '7'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
sum(a.amount) ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 2
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  and allocation_type = '7'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
sum(a.amount) ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 3
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  and allocation_type = '7'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
		 union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
sum(a.amount) ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 4
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  and allocation_type = '7'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
		 union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
sum(a.amount) ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 5
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  and allocation_type = '7'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
sum(a.amount) ayr7,
0 ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 6
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  and allocation_type = '7'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
sum(a.amount) ayr8,
0 ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 7
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  and allocation_type = '7'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
sum(a.amount) ayr9,
0 ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 8
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  and allocation_type = '7'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)
union
select g.budget_value project,
       g.description description,
       --cc.description,
       e.allocation_type,
	   e.budget_rollup_descr,
0 yr1,
0 yr2,
0 yr3,
0 yr4,
0 yr5,
0 yr6,
0 yr7,
0 yr8,
0 yr9,
0 yr10,
0 ayr1,
0 ayr2,
0 ayr3,
0 ayr4,
0 ayr5,
0 ayr6,
0 ayr7,
0 ayr8,
0 ayr9,
sum(a.amount) ayr10
from cr_budget_data_sv a,
     cr_epe_charging_exp_type_mv e,
	 cr_epe_account_mv f,
	 cr_epe_charging_cost_center_mv c,
	 cr_epe_project_mv g,
	 (select control_value from cr_alloc_system_control
	   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
	 (select min (case
            when control_id = 8
            then to_char(to_date(control_value, 'YYYYMM'), 'YYYY') + 9
            end) yr
            from cr_alloc_system_control) d
where a.charging_cost_center = c.charging_cost_center
  and a.charging_expense_type = e.charging_expense_type
  and a.ferc_account = f.account
  and a.project = g.project
  and a.budget_version = b.control_value
  and substr(a.month_number,1,4)= d.yr
  and upper(trim(c.element_type)) = 'BUDGET'
  and upper(trim(e.element_type)) = 'BUDGET'
  and upper(trim(f.element_type)) = 'BUDGET'
  and upper(trim(g.element_type)) = 'BUDGET'
  and f.acct_group = 'X'
  and allocation_type = '7'
    group by g.budget_value,
         g.description,
         --cc.description,
         e.allocation_type,
		 e.budget_rollup_descr,
		 substr(month_number,1,4)) s
		 group by s.project,
		 s.description,
         --s.description,
		 allocation_type,
		 budget_rollup_descr
 
 
 
 
 
 ;