CREATE OR REPLACE VIEW currency_rate_default_vw AS
SELECT
  currency_from
  , currency_to
  , exchange_date
  , next_date
  , actual_rate
  , estimate_rate
  , report_rate
  , average_rate
  , rn
  , Max(CASE WHEN SYSDATE BETWEEN exchange_date AND next_date THEN actual_rate ELSE NULL END) OVER (PARTITION BY currency_from, currency_to) AS current_rate
FROM
(
  SELECT
    currency_from
    , currency_to
    , Trunc(exchange_date, 'MONTH') exchange_date
    , nvl(Lead(Trunc(exchange_date,'MONTH')) OVER (PARTITION by currency_from, currency_to ORDER BY exchange_date), To_Date('999912', 'YYYYMM')) next_date
    , Nvl(Last_Value(actual_rate ignore nulls ) OVER (PARTITION BY currency_from, currency_to ORDER BY exchange_date), 0) AS actual_rate
    , Nvl(Last_Value(estimate_rate ignore nulls ) OVER (PARTITION BY currency_from, currency_to ORDER BY exchange_date), 0) AS estimate_rate
    , Nvl(Last_Value(report_rate ignore nulls ) OVER (PARTITION BY currency_from, currency_to ORDER BY exchange_date), 0) AS report_rate
    , Nvl(Last_Value(average_rate ignore nulls ) OVER (PARTITION BY currency_from, currency_to ORDER BY exchange_date), 0) AS average_rate
    , Row_Number() OVER (PARTITION BY currency_from, currency_to, TRUNC(exchange_date, 'MONTH') ORDER BY exchange_date DESC) rn
  FROM 
  (
    SELECT currency_from, currency_to, exchange_rate_type_id, exchange_date, rate
    FROM currency_rate_default
  ) 
  PIVOT 
  (
    Max(rate) rate FOR exchange_rate_type_id IN (1 AS actual, 2 AS estimate, 3 AS report, 4 AS average)
  )
)
WHERE rn = 1   
;

