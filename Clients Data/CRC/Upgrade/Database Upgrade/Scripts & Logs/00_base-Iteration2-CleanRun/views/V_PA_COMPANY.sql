
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."V_PA_COMPANY" ("COMPANY_ID", "DESCRIPTION", "SHORT_DESCRIPTION", "COMPANY_TYPE") AS 
  (
   SELECT company_setup.COMPANY_ID,
     company_setup.DESCRIPTION,
     company_setup.SHORT_DESCRIPTION,
     company_setup.COMPANY_TYPE
   FROM pwrplant.company_setup, pwrplant.pp_company_security
   WHERE company_setup.company_id = pp_company_security.company_id
     AND lower(users) = lower(USER)
     AND 1 <> (SELECT COUNT(*) FROM pwrplant.pp_company_security_temp WHERE rownum = 1)
	 AND company_setup.COMPANY_ID NOT IN (SELECT COMPANY_ID FROM PA_EXCLUDE_COMPANIES)
   UNION ALL
   SELECT pp_company_security_temp.COMPANY_ID,
     pp_company_security_temp.DESCRIPTION,
     pp_company_security_temp.SHORT_DESCRIPTION,
     pp_company_security_temp.COMPANY_TYPE
   FROM pwrplant.pp_company_security_temp
   WHERE COMPANY_ID NOT IN (SELECT COMPANY_ID FROM PA_EXCLUDE_COMPANIES)
   )
 
 ;