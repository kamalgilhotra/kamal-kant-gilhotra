
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_JUR_ALLO_VIEW" ("JUR_ALLO_TYPE_ID", "JURISDICTION_ID", "EFFECTIVE_DATE", "JUR_ALLO_PCT", "ENTITY_ID", "DAMPENING_IND", "NORMALIZATION_PCT", "FAS109_PCT", "TOTAL_GROSSUP_INCLUDE", "REG_IND", "MULT", "FAS109_IS_ZERO") AS 
  (
	select	jur_allo.jur_allo_type_id,
				jur_allo.jurisdiction_id,
				jur_allo.effective_date,
				jur_allo.jur_allo_pct,
				exceptions.entity_id,
				1 dampening_ind,
				nvl(exceptions.normalization_pct,jur_allo.normalization_pct),
				exceptions.fas109_pct,
				exceptions.total_grossup_include,
				nvl(exceptions.reg_ind,jur_allo.reg_ind),
				exceptions.mult,
				nvl(exceptions.fas109_is_zero,0) fas109_is_zero
	from		tax_accrual_jur_allo_type jur_allo_type,
				tax_accrual_jur_allo jur_allo,
				tax_accrual_jur_exceptions exceptions
	where	jur_allo_type.jur_allo_type_id = jur_allo.jur_allo_type_id
		and jur_allo.jur_allo_type_id = exceptions.jur_allo_type_id
		and jur_allo.jurisdiction_id = exceptions.jurisdiction_id
		and exceptions.effective_date =
		(	select max(inside.effective_date)
			from tax_accrual_jur_exceptions inside
			where inside.jurisdiction_id = exceptions.jurisdiction_id
				and inside.jur_allo_type_id = exceptions.jur_allo_type_id
				and inside.entity_id = exceptions.entity_id
				and inside.dampening_ind = exceptions.dampening_ind
				and inside.effective_date <= jur_allo.effective_date
		)
		and dampening_ind in (-1,1)
	union all
	select	jur_allo.jur_allo_type_id,
				jur_allo.jurisdiction_id,
				jur_allo.effective_date,
				jur_allo.jur_allo_pct,
				exceptions.entity_id,
				3 dampening_ind,
				nvl(exceptions.normalization_pct,jur_allo.normalization_pct),
				exceptions.fas109_pct,
				exceptions.total_grossup_include,
				nvl(exceptions.reg_ind,jur_allo.reg_ind),
				exceptions.mult,
				nvl(exceptions.fas109_is_zero,0) fas109_is_zero
	from		tax_accrual_jur_allo_type jur_allo_type,
				tax_accrual_jur_allo jur_allo,
				tax_accrual_jur_exceptions exceptions
	where	jur_allo_type.jur_allo_type_id = jur_allo.jur_allo_type_id
		and jur_allo.jur_allo_type_id = exceptions.jur_allo_type_id
		and jur_allo.jurisdiction_id = exceptions.jurisdiction_id
		and exceptions.effective_date =
		(	select max(inside.effective_date)
			from tax_accrual_jur_exceptions inside
			where inside.jurisdiction_id = exceptions.jurisdiction_id
				and inside.jur_allo_type_id = exceptions.jur_allo_type_id
				and inside.entity_id = exceptions.entity_id
				and inside.dampening_ind = exceptions.dampening_ind
				and inside.effective_date <= jur_allo.effective_date
		)
		and dampening_ind in (-1,3)
	union all
	select	jur_allo.jur_allo_type_id,
				jur_allo.jurisdiction_id,
				jur_allo.effective_date,
				jur_allo.jur_allo_pct,
				exceptions.entity_id,
				0 dampening_ind,
				nvl(exceptions.normalization_pct,jur_allo.normalization_pct),
				exceptions.fas109_pct,
				exceptions.total_grossup_include,
				nvl(exceptions.reg_ind,jur_allo.reg_ind),
				exceptions.mult,
				nvl(exceptions.fas109_is_zero,0) fas109_is_zero
	from		tax_accrual_jur_allo_type jur_allo_type,
				tax_accrual_jur_allo jur_allo,
				tax_accrual_jur_exceptions exceptions
	where	jur_allo_type.jur_allo_type_id = jur_allo.jur_allo_type_id
		and jur_allo.jur_allo_type_id = exceptions.jur_allo_type_id
		and jur_allo.jurisdiction_id = exceptions.jurisdiction_id
		and exceptions.effective_date =
		(	select max(inside.effective_date)
			from tax_accrual_jur_exceptions inside
			where inside.jurisdiction_id = exceptions.jurisdiction_id
				and inside.jur_allo_type_id = exceptions.jur_allo_type_id
				and inside.entity_id = exceptions.entity_id
				and inside.dampening_ind = exceptions.dampening_ind
				and inside.effective_date <= jur_allo.effective_date
		)
		and dampening_ind in (-1,0)
)
union all
(
	select	jur_allo.jur_allo_type_id,
				jur_allo.jurisdiction_id,
				jur_allo.effective_date,
				jur_allo.jur_allo_pct,
				entity.entity_id,
				dampening_ind.dampening_ind,
				jur_allo.normalization_pct,
				to_number(null) fas109_pct,
				0 total_grossup_include,
				jur_allo.reg_ind,
				1 mult,
				0 fas109_is_zero
	from		tax_accrual_jur_allo_type jur_allo_type,
				tax_accrual_jur_allo jur_allo,
				tax_accrual_entity entity,
				(	select distinct dampening_ind
					from tax_accrual_dampening_type
				) dampening_ind
	where	jur_allo_type.jur_allo_type_id = jur_allo.jur_allo_type_id
		and 	(	entity.pp_use_only is null or
					entity.pp_use_only = 0
				)
	minus
	(
		select	jur_allo.jur_allo_type_id,
					jur_allo.jurisdiction_id,
					jur_allo.effective_date,
					jur_allo.jur_allo_pct,
					exceptions.entity_id,
					1 dampening_ind,
					jur_allo.normalization_pct,
					to_number(null) fas109_pct,
					0 total_grossup_include,
					jur_allo.reg_ind,
					1 mult,
					0 fas109_is_zero
		from		tax_accrual_jur_allo_type jur_allo_type,
					tax_accrual_jur_allo jur_allo,
					tax_accrual_jur_exceptions exceptions
		where	jur_allo_type.jur_allo_type_id = jur_allo.jur_allo_type_id
			and jur_allo.jur_allo_type_id = exceptions.jur_allo_type_id
			and jur_allo.jurisdiction_id = exceptions.jurisdiction_id
			and exceptions.effective_date =
			(	select max(inside.effective_date)
				from tax_accrual_jur_exceptions inside
				where inside.jurisdiction_id = exceptions.jurisdiction_id
					and inside.jur_allo_type_id = exceptions.jur_allo_type_id
					and inside.entity_id = exceptions.entity_id
					and inside.dampening_ind = exceptions.dampening_ind
					and inside.effective_date <= jur_allo.effective_date
			)
			and exceptions.dampening_ind in (-1,1)
		union all
		select	jur_allo.jur_allo_type_id,
					jur_allo.jurisdiction_id,
					jur_allo.effective_date,
					jur_allo.jur_allo_pct,
					exceptions.entity_id,
					3 dampening_ind,
					jur_allo.normalization_pct,
					to_number(null) fas109_pct,
					0 total_grossup_include,
					jur_allo.reg_ind,
					1 mult,
					0 fas109_is_zero
		from		tax_accrual_jur_allo_type jur_allo_type,
					tax_accrual_jur_allo jur_allo,
					tax_accrual_jur_exceptions exceptions
		where	jur_allo_type.jur_allo_type_id = jur_allo.jur_allo_type_id
			and jur_allo.jur_allo_type_id = exceptions.jur_allo_type_id
			and jur_allo.jurisdiction_id = exceptions.jurisdiction_id
			and exceptions.effective_date =
			(	select max(inside.effective_date)
				from tax_accrual_jur_exceptions inside
				where inside.jurisdiction_id = exceptions.jurisdiction_id
					and inside.jur_allo_type_id = exceptions.jur_allo_type_id
					and inside.entity_id = exceptions.entity_id
					and inside.dampening_ind = exceptions.dampening_ind
					and inside.effective_date <= jur_allo.effective_date
			)
			and exceptions.dampening_ind in (-1,3)
		union all
		select	jur_allo.jur_allo_type_id,
					jur_allo.jurisdiction_id,
					jur_allo.effective_date,
					jur_allo.jur_allo_pct,
					exceptions.entity_id,
					0 dampening_ind,
					jur_allo.normalization_pct,
					to_number(null) fas109_pct,
					0 total_grossup_include,
					jur_allo.reg_ind,
					1 mult,
					0 fas109_is_zero
		from		tax_accrual_jur_allo_type jur_allo_type,
					tax_accrual_jur_allo jur_allo,
					tax_accrual_jur_exceptions exceptions
		where	jur_allo_type.jur_allo_type_id = jur_allo.jur_allo_type_id
			and jur_allo.jur_allo_type_id = exceptions.jur_allo_type_id
			and jur_allo.jurisdiction_id = exceptions.jurisdiction_id
			and exceptions.effective_date =
			(	select max(inside.effective_date)
				from tax_accrual_jur_exceptions inside
				where inside.jurisdiction_id = exceptions.jurisdiction_id
					and inside.jur_allo_type_id = exceptions.jur_allo_type_id
					and inside.entity_id = exceptions.entity_id
					and inside.dampening_ind = exceptions.dampening_ind
					and inside.effective_date <= jur_allo.effective_date
			)
			and exceptions.dampening_ind in (-1,0)
	)
)
 
 
 
 
 ;