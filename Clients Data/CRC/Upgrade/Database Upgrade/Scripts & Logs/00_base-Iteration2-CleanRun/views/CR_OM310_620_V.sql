
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CR_OM310_620_V" ("DIVISION", "DIV_DESCRIPTION", "ALLOCATION_TYPE", "ATYPE", "TTYPE", "SUBTOTAL3", "GL_EXPENSE_TYPE", "GL_EXP", "BUDGET_ROLLUP", "BUDGET_ROLLUP_DESCR", "ACTUALS_AMT", "DAA", "BUDGET_AMT", "DBA", "VARIANCE", "VARPERCENT", "ACTUALSYTD", "DIAY", "BUDGETYTD", "DBY", "VARIANCEYTD", "VARPERCENT2", "PROJECTED_ACTUALS", "DPA", "PROJECTED_BUDGET", "DPB", "PROJECTED_VARIANCE", "PROJECTED_VARPERCENT", "UNEXPENDED_BUDGET", "CURRENT_MONTH") AS 
  SELECT        DIVISION, DIV_DESCRIPTION,
            allocation_type,
            decode(allocation_type, '1', 'PAYROLL', '2', 'PAYROLL', '3', 'PAYROLL', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            decode(allocation_type, '1', '390 PAYROLL', '2', '390 PAYROLL', '3', '390 PAYROLL', '4', '399 LABOR RELATED', '5', 'NON LABOR', '6', '999 ALLOCATIONS') as "TTYPE",
            decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '0') as "SUBTOTAL3",
            decode(allocation_type, '5',gl_expense_type,null) gl_expense_type,
            gl_expenditure gl_exp,
            min(budget_rollup) budget_rollup,
            min(budget_rollup_descr) budget_rollup_descr,
            round(sum(actuals_amount)) actuals_amt,
            round(sum(daa)) daa,
            round(sum(budget_amount)) budget_amt,
            round(sum(dba)) dba,
            round(sum(actuals_amount)) - round(sum(budget_amount)) Variance,
            round(case when round(sum(budget_amount)) = 0
                        and round(sum(actuals_amount)) = 0 then 0
                        when round(sum(budget_amount)) = 0
                        and round(sum(actuals_amount)) != 0 then 100 * sign(sum(actuals_amount))
                        else (round((sum(actuals_amount)) - round(sum(budget_amount)))/round(sum(budget_amount)))*100
                              end,1) Varpercent,
            round(sum(actualsytd)) actualsytd,
            round(sum(diay)) diay,
            round(sum(budgetytd)) budgetytd,
            round(sum(dby)) dby,
            round(sum(actualsytd)) - round(sum(budgetytd)) VarianceYTD,
            round(case when round(sum(budgetytd)) = 0
                        and round(sum(actualsytd)) = 0 then 0
                        when round(sum(budgetytd)) = 0
                        and round(sum(actualsytd)) != 0 then 100 * sign(sum(actualsytd))
                        else (round((sum(actualsytd)) - round(sum(budgetytd)))/round(sum(budgetytd)))*100
                              end,1) Varpercent2,
/* PROJECTED */
            round(sum(projected_actuals)) projected_actuals,
            round(sum(dpa)) dpa,
            round(sum(projected_budget)) projected_budget,
            round(sum(dpb)) dpb,
            round(sum(projected_actuals)) - round(sum(projected_budget)) projected_variance,
            round(case when round(sum(projected_budget)) = 0
                        and round(sum(projected_actuals)) = 0 then 0
                        when round(sum(projected_budget)) = 0
                        and round(sum(projected_actuals)) != 0 then 100 * sign(sum(projected_actuals))
                        else (round((sum(projected_actuals)) - round(sum(projected_budget)))/round(sum(projected_budget)))*100
                              end,1) projected_varpercent,
            round(sum(projected_budget)) - round(sum(actualsytd)) Unexpended_Budget,
            min(to_char(to_date(control_value, 'YYYYMM'), 'fmMonth YYYY')) Current_Month
FROM cr_alloc_system_control,
(select     c.division,
            c.div_description,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            g.expense_type||' '||g.description gl_expense_type,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 daa,
            sum (a.amount*decode(c.division,'000',-1,1)) budget_amount,
            sum (a.amount) dba,
            0 actualsytd,
            0 diay,
            0 budgetytd,
            0 dby,
            0 projected_actuals,
            0 dpa,
            0 projected_budget,
            0 dpb
from         cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select control_value from cr_alloc_system_control
                   where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_expense_type_mv g
where     a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and a.month_number = d.control_value
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and upper(trim(g.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and e.gl_expenditure = g.expense_type
group     by    c.division,
            c.div_description,
            e.allocation_type,
            g.expense_type||' '||g.description,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr
union
select      c.division,
            c.div_description,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            g.expense_type||' '||g.description gl_expense_type,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr,
            sum (a.amount*decode(c.division,'000',-1,1)) actuals_amount,
            sum (a.amount) daa,
            0 budget_amount,
            0 dba,
            0 actualsytd,
            0 diay,
            0 budgetytd,
            0 dby,
            0 projected_actuals,
            0 dpa,
            0 projected_budget,
            0 dpb
from        cr_epe_charging_cost_center_mv c,
            cr_cost_repository_sv a,
                        (select control_value from cr_alloc_system_control
                   where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_expense_type_mv g
where    a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.account = f.account
            and a.month_number = d.control_value
            and upper(trim(c.element_type)) = 'ACTUALS'
            and upper(trim(e.element_type)) = 'ACTUALS'
            and upper(trim(f.element_type)) = 'ACTUALS'
            and upper(trim(g.element_type)) = 'ACTUALS'
            and f.acct_group between 'A' and 'E'
            and e.gl_expenditure = g.expense_type
group by    c.division,
            c.div_description,
            e.allocation_type,
            g.expense_type||' '||g.description,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr
union
select      c.division,
            c.div_description,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            g.expense_type||' '||g.description gl_expense_type,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 daa,
            0 budget_amount,
            0 dba,
            sum (a.amount*decode(c.division,'000',-1,1)) actualsytd,
            sum (a.amount) diay,
            0 budgetytd,
            0 dby,
            0 projected_actuals,
            0 dpa,
            0 projected_budget,
            0 dpb
from        cr_epe_charging_cost_center_mv c,
            cr_cost_repository_sv a,
            (select min (case
                         when control_id = 23
                         then control_value
                         end
                         ) as start_month,
                    min (case
                         when control_id = 24
                         then control_value
                         end
                         ) as end_month
              from cr_alloc_system_control
              )    d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_expense_type_mv g
where    a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.account = f.account
            and a.month_number between d.start_month and d.end_month
            and upper(trim(c.element_type)) = 'ACTUALS'
            and upper(trim(e.element_type)) = 'ACTUALS'
            and upper(trim(f.element_type)) = 'ACTUALS'
            and upper(trim(g.element_type)) = 'ACTUALS'
            and f.acct_group between 'A' and 'E'
            and e.gl_expenditure = g.expense_type
group     by    c.division,
            c.div_description,
            e.allocation_type,
            g.expense_type||' '||g.description,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr
union
select      c.division,
            c.div_description,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            g.expense_type||' '||g.description gl_expense_type,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 daa,
            0 budget_amount,
            0 dba,
            0 actualsytd,
            0 diay,
            sum (a.amount*decode(c.division,'000',-1,1)) budgetytd,
			sum(a.amount) dby,
            0 projected_actuals,
            0 dpa,
            0 projected_budget,
            0 dpb
from        cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select min (case
                         when control_id = 23
                         then control_value
                         end
                         ) as start_month,
                    min (case
                         when control_id = 24
                         then control_value
                         end
                         ) as end_month
              from cr_alloc_system_control
              )    d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_expense_type_mv g
where     a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and a.month_number between d.start_month and d.end_month
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and upper(trim(g.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and e.gl_expenditure = g.expense_type
group     by    c.division,
            c.div_description,
            e.allocation_type,
            g.expense_type||' '||g.description,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr
union
select      c.division,
            c.div_description,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            g.expense_type||' '||g.description gl_expense_type,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 daa,
            0 budget_amount,
            0 dba,
            0 actualsytd,
            0 diay,
            0 budgetytd,
            0 dby,
            sum (a.amount*decode(c.division,'000',-1,1)) projected_actuals,
			sum(a.amount) dpa,
            0 projected_budget,
            0 dpb
from        cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            cr_alloc_system_control b,
            cr_alloc_system_control d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_expense_type_mv g
where       a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and upper(trim(b.control_name)) = 'REVISED BUDGET VERSION'
            and substr(a.month_number,1,4) = substr(d.control_value,1,4)
            and d.control_id = 5
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and upper(trim(g.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and e.gl_expenditure = g.expense_type
group     by c.division, c.div_description, e.allocation_type,
             g.expense_type||' '||g.description,e.gl_expenditure,
             e.budget_rollup, e.budget_rollup_descr
union
select      c.division,
            c.div_description,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            g.expense_type||' '||g.description gl_expense_type,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 daa,
            0 budget_amount,
            0 dba,
            0 actualsytd,
            0 diay,
            0 budgetytd,
            0 dby,
            0 projected_actuals,
            0 dpa,
            sum (a.amount*decode(c.division,'000',-1,1)) projected_budget,
			sum(a.amount) dpb
from        cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select min (case
                        when control_id = 5
                        then substr(control_value,1,4)
                        end
                        )as allyear
                        from cr_alloc_system_control) d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_expense_type_mv g
where     a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and substr(a.month_number,1,4) = d.allyear
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and upper(trim(g.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and e.gl_expenditure = g.expense_type
group     by c.division,
             c.div_description,
            e.allocation_type,
            g.expense_type||' '||g.description,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr)
WHERE allocation_type = 1 and control_id = 5 and division = '620'
GROUP BY    division,
            div_description,
            allocation_type, gl_expense_type, gl_expenditure
HAVING abs(sum(nvl(actuals_amount,0)) + sum(nvl(budget_amount,0)))>.49
   or abs(sum(nvl(actualsytd,0)) + sum(nvl(budgetytd,0))) > .49
   or abs(sum(projected_budget) - sum(actualsytd)) > .49
UNION
SELECT        division,
            div_description,
            allocation_type,
            decode(allocation_type, '1', 'PAYROLL', '6', 'ALLOCATIONS', '3', 'PAYROLL', '4', 'LABOR RELATED', '5', 'NON LABOR', '2', 'PAYROLL') as "ALLOCATION TYPE",
            decode(allocation_type, '1', '390 PAYROLL', '2', '390 PAYROLL', '3', '390 PAYROLL', '4', '399 LABOR RELATED', '5', 'NON LABOR', '6', '999 ALLOCATIONS') as "TTYPE",
            decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '0') as "SUBTOTAL3",
            decode(allocation_type, '5',gl_expense_type,null) gl_expense_type,
            gl_expenditure gl_exp,
            budget_rollup,
            budget_rollup_descr,
            round(sum(actuals_amount)) actuals_amt,
            round(sum(daa)) daa,
            round(sum(budget_amount)) budget_amt,
            round(sum(dba)) dba,
            round(sum(actuals_amount)) - round(sum(budget_amount)) Variance,
            round(case when round(sum(budget_amount)) = 0
                        and round(sum(actuals_amount)) = 0 then 0
                        when round(sum(budget_amount)) = 0
                        and round(sum(actuals_amount)) != 0 then 100 * sign(sum(actuals_amount))
                        else (round((sum(actuals_amount)) - round(sum(budget_amount)))/round(sum(budget_amount)))*100
                              end,1) Varpercent,
            round(sum(actualsytd)) actualsytd,
            round(sum(diay)) diay,
            round(sum(budgetytd)) budgetytd,
            round(sum(dby)) dby,
            round(sum(actualsytd)) - round(sum(budgetytd)) VarianceYTD,
            round(case when round(sum(budgetytd)) = 0
                        and round(sum(actualsytd)) = 0 then 0
                        when round(sum(budgetytd)) = 0
                        and round(sum(actualsytd)) != 0 then 100 * sign(sum(actualsytd))
                        else (round((sum(actualsytd)) - round(sum(budgetytd)))/round(sum(budgetytd)))*100
                              end,1) Varpercent2,
/* PROJECTED */
            round(sum(projected_actuals)) projected_actuals,
            round(sum(dpa)) dpa,
            round(sum(projected_budget)) projected_budget,
            round(sum(dpb)) dpb,
            round(sum(projected_actuals)) - round(sum(projected_budget)) projected_variance,
            round(case when round(sum(projected_budget)) = 0
                        and round(sum(projected_actuals)) = 0 then 0
                        when round(sum(projected_budget)) = 0
                        and round(sum(projected_actuals)) != 0 then 100 * sign(sum(projected_actuals))
                        else (round((sum(projected_actuals)) - round(sum(projected_budget)))/round(sum(projected_budget)))*100
                              end,1) projected_varpercent,
            round(sum(projected_budget)) - round(sum(actualsytd)) Unexpended_Budget,
			(select min (case
            when control_id = 5
            then to_char(to_date(control_value, 'YYYYMM'), 'fmMonth YYYY')
            end)
            from cr_alloc_system_control) Current_Month
FROM (
select      c.division,
            c.div_description,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            g.expense_type||' '||g.description gl_expense_type,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 daa,
            sum (a.amount*decode(c.division,'000',-1,1)) budget_amount,
            sum (a.amount) dba,
            0 actualsytd,
            0 diay,
            0 budgetytd,
            0 dby,
            0 projected_actuals,
            0 dpa,
            0 projected_budget,
            0 dpb
from         cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select control_value from cr_alloc_system_control
                   where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_expense_type_mv g
where     a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and a.month_number = d.control_value
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and upper(trim(g.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and e.gl_expenditure = g.expense_type
group     by    c.division,
            c.div_description,
            e.allocation_type,
            g.expense_type||' '||g.description,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr
union
select      c.division,
            c.div_description,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            g.expense_type||' '||g.description gl_expense_type,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr,
            sum (a.amount*decode(c.division,'000',-1,1)) actuals_amount,
            sum (a.amount) daa,
            0 budget_amount,
            0 dba,
            0 actualsytd,
            0 diay,
            0 budgetytd,
            0 dby,
            0 projected_actuals,
            0 dpa,
            0 projected_budget,
            0 dpb
from         cr_epe_charging_cost_center_mv c,
            cr_cost_repository_sv a,
                        (select control_value from cr_alloc_system_control
                   where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_expense_type_mv g
where    a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.account = f.account
            and a.month_number = d.control_value
            and upper(trim(c.element_type)) = 'ACTUALS'
            and upper(trim(e.element_type)) = 'ACTUALS'
            and upper(trim(f.element_type)) = 'ACTUALS'
            and upper(trim(g.element_type)) = 'ACTUALS'
            and f.acct_group between 'A' and 'E'
            and e.gl_expenditure = g.expense_type
group by    c.division,
            c.div_description,
            e.allocation_type,
            g.expense_type||' '||g.description,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr
union
select      c.division,
            c.div_description,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            g.expense_type||' '||g.description gl_expense_type,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 daa,
            0 budget_amount,
            0 dba,
            sum (a.amount*decode(c.division,'000',-1,1)) actualsytd,
            sum (a.amount) diay,
            0 budgetytd,
            0 dby,
            0 projected_actuals,
            0 dpa,
            0 projected_budget,
            0 dpb
from         cr_epe_charging_cost_center_mv c,
            cr_cost_repository_sv a,
            (select min (case
                         when control_id = 23
                         then control_value
                         end
                         ) as start_month,
                    min (case
                         when control_id = 24
                         then control_value
                         end
                         ) as end_month
              from cr_alloc_system_control
              )    d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_expense_type_mv g
where    a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.account = f.account
            and a.month_number between d.start_month and d.end_month
            and upper(trim(c.element_type)) = 'ACTUALS'
            and upper(trim(e.element_type)) = 'ACTUALS'
            and upper(trim(f.element_type)) = 'ACTUALS'
            and upper(trim(g.element_type)) = 'ACTUALS'
            and f.acct_group between 'A' and 'E'
            and e.gl_expenditure = g.expense_type
group     by    c.division,
            c.div_description,
            e.allocation_type,
            g.expense_type||' '||g.description,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr
union
select      c.division,
            c.div_description,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            g.expense_type||' '||g.description gl_expense_type,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 daa,
            0 budget_amount,
            0 dba,
            0 actualsytd,
            0 diay,
            sum (a.amount*decode(c.division,'000',-1,1)) budgetytd,
			sum(a.amount) dby,
            0 projected_actuals,
            0 dpa,
            0 projected_budget,
            0 dpb
from         cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select min (case
                         when control_id = 23
                         then control_value
                         end
                         ) as start_month,
                    min (case
                         when control_id = 24
                         then control_value
                         end
                         ) as end_month
              from cr_alloc_system_control
              )    d,
             cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_expense_type_mv g
where     a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and a.month_number between d.start_month and d.end_month
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and upper(trim(g.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and e.gl_expenditure = g.expense_type
group     by    c.division,
            c.div_description,
            e.allocation_type,
            g.expense_type||' '||g.description,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr
union
select      c.division,
            c.div_description,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            g.expense_type||' '||g.description gl_expense_type,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 daa,
            0 budget_amount,
            0 dba,
            0 actualsytd,
            0 diay,
            0 budgetytd,
            0 dby,
            sum (a.amount*decode(c.division,'000',-1,1)) projected_actuals,
			sum(a.amount) dpa,
            0 projected_budget,
            0 dpb
from        cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            cr_alloc_system_control b,
            cr_alloc_system_control d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_expense_type_mv g
where       a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and upper(trim(b.control_name)) = 'REVISED BUDGET VERSION'
            and substr(a.month_number,1,4) = substr(d.control_value,1,4)
            and d.control_id = 5
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and upper(trim(g.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and e.gl_expenditure = g.expense_type
group     by c.division, c.div_description, e.allocation_type, e.gl_expenditure,
             g.expense_type||' '||g.description,
             e.budget_rollup, e.budget_rollup_descr
union
select      c.division,
            c.div_description,
            e.allocation_type,
            decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS') as "ATYPE",
            g.expense_type||' '||g.description gl_expense_type,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr,
            0 actuals_amount,
            0 daa,
            0 budget_amount,
            0 dba,
            0 actualsytd,
            0 diay,
            0 budgetytd,
            0 dby,
            0 projected_actuals,
            0 dpa,
            sum (a.amount*decode(c.division,'000',-1,1)) projected_budget,
			sum(a.amount) dpb
from         cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select min (case
                        when control_id = 5
                        then substr(control_value,1,4)
                        end
                        )as allyear
                        from cr_alloc_system_control) d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_expense_type_mv g
where     a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.budget_version = b.control_value
            and substr(a.month_number,1,4) = d.allyear
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and upper(trim(g.element_type)) = 'BUDGET'
            and f.acct_group between 'A' and 'E'
            and e.gl_expenditure = g.expense_type
group     by    c.division,
            c.div_description,
            e.allocation_type,
            g.expense_type||' '||g.description,
            e.gl_expenditure,
            e.budget_rollup,
            e.budget_rollup_descr)
WHERE allocation_type IN (2,3,4,5,6) and division = '620'
GROUP BY    division,
            div_description,
            allocation_type, gl_expense_type, gl_expenditure,
            budget_rollup,
            budget_rollup_descr
HAVING abs(sum(nvl(actuals_amount,0)) + sum(nvl(budget_amount,0)))>.49
   or abs(sum(nvl(actualsytd,0)) + sum(nvl(budgetytd,0))) > .49
   or abs(sum(projected_budget) - sum(actualsytd)) > .49
order by 1,2,3

 
 
 
 
 
 ;