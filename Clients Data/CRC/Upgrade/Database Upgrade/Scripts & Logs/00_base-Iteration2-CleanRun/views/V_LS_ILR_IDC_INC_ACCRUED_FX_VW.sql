CREATE OR replace VIEW v_ls_ilr_idc_inc_accrued_fx_vw
AS
WITH ilr_info AS
	 (SELECT stg.ilr_id, stg.revision, appr.revision appr_revision,
       decode(sign(stg.revision),
               -1,
               npv_start_date,
               curr.remeasurement_date) remeasurement_date,
       i.est_in_svc_date,
       Decode(curr_cap.fasb_cap_type_id, 4, 1, 5, 1, 0) curr_is_om,
       Decode(appr_cap.fasb_cap_type_id, 4, 1, 5, 1, 0) appr_is_om,
       stg.set_of_books_id, npv_start_date
  FROM ls_ilr i, ls_ilr_options curr, ls_ilr_options appr, ls_ilr_stg stg,
       ls_fasb_cap_type_sob_map curr_cap, ls_fasb_cap_type_sob_map appr_cap
 WHERE i.ilr_id = curr.ilr_id
   AND curr.ilr_id = stg.ilr_id
   AND curr.revision = stg.revision
   AND i.current_revision = appr.revision
   AND i.ilr_id = appr.ilr_id
   AND curr.lease_cap_type_id = curr_cap.lease_cap_type_id
   AND stg.set_of_books_id = curr_cap.set_of_books_id
   AND appr.lease_cap_type_id = appr_cap.lease_cap_type_id
   AND stg.set_of_books_id = appr_cap.set_of_books_id
),
	num_months AS
	 (SELECT ilr.ilr_id,
			 ilr.set_of_books_id,
			 COUNT(MONTH) num_months
		FROM ls_ilr_schedule sched,
			 ilr_info        ilr
	   WHERE sched.ilr_id = ilr.ilr_id
		 AND sched.revision = ilr.appr_revision
		 AND sched.set_of_books_id = ilr.set_of_books_id
	   GROUP BY ilr.ilr_id,
				ilr.set_of_books_id),
    incentive_subtract as 
     (select ilr.ilr_id, inc.revision,  sum(case when lr.est_in_svc_date >= inc.date_incurred then 0 else inc.amount end) subtract_inc
          from LS_ILR_INCENTIVE inc, ls_ilr_stg ilr, ls_ilr lr
          where ilr.ilr_id = inc.ilr_id
          and ilr.ilr_id = lr.ilr_id
          and ilr.set_of_books_id = 1
          and inc.date_incurred < ilr.npv_start_date
          group by ilr.ilr_id, inc.revision)
	SELECT i.ilr_id ilr_id,
		   i.revision revision,
		   i.set_of_books_id set_of_books_id,
		   i.npv_start_date,
		   n.num_months,
           (idc_stg.initial_direct_cost/n.num_months)*( months_between(remeasurement_date, MIN(s.month))) accrued_idc,
		   SUM(incentive_amount ) - nvl(in_sub.subtract_inc,0) accrued_incentive,
		   months_between(remeasurement_date, MIN(s.month)) months_already_accrued,
		   i.remeasurement_date,
		   i.curr_is_om curr_is_om,
		   i.appr_is_om approved_is_om,
		   nvl(decode(i.appr_is_om, 1, decode(i.curr_is_om, 0, 1), 0),0) switch_to_cap --whether or not we are converting cap types on the schedule being calculated
	  FROM ls_ilr_schedule s,
		   ilr_info        i,
		   num_months      n,
           incentive_subtract in_sub,
           LS_ILR_IDC_STG idc_stg
	 WHERE s.ilr_id = i.ilr_id
	   AND s.revision = i.appr_revision
       AND in_sub.ilr_id(+) = i.ilr_id
       AND in_sub.revision(+) = i.appr_revision 
	   AND in_sub.revision(+) = i.appr_revision
       and idc_stg.ilr_id(+) = i.ilr_id
       and idc_stg.revision(+) = i.revision
	   AND s.set_of_books_id = i.set_of_books_id
	   AND n.ilr_id = i.ilr_id
	   AND n.set_of_books_id = i.set_of_books_id
	   AND s.month < i.npv_start_date
	   AND i.npv_start_date is not null -- Make sure to filter out Sets of Books in ILR_STG that have no NPV Start Date Which means no IDC Incentive Accrual
	 GROUP BY i.ilr_id,
			  i.revision,
			  i.set_of_books_id,
			  i.remeasurement_date,
			  i.curr_is_om,
			  i.appr_is_om,
			  npv_start_date,
			  n.num_months,
              in_sub.subtract_inc,
			  idc_stg.initial_direct_cost
	UNION
	SELECT DISTINCT ilr_id,
					revision,
					set_of_books_id,
					npv_start_date,
					0               num_months,
					0               accrued_idc,
					0               accrued_incentive,
					NULL            months_already_accrued,
					NULL            remeasurement_date,
					0               curr_is_om,
					0               approved_is_om,
					0               switch_to_cap
	  FROM ls_ilr_stg
	 WHERE npv_start_date IS NULL;