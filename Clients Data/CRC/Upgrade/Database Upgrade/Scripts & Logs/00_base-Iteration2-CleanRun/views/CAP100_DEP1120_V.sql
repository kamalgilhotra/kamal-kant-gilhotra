
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CAP100_DEP1120_V" ("PROJECT", "DESCRIPTION", "ALLOCATION_TYPE", "ATYPE", "SUBTOTAL3", "SUBTOTAL4", "AFUDC", "BUDGET_ROLLUP", "BUDGET_ROLLUP_DESCR", "ACTUALS_AMT", "BUDGET_AMT", "VARIANCE", "VARPERCENT", "ACTUALSYTD", "BUDGETYTD", "VARIANCEYTD", "VARPERCENT2", "PROJECTED_BUDGET", "AACTUALS_AMT", "ABUDGET_AMT", "AACTUALSYTD", "ABUDGETYTD", "APROJECTED_BUDGET", "UNEXPENDED_BUDGET", "AUNEXPENDED_BUDGET", "CURRENT_MONTH") AS 
  SELECT	project,
             description,
			allocation_type,
			decode(allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '6','1','0') as "SUBTOTAL3",
			decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '0') as "SUBTOTAL4",
			decode(allocation_type, '7','1','0') as "AFUDC",
			min(budget_rollup) budget_rollup,
			min(budget_rollup_descr) budget_rollup_descr,
			round(sum(actuals_amount)) actuals_amt,
			round(sum(budget_amount)) budget_amt,
			round(sum(actuals_amount)) - round(sum(budget_amount)) Variance,
			round(case when round(sum(budget_amount)) = 0
			            and round(sum(actuals_amount)) = 0 then 0
						when round(sum(budget_amount)) = 0
			            and round(sum(actuals_amount)) != 0 then 100 * sign(sum(actuals_amount))
						else (round((sum(actuals_amount)) - round(sum(budget_amount)))/round(sum(budget_amount)))*100
                              end,1) Varpercent,
			round(sum(actualsytd)) actualsytd,
			round(sum(budgetytd)) budgetytd,
			round(sum(actualsytd)) - round(sum(budgetytd)) VarianceYTD,
			round(case when round(sum(budgetytd)) = 0
			            and round(sum(actualsytd)) = 0 then 0
						when round(sum(budgetytd)) = 0
			            and round(sum(actualsytd)) != 0 then 100 * sign(sum(actualsytd))
						else (round((sum(actualsytd)) - round(sum(budgetytd)))/round(sum(budgetytd)))*100
                              end,1) Varpercent2,
			round(sum(projected_budget)) projected_budget,
			round(sum(aactuals_amount)) aactuals_amt,
			round(sum(abudget_amount)) abudget_amt,
			round(sum(aactualsytd)) aactualsytd,
			round(sum(abudgetytd)) abudgetytd,
			round(sum(aprojected_budget)) aprojected_budget,
			round(sum(projected_budget)) - round(sum(actualsytd)) Unexpended_Budget,
			round(sum(aprojected_budget)) - round(sum(aactualsytd)) aUnexpended_Budget,
			(select min (case
            when control_id = 5
            then to_char(to_date(control_value, 'YYYYMM'), 'fmMonth YYYY')
            end)
            from cr_alloc_system_control) Current_Month
FROM  pwrplant.pm_epe_reportable_projects_mv,
(select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			sum(a.amount) budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c, cr_epe_charging_cost_center_mv h,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select control_value from cr_alloc_system_control
		           where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
             cr_epe_project_mv g
where 	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.ferc_account = f.account
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
            and a.project = g.project
			and a.budget_version = b.control_value
			and a.month_number = d.control_value
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
			and f.acct_group = 'X'
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by	 g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			sum(a.amount) actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			cr_cost_repository_sv a,
                        (select control_value from cr_alloc_system_control
		           where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
              cr_epe_project_mv g
where	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.account = f.account
             and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.month_number = d.control_value
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and upper(trim(f.element_type)) = 'ACTUALS'
              and upper(trim(g.element_type)) = 'ACTUALS'
			and f.acct_group = 'X'
             and substr(a.gl_journal_category,1,4) not in ( '7936', '7937')
			--and (to_number(a.gaap) between 5110 and 5390 or a.gaap = '5499')
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group by g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			sum(a.amount) actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			cr_cost_repository_sv a,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
              cr_epe_project_mv g
where	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.account = f.account
             and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.month_number between d.start_month and d.end_month
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and upper(trim(f.element_type)) = 'ACTUALS'
             and upper(trim(g.element_type)) = 'ACTUALS'
			and f.acct_group = 'X'
             and substr(a.gl_journal_category,1,4) not in ( '7936', '7937')
			--and (to_number(a.gaap) between 5110 and 5390 or a.gaap = '5499')
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by	 g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			sum(a.amount) budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			 cr_epe_charging_exp_type_mv e,
			 cr_epe_account_mv f,
              cr_epe_project_mv g
where 	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.ferc_account = f.account
              and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.budget_version = b.control_value
			and a.month_number between d.start_month and d.end_month
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
			and f.acct_group = 'X'
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			sum(a.amount) projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
					     when control_id = 8
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 9
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
             cr_epe_project_mv g
where 	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.ferc_account = f.account
             and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.budget_version = b.control_value
			and a.month_number between d.start_month and d.end_month
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
			and f.acct_group = 'X'
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by  g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			sum(a.amount) abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select control_value from cr_alloc_system_control
		           where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
             cr_epe_project_mv g
where 	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.ferc_account = f.account
            --and c.charging_cost_center = g.owning_department
            and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.budget_version = b.control_value
			and a.month_number = d.control_value
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
			and f.acct_group = 'X'
			and allocation_type = '7'
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by	 g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			sum(a.amount) aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			cr_cost_repository_sv a,
                        (select control_value from cr_alloc_system_control
		           where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
              cr_epe_project_mv g
where	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.account = f.account
             and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.month_number = d.control_value
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and upper(trim(f.element_type)) = 'ACTUALS'
              and upper(trim(g.element_type)) = 'ACTUALS'
			and f.acct_group = 'X'
             and substr(a.gl_journal_category,1,4) not in ( '7936', '7937')
			 and allocation_type = '7'
			--and (to_number(a.gaap) between 5110 and 5390 or a.gaap = '5499')
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group by g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			sum(a.amount) aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			cr_cost_repository_sv a,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
              cr_epe_project_mv g
where	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.account = f.account
             and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.month_number between d.start_month and d.end_month
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and upper(trim(f.element_type)) = 'ACTUALS'
             and upper(trim(g.element_type)) = 'ACTUALS'
			and f.acct_group = 'X'
             and substr(a.gl_journal_category,1,4) not in ( '7936', '7937')
			 and allocation_type = '7'
			--and (to_number(a.gaap) between 5110 and 5390 or a.gaap = '5499')
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by	 g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			sum(a.amount) abudgetytd,
			0 aprojected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			 cr_epe_charging_exp_type_mv e,
			 cr_epe_account_mv f,
              cr_epe_project_mv g
where 	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.ferc_account = f.account
              and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.budget_version = b.control_value
			and a.month_number between d.start_month and d.end_month
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
			and f.acct_group = 'X'
			and allocation_type = '7'
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			sum(a.amount) aprojected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
					     when control_id = 8
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 9
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
             cr_epe_project_mv g
where 	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.ferc_account = f.account
             and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.budget_version = b.control_value
			and a.month_number between d.start_month and d.end_month
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
			and f.acct_group = 'X'
			and allocation_type = '7'
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by  g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
) main_sql
WHERE allocation_type = 1
and project = mv_project
GROUP BY project,
               description,
			 allocation_type
HAVING (abs(sum(nvl(actuals_amount,0)) + sum(nvl(budget_amount,0)))>.49
   or abs(sum(nvl(actualsytd,0)) + sum(nvl(budgetytd,0))) > .49
   or abs(sum(projected_budget) - sum(actualsytd)) > .49)
UNION
SELECT	project,
             description,
			allocation_type,
			decode(allocation_type, '1', 'PAYROLL', '2', 'PAYROLL', '3', 'PAYROLL', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '6','1','0') as "SUBTOTAL3",
			decode(allocation_type, '1', '1', '2', '1', '3', '1','4','1','5','1', '0') as "SUBTOTAL4",
			decode(allocation_type, '7','1','0') as "AFUDC",
			budget_rollup,
			budget_rollup_descr,
			round(sum(actuals_amount)) actuals_amt,
			round(sum(budget_amount)) budget_amt,
			round(sum(actuals_amount)) - round(sum(budget_amount)) Variance,
			round(case when round(sum(budget_amount)) = 0
			            and round(sum(actuals_amount)) = 0 then 0
						when round(sum(budget_amount)) = 0
			            and round(sum(actuals_amount)) != 0 then 100 * sign(sum(actuals_amount))
						else (round((sum(actuals_amount)) - round(sum(budget_amount)))/round(sum(budget_amount)))*100
                              end,1) Varpercent,
			round(sum(actualsytd)) actualsytd,
			round(sum(budgetytd)) budgetytd,
			round(sum(actualsytd)) - round(sum(budgetytd)) VarianceYTD,
			round(case when round(sum(budgetytd)) = 0
			            and round(sum(actualsytd)) = 0 then 0
						when round(sum(budgetytd)) = 0
			            and round(sum(actualsytd)) != 0 then 100 * sign(sum(actualsytd))
						else (round((sum(actualsytd)) - round(sum(budgetytd)))/round(sum(budgetytd)))*100
                              end,1) Varpercent2,
			round(sum(projected_budget)) projected_budget,
			round(sum(aactuals_amount)) aactuals_amt,
			round(sum(abudget_amount)) abudget_amt,
			round(sum(aactualsytd)) aactualsytd,
			round(sum(abudgetytd)) abudgetytd,
			round(sum(aprojected_budget)) aprojected_budget,
			round(sum(projected_budget)) - round(sum(actualsytd)) Unexpended_Budget,
			round(sum(aprojected_budget)) - round(sum(aactualsytd)) aUnexpended_Budget,
			(select min (case
            when control_id = 5
            then to_char(to_date(control_value, 'YYYYMM'), 'fmMonth YYYY')
            end)
            from cr_alloc_system_control) Current_Month
FROM  pwrplant.pm_epe_reportable_projects_mv,
(select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			sum(a.amount) budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select control_value from cr_alloc_system_control
		           where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
             cr_epe_project_mv g
where 	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.ferc_account = f.account
            --and c.charging_cost_center = g.owning_department
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
            and a.project = g.project
			and a.budget_version = b.control_value
			and a.month_number = d.control_value
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
			and f.acct_group = 'X'
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by	 g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			sum(a.amount) actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			cr_cost_repository_sv a,
                        (select control_value from cr_alloc_system_control
		           where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
              cr_epe_project_mv g
where	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.account = f.account
             and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.month_number = d.control_value
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and upper(trim(f.element_type)) = 'ACTUALS'
              and upper(trim(g.element_type)) = 'ACTUALS'
			and f.acct_group = 'X'
             and substr(a.gl_journal_category,1,4) not in ( '7936', '7937')
			--and (to_number(a.gaap) between 5110 and 5390 or a.gaap = '5499')
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group by g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			sum(a.amount) actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			cr_cost_repository_sv a,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
              cr_epe_project_mv g
where	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.account = f.account
             and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.month_number between d.start_month and d.end_month
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and upper(trim(f.element_type)) = 'ACTUALS'
             and upper(trim(g.element_type)) = 'ACTUALS'
			and f.acct_group = 'X'
             and substr(a.gl_journal_category,1,4) not in ( '7936', '7937')
			--and (to_number(a.gaap) between 5110 and 5390 or a.gaap = '5499')
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by	 g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			sum(a.amount) budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			 cr_epe_charging_exp_type_mv e,
			 cr_epe_account_mv f,
              cr_epe_project_mv g
where 	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.ferc_account = f.account
              and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.budget_version = b.control_value
			and a.month_number between d.start_month and d.end_month
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
			and f.acct_group = 'X'
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			sum(a.amount) projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
					     when control_id = 8
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 9
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
             cr_epe_project_mv g
where 	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.ferc_account = f.account
             and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.budget_version = b.control_value
			and a.month_number between d.start_month and d.end_month
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
			and f.acct_group = 'X'
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by  g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			sum(a.amount) abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select control_value from cr_alloc_system_control
		           where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
             cr_epe_project_mv g
where 	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.ferc_account = f.account
            --and c.charging_cost_center = g.owning_department
            and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.budget_version = b.control_value
			and a.month_number = d.control_value
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
			and f.acct_group = 'X'
			and allocation_type = '7'
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by	 g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			sum(a.amount) aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			cr_cost_repository_sv a,
                        (select control_value from cr_alloc_system_control
		           where upper(trim(control_name)) = 'CURRENT OPEN MONTH') d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
              cr_epe_project_mv g
where	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.account = f.account
             and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.month_number = d.control_value
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and upper(trim(f.element_type)) = 'ACTUALS'
              and upper(trim(g.element_type)) = 'ACTUALS'
			and f.acct_group = 'X'
             and substr(a.gl_journal_category,1,4) not in ( '7936', '7937')
			 and allocation_type = '7'
			--and (to_number(a.gaap) between 5110 and 5390 or a.gaap = '5499')
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group by g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			sum(a.amount) aactualsytd,
			0 abudgetytd,
			0 aprojected_budget
from 		cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			cr_cost_repository_sv a,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
              cr_epe_project_mv g
where	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.account = f.account
             and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.month_number between d.start_month and d.end_month
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'ACTUALS'
			and upper(trim(e.element_type)) = 'ACTUALS'
			and upper(trim(f.element_type)) = 'ACTUALS'
             and upper(trim(g.element_type)) = 'ACTUALS'
			and f.acct_group = 'X'
             and substr(a.gl_journal_category,1,4) not in ( '7936', '7937')
			 and allocation_type = '7'
			--and (to_number(a.gaap) between 5110 and 5390 or a.gaap = '5499')
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by	 g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			sum(a.amount) abudgetytd,
			0 aprojected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
					     when control_id = 23
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 24
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			 cr_epe_charging_exp_type_mv e,
			 cr_epe_account_mv f,
              cr_epe_project_mv g
where 	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.ferc_account = f.account
              and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.budget_version = b.control_value
			and a.month_number between d.start_month and d.end_month
			--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
			and f.acct_group = 'X'
			and allocation_type = '7'
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr
union
select 	g.budget_value project,
             g.description description,
			e.allocation_type,
			decode(e.allocation_type, '1', 'PAYROLL', '2', 'OVERTIME', '3', 'BONUSES', '4', 'LABOR RELATED', '5', 'NON LABOR', '6', 'ALLOCATIONS', '7', 'AFUDC') as "ATYPE",
			e.budget_rollup,
			e.budget_rollup_descr,
			0 actuals_amount,
			0 budget_amount,
			0 actualsytd,
			0 budgetytd,
			0 projected_budget,
			0 aactuals_amount,
			0 abudget_amount,
			0 aactualsytd,
			0 abudgetytd,
			sum(a.amount) aprojected_budget
from 		cr_budget_data_sv a,
			cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
			(select control_value from cr_alloc_system_control
			   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
			(select min (case
					     when control_id = 8
						 then control_value
						 end
						 ) as start_month,
					min (case
						 when control_id = 9
						 then control_value
						 end
						 ) as end_month
			  from cr_alloc_system_control
			  )	d,
			cr_epe_charging_exp_type_mv e,
			cr_epe_account_mv f,
             cr_epe_project_mv g
where 	a.charging_cost_center = c.charging_cost_center
            and a.charging_expense_type = e.charging_expense_type
			and a.ferc_account = f.account
             and a.project = g.project
            and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
			and a.budget_version = b.control_value
			and a.month_number between d.start_month and d.end_month
			and upper(trim(c.element_type)) = 'BUDGET'
			and upper(trim(e.element_type)) = 'BUDGET'
			and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
			and f.acct_group = 'X'
			and allocation_type = '7'
			/*and trim(a.charging_cost_center) in (select trim(budget_value)
				from cr_budget_users_valid_values
				where trim(lower(users)) = trim(lower(user)))*/
group	 by  g.budget_value,
               g.description,
			 e.allocation_type,
			 e.budget_rollup,
			 e.budget_rollup_descr) main_sql
WHERE allocation_type IN (2,3,4,5,6,7)
  and       project = mv_project
            and (exists (select 	1
						from 		cr_budget_data_sv a,
									cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
									(select control_value from cr_alloc_system_control
									   where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
									(select min (case
											     when control_id = 23
												 then control_value
												 end
												 ) as start_month,
											min (case
												 when control_id = 24
												 then control_value
												 end
												 ) as end_month
									  from cr_alloc_system_control
									  )	d,
									cr_epe_charging_exp_type_mv e,
									cr_epe_account_mv f,
						             cr_epe_project_mv g
						where 	a.charging_cost_center = c.charging_cost_center
						            and a.charging_expense_type = e.charging_expense_type
									and a.ferc_account = f.account
						             and a.project = g.project
                                     and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
									and a.budget_version = b.control_value
									and a.month_number between d.start_month and d.end_month
									and upper(trim(c.element_type)) = 'BUDGET'
									and upper(trim(e.element_type)) = 'BUDGET'
									and upper(trim(f.element_type)) = 'BUDGET'
						             and upper(trim(g.element_type)) = 'BUDGET'
									and f.acct_group = 'X'
									and e.allocation_type <> 7
									and main_sql.project = a.project)
							or exists (select 	1
								  from 		cr_epe_charging_cost_center_mv c,cr_epe_charging_cost_center_mv h,
											cr_cost_repository_sv a,
											(select min (case
													     when control_id = 23
														 then control_value
														 end
														 ) as start_month,
													min (case
														 when control_id = 24
														 then control_value
														 end
														 ) as end_month
											  from cr_alloc_system_control
											  )	d,
											cr_epe_charging_exp_type_mv e,
											cr_epe_account_mv f,
								              cr_epe_project_mv g
								where	a.charging_cost_center = c.charging_cost_center
								            and a.charging_expense_type = e.charging_expense_type
											and a.account = f.account
								             and a.project = g.project
                                             and g.owning_department = h.charging_cost_center
            and h.department = '1120' and upper(trim(h.element_type)) = 'BUDGET'
  			                                 and a.month_number between d.start_month and d.end_month
											--and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
											and upper(trim(c.element_type)) = 'ACTUALS'
											and upper(trim(e.element_type)) = 'ACTUALS'
											and upper(trim(f.element_type)) = 'ACTUALS'
								             and upper(trim(g.element_type)) = 'ACTUALS'
											and f.acct_group = 'X'
								             and substr(a.gl_journal_category,1,4) not in ( '7936', '7937')
									and e.allocation_type <> 7
									and main_sql.project = a.project   ))
GROUP BY project,
               description,
			allocation_type,
              budget_rollup,
              budget_rollup_descr
HAVING (abs(sum(nvl(actuals_amount,0)) + sum(nvl(budget_amount,0)))>.49
   or abs(sum(nvl(actualsytd,0)) + sum(nvl(budgetytd,0))) > .49
   or abs(sum(projected_budget) - sum(actualsytd)) > .49)
order by 1,2,3
 
 
 
 
 
 ;