
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ACCRUAL_VERSION_EFF_DATE" ("TA_VERSION_ID", "GL_MONTH", "PROCESS_OPTION_ID", "PRIOR_TA_VERSION_ID", "PRIOR_GL_MONTH", "TAX_MONTH_LOCK", "YTD_INCLUDE", "OVERRIDE_EFF_DATE", "EFFECTIVE_DATE_FAS109", "DIT_PROCESS_OPTION_ID", "RTP_MONTH", "TAX_YEAR", "GL_MONTH_DESCRIPTION", "BUDGET_VERSION", "JE_SCHEMA_ID", "ACTIVITY_TYPE_ID", "EFFECTIVE_DATE", "EFFECTIVE_DATE_ALLOC", "CR_PULL_MONTH_TYPE_ID", "CR_POST_MONTH_TYPE_ID", "BEG_BAL_IND", "YTD_DEFTAX", "NON_OVERRIDE_DATE", "MONTH_TYPE_ID") AS 
  select	a.ta_version_id,
			a.gl_month,
			b.activity_type_id process_option_id,
			a.prior_ta_version_id prior_ta_version_id,
			a.prior_gl_month prior_gl_month,
			a.tax_month_lock tax_month_lock,
			decode(b.activity_type_id,1,1,3,1,0) ytd_include,
			a.override_eff_date_rate override_eff_date,
			nvl(a.override_eff_date_rate, to_date((d.year + c.year_adjust) * 100 + c.calendar_month,'yyyymm')) fas109_eff_date,
			decode(b.activity_type_id,1,1,3,1,2) dit_process_option_id,
			decode(a.period,13,1,0) rtp_month,
			tax_year,
			month_description,
			gl_value1 budget_version,
			je_schema_id,
			activity_type_id,
			nvl(a.override_eff_date_rate, to_date((d.year + c.year_adjust) * 100 + c.calendar_month,'yyyymm')) effective_date,
			nvl(a.override_eff_date_alloc, to_date((d.year + c.year_adjust) * 100 + c.calendar_month,'yyyymm')) effective_date_alloc,
			decode(d.ta_version_type_id,1,1,2) cr_pull_month_type_id,
			decode(d.ta_version_type_id,1,1,2) cr_post_month_type_id,
			b.beg_bal_ind,
			decode(b.activity_type_id,2,2,1) ytd_deftax,
			nvl(a.override_eff_date_rate, to_date((d.year + c.year_adjust) * 100 + c.calendar_month,'yyyymm')) non_override_date,
			a.month_type_id
from	tax_accrual_gl_month_def a,
		tax_accrual_month_type b,
		tax_accrual_period c,
		tax_accrual_version d
where a.ta_version_id = d.ta_version_id
	and a.month_type_id = b.month_type_id
	and a.period = c.period
	and c.ta_version_type_id = d.ta_version_type_id
 
 
 
 
 ;