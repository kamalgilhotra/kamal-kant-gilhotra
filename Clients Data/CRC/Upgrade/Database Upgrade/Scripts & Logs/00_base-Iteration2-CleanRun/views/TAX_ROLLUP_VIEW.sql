
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_ROLLUP_VIEW" ("TAX_CLASS_ID", "DESCRIPTION", "TAX_ROLLUP_ID", "ROLLUP_DESCRIPTION") AS 
  ((    		select tax_class_rollups.tax_class_id,  				 tax_rollup_detail.description ,   				 tax_rollup_detail.tax_rollup_id,  				 tax_rollup.description rollup_description   		  from tax_class_rollups,  				 tax_rollup_detail,  				 tax_rollup   		 where tax_class_rollups.tax_rollup_detail_id = tax_Rollup_detail.tax_rollup_detail_id   			and tax_rollup_detail.tax_rollup_id = tax_rollup.tax_rollup_id   		  		 union   		  		select inside.tax_class_id, 		  				 'Unclassified',  				 inside.tax_rollup_id,  				 tax_rollup.description   		  from tax_rollup,   				(select tax_class_id,   						  tax_rollup_id  					from tax_class,  						  tax_rollup   				  minus   				 select tax_class_id,  						  tax_rollup_id  					from tax_rollup_detail,  						  tax_class_rollups   				  where tax_rollup_detail.tax_Rollup_detail_id =  						  tax_class_rollups.tax_rollup_detail_id) inside   		  		where tax_rollup.tax_rollup_id = inside.tax_rollup_id  		  and tax_rollup.tax_rollup_id > 0)    		 union    	   select tax_class_id,  				 description,  				 0,  				 'Tax Class'  		  from tax_class    		 union    	   select tax_class_id,  				 'Total Tax Classes',  				 -1,  				 'Total Tax Classes'   		  from tax_class )
 
 
 
 
 
 ;