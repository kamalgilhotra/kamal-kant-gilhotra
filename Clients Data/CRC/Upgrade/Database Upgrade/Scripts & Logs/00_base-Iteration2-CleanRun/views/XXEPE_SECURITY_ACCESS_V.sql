
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."XXEPE_SECURITY_ACCESS_V" ("USER_ID", "OBJECT_TYPE", "OBJECT_NAME", "USER_STATUS") AS 
  select u.users user_id, 'GROUPS' OBJECT, groups pwp_object, s.description user_status
from pwrplant.pp_security_users_groups g, pwrplant.pp_security_users u, pwrplant.pp_security_user_status s
where u.users = trim(g.users)
and u.status = s.status_id
union
select u.users, 'COMPANIES' OBJECT, decode(sum(c.company_id),746,'SHS',264, 'T&D', 186, 'GEN', 14,'MIR','UNK'), s.description
from pwrplant.pp_company_security c, pwrplant.pp_security_users u, pwrplant.pp_security_user_status s
where u.users = c.USERS
and u.status = s.status_id
group by u.users,s.description
union
select u.users, 'COBS' OBJECT, decode(sum(cr.valid_values),440703,'SHS',205467, 'T&D', 110256, 'GEN', 1407, 'MIR','UNK') cobs,s.description
from pwrplant.cr_company_security cr, pwrplant.pp_security_users u, pwrplant.pp_security_user_status s
where u.users = cr.USERS
and u.status = s.status_id
group by u.users, s.description
union
select nvl(u.users, p.grantee) USERS, 'ROLES' OBJECT, p.granted_role, nvl(s.description, 'NO USER') DESCRIPTION
from sys.dba_role_privs p, pwrplant.pp_security_users u, pwrplant.pp_security_user_status s
where upper(u.users(+)) = p.grantee
and u.status = s.status_id(+)
order by 4, 1, 2, 3
 
 
 
 
 
 ;