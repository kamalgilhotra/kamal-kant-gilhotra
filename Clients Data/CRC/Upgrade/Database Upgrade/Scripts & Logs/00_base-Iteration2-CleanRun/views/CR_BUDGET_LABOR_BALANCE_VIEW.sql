
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CR_BUDGET_LABOR_BALANCE_VIEW" ("DEPARTMENT", "FUNDING_PROJECT", "CR_BUDGET_VERSION_ID", "YEAR", "TOTAL_LABOR_AMT", "JAN_LABOR_AMT", "FEB_LABOR_AMT", "MAR_LABOR_AMT", "APR_LABOR_AMT", "MAY_LABOR_AMT", "JUN_LABOR_AMT", "JUL_LABOR_AMT", "AUG_LABOR_AMT", "SEP_LABOR_AMT", "OCT_LABOR_AMT", "NOV_LABOR_AMT", "DEC_LABOR_AMT", "TOTAL_CAP_AMT", "JAN_CAP_AMT", "FEB_CAP_AMT", "MAR_CAP_AMT", "APR_CAP_AMT", "MAY_CAP_AMT", "JUN_CAP_AMT", "JUL_CAP_AMT", "AUG_CAP_AMT", "SEP_CAP_AMT", "OCT_CAP_AMT", "NOV_CAP_AMT", "DEC_CAP_AMT", "TOTAL_LABOR_HRS", "JAN_LABOR_HRS", "FEB_LABOR_HRS", "MAR_LABOR_HRS", "APR_LABOR_HRS", "MAY_LABOR_HRS", "JUN_LABOR_HRS", "JUL_LABOR_HRS", "AUG_LABOR_HRS", "SEP_LABOR_HRS", "OCT_LABOR_HRS", "NOV_LABOR_HRS", "DEC_LABOR_HRS", "TOTAL_CAP_HRS", "JAN_CAP_HRS", "FEB_CAP_HRS", "MAR_CAP_HRS", "APR_CAP_HRS", "MAY_CAP_HRS", "JUN_CAP_HRS", "JUL_CAP_HRS", "AUG_CAP_HRS", "SEP_CAP_HRS", "OCT_CAP_HRS", "NOV_CAP_HRS", "DEC_CAP_HRS", "TOTAL_CAP_QTY", "JAN_CAP_QTY", "FEB_CAP_QTY", "MAR_CAP_QTY", "APR_CAP_QTY", "MAY_CAP_QTY", "JUN_CAP_QTY", "JUL_CAP_QTY", "AUG_CAP_QTY", "SEP_CAP_QTY", "OCT_CAP_QTY", "NOV_CAP_QTY", "DEC_CAP_QTY", "LABOR_TYPE", "REMAINING_AMT", "REMAINING_HRS") AS 
  (
select department, funding_project, cr_budget_version_id, year,
       total_labor_amt,
       jan_labor_amt, feb_labor_amt, mar_labor_amt, apr_labor_amt, may_labor_amt, jun_labor_amt,
       jul_labor_amt, aug_labor_amt, sep_labor_amt, oct_labor_amt, nov_labor_amt, dec_labor_amt,
       total_cap_amt,
       jan_cap_amt, feb_cap_amt, mar_cap_amt, apr_cap_amt, may_cap_amt, jun_cap_amt,
       jul_cap_amt, aug_cap_amt, sep_cap_amt, oct_cap_amt, nov_cap_amt, dec_cap_amt,
       total_labor_hrs,
       jan_labor_hrs, feb_labor_hrs, mar_labor_hrs, apr_labor_hrs, may_labor_hrs, jun_labor_hrs,
       jul_labor_hrs, aug_labor_hrs, sep_labor_hrs, oct_labor_hrs, nov_labor_hrs, dec_labor_hrs,
       total_cap_hrs,
       jan_cap_hrs, feb_cap_hrs, mar_cap_hrs, apr_cap_hrs, may_cap_hrs, jun_cap_hrs,
       jul_cap_hrs, aug_cap_hrs, sep_cap_hrs, oct_cap_hrs, nov_cap_hrs, dec_cap_hrs,
       total_cap_qty,
       jan_cap_qty, feb_cap_qty, mar_cap_qty, apr_cap_qty, may_cap_qty, jun_cap_qty,
       jul_cap_qty, aug_cap_qty, sep_cap_qty, oct_cap_qty, nov_cap_qty, dec_cap_qty,
       labor_type,
       (total_labor_amt - total_cap_amt) remaining_amt,
       (total_labor_hrs - total_cap_hrs) remaining_hrs
  from (
    select department, funding_project, cr_budget_version_id, year,
           sum(jan_labor_amt+feb_labor_amt+mar_labor_amt+apr_labor_amt+may_labor_amt+jun_labor_amt+jul_labor_amt+aug_labor_amt+sep_labor_amt+oct_labor_amt+nov_labor_amt+dec_labor_amt) total_labor_amt,
           sum(jan_labor_amt) jan_labor_amt, sum(feb_labor_amt) feb_labor_amt,
           sum(mar_labor_amt) mar_labor_amt, sum(apr_labor_amt) apr_labor_amt,
           sum(may_labor_amt) may_labor_amt, sum(jun_labor_amt) jun_labor_amt,
           sum(jul_labor_amt) jul_labor_amt, sum(aug_labor_amt) aug_labor_amt,
           sum(sep_labor_amt) sep_labor_amt, sum(oct_labor_amt) oct_labor_amt,
           sum(nov_labor_amt) nov_labor_amt, sum(dec_labor_amt) dec_labor_amt,
           sum(jan_cap_amt+feb_cap_amt+mar_cap_amt+apr_cap_amt+may_cap_amt+jun_cap_amt+jul_cap_amt+aug_cap_amt+sep_cap_amt+oct_cap_amt+nov_cap_amt+dec_cap_amt) total_cap_amt,
           sum(jan_cap_amt) jan_cap_amt, sum(feb_cap_amt) feb_cap_amt,
           sum(mar_cap_amt) mar_cap_amt, sum(apr_cap_amt) apr_cap_amt,
           sum(may_cap_amt) may_cap_amt, sum(jun_cap_amt) jun_cap_amt,
           sum(jul_cap_amt) jul_cap_amt, sum(aug_cap_amt) aug_cap_amt,
           sum(sep_cap_amt) sep_cap_amt, sum(oct_cap_amt) oct_cap_amt,
           sum(nov_cap_amt) nov_cap_amt, sum(dec_cap_amt) dec_cap_amt,
           sum(jan_labor_hrs+feb_labor_hrs+mar_labor_hrs+apr_labor_hrs+may_labor_hrs+jun_labor_hrs+jul_labor_hrs+aug_labor_hrs+sep_labor_hrs+oct_labor_hrs+nov_labor_hrs+dec_labor_hrs) total_labor_hrs,
           sum(jan_labor_hrs) jan_labor_hrs, sum(feb_labor_hrs) feb_labor_hrs,
           sum(mar_labor_hrs) mar_labor_hrs, sum(apr_labor_hrs) apr_labor_hrs,
           sum(may_labor_hrs) may_labor_hrs, sum(jun_labor_hrs) jun_labor_hrs,
           sum(jul_labor_hrs) jul_labor_hrs, sum(aug_labor_hrs) aug_labor_hrs,
           sum(sep_labor_hrs) sep_labor_hrs, sum(oct_labor_hrs) oct_labor_hrs,
           sum(nov_labor_hrs) nov_labor_hrs, sum(dec_labor_hrs) dec_labor_hrs,
           sum(jan_cap_hrs+feb_cap_hrs+mar_cap_hrs+apr_cap_hrs+may_cap_hrs+jun_cap_hrs+jul_cap_hrs+aug_cap_hrs+sep_cap_hrs+oct_cap_hrs+nov_cap_hrs+dec_cap_hrs) total_cap_hrs,
           sum(jan_cap_hrs) jan_cap_hrs, sum(feb_cap_hrs) feb_cap_hrs,
           sum(mar_cap_hrs) mar_cap_hrs, sum(apr_cap_hrs) apr_cap_hrs,
           sum(may_cap_hrs) may_cap_hrs, sum(jun_cap_hrs) jun_cap_hrs,
           sum(jul_cap_hrs) jul_cap_hrs, sum(aug_cap_hrs) aug_cap_hrs,
           sum(sep_cap_hrs) sep_cap_hrs, sum(oct_cap_hrs) oct_cap_hrs,
           sum(nov_cap_hrs) nov_cap_hrs, sum(dec_cap_hrs) dec_cap_hrs,
           sum(jan_cap_qty+feb_cap_qty+mar_cap_qty+apr_cap_qty+may_cap_qty+jun_cap_qty+jul_cap_qty+aug_cap_qty+sep_cap_qty+oct_cap_qty+nov_cap_qty+dec_cap_qty) total_cap_qty,
           sum(jan_cap_qty) jan_cap_qty, sum(feb_cap_qty) feb_cap_qty,
           sum(mar_cap_qty) mar_cap_qty, sum(apr_cap_qty) apr_cap_qty,
           sum(may_cap_qty) may_cap_qty, sum(jun_cap_qty) jun_cap_qty,
           sum(jul_cap_qty) jul_cap_qty, sum(aug_cap_qty) aug_cap_qty,
           sum(sep_cap_qty) sep_cap_qty, sum(oct_cap_qty) oct_cap_qty,
           sum(nov_cap_qty) nov_cap_qty, sum(dec_cap_qty) dec_cap_qty,
           ' ' labor_type
      from (
           select e.charging_cost_center department, e.project funding_project, e.cr_budget_version_id cr_budget_version_id, e.year year,
                  nvl(e.jan,0) jan_labor_amt, nvl(e.feb,0) feb_labor_amt, nvl(e.mar,0) mar_labor_amt, nvl(e.apr,0) apr_labor_amt, nvl(e.may,0) may_labor_amt,
                  nvl(e.jun,0) jun_labor_amt, nvl(e.jul,0) jul_labor_amt, nvl(e.aug,0) aug_labor_amt, nvl(e.sep,0) sep_labor_amt, nvl(e.oct,0) oct_labor_amt,
                  nvl(e.nov,0) nov_labor_amt, nvl(e.dec,0) dec_labor_amt,
                  0 jan_cap_amt, 0 feb_cap_amt, 0 mar_cap_amt, 0 apr_cap_amt, 0 may_cap_amt, 0 jun_cap_amt,
                  0 jul_cap_amt, 0 aug_cap_amt, 0 sep_cap_amt, 0 oct_cap_amt, 0 nov_cap_amt, 0 dec_cap_amt,
                  0 jan_labor_hrs, 0 feb_labor_hrs, 0 mar_labor_hrs, 0 apr_labor_hrs, 0 may_labor_hrs, 0 jun_labor_hrs,
                  0 jul_labor_hrs, 0 aug_labor_hrs, 0 sep_labor_hrs, 0 oct_labor_hrs, 0 nov_labor_hrs, 0 dec_labor_hrs,
                  0 jan_cap_hrs, 0 feb_cap_hrs, 0 mar_cap_hrs, 0 apr_cap_hrs, 0 may_cap_hrs, 0 jun_cap_hrs,
                  0 jul_cap_hrs, 0 aug_cap_hrs, 0 sep_cap_hrs, 0 oct_cap_hrs, 0 nov_cap_hrs, 0 dec_cap_hrs,
                  0 jan_cap_qty, 0 feb_cap_qty, 0 mar_cap_qty, 0 apr_cap_qty, 0 may_cap_qty, 0 jun_cap_qty,
                  0 jul_cap_qty, 0 aug_cap_qty, 0 sep_cap_qty, 0 oct_cap_qty, 0 nov_cap_qty, 0 dec_cap_qty
             from cr_budget_data_entry e
            where e."TYPE" = 'Amt'
           union all
           select e.charging_cost_center, e.project, e.cr_budget_version_id, e.year,
                  0,0,0,0,0,0,0,0,0,0,0,0,
                  0,0,0,0,0,0,0,0,0,0,0,0,
                  nvl(e.jan,0), nvl(e.feb,0), nvl(e.mar,0), nvl(e.apr,0), nvl(e.may,0),
                  nvl(e.jun,0), nvl(e.jul,0), nvl(e.aug,0), nvl(e.sep,0), nvl(e.oct,0),
                  nvl(e.nov,0), nvl(e.dec,0),
                  0,0,0,0,0,0,0,0,0,0,0,0,
                  0,0,0,0,0,0,0,0,0,0,0,0
             from cr_budget_data_entry e
            where e."TYPE" = 'Qty'
           union all
           select c.department, c.funding_project, c.cr_budget_version_id, c.year,
                  0,0,0,0,0,0,0,0,0,0,0,0,
                  nvl(c.jan_amt,0), nvl(c.feb_amt,0), nvl(c.mar_amt,0), nvl(c.apr_amt,0), nvl(c.may_amt,0),
                  nvl(c.jun_amt,0), nvl(c.jul_amt,0), nvl(c.aug_amt,0), nvl(c.sep_amt,0), nvl(c.oct_amt,0),
                  nvl(c.nov_amt,0), nvl(c.dec_amt,0),
                  0,0,0,0,0,0,0,0,0,0,0,0,
                  nvl(c.jan_hrs,0), nvl(c.feb_hrs,0), nvl(c.mar_hrs,0), nvl(c.apr_hrs,0), nvl(c.may_hrs,0),
                  nvl(c.jun_hrs,0), nvl(c.jul_hrs,0), nvl(c.aug_hrs,0), nvl(c.sep_hrs,0), nvl(c.oct_hrs,0),
                  nvl(c.nov_hrs,0), nvl(c.dec_hrs,0),
                  nvl(c.jan_qty,0), nvl(c.feb_qty,0), nvl(c.mar_qty,0), nvl(c.apr_qty,0), nvl(c.may_qty,0),
                  nvl(c.jun_qty,0), nvl(c.jul_qty,0), nvl(c.aug_qty,0), nvl(c.sep_qty,0), nvl(c.oct_qty,0),
                  nvl(c.nov_qty,0), nvl(c.dec_qty,0)
             from cr_budget_labor_cap_dollars c
           )
     group by department, funding_project, cr_budget_version_id, year
    )
)
 
 
 
 ;