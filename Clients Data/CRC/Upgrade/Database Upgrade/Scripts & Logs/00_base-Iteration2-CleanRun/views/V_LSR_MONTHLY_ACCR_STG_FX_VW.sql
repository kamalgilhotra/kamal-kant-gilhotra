CREATE OR REPLACE VIEW V_LSR_MONTHLY_ACCR_STG_FX_VW AS
WITH
cur AS (
  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
    contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
  FROM currency contract_cur
  UNION
  SELECT 2, company_cur.currency_id,
    company_cur.currency_display_symbol, company_cur.iso_code, 0
  FROM currency company_cur
)
    SELECT
    acr.accrual_id,
    acr.accrual_type_id,
    acr.ilr_id,
    gl_posting_mo_yr,
    acr.set_of_books_id,
    acr.amount * Decode(ls_cur_type, 2, cr.rate, 1) amount,
    cur.ls_cur_type ls_cur_type,
    cr.exchange_date,
    lease.contract_currency_id,
    cur.currency_id display_currency_id,
    cs.currency_id company_currency_id,
    cur.iso_code,
    cur.currency_display_symbol,
    Decode(ls_cur_type, 2, cr.rate, 1) rate
FROM lsr_monthly_accrual_stg acr
INNER JOIN lsr_ilr ilr on acr.ilr_id = ilr.ilr_id
INNER JOIN currency_schema cs  ON ilr.company_id = cs.company_id
INNER JOIN lsr_lease lease  ON ilr.lease_id = lease.lease_id
INNER JOIN cur  ON (cur.ls_cur_type = 1
    AND cur.currency_id = lease.contract_currency_id)
    OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
left outer join ls_lease_calculated_date_rates cr ON lease.contract_currency_id = cr.contract_currency_id
  AND cur.currency_id = cr.company_currency_id
  AND ilr.company_id = cr.company_id
  AND acr.gl_posting_mo_yr = cr.accounting_month
inner join pp_system_control_companies sc ON ilr.company_id = sc.company_id
WHERE lower(trim(sc.control_name)) = 'lease mc: use average rates' 
and Nvl(cr.exchange_rate_type_id, decode(lower(sc.control_value), 'yes', 4, 1)) = decode(lower(sc.control_value), 'yes', 4, 1)
AND cs.currency_type_id = 1;