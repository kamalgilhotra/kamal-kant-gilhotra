
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_EST_RETIREMENT_VIEW" ("WORK_ORDER_ID", "REVISION", "RETIREMENT") AS 
  (select  work_order_id, revision, sum(retirement) retirement from
 ( select   a.work_order_id, nvl(a.revision, 1) revision,
nvl(sum(b.total), 0) retirement
from     wo_est_first_view a, wo_est_monthly b
where    a.work_order_id = b.work_order_id (+)              and
   a.revision = b.revision (+)               and
   b.expenditure_type_id (+) = 2                and
   b.est_chg_type_id (+) is null
group by a.work_order_id, a.revision
union
    select  a.work_order_id, a.revision, sum(b.total)
from     wo_est_first_view a, wo_est_monthly b
where    a.work_order_id = b.work_order_id (+)              and
   a.revision = b.revision (+)               and
   b.expenditure_type_id (+) = 2                and
   b.est_chg_type_id =
(  select   c.est_chg_type_id
from     estimate_charge_type c
where    c.processing_type_id = 1
and
   b.est_chg_type_id = c.est_chg_type_id                    )
         group by    a.work_order_id, a.revision         )
group by work_order_id, revision)

 
 
 
 
 
 ;