CREATE OR REPLACE VIEW V_LS_MONTHLY_ACCRUAL_STG_FX AS
WITH cur
     AS (SELECT 1                                    ls_cur_type,
                contract_cur.currency_id             AS currency_id,
                contract_cur.currency_display_symbol currency_display_symbol,
                contract_cur.iso_code                iso_code
         FROM   currency contract_cur
         UNION
         SELECT 2,
                company_cur.currency_id,
                company_cur.currency_display_symbol,
                company_cur.iso_code
         FROM   currency company_cur)
SELECT lmas.accrual_id,
       lmas.accrual_type_id,
       lmas.ls_asset_id,
       lmas.amount * Decode(ls_cur_type, 2, Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg_rate.rate, cr.rate), 1) amount,
       lmas.gl_posting_mo_yr,
       lmas.set_of_books_id,
       la.contract_currency_id,
       cs.currency_id                       company_currency_id,
       cur.ls_cur_type                      AS ls_cur_type,
       Decode(lower(sc.CONTROL_VALUE), 'yes', cr_avg_rate.exchange_date, cr.exchange_date) exchange_date,
       Decode(ls_cur_type, 2, cr.rate,
                           1)               rate,
       Decode(ls_cur_type, 2, decode(lower(sc.control_value), 'yes', cr_avg_rate.rate, cr.rate),
                           1)               average_rate,
       cur.iso_code,
       cur.currency_display_symbol
FROM   ls_monthly_accrual_stg lmas
       inner join ls_asset la
               ON lmas.ls_asset_id = la.ls_asset_id
       inner join currency_schema cs
               ON la.company_id = cs.company_id
       inner join cur
               ON ( ( cur.ls_cur_type = 1
                      AND cur.currency_id = la.contract_currency_id )
                     OR ( cur.ls_cur_type = 2
                          AND cur.currency_id = cs.currency_id ) )
       inner join ls_lease_calculated_date_rates cr
               ON ( cr.company_id = la.company_id
                    AND cr.contract_currency_id = la.contract_currency_id
                    AND cr.company_currency_id = cs.currency_id
                    AND cr.accounting_month = lmas.gl_posting_mo_yr 
                    AND Nvl(cr.exchange_rate_type_id, 1) = 1)
       left outer join ls_lease_calculated_date_rates cr_avg_rate
               ON ( cr_avg_rate.company_id = la.company_id
                    AND cr_avg_rate.contract_currency_id = la.contract_currency_id
                    AND cr_avg_rate.company_currency_id = cs.currency_id
                    AND cr_avg_rate.accounting_month = lmas.gl_posting_mo_yr 
                    and Nvl(cr_avg_rate.exchange_rate_type_id, 4) = 4)
       inner join pp_system_control_companies sc
                 ON la.company_id = sc.company_id AND 
                    lower(trim(sc.control_name)) = 'lease mc: use average rates'                     
WHERE cs.currency_type_id = 1
