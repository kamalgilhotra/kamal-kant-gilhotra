
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."EVERY_PROJECT_EVERY_REVISION" ("AMOUNT", "WORK_ORDER_ID", "REVISION", "BUDGET_VERSION_ID") AS 
  ( select sum(amount) amount, work_order_id,revision,budget_version_id from
   (select  0 amount, woc.work_order_id work_order_id, woa.revision revision,
bv.budget_version_id budget_version_id
from     work_order_control woc, work_order_approval woa, budget_version bv
where    woc.work_order_id = woa.work_order_id     and
   woc.funding_wo_indicator = 1
   union
   select   sum(total), d.work_order_id, fp.revision, budget_version_id
from     budget_version_fund_proj fp, wo_est_monthly d
where    d.work_order_id = fp.work_order_id     and
   d.revision     =  fp.revision       and   fp.active      = 1
group by d.work_order_id,fp.revision,budget_version_id )
group by work_order_id,revision,budget_version_id)

 
 
 
 
 
 ;