/*
||============================================================================
|| Application: PowerPlan
|| File Name: REPAIR_MONTH_VIEW.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 08/08/2017 Eric Berger    Updating the datawindow values in
||                                      reporting tables to reflect the new
||                                      tax repairs standard.
||============================================================================
*/

CREATE OR REPLACE VIEW REPAIR_MONTH_VIEW AS
   SELECT
       TAX_YEAR,
       START_MONTH,
       END_MONTH,
       DECODE(TAX_EXPENSE_PRIOR_MONTH,
              0,
              START_MONTH,
              TAX_EXPENSE_PRIOR_MONTH) TAX_EXPENSE_PRIOR_MONTH,
       DECODE(TAX_EXPENSE_PRIOR_MONTH_DISP,
              0,
              START_MONTH,
              TAX_EXPENSE_PRIOR_MONTH_DISP) TAX_EXPENSE_PRIOR_MONTH_DISP,
       DECODE(TAX_EXPENSE_FUTURE_MONTH,
              0,
              END_MONTH,
              TAX_EXPENSE_FUTURE_MONTH) TAX_EXPENSE_FUTURE_MONTH,
       TAX_EXPENSE_PRIOR_MONTH PRIOR_MONTHS,
       TAX_EXPENSE_FUTURE_MONTH FUTURE_MONTHS,
       TO_CHAR(ADD_MONTHS(TO_DATE(END_MONTH, 'YYYYMM'), 1), 'YYYYMM') FUTURE_START,
       'Exclude Tax Year ' || to_char(TAX_YEAR - 1) || ' Entries Posted From :   ' ||   case when TAX_EXPENSE_PRIOR_MONTH = 0 then '(No Value Set)' else  start_month  || '  to  ' || tax_expense_prior_month_disp end as exclude_text,
       'Include Tax Year '  || to_char(TAX_YEAR ) || ' Entries Posted From :   ' ||  case when  TAX_EXPENSE_FUTURE_MONTH  = 0 then '(No Value Set)' else TO_CHAR(ADD_MONTHS(TO_DATE(END_MONTH, 'YYYYMM'), 1), 'YYYYMM')  || '  to  ' ||  DECODE(TAX_EXPENSE_FUTURE_MONTH, 0, END_MONTH, TAX_EXPENSE_FUTURE_MONTH) end as include_text,
       'Tax Year : '  || to_char(TAX_YEAR ) || '   ( From ' ||  start_month  || ' to ' ||  end_month || ' )' as tax_year_display
FROM (
SELECT tax_year,
       sum(START_MONTH) START_MONTH,
       sum(END_MONTH) END_MONTH,
       sum(TAX_EXPENSE_PRIOR_MONTH) TAX_EXPENSE_PRIOR_MONTH,
       sum(TAX_EXPENSE_PRIOR_MONTH_DISP) TAX_EXPENSE_PRIOR_MONTH_DISP,
       sum(TAX_EXPENSE_FUTURE_MONTH) TAX_EXPENSE_FUTURE_MONTH
FROM (
SELECT SUBSTR(TRAE.TAX_YEAR,1,4) tax_year,
               DECODE(FISCAL_MONTH, 1, MONTH_NUMBER, 0) START_MONTH,
               DECODE(FISCAL_MONTH, 12, MONTH_NUMBER, 0) END_MONTH,
               DECODE(FISCAL_MONTH, TAX_EXPENSE_PRIOR_MONTH, MONTH_NUMBER + 1, 0) TAX_EXPENSE_PRIOR_MONTH,
               DECODE(FISCAL_MONTH, TAX_EXPENSE_PRIOR_MONTH, MONTH_NUMBER + 1, 0) TAX_EXPENSE_PRIOR_MONTH_DISP,
               0 TAX_EXPENSE_FUTURE_MONTH
          FROM PP_CALENDAR PPC, TAX_REPAIRS_ADD_EXPENSE TRAE
         WHERE PPC.FISCAL_YEAR = SUBSTR(TRAE.TAX_YEAR, 1, 4)
        UNION
        SELECT SUBSTR(TRAE.TAX_YEAR,1,4) tax_year,
               0            START_MONTH,
               0            END_MONTH,
               0            TAX_EXPENSE_PRIOR_MONTH,
               0            TAX_EXPENSE_PRIOR_MONTH_DISP,
               MONTH_NUMBER TAX_EXPENSE_FUTURE_MONTH
          FROM PP_CALENDAR PPC, TAX_REPAIRS_ADD_EXPENSE TRAE
         WHERE PPC.FISCAL_YEAR = SUBSTR(TRAE.TAX_YEAR, 1, 4) + 1
           AND FISCAL_MONTH = TRAE.TAX_EXPENSE_FUTURE_MONTH
           )
           GROUP BY tax_year
           ORDER BY tax_year
      )     ;

