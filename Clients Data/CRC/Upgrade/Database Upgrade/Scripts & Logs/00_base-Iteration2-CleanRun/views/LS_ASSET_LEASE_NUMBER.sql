
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."LS_ASSET_LEASE_NUMBER" ("LS_ASSET_ID", "LEASE_NUMBER", "LEASE_ID") AS 
  select A.LS_ASSET_ID, C.LEASE_NUMBER, C.LEASE_ID
                           from (select A.LS_ASSET_ID, B.LEASE_ID from LS_ASSET A, LS_ILR B where A.ILR_ID = B.ILR_ID(+)) A,
                                LS_LEASE C
                          where A.LEASE_ID = C.LEASE_ID(+)
 
 ;