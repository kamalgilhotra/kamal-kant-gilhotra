
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."REG_LAYER_EA_DETAILS_VIEW" ("LAYER", "LONG_DESCRIPTION", "LAYER_GROUP", "SELECTED_FIELD", "SELECTED_VALUE", "AS_OF_DATE", "START_MONTH", "END_MONTH", "MONTH_YEAR", "LABEL", "ITEM", "AMOUNT", "LABEL_SORT", "ITEM_SORT") AS 
  (
select layer, long_description, layer_group, selected_field, selected_value, as_of_date, start_month, end_month,
		 month_year, label, item, sum(amount) amount, label_sort, item_sort
  from (
	select a.description layer,
			 a.long_description long_description,
			 nvl((select g.description from reg_incremental_group g where g.incremental_group_id = r.incremental_group_id),' ') layer_group,
			 decode(h.selected_field, 1, 'Funding Project',
											  2, 'Work Order',
											  3, 'Class Code: Value') selected_field,
			 decode(h.selected_field, 1, w.work_order_number||': '||w.description,
											  2, w.work_order_number||': '||w.description,
											  3, c.description||': '||h.class_code_value) selected_value,
			 substr(to_char(h.as_of_date),5,2)||'/'||substr(to_char(h.as_of_date),1,4) as_of_date,
			 substr(to_char(h.start_month),5,2)||'/'||substr(to_char(h.start_month),1,4) start_month,
			 substr(to_char(h.end_month),5,2)||'/'||substr(to_char(h.end_month),1,4) end_month,
			 d.month_year month_year,
			 l.description label,
			 i.description item,
			 d.amount amount,
			 l.sort_order label_sort,
			 i.sort_order item_sort
			, a.hist_layer_id
	  from reg_incremental_adjustment a, work_order_control w, class_code c,
			 incremental_hist_layer_data d, incremental_fp_adj_label l, incremental_fp_adj_item i,
			 (
				select hist_layer_id, funding_project_id, work_order_id, class_code_id, class_code_value,
						 decode(nvl(funding_project_id,0), 0, decode(nvl(work_order_id,0), 0, 3, 2), 1) selected_field,
						 as_of_date, start_month, end_month
				  from incremental_hist_layer
			 ) h,
		 	 reg_incremental_adj_group r
	 where a.incremental_adj_type_id = 5
		and a.hist_layer_id = h.hist_layer_id
		and w.work_order_id (+) = decode(h.selected_field, 1, h.funding_project_id, 2, h.work_order_id)
		and c.class_code_id (+) = decode(h.selected_field, 3, h.class_code_id)
		and a.hist_layer_id = d.hist_layer_id
		and d.label_id = l.label_id
		and d.label_id = i.label_id
		and d.item_id = i.item_id
		and a.incremental_adj_id = r.incremental_adj_id (+))
 group by layer, long_description, layer_group, selected_field, selected_value, as_of_date, start_month, end_month,
			 month_year, label, item, label_sort, item_sort
)
 ;