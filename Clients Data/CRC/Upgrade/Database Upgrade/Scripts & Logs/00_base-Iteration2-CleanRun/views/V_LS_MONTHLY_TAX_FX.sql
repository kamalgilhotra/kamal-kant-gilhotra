
CREATE OR REPLACE VIEW V_LS_MONTHLY_TAX_FX (
  LS_ASSET_ID, COMPANY_ID, GL_POSTING_MO_YR, SET_OF_BOOKS_ID, TAX_LOCAL_ID, SCHEDULE_MONTH, TAX_DISTRICT_ID, TAX_RATE, 
  PAYMENT_AMOUNT, ACCRUAL_AMOUNT, ADJUSTMENT_AMOUNT, TAX_BASE, CONTRACT_CURRENCY_ID, COMPANY_CURRENCY_ID, LS_CUR_TYPE, 
  EXCHANGE_DATE, RATE, ISO_CODE, CURRENCY_DISPLAY_SYMBOL) AS 
SELECT lmt.ls_asset_id,
       asset.company_id,
       lmt.gl_posting_mo_yr,
       lmt.set_of_books_id,
       lmt.tax_local_id,
       lmt.schedule_month,
       lmt.tax_district_id,
       lmt.tax_rate                                       tax_rate,
       lmt.payment_amount * Decode(ls_cur_type, 2, cr.actual_rate,
                                        1)           payment_amount,
       lmt.accrual_amount * Decode(ls_cur_type, 2, cr.actual_rate,
                                        1)           accrual_amount,
       lmt.adjustment_amount * Decode(ls_cur_type, 2, cr.actual_rate,
                                                   1) adjustment_amount,
       lmt.tax_base * Decode(ls_cur_type, 2, cr.actual_rate,
                                          1)          tax_base,
       lease.contract_currency_id,
       cs.currency_id                                 company_currency_id,
       cur.ls_cur_type                                AS ls_cur_type,
       cr.exchange_date,
       Decode(ls_cur_type, 2, cr.actual_rate,
                           1)                         rate,
       cur.iso_code,
       cur.currency_display_symbol
FROM   ls_monthly_tax lmt
       inner join ls_asset asset
               ON asset.ls_asset_id = lmt.ls_asset_id
       inner join currency_schema cs
               ON asset.company_id = cs.company_id
       inner join ls_ilr ilr
               ON ( ilr.ilr_id = asset.ilr_id
                    AND ilr.company_id = asset.company_id )
       inner join ls_lease lease
               ON ( ilr.lease_id = lease.lease_id )
       INNER JOIN (
                    SELECT 
                      ls_currency_type_id AS ls_cur_type
                      , currency_id
                      , currency_display_symbol
                      , iso_code
                      , CASE ls_currency_type_id WHEN 1 THEN 1 ELSE NULL END AS contract_approval_rate
                    FROM currency
                    CROSS JOIN ls_lease_currency_type
                  ) cur 
          ON cur.currency_id = CASE cur.ls_cur_type
            WHEN 1 THEN lease.contract_currency_id
            WHEN 2 THEN cs.currency_id
          END
       INNER JOIN currency_rate_default_vw cr 
          ON cr.currency_from = lease.contract_currency_id
          AND cr.currency_to = cur.currency_id
          AND trunc(cr.exchange_date, 'MONTH') <= trunc(lmt.gl_posting_mo_yr,'MONTH')
          AND trunc(cr.next_date, 'MONTH') > trunc(lmt.gl_posting_mo_yr,'MONTH')
      LEFT JOIN
      (
        SELECT * FROM
        (
          SELECT
            company_id
            , contract_currency_id
            , company_currency_id
            , exchange_rate_type_id
            , accounting_month
            , exchange_date
            , rate
            , Lag(exchange_date) OVER (PARTITION BY company_id, contract_currency_id, company_currency_id, exchange_rate_type_id ORDER BY accounting_month) prev_exchange_date
            , Lag(rate) OVER (PARTITION BY company_id, contract_currency_id, company_currency_id, exchange_rate_type_id ORDER BY accounting_month) prev_rate
          FROM
          (     
            SELECT company_id, contract_currency_id, company_currency_id, exchange_rate_type_id, accounting_month, exchange_date, rate 
            FROM ls_lease_calculated_date_rates
            UNION ALL
            SELECT company_id, contract_currency_id, company_currency_id, exchange_rate_type_id, Add_Months(Max(accounting_month),1) accounting_month, NULL exchange_date, NULL rate 
            FROM ls_lease_calculated_date_rates
            GROUP BY company_id, contract_currency_id, company_currency_id, exchange_rate_type_id
          )
        )
        PIVOT
        (
          Max(exchange_date) AS exchange_date
          , max(rate) AS rate
          , max(prev_exchange_date) AS prev_exchange_date
          , max(prev_rate) AS prev_rate
          FOR exchange_rate_type_id IN (1 AS Actual)
        )
      ) calc_rate
        ON lease.contract_currency_id = calc_rate.contract_currency_id
        AND cur.currency_id = calc_rate.company_currency_id
        AND ilr.company_id = calc_rate.company_id
        AND lmt.gl_posting_mo_yr = calc_rate.accounting_month
WHERE cs.currency_type_id = 1;
