
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."HW_TAX_VIEW" ("HW_TABLE_LINE_ID", "HW_REGION_ID", "YEAR", "RATE") AS 
  select HANDY_WHITMAN_RATES.HW_TABLE_LINE_ID,
       HANDY_WHITMAN_RATES.HW_REGION_ID,
       HANDY_WHITMAN_RATES.YEAR,
       HANDY_WHITMAN_RATES.RATE
  from HANDY_WHITMAN_RATES
union
select MIN_MAX_RATES.HW_TABLE_LINE_ID,
       MIN_MAX_RATES.HW_REGION_ID,
       VINTAGES.VINTAGE,
       MIN_MAX_RATES.MIN_RATE
  from (select INSIDE.*,
               MIN_HW.RATE MIN_RATE,
               MAX_HW.RATE MAX_RATE
          from HANDY_WHITMAN_RATES MIN_HW,
               HANDY_WHITMAN_RATES MAX_HW,
               (select HW_TABLE_LINE_ID,
                       HW_REGION_ID,
                       min(year) MIN_YEAR,
                       max(year) MAX_YEAR
                  from HANDY_WHITMAN_RATES
                 where RATE <> 0
                 group by HW_TABLE_LINE_ID,
                          HW_REGION_ID) INSIDE
         where INSIDE.HW_TABLE_LINE_ID = MIN_HW.HW_TABLE_LINE_ID
           and INSIDE.HW_REGION_ID = MIN_HW.HW_REGION_ID
           and INSIDE.MIN_YEAR = MIN_HW.YEAR
           and INSIDE.HW_TABLE_LINE_ID = MAX_HW.HW_TABLE_LINE_ID
           and INSIDE.HW_REGION_ID = MAX_HW.HW_REGION_ID
           and INSIDE.MAX_YEAR = MAX_HW.YEAR) MIN_MAX_RATES,
       (select ROWNUM + 1899 VINTAGE from TAX_RATE_CONTROL where ROWNUM <= 201) VINTAGES
 where MIN_MAX_RATES.MIN_YEAR > VINTAGES.VINTAGE
union
select MIN_MAX_RATES.HW_TABLE_LINE_ID,
       MIN_MAX_RATES.HW_REGION_ID,
       VINTAGES.VINTAGE,
       MIN_MAX_RATES.MAX_RATE
  from (select INSIDE.*,
               MIN_HW.RATE MIN_RATE,
               MAX_HW.RATE MAX_RATE
          from HANDY_WHITMAN_RATES MIN_HW,
               HANDY_WHITMAN_RATES MAX_HW,
               (select HW_TABLE_LINE_ID,
                       HW_REGION_ID,
                       min(year) MIN_YEAR,
                       max(year) MAX_YEAR
                  from HANDY_WHITMAN_RATES
                 where RATE <> 0
                 group by HW_TABLE_LINE_ID,
                          HW_REGION_ID) INSIDE
         where INSIDE.HW_TABLE_LINE_ID = MIN_HW.HW_TABLE_LINE_ID
           and INSIDE.HW_REGION_ID = MIN_HW.HW_REGION_ID
           and INSIDE.MIN_YEAR = MIN_HW.YEAR
           and INSIDE.HW_TABLE_LINE_ID = MAX_HW.HW_TABLE_LINE_ID
           and INSIDE.HW_REGION_ID = MAX_HW.HW_REGION_ID
           and INSIDE.MAX_YEAR = MAX_HW.YEAR) MIN_MAX_RATES,
       (select ROWNUM + 1899 VINTAGE from TAX_RATE_CONTROL where ROWNUM <= 201) VINTAGES
 where MIN_MAX_RATES.MAX_YEAR < VINTAGES.VINTAGE
 
 
 
 
 
 ;