
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."PENDING_WORKFLOW_DETAIL_VIEW" ("WORKFLOW_ID", "ID", "WORKFLOW_RULE_ID", "WORKFLOW_RULE_DESC", "RULE_ORDER", "USERS", "DATE_APPROVED", "USER_APPROVED", "APPROVAL_STATUS_ID", "REQUIRED", "NOTES", "AUTHORITY_LIMIT", "NUM_APPROVERS", "NUM_REQUIRED", "SQL", "ALLOW_EDIT", "ORIG_AUTHORITY_LIMIT", "IS_CUSTOM_LIMIT", "MOBILE_APPROVAL", "GROUP_APPROVAL", "SQL_USERS", "USER_ID", "TIME_STAMP", "DATE_SENT") AS 
  SELECT pending.workflow_id, pending.id, pending.workflow_rule_id, pending.workflow_rule_desc, pending.rule_order, pending.users, pending.date_approved,
pending.user_approved, pending.approval_status_id, pending.required, pending.notes, pending.authority_limit, pending.num_approvers, pending.num_required,
pending.SQL, pending.allow_edit, pending.orig_authority_limit, pending.is_custom_limit, pending.mobile_approval, pending.group_approval, pending.sql_users,
pending.user_id, pending.time_stamp, prev_approved.date_approved date_sent
FROM workflow_detail pending, workflow_detail prev_approved
WHERE pending.workflow_id = prev_approved.workflow_id
AND pending.approval_status_id = 2
AND prev_approved.rule_order = (
  SELECT Max(rule_order)
  FROM workflow_detail pending2
  WHERE pending.workflow_id = pending2.workflow_id
  AND pending.rule_order > pending2.rule_order)
AND pending.rule_order <> (
  SELECT Min(rule_order)
  FROM workflow_detail pending2
  WHERE pending.workflow_id = pending2.workflow_id)
UNION ALL
SELECT pending.workflow_id, pending.id, pending.workflow_rule_id, pending.workflow_rule_desc, pending.rule_order, pending.users, pending.date_approved,
pending.user_approved, pending.approval_status_id, pending.required, pending.notes, pending.authority_limit, pending.num_approvers, pending.num_required,
pending.SQL, pending.allow_edit, pending.orig_authority_limit, pending.is_custom_limit, pending.mobile_approval, pending.group_approval, pending.sql_users,
pending.user_id, pending.time_stamp, workflow.date_sent date_sent
FROM workflow_detail pending, workflow
WHERE pending.workflow_id = workflow.workflow_id
AND pending.approval_status_id = 2
AND pending.rule_order = (
  SELECT Min(rule_order)
  FROM workflow_detail pending2
  WHERE pending.workflow_id = pending2.workflow_id)
 ;