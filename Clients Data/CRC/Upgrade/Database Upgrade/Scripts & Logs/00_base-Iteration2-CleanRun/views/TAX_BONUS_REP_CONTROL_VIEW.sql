
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."TAX_BONUS_REP_CONTROL_VIEW" ("COMPANY_ID", "CONTROL_VALUE") AS 
  select COMPANY_ID, UPPER(CONTROL_VALUE) CONTROL_VALUE
  from PP_SYSTEM_CONTROL_COMPANY
 where CONTROL_NAME = 'TAX_PRIOR_BONUS_REPORTS'
   and COMPANY_ID <> -1
union
select COMPANY.COMPANY_ID, NVL(UPPER(PP_SYSTEM_CONTROL_COMPANY.CONTROL_VALUE), 'NO') CONTROL_VALUE
  from COMPANY,
       (select CONTROL_VALUE
          from PP_SYSTEM_CONTROL_COMPANY
         where COMPANY_ID = -1
           and CONTROL_NAME = 'TAX_PRIOR_BONUS_REPORTS') PP_SYSTEM_CONTROL_COMPANY
 where COMPANY.COMPANY_ID in (select COMPANY_ID
                                from COMPANY
                              minus
                              select COMPANY_ID
                                from PP_SYSTEM_CONTROL_COMPANY
                               where CONTROL_NAME = 'TAX_PRIOR_BONUS_REPORTS'
                                 and COMPANY_ID <> -1)
union
select COMPANY_ID, 'NO' CONTROL_VALUE
  from COMPANY
 where COMPANY_ID not in
       (select COMPANY_ID
          from PP_SYSTEM_CONTROL_COMPANY
         where CONTROL_NAME = 'TAX_PRIOR_BONUS_REPORTS'
           and COMPANY_ID <> -1
        union (select COMPANY.COMPANY_ID
                from COMPANY,
                     (select CONTROL_VALUE
                        from PP_SYSTEM_CONTROL_COMPANY
                       where COMPANY_ID = -1
                         and CONTROL_NAME = 'TAX_PRIOR_BONUS_REPORTS') PP_SYSTEM_CONTROL_COMPANY
               where COMPANY.COMPANY_ID in (select COMPANY_ID
                                              from COMPANY
                                            minus
                                            select COMPANY_ID
                                              from PP_SYSTEM_CONTROL_COMPANY
                                             where CONTROL_NAME = 'TAX_PRIOR_BONUS_REPORTS'
                                               and COMPANY_ID <> -1)))
 
 
 
 
 
 ;