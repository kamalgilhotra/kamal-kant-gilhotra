
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."CAP500_V" ("CO", "COMPANY", "ID", "PROJECT_SUBTOTAL", "PROJECT_TYPE", "ACTUALSYTD", "BUDGETYTD", "VARIANCE", "VARPERCENT", "PROJECTED_ACTUALS", "PROJECTED_BUDGET", "VARIANCE2", "VARPERCENT2", "CURRENT_MONTH") AS 
  SELECT       co,
             pq.company||decode(pq.co,'99',to_char(null),' ('||initcap(last_name)||')') company,
             id,
             project_subtotal,
             project_type,
            round(sum(actualsytd)) actualsytd,
            round(sum(budgetytd)) budgetytd,
            round(sum(actualsytd)) - round(sum(budgetytd)) Variance,
					          round(case when round(sum(budgetytd)) = 0
			            and round(sum(actualsytd)) = 0 then 0
						when round(sum(budgetytd)) = 0
			            and round(sum(actualsytd)) != 0 then 100 * sign(sum(actualsytd))
						else (round((sum(actualsytd)) - round(sum(budgetytd)))/round(sum(budgetytd)))*100
                              end,1) Varpercent,
            round(sum(projected_actuals)) projected_actuals,
            round(sum(projected_budget)) projected_budget,
            round(sum(projected_actuals)) - round(sum(projected_budget)) Variance2,
							  round(case when round(sum(projected_budget)) = 0
			            and round(sum(projected_actuals)) = 0 then 0
						when round(sum(projected_budget)) = 0
			            and round(sum(projected_actuals)) != 0 then 100 * sign(sum(projected_actuals))
						else (round((sum(projected_actuals)) - round(sum(projected_budget)))/round(sum(projected_budget)))*100
                              end,1) Varpercent2,
        (select to_char(to_date(control_value, 'YYYYMM'), 'fmMonth YYYY') from cr_alloc_system_control where control_id = 5) Current_Month
FROM (
select      c.company_rollup co,
            c.co_rollup_description company,
            g.project_type_id id,
            substr(g.project_type_subtotal,7) project_subtotal,
            g.project_type project_type,
            sum (a.amount) actualsytd,
            0 budgetytd,
            0 projected_actuals,
            0 projected_budget
from         cr_epe_charging_cost_center_mv c,
            cr_cost_repository_sv a,
                        (select min (case
                         when control_id = 23
                         then control_value
                         end
                         ) as start_month,
                    min (case
                         when control_id = 24
                         then control_value
                         end
                         ) as end_month
              from cr_alloc_system_control
              )    d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
              cr_epe_project_mv g
where    a.charging_expense_type = e.charging_expense_type
            and a.account = f.account
             and a.project = g.project
             and g.owning_department = c.charging_cost_center
            and a.month_number between d.start_month and d.end_month
            --and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
            and upper(trim(c.element_type)) = 'ACTUALS'
            and upper(trim(e.element_type)) = 'ACTUALS'
            and upper(trim(f.element_type)) = 'ACTUALS'
            and upper(trim(g.element_type)) = 'ACTUALS'
            and f.acct_group = 'X'
            and e.allocation_type <> '7'
            and substr(a.gl_journal_category,1,4) not in ( '7936', '7937')
group by    c.company_rollup,
            c.co_rollup_description,
              g.project_type_id,
          g.project_type_subtotal,
            g.project_type
union
select      c.company_rollup co,
            c.co_rollup_description company,
            g.project_type_id id,
            substr(g.project_type_subtotal,7) project_subtotal,
            g.project_type project_type,
            0 actualsytd,
            sum(a.amount) budgetytd,
            0 projected_actuals,
            0 projected_budget
from         cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select min (case
                         when control_id = 23
                         then control_value
                         end
                         ) as start_month,
                    min (case
                         when control_id = 24
                         then control_value
                         end
                         ) as end_month
              from cr_alloc_system_control
              )    d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
             cr_epe_project_mv g
where     a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.project = g.project
            and c.charging_cost_center = g.owning_department
            and a.budget_version = b.control_value
            and a.month_number between d.start_month and d.end_month
            --and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
            and f.acct_group = 'X'
            and e.allocation_type <> '7'
group by     c.company_rollup,
            c.co_rollup_description,
            g.project_type_id,
            g.project_type_subtotal,
            g.project_type
union
select      c.company_rollup co,
            c.co_rollup_description company,
            g.project_type_id id,
            substr(g.project_type_subtotal,7) project_subtotal,
            g.project_type project_type,
            0 actuals_amount,
            0 budget_amount,
            sum(a.amount) projected_actuals,
            0 projected_budget
from        cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'REVISED BUDGET VERSION') b,
            (select min (case
                        when control_id = 5
                        then substr(control_value,1,4)
                        end
                        )as allyear
                        from cr_alloc_system_control) d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_project_mv g
where     a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.project = g.project
            and c.charging_cost_center = g.owning_department
            and a.budget_version = b.control_value
            and substr(a.month_number,1,4) = d.allyear
            --and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
             and upper(trim(g.element_type)) = 'BUDGET'
            and f.acct_group = 'X'
            and e.allocation_type <> '7'
group by     c.company_rollup,
            c.co_rollup_description,
            g.project_type_id,
            g.project_type_subtotal,
            g.project_type
union
select      c.company_rollup co,
            c.co_rollup_description company,
            g.project_type_id id,
            substr(g.project_type_subtotal,7) project_subtotal,
            g.project_type project_type,
            0 actuals_amount,
            0 budget_amount,
            0 projected_actuals,
            sum(a.amount) projected_budget
from        cr_budget_data_sv a,
            cr_epe_charging_cost_center_mv c,
            (select control_value from cr_alloc_system_control
               where upper(trim(control_name)) = 'ORIGINAL BUDGET VERSION') b,
            (select min (case
                        when control_id = 5
                        then substr(control_value,1,4)
                        end
                        )as allyear
                        from cr_alloc_system_control) d,
            cr_epe_charging_exp_type_mv e,
            cr_epe_account_mv f,
            cr_epe_project_mv g
where     a.charging_expense_type = e.charging_expense_type
            and a.ferc_account = f.account
            and a.project = g.project
            and c.charging_cost_center = g.owning_department
            and a.budget_version = b.control_value
            and substr(a.month_number,1,4) = d.allyear
            --and substr(a.month_number,1,4) = to_char(to_date(sysdate), 'yyyy')
            and upper(trim(c.element_type)) = 'BUDGET'
            and upper(trim(e.element_type)) = 'BUDGET'
            and upper(trim(f.element_type)) = 'BUDGET'
            and upper(trim(g.element_type)) = 'BUDGET'
            and f.acct_group = 'X'
            and e.allocation_type <> '7'
group by    c.company_rollup,
            c.co_rollup_description,
            g.project_type_id,
            g.project_type_subtotal,
            g.project_type
) pq, xxepe_fin_rpt_deliveries cc, pp_security_users pp
WHERE cc.COMPANY_ROLLUP = pq.co
  and cc.report_level in  ('A','B')
  and cc.deliver_to_user = pp.users
  and cc.source = 'H'
GROUP BY  co,
             pq.company||decode(pq.co,'99',to_char(null),' ('||initcap(last_name)||')'),
             id,
             project_subtotal,
             project_type
HAVING abs(sum(actualsytd) + sum(budgetytd) + sum(projected_actuals) + sum(projected_budget)) > .49
 
 
 
 
 
 ;