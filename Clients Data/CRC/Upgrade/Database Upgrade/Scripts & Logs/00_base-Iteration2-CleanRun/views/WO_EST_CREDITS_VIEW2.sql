
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "PWRPLANT"."WO_EST_CREDITS_VIEW2" ("WORK_ORDER_ID", "REVISION", "CREDITS") AS 
  (  	select work_order_id, revision, sum(credits) credits   	  from (select a.work_order_id, nvl(a.revision, 1) revision, nvl(sum(b.amount), 0) credits   				 from wo_est_first_view a, wo_estimate b  				where a.work_order_id = b.work_order_id (+)   				  and a.revision = b.revision (+)   				  and b.est_chg_type_id (+) is null   				group by a.work_order_id, a.revision   				union   			  select a.work_order_id, a.revision, sum(b.amount)  				 from wo_est_first_view a, wo_estimate b   				where a.work_order_id = b.work_order_id (+)   				  and a.revision = b.revision (+)   				  and b.est_chg_type_id in   					  (select est_chg_type_id from estimate_charge_type  						 where processing_type_id = 3 or processing_type_id = 6)  				group by a.work_order_id, a.revision) group by work_order_id, revision)
 
 
 
 
 
 ;