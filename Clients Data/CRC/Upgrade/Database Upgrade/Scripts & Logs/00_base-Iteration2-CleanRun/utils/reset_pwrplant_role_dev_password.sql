/*
The role password is now encrypted

The DBA should replace the the password below 'Tr9uI22k' with
the correct password that has been set for the PWRPLANT_ROLE_DEV role.
*/

declare
   CODE      int;
   NEWPASSWD varchar(200);
begin
   NEWPASSWD := 'Tr9uI22k';
   CODE      := PWRPLANT_ADMIN('setrole', 'PWRPLANT42PPC', NEWPASSWD, '', '', '', '');
end;
/
