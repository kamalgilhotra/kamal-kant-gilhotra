SET ECHO OFF
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.increment_sequences.log

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: increment_sequences.sql
|| Description: Increaments all PWRPLANT sequences by LL_INCREMENT_AMOUNT
||============================================================================
|| Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- -----------------------------------------
|| 10.2.1.5 01/13/2011 Lee Quinn      Created
||============================================================================
*/

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   LL_INCREMENT_AMOUNT number := 10000;
   LL_COUNT            number := 0;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
   for CSR_ALTER_SEQUENCES in (select 'alter sequence ' || SEQUENCE_NAME || ' increment by ' || LL_INCREMENT_AMOUNT || ' nocache' ALTER_SEQUENCE,
                                      SEQUENCE_NAME,
                                      LAST_NUMBER
                                 from ALL_SEQUENCES
                                where SEQUENCE_OWNER = 'PWRPLANT'
                                order by SEQUENCE_NAME)
   loop
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Sequence ' || CSR_ALTER_SEQUENCES.SEQUENCE_NAME || ' has a current value of ' || CSR_ALTER_SEQUENCES.LAST_NUMBER || '.');
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_ALTER_SEQUENCES.ALTER_SEQUENCE || ';');
      DBMS_OUTPUT.NEW_LINE();
      execute immediate CSR_ALTER_SEQUENCES.ALTER_SEQUENCE;
      LL_COUNT := LL_COUNT + 1;
   end loop;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || LL_COUNT || ' sequences were incremented by ' || LL_INCREMENT_AMOUNT || '.');
end;
/

/*
|| END increment_sequences
*/
SPOOL OFF
SET ECHO ON