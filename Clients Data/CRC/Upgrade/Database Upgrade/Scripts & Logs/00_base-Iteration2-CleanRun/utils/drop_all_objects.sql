SET TIME ON
SET ECHO OFF
SET VERIFY OFF
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
DEFINE PP_SCRIPT_PATH='C:\oracle\product\10.2.0\admin\ldeliv10\create\utils\'

SPOOL &&PP_SCRIPT_PATH.drop_all_objects.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: drop_all_objects.sql
|| Description: Disable all constraints
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| V10     09/27/2009 Lee Quinn      Created
||============================================================================
*/

PROMPT This will drop all TABLES and OBJECTS from the PWRPLANT schema
ACCEPT ls_process CHAR PROMPT 'are you sure you want to continue? (Y/N) '

declare
   PPCMSG     varchar2(10) := 'PPC-MSG> ';
   PPCERR     varchar2(10) := 'PPC-ERR> ';
   PPCSQL     varchar2(10) := 'PPC-SQL> ';
   PPCORA     varchar2(10) := 'PPC-ORA' || '-> ';

begin
   DBMS_OUTPUT.ENABLE(buffer_size => NULL);

   if '&ls_process' = 'y' or '&ls_process' = 'Y' then
      -- DROP all Tables
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'DROPPING all Tables');
      for DROP_SQL in (select 'drop table ' || TABLE_NAME || ' cascade constraints purge' SQL_LINE
                               from ALL_TABLES
                              where OWNER = 'PWRPLANT'
                              order by TABLE_NAME)
      loop
         DBMS_OUTPUT.PUT_LINE(PPCSQL || DROP_SQL.SQL_LINE || ';');
         begin
            execute immediate DROP_SQL.SQL_LINE;
         exception
            when others then
               DBMS_OUTPUT.PUT_LINE(PPCORA || sqlcode || ' SQLERRM = ' || sqlerrm);
         end;
      end loop;

      -- DROP all FUNCTIONS
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'DROPPING all Fucntions');
      for DROP_SQL in (select 'drop function ' || OBJECT_NAME SQL_LINE
                               from ALL_OBJECTS
                              where OWNER = 'PWRPLANT'
                                and OBJECT_TYPE = 'FUNCTION'
                              order by OBJECT_NAME)
      loop
         DBMS_OUTPUT.PUT_LINE(PPCSQL || DROP_SQL.SQL_LINE || ';');
         begin
            execute immediate DROP_SQL.SQL_LINE;
         exception
            when others then
               DBMS_OUTPUT.PUT_LINE(PPCORA || sqlcode || ' SQLERRM = ' || sqlerrm);
         end;
      end loop;

      -- DROP all PROCEDURES
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'DROPPING all Procedures');
      for DROP_SQL in (select 'drop procedure ' || OBJECT_NAME SQL_LINE
                               from ALL_OBJECTS
                              where OWNER = 'PWRPLANT'
                                and OBJECT_TYPE  = 'PROCEDURE'
                              order by OBJECT_NAME)
      loop
         DBMS_OUTPUT.PUT_LINE(PPCSQL || DROP_SQL.SQL_LINE || ';');
         begin
            execute immediate DROP_SQL.SQL_LINE;
         exception
            when others then
               DBMS_OUTPUT.PUT_LINE(PPCORA || sqlcode || ' SQLERRM = ' || sqlerrm);
         end;
      end loop;

      -- DROP all VIEWS
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'DROPPING all Views');
      for DROP_SQL in (select 'drop view ' || OBJECT_NAME SQL_LINE
                               from ALL_OBJECTS
                              where OWNER = 'PWRPLANT'
                                and OBJECT_TYPE = 'VIEW'
                              order by OBJECT_NAME)
      loop
         DBMS_OUTPUT.PUT_LINE(PPCSQL || DROP_SQL.SQL_LINE || ';');
         begin
            execute immediate DROP_SQL.SQL_LINE;
         exception
            when others then
               DBMS_OUTPUT.PUT_LINE(PPCORA || sqlcode || ' SQLERRM = ' || sqlerrm);
         end;
      end loop;

      -- DROP all Packages
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'DROPPING all Packages');
      for DROP_SQL in (select 'drop package ' || OBJECT_NAME SQL_LINE
                               from ALL_OBJECTS
                              where OWNER = 'PWRPLANT'
                                and OBJECT_TYPE = 'PACKAGE'
                              order by OBJECT_NAME)
      loop
         DBMS_OUTPUT.PUT_LINE(PPCSQL || DROP_SQL.SQL_LINE || ';');
         begin
            execute immediate DROP_SQL.SQL_LINE;
         exception
            when others then
               DBMS_OUTPUT.PUT_LINE(PPCORA || sqlcode || ' SQLERRM = ' || sqlerrm);
         end;
      end loop;

      -- DROP all Types
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'DROPPING all Types');
      for DROP_SQL in (select 'drop type ' || OBJECT_NAME SQL_LINE
                               from ALL_OBJECTS
                              where OWNER = 'PWRPLANT'
                                and OBJECT_TYPE = 'TYPE'
                              order by OBJECT_NAME)
      loop
         DBMS_OUTPUT.PUT_LINE(PPCSQL || DROP_SQL.SQL_LINE || ';');
         begin
            execute immediate DROP_SQL.SQL_LINE;
         exception
            when others then
               DBMS_OUTPUT.PUT_LINE(PPCORA || sqlcode || ' SQLERRM = ' || sqlerrm);
         end;
      end loop;

      -- DROP all Sequences
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'DROPPING all Sequences');
      for DROP_SQL in (select 'drop sequence ' || OBJECT_NAME SQL_LINE
                               from ALL_OBJECTS
                              where OWNER = 'PWRPLANT'
                                and OBJECT_TYPE = 'SEQUENCE'
                              order by OBJECT_NAME)
      loop
         DBMS_OUTPUT.PUT_LINE(PPCSQL || DROP_SQL.SQL_LINE || ';');
         begin
            execute immediate DROP_SQL.SQL_LINE;
         exception
            when others then
               DBMS_OUTPUT.PUT_LINE(PPCORA || sqlcode || ' SQLERRM = ' || sqlerrm);
         end;
      end loop;

      -- DROP all Synonyms
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'DROPPING all Sequences');
      for DROP_SQL in (select 'drop public synonym ' || SYNONYM_NAME SQL_LINE
                         from DBA_SYNONYMS
                        where OWNER = 'PUBLIC'
                          and TABLE_OWNER = 'PWRPLANT'
                        order by SYNONYM_NAME)
      loop
         DBMS_OUTPUT.PUT_LINE(PPCSQL || DROP_SQL.SQL_LINE || ';');
         begin
            execute immediate DROP_SQL.SQL_LINE;
         exception
            when others then
               DBMS_OUTPUT.PUT_LINE(PPCORA || sqlcode || ' SQLERRM = ' || sqlerrm);
         end;
      end loop;
   else
      DBMS_OUTPUT.PUT_LINE('/* ');
      DBMS_OUTPUT.PUT_LINE('|| Process Aborted');
      DBMS_OUTPUT.PUT_LINE('*/ ');
   end if;
end;
/

/*
|| END Drop All Objects
*/
SPOOL OFF
SET ECHO ON