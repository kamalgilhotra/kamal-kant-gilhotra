SET ECHO ON
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.alter_default_on_null.log

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: alter_default_on_null.sql
|| Description: This is for Oracle 12 database.  It will update the null rows
||              in any column and set them to thier default value and make the column not null.
||              It changes the default to default on null so any new inserts will
||              will be set to the default value even if an insert statement specifies
||              null in the statement.  This was an issue with datawindows.  If
||              a column wasn't entered in a datawindow the resulting insert or update
||              statement would set the column explicity to null.
||
||              This script will need to be rerun from time to time to fix any
||              new columns that have been added with default values untill we have
||              completely moved to Oracle 12.
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 03/07/2014 Lee Quinn
||============================================================================
*/


declare
   L_SQL      varchar2(4000);
   START_TIME date;
   END_TIME   date;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   for CSR_DEFAULTS in (select ATC.TABLE_NAME,
                               ATC.COLUMN_NAME,
                               ATC.DATA_DEFAULT,
                               ATC.NULLABLE,
                               ATC.DATA_TYPE
                          from ALL_TAB_COLUMNS ATC, ALL_TABLES AT
                         where ATC.OWNER = 'PWRPLANT'
                           and ATC.OWNER = AT.OWNER
                           and ATC.TABLE_NAME = AT.TABLE_NAME
                           and ATC.DATA_DEFAULT is not null --Columns that have a default
                           and ATC.DEFAULT_ON_NULL <> 'YES' --Don't redo columns that already have default on null
                         order by ATC.TABLE_NAME, ATC.COLUMN_ID)

   loop
      --Output the defaults
      DBMS_OUTPUT.PUT_LINE('Table Name: ' || RPAD(CSR_DEFAULTS.TABLE_NAME, 30) || 'Column Name: ' ||
                           RPAD(CSR_DEFAULTS.COLUMN_NAME, 30) || 'Data Type: ' ||
                           RPAD(CSR_DEFAULTS.DATA_TYPE, 15) || 'Nullable: ' ||
                           RPAD(CSR_DEFAULTS.NULLABLE, 5) || 'Default Value: <' ||
                           CSR_DEFAULTS.DATA_DEFAULT || '>');

      if UPPER(CSR_DEFAULTS.DATA_DEFAULT) = 'NULL' then
         --Don't change columns that have NULL as thier default value.  This happens when
         --a column had a default at some point and it was removed with an modify column statement.
         DBMS_OUTPUT.PUT_LINE(CSR_DEFAULTS.TABLE_NAME || ' colunm ' || CSR_DEFAULTS.COLUMN_NAME ||
                              ' default value is NULL.');
      else
         START_TIME := sysdate;
         --Don't try to update the column to it's default value if the column is already NOT NULL.
         if CSR_DEFAULTS.NULLABLE = 'Y' then
            L_SQL := 'update "' || CSR_DEFAULTS.TABLE_NAME || '" set "' || CSR_DEFAULTS.COLUMN_NAME ||
                     '" = ' || CSR_DEFAULTS.DATA_DEFAULT || ' where "' || CSR_DEFAULTS.COLUMN_NAME ||
                     '" is null';
            DBMS_OUTPUT.PUT_LINE(L_SQL || ';');
            execute immediate L_SQL;
            DBMS_OUTPUT.PUT_LINE('Rows Updated: ' || sql%rowcount);
         end if;

         L_SQL := 'alter table "' || CSR_DEFAULTS.TABLE_NAME || '" modify "' ||
                  CSR_DEFAULTS.COLUMN_NAME || '" default on null ' || CSR_DEFAULTS.DATA_DEFAULT;
         DBMS_OUTPUT.PUT_LINE(L_SQL || ';');
         execute immediate L_SQL;
         END_TIME := sysdate;
         DBMS_OUTPUT.PUT_LINE('Duration in seconds: ' ||
                              ROUND(((END_TIME - START_TIME) * 24 * 60 * 60), 2));

      end if;
   end loop;

exception
   when others then
      DBMS_OUTPUT.PUT_LINE('SQL=' || L_SQL || ' error=' || sqlerrm(sqlcode));
end;
/

SPOOL OFF