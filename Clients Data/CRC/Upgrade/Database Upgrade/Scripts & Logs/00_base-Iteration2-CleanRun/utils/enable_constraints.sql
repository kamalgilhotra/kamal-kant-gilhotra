SET ECHO OFF
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.enable_constraints.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: enable_constraints.sql
|| Description: Enable all constraints
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| V10     09/23/2008 Lee Quinn      Created
||============================================================================
*/

declare
   PPCMSG     varchar2(10) := 'PPC-MSG> ';
   PPCERR     varchar2(10) := 'PPC-ERR> ';
   PPCSQL     varchar2(10) := 'PPC-SQL> ';

begin
   DBMS_OUTPUT.ENABLE(2000000);
   for CONSTRAINT_SQL in (select 'alter table ' || TABLE_NAME || ' enable constraint ' ||
                                 CONSTRAINT_NAME SQL_LINE
                            from USER_CONSTRAINTS
                           where CONSTRAINT_TYPE = 'R'
                             and STATUS = 'DISABLED'
                           order by TABLE_NAME)
   loop
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CONSTRAINT_SQL.SQL_LINE || ';');
      begin
         execute immediate CONSTRAINT_SQL.SQL_LINE;
      exception
         when others then
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
      end;
   end loop;
end;
/

/*
|| END Enable Constraints
*/
SPOOL OFF
SET ECHO ON