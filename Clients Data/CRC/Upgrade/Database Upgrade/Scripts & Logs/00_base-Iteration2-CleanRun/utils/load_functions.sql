/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.load_functions.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: load_functions.sql
|| Description: Load all the PowerPlant database functions.
||============================================================================
|| Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.0.09  06/11/2008 Lee Quinn      Created
|| 10.1.10  10/14/2008 Lee Quinn      Updated
|| 10.1.2   01/05/2009 Lee Quinn      Point Release
|| 10.2.1.4 06/10/2010 Lee Quinn      Point Release
||============================================================================
*/

@&&PP_SCRIPT_PATH.functions/ADD_DATE_FY.sql
@&&PP_SCRIPT_PATH.functions/ALTER_TABLE.sql
@&&PP_SCRIPT_PATH.functions/ANALYZE_TABLE.sql
@&&PP_SCRIPT_PATH.functions/CR_COMBOS_BDG_GET_RULE_ID.sql
@&&PP_SCRIPT_PATH.functions/CURR_BV.sql
@&&PP_SCRIPT_PATH.functions/CURR_CO.sql
@&&PP_SCRIPT_PATH.functions/DELETE_TABLE.sql
@&&PP_SCRIPT_PATH.functions/F_PP_GET_REPAIR_METHOD.sql
@&&PP_SCRIPT_PATH.functions/F_TAX_ACCRUAL_REPORT_DEBUG.sql
@&&PP_SCRIPT_PATH.functions/GET_CONS_COLUMNS.sql
@&&PP_SCRIPT_PATH.functions/GET_DEPR_GROUP_ID.sql
@&&PP_SCRIPT_PATH.functions/GET_R_TABLE_NAME.sql
@&&PP_SCRIPT_PATH.functions/IS_DATE.sql
@&&PP_SCRIPT_PATH.functions/IS_NUMBER.sql
@&&PP_SCRIPT_PATH.functions/NULLIF.sql
@&&PP_SCRIPT_PATH.functions/PPCPRODUCT.sql
@&&PP_SCRIPT_PATH.functions/PPC_TEST_SQL.sql
@&&PP_SCRIPT_PATH.functions/PPWO.sql
@&&PP_SCRIPT_PATH.functions/PPWOCO.sql
@&&PP_SCRIPT_PATH.functions/PP_CURRENCY_EXCHANGE_BUDGET.sql
@&&PP_SCRIPT_PATH.functions/PP_CURRENCY_EXCHANGE_COMP.sql
@&&PP_SCRIPT_PATH.functions/PP_CURRENCY_EXCHANGE_STR.sql
@&&PP_SCRIPT_PATH.functions/PP_GL_TRANSACTION.sql
@&&PP_SCRIPT_PATH.functions/PP_GL_TRANSACTION2.sql
@&&PP_SCRIPT_PATH.functions/PP_REPORT_END_MONTH.sql
@&&PP_SCRIPT_PATH.functions/PP_REPORT_END_MONTH_NUMBER.sql
@&&PP_SCRIPT_PATH.functions/PP_REPORT_START_MONTH.sql
@&&PP_SCRIPT_PATH.functions/PP_REPORT_START_MONTH_NUMBER.sql
@&&PP_SCRIPT_PATH.functions/PP_SEND_MAIL.sql
@&&PP_SCRIPT_PATH.functions/PP_USER_DESCR.sql
@&&PP_SCRIPT_PATH.functions/PP_VERIFY_USER.sql
@&&PP_SCRIPT_PATH.functions/PWRPLANT_ADMIN.sql
@&&PP_SCRIPT_PATH.functions/STRING_AGG.sql
@&&PP_SCRIPT_PATH.functions/WO.sql
@&&PP_SCRIPT_PATH.functions/WOCO.sql

SPOOL OFF