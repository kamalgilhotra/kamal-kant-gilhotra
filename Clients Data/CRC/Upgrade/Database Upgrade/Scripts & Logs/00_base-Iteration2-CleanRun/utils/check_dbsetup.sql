SET SERVEROUTPUT ON
SET LINESIZE 120
SET ECHO OFF
/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.check_dbsetup.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: check_dbsetup.sql
|| Description: Checks Roles and Grants given to Roles and to PWRPLANT
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| V10     09/26/2008 Lee Quinn      Created
||============================================================================
*/

declare
   LB_DBA_ROLES boolean := true;
   TABLE_NOT_EXIST exception;
   pragma exception_init(TABLE_NOT_EXIST, -00942);
   L_ROLE              varchar2(30);
   L_PRIVILEGE         varchar2(30);
   L_TABLE_NAME        varchar2(30);
   L_GRANTED_ROLE      varchar2(30);
   L_ADMIN_OPTION      varchar2(30);
   L_DEFAULT_ROLE      varchar2(30);
begin
   DBMS_OUTPUT.ENABLE(2000000);
   /*
   || Check access to dba_roles table
   */
   begin
      execute immediate 'select count(*) from dba_roles where rownum < 2';
   exception
      when TABLE_NOT_EXIST then
         LB_DBA_ROLES := false;
      when others then
         DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
   end;
   /*
   || Check the Roles
   */
   DBMS_OUTPUT.PUT_LINE('/* Check for Roles */');
   DBMS_OUTPUT.PUT_LINE('');
   DBMS_OUTPUT.PUT_LINE('Role Name                    Status');
   DBMS_OUTPUT.PUT_LINE('____________________     ______________');
   if not LB_DBA_ROLES then
      DBMS_OUTPUT.PUT_LINE('');
      DBMS_OUTPUT.PUT_LINE('/*');
      DBMS_OUTPUT.PUT_LINE('|| Acces to DBA_ROLES is not available.');
      DBMS_OUTPUT.PUT_LINE('*/');
   else
      /* Check PWRPLANT_ROLE_USER */
      begin
         execute immediate 'select role                          ' ||
                           '  from dba_roles                     ' ||
                           ' where role = ''PWRPLANT_ROLE_USER'' '
            into L_ROLE;
         DBMS_OUTPUT.PUT_LINE(L_ROLE || '             OK');
      exception
         when NO_DATA_FOUND then
            DBMS_OUTPUT.PUT_LINE('PWRPLANT_ROLE_USER       FAIL');
      end;
      /* Check PWRPLANT_ROLE_DEV */
      begin
         execute immediate 'select ROLE                         ' ||
                           '  from DBA_ROLES                    ' ||
                           ' where ROLE = ''PWRPLANT_ROLE_DEV'' '
            into L_ROLE;
         DBMS_OUTPUT.PUT_LINE(L_ROLE || '              OK');
      exception
         when NO_DATA_FOUND then
            DBMS_OUTPUT.PUT_LINE(' PWRPLANT_ROLE_DEV FAIL ');
      end;
      /* Check PWRPLANT_ROLE_ADMIN */
      begin
         execute immediate 'select ROLE                             ' ||
                           '  from DBA_ROLES                        ' ||
                           '  where ROLE = ''PWRPLANT_ROLE_ADMIN'' '
            into L_ROLE;
         DBMS_OUTPUT.PUT_LINE(L_ROLE || '            OK ');
      exception
         when NO_DATA_FOUND then
            DBMS_OUTPUT.PUT_LINE(' PWRPLANT_ROLE_ADMIN FAIL ');
      end;
      /* Check PWRPLANT_ROLE_RDONLY */
      begin
         execute immediate 'select ROLE                             ' ||
                           '  from DBA_ROLES                        ' ||
                           '  where ROLE = ''PWRPLANT_ROLE_RDONLY'' '
            into L_ROLE;
         DBMS_OUTPUT.PUT_LINE(L_ROLE || '           OK ');
      exception
         when NO_DATA_FOUND then
            DBMS_OUTPUT.PUT_LINE(' PWRPLANT_ROLE_RDONLY FAIL ');
      end;
      DBMS_OUTPUT.PUT_LINE('');
      DBMS_OUTPUT.PUT_LINE('');
   end if;
   /*
   || Check PWRPLANT Table Privileges
   */
   DBMS_OUTPUT.PUT_LINE('/* Check PWRPLANT Table Privileges */');
   DBMS_OUTPUT.PUT_LINE('');
   DBMS_OUTPUT.PUT_LINE('Table Name                  Privilege     Status');
   DBMS_OUTPUT.PUT_LINE('____________________     ______________   ______');
   /* Check for V_$SESSION */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'V_$SESSION'
         and PRIVILEGE = 'SELECT';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                   ' || L_PRIVILEGE || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('V_$SESSION                                 FAIL');
   end;
   /* Check for V_$PROCESS */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'V_$PROCESS'
         and PRIVILEGE = 'SELECT';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                   ' || L_PRIVILEGE || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('V_$PROCESS                                 FAIL');
   end;
   /* Check for V_$SESSTAT */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'V_$SESSTAT'
         and PRIVILEGE = 'SELECT';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                   ' || L_PRIVILEGE || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('V_$SESSTAT                                 FAIL');
   end;
   /* Check for V_$PARAMETER */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'V_$PARAMETER'
         and PRIVILEGE = 'SELECT';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                 ' || L_PRIVILEGE || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('V_$PARAMETER                               FAIL');
   end;
   /* Check for V_$INSTANCE */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'V_$INSTANCE'
         and PRIVILEGE = 'SELECT';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                  ' || L_PRIVILEGE || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('V_$INSTANCE                                FAIL');
   end;
   /* Check for DBA_PROFILES */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'DBA_PROFILES'
         and PRIVILEGE = 'SELECT';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                 ' || L_PRIVILEGE || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('DBA_PROFILES                               FAIL');
   end;
   /* Check for USER_ROLE_PRIVS */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'USER_ROLE_PRIVS'
         and PRIVILEGE = 'SELECT';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '               ' || L_PRIVILEGE || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('USER_ROLE_PRIVS                            FAIL');
   end;
   /* Check for DBA_USERS */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'DBA_USERS'
         and PRIVILEGE = 'SELECT';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                    ' || L_PRIVILEGE || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('DBA_USERS                                  FAIL');
   end;
   /* Check for DBA_PROXIES */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'DBA_PROXIES'
         and PRIVILEGE = 'SELECT';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                  ' || L_PRIVILEGE || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('DBA_PROXIES                                FAIL');
   end;
   /* Check for UTL_RAW */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'UTL_RAW'
         and PRIVILEGE = 'EXECUTE';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                      ' || L_PRIVILEGE || '        OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('UTL_RAW                                    FAIL');
   end;
   /* Check for UTL_SMTP */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'UTL_SMTP'
         and PRIVILEGE = 'EXECUTE';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                     ' || L_PRIVILEGE || '        OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('UTL_SMTP                                   FAIL');
   end;
   /* Check for UTL_ENCODE */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'UTL_ENCODE'
         and PRIVILEGE = 'EXECUTE';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                   ' || L_PRIVILEGE || '        OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('UTL_ENCODE                                 FAIL');
   end;
   /* Check for DBMS_LOB */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'DBMS_LOB'
         and PRIVILEGE = 'EXECUTE';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                     ' || L_PRIVILEGE || '        OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('DBMS_LOB                                   FAIL');
   end;
   /* Check for DBMS_JOB */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'DBMS_JOB'
         and PRIVILEGE = 'EXECUTE';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '                     ' || L_PRIVILEGE || '        OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('DBMS_JOB                                   FAIL');
   end;
   /* Check for DBMS_OBFUSCATION_TOOLKIT */
   begin
      select TABLE_NAME, PRIVILEGE
        into L_TABLE_NAME, L_PRIVILEGE
        from USER_TAB_PRIVS
       where GRANTEE = 'PWRPLANT'
         and TABLE_NAME = 'DBMS_OBFUSCATION_TOOLKIT'
         and PRIVILEGE = 'EXECUTE';
      DBMS_OUTPUT.PUT_LINE(L_TABLE_NAME || '     ' || L_PRIVILEGE || '        OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('DBMS_OBFUSCATION_TOOLKIT                   FAIL');
   end;
   DBMS_OUTPUT.PUT_LINE('');
   DBMS_OUTPUT.PUT_LINE('');
   /*
   || Check Roles granted to PWRPLANT
   */
   DBMS_OUTPUT.PUT_LINE('/* Check Roles granted to PWRPLANT */');
   DBMS_OUTPUT.PUT_LINE('');
   DBMS_OUTPUT.PUT_LINE('Granted Roles          Admin Option   Default Role   Status');
   DBMS_OUTPUT.PUT_LINE('____________________   ____________   ____________   ______');
   /* Check for DBA */
   begin
      select GRANTED_ROLE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION), DEFAULT_ROLE
        into L_GRANTED_ROLE, L_ADMIN_OPTION, L_DEFAULT_ROLE
        from USER_ROLE_PRIVS
       where USERNAME = 'PWRPLANT'
         and GRANTED_ROLE = 'DBA';
      DBMS_OUTPUT.PUT_LINE(L_GRANTED_ROLE || '                         ' || L_ADMIN_OPTION ||
                           '             ' || L_DEFAULT_ROLE || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('DBA                                                   FAIL');
   end;
   /* Check for PWRPLANT_ROLE_USER */
   begin
      select GRANTED_ROLE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION), DEFAULT_ROLE
        into L_GRANTED_ROLE, L_ADMIN_OPTION, L_DEFAULT_ROLE
        from USER_ROLE_PRIVS
       where USERNAME = 'PWRPLANT'
         and GRANTED_ROLE = 'PWRPLANT_ROLE_USER';
      DBMS_OUTPUT.PUT_LINE(L_GRANTED_ROLE || '          ' || L_ADMIN_OPTION || '            ' ||
                           L_DEFAULT_ROLE || '          OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('PWRPLANT_ROLE_USER                                    FAIL');
   end;
   /* Check for PWRPLANT_ROLE_DEV */
   begin
      select GRANTED_ROLE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION), DEFAULT_ROLE
        into L_GRANTED_ROLE, L_ADMIN_OPTION, L_DEFAULT_ROLE
        from USER_ROLE_PRIVS
       where USERNAME = 'PWRPLANT'
         and GRANTED_ROLE = 'PWRPLANT_ROLE_DEV';
      DBMS_OUTPUT.PUT_LINE(L_GRANTED_ROLE || '           ' || L_ADMIN_OPTION || '            ' ||
                           L_DEFAULT_ROLE || '          OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('PWRPLANT_ROLE_DEV                                     FAIL');
   end;
   /* Check for PWRPLANT_ROLE_ADMIN */
   begin
      select GRANTED_ROLE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION), DEFAULT_ROLE
        into L_GRANTED_ROLE, L_ADMIN_OPTION, L_DEFAULT_ROLE
        from USER_ROLE_PRIVS
       where USERNAME = 'PWRPLANT'
         and GRANTED_ROLE = 'PWRPLANT_ROLE_ADMIN';
      DBMS_OUTPUT.PUT_LINE(L_GRANTED_ROLE || '         ' || L_ADMIN_OPTION || '             ' ||
                           L_DEFAULT_ROLE || '          OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('PWRPLANT_ROLE_ADMIN                                   FAIL');
   end;
   /* Check for SELECT_CATALOG_ROLE */
   begin
      select GRANTED_ROLE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION), DEFAULT_ROLE
        into L_GRANTED_ROLE, L_ADMIN_OPTION, L_DEFAULT_ROLE
        from USER_ROLE_PRIVS
       where USERNAME = 'PWRPLANT'
         and GRANTED_ROLE = 'SELECT_CATALOG_ROLE';
      DBMS_OUTPUT.PUT_LINE(L_GRANTED_ROLE || '         ' || L_ADMIN_OPTION || '             ' ||
                           L_DEFAULT_ROLE || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('SELECT_CATALOG_ROLE                                   FAIL');
   end;
   /* Check for CONNECT */
   begin
      select GRANTED_ROLE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION), DEFAULT_ROLE
        into L_GRANTED_ROLE, L_ADMIN_OPTION, L_DEFAULT_ROLE
        from USER_ROLE_PRIVS
       where USERNAME = 'PWRPLANT'
         and GRANTED_ROLE = 'CONNECT';
      DBMS_OUTPUT.PUT_LINE(L_GRANTED_ROLE || '                     ' || L_ADMIN_OPTION ||
                           '             ' || L_DEFAULT_ROLE || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('CONNECT                                               FAIL');
   end;
   DBMS_OUTPUT.PUT_LINE('');
   DBMS_OUTPUT.PUT_LINE('');
   /*
   || Check System Privileges granted to PWRPLANT
   */
   DBMS_OUTPUT.PUT_LINE('/* Check System Privileges granted to PWRPLANT */');
   DBMS_OUTPUT.PUT_LINE('');
   DBMS_OUTPUT.PUT_LINE('Privilege              Admin Option   Status');
   DBMS_OUTPUT.PUT_LINE('____________________   ____________   ______');
   /* Check for CREATE SESSION */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'CREATE SESSION';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '              ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('CREATE SESSION                         FAIL');
   end;
   /* Check for CREATE TABLE */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'CREATE TABLE';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '                ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('CREATE TABLE                           FAIL');
   end;
   /* Check for CREATE VIEW */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'CREATE VIEW';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '                 ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('CREATE TABLE                           FAIL');
   end;
   /* Check for CREATE PUBLIC SYNONYM */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'CREATE PUBLIC SYNONYM';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '       ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('CREATE PUBLIC SYNONYM                  FAIL');
   end;
   /* Check for DROP PUBLIC SYNONYM */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'DROP PUBLIC SYNONYM';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '         ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('DROP PUBLIC SYNONYM                    FAIL');
   end;
   /* Check for CREATE PROCEDURE */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'CREATE PROCEDURE';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '            ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('CREATE PROCEDURE                       FAIL');
   end;
   /* Check for CREATE SEQUENCE */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'CREATE SEQUENCE';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '             ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('CREATE SEQUENCE                        FAIL');
   end;
   /* Check for CREATE TRIGGER */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'CREATE TRIGGER';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '              ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('CREATE TRIGGER                         FAIL');
   end;
   /* Check for SELECT ANY TABLE */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'SELECT ANY TABLE';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '            ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('SELECT ANY TABLE                       FAIL');
   end;
   /* Check for DROP USER */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'DROP USER';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '                   ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('DROP USER                              FAIL');
   end;
   /* Check for CREATE USER */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'CREATE USER';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '                 ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('CREATE USER                            FAIL');
   end;
   /* Check for ALTER USER */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'ALTER USER';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '                  ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('ALTER USER                             FAIL');
   end;
   /* Check for CREATE TYPE */
   begin
      select PRIVILEGE, DECODE(ADMIN_OPTION, 'NO', 'NO ', ADMIN_OPTION)
        into L_PRIVILEGE, L_ADMIN_OPTION
        from USER_SYS_PRIVS
       where USERNAME = 'PWRPLANT'
         and PRIVILEGE = 'CREATE TYPE';
      DBMS_OUTPUT.PUT_LINE(L_PRIVILEGE || '                  ' || L_ADMIN_OPTION || '         OK');
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE('CREATE TYPE                             FAIL');
   end;

   /*
   || Check for disabled constraints
   */
   DBMS_OUTPUT.PUT_LINE('/* Check for disabled constraints */');
   DBMS_OUTPUT.PUT_LINE('');
   DBMS_OUTPUT.PUT_LINE('Constraint Name                T Table Name                     R Constraint Name              Status');
   DBMS_OUTPUT.PUT_LINE('______________________________ _ ______________________________ ______________________________ _____________');

begin
   for CSR_SQL in (
select CONSTRAINT_NAME,
         CONSTRAINT_TYPE,
         TABLE_NAME,
         R_CONSTRAINT_NAME,
         STATUS
 from ALL_CONSTRAINTS
where owner = 'PWRPLANT'
    and status <> 'ENABLED')
loop

   DBMS_OUTPUT.PUT_LINE(rpad(CSR_SQL.CONSTRAINT_NAME,31)||rpad(CSR_SQL.CONSTRAINT_TYPE,2)||rpad( CSR_SQL.TABLE_NAME,31)|| rpad(CSR_SQL.R_CONSTRAINT_NAME,31)|| CSR_SQL.STATUS);
end loop;
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('SQLCODE = '|| sqlcode || ' SQLERRM = '|| sqlerrm);
   end;
end;
/

SET ECHO ON
SPOOL OFF