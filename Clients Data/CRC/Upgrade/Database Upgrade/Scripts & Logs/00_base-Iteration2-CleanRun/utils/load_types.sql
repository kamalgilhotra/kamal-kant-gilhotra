/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.load_types.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: load_types.sql
|| Description: Create all the PowerPlant database types.
||============================================================================
|| Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.1.10  10/14/2008 Lee Quinn      Created
|| 10.2.1.4 06/10/2010 Lee Quinn      Point Release
||============================================================================
*/

@&&PP_SCRIPT_PATH.types/T_STRING_AGG.sql
@&&PP_SCRIPT_PATH.types/T_PRODUCT.sql

SPOOL OFF
