SET ECHO ON
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.create_all_synonyms_view.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: create_all_synonyms_view.sql
|| Description: Doc ID:  Note 377037.1 Selects Against ALL_SYNONYMS Perform Badly
||              Applies to: 10.2.0.1 to 10.2.0.4
||              This script will create a new view called ALL_SYNONYMS_920X and
||              then create private synonyms for every PowerPlant user user to
||              access this view as ALL_SYNONYMS
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| -------- ---------- -------------- -----------------------------------------
|| V10      06/22/2011 Lee Quinn      Created
||============================================================================
*/

-- Log on as SYS as sysdba
alter system set optimizer_secure_view_merging = FALSE SCOPE = BOTH;

create or replace view SYS.ALL_SYNONYMS_920X
(OWNER, SYNONYM_NAME, TABLE_OWNER, TABLE_NAME, DB_LINK)
as
select U.NAME, O.NAME, S.OWNER, S.NAME, S.NODE
  from SYS.USER$ U, SYS.SYN$ S, SYS.OBJ$ O
 where O.OBJ# = S.OBJ#
   and O.TYPE# = 5
   and O.OWNER# = U.USER#
   and (O.OWNER# in (USERENV('SCHEMAID'), 1 /* PUBLIC */) /* user's private, any public */
        or /* user has any privs on base object */
        exists
        (select null
           from SYS.OBJAUTH$ BA, SYS.OBJ$ BO, SYS.USER$ BU
          where BU.NAME = S.OWNER
            and BO.NAME = S.NAME
            and BU.USER# = BO.OWNER#
            and BA.OBJ# = BO.OBJ#
            and (BA.GRANTEE# in (select KZSROROL from X$KZSRO) or BA.GRANTOR# = USERENV('SCHEMAID'))) or /* user has system privileges */
        exists (select null
                  from V$ENABLEDPRIVS
                 where PRIV_NUMBER in (-45 /* LOCK ANY TABLE */,
                                       -47 /* SELECT ANY TABLE */,
                                       -48 /* INSERT ANY TABLE */,
                                       -49 /* UPDATE ANY TABLE */,
                                       -50 /* DELETE ANY TABLE */)));

grant select on SYS.ALL_SYNONYMS_920X to PUBLIC;

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   V_RCOUNT  number := 0;
   V_RERRORS number := 0;
begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
   --Replaces the ALL_SYNONYMS view for the new ALL_SYNONYMS_920X view by creating a
   --private synonym called ALL_SYNONYMS that points to the new ALL_SYNONYMS_920X view
   --for each PowerPlant user.
   for CSR_CREATE_SQL in (select 'create or replace synonym ' || GRANTEE ||
                                 '.all_synonyms for sys.all_synonyms_920x' CREATE_SQL
                            from DBA_ROLE_PRIVS R, ALL_USERS U
                           where U.USERNAME = R.GRANTEE
                             and U.USERNAME <> 'SYS'
                             and U.USERNAME <> 'SYSTEM'
                             and (R.GRANTED_ROLE = 'PWRPLANT_ROLE_DEV' or
                                  R.GRANTED_ROLE = 'PWRPLANT_ROLE_USER' or
                                  R.GRANTED_ROLE = 'PWRPLANT_ROLE_RDONLY')
                           group by GRANTEE
                           order by GRANTEE)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE(SUBSTR(PPCERR || 'SQLCODE = ' || sqlcode || ' SQLERRM = ' ||
                                        sqlerrm,
                                        1,
                                        255));
      end;
   end loop;

   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' Private Synonyms created.');

   -- SHOW ERRORS and Log Run
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RERRORS || ' Errors in create_all_synonyms_view.sql.');

   insert into PWRPLANT.PP_UPDATE_FLEX
      (COL_ID, TYPE_NAME, FLEXVCHAR1)
      select NVL(max(COL_ID), 0) + 1, 'UPDATE_SCRIPTS', 'create_all_synonyms_view.sql'
        from PWRPLANT.PP_UPDATE_FLEX;
   commit;

end;
/

SPOOL OFF