SET ECHO ON
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.create_grants.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: create_grants.sql
|| Description: Create Grants for TABLES, VIEWS, SEQUENCES, PROCEDURES,
||              PACKAGES and FUNCTIONS.
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| V10     10/23/2007 Lee Quinn      Created
|| V10.2   12/19/2007 Lee Quinn      Added logging
|| V10.5   03/06/2008 Lee Quinn      Removed PWRPLANT_ROLE_USER
|| V10.6   03/12/2008 Lee Quinn      Added Functions
|| V10.1   08/01/2008 Lee Quinn      Added archive_tax grant to PUBLIC
|| V10.2   03/24/2009 Lee Quinn      Version update
||============================================================================
*/

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   V_RCOUNT  number;
   V_RERRORS number;

begin
   DBMS_OUTPUT.ENABLE(buffer_size => NULL);
   -- Log the start of the run
   insert into PWRPLANT.PP_UPDATE_FLEX
      (COL_ID,
       TYPE_NAME,
       FLEXVCHAR1,
       FLEXVCHAR2)
      select NVL(max(COL_ID), 0) + 1,
             'UPDATE_SCRIPTS',
             'create_grants.sql',
             'Status = Running'
        from PWRPLANT.PP_UPDATE_FLEX;
   commit;

   V_RERRORS := 0;
   /*
   || TABLE GRANTS for PWRPLANT_ROLE_DEV
   */
   DBMS_OUTPUT.PUT_LINE('/* TABLE Grants for PWRPLANT_ROLE_DEV */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'grant all on PWRPLANT."' || OBJECT_NAME ||
                                 '" to PWRPLANT_ROLE_DEV' CREATE_SQL
                            from ALL_OBJECTS
                           where OWNER = 'PWRPLANT'
                             and OBJECT_TYPE = 'TABLE'
                             and OBJECT_NAME not like 'BIN$%'
                             and GENERATED <> 'Y'
                           order by OBJECT_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE(substr('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm, 1, 255));
            null;
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' TABLE Grants created for PWRPLANT_ROLE_DEV.');
   DBMS_OUTPUT.NEW_LINE;
   /*
   || VIEW GRANTS
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('/* VIEW Grants for PWRPLANT_ROLE_DEV */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'grant all on PWRPLANT."' || OBJECT_NAME ||
                                 '" to PWRPLANT_ROLE_DEV' CREATE_SQL
                            from ALL_OBJECTS
                           where OWNER = 'PWRPLANT'
                             and OBJECT_TYPE = 'VIEW'
                             and OBJECT_NAME not like 'BIN$%'
                           order by OBJECT_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE(substr('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm, 1, 255));
            null;
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' VIEW Grants created for PWRPLANT_ROLE_DEV.');
   DBMS_OUTPUT.NEW_LINE;
   /*
   || SEQUENCE GRANTS for PWRPLANT_ROLE_DEV
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('/* SEQUENCE Grants for PWRPLANT_ROLE_DEV */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'grant all on PWRPLANT."' || OBJECT_NAME ||
                                 '" to PWRPLANT_ROLE_DEV' CREATE_SQL
                            from ALL_OBJECTS
                           where OWNER = 'PWRPLANT'
                             and OBJECT_TYPE = 'SEQUENCE'
                             and OBJECT_NAME not like 'BIN$%'
                           order by OBJECT_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE(substr('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm, 1, 255));
            null;
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' SEQUENCE Grants created for PWRPLANT_ROLE_DEV.');
   DBMS_OUTPUT.NEW_LINE;
   /*
   || PROCEDURE GRANTS for PWRPLANT_ROLE_DEV
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('/* PROCEDURE Grants for PWRPLANT_ROLE_DEV */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'grant all on PWRPLANT."' || OBJECT_NAME ||
                                 '" to PWRPLANT_ROLE_DEV' CREATE_SQL
                            from ALL_OBJECTS
                           where OWNER = 'PWRPLANT'
                             and OBJECT_TYPE = 'PROCEDURE'
                             and OBJECT_NAME not like 'BIN$%'
                           order by OBJECT_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE(substr('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm, 1, 255));
            null;
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' PROCEDURE Grants created for PWRPLANT_ROLE_DEV.');
   DBMS_OUTPUT.NEW_LINE;
   /*
   || PACKAGE GRANTS for PWRPLANT_ROLE_DEV
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('/* PACKAGE Grants for PWRPLANT_ROLE_DEV */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'grant all on PWRPLANT."' || OBJECT_NAME ||
                                 '" to PWRPLANT_ROLE_DEV' CREATE_SQL
                            from ALL_OBJECTS
                           where OWNER = 'PWRPLANT'
                             and OBJECT_TYPE = 'PACKAGE'
                             and OBJECT_NAME not like 'BIN$%'
                           order by OBJECT_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE(substr('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm, 1, 255));
            null;
      end;
   end loop;

   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' PACKAGE Grants created for PWRPLANT_ROLE_DEV.');
   /*
   || FUNCTION GRANTS for PWRPLANT_ROLE_DEV
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('/* FUNCTION Grants for PWRPLANT_ROLE_DEV */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'grant all on PWRPLANT."' || OBJECT_NAME ||
                                 '" to PWRPLANT_ROLE_DEV' CREATE_SQL
                            from ALL_OBJECTS
                           where OWNER = 'PWRPLANT'
                             and OBJECT_TYPE = 'FUNCTION'
                             and OBJECT_NAME not like 'BIN$%'
                           order by OBJECT_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE(substr('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm, 1, 255));
            null;
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' FUNCTION Grants created for PWRPLANT_ROLE_DEV.');
   DBMS_OUTPUT.NEW_LINE;

   /*
   || FUNCTION - pwrplant_admin GRANT for PWRPLANT_ROLE_USER
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('/* FUNCTION pwrplant_admin Grant for PWRPLANT_ROLE_USER */');

   begin
      V_RCOUNT := 0;
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || 'grant execute on pwrplant_admin to pwrplant_role_user;');
      execute immediate 'grant execute on pwrplant_admin to pwrplant_role_user';
   exception
      when others then
         V_RERRORS := V_RERRORS + 1;
         DBMS_OUTPUT.PUT_LINE(PPCERR);
         DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' ||
                              'grant execute on pwrplant_admin to pwrplant_role_user');
         DBMS_OUTPUT.PUT_LINE(substr('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm, 1, 255));
         null;
   end;

   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' FUNCTION Grants created for PWRPLANT_ROLE_USER.');

   /*
   || PACKAGE - archive_tax GRANT for PUBLIC
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('/* PACKAGE archive_tax Grant for PUBLIC */');

   begin
      V_RCOUNT := 0;
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || 'grant execute on archive_tax to public;');
      execute immediate 'grant execute on archive_tax to public';
   exception
      when others then
         V_RERRORS := V_RERRORS + 1;
         DBMS_OUTPUT.PUT_LINE(PPCERR);
         DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || 'grant execute on archive_tax to public');
         DBMS_OUTPUT.PUT_LINE(substr('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm, 1, 255));
         null;
   end;

   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' PACKAGE Grants created for PUBLIC.');

   /*
   || SHOW ERRORS and Log Run
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RERRORS || ' Errors in Create Grants.');

   update PWRPLANT.PP_UPDATE_FLEX
      set FLEXVCHAR2 = 'Status = Complete', FLEXVCHAR3 = V_RERRORS || ' ERROR(s)'
    where TYPE_NAME = 'UPDATE_SCRIPTS'
      and COL_ID in (select max(COL_ID)
                       from PWRPLANT.PP_UPDATE_FLEX
                      where FLEXVCHAR1 = 'create_grants.sql'
                        and TYPE_NAME = 'UPDATE_SCRIPTS');
   commit;
end;
/
/*
|| END Create Grants
*/
SPOOL OFF