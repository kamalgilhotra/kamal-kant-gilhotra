SET ECHO ON
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.revoke_grants_pwrplant_role_user.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: revoke_grants_pwrplant_role_user.sql
|| Description: Revoke Grants for TABLES, VIEWS, SEQUENCES, PROCEDURES,
||              PACKAGES and FUNCTIONS from PWRPLANT_ROLE_USER
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- -----------------------------------------
|| 10.2.1.1 10/28/2009 Lee Quinn      Created
||============================================================================
*/

declare
   PPCMSG varchar2(10) := 'PPC-MSG' || '> ';
   PPCERR varchar2(10) := 'PPC-ERR' || '> ';
   PPCSQL varchar2(10) := 'PPC-SQL' || '> ';
   PPCORA varchar2(10) := 'PPC-ORA' || '-> ';

   V_RCOUNT  number;
   V_RERRORS number;

begin
   DBMS_OUTPUT.ENABLE(buffer_size => NULL);
   -- Log the start of the run
   insert into PWRPLANT.PP_UPDATE_FLEX
      (COL_ID,
       TYPE_NAME,
       FLEXVCHAR1,
       FLEXVCHAR2)
      select NVL(max(COL_ID), 0) + 1,
             'UPDATE_SCRIPTS',
             'revoke_grants_pwrplant_role_user.sql',
             'Status = Running'
        from PWRPLANT.PP_UPDATE_FLEX;
   commit;

   V_RERRORS := 0;
   /*
   || REVOKE GRANTS for PWRPLANT_ROLE_USER
   */
   DBMS_OUTPUT.PUT_LINE('/* Revoke Grants from PWRPLANT_ROLE_USER */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'revoke all on ' || TABLE_NAME || ' from PWRPLANT_ROLE_USER' REVOKE_SQL
                            from (select distinct TABLE_NAME
                                    from DBA_TAB_PRIVS
                                   where GRANTEE = 'PWRPLANT_ROLE_USER'
                                     and GRANTOR = 'PWRPLANT'
                                     and TABLE_NAME not like 'BIN$%')
                                   order by TABLE_NAME)

   loop
      V_RCOUNT := V_RCOUNT + 1;
      begin
         execute immediate CSR_CREATE_SQL.REVOKE_SQL;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.REVOKE_SQL || ';');
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR || CSR_CREATE_SQL.REVOKE_SQL);
            DBMS_OUTPUT.PUT_LINE(substr(PPCORA || sqlcode || ' SQLERRM = ' || sqlerrm,255));
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' Revokes processed for PWRPLANT_ROLE_USER.');
   DBMS_OUTPUT.NEW_LINE;

   /*
   || Function pwrplant_admin GRANT for PWRPLANT_ROLE_USER
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('/* FUNCTION pwrplant_admin Grant for PWRPLANT_ROLE_USER */');

   begin
      V_RCOUNT := 0;
      V_RCOUNT := V_RCOUNT + 1;
      execute immediate 'grant execute on pwrplant_admin to pwrplant_role_user';
      DBMS_OUTPUT.PUT_LINE(PPCSQL || 'grant execute on pwrplant_admin to pwrplant_role_user;');
   exception
      when others then
         V_RERRORS := V_RERRORS + 1;
         DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' ||
                              'grant execute on pwrplant_admin to pwrplant_role_user;');
         DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
         null;
   end;

   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(V_RCOUNT || ' FUNCTION Grants created for PWRPLANT_ROLE_USER.');

   /*
   || SHOW ERRORS and Log Run
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RERRORS || ' Errors in Revoke Grants.');

   update PWRPLANT.PP_UPDATE_FLEX
      set FLEXVCHAR2 = 'Status = Complete', FLEXVCHAR3 = V_RERRORS || ' ERROR(s)'
    where TYPE_NAME = 'UPDATE_SCRIPTS'
      and COL_ID in (select max(COL_ID)
                       from PWRPLANT.PP_UPDATE_FLEX
                      where FLEXVCHAR1 = 'revoke_grants_pwrplant_role_user.sql'
                        and TYPE_NAME = 'UPDATE_SCRIPTS');
   commit;
end;
/
/*
|| END Revoke Grants from PWRPLANT_ROLE_USER
*/
SPOOL OFF