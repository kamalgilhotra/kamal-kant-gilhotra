SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.enable_audit_triggers.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: enable_audit_triggers.sql
|| Description: Enable all Audit Triggers that were disabled by the previous
||              run of disable_audit_triggers.sql
||============================================================================
|| Copyright (C) 2007 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| V10     12/18/2007 Lee Quinn      Created
||============================================================================
*/

/*
|| BEGIN Enable Audit Triggers
*/
declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   TRIGGER_DOES_NOT_EXIST exception;
   pragma exception_init(TRIGGER_DOES_NOT_EXIST, -4080);
   V_RCOUNT       number;
   V_RERRORS      number;
   V_WARNINGS     number;
   V_RUN_SEQUENCE number;

begin
   DBMS_OUTPUT.ENABLE(2000000);
   -- Get Sequence for the Run, use the sequence of the last disable_audit_trigger run
   begin
      select max(FLEXNUM1)
        into V_RUN_SEQUENCE
        from PP_UPDATE_FLEX
       where TYPE_NAME = 'AUDIT_TRIGGERS';
      if V_RUN_SEQUENCE is null then
         raise NO_DATA_FOUND;
      end if;
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE(PPCERR || 'Previous run of Disable_audit_triggers not found');
         return;
   end;
   -- Log the start of the run
   insert into PP_UPDATE_FLEX
      (COL_ID,
       TYPE_NAME,
       FLEXVCHAR1,
       FLEXVCHAR2,
       FLEXNUM1)
      select NVL(max(COL_ID), 0) + 1,
             'UPDATE_SCRIPTS',
             'enable_audit_triggers.sql',
             'Status = Running',
             V_RUN_SEQUENCE
        from PP_UPDATE_FLEX;
   commit;

   -- Enable triggers that are disabled
   V_RCOUNT   := 0;
   V_RERRORS  := 0;
   V_WARNINGS := 0;

   for ALTER_TRIGGER_SQL in (select 'alter trigger ' || FLEXVCHAR1 || ' enable' SQL_LINE,
                                    FLEXVCHAR1 TRIGGER_NAME
                               from PP_UPDATE_FLEX
                              where TYPE_NAME = 'AUDIT_TRIGGERS'
                                and FLEXVCHAR2 = 'ENABLED'
                                and FLEXNUM1 = V_RUN_SEQUENCE
                              order by FLEXVCHAR1)
   loop
      begin
         execute immediate ALTER_TRIGGER_SQL.SQL_LINE;
         DBMS_OUTPUT.PUT_LINE(PPCSQL || ALTER_TRIGGER_SQL.SQL_LINE || ';');
         V_RCOUNT := V_RCOUNT + 1;
      exception
         when TRIGGER_DOES_NOT_EXIST then
            V_WARNINGS := V_WARNINGS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCMSG || ALTER_TRIGGER_SQL.TRIGGER_NAME ||
                                 ' no longer exists.');
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE('ERROR SQL ' || ALTER_TRIGGER_SQL.SQL_LINE || ';');
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
      end;
   end loop;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' TRIGGER(s) altered.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RERRORS || ' ERROR(s)');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_WARNINGS || ' WARNING(s)');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || (V_RCOUNT + V_RERRORS + V_WARNINGS) || ' Total TRIGGER(s) processed.');
   -- Update the log record
   update PP_UPDATE_FLEX
      set FLEXVCHAR2 = 'Status = Complete',
          FLEXVCHAR3 = V_RCOUNT || ' TRIGGER(s) altered.',
          FLEXVCHAR4 = V_RERRORS || ' ERROR(s)',
          FLEXVCHAR5 = V_WARNINGS || ' WARNING(s)',
          FLEXVCHAR6 = V_RCOUNT + V_RERRORS + V_WARNINGS || ' Total TRIGGER(s) processed.',
          FLEXVCHAR7 = 'Run ID in FLEXNUM1 field'
    where FLEXNUM1 = V_RUN_SEQUENCE
      and TYPE_NAME = 'UPDATE_SCRIPTS'
      and COL_ID in (select max(COL_ID)
                       from PP_UPDATE_FLEX
                      where FLEXNUM1 = V_RUN_SEQUENCE
                        and TYPE_NAME = 'UPDATE_SCRIPTS');
   commit;

exception
   when others then
      DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
end;
/
/*
|| END Enable Audit Triggers
*/

SPOOL OFF