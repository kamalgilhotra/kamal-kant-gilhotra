SET ECHO OFF
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.drop_all_timestamp_triggers.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: drop_all_timestamp_triggers.sql
|| Description: Creates and runs DDL to Drop all Timestamp Triggers
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- -----------------------------------------
|| 10.3.5.0 09/05/2012 Lee Quinn      Created
||============================================================================
*/

create table TEMP_TRIG_COMPARE
(
 TRIGGER_NAME      varchar2(30),
 TRIGGER_TYPE      varchar2(16),
 TRIGGERING_EVENT  varchar2(227),
 BASE_OBJECT_TYPE  varchar2(16),
 TABLE_NAME        varchar2(30),
 COLUMN_NAME       varchar2(4000),
 REFERENCING_NAMES varchar2(128),
 WHEN_CLAUSE       varchar2(4000),
 STATUS            varchar2(8),
 DESCRIPTION       varchar2(4000),
 ACTION_TYPE       varchar2(11),
 TRIGGER_BODY      nclob
);

insert into TEMP_TRIG_COMPARE
   (TRIGGER_NAME,
    TRIGGER_TYPE,
    TRIGGERING_EVENT,
    BASE_OBJECT_TYPE,
    TABLE_NAME,
    COLUMN_NAME,
    REFERENCING_NAMES,
    WHEN_CLAUSE,
    STATUS,
    DESCRIPTION,
    ACTION_TYPE,
    TRIGGER_BODY)
   select TRIGGER_NAME,
          TRIGGER_TYPE,
          TRIGGERING_EVENT,
          BASE_OBJECT_TYPE,
          TABLE_NAME,
          COLUMN_NAME,
          REFERENCING_NAMES,
          WHEN_CLAUSE,
          STATUS,
          DESCRIPTION,
          ACTION_TYPE,
          TO_LOB(TRIGGER_BODY)
     from ALL_TRIGGERS
    where OWNER = 'PWRPLANT'
      and TRIGGER_NAME not like 'BIN$%';

/*
|| Dynamic SQL to DROP Old Triggers
*/

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   V_COUNT  number;
   V_ERRORS number;

begin
   DBMS_OUTPUT.ENABLE(2000000);
   V_COUNT  := 0;
   V_ERRORS := 0;
   -- Log the start of the run
   insert into PP_UPDATE_FLEX
      (COL_ID,
       TYPE_NAME,
       FLEXVCHAR1,
       FLEXVCHAR2)
      select NVL(max(COL_ID), 0) + 1,
             'UPDATE_SCRIPTS',
             'drop_timestamp_triggers.sql',
             'Status = Running'
        from PP_UPDATE_FLEX;
   commit;

   for CSR_CREATE_DDL in (select 'drop trigger ' || TRIGGER_NAME CREATE_DDL
                            from TEMP_TRIG_COMPARE
                           where INSTR(LOWER(TRIGGER_BODY), ':new.user_id') > 0
                             and INSTR(LOWER(TRIGGER_BODY), ':new.time_stamp') > 0
                             and TRIGGER_TYPE = 'BEFORE EACH ROW'
                             and TRIGGERING_EVENT = 'INSERT OR UPDATE'
                             and LENGTH(TRIGGER_BODY) < 110
                           order by TABLE_NAME, TRIGGER_NAME)
   loop
      begin
         V_COUNT := V_COUNT + 1;
         -- If you don't want to automatically drop the triggers then comment
         -- out the following line "execute immediate" and the script will just
         -- return the DDL that would have been run.
         execute immediate CSR_CREATE_DDL.CREATE_DDL;
         DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_DDL.CREATE_DDL || ';');
      exception
         when others then
            V_ERRORS := V_ERRORS + 1;
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_DDL.CREATE_DDL ||';');
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
      end;
   end loop;

   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_COUNT || ' Triggers Dropped.');
   DBMS_OUTPUT.NEW_LINE;

   /*
   || SHOW ERRORS and Log Run
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_ERRORS || ' Errors in drop_all_timestamp_triggers.sql.');

   update PP_UPDATE_FLEX
      set FLEXVCHAR2 = 'Status = Complete',
          FLEXVCHAR3 = V_ERRORS || ' ERROR(s)'
    where TYPE_NAME = 'UPDATE_SCRIPTS'
      and COL_ID in (select max(COL_ID)
                       from PP_UPDATE_FLEX
                      where FLEXVCHAR1 = 'drop_all_timestamp_triggers.sql'
                        and TYPE_NAME = 'UPDATE_SCRIPTS');
   commit;
end;
/

DROP TABLE temp_trig_compare;

SPOOL OFF
SET ECHO ON