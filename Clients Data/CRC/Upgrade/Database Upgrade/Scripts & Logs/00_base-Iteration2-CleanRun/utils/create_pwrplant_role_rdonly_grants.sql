SET ECHO OFF
SET SERVEROUTPUT ON
SET LINESIZE 120
/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

/*
||============================================================================
|| Application: PowerPlan
|| Object Name: create_pwrplant_role_rdonly_grants.sql
|| Description: Create the PWRPLANT_ROLE_RDONLY and assign grants.
||              This role can be used to make application users that have read
||              only access while running the application.
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10         05/30/2008 Lee Quinn      Created
|| 10.2       04/08/2009 Lee Quinn      Version update
|| 10.2.1.2.0 01/14/2010 Lee Quinn      Point release
|| 10.2.1.4.0 07/28/2010 Lee Quinn      Added cr_drilldown_keys
|| 10.2.1.6.0 03/11/2011 Lee Quinn      Added update privs on Query Save tables
||            05/24/2011 Lee Quinn      Changed to use password protected role
|| 10.3.2.0.0 08/08/2011 Lee Quinn      Added execute on PP_GL_TRANSACTION and PP_GL_TRANSACTION2
|| 10.3.4.2   07/06/2012 Lee Quinn      Ignore if role already exists
|| 10.3.5.0 0 08/03/2012 Lee Quinn      Removed TAX_ACCRUAL_REP_NS2FAS109_TEMP
|| 10.3.6.0 0 12/17/2012 Lee Quinn      Added dynamic grant of select, insert, update, delete on
||                                      all Global Temp tables and removed existing Temp table grants
||                                      from the bottom of the script.
||============================================================================
*/

/*
||============================================================================
||
|| Note: In version 10.2 and above ini file changes are NOT needed.  The application will try to
||       alter the users role to PWRPLANT_ROLE_DEV and if that fails then it will
||       try to alter the users role to PWRPLANT_ROLE_RDONLY.  This role is now supports
||       password protection.  The password should be the same for both the PWRPLANT_ROLE_DEV
||       and PWRPLANT_ROLE_RDONLY roles.
||
|| How to Setup PowerPlant to use the Read Only Role:
||
|| 1. Make sure the users get the pwrplant_role_user as default.
||
|| 2. Remove the PWRPLANT_ROLE_DEV role from the user if it exists.
||
|| 3. Need to grant PWRPLANT_ROLE_RDONLY role to the user(s).
||
|| 4. Make a new database entry in the pwrplant.ini file with the line
||
||    Developer_Role=PWRPLANT_ROLE_RDONLY.
||
|| Example pwrplant.ini file entry:
||
||      [Database1]
||      DBMS=o90
||      Database=Production Read-Only
||      UserId=
||      DatabasePassword=
||      LogPassword=
||      ServerName=ppprod
||      LogId=
||      DbParm=DisableBind=1,StaticBind=0,CommitOnDisconnect='No',pwdialog=1,PWExpDialog=1
||      Developer_Role=PWRPLANT_ROLE_RDONLY
||
||============================================================================
|| Select Privileges from PWRPLANT_ROLE_RDONLY to verify that it doesn't have
|| unneeded privileges.
||
||select ATP.TABLE_NAME OBJECT_NAME, AO.OBJECT_TYPE, STRING_AGG(PRIVILEGE) "PRIVILEGES"
||  from ALL_TAB_PRIVS ATP, ALL_OBJECTS AO
|| where AO.OWNER = 'PWRPLANT'
||   and AO.OBJECT_NAME = ATP.TABLE_NAME
||   and ATP.GRANTEE = 'PWRPLANT_ROLE_RDONLY'
||   and ATP.PRIVILEGE in ('INSERT', 'UPDATE', 'DELETE', 'ALTER', 'EXECUTE')
||   and AO.OBJECT_TYPE in ('SEQUENCE', 'PROCEDURE', 'PACKAGE', 'TABLE', 'VIEW', 'FUNCTION', 'TYPE')
|| group by ATP.TABLE_NAME, AO.OBJECT_TYPE
|| order by OBJECT_TYPE, ATP.TABLE_NAME;
||
|| If a user can't login then check the following substituting <RDONLY_USER_NAME> with the user having problems logging in.
||
|| select *
||   from ALL_TAB_PRIVS T1, (select GRANTED_ROLE from DBA_ROLE_PRIVS where GRANTEE = '<RDONLY_USER_NAME>') T2
||  where T1.TABLE_NAME in ('PP_COMPANY_SECURITY', 'COMPANY_SETUP', 'PP_COMPANY_SECURITY_TEMP')
||    and T1.GRANTEE = T2.GRANTED_ROLE
||  order by TABLE_NAME, GRANTEE
||
|| They should have INSERT, UPDATE, DELETE on PP_COMPANY_SECURITY_TEMP and SELECT on PP_COMPANY_SECURITY, COMPANY_SETUP
||============================================================================
*/

SPOOL &&PP_SCRIPT_PATH.create_pwrplant_role_rdonly_grants.log
SET ECHO ON
SET SERVEROUTPUT ON

-- Create the PWRPLANT_ROLE_RDONLY if it doesn't exist
-- Ignore error if it already exists
declare
   ROLE_EXISTS exception;
   pragma exception_init(ROLE_EXISTS, -01921);

begin
   execute immediate 'create role PWRPLANT_ROLE_RDONLY identified by Tr9uI22k';
exception
   when ROLE_EXISTS then
      DBMS_OUTPUT.PUT_LINE('The PWRPLANT_ROLE_RDONLY role already exists.');
end;
/

SET ECHO OFF
declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   V_RCOUNT  number;
   V_RERRORS number;

begin
   DBMS_OUTPUT.ENABLE(2000000);
   -- Log the start of the run
   insert into PWRPLANT.PP_UPDATE_FLEX
      (COL_ID,
       TYPE_NAME,
       FLEXVCHAR1,
       FLEXVCHAR2)
      select NVL(max(COL_ID), 0) + 1,
             'UPDATE_SCRIPTS',
             'create_pwrplant_role_rdonly.sql',
             'Status = Running'
        from PWRPLANT.PP_UPDATE_FLEX;
   commit;

   V_RERRORS := 0;
   /*
   || TABLE GRANTS for PWRPLANT_ROLE_RDONLY
   */
   DBMS_OUTPUT.PUT_LINE('/* TABLE Grants for PWRPLANT_ROLE_RDONLY */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'grant select on PWRPLANT."' || OBJECT_NAME ||
                                 '" to PWRPLANT_ROLE_RDONLY' CREATE_SQL
                            from ALL_OBJECTS
                           where OWNER = 'PWRPLANT'
                             and OBJECT_TYPE = 'TABLE'
                             and TEMPORARY = 'N'
                             and OBJECT_NAME not like 'BIN$%'
                             and GENERATED <> 'Y'
                           order by OBJECT_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
            null;
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' TABLE Grants created for PWRPLANT_ROLE_RDONLY.');
   DBMS_OUTPUT.NEW_LINE;
   /*
   || VIEW GRANTS
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('/* VIEW Grants for PWRPLANT_ROLE_RDONLY */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'grant select on PWRPLANT."' || OBJECT_NAME ||
                                 '" to PWRPLANT_ROLE_RDONLY' CREATE_SQL
                            from ALL_OBJECTS
                           where OWNER = 'PWRPLANT'
                             and OBJECT_TYPE = 'VIEW'
                             and OBJECT_NAME not like 'BIN$%'
                           order by OBJECT_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
            null;
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' VIEW Grants created for PWRPLANT_ROLE_RDONLY.');
   DBMS_OUTPUT.NEW_LINE;
   /*
   || Global Temp Tables GRANTS
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('/* Global Temp Table Grants for PWRPLANT_ROLE_RDONLY */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'grant select, insert, update, delete on PWRPLANT."' || OBJECT_NAME ||
                                 '" to PWRPLANT_ROLE_RDONLY' CREATE_SQL
                            from ALL_OBJECTS
                           where OWNER = 'PWRPLANT'
                             and OBJECT_TYPE = 'TABLE'
                             and TEMPORARY = 'Y'
                             and OBJECT_NAME not like 'BIN$%'
                           order by OBJECT_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
            null;
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' Global Temp Table Grants created for PWRPLANT_ROLE_RDONLY.');
   DBMS_OUTPUT.NEW_LINE;
   /*
   || TYPE GRANTS (Need execute to use type constructor). Our types are created with AUTHID CURRENT_USER, so this should be acceptable
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('/* Type Grants for PWRPLANT_ROLE_RDONLY */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'grant execute on PWRPLANT."' || OBJECT_NAME ||
                                 '" to PWRPLANT_ROLE_RDONLY' CREATE_SQL
                            from ALL_OBJECTS
                           WHERE OWNER = 'PWRPLANT'
                             and OBJECT_TYPE = 'TYPE'
                             AND OBJECT_NAME NOT LIKE 'BIN$%'
                             and object_name NOT LIKE 'SYS_PLSQL%'
                           order by OBJECT_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL);
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
            null;
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' Type Grants created for PWRPLANT_ROLE_RDONLY.');
   DBMS_OUTPUT.NEW_LINE;
   /*
   || SHOW ERRORS and Log Run
   */
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RERRORS || ' Errors in Create Grants.');

   update PWRPLANT.PP_UPDATE_FLEX
      set FLEXVCHAR2 = 'Status = Complete', FLEXVCHAR3 = V_RERRORS || ' ERROR(s)'
    where TYPE_NAME = 'UPDATE_SCRIPTS'
      and COL_ID in (select max(COL_ID)
                       from PWRPLANT.PP_UPDATE_FLEX
                      where FLEXVCHAR1 = 'create_pwrplant_role_rdonly.sql'
                        and TYPE_NAME = 'UPDATE_SCRIPTS');
   commit;
end;
/
/*
|| END Create Grants
*/

SET ECHO ON

grant select, insert, update, delete on PWRPLANT.PP_QUERY to PWRPLANT_ROLE_RDONLY;

grant select, insert, update on PWRPLANT.PP_SECURITY_LOG_RECORD to PWRPLANT_ROLE_RDONLY;

grant select, update on PWRPLANT.PP_SECURITY_USERS to PWRPLANT_ROLE_RDONLY;

grant select, insert, update, delete on PWRPLANT.PP_USER_PROFILE          to PWRPLANT_ROLE_RDONLY;
grant select, insert, update, delete on PWRPLANT.PP_USER_PROFILE_DETAIL   to PWRPLANT_ROLE_RDONLY;
grant select, insert, update, delete on PWRPLANT.PP_MYPP_USER_VALUES      to PWRPLANT_ROLE_RDONLY;

grant execute on PWRPLANT.AUDIT_TABLE_PKG to PWRPLANT_ROLE_RDONLY;
grant execute on PWRPLANT.PWRPLANT_ADMIN to PWRPLANT_ROLE_RDONLY;
grant execute on PWRPLANT.ANALYZE_TABLE to PWRPLANT_ROLE_RDONLY;

grant execute on PWRPLANT.PP_GL_TRANSACTION to PWRPLANT_ROLE_RDONLY;
grant execute on PWRPLANT.PP_GL_TRANSACTION2 to PWRPLANT_ROLE_RDONLY;

grant select on PWRPLANT.PWRPLANT1 to PWRPLANT_ROLE_RDONLY;
grant select on PWRPLANT.PWRPLANT2 to PWRPLANT_ROLE_RDONLY;
grant select on PWRPLANT.PWRPLANT3 to PWRPLANT_ROLE_RDONLY;

grant select, insert, update, delete on PWRPLANT.CR_RECORD_COUNT to PWRPLANT_ROLE_RDONLY;

grant select, insert, update, delete on PWRPLANT.CR_DRILLDOWN_KEYS to PWRPLANT_ROLE_RDONLY;

--Ability to save queries for Asset, Depr, WO/FP standard Queries
grant select, insert, update, delete on PWRPLANT.PP_QUERY_DW           to PWRPLANT_ROLE_RDONLY;
grant select, insert, update, delete on PWRPLANT.PP_QUERY_DW_COLS      to PWRPLANT_ROLE_RDONLY;
grant select, insert, update, delete on PWRPLANT.PP_QUERY_DW_FILTER    to PWRPLANT_ROLE_RDONLY;
grant select, insert, update, delete on PWRPLANT.PP_QUERY_DW_CC_VALUES to PWRPLANT_ROLE_RDONLY;

--Ability to save queries for the Any Queries:
grant select, insert, update, delete on PWRPLANT.PP_QUERY_DW_CC_VALUES        to PWRPLANT_ROLE_RDONLY;
grant select, insert, update, delete on PWRPLANT.PP_ANY_QUERY_CRITERIA        to PWRPLANT_ROLE_RDONLY;
grant select, insert, update, delete on PWRPLANT.PP_MYPP_TASK                 to PWRPLANT_ROLE_RDONLY;
grant select, insert, update, delete on PWRPLANT.PP_MYPP_TASK_STEP            to PWRPLANT_ROLE_RDONLY;
grant select, insert, update, delete on PWRPLANT.PP_ANY_QUERY_CRITERIA_FIELDS to PWRPLANT_ROLE_RDONLY;

--Ability to save queries for the Select screen Queries:
grant select, insert, update, delete on PWRPLANT.CPR_WHERE_CLAUSE to PWRPLANT_ROLE_RDONLY;

--Ability to log statistics
grant select, insert, update, delete on PWRPLANT.PP_STATISTICS to PWRPLANT_ROLE_RDONLY;
grant select, insert, update, delete on PWRPLANT.PP_STATS_LOGGING to PWRPLANT_ROLE_RDONLY;

--Ability to execute SQL statements via PP_MISC_PKG functions
grant execute on PWRPLANT.PP_MISC_PKG to PWRPLANT_ROLE_RDONLY;

SPOOL OFF
