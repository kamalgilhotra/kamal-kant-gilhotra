/*
   Verify that the Database server can access the Mail server outside of Oracle

   Unix/Linux Machine test

   Test connecting to Mail Server:
   telnet <IP> <PORT>
   ex. telnet 100.0.0.253 25

   Test sending a E-mail from command prompt:
   mail -v -s "Mail Test" lquinn@pwrplan.com

   Test Telnet to Mail Server
   telnet mx1.sherweb2010.com 25
   helo mx1.sherweb2010.com
   mail from: lquinn@pwrplan.com
   rcpt to: lquinn@pwrplan.com
   data
   this is a test
   .
   quit
*/

/* Test sending E-mail from Oracle using SQLPlus */
declare
   NUM number(22, 0);
begin
   NUM := PP_SEND_MAIL('lquinn@pwrplan.com', -- From E-mail
                       '',
                       '',
                       'Test Message',
                       'Test Message',
                       'lquinn@pwrplan.com', -- To E-mail
                       'mail.pwrplan.com', -- Mail Serevr
                       ' ');
end;
/

/* Test an E-mail with an attachment */
delete pp_mail_data where sessionid = userenv('SESSIONID');

insert into pp_mail_data(sessionid, filename,filedata)
   values (userenv('SESSIONID'), 'test_file', rawtohex('This is a test attachment'));

declare
   num number(22,0);

begin
   num := pp_send_mail('lquinn@pwrplan.com',
                       '',
                       '',
                       'Test Message',
                       'Test Message',
                       'lquinn@pwrplan.com',
                       'mail.pwrplan.com',
                       'test_file');
end;
/

