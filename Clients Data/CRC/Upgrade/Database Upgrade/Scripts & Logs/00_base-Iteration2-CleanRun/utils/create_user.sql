SET ECHO ON
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: create_user.sql
|| Description: Creates a user that can be added to the PowerPlant application.
||              After the user is created in Oracle, the PowerPlant administrator
||              will need to add that user to the application and assign application
||              security.
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| V10.0.0  10/09/2008 Lee Quinn      Created
||============================================================================
*/

/*
|| You can define the USER_NAME and USER_PASSWORD below and uncomment out the lines
|| or just run the script and it will prompt for the replacement variables.
*/

--DEFINE USER_NAME=''
--DEFINE USER_PASSWORD=''

create user &&USER_NAME identified by &&USER_PASSWORD
   default tablespace USERS
   temporary tablespace TEMP
   quota unlimited on USERS;

grant PWRPLANT_ROLE_DEV to &&USER_NAME;
grant PWRPLANT_ROLE_USER to &&USER_NAME;
alter user &&USER_NAME default role PWRPLANT_ROLE_USER;