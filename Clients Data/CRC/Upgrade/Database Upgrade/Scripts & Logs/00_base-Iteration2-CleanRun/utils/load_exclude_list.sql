/*
||============================================================================
|| Application: PowerPlan
|| Object Name: load_exclude_list.sql
|| Description: Load the temp_exclude_table_list.
||              This is used to filter in DBADMIN by module.  This is now used
||              for include lists as well as exclude lists.
||              This script can be run at any time.
||              The Base Tables List is used in the following update scripts:
||                   130_changedb_v10_user_id_18.sql
||                   132_changedb_v10_rename_constraints_make_sql.sql
||
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| V10.0.6    03/25/2008 Lee Quinn      Created
|| V10.0.7    03/28/2008 Lee Quinn      Point Release
|| V10.0.8    05/01/2008 Lee Quinn      Added 'REIMB_BILL_SUPPORT_CR', 'REIMB_CR_TARGET_CRITERIA'
|| V10.0.8    05/16/2008 Lee Quinn      Point Release
|| V10.1.1    12/11/2008 Lee Quinn      Added Include as well as Exclude lists
|| V10.1.2    01/05/2009 Lee Quinn      Point Release
|| V10.1.3    03/16/2009 Lee Quinn      Point Release
|| V10.2      04/07/2009 Lee Quinn      Version Release
|| 10.2.0.1   05/14/2009 Lee Quinn      Point Release
|| 10.2.1.1.0 11/06/2009 Lee Quinn      PT and Lease Changes
|| 10.2.1.2.0 01/04/2010 Lee Quinn      Point Release
|| 10.2.1.4.0 07/23/2010 Lee Quinn      Point Release
|| 10.2.1.5.0 09/14/2010 Lee Quinn      Point Release
|| 10.2.1.6.0 11/29/2010 Lee Quinn      Added TMP_TA_SUB_EXT_ROLL to Prov exclude list
|| 10.2.2.5.0 05/27/2011 Lee Quinn      Point Release
|| 10.2.2.6.0 05/27/2011 Lee Quinn      Point Release
|| 10.3.0.0.0 05/27/2011 Lee Quinn      Point Release
|| 10.3.0.1.0 05/27/2011 Lee Quinn      Point Release
|| 10.3.1.0.0 05/27/2011 Lee Quinn      Point Release
|| 10.3.1.0.1 06/22/2011 Lee Quinn      Point Release
|| 10.3.1.1.0 08/01/2011 Lee Quinn      Point Release
|| 10.3.2.0.0 09/01/2011 Lee Quinn      Point Release
|| 10.3.3.0.0 12/21/2011 Lee Quinn      Point Release
|| 10.3.3.1.0 01/23/2011 Lee Quinn      Point Release
|| 10.3.3.2.0 04/02/2012 Lee Quinn      Added CR_APPROVAL_STATUS, CR_APPROVAL_TYPE
|| 10.3.4.0.0 04/12/2012 Lee Quinn      Added more CR tables from Joseph, exclude PP_JOURNAL_LAYOUTS
|| 10.3.4.1.0 07/06/2012 Lee Quinn      Added point release
|| 10.3.5.0.0 08/31/2012 Lee Quinn      Added point release
|| 10.3.5.0.0 10/26/2012 Lee Quinn      Removed CR_OPEN_MONTH_NUMBER, CR_SCO_BILLING_TYPE_RATES
|| 10.4.0.0.0 05/07/2013 Lee Quinn      Added point release
|| 10.4.0.1.0 06/18/2013 Lee Quinn      Added Patch Release
|| 10.4.1.0.0 09/26/2013 Lee Quinn      Added point Release, Exclude TAX_ACCRUAL_JE_EXPORT, TAX_ACCRUAL_JE_TRANSLATE
||                                      because they are customized by client.
|| 10.4.1.1.0 10/28/2013 Lee Quinn      Added Patch Release
|| 10.4.1.2.0 12/17/2013 Lee Quinn      Added Patch Release
|| 10.4.1.3.0 02/13/2014 Lee Quinn      Added Patch Release
|| 10.4.2.0.0 04/05/2014 Lee Quinn      Point Release
|| 10.4.2.1.0 05/30/2014 Lee Quinn      Patch Release
|| 10.4.2.2.0 07/16/2014 Lee Quinn      Patch Release
|| 10.4.2.3.0 08/25/2014 Lee Quinn      Patch Release
|| 10.4.2.4.0 09/30/2014 Alex P/Luke W  Patch Release
|| 10.4.2.4.1 10/13/2014 Alex P         Patch Release
|| 10.4.3.0.0 10/13/2014 Alex P         Point Release
||============================================================================
*/

/*
SQL to build the insert statements - Must use DELIV10 database:

--||============================================================================
--|| All Base Tables - Include List
--||============================================================================
--
select TABLE_NAME,
       'Base Tables' MODULE,
       'insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)' || CHR(013) || ' values (''' || TABLE_NAME ||
       ''', ''Base Tables'', ''Listing of all Base Tables'');' SQL_STMNT
  from USER_TABLES
union all
--
--||============================================================================
--|| CR Tables - Exclude List
--||============================================================================
--
select TABLE_NAME,
       'CR' MODULE,
       'insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)' || CHR(013) || ' values (''' || TABLE_NAME ||
       ''', ''CR'', ''CR Module Exclude List'');' SQL_STMNT
  from USER_TABLES
 where TABLE_NAME like 'CR%'
   and TABLE_NAME not in --Tables that David said should be the same
       ('CR_ALLOC_PROCESS_CONTROL2',
        'CR_ALLOC_PROCESS_CONTROL2_BDG',
        'CR_ALLOC_PROCESS_CONTROL3',
        'CR_ALLOC_PROCESS_CONTROL3_BDG',
        'CR_ALLOC_WHERE_CLAUSE',
        'CR_ALLOC_WHERE_CLAUSE_BDG',
        'CR_ALLOCATION_ATTACHMENTS',
        'CR_ALLOCATION_BUDGET_VALUES',
        'CR_AMOUNT_TYPE_BOOK',
        'CR_AMOUNT_TYPE_TYPE',
        'CR_APPROVAL_GROUP',
        'CR_APPROVAL_GROUP_USERS',
        'CR_BATCH_DERIVATION_COMPANY',
        'CR_BATCH_DERIVATION_CONTROL',
        'CR_BATCH_DERIVATION_DATES_BDG',
        'CR_BATCH_DERIVATION_EXCL_WO',
        'CR_BATCH_DERIVATION_EXCLUSION',
        'CR_BATCH_DERIVATION_QUEUE',
        'CR_BATCH_DERIVATION_RESULTS',
        'CR_BUDGET_ADDITIONAL_FIELDS',
        'CR_BUDGET_APPROVAL',
        'CR_BUDGET_APPROVAL_AUTH_LEVEL',
        'CR_BUDGET_APPROVAL_TYPE',
        'CR_BUDGET_ATTACHMENTS',
        'CR_BUDGET_CALC_ALL_CONTROL',
        'CR_BUDGET_CALC_ALL_TEMPLATE',
        'CR_BUDGET_DATA_LABOR',
        'CR_BUDGET_DATA_LABOR_LABELS',
        'CR_BUDGET_DATA_LABOR_MONTHLY',
        'CR_BUDGET_DATA_LABOR_SPREAD',
        'CR_BUDGET_DATA_LABOR2_DEFINE',
        'CR_BUDGET_DATA_LBR_MNTH_ROWS',
        'CR_BUDGET_ELEMENT_CRITERIA',
        'CR_BUDGET_ELEMENT_GROUP_BY',
        'CR_BUDGET_ENT_RATE_OVR_ELEMENT',
        'CR_BUDGET_ENTRY_GROSS_UP',
        'CR_BUDGET_ESC_TYPE',
        'CR_BUDGET_ESC_TYPE_BV',
        'CR_BUDGET_ESC_TYPE_ORIG',
        'CR_BUDGET_ESC_TYPE_RATE',
        'CR_BUDGET_ESC_TYPE_WHERE',
        'CR_BUDGET_GROUPS',
        'CR_BUDGET_GROUPS_HR',
        'CR_BUDGET_HR_GROUPS_SECURITY',
        'CR_BUDGET_HR_USERS_GRPS',
        'CR_BUDGET_LABOR_ADDITIONS',
        'CR_BUDGET_LABOR_MONTHLY_UPDATE',
        'CR_BUDGET_LABOR_POSITION',
        'CR_BUDGET_LABOR_POSITION_RATES',
        'CR_BUDGET_LABOR_RESOURCE',
        'CR_BUDGET_LABOR_RESOURCE_ORDER',
        'CR_BUDGET_LABOR_WORKSHEET',
        'CR_BUDGET_RATE_TYPE_ELEMENT',
        'CR_BUDGET_RATE_TYPE_TEMPLATE',
        'CR_BUDGET_REPORT_DATA_SOURCE',
        'CR_BUDGET_SUP_COLUMN_CONTROL',
        'CR_BUDGET_SUP_DATA',
        'CR_BUDGET_SUP_WHERE_CLAUSE',
        'CR_BUDGET_TEMPLATES',
        'CR_BUDGET_TEMPLATES_ALLOC',
        'CR_BUDGET_TEMPLATES_APPR_TYPE',
        'CR_BUDGET_TEMPLATES_BUDGET_BY',
        'CR_BUDGET_TEMPLATES_BV',
        'CR_BUDGET_TEMPLATES_COMPUTED',
        'CR_BUDGET_TEMPLATES_DTL_FIELDS',
        'CR_BUDGET_TEMPLATES_FIELDS',
        'CR_BUDGET_TEMPLATES_GROUP_BY',
        'CR_BUDGET_TEMPLATES_GROUPS',
        'CR_BUDGET_TEMPLATES_STRUCTURE',
        'CR_BUDGET_TEMPLATES_USERS_GRPS',
        'CR_BUDGET_USERS_DEPT',
        'CR_BUDGET_USERS_VALID_VALUES',
        'CR_BUDGET_VALUE_TYPE',
        'CR_BUDGET_VALUE_TYPE_TEMPLATE',
        'CR_BUDGET_WHERE',
        'CR_BUDGET_WHERE_APPLY_TO_YEAR',
        'CR_BUDGET_WHERE_CLAUSE',
        'CR_BUDGET_WHERE_COPY_FIELDS',
        'CR_COMPANY_SUMMARY',
        'CR_DATA_MOVER',
        'CR_DATA_MOVER_DATABASE',
        'CR_DATA_MOVER_DBMS',
        'CR_DD_QUERY_DEFAULT',
        'CR_DD_REQUIRED_FILTER',
        'CR_DERIVATION_ROLLUP_PRIORITY',
        'CR_DERIVATION_TYPE_SECURITY',
        'CR_DERIVER_ADDITIONAL_FIELDS_B',
        'CR_DERIVER_OR2_SOURCES_ALLOC',
        'CR_DERIVER_OR2_SOURCES_ALLOCB',
        'CR_DERIVER_OVERRIDE2_SOURCES',
        'CR_DERIVER_TYPE_SOURCES',
        'CR_DERIVER_TYPE_SOURCES_ALLOC',
        'CR_DERIVER_TYPE_SOURCES_ALLOCB',
        'CR_DERIVER_TYPE_SOURCES_BUDGET',
        'CR_DRILLDOWN_KEYS',
        'CR_ELEMENTS',
        'CR_ELEMENTS_FIELDS',
        'CR_FERC_ALLOCATION_CONTROL',
        'CR_GL_JOURNAL_CATEGORY',
        'CR_GL_JOURNAL_CATEGORY',
        'CR_INTERCO_BALANCING',
        'CR_INTERCO_BALANCING',
        'CR_INTERFACE_ATTACHMENTS',
        'CR_INTERFACE_DATES',
        'CR_INTERFACE_MONTH_PERIOD',
        'CR_INTERFACES',
        'CR_JE_IMPORT_TEMPLATES',
        'CR_JE_IMPORT_TEMPLATES_DATA',
        'CR_JOURNAL_APPROVAL',
        'CR_JOURNAL_ATTACHMENTS',
        'CR_JOURNAL_BATCHES',
        'CR_JOURNALS',
        'CR_MANUAL_COMMITMENT_ELEM_HIDE',
        'CR_MANUAL_JE_ELEMENT_HIDE',
        'CR_MANUAL_JE_RECURRING_MONTHS',
        'CR_MANUAL_JE_TEMPLATES',
        'CR_MANUAL_JE_TEMPLATES_DATA',
        'CR_MASTER_ELEMENT_AUTOEXTEND',
        'CR_MASTER_ELEMENT_FORMAT',
        'CR_MONTH_NUMBER',
        'CR_MONTH_NUMBER_BDG',
        'CR_POSTING_APPROVAL',
        'CR_POSTING_APPROVAL_BDG',
        'CR_POSTING_APPROVAL_IDS',
        'CR_POSTING_APPROVAL_IDS_BDG',
        'CR_PPTOCR_DA_MAPPING',
        'CR_PROJECT_APPROVAL',
        'CR_PROJECT_APPROVAL_IDS',
        'CR_QUERY_REPORTING_CONTROL',
        'CR_QUERY_SCALAR_SELECT',
        'CR_RATES',
        'CR_RATES_INPUT',
        'CR_RECON',
        'CR_RECON_FILTER',
        'CR_RECON_SOURCES',
        'CR_RECON_SOURCES_FIELDS',
        'CR_RECON_WHERE',
        'CR_RECON_WHERE_CLAUSE',
        'CR_RECORD_COUNT',
        'CR_SAP_POSTING_TO_ORDER',
        'CR_SAP_TRUEUP_COMPANY',
        'CR_SAP_TRUEUP_CONTROL',
        'CR_SAP_TRUEUP_ELIGIBLE_ACCTS',
        'CR_SAP_TRUEUP_EXCLUSION',
        'CR_SAP_TRUEUP_EXCLUSION_WO',
        'CR_SAVED_QUERIES',
        'CR_SAVED_QUERIES_DATA',
        'CR_SAVED_QUERIES_DATA_BACK',
        'CR_SCO_BILLING_TYPE',
        'CR_SOURCES',
        'CR_SOURCES_FIELDS',
        'CR_SPECIAL_PROCESSING',
        'CR_SPECIAL_PROCESSING_BDG',
        'CR_SPECIAL_PROCESSING_DATES',
        'CR_SPECIAL_PROCESSING_DATES_BD',
        'CR_SPECIAL_PROCESSING_DATES2',
        'CR_SPECIAL_PROCESSING_DATES2_B',
        'CR_STRUCTURE_SECURITY',
        'CR_STRUCTURE_VALUES',
        'CR_STRUCTURE_VALUES2',
        'CR_STRUCTURE_VALUES2_BDG_TEMP',
        'CR_STRUCTURES',
        'CR_STRUCTURES_FLATTENED',
        'CR_STRUCTURES_FLATTENED2',
        'CR_SUM_DOLLAR_COLUMNS',
        'CR_SYSTEM_CONTROL',
        'CR_TEMP_CR_GL_IDS',
        'CR_TEMP_CR_GL_IDS_BDG',
        'CR_TO_BUDGET_TO_CR_AUDIT',
        'CR_TO_BUDGET_TO_CR_COLUMNS',
        'CR_TO_BUDGET_TO_CR_CONTROL',
        'CR_TO_CC_DETAIL_TABLES',
        'CR_TO_COMM_CONTROL',
        'CR_TO_COMM_DETAIL_TABLES',
        'CR_TO_COMM_TABLE_LIST',
        'CR_TO_COMM_TRANSLATE',
        'CR_TO_COMM_TRANSLATE_CLAUSE',
        'CR_TO_CWIP_CONTROL',
        'CR_TO_CWIP_TABLE_LIST',
        'CR_TO_CWIP_TRANSLATE',
        'CR_TO_CWIP_TRANSLATE_CLAUSE',
        'CR_VALIDATION_ACCT_RANGE',
        'CR_VALIDATION_ACCT_RANGE_EXCL',
        'CR_VALIDATION_EXCLUSION',
        'CR_VALIDATION_RULES_HINTS',
        'CR_VALIDATION_RULES_PROJECTS',
        'CR_VALIDATIONS_INVALID_IDS',
        'CR_VALIDATIONS_INVALID_IDS2',
        'CR_VALIDATIONS_INVALID_IDS2A',
        'CR_VALIDATIONS_INVALID_IDS3',
        'CR_WO_CLEAR',
        'CR_WO_CLEAR_BDG')
    or TABLE_NAME in ('REIMB_BILL_SUPPORT_CR', 'REIMB_CR_TARGET_CRITERIA')
union all
--
--||============================================================================
--|| LS Tables - Exclude List
--||============================================================================
--
select TABLE_NAME,
       'LS' MODULE,
       'insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)' || CHR(013) || ' values (''' || TABLE_NAME ||
       ''', ''LS'', ''LS Module Exclude List'');' SQL_STMNT
  from USER_TABLES
 where TABLE_NAME like 'LS%'
union all
--
--||============================================================================
--|| PT Tables - Exclude List
--||============================================================================
--
select TABLE_NAME,
       'PT' MODULE,
       'insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)' || CHR(013) || ' values (''' || TABLE_NAME ||
       ''', ''PT'', ''PT Module Exclude List'');' SQL_STMNT
  from USER_TABLES
 where TABLE_NAME like 'PROP%'
   and TABLE_NAME not in ('PROP_TAX_LOCATION',
                          'PROP_TAX_TYPE_CODE',
                          'PROP_TAX_DISTRICT',
                          'PROP_GROUP_PROP_UNIT',
                          'PROP_UNIT_TYPE_SIZE',
                          'PROPERTY_GROUP',
                          'PROPERTY_UNIT',
                          'PROPERTY_UNIT_DEFAULT_LIFE',
                          'PROP_TAX_DISTRI_PCT')
    or (TABLE_NAME like 'PT%' and TABLE_NAME not in ('PT_BUSINESS_SEGMENT', 'PT_BUSINESS_SEGMENT_PCT'))
    or TABLE_NAME in ('TAX_AUTH_PROP_TAX_DIST')
 union all
--
--||============================================================================
--|| Provision Tables - Exclude List
--||============================================================================
--
select TABLE_NAME,
       'PROV' MODULE,
       'insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)' || CHR(013) || ' values (''' || TABLE_NAME ||
       ''', ''PROV'', ''PROV Module Exclude List'');' SQL_STMNT
  from USER_TABLES
 where TABLE_NAME like 'APPROVAL_STEPS%'
    or TABLE_NAME like 'COMPANY_APPROVAL_AUTH_LEVEL%'
    or TABLE_NAME like 'TAX_ACCRUAL%'
    or TABLE_NAME = 'TMP_TA_SUB_EXT_ROLL'
 union all
--
--||============================================================================
--||  Temp - Exclude List
--||============================================================================
--
select TABLE_NAME,
       'NONE' MODULE,
       'insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)' || CHR(013) || ' values (''' || TABLE_NAME ||
       ''', ''NONE'', ''NONE Tables that are different by client'');' SQL_STMNT
  from USER_TABLES
 where TABLE_NAME in ('PP_JOURNAL_LAYOUTS', 'TAX_ACCRUAL_JE_EXPORT', 'TAX_ACCRUAL_JE_TRANSLATE')
union all
--
--||============================================================================
--|| Tax Tables - Include List
--||============================================================================
--
select TABLE_NAME,
       'TAX INCLUDE' MODULE,
       'insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)' || CHR(013) || ' values (''' || TABLE_NAME ||
       ''', ''TAX INCLUDE'', ''Include List for all Tax Tables'');' SQL_STMNT
  from USER_TABLES
 where TABLE_NAME like 'TAX%'
   and TABLE_NAME not like 'TAX_ACCRUAL%'
   and TABLE_NAME <> 'TAX_AUTH_PROP_TAX_DIST'
    or TABLE_NAME in ('ADJUST_CONVENTION',
                      'AMORTIZATION_TYPE',
                      'ARC_BASIS_AMOUNTS',
                      'ARC_DEFERRED_INCOME_TAX',
                      'ARC_DEFERRED_INCOME_TAX_TRANSF',
                      'ARC_DFIT_FORECAST_OUTPUT',
                      'ARC_FCST_DFIT_FORECAST_OUTPUT',
                      'ARC_FCST_TAX_FORECAST_INPUT',
                      'ARC_FCST_TAX_FORECAST_OUTPUT',
                      'ARC_TAX_ANNOTATION',
                      'ARC_TAX_BOOK_RECONCILE',
                      'ARC_TAX_BOOK_RECONCILE_TRANSFE',
                      'ARC_TAX_CONTROL',
                      'ARC_TAX_DEPRECIATION',
                      'ARC_TAX_DEPRECIATION_TRANSFER',
                      'ARC_TAX_DEPR_ADJUST',
                      'ARC_TAX_FORECAST_INPUT',
                      'ARC_TAX_FORECAST_OUTPUT',
                      'ARC_TAX_RECORD_CONTROL',
                      'ARC_TAX_RENUMBER_ID',
                      'ARC_TAX_TRANSFER_CONTROL',
                      'BASIS_AMOUNTS',
                      'BOOK_ALLOC_ASSIGN',
                      'BOOK_ALLOC_GROUP',
                      'BOOK_ALLOC_RETIREMENTS',
                      'BOOK_ALLOC_RETIRE_PROCESS',
                      'CAP_GAIN_CONVENTION',
                      'COST_OF_REMOVAL_CONVENTION',
                      'DEFERRED_INCOME_TAX',
                      'DEFERRED_INCOME_TAX_RATES',
                      'DEFERRED_INCOME_TAX_TRANSFER',
                      'DEFERRED_RATES',
                      'DEFERRED_TAX_ASSIGN',
                      'DEFERRED_TAX_SCHEMA',
                      'DEF_INCOME_TAX_RATES_VERSION',
                      'DFIT_FORECAST_OUTPUT',
                      'DIT_RATE_VERSION',
                      'EST_SALVAGE_CONVENTION',
                      'FCST_DEF_TAX_RATE',
                      'FCST_TAX_DEPR',
                      'FCST_TAX_DEPR_RATE',
                      'FCST_TAX_METHOD',
                      'GAIN_LOSS_CONVENTION',
                      'JURISDICTION',
                      'JUR_ALLO',
                      'NORMALIZATION_PCT',
                      'NORMALIZATION_SCHEMA',
                      'NORM_TYPE',
                      'RECOVERY_PERIOD',
                      'REG_FUNCTION',
                      'RETIRE_BAL_CONVENTION',
                      'RETIRE_DEPR_CONVENTION',
                      'RETIRE_RESERVE_CONVENTION',
                      'SUMMARY_4562',
                      'SUMMARY_TAX_CLASS',
                      'UTILITY_ACCOUNT_DEPRECIATION',
                      'VERSION',
                      'VINTAGE')
union all
--
--||============================================================================
--|| System Tables - Include List
--||============================================================================
--
select TABLE_NAME,
       'SYSTEM INCLUDE' MODULE,
       'insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)' || CHR(013) || ' values (''' || TABLE_NAME ||
       ''', ''SYSTEM INCLUDE'', ''Include List for all System Tables'');' SQL_STMNT
  from USER_TABLES
 where TABLE_NAME like 'PP%'
    or TABLE_NAME in ('CLASS_CODE',
                      'CLASS_CODE_DEFAULT',
                      'CLASS_CODE_VALUES',
                      'COMPANY_SETUP',
                      'COMPANY_SUMMARY',
                      'CURRENCY',
                      'CURRENCY_RATE',
                      'CURRENCY_RATE_TYPE',
                      'CURRENCY_SCHEMA',
                      'CURRENCY_TYPE',
                      'MORTALITY_CURVE',
                      'MORTALITY_CURVE_POINTS',
                      'MORTALITY_CURVE_RET_POINTS',
                      'POWERPLANT_COLUMNS',
                      'POWERPLANT_DDDW',
                      'POWERPLANT_TABLES',
                      'POWERPLANT_TEMPLATE',
                      'REPORT_TIME',
                      'YES_NO')
order by MODULE, TABLE_NAME;
*/

/*
   The following table change was implemented in update 240_changedb_10.0.5_misc.sql but added here for
   use in scripts prior to 240.
*/
SET SERVEROUTPUT ON

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   COLUMN_EXISTS exception;
   pragma exception_init(COLUMN_EXISTS, -1430);

begin
   execute immediate 'alter table TEMP_EXCLUDE_TABLE_LIST' ||
                     '  add (MODULE varchar2(30) default ''NONE'',' ||
                     '       EXCLUDE_COMMENT varchar2(2000))';
   DBMS_OUTPUT.PUT_LINE('Columns added.');
exception
   when COLUMN_EXISTS then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Column already exists.');
end;
/

update TEMP_EXCLUDE_TABLE_LIST set MODULE = 'NONE' where MODULE is null;


declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   COLUMN_ALREADY_NOTNULL exception;
   pragma exception_init(COLUMN_ALREADY_NOTNULL, -1442);

begin
   execute immediate 'alter table TEMP_EXCLUDE_TABLE_LIST modify MODULE not null';
   DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Column altered.');
exception
   when COLUMN_ALREADY_NOTNULL then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Column already Not Null.');
end;
/


declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   NONEXISTENT_PRIMARY_KEY exception;
   pragma exception_init(NONEXISTENT_PRIMARY_KEY, -2441);

begin
   execute immediate 'alter table TEMP_EXCLUDE_TABLE_LIST drop primary key drop index';
   DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Primary Key Dropped.');
exception
   when NONEXISTENT_PRIMARY_KEY then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Primary Key does not exist.');
end;
/


declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   PRIMARY_KEY_ALREADY_EXISTS exception;
   pragma exception_init(PRIMARY_KEY_ALREADY_EXISTS, -2260);

begin
   execute immediate 'alter table TEMP_EXCLUDE_TABLE_LIST ' ||
                     '   add constraint PK_TEMP_EXCLUDE_TABLE_LIST primary key (TABLE_NAME, MODULE)' ||
                     '   using index tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Primary Key Created.');
exception
   when PRIMARY_KEY_ALREADY_EXISTS then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Primary Key already exists.');
end;
/


/* Remove Exclude List where the Module is in 'Base Tables', 'CR', 'LS', 'PT', 'PROV', 'TAX INCLUDE' */
/* This should leave just exclude lists where the module is 'NONE' */
/* The 'NONE' module is for any custom list of tables that you want to be able to exclude from the compare. */
delete from TEMP_EXCLUDE_TABLE_LIST
   where MODULE in ('CR', 'LS', 'PT', 'PROV', 'Base Tables', 'SYSTEM INCLUDE', 'TAX INCLUDE');
delete from TEMP_EXCLUDE_TABLE_LIST
   where TABLE_NAME in ('PP_JOURNAL_LAYOUTS', 'TAX_ACCRUAL_JE_EXPORT', 'TAX_ACCRUAL_JE_TRANSLATE')
     and MODULE = 'NONE';

/* Load TEMP_EXCLUDE_TABLE_LIST Table */
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ACCOUNT_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ACCOUNT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ACTION_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ACTION_INDICATOR_XLAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ACTIVITY_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ADJUSTED_PLANT_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ADJUST_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AFUDC_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AFUDC_CALC_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AFUDC_CALC_WO_LIST_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AFUDC_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AFUDC_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AFUDC_DATA_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AFUDC_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AFUDC_INPUT_RATIO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AFUDC_OH_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AFUDC_RATE_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AFUDC_RATE_CALC_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AFUDC_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AGED_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AMORTIZATION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ANALYSIS_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ANALYSIS_ACCOUNT_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ANALYSIS_ACCOUNT_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ANALYSIS_ACCOUNT_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ANALYSIS_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ANALYSIS_DEPR_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ANALYSIS_GRAPH_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ANALYSIS_RSRV_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ANALYSIS_TRANSACTION_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ANALYSIS_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPORT_CASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPORT_COMPANY_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPORT_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPORT_DATA_LOAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPORT_FACTOR_ITEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPORT_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPORT_STATE_WEIGHT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVALS_PENDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL_AUTH_LEVEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL_DEFAULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL_DELEGATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL_DELEGATION_HIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL_HIERARCHY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL_NOTIFICATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL_ROUTING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL_STEPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL_STEPS_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARCCR_ARCHIVE_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARCHIVE_FILES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARCHIVE_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARCHIVE_SYSTEM_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARCHIVE_SYSTEM_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARCHIVE_TABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARCHIVE_TAX_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARCHIVE_TRACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_ACCOUNT_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_AFUDC_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_BASIS_AMOUNTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_COMPRESS_CPR_ACT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_CPR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_CPR_ACT_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_CPR_MEMO_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_DEFERRED_INCOME_TAX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_DEFERRED_INCOME_TAX_TRANSF', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_DEPR_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_DEPR_RES_ALLO_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_DEPR_VINTAGE_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_DFIT_FORECAST_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_FCST_DFIT_FORECAST_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_FCST_TAX_FORECAST_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_FCST_TAX_FORECAST_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_FUNDING_JUSTIFICATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_GL_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_PP_PROCESSES_MESSAGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_PP_PROCESSES_OCCURRENCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_RETIRE_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_ANNOTATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_BOOK_RECONCILE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_BOOK_RECONCILE_TRANSFE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_DEPRECIATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_DEPRECIATION_TRANSFER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_FORECAST_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_FORECAST_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_RECORD_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_RENUMBER_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_RENUMBER_TIDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_TRANSFER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_WORK_ORDER_BLANKETS_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_WORK_ORDER_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_WO_AVAILABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_WO_BK_AVAILABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_WO_BK_CHARGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_WO_BK_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_WO_BK_UNITS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_WO_PROBLEMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_CHARGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_DELETE_CHARGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_DEPR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_FCST_LIABILITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_FCST_LIABILITY_DTL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_IMPORT_LIABILITY_ADJ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_IMPORT_LIABILITY_ADJ_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_LAYER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_LAYER_DISCOUNTING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_LAYER_STREAM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_LAYER_STREAM_DOWNWARD_ADJ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_LAYER_WORK_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_LIABILITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_LIABILITY_ACCR_DTL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_LIABILITY_ADJ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_LIABILITY_PREVIEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_MASS_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_MASS_RU', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_MULTI_EST_REVIEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_COST_COMPONENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_COST_STUDY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_COST_STUDY_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_COST_STUDY_FCF', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_DISCOUNT_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_DISCOUNT_GROUP_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_ESCALATION_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_ESC_FACTOR_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_INF_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_INF_FACTOR_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_LAYER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_SCENARIO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_SCENARIO_ESTIMATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_SCENARIO_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_SCENARIO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_SCEN_SUM_EST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_SETTLEMENT_ADJ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_SLIDE_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_PP_SLIDE_FACTOR_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_RELATED_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_SETTLEMENT_ADJ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_SETTLE_LAYER_STREAM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_STREAM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_TEMP_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_TEMP_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_TEMP_RELATED_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_TRANSITION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_TRANSITION_MASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_WAM_RATE_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_WAM_RATE_CALC_DETAILED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARO_WORK_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ASSET_ACCT_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ASSET_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BASIS_AMOUNTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BASIS_AMOUNTS_FP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BILL_MATERIAL_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BKS_SCHEM_SET_BKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BLEND_DEPR_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BOOK_ALLOC_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BOOK_ALLOC_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BOOK_ALLOC_RETIREMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BOOK_ALLOC_RETIRE_PROCESS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BOOK_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BOOK_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_ACTUALS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_AFUDC_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_AFUDC_CALC_BEG_BAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_AFUDC_CALC_BEG_BAL_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_AFUDC_CALC_CLOSING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_AFUDC_CALC_CLOSING_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_AFUDC_CALC_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_AFUDC_DEFAULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_AMOUNTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_AMOUNTS_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_APPROVAL_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_APPROVAL_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_APPROVAL_DATA_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_APPROVAL_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_APPROVAL_DETAIL_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_APPROVAL_LEVELS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_APPROVAL_LEVELS_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_CAP_INTEREST_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_CAP_INTEREST_WO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_CLASS_CODE_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_CLOSINGS_PCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_CLOSINGS_PCT_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_CLOSINGS_STAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_CWIP_IN_RATE_BASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_DELETE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_DELETE_CUSTOM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_ESCALATIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_ESCALATIONS_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_FCST_DEPR_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_LOAD_DEPARTMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_MONTHLY_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_MONTHLY_DATA_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_MONTHLY_DATA_CR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_MONTHLY_DATA_ESCALATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_MONTHLY_DATA_MO_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_MONTHLY_DATA_TRANSPOSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_MONTHLY_DATA_TRANS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_MONTHLY_SPREAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_MONTHLY_SPREAD_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_ONE_BUTTON', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_ORGANIZATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_ORG_CLEAR_DFLT_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_PLANT_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_REIMB_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_REVIEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_REVIEW_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_REVIEW_DEFAULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_REVIEW_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_REVIEW_DETAIL_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_REVIEW_LEVEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_REVIEW_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_REVIEW_USERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_SUBSTITUTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_SUBSTITUTIONS_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_SUBS_DEFAULT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_SUMMARY_CLEAR_DFLT_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_SUMMARY_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_SUMMARY_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_VERSION_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_VERSION_FUND_PROJ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_VERSION_FUND_PROJ_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_VERSION_MAINT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_VERSION_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_VERSION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_WIP_COMP_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_WIP_COMP_CALC_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_WIP_COMP_EXTENSIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_WIP_COMP_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUDGET_WIP_COMP_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUD_SUMMARY_BUD_PLANT_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BUSINESS_SEGMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BV_FP_RESPREAD_AUDIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BV_PROJ_RANKING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BV_PROJ_RANKING_ATTRIBUTES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BV_PROJ_RANKING_ATTRIB_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BV_PROJ_RANKING_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BV_PROJ_RANKING_FILTERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BV_PROJ_RANKING_FILTER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BV_PROJ_RANKING_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BV_PROJ_RANKING_THRESHOLDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CAP_GAIN_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CAP_INTEREST_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CAP_INTEREST_WO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CAP_INT_ADJUST_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CHARGE_COLLECT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CHARGE_ERROR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CHARGE_FORMAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CHARGE_GROUP_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CHARGE_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CHARGE_LOCATION_LEVEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CHARGE_LOCATION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CHARGE_SUMMARY_CLOSINGS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CHARGE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CHARGE_TYPE_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLASS_CODE_CPR_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLASS_CODE_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLASS_CODE_DISPLAY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLASS_CODE_PENDING_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLASS_CODE_PENDING_TRANS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLASS_CODE_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLASS_CODE_VALUES_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLASS_CODE_WO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLEARING_WO_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLEARING_WO_CONTROL_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLEARING_WO_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLEARING_WO_RATE_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLOSING_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLOSING_PATTERN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLOSING_PATTERN_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMBINED_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMBINED_DEPR_RES_ALLO_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMMITMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMMITMENTS_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMMITMENT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_ACCOUNT_CURVES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_APPROVAL_AUTH_LEVEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_BUS_SEGMENT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_CLOSING_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_GL_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_JE_METHOD_EXCLUDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_MAJOR_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_PERCENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_PROPERTY_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_SETUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_SUB_ACCOUNT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPATIBLE_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPUTED_AGE_DISTRIBUTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMP_UNIT_ACTION_CODE_STD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMP_UNIT_HIERARCHY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMP_UNIT_STOCK_KEEPING_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMP_UNIT_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMP_UNIT_TEMPLATE_ROWS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMP_UNIT_WORK_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CONVERSION_BATCH_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COST_ELEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COST_OF_REMOVAL_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COUNTRY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COUNTY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CO_TENANCY_AGREEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CO_TENANCY_PARTNERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CO_TENANCY_WO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPI_RETRO_106_OOB_WO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPI_RETRO_ALLOC_ERROR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPI_RETRO_ASSET_ALLOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPI_RETRO_BASE_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPI_RETRO_BATCH_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPI_RETRO_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPI_RETRO_CLOSINGS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPI_RETRO_CLOSINGS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPI_RETRO_COMPARE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPI_RETRO_CPR_ASSETS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPI_RETRO_WO_LIST_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPI_WO_COMPARE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_ACTIVITY_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_ACT_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_ACT_BASIS_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_ACT_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_ACT_MONTH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_CLASS_CODE_IMPORT_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_CLASS_CODE_IMPORT_STG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_DEPR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_DEPR_CALC_CFNU_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_DEPR_CALC_NURV_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_DEPR_CALC_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_DEPR_CALC_STG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_ASSET_RELATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_ASSET_RELATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_ATTRIBUTE_DEFINE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_ATTRIBUTE_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_ESTIMATE_RELATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_EVENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_EVENT_ATTRIBUTE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_EVENT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_LEDGER_API', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_LEDGER_API_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_LEDGER_API_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_LEDGER_ATTRIBUTE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_LEDGER_DOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_RETIRE_RELATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_EQUIP_TYPE_ATTRIBUTE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_IMPAIRMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_IMPAIRMENT_ALLOC_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_IMPAIRMENT_EVENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_IMPAIRMENT_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_IMPAIR_CALC_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_IMPAIR_REVERSAL_EXP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_IMPAIR_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_LDG_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_LDG_BASIS_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_LEDGER_ACT_IMPORT_AID_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_LEDGER_ACT_IMPORT_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_LEDGER_ACT_IMPORT_STG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_LEDGER_ACT_IMP_ASTID_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_LEDGER_COMMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_LEDGER_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_MEMO_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CPR_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CREW_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CREW_TYPE_RATE_HIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ACCOUNT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOCATION_ATTACHMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOCATION_BUDGET_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOCATION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOCATION_CONTROL_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_CREDIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_CREDIT_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_CREDIT_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_CREDIT_CRITERIA_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_CUSTOM_DW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_CUSTOM_DW_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_GROUP_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_GROUP_BY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_GROUP_BY_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_INTERCO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_INTERCO_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_INTERCO_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_INTERCO_CRITERIA2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_INTERCO_CRITERIA2_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_INTERCO_CRITERIA_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_PROCESS_CONTROL2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_PROCESS_CONTROL2_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_PROCESS_CONTROL3', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_PROCESS_CONTROL3_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_PROCESS_CONTROL_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_SYSTEM_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_TARGET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_TARGET_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_TARGET_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_TARGET_CRITERIA_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_WHERE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_WHERE_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_WHERE_CLAUSE_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_AMOUNT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_AMOUNT_TYPE_BOOK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_AMOUNT_TYPE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_APPROVAL_AUTH_LEVEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_APPROVAL_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_APPROVAL_GROUP_USERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_APPROVAL_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_APPROVAL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BALANCES_BDG_YE_CLOSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BALANCES_DD_KEYS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BALANCES_YE_CLOSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BATCH_DERIVATION_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BATCH_DERIVATION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BATCH_DERIVATION_DATES_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BATCH_DERIVATION_EXCLUSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BATCH_DERIVATION_EXCL_WO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BATCH_DERIVATION_QUEUE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BATCH_DERIVATION_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ACTIVE_ENTRY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ADDITIONAL_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_AFUDC_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_AFUDC_WOTYPE_EXCLUDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_APPROVAL_AUTH_LEVEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_APPROVAL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ATTACHMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_CALC_ALL_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_CALC_ALL_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_COMPONENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_ENTRY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_LABOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_LABOR2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_LABOR2_DEFINE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_LABOR_LABELS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_LABOR_MONTHLY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_LABOR_SPREAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_LBR_MNTH_ROWS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ELEMENT_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ELEMENT_GROUP_BY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ENTRY_GROSS_UP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ENT_RATE_OVR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ENT_RATE_OVR_ELEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ESC_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ESC_TYPE_BV', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ESC_TYPE_ORIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ESC_TYPE_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ESC_TYPE_WHERE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_FILTER_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_FIXED_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_GROUPS_HR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_HR_GROUPS_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_HR_USERS_GRPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_CAP_DOLLARS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_LT_TO_ECT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_MONTHLY_UPDATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_POSITION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_POSITION_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_REPORT_GRAPHS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_RESOURCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_RESOURCE_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_VERSION_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_RATE_TYPE_ELEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_RATE_TYPE_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_REPORT_DATA_SOURCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SPREAD_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_STEP1', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_STEP1_CALCULATIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_STEP1_JUSTIFICATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_STEP2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUBMITTED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_BV_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_ELEMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_ELEMENTS_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_VALUES_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_VALUES_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUP_COLUMN_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUP_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUP_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_TEMPLATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_TEMPLATES_ALLOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_TEMPLATES_APPR_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_TEMPLATES_BUDGET_BY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_TEMPLATES_BV', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_TEMPLATES_COMPUTED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_TEMPLATES_DTL_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_TEMPLATES_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_TEMPLATES_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_TEMPLATES_GROUP_BY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_TEMPLATES_STRUCTURE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_TEMPLATES_USERS_GRPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_USERS_DEPT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_USERS_VALID_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_VALUE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_VALUE_TYPE_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_VERSION_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_VERSION_SECURITY_ALL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_WHERE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_WHERE_APPLY_TO_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_WHERE_COPY_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_CANCEL_ADDITIONAL_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_CANCEL_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMMITMENTS_FIELD_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMMITMENTS_SUM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMMITMENTS_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMMITMENTS_VALIDATIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMPANY_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMPANY_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COST_REPOSITORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_CROSSTAB_SOURCE_DW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_CR_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_CWIP_CHARGE_DRILL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_CWIP_CHARGE_SUM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_CWIP_CHARGE_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_DATABASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_DBMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_FILTER_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_MAP_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_MODULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_MODULE_COLUMN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_MODULE_JOIN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_MODULE_TABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DD_QUERY_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DD_REQUIRED_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DD_SOURCES_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DD_SOURCES_CRITERIA_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DEBIT_CREDIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVATION_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVATION_ROLLUP_PRIORITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVATION_TYPE_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_ADDITIONAL_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_CONTROL_VIRTUAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_OR2_SOURCES_ALLOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_OR2_SOURCES_ALLOCB', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_OVERRIDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_OVERRIDE2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_OVERRIDE2_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_TYPE_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_TYPE_SOURCES_ALLOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_TYPE_SOURCES_ALLOCB', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_TYPE_SOURCES_BUDGET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_WO_LINK_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DISTRIB_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DISTRIB_QUERIES_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DISTRIB_USERS_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DRILLDOWN_KEYS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ELEMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ELEMENTS_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FERC_ALLOCATION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_BUILD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_DOL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_FILTERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_FORMULAS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_FORMULAS2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_LINKED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_RUN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_RUN2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_RUN_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_SETUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_SETUP_LBL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_SPEC_CHAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_GL_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_GL_JOURNAL_CATEGORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_INTERCO_BALANCING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_INTERFACES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_INTERFACE_ATTACHMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_INTERFACE_DATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_INTERFACE_MONTH_PERIOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_JE_IMPORT_TEMPLATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_JE_IMPORT_TEMPLATES_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_JOURNALS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_JOURNAL_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_JOURNAL_ATTACHMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_JOURNAL_BATCHES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_MANUAL_COMMITMENT_ELEM_HIDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_MANUAL_JE_ELEMENT_HIDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_MANUAL_JE_RECURRING_MONTHS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_MANUAL_JE_TEMPLATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_MANUAL_JE_TEMPLATES_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_MASTER_ELEMENT_AUTOEXTEND', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_MASTER_ELEMENT_FORMAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_MONTH_NUMBER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_MONTH_NUMBER_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_OPEN_MONTH_NUMBER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_POSTING_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_POSTING_APPROVAL_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_POSTING_APPROVAL_IDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_POSTING_APPROVAL_IDS_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_POST_TO_GL_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_POST_TO_GL_COLUMNS_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_POST_TO_GL_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_POST_TO_GL_CONTROL_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_PPTOCR_DA_MAPPING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_PROJECT_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_PROJECT_APPROVAL_IDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_QUERY_REPORTING_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_QUERY_SCALAR_SELECT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_RATES_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_RECON', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_RECON_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_RECON_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_RECON_SOURCES_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_RECON_WHERE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_RECON_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_RECORD_COUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SAP_POSTING_TO_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SAP_TRUEUP_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SAP_TRUEUP_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SAP_TRUEUP_ELIGIBLE_ACCTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SAP_TRUEUP_EXCLUSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SAP_TRUEUP_EXCLUSION_WO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SAVED_QUERIES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SAVED_QUERIES_BACK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SAVED_QUERIES_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SAVED_QUERIES_DATA_BACK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SCO_BILLING_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SCO_BILLING_TYPE_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SOURCES_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SPECIAL_PROCESSING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SPECIAL_PROCESSING_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SPECIAL_PROCESSING_DATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SPECIAL_PROCESSING_DATES2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SPECIAL_PROCESSING_DATES2_B', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SPECIAL_PROCESSING_DATES_BD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_STRUCTURES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_STRUCTURES_FLATTENED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_STRUCTURES_FLATTENED2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_STRUCTURE_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_STRUCTURE_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_STRUCTURE_VALUES2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_STRUCTURE_VALUES2_BDG_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SUM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SUM_DOLLAR_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SUM_HIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SUSPENSE_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SYSTEM_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_ALLOCATIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_COMMITMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_GL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_GL_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_GL_BDG_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_GL_IDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_GL_IDS_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_GL_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_JE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_BUDGET_TO_CR_AUDIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_BUDGET_TO_CR_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_BUDGET_TO_CR_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_CC_DETAIL_TABLES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_COMM_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_COMM_DETAIL_TABLES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_COMM_TABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_COMM_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_COMM_TRANSLATE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_CWIP_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_CWIP_TABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_CWIP_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TO_CWIP_TRANSLATE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATIONS_INVALID_IDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATIONS_INVALID_IDS2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATIONS_INVALID_IDS2A', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATIONS_INVALID_IDS3', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_ACCT_RANGE_EXCL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_COMBOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_COMBOS_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_COMBOS_STAGING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_COMBOS_STG_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_CONTROL_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_EXCLUSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_RULES_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_RULES_HINTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_RULES_PROJECTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_WO_CLEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_WO_CLEAR_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CURRENCY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CURRENCY_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CURRENCY_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CURRENCY_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CURRENCY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CUSTOM_VALIDATIONS_INFO_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CWIP_CHARGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CWIP_IN_RATE_BASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DATA_ACCOUNT_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_INCOME_TAX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_INCOME_TAX_FP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_INCOME_TAX_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_INCOME_TAX_TRANSFER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_TAX_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_TAX_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEF_INCOME_TAX_RATES_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPARTMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPRECIATION_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_ACTIVITY_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_ACTIVITY_RECURRING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_BEG_BALS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_BLENDING_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_BLENDING_TYPE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_CALC_AMORT_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_CALC_AMORT_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_CALC_COMBINED_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_CALC_COMBINED_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_CALC_OVER_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_CALC_OVER_STG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_CALC_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_CALC_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_CALC_STG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_COMBINED_GROUP_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_CPR_DEPR_IMPORT_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_CPR_DEPR_IMPORT_STG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_FCST_GROUP_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_GROUP_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_GROUP_JUR_ALLO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_GROUP_UOP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_LEDGER_BLENDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_LEDGER_BLENDING_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_LEDGER_BLENDING_STG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_LEDGER_BLENDING_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_LEDGER_IMPORT_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_LEDGER_IMPORT_STG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_METHOD_BLENDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_METHOD_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_METHOD_RATES_IMPORT_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_METHOD_RATES_IMP_STG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_METHOD_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_METHOD_SLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_METHOD_UOP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_MID_PERIOD_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_NET_SALVAGE_AMORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_RATES_COMMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_RES_ALLO_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_SUBLEDGER_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_SUBLEDGER_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_SUMMARY2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_TRANS_ALLO_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_TRANS_SET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_TRANS_SET_DG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_TRANS_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPR_VINTAGE_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPT_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEPT_TEMPLATE_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DFIT_FORECAST_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DISCOUNT_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DISPOSITION_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DIT_RATE_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DIVISION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DOCUMENT_STAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_ACCT_DATASET_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_ACTIVITY_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_ANALYSIS_DATASET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_COMPANY_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DATASET_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DATASET_EXTERNAL_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DATASET_RSRV_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DATASET_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DATA_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DATA_ACCOUNT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DATA_ACCOUNT_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DATA_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DB_IMPORT_ACCT_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DB_IMPORT_CODE_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DB_IMPORT_PLANT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DB_IMPORT_RSRV', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DB_IMPORT_VARIABLES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_DG_DATASET_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DS_RSRV_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ENG_ESTIMATE_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ESTIMATE_CHARGE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ESTIMATE_CURVES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ESTIMATE_CURVE_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ESTIMATE_OVERHEAD_BASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ESTIMATE_OVERHEAD_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ESTIMATE_UPLOAD_CUSTOMIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('EST_SALVAGE_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('EXPENDITURE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('EXP_TYPE_EST_CHG_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_BUDGET_DEPR_CLOSINGS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_BUDGET_DEPR_CLOS_FP_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_BUDGET_LOAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_BUDGET_LOAD_ADDITIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_BUDGET_LOAD_ADDITIONS_FP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_BUDGET_LOAD_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_BUDG_CPR_DEPR_ASSET_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_BUDG_CPR_DEPR_CLOSING_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_BUDG_CPR_DEPR_CLOS_FP_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_COMBINED_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_CPR_DEPR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_CPR_DEPR_FP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_CPR_DEPR_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEF_TAX_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPRECIATION_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_CALC_AMORT_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_CALC_COMBINED_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_CALC_STG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_GROUP_UOP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_GROUP_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_GROUP_XLAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_LEDGER_ANNUAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_LEDGER_ANNUAL_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_LEDGER_BLENDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_LEDGER_BLEND_STG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_LEDGER_FP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_METHOD_BLENDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_METHOD_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_METHOD_UOP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_RES_ALLO_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_SUBLEDGER_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_VINTAGE_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEPR_VINTAGE_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DVS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_GL_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_METHOD_RETIRE_PCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_NET_SALVAGE_AMORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_RATE_RECALC2_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_RATE_RECALC_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_SUBLEDGER_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_TAX_DEPR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_TAX_DEPR_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_TAX_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_UNIT_OF_PRODUCTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_VERSION_SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FERC_ACTIVITY_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FERC_PLANT_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FERC_REPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FERC_SYS_OF_ACCTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FIT_STATISTICS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FIT_STATS_ALL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FORECASTED_RETIREMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FUNC_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FUNC_CLASS_LOC_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FUNC_CLASS_PROP_GRP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FUNDING_JUSTIFICATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('GAIN_LOSS_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('GENERATION_ARRANGEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('GENERATION_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('GL_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('GL_ACCT_BUS_SEGMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('GL_JE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('GL_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('GL_TRANS_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('HANDY_WHITMAN_INDEX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('HANDY_WHITMAN_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('HANDY_WHITMAN_REGION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('HRS_QTY_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('INCREMENTAL_DEPR_ADJ_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('INCREMENTAL_FP_ADJ_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('INCREMENTAL_FP_ADJ_ITEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('INCREMENTAL_FP_ADJ_LABEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('INCREMENTAL_FP_RUN_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('INSTRUCTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JE_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JE_METHOD_SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JE_METHOD_TRANS_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JE_TRANS_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_INTERFACE_STAGING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_INTERFACE_STAGING_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_MASS_UPDATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_PRIORITY_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_SETUP_CC_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_SETUP_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_SYSTEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_TEMPLATE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JOB_TASK_TEMPLATE_WO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JURISDICTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JUR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JUR_ACTIVITY_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JUR_AFC_SHADOW_MAPPING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JUR_AFC_SHADOW_WO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JUR_ALLO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JUR_ALLO_BOOK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JUR_ALLO_BOOK_DESCRIPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JUR_ALLO_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JUR_ALLO_VERSION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JUR_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LIFE_ANALYSIS_PARAMETERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LOCATION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_AP_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_COMPONENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_LOCAL_TAX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_LOC_LOCAL_TAX_DISTRI', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_TAX_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_CANCELABLE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_COLUMN_USAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_COMPONENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_COMPONENT_CHARGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_COMPONENT_MONTHLY_II_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_COMPONENT_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_CPR_ASSET_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_DEPR_FORECAST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_EXTENDED_RENTAL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_AMOUNTS_SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_ASSET_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_ASSET_SCHEDULE_CALC_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_ASSET_SCHEDULE_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_ASSET_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_GROUP_FLOATING_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_OPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_PAYMENT_TERM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_SCHEDULE_BUCKET_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_SCHEDULE_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_ASSET_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_COMPONENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_COMPONENT_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_D_TAX_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_D_TAX_RATES_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_FLOAT_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_FLOAT_RATES_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_ILR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_ILR_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_INTERIM_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_INTERIM_RATES_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_INVOICE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_INVOICE_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_LEASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_LEASE_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_LESSOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_LESSOR_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_S_TAX_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_S_TAX_RATES_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_INVOICE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_INVOICE_LINE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_INVOICE_PAYMENT_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_CAP_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_GROUP_USER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_INTERIM_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_OPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_TYPE_JES_EXCLUDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_TYPE_TOLERANCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_VENDOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LESSOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_MEC_CONFIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_MLA_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_MONTHLY_ACCRUAL_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_MONTHLY_DEPR_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_MONTHLY_TAX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PAYMENT_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PAYMENT_FREQ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PAYMENT_HDR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PAYMENT_LINE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PAYMENT_TERM_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PAYMENT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_BASIS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_CLASS_CODE_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_SET_OF_BOOKS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_TRANSACTION_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_POST_CPR_FIND', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PURCHASE_OPTION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_RECONCILE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_RENEWAL_OPTION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_RENT_BUCKET_ADMIN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_RETIREMENT_NEW_TERMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_RETIREMENT_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_TAX_DISTRICT_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_TAX_LOCAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_TAX_RATE_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_TAX_STATE_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_TAX_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_VENDOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MAJOR_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_CRITERIA_LINES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_DATADEF', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_DATADEF_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_MAP_TO_AS_BUILT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_MAX_SOURCE_IDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_NON_TABLE_CUSTOMIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_SOURCES_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_SOURCES_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MATLREC_TRANS_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MINOR_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MONTHLY_ESTIMATE_MODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MORTALITY_CURVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MORTALITY_CURVE_POINTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MORTALITY_CURVE_RET_POINTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MORTALITY_CURVE_SOURCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MORTALITY_MEMORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MORTALITY_MEMORY_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MUNICIPALITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('NARUC_PLANT_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('NARUC_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('NORMALIZATION_PCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('NORMALIZATION_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('NORM_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('OH_ALLOC_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('OH_ALLOC_BASIS_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('OVERHEAD_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('OVERHEAD_BASIS_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_ARO_REG_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_ARO_REG_ACTIVITY_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_BASIS_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_BASIS_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_BASIS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_DEPR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_EQUIP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_EQUIP_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_RELATED_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_SUBLEDGER_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_SUBLEDGER_BASIS_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_SUBLEDGER_ENTRY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_SUBLEDGER_ENTRY_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_TRANSACTION_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_TRANSACTION_MEMO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_TRANSACTION_MEMO_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_TRANSACTION_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_TRANSACTION_SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_TRANSACTION_SOB_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PEND_TRANSACTION_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('POSTING_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('POWERPLANT_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('POWERPLANT_DDDW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('POWERPLANT_TABLES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('POWERPLANT_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_ACTIONS_WINDOWS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_MENU_ITEMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_QUERY_FILTERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_REPORT_PACKAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_REPORT_PACKAGE_REPORTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_SYSTEM_OPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_SYSTEM_OPTIONS_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_SYSTEM_OPTIONS_MODULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_SYSTEM_OPTIONS_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_SYSTEM_OPTIONS_WKSP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_USER_OPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_WORKSPACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_WORKSPACE_LINKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_WORKSPACE_OBJECT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPVERIFY_LOCK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ADSI_ATTRIBUTES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ADSI_DIRECTORY_PATH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ADSI_MAP_ATTRIBUTES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ALERT_PROCESSES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ALERT_PROCESS_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AMT_FORMAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ANY_QUERY_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ANY_QUERY_CRITERIA_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ANY_REQUIRED_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_ADMIN_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_ARO_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_BUDGET_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_CPR_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_CR_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_DEPRFCST_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_DEPRSTUDY_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_DEPR_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_LEASE_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_POWERTAX_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_PROJECT_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_PROPTAX_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_PROVISION_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_REG_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_SYSTEM_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_TABLES_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_TASK_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDIT_DDL_TABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDIT_LOGON_TABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDIT_TRAIL_DELETES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_JOBS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_PROGRAMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_PROGRAM_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_QUEUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORTS_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORTS_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORT_ARGS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORT_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORT_FORMAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORT_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORT_PRINTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_RPTS_DISTR_GRPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_RPTS_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_RPTS_VIEWED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CALENDAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CHARGE_SPLIT_RUNNING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CLASS_CODE_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CLIENT_EXTENSIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_COLOR_LOOKUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_COMPANY_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_COMPANY_SECURITY_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONVERSION_WINDOWS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_BATCHES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_BATCHES_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_FERC_XLAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_MAPPING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_REF_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_STAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_TEMPLATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_TEMPLATES_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_TEMP_ACTIVITY_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_TEMP_COST_QUANTITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DATABASE_OBJECT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DATAWINDOW_EXCEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DATAWINDOW_HINTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DROP_CONSTRAINTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER_MAPPING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER_RESTRICTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER_SAVED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER_SAVED_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER_VALUES_DW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_RPT_USER_SAVED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYN_FILTER_SAVED_VALUES_DW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_EDIT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ERROR_MESSAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_FLEX_NAMES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_FLEX_NAMES_MLS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_HTMLHELP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_COLUMN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_COLUMN_LOOKUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_FILE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_LOOKUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_RUN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_SUBSYSTEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_TEMPLATE_EDITS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_TEMPLATE_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_TYPE_SUBSYSTEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_TYPE_UPDATES_LOOKUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_INTEGRATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_INTEGRATION_COMPONENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_INTERFACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_INTERFACE_DATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_INTERFACE_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JL_BIND_ARG_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JOB_REQUEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JOURNAL_DATA_SQL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JOURNAL_KEYWORDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JOURNAL_LAYOUTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JOURNAL_TYPES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LANGUAGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LDAP_ATTRIBUTES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LDAP_MAP_ATTRIBUTES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LDAP_PROFILES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LDAP_USER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LDAP_USER_SYNC_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LOCALE_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MAIL_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MANUAL_INTERFACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MISC_SEARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MODULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_ALERT_DASHBOARD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_ALERT_DASHBOARD_PG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_ALERT_DASH_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_APPROVAL_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_METRIC_DW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_REPORT_TYPE_TASK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_TASK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_TASK_STEP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_TAX_REPORT_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_TIME', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_TIME_REPORT_TIME', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_LAYOUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_METRIC_DW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_QUERY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_REPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_REPORT_PARM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_TASK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_TODO_ATTACHMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_TODO_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_OBJECTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_OBJECTS_COPY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PLSQL_DEBUG_ENABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_POST_LOCK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_DOCUMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_ERROR_INFO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_ERROR_MSG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_ERR_INFO_ACT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_INFO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_KICKOUT_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_KICKOUT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_KICKOUT_TABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_MESSAGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_OCCURRENCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_OCC_ATTRIBUTE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_OCC_ERROR_INFO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_OCC_METRIC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_RETURN_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_RUNNING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESS_ERROR_ACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY_DW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY_DW_CC_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY_DW_COLS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY_DW_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY_SCALAR_KEYWORDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY_SCALAR_SELECT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_RECORD_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_ARGS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_COMPANY_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_ENVIR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_LINK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_LINK_SUBSTITUTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_SUBSYSTEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_TIME_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORT_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORT_FILTER_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REQUIRED_TABLE_COLUMN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SCHEMA_CHANGE_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SCHEMA_UPGRADE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_GROUP_TYPES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_LOGIN_PROFILE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_LOG_RECORD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_OBJECTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_OBJECT_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_PASSWD_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_RULES_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_USERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_USERS_ALIAS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_USERS_FILTERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_USERS_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_USER_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SESSION_PARAMETERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_STATISTICS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SYSTEM_CONTROL_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SYSTEM_ERRORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_AUDITS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_AUDITS_OBJ_ACTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_AUDITS_PK_LOOKUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_AUDITS_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_MONTHS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_YEARS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TASK_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TEMP_IMPORT_FILE_LINES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TEST_PERFORMANCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TOOLTIP_HELP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TREE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TREEVIEW_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TREE_CATEGORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TREE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_UPDATE_FLEX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_USER_DW_MICROHELP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_USER_PROFILE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_USER_PROFILE_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_BATCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_CATEGORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_CATEGORY_DESCRIPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_COMMENTS_CATEGORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_COMMENTS_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_USER_COMMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_USER_KEY_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_USER_STD_COMMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERSION_EXES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_AUTHENTICATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_AUTHENTICATION_DEVICE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_AUTHENTICATION_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_AUTHENTICATION_USERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_COMPONENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_CONFIGURATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_DATATYPES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_LOCALIZATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_LOCALIZATION_REVISION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_LOCALIZATION_STRING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_LOCALIZATION_STRING2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_MODULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_MODULE_COMPONENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_PRINTERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_SECURITY_PERMISSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_SECURITY_REVISION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_SITES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_USER_ACCESS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_VIEW_CHANGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_VIEW_SESSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WINDOW_DISABLE_RESIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WINDOW_SIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WORKSHEETS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WORK_ORDER_TYPE_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WO_EST_CUSTOMIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WO_EST_CUSTOMIZE_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WO_TASK_CUSTOMIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROCESSING_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_ADJUST_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_AUTHORITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_AUTHORITY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_TYPE_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_YEAR_LOCK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_UNIT_DEFAULT_LIFE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_GROUP_PROP_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_ALLO_DEFINITION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_DISTRICT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_DISTRI_PCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_NET_TAX_RESERVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_NET_TAX_RESERVE_REP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_TAX_RESERVE_PERCENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_TYPE_ASSIGN_TREE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_TYPE_ASSIGN_TREE_CWIP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_TYPE_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_TYPE_PROP_TAX_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_UNIT_TYPE_SIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_DOCUMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_DOCUMENTS_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_SCRIPT_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_SYSTEM_OPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_SYSTEM_OPTIONS_CENTERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_SYSTEM_OPTIONS_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_USER_OPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_ASSIGN_TREE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_ASSIGN_TREE_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_ASSIGN_TREE_LEVELS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_CHARGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_DEFERRAL_AMOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_DEFERRAL_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_PENDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_PENDING_BAK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_YEAR_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACTUALS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACTUALS_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ALLOCATE_ASSESSMENT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ALLOCATION_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ALLOCATION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_APPRAISER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_APPROVAL_AUTH_LEVEL_BAK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSED_VALUE_ALLOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSED_VALUE_COUNTY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSED_VALUE_TAX_DISTRICT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSMENT_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSMENT_GROUP_CASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSMENT_GROUP_TYPES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSMENT_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_AUTHORITY_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_BUSINESS_SEGMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_BUSINESS_SEGMENT_PCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_CASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_CASE_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_CASE_CALC_AUTHORITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_CASE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_CASE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_COMPANY_OVERRIDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_COUNTY_EQUALIZATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_GENERATOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_LINK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_TYPE_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_TYPE_PERSON', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_WEEKEND_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DEPR_FLOOR_AUTOADJ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DEPR_FLOOR_AUTOADJ_TYPES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DISTRICT_EQUALIZATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_EQUALIZATION_FACTOR_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ESCALATED_VALUE_INDEX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ESCALATED_VALUE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ASSESSOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ASSESSOR_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ASSETS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ASSETS_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ASSET_LOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ASSET_LOC_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_AUTH_DIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_AUTH_DIST_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_CLASS_CODE_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_CWIP_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_CWIP_ASSIGN_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_DIVISION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_DIVISION_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ESCVAL_INDEX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ESCVAL_INDEX_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LEDGER_ADJ_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LEDGER_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LEVY_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LEVY_RATES_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LOC_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LOC_TYPE_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_MAJOR_LOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_MAJOR_LOC_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_MINOR_LOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_MINOR_LOC_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_MUNICIPALITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_MUNICIPALITY_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PARCEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PARCEL_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PAYMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PAYMENT_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_ASMT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_ASMT_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_GEO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_GEOFCTR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_GEOFCTR_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_GEO_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_HISTORY_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_LOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_LOC_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_RSP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_RSP_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_RSP_ENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_RSP_ENT_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PREALLO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PREALLO_ADJ_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PREALLO_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PROPTAX_LOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PROPTAX_LOC_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_RETIRE_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_RETIRE_UNIT_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_RSVFCTR_PCTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_RSVFCTR_PCTS_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STATS_FULL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STATS_FULL_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STATS_INCR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STATS_INCR_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STMT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STMT_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STMT_LINE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STMT_LINE_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_TAX_DIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_TAX_DIST_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_TYPE_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_TYPE_CODE_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_UNIT_COST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_UNIT_COST_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_UTIL_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_UTIL_ACCOUNT_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_VALUE_VAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_VALUE_VAULT_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_INCREMENTAL_BASE_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_INCREMENTAL_INITIALIZATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_INCREMENTAL_INIT_ADJ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_INSTALLMENT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_INTERFACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_INTERFACE_TAX_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_ADJUSTMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_DETAIL_COLUMN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_DETAIL_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_DETAIL_MAP_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_DETAIL_SOURCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_TAX_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_TRANSFER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEVY_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEVY_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LOCATION_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_MARKET_VALUE_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_MARKET_VALUE_RATE_COUNTY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_NEG_BAL_AUTOTRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_APPEAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_APPEAL_EVENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_APPEAL_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_APPEAL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_APPRAISAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_ASSESSMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_AUTO_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_FLEX_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_FLEX_FIELD_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_FLEX_FIELD_USAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_FLEX_FIELD_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_GEOGRAPHY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_GEOGRAPHY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_GEOGRAPHY_TYPE_STATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_GEO_TYPE_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_RESPONSIBILITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_RESPONSIBILITY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_RESPONSIBLE_ENTITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PAYMENT_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PERIOD_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_POWERTAX_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PREALLO_ADJUSTMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PREALLO_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PREALLO_TRANSFER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PROCESS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PROCESS_COMPANY_STATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PROCESS_COMPANY_STATE_TY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PROCESS_DEPENDENCY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PROCESS_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PROCESS_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PYMT_CNTR_1000_IDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_QUANTITY_CONVERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_QUERY_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_QUERY_COL_SAVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_QUERY_FILTER_SAVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_QUERY_MAIN_SAVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_QUERY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RATE_DEFINITION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_COMPANY_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_COUNTY_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_FIELD_LABELS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_LOCATION_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_PARCEL_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_PT_COMPANY_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_STATE_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_TAX_DISTRICT_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_TYPE_CODE_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RESERVE_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RESERVE_FACTOR_COUNTY_PCTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RESERVE_FACTOR_DIST_PCTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RESERVE_FACTOR_PERCENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RESERVE_FACTOR_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RESERVE_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_SCHEDULE_INSTALLMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_SCHEDULE_PERIOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_SGROUP_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_SGROUP_ROLLUP_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_SGROUP_ROLLUP_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_GROUP_PAYMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_GROUP_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_INSTALLMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_LINE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_LINE_PAYMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_PAYEE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_PAYMENT_APP_BAK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_PERIOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_STATEMENT_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_VERIFIED_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATISTICS_AUTOCREATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATISTICS_FULL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATISTICS_INCREMENTAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATISTICS_SPREAD_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TAXABLE_VALUE_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TAXABLE_VALUE_RATE_COUNTY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ACCRUAL_PRIOR_MONTH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ACTUALS_ASMTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ACTUALS_LOAD_PCTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ACTUALS_TOTAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ALLO_INCR_BASE_PCTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ALLO_INCR_TOTAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ALLO_PERCENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ALLO_SPREAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_BILLS_COPY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_BILLS_RECALC_TERMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_CASECALC_ROUNDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_CASECALC_TOTALS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_CPRPULL_BASIS_ADJS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_CPRPULL_DEPRMONTH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_CPRPULL_LEDGERDETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_IDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_IDS2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_IDS3', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_IDS4', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_LEDGER_ADDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_LEDGER_ADJS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_LEDGER_ADJUSTMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_LEDGER_TAX_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_LEDGER_TRANSFERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_PARCELS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_PARCELS_ASMT_GP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_PREALLO_ADJS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_PREALLO_TRANSFERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_SCENARIO_FORMULA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_STATEMENT_LINES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TYPE_ASSIGN_CWIP_PSEUDO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TYPE_NATIONAL_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TYPE_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TYPE_ROLLUP_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TYPE_ROLLUP_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_UNIT_COST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_UNIT_COST_AMOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_UNIT_COST_AMOUNT_VINTAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_UNIT_COST_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_USER_INPUT_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VALUE_VAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_SCENARIO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_SCENARIO_RESULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_SCENARIO_RUN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_TEMPLATE_EQUATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_TEMPLATE_EQ_FORMULA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_TIMEFRAME', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_VARIABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_VARIABLE_SOURCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VINTAGE_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_YEAR_OVERRIDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RATE_ANALYSIS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RATE_ANALYSIS_RESERVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RATE_AREA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REASON_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RECOVERY_PERIOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REGULATORY_ENTRIES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REGULATORY_ENTRIES_ELIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REGULATORY_ENTRY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REGULATORY_TRANSACTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ACCT_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ACCT_MASTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ACCT_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ACCT_ROLLUP_LEVELS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ACCT_ROLLUP_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ACCT_TAX_ENTITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ACCT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ADJUSTMENT_DISPOSITION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ADJUSTMENT_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ADJUST_JUR_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ADJUST_JUR_DEFAULT_ACCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ADJUST_TYPES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ALLOCATOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ALLOCATOR_DYN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ALLOCATOR_DYN_EXT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ALLOC_CATEGORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ALLOC_RESULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ALLOC_TARGET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ANALYSIS_CHARTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ANALYSIS_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ANALYSIS_DETAIL_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ANALYSIS_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ANALYSIS_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ANALYSIS_PRESENTATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ANALYSIS_REPORTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ANNUALIZATION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_ANNUALIZE_WORK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_COMPONENT_ELEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_COMPONENT_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_COMP_MEMBER_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_COMP_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_EXPENDITURE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_FAMILY_MEMBER_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_GL_ACCOUNT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_MEMBER_ELEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_MEMBER_RULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_MEMBER_RULE_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_SOURCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_SOURCE_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_BUDGET_SOURCE_BALANCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CALCULATIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CAPITAL_STRUCTURE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CAPITAL_STRUCT_INDICATOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ACCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ACTUALS_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ADJUSTMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ADJUST_ACCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ADJUST_ACCT_MONTHLY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ADJUST_ALLOC_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ADJUST_ALLOC_RESULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ADJUST_CALC_EXT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ADJUST_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ADJUST_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ADJ_ATTACHMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ADJ_CALC_INPUT_ACCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ALLOCATOR_DYN_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ALLOC_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ALLOC_DIRECT_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ALLOC_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ALLOC_STEPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ALLOC_TAX_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_ATTACHMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_CALCULATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_CATEGORY_ALLOCATOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_COST_OF_CAPITAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_COST_OF_CAPITAL_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_COVERAGE_VERSIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_FORECAST_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_INTEREST_SYNC_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_INTEREST_SYNC_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_MONITOR_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_MULTI_CALC_CAP_STRUCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_MULTI_CALC_LABEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_MULTI_CALC_RESULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_PARAMETER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_PROCESS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_PROCESS_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_PROCESS_STEPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_RESULTS_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_RETURN_RESULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_RETURN_RESULT_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_REV_GROSSUP_ITEMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_REV_GROSSUP_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_REV_REQ_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_STATUS_DETAIL_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_STATUS_DETAIL_ALLOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_STATUS_DETAIL_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_STATUS_DETAIL_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_SUMMARY_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CASE_TARGET_FORWARD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CHART', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CHART_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CHART_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CHART_GROUP_CASES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CHART_GROUP_LABELS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_COLUMN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_COMPANY_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CO_INPUT_SCALE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_COMBOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_COMBOS_ELEMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_COMBOS_ELEMENTS_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_COMBOS_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_COMBOS_FIELDS_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_COMBOS_LISTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_COMBOS_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_COMBOS_MAP_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_COMBOS_SOURCES_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_PULL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_TABLES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_WHERE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CR_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CWIP_COMPONENT_ELEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CWIP_COMPONENT_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CWIP_COMP_MEMBER_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CWIP_COMP_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CWIP_FAMILY_COMPONENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CWIP_FAMILY_MEMBER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CWIP_FAMILY_MEMBER_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CWIP_GL_ACCOUNT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CWIP_MEMBER_ELEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CWIP_MEMBER_RULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CWIP_MEMBER_RULE_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_CWIP_SOURCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_DEPR_COMPONENT_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_DEPR_FAMILY_COMPONENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_DEPR_FAMILY_MEMBER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_DEPR_FAMILY_MEMBER_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_DEPR_LEDGER_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_DEPR_LEDGER_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_DEPR_LEDGER_MAP_ACT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_EXT_COMBOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_EXT_COMBOS_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_EXT_COMBOS_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_EXT_COMPANY_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_EXT_LOAD_BAL_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_EXT_LOAD_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_EXT_PULL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_EXT_SOURCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_EXT_SOURCE_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_EXT_STAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_EXT_STG_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FAMILY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_CAP_BDG_LOAD_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_CR_LOAD_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_DEPR_FAM_MEM_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_DEPR_LEDGER_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_DEPR_LEDGER_MAP_ACT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_DEPR_SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_INC_ADJ_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_RECON_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_RECON_ITEMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_RECON_REGACCT_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_RECON_TB_COMBOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_RECON_TB_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_RECON_TB_SETUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_TAX_LOAD_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FCST_TA_LOAD_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FERC_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FINANCIAL_MONITOR_RESULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FINANCIAL_MONITOR_RES_BAK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FINANCIAL_MONITOR_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FIN_COST_OF_CAPITAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FIN_MONITOR_ALLOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FIN_MONITOR_ALLOC_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FIN_MONITOR_CALC_KEY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FIN_MONITOR_CALC_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FIN_MONITOR_KEY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FIN_MONITOR_PROCESS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FIN_MONITOR_RESULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FIN_MONITOR_SUB_ACCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FIN_MONITOR_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FIN_MONITOR_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FORECAST_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FORECAST_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FORECAST_LEDGER_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FORECAST_RECON_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FORECAST_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FUNCTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HISTORIC_RECON_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HISTORIC_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HISTORY_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HISTORY_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HISTORY_LEDGER_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HIST_CR_LOAD_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HIST_CWIP_LOAD_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HIST_DEPR_SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HIST_RECON_CR_COMBOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HIST_RECON_CR_ELEMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HIST_RECON_CR_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HIST_RECON_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HIST_RECON_ITEMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HIST_RECON_REGACCT_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HIST_TAX_LOAD_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_HIST_TA_LOAD_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_IMPORT_ALLOC_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_IMPORT_ALLO_FACTOR_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_IMPORT_ALLO_FACTOR_STG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_IMPORT_EXT_STAGE_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_IMPORT_EXT_STAGE_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_IMPORT_HISTORY_LDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_IMPORT_HISTORY_LDG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INCREMENTAL_ADJUSTMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INCREMENTAL_ADJUST_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INCREMENTAL_ADJ_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INCREMENTAL_ADJ_RELATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INCREMENTAL_ADJ_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INCREMENTAL_DEPR_ADJ_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INCREMENTAL_FP_ADJ_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INCREMENTAL_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INCREMENTAL_ITEM_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INCREMENTAL_LABEL_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INCREMENTAL_RELATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INPUT_SCALE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INTERFACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_INTERFACE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JURISDICTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_ACCT_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_ADJUST_CALC_EXT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_ADJ_ATTACHMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_ADJ_CALC_INPUT_ACCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_ALLOC_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_ALLOC_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_ALLOC_STEPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_ALLOC_TAX_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_CATEGORY_ALLOCATOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_INPUT_SCALE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_MONITOR_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_TARGET_FORWARD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_TAX_APPORTIONMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_JUR_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_MONITOR_CONTROL_ADJ_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_MONITOR_JUR_CASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_MULTI_CALC_PARM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_PARAMETER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_QUERY_DRILLBACK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_QUERY_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_RECOVERY_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_RELATED_CASES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_SOURCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_STATUS_CHECK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_SUB_ACCT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TAX_COMPONENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TAX_COMP_MEMBER_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TAX_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TAX_ENTITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TAX_FAMILY_MEMBER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TAX_FAMILY_MEMBER_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TAX_LOAD_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TAX_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TAX_STAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TAX_VERSION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TA_COMPONENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TA_COMP_MEMBER_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TA_CONTROL_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TA_FAMILY_MEMBER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TA_FAMILY_MEMBER_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TA_M_TYPE_ENTITY_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TA_SPLIT_PROV_AMOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TA_SPLIT_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TA_SPLIT_RULE_AMOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_TA_VERSION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMBURSABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMBURSABLE_CALCULATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMBURSABLE_PAYMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_AMT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILLING_FREQ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILLING_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_CAP_ADJUSTMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_DETAILS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_DETAILS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_DOLLAR_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_GRP_RELATED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_HOLD_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_INTERFACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_LINE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_MESSAGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_PREMISES_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_REVIEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_REVIEW_ADJS_APPR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_REVIEW_ADJS_DETAILS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_REVIEW_ADJUSTMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_REVIEW_ADJ_SPLIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_REVIEW_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_REVIEW_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_REVIEW_EXCESS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_REVIEW_SPLIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_STATISTICS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_SUPPORT_CR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_SUPPORT_CWIP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_TRANS_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CREDIT_MEMO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_GROUP_BY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_OH_ELIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_OH_EXCLUSIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_OH_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_OVERHEAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_SOURCES_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_TARGET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_TARGET_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_WHERE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CUSTOMER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CUSTOMER_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CUSTOMER_SPLIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CUST_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_DISPLAY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_DISPLAY_ELEMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_ELIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_ELIG_CHARGE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_ELIG_EST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_ESTIMATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_EST_BILL_HIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_EST_OH_ALLOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_EST_OH_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_EST_OH_ELIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_EST_OH_EXCLUSIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_EST_OH_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_EST_OVERHEAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_GL_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_JE_TIMING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_JE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_METHOD_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_OH_ALLOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_OH_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_OH_ELIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_OH_EXCLUSIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_OH_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_OH_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_OVERHEAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_PAYMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_PENALTY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_PENALTY_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_PREMISES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_QUEUE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_QUEUE_USERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_REFUND_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_REFUND_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_REFUND_RATE_SCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_REFUND_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_REFUND_RATIO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_REFUND_RATIO_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_REFUND_SPECIAL_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_REFUND_SPECIAL_CALC_ARGS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_REFUND_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_REPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_REPORT_CR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_REPORT_CWIP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_STATISTIC_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_STATISTIC_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_STATUS_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_SUPER_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_SYSTEM_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_TAX_GROSS_UP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_WORK_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RELATED_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RELATED_ASSET_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RELATED_WOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_AMOUNT_ALLOCATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_AMOUNT_ALLOCATE_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_AMT_ALLOC_REPORTING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_BATCH_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_BLANKET_PROCESSING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_BLANKET_PROCESS_REPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_BLANKET_PROC_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_BLANKET_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_BLANKET_RESULTS_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_BLKT_RESULTS_REPORTING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_CALC_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_CALC_ASSET_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_CALC_CWIP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_CALC_PRA_REVERSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_CALC_PRA_REVERSE_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_DETAIL_WMIS_FEED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_LOCATION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_LOC_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_LOC_ROLLUP_FUNC_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_LOC_ROLLUP_MAPPING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_LOOKUP_QTY_COST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_MAJOR_UNIT_PCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_PRETEST_EXPLAIN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_PRIORITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_PRIORITY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_PRIORITY_TYPE_PRIORITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_PROCESSING_PERIOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_RANGE_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_RANGE_TEST_HEADER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_TABLES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_TEST_PRIORITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_THRESHOLDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_UNIT_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_UNIT_CODE_FUNC_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_WORK_ORDER_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_WORK_ORDER_ORIG_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_WORK_ORDER_SEGMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_WORK_ORDER_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_WORK_ORDER_TEMP_ORIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_WO_RUC_EXCEPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPAIR_WO_SEG_REPORTING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPORT_TIME', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REQ_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RESERVE_RATIOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIREMENT_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIREMENT_UNIT_ASSOCIATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIREMENT_UNIT_WORK_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_BAL_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_DEPR_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_RESERVE_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_UNIT_COMP_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_UNIT_SKU', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_UNIT_STD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_UNIT_TAX_DISTINCTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_UNIT_TYP_SIZ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RET_UNIT_MORT_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REVISION_SELECTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_LOCATION_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_LOC_AST_LOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_LOC_AST_LOC_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_LOC_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_LOC_ROLLUP_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_LOC_ROLL_FC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_LOC_ROLL_FC_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_LOC_ROLL_LOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_LOC_ROLL_LOC_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_PRI_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_PRI_TEST_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_RANGE_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_RANGE_TEST_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_RANGE_TEST_RNG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_RANGE_TEST_RNG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_RNG_TST_UNT_CD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_RNG_TST_UNT_CD_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_SCHEMA_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_TAX_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_TAX_STATUS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_TEST_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_THRESHOLD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_THRESHOLD_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_TST_WOTYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_TST_WOTYPE_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_UC_FC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_UC_FC_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_UC_PRP_GRP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_UC_PRP_GRP_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_UC_UAPU', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_UC_UAPU_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_UC_WOTYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_UC_WOTYPE_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_UNIT_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_UNIT_CODE_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_WO_TAX_REPAIRS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RPR_IMPORT_WO_TAX_REPAIRS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RUS_LINE_ITEMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_ALLO_ERRORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_ALLO_EST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_ALLO_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_ALLO_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_CLOSEOUT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_CLOSEOUT_EXCLUDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_CLOSEOUT_PENDTRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_CLOSEOUT_REVISION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_CLOSEOUT_ROUND2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_CLOSEOUT_ROUNDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_CLOSEOUT_STAGING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_CLOSEOUT_STAG_AL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_CLOSEOUT_STAG_AL_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_CLOSEOUT_STAG_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_CLOSEOUT_STAG_WOE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_SOB_ID_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RWIP_TYPE_SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SALVAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SALVAGE_ANALYSIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SALVAGE_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SELECT_TABS_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SELECT_TABS_GRIDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SKU_SUBSTITUTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SPREAD_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SPR_ANALYSIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('STANDARD_JOURNAL_ENTRIES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('STATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('STATUS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('STCK_KEEP_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('STCK_KEEP_UNIT_PRICE_HIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('STOCK_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('STORES_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUBLEDGER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUBLEDGER_DEPR_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUBLEDGER_ENTRY_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUBLEDGER_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUBSTITUTION_REVIEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUBSTITUTION_REVIEW_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUBSTITUTION_REVIEW_DETAILS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUBST_REVIEW_DETAILS_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUB_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUB_ACCOUNT_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUMMARY_4562', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUMMARY_TRANSACTION_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUSPENDED_CHARGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ACCOUNT_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ACCOUNT_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ACCOUNT_TYPE_DEF', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ACCT_ROLLUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ACTIVITY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ALLOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ALLOC_AMOUNTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ALLOC_DRILL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ALLOC_PERCENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ALLOC_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_APPORTIONMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_APPROVAL_CO_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_APPROVAL_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ASSIGN_NS_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ASSIGN_NS_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BALANCE_DIFFS_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BEG_BAL_LOAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BEG_BAL_LOAD_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS_BOOK_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS_LINE_ITEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS_REPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS_SCHEMA_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS_TAX_BASIS_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_COMPANY_POSTING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_COMPANY_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_COMPLIANCE_EXPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CONSOL_DRILL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CONTROL_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CO_CONSOL_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CO_POST_MS_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CO_POST_MS_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_BALANCES_CON', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_BALANCES_STG1', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_BALANCES_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_BAL_UPDATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_BUDGET_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_COMPANY_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_CO_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_DRILL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_DRILL_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_DRILL_EXPLODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_JE_FIELD_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_JE_UPDATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PAY_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PAY_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PAY_UPDATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PULL_CATEGORIES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PULL_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PULL_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PULL_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PULL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PULL_UPDATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_STRUCT2_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_STRUCTURES2_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_STRUCTURES_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_STRUCT_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CUSTOM_TAB', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CUSTOM_TAB_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DAMPENING_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DAMPENING_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DAMPENING_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DATA_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DATA_SOURCE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DB_SESSION_PARAMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DEF_TAX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DEF_TAX_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DEF_TAX_PROC_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DEF_TAX_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DIFF_IND', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DIT_PROCESS_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DIT_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DIT_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DIT_SCHEMA_CHECKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DS_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EFF_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EFF_RATE_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EFF_RATE_DEBUG1', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EFF_RATE_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EFF_RATE_TMP2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_DEDUCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_DEDUCT_EXC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_DEDUCT_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_DED_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_INCLUDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_INCLUDE_ACT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_JURIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_REP_INCLUDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ERROR_CODES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EST_IMPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EST_IMPORT_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EST_IMPORT_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_FAS109', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_FAS109_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_FAS109_PROC_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_FAS109_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_ACCOUNT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_DATA_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_DATA_CON_DTLS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_ERRORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_MONTH_DEF', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_PAY_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_PAY_CON_DTLS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_IMPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_IMPORT_BS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_IMPORT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_IMPORT_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_EXPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_FIELD_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_HISTORY_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_HISTORY_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_RESULTS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_RESULTS_CON_HIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_UPDATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JUR_ALLO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JUR_ALLO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JUR_EXCEPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JUR_TA_NORM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_MONTHLY_SPREAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_MONTH_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_MONTH_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_MONTH_TYPE_TRUEUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_CURRENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM_DRILL_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM_ETR_CALCS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM_PROC_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM_ROLLUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_MASTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_TYPE_TREATMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_NORM_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_OPER_CO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_OPER_IND', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PAY_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PERIOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PLSQL_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POSTING_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_ERRORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_MAP_NR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_RETRIEVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_RETR_NR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PP_TREE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PROCESSING_ERRORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PROCESS_INFO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PROCESS_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PROCESS_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PROC_DEDUCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_IND', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_TC_ROLLUP_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_TC_ROLLUP_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_TRC_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_TRC_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_UNUSED_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_UNUSED_TC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REG_IND', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REPORTS_CUSTOMIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REPORT_OBJECT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REPORT_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REPORT_OPTION_DTL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REPORT_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CONS_COLS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CONS_ROWS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CONS_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CRITERIA_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CRITERIA_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CUSTOM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CUSTOM_MOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CUSTOM_MOD_DTL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CUSTOM_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CUSTOM_OPT_DTL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_DEFERRED_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_JE_TYPE_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_JE_TYPE_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_MONTHS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_MONTHS_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_ROLLUP_DTL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_ROLLUP_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_ROLL_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_SPECIAL_CRIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_SPECIAL_NOTE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_TREE_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_TREE_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RESULTS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RESULTS_CON_JE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RESULTS_CON_JE_ALT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RESULTS_ETR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ROLLUP_ACCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ROLLUP_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ROLLUP_DTL_ACCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RTP_CHECK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RTP_PROC_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SCHM_M3', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SIGN_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SPREAD_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SPREAD_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SQL_HINTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SQL_MODS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_ADJ_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_EXT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_EXT_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_EXT_ROLL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_FREQ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_PAY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_SUB_CAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_TAX_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUB_ADJ_DEBUG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SYSTEM_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SYSTEM_OPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SYS_OPTION_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TAX_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TAX_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TBS_ACCOUNT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TBS_TREATMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TEMPLATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TEMPLATES_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TRUEUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TRUEUP_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_USER_OPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_VALIDATION_MSGS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_VERSION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACTIVITY_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ADD_AUDIT_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ANNOTATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ARCHIVE_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_AUTH_PROP_TAX_DIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_RECONCILE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_RECONCILE_FP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_RECONCILE_TRANSFER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSACTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSACTIONS_GRP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSFERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSFERS_GRP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSLATE_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSLATE_GROUP_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANS_ADDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CLASS_ROLLUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_COMPANY_CONSOL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CO_DEF_TAX_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CREDIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CREDIT_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CWIP_CLASS_RECONCILE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CWIP_COMPANY_RECORD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CWIP_COMPANY_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_DEPRECIATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_DEPRECIATION_FP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_DEPRECIATION_TRANSFER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_DEPR_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_DEPR_SCHEMA_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_DISTRICT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_EVENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FCST_ARCHIVE_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FCST_BUDGET_ADDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FCST_BUDGET_ADDS_FP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FCST_BUDGET_RETIRES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FORECAST_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FORECAST_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FORECAST_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_HALF_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_ADDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_ADDS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BASIS_AMOUNTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BASIS_AMOUNTS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BK_ALLOC_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BK_ALLOC_ASSIGN_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BK_ALLOC_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BK_ALLOC_GROUP_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BK_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BK_TRANSLATE_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BOOK_RECONCILE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BOOK_RECONCILE_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_CLASS_ROLLUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_CLASS_ROLLUPS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_DEPR_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_DEPR_ADJUST_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_DEPR_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_DEPR_SCHEMA_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_DEPR_SCH_CTRL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_DEPR_SCH_CTRL_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_LOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_LOC_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_LOC_ASSET_LOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_LOC_ASSET_LOC_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_NORM_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_NORM_SCHEMA_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_REC_ITEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_REC_ITEM_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_RETIRE_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_RETIRE_RULES_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_RETS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_RETS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_ROLLUP_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_ROLLUP_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_ROLLUP_DETAIL_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TAX_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TAX_CLASS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TAX_UA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TAX_UA_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TRANS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TRANS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TRANS_CONTROL_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_UA_DEPR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_UA_DEPR_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_VINTAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_VINTAGE_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_INCLUDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_INCLUDE_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_INTERFACE_COS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_JOB_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_JOB_PARAMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_CONFIG_AVAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_CONFIG_SAVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_DEFINITION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_DEF_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_RUN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_RUN_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_LAW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_LAYER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_LIMIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_LIMITATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_MX_INFLATION_INDEX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_MX_INFLATION_MIDPOINT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_PACKAGE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RATES_DELV', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RATE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RATE_CONTROL_DELV', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RECONCILE_ITEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RECORD_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RECORD_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_REPAIRS_ADD_EXPENSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RETIRE_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RET_AUDIT_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RET_AUDIT_TRAIL_GRP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_REVERSAL_BOOK_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ROLLUP_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_SUMMARY_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_BOOK_TRANSFERS_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_COPY_ADD_INIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_COPY_ADD_TRID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_INCFCST_DEPR_ALLOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_MLP_DIS_SALE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_MLP_DIS_SALE_TBTI', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_TRANSFER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_TRID_DELETES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEST_CPR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEST_PEND_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEST_PEND_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEST_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TRANSFER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TRANS_AUDIT_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TRANS_AUDIT_TRAIL_GRP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TYPE_OF_PROPERTY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_UTILITY_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_VINTAGE_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_VINTAGE_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_XFER_TRACEBACK_FINAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_XFER_TRACEBACK_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_YEAR_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMPLATE_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMPLATE_DEPR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_ACCRUAL_WORK_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_ARCHIVE_WORK_ORDER_NO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_ARO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_ARO_LIABILITY_ACCR_DTL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_BILL_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_BUDGET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_CHARGE_SUMMARY_CLOSINGS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_COMPRESS_CPR_ACT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_CONS_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_CONS_LIST_NEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_CPR_QUERY_ID_CHANGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_EQUIP_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_EXCLUDE_TABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_GLOBAL_COMPANY_ID_TBL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_GLOBAL_TAX_BASIS_AMOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_GLOBAL_TAX_BOOK_TBL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_GLOBAL_TAX_CLASS_TBL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_GLOBAL_VINTAGE_TBL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_G_DEF_INCOME_TAX_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_IND_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_IND_LIST_NEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_JOB_TASK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_LDAP_ATTRIB_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_LDAP_SEARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_MYPP_REPORT_ID_CHANGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_MYPP_TODO_ID_CHANGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_NORM_ID_TBL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_PP_ANYQUERY_ID_CHANGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_PROCEDURE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_QUERY_DW_ID_CHANGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_RENUMBER_TRANSFERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_RENUMBER_TRIDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_REPORT_FILTER_ID_CHANGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_RETIRE_TRENDING_FLAGS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_SECURITY_RULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_TABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_TABLE_LIST_NEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_TASK_ID_CHANGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_VERIFY_CAT_ID_CHANGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_VERIFY_ID_CHANGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_VIEW_LIST_NEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_WOCG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_WORK_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TEMP_WORK_ORDER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TE_AGGREGATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TMP_COMPANIES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TMP_TA_SUB_EXT_ROLL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TOWN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TRANSACTION_INPUT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TRANS_LINE_NUMBER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TRANS_LINE_STATS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TREPAIRS_WO_COMPARE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TREPAIRS_WO_CPR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TREPAIRS_WO_CWIP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TREPAIR_FUNDING_PROJ_CHARGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TYPE_SIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNADJUSTED_PLANT_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZATION_TARGET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZATION_TOLERANCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZATION_TOLERANCE_CT_EXCL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZATION_TOLERANCE_ECT_EXCL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZED_WORK_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZED_WORK_ORDER_MEMO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZED_WORK_ORDER_SL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZED_WORK_ORDER_SL_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ALLOC_CGC_INSERTS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ALLOC_CGC_INSERTS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ALLOC_ERRORS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ALLOC_EST_STG_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ALLOC_EST_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ALLOC_LATE_BK_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ALLOC_LATE_BK_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ALLOC_LATE_PT_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ALLOC_LATE_PT_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ALLOC_LIST_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ALLOC_LIST_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ALLOC_STND_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_CONVERT_OCR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_CONVERT_OCR_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ELIG_UNITS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_ELIG_UNITS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_PEND_TRANS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_RATIO_LATE_ADD_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_RATIO_LATE_ADD_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_RATIO_LATE_RWIP_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_RATIO_LATE_RWIP_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_RATIO_UNITS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_RATIO_UNITS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_WO_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_WO_LIST_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_WO_LIST_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNITIZE_WO_STG_UNITS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNIT_ALLOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNIT_ALLOC_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNIT_ALLOC_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNIT_ALLOC_METH_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNIT_ITEM_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNIT_OF_MEASURE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNIT_OF_PRODUCTION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UNIT_TARGET_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UPDATE_WITH_ACTUALS_EXCLUSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UPDATE_WITH_ACTUALS_EXCL_ET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('USER_STAT_TABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('USER_STAT_TABLE_IO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('USE_INDICATOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UTILITY_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UTILITY_ACCOUNT_DEPRECIATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UTILITY_ACCOUNT_NARUC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UTILITY_ACCT_DEPRECIATION_FP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UTIL_ACCT_PROP_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('VALUATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('VERSION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('VINTAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('VINTAGE_SURVIVORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WIP_COMPUTATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WIP_COMP_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WIP_COMP_CHARGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WIP_COMP_EXTENSIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WIP_COMP_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WIP_COMP_PENDING_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WIP_COMP_PEND_TRANS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WIP_COMP_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WIP_COMP_TEMP_WO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WIP_COMP_UNIT_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WIP_COMP_WO_OVERRIDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WIP_COMP_WO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORKFLOW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORKFLOW_AMOUNT_SQL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORKFLOW_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORKFLOW_RULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORKFLOW_SUBSYSTEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORKFLOW_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORKFLOW_TYPE_RULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_ALTERNATIVES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_APPROVAL_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_CASHFLOW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_CHARGE_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_DEPARTMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_FUNDING_PROJ_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_GROUP_BUDGET_ORG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_INITIATOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_MASS_UPDATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_PREREQS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_TAX_REPAIRS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_TAX_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_TYPE_BUDGET_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_TYPE_EST_IMPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_ORDER_VALIDATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_SITUATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_TYPE_ATTRIBUTE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WORK_TYPE_ATTR_VALUE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ACCRUALS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ACCRUALS_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ACCRUAL_EXC_COST_ELEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ACCRUAL_SESSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ACCRUAL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ACCRUAL_TYPE_EXP_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ACCRUED_GAIN_LOSS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_APPROVAL_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_APPROVAL_MULTIPLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_APPROVAL_MULTIPLE_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_APPROVAL_MULTIPLE_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_APPROVAL_MULT_CATEGORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_APPROVAL_MULT_CATEGORY_USER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ARC_ALERT_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ARC_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ARC_CLASS_CL_OPT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ARC_CLASS_WOT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ARC_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ARC_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ARC_RULE_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ARC_RUN_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ARC_RUN_MODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ARC_TEMP_RUN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_AUTO101_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_AUTO106_CHARGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_AUTO106_CHARGES_SUMM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_AUTO106_CLASS_CODES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_AUTO106_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_AUTO106_PENDING_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_AUTO106_PENDING_TRANS_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_AUTO106_TEMP_WOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_AUTO106_WOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_BILL_MATERIAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_CLEAR_OVER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_CLEAR_OVER_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_COMPLETE_DATES_APPROVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DASHBOARD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DASHBOARD_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DASHBOARD_DATAWINDOW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DASHBOARD_DEFAULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DASHBOARD_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DASHBOARD_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DELETE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DELETE_CUSTOM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DOCUMENTATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DOC_COMMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DOC_JUSTIFICATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DOC_JUSTIFICATION_BUD_SUM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DOC_JUSTIFICATION_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DOC_JUSTIFICATION_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DOC_JUSTIFICATION_TABS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DOC_JUSTIFICATION_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_DOC_JUSTIFICATION_WO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ENG_ESTIMATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ENG_ESTIMATE_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ENG_EST_CHILD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ENG_EST_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ESTIMATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ESTIMATED_RETIRE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ESTIMATE_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ESTIMATE_CLASS_CODE_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ESTIMATE_IMPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_ESTIMATE_UNIT_ROLLUP_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_ACTUALS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_ACTUALS_TEMP2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_CLOSINGS_WOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_COPY_REVISION_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_COTENANT_SPREAD_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_DATE_CHANGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_DERIVATION_PCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_FORECAST_CUSTOMIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_FORECAST_OPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_GRID_USER_OPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_HIERARCHY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_HIERARCHY_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTHLY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTHLY_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTHLY_CR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTHLY_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTHLY_ESCALATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTHLY_ESCALATION_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTHLY_FY_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTHLY_SPREAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTHLY_SPREAD_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTHLY_SUBS_DET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTHLY_UPLOAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTHLY_UPLOAD_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTH_IS_EDITABLE_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_MONTH_IS_EDIT_JT_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_PROCESSING_MO_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_PROCESSING_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_PROCESSING_TRANSPOSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_RATE_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_RATE_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_SLIDE_ADJUST_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_SLIDE_RESULTS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_SUMMARY_TBL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_SUPPLEMENTAL_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_SUPPLEMENTAL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_SUPPLEMENTAL_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_TEMPLATE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_TEMPLATE_WO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_TEMP_WEM_FROM_WE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_TRANSPOSE_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_TRANS_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_UPDATE_WITH_ACT_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_UPLOAD_NEW_COMBOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_UPLOAD_NEW_COMBOS2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_UPLOAD_NEW_COMBOS3', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_UPLOAD_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_EST_WO_ENG_EST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_GL_ACCOUNT_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_GL_ACCOUNT_SUMMARY_MO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_GL_ACCOUNT_SUMMARY_PRESET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_GRP_WO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_IMAGE_INTERFACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_INTERFACE_MONTHLY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_INTERFACE_MONTHLY_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_INTERFACE_MONTHLY_IDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_INTERFACE_MONTHLY_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_INTERFACE_OCR_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_INTERFACE_OCR_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_INTERFACE_OCR_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_INTERFACE_REVISIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_INTERFACE_STAGING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_INTERFACE_STAGING_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_INTERFACE_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_INTERFACE_UNIT_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_MATERIAL_REQ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_METRIC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_METRIC_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_METRIC_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_METRIC_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_METRIC_SAVED_GRAPH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_OVERHEAD_DW_NAME', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_OVERHEAD_JUR_ALLO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_OVERHEAD_TARGETS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_OVERHEAD_TARGET_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_PSEUDO_UNITIZE_CC_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_PSEUDO_UNITIZE_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_PSEUDO_UNITIZE_SUMMARY_MO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_REIMBURSABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_REPAIR_LOCATION_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_REPORT_DYNAMIC_SUBTOTAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_STATUS_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_TAX_EXPENSE_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_TAX_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_TYPE_CLASS_CODE_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_TYPE_CLEAR_DFLT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_TYPE_DEPT_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_UNIT_ITEM_PEND_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_VALIDATION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_VALIDATION_RUN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('WO_VALIDATION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('YES_NO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CREW_TYPE', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CREW_TYPE_RATE_HIST', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_CREDIT_CRITERIA', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_CREDIT_CRITERIA_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_CUSTOM_DW', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_CUSTOM_DW_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_GROUP', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_GROUP_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_GROUP_BY', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_GROUP_BY_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_INTERCO', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_INTERCO_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_INTERCO_CRITERIA', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_INTERCO_CRITERIA2', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_INTERCO_CRITERIA2_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_INTERCO_CRITERIA_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_PROCESS_CONTROL', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_PROCESS_CONTROL_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_SYSTEM_CONTROL', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_TARGET', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_TARGET_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_TARGET_CRITERIA', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_ALLOC_TARGET_CRITERIA_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BALANCES_BDG_YE_CLOSE', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_AFUDC_CALC', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_AFUDC_WOTYPE_EXCLUDE', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_COMPONENT', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_ENTRY', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_TEMP', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_DATA_TEST', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_ENT_RATE_OVR', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_FILTER_CRITERIA', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_FIXED_FACTOR', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_CAP_DOLLARS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_LT_TO_ECT', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_REPORT_GRAPHS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_LABOR_VERSION_MAP', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_STEP1', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_STEP1_CALCULATIONS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_STEP1_JUSTIFICATION', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_STEP2', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_BV_MAP', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_ELEMENTS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_ELEMENTS_STG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_TEMPLATE', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_VALUES', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_VALUES_MAP', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_BUDGET_SUM_VALUES_STG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_CANCEL_CONTROL', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMMITMENTS_FIELD_MAP', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMMITMENTS_SUM', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMMITMENTS_TRANSLATE', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMMITMENTS_VALIDATIONS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMPANY', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COMPANY_SECURITY', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_COST_REPOSITORY', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_CROSSTAB_SOURCE_DW', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_CR_ID', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_MODULE', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_MODULE_COLUMN', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_MODULE_JOIN', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DATA_MOVER_MODULE_TABLE', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_CONTROL', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_CONTROL_VIRTUAL', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_OVERRIDE', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_OVERRIDE2', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DERIVER_WO_LINK_CONTROL', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DISTRIB_GROUPS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DISTRIB_QUERIES_GROUPS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_DISTRIB_USERS_GROUPS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_BUILD', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_COLUMNS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_DATA', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_DOL_TYPE', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_FILTERS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_FORMULAS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_FORMULAS2', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_LINKED', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_RUN', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_RUN2', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_RUN_ARC', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_SETUP', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_SETUP_LBL', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_FINANCIAL_REPORTS_SPEC_CHAR', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_GL_ID', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_OPEN_MONTH_NUMBER', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_POST_TO_GL_COLUMNS_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SAVED_QUERIES_BACK', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SCO_BILLING_TYPE_RATES', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SUM', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SUM_HIST', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_SUSPENSE_ACCOUNT', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_ALLOCATIONS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_COMMITMENT', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_GL', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_GL_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_GL_BDG_TEMP', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_GL_TEMP', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_TEMP_CR_JE', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_COMBOS', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_COMBOS_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_COMBOS_STAGING', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_COMBOS_STG_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_CONTROL', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_CONTROL_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_RULES', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CR_VALIDATION_RULES_BDG', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_BILL_SUPPORT_CR', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REIMB_CR_TARGET_CRITERIA', 'CR', 'CR Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_AP_STATUS', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_CLASS_CODE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_COMPONENT', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_DOCUMENT', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_LOCAL_TAX', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_LOC_LOCAL_TAX_DISTRI', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_SCHEDULE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_STATUS', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ASSET_TAX_MAP', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_CANCELABLE_TYPE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_COLUMN_USAGE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_COMPONENT', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_COMPONENT_CHARGE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_COMPONENT_MONTHLY_II_STG', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_COMPONENT_STATUS', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_CPR_ASSET_MAP', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_DEPR_FORECAST', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_EXTENDED_RENTAL_TYPE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_ACCOUNT', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_AMOUNTS_SET_OF_BOOKS', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_APPROVAL', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_ASSET_MAP', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_ASSET_SCHEDULE_CALC_STG', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_ASSET_SCHEDULE_STG', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_ASSET_STG', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_CLASS_CODE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_DOCUMENT', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_GROUP', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_GROUP_FLOATING_RATES', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_OPTIONS', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_PAYMENT_TERM', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_SCHEDULE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_SCHEDULE_BUCKET_STG', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_SCHEDULE_STG', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_STATUS', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_ILR_STG', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_ASSET', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_ASSET_ARCHIVE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_COMPONENT', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_COMPONENT_ARCHIVE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_D_TAX_RATES', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_D_TAX_RATES_ARCHIVE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_FLOAT_RATES', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_FLOAT_RATES_ARCHIVE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_ILR', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_ILR_ARCHIVE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_INTERIM_RATES', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_INTERIM_RATES_ARC', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_INVOICE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_INVOICE_ARCHIVE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_LEASE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_LEASE_ARCHIVE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_LESSOR', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_LESSOR_ARCHIVE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_S_TAX_RATES', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_IMPORT_S_TAX_RATES_ARCHIVE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_INVOICE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_INVOICE_LINE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_INVOICE_PAYMENT_MAP', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_APPROVAL', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_CAP_TYPE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_COMPANY', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_DOCUMENT', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_GROUP', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_GROUP_USER', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_INTERIM_RATES', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_OPTIONS', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_STATUS', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_TYPE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_TYPE_JES_EXCLUDE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_TYPE_TOLERANCE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LEASE_VENDOR', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_LESSOR', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_MEC_CONFIG', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_MLA_CLASS_CODE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_MONTHLY_ACCRUAL_STG', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_MONTHLY_DEPR_STG', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_MONTHLY_TAX', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PAYMENT_APPROVAL', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PAYMENT_FREQ', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PAYMENT_HDR', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PAYMENT_LINE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PAYMENT_TERM_TYPE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PAYMENT_TYPE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_BASIS', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_BASIS_ARC', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_CLASS_CODE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_CLASS_CODE_ARC', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_SET_OF_BOOKS', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_SET_OF_BOOKS_ARC', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_TRANSACTION', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PEND_TRANSACTION_ARC', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_POST_CPR_FIND', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PROCESS_CONTROL', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_PURCHASE_OPTION_TYPE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_RECONCILE_TYPE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_RENEWAL_OPTION_TYPE', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_RENT_BUCKET_ADMIN', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_RETIREMENT_NEW_TERMS', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_RETIREMENT_STATUS', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_TAX_DISTRICT_RATES', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_TAX_LOCAL', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_TAX_RATE_OPTION', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_TAX_STATE_RATES', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_TAX_SUMMARY', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('LS_VENDOR', 'LS', 'LS Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JOURNAL_LAYOUTS', 'NONE', 'NONE Tables that are different by client');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_EXPORT', 'NONE', 'NONE Tables that are different by client');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_TRANSLATE', 'NONE', 'NONE Tables that are different by client');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL_STEPS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('APPROVAL_STEPS_HISTORY', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_APPROVAL_AUTH_LEVEL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ACCOUNT_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ACCOUNT_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ACCOUNT_TYPE_DEF', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ACCT_ROLLUPS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ACTIVITY_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ALLOC', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ALLOC_AMOUNTS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ALLOC_DRILL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ALLOC_PERCENTS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ALLOC_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_APPORTIONMENT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_APPROVAL_CO_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_APPROVAL_DOCUMENT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ASSIGN_NS_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ASSIGN_NS_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BALANCE_DIFFS_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BEG_BAL_LOAD', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BEG_BAL_LOAD_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS_BOOK_ASSIGN', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS_LINE_ITEM', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS_REPORT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS_SCHEMA', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS_SCHEMA_ASSIGN', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_BS_TAX_BASIS_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_COMPANY_POSTING', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_COMPANY_ROLLUP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_COMPLIANCE_EXPORT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CONSOL_DRILL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CONTROL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CONTROL_DOCUMENT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CO_CONSOL_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CO_POST_MS_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CO_POST_MS_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_BALANCES_CON', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_BALANCES_STG1', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_BALANCES_TRANS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_BAL_UPDATES', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_BUDGET_DATA', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_COMPANY_FIELDS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_CO_TRANS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_DRILL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_DRILL_ARC', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_DRILL_EXPLODE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_JE_FIELD_MAP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_JE_UPDATES', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PAY_CONTROL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PAY_CRITERIA', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PAY_UPDATES', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PULL_CATEGORIES', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PULL_CONTROL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PULL_CRITERIA', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PULL_FIELDS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PULL_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_PULL_UPDATES', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_STRUCT2_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_STRUCTURES2_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_STRUCTURES_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CR_STRUCT_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CUSTOM_TAB', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_CUSTOM_TAB_FIELDS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DAMPENING_MAP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DAMPENING_OPTION', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DAMPENING_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DATA_SOURCES', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DATA_SOURCE_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DB_SESSION_PARAMS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DEF_TAX', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DEF_TAX_CONTROL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DEF_TAX_PROC_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DEF_TAX_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DIFF_IND', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DIT_PROCESS_OPTION', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DIT_RATE_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DIT_SCHEMA', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DIT_SCHEMA_CHECKS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DOCUMENT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_DS_VALUES', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EFF_RATE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EFF_RATE_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EFF_RATE_DEBUG1', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EFF_RATE_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EFF_RATE_TMP2', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_DEDUCT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_DEDUCT_EXC', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_DEDUCT_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_DED_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_INCLUDE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_INCLUDE_ACT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_JURIS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ENTITY_REP_INCLUDE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ERROR_CODES', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EST_IMPORT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EST_IMPORT_ARC', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_EST_IMPORT_STG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_FAS109', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_FAS109_CONTROL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_FAS109_PROC_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_FAS109_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_ACCOUNT_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_DATA', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_DATA_CONTROL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_DATA_CON_DTLS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_ERRORS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_MONTH_DEF', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_PAY_CONTROL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_GL_PAY_CON_DTLS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_IMPORT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_IMPORT_BS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_IMPORT_CONTROL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_IMPORT_DOCUMENT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_DETAIL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_EXPORT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_FIELD_MAP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_HISTORY', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_HISTORY_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_HISTORY_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_RESULTS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_RESULTS_CONTROL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_RESULTS_CON_HIS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_SCHEMA', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_TRANSLATE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JE_UPDATES', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JUR_ALLO', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JUR_ALLO_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JUR_EXCEPTIONS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_JUR_TA_NORM', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_MONTHLY_SPREAD', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_MONTH_GROUP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_MONTH_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_MONTH_TYPE_TRUEUP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_CURRENT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM_DRILL_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM_ETR_CALCS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM_PROC_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM_ROLLUPS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ITEM_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_MASTER', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_RATE_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_ROLLUP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_M_TYPE_TREATMENT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_NORM_SCHEMA', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_OPER_CO', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_OPER_IND', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PAY_FIELD', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PERIOD', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PLSQL_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POSTING_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_ARC', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_ERRORS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_MAP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_MAP_NR', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_OPTION', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_RETRIEVE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_POWERTAX_RETR_NR', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PP_TREE_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PROCESSING_ERRORS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PROCESS_INFO', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PROCESS_OPTION', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PROCESS_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PROC_DEDUCT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_IND', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_TC_ROLLUP_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_TC_ROLLUP_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_TRC_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_TRC_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_UNUSED_SCHEMA', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_PT_UNUSED_TC', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REG_IND', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REPORTS_CUSTOMIZE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REPORT_OBJECT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REPORT_OPTION', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REPORT_OPTION_DTL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REPORT_TRANSACTION', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CONS_COLS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CONS_ROWS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CONS_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CRITERIA_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CRITERIA_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CUSTOM', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CUSTOM_MOD', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CUSTOM_MOD_DTL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CUSTOM_OPTION', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_CUSTOM_OPT_DTL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_DEFERRED_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_JE_TYPE_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_JE_TYPE_TEMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_MONTHS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_MONTHS_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_ROLLUP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_ROLLUP_DTL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_ROLLUP_GROUP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_ROLL_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_SPECIAL_CRIT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_SPECIAL_NOTE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_TREE_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_REP_TREE_TEMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RESULTS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RESULTS_CONTROL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RESULTS_CON_JE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RESULTS_CON_JE_ALT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RESULTS_ETR', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ROLLUP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ROLLUP_ACCT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ROLLUP_DETAIL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_ROLLUP_DTL_ACCT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RTP_CHECK', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_RTP_PROC_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SCHM_M3', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SIGN_FILTER', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SPREAD_TEMPLATE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SPREAD_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SQL_HINTS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SQL_MODS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_ADJUST', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_ADJ_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_EXT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_EXT_DATA', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_EXT_ROLL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_FREQ', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_PAY_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_SUB_CAT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_TAX_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUBLEDGER_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SUB_ADJ_DEBUG', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SYSTEM_CONTROL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SYSTEM_OPTIONS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_SYS_OPTION_VALUES', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TAX_CONTROL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TAX_YEAR', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TBS_ACCOUNT_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TBS_TREATMENT', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TEMPLATES', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TEMPLATES_DATA', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TRUEUP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_TRUEUP_TMP', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_USER_OPTIONS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_VALIDATION_MSGS', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_VERSION', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACCRUAL_VERSION_TYPE', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TMP_TA_SUB_EXT_ROLL', 'PROV', 'PROV Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_ADJUST', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_ADJUST_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_AUTHORITY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_AUTHORITY_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_TYPE_DATA', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_YEAR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROPERTY_TAX_YEAR_LOCK', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_ALLO_DEFINITION', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_NET_TAX_RESERVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_NET_TAX_RESERVE_REP', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_TAX_RESERVE_PERCENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_TYPE_ASSIGN_TREE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_TYPE_ASSIGN_TREE_CWIP', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PROP_TAX_TYPE_PROP_TAX_ADJUST', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_DOCUMENTS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_DOCUMENTS_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_SCRIPT_LOG', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_SYSTEM_OPTIONS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_SYSTEM_OPTIONS_CENTERS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_SYSTEM_OPTIONS_VALUES', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PTC_USER_OPTIONS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_ASSIGN_TREE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_ASSIGN_TREE_ASSIGN', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_ASSIGN_TREE_LEVELS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_CHARGE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_CONTROL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_DEFERRAL_AMOUNT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_DEFERRAL_GROUP', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_PENDING', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_PENDING_BAK', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_STATUS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACCRUAL_YEAR_CONTROL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACTUALS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ACTUALS_SUMMARY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ALLOCATE_ASSESSMENT_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ALLOCATION_METHOD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ALLOCATION_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_APPRAISER', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_APPROVAL_AUTH_LEVEL_BAK', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSED_VALUE_ALLOCATION', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSED_VALUE_COUNTY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSED_VALUE_TAX_DISTRICT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSMENT_GROUP', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSMENT_GROUP_CASE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSMENT_GROUP_TYPES', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSMENT_YEAR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ASSESSOR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_AUTHORITY_RATE_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_CASE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_CASE_CALC', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_CASE_CALC_AUTHORITY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_CASE_CONTROL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_CASE_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_COMPANY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_COMPANY_OVERRIDE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_CONTROL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_COUNTY_EQUALIZATION', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_FIELD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_GENERATOR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_LINK', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_TYPE_FILTER', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_TYPE_PERSON', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DATE_WEEKEND_METHOD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DEPR_FLOOR_AUTOADJ', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DEPR_FLOOR_AUTOADJ_TYPES', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_DISTRICT_EQUALIZATION', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_EQUALIZATION_FACTOR_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ESCALATED_VALUE_INDEX', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_ESCALATED_VALUE_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ASSESSOR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ASSESSOR_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ASSETS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ASSETS_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ASSET_LOC', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ASSET_LOC_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_AUTH_DIST', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_AUTH_DIST_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_CLASS_CODE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_CLASS_CODE_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_CWIP_ASSIGN', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_CWIP_ASSIGN_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_DIVISION', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_DIVISION_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ESCVAL_INDEX', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_ESCVAL_INDEX_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LEDGER', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LEDGER_ADJ_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LEDGER_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LEVY_RATES', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LEVY_RATES_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LOC_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_LOC_TYPE_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_MAJOR_LOC', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_MAJOR_LOC_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_MINOR_LOC', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_MINOR_LOC_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_MUNICIPALITY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_MUNICIPALITY_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PARCEL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PARCEL_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PAYMENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PAYMENT_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_ASMT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_ASMT_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_GEO', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_GEOFCTR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_GEOFCTR_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_GEO_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_HISTORY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_HISTORY_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_LOC', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_LOC_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_RSP', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_RSP_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_RSP_ENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PRCL_RSP_ENT_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PREALLO', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PREALLO_ADJ_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PREALLO_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PROPTAX_LOC', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_PROPTAX_LOC_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_RETIRE_UNIT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_RETIRE_UNIT_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_RSVFCTR_PCTS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_RSVFCTR_PCTS_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STATS_FULL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STATS_FULL_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STATS_INCR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STATS_INCR_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STMT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STMT_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STMT_LINE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_STMT_LINE_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_TAX_DIST', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_TAX_DIST_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_TYPE_CODE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_TYPE_CODE_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_UNIT_COST', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_UNIT_COST_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_UTIL_ACCOUNT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_UTIL_ACCOUNT_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_VALUE_VAULT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_IMPORT_VALUE_VAULT_ARCHIVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_INCREMENTAL_BASE_YEAR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_INCREMENTAL_INITIALIZATION', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_INCREMENTAL_INIT_ADJ', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_INSTALLMENT_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_INTERFACE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_INTERFACE_TAX_YEAR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_ADJUSTMENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_DETAIL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_DETAIL_COLUMN', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_DETAIL_MAP', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_DETAIL_MAP_FIELDS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_DETAIL_SOURCE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_TAX_YEAR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEDGER_TRANSFER', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEVY_CLASS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LEVY_RATE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_LOCATION_ROLLUP', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_MARKET_VALUE_RATE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_MARKET_VALUE_RATE_COUNTY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_METHOD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_NEG_BAL_AUTOTRANS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_APPEAL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_APPEAL_EVENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_APPEAL_STATUS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_APPEAL_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_APPRAISAL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_ASSESSMENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_ASSET', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_AUTO_ADJUST', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_FLEX_FIELDS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_FLEX_FIELD_CONTROL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_FLEX_FIELD_USAGE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_FLEX_FIELD_VALUES', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_GEOGRAPHY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_GEOGRAPHY_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_GEOGRAPHY_TYPE_STATE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_GEO_TYPE_FACTORS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_HISTORY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_LOCATION', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_RESPONSIBILITY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_RESPONSIBILITY_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_RESPONSIBLE_ENTITY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PARCEL_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PAYMENT_STATUS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PERIOD_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_POWERTAX_COMPANY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PREALLO_ADJUSTMENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PREALLO_LEDGER', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PREALLO_TRANSFER', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PROCESS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PROCESS_COMPANY_STATE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PROCESS_COMPANY_STATE_TY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PROCESS_DEPENDENCY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PROCESS_STATUS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PROCESS_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_PYMT_CNTR_1000_IDS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_QUANTITY_CONVERSION', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_QUERY_COLUMNS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_QUERY_COL_SAVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_QUERY_FILTER_SAVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_QUERY_MAIN_SAVE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_QUERY_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RATES', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RATE_DEFINITION', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RATE_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_COMPANY_FIELD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_COUNTY_FIELD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_FIELD_LABELS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_LOCATION_FIELD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_PARCEL_FIELD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_PT_COMPANY_FIELD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_STATE_FIELD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_TAX_DISTRICT_FIELD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_REPORT_TYPE_CODE_FIELD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RESERVE_FACTORS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RESERVE_FACTOR_COUNTY_PCTS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RESERVE_FACTOR_DIST_PCTS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RESERVE_FACTOR_PERCENTS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RESERVE_FACTOR_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_RESERVE_METHOD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_SCHEDULE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_SCHEDULE_INSTALLMENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_SCHEDULE_PERIOD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_SGROUP_ROLLUP', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_SGROUP_ROLLUP_ASSIGN', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_SGROUP_ROLLUP_VALUES', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_GROUP', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_GROUP_PAYMENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_GROUP_SCHEDULE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_INSTALLMENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_LINE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_LINE_PAYMENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_PAYEE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_PAYMENT_APP_BAK', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_PERIOD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_STATEMENT_YEAR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_STATUS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_VERIFIED_STATUS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATEMENT_YEAR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATISTICS_AUTOCREATE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATISTICS_FULL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATISTICS_INCREMENTAL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_STATISTICS_SPREAD_RULES', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TAXABLE_VALUE_RATE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TAXABLE_VALUE_RATE_COUNTY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ACCRUAL_PRIOR_MONTH', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ACTUALS_ASMTS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ACTUALS_LOAD_PCTS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ACTUALS_TOTAL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ALLO_INCR_BASE_PCTS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ALLO_INCR_TOTAL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ALLO_PERCENTS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_ALLO_SPREAD', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_BILLS_COPY', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_BILLS_RECALC_TERMS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_CASECALC_ROUNDING', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_CASECALC_TOTALS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_CPRPULL_BASIS_ADJS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_CPRPULL_DEPRMONTH', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_CPRPULL_LEDGERDETAIL', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_IDS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_IDS2', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_IDS3', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_IDS4', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_LEDGER_ADDS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_LEDGER_ADJS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_LEDGER_ADJUSTMENT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_LEDGER_TAX_YEAR', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_LEDGER_TRANSFERS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_PARCELS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_PARCELS_ASMT_GP', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_PREALLO_ADJS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_PREALLO_TRANSFERS', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_SCENARIO_FORMULA', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TEMP_STATEMENT_LINES', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TYPE_ASSIGN_CWIP_PSEUDO', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TYPE_NATIONAL_MAP', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TYPE_ROLLUP', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TYPE_ROLLUP_ASSIGN', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_TYPE_ROLLUP_VALUES', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_UNIT_COST', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_UNIT_COST_AMOUNT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_UNIT_COST_AMOUNT_VINTAGE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_UNIT_COST_TYPE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_USER_INPUT_LEDGER', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VALUE_VAULT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_SCENARIO', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_SCENARIO_RESULT', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_SCENARIO_RUN', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_TEMPLATE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_TEMPLATE_EQUATION', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_TEMPLATE_EQ_FORMULA', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_TIMEFRAME', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_VARIABLE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VAL_VARIABLE_SOURCE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_VINTAGE_OPTION', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PT_YEAR_OVERRIDE', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_AUTH_PROP_TAX_DIST', 'PT', 'PT Module Exclude List');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLASS_CODE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLASS_CODE_DEFAULT', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CLASS_CODE_VALUES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_SETUP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COMPANY_SUMMARY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CURRENCY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CURRENCY_RATE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CURRENCY_RATE_TYPE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CURRENCY_SCHEMA', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CURRENCY_TYPE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MORTALITY_CURVE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MORTALITY_CURVE_POINTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('MORTALITY_CURVE_RET_POINTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('POWERPLANT_COLUMNS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('POWERPLANT_DDDW', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('POWERPLANT_TABLES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('POWERPLANT_TEMPLATE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_ACTIONS_WINDOWS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_MENU_ITEMS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_QUERY_FILTERS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_REPORT_PACKAGE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_REPORT_PACKAGE_REPORTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_SYSTEM_OPTIONS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_SYSTEM_OPTIONS_COMPANY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_SYSTEM_OPTIONS_MODULE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_SYSTEM_OPTIONS_VALUES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_SYSTEM_OPTIONS_WKSP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_USER_OPTIONS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_WORKSPACE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_WORKSPACE_LINKS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPBASE_WORKSPACE_OBJECT_TYPE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PPVERIFY_LOCK', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ADSI_ATTRIBUTES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ADSI_DIRECTORY_PATH', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ADSI_MAP_ATTRIBUTES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ALERT_PROCESSES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ALERT_PROCESS_LOG', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AMT_FORMAT', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ANY_QUERY_CRITERIA', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ANY_QUERY_CRITERIA_FIELDS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ANY_REQUIRED_FILTER', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_ADMIN_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_ARO_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_BUDGET_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_CPR_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_CR_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_DEPRFCST_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_DEPRSTUDY_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_DEPR_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_LEASE_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_POWERTAX_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_PROJECT_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_PROPTAX_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_PROVISION_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_REG_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_SYSTEM_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_TABLES_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDITS_TASK_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDIT_DDL_TABLE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDIT_LOGON_TABLE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_AUDIT_TRAIL_DELETES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_JOBS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_PROGRAMS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_PROGRAM_GROUP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_QUEUES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORTS_DATA', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORTS_LOG', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORT_ARGS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORT_CLASS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORT_FORMAT', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORT_OUTPUT', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_REPORT_PRINTER', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_RPTS_DISTR_GRPS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_RPTS_SCHEDULE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_BATCH_RPTS_VIEWED', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CALENDAR', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CHARGE_SPLIT_RUNNING', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CLASS_CODE_GROUPS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CLIENT_EXTENSIONS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_COLOR_LOOKUP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_COMPANY_SECURITY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_COMPANY_SECURITY_TEMP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONVERSION_WINDOWS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_BATCHES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_BATCHES_CLASS_CODE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_FERC_XLAT', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_MAPPING', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_REF_ID', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_STAGE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_TEMPLATES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_TEMPLATES_DATA', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_TEMP_ACTIVITY_ID', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_CONV_TEMP_COST_QUANTITY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DATABASE_OBJECT', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DATAWINDOW_EXCEL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DATAWINDOW_HINTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DROP_CONSTRAINTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER_MAPPING', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER_RESTRICTIONS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER_SAVED', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER_SAVED_VALUES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER_VALUES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_FILTER_VALUES_DW', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYNAMIC_RPT_USER_SAVED', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_DYN_FILTER_SAVED_VALUES_DW', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_EDIT_TYPE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_ERROR_MESSAGE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_FLEX_NAMES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_FLEX_NAMES_MLS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_HTMLHELP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_COLUMN', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_COLUMN_LOOKUP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_FILE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_LOOKUP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_RUN', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_SUBSYSTEM', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_TEMPLATE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_TEMPLATE_EDITS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_TEMPLATE_FIELDS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_TYPE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_TYPE_SUBSYSTEM', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_IMPORT_TYPE_UPDATES_LOOKUP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_INTEGRATION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_INTEGRATION_COMPONENTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_INTERFACE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_INTERFACE_DATES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_INTERFACE_GROUPS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JL_BIND_ARG_CODE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JOB_REQUEST', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JOURNAL_DATA_SQL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JOURNAL_KEYWORDS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JOURNAL_LAYOUTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_JOURNAL_TYPES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LANGUAGES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LDAP_ATTRIBUTES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LDAP_MAP_ATTRIBUTES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LDAP_PROFILES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LDAP_USER', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LDAP_USER_SYNC_LOG', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_LOCALE_ID', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MAIL_DATA', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MANUAL_INTERFACE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MISC_SEARCH', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MODULES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_ALERT_DASHBOARD', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_ALERT_DASHBOARD_PG', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_ALERT_DASH_COLUMNS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_APPROVAL_CONTROL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_METRIC_DW', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_REPORT_TYPE_TASK', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_TASK', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_TASK_STEP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_TAX_REPORT_FILTER', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_TIME', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_TIME_REPORT_TIME', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_LAYOUT', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_METRIC_DW', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_QUERY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_REPORT', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_REPORT_PARM', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_TASK', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_TEMPLATE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_TODO_ATTACHMENTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_TODO_LIST', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_MYPP_USER_VALUES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_OBJECTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_OBJECTS_COPY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PLSQL_DEBUG_ENABLE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_POST_LOCK', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_DOCUMENTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_ERROR_INFO', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_ERROR_MSG', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_ERR_INFO_ACT', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_INFO_TYPE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_KICKOUT_COLUMNS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_KICKOUT_CONTROL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_KICKOUT_TABLE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_MESSAGES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_OCCURRENCES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_OCC_ATTRIBUTE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_OCC_ERROR_INFO', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_OCC_METRIC', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_RETURN_VALUES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESSES_RUNNING', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_PROCESS_ERROR_ACTION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY_DW', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY_DW_CC_VALUES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY_DW_COLS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY_DW_FILTER', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY_SCALAR_KEYWORDS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_QUERY_SCALAR_SELECT', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_RECORD_LOG', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_ARGS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_COMPANY_SECURITY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_DATA', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_ENVIR', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_FILTER', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_GROUPS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_LINK', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_LINK_SUBSTITUTION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_STATUS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_SUBSYSTEM', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORTS_TIME_OPTION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORT_FILTER', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORT_FILTER_COLUMNS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REPORT_TYPE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_REQUIRED_TABLE_COLUMN', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SCHEMA_CHANGE_LOG', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SCHEMA_UPGRADE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_DATA', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_GROUPS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_GROUP_TYPES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_LOGIN_PROFILE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_LOG_RECORD', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_OBJECTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_OBJECT_RULES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_PASSWD_GROUP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_RULES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_RULES_GROUP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_USERS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_USERS_ALIAS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_USERS_FILTERS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_USERS_GROUPS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SECURITY_USER_STATUS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SESSION_PARAMETERS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_STATISTICS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SYSTEM_CONTROL_COMPANY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_SYSTEM_ERRORS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_AUDITS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_AUDITS_OBJ_ACTIONS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_AUDITS_PK_LOOKUP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_AUDITS_TRAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_GROUPS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_LOG', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_MONTHS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_TYPE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TABLE_YEARS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TASK_LIST', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TEMP_IMPORT_FILE_LINES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TEST_PERFORMANCE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TOOLTIP_HELP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TREE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TREEVIEW_TEMP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TREE_CATEGORY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_TREE_TYPE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_UPDATE_FLEX', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_USER_DW_MICROHELP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_USER_PROFILE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_USER_PROFILE_DETAIL', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_BATCH', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_CATEGORY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_CATEGORY_DESCRIPTION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_COMMENTS_CATEGORY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_COMMENTS_LIST', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_RESULTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_USER_COMMENTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_USER_KEY_FIELDS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERIFY_USER_STD_COMMENTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERSION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_VERSION_EXES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_AUTHENTICATION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_AUTHENTICATION_DEVICE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_AUTHENTICATION_HISTORY', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_AUTHENTICATION_USERS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_COMPONENT', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_CONFIGURATION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_DATA', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_DATATYPES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_LOCALIZATION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_LOCALIZATION_REVISION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_LOCALIZATION_STRING', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_LOCALIZATION_STRING2', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_LOG', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_MODULES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_MODULE_COMPONENTS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_PRINTERS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_SECURITY_PERMISSION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_SECURITY_REVISION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_SITES', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_USER_ACCESS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_VIEW_CHANGE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WEB_VIEW_SESSION', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WINDOW_DISABLE_RESIZE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WINDOW_SIZE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WORKSHEETS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WORK_ORDER_TYPE_GROUPS', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WO_EST_CUSTOMIZE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WO_EST_CUSTOMIZE_TEMP', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('PP_WO_TASK_CUSTOMIZE', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REPORT_TIME', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('YES_NO', 'SYSTEM INCLUDE', 'Include List for all System Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ADJUST_CONVENTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('AMORTIZATION_TYPE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_BASIS_AMOUNTS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_DEFERRED_INCOME_TAX', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_DEFERRED_INCOME_TAX_TRANSF', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_DFIT_FORECAST_OUTPUT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_FCST_DFIT_FORECAST_OUTPUT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_FCST_TAX_FORECAST_INPUT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_FCST_TAX_FORECAST_OUTPUT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_ANNOTATION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_BOOK_RECONCILE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_BOOK_RECONCILE_TRANSFE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_DEPRECIATION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_DEPRECIATION_TRANSFER', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_FORECAST_INPUT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_FORECAST_OUTPUT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_RECORD_CONTROL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_RENUMBER_ID', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('ARC_TAX_TRANSFER_CONTROL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BASIS_AMOUNTS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BOOK_ALLOC_ASSIGN', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BOOK_ALLOC_GROUP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BOOK_ALLOC_RETIREMENTS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('BOOK_ALLOC_RETIRE_PROCESS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('CAP_GAIN_CONVENTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('COST_OF_REMOVAL_CONVENTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_INCOME_TAX', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_INCOME_TAX_RATES', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_INCOME_TAX_TRANSFER', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_RATES', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_TAX_ASSIGN', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEFERRED_TAX_SCHEMA', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DEF_INCOME_TAX_RATES_VERSION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DFIT_FORECAST_OUTPUT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('DIT_RATE_VERSION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('EST_SALVAGE_CONVENTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_DEF_TAX_RATE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_TAX_DEPR', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_TAX_DEPR_RATE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('FCST_TAX_METHOD', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('GAIN_LOSS_CONVENTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JURISDICTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('JUR_ALLO', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('NORMALIZATION_PCT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('NORMALIZATION_SCHEMA', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('NORM_TYPE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RECOVERY_PERIOD', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('REG_FUNCTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_BAL_CONVENTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_DEPR_CONVENTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('RETIRE_RESERVE_CONVENTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('SUMMARY_4562', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ACTIVITY_CODE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ADD_AUDIT_TRAIL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ANNOTATION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ARCHIVE_STATUS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_RECONCILE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_RECONCILE_FP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_RECONCILE_TRANSFER', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_SCHEMA', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSACTIONS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSACTIONS_GRP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSFERS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSFERS_GRP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSLATE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSLATE_GROUP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANSLATE_GROUP_MAP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_BOOK_TRANS_ADDS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CLASS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CLASS_ROLLUPS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_COMPANY_CONSOL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CONVENTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CO_DEF_TAX_SCHEMA', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CREDIT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CREDIT_SCHEMA', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CWIP_CLASS_RECONCILE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CWIP_COMPANY_RECORD', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_CWIP_COMPANY_ROLLUP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_DEPRECIATION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_DEPRECIATION_FP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_DEPRECIATION_TRANSFER', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_DEPR_SCHEMA', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_DEPR_SCHEMA_CONTROL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_DISTRICT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_EVENT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FCST_ARCHIVE_STATUS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FCST_BUDGET_ADDS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FCST_BUDGET_ADDS_FP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FCST_BUDGET_RETIRES', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FORECAST_INPUT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FORECAST_OUTPUT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_FORECAST_VERSION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_HALF_YEAR', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_ADDS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_ADDS_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BASIS_AMOUNTS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BASIS_AMOUNTS_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BK_ALLOC_ASSIGN', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BK_ALLOC_ASSIGN_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BK_ALLOC_GROUP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BK_ALLOC_GROUP_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BK_TRANSLATE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BK_TRANSLATE_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BOOK_RECONCILE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_BOOK_RECONCILE_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_CLASS_ROLLUPS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_CLASS_ROLLUPS_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_DEPR_ADJUST', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_DEPR_ADJUST_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_DEPR_SCHEMA', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_DEPR_SCHEMA_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_DEPR_SCH_CTRL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_DEPR_SCH_CTRL_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_LOC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_LOC_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_LOC_ASSET_LOC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_LOC_ASSET_LOC_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_NORM_SCHEMA', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_NORM_SCHEMA_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_REC_ITEM', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_REC_ITEM_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_RETIRE_RULES', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_RETIRE_RULES_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_RETS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_RETS_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_ROLLUP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_ROLLUP_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_ROLLUP_DETAIL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_ROLLUP_DETAIL_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TAX_CLASS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TAX_CLASS_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TAX_UA', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TAX_UA_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TRANS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TRANS_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TRANS_CONTROL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_TRANS_CONTROL_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_UA_DEPR', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_UA_DEPR_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_VINTAGE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_IMPORT_VINTAGE_ARC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_INCLUDE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_INCLUDE_ACTIVITY', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_INTERFACE_COS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_JOB_LOG', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_JOB_PARAMS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_CONFIG_AVAIL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_CONFIG_SAVE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_DEFINITION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_DEF_FIELDS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_FIELDS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_RUN', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_K1_EXPORT_RUN_RESULTS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_LAW', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_LAYER', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_LIMIT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_LIMITATION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_LOCATION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_METHOD', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_MX_INFLATION_INDEX', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_MX_INFLATION_MIDPOINT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_PACKAGE_CONTROL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RATES', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RATES_DELV', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RATE_CONTROL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RATE_CONTROL_DELV', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RECONCILE_ITEM', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RECORD_CONTROL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RECORD_DOCUMENT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_REPAIRS_ADD_EXPENSE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RETIRE_RULES', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RET_AUDIT_TRAIL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_RET_AUDIT_TRAIL_GRP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_REVERSAL_BOOK_SUMMARY', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ROLLUP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_ROLLUP_DETAIL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_SUMMARY_CONTROL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_BOOK_TRANSFERS_STG', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_COPY_ADD_INIT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_COPY_ADD_TRID', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_INCFCST_DEPR_ALLOC', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_MLP_DIS_SALE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_MLP_DIS_SALE_TBTI', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_TRANSFER_CONTROL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEMP_TRID_DELETES', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEST_CPR_ACTIVITY', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEST_PEND_BASIS', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEST_PEND_TRANSACTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TEST_VERSION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TRANSFER_CONTROL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TRANS_AUDIT_TRAIL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TRANS_AUDIT_TRAIL_GRP', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_TYPE_OF_PROPERTY', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_UTILITY_ACCOUNT', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_VINTAGE_CONVENTION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_VINTAGE_TRANSLATE', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_XFER_TRACEBACK_FINAL', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_XFER_TRACEBACK_STG', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('TAX_YEAR_VERSION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('UTILITY_ACCOUNT_DEPRECIATION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('VERSION', 'TAX INCLUDE', 'Include List for all Tax Tables');
insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
 values ('VINTAGE', 'TAX INCLUDE', 'Include List for all Tax Tables');

commit;