SET SERVEROUTPUT ON
/*

This block of code is used to change the PWRPLANT_ROLE_DEV password to some other value.
Changing this password does not affect the PowerPlan processing as long as you set the PWRPLANT_ROLE_DEV
role password in Oracle and use the same password that you are using in this function.  This function encrypts
the password and inserts the encrypted value into one of the PWRPLANT security tables.

To set the PWRPLANT_ROLE_DEV password in Oracle use the following command replace <password> with the new password:
   alter user PWRPLANT_ROLE_DEV identified by <password>;

** PWRPLANT_ROLE_DEV should not be a users default role.
** It should however be granted as a valid role for the PowerPlan users unless they have been set up
** as Read Only users in which case they should be granted the PWRPLANT_RDONLY_ROLE role instead.

*/

declare
   CODE      int;
   NEWPASSWD varchar(200);

begin
   NEWPASSWD := '<password>'; --change <password> to whatever password you used for the PWRPLANT_ROLE_DEV role.

   CODE      := PWRPLANT_ADMIN('setrole', 'PWRPLANT42PPC', NEWPASSWD, '', '', '', '');
   if CODE = 0 then
      DBMS_OUTPUT.PUT_LINE('Successful');
   else
      DBMS_OUTPUT.PUT_LINE('NOT Successful');
   end if;
end;
/
