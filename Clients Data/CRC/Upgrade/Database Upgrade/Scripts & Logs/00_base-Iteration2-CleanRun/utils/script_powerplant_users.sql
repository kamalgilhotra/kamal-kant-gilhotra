SET ECHO OFF
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: script_powerplant_users.sql
|| Description: Scripts out users in PowerPlant to create Oracle users.
||              Uncomment Line10 if you are using a Proxy User
||
||              Example of a proxy user: create user PPCSSO identified by password;
||                                       grant connect to PPCSSO;
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| V10.1   11/13/2008 Lee Quinn      Created
||============================================================================
*/

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.script_powerplant_users.log

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   V_RCOUNT number;

begin
   DBMS_OUTPUT.ENABLE(buffer_size => NULL);
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select USERS,
                                 'create user ' || USERS || ' identified by password' LINE1,
                                 '   default tablespace USERS' LINE2,
                                 '   temporary tablespace TEMP' LINE3,
                                 '   profile DEFAULT' LINE4,
                                 '   quota unlimited on USERS;' LINE5,
                                 'grant connect to ' || USERS || ';' LINE6,
                                 'grant pwrplant_role_dev to ' || USERS || ';' LINE7,
                                 'grant pwrplant_role_user to ' || USERS || ';' LINE8,
                                 'alter user ' || USERS ||
                                 ' default role CONNECT, PWRPLANT_ROLE_USER;' LINE9,
                                 'alter user ' || USERS || ' grant connect through PPCSSO;' LINE10
                            from PP_SECURITY_USERS
                           order by USERS)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE('/*');
      DBMS_OUTPUT.PUT_LINE('|| Name = ' || CSR_CREATE_SQL.USERS);
      DBMS_OUTPUT.PUT_LINE('*/');
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE1);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE2);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE3);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE4);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE5);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE6);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE7);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE8);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE9);
      --     DBMS_OUTPUT.PUT_LINE(csr_create_sql.LINE10);
   end loop;

   DBMS_OUTPUT.PUT_LINE('/*');
   DBMS_OUTPUT.PUT_LINE('||     ' || V_RCOUNT || ' Users scripted.');
   DBMS_OUTPUT.PUT_LINE('*/');
end;
/

/*
|| END Script PowerPlant Users
*/
SPOOL OFF
SET ECHO ON
