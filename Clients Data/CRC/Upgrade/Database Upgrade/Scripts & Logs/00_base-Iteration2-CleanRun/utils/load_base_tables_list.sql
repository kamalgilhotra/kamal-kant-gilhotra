/*
||============================================================================
|| Application: PowerPlant
|| Object Name: load_base_tables_list.sql
|| Description: Create Insert statements from Base.
||              This is used to filter in the following scripts:
||              130_changedb_v10_user_id_18.sql
||              132_changedb_v10_rename_constraints_make_sql.sql
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| V10.0.6 03/25/2008 Lee Quinn      Created
|| V10.0.7 03/28/2008 Lee Quinn      Point Release
|| V10.0.8 05/01/2008 Lee Quinn      Point Release
|| V10.0.9 06/16/2008 Lee Quinn      Point Release
|| V10.1.1 08/29/2008 Lee Quinn      Point Release
||============================================================================
*/

/*
SQL to build the insert statements:

select 'insert into TEMP_EXCLUDE_TABLE_LIST (TABLE_NAME, MODULE, EXCLUDE_COMMENT)'||
       CHR(013)||' values ('''||TABLE_NAME||''', ''Base Tables'', ''Listing of all Base Tables'');'
  from USER_TABLES
 order by TABLE_NAME;
*/

/*
   The following table change was implemented in update 240_changedb_10.0.5_misc.sql but added here for
   use in scripts prior to 240.
*/
SET SERVEROUTPUT ON

declare
   COLUMN_EXISTS exception;
   pragma exception_init(COLUMN_EXISTS, -1430);

begin
   execute immediate 'alter table TEMP_EXCLUDE_TABLE_LIST' ||
                     '  add (MODULE varchar2(30) default ''NONE'',' ||
                     '       EXCLUDE_COMMENT varchar2(2000))';
   DBMS_OUTPUT.PUT_LINE('Columns added.');
exception
   when COLUMN_EXISTS then
      DBMS_OUTPUT.PUT_LINE('Column already exists.');
end;
/

update TEMP_EXCLUDE_TABLE_LIST set MODULE = 'NONE' where MODULE is null;

declare
   COLUMN_ALREADY_NOTNULL exception;
   pragma exception_init(COLUMN_ALREADY_NOTNULL, -1442);

begin
   execute immediate 'alter table TEMP_EXCLUDE_TABLE_LIST modify MODULE not null';
   DBMS_OUTPUT.PUT_LINE('Column altered.');
exception
   when COLUMN_ALREADY_NOTNULL then
      DBMS_OUTPUT.PUT_LINE('Column already Not Null.');
end;
/

declare NONEXISTENT_PRIMARY_KEY exception;
pragma exception_init(NONEXISTENT_PRIMARY_KEY, -2441);

begin
   execute immediate 'alter table TEMP_EXCLUDE_TABLE_LIST drop primary key drop index';
   DBMS_OUTPUT.PUT_LINE('Primary Key Dropped.');
exception
   when NONEXISTENT_PRIMARY_KEY then
      DBMS_OUTPUT.PUT_LINE('Primary Key does not exist.');
end;
/

declare PRIMARY_KEY_ALREADY_EXISTS exception;
pragma exception_init(PRIMARY_KEY_ALREADY_EXISTS, -2260);

begin
   execute immediate 'alter table TEMP_EXCLUDE_TABLE_LIST ' ||
                     '   add constraint PK_TEMP_EXCLUDE_TABLE_LIST primary key (TABLE_NAME, MODULE)' ||
                     '   using index tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE('Primary Key Created.');
exception
   when PRIMARY_KEY_ALREADY_EXISTS then
      DBMS_OUTPUT.PUT_LINE('Primary Key already exists.');
end;
/

delete from TEMP_EXCLUDE_TABLE_LIST where MODULE = 'Base Tables';

insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ACCOUNT_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ACCOUNT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ACTIVITY_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ADJUSTED_PLANT_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ADJUST_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('AFUDC_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('AFUDC_CALC_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('AFUDC_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('AFUDC_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('AFUDC_DATA_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('AFUDC_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('AFUDC_INPUT_RATIO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('AFUDC_OH_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('AFUDC_RATE_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('AFUDC_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('AGED_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('AMORTIZATION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ANALYSIS_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ANALYSIS_ACCOUNT_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ANALYSIS_ACCOUNT_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ANALYSIS_ACCOUNT_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ANALYSIS_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ANALYSIS_DEPR_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ANALYSIS_GRAPH_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ANALYSIS_RSRV_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ANALYSIS_TRANSACTION_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ANALYSIS_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPORT_CASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPORT_COMPANY_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPORT_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPORT_DATA_LOAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPORT_FACTOR_ITEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPORT_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPORT_STATE_WEIGHT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPROVALS_PENDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPROVAL_AUTH_LEVEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPROVAL_DEFAULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPROVAL_DELEGATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPROVAL_DELEGATION_HIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPROVAL_HIERARCHY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPROVAL_NOTIFICATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPROVAL_ROUTING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPROVAL_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPROVAL_STEPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('APPROVAL_STEPS_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARCCR_ARCHIVE_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARCHIVE_FILES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARCHIVE_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARCHIVE_SYSTEM_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARCHIVE_SYSTEM_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARCHIVE_TABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARCHIVE_TAX_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARCHIVE_TRACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_ACCOUNT_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_AFUDC_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_BASIS_AMOUNTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_COMPRESS_CPR_ACT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_CPR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_CPR_ACT_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_CPR_MEMO_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_DEFERRED_INCOME_TAX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_DEFERRED_INCOME_TAX_TRANSF', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_DEPR_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_DEPR_RES_ALLO_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_DEPR_VINTAGE_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_DFIT_FORECAST_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_FCST_DFIT_FORECAST_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_FCST_TAX_FORECAST_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_FCST_TAX_FORECAST_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_FUNDING_JUSTIFICATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_GL_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_PP_PROCESSES_MESSAGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_PP_PROCESSES_OCCURRENCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_RETIRE_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_TAX_ANNOTATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_TAX_BOOK_RECONCILE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_TAX_BOOK_RECONCILE_TRANSFE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_TAX_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_TAX_DEPRECIATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_TAX_DEPRECIATION_TRANSFER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_TAX_DEPR_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_TAX_FORECAST_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_TAX_FORECAST_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_TAX_RECORD_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_TAX_RENUMBER_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_TAX_TRANSFER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_WORK_ORDER_BLANKETS_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_WORK_ORDER_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_WO_AVAILABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_WO_BK_AVAILABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_WO_BK_CHARGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_WO_BK_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_WO_BK_UNITS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARC_WO_PROBLEMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_CHARGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_DELETE_CHARGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_DEPR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_FCST_LIABILITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_LAYER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_LAYER_DISCOUNTING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_LAYER_STREAM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_LAYER_WORK_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_LIABILITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_LIABILITY_ADJ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_MASS_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_MASS_RU', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_MULTI_EST_REVIEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_RELATED_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_SETTLEMENT_ADJ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_STREAM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_TEMP_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_TEMP_RELATED_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_TRANSITION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_TRANSITION_MASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_WAM_RATE_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ARO_WORK_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ASSET_ACCT_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ASSET_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BASIS_AMOUNTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BILL_MATERIAL_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BKS_SCHEM_SET_BKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BLEND_DEPR_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BOOK_ALLOC_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BOOK_ALLOC_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BOOK_ALLOC_RETIREMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BOOK_ALLOC_RETIRE_PROCESS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BOOK_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BOOK_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_ACTUAL_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_AFUDC_DEFAULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_AMOUNTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_AMOUNTS_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_APPROVAL_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_APPROVAL_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_APPROVAL_LEVELS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_CLASS_CODE_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_ESCALATIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_ESCALATIONS_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_FCST_DEPR_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_LOAD_DEPARTMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_MONTHLY_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_MONTHLY_DATA_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_MONTHLY_DATA_MO_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_MONTHLY_DATA_TRANSPOSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_MONTHLY_DATA_TRANS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_MONTHLY_SPREAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_MONTHLY_SPREAD_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_ONE_BUTTON', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_ORGANIZATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_ORG_CLEAR_DFLT_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_PLANT_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_REIMB_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_REVIEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_REVIEW_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_REVIEW_DEFAULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_REVIEW_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_REVIEW_DETAIL_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_REVIEW_LEVEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_REVIEW_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_REVIEW_USERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_SUBSTITUTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_SUBSTITUTIONS_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_SUBS_DEFAULT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_SUMMARY_CLEAR_DFLT_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_SUMMARY_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_SUMMARY_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_VERSION_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_VERSION_FUND_PROJ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_VERSION_FUND_PROJ_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUDGET_VERSION_MAINT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUD_SUMMARY_BUD_PLANT_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BUSINESS_SEGMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('BV_FP_RESPREAD_AUDIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CAP_GAIN_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CAP_INTEREST_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CAP_INTEREST_WO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CHARGE_ARCH_INFO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CHARGE_COLLECT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CHARGE_ERROR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CHARGE_FORMAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CHARGE_GROUP_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CHARGE_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CHARGE_LOCATION_LEVEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CHARGE_LOCATION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CHARGE_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CHARGE_SUMMARY_CLOSINGS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CHARGE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CHARGE_TYPE_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLASS_CODE_CPR_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLASS_CODE_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLASS_CODE_PENDING_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLASS_CODE_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLASS_CODE_WO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLEARING_WO_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLEARING_WO_CONTROL_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLEARING_WO_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLEARING_WO_RATE_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLOSING_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLOSING_PATTERN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CLOSING_PATTERN_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMBINED_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMBINED_DEPR_RES_ALLO_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMMITMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMMITMENTS_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMMITMENT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPANY_ACCOUNT_CURVES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPANY_APPROVAL_AUTH_LEVEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPANY_BUS_SEGMENT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPANY_CLOSING_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPANY_GL_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPANY_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPANY_MAJOR_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPANY_PROPERTY_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPANY_SETUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPANY_SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPANY_SUB_ACCOUNT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPANY_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPATIBLE_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMPUTED_AGE_DISTRIBUTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMP_UNIT_STOCK_KEEPING_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMP_UNIT_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COMP_UNIT_TEMPLATE_ROWS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CONVERSION_BATCH_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COST_ELEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COST_OF_REMOVAL_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COUNTRY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('COUNTY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CO_TENANCY_AGREEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CO_TENANCY_PARTNERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CO_TENANCY_WO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPI_RETRO_ALLOC_ERROR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPI_RETRO_ASSET_ALLOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_ACTIVITY_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_ACT_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_ACT_BASIS_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_ACT_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_ACT_MONTH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_DEPR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_LDG_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_LDG_BASIS_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_LEDGER_COMMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_LEDGER_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_MEMO_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CPR_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ACCOUNT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOCATION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOCATION_CONTROL_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_CREDIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_CREDIT_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_CREDIT_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_CREDIT_CRITERIA_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_CUSTOM_DW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_CUSTOM_DW_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_GROUP_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_GROUP_BY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_GROUP_BY_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_INTERCO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_INTERCO_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_INTERCO_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_INTERCO_CRITERIA2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_INTERCO_CRITERIA2_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_INTERCO_CRITERIA_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_PROCESS_CONTROL_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_SYSTEM_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_TARGET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_TARGET_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_TARGET_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_TARGET_CRITERIA_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_WHERE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_WHERE_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ALLOC_WHERE_CLAUSE_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_AMOUNT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_APPROVAL_AUTH_LEVEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_APPROVAL_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_APPROVAL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BALANCES_DD_KEYS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BALANCES_YE_CLOSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_ACTIVE_ENTRY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_AFUDC_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_AFUDC_WOTYPE_EXCLUDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_COMPONENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_DATA_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_ELEMENT_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_ELEMENT_GROUP_BY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_FIXED_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_SPREAD_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_STEP1', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_STEP1_CALCULATIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_STEP1_JUSTIFICATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_STEP2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_SUBMITTED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_USERS_DEPT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_VERSION_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_VERSION_SECURITY_ALL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_WHERE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_WHERE_APPLY_TO_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_BUDGET_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_CANCEL_ADDITIONAL_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_CANCEL_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_COMMITMENTS_FIELD_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_COMMITMENTS_SUM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_COMMITMENTS_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_COMMITMENTS_VALIDATIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_COMPANY_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_COST_REPOSITORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_CROSSTAB_SOURCE_DW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_CR_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_CWIP_CHARGE_DRILL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_CWIP_CHARGE_SUM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_CWIP_CHARGE_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DD_SOURCES_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DD_SOURCES_CRITERIA_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DEBIT_CREDIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DERIVATION_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DERIVATION_ROLLUP_PRIORITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DERIVER_ADDITIONAL_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DERIVER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DERIVER_OVERRIDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DERIVER_OVERRIDE2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DERIVER_WO_LINK_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DISTRIB_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DISTRIB_QUERIES_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DISTRIB_USERS_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_DRILLDOWN_KEYS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ELEMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_ELEMENTS_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FERC_ALLOCATION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_BUILD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_DOL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_FILTERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_FORMULAS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_FORMULAS2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_LINKED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_RUN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_RUN2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_RUN_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_SETUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_SETUP_LBL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_FINANCIAL_REPORTS_SPEC_CHAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_GL_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_GL_JOURNAL_CATEGORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_INTERFACES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_INTERFACE_DATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_INTERFACE_MONTH_PERIOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_JE_IMPORT_TEMPLATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_JE_IMPORT_TEMPLATES_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_JOURNALS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_JOURNAL_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_JOURNAL_ATTACHMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_JOURNAL_BATCHES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_MANUAL_COMMITMENT_ELEM_HIDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_MANUAL_JE_ELEMENT_HIDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_MANUAL_JE_TEMPLATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_MANUAL_JE_TEMPLATES_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_MASTER_ELEMENT_FORMAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_MONTH_NUMBER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_MONTH_NUMBER_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_OPEN_MONTH_NUMBER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_POST_TO_GL_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_POST_TO_GL_COLUMNS_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_POST_TO_GL_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_POST_TO_GL_CONTROL_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_QUERY_SUBSELECT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_RATES_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_RECON', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_RECON_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_RECON_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_RECON_SOURCES_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_RECON_WHERE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_RECON_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_RECORD_COUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SAP_POSTING_TO_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SAP_TRUEUP_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SAP_TRUEUP_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SAP_TRUEUP_EXCLUSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SAP_TRUEUP_EXCLUSION_WO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SAVED_QUERIES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SAVED_QUERIES_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SCO_BILLING_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SCO_BILLING_TYPE_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SOURCES_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SPECIAL_PROCESSING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SPECIAL_PROCESSING_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SPECIAL_PROCESSING_DATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SPECIAL_PROCESSING_DATES2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SPECIAL_PROCESSING_DATES_BD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_STRUCTURES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_STRUCTURES_FLATTENED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_STRUCTURES_FLATTENED2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_STRUCTURE_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_STRUCTURE_VALUES2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SUM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SUM_DOLLAR_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SUM_HIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_SYSTEM_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TEMP_CR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TEMP_CR_ALLOCATIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TEMP_CR_COMMITMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TEMP_CR_GL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TEMP_CR_GL_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TEMP_CR_GL_IDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TEMP_CR_GL_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TEMP_CR_JE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TO_CC_DETAIL_TABLES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TO_COMM_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TO_COMM_DETAIL_TABLES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TO_COMM_TABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TO_COMM_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TO_COMM_TRANSLATE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TO_CWIP_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TO_CWIP_TABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TO_CWIP_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_TO_CWIP_TRANSLATE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_VALIDATIONS_INVALID_IDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_VALIDATIONS_INVALID_IDS2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_VALIDATIONS_INVALID_IDS2A', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_VALIDATIONS_INVALID_IDS3', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_VALIDATION_COMBOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_VALIDATION_COMBOS_STAGING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_VALIDATION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_VALIDATION_EXCLUSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_VALIDATION_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_VALIDATION_RULES_PROJECTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CR_WO_CLEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CURRENCY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CURRENCY_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CURRENCY_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CURRENCY_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CURRENCY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CUSTOM_VALIDATIONS_INFO_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CWIP_CHARGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('CWIP_IN_RATE_BASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DATA_ACCOUNT_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEFERRED_INCOME_TAX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEFERRED_INCOME_TAX_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEFERRED_INCOME_TAX_TRANSFER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEFERRED_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEFERRED_TAX_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEFERRED_TAX_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEF_INCOME_TAX_RATES_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPARTMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPRECIATION_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_ACTIVITY_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_ACTIVITY_RECURRING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_GROUP_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_GROUP_JUR_ALLO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_GROUP_UOP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_LEDGER_BLENDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_METHOD_BLENDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_METHOD_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_METHOD_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_METHOD_UOP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_NET_SALVAGE_AMORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_RATES_COMMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_RES_ALLO_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_SUBLEDGER_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_SUBLEDGER_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_SUMMARY2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DEPR_VINTAGE_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DFIT_FORECAST_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DISPOSITION_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DIT_RATE_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DIVISION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DOCUMENT_STAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_ACCT_DATASET_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_ACTIVITY_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_ANALYSIS_DATASET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_COMPANY_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_DATASET_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_DATASET_EXTERNAL_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_DATASET_RSRV_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_DATASET_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_DATA_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_DATA_ACCOUNT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_DATA_ACCOUNT_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_DATA_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_DG_DATASET_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('DS_RSRV_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ELEMENT_TABLE_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ENG_ESTIMATE_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ESTIMATE_CHARGE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ESTIMATE_CURVES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ESTIMATE_CURVE_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ESTIMATE_OVERHEAD_BASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ESTIMATE_OVERHEAD_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('ESTIMATE_UPLOAD_CUSTOMIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('EST_SALVAGE_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('EXPENDITURE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('EXP_TYPE_EST_CHG_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_BUDGET_LOAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_BUDGET_LOAD_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_COMBINED_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_CPR_DEPR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEF_TAX_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPRECIATION_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_GROUP_UOP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_GROUP_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_GROUP_XLAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_LEDGER_ANNUAL_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_METHOD_BLENDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_METHOD_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_METHOD_UOP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_RES_ALLO_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_SUBLEDGER_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DEPR_VINTAGE_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_DVS_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_GL_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_METHOD_RETIRE_PCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_SUBLEDGER_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_TAX_DEPR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_TAX_DEPR_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_TAX_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_UNIT_OF_PRODUCTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FCST_VERSION_SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FERC_ACTIVITY_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FERC_PLANT_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FERC_REPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FERC_SYS_OF_ACCTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FIT_STATISTICS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FIT_STATS_ALL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FORECASTED_RETIREMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FUNC_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FUNC_CLASS_LOC_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FUNC_CLASS_PROP_GRP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('FUNDING_JUSTIFICATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('GAIN_LOSS_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('GENERATION_ARRANGEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('GENERATION_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('GL_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('GL_ACCT_BUS_SEGMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('GL_JE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('GL_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('GL_TRANS_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('HANDY_WHITMAN_INDEX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('HANDY_WHITMAN_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('HANDY_WHITMAN_REGION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('INSTRUCTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JOB_TASK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JOB_TASK_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JOB_TASK_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JOB_TASK_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JOB_TASK_TEMPLATE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JOB_TASK_TEMPLATE_WO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JURISDICTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JUR_AFC_SHADOW_MAPPING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JUR_AFC_SHADOW_WO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JUR_ALLO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JUR_ALLO_BOOK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JUR_ALLO_BOOK_DESCRIPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JUR_ALLO_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JUR_ALLO_VERSION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('JUR_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LIFE_ANALYSIS_PARAMETERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LOCATION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ACCOUNTING_DEFAULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ACTIVITY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET_ACTIVITY_LT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET_COMPONENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET_CPR_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET_FLEX_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET_FLEX_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET_FLEX_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET_LOCAL_TAX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET_LOC_LOCAL_TAX_DISTRI', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET_RETIREMENT_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ASSET_TRANSFER_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_CALC_APPROVAL_RULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_CANCELABLE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_COLUMN_USAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_COMPONENT_CHARGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_COMPONENT_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_COST_ELEMENT_VALID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_DISTRIBUTION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_DIST_DEF', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_DIST_DEF_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_DIST_DEF_PARMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_DIST_DEF_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_DIST_DEF_VALIDATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_DIST_DEPARTMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_DIST_GL_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_DIST_LINE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_EOL_DEFAULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_EXTENDED_RENTAL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_GL_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_GRANDFATHER_LEVEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ILR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ILR_PAYMENT_TERM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ILR_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_ILR_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_JE_ALLOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_JE_ALLOCATION_CREDITS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_JE_ASSET_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_JE_DIST_DEF', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_JE_EXCHANGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_JE_EXCHANGE_STLT_RECAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_JE_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_JE_ROUNDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_JE_ROUNDING2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_JE_STAGING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_AIR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_CALC_BALANCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_CALC_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_CALC_ROLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_CALC_ROLE_USER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_CALC_STAGING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_CALC_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_CALC_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_CALC_USER_APPROVER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_DISPOSITION_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_EXPENSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_PAYMENT_TERM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_RULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LEASE_TYPE_TOLERANCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LESSOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LESSOR_STATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LOCAL_TAX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LOCAL_TAX_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LOCAL_TAX_DISTRICT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LOCAL_TAX_ELIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LOCAL_TAX_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LOCAL_TAX_SNAPSHOT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_LOCAL_TAX_SNAPSHOT_STATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_MASS_CHANGE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_PAYMENT_FREQ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_PAYMENT_TERM_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_POST_CPR_FIND', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_PURCHASE_OPTION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_RENEWAL_OPTION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_RETIREMENT_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_STATE_GRANDFATHER_DATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_STATE_LOCAL_TAX_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_SUMMARY_LOCAL_TAX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_TEMP_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_TEMP_COMPONENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_TEMP_ILR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_TEMP_LEASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('LS_VALID_COMPANY_PO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MAJOR_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MATLREC_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MATLREC_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MATLREC_CRITERIA_LINES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MATLREC_DATADEF', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MATLREC_DATADEF_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MATLREC_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MATLREC_SOURCES_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MATLREC_SOURCES_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MATLREC_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MATLREC_TRANS_STG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MINOR_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MORTALITY_CURVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MORTALITY_CURVE_POINTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MORTALITY_CURVE_RET_POINTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MORTALITY_MEMORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MORTALITY_MEMORY_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('MUNICIPALITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('NARUC_PLANT_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('NARUC_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('NORMALIZATION_PCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('NORMALIZATION_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('NORM_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('OH_ALLOC_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('OH_ALLOC_BASIS_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('OVERHEAD_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('OVERHEAD_BASIS_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PBCATCOL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PBCATEDT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PBCATFMT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PBCATTBL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PBCATVLD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_BASIS_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_BASIS_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_DEPR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_RELATED_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_SUBLEDGER_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_SUBLEDGER_BASIS_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_SUBLEDGER_ENTRY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_SUBLEDGER_ENTRY_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_TRANSACTION_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_TRANSACTION_MEMO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_TRANSACTION_MEMO_ARCHIVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PEND_TRANSACTION_POST_TEST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PLAN_TABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('POSTING_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('POWERPLANT_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('POWERPLANT_DDDW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('POWERPLANT_TABLES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('POWERPLANT_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PPVERIFY_LOCK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_ADSI_ATTRIBUTES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_ADSI_DIRECTORY_PATH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_ADSI_MAP_ATTRIBUTES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_ANY_QUERY_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_ANY_QUERY_CRITERIA_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_ADMIN_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_ARO_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_BUDGET_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_CPR_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_CR_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_DEPRFCST_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_DEPRSTUDY_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_DEPR_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_LEASE_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_POWERTAX_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_PROJECT_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_PROPTAX_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_PROVISION_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_SYSTEM_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDITS_TABLES_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDIT_DDL_TABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_AUDIT_LOGON_TABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_JOBS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_PROGRAMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_PROGRAM_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_QUEUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_REPORTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_REPORTS_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_REPORTS_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_REPORT_ARGS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_REPORT_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_REPORT_FORMAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_REPORT_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_REPORT_PRINTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_RPTS_DISTR_GRPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_RPTS_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_BATCH_RPTS_VIEWED', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CALENDAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CHARGE_SPLIT_RUNNING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CLASS_CODE_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_COMPANY_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_COMPANY_SECURITY_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CONVERSION_WINDOWS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CONV_BATCHES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CONV_BATCHES_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CONV_FERC_XLAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CONV_MAPPING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CONV_REF_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CONV_STAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CONV_TEMPLATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CONV_TEMPLATES_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_CONV_TEMP_COST_QUANTITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_DATABASE_OBJECT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_DATAWINDOW_EXCEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_DATAWINDOW_HINTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_DROP_CONSTRAINTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_EDIT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_ERROR_MESSAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_FLEX_NAMES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_FLEX_NAMES_MLS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_HTMLHELP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_INTERFACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_INTERFACE_DATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_LANGUAGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_LDAP_ATTRIBUTES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_LDAP_MAP_ATTRIBUTES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_LDAP_PROFILES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_LDAP_USER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_LDAP_USER_SYNC_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_LOCALE_ID', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MAIL_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MANUAL_INTERFACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MISC_SEARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MODULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_ALERT_DASHBOARD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_ALERT_DASH_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_REPORT_TYPE_TASK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_TASK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_TASK_STEP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_TAX_REPORT_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_TIME', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_TIME_REPORT_TIME', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_USER_LAYOUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_USER_QUERY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_USER_REPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_USER_REPORT_PARM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_USER_TASK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_USER_TODO_ATTACHMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_USER_TODO_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_MYPP_USER_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_OBJECTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_POST_LOCK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_PROCESSES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_PROCESSES_MESSAGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_PROCESSES_OCCURRENCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_QUERY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_QUERY_DW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_QUERY_DW_CC_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_QUERY_DW_COLS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_QUERY_DW_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_RECORD_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORTS_ARGS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORTS_COMPANY_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORTS_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORTS_ENVIR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORTS_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORTS_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORTS_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORTS_SUBSYSTEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORTS_TIME_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORT_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORT_FILTER_COLUMNS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_REPORT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_LOGIN_PROFILE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_LOG_RECORD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_OBJECTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_OBJECT_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_OBJECT_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_PASSWD_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_RULES_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_USERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_USERS_ALIAS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_USERS_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SECURITY_USER_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SESSION_PARAMETERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_STATISTICS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SYSTEM_CONTROL_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_SYSTEM_ERRORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TABLE_AUDITS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TABLE_AUDITS_OBJ_ACTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TABLE_AUDITS_PK_LOOKUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TABLE_AUDITS_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TABLE_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TABLE_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TABLE_MONTHS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TABLE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TABLE_YEARS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TASK_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TEMP_CWIP_CHARGE_SPLIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TEST_PERFORMANCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TOOLTIP_HELP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TREE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TREEVIEW_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TREE_CATEGORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_TREE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_UPDATE_FLEX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_USER_DW_MICROHELP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_USER_PROFILE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_USER_PROFILE_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_VERIFY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_VERIFY_BATCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_VERIFY_CATEGORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_VERIFY_CATEGORY_DESCRIPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_VERIFY_COMMENTS_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_VERIFY_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_VERIFY_USER_COMMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_VERIFY_USER_KEY_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_VERIFY_USER_STD_COMMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_WEB_PRINTERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_WINDOW_DISABLE_RESIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_WINDOW_SIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_WORKSHEETS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_WORK_ORDER_TYPE_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_WO_EST_CUSTOMIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_WO_EST_CUSTOMIZE_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PP_WO_TASK_CUSTOMIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROCESSING_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROPERTY_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROPERTY_TAX_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROPERTY_TAX_ADJUST_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROPERTY_TAX_AUTHORITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROPERTY_TAX_AUTHORITY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROPERTY_TAX_LEDGER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROPERTY_TAX_LEDGER_NEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROPERTY_TAX_TYPE_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROPERTY_TAX_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROPERTY_TAX_YEAR_LOCK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROPERTY_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROPERTY_UNIT_DEFAULT_LIFE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_GROUP_PROP_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_ALLOCATIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_ALLO_DEFINITION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_DISTRICT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_DISTRI_PCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_NET_TAX_RESERVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_NET_TAX_RESERVE_REP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_PREALLO_PULL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_TAX_RESERVE_PERCENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_TYPE_ASSIGN_TREE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_TYPE_ASSIGN_TREE_CWIP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_TYPE_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_TAX_TYPE_PROP_TAX_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PROP_UNIT_TYPE_SIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACCRUAL_ALLOWED_AMT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACCRUAL_ASSIGN_TREE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACCRUAL_ASSIGN_TREE_LEVELS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACCRUAL_CHARGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACCRUAL_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACCRUAL_MERGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACCRUAL_MERGE_SAVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACCRUAL_PENDING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACCRUAL_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACCRUAL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACCRUAL_YEAR_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACTUALS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ACTUALS_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ADJUSTMENT_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ADJUSTMENT_TRANSFER_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ALLOCATE_ASSESSMENT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ALLOCATION_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ALLOCATION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_APPRAISER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ASSESSED_VALUE_ALLOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ASSESSED_VALUE_COUNTY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ASSESSED_VALUE_TAX_DISTRICT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ASSESSMENT_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ASSESSMENT_GROUP_CASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ASSESSMENT_GROUP_TYPES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ASSESSMENT_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ASSESSOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ASSET_LOCATION_SNAPSHOT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_AUTHORITY_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_BUSINESS_SEGMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_BUSINESS_SEGMENT_PCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_CASE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_CASE_CALC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_CASE_CALC_AUTHORITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_CASE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_CASE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_CLASS_CODE_VALUE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_COLUMN_HELP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_COUNTY_EQUALIZATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_CPR_PULL_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_DATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_DATE_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_DATE_GENERATOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_DATE_LINK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_DATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_DATE_TYPE_FILTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_DATE_TYPE_PERSON', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_DATE_WEEKEND_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_DISTRICT_EQUALIZATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_DISTRICT_SNAPSHOT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_EQUALIZATION_FACTOR_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ESCALATED_VALUE_INDEX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_ESCALATED_VALUE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_INCREMENTAL_BASE_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_INCREMENTAL_INITIALIZATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_INCREMENTAL_STATISTICS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_INTERFACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_INTERFACE_TAX_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_LEVY_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_LEVY_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_LOCATION_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_LOCATION_SNAPSHOT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_MARKET_VALUE_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_APPEAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_APPEAL_EVENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_APPEAL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_APPRAISAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_ASSESSMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_ASSESSMENT_COLUMN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_FLEX_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_FLEX_FIELD_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_FLEX_FIELD_USAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_FLEX_FIELD_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_GEOGRAPHY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_GEOGRAPHY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_GEOGRAPHY_TYPE_STATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_RESPONSIBILITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_RESPONSIBLE_ENTITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PARCEL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PAYMENT_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PERIOD_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PREALLO_ADJUSTMENT_LEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_PREALLO_ADJ_TRANS_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_QUANTITY_CONVERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_RATE_DEFINITION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_REPORT_COMPANY_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_REPORT_COUNTY_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_REPORT_FIELD_LABELS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_REPORT_LOCATION_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_REPORT_PT_COMPANY_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_REPORT_STATE_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_REPORT_TAX_DISTRICT_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_REPORT_TYPE_CODE_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_RESERVE_FACTORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_RESERVE_FACTOR_COUNTY_PCTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_RESERVE_FACTOR_DIST_PCTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_RESERVE_FACTOR_PERCENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_RESERVE_FACTOR_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_RESERVE_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_SCHEDULE_INSTALLMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_SCHEDULE_PERIOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_SNAPSHOT_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_GROUP_PAYMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_GROUP_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_INSTALLMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_LINE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_LINE_PAYMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_PAYEE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_PAYMENT_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_PERIOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_STATEMENT_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_TAX_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_VERIFIED_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_STATEMENT_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_TAXABLE_VALUE_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_TYPE_CWIP_PSEUDO_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_TYPE_CWIP_PSEUDO_LEVELS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_TYPE_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_TYPE_ROLLUP_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_TYPE_ROLLUP_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_UNIT_COST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_UNIT_COST_AMOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('PT_VINTAGE_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RATE_ANALYSIS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RATE_ANALYSIS_RESERVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REASON_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RECOVERY_PERIOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REGULATORY_ENTRIES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REGULATORY_ENTRIES_ELIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REGULATORY_ENTRY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REGULATORY_TRANSACTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REG_FUNCTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMBURSABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMBURSABLE_CALCULATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMBURSABLE_PAYMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_AMT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILLING_FREQ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILLING_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILL_CAP_ADJUSTMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILL_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILL_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILL_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILL_INTERFACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILL_LINE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILL_MESSAGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILL_SCHEDULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILL_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILL_SUPPORT_CR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_BILL_SUPPORT_CWIP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CREDIT_MEMO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CR_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CR_GROUP_BY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CR_OH_ELIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CR_OH_EXCLUSIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CR_OH_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CR_OVERHEAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CR_SOURCES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CR_SOURCES_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CR_TARGET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CR_TARGET_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CR_WHERE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CR_WHERE_CLAUSE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CUSTOMER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CUSTOMER_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CUSTOMER_SPLIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_CUST_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_DISPLAY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_DISPLAY_ELEMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_ELIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_ELIG_CHARGE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_ELIG_EST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_ESTIMATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_EST_BILL_HIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_EST_OH_ALLOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_EST_OH_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_EST_OH_ELIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_EST_OH_EXCLUSIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_EST_OH_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_EST_OVERHEAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_GL_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_JE_TIMING', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_JE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_METHOD_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_OH_ALLOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_OH_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_OH_ELIG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_OH_EXCLUSIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_OH_RATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_OH_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_OVERHEAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_PAYMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_REFUND', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_REPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_REPORT_CR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_REPORT_CWIP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_STATUS_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_SUPER_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_SYSTEM_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_TAX_GROSS_UP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REIMB_WORK_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RELATED_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RELATED_ASSET_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REPORT_TIME', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REQ_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RESERVE_RATIOS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RETIREMENT_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RETIREMENT_UNIT_ASSOCIATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RETIREMENT_UNIT_WORK_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RETIRE_BAL_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RETIRE_DEPR_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RETIRE_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RETIRE_RESERVE_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RETIRE_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RETIRE_UNIT_COMP_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RETIRE_UNIT_SKU', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RETIRE_UNIT_STD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RETIRE_UNIT_TYP_SIZ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RET_UNIT_MORT_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('REVISION_SELECTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RWIP_ALLO_ERRORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RWIP_ALLO_EST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RWIP_ALLO_GROUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('RWIP_ALLO_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SALVAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SALVAGE_ANALYSIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SALVAGE_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SELECT_TABS_GRIDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SET_OF_BOOKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SKU_SUBSTITUTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SPREAD_FACTOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SPR_ANALYSIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('STANDARD_JOURNAL_ENTRIES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('STATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('STATUS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('STCK_KEEP_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('STORES_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUBLEDGER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUBLEDGER_DEPR_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUBLEDGER_ENTRY_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUBLEDGER_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUBSTITUTION_REVIEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUBSTITUTION_REVIEW_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUBSTITUTION_REVIEW_DETAILS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUBST_REVIEW_DETAILS_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUB_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUB_ACCOUNT_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUMMARY_4562', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUMMARY_TAX_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUMMARY_TRANSACTION_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('SUSPENDED_CHARGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ACCOUNT_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ACCT_ROLLUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ALLOC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ALLOC_AMOUNTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ALLOC_PERCENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_APPORTIONMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_BATCH_LOG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_BEG_BAL_LOAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_BS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_BS_BOOK_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_BS_LINE_ITEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_BS_REPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_BS_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_BS_SCHEMA_ASSIGN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_COMPANY_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_COMPLIANCE_EXPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CONTROL_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CO_CONSOL_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_DRILL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_DRILL_EXPLODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_JE_FIELD_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_JE_UPDATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_PAY_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_PAY_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_PAY_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_PAY_UPDATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_PULL_CATEGORIES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_PULL_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_PULL_CRITERIA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_PULL_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CR_PULL_UPDATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CUSTOM_TAB', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_CUSTOM_TAB_FIELDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_DAMPENING_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_DAMPENING_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_DAMPENING_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_DEF_TAX', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_DEF_TAX_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_DEF_TAX_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_DIFF_IND', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_DIT_PROCESS_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_DIT_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_DIT_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_DIT_SCHEMA_CHECKS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_EFF_RATE_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_EFF_RATE_TMP2', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ENTITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ENTITY_DEDUCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ENTITY_DEDUCT_EXC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ENTITY_INCLUDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ENTITY_INCLUDE_ACT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ENTITY_JURIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ENTITY_REP_INCLUDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_FAS109', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_FAS109_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_FAS109_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_GL_ACCOUNT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_GL_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_GL_DATA_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_GL_DATA_CON_DTLS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_GL_ERRORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_GL_MONTH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_GL_MONTH_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_GL_PAY_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_GL_PAY_CON_DTLS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_IMPORT_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JE_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JE_EXPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JE_FIELD_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JE_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JE_RESULTS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JE_RESULTS_CON_HIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JE_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JE_UPDATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JUR_ALLO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JUR_ALLO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JUR_EXCEPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_JUR_TA_NORM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_MONTHLY_SPREAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_M_ITEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_M_ITEM_DRILL_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_M_ITEM_ETR_CALCS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_M_ITEM_ROLLUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_M_ITEM_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_M_MASTER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_M_RATE_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_M_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_M_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_NORM_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_OPER_IND', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_PAY_FIELD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_POWERTAX_ERRORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_POWERTAX_MAP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_POWERTAX_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_POWERTAX_RETRIEVE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_PROCESSING_ERRORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_PROCESS_INFO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_PROCESS_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_PT_IND', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_PT_UNUSED_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_PT_UNUSED_TC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REG_IND', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REPORTS_CUSTOMIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REPORT_OBJECT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REPORT_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REPORT_OPTION_DTL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REPORT_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_CRITERIA_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_CUSTOM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_CUSTOM_MOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_CUSTOM_MOD_DTL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_CUSTOM_OPTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_CUSTOM_OPT_DTL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_DEFERRED_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_JE_TYPE_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_NS2FAS109_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_ROLLUP_DTL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_ROLLUP_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_ROLL_TMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_SPECIAL_NOTE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_REP_TREE_TEMP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_RESULTS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_RESULTS_ETR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ROLLUP_ACCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ROLLUP_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_ROLLUP_DTL_ACCT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_SCHM_M3', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_SUBLEDGER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_SUBLEDGER_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_SUBLEDGER_EXT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_SUBLEDGER_EXT_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_SUBLEDGER_EXT_ROLL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_SUBLEDGER_FREQ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_SUBLEDGER_PAY_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_SUBLEDGER_SUB_CAT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_SUBLEDGER_TAX_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_TAX_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_TBS_ACCOUNT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_TBS_TREATMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_TRUEUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_VERSION_GL_MONTH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACCRUAL_VERSION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ACTIVITY_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ANNOTATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ARCHIVE_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_AUTH_PROP_TAX_DIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_BOOK', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_BOOK_RECONCILE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_BOOK_RECONCILE_TRANSFER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_BOOK_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_BOOK_TRANSACTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_BOOK_TRANSFERS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_BOOK_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_BOOK_TRANS_ADDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_CLASS_ROLLUPS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_CWIP_CLASS_RECONCILE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_CWIP_COMPANY_RECORD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_CWIP_COMPANY_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_DEPRECIATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_DEPRECIATION_TRANSFER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_DEPR_ADJUST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_DEPR_SCHEMA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_DEPR_SCHEMA_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_FCST_ARCHIVE_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_FCST_SUM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_FORECAST_INPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_FORECAST_OUTPUT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_FORECAST_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_HALF_YEAR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_INCLUDE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_INCLUDE_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_JOB_PARAMS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_LAW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_LIMIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_LIMITATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_LOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_NET_OR_GROSS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_PACKAGE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_RATES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_RATES_DELV', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_RATE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_RATE_CONTROL_DELV', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_RECONCILE_ITEM', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_RECORD_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_RECORD_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_RETIRE_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_RET_AUDIT_TRAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ROLLUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_ROLLUP_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_SERVICE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_SUMMARY_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_TEST_CPR_ACTIVITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_TEST_PEND_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_TEST_PEND_TRANSACTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_TEST_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_TRANSFER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_TYPE_OF_PROPERTY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_UTILITY_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_VINTAGE_CONVENTION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_VINTAGE_TRANSLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TAX_YEAR_VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMPLATE_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMPLATE_DEPR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_ACCRUAL_WORK_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_ARCHIVE_WORK_ORDER_NO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_ARO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_ASSET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_BILL_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_BUDGET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_CHARGE_SUMMARY_CLOSINGS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_COMPRESS_CPR_ACT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_CONS_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_CONS_LIST_NEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_EXCLUDE_TABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_IND_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_IND_LIST_NEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_LDAP_ATTRIB_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_LDAP_SEARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_PROCEDURE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_SECURITY_RULE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_TABLE_LIST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_TABLE_LIST_NEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_VIEW_LIST_NEW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_WOCG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TEMP_WORK_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TOWN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TRANSACTION_INPUT_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TRANS_LINE_NUMBER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TRANS_LINE_STATS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('TYPE_SIZE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNADJUSTED_PLANT_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNITIZATION_TARGET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNITIZATION_TOLERANCE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNITIZATION_TOLERANCE_CT_EXCL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNITIZATION_TOLERANCE_ECT_EXCL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNITIZED_WORK_ORDER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNITIZED_WORK_ORDER_MEMO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNITIZED_WORK_ORDER_SL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNITIZED_WORK_ORDER_SL_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNIT_ALLOCATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNIT_ALLOC_BASIS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNIT_ALLOC_METHOD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNIT_ALLOC_METH_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNIT_ITEM_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNIT_OF_MEASURE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNIT_OF_PRODUCTION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UNIT_TARGET_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UPDATE_WITH_ACTUALS_EXCLUSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('USER_STAT_TABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('USER_STAT_TABLE_IO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('USE_INDICATOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UTILITY_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UTILITY_ACCOUNT_DEPRECIATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UTILITY_ACCOUNT_NARUC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('UTIL_ACCT_PROP_UNIT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('VALUATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('VERSION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('VINTAGE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('VINTAGE_SURVIVORS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_ACCOUNT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_APPROVAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_APPROVAL_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_CHARGE_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_DEPARTMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_DOCUMENT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_FUNDING_PROJ_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_GROUP_BUDGET_ORG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_INITIATOR', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_MASS_UPDATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_TYPE_BUDGET_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_TYPE_EST_IMPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_ORDER_VALIDATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_TYPE_ATTRIBUTE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WORK_TYPE_ATTR_VALUE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ACCRUALS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ACCRUALS_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ACCRUAL_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ACCRUAL_TYPE_EXP_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ACCRUED_GAIN_LOSS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_APPROVAL_GROUP', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_APPROVAL_MULTIPLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_APPROVAL_MULTIPLE_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_APPROVAL_MULTIPLE_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_APPROVAL_MULT_CATEGORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_APPROVAL_MULT_CATEGORY_USER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ARC_ALERT_HISTORY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ARC_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ARC_CLASS_CL_OPT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ARC_CLASS_WOT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ARC_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ARC_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ARC_RULE_CLASS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ARC_RUN_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ARC_RUN_MODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ARC_TEMP_RUN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_AUTO101_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_AUTO106_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_BILL_MATERIAL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_CLEAR_OVER', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_CLEAR_OVER_BDG', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_DASHBOARD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_DASHBOARD_DATA', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_DASHBOARD_DATAWINDOW', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_DASHBOARD_DEFAULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_DASHBOARD_SECURITY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_DASHBOARD_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_DOCUMENTATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_DOC_COMMENTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_DOC_JUSTIFICATION', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_DOC_JUSTIFICATION_RULES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_DOC_JUSTIFICATION_VALUES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_DOC_JUSTIFICATION_WO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ENG_ESTIMATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ENG_ESTIMATE_STATUS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ENG_EST_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ESTIMATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ESTIMATED_RETIRE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ESTIMATE_CLASS_CODE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_ESTIMATE_IMPORT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_DATE_CHANGES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_FORECAST_OPTIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_MONTHLY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_MONTHLY_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_MONTHLY_CURVES', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_MONTHLY_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_MONTHLY_SPREAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_MONTHLY_SPREAD_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_MONTHLY_SUBS_DET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_MONTHLY_UPLOAD', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_MONTHLY_UPLOAD_ARCH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_TEMPLATE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_TEMPLATE_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_TEMPLATE_WO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_TEMP_WEM_FROM_WE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_TRANS_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_EST_WO_ENG_EST', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_GL_ACCOUNT_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_GL_ACCOUNT_SUMMARY_MO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_GL_ACCOUNT_SUMMARY_PRESET', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_GRP_WO_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_IMAGE_INTERFACE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_INTERFACE_MONTHLY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_INTERFACE_MONTHLY_ARC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_INTERFACE_MONTHLY_IDS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_INTERFACE_REVISIONS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_MATERIAL_REQ', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_METRIC', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_METRIC_COMPANY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_METRIC_DETAIL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_METRIC_RESULTS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_METRIC_SAVED_GRAPH', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_OVERHEAD_DW_NAME', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_OVERHEAD_JUR_ALLO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_OVERHEAD_TARGETS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_OVERHEAD_TARGET_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_PROCESS_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_PSEUDO_UNITIZE_CC_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_PSEUDO_UNITIZE_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_PSEUDO_UNITIZE_SUMMARY_MO', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_REIMBURSABLE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_SUMMARY', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_TYPE_CLASS_CODE_DEFAULT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_TYPE_CLEAR_DFLT', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_UNIT_ITEM_PEND_TRANS', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_VALIDATION_CONTROL', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_VALIDATION_RUN', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('WO_VALIDATION_TYPE', 'Base Tables', 'Listing of all Base Tables');
insert into TEMP_EXCLUDE_TABLE_LIST
   (TABLE_NAME, MODULE, EXCLUDE_COMMENT)
values
   ('YES_NO', 'Base Tables', 'Listing of all Base Tables');

commit;