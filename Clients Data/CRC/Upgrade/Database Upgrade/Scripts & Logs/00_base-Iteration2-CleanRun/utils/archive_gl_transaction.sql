/*
||============================================================================
|| Application: PowerPlant
|| File Name:   archive_gl_transaction.sql
|| Description: Archive all records for GL_TRANSACTION table that have a month 
||              number less then sysdate - 3 months and a GL_STATUS_ID = 3
||============================================================================
|| Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 10.2.1.6.0 11/03/2010 Lee Quinn      Created
||============================================================================
*/

declare
   PPCMSG varchar2(10) := 'PPC' || '-MSG> ';
   PPCERR varchar2(10) := 'PPC' || '-ERR> ';
   PPCSQL varchar2(10) := 'PPC' || '-SQL> ';
   PPCORA varchar2(10) := 'PPC' || '-ORA' || '-> ';

   LV_COLUMN_STRING varchar2(2000);
   LV_SQL           varchar2(2000);
   LN_INSERT_COUNT  number;
   LN_DELETE_COUNT  number;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   select STRING_AGG(COLUMN_NAME)
     into LV_COLUMN_STRING
     from ALL_TAB_COLUMNS
    where TABLE_NAME = 'GL_TRANSACTION'
      and OWNER = 'PWRPLANT'
      and COLUMN_NAME not in ('TIME_STAMP', 'USER_ID')
    order by COLUMN_ID;

   LV_SQL := 'insert into ARC_GL_TRANSACTION (' || LV_COLUMN_STRING || ') select ' || LV_COLUMN_STRING ||
             ' from GL_TRANSACTION where GL_STATUS_ID = 3 and month <= ADD_MONTHS(sysdate, -3)';
   DBMS_OUTPUT.PUT_LINE(PPCSQL || LV_SQL);
   execute immediate LV_SQL;
   LN_INSERT_COUNT := sql%rowcount;
   DBMS_OUTPUT.PUT_LINE(LN_INSERT_COUNT || ' Records inserted into ARC_GL_TRANSACTION.');

   LV_SQL := 'delete from GL_TRANSACTION where GL_TRANS_ID in (select GL_TRANS_ID from GL_TRANSACTION where GL_STATUS_ID = 3' ||
             '    and month <= ADD_MONTHS(sysdate, -3))';
   DBMS_OUTPUT.PUT_LINE(PPCSQL || LV_SQL);
   execute immediate LV_SQL;
   LN_DELETE_COUNT := sql%rowcount;
   DBMS_OUTPUT.PUT_LINE(LN_DELETE_COUNT || ' Records deleted from GL_TRANSACTION.');

   if LN_INSERT_COUNT <> LN_DELETE_COUNT then
      DBMS_OUTPUT.PUT_LINE(PPCORA || 'Archive of GL_TRANSACTION failed because Insert and Delete count did not match.');
      rollback;
      DBMS_OUTPUT.PUT_LINE('Transaction rolled backed.');
      return;
   end if;

   commit;
   DBMS_OUTPUT.PUT_LINE('Archive of GL_TRANSACTION table completed successfully for all records that had a month number less then ' || ADD_MONTHS(sysdate, -3) ||
                        ' and a GL_STATUS_ID = 3');

exception
   when others then
      DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
      rollback;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || 'rollback;');
      raise;
end;
/

/*
Log the run of the script
*/
insert into PP_UPDATE_FLEX
            (COL_ID,
             TYPE_NAME,
             FLEXVCHAR1)
      select nvl(max(COL_ID), 0) + 1,
             'UPDATE_SCRIPTS',
             'archive_gl_transaction.sql'
        from PP_UPDATE_FLEX;
commit;