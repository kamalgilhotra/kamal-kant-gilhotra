SET ECHO OFF
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: grant_pwrproxy.sql
|| Description: Grants the Proxy user PWRPROXY to PowerPlan users.
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.3.0 05/14/2014 Shane "C" Ward
||============================================================================
*/

/*
||============================================================================
|| Create Poxy user if it doesn't exist.
|| Password can be changed to whatever you want.  The Proxy User name and
|| Password would be entered in PPCSET to run interfaces.
||============================================================================
*/
declare
   USER_EXISTS exception;
   pragma exception_init(USER_EXISTS, -01920);

begin
   execute immediate 'create user PWRPROXY identified by Tr9uI22k';
exception
   when USER_EXISTS then
      DBMS_OUTPUT.PUT_LINE('The PWRPROXY user already exists.');
end;
/

grant create session to PWRPROXY;


declare
   USERS PKG_PP_COMMON.VARCHAR_TABTYPE;
   PROXY varchar2(18);

begin

   --Change Proxy user here if needing to use a different Proxy User
   PROXY := 'PWRPROXY';

   select P.USERS bulk collect
     into USERS
     from PP_SECURITY_USERS P, DBA_USERS D
    where D.DEFAULT_TABLESPACE not in ('SYSAUX', 'SYSTEM')
      and D.ACCOUNT_STATUS = 'OPEN'
      and UPPER(D.USERNAME) = UPPER(P.USERS);

   PKG_PP_COMMON.P_SET_SSP_PROXY(PROXY, USERS);

end;
/


