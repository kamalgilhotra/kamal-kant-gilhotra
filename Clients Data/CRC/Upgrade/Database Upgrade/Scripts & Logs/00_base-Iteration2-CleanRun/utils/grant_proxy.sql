SET ECHO OFF
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: grant_proxy.sql
|| Description: Grants the Proxy user PPCSSO to PowerPlant users.
||============================================================================
|| Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version   Date       Revised By     Reason for Change
|| --------- ---------- -------------- -----------------------------------------
|| V10.2.1.3 05/06/2010 Lee Quinn      Created
||============================================================================
*/

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.grant_proxy.log

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   V_RCOUNT  number := 0;
   V_RERRORS number := 0;
begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
   -- Use if you want to match DB Users with Users defined in PowerPlant.
   for CSR_CREATE_SQL in (select 'alter user ' || USERNAME || ' grant connect through PPCSSO' CREATE_SQL
                            from DBA_USERS U, PP_SECURITY_USERS SU
                           where LOWER(U.USERNAME) = LOWER(SU.USERS)
                           order by USERNAME)

   -- Use if you want to only match DB Users that are assigned PowerPlant roles.
/*
   for CSR_CREATE_SQL in (select 'alter user ' || USERNAME || ' grant connect through PPCSSO' CREATE_SQL
                            from DBA_ROLE_PRIVS DRP, DBA_USERS DU
                           where DRP.GRANTEE = DU.USERNAME
                             and GRANTED_ROLE in
                                 ('PWRPLANT_ROLE_DEV', 'PWRPLANT_ROLE_ADMIN', 'PWRPLANT_ROLE_USER',
                                  'PWRPLANT_ROLE_RDONLY')
                           order by USERNAME)
*/
   loop
      begin
         DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
         V_RCOUNT := V_RCOUNT + 1;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR ||
                                 SUBSTR('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm, 1, 255));
      end;
   end loop;

   DBMS_OUTPUT.PUT_LINE('');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' Users altered.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RERRORS || ' errors in Grant Proxy.');
end;
/

/*
|| END Script DB Users
*/
SPOOL OFF
SET ECHO ON