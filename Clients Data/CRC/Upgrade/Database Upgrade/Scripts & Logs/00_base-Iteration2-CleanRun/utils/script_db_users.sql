SET ECHO OFF
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: script_db_users.sql
|| Description: Scripts out Oracle Users from a database that exist in the
||              PowerPlant application.
||              Uncomment Line10 if you are using a Proxy User
||
||              Example of a proxy user: create user PPCSSO identified by password;
||                                       grant connect to PPCSSO;
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| V10.1   11/13/2008 Lee Quinn      Created
||============================================================================
*/

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.script_db_users.log

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   V_RCOUNT number;

begin
   DBMS_OUTPUT.ENABLE(buffer_size => NULL);
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select USERNAME,
                                 'create user ' || USERNAME || ' identified by password' LINE1,
                                 '   default tablespace ' || DEFAULT_TABLESPACE LINE2,
                                 '   temporary tablespace ' || TEMPORARY_TABLESPACE LINE3,
                                 '   profile ' || PROFILE LINE4,
                                 'quota unlimited on ' || DEFAULT_TABLESPACE || ';' LINE5,
                                 'grant connect to ' || USERNAME || ';' LINE6,
                                 'grant PWRPLANT_ROLE_DEV to ' || USERNAME || ';' LINE7,
                                 'grant PWRPLANT_ROLE_USER to ' || USERNAME || ';' LINE8,
                                 'alter user ' || USERNAME ||
                                 ' default role CONNECT, PWRPLANT_ROLE_USER;' LINE9,
                                 'alter user ' || USERNAME || ' grant connect through PPCSSO;' LINE10
                            from DBA_USERS U, PP_SECURITY_USERS SU
                           where LOWER(U.USERNAME) = LOWER(SU.USERS)
                             and U.USERNAME not in ('SYS', 'SYSTEM', 'PWRPLANT')
                           order by USERNAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      DBMS_OUTPUT.PUT_LINE('/*');
      DBMS_OUTPUT.PUT_LINE('|| Name = ' || CSR_CREATE_SQL.USERNAME);
      DBMS_OUTPUT.PUT_LINE('*/');
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE1);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE2);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE3);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE4);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE5);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE6);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE7);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE8);
      DBMS_OUTPUT.PUT_LINE(CSR_CREATE_SQL.LINE9);
      -- DBMS_OUTPUT.PUT_LINE(csr_create_sql.LINE10);
   end loop;

   DBMS_OUTPUT.PUT_LINE('/*');
   DBMS_OUTPUT.PUT_LINE('||    ' || V_RCOUNT || ' Users scripted.');
   DBMS_OUTPUT.PUT_LINE('*/');
end;
/

/*
|| END Script DB Users
*/
SPOOL OFF
SET ECHO ON