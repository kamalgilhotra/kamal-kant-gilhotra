SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.disable_audit_triggers.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: disable_audit_triggers.sql
|| Description: Disable all Audit Triggers
||============================================================================
|| Copyright (C) 2007 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| V10     12/18/2007 Lee Quinn      Created
||============================================================================
*/

/*
|| BEGIN Create PP_UPDATE_FLEX Table if it doesn't exist
||       This table is a multi purpose table designed for update processing.
*/

declare
   TABLE_EXISTS exception;
   pragma exception_init(TABLE_EXISTS, -955);

begin
   DBMS_OUTPUT.ENABLE(2000000);
   execute immediate 'create table PP_UPDATE_FLEX
                      (
                         COL_ID      number(22,0) not null,
                         TYPE_NAME   varchar2(30) not null,
                         FLEXVCHAR1  varchar2(2000) null,
                         FLEXVCHAR2  varchar2(2000) null,
                         FLEXVCHAR3  varchar2(2000) null,
                         FLEXVCHAR4  varchar2(2000) null,
                         FLEXVCHAR5  varchar2(2000) null,
                         FLEXVCHAR6  varchar2(2000) null,
                         FLEXVCHAR7  varchar2(2000) null,
                         FLEXVCHAR8  varchar2(2000) null,
                         FLEXVCHAR9  varchar2(2000) null,
                         FLEXVCHAR10 varchar2(2000) null,
                         FLEXNUM1    number(22,0) null,
                         FLEXNUM2    number(22,0) null,
                         FLEXNUM3    number(22,0) null,
                         FLEXNUM4    number(22,0) null,
                         FLEXNUM5    number(22,0) null,
                         FLEXNUM6    number(22,0) null,
                         FLEXNUM7    number(22,0) null,
                         FLEXNUM8    number(22,0) null,
                         FLEXNUM9    number(22,0) null,
                         FLEXNUM10   number(22,0) null,
                         TIME_STAMP  date null,
                         USER_ID     varchar2(18) null
                      )';
   -- Create the Timestamp/User Trigger for the table.
   execute immediate 'create or replace trigger PP_UPDATE_FLEX
                      before update or insert on PP_UPDATE_FLEX
                      for each row
                      begin
                         :new.user_id := USER;
                         :new.time_stamp := SYSDATE;
                      end;';
exception
   when TABLE_EXISTS then
      --DBMS_OUTPUT.PUT_LINE('SQLCODE = '||SQLCODE||' SQLERRM = '||SQLERRM);
      null;
end;
/

/*
|| END Create PP_UPDATE_FLEX Table
*/

/*
|| BEGIN Disable Audit Triggers
*/
declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   V_RCOUNT       number;
   V_RERRORS      number;
   V_RUN_SEQUENCE number;

begin
   DBMS_OUTPUT.ENABLE(2000000);
   -- Get Sequence for the Run
   select NVL(max(FLEXNUM1), 0) + 1
     into V_RUN_SEQUENCE
     from PP_UPDATE_FLEX;

   -- Log the start of the run
   insert into PP_UPDATE_FLEX
      (COL_ID,
       TYPE_NAME,
       FLEXVCHAR1,
       FLEXVCHAR2,
       FLEXNUM1)
      select NVL(max(COL_ID), 0) + 1,
             'UPDATE_SCRIPTS',
             'disable_audit_triggers.sql',
             'Status = Running',
             V_RUN_SEQUENCE
        from PP_UPDATE_FLEX;
   commit;

   -- Fill table with list of Audit Trigger data
   insert into PP_UPDATE_FLEX
      (COL_ID,
       TYPE_NAME,
       FLEXVCHAR1,
       FLEXVCHAR2,
       FLEXNUM1)
      (select MAX_ID + ROWNUM,
              'AUDIT_TRIGGERS'
              TYPE_NAME,
              TRIGGER_NAME,
              STATUS,
              V_RUN_SEQUENCE
         from ALL_TRIGGERS,
              (select NVL(max(COL_ID), 0) MAX_ID from PP_UPDATE_FLEX) MAX_VIEW
        where OWNER = 'PWRPLANT'
          and (TRIGGER_NAME like 'PP_AU/_%' escape '/' or TRIGGER_NAME like 'PP_AUDITS/_%' escape '/'));
   commit;

   -- Disable Audit triggers that are enabled
   V_RCOUNT  := 0;
   V_RERRORS := 0;
   for ALTER_TRIGGER_SQL in (select 'alter trigger ' || FLEXVCHAR1 || ' disable' SQL_LINE
                               from PP_UPDATE_FLEX
                              where TYPE_NAME = 'AUDIT_TRIGGERS'
                                and FLEXVCHAR2 = 'ENABLED'
                                and FLEXNUM1 = V_RUN_SEQUENCE
                              order by FLEXVCHAR1)
   loop
      begin
         execute immediate ALTER_TRIGGER_SQL.SQL_LINE;
         DBMS_OUTPUT.PUT_LINE(PPCSQL || ALTER_TRIGGER_SQL.SQL_LINE || ';');
         V_RCOUNT := V_RCOUNT + 1;
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE(PPCERR);
            DBMS_OUTPUT.PUT_LINE('ERROR SQL ' || ALTER_TRIGGER_SQL.SQL_LINE || ';');
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
      end;
   end loop;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' TRIGGER(s) altered.');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RERRORS || ' ERROR(s)');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || (V_RCOUNT + V_RERRORS) || ' Total TRIGGER(s) processed.');
   -- Update the log record
   update PP_UPDATE_FLEX
      set FLEXVCHAR2 = 'Status = Complete',
          FLEXVCHAR3 = V_RCOUNT || ' TRIGGER(s) altered.',
          FLEXVCHAR4 = V_RERRORS || ' ERROR(s)',
          FLEXVCHAR5 = V_RCOUNT + V_RERRORS || ' Total TRIGGER(s) processed.',
          FLEXVCHAR6 = 'Run ID in FLEXNUM1 field'
    where FLEXNUM1 = V_RUN_SEQUENCE
      and TYPE_NAME = 'UPDATE_SCRIPTS';
   commit;

exception
   when others then
      DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
end;
/
/*
|| END Disable Audit Triggers
*/
SPOOL OFF