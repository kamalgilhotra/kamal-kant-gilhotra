SET ECHO ON
SET TIME ON
SET SERVEROUTPUT ON

SET SQLPROMPT '_CONNECT_IDENTIFIER> '

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory
|| then there is no need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.v10_sys_grants.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: v10_sys_grants.sql
|| Description: Grants that are run by SYS user.
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.1.0.0 08/04/2008 Lee Quinn      Added GRANT CREATE VIEW TO PWRPLANT_ROLE_DEV
||          12/01/2008 Lee Quinn      Removed GRANT CREATE VIEW TO PWRPLANT_ROLE_DEV
|| 10.1.2.0 12/09/2008 Lee Quinn      Added grant select on SYS.V_$SESSION to PWRPLANT_ROLE_DEV
||          10/08/2009 Lee Quinn      Added create for PP_UPDATE_FLEX table.
||          10/08/2009 Lee Quinn      Added all system grants from db_setup.
||          10/22/2009 Lee Quinn      Added grants to PWRPLANT_ROLE_DEV
||          05/20/2010 Lee Quinn      Commented all grants
||                                    Removed - grant create  procedure to PWRPLANT_ROLE_DEV
||                                    Removed - grant analyze any       to PWRPLANT_ROLE_DEV
||                                    Added - grant select on SYS.DBA_USERS      to PWRPLANT_ROLE_ADMIN
||                                    Added - grant select on SYS.DBA_ROLE_PRIVS to PWRPLANT_ROLE_ADMIN
||                                    Added - grant select on SYS.DBA_PROFILES   to PWRPLANT_ROLE_ADMIN
||          10/21/2010 Lee Quinn      Added - grant administer database trigger  to PWRPLANT
||          05/24/2011 Lee Quinn      Changed PWRPLANT_ROLE_RDONLY to be password protected
||          05/26/2011 Lee Quinn      Added PWRPLANT_ROLE_BATCH for Post and insterface processing
|| 10.3.4.0 02/16/2012 Lee Quinn      Added grant select on gv_$session to pwrplant
|| 10.3.4.0 04/27/2012 Lee Quinn      Added SQLPROMPT
|| 10.3.6.0 01/10/2013 Joseph King    Grants necessary for standard interface shell metrics capture
|| 10.3.6.0 01/10/2013 Lee Quinn      Added logic around create roles not ignore when roles already exist.
|| 10.4.2.0 11/14/2013 Lee Quinn      Added execute on DBMS_ALERT to PWRPLANT
|| 10.4.3.0 07/21/2014 Lee Quinn      Added execute grant on DBMS_LOCK to PWRPLANT for use with POST_PKG
|| 10.4.3.0 10/28/2014 Lee Quinn      Added PWRPLANT_ROLE_SELECT
|| 2015.2.0 08/17/2015 Andrew Scott   Grant create JOB for tax processing to use DBMS_SCHEDULER
|| 2015.2.4 09/29/2015 Lee Quinn      Remove PWRPLANT_ROLE_BATCH because Oracle security doesn't allow 
||                                    a secure role to be granted to a non-secure role as of Oracle 11.2.0.4
||============================================================================
*/

/*
|| BEGIN Create PP_UPDATE_FLEX Table if it doesn't exist
||       This table is a multi purpose table designed for update processing.
*/

declare
   TABLE_EXISTS exception;
   pragma exception_init(TABLE_EXISTS, -955);

begin
   DBMS_OUTPUT.ENABLE(2000000);
   execute immediate 'create table PWRPLANT.PP_UPDATE_FLEX
                      (
                         COL_ID      number(22,0) not null,
                         TYPE_NAME   varchar2(30) not null,
                         FLEXVCHAR1  varchar2(2000) null,
                         FLEXVCHAR2  varchar2(2000) null,
                         FLEXVCHAR3  varchar2(2000) null,
                         FLEXVCHAR4  varchar2(2000) null,
                         FLEXVCHAR5  varchar2(2000) null,
                         FLEXVCHAR6  varchar2(2000) null,
                         FLEXVCHAR7  varchar2(2000) null,
                         FLEXVCHAR8  varchar2(2000) null,
                         FLEXVCHAR9  varchar2(2000) null,
                         FLEXVCHAR10 varchar2(2000) null,
                         FLEXNUM1    number(22,0) null,
                         FLEXNUM2    number(22,0) null,
                         FLEXNUM3    number(22,0) null,
                         FLEXNUM4    number(22,0) null,
                         FLEXNUM5    number(22,0) null,
                         FLEXNUM6    number(22,0) null,
                         FLEXNUM7    number(22,0) null,
                         FLEXNUM8    number(22,0) null,
                         FLEXNUM9    number(22,0) null,
                         FLEXNUM10   number(22,0) null,
                         TIME_STAMP  date null,
                         USER_ID     varchar2(18) null
                      )';
   -- Create the Timestamp/User Trigger for the table.
   execute immediate 'create or replace trigger PWRPLANT.PP_UPDATE_FLEX
                      before update or insert on PWRPLANT.PP_UPDATE_FLEX
                      for each row
                      begin
                         :new.user_id := USER;
                         :new.time_stamp := SYSDATE;
                      end;';
exception
   when TABLE_EXISTS then
      --DBMS_OUTPUT.PUT_LINE('SQLCODE = '||SQLCODE||' SQLERRM = '||SQLERRM);
      null;
end;
/

/*
|| END Create PP_UPDATE_FLEX Table
*/

/*
||============================================================================
|| PWRPLANT - Grants
||============================================================================
*/

grant select on SYS.DBA_SYNONYMS to PWRPLANT --Used by NERDMAN diagnostic tool.
/
grant select on SYS.V_$SESSION   to PWRPLANT --Used for Tax Archiving, Tracing Application, logging info on log in to application
/
grant select on SYS.GV_$SESSION  to PWRPLANT --Used for PP_COUNT_AUDSID function to get count AUDSID
/
grant select on SYS.V_$PROCESS   to PWRPLANT --Used for Tracing Application.
/
grant select on SYS.V_$SESSTAT   to PWRPLANT --Used by NERDMAN diagnostic tool.
/
grant select on SYS.V_$INSTANCE  to PWRPLANT --Used to get DB version info for App.
/

-- Required for Archive System
grant select  on SYS.V_$PARAMETER to PWRPLANT --Used for TAX archiving.
/
grant execute on SYS.DBMS_JOB     to PWRPLANT --Used for TAX archiving.
/
grant create JOB                  to PWRPLANT --Used for Tax processing through DBMS_SCHEDULER
/

-- Sync PowerPlant IDs with Oracle IDs
grant select on SYS.DBA_ROLE_PRIVS to PWRPLANT --Used by the Security System.
/
grant select on SYS.DBA_USERS      to PWRPLANT --Synch DB ID's with application ID's, LDAP package
/
grant select on SYS.DBA_PROFILES   to PWRPLANT --Synch DB ID's with application ID's.
/

grant select, insert, update, delete on SYS.DUAL to PWRPLANT with grant option --Used by a view.
/

grant create session        to PWRPLANT --Allow PWRPLANT to creat session.
/
grant create table          to PWRPLANT --Allow PWRPLANT to create tables.
/
grant create view           to PWRPLANT --Allow PWRPLANT to create Views.
/
grant create public synonym to PWRPLANT --Allow PWRPLANT to create Public Synonyms.
/
grant drop   public synonym to PWRPLANT --Allow PWRPLANT to drop public Synonyms.
/
grant create procedure      to PWRPLANT --Allow PWRPLANT to create Procedures.
/
grant create sequence       to PWRPLANT --Allow PWRPLANT to create Sequences.
/
grant create trigger        to PWRPLANT --Allow PWRPLANT to create Triggers.
/
grant create type           to PWRPLANT --Allow PWRPLANT to create Types.
/
grant select any table      to PWRPLANT --Used for Development.
/
grant analyze any           to PWRPLANT --Allow PWRPLANT Analyze Function to create statistics.
/
grant advisor               to PWRPLANT --Used by Nerdman to look at AWR reports.
/

grant administer database trigger to PWRPLANT --Login trigger to set up POWERPLANT_CTX for auditing
/

-- Needed if you want to be able to create and alter Users in the system
-- and have the Oracle User modified at the same time.
grant drop   USER to PWRPLANT --Allow PWRPLANT to drop DB users.
/
grant create USER to PWRPLANT --Allow PWRPLANT to create DB users.
/
grant alter  USER to PWRPLANT --Allow PWRPLANT to reset DB User accounts and passwords.
/

-- For PowerPlant's packages
grant execute on SYS.UTL_SMTP   to PWRPLANT --Used for the Mail System.
/
grant execute on SYS.DBMS_LOB   to PWRPLANT --Used for the Mail System.
/
grant execute on SYS.UTL_RAW    to PWRPLANT --Used for encryption of the Role password.
/
grant execute on SYS.UTL_ENCODE to PWRPLANT --Used for the Mail System.
/
grant execute on SYS.UTL_TCP    to PWRPLANT --Used for the Mail System.
/
grant execute on SYS.DBMS_OBFUSCATION_TOOLKIT to PWRPLANT --Used for encryption of the Role password.
/
grant execute on DBMS_ALERT to PWRPLANT --Used by new system packages - PP_ALERT_PROCESS
/
grant execute on DBMS_LOCK to PWRPLANT --Used by new system packages - POST_PKG
/

/*
||============================================================================
|| Create Role PWRPLANT_ROLE_USER - Default user role
|| Ignore error if it already exists.
||============================================================================
*/
declare
   ROLE_EXISTS exception;
   pragma exception_init(ROLE_EXISTS, -01921);

begin
   execute immediate 'create role PWRPLANT_ROLE_USER';
exception
   when ROLE_EXISTS then
      DBMS_OUTPUT.PUT_LINE('The PWRPLANT_ROLE_USER role already exists.');
end;
/

grant create session to PWRPLANT_ROLE_USER;

/*
||============================================================================
|| Create Role PWRPLANT_ROLE_DEV - Password protected role
|| Ignore error if it already exists.
||============================================================================
*/
declare
   ROLE_EXISTS exception;
   pragma exception_init(ROLE_EXISTS, -01921);

begin
   execute immediate 'create role PWRPLANT_ROLE_DEV identified by Tr9uI22k';
exception
   when ROLE_EXISTS then
      DBMS_OUTPUT.PUT_LINE('The PWRPLANT_ROLE_DEV role already exists.');
end;
/

grant alter   session   to PWRPLANT_ROLE_DEV --Create Database Trace.
/
grant create  table     to PWRPLANT_ROLE_DEV --Create temp tables for Tax and CR.
/
grant create  trigger   to PWRPLANT_ROLE_DEV --Create Audit Triggers.
/
grant create  procedure to PWRPLANT_ROLE_DEV --Create Procedures.
/
grant create  view      to PWRPLANT_ROLE_DEV --CR uses this
/
grant create  session   to PWRPLANT_ROLE_DEV --Allows users to login to Oracle.
/
grant analyze any       to PWRPLANT_ROLE_DEV --Used for ANALYZE_TABLE function.
/

-- Required for Archive System
grant execute on SYS.DBMS_JOB     to PWRPLANT_ROLE_DEV --Used for TAX archiving.
/
grant select  on SYS.V_$PARAMETER to PWRPLANT_ROLE_DEV --Used for TAX archiving.
/
grant select  on SYS.V_$SESSION   to PWRPLANT_ROLE_DEV --Used for Tax Archiving, Tracing Application, logging info on log in to application
/
grant create JOB                  to PWRPLANT_ROLE_DEV --Used for Tax processing through DBMS_SCHEDULER
/


/*
||============================================================================
|| Create Role PWRPLANT_ROLE_ADMIN - Role required for a user to add new users
|| Ignore error if it already exists.
||============================================================================
*/
declare
   ROLE_EXISTS exception;
   pragma exception_init(ROLE_EXISTS, -01921);

begin
   execute immediate 'create role PWRPLANT_ROLE_ADMIN';
exception
   when ROLE_EXISTS then
      DBMS_OUTPUT.PUT_LINE('The PWRPLANT_ROLE_ADMIN role already exists.');
end;
/

grant select on SYS.DBA_USERS      to PWRPLANT_ROLE_ADMIN --Needed to alter Oracle users.
/
grant select on SYS.DBA_ROLE_PRIVS to PWRPLANT_ROLE_ADMIN --Needed to alter Oracle users.
/
grant select on SYS.DBA_PROFILES   to PWRPLANT_ROLE_ADMIN --Needed to alter Oracle users.
/

/*
||============================================================================
|| Create Role PWRPLANT_ROLE_RDONLY - Role not required
|| Ignore error if it already exists.
||============================================================================
*/
declare
   ROLE_EXISTS exception;
   pragma exception_init(ROLE_EXISTS, -01921);

begin
   execute immediate 'create role PWRPLANT_ROLE_RDONLY identified by Tr9uI22k';
exception
   when ROLE_EXISTS then
      DBMS_OUTPUT.PUT_LINE('The PWRPLANT_ROLE_RDONLY role already exists.');
end;
/

grant alter session to PWRPLANT_ROLE_RDONLY --Create Database Trace.
/

/*
||============================================================================
|| Create the PWRPLANT_ROLE_SELECT if it doesn't exist
|| Ignore error if it already exists.
||============================================================================
*/
declare
   ROLE_EXISTS exception;
   pragma exception_init(ROLE_EXISTS, -01921);

begin
   execute immediate 'create role PWRPLANT_ROLE_SELECT';
exception
   when ROLE_EXISTS then
      DBMS_OUTPUT.PUT_LINE('The PWRPLANT_ROLE_SELECT role already exists.');
end;
/

/*
||============================================================================
|| Grant roles to PWRPLANT
||============================================================================
*/

grant PWRPLANT_ROLE_DEV    to PWRPLANT with admin option --Allows PWRPLANT to grant role to new users.
/
grant PWRPLANT_ROLE_USER   to PWRPLANT with admin option --Allows PWRPLANT to grant role to new users.
/
grant PWRPLANT_ROLE_RDONLY to PWRPLANT with admin option --Allows PWRPLANT to grant role to new users.
/
grant PWRPLANT_ROLE_ADMIN  to PWRPLANT --Allows the PowerPlant ID to create Oracle users.
/
-- DBA role for Development only.
grant DBA                 to PWRPLANT --Used for Development.
/
grant CONNECT             to PWRPLANT --Allow PWRPLANT to Connect.
/
grant SELECT_CATALOG_ROLE to PWRPLANT --Used for Development.
/

/*
|| IF SELECT_CATALOG_ROLE is not granted then need:
*/
grant select on DBA_ROLES     to PWRPLANT;
grant select on DBA_TAB_PRIVS to PWRPLANT;
grant select on DBA_SYS_PRIVS to PWRPLANT;

-- If you removed roles from PWRPLANT then remove from the following default role statement.
alter user PWRPLANT default role CONNECT, PWRPLANT_ROLE_DEV, DBA, SELECT_CATALOG_ROLE;

/*
||============================================================================
|| Create the Context -- Needed for Auditing.
|| Ignore if it already exists.
||============================================================================
*/
declare
   ALREADY_EXISTS exception;
   pragma exception_init(ALREADY_EXISTS, -00955);

begin
   execute immediate 'create context POWERPLANT_CTX using PWRPLANT.AUDIT_TABLE_PKG';
exception
   when ALREADY_EXISTS then
      DBMS_OUTPUT.PUT_LINE('The POWERPLANT_CTX context already exists.');
end;
/

/*
||============================================================================
|| Oracle 10.2 Single Sign-On
||============================================================================
*/

grant select on SYS.DBA_PROXIES to PWRPLANT_ROLE_ADMIN --Needed for Single Sign-on.
/
grant select on SYS.DBA_PROXIES to PWRPLANT --Needed for Single Sign-on.
/

/*
||============================================================================
|| Oracle 11g - uncomment if running Oracle 11g
||============================================================================
*/

/*
declare
   MAILSERVER varchar2(200);
   ACL        varchar2(200);

begin
   MAILSERVER := 'smtp.crc.com';
   ACL        := 'mail.xml';
   DBMS_NETWORK_ACL_ADMIN.CREATE_ACL(ACL, 'Mail ACL', 'PWRPLANT', true, 'connect');
   DBMS_NETWORK_ACL_ADMIN.ASSIGN_ACL(ACL, '*');
   DBMS_NETWORK_ACL_ADMIN.ASSIGN_ACL(ACL, MAILSERVER);
   DBMS_NETWORK_ACL_ADMIN.ADD_PRIVILEGE(ACL, 'PWRPLANT', true, 'connect');
   DBMS_NETWORK_ACL_ADMIN.ADD_PRIVILEGE(ACL, 'PWRPLANT', true, 'resolve');
end;
/

commit;
*/

/*
||============================================================================
|| Check what ACL's are defined
||============================================================================
*/

--select * from DBA_NETWORK_ACLS;
--select * from DBA_NETWORK_ACL_PRIVILEGES;

/*
||============================================================================
|| Can be used to drop an ACL
||============================================================================
*/

--declare
--   ACL varchar2(200);
--
--begin
--   --select * from SYS.DBA_NETWORK_ACLS;
--   ACL := 'mail.xml';
--   DBMS_NETWORK_ACL_ADMIN.DROP_ACL(ACL);
--   commit;
--end;
--/

/*
||============================================================================
|| Maint 11693: Grants necessary for standard interface shell metrics capture
||============================================================================
*/
grant select on SYS.V_$STATNAME         to PWRPLANT;
grant select on SYS.V_$MYSTAT           to PWRPLANT;
grant select on SYS.V_$LATCH            to PWRPLANT;
grant select on SYS.V_$SESS_TIME_MODEL  to PWRPLANT;
grant select on SYS.V_$TIMER            to PWRPLANT;
grant select on SYS.V_$STATNAME         to PWRPLANT_ROLE_DEV;
grant select on SYS.V_$MYSTAT           to PWRPLANT_ROLE_DEV;
grant select on SYS.V_$LATCH            to PWRPLANT_ROLE_DEV;
grant select on SYS.V_$SESS_TIME_MODEL  to PWRPLANT_ROLE_DEV;
grant select on SYS.V_$TIMER            to PWRPLANT_ROLE_DEV;

grant select on SYS.GV_$STATNAME         to PWRPLANT;
grant select on SYS.GV_$MYSTAT           to PWRPLANT;
grant select on SYS.GV_$LATCH            to PWRPLANT;
grant select on SYS.GV_$SESS_TIME_MODEL  to PWRPLANT;
grant select on SYS.GV_$TIMER            to PWRPLANT;
grant select on SYS.GV_$STATNAME         to PWRPLANT_ROLE_DEV;
grant select on SYS.GV_$MYSTAT           to PWRPLANT_ROLE_DEV;
grant select on SYS.GV_$LATCH            to PWRPLANT_ROLE_DEV;
grant select on SYS.GV_$SESS_TIME_MODEL  to PWRPLANT_ROLE_DEV;
grant select on SYS.GV_$TIMER            to PWRPLANT_ROLE_DEV;

grant select on SYS.GV_$MYSTAT to PWRPLANT --Grant necessary for concurrent process locks
/

--**************************
-- Log the run of the script
--**************************

insert into PWRPLANT.PP_UPDATE_FLEX
   (COL_ID,
    TYPE_NAME,
    FLEXVCHAR1)
   select NVL(max(COL_ID), 0) + 1,
          'UPDATE_SCRIPTS',
          'v10_sys_grants.sql'
     from PWRPLANT.PP_UPDATE_FLEX;
commit;

SPOOL OFF