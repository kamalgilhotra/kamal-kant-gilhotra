SET ECHO OFF
SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.revoke_grants_pwrplant_role_dev.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: revoke_grants.sql
|| Description: Revoke Grants for TABLES, VIEWS, SEQUENCES, PROCEDURES,
||              PACKAGES and FUNCTIONS from PWRPLANT_ROLE_DEV.
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| V10.5   03/19/2009 Lee Quinn      Revoke grants from PWRPLANT_ROLE_DEV
||============================================================================
*/

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   V_RCOUNT  number;
   V_RERRORS number;

begin
   DBMS_OUTPUT.ENABLE(2000000);
   -- Log the start of the run
   insert into PP_UPDATE_FLEX
      (COL_ID,
       TYPE_NAME,
       FLEXVCHAR1,
       FLEXVCHAR2)
      select NVL(max(COL_ID), 0) + 1,
             'UPDATE_SCRIPTS',
             'revoke_grants_pwrplant_role_dev.sql',
             'Status = Running'
        from PP_UPDATE_FLEX;
   commit;

   V_RERRORS := 0;

   DBMS_OUTPUT.PUT_LINE('/* Revoke Grants from PWRPLANT_ROLE_DEV */');
   V_RCOUNT := 0;
   for CSR_CREATE_SQL in (select 'revoke all on "' || TABLE_NAME ||
                                 '" from PWRPLANT_ROLE_DEV' CREATE_SQL
                            from (select distinct TABLE_NAME
                                    from DBA_TAB_PRIVS
                                   where GRANTEE = 'PWRPLANT_ROLE_DEV'
                                     and GRANTOR = 'PWRPLANT'
                                     and TABLE_NAME not like 'BIN$%')
                           order by TABLE_NAME)
   loop
      V_RCOUNT := V_RCOUNT + 1;
      begin
         execute immediate CSR_CREATE_SQL.CREATE_SQL;
         DBMS_OUTPUT.PUT_LINE(PPCSQL || CSR_CREATE_SQL.CREATE_SQL || ';');
      exception
         when others then
            V_RERRORS := V_RERRORS + 1;
            DBMS_OUTPUT.PUT_LINE('ERROR SQL = ' || CSR_CREATE_SQL.CREATE_SQL || ';');
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
      end;
   end loop;
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RCOUNT || ' Revokes processed for PWRPLANT_ROLE_DEV.');
   /*
   SHOW ERRORS and Log Run
   */

   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE(PPCMSG || V_RERRORS || ' Errors in Revoke Grants.');

   update PP_UPDATE_FLEX
      set FLEXVCHAR2 = 'Status = Complete',
          FLEXVCHAR3 = V_RERRORS || ' ERROR(s)'
    where TYPE_NAME = 'UPDATE_SCRIPTS'
      and COL_ID in (select max(COL_ID)
                       from PP_UPDATE_FLEX
                      where FLEXVCHAR1 = 'revoke_grants_pwrplant_role_dev.sql'
                        and TYPE_NAME = 'UPDATE_SCRIPTS');
   commit;
end;
/

/*
|| END Revoke Grants from PWRPLANT_ROLE_DEV
*/

SPOOL OFF