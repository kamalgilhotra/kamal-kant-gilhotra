SET ECHO OFF
SET SERVEROUTPUT ON
SET LINESIZE 120

/********
|| run disable_constraints.sql before running this script.
********/

/********
|| run enable_constraints.sql after running this script.
********/

/*
||============================================================================
|| Set PP_SCRIPT_PATH = to the path of the update scripts.
|| If you are running sqlplus from the script directory then there is no
|| need to set PP_SCRIPT_PATH.
|| Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'
||============================================================================
*/
--DEFINE PP_SCRIPT_PATH=''

SPOOL &&PP_SCRIPT_PATH.truncate_all_tables.log
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: truncate_all_tables.sql
|| Description: Truncate all tables
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| V10     05/26/2009 Lee Quinn      Created
||============================================================================
*/

declare
   PPCMSG     varchar2(10) := 'PPC-MSG> ';
   PPCERR     varchar2(10) := 'PPC-ERR> ';
   PPCSQL     varchar2(10) := 'PPC-SQL> ';

begin
   DBMS_OUTPUT.ENABLE(2000000);
   for EXECUTE_SQL in (select 'truncate table ' || TABLE_NAME SQL_LINE
                            from USER_TABLES
                           order by TABLE_NAME)
   loop
      DBMS_OUTPUT.PUT_LINE(PPCSQL || EXECUTE_SQL.SQL_LINE || ';');
      begin
         execute immediate EXECUTE_SQL.SQL_LINE;
      exception
         when others then
            DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
      end;
   end loop;
end;
/

/*
|| END Truncate all Tables
*/
SPOOL OFF
SET ECHO ON