SET ECHO ON
SET TIME ON

/*
***** NOTE: Run before update *****
Log in as SYS and run the GRANT script.

SPOOL &&PP_SCRIPT_PATH.v10_sys_grants.log
@&&PP_SCRIPT_PATH.v10_sys_grants.sql
SPOOL OFF

Log back in as PWPRLANT to run this script.
*/

SET SQLPROMPT '_CONNECT_IDENTIFIER> '

/* Set PP_SCRIPT_PATH = to the path of the update scripts. */
/* If you are running sqlplus from the script directory    */
/* then there is no need to set PP_SCRIPT_PATH.            */
/* Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'       */
DEFINE PP_SCRIPT_PATH=''

/*
***** NOTE: The following statement will exit SQLPlus if any ORA- errors occur.
*****       If SQLPlus exists then review last Log file, resolve issues and
*****       continue running the rest of the scripts after the error location.
*****
*****       Certain sripts may turn on and off the SQLEROR so look below for
*****       comments about specific scripts.
*/
WHENEVER SQLERROR exit failure rollback

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: create_v2018.1.0.0_upgrade.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0  12/12/2018 build script         2018.1.0.0 Release
||============================================================================
|| NOTES: Check log files for: ORA-
||                             SP2-
||                             Enter value for
||                             PPC-ERR>
||                             maximum size
||                             Warning:
||============================================================================
*/

-- ** 2018.1.0.0 **
SPOOL &&PP_SCRIPT_PATH.load_version_data.log
@&&PP_SCRIPT_PATH.load_version_data.sql
SPOOL OFF
SET ECHO OFF
SPOOL &&PP_SCRIPT_PATH.run_auto_generated_upgrade.sql
@&&PP_SCRIPT_PATH.generate_upgrade.sql
SPOOL OFF
SET ECHO ON

/*
Log the run of the script
*/
insert into PP_UPDATE_FLEX
            (COL_ID,
             TYPE_NAME,
             FLEXVCHAR1)
      select NVL(MAX(COL_ID), 0) + 1,
             'UPDATE_SCRIPTS',
             'create_v2018.1.0.0_upgrade.sql'
        from PP_UPDATE_FLEX;
commit;