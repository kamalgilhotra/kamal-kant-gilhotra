create or replace package PKG_DEPR_INCREMENTAL
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: PKG_DEPR_INCREMENTAL
|| Description: Loads and handles the results for Incremental Depreciation
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| 1.0     02/10/2014 Brandon Beck   Create
|| 2.0     03/27/2015 Anand R        PP-43314 - Add NVL around rwip allocation
|| 3.0     06/05/2015 Andrew Scott   PP-43464 - reg incremental depr calc
|| 4.0     08/24/15   Anand R        PP-44361 incremental depr calc for Reg layer
|| 5.0	   09/01/2015 Sarah Byers	 PP-44771 - Added argument to p_handleResults and changed to merge into fcst_cpr_depr_inc
||============================================================================
*/
 as
	/*
	*	This procedure stages data from fcst_depr_ledger_inc into the staging table for depreciation calculation
	*
	*/
   G_PKG_VERSION varchar(35) := '2018.1.0.0';
   procedure P_DEPRSTAGE
   (
		A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
		A_INC_PROCESS_IDS PKG_PP_COMMON.NUM_TABTYPE,
		A_MONTHS PKG_PP_COMMON.DATE_TABTYPE
	);

	/*
	*	This procedure stages data from fcst_cpr_depr_inc into the staging table for depreciation calculation
	*
	*/
   procedure P_CPRDEPRSTAGE
   (
		A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
		A_INC_PROCESS_IDS PKG_PP_COMMON.NUM_TABTYPE,
		A_MONTHS PKG_PP_COMMON.DATE_TABTYPE
	);

	/*
	*	Handles the results of the incremental depreciation calculation
	*/
	procedure P_HANDLERESULTS
	(
		A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE
	);


end PKG_DEPR_INCREMENTAL;
/
create or replace package body PKG_DEPR_INCREMENTAL as
   --**************************************************************
   --       TYPE DECLARATIONS
   --**************************************************************

   --**************************************************************
   --       VARIABLES
   --**************************************************************
   G_RTN           NUMBER;

   --**************************************************************
   --       PROCEDURES
   --**************************************************************

   --**************************************************************************
   --                            P_DEPRSTAGE
   --**************************************************************************
   --
   --  Loads the global temp table for calculating depreciation forecast.
   -- THE Load is based on fcst depr ledger for a version id
   -- AND a single month
   --
	procedure P_DEPRSTAGE(
		A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
		A_INC_PROCESS_IDS PKG_PP_COMMON.NUM_TABTYPE,
		A_MONTHS PKG_PP_COMMON.DATE_TABTYPE
	) is
	my_first_month date;
	my_last_month date;
   begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_INCREMENTAL.P_DEPRSTAGE');

		my_first_month := a_months( a_months.FIRST );
		my_last_month := a_months( a_months.LAST );

		FORALL indx in indices of A_INC_PROCESS_IDS
			insert into DEPR_CALC_STG
			(
				DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
				DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, SMOOTH_CURVE,
				DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD, COMPANY_ID, SUBLEDGER_TYPE_ID,
				DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV, MID_PERIOD_METHOD,
				RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE,
				END_OF_LIFE, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE,
				COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT,
				SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE, DEPR_LEDGER_STATUS,
				BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE,
				RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
				RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR, BEGIN_BALANCE,
				ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE,
				END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
				DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS,
				SALVAGE_CASH, RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT,
				GAIN_LOSS, VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE,
				CURRENT_NET_SALVAGE_AMORT, CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG,
				IMPAIRMENT_RESERVE_ACT, IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION,
				COR_BEG_RESERVE, COR_EXPENSE, COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN,
				COR_RES_TRAN_OUT, COR_RES_ADJUST, COR_END_RESERVE, COST_OF_REMOVAL_RATE,
				COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL, RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS,
				RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST,
				SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT,
				RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER,
				IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT,
				ORIG_DEPR_EXPENSE, ORIG_DEPR_EXP_ALLOC_ADJUST, ORIG_SALV_EXPENSE,
				ORIG_SALV_EXP_ALLOC_ADJUST, ORIG_COR_EXP_ALLOC_ADJUST, ORIG_COR_EXPENSE,
				ORIG_BEGIN_RESERVE, ORIG_COR_BEG_RESERVE, impairment_asset_activity_salv, impairment_asset_begin_balance,
				FCST_DEPR_VERSION_ID, curve_trueup_adj, CURVE_TRUEUP_ADJ_SALV, CURVE_TRUEUP_ADJ_COR,
				SPREAD_FACTOR_ID, INCREMENTAL_PROCESS_ID
			)
			select DL.FCST_DEPR_GROUP_ID, --  depr_group_id                   NULL,
				DL.SET_OF_BOOKS_ID, --  set_of_books_id              NUMBER(22,0)   NULL,
				DL.GL_POST_MO_YR, --  gl_post_mo_yr                DATE           NULL,
				DL.GL_POST_MO_YR,   --  calc_month                   DATE           NULL,
				1, --  depr_calc_status             NUMBER(2,0)    NULL,
				'', --  depr_calc_message            VARCHAR2(2000) NULL,
				LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
															 DGV.COMPANY_ID),
					   'no')), --  trf_in_est_adds              VARCHAR2(35)   NULL,
				LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
															 DGV.COMPANY_ID),
					   'no')), --  include_rwip_in_net          VARCHAR2(35)   NULL,
				LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('DEPR - Smooth Curve True-up',
															 DGV.COMPANY_ID),
					   '0')), -- smooth_curve
				0, --  dnsa_cor_bal                 NUMBER(22,2)   NULL,
				0, --  dnsa_salv_bal                NUMBER(22,2)   NULL,
				0, --  cor_ytd                      NUMBER(22,2)   NULL,
				0, --  salv_ytd                     NUMBER(22,2)   NULL,
				DGV.COMPANY_ID, --  company_id                   NUMBER(22,0)   NULL,
				DGV.SUBLEDGER_TYPE_ID, --  subledger_type_id            NUMBER(22,0)   NULL,
				DGV.DESCRIPTION, --  description                  VARCHAR2(254)  NULL,
				DGV.FCST_DEPR_METHOD_ID, --  depr_method_id               NUMBER(22,0)   NULL,
				DGV.MID_PERIOD_CONV, --  mid_period_conv              NUMBER(22,2)   NULL,
				lower(trim(DGV.MID_PERIOD_METHOD)), --  mid_period_method            VARCHAR2(35)   NULL,
				DMR.RATE, --  rate                         NUMBER(22,8)   NULL,
				CASE
					  WHEN Lower(Trim(DGV.MID_PERIOD_METHOD)) = 'end of life' THEN 1
					  WHEN Lower(Trim(DGV.MID_PERIOD_METHOD)) = 'curve' or Lower(Trim(DGV.MID_PERIOD_METHOD)) = 'curve_no_true_up' THEN 2
					  ELSE DMR.NET_GROSS
				END, --  net_gross                    NUMBER(22,0)   NULL,
				NVL(DMR.OVER_DEPR_CHECK, 0), --  over_depr_check              NUMBER(22,0)   NULL,
				DMR.NET_SALVAGE_PCT, --  net_salvage_pct              NUMBER(22,8)   NULL,
				NVL(DMR.RATE_USED_CODE, 0), --  rate_used_code               NUMBER(22,0)   NULL,
				DMR.END_OF_LIFE, --  end_of_life                  NUMBER(22,0)   NULL,
				NVL(DMR.EXPECTED_AVERAGE_LIFE, 0), --  expected_average_life        NUMBER(22,0)   NULL,
				NVL(DMR.RESERVE_RATIO_ID, 0), --  reserve_ratio_id             NUMBER(22,0)   NULL,
				DMR.COST_OF_REMOVAL_RATE, --  dmr_cost_of_removal_rate     NUMBER(22,8)   NULL,
				DMR.COST_OF_REMOVAL_PCT, --  cost_of_removal_pct          NUMBER(22,8)   NULL,
				DMR.SALVAGE_RATE, --  dmr_salvage_rate             NUMBER(22,8)   NULL,
				DMR.INTEREST_RATE, --  interest_rate                NUMBER(22,8)   NULL,
				NVL(DMR.AMORTIZABLE_LIFE, 0), --  amortizable_life             NUMBER(22,0)   NULL,
				DECODE(LOWER(NVL(DMR.COR_TREATMENT, 'no')), 'no', 0, 1), --  cor_treatment                NUMBER(1,0)    NULL,
				DECODE(LOWER(NVL(DMR.SALVAGE_TREATMENT, 'no')), 'no', 0, 1), --  salvage_treatment            NUMBER(1,0)    NULL,
				NVL(DMR.NET_SALVAGE_AMORT_LIFE, 0), --  net_salvage_amort_life       NUMBER(22,0)   NULL,
				DMR.ALLOCATION_PROCEDURE, --  allocation_procedure         VARCHAR2(5)    NULL,
				DL.DEPR_LEDGER_STATUS, --  depr_ledger_status           NUMBER(22,0)   NULL,
				nvl(DL.BEGIN_RESERVE,0), --  begin_reserve                NUMBER(22,2)   NULL,
				0 AS END_RESERVE, --  end_reserve                  NUMBER(22,2)   NULL,
				DL.RESERVE_BAL_PROVISION, --  reserve_bal_provision        NUMBER(22,2)   NULL,
				DL.RESERVE_BAL_COR, --  reserve_bal_cor              NUMBER(22,2)   NULL,
				DL.SALVAGE_BALANCE, --  salvage_balance              NUMBER(22,2)   NULL,
				DL.RESERVE_BAL_ADJUST, --  reserve_bal_adjust           NUMBER(22,2)   NULL,
				DL.RESERVE_BAL_RETIREMENTS, --  reserve_bal_retirements      NUMBER(22,2)   NULL,
				DL.RESERVE_BAL_TRAN_IN, --  reserve_bal_tran_in          NUMBER(22,2)   NULL,
				DL.RESERVE_BAL_TRAN_OUT, --  reserve_bal_tran_out         NUMBER(22,2)   NULL,
				DL.RESERVE_BAL_OTHER_CREDITS, --  reserve_bal_other_credits    NUMBER(22,2)   NULL,
				DL.RESERVE_BAL_GAIN_LOSS, --  reserve_bal_gain_loss        NUMBER(22,2)   NULL,
				DL.RESERVE_ALLOC_FACTOR, --  reserve_alloc_factor         NUMBER(22,2)   NULL,
				DL.BEGIN_BALANCE, --  begin_balance                NUMBER(22,2)   NULL,
				DL.ADDITIONS, --  additions
				DL.RETIREMENTS, --  retirements                  NUMBER(22,2)   NULL,
				DL.TRANSFERS_IN, --  transfers_in                 NUMBER(22,2)   NULL,
				DL.TRANSFERS_OUT, --  transfers_out                NUMBER(22,2)   NULL,
				DL.ADJUSTMENTS, --  adjustments                  NUMBER(22,2)   NULL,
				0 AS DEPRECIATION_BASE, --  depreciation_base            NUMBER(22,2)
				0 AS END_BALANCE, --  end_balance                  NUMBER(22,2)   NULL
				decode(DL.DEPR_LEDGER_STATUS, 1, DL.DEPRECIATION_RATE, DMR.RATE), --  depreciation_rate            NUMBER(22,8)   NULL,
				0 AS DEPRECIATION_EXPENSE, --  depreciation_expense         NUMBER(22,2)   NULL,
				DL.DEPR_EXP_ADJUST, --  depr_exp_adjust              NUMBER(22,2)   NULL,
				0 AS DEPR_EXP_ALLOC_ADJUST, --  depr_exp_alloc_adjust        NUMBER(22,2)   NULL,
				DL.COST_OF_REMOVAL, --  cost_of_removal              NUMBER(22,2)   NULL,
				DL.RESERVE_RETIREMENTS, --  reserve_retirements          NUMBER(22,2)   NULL,
				DL.SALVAGE_RETURNS, --  salvage_returns              NUMBER(22,2)   NULL,
				DL.SALVAGE_CASH, --  salvage_cash                 NUMBER(22,2)   NULL,
				DL.RESERVE_CREDITS, --  reserve_credits              NUMBER(22,2)   NULL,
				DL.RESERVE_ADJUSTMENTS, --  reserve_adjustments          NUMBER(22,2)   NULL,
				DL.RESERVE_TRAN_IN, --  reserve_tran_in              NUMBER(22,2)   NULL,
				DL.RESERVE_TRAN_OUT, --  reserve_tran_out             NUMBER(22,2)   NULL,
				DL.GAIN_LOSS, --  gain_loss                    NUMBER(22,2)   NULL,
				DL.VINTAGE_NET_SALVAGE_AMORT, --  vintage_net_salvage_amort    NUMBER(22,2)   NULL,
				DL.VINTAGE_NET_SALVAGE_RESERVE, --  vintage_net_salvage_reserve  NUMBER(22,2)   NULL,
				DL.CURRENT_NET_SALVAGE_AMORT, --  current_net_salvage_amort    NUMBER(22,2)   NULL,
				DL.CURRENT_NET_SALVAGE_RESERVE, --  current_net_salvage_reserve  NUMBER(22,2)   NULL,
				DL.IMPAIRMENT_RESERVE_BEG, --  impairment_reserve_beg       NUMBER(22,2)   NULL,
				DL.IMPAIRMENT_RESERVE_ACT, --  impairment_reserve_act       NUMBER(22,2)   NULL,
				DL.IMPAIRMENT_RESERVE_END, --  impairment_reserve_end       NUMBER(22,2)   NULL,
				DL.EST_ANN_NET_ADDS, --  est_ann_net_adds             NUMBER(22,2)   NULL,
				NVL(DL.RWIP_ALLOCATION, 0), --  rwip_allocation              NUMBER(22,2)   NULL,
				DL.COR_BEG_RESERVE, --  cor_beg_reserve              NUMBER(22,2)   NULL,
				0 AS COR_EXPENSE, --  cor_expense                  NUMBER(22,2)   NULL,
				NVL(DL.COR_EXP_ADJUST,0), --  cor_exp_adjust               NUMBER(22,2)   NULL,
				0 AS COR_EXP_ALLOC_ADJUST, --  cor_exp_alloc_adjust         NUMBER(22,2)   NULL,
				DL.COR_RES_TRAN_IN, --  cor_res_tran_in              NUMBER(22,2)   NULL,
				DL.COR_RES_TRAN_OUT, --  cor_res_tran_out             NUMBER(22,2)   NULL,
				DL.COR_RES_ADJUST, --  cor_res_adjust               NUMBER(22,2)   NULL,
				0 AS COR_END_RESERVE, --  cor_end_reserve              NUMBER(22,2)   NULL,
				DL.COST_OF_REMOVAL_RATE, --  cost_of_removal_rate         NUMBER(22,8)   NULL,
				DL.COST_OF_REMOVAL_BASE, --  cost_of_removal_base         NUMBER(22,2)   NULL,
				nvl(DL.RWIP_COST_OF_REMOVAL,0), --  rwip_cost_of_removal         NUMBER(22,2)   NULL,
				nvl(DL.RWIP_SALVAGE_CASH,0), --  rwip_salvage_cash            NUMBER(22,2)   NULL,
				nvl(DL.RWIP_SALVAGE_RETURNS,0), --  rwip_salvage_returns         NUMBER(22,2)   NULL,
				nvl(DL.RWIP_RESERVE_CREDITS,0), --  rwip_reserve_credits         NUMBER(22,2)   NULL,
				nvl(DL.SALVAGE_RATE,0), --  salvage_rate                 NUMBER(22,8)   NULL,
				nvl(SALVAGE_BASE,0), --  salvage_base                 NUMBER(22,2)   NULL,
				nvl(SALVAGE_EXPENSE,0), --  salvage_expense              NUMBER(22,2)   NULL,
				nvl(DL.SALVAGE_EXP_ADJUST,0), --  salvage_exp_adjust           NUMBER(22,2)   NULL,
				nvl(SALVAGE_EXP_ALLOC_ADJUST,0), --  salvage_exp_alloc_adjust     NUMBER(22,2)   NULL,
				nvl(DL.RESERVE_BAL_SALVAGE_EXP,0), --  reserve_bal_salvage_exp      NUMBER(22,2)   NULL,
				nvl(DL.RESERVE_BLENDING_ADJUSTMENT,0), --  reserve_blending_adjustment  NUMBER(22,2)   NULL,
				nvl(DL.RESERVE_BLENDING_TRANSFER,0), --  reserve_blending_transfer    NUMBER(22,2)   NULL,
				nvl(DL.COR_BLENDING_ADJUSTMENT,0), --  cor_blending_adjustment      NUMBER(22,2)   NULL,
				nvl(DL.COR_BLENDING_TRANSFER,0), --  cor_blending_transfer        NUMBER(22,2)   NULL,
				nvl(DL.IMPAIRMENT_ASSET_AMOUNT,0), --  impairment_asset_amount      NUMBER(22,2)   NULL,
				nvl(DL.IMPAIRMENT_EXPENSE_AMOUNT,0), --  impairment_expense_amount    NUMBER(22,2)   NULL,
				nvl(DL.RESERVE_BAL_IMPAIRMENT,0), --  reserve_bal_impairment       NUMBER(22,2)   NUll,
				0, --ORIG_DEPR_EXPENSE
				0, --  orig_depr_exp_alloc_adjust   NUMBER(22,2)   NULL,
				0, --  orig_salv_expense            NUMBER(22,2)   NULL,
				0, --  orig_salv_exp_alloc_adjust   NUMBER(22,2)   NULL,
				0, --  orig_cor_exp_alloc_adjust    NUMBER(22,2)   NULL,
				0, --  orig_cor_expense             NUMBER(22,2)   NULL
				nvl(DL.BEGIN_RESERVE,0), --orig_begin_reserve         NUMBER(22,2) DEFAULT 0  NULL,
				DL.COR_BEG_RESERVE, --  orig_cor_begin_reserve     NUMBER(22,2) DEFAULT 0 NULL
				dl.impairment_asset_activity_salv, dl.impairment_asset_begin_balance,
				A_VERSION_ID,
				0 as curve_trueup_adj,
				0 as CURVE_TRUEUP_ADJ_SALV,
				0 as CURVE_TRUEUP_ADJ_COR,
				DGV.FACTOR_ID,
				INC.INCREMENTAL_PROCESS_ID
			from FCST_DEPR_METHOD_RATES DMR, FCST_DEPR_LEDGER_INC DL, FCST_DEPR_GROUP_VERSION DGV, INCREMENTAL_PROCESS INC
		   where DMR.FCST_DEPR_METHOD_ID = DGV.FCST_DEPR_METHOD_ID
			 and DMR.FCST_DEPR_VERSION_ID = DGV.FCST_DEPR_VERSION_ID
			 and DMR.EFFECTIVE_DATE =
				(select max(DMR1.EFFECTIVE_DATE)
				  from FCST_DEPR_METHOD_RATES DMR1
				 where DMR.FCST_DEPR_METHOD_ID = DMR1.FCST_DEPR_METHOD_ID
				   and DMR.FCST_DEPR_VERSION_ID = DMR1.FCST_DEPR_VERSION_ID
				   and DMR.SET_OF_BOOKS_ID = DMR1.SET_OF_BOOKS_ID
				   and DMR1.EFFECTIVE_DATE <= DL.GL_POST_MO_YR
				 GROUP BY DMR1.FCST_DEPR_METHOD_ID, DMR1.FCST_DEPR_VERSION_ID, DMR1.SET_OF_BOOKS_ID)
			 and DL.FCST_DEPR_GROUP_ID = DGV.FCST_DEPR_GROUP_ID
			 and DL.FCST_DEPR_VERSION_ID = DGV.FCST_DEPR_VERSION_ID
			 and DL.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
			 and DGV.FCST_DEPR_VERSION_ID = A_VERSION_ID
			 and DL.INCREMENTAL_PROCESS_ID = INC.INCREMENTAL_PROCESS_ID
			 and INC.INCREMENTAL_PROCESS_ID = a_inc_process_ids(indx)
			 and dl.gl_post_mo_yr between my_first_month and my_last_month
			 --Either we don't ignore inactive depr groups, or the depr group is active
			 and (
				LOWER(trim(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Ignore Inactive Depr Groups',DGV.COMPANY_ID),'no'))) = 'no'
				or NVL(DGV.STATUS_ID, 1) = 1 or DL.END_BALANCE <> 0 or DL.END_RESERVE <> 0 or
				DL.COR_END_RESERVE <> 0 or DL.BEGIN_BALANCE <> 0 or DL.BEGIN_RESERVE <> 0 or
				DL.COR_BEG_RESERVE <> 0
			 )
			 and DGV.SUBLEDGER_TYPE_ID = 0;

      PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT|| ' rows inserted into DEPR CALC STG');

       update DEPR_CALC_STG
	   set calc_month = my_first_month;

       -- Call a function to stage the depr method blending percents for the version
	   PP_DEPR_PKG.P_STAGE_BLENDING_FCST;

       PP_DEPR_PKG.P_SET_FISCALYEARSTART;

       PP_DEPR_PKG.G_VERSION_ID := A_VERSION_ID;
       PP_DEPR_PKG.P_BACKFILLDATA_FCST(my_first_month, my_last_month);

		G_RTN := analyze_table('DEPR_CALC_STG',100);

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_DEPRSTAGE;


   --**************************************************************************
   --                            P_CPRDEPRSTAGE
   --**************************************************************************
   --
   --  Loads the global temp table for calculating depreciation forecast.
   -- THE Load is based on fcst cpr depr inc for a version id,
   -- array of incremental process ids, and array of months
   --

    procedure P_CPRDEPRSTAGE(
        A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
        A_INC_PROCESS_IDS PKG_PP_COMMON.NUM_TABTYPE,
        A_MONTHS PKG_PP_COMMON.DATE_TABTYPE
    ) is

   begin
        PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_INCREMENTAL.P_CPRDEPRSTAGE');

        FOR i in 1..A_INC_PROCESS_IDS.count loop

            FORALL mndx in indices of A_MONTHS
            insert into CPR_DEPR_CALC_STG
             (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, INIT_LIFE, REMAINING_LIFE,
              ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS,
              DEPR_CALC_STATUS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH,
              SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN,
              RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS,
              DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE,
              YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE, PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB,
              MONTH_RATE, COMPANY_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID,
              DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR, SALVAGE_EXPENSE,
              SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT,
              IMPAIRMENT_EXPENSE_AMOUNT, END_OF_LIFE, NET_GROSS, OVER_DEPR_CHECK, RATE,
              EFFECTIVE_DATE, SUBLEDGER_TYPE_ID, ENG_IN_SERVICE_YEAR, TRF_WEIGHT, MIN_MPC, TRUEUP_ADJ,
              NET_TRF, ACTIVITY, ACTIVITY_3, BEG_RES_AMT, NET_IMP_AMT, EXISTS_TWO_MONTHS, EXISTS_ARO, HAS_CFNU,
              HAS_NURV, HAS_NURV_LAST_MONTH, EXISTS_LAST_MONTH, NURV_ADJ,
              IMPAIRMENT_ASSET_ACTIVITY_SALV, IMPAIRMENT_ASSET_BEGIN_BALANCE,
              INCREMENTAL_PROCESS_ID)
             with CPR_DEPR_VIEW as
              (select a.*
                  from FCST_CPR_DEPR_INC a, depr_fcst_group_stg tt
                 where a.GL_POSTING_MO_YR = A_MONTHS(mndx)
                    and a.FCST_DEPR_VERSION_ID = A_VERSION_ID
                    and a.fcst_depr_group_id = tt.fcst_depr_group_id
                    and a.INCREMENTAL_PROCESS_ID = a_inc_process_ids(i)),
             DEPR_METHOD_RATES_VIEW as
              (select DD.FCST_DEPR_METHOD_ID as DEPR_METHOD_ID,
                         DD.SET_OF_BOOKS_ID,
                         DD.EFFECTIVE_DATE,
                         DD.RATE,
                         DD.OVER_DEPR_CHECK,
                         DD.NET_GROSS,
                         DD.END_OF_LIFE,
                         ROW_NUMBER() OVER(partition by DD.FCST_DEPR_METHOD_ID, DD.SET_OF_BOOKS_ID order by DD.EFFECTIVE_DATE desc) as THE_ROW
                  from FCST_DEPR_METHOD_RATES DD
                  where DD.EFFECTIVE_DATE <= A_MONTHS(mndx)
                    and DD.FCST_DEPR_VERSION_ID = A_VERSION_ID)
              select A.ASSET_ID,
                        A.SET_OF_BOOKS_ID,
                        A.GL_POSTING_MO_YR,
                        nvl(A.INIT_LIFE,0),
                        nvl(A.REMAINING_LIFE,0),
                        nvl(A.ESTIMATED_SALVAGE,0),
                        nvl(A.BEG_ASSET_DOLLARS,0),
                        nvl(A.NET_ADDS_AND_ADJUST,0),
                        nvl(A.RETIREMENTS,0),
                        2,
                        nvl(A.TRANSFERS_IN,0),
                        nvl(A.TRANSFERS_OUT,0),
                        nvl(A.ASSET_DOLLARS,0),
                        A.BEG_RESERVE_MONTH,
                        nvl(A.SALVAGE_DOLLARS,0),
                        nvl(A.RESERVE_ADJUSTMENT,0),
                        nvl(A.COST_OF_REMOVAL,0),
                        nvl(A.RESERVE_TRANS_IN,0),
                        nvl(A.RESERVE_TRANS_OUT,0),
                        nvl(A.DEPR_EXP_ADJUST,0),
                        nvl(A.OTHER_CREDITS_AND_ADJUST,0),
                        nvl(A.GAIN_LOSS,0),
                        0,--A.DEPRECIATION_BASE,
                        0,--CURR_DEPR_EXPENSE
                        nvl(A.DEPR_RESERVE,0),
                        nvl(A.BEG_RESERVE_YEAR,0),
                        nvl(A.YTD_DEPR_EXPENSE,0),
                        nvl(A.YTD_DEPR_EXP_ADJUST,0),
                        nvl(A.PRIOR_YTD_DEPR_EXPENSE,0),
                        nvl(A.PRIOR_YTD_DEPR_EXP_ADJUST,0),
                        A.ACCT_DISTRIB,
                        nvl(A.MONTH_RATE,0),
                        B.COMPANY_ID,
                        lower(trim(B.MID_PERIOD_METHOD)),
                        DECODE(GL_POSTING_MO_YR, A_MONTHS(mndx), NVL(B.MID_PERIOD_CONV, 0), A.MID_PERIOD_CONV) MID_PERIOD_CONV,
                        DECODE(GL_POSTING_MO_YR, A_MONTHS(mndx), B.FCST_DEPR_GROUP_ID, A.FCST_DEPR_GROUP_ID) DEPR_GROUP_ID,
                        0,--DEPR_EXP_ALLOC_ADJUST,
                        NVL(A.FCST_DEPR_METHOD_ID, B.FCST_DEPR_METHOD_ID) DEPR_METHOD_ID,
                        0 TRUE_UP_CPR_DEPR,
                        0,--A.SALVAGE_EXPENSE,
                        nvl(A.SALVAGE_EXP_ADJUST,0),
                        0,--SALVAGE_EXP_ALLOC_ADJUST,
                        nvl(A.IMPAIRMENT_ASSET_AMOUNT,0),
                        nvl(A.IMPAIRMENT_EXPENSE_AMOUNT,0),
                        C.END_OF_LIFE,
                        C.NET_GROSS,
                        C.OVER_DEPR_CHECK,
                        nvl(C.RATE,0),
                        C.EFFECTIVE_DATE,
                        B.SUBLEDGER_TYPE_ID,
                        nvl( ( select D.ENG_IN_SERVICE_YEAR from cpr_ledger d where d.asset_id = a.asset_id ), A_MONTHS(mndx) ),
                        DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                                    B.COMPANY_ID),
                                 'no')),
                            'no',
                            1,
                            0),
                        DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('CPR DEPR: Allow Exp in Month Added',
                                                                    B.COMPANY_ID),
                                 'yes')),
                            'no',
                            0,
                            -1
                            ),
                       0,--TRUEUP_ADJ
                       0,0,0,0,0,
                       0,--EXISTS_TWO_MONTHS
                       0,--EXISTS_ARO
                       0, --HAS_CFNU
                       0, --HAS_NURV
                       0, --HAS_NURV_LAST_MONTH
                       1, --EXISTS_LAST_MONTH
                       0, --NuRV_ADJ
                       nvl(A.IMPAIRMENT_ASSET_ACTIVITY_SALV, 0), nvl(A.IMPAIRMENT_ASSET_BEGIN_BALANCE, 0),
                       a_inc_process_ids(i)
                 from CPR_DEPR_VIEW A, FCST_DEPR_GROUP_VERSION B, DEPR_METHOD_RATES_VIEW C,
                       depr_fcst_group_stg tt, COMPANY_SET_OF_BOOKS CSOB
               where A.FCST_DEPR_GROUP_ID = B.FCST_DEPR_GROUP_ID
                  --and B.SUBLEDGER_TYPE_ID = A_SUBLEDGER --Load all the subledgers for this FCST run
                  and A.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
                  and NVL(A.FCST_DEPR_METHOD_ID, B.FCST_DEPR_METHOD_ID) = C.DEPR_METHOD_ID
                  and A.FCST_DEPR_VERSION_ID = A_VERSION_ID
                  and A.FCST_DEPR_VERSION_ID = B.FCST_DEPR_VERSION_ID
                  and tt.fcst_depr_group_id = b.fcst_depr_group_id
                  and C.THE_ROW = 1
                  and A.SET_OF_BOOKS_ID = CSOB.SET_OF_BOOKS_ID
                  and B.COMPANY_ID = CSOB.COMPANY_ID
				  AND B.SUBLEDGER_TYPE_ID <> 0;

        PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT|| ' rows inserted into CPR DEPR CALC STG, Incremental Process ID : '||a_inc_process_ids(i));

        G_RTN := analyze_table('CPR_DEPR_CALC_STG',100);

        END LOOP;

        PKG_DEPR_IND_CALC.P_BACKFILL_FCST;

        PKG_PP_ERROR.REMOVE_MODULE_NAME;
    exception
        when others then
            PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_CPRDEPRSTAGE;


   --**************************************************************************
   --                            P_HANDLERESULTS
   --**************************************************************************
	procedure P_HANDLERESULTS(
		A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE
	)
	is
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_INCREMENTAL.P_HANDLERESULTS');

      update FCST_DEPR_LEDGER_INC D
         set (BEGIN_RESERVE,
               END_RESERVE,
               BEGIN_BALANCE,
               END_BALANCE,
               DEPRECIATION_BASE,
               DEPRECIATION_RATE,
               DEPRECIATION_EXPENSE,
               DEPR_EXP_ALLOC_ADJUST,
               SALVAGE_BASE,
               SALVAGE_RATE,
               SALVAGE_EXPENSE,
               SALVAGE_EXP_ALLOC_ADJUST,
               SALVAGE_BALANCE,
               COST_OF_REMOVAL_BASE,
               COST_OF_REMOVAL_RATE,
               COR_EXPENSE,
               COR_EXP_ALLOC_ADJUST,
               COR_BEG_RESERVE,
               COR_END_RESERVE,
               IMPAIRMENT_RESERVE_END,
               RESERVE_BAL_IMPAIRMENT,
               RESERVE_BAL_PROVISION,
               RESERVE_BAL_COR,
               RESERVE_BAL_ADJUST,
               RESERVE_BAL_RETIREMENTS,
               RESERVE_BAL_TRAN_IN,
               RESERVE_BAL_TRAN_OUT,
               RESERVE_BAL_GAIN_LOSS,
               RESERVE_BAL_SALVAGE_EXP,
               EST_ANN_NET_ADDS,
               COR_BLENDING_ADJUSTMENT,
               COR_BLENDING_TRANSFER,
               IMPAIRMENT_ASSET_AMOUNT,
               IMPAIRMENT_EXPENSE_AMOUNT,
               RESERVE_BLENDING_ADJUSTMENT,
               RESERVE_BLENDING_TRANSFER,
               RWIP_COST_OF_REMOVAL,
               RWIP_RESERVE_CREDITS,
               RWIP_SALVAGE_CASH,
               RWIP_SALVAGE_RETURNS,
               SALVAGE_EXP_ADJUST) =
              (select S.BEGIN_RESERVE,
                      S.END_RESERVE,
                      S.BEGIN_BALANCE,
                      S.END_BALANCE,
                      S.DEPRECIATION_BASE,
                      S.DEPRECIATION_RATE,
                      S.DEPRECIATION_EXPENSE,
                      (S.DEPR_EXP_ALLOC_ADJUST + S.OVER_DEPR_ADJ + S.RETRO_DEPR_ADJ +
                      S.CURVE_TRUEUP_ADJ),
                      S.SALVAGE_BASE,
                      S.SALVAGE_RATE,
                      S.SALVAGE_EXPENSE,
                      (S.SALVAGE_EXP_ALLOC_ADJUST + S.OVER_DEPR_ADJ_SALV + S.RETRO_SALV_ADJ +
                      S.CURVE_TRUEUP_ADJ_SALV),
                      S.SALVAGE_BALANCE,
                      S.COST_OF_REMOVAL_BASE,
                      S.COST_OF_REMOVAL_RATE,
                      S.COR_EXPENSE,
                      (S.COR_EXP_ALLOC_ADJUST + S.OVER_DEPR_ADJ_COR + S.RETRO_COR_ADJ +
                      S.CURVE_TRUEUP_ADJ_COR),
                      S.COR_BEG_RESERVE,
                      S.COR_END_RESERVE,
                      S.IMPAIRMENT_RESERVE_END,
                      S.RESERVE_BAL_IMPAIRMENT,
                      S.RESERVE_BAL_PROVISION,
                      S.RESERVE_BAL_COR,
                      S.RESERVE_BAL_ADJUST,
                      S.RESERVE_BAL_RETIREMENTS,
                      S.RESERVE_BAL_TRAN_IN,
                      S.RESERVE_BAL_TRAN_OUT,
                      S.RESERVE_BAL_GAIN_LOSS,
                      S.RESERVE_BAL_SALVAGE_EXP,
                      S.EST_ANN_NET_ADDS,
                      S.COR_BLENDING_ADJUSTMENT,
                      S.COR_BLENDING_TRANSFER,
                      S.IMPAIRMENT_ASSET_AMOUNT,
                      S.IMPAIRMENT_EXPENSE_AMOUNT,
                      S.RESERVE_BLENDING_ADJUSTMENT,
                      S.RESERVE_BLENDING_TRANSFER,
                      S.RWIP_COST_OF_REMOVAL,
                      S.RWIP_RESERVE_CREDITS,
                      S.RWIP_SALVAGE_CASH,
                      S.RWIP_SALVAGE_RETURNS,
                      S.SALVAGE_EXP_ADJUST
                 from DEPR_CALC_STG S
                where D.FCST_DEPR_GROUP_ID = S.DEPR_GROUP_ID
                  and D.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                  and D.GL_POST_MO_YR = S.GL_POST_MO_YR
                  and D.FCST_DEPR_VERSION_ID = S.FCST_DEPR_VERSION_ID
                  and S.DEPR_CALC_STATUS = 9)
       where exists (select 1
                from DEPR_CALC_STG S
               where D.FCST_DEPR_GROUP_ID = S.DEPR_GROUP_ID
                 and D.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                 and D.GL_POST_MO_YR = S.GL_POST_MO_YR
                 and D.FCST_DEPR_VERSION_ID = S.FCST_DEPR_VERSION_ID
                 and D.INCREMENTAL_PROCESS_ID = S.INCREMENTAL_PROCESS_ID
                 and S.DEPR_CALC_STATUS = 9);

      PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT|| ' rows updated on FCST DEPR LEDGER INC (with staged data).');

      merge into FCST_CPR_DEPR_INC c
		using (
			select ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, A_VERSION_ID as FCST_DEPR_VERSION_ID, INIT_LIFE,
			REMAINING_LIFE, ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST,
			RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH,
			SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN,
			RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS,
			DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR,
			YTD_DEPR_EXPENSE, YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE,
			PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB, MONTH_RATE, COMPANY_ID,
			MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID as FCST_DEPR_GROUP_ID, DEPR_EXP_ALLOC_ADJUST,
			DEPR_METHOD_ID as FCST_DEPR_METHOD_ID, IMPAIRMENT_ASSET_ACTIVITY_SALV,
			IMPAIRMENT_ASSET_BEGIN_BALANCE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST,
			SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, INCREMENTAL_PROCESS_ID
			from CPR_DEPR_CALC_STG
			WHERE DEPR_CALC_STATUS = 9
		) s
		on (s.asset_id = c.asset_id
			and s.set_of_books_id = c.set_of_books_id
		    and s.gl_posting_mo_yr = c.gl_posting_mo_yr
			and S.FCST_DEPR_VERSION_ID = C.FCST_DEPR_VERSION_ID
			and S.INCREMENTAL_PROCESS_ID = C.INCREMENTAL_PROCESS_ID)
		when matched then
		update set c.depreciation_base = s.depreciation_base,
				   c.curr_depr_expense = s.curr_depr_expense,
				   c.month_rate = s.month_rate,
				   c.depr_exp_alloc_adjust = s.depr_exp_alloc_adjust,
				   c.salvage_expense = s.salvage_expense,
				   c.salvage_exp_alloc_adjust = s.salvage_exp_alloc_adjust,
				   c.depr_reserve = s.depr_reserve,
				   c.mid_period_conv = s.mid_period_conv,
				   c.mid_period_method = s.mid_period_method,
				   c.fcst_depr_method_id = s.fcst_depr_method_id,
				   c.remaining_life = s.remaining_life,
				   c.asset_dollars = s.asset_dollars,
				   c.acct_distrib = s.acct_distrib,
				   c.init_life = s.init_life
		when not matched then insert
		(
			C.ASSET_ID, C.SET_OF_BOOKS_ID, C.GL_POSTING_MO_YR, C.FCST_DEPR_VERSION_ID, C.INIT_LIFE,
			C.REMAINING_LIFE, C.ESTIMATED_SALVAGE, C.BEG_ASSET_DOLLARS, C.NET_ADDS_AND_ADJUST,
			C.RETIREMENTS, C.TRANSFERS_IN, C.TRANSFERS_OUT, C.ASSET_DOLLARS, C.BEG_RESERVE_MONTH,
			C.SALVAGE_DOLLARS, C.RESERVE_ADJUSTMENT, C.COST_OF_REMOVAL, C.RESERVE_TRANS_IN,
			C.RESERVE_TRANS_OUT, C.DEPR_EXP_ADJUST, C.OTHER_CREDITS_AND_ADJUST, C.GAIN_LOSS,
			C.DEPRECIATION_BASE, C.CURR_DEPR_EXPENSE, C.DEPR_RESERVE, C.BEG_RESERVE_YEAR,
			C.YTD_DEPR_EXPENSE, C.YTD_DEPR_EXP_ADJUST, C.PRIOR_YTD_DEPR_EXPENSE,
			C.PRIOR_YTD_DEPR_EXP_ADJUST, C.ACCT_DISTRIB, C.MONTH_RATE, C.COMPANY_ID,
			C.MID_PERIOD_METHOD, C.MID_PERIOD_CONV, C.FCST_DEPR_GROUP_ID, C.DEPR_EXP_ALLOC_ADJUST,
			C.FCST_DEPR_METHOD_ID, C.IMPAIRMENT_ASSET_ACTIVITY_SALV,
			C.IMPAIRMENT_ASSET_BEGIN_BALANCE, C.SALVAGE_EXPENSE, C.SALVAGE_EXP_ADJUST,
			C.SALVAGE_EXP_ALLOC_ADJUST, C.IMPAIRMENT_ASSET_AMOUNT, C.IMPAIRMENT_EXPENSE_AMOUNT, C.INCREMENTAL_PROCESS_ID
		) values
		(
			S.ASSET_ID, S.SET_OF_BOOKS_ID, S.GL_POSTING_MO_YR, S.FCST_DEPR_VERSION_ID, S.INIT_LIFE,
			S.REMAINING_LIFE, S.ESTIMATED_SALVAGE, S.BEG_ASSET_DOLLARS, S.NET_ADDS_AND_ADJUST,
			S.RETIREMENTS, S.TRANSFERS_IN, S.TRANSFERS_OUT, S.ASSET_DOLLARS, S.BEG_RESERVE_MONTH,
			S.SALVAGE_DOLLARS, S.RESERVE_ADJUSTMENT, S.COST_OF_REMOVAL, S.RESERVE_TRANS_IN,
			S.RESERVE_TRANS_OUT, S.DEPR_EXP_ADJUST, S.OTHER_CREDITS_AND_ADJUST, S.GAIN_LOSS,
			S.DEPRECIATION_BASE, S.CURR_DEPR_EXPENSE, S.DEPR_RESERVE, S.BEG_RESERVE_YEAR,
			S.YTD_DEPR_EXPENSE, S.YTD_DEPR_EXP_ADJUST, S.PRIOR_YTD_DEPR_EXPENSE,
			S.PRIOR_YTD_DEPR_EXP_ADJUST, S.ACCT_DISTRIB, S.MONTH_RATE, S.COMPANY_ID,
			S.MID_PERIOD_METHOD, S.MID_PERIOD_CONV, S.FCST_DEPR_GROUP_ID, S.DEPR_EXP_ALLOC_ADJUST,
			S.FCST_DEPR_METHOD_ID, S.IMPAIRMENT_ASSET_ACTIVITY_SALV,
			S.IMPAIRMENT_ASSET_BEGIN_BALANCE, S.SALVAGE_EXPENSE, S.SALVAGE_EXP_ADJUST,
			S.SALVAGE_EXP_ALLOC_ADJUST, S.IMPAIRMENT_ASSET_AMOUNT, S.IMPAIRMENT_EXPENSE_AMOUNT, S.INCREMENTAL_PROCESS_ID
		);

     PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT|| ' rows updated on FCST CPR DEPR INC (with staged CPR data).');

      update FCST_DEPR_LEDGER_INC C
         set (DEPRECIATION_BASE,
               DEPRECIATION_EXPENSE,
               DEPR_EXP_ALLOC_ADJUST,
               SALVAGE_EXPENSE,
               SALVAGE_EXP_ALLOC_ADJUST,
               END_RESERVE,
               END_BALANCE,
               BEGIN_RESERVE,
               BEGIN_BALANCE,
               ADDITIONS,
               RETIREMENTS,
               TRANSFERS_IN,
               TRANSFERS_OUT) =
              (select sum(S.DEPRECIATION_BASE),
                      sum(S.CURR_DEPR_EXPENSE),
                      sum(S.DEPR_EXP_ALLOC_ADJUST) + sum(S.TRUEUP_ADJ),
                      sum(S.SALVAGE_EXPENSE),
                      sum(S.SALVAGE_EXP_ALLOC_ADJUST),
                      sum(S.DEPR_RESERVE),
                      sum(S.ASSET_DOLLARS),
                      sum(S.BEG_RESERVE_MONTH),
                      sum(S.BEG_ASSET_DOLLARS),
                      sum(S.NET_ADDS_AND_ADJUST),
                      sum(S.RETIREMENTS),
                      sum(S.TRANSFERS_IN),
                      sum(S.TRANSFERS_OUT)
                 from CPR_DEPR_CALC_STG S
                where S.DEPR_GROUP_ID = C.FCST_DEPR_GROUP_ID
                  and S.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
                  and S.GL_POSTING_MO_YR = C.GL_POST_MO_YR
                  and S.DEPR_CALC_STATUS = 9)
       where exists (select 1
                from CPR_DEPR_CALC_STG S
               where S.DEPR_GROUP_ID = C.FCST_DEPR_GROUP_ID
                 and S.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
                 and S.GL_POSTING_MO_YR = C.GL_POST_MO_YR
                 and S.INCREMENTAL_PROCESS_ID = C.INCREMENTAL_PROCESS_ID
                 and S.DEPR_CALC_STATUS = 9);

      PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT|| ' rows updated on FCST DEPR LEDGER INC (with staged CPR data).');


		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_HANDLERESULTS;

end PKG_DEPR_INCREMENTAL;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (11629, 0, 2018, 1, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2018.1.0.0_PKG_DEPR_INCREMENTAL.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
