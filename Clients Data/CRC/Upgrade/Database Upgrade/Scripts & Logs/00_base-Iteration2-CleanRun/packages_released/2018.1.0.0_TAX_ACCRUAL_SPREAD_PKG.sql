  CREATE OR REPLACE PACKAGE "PWRPLANT"."TAX_ACCRUAL_SPREAD_PKG"
as
   G_PKG_VERSION varchar(35) := '2018.1.0.0';
   SUBTYPE id_t            IS TAX_ACCRUAL_CONTROL_PKG.ID_T;
   SUBTYPE gl_month_t      IS TAX_ACCRUAL_CONTROL_PKG.GL_MONTH_T;
   SUBTYPE currency_t      IS TAX_ACCRUAL_CONTROL_PKG.CURRENCY_T;
   SUBTYPE percent_t       IS TAX_ACCRUAL_CONTROL_PKG.RATE_T;
   SUBTYPE indicator_t     IS TAX_ACCRUAL_CONTROL_PKG.INDICATOR_T;
   SUBTYPE long_t          IS TAX_ACCRUAL_CONTROL_PKG.LONG_T;
   SUBTYPE spread_pct_t    IS TAX_ACCRUAL_CONTROL_PKG.SPREAD_PCT_T; /*###PATCH(513)*/


   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_spread_ms:   Spread the estimates for all Ms in a given case and set of companies.  This requires that --
   --                the tmp_companies table be populated before calling this function.  If this is called     --
   --                from the process_script, tmp_companies is populated there.                                --
   --                                                                                                          --
   -- Parameters: a_ta_version_id: The ID of the Provision case currently being processed                      --
   --             a_gl_month:       The current month of the case currently being processed                    --
   --             a_spread_sql:     An SQL statement that retrieves the different spread options in use for the--
   --                                  case and companies currently being processed.                           --
   --             a_spread_ms:      An SQL statement that retrieves the M Items that are being spread in the   --
   --                                  current case and companies.                                             --
   --             a_msg:            A string passed back to the calling function containing any error messages.--
   -- ******************************************************************************************************** --
   FUNCTION f_spread_ms (  a_ta_version_id   in tax_accrual_version.ta_version_id%TYPE,
                           a_gl_month        in tax_accrual_gl_month_def.gl_month%TYPE,
                           a_spread_sql      in varchar2,
                           a_spread_ms       in varchar2,
                           a_msg             in out varchar2
                        ) RETURN number;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_spread_dt:   Spread the estimates for all Deferred Taxes in a given case and set of companies.         --
   --                This requires that the tmp_companies table be populated before calling this function.  If --
   --                this is called from the "Go" script, tmp_companies is populated there.                    --
   --                                                                                                          --
   -- Parameters: a_ta_version_id:  The ID of the Provision case currently being processed                     --
   --             a_gl_month:       The current month of the case currently being processed                    --
   --             a_spread_ms:      An SQL statement that retrieves the M Items that are being spread in the   --
   --                                  current case and companies.                                             --
   --             a_msg:         A string passed back to the calling function containing any error messages.   --
   -- ******************************************************************************************************** --
   FUNCTION f_spread_dt (  a_ta_version_id   in tax_accrual_version.ta_version_id%TYPE,
                           a_gl_month        in tax_accrual_gl_month_def.gl_month%TYPE,
                           a_spread_ms       in varchar2,
                           a_msg             in out varchar2
                        ) RETURN number;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_pop_monthly_spread_table:   Populate the temporary table that stores the estimate true-up percentages  --
   --                                                                                                          --
   -- NOTE: All arrays passed in have the same number of elements and elements that are indexed with the same  --
   --       identifier are related.  For example, monthly_spread_id(1) = 1, company_id(1) = 10 refers to all M --
   --       Items with monthly_spread_id 1 and company_id 10                                                   --
   --                                                                                                          --
   -- Parameters: a_ta_version_id:     The ID of the Provision case currently being processed                  --
   --             a_monthly_spread_id: An array containing all of the different monthly spreads in use by the  --
   --                                     case and companies currently being processed                         --
   --             a_company_id:        An array containing all of the companies currently being processed.     --
   --             a_oper_ind:          An array containing all of the oper_inds currently being processed.     --
   --             a_m_id:              An array containing all of the m_ids currently being processed          --
   --             a_by_oper_ind:       An array specifying whether the monthly spread option should pay        --
   --                                     attention to operating indicator.                                    --
   --             a_msg:         A string passed back to the calling function containing any error messages.   --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_pop_monthly_spread_table (  a_ta_version_id      in tax_accrual_version.ta_version_id%TYPE,
                                          a_monthly_spread_id  in id_t,
                                          a_company_id         in id_t,
                                          a_oper_ind           in id_t,
                                          a_m_id               in id_t,
                                          a_by_oper_ind        in indicator_t,
                                          a_msg                in out varchar2
                                       ) RETURN number;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_calc_spread_ms:  Calculates the monthly estimate spread for a set of Ms.                               --
   --                                                                                                          --
   -- NOTE:  The array parameters passed in can really be thought of as an array of structures.  For example,  --
   --          a_m_id(1) and a_gl_month(1) are the m_id and gl_month of the first record                       --
   --                                                                                                          --
   -- Parameters: a_m_id:              An array containing the m_ids of the records to be spread               --
   --             a_gl_month:          An array containing the months that the estimate will be spread over    --
   --             a_annual_amount_est: An array containing the annual estimate to be spread                    --
   --             a_ytd_percent:       An array containing the percentage of the annual estimate that should be--
   --                                     spread up through and including the current month.  The value is -99 --
   --                                     if the estimate is not to be spread in this month  The value is -98  --
   --                                     if the estimate is an input.
   --             a_prior_ytd_est      An array containing the estimate that has been spread through all months--
   --                                     prior to the current month.                                          --
   --             a_update:            1/0 indicator specifying if we want to update the calculated result in  --
   --                                     tax_accrual_m_item.  Typically, we update when we're processing,     --
   --                                     but do not update when we're spreading in the M Item window.         --
   --             a_msg:         A string passed back to the calling function containing any error messages.   --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_calc_spread_ms  (  a_m_id               in id_t,
                                 a_gl_month           in gl_month_t,
                                 a_annual_amount_est  in currency_t,
                                 a_amount_est         in out currency_t,
                                 a_ytd_percent        in spread_pct_t, /*###PATCH(513) Data Type Changed from percent_t*/
                                 a_prior_ytd_est      in currency_t,
                                 a_update             in number,
                                 a_msg                in out varchar2
                              ) RETURN number;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_calc_spread_dt:  Calculates the monthly estimate spread for a set of Ms.                               --
   --                                                                                                          --
   -- NOTE:  The array parameters passed in can really be thought of as an array of structures.  For example,  --
   --          a_m_id(1) and a_ta_norm_id(1) and a_gl_month(1) are the m_id and gl_month of the first record   --
   --                                                                                                          --
   -- Parameters: a_m_id:              An array containing the m_ids of the records to be spread               --
   --             a_ta_norm_id:        An array containing the ta_norm_ids of the records to be spread         --
   --             a_gl_month:          An array containing the months that the estimate will be spread over    --
   --             a_annual_amount_est: An array containing the annual estimate to be spread                    --
   --             a_ytd_percent:       An array containing the percentage of the annual estimate that should be--
   --                                     spread up through and including the current month.  The value is -99 --
   --                                     if the estimate is not to be spread in this month  The value is -98  --
   --                                     if the estimate is an input.
   --             a_prior_ytd_est      An array containing the estimate that has been spread through all months--
   --                                     prior to the current month.                                          --
   --             a_update:            1/0 indicator specifying if we want to update the calculated result in  --
   --                                     tax_accrual_def_tax.  Typically, we update when we're processing,    --
   --                                     but do not update when we're spreading in the M Item window.         --
   --             a_msg:               A string passed back to the calling function containing any error       --
   --                                     messages.                                                            --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_calc_spread_dt  (  a_m_id               in id_t,
                                 a_ta_norm_id         in id_t,
                                 a_gl_month           in gl_month_t,
                                 a_annual_amount_est  in currency_t,
                                 a_amount_est         in out currency_t,
                                 a_ytd_percent        in spread_pct_t, /*###PATCH(513) Data Type Changed from percent_t*/
                                 a_prior_ytd_est      in currency_t,
                                 a_update             in number,
                                 a_msg                in out varchar2
                              ) RETURN number;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_trueup_ms:   Trues up actuals to estimates and pulls RTP months data from other cases for schedule M   --
   --                activity.                                                                                 --
   --                                                                                                          --
   --                                                                                                          --
   -- Parameters: a_ta_version_id:  The ID of the case currently being processed                               --
   --             a_gl_month:       The GL month currently being processed                                     --
   --             a_trueup_sql:     An SQL select statement that retrieves the trueup IDs  used in the current --
   --                                  case.                                                                   --
   --             a_trueup_ms:      An SQL insert statement that inserts the Ms that exist in the current case --
   --                                  into a global temporary table for processing                            --
   --             a_rtp_insert_sql: An SQL insert statement that inserts the activity being pulled from other  --
   --                                  cases into a global temporary table for processing                      --
   --             a_rtp_update_sql: An SQL update statement that updates the activity in the current case with --
   --                                  RTP activity from prior cases.                                          --
   --             a_msg:               A string passed back to the calling function containing any error       --
   --                                     messages.                                                            --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_trueup_ms (  a_ta_version_id   in tax_accrual_version.ta_version_id%TYPE,
                           a_gl_month        in tax_accrual_gl_month_def.gl_month%TYPE,
                           a_trueup_sql      in varchar2,
                           a_trueup_ms       in varchar2,
                           a_rtp_insert_sql  in varchar2,
                           a_rtp_update_sql  in varchar2,
                           a_msg             in out varchar2
                        ) RETURN number;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_pop_trueup_table:  Populate the temporary table that stores the actuals true-up percentages   --
   --                                                                                                          --
   -- NOTE: All arrays passed in have the same number of elements and elements that are indexed with the same  --
   --       identifier are related.  For example, monthly_spread_id(1) = 1, company_id(1) = 10 refers to all M --
   --       Items with monthly_spread_id 1 and company_id 10                                                   --
   --                                                                                                          --
   -- Parameters: a_ta_version_id:  The ID of the Provision case currently being processed                     --
   --             a_trueup_id:      An array containing all of the different trueup IDs in use by the          --
   --                                  case and companies currently being processed                            --
   --             a_company_id:     An array containing all of the companies currently being processed.        --
   --             a_oper_ind:       An array containing all of the oper_inds currently being processed.        --
   --             a_m_id:           An array containing all of the m_ids currently being processed             --
   --             a_by_oper_ind:    An array specifying whether the monthly spread option should pay attention --
   --                                  to operating indicator.                                                 --
   --             a_msg:            A string passed back to the calling function containing any error messages.--
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_pop_trueup_table   (  a_ta_version_id      in tax_accrual_version.ta_version_id%TYPE,
                                    a_trueup_id          in id_t,
                                    a_company_id         in id_t,
                                    a_oper_ind           in id_t,
                                    a_m_id               in id_t,
                                    a_by_oper_ind        in indicator_t,
                                    a_msg                in out varchar2
                                 ) RETURN number;


   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_calc_trueup_ms: Calculates the monthly actuals trueup to an estimate for a set of Ms.                  --
   --                                                                                                          --
   -- NOTE:  The array parameters passed in can really be thought of as an array of structures.  For example,  --
   --          a_m_id(1) and a_gl_month(1) are the m_id and gl_month of the first record                       --
   --                                                                                                          --
   -- Parameters: a_m_id:              An array containing the m_ids of the records to be spread               --
   --             a_gl_month:          An array containing the months that the estimate will be spread over    --
   --             a_annual_amount_est: An array containing the annual estimate to be spread                    --
   --             a_amount_est:        An array containing the current month estimate                          --
   --             a_ytd_percent:       An array containing the total percentage of actuals that should be      --
   --                                     calculated through the current month.  A value of -999 in this field --
   --                                     means that actuals = estimate in this month.  A value of -888 means  --
   --                                     that we're just truing up to the estimate in the the current month   --
   --                                     (no percentage is applied)                                           --
   --             a_ytd_est:           An array containing the total estimate through the current month        --
   --             a_prior_ytd_act:     An array containing the actuals that have been spread through all months--
   --                                     prior to the current month.                                          --
   --             a_amount_calc:       An array containing the M Item calculated amount.                       --
   --             a_amount_manual:     An array containing the M Item input amount                             --
   --             a_amount_act:        An array containing the M Item current month amount (amount_calc +      --
   --                                     amount_manual)                                                       --
   --             a_amount_adj:        An array containing the M Item non-current amount                       --
   --             a_beg_balance:       An array containing the M Item current month beginning balance          --
   --             a_end_balance:       An array containing the M Item current month ending balance             --
   --             a_trueup_option:     An array containing a 1/0 indicator that specifies whether or not we're --
   --                                     truing up to the estimate in the current month.                      --
   --             a_do_not_update:     An array containing a 1/0 indicator that specifies whether or not we're --
   --                                     truing up to the estimate in the current month.  A value of 1 in this--
   --                                     parameter also indicates that if a_update = 1, then we want to       --
   --                                     update tax_accrual_m_item even though the amount_calc value has not  --
   --                                     changed.                                                             --
   --             a_ytd_include:       1/0 indicator specifying whether or not this month should be included   --
   --                                     in the calculation of year-to-date actuals.  1 means exclude from    --
   --                                     year-to-date actuals.   0 means include.       
   --             a_trueup_includes_input:   Indicator that specifies whether prior manual input amounts should--
   --                                     be taken into account when truing up to the estimate.                --
   --             a_num_remaining_trueup_months:   Number of months remaining that will true up to the         --
   --                                     estimate.  This is used to spread the remaining estimate over the    --
   --                                     remaining months.                                                    --
   --             a_spread_remaining_estimate:  1/0 indicator specifying whether or not the trueup option      --
   --                                     calls for us to spread the remaining estimate over the remaining     --
   --                                     months.                                                              --
   --             a_update:            1/0 indicator specifying if we want to update the calculated result in  --
   --                                     tax_accrual_m_item.  Typically, we update when we're processing,     --
   --                                     but do not update when we're spreading in the M Item window.         --
   --             a_msg:               A string passed back to the calling function containing any error       --
   --                                     messages.                                                            --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_calc_trueup_ms  (  a_m_id                        in id_t,
                                 a_gl_month                    in gl_month_t,
                                 a_annual_amount_est           in currency_t,
                                 a_amount_est                  in currency_t,
                                 a_ytd_percent                 in spread_pct_t, /*###PATCH(513) Data Type Changed from percent_t*/
                                 a_ytd_est                     in currency_t,
                                 a_prior_ytd_act               in currency_t,
                                 a_amount_calc                 in out currency_t,
                                 a_amount_manual               in out currency_t,
                                 a_amount_act                  in out currency_t,
                                 a_amount_adj                  in out currency_t,
                                 a_beg_balance                 in out currency_t,
                                 a_end_balance                 in out currency_t,
                                 a_trueup_option               in indicator_t,
                                 a_do_not_update               in indicator_t,
                                 a_ytd_include                 in indicator_t,
								 a_trueup_includes_input       in indicator_t, --###PATCH(8048)
                                 a_num_remaining_trueup_months in long_t,
                                 a_spread_remaining_estimate   in long_t,
                                 a_update                      in number,
                                 a_msg                         in out varchar2
                              ) RETURN number;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_calc_trueup_dt: Calculates the monthly actuals trueup to an estimate for a set of deferred tax records.--
   --                                                                                                          --
   -- NOTE:  The array parameters passed in can really be thought of as an array of structures.  For example,  --
   --          a_m_id(1) and a_ta_norm_id(1) and a_gl_month(1) are the m_id and gl_month of the first record   --
   --                                                                                                          --
   -- Parameters: a_m_id:              An array containing the m_ids of the records to be trued up             --
   --             a_ta_norm_id:        An array containing the ta_norm_ids of the records to be trued up       --
   --             a_gl_month:          An array containing the months that the estimate will be spread over    --
   --             a_annual_amount_est: An array containing the annual estimate to be spread                    --
   --             a_amount_est:        An array containing the current month estimate                          --
   --             a_ytd_percent:       An array containing the total percentage of actuals that should be      --
   --                                     calculated through the current month.  A value of -999 in this field --
   --                                     means that actuals = estimate in this month.  A value of -888 means  --
   --                                     that we're just truing up to the estimate in the the current month   --
   --                                     (no percentage is applied)                                           --
   --             a_ytd_est:           An array containing the total estimate through the current month        --
   --             a_prior_ytd_act:     An array containing the actuals that have been spread through all months--
   --                                     prior to the current month.                                          --
   --             a_amount_calc:       An array containing the calculated deferred tax amount.                 --
   --             a_amount_manual:     An array containing the input deferred tax amount                       --
   --             a_amount_act:        An array containing the deferred tax current month amount               --
   --                                     (amount_calc + amount_manual)                                        --
   --             a_beg_balance:       An array containing the current month deferred tax beginning balance    --
   --             a_end_balance:       An array containing the current month deferred tax ending balance       --
   --             a_trueup_option:     An array containing a 1/0 indicator that specifies whether or not we're --
   --                                     truing up to the estimate in the current month.                      --
   --             a_do_not_update:     An array containing a 1/0 indicator that specifies whether or not we're --
   --                                     truing up to the estimate in the current month.  A value of 1 in this--
   --                                     parameter also indicates that if a_update = 1, then we want to       --
   --                                     update tax_accrual_def_tax even though the amount_calc value has not --
   --                                     changed.                                                             --
   --             a_ytd_include:       1/0 indicator specifying whether or not this month should be included   --
   --                                     in the calculation of year-to-date actuals.  1 means exclude from    --
   --                                     year-to-date actuals.   0 means include.          
   --             a_trueup_includes_input:   Indicator that specifies whether prior manual input amounts should--
   --                                     be taken into account when truing up to the estimate.                --
   --             a_num_remaining_trueup_months:   Number of months remaining that will true up to the         --
   --                                     estimate.  This is used to spread the remaining estimate over the    --
   --                                     remaining months.                                                    --
   --             a_spread_remaining_estimate:  1/0 indicator specifying whether or not the trueup option      --
   --                                     calls for us to spread the remaining estimate over the remaining     --
   --                                     months.                                                              --
   --             a_update:            1/0 indicator specifying if we want to update the calculated result in  --
   --                                     tax_accrual_def_tax.  Typically, we update when we're processing,    --
   --                                     but do not update when we're spreading in the M Item window.         --
   --             a_msg:               A string passed back to the calling function containing any error       --
   --                                     messages.                                                            --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_calc_trueup_dt  (  a_m_id                        in id_t,
                                 a_ta_norm_id                  in id_t,
                                 a_gl_month                    in gl_month_t,
                                 a_annual_amount_est           in currency_t,
                                 a_amount_est                  in currency_t,
                                 a_ytd_percent                 in spread_pct_t, /*###PATCH(513) Data Type Changed from percent_t*/
                                 a_ytd_est                     in currency_t,
                                 a_prior_ytd_act               in currency_t,
                                 a_amount_calc                 in out currency_t,
                                 a_amount_manual               in out currency_t,
                                 a_amount_act                  in out currency_t,
                                 a_beg_balance                 in out currency_t,
                                 a_end_balance                 in out currency_t,
                                 a_trueup_option               in indicator_t,
                                 a_do_not_update               in indicator_t,
                                 a_ytd_include                 in indicator_t,
								 a_trueup_includes_input       in indicator_t, --###PATCH(8048)
                                 a_num_remaining_trueup_months in long_t,
                                 a_spread_remaining_estimate   in long_t,
                                 a_update                      in number,
                                 a_msg                         in out varchar2
                              ) RETURN number;

end tax_accrual_spread_pkg;
/

CREATE OR REPLACE PACKAGE BODY "PWRPLANT"."TAX_ACCRUAL_SPREAD_PKG"
AS

   SUBTYPE currency_type   IS number(22,2);


   --**************************************************************************************
   --
   -- f_spread_ms
   --
   --**************************************************************************************
   FUNCTION f_spread_ms (  a_ta_version_id   in tax_accrual_version.ta_version_id%TYPE,
                           a_gl_month        in tax_accrual_gl_month_def.gl_month%TYPE,
                           a_spread_sql      in varchar2,
                           a_spread_ms       in varchar2,
                           a_msg             in out varchar2
                        ) RETURN number

   is
      function_name        varchar2(254) := 'f_spread_ms';
      sql_msg              varchar2(254);
      monthly_spread_id    id_t;
      company_id           id_t;
      oper_ind             id_t;
      m_id                 id_t;
      m_m_id               id_t;
      m_gl_month           gl_month_t;
      m_annual_amount_est  currency_t;
      m_amount_est         currency_t;
      m_ytd_percent        spread_pct_t; /*###PATCH(513) Data Type Changed from percent_t*/
      m_prior_ytd_est      currency_t;
      by_oper_ind          indicator_t;
      rtn                  number(22);
      update_count         number(22);

      exc_pop_monthly_spread  exception;
      exc_calc_spread         exception;

   begin

      sql_msg := 'Retrieving monthly spread options for processing';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      -- This statement populates arrays that contain the distinct list of spread options, company_ids, and oper_inds
      --    in the current case.  The m_id array contains the m_ids that are being used as the source for the estimate
      --    spread.
      execute immediate a_spread_sql
      bulk collect into monthly_spread_id,
                        company_id,
                        oper_ind,
                        m_id,
                        by_oper_ind;

      -- Populate a temporary table that stores the percentage of the estimate that should be spread by month for each
      --    spread option, company, and oper_ind.
      rtn := f_pop_monthly_spread_table(a_ta_version_id,monthly_spread_id,company_id,oper_ind,m_id,by_oper_ind,a_msg);

      if rtn = -1 then
         raise exc_pop_monthly_spread;
      end if;

      sql_msg := 'Retrieving ms for spreading';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      -- Get the list of Ms and months that we are spreading estimates for.
      execute immediate a_spread_ms
      bulk collect into m_m_id,
                        m_gl_month,
                        m_annual_amount_est,
                        m_amount_est,
                        m_ytd_percent,
                        m_prior_ytd_est;

      -- Spread the estimates, and update the tax_accrual_m_item table with the results.
      rtn := f_calc_spread_ms(m_m_id,m_gl_month,m_annual_amount_est,m_amount_est,m_ytd_percent,m_prior_ytd_est,1,a_msg);

      if rtn = -1 then
         raise exc_calc_spread;
      end if;

      return 1;

   EXCEPTION
      when exc_pop_monthly_spread then
         a_msg := 'Populating estimate spread temporary table: ' || a_msg;
         return -1;
      when exc_calc_spread then
         a_msg := 'Calculating monthly spread: ' || a_msg;
         return -1;
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;

   end;

   --**************************************************************************************
   --
   -- f_spread_dt
   --
   --**************************************************************************************
   FUNCTION f_spread_dt (  a_ta_version_id   in tax_accrual_version.ta_version_id%TYPE,
                           a_gl_month        in tax_accrual_gl_month_def.gl_month%TYPE,
                           a_spread_ms       in varchar2,
                           a_msg             in out varchar2
                        ) RETURN number

   is
      function_name        varchar2(254) := 'f_spread_dt';
      sql_msg              varchar2(254);
      m_m_id               id_t;
      m_ta_norm_id         id_t;
      m_gl_month           gl_month_t;
      m_annual_amount_est  currency_t;
      m_amount_est         currency_t;
      m_ytd_percent        spread_pct_t; /*###PATCH(513) Data Type Changed from percent_t*/
      m_prior_ytd_est      currency_t;
      rtn                  number(22);

      exc_pop_monthly_spread  exception;
      exc_calc_spread         exception;

   begin

      sql_msg := 'Retrieving deferred taxes for spreading';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);


      -- Get the list of deferred tax records and months that we are spreading estimates for.
      execute immediate a_spread_ms
      bulk collect into m_m_id,
                        m_ta_norm_id,
                        m_gl_month,
                        m_annual_amount_est,
                        m_amount_est,
                        m_ytd_percent,
                        m_prior_ytd_est;

      -- Calculate the monthly spread.  Update the tax_accrual_def_tax database.
      rtn := f_calc_spread_dt(m_m_id,m_ta_norm_id,m_gl_month,m_annual_amount_est,m_amount_est,m_ytd_percent,m_prior_ytd_est,1,a_msg);

      if rtn = -1 then
         raise exc_calc_spread;
      end if;

      return 1;

   EXCEPTION
      when exc_calc_spread then
         a_msg := 'Calculating monthly spread: ' || a_msg;
         return -1;
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;

   end;

   --**************************************************************************************
   --
   -- f_pop_monthly_spread_table
   --
   --**************************************************************************************
   FUNCTION f_pop_monthly_spread_table (  a_ta_version_id      in tax_accrual_version.ta_version_id%TYPE,
                                          a_monthly_spread_id  in id_t,
                                          a_company_id         in id_t,
                                          a_oper_ind           in id_t,
                                          a_m_id               in id_t,
                                          a_by_oper_ind        in indicator_t,
                                          a_msg                in out varchar2
                                       ) RETURN number
   is
      function_name        varchar2(254) := 'f_pop_monthly_spread_table';
      sql_msg              varchar2(254);
      num_rows             number(22);
   begin


      sql_msg := 'Deleting from tax_accrual_spread_tmp';
      delete from tax_accrual_spread_tmp;

      num_rows := a_monthly_spread_id.COUNT;

      sql_msg := 'Inserting into temporary estimate spread table for Pretax income amounts';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      -- Insert into the estimate spread table for all Ms that are spread from estimated Pretax income.
      FORALL indx IN 1 .. num_rows
         insert into tax_accrual_spread_tmp
         (  monthly_spread_id,
            company_id,
            oper_ind,
            gl_month,
            ytd_percent
         )
         select   monthly_spread_id,
                  company_id,
                  oper_ind,
                  gl_month,
                  round(decode(sum(annual_amount_est),0,0,sum(monthly_est)/sum(annual_amount_est)),18) ytd_percent /*###PATCH(513) Changed rounding from 8 to 18*/
         from  (  select   control.m_id m_id,
                           spread_template.monthly_spread_id monthly_spread_id,
                           control.company_id company_id,
                           a_oper_ind(indx) oper_ind,
                           spread_template.gl_month gl_month,
                           control.annual_amount_est annual_amount_est,
                           sum(m_item.amount_est) monthly_est
                  from  tax_accrual_control control,
                        tax_accrual_m_item m_item,
                        tax_accrual_spread_template spread_template,
                        tax_accrual_m_master m_master
                  where control.m_id = m_item.m_id
                     and control.m_item_id = m_master.m_item_id
                     and control.ta_version_id = a_ta_version_id
                     and m_master.m_type_id in (1,10)
                     and spread_template.monthly_spread_id = a_monthly_spread_id(indx)
                     and control.company_id = a_company_id(indx)
                     and a_m_id(indx) = -1
                     and decode(a_by_oper_ind(indx),1,control.oper_ind,-1) = decode(a_by_oper_ind(indx),1,a_oper_ind(indx),-1)
                     and m_item.gl_month <= spread_template.gl_month
                  group by control.m_id,
                           spread_template.monthly_spread_id,
                           control.company_id,
                           a_oper_ind(indx),
                           spread_template.gl_month,
                           control.annual_amount_est
               )
         group by monthly_spread_id,
                  company_id,
                  oper_ind,
                  gl_month;

      sql_msg := 'Inserting into temporary estimate spread table for non-pretax income M Item templates';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      -- Insert into the estimate spread table for all Ms that are spread from Ms other than Pretax income.
      FORALL indx IN 1 .. num_rows
         insert into tax_accrual_spread_tmp
         (  monthly_spread_id,
            company_id,
            oper_ind,
            gl_month,
            ytd_percent
         )
         select   monthly_spread_id,
                  company_id,
                  oper_ind,
                  gl_month,
                  round(decode(sum(annual_amount_est),0,0,sum(monthly_est)/sum(annual_amount_est)),18) ytd_percent /*###PATCH(513) Changed rounding from 8 to 18*/
         from  (  select   control.m_id m_id,
                           spread_template.monthly_spread_id monthly_spread_id,
                           control.company_id company_id,
                           a_oper_ind(indx) oper_ind,
                           spread_template.gl_month gl_month,
                           control.annual_amount_est annual_amount_est,
                           sum(m_item.amount_est) monthly_est
                  from  tax_accrual_control control,
                        tax_accrual_m_item m_item,
                        tax_accrual_spread_template spread_template
                  where control.m_id = m_item.m_id
                     and control.ta_version_id = a_ta_version_id
                     and control.m_item_id = a_m_id(indx)
                     and control.company_id = a_company_id(indx)
                     and spread_template.monthly_spread_id = a_monthly_spread_id(indx)
                     and decode(a_by_oper_ind(indx),1,control.oper_ind,-1) = decode(a_by_oper_ind(indx),1,a_oper_ind(indx),-1)
                     and m_item.gl_month <= spread_template.gl_month
                  group by control.m_id,
                           spread_template.monthly_spread_id,
                           control.company_id,
                           a_oper_ind(indx),
                           spread_template.gl_month,
                           control.annual_amount_est
                  union all
                  select   a_m_id(indx),
                           a_monthly_spread_id(indx),
                           a_company_id(indx),
                           a_oper_ind(indx),
                           spread_template.gl_month,
                           0,
                           0
                  from tax_accrual_spread_template spread_template
                  where a_m_id(indx) > 0
                     and not exists (  select 1
                                       from tax_accrual_control
                                       where ta_version_id = a_ta_version_id
                                          and company_id = a_company_id(indx)
                                          and decode(a_by_oper_ind(indx),1,oper_ind,-1) = decode(a_by_oper_ind(indx),1,a_oper_ind(indx),-1)
                                          and m_item_id = a_m_id(indx)
                                    )
               )
         group by monthly_spread_id,
                  company_id,
                  oper_ind,
                  gl_month;

      sql_msg := 'Inserting into temporary estimate spread table for items with normal spreads';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      -- Insert into the estimate spread table for all Ms that are are spread using a "normal" percentage such as Monthly or Quarterly
      FORALL indx IN 1 .. num_rows
         insert into tax_accrual_spread_tmp
         (  monthly_spread_id,
            company_id,
            oper_ind,
            gl_month,
            ytd_percent
         )
         select   spread_template.monthly_spread_id monthly_spread_id,
                  a_company_id(indx) company_id,
                  a_oper_ind(indx) oper_ind,
                  gl_month,
                  ytd_percent
         from tax_accrual_spread_template spread_template
         where spread_template.monthly_spread_id = a_monthly_spread_id(indx)
            and a_m_id(indx) = 0;

      sql_msg := 'Inserting into temporary estimate spread table for items with input estimates';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      -- Insert into the estimate spread table for all Ms that are are use input estimates
      FORALL indx IN 1 .. num_rows
         insert into tax_accrual_spread_tmp
         (  monthly_spread_id,
            company_id,
            oper_ind,
            gl_month,
            ytd_percent
         )
         select   2 monthly_spread_id,
                  a_company_id(indx) company_id,
                  a_oper_ind(indx) oper_ind,
                  gl_month.gl_month,
                  -98
         from tax_accrual_version_gl_month gl_month
         where a_monthly_spread_id(indx) = 2
            and gl_month.ta_version_id = a_ta_version_id;

      a_msg := 'SUCCESS';
      return 1;

   EXCEPTION
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;

   end f_pop_monthly_spread_table;


   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_calc_spread:  Calculates the monthly estimate spread for schedule Ms and deferred taxes.               --
   --                                                                                                          --
   -- NOTE:  The array parameters passed in can really be thought of as an array of structures.  For example,  --
   --          a_m_id(1) and a_ta_norm_id(1) and a_gl_month(1) are the m_id and gl_month of the first record   --
   --                                                                                                          --
   -- Parameters: a_m_id:              An array containing the m_ids of the records to be spread               --
   --             a_ta_norm_id:        An array containing the ta_norm_ids of the records to be spread.  If we --
   --                                     are spreading schedule Ms, the array only contains one element and   --
   --                                     the value in that element is -99                                     --
   --             a_gl_month:          An array containing the months that the estimate will be spread over    --
   --             a_annual_amount_est: An array containing the annual estimate to be spread                    --
   --             a_ytd_percent:       An array containing the percentage of the annual estimate that should be--
   --                                     spread up through and including the current month.  The value is -99 --
   --                                     if the estimate is not to be spread in this month.  The value is -98 --
   --                                     if the estimate is an input.
   --             a_prior_ytd_est      An array containing the estimate that has been spread through all months--
   --                                     prior to the current month.                                          --
   --             a_update:            1/0 indicator specifying if we want to update the calculated result in  --
   --                                     the database.  Typically, we update when we're processing,           --
   --                                     but do not update when we're spreading in the M Item window.         --
   --             a_msg:               A string passed back to the calling function containing any error       --
   --                                     messages.                                                            --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_calc_spread  (  a_m_id               in id_t,
                              a_ta_norm_id         in id_t,
                              a_gl_month           in gl_month_t,
                              a_annual_amount_est  in currency_t,
                              a_amount_est         in out currency_t,
                              a_ytd_percent        in spread_pct_t, /*###PATCH(513) Data Type Changed from percent_t*/
                              a_prior_ytd_est      in currency_t,
                              a_update             in number,
                              a_msg                in out varchar2
                           ) RETURN number

   is
      amount_est_orig   currency_type;
      m_id_update       id_t;
      ta_norm_id_update id_t;
      gl_month_update   gl_month_t;
      amount_est_update currency_t;
      num_rows          number(22);
      prev_m_id         tax_accrual_m_item.m_id%TYPE := 0;
      ytd_est_roll      currency_type;
      update_count      number(22) := 0;
      sql_msg           varchar2(254);
      prev_ta_norm_id   tax_accrual_norm_schema.ta_norm_id%TYPE := 0;
      ta_norm_id        tax_accrual_norm_schema.ta_norm_id%TYPE := 0;

      is_dt_calc        BOOLEAN;

      function_name     varchar2(254) := 'f_calc_spread';
   begin

      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'Begin f_calc_spread');
      num_rows := a_m_id.COUNT;

      -- We'll get an error in our check of a_ta_norm_id(1) if we have no rows in the array.  That could happen if we process
      --    a company that has no deferred taxes.
      if num_rows = 0 then
         return 1;
      end if;

      -- If we're spreading M Items, then the a_ta_norm_id array will contain only one element, and the value in that element will be -99.
      --    The logic for spreading estimates is essentially the same for Ms and deferred taxes, but there are subtle differences, so we need
      --    to determine what is that we're spreading here.
      if a_ta_norm_id(1) = -99 then
         is_dt_calc := FALSE;
         prev_ta_norm_id := -99;
         ta_norm_id := -99;
      else
         is_dt_calc := TRUE;
      end if;

      -- Loop through all M Item/Deferred Tax records and gl_months and calculate the amount of the annual estimate to spread to each month.
      --    The array needs to be sorted correctly (m_id, ta_norm_id (for deftax only), gl_month) for this to work correctly.  That sort is
      --    is done when the array is populated from the select statement.
      for i in 1 .. num_rows loop
         amount_est_orig := a_amount_est(i);

         if is_dt_calc then
            ta_norm_id := a_ta_norm_id(i);
         end if;

         -- If this is the first month for a given m_id and ta_norm_id (deftax only) then we retrieved the prior ytd amounts in the SQL statement.
         --    Otherwise, we calculate a rolling amount.
         if prev_m_id <> a_m_id(i) or prev_ta_norm_id <> ta_norm_id then
            ytd_est_roll := a_prior_ytd_est(i);
         end if;

         prev_m_id := a_m_id(i);

         if is_dt_calc then
            prev_ta_norm_id := a_ta_norm_id(i);
         end if;

         if a_ytd_percent(i) = -99 then --This is a month that we're not spreading to
            a_amount_est(i) := 0;
         elsif a_ytd_percent(i) = -98 then --This indicates that this is an input estimate, so leave it alone
            null; --No calc requred
         else
            -- The current month estimate is calculated as the total estimate (annual_amount_est), multiplied by the year-to-date
            --    percentage of the estimate that we should have in the current month, minus the prior year-to-date estimate that we've
            --    already calculated.
            a_amount_est(i) := round(a_annual_amount_est(i) * a_ytd_percent(i) - ytd_est_roll,2);
         end if; --a_ytd_percent(i) = -99

         -- We need to add the newly-calculated estimate to our rolling prior year-to-date amount
         ytd_est_roll := ytd_est_roll + a_amount_est(i);

         -- In order by speed up processing, we only want to update this record to the database if the new estimate for the month calculated
         --    in this function is different than the original monthly estimate.
         if a_update = 1 and a_amount_est(i) <> amount_est_orig then
            update_count := update_count + 1;
            amount_est_update(update_count) := a_amount_est(i);
            m_id_update(update_count) := a_m_id(i);
            gl_month_update(update_count) := a_gl_month(i);
            ta_norm_id_update(update_count) := ta_norm_id;
         end if;

      end loop; --i in 1 .. num_rows loop

      if a_update = 1 then
         if is_dt_calc then
            sql_msg := 'Updating Def Tax estimates with new amounts';
            tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);
            FORALL indx IN 1 .. update_count
            update tax_accrual_def_tax
            set   amount_est = amount_est_update(indx)
            where m_id = m_id_update(indx)
               and ta_norm_id = ta_norm_id_update(indx)
               and gl_month = gl_month_update(indx);
         else
            sql_msg := 'Updating M Item estimates with new amounts';
            tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);
            FORALL indx IN 1 .. update_count
            update tax_accrual_m_item
            set   amount_est = amount_est_update(indx)
            where m_id = m_id_update(indx)
               and gl_month = gl_month_update(indx);
         end if;

      end if; --a_update = 1

      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'End f_calc_spread');

      return 1;

   EXCEPTION
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;

   end f_calc_spread;


   --**************************************************************************************
   --
   -- f_calc_spread_ms
   --
   --**************************************************************************************
   FUNCTION f_calc_spread_ms  (  a_m_id               in id_t,
                                 a_gl_month           in gl_month_t,
                                 a_annual_amount_est  in currency_t,
                                 a_amount_est         in out currency_t,
                                 a_ytd_percent        in spread_pct_t, /*###PATCH(513) Data Type Changed from percent_t*/
                                 a_prior_ytd_est      in currency_t,
                                 a_update             in number,
                                 a_msg                in out varchar2
                              ) RETURN number

   is
      rtn         number(22);
      ta_norm_id  id_t;
      function_name varchar2(254) := 'f_calc_spread_ms';
   begin
      ta_norm_id(1) := -99;

      rtn := f_calc_spread(a_m_id, ta_norm_id, a_gl_Month, a_annual_amount_est, a_amount_est, a_ytd_percent,
                           a_prior_ytd_est, a_update, a_msg);

      return rtn;

   EXCEPTION
      when others then
         a_msg := 'Error Spreading Ms : ' || a_msg || ': ' || SQLERRM;
         return -1;

   end f_calc_spread_ms;

   --**************************************************************************************
   --
   -- f_calc_spread_dt
   --
   --**************************************************************************************
   FUNCTION f_calc_spread_dt  (  a_m_id               in id_t,
                                 a_ta_norm_id         in id_t,
                                 a_gl_month           in gl_month_t,
                                 a_annual_amount_est  in currency_t,
                                 a_amount_est         in out currency_t,
                                 a_ytd_percent        in spread_pct_t, /*###PATCH(513) Data Type Changed from percent_t*/
                                 a_prior_ytd_est      in currency_t,
                                 a_update             in number,
                                 a_msg                in out varchar2
                              ) RETURN number

   is
      rtn   number(22);
      function_name varchar2(254) := 'f_calc_spread_dt';
   begin

      rtn := f_calc_spread(a_m_id, a_ta_norm_id, a_gl_month, a_annual_amount_est, a_amount_est, a_ytd_percent,
                           a_prior_ytd_est, a_update, a_msg);

      return rtn;

   EXCEPTION
      when others then
         a_msg := 'Error Spreading DefTax : ' || a_msg || ': ' || SQLERRM;
         return -1;

   end f_calc_spread_dt;

   --**************************************************************************************
   --
   -- f_trueup_ms
   --
   --**************************************************************************************
   FUNCTION f_trueup_ms (  a_ta_version_id   in tax_accrual_version.ta_version_id%TYPE,
                           a_gl_month        in tax_accrual_gl_month_def.gl_month%TYPE,
                           a_trueup_sql      in varchar2,
                           a_trueup_ms       in varchar2,
                           a_rtp_insert_sql  in varchar2,
                           a_rtp_update_sql  in varchar2,
                           a_msg             in out varchar2
                        ) RETURN number
   is
      function_name                 varchar2(254) := 'f_trueup_ms';
      sql_msg                       varchar2(254);
      trueup_id                     id_t;
      company_id                    id_t;
      oper_ind                      id_t;
      m_id                          id_t;
      m_m_id                        id_t;
      m_gl_month                    gl_month_t;
      m_annual_amount_est           currency_t;
      m_amount_est                  currency_t;
      m_ytd_percent                 spread_pct_t; /*###PATCH(513) Data Type Changed from percent_t*/
      m_ytd_est                     currency_t;
      m_prior_ytd_act               currency_t;
      m_amount_calc                 currency_t;
      m_amount_manual               currency_t;
      m_amount_act                  currency_t;
      m_amount_adj                  currency_t;
      m_beg_balance                 currency_t;
      m_end_balance                 currency_t;
      m_trueup_option               indicator_t;
      by_oper_ind                   indicator_t;
      do_not_update                 indicator_t;
	  ytd_include                   indicator_t;
      trueup_includes_input         indicator_t; --###PATCH(8048)
	  rtn                           number(22);
      update_count                  number(22);
      num_remaining_trueup_months   long_t;
      spread_remaining_estimate     long_t;

      exc_pop_trueup    exception;
      exc_calc_trueup   exception;

      cursor m_item_tmp_cur is
      select   m_id,
               gl_month,
               annual_amount_est,
               amount_est,
               ytd_percent,
               ytd_est,
               prior_ytd_act,
               amount_calc,
               amount_manual,
               amount_act,
               amount_adj,
               beg_balance,
               end_balance,
               trueup_option,
               do_not_update,
               ytd_include,
			   trueup_includes_input, --###PATCH(8048)
               num_remaining_trueup_months,
               spread_remaining_estimate
      from tax_accrual_m_item_proc_tmp
      order by m_id, gl_month;   -- order by is very important here, as the actuals true-up function relies on these records being
                                 --    ordered by m_id and gl_month

   begin
      sql_msg := 'Retrieving trueup options for processing';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      -- Get the distinct list of trueup ids, company ids, and oper inds that we're using to calculate actuals trueup
      execute immediate a_trueup_sql
      bulk collect into trueup_id,
                        company_id,
                        oper_ind,
                        m_id,
                        by_oper_ind;

      -- Populate the temporary table that specifies the total actual percentage to be used to true up schedule Ms
      rtn := f_pop_trueup_table(a_ta_version_id,trueup_id,company_id,oper_ind,m_id,by_oper_ind,a_msg);

      if rtn = -1 then
         raise exc_pop_trueup;
      end if;

      sql_msg := 'Inserting into M Item Temp Table';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      -- Insert into the temporary table that stores all schedule Ms being processed in the current case and set of companies.
      execute immediate a_trueup_ms;

      if upper(a_rtp_insert_sql) <> 'NONE' then

         sql_msg := 'Inserting into M Item RTP Temp Table';
         tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

         -- Insert into the temporary table that stores the schedule M amounts being brought forward from other cases.
         execute immediate a_rtp_insert_sql;

         sql_msg := 'Updating M Item Temp Table with RTP Amounts';
         tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

         -- Update the M Item temporary table with the amounts being brought forward from other cases.
         execute immediate a_rtp_update_sql;
      end if;

      sql_msg := 'Retrieving ms for trueup';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      open m_item_tmp_cur;

      -- Retrieve the M Items that are being processed in the current case
      fetch m_item_tmp_cur
      bulk collect into m_m_id,
                        m_gl_month,
                        m_annual_amount_est,
                        m_amount_est,
                        m_ytd_percent,
                        m_ytd_est,
                        m_prior_ytd_act,
                        m_amount_calc,
                        m_amount_manual,
                        m_amount_act,
                        m_amount_adj,
                        m_beg_balance,
                        m_end_balance,
                        m_trueup_option,
                        do_not_update,
                        ytd_include,
						trueup_includes_input, --###PATCH(8048)
                        num_remaining_trueup_months,
                        spread_remaining_estimate;

      close m_item_tmp_cur;

      -- Calculate the monthly trueup
      rtn := f_calc_trueup_ms (  m_m_id,m_gl_month,m_annual_amount_est,m_amount_est,m_ytd_percent,m_ytd_est,m_prior_ytd_act,
                                 m_amount_calc,m_amount_manual,m_amount_act,m_amount_adj,m_beg_balance,m_end_balance,
                                 m_trueup_option,do_not_update,ytd_include,trueup_includes_input, --###PATCH(8048)
								 num_remaining_trueup_months,spread_remaining_estimate,1,a_msg
                              );

      if rtn = -1 then
         raise exc_calc_trueup;
      end if;

      return 1;

   EXCEPTION
      when exc_pop_trueup then
         a_msg := 'Populating trueup temporary table: ' || a_msg;
         return -1;
      when exc_calc_trueup then
         a_msg := 'Calculating monthly spread: ' || a_msg;
         return -1;
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;
   end f_trueup_ms;

   --**************************************************************************************
   --
   -- f_pop_trueup_table
   --
   --**************************************************************************************
   FUNCTION f_pop_trueup_table   (  a_ta_version_id      in tax_accrual_version.ta_version_id%TYPE,
                                    a_trueup_id          in id_t,
                                    a_company_id         in id_t,
                                    a_oper_ind           in id_t,
                                    a_m_id               in id_t,
                                    a_by_oper_ind        in indicator_t,
                                    a_msg                in out varchar2
                                 ) RETURN number
   is
      function_name        varchar2(254) := 'f_pop_monthly_trueup_table';
      sql_msg              varchar2(254);
      num_rows             number(22);
   begin
      sql_msg := 'Deleting from tax_accrual_trueup_tmp';
      delete from tax_accrual_trueup_tmp;

      num_rows := a_trueup_id.COUNT;

      sql_msg := 'Inserting into temporary trueup table for Pretax income amounts';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      -- Insert into the temporary table that stores actuals true up percentages for items being trued up based on pretax income
      FORALL indx IN 1 .. num_rows
         insert into tax_accrual_trueup_tmp
         (  trueup_id,
            from_pt_ind, /*###PATCH(475)*/
            company_id,
            oper_ind,
            gl_month,
            ytd_percent,
            current_month_trueup,
            out_month_trueup,
            current_month_trueup_dt,
            out_month_trueup_dt,
            num_remaining_trueup_months,
            num_remaining_trueup_months_dt,
            spread_remaining_estimate,
			trueup_includes_input, --###PATCH(8048)
            trueup_includes_input_dt --###PATCH(8048)
         )
         select   trueup_id,
                  from_pt_ind, /*###PATCH(475)*/
                  company_id,
                  oper_ind,
                  gl_month,
                  ytd_percent,
                  current_month_trueup,
                  out_month_trueup,
                  current_month_trueup_dt,
                  out_month_trueup_dt,
                  sum(out_month_trueup) over (  partition by   trueup_id,
                                                               from_pt_ind, /*###PATCH(513)*/
                                                               company_id,
                                                               oper_ind
                                             ) -
                     sum(out_month_trueup) over (  partition by   trueup_id,
                                                                  from_pt_ind, /*###PATCH(513)*/
                                                                  company_id,
                                                                  oper_ind
                                                   order by trueup_id,
                                                            from_pt_ind, /*###PATCH(513)*/
                                                            company_id,
                                                            oper_ind,
                                                            gl_month
                                                ) num_remaining_trueup_months,
                  sum(out_month_trueup_dt) over (  partition by   trueup_id,
                                                                  from_pt_ind, /*###PATCH(513)*/
                                                                  company_id,
                                                                  oper_ind
                                                ) -
                     sum(out_month_trueup_dt) over (  partition by   trueup_id,
                                                                     from_pt_ind, /*###PATCH(513)*/
                                                                     company_id,
                                                                     oper_ind
                                                      order by trueup_id,
                                                               from_pt_ind, /*###PATCH(513)*/
                                                               company_id,
                                                               oper_ind,
                                                               gl_month
                                                   ) num_remaining_trueup_months_dt,
                  spread_remaining_estimate,
                  trueup_includes_input, --###PATCH(8048)
                  trueup_includes_input_dt --###PATCH(8048)
         from  (  select   trueup_id,
                           from_pt_ind, /*###PATCH(475)*/
                           company_id,
                           oper_ind,
                           gl_month,
                           round(decode(sum(annual_amount_est),0,0,sum(amount_act)/sum(annual_amount_est)),18) ytd_percent, /*###PATCH(513) Rounding changed from 8 to 18*/
                           current_month_trueup,
                           out_month_trueup,
                           current_month_trueup_dt,
                           out_month_trueup_dt,
                           spread_remaining_estimate,
                           trueup_includes_input, --###PATCH(8048)
                           trueup_includes_input_dt --###PATCH(8048)
                  from  (  select   control.m_id m_id,
                                    trueup.trueup_id trueup_id,
                                    month_trueup.from_pt_ind from_pt_ind, /*###PATCH(475)*/
                                    control.company_id company_id,
                                    a_oper_ind(indx) oper_ind,
                                    ytd_gl_month.gl_month gl_month,
                                    control.annual_amount_est annual_amount_est,
                                    sum(decode(m_month_type.activity_type_id,2,m_item.amount_act,0)) amount_act,
                                    month_trueup.current_month_trueup current_month_trueup,
                                    month_trueup.out_month_trueup out_month_trueup,
                                    month_trueup.current_month_trueup_dt current_month_trueup_dt,
                                    month_trueup.out_month_trueup_dt out_month_trueup_dt,
                                    trueup.spread_remaining_estimate spread_remaining_estimate,
                                    trueup.trueup_includes_input, --###PATCH(8048)
                                    trueup.trueup_includes_input_dt --###PATCH(8048)
                           from  tax_accrual_control control,
                                 tax_accrual_m_item m_item,
                                 tax_accrual_m_master m_master,
                                 tax_accrual_trueup trueup,
                                 tax_accrual_gl_month_def m_gl_month,
                                 tax_accrual_gl_month_def ytd_gl_month,
                                 tax_accrual_month_type m_month_type,
                                 tax_accrual_month_type ytd_month_type,
                                 tax_accrual_month_type_trueup month_trueup
                           where control.m_id = m_item.m_id
                              and control.m_item_id = m_master.m_item_id
                              and m_master.m_type_id in (1,10)
                              and control.ta_version_id = a_ta_version_id
                              and m_gl_month.ta_version_id = a_ta_version_id
                              and ytd_gl_month.ta_version_id = a_ta_version_id
                              and m_gl_month.month_type_id = m_month_type.month_type_id
                              and ytd_gl_month.month_type_id = ytd_month_type.month_type_id
                              and trueup.trueup_id = a_trueup_id(indx)
                              and month_trueup.trueup_id = a_trueup_id(indx)
                              and month_trueup.month_type_id = ytd_month_type.month_type_id
                              and control.company_id = a_company_id(indx)
                              and a_m_id(indx) = -99
                              and decode(a_by_oper_ind(indx),1,control.oper_ind,-1) = decode(a_by_oper_ind(indx),1,a_oper_ind(indx),-1)
                              and m_item.gl_month = m_gl_month.gl_month
                              and m_item.gl_month <= ytd_gl_month.gl_month
                           group by control.m_id,
                                    trueup.trueup_id,
                                    month_trueup.from_pt_ind, /*###PATCH(475)*/
                                    control.company_id,
                                    a_oper_ind(indx),
                                    ytd_gl_month.gl_month,
                                    control.annual_amount_est,
                                    month_trueup.current_month_trueup,
                                    month_trueup.out_month_trueup,
                                    month_trueup.current_month_trueup_dt,
                                    month_trueup.out_month_trueup_dt,
                                    trueup.spread_remaining_estimate,
                                    trueup.trueup_includes_input, --###PATCH(8048)
                                    trueup.trueup_includes_input_dt --###PATCH(8048)
                        )
                  group by trueup_id,
                           from_pt_ind, /*###PATCH(475)*/
                           company_id,
                           oper_ind,
                           gl_month,
                           current_month_trueup,
                           out_month_trueup,
                           current_month_trueup_dt,
                           out_month_trueup_dt,
                           spread_remaining_estimate,
                           trueup_includes_input, --###PATCH(8048)
                           trueup_includes_input_dt --###PATCH(8048)
               );

      sql_msg := 'Inserting into temporary trueup table for items spread from other m items';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      -- Insert into the temporary table that stores actuals true up percentages for items being trued up based on M Items other than pretax income
      FORALL indx IN 1 .. num_rows
         insert into tax_accrual_trueup_tmp
         (  trueup_id,
            from_pt_ind, /*###PATCH(475)*/
            company_id,
            oper_ind,
            gl_month,
            ytd_percent,
            current_month_trueup,
            out_month_trueup,
            current_month_trueup_dt,
            out_month_trueup_dt,
            num_remaining_trueup_months,
            num_remaining_trueup_months_dt,
            spread_remaining_estimate,
            trueup_includes_input, --###PATCH(8048)
            trueup_includes_input_dt --###PATCH(8048)
         )
         select   trueup_id,
                  from_pt_ind, /*###PATCH(475)*/
                  company_id,
                  oper_ind,
                  gl_month,
                  ytd_percent,
                  current_month_trueup,
                  out_month_trueup,
                  current_month_trueup_dt,
                  out_month_trueup_dt,
                  sum(out_month_trueup) over (  partition by   trueup_id,
                                                               from_pt_ind, /*###PATCH(513)*/
                                                               company_id,
                                                               oper_ind
                                             ) -
                     sum(out_month_trueup) over (  partition by   trueup_id,
                                                                  from_pt_ind, /*###PATCH(513)*/
                                                                  company_id,
                                                                  oper_ind
                                                   order by trueup_id,
                                                            from_pt_ind, /*###PATCH(513)*/
                                                            company_id,
                                                            oper_ind,
                                                            gl_month
                                                ) num_remaining_trueup_months,
                  sum(out_month_trueup_dt) over (  partition by   trueup_id,
                                                                  from_pt_ind, /*###PATCH(513)*/
                                                                  company_id,
                                                                  oper_ind
                                                ) -
                     sum(out_month_trueup_dt) over (  partition by   trueup_id,
                                                                     from_pt_ind, /*###PATCH(513)*/
                                                                     company_id,
                                                                     oper_ind
                                                      order by trueup_id,
                                                               from_pt_ind, /*###PATCH(513)*/
                                                               company_id,
                                                               oper_ind,
                                                               gl_month
                                                   ) num_remaining_trueup_months_dt,
                  spread_remaining_estimate,
                  trueup_includes_input, --###PATCH(8048)
                  trueup_includes_input_dt --###PATCH(8048)
         from  (  select   trueup_id,
                           from_pt_ind, /*###PATCH(475)*/
                           company_id,
                           oper_ind,
                           gl_month,
                           round(decode(sum(annual_amount_est),0,0,sum(amount_act)/sum(annual_amount_est)),18) ytd_percent, /*###PATCH(513) Rounding changed from 8 to 18*/
                           current_month_trueup,
                           out_month_trueup,
                           current_month_trueup_dt,
                           out_month_trueup_dt,
                           spread_remaining_estimate,
                           trueup_includes_input, --###PATCH(8048)
                           trueup_includes_input_dt --###PATCH(8048)
                  from  (  select   control.m_id m_id,
                                    trueup.trueup_id trueup_id,
                                    month_trueup.from_pt_ind from_pt_ind, /*###PATCH(475)*/
                                    control.company_id company_id,
                                    a_oper_ind(indx) oper_ind,
                                    ytd_gl_month.gl_month gl_month,
                                    control.annual_amount_est annual_amount_est,
                                    sum(decode(m_month_type.activity_type_id,2,m_item.amount_act,0)) amount_act,
                                    month_trueup.current_month_trueup current_month_trueup,
                                    month_trueup.out_month_trueup out_month_trueup,
                                    month_trueup.current_month_trueup_dt current_month_trueup_dt,
                                    month_trueup.out_month_trueup_dt out_month_trueup_dt,
                                    trueup.spread_remaining_estimate spread_remaining_estimate,
                                    trueup.trueup_includes_input, --###PATCH(8048)
                                    trueup.trueup_includes_input_dt --###PATCH(8048)
                           from  tax_accrual_control control,
                                 tax_accrual_m_item m_item,
                                 tax_accrual_trueup trueup,
                                 tax_accrual_gl_month_def m_gl_month,
                                 tax_accrual_gl_month_def ytd_gl_month,
                                 tax_accrual_month_type m_month_type,
                                 tax_accrual_month_type ytd_month_type,
                                 tax_accrual_month_type_trueup month_trueup
                           where control.m_id = m_item.m_id
                              and control.m_item_id = trueup.m_id
                              and control.ta_version_id = a_ta_version_id
                              and m_gl_month.ta_version_id = a_ta_version_id
                              and ytd_gl_month.ta_version_id = a_ta_version_id
                              and m_gl_month.month_type_id = m_month_type.month_type_id
                              and ytd_gl_month.month_type_id = ytd_month_type.month_type_id
                              and trueup.trueup_id = a_trueup_id(indx)
                              and month_trueup.trueup_id = a_trueup_id(indx)
                              and month_trueup.month_type_id = ytd_month_type.month_type_id
                              and control.company_id = a_company_id(indx)
                              and a_m_id(indx) > 0
                              and decode(a_by_oper_ind(indx),1,control.oper_ind,-1) = decode(a_by_oper_ind(indx),1,a_oper_ind(indx),-1)
                              and m_item.gl_month = m_gl_month.gl_month
                              and m_item.gl_month <= ytd_gl_month.gl_month
                           group by control.m_id,
                                    trueup.trueup_id,
                                    month_trueup.from_pt_ind, /*###PATCH(475)*/
                                    control.company_id,
                                    a_oper_ind(indx),
                                    ytd_gl_month.gl_month,
                                    control.annual_amount_est,
                                    month_trueup.current_month_trueup,
                                    month_trueup.out_month_trueup,
                                    month_trueup.current_month_trueup_dt,
                                    month_trueup.out_month_trueup_dt,
                                    trueup.spread_remaining_estimate,
                                    trueup.trueup_includes_input, --###PATCH(8048)
                                    trueup.trueup_includes_input_dt --###PATCH(8048)
                           union all
                           select   a_m_id(indx) m_id,
                                    a_trueup_id(indx) trueup_id,
                                    month_trueup.from_pt_ind from_pt_ind,
                                    a_company_id(indx) company_id,
                                    a_oper_ind(indx) oper_ind,
                                    gl_month.gl_month gl_month,
                                    0 annual_amount_est,
                                    0 amount_act,
                                    month_trueup.current_month_trueup current_month_trueup,
                                    month_trueup.out_month_trueup out_month_trueup,
                                    month_trueup.current_month_trueup_dt current_month_trueup_dt,
                                    month_trueup.out_month_trueup_dt out_month_trueup_dt,
                                    trueup.spread_remaining_estimate spread_remaining_estimate,
                                    trueup.trueup_includes_input, --###PATCH(8048)
                                    trueup.trueup_includes_input_dt --###PATCH(8048)
                           from  tax_accrual_trueup trueup,
                                 tax_accrual_gl_month_def gl_month,
                                 tax_accrual_month_type_trueup month_trueup,
                                 tax_accrual_trueup
                           where gl_month.ta_version_id = a_ta_version_id
                              and gl_month.month_type_id = month_trueup.month_type_id
                              and trueup.trueup_id = a_trueup_id(indx)
                              and month_trueup.trueup_id = a_trueup_id(indx)
                              and a_m_id(indx) > 0
                              and not exists (  select 1
                                                from tax_accrual_control
                                                where ta_version_id = a_ta_version_id
                                                   and company_id = a_company_id(indx)
                                                   and m_item_id = a_m_id(indx)
                                                   and decode(a_by_oper_ind(indx),1,oper_ind,-1) = decode(a_by_oper_ind(indx),1,a_oper_ind(indx),-1)
                                             )
                        )
                  group by trueup_id,
                           from_pt_ind, /*###PATCH(475)*/
                           company_id,
                           oper_ind,
                           gl_month,
                           current_month_trueup,
                           out_month_trueup,
                           current_month_trueup_dt,
                           out_month_trueup_dt,
                           spread_remaining_estimate,
                           trueup_includes_input, --###PATCH(8048)
                           trueup_includes_input_dt --###PATCH(8048)
               );

      sql_msg := 'Inserting into temporary trueup table for "normal" trueup items';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      -- Insert into the temporary table that stores actuals true up percentages for items being trued up to the monthly or year-to-date estimate
      FORALL indx IN 1 .. num_rows
         insert into tax_accrual_trueup_tmp
         (  trueup_id,
            from_pt_ind, /*###PATCH(475)*/
            company_id,
            oper_ind,
            gl_month,
            ytd_percent,
            current_month_trueup,
            out_month_trueup,
            current_month_trueup_dt,
            out_month_trueup_dt,
            num_remaining_trueup_months,
            num_remaining_trueup_months_dt,
            spread_remaining_estimate,
            trueup_includes_input, --###PATCH(8048)
            trueup_includes_input_dt --###PATCH(8048)
         )
         select   a_trueup_id(indx) trueup_id,
                  month_trueup.from_pt_ind, /*###PATCH(475)*/
                  a_company_id(indx) company_id,
                  a_oper_ind(indx) oper_ind,
                  gl_month.gl_month,
                  decode(a_m_id(indx),-1,-999,-2,-888) ytd_percent, -- -999 means amount_act = amount_est, -888 means trueup to annual estimate
                  month_trueup.current_month_trueup current_month_trueup,
                  month_trueup.out_month_trueup out_month_trueup,
                  month_trueup.current_month_trueup_dt current_month_trueup_dt,
                  month_trueup.out_month_trueup_dt out_month_trueup_dt,
                  sum(month_trueup.out_month_trueup) over   (  partition by   month_trueup.trueup_id,
                                                                              month_trueup.from_pt_ind, /*###PATCH(513)*/
                                                                              a_company_id(indx),
                                                                              a_oper_ind(indx)
                                                            ) -
                     sum(month_trueup.out_month_trueup) over   (  partition by   month_trueup.trueup_id,
                                                                                 month_trueup.from_pt_ind, /*###PATCH(513)*/
                                                                                 a_company_id(indx),
                                                                                 a_oper_ind(indx)
                                                                  order by month_trueup.trueup_id,
                                                                           month_trueup.from_pt_ind, /*###PATCH(513)*/
                                                                           a_company_id(indx),
                                                                           a_oper_ind(indx),
                                                                           gl_month.gl_month
                                                               ) num_remaining_trueup_months,
                  sum(month_trueup.out_month_trueup_dt) over ( partition by   month_trueup.trueup_id,
                                                                              month_trueup.from_pt_ind, /*###PATCH(513)*/
                                                                              a_company_id(indx),
                                                                              a_oper_ind(indx)
                                                            ) -
                     sum(month_trueup.out_month_trueup_dt) over   (  partition by   month_trueup.trueup_id,
                                                                                    month_trueup.from_pt_ind, /*###PATCH(513)*/
                                                                                    a_company_id(indx),
                                                                                    a_oper_ind(indx)
                                                                     order by month_trueup.trueup_id,
                                                                              month_trueup.from_pt_ind, /*###PATCH(513)*/
                                                                              a_company_id(indx),
                                                                              a_oper_ind(indx),
                                                                              gl_month.gl_month
                                                                  ) num_remaining_trueup_months_dt,
                  trueup.spread_remaining_estimate,
                  trueup.trueup_includes_input, --###PATCH(8048)
                  trueup.trueup_includes_input_dt --###PATCH(8048)
         from  tax_accrual_gl_month_def gl_month,
               tax_accrual_month_type_trueup month_trueup,
               tax_accrual_trueup trueup
         where gl_month.ta_version_id = a_ta_version_id
            and gl_month.month_type_id = month_trueup.month_type_id
            and month_trueup.trueup_id = a_trueup_id(indx)
            and trueup.trueup_id = a_trueup_id(indx)
            and a_m_id(indx) in (-1,-2);

      return 1;
   EXCEPTION
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;
   end f_pop_trueup_table;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_calc_trueup: Calculates the monthly actuals trueup to an estimate for a set of schedule M or deferred  --
   --                tax records.                                                                              --
   --                                                                                                          --
   -- NOTE:  The array parameters passed in can really be thought of as an array of structures.  For example,  --
   --          a_m_id(1) and a_ta_norm_id(1) and a_gl_month(1) are the m_id and gl_month of the first record   --
   --                                                                                                          --
   -- Parameters: a_m_id:              An array containing the m_ids of the records to be trued up             --
   --             a_ta_norm_id:        An array containing the ta_norm_ids of the records to be trued up.  If  --
   --                                     we are truing up schedule M Items, this array only has one element   --
   --                                     and the element's value is -99.                                      --
   --             a_gl_month:          An array containing the months that the estimate will be spread over    --
   --             a_annual_amount_est: An array containing the annual estimate to be spread                    --
   --             a_amount_est:        An array containing the current month estimate                          --
   --             a_ytd_percent:       An array containing the total percentage of actuals that should be      --
   --                                     calculated through the current month.  A value of -999 in this field --
   --                                     means that actuals = estimate in this month.  A value of -888 means  --
   --                                     that we're just truing up to the estimate in the the current month   --
   --                                     (no percentage is applied)                                           --
   --             a_ytd_est:           An array containing the total estimate through the current month        --
   --             a_prior_ytd_act:     An array containing the actuals that have been spread through all months--
   --                                     prior to the current month.                                          --
   --             a_amount_calc:       An array containing the calculated deferred tax amount.                 --
   --             a_amount_manual:     An array containing the input deferred tax amount                       --
   --             a_amount_act:        An array containing the deferred tax current month amount               --
   --                                     (amount_calc + amount_manual)                                        --
   --             a_amount_adj:        An array containing the current month non-current schedule M amount.    --
   --                                     If we're spreading deferred taxes, this array contains one element   --
   --                                     and the value in that element is 0.                                  --
   --             a_beg_balance:       An array containing the current month deferred tax beginning balance    --
   --             a_end_balance:       An array containing the current month deferred tax ending balance       --
   --             a_trueup_option:     An array containing a 1/0 indicator that specifies whether or not we're --
   --                                     truing up to the estimate in the current month.                      --
   --             a_do_not_update:     An array containing a 1/0 indicator that specifies whether or not we're --
   --                                     truing up to the estimate in the current month.  A value of 1 in this--
   --                                     parameter also indicates that if a_update = 1, then we want to       --
   --                                     update the database even though the amount_calc value has not        --
   --                                     changed.                                                             --
   --             a_ytd_include:       1/0 indicator specifying whether or not this month should be included   --
   --                                     in the calculation of year-to-date actuals.  1 means exclude from    --
   --                                     year-to-date actuals.   0 means include.
   --             a_trueup_includes_input:   Indicator that specifies whether prior manual input amounts should--
   --                                     be taken into account when truing up to the estimate.                --
   --             a_num_remaining_trueup_months:   Number of months remaining that will true up to the         --
   --                                     estimate.  This is used to spread the remaining estimate over the    --
   --                                     remaining months.                                                    --
   --             a_spread_remaining_estimate:  1/0 indicator specifying whether or not the trueup option      --
   --                                     calls for us to spread the remaining estimate over the remaining     --
   --                                     months.                                                              --
   --             a_update:            1/0 indicator specifying if we want to update the calculated result in  --
   --                                     the database.  Typically, we update when we're processing,           --
   --                                     but do not update when we're spreading in the M Item window.         --
   --             a_msg:               A string passed back to the calling function containing any error       --
   --                                     messages.                                                            --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_calc_trueup  (  a_m_id                        in id_t,
                              a_ta_norm_id                  in id_t,
                              a_gl_month                    in gl_month_t,
                              a_annual_amount_est           in currency_t,
                              a_amount_est                  in currency_t,
                              a_ytd_percent                 in spread_pct_t, /*###PATCH(513) Data Type Changed from percent_t*/
                              a_ytd_est                     in currency_t,
                              a_prior_ytd_act               in currency_t,
                              a_amount_calc                 in out currency_t,
                              a_amount_manual               in out currency_t,
                              a_amount_act                  in out currency_t,
                              a_amount_adj                  in out currency_t,
                              a_beg_balance                 in out currency_t,
                              a_end_balance                 in out currency_t,
                              a_trueup_option               in indicator_t,
                              a_do_not_update               in indicator_t,
                              a_ytd_include                 in indicator_t,
							  a_trueup_includes_input       in indicator_t, --###PATCH(8048)
                              a_num_remaining_trueup_months in long_t,
                              a_spread_remaining_estimate   in long_t,
                              a_update                      in number,
                              a_msg                         in out varchar2
                           ) RETURN number

   is
      amount_act_orig      currency_type;
      amount_calc_orig     currency_type;
      beg_balance_orig     currency_type;
      end_balance_orig     currency_type;
      m_id_update          id_t;
      ta_norm_id_update    id_t;
      gl_month_update      gl_month_t;
      amount_calc_update   currency_t;
      amount_act_update    currency_t;
      amount_adj_update    currency_t;
      amount_manual_update currency_t;
      beg_balance_update   currency_t;
      end_balance_update   currency_t;
      num_rows             number(22);
      prev_m_id            tax_accrual_m_item.m_id%TYPE := 0;
      ta_norm_id           tax_accrual_norm_schema.ta_norm_id%TYPE := 0;
      prev_ta_norm_id      tax_accrual_norm_schema.ta_norm_id%TYPE := 0;
      ytd_calc_roll        currency_type;
      balance_roll         currency_type;
      amount_adj           currency_type;
      update_count         number(22) := 0;
      sql_msg              varchar2(254);
      is_dt_trueup         BOOLEAN;

      function_name     varchar2(254) := 'f_calc_trueup';
   begin

      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'Begin f_calc_trueup');
      num_rows := a_m_id.COUNT;

      -- We'll get an error in our check of a_ta_norm_id(1) if we have no rows in the array.  That could happen if we process
      --    a company that has no deferred taxes.
      if num_rows = 0 then
         return 1;
      end if;

      -- The actuals true-up functionality is very similar between deferreds and schedule Ms, but there are some minor differences, so
      --    we need to determine if we are truing up deferred tax records or schedule M amounts.  If we are truing up deferred tax records,
      --    then the a_ta_norm_id array parameter will only have one element and the value in that element will be -99.
      if a_ta_norm_id(1) = -99 then
         is_dt_trueup := FALSE;
         ta_norm_id := -99;
         prev_ta_norm_id := -99;
      else
         is_dt_trueup := TRUE;
      end if;

      for i in 1 .. num_rows loop
         amount_calc_orig := a_amount_calc(i);
         amount_act_orig := a_amount_act(i);
         beg_balance_orig := a_beg_balance(i);
         end_balance_orig := a_end_balance(i);

         if is_dt_trueup then
            ta_norm_id := a_ta_norm_id(i);
--###PATCH(503)            prev_ta_norm_id := a_ta_norm_id(i);
            amount_adj := 0;
         else
            amount_adj := a_amount_adj(i);
         end if;

         -- If this is the first month for a given m_id and ta_norm_id (deftax only) then we need to get the prior ytd amount
         --    from the SQL statement.  Otherwise, we calculate the prior ytd amount in this function.
         if prev_m_id <> a_m_id(i) or prev_ta_norm_id <> ta_norm_id then
            ytd_calc_roll := a_prior_ytd_act(i);
            balance_roll := a_beg_balance(i);
         end if;

         prev_m_id := a_m_id(i);

         if is_dt_trueup then
            prev_ta_norm_id := a_ta_norm_id(i);
         end if;

         tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'trueup_option = ' || a_trueup_option(i));
         tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'do_not_update = ' || a_do_not_update(i));
         tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'a_ytd_percent = ' || a_ytd_percent(i));
         tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'a_spread_remaining_estimate = ' || a_spread_remaining_estimate(i));
         tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'num trueup_months = ' || a_num_remaining_trueup_months(i));

         if a_trueup_option(i) = 1 and a_do_not_update(i) = 0 then -- don't do anything if we're not truing up in this month
            if a_ytd_percent(i) = -999 then -- -999 means we're just setting actuals equal to current month estimates
               a_amount_calc(i) := a_amount_est(i);
            elsif a_ytd_percent(i) = -888 then -- -888 means True-up to the annual estimate
               -- If we're spreading the remaining estimate over the remaining months, then divide the remaining estimate by the remaining months
               if a_spread_remaining_estimate(i) = 1 then
                  if a_num_remaining_trueup_months(i) = 0 then
                     a_amount_calc(i) := 0;
                  else
                     tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'gl_month = ' || a_gl_month(i));
                     tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'ytd_calc_roll = ' || ytd_calc_roll);
                     tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'num trueup_months = ' || a_num_remaining_trueup_months(i));
                     a_amount_calc(i) := round((a_annual_amount_est(i) - ytd_calc_roll)/a_num_remaining_trueup_months(i),2);
                  end if;
               else
                  a_amount_calc(i) := a_ytd_est(i) - ytd_calc_roll;
               end if;
            else --True up based on a percentage of ptbi or another M Item
               a_amount_calc(i) := round(a_annual_amount_est(i) * a_ytd_percent(i),2) - ytd_calc_roll;
            end if; --a_ytd_percent(i) = -99
		elsif a_trueup_option(i) = 2 and a_do_not_update(i) = 0 then --###PATCH(8048) 2 indicates that this is a manual item that is not trued up in the current month, so zero it out
            a_amount_calc(i) := 0; --###PATCH(8048)
         end if; --a_trueup_option(i) = 1

         -- If this item is a current year item (a_ytd_include = 0), then add it to the rolling year-to-date amount for use in the next loop.
         if a_ytd_include(i) = 0 then
		   --###PATCH(8048) Begin Change
            if a_trueup_includes_input(i) = 1 then
               ytd_calc_roll := ytd_calc_roll + a_amount_calc(i) + a_amount_manual(i);
            else
               ytd_calc_roll := ytd_calc_roll + a_amount_calc(i);
			end if;
            --###PATCH(8048) End Change
         end if;

         a_amount_act(i) := a_amount_calc(i) + a_amount_manual(i);
         a_beg_balance(i) := balance_roll;
         balance_roll := balance_roll + a_amount_act(i) + amount_adj;
         a_end_balance(i) := balance_roll;

         -- We only want to update if the calculated amounts are different than the original amounts to save processing time, however
         -- when a_do_not_update = 1, that means we don't want to recalc the M Item because it's being pulled from another case, but we
         -- do want to update the RTP numbers to the database.
         if a_update = 1 and (a_do_not_update(i) = 1 or a_amount_calc(i) <> amount_calc_orig or a_beg_balance(i) <> beg_balance_orig
               or a_end_balance(i) <> end_balance_orig or a_amount_act(i) <> amount_act_orig) then
            update_count := update_count + 1;
            amount_calc_update(update_count) := a_amount_calc(i);
            amount_act_update(update_count) := a_amount_act(i);
            amount_adj_update(update_count) := amount_adj;
            amount_manual_update(update_count) := a_amount_manual(i);
            beg_balance_update(update_count) := a_beg_balance(i);
            end_balance_update(update_count) := a_end_balance(i);
            m_id_update(update_count) := a_m_id(i);
            gl_month_update(update_count) := a_gl_month(i);

            if is_dt_trueup then
               ta_norm_id_update(update_count) := a_ta_norm_id(i);
            end if;
         end if;

      end loop; --i in 1 .. num_rows loop

      if a_update = 1 then
         if is_dt_trueup then

            sql_msg := 'Updating Deferred Tax actuals with new amounts - Estimates';
            tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);
            FORALL indx IN 1 .. update_count
            update tax_accrual_def_tax
            set   amount_calc = amount_calc_update(indx),
                  amount_act = amount_act_update(indx),
                  amount_manual = amount_manual_update(indx),
                  beg_balance = beg_balance_update(indx),
                  end_balance = end_balance_update(indx)
            where m_id = m_id_update(indx)
               and ta_norm_id = ta_norm_id_update(indx)
               and gl_month = gl_month_update(indx);

            tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'End ' || sql_msg);

         else -- is_dt_trueup

            sql_msg := 'Updating M Item actuals with new amounts';
            tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);
            FORALL indx IN 1 .. update_count
            update tax_accrual_m_item
            set   amount_calc = amount_calc_update(indx),
                  amount_act = amount_act_update(indx),
                  amount_manual = amount_manual_update(indx),
                  amount_adj = amount_adj_update(indx),
                  beg_balance = beg_balance_update(indx),
                  end_balance = end_balance_update(indx)
            where m_id = m_id_update(indx)
               and gl_month = gl_month_update(indx);

            tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'End ' || sql_msg);

         end if; --is_dt_trueup

      end if; --a_update = 1

      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'End f_calc_trueup');

      return 1;

   EXCEPTION
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;

   end f_calc_trueup;

   --**************************************************************************************
   --
   -- f_calc_trueup_ms
   --
   --**************************************************************************************
   FUNCTION f_calc_trueup_ms  (  a_m_id                        in id_t,
                                 a_gl_month                    in gl_month_t,
                                 a_annual_amount_est           in currency_t,
                                 a_amount_est                  in currency_t,
                                 a_ytd_percent                 in spread_pct_t, /*###PATCH(513) Data Type Changed from percent_t*/
                                 a_ytd_est                     in currency_t,
                                 a_prior_ytd_act               in currency_t,
                                 a_amount_calc                 in out currency_t,
                                 a_amount_manual               in out currency_t,
                                 a_amount_act                  in out currency_t,
                                 a_amount_adj                  in out currency_t,
                                 a_beg_balance                 in out currency_t,
                                 a_end_balance                 in out currency_t,
                                 a_trueup_option               in indicator_t,
                                 a_do_not_update               in indicator_t,
                                 a_ytd_include                 in indicator_t,
                                 a_trueup_includes_input       in indicator_t, --###PATCH(8048)
								 a_num_remaining_trueup_months in long_t,
                                 a_spread_remaining_estimate   in long_t,
                                 a_update                      in number,
                                 a_msg                         in out varchar2
                              ) RETURN number

   is
      ta_norm_id  id_t;
      rtn         number(22);

   begin


      rtn := 1;
      ta_norm_id(1) := -99;
      rtn := f_calc_trueup (  a_m_id,ta_norm_id, a_gl_month,a_annual_amount_est, a_amount_est, a_ytd_percent,
                              a_ytd_est, a_prior_ytd_act, a_amount_calc, a_amount_manual, a_amount_act,
                              a_amount_adj, a_beg_balance, a_end_balance, a_trueup_option, a_do_not_update,
                              a_ytd_include, 
                              a_trueup_includes_input, --###PATCH(8048) 
							  a_num_remaining_trueup_months, a_spread_remaining_estimate,  a_update, a_msg
                           );

      return rtn;

   EXCEPTION
      when others then
         a_msg := 'Error truing up Ms to estimates: ' || SQLERRM;
         return -1;

   end f_calc_trueup_ms;

   --**************************************************************************************
   --
   -- f_calc_trueup_dt
   --
   --**************************************************************************************
   FUNCTION f_calc_trueup_dt  (  a_m_id                        in id_t,
                                 a_ta_norm_id                  in id_t,
                                 a_gl_month                    in gl_month_t,
                                 a_annual_amount_est           in currency_t,
                                 a_amount_est                  in currency_t,
                                 a_ytd_percent                 in spread_pct_t, /*###PATCH(513) Data Type Changed from percent_t*/
                                 a_ytd_est                     in currency_t,
                                 a_prior_ytd_act               in currency_t,
                                 a_amount_calc                 in out currency_t,
                                 a_amount_manual               in out currency_t,
                                 a_amount_act                  in out currency_t,
                                 a_beg_balance                 in out currency_t,
                                 a_end_balance                 in out currency_t,
                                 a_trueup_option               in indicator_t,
                                 a_do_not_update               in indicator_t,
                                 a_ytd_include                 in indicator_t,
								 a_trueup_includes_input       in indicator_t, --###PATCH(8048)
                                 a_num_remaining_trueup_months in long_t,
                                 a_spread_remaining_estimate   in long_t,
                                 a_update                      in number,
                                 a_msg                         in out varchar2
                              ) RETURN number

   is
      amount_adj  currency_t;
      rtn         number(22);

   begin

      rtn := 1;
      amount_adj(1) := 0;

      rtn := f_calc_trueup (  a_m_id,a_ta_norm_id, a_gl_month,a_annual_amount_est, a_amount_est, a_ytd_percent,
                              a_ytd_est, a_prior_ytd_act, a_amount_calc, a_amount_manual, a_amount_act,
                              amount_adj, a_beg_balance, a_end_balance, a_trueup_option, a_do_not_update,
                              a_ytd_include,  a_trueup_includes_input, --###PATCH(8048)
							  a_num_remaining_trueup_months, a_spread_remaining_estimate, a_update, a_msg
                           );

      return rtn;

   EXCEPTION
      when others then
         a_msg := 'Error truing up Deferred Taxes to estimates: ' || SQLERRM;
         return -1;

   end f_calc_trueup_dt;

end tax_accrual_spread_pkg;
--End of Package Body
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (11863, 0, 2018, 1, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2018.1.0.0_TAX_ACCRUAL_SPREAD_PKG.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
