create or replace package pkg_lease_var_payments is

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_VAR_PAYMENTS
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version    Date       Revised By     Reason for Change
   || ---------- ---------- -------------- ----------------------------------------
   || 2017.1.0.0 03/16/2017 D. Haupt        Original Version
   || 2017.1.0.0 03/20/2017 Anand R         Add month end calculations for variable payments
   || 2017.1.0.0 03/22/2017 D. Haupt        PP-47282: added the rest of schedule VP calc functions
   || 2017.1.0.0 03/31/2017 Anand R        PP-47139: Add variable payments to month end
   || 2017.1.0.0 04/03/2017 Anand R        PP-47139: calculate asset npv to determine gain loss
   || 2017.1.0.0 04/10/2017 D. Haupt       Change to calc index components like var components
   || 2017.1.0.0 05/22/2017 D. Haupt        Added function to calc historic components
   ||============================================================================
   */

  -- Function to calculate amount for inclusion in initial measure
  function F_CALC_INITIAL_AMT return varchar2;

  -- Function to load payment terms staging table for use in calcing initial amount
  function F_LOAD_ILR_PAYMENT_TERM_STG return varchar2;

  -- Function to calc the value of an index/rate formula component for a given variable name and month
  function F_CALC_VAR_PAYMENT_INITIAL_AMT(A_VARIABLE_PAYMENT_ID number,
                                          A_ILR_ID              number,
                                          A_REVISION            number) return number;

  -- Function to calculate an index/rate formula component value
  FUNCTION F_CALC_INDEX(A_COMPONENT_ID           number,
                        A_ILR_ID                 number,
                        A_REVISION               number,
                        A_INITIAL_AMT_FLAG       number,
                        A_MONTH                  date ) return number;

  -- Function to calculate a variable component
  function F_CALC_VAR_COMPONENT(A_COMPONENT_ID number,
--                                A_ILR_ID       number,
                                A_ASSET_ID     number,
                                A_MONTH        date) return number;

   -- Function to calculate the value of a historic component
   function F_CALC_HISTORIC(A_COMPONENT_ID          number,
                            A_ID                    number,
                            A_REVISION              number,
                            A_ASSET_LEVEL_FLAG      number,
                            A_SET_OF_BOOKS_ID       number,
                            A_MONTH	                date) return number;

   -- Function to calculate a single variable payment for an ILR
   function F_CALC_VAR_PAYMENT(A_ILR_ID               number,
                               A_REVISION             number,
                               A_VARIABLE_PAYMENT_ID  number,
                               A_BUCKET_NUMBER        number) return varchar2;

   -- Function to calculate all variable payments for an ILR in appropriate order
   function F_CALC_ILR_VAR_PAYMENTS(A_ILR_ID     number,
                                    A_REVISION   number,
                                    A_MONTH      date) return varchar2;

   -- Function to calculate variable payments for all ILR/revision combos currently in the schedule build staging tables

   function F_CALC_ALL_VAR_PAYMENTS(A_MONTH   date) return varchar2;

   -- Function to calculate variable payments for each asset bucket
   function F_CALC_ASSET_BUCKET( A_ILR_ID number, A_ASSET_ID number, A_REVISION number, A_SET_OF_BOOKS_ID number, A_PAYMENT_TERM_ID number, A_BUCKET_NUMBER number, A_MONTH date, A_PROCESS_TYPE varchar2) return number;

   -- Function to rollup all buckets from the assets to ILR level.
   function F_ROLLUP_ASSETS_BUCKETS_TO_ILR(A_COMPANY_ID in number, A_MONTH in date, A_CALC_TYPE varchar2) return varchar2;

   -- Function to calculate variable payments for all assets of a company during month end.
   function F_CALC_ASSET_BUCKETS_MONTH_END(A_COMPANY_ID in number, A_MONTH in date, CALC_TYPE varchar2) return varchar2;

   -- Function to calculate paid amount for a given asset and month
   function F_CALC_ASSET_PAID_AMOUNT(A_ASSET_ID       number,
                                     A_REVISION       number,
                                     A_MONTH          date,
                                     A_BUCKET_NUMBER  number,
                                     A_SET_OF_BOOKS_ID number) return number;

   -- Function to update the ILR and asset schedule tables during calculation of variable payments
   function F_UPDATE_SCHEDULE_TABLES(A_ILR_ID            number,
                                     A_MONTH             date,
                                     A_REVISION          number) return varchar2;

   -- Function to return if a given month is a payment month for a given ILR and revision
   function F_IS_PAYMENT_MONTH(A_ILR_ID     number,
                               A_REVISION   number,
                               A_MONTH      date) return boolean;

   -- Function to return the first payment month for a given ILR and revision
   function F_GET_FIRST_PAYMENT_TERM_MONTH(A_ILR_ID     number,
                                           A_REVISION   number) return date;

	function F_IS_PAYMENT_MONTH_RTN_NUMBER(A_ILR_ID     number,
                               A_REVISION   number,
                               A_MONTH      date) return INTEGER;

  FUNCTION F_GET_ESCALATION_PCT (A_FORMULA_COMPONENT_ID IN NUMBER, A_BASE_DATE IN DATE, A_ESCALATION_DATE IN DATE) RETURN NUMBER;

end PKG_LEASE_VAR_PAYMENTS;
/

create or replace package body pkg_lease_var_payments as
/*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_VAR_PAYMENTS
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version    Date       Revised By     Reason for Change
   || ---------- ---------- -------------- ----------------------------------------
   || 2017.1.0.0 03/03/2017 D. Haupt         Original Version
   || 2017.1.0.0 03/20/2017 Anand R         Add month end calculations for variable payments
   || 2017.1.0.0 03/22/2017 D. Haupt        PP-47282: added the rest of VP calc functions
   || 2017.1.0.0 03/31/2017 Anand R        PP-47139: Add variable payments to month end
   || 2017.1.0.0 04/03/2017 Anand R        PP-47139: calculate asset npv to determine gain loss
   || 2017.1.0.0 04/10/2017 D. Haupt       Change to calc index components like var components
   || 2017.1.0.0 05/22/2017 D. Haupt        Added function to calc historic components
   ||============================================================================
   */

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

  -- Function and procedure implementations
  FUNCTION F_GET_ESCALATION_PCT(A_FORMULA_COMPONENT_ID IN NUMBER, A_BASE_DATE IN DATE, A_ESCALATION_DATE IN DATE)
  RETURN NUMBER
  IS
  L_STATUS varchar2(2000);
  L_ESCALATION_PCT NUMBER;
  BEGIN

  SELECT DISTINCT
  (Last_Value(amount) OVER(PARTITION BY val.formula_component_id ORDER BY effective_date ASC RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
  -
  First_Value(amount) OVER(PARTITION BY val.formula_component_id ORDER BY effective_date ASC RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING))/
  First_Value(amount) OVER(PARTITION BY val.formula_component_id ORDER BY effective_date ASC RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
  INTO L_ESCALATION_PCT
  FROM ls_index_component_values val
  WHERE formula_component_id = A_FORMULA_COMPONENT_ID
  AND effective_date IN (A_BASE_DATE,A_ESCALATION_DATE);

  RETURN L_ESCALATION_PCT;

  EXCEPTION
    WHEN No_Data_Found THEN
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: No Escalation Percent Found');
    WHEN OTHERS THEN
      PKG_PP_LOG.P_WRITE_MESSAGE('Error Getting Escalation Percent: ' || sqlerrm);
  END F_GET_ESCALATION_PCT;

  function F_CALC_INITIAL_AMT return varchar2 is

     L_MSG                    varchar2(2000);
     L_SQLS                   varchar2(2000);
     L_BUCKET_NAME            varchar2(254);
     L_FORMULA                varchar2(4000);
     L_FORMULA_RESULT         number(22,8);
     L_AMOUNT                 number(22,2);

     L_PAYMENT_TERM_ID        number(22,0);
     L_RENT_TYPE              varchar2(254);
     L_BUCKET_NUMBER          number(22,0);
     L_VARIABLE_PAYMENT_ID    number(22,0);

     cursor STAGED_ILRS is
            SELECT DISTINCT ILR_ID, REVISION
            FROM LS_ILR_STG;

     /*cursor VAR_PAYMENTS is
            SELECT PAYMENT_TERM_ID, RENT_TYPE, BUCKET_NUMBER, VARIABLE_PAYMENT_ID
            FROM LS_ILR_PAYMENT_TERM_VAR_PAYMNT l
            WHERE l.ILR_ID = A_ILR_ID
            AND l.REVISION = A_REVISION
            AND l.INCL_IN_INITIAL_MEASURE = 1;        */

  begin
    PKG_PP_LOG.P_WRITE_MESSAGE('Calculating initial amount');

    FOR ILR IN STAGED_ILRS
      LOOP
        L_MSG := '  Determining initial measure info for ILR: ' || to_char(ILR.ILR_ID);
        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

        FOR BUCKET IN (SELECT PAYMENT_TERM_ID, RENT_TYPE, BUCKET_NUMBER, VARIABLE_PAYMENT_ID, SET_OF_BOOKS_ID
                      FROM LS_ILR_PAYMENT_TERM_VAR_PAYMNT l
                      WHERE l.ILR_ID = ILR.ILR_ID
                      AND l.REVISION = ILR.REVISION
                      AND l.INCL_IN_INITIAL_MEASURE = 1)
          LOOP
            L_MSG := '  Checking bucket: ' || BUCKET.RENT_TYPE || ' ' || to_char(BUCKET.BUCKET_NUMBER);
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

            IF BUCKET.VARIABLE_PAYMENT_ID IS NULL THEN
              -- if amount, just update LS_ILR_PAYMENT_TERM_STG (increase initial  decrease contingent)
              PKG_PP_LOG.P_WRITE_MESSAGE('   Determined this bucket is an amount');

              --figure out the right bucket
              IF BUCKET.RENT_TYPE = 'Executory' THEN
                L_BUCKET_NAME := CONCAT('E_BUCKET_', TO_CHAR(BUCKET.BUCKET_NUMBER));
              ELSE
                L_BUCKET_NAME := CONCAT('C_BUCKET_', TO_CHAR(BUCKET.BUCKET_NUMBER));
              END IF;

              --get the actual amount
              L_SQLS := 'SELECT '   ||
                        L_BUCKET_NAME   ||
                        ' from LS_ILR_PAYMENT_TERM_STG
                         WHERE ILR_ID = :ilr_id
                         AND REVISION = :revision
                         AND PAYMENT_TERM_ID = :payment_term_id
						 AND SET_OF_BOOKS_ID = :set_of_books_id';
              --PKG_PP_LOG.P_WRITE_MESSAGE(L_SQLS);

              EXECUTE IMMEDIATE L_SQLS
                INTO L_AMOUNT
                USING ILR.ILR_ID, ILR.REVISION, BUCKET.PAYMENT_TERM_ID, BUCKET.SET_OF_BOOKS_ID;

              --reduce the amount in the corresponding bucket to 0 so it isn't double counted by the rest of the calc
              --the end of the loop will add the amount to the fixed amount
              L_MSG := '   Reducing bucket amount in staging table by: ' || to_char(L_AMOUNT);
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

              L_SQLS := 'UPDATE LS_ILR_PAYMENT_TERM_STG
                        SET ' || L_BUCKET_NAME || ' = 0' ||
                        ' WHERE ILR_ID = :ilr_id
                         AND REVISION = :revision
                         AND PAYMENT_TERM_ID = :payment_term_id
						 AND SET_OF_BOOKS_ID = :set_of_books_id';

              EXECUTE IMMEDIATE L_SQLS
                USING ILR.ILR_ID, ILR.REVISION, BUCKET.PAYMENT_TERM_ID, BUCKET.SET_OF_BOOKS_ID;

            ELSE
              -- if var pmt, calc formula
              --   TODO: check each for var component  error if one is found
              PKG_PP_LOG.P_WRITE_MESSAGE('  Determined this bucket is calculated by a variable payment');

              --call the function to execute the formula  get the value
                L_AMOUNT := F_CALC_VAR_PAYMENT_INITIAL_AMT(BUCKET.VARIABLE_PAYMENT_ID, ILR.ILR_ID, ILR.REVISION);


              --PKG_PP_LOG.P_WRITE_MESSAGE('  Updating ls_ilr_payment_term_var_paymnt with amount ' || To_Char(l_amount));

               --update the correct column to record this initial value for use in month end calculation
               UPDATE LS_ILR_PAYMENT_TERM_VAR_PAYMNT
               SET INITIAL_VAR_PAYMENT_VALUE = L_AMOUNT
               WHERE VARIABLE_PAYMENT_ID = BUCKET.VARIABLE_PAYMENT_ID
			   AND SET_OF_BOOKS_ID = BUCKET.SET_OF_BOOKS_ID;


              --PKG_PP_LOG.P_WRITE_MESSAGE('  Done updating ls_ilr_payment_term_var_paymnt');


            END IF;

            --if the amount is non-zero, adjust the correct columns in LS_ILR_PAYMENT_TERM_STG
            --increase the fixed amount and decrease the contingent amount by the same
            IF L_AMOUNT <> 0 AND L_AMOUNT IS NOT NULL THEN
               --PKG_PP_LOG.P_WRITE_MESSAGE('  Updating LS_ILR_PAYMENT_TERM_STG with amount: ' || to_char(L_AMOUNT));

               --update the paid amount
               L_SQLS := 'UPDATE LS_ILR_PAYMENT_TERM_STG ' ||
                         'SET PAID_AMOUNT = (PAID_AMOUNT + :include_in_initial) ' ||
                         'WHERE ILR_ID = :ilr_id
                         AND REVISION = :revision
                         AND PAYMENT_TERM_ID = :payment_term_id
						 AND SET_OF_BOOKS_ID = :set_of_books_id';

               EXECUTE IMMEDIATE L_SQLS
               USING L_AMOUNT, ILR.ILR_ID, ILR.REVISION, BUCKET.PAYMENT_TERM_ID, BUCKET.SET_OF_BOOKS_ID;

            END IF;

        END LOOP;

    END LOOP;

    return 'OK';
  exception
      when others then
         L_MSG := SUBSTR('ERROR calculating initial amount: ' || sqlerrm, 1, 2000);
         return L_MSG;
  end F_CALC_INITIAL_AMT;

  --Function to load ILR payment terms staging table
  function F_LOAD_ILR_PAYMENT_TERM_STG return varchar2 is
    L_MSG varchar2(2000);

    cursor ILR_IDS is
           select distinct ilr_id
           from ls_ilr_stg;

  begin
    PKG_PP_LOG.P_WRITE_MESSAGE('  Clearing out the ILR payment terms staging table');
    DELETE FROM LS_ILR_PAYMENT_TERM_STG;

    PKG_PP_LOG.P_WRITE_MESSAGE('  Beginning load of the ILR payment terms staging table');

    FOR ILR_ID IN ILR_IDS
      LOOP
        L_MSG := '  Inserting for ILR with ID = ' || to_char(ILR_ID.ILR_ID);
        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

        insert into ls_ilr_payment_term_stg
          (ILR_ID, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE, PAYMENT_FREQ_ID, NUMBER_OF_TERMS,
          EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT, CURRENCY_TYPE_ID,
          C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6, C_BUCKET_7, C_BUCKET_8, C_BUCKET_9,
          C_BUCKET_10,
          E_BUCKET_1, E_BUCKET_2, E_BUCKET_3, E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9,
          E_BUCKET_10,
          REVISION, MAKE_II_PAYMENT, INTERIM_INTEREST_BEGIN_DATE, SET_OF_BOOKS_ID,
          ESCALATION, ESCALATION_FREQ_ID, ESCALATION_PCT)
        SELECT
          L.ILR_ID, L.PAYMENT_TERM_ID, L.PAYMENT_TERM_TYPE_ID, L.PAYMENT_TERM_DATE, L.PAYMENT_FREQ_ID, L.NUMBER_OF_TERMS,
          L.EST_EXECUTORY_COST, L.PAID_AMOUNT, L.CONTINGENT_AMOUNT, L.CURRENCY_TYPE_ID,
          L.C_BUCKET_1, L.C_BUCKET_2, L.C_BUCKET_3, L.C_BUCKET_4, L.C_BUCKET_5, L.C_BUCKET_6, L.C_BUCKET_7, L.C_BUCKET_8, L.C_BUCKET_9,
          L.C_BUCKET_10,
          L.E_BUCKET_1, L.E_BUCKET_2, L.E_BUCKET_3, L.E_BUCKET_4, L.E_BUCKET_5, L.E_BUCKET_6, L.E_BUCKET_7, L.E_BUCKET_8, L.E_BUCKET_9,
          L.E_BUCKET_10,
          L.REVISION, L.MAKE_II_PAYMENT, L.INTERIM_INTEREST_BEGIN_DATE, CSOB.SET_OF_BOOKS_ID,
          ESCALATION, ESCALATION_FREQ_ID, ESCALATION_PCT
        FROM LS_ILR_PAYMENT_TERM L
		INNER JOIN ls_ilr_stg stg
			ON stg.ilr_id = l.ilr_id
			AND stg.revision = l.revision
      INNER JOIN ls_Ilr ilr
      ON stg.ilr_id = ilr.ilr_id
		INNER JOIN company_set_of_books csob
			ON csob.company_id = ilr.company_id
      AND stg.set_of_books_id = csob.set_of_books_id
        WHERE csob.include_indicator = 1
		AND L.ILR_ID = ILR_ID.ILR_ID;

        L_MSG := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

    END LOOP;

    return 'OK';

  exception
    when others then
      L_MSG := SUBSTR('ERROR loading LS_ILR_PAYMENT_TERM_STG: ' || sqlerrm, 1, 2000);
         return L_MSG;
  end F_LOAD_ILR_PAYMENT_TERM_STG;

  function F_CALC_VAR_PAYMENT_INITIAL_AMT(A_VARIABLE_PAYMENT_ID number,
                                          A_ILR_ID              number,
                                          A_REVISION            number) return number is

           L_MSG varchar2(2000);
           L_SQLS varchar2(2000);
           L_FORMULA varchar2(4000);
           L_MONTH date;
           L_RESULT number(22,8);

  begin
    PKG_PP_LOG.P_WRITE_MESSAGE('  Beginning calculation of variable payment initial amount');

    SELECT PAYMENT_FORMULA_DB
      INTO L_FORMULA
      FROM LS_VARIABLE_PAYMENT
      WHERE VARIABLE_PAYMENT_ID = A_VARIABLE_PAYMENT_ID;

      --use the first payment term month when calculating the initial amount
      L_MONTH := F_GET_FIRST_PAYMENT_TERM_MONTH(A_ILR_ID, A_REVISION);

      L_MSG := '   Executing formula: ' || L_FORMULA;
      --PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      --This formula can only contain formula components  constants if included in initial amount
      --So a simple select will work
      L_SQLS := 'SELECT nvl(' || L_FORMULA || ',0)' ||
                ' FROM (
                       SELECT to_date('|| to_char(L_MONTH,'yyyymm') || ',''yyyymm'') as month from dual
                   ) a,
                   ( select 1 as initial_amt_flag from dual) b,
                   ls_ilr_options lio
                   where lio.ilr_id = :ilr_id
                   and lio.revision = :revision';

      EXECUTE IMMEDIATE L_SQLS
       INTO L_RESULT
       USING A_ILR_ID, A_REVISION;

       L_MSG := '   Got the following initial amount result: ' || to_char(L_RESULT);
       --PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

       return L_RESULT;

  exception
    when others then
      L_MSG := SUBSTR('ERROR calculating variable payment initial amount: ' || sqlerrm, 1, 2000);
         return L_MSG;
  end F_CALC_VAR_PAYMENT_INITIAL_AMT;

  FUNCTION F_CALC_INDEX(A_COMPONENT_ID            number,
                        A_ILR_ID                  number,
                        A_REVISION                number,
                        A_INITIAL_AMT_FLAG        number,
                        A_MONTH                   date ) return number is

    L_COMPONENT_ID      number(22,0);
    L_SQLS              varchar2(2000);
    L_MSG               varchar2(2000);
    L_COMPONENT_AMOUNT  number(22,8);
    L_COMPONENT_TYPE    number(1,0);

  begin
    --check if we're calculating an initial amount and if given month is a payment month
    --if not calculating initial amount and not a payment month, return null to zero out the formula value
    --if calculating an initial amount, we want to return the component value regardless of if it's a payment month or not
    if A_INITIAL_AMT_FLAG = 0 and F_IS_PAYMENT_MONTH(A_ILR_ID, A_REVISION, A_MONTH) = false then
      return null;
    end if;

    L_COMPONENT_ID := TO_NUMBER(A_COMPONENT_ID);

    L_SQLS := 'SELECT amount
                FROM (
                    SELECT amount
                    FROM ls_index_component_values
                    WHERE formula_component_id = :component_id
                    AND effective_date <= :open_month
                    ORDER BY effective_date DESC
                )
                WHERE ROWNUM = 1';

  BEGIN
      EXECUTE IMMEDIATE L_SQLS
        INTO L_COMPONENT_AMOUNT
        USING L_COMPONENT_ID, A_MONTH;
  EXCEPTION
    WHEN No_Data_Found THEN
      RETURN 0 ;
  END;

    L_MSG := '   Got the following component value: ' || to_char(L_COMPONENT_AMOUNT);
     --  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

    RETURN L_COMPONENT_AMOUNT;
  exception
    when others then
      L_MSG := SUBSTR('Error calculating formula index/rate component: ' || sqlerrm, 1, 2000);
         return L_MSG;
  end F_CALC_INDEX;

  function F_CALC_VAR_COMPONENT(A_COMPONENT_ID NUMBER,
                                --A_ILR_ID       number,
                                A_ASSET_ID     number,
                                A_MONTH        date) return number is

   L_COMPONENT_ID               number(22,0);
   L_SQLS                       varchar2(2000);
   L_MSG                        varchar2(2000);
   L_COMPONENT_AMOUNT           number(22,8);
   L_COMPONENT_NUMBER_FORMAT    number(1,0);
   L_ARG_MONTH                  number(2,0);
   L_ARG_YEAR                   number(4,0);
   L_PAY_MONTH                  number(2,0);
   L_TEMP_MONTH                 number(2,0);
   L_TEMP_MONTH_CHAR            varchar2(35);
   L_CLOSEST_PAY_MONTH         number(2,0);
   L_LOAD_MONTH                varchar2(35);
   L_TEMP_MONTH_NUMBER         number(6,0);
   L_LOAD_MONTHS               varchar2(254);

   begin
    --get month number from A_MONTH and check if it's a payment month for any load month on the component calendar
    L_ARG_MONTH := to_number(to_char(A_MONTH,'mm'));
    L_ARG_YEAR := to_number(to_char(A_MONTH,'yyyy'));

    L_SQLS :=  'SELECT distinct PAYMENT_MONTH_NUMBER
                FROM LS_FORMULA_COMPONENT_CALENDAR
                WHERE FORMULA_COMPONENT_ID = :component_id
                AND PAYMENT_MONTH_NUMBER = :month';

    BEGIN
          EXECUTE IMMEDIATE L_SQLS
            INTO L_PAY_MONTH
            USING A_COMPONENT_ID, L_ARG_MONTH;
      EXCEPTION
        --if the SQL returns no data, then the passed in month is not a payment month, return null to zero out the formula
        WHEN No_Data_Found THEN
          RETURN null ;
      END;

    SELECT NUMBER_FORMAT_ID
    INTO L_COMPONENT_NUMBER_FORMAT
    FROM LS_FORMULA_COMPONENT
    WHERE FORMULA_COMPONENT_ID = A_COMPONENT_ID;

    -- only sum the amounts if the number is not a percentage
    IF L_COMPONENT_NUMBER_FORMAT <> 3 THEN
      --since the calendar only has pure month numbers (2 digits), we need to relate those months to
      --the corresponding full month numbers (6 digits) related to the component value data in LS_VARIABLE_COMPONENT_VALUES

      --start the string build
      L_LOAD_MONTHS := '(';

     --get the load_month_numbers from the calendar with this payment_month_number
     for month in (select load_month_number
                    from ls_formula_component_calendar
                    where formula_component_id = A_COMPONENT_ID
                    and payment_month_number = L_ARG_MONTH)
      loop
        --convert so that leading 0s are added
        L_TEMP_MONTH_CHAR := to_char(to_date(month.load_month_number,'mm'),'mm');

        --add the year, taking care to use previous year for any month > the passed in month
        if month.load_month_number > L_ARG_MONTH then
          L_TEMP_MONTH_NUMBER := to_number(to_char(L_ARG_YEAR - 1) || L_TEMP_MONTH_CHAR);
        else
          L_TEMP_MONTH_NUMBER := to_number(to_char(L_ARG_YEAR) || L_TEMP_MONTH_CHAR);
        end if;

        --add month number to the string containing all full month numbers
        L_LOAD_MONTHS := L_LOAD_MONTHS || to_char(L_TEMP_MONTH_NUMBER) || ',';

      end loop;

      --remove the last ',' and finish the string
      L_LOAD_MONTHS := SUBSTR(L_LOAD_MONTHS, 1, LENGTH(L_LOAD_MONTHS)-1);
      L_LOAD_MONTHS := L_LOAD_MONTHS || ')';

      L_SQLS := 'select sum(amount) amount
                 from ls_variable_component_values vc
                 where vc.formula_component_id = :component_id
                 and vc.ls_asset_id = :asset_id
                 and vc.incurred_month_number in '|| L_LOAD_MONTHS || ' ' || '
                 group by vc.formula_component_id, vc.ilr_id, vc.ls_asset_id';

      --retrieve the value
      BEGIN
          EXECUTE IMMEDIATE L_SQLS
            INTO L_COMPONENT_AMOUNT
            USING A_COMPONENT_ID, A_ASSET_ID;
      EXCEPTION
        WHEN No_Data_Found THEN
          RETURN 0 ;
      END;

    ELSE
      -- if it is a percentage, retrieve the latest
      -- this requires building a list of the incurred months for this particular payment month
      -- then selecting the most recently loaded value

       --start the string build
      L_LOAD_MONTHS := '(';

       --get the load_month_numbers from the calendar with this payment_month_number
       for month in (select load_month_number
                      from ls_formula_component_calendar
                      where formula_component_id = A_COMPONENT_ID
                      and payment_month_number = L_ARG_MONTH)
        loop
          --convert so that leading 0s are added
          L_TEMP_MONTH_CHAR := to_char(to_date(month.load_month_number,'mm'),'mm');

          --add the year, taking care to use previous year for any month > the passed in month
          if month.load_month_number > L_ARG_MONTH then
            L_TEMP_MONTH_NUMBER := to_number(to_char(L_ARG_YEAR - 1) || L_TEMP_MONTH_CHAR);
          else
            L_TEMP_MONTH_NUMBER := to_number(to_char(L_ARG_YEAR) || L_TEMP_MONTH_CHAR);
          end if;

          --add month number to the string containing all full month numbers
          L_LOAD_MONTHS := L_LOAD_MONTHS || to_char(L_TEMP_MONTH_NUMBER) || ',';

        end loop;

        --remove the last ',' and finish the string
        L_LOAD_MONTHS := SUBSTR(L_LOAD_MONTHS, 1, LENGTH(L_LOAD_MONTHS)-1);
        L_LOAD_MONTHS := L_LOAD_MONTHS || ')';

         --get the actual value from the list of load months
        L_SQLS := ' select amount from (
                      select amount
                      from ls_variable_component_values
                      where formula_component_id = :component_id
                      and ls_asset_id = :asset_id
                      and incurred_month_number in '|| L_LOAD_MONTHS || '
                      order by incurred_month_number desc
                     )
                     where ROWNUM = 1'
                    ;

        BEGIN
            EXECUTE IMMEDIATE L_SQLS
              INTO L_COMPONENT_AMOUNT
              USING A_COMPONENT_ID, A_ASSET_ID;
        EXCEPTION
          WHEN No_Data_Found THEN
            RETURN 0 ;
        END;

    END IF;

    RETURN L_COMPONENT_AMOUNT;

   exception
    when others then
      L_MSG := SUBSTR('Error calculating formula variable component: ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_CALC_VAR_COMPONENT;

   --Function to calculate historic component value
   --The A_ID argument is either ILR ID or asset ID
   --The formula with a historic component follows the associated VP for asset or ILR level
   --So this function must support calculating either way
   function F_CALC_HISTORIC(A_COMPONENT_ID          number,
                            A_ID                    number,
                            A_REVISION              number,
                            A_ASSET_LEVEL_FLAG      number,
                            A_SET_OF_BOOKS_ID       number,
                            A_MONTH                 date) return number is

   L_SQLS                   varchar2(5000);
   L_MSG                    varchar2(2000);
   L_COMPONENT_AMOUNT       number;
   L_ARG_MONTH              number(2,0);
   L_ARG_YEAR               number(4,0);
   L_COMPONENT_YEAR         number(4,0);
   L_COMPONENT_MONTH        number(2,0);
   L_COMPONENT_STR          varchar2(6);
   L_PAY_MONTH              number(2,0);
   L_ASSOC_BUCKET_NUMBER    number;
   L_ASSOC_BUCKET_STR       varchar2(2000);
   L_ILR_ID                 number;
   L_MONTHS_PER_TERM         number;
   L_ANNUAL                  number;
   L_SEMI_ANNUAL             number;
   L_QUARTERLY               number;
   L_MONTHLY                 number;
   L_ACTUAL                  number;
   L_NEXT_PAYMENT_MONTH      varchar2(35);
   L_TERM_COMPONENT_PAY_MONTHS varchar2(2000);
   L_TEMP_AMOUNT               number;
	 L_PREPAY_SWITCH          number(1,0);

   TYPE T_PAYMENT_MONTHS is table of number index by varchar2(35);

   L_COMPONENT_CALC_MONTHS          t_payment_months;
   L_TERM_PAYMENT_MONTHS               t_payment_months;

   begin
     L_ARG_MONTH := to_number(to_char(A_MONTH,'mm'));
     L_ARG_YEAR := to_number(to_char(A_MONTH,'yyyy'));

     --determine if the given month is a payment month for this component
     L_SQLS :=  'SELECT distinct PAYMENT_MONTH_NUMBER
                FROM LS_FORMULA_COMPONENT_CAL_HIST
                WHERE FORMULA_COMPONENT_ID = :component_id
                AND PAYMENT_MONTH_NUMBER = :month';

      BEGIN
          EXECUTE IMMEDIATE L_SQLS
            INTO L_PAY_MONTH
            USING A_COMPONENT_ID, L_ARG_MONTH;
      EXCEPTION
        --if the SQL returns no data, then the passed in month is not a payment month, return null to zero out the formula
        WHEN No_Data_Found THEN
          RETURN null ;
      END;

    --if this component is asset level, get the ILR ID
    --A_ID will be an asset ID in this case
    if A_ASSET_LEVEL_FLAG = 1 then
      select ilr_id
      into L_ILR_ID
      from ls_ilr_asset_map
      where ls_asset_id = A_ID
      and revision = A_REVISION;
    else
      L_ILR_ID := A_ID;
    end if;

		SELECT pre_payment_sw INTO l_prepay_switch
    FROM ls_lease
    where lease_id = (select distinct lease_id from ls_ilr where ilr_id = l_ilr_id);

    --create a master list (array) of all payment months for the component
    --this will be based on the calc months combined with the timing column, so it can only span over 2 years at most
    --example: its 201203 and March is a payment month for Jan-Mar, with Jan timing as Previous Year and Feb-Mar as Current Year
    --the month numbers stored would be 201101,201202,201203
    for calc_month in (select calc_month_number, timing from ls_formula_component_cal_hist
                       where formula_component_id = A_COMPONENT_ID and payment_month_number = L_ARG_MONTH)
    loop
      --extract correct year based on timing
      if calc_month.timing = 1 then
        L_COMPONENT_YEAR := L_ARG_YEAR;
      elsif calc_month.timing = 2 then
        L_COMPONENT_YEAR := L_ARG_YEAR - 1;
      end if;

      --create the month number string with leading zero on the month if needed
      L_COMPONENT_STR := to_char(L_COMPONENT_YEAR) || lpad(to_char(calc_month.calc_month_number), 2, '0');

      --add the full month number to the master list
      --the index is the month number string, which is all we care about
      --so the value associated is just a dummy- use the calc_month_number
      L_COMPONENT_CALC_MONTHS(L_COMPONENT_STR) := calc_month.calc_month_number;

    end loop;

     --set the variables for payment frequency types
     L_ANNUAL := 1;
     L_SEMI_ANNUAL := 2;
     L_QUARTERLY := 3;
     L_MONTHLY := 4;
     L_ACTUAL := 5;

    --initialize the amount
    L_COMPONENT_AMOUNT := 0;

    --loop over the payment terms
    for payment_term in (select payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms
                         from ls_ilr_payment_term where ilr_id = L_ILR_ID and revision = A_REVISION order by payment_term_date)
    loop
      --reset the string to hold the months as well as the string to hold associated buckets to sum
      L_TERM_COMPONENT_PAY_MONTHS := '(';
      L_ASSOC_BUCKET_STR := '(';

      --get the buckets for the VP associated to this component in this payment term
      for bucket in (select bucket_number
                     from ls_ilr_payment_term_var_paymnt ptvp
                     inner join ls_historical_component_values hcv
                       on hcv.formula_component_id = A_COMPONENT_ID
                       and hcv.variable_payment_id = ptvp.variable_payment_id
                     where ptvp.ilr_id = L_ILR_ID
                     and ptvp.revision = A_REVISION
					 and ptvp.set_of_books_id = A_SET_OF_BOOKS_ID
                     and payment_term_id = payment_term.payment_term_id)
       loop
         L_ASSOC_BUCKET_STR := L_ASSOC_BUCKET_STR || 'sum(contingent_paid' || to_char(bucket.bucket_number) || ')+';
       end loop;

       --if the VP is not associated to any buckets for this term, go to the next loop
       --the string will only have the leading paren if this is the case
       if LENGTH(L_ASSOC_BUCKET_STR) = 1 then
         continue;
       end if;

       --trim the final + from the bucket string and add the trailing paren
      L_ASSOC_BUCKET_STR := SUBSTR(L_ASSOC_BUCKET_STR, 1, LENGTH(L_ASSOC_BUCKET_STR)-1) || ')';

      --create a list of months in this payment term that are calculation months for the historic component
      --  i.e. if the component calcs jan-mar to pay in mar, then get the month numbers from this payment term for jan-mar
      --  if instead feb was the payment term start date, then this prevents double counting, as it would have been calced in a previous loop
      --  in that case, we'd only add feb and mar to the list since jan would have been included in the sum already

      --set the number of months per term for this payment term
       if payment_term.payment_freq_id = L_ANNUAL then
         L_MONTHS_PER_TERM := 12;
       elsif payment_term.payment_freq_id = L_SEMI_ANNUAL then
         L_MONTHS_PER_TERM := 6;
       elsif payment_term.payment_freq_id = L_QUARTERLY then
         L_MONTHS_PER_TERM := 3;
       elsif payment_term.payment_freq_id = L_MONTHLY or payment_term.payment_freq_id = L_ACTUAL then
         L_MONTHS_PER_TERM := 1;
       end if;

       --loop over the number of terms and check if payment months coincide with component payment months
       --if so, add those months to a string to be used in summing the component value
       for term in 1..payment_term.number_of_terms
         loop
           --add months to payment_term_date based on payment frequency and current term number
           --subtract 1 month from the start month because add_months appends to the end, which would throw everything off by 1 month
					 IF l_prepay_switch = 0 THEN
              l_next_payment_month := to_char(add_months(add_months(payment_term.payment_term_date,-1), l_months_per_term*TERM),'yyyymm');
           ELSE
              l_next_payment_month := to_char(add_months(payment_term.payment_term_date, l_months_per_term * (term - 1)), 'yyyymm');
           END IF;

           --check if this payment month is in the component calc months
           if L_COMPONENT_CALC_MONTHS.EXISTS(L_NEXT_PAYMENT_MONTH) then
             L_TERM_COMPONENT_PAY_MONTHS := L_TERM_COMPONENT_PAY_MONTHS || L_NEXT_PAYMENT_MONTH || ',';
           end if;

       end loop;

      --if no months were added to the string, proceed to the next loop
      if LENGTH(L_TERM_COMPONENT_PAY_MONTHS) = 1 then
        continue;
      end if;

      --finish the string holding the months - remove the trailing , and add the final )
      L_TERM_COMPONENT_PAY_MONTHS := SUBSTR(L_TERM_COMPONENT_PAY_MONTHS, 1, LENGTH(L_TERM_COMPONENT_PAY_MONTHS)-1) || ')';

      --now sum the buckets over the months from this payment term which are also calc months for the component
      --if asset level, select from the asset schedule
      --otherwise, select from the ilr_schedule
      if A_ASSET_LEVEL_FLAG = 1 then
        L_SQLS := 'select ' || L_ASSOC_BUCKET_STR || ' ' ||
                  'from LS_ASSET_SCHEDULE ' ||
                  'where ls_asset_id = :a_asset_id ' ||
                  'and revision = :a_revision ' ||
                  'and set_of_books_id = :a_set_of_books_id ' ||
                  'and to_char(month,''yyyymm'') in ' || L_TERM_COMPONENT_PAY_MONTHS;

         execute immediate L_SQLS
         into L_TEMP_AMOUNT
         using A_ID, A_REVISION, A_SET_OF_BOOKS_ID;

       else
         L_SQLS := 'select ' || L_ASSOC_BUCKET_STR || ' ' ||
                  'from LS_ILR_SCHEDULE ' ||
                  'where ilr_id = :a_ilr_id ' ||
                  'and revision = :a_revision ' ||
                  'and set_of_books_id = :a_set_of_books_id ' ||
                  'and to_char(month,''yyyymm'') in ' || L_TERM_COMPONENT_PAY_MONTHS;

         execute immediate L_SQLS
         into L_TEMP_AMOUNT
         using L_ILR_ID, A_REVISION, A_SET_OF_BOOKS_ID;

       end if;

      --add the sum to the running total
      L_COMPONENT_AMOUNT := L_COMPONENT_AMOUNT + L_TEMP_AMOUNT;

    end loop;

    --all done, just return the calculated amount
    return L_COMPONENT_AMOUNT;

   exception
     when others then
       return 'Error! ' + sqlerrm;
   end F_CALC_HISTORIC;

   --Function to calculate a variable payment for an ILR
   --  If the payment has a variable component, each asset will get an individual calc
   --  If there's no variable component, the formula is calculated once and each asset gets the correct amount
   function F_CALC_VAR_PAYMENT(A_ILR_ID               number,
                               A_REVISION             number,
                               A_VARIABLE_PAYMENT_ID  number,
                               A_BUCKET_NUMBER        number) return varchar2 is

   L_SQLS                       varchar2(5000);
   L_MSG                        varchar2(2000);
   L_FORMULA                    varchar2(4000);
   L_AMOUNT                     number(22,8);
   MONTH_CHAR                   varchar2(2000);
   L_BUCKET_NAME                varchar2(254);

   cursor months is
     SELECT DISTINCT MONTH
     FROM LS_ILR_SCHEDULE
     WHERE ILR_ID = A_ILR_ID;

   cursor assets is
     SELECT DISTINCT ls_asset_id
     FROM LS_ILR_ASSET_MAP
     WHERE ILR_ID = A_ILR_ID
     AND REVISION = A_REVISION;

    cursor sobs is
      SELECT DISTINCT set_of_books_id
      FROM LS_ILR_SCHEDULE
      WHERE ILR_ID = A_ILR_ID;

   begin
     PKG_PP_LOG.P_WRITE_MESSAGE('  Beginning single var paymnt calc');

     --get the formula for this payment
     SELECT PAYMENT_FORMULA_DB
     INTO L_FORMULA
     FROM LS_VARIABLE_PAYMENT
     WHERE VARIABLE_PAYMENT_ID = A_VARIABLE_PAYMENT_ID;

     --loop over assets
     for asset in assets
       loop

       --loop over months and calc formula per asset
       for month in months
         loop
           --convert month to char for use in SQL execution
           MONTH_CHAR := to_char(month.month, 'yyyymm');

           --calculate the formula, whether it has variable components or not
           /*L_SQLS := 'SELECT ' || L_FORMULA ||
                     ' FROM LS_ASSET_SCHEDULE_STG a
                       INNER JOIN LS_ASSET
                         LS_ASSET.LS_ASSET_ID = LS_ASSET_SCHEDULE_STG.LS_ASSET_ID
                       WHERE LS_ASSET.ILR_ID = :ilr_id
                       AND LS_ASSET_SCHEDULE_STG.revision = :revision
                       AND LS_ASSET_SCHEDULE_STG.MONTH = to_date(:month, ''yyyymm'')';*/

           /* EXECUTE IMMEDIATE L_SQLS
              INTO L_AMOUNT
              USING A_ILR_ID, A_REVISION, MONTH_CHAR;*/

           -- call the function to calc the formula associated with a single asset, set of books, payment term

           --if variable components, allocate to assets as-calculated
           L_BUCKET_NAME := 'CONTINGENT_ACCRUAL' || to_char(A_BUCKET_NUMBER);
           L_SQLS := 'UPDATE LS_ASSET_SCHEDULE_STG
                     SET ' || L_BUCKET_NAME || ' = :amount
                     WHERE ASSET_ID';

           --if no variable components, allocate to assets based on FMV

           --allocate to each asset paid based on payment terms

       end loop;

     end loop;

   return 'OK';

   end F_CALC_VAR_PAYMENT;

   --Function to calculate the variable payments for an ILR in appropriate run order
   function F_CALC_ILR_VAR_PAYMENTS(A_ILR_ID     number,
                                    A_REVISION   number,
                                    A_MONTH      date) return varchar2 is

   L_SQLS                       varchar2(2000);
   L_BUCKET_NAME                varchar2(35);
   L_MSG                        varchar2(2000);
   L_STATUS                     varchar2(2000);
   L_VALUE                      number(22,8);
   L_PAID_AMOUNT                number(22,8);

   cursor c_assets is
          select lap.ilr_id, lap.revision, las.month, las.set_of_books_id, las.ls_asset_id, ipt.payment_term_id, ptvp.rent_type,
         ptvp.bucket_number, ptvp.run_order, lvp.asset_level_formula, ipt.payment_term_date
         from ls_ilr_payment_term ipt, ls_ilr_payment_term_var_paymnt ptvp, ls_ilr_asset_schedule_stg las, ls_ilr_asset_map lap, ls_variable_payment lvp
         WHERE lap.ilr_id = A_ILR_ID
             AND lap.revision = A_REVISION
             AND ipt.ilr_id = lap.ilr_id
             AND lap.ls_asset_id = las.ls_asset_id
             AND lower(ptvp.rent_type) = 'contingent'
             AND ptvp.ilr_id = lap.ilr_id
             AND ptvp.revision = lap.revision
			 AND ptvp.set_of_books_id = las.set_of_books_id
             AND ptvp.incl_in_initial_measure = 0
             AND lvp.variable_payment_id = ptvp.variable_payment_id
             AND las.month >= ipt.payment_term_date
             AND las.month < add_months(ipt.payment_term_date, decode(payment_freq_id, 4, 1, 3, 3, 2, 6, 1, 12) * number_of_terms)
             AND ptvp.payment_term_id IN (
                SELECT payment_term_id
                FROM ls_ilr_payment_term b
                WHERE b.ilr_id = lap.ilr_id
                AND b.revision = las.revision
                AND las.month >= payment_term_date
                AND las.month < add_months(payment_term_date, decode(payment_freq_id, 4, 1, 3, 3, 2, 6, 1, 12) * number_of_terms)
                )
             order by ilr_id, revision, run_order, bucket_number, ls_asset_id, month, payment_term_id, set_of_books_id;

   begin
     L_MSG := '  Beginning variable payment calculation';
     PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

     --roll up to the schedule tables before beginning calculation
     L_STATUS := PKG_LEASE_SCHEDULE.F_SAVE_SCHEDULES(A_MONTH);
     if L_STATUS <> 'OK' then
       PKG_PP_LOG.P_WRITE_MESSAGE('Saving schedule tables before VP calc failed: ' || L_STATUS);
       L_STATUS := 'Saving schedule tables before VP calc failed:'  || L_STATUS;
       return L_STATUS;
     end if;

	 --PKG_PP_LOG.P_WRITE_MESSAGE('Beginning loop over assets');

     --loop over every row in the cursor
     for asset in c_assets
       loop

		--PKG_PP_LOG.P_WRITE_MESSAGE('Before F_IS_PAYMENT_MONTH()');
         --call the function to calc the formula
         --check if the current month is a payment month, if not then don't calc anything
         --formulas with components will handle this correctly on their own,
         -- but formulas with only amounts and/or schedule fields will not, so we handle them here
         if F_IS_PAYMENT_MONTH(A_ILR_ID, A_REVISION, asset.month) then

			--PKG_PP_LOG.P_WRITE_MESSAGE('Before F_CALC_ASSET_BUCKET()');

            L_VALUE := F_CALC_ASSET_BUCKET(asset.ilr_id, asset.ls_asset_id, asset.revision, asset.set_of_books_id,
                                           asset.payment_term_id, asset.bucket_number, asset.month,'SCHEDULE') ;
         else
           L_VALUE := 0;
         end if;

		--PKG_PP_LOG.P_WRITE_MESSAGE('Before CONTINGENT_ACCRUAL update');

         --update the bucket accrual column
         L_BUCKET_NAME := 'CONTINGENT_ACCRUAL' || to_char(asset.bucket_number);

         L_SQLS := 'UPDATE LS_ILR_ASSET_SCHEDULE_STG
                   SET ' || L_BUCKET_NAME || ' = :value
                    WHERE ls_asset_id = :asset_id
                    AND revision = :revision
                    AND set_of_books_id = :set_of_books_id
                    AND month = :month';

          EXECUTE IMMEDIATE L_SQLS
            USING L_VALUE, asset.LS_ASSET_ID, asset.REVISION, asset.SET_OF_BOOKS_ID, to_char(asset.MONTH);

         --update the bucket paid column
         --  if asset-level formula, update no matter what
         L_BUCKET_NAME := 'CONTINGENT_PAID' || to_char(asset.bucket_number);
         if asset.asset_level_formula = 1 then

		--PKG_PP_LOG.P_WRITE_MESSAGE('Before CONTINGENT_PAID update1');

           L_SQLS := 'UPDATE LS_ILR_ASSET_SCHEDULE_STG
                     SET ' || L_BUCKET_NAME || ' = :value
                      WHERE ls_asset_id = :asset_id
                      AND revision = :revision
                      AND set_of_books_id = :set_of_books_id
                      AND month = :month';

           EXECUTE IMMEDIATE L_SQLS
            USING L_VALUE, asset.LS_ASSET_ID, asset.REVISION, asset.SET_OF_BOOKS_ID, to_char(asset.MONTH);

         elsif asset.asset_level_formula = 0 then
           --  if ILR-level formula, calc the paid amount and update
           --  for formulas w/ index/rate components, paid amount will be 0 in non payment month, and full amount in payment month (matches accrual)
           --  for formulas w/ schedule fields, we need to use the function so that payment follows the payment term frequency

		--PKG_PP_LOG.P_WRITE_MESSAGE('Before F_CALC_ASSET_PAID_AMOUNT');

           L_PAID_AMOUNT := F_CALC_ASSET_PAID_AMOUNT(asset.ls_asset_id, asset.revision, asset.month, asset.bucket_number, asset.set_of_books_id);

		--PKG_PP_LOG.P_WRITE_MESSAGE('Before CONTINGENT_PAID update2');

           L_SQLS := 'UPDATE LS_ILR_ASSET_SCHEDULE_STG
                     SET ' || L_BUCKET_NAME || ' = :value
                      WHERE ls_asset_id = :asset_id
                      AND revision = :revision
                      AND set_of_books_id = :set_of_books_id
                      AND month = :month';

           EXECUTE IMMEDIATE L_SQLS
            USING L_PAID_AMOUNT, asset.LS_ASSET_ID, asset.REVISION, asset.SET_OF_BOOKS_ID, to_char(asset.MONTH);
         else
           return 'Formula has no value denoting asset-level or ILR-level';
         end if;

		--PKG_PP_LOG.P_WRITE_MESSAGE('Before F_UPDATE_SCHEDULE_TABLES');

         --call the update function to roll up the staging tables to the live tables
         --this ensures the next calculation uses the most up to date numbers
         L_STATUS := F_UPDATE_SCHEDULE_TABLES(A_ILR_ID, asset.month, A_REVISION);
         --L_STATUS := PKG_LEASE_SCHEDULE.F_SAVE_SCHEDULES(A_MONTH);
         if L_STATUS <> 'OK' then
           PKG_PP_LOG.P_WRITE_MESSAGE('Updating schedule tables during VP calc failed: ' || L_STATUS);
           L_STATUS := 'Updating schedule tables during VP calc failed: ' || L_STATUS;
           return L_STATUS;
         end if;

      end loop;

      return 'OK';

  exception
    when zero_divide then
       L_MSG := 'Variable payment calculation error: divide by zero. Ensure formula components used in division are non-zero';
       return L_MSG;
    when others then
      L_MSG := SUBSTR('ERROR calculating variable payments: ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_CALC_ILR_VAR_PAYMENTS;

   -- Function to calculate variable payments for all staged ILRs
   function F_CALC_ALL_VAR_PAYMENTS(A_MONTH date) return varchar2 is
     L_SQLS                       varchar2(2000);
     L_MSG                        varchar2(2000);
     L_STATUS                     varchar2(2000);
     L_DEL_STATUS                 varchar2(2000);

     cursor STAGED_ILRS is
          SELECT DISTINCT ILR_ID, REVISION
          FROM LS_ILR_ASSET_SCHEDULE_STG;

   begin
     PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
     PKG_PP_LOG.P_WRITE_MESSAGE('Starting var payment calc for all staged ILRs');

     for STAGED_ILR in STAGED_ILRS
       loop
         L_MSG := ' Calculating variable payments for ILR with ID: ' || to_char(STAGED_ILR.ILR_ID);

         L_STATUS := F_CALC_ILR_VAR_PAYMENTS(STAGED_ILR.ILR_ID, STAGED_ILR.REVISION, A_MONTH);

         IF L_STATUS <> 'OK' THEN
           L_MSG := 'Calculation of variable payments failed for ILR ID: ' || to_char(STAGED_ILR.ILR_ID) || ' with message : ' || L_STATUS;

           --clear the staging tables since the schedule build will end now
           L_STATUS := PKG_LEASE_SCHEDULE.F_CLEAR_STAGING_TABLES;
           if L_STATUS <> 'OK' then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
           end if;

           --return the constructed error message
           return L_MSG;
         END IF;

       end loop;


     return 'OK';

   exception
    when others then
      L_MSG := SUBSTR('ERROR calculating variable payments: ' || sqlerrm, 1, 2000);
         return L_MSG;

   end F_CALC_ALL_VAR_PAYMENTS;


   --**************************************************************************
   --  F_CALC_ASSET_BUCKET.
   -- This function calculates the variable payments for the passed in arguments.
   -- If the variable payment type is variable component, ls_asset_schedule is used
   -- else ls_ilr_schedule is used for calculations
   --**************************************************************************

   function F_CALC_ASSET_BUCKET( A_ILR_ID number, A_ASSET_ID number, A_REVISION number, A_SET_OF_BOOKS_ID number, A_PAYMENT_TERM_ID number, A_BUCKET_NUMBER number, A_MONTH date, A_PROCESS_TYPE varchar2) return number is
   L_MSG varchar2(100);
   l_formula_db varchar2(4000);
   l_asset_level_formula number(1,0);
   l_amount number(22,2);
   l_sqls varchar2(8000);
   l_asset_fmv number; l_ilr_current_cost number; l_asset_npv number;

      begin
        select lvp.payment_formula_db, asset_level_formula into l_formula_db, l_asset_level_formula
       from ls_variable_payment lvp, ls_ilr_payment_term_var_paymnt ptvp
       where ptvp.ilr_id = A_ILR_ID
          and ptvp.revision = A_REVISION
          and ptvp.payment_term_id = A_PAYMENT_TERM_ID
          and lower(ptvp.rent_type) = 'contingent'
          and ptvp.bucket_number = A_BUCKET_NUMBER
          and ptvp.variable_payment_id = lvp.variable_payment_id
		  and ptvp.set_of_books_id = A_SET_OF_BOOKS_ID;


     begin
       if l_asset_level_formula = 1 then
         --calculate the formula for the assets
         L_SQLS := 'SELECT nvl(' || l_formula_db || ', 0)' ;
         if A_PROCESS_TYPE = 'MONTH_END' then
           L_SQLS := L_SQLS || ' FROM LS_ASSET_SCHEDULE a, LS_ILR_OPTIONS LIO, (select 0 as initial_amt_flag from dual) b';
         else
           L_SQLS := L_SQLS || ' FROM LS_ASSET_SCHEDULE a, LS_ILR_OPTIONS LIO, (select 0 as initial_amt_flag from dual) b';

         end if;

         L_SQLS := L_SQLS || '  WHERE a.LS_ASSET_ID = :A_ASSET_ID
                       AND a.revision = :A_REVISION
                       AND a.MONTH = :A_MONTH
                       and a.SET_OF_BOOKS_ID = :A_SET_OF_BOOKS_ID
                       AND LIO.ILR_ID = :A_ILR_ID
                       AND LIO.REVISION = A.REVISION';

		--PKG_PP_LOG.P_WRITE_MESSAGE('Before EXECUTE IMMEDIATE 1');

            EXECUTE IMMEDIATE L_SQLS
            INTO L_AMOUNT
            USING A_ASSET_ID, A_REVISION, A_MONTH, A_SET_OF_BOOKS_ID, A_ILR_ID;

       else
         ---calculate the formula for the asset by first calculating the npv ratio and then
         -- evaluate the formula at ILR level and then apply tha to the variable payment
         select la.fmv, asob.current_lease_cost, (la.fmv/asob.current_lease_cost) into l_asset_fmv, l_ilr_current_cost, l_asset_npv
         from ls_asset la, ls_ilr_amounts_set_of_books asob
         where asob.ilr_id = A_ILR_ID
           and asob.revision = A_REVISION
           and set_of_books_id = A_SET_OF_BOOKS_ID
           and la.ls_asset_id = A_ASSET_id;


        l_sqls := 'SELECT ' || to_char(l_asset_npv) || '* (nvl(' || l_formula_db || ',0))';
        if A_PROCESS_TYPE = 'MONTH_END' then
           L_SQLS := L_SQLS || ' FROM LS_ILR_SCHEDULE A, LS_ILR_OPTIONS LIO, (select 0 as initial_amt_flag from dual) b';
         else
           L_SQLS := L_SQLS || ' FROM LS_ILR_SCHEDULE A, LS_ILR_OPTIONS LIO, (select 0 as initial_amt_flag from dual) b';

         end if;


        L_SQLS := L_SQLS ||  ' WHERE LIO.ILR_ID = :A_ILR_ID
                       AND A.revision = :A_REVISION
                       AND A.MONTH = :A_MONTH
                       and A.SET_OF_BOOKS_ID = :A_SET_OF_BOOKS_ID
                       AND A.ILR_ID = LIO.ILR_ID
                       AND A.REVISION = LIO.REVISION';

		--PKG_PP_LOG.P_WRITE_MESSAGE('Before EXECUTE IMMEDIATE 2');

         EXECUTE IMMEDIATE L_SQLS
         INTO L_AMOUNT
         USING A_ILR_ID, A_REVISION, A_MONTH, A_SET_OF_BOOKS_ID;

       end if;


        exception when no_data_found then
         return 0 ;

       end;


       return L_AMOUNT;

     end F_CALC_ASSET_BUCKET;


   --**************************************************************************
   --  F_CALC_ASSET_BUCKETS_MONTH_END.
   -- This function calculates accrual and paid contingent buckets for all assets
   -- for a given company id. The buckets are then rolled up to the ILRs
   --**************************************************************************

   function F_CALC_ASSET_BUCKETS_MONTH_END(A_COMPANY_ID in number, A_MONTH in date, CALC_TYPE varchar2) return varchar2 is
       cursor C_ASSETS is
         select ls_ilr.ilr_id, ls_ilr_asset_map.ls_asset_id, ls_ilr.current_revision, csob.set_of_books_id, ipt.payment_term_id, ptvp.rent_type,
         ptvp.bucket_number,ptvp.run_order, ptvp.incl_in_initial_measure, ptvp.initial_var_payment_value, ipt.payment_term_date
         from ls_ilr, company_set_of_books csob, ls_ilr_asset_map, ls_ilr_payment_term ipt, ls_ilr_payment_term_var_paymnt ptvp,
		      ls_ilr_approval lia
         WHERE ls_ilr.company_id = A_COMPANY_ID
             AND ls_ilr.ilr_id = ls_ilr_asset_map.ilr_id
             AND csob.company_id = ls_ilr.company_id
             AND ls_ilr_asset_map.revision = ls_ilr.current_revision
             AND ipt.ilr_id = ls_ilr.ilr_id
             AND ipt.revision = ls_ilr.current_revision
             AND ptvp.ilr_id = ls_ilr.ilr_id
             AND ptvp.revision = ls_ilr.current_revision
             AND ls_ilr.ilr_id = lia.ilr_id
             AND ls_ilr.current_revision = lia.revision
			 AND ptvp.set_of_books_id = csob.set_of_books_id
             AND lia.approval_status_id in (3,6)
             AND lower(ptvp.rent_type) = 'contingent'
             AND ptvp.variable_payment_id IS NOT NULL
             AND A_MONTH >= ipt.payment_term_date
             AND A_MONTH < add_months(ipt.payment_term_date, decode(payment_freq_id, 4, 1, 3, 3, 2, 6, 1, 12) * number_of_terms)
             AND ptvp.payment_term_id IN (
                SELECT payment_term_id
                FROM ls_ilr_payment_term b
                WHERE b.ilr_id = ls_ilr.ilr_id
                AND b.revision = ls_ilr.current_revision
                AND A_MONTH >= payment_term_date
                AND A_MONTH < add_months(payment_term_date, decode(payment_freq_id, 4, 1, 3, 3, 2, 6, 1, 12) * number_of_terms)
                )
             order by ilr_id, ls_asset_id, current_revision, payment_term_id, bucket_number, run_order;


       L_MSG varchar2(100);
       l_amount number(22,2); L_TEMP_AMOUNT  number(22,2); l_asset_npv NUMBER(22,2);
       l_sqls varchar2(4000); l_temp_col varchar2(100);

       begin
         PKG_PP_LOG.P_WRITE_MESSAGE(' Calculating Variable Payments for assets.');
         for asset in c_assets
         LOOP
		    -- If running accruals and payments more than once, reset the buckets to their initial values
		    l_sqls := 'update ls_asset_schedule ';
           if lower(calc_type) = 'accruals' then
             l_temp_col:= 'contingent_accrual';
           else
             l_temp_col:= 'contingent_paid';

           end if;


           l_temp_col := l_temp_col || to_char(asset.bucket_number);

           l_sqls:= l_sqls || ' set ' || l_temp_col || ' = ' || to_char(nvl(asset.initial_var_payment_value,0)) ;
           l_sqls:= l_sqls || ' where ls_asset_id = ' || to_char(asset.ls_asset_id) ;
           l_sqls:= l_sqls || ' and revision = ' || to_char(asset.current_revision);
           l_sqls:= l_sqls || ' and set_of_books_id = ' || to_char(asset.set_of_books_id);
           l_sqls:= l_sqls || ' and month = ''' || to_char(A_MONTH, 'DD-MON-YYYY') || '''';

           begin
              execute immediate l_sqls;
           exception
              when no_data_found then
              CONTINUE;

           end;


            l_temp_amount := F_CALC_ASSET_BUCKET(asset.ilr_id, asset.ls_asset_id, asset.current_revision, asset.set_of_books_id, asset.payment_term_id, asset.bucket_number, A_MONTH, 'MONTH_END' );

		   --calculate gain loss if this was included in the initial measure. The initial value for the ILR must be broken based on the asset NPV

           if asset.incl_in_initial_measure = 1 then

		       select nvl(la.fmv,1)/nvl(asob.current_lease_cost,1)
               into l_asset_npv
               from ls_asset la, ls_ilr_amounts_set_of_books asob
               where asob.ilr_id = asset.ilr_id
                 and asob.revision = asset.current_revision
                 and set_of_books_id = asset.set_of_books_id
                 and la.ls_asset_id = asset.ls_asset_id;


			  l_amount := l_temp_amount - (asset.initial_var_payment_value * l_asset_npv);
           else
              l_amount :=  l_temp_amount;

           end if;


           l_sqls := 'update ls_asset_schedule ';
           if lower(calc_type) = 'accruals' then
             l_temp_col:= 'contingent_accrual';
           else
             l_temp_col:= 'contingent_paid';

           end if;


           l_temp_col := l_temp_col || to_char(asset.bucket_number);

           --l_sqls:= 'set ' || l_temp_col || ' = ' || l_temp_col || ' + ' ||  to_char(l_amount) ;
           l_sqls:= l_sqls || ' set ' || l_temp_col || ' = ' || to_char(l_amount) ;
           l_sqls:= l_sqls || ' where ls_asset_id = ' || to_char(asset.ls_asset_id) ;
           l_sqls:= l_sqls || ' and revision = ' || to_char(asset.current_revision);
           l_sqls:= l_sqls || ' and set_of_books_id = ' || to_char(asset.set_of_books_id);
           l_sqls:= l_sqls || ' and month = ''' || to_char(A_MONTH, 'DD-MON-YYYY') || '''';

           begin
              execute immediate l_sqls;
           exception
              when no_data_found then
              CONTINUE;

           end;


         END LOOP;


         -- Roll up the asset buckets to ILR level
         L_MSG := F_ROLLUP_ASSETS_BUCKETS_TO_ILR(A_COMPANY_ID, A_MONTH, CALC_TYPE);
         if L_MSG <> 'OK' then
           return L_MSG;
         end if;


       return 'OK';
     exception
        when others then
         L_MSG := SUBSTR(sqlerrm, 1, 2000);
		 return L_MSG;

   end F_CALC_ASSET_BUCKETS_MONTH_END;


   --**************************************************************************
   --  F_ROLLUP_ASSETS_BUCKETS_TO_ILR.
   -- This function rolls up all bucket values from the assets to the ILR level.
   --
   --**************************************************************************

   function F_ROLLUP_ASSETS_BUCKETS_TO_ILR(A_COMPANY_ID in number, A_MONTH in date, A_CALC_TYPE varchar2) return varchar2 is
     begin
       if lower(A_CALC_TYPE) = 'accruals' then
         update ls_ilr_schedule LIS set (contingent_accrual1, contingent_accrual2,contingent_accrual3,contingent_accrual4,contingent_accrual5, contingent_accrual6, contingent_accrual7,contingent_accrual8,contingent_accrual9,contingent_accrual10)=
           (
             select contingent_accrual1, contingent_accrual2,contingent_accrual3,contingent_accrual4,contingent_accrual5, contingent_accrual6, contingent_accrual7, contingent_accrual8, contingent_accrual9, contingent_accrual10
             from
                 (
                   select lim.ilr_id, lim.revision, las.set_of_books_id, las.month, sum(contingent_accrual1) contingent_accrual1, sum(contingent_accrual2) contingent_accrual2, sum(contingent_accrual3) contingent_accrual3, sum(contingent_accrual4) contingent_accrual4,
                          sum(contingent_accrual5) contingent_accrual5, sum(contingent_accrual6) contingent_accrual6, sum(contingent_accrual7) contingent_accrual7, sum(contingent_accrual8) contingent_accrual8, sum(contingent_accrual9) contingent_accrual9, sum(contingent_accrual10) contingent_accrual10
                   from ls_asset_schedule las, ls_ilr_asset_map lim, ls_ilr ilr
                   where ilr.ilr_id = lim.ilr_id
                     and  ilr.current_revision = lim.revision
                     and las.revision = ilr.current_revision
                     and lim.ls_asset_id = las.ls_asset_id
                     and ilr.company_id = A_COMPANY_ID
                     and las.month = A_MONTH
                   group by lim.ilr_id, lim.revision, las.set_of_books_id, las.month
                   order by 1, 2, 3, 4
                 ) DS
             where LIS.ilr_id = DS.ilr_id and LIS.revision = DS.revision and LIS.set_of_books_id = DS.set_of_books_id and LIS.month = DS.month
           )
         where (ilr_id, revision) in (select ilr_id, current_revision from ls_ilr where company_id = A_COMPANY_ID )
         and month = A_MONTH;

       else
                  update ls_ilr_schedule LIS set (contingent_paid1, contingent_paid2,contingent_paid3,contingent_paid4,contingent_paid5, contingent_paid6, contingent_paid7,contingent_paid8,contingent_paid9,contingent_paid10)=
           (
             select contingent_paid1, contingent_paid2,contingent_paid3,contingent_paid4,contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8, contingent_paid9, contingent_paid10
             from
                 (
                   select lim.ilr_id, lim.revision, las.set_of_books_id, las.month, sum(contingent_paid1) contingent_paid1, sum(contingent_paid2) contingent_paid2, sum(contingent_paid3) contingent_paid3, sum(contingent_paid4) contingent_paid4,
                          sum(contingent_paid5) contingent_paid5, sum(contingent_paid6) contingent_paid6, sum(contingent_paid7) contingent_paid7, sum(contingent_paid8) contingent_paid8, sum(contingent_paid9) contingent_paid9, sum(contingent_paid10) contingent_paid10
                   from ls_asset_schedule las, ls_ilr_asset_map lim, ls_ilr ilr
                   where ilr.ilr_id = lim.ilr_id
                     and  ilr.current_revision = lim.revision
                     and las.revision = ilr.current_revision
                     and lim.ls_asset_id = las.ls_asset_id
                     and ilr.company_id = A_COMPANY_ID
                     and las.month = A_MONTH
                   group by lim.ilr_id, lim.revision, las.set_of_books_id, las.month
                   order by 1, 2, 3, 4
                 ) DS
             where LIS.ilr_id = DS.ilr_id and LIS.revision = DS.revision and LIS.set_of_books_id = DS.set_of_books_id and LIS.month = DS.month
           )
         where (ilr_id, revision) in (select ilr_id, current_revision from ls_ilr where company_id = A_COMPANY_ID )
         and month = A_MONTH;

       end if;

       return 'OK';
     exception when no_data_found then
       return 'OK';
       when others then
         return sqlerrm;

     end F_ROLLUP_ASSETS_BUCKETS_TO_ILR;


     function F_CALC_ASSET_PAID_AMOUNT(A_ASSET_ID       number,
                                       A_REVISION       number,
                                       A_MONTH          date,
                                       A_BUCKET_NUMBER  number,
                                       A_SET_OF_BOOKS_ID number) return number is

      L_SQLS                         varchar2(2000);
      L_BUCKET_NAME                  varchar2(35);
      L_AMOUNT                       number(22,8);

      begin
        --PKG_PP_LOG.P_WRITE_MESSAGE('   Starting calc for paid amount');

        L_BUCKET_NAME := 'CONTINGENT_ACCRUAL' || to_char(A_BUCKET_NUMBER);

        L_SQLS := 'select case from (
                    SELECT asset_id, revision, set_of_books_id, MONTH, payment_month, accrual, CASE WHEN MONTH = payment_month THEN Sum(accrual) OVER (PARTITION BY asset_id, revision, set_of_books_id, payment_month) ELSE 0 END CASE
                    FROM (
                        SELECT a.ls_asset_id AS asset_id, a.revision AS revision, a.set_of_books_id AS set_of_books_id,
                            a.MONTH AS month, Min(payment_month.month) AS payment_month, a.' || L_BUCKET_NAME || ' AS accrual
                        FROM ls_ilr_asset_schedule_stg a
                        INNER JOIN (
                            SELECT ls_asset_id, revision, set_of_books_id, MONTH
                            FROM ls_ilr_asset_schedule_stg
                            WHERE payment_month in (1,2)
                        ) payment_month
                            ON payment_month.ls_asset_id = a.ls_asset_id
                            AND payment_month.revision = a.revision
                            AND payment_month.set_of_books_id = a.set_of_books_id
                            AND payment_month.MONTH >= a.month
                        WHERE a.ls_asset_id = :asset_id
                        AND a.revision = :revision
                        AND a.set_of_books_id = :set_of_books_id
                        GROUP BY a.ls_asset_id, a.revision, a.set_of_books_id, a.MONTH, a.' || L_BUCKET_NAME || '
                    )
                    ORDER BY month
                  )
                  where month = :month';

      EXECUTE IMMEDIATE L_SQLS
        INTO L_AMOUNT
        USING A_ASSET_ID, A_REVISION, A_SET_OF_BOOKS_ID, A_MONTH;

      return L_AMOUNT;

      exception
        when no_data_found then
        return 0;
        when others then
         return sqlerrm;

      end F_CALC_ASSET_PAID_AMOUNT;


   function F_UPDATE_SCHEDULE_TABLES(A_ILR_ID            number,
                                     A_MONTH             date,
                                     A_REVISION          number) return varchar2
   is
     L_STATUS                        varchar2(2000);

   begin

     --update the asset schedule table using the staging table
     update ls_asset_schedule sch
     set (contingent_accrual1, contingent_accrual2,contingent_accrual3,contingent_accrual4,contingent_accrual5, contingent_accrual6, contingent_accrual7,contingent_accrual8,contingent_accrual9,contingent_accrual10,
          contingent_paid1, contingent_paid2,contingent_paid3,contingent_paid4,contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8, contingent_paid9, contingent_paid10)=
           (
           select ds.contingent_accrual1, ds.contingent_accrual2, ds.contingent_accrual3, ds.contingent_accrual4, ds.contingent_accrual5, ds.contingent_accrual6, ds.contingent_accrual7, ds.contingent_accrual8, ds.contingent_accrual9, ds.contingent_accrual10,
                  ds.contingent_paid1, ds.contingent_paid2, ds.contingent_paid3, ds.contingent_paid4, ds.contingent_paid5, ds.contingent_paid6, ds.contingent_paid7, ds.contingent_paid8, ds.contingent_paid9, ds.contingent_paid10
             from
              (
               select las.ls_asset_id, las.revision, set_of_books_id, month, contingent_accrual1, contingent_accrual2,contingent_accrual3,contingent_accrual4,contingent_accrual5, contingent_accrual6, contingent_accrual7,contingent_accrual8,contingent_accrual9,contingent_accrual10,
                                           contingent_paid1, contingent_paid2,contingent_paid3,contingent_paid4,contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8, contingent_paid9, contingent_paid10
               from ls_ilr_asset_schedule_stg las, ls_ilr_asset_map lim
               where lim.revision = A_REVISION
               and las.revision = lim.revision
               and las.ls_asset_id = lim.ls_asset_id
               and lim.ilr_id = A_ILR_ID
               and las.month = A_MONTH
               order by 1, 2, 3, 4
               ) DS
            where sch.ls_asset_id = ds.ls_asset_id
            and ds.set_of_books_id = sch.set_of_books_id
            and sch.revision = ds.revision
            and sch.month = ds.month
            )
       where (ls_asset_id, revision) in (select ls_asset_id, revision from ls_ilr_asset_map where ilr_id = A_ILR_ID and revision = A_REVISION)
       and month = A_MONTH
       --order by ls_asset_id, revision, set_of_books_id, month
       ;


     --update the ILR schedule table using the asset schedule
     update ls_ilr_schedule LIS set (contingent_accrual1, contingent_accrual2,contingent_accrual3,contingent_accrual4,contingent_accrual5, contingent_accrual6, contingent_accrual7,contingent_accrual8,contingent_accrual9,contingent_accrual10,
                                         contingent_paid1, contingent_paid2,contingent_paid3,contingent_paid4,contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8, contingent_paid9, contingent_paid10)=
           (
             select contingent_accrual1, contingent_accrual2,contingent_accrual3,contingent_accrual4,contingent_accrual5, contingent_accrual6, contingent_accrual7, contingent_accrual8, contingent_accrual9, contingent_accrual10,
                    contingent_paid1, contingent_paid2,contingent_paid3,contingent_paid4,contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8, contingent_paid9, contingent_paid10
             from
                 (
                   select lim.ilr_id, lim.revision, las.set_of_books_id, las.month, sum(contingent_accrual1) contingent_accrual1, sum(contingent_accrual2) contingent_accrual2, sum(contingent_accrual3) contingent_accrual3, sum(contingent_accrual4) contingent_accrual4,
                          sum(contingent_accrual5) contingent_accrual5, sum(contingent_accrual6) contingent_accrual6, sum(contingent_accrual7) contingent_accrual7, sum(contingent_accrual8) contingent_accrual8, sum(contingent_accrual9) contingent_accrual9, sum(contingent_accrual10) contingent_accrual10,
                          sum(contingent_paid1) contingent_paid1, sum(contingent_paid2) contingent_paid2, sum(contingent_paid3) contingent_paid3, sum(contingent_paid4) contingent_paid4,
                          sum(contingent_paid5) contingent_paid5, sum(contingent_paid6) contingent_paid6, sum(contingent_paid7) contingent_paid7, sum(contingent_paid8) contingent_paid8, sum(contingent_paid9) contingent_paid9, sum(contingent_paid10) contingent_paid10
                   from ls_asset_schedule las, ls_ilr_asset_map lim, ls_ilr ilr
                   where ilr.ilr_id = lim.ilr_id
                     and  ilr.current_revision = lim.revision
                     and las.revision = ilr.current_revision
                     and lim.ls_asset_id = las.ls_asset_id
                     and ilr.ilr_id = A_ILR_ID
                     and las.month = A_MONTH
                   group by lim.ilr_id, lim.revision, las.set_of_books_id, las.month
                   order by 1, 2, 3, 4
                 ) DS
             where LIS.ilr_id = DS.ilr_id and LIS.revision = DS.revision and LIS.set_of_books_id = DS.set_of_books_id and LIS.month = DS.month
           )
         where ilr_id = A_ILR_ID
         and revision = A_REVISION
         and month = A_MONTH;


         return 'OK';
      exception
        when others then
         return sqlerrm;
      end F_UPDATE_SCHEDULE_TABLES;

	function F_IS_PAYMENT_MONTH_RTN_NUMBER(A_ILR_ID     number,
                               A_REVISION   number,
                               A_MONTH      date) return integer
	is
		L_RTN 	BOOLEAN;
	begin
		L_RTN := F_IS_PAYMENT_MONTH(A_ILR_ID, A_REVISION, A_MONTH);

		if L_RTN = TRUE then
			return 1;
		else
			return 0;
		end IF;
	end F_IS_PAYMENT_MONTH_RTN_NUMBER;


   function F_IS_PAYMENT_MONTH(A_ILR_ID     number,
                               A_REVISION   number,
                               A_MONTH      date) return boolean
     is
     L_SQLS                    varchar2(2000);
     L_MSG                     varchar2(2000);
     L_NEXT_PAYMENT_MONTH      date;
     L_MONTHS_PER_TERM         number;
     L_ANNUAL                  number;
     L_SEMI_ANNUAL             number;
     L_QUARTERLY               number;
     L_MONTHLY                 number;
     L_ACTUAL                  number;
		 L_PREPAY_SWITCH           number(1,0);

     TYPE T_PAYMENT_MONTHS is table of number index by varchar2(35);

     L_PAYMENT_MONTHS          t_payment_months;

     cursor PAYMENT_TERMS is
       SELECT payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms
       FROM LS_ILR_PAYMENT_TERM
       WHERE ilr_id = A_ILR_ID
       AND revision = A_REVISION
       ORDER BY payment_term_id;


     begin
       --set the variables for payment frequency types
       L_ANNUAL := 1;
       L_SEMI_ANNUAL := 2;
       L_QUARTERLY := 3;
       L_MONTHLY := 4;
       L_ACTUAL := 5;

	   --pkg_pp_log.p_write_message('begin F_IS_PAYMENT_MONTH()');

			 SELECT pre_payment_sw INTO l_prepay_switch
			 FROM ls_lease
			 where lease_id = (select distinct lease_id from ls_ilr where ilr_id = A_ILR_ID);

	   --pkg_pp_log.p_write_message('before payment_terms_loop');

       for payment_term in payment_terms
         loop
           --set the number of months per term for this payment term
           if payment_term.payment_freq_id = L_ANNUAL then
             L_MONTHS_PER_TERM := 12;
           elsif payment_term.payment_freq_id = L_SEMI_ANNUAL then
             L_MONTHS_PER_TERM := 6;

           elsif payment_term.payment_freq_id = L_QUARTERLY then
             L_MONTHS_PER_TERM := 3;

           elsif payment_term.payment_freq_id = L_MONTHLY or payment_term.payment_freq_id = L_ACTUAL then
             L_MONTHS_PER_TERM := 1;

           end if;


	   --pkg_pp_log.p_write_message('before nested loop. payment_term_id: ' || to_char(payment_term.payment_term_id));

           for term in 1..payment_term.number_of_terms
             loop
              --add months to payment_term_date based on payment frequency and current term number
              --subtract 1 month from the start month because add_months appends to the end, which would throw everything off by 1 month
              --L_NEXT_PAYMENT_MONTH := add_months(add_months(payment_term.payment_term_date,-1), L_MONTHS_PER_TERM*term);

							IF l_prepay_switch = 0 THEN
              	L_NEXT_PAYMENT_MONTH := add_months(add_months(payment_term.payment_term_date,-1), L_MONTHS_PER_TERM*term);
							ELSE
              	L_NEXT_PAYMENT_MONTH := add_months(payment_term.payment_term_date, L_MONTHS_PER_TERM * (term - 1));
           		END IF;

               --add this payment month to the array
               L_PAYMENT_MONTHS(to_char(L_NEXT_PAYMENT_MONTH,'yyyymm')) := L_PAYMENT_MONTHS.LAST + 1;
           end loop;

       end loop;



	   --pkg_pp_log.p_write_message('after  payment_terms_loop');


        --check if the given month is present in the array
        if L_PAYMENT_MONTHS.EXISTS(to_char(A_MONTH,'yyyymm')) then
          return true;
        else
          return false;

        end if;

	   --pkg_pp_log.p_write_message('end F_IS_PAYMENT_MONTH');


     --exception
       --when others then
         --return sqlerrm;
     end F_IS_PAYMENT_MONTH;

     function F_GET_FIRST_PAYMENT_TERM_MONTH(A_ILR_ID     number,
                                        A_REVISION   number) return date is

     L_SQLS                    varchar2(2000);
     L_MSG                     varchar2(2000);
     L_FIRST_PAYMENT_TERM_MONTH     date;

     cursor PAYMENT_TERMS is
       SELECT payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms
       FROM LS_ILR_PAYMENT_TERM
       WHERE ilr_id = A_ILR_ID
       AND revision = A_REVISION
       ORDER BY payment_term_id;

     begin

       for payment_term in payment_terms
         loop
           --get the first payment term date
           L_FIRST_PAYMENT_TERM_MONTH := payment_term.payment_term_date;

           return L_FIRST_PAYMENT_TERM_MONTH;

        end loop;

     end F_GET_FIRST_PAYMENT_TERM_MONTH;



end PKG_LEASE_VAR_PAYMENTS;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (8242, 0, 2017, 4, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2017.4.0.0_PKG_LEASE_VAR_PAYMENTS.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
