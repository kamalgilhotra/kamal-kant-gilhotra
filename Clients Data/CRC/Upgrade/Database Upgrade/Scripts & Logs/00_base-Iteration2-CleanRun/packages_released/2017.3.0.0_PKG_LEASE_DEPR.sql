CREATE OR REPLACE package pkg_lease_depr as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_DEPR
   || Description:
   ||============================================================================
   || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By      Reason for Change
   || -------- ---------- --------------  ----------------------------------------
   || 10.4.3.0 06/25/2014 B.Beck          Original Version
   || 10.4.3.0 07/07/2014 Charlie Shilling
   || 2015.2.0 09/29/2015 Andrew Scott    maint-45012.  Change how SL is calced for performance.
   || 2016.1.0 02/26/2016 Anand R         maint-45470.
   ||============================================================================
   */
	procedure P_DEPR_SLE( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_LIFE number, A_MSG out varchar2);

	procedure P_DEPR_SL( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_LIFE number, A_MP_CONV NUMBER, A_MSG out varchar2);

	procedure P_DEPR_FERC( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_MSG out varchar2);

	procedure P_FCST_LEASE(A_ILR_ID in number, A_REVISION in number);

	procedure P_GET_LEASE_DEPR(A_ILR_ID in number, A_REVISION in number);

	PROCEDURE p_fcst_lease_use_temp_table(    a_ilr_id   NUMBER,    a_revision NUMBER);

end PKG_LEASE_DEPR;
/

CREATE OR REPLACE package body pkg_lease_depr as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_DEPR
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By      Reason for Change
   || -------- ---------- --------------  ----------------------------------------
   || 10.4.3.0 06/25/2014 B.Beck          Original Version
   || 10.4.3.0 07/07/2014 Charlie Shilling
   || 2015.2.0 09/29/2015 Andrew Scott    maint-45012.  Change how SL is calced for performance.
   || 2016.1.0 02/26/2016 Anand R         maint-45470.
   ||============================================================================
   */

	--**************************************************************************
	--                            Start Body
	--**************************************************************************
	--**************************************************************************
	--                            PROCEDURES
	--**************************************************************************
	   procedure P_DEPR_SLE( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_LIFE number, A_MSG out varchar2, A_IS_FCST number)
   is
      L_MONTHLY_OBL number(22,2);
      L_TOTAL_OBL number(22,2);
      L_TOTAL_CHECK number(22, 2);
      L_NUM_MONTHS number(22,0);
      L_ROUND number(22,2);
    L_MAX_MONTH  LS_ASSET_SCHEDULE."MONTH"%type;
    L_CUR_MONTH  LS_ASSET_SCHEDULE."MONTH"%type;
    L_START_MONTH  LS_ASSET_SCHEDULE."MONTH"%type;
    L_MONTHLOOP number;
	l_is_om number;
    L_IS_REMEASUREMENT number;
    L_REMEASUREMENT_DATE date;
    L_OWNER_TRF number;
   begin
		if a_is_fcst = 0 then
		  A_MSG := 'DELETING from depr_method_sle for current month through end of lease';
		  delete from DEPR_METHOD_SLE
		  where ASSET_ID = A_ASSET_ID
		  and SET_OF_BOOKS_ID = A_SOB_ID
		  ;
		end if;

      select case when purchase_option_type_id = 1 then 0 else 1 end
      into L_OWNER_TRF
      from ls_ilr_options
        JOIN ls_asset ON ls_asset.ilr_id = ls_ilr_options.ilr_id
      where ls_asset.ls_asset_id = a_ls_asset_id
      and ls_ilr_options.revision = a_revision;

      a_msg := 'GET number of months';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
	  begin
	  	select abc."MONTH",  add_months( abc."MONTH", num_months - 1), num_months, total_expense, abc.is_om, is_remeasurement, remeasurement_date
	   	into L_START_MONTH, L_MAX_MONTH, L_NUM_MONTHS, L_TOTAL_OBL, l_is_om, L_IS_REMEASUREMENT, L_REMEASUREMENT_DATE
		from
		(
			select
					las.month, las.is_om, remeasurement.is_remeasurement, Trunc(remeasurement.remeasurement_date, 'month') remeasurement_date,
					count(1) over() as num_months,
          Decode(Nvl(is_remeasurement,0),
                0, sum(las.INTEREST_PAID) over() + las.end_capital_cost,
                sum(principal_paid) OVER() + sum(las.INTEREST_PAID) over() + Nvl(remeasurement.prior_month_end_nbv, 0) - Nvl(remeasurement.prior_month_end_liability, 0)
          )
          - case when la.estimated_residual = 0 and lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual', la.COMPANY_ID))) = 'yes'
          	  then nvl(la.guaranteed_residual_amount, 0)
          	  else la.fmv * la.estimated_residual
            END AS total_expense,
          row_number() over(order by month) as the_row
				from ls_asset_schedule las
          JOIN ls_asset la ON la.ls_asset_id = las.ls_asset_id
          JOIN ls_ilr_options o ON o.ilr_id = la.ilr_id AND o.revision = las.revision
          left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON las.ls_asset_id = remeasurement.ls_asset_id
            AND las.revision = remeasurement.revision
            AND las.set_of_books_id = remeasurement.set_of_books_id
            AND las.month = Trunc(remeasurement.remeasurement_date, 'month')
				where las.ls_asset_id = A_LS_ASSET_ID
				and las.set_of_books_id = A_SOB_ID
				and las.revision = A_REVISION
				and la.ls_asset_id = A_LS_ASSET_ID
      		and las.is_om = 0
        and las.MONTH >= Trunc(Nvl(o.remeasurement_date, To_Date('180001', 'yyyymm')), 'month')
			) abc
			where the_row = 1;
	  EXCEPTION
	  	WHEN no_data_found THEN
			a_msg:='OK';
			RETURN;
	  END;

      if A_LIFE > 0 THEN
         L_MONTHLY_OBL := round( L_TOTAL_OBL / L_NUM_MONTHS, 2);
         L_TOTAL_CHECK := L_NUM_MONTHS * L_MONTHLY_OBL;
         L_ROUND := L_TOTAL_OBL - L_TOTAL_CHECK;

		 if a_is_fcst = 0 then
          A_MSG := 'INSERTING depreciation expense for SLE Method';
          PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
			 insert into DEPR_METHOD_SLE
			 (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, DEPR_EXPENSE)
			 select A_ASSET_ID, A_SOB_ID, LAS."MONTH",
					decode(las.is_om, 1, 0, depr_expense)
			 from LS_ASSET_SCHEDULE LAS
            JOIN LS_DEPR_FORECAST LDF ON LAS.ls_asset_id = LDF.ls_asset_id AND LAS.revision = LDF.revision AND LAS.set_of_books_id = LDF.set_of_books_id AND LAS.MONTH = LDF.MONTH
			 where LAS.LS_ASSET_ID = A_LS_ASSET_ID
			 and LAS.REVISION = A_REVISION
			    and LAS.SET_OF_BOOKS_ID = A_SOB_ID;

          PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows inserted into DEPR_METHOD_SLE.');
		else
          A_MSG := 'INSERTING depreciation expense for SLE Method';
          PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

			 insert into LS_DEPR_FORECAST
			(LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, MONTH, begin_reserve, depr_expense, depr_exp_alloc_adjust, end_reserve)
			select las.ls_asset_id, las.set_of_books_id, las.revision, las."MONTH",
				sum( case when LAS."MONTH" <> L_MAX_MONTH then L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL
				   else L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL + L_ROUND
				   end ) over(partition by  las.ls_asset_id,  las.set_of_books_id,  las.revision order by  las.month) -
					 case when LAS."MONTH" <> L_MAX_MONTH then L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL
				   else L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL + L_ROUND
				      end + Nvl(remeasurement.prior_month_end_depr_reserve,0) as begin_reserve,
				 case when LAS."MONTH" <> L_MAX_MONTH then L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL
				   else L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL + L_ROUND
				   end as depr_expense, 0 as depr_exp_alloc_adjust,
				sum( case when LAS."MONTH" <> L_MAX_MONTH then L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL
				   else L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL + L_ROUND
				      end) over(partition by  las.ls_asset_id,  las.set_of_books_id,  las.revision order by  las.month) + Nvl(remeasurement.prior_month_end_depr_reserve,0) as end_reserve
			from ls_asset_schedule las
          left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON las.revision = remeasurement.revision
                  AND las.ls_asset_id = remeasurement.ls_asset_id
                  AND las.set_of_books_id = remeasurement.set_of_books_id
                  AND las.month >= Trunc(remeasurement.remeasurement_date, 'month')
			where LAS.LS_ASSET_ID = A_LS_ASSET_ID
			 and LAS.REVISION = A_REVISION
			 and LAS.SET_OF_BOOKS_ID = A_SOB_ID
			 and las.is_om = 0
          and las.month >= Trunc(Nvl(L_REMEASUREMENT_DATE, To_Date('180001', 'yyyymm')), 'month')
			 ;
          PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows inserted into LS_DEPR_FORECAST.');
		end if;

       -- add extra months if A_LIFE > L_NUM_MONTHS.  This is the portion after the lease term
       -- and when the asset is "an owned asset"
       --Only need to handle ls_depr_forecast. For in-servicing/approval, extra months/rows copy directly from LS_DEPR_FORECAST to DEPR_METHOD_SLE
       if A_LIFE > L_NUM_MONTHS and A_REVISION > 0 AND L_OWNER_TRF = 1 then
        for L_MONTHLOOP in L_NUM_MONTHS .. (A_LIFE - 1 - (CASE WHEN L_IS_REMEASUREMENT = 1 THEN (A_LIFE-L_NUM_MONTHS) ELSE 0 END) ) loop
			L_CUR_MONTH := add_months( L_START_MONTH, L_MONTHLOOP );

			    if a_is_fcst <> 0 then
				if l_is_om = 0 then
					insert into LS_DEPR_FORECAST
					(LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, MONTH, begin_reserve, depr_expense, depr_exp_alloc_adjust, end_reserve)
					select LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, L_CUR_MONTH, dg.end_reserve,
						CASE WHEN L_MONTHLOOP <> (A_LIFE - 1) then L_MONTHLY_OBL else L_MONTHLY_OBL + L_ROUND end, 0,
						dg.end_reserve + CASE WHEN L_MONTHLOOP <> (A_LIFE - 1) then L_MONTHLY_OBL else L_MONTHLY_OBL + L_ROUND end
					from LS_DEPR_FORECAST dg
					where dg.LS_ASSET_ID = A_LS_ASSET_ID
					and dg.REVISION = A_REVISION
					and dg.SET_OF_BOOKS_ID = A_SOB_ID
					and dg."MONTH" = add_months(L_CUR_MONTH, -1)
					;
				end if;
			end if;
        end loop;
       end if;
      end if;

      A_MSG := 'OK';
   end P_DEPR_SLE;

	procedure P_DEPR_SL( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_LIFE number, A_MP_CONV NUMBER, A_MSG out varchar2)
   is
      L_MONTHLY_EXP number(22,2);
      L_TOTAL_EXP number(22,2);
      L_TOTAL_CHECK number(22, 2);
      L_NUM_MONTHS number(22,0);
      L_ROUND number(22,2);
    L_MAX_MONTH  LS_ASSET_SCHEDULE."MONTH"%type;
    L_CUR_MONTH  LS_ASSET_SCHEDULE."MONTH"%type;
    L_START_MONTH  LS_ASSET_SCHEDULE."MONTH"%type;
    L_MONTHLOOP number;
    L_REMEASUREMENT_DATE DATE;
   begin

      -- CJS 5/17/17 Use A_LIFE - 1, otherwise end up with an extra month
	  a_msg := 'GET number of months';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
	  begin
		  SELECT
      Decode(Trunc(remeasurement_date, 'month'),NULL, abc.MONTH, Add_Months(abc.MONTH, Months_Between(Trunc(remeasurement_date, 'month'), abc.MONTH))) l_start_month,
      Decode(Trunc(remeasurement_date, 'month'),NULL, add_months( abc."MONTH", Decode(Trunc(remeasurement_date, 'month'), null, A_LIFE, num_months) - 1),
        Add_Months(Add_Months(abc.MONTH, Months_Between(Trunc(remeasurement_date, 'month'), abc.MONTH)), Least(A_LIFE, num_months) - Months_Between(Trunc(remeasurement_date, 'month'), abc.MONTH) - 1)
      ) l_max_month,
      Decode(Trunc(remeasurement_date, 'month'),NULL, Least(A_LIFE, num_months), Least(A_LIFE, num_months) - Months_Between(Trunc(remeasurement_date, 'month'), abc.MONTH)) l_num_months,
      total_expense,
      Trunc(remeasurement_date, 'month') remeasurement_date
		   into L_START_MONTH, L_MAX_MONTH, L_NUM_MONTHS, L_TOTAL_EXP, L_REMEASUREMENT_DATE
		  from
		  (
			 select
				las.month,
				count(1) over() as num_months,
				row_number() over(order by month) as the_row,
        --Last_Value(end_capital_cost) OVER() is equal to the new end capital cost after remeasurement
        (Last_Value(end_capital_cost) OVER() - Decode(o.remeasurement_date, NULL, 0, 1) * Nvl(remeasurement.prior_month_end_depr_reserve, 0) - case when (la.ESTIMATED_RESIDUAL = 0
                     and lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual', la.COMPANY_ID))) = 'yes') /* WMD */
                        then la.GUARANTEED_RESIDUAL_AMOUNT
                        else round(la.ESTIMATED_RESIDUAL * la.FMV, 2) end ) as total_expense,
        Trunc(o.remeasurement_date, 'month') remeasurement_date
				from ls_asset_schedule las
          JOIN ls_asset la ON las.ls_asset_id = la.ls_asset_id
          JOIN ls_ilr_payment_term ilrpt ON la.ilr_id = ilrpt.ilr_id
          JOIN ls_ilr_options o ON o.ilr_id = la.ilr_id AND o.revision = las.revision
          left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON (las.ls_asset_id = remeasurement.ls_asset_id
            AND las.revision = remeasurement.revision
            AND las.set_of_books_id = remeasurement.set_of_books_id
            --Does not join on date so the prior month reserve will be included in "the_row = 1"
            )
				where las.ls_asset_id = A_LS_ASSET_ID
				and las.set_of_books_id = A_SOB_ID
				and las.revision = A_REVISION
				and la.ls_asset_id = A_LS_ASSET_ID
			 and ilrpt.revision = A_REVISION
			 and ilrpt.payment_term_id = 1
			 and las.is_om = 0 --wmd
			 and las.month >= trunc(ilrpt.payment_term_date, 'month')
		  ) abc
		  where the_row = 1;
	  exception
			when no_data_found then
				a_msg := 'OK';
				return;
		end ;

    -- If the ILR is in transition then there will be IS_OM = 1 months that are excluded from determining L_MAX_MONTH above.
    -- Need to decrement the date by the number of IS_OM = 1 to get the actual max month.
    SELECT Add_Months(L_MAX_MONTH, -1 * Nvl(Count(1), 0))
    INTO L_MAX_MONTH
    from ls_asset_schedule las
      JOIN ls_asset la ON la.ls_asset_id = las.ls_asset_id
      JOIN ls_ilr_options o ON o.ilr_id = la.ilr_id AND o.revision = las.revision
      left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON las.ls_asset_id = remeasurement.ls_asset_id
        AND las.revision = remeasurement.revision
        AND las.set_of_books_id = remeasurement.set_of_books_id
        AND las.month = Trunc(remeasurement.remeasurement_date, 'month')
		where las.ls_asset_id = A_LS_ASSET_ID
		and las.set_of_books_id = A_SOB_ID
		and las.revision = A_REVISION
		and la.ls_asset_id = A_LS_ASSET_ID
    and las.is_om = 1
    and las.MONTH >= Trunc(Nvl(o.remeasurement_date, To_Date('180001', 'yyyymm')), 'month');

    -- If the calculated remaining number of months is less than 0, then set remaining months to 1 to expense all remaining depreciaton in the remeasurement month
    -- i.e. economic life is less than the number of lease terms and the asset was fully depreciated prior to remeasurement
    IF L_REMEASUREMENT_DATE IS NOT NULL AND L_NUM_MONTHS <= 0 THEN
      L_NUM_MONTHS := 1;
      L_MAX_MONTH := L_REMEASUREMENT_DATE;
    END IF;

    /* CJS 4/14/17 If we have a MPC of 0.5, will be off by a penny on odd numbers if we don't add check here */
    L_MONTHLY_EXP := round( L_TOTAL_EXP / L_NUM_MONTHS, 2);
    if Nvl(A_MP_CONV, 1) = 0.5 THEN
      L_TOTAL_CHECK := (L_NUM_MONTHS - 1) * L_MONTHLY_EXP + (2 * Round(L_MONTHLY_EXP * Nvl(A_MP_CONV, 1), 2));
    else
      L_TOTAL_CHECK := L_NUM_MONTHS * L_MONTHLY_EXP;
    end if;
    L_ROUND := L_TOTAL_EXP - L_TOTAL_CHECK;

    PKG_PP_LOG.P_WRITE_MESSAGE('A_LIFE : '||A_LIFE);
    PKG_PP_LOG.P_WRITE_MESSAGE('A_MP_CONV : '||A_MP_CONV);
    PKG_PP_LOG.P_WRITE_MESSAGE('L_MAX_MONTH : '||L_MAX_MONTH);
    PKG_PP_LOG.P_WRITE_MESSAGE('L_NUM_MONTHS : '||L_NUM_MONTHS);
    PKG_PP_LOG.P_WRITE_MESSAGE('L_TOTAL_EXP : '||L_TOTAL_EXP);
    PKG_PP_LOG.P_WRITE_MESSAGE('L_MONTHLY_EXP : '||L_MONTHLY_EXP);
    PKG_PP_LOG.P_WRITE_MESSAGE('L_TOTAL_CHECK : '||L_TOTAL_CHECK);
    PKG_PP_LOG.P_WRITE_MESSAGE('L_ROUND : '||L_ROUND);

		A_MSG := 'INSERTING depreciation expense for SL Method';
        PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        insert into LS_DEPR_FORECAST
		(LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, MONTH, begin_reserve, depr_expense, depr_exp_alloc_adjust, end_reserve)
		select las.ls_asset_id, las.set_of_books_id, las.revision, las."MONTH",
			/* CJS 3/7/17 Should only put amounts up to max month, round in max month, then 0 after */
			/* CJS 4/11/17 Factor in mid-period_method for start month and additional month; in BPO month extensions, include MP Convention in first/last month and extend if needed
			--Add in a new month for anything besides a MPC of 1; we're assuming 0, 0.5, and 1; if others happen to ever be needed, this will need to be updated to use (1 - MPC) in last month and other changes */
			sum( case WHEN LAS."MONTH" < L_MAX_MONTH AND LAS."MONTH" = L_START_MONTH THEN L_MONTHLY_EXP * Nvl(A_MP_CONV, 1)
          when LAS."MONTH" < Add_Months(L_MAX_MONTH, Decode(Nvl(A_MP_CONV, 1), 1, 0, 1)) AND LAS."MONTH" <> L_START_MONTH then L_MONTHLY_EXP
				  when LAS."MONTH" = Add_Months(L_MAX_MONTH, Decode(Nvl(A_MP_CONV, 1), 1, 0, 1)) then L_MONTHLY_EXP * Decode(Nvl(A_MP_CONV, 1), 0, 1, Nvl(A_MP_CONV, 1)) + L_ROUND
          else 0
				  end ) over(partition by  las.ls_asset_id,  las.set_of_books_id,  las.revision order by  las.month) -
					case WHEN LAS."MONTH" < L_MAX_MONTH AND LAS."MONTH" = L_START_MONTH THEN L_MONTHLY_EXP * Nvl(A_MP_CONV, 1)
          when LAS."MONTH" < Add_Months(L_MAX_MONTH, Decode(Nvl(A_MP_CONV, 1), 1, 0, 1)) AND LAS."MONTH" <> L_START_MONTH then L_MONTHLY_EXP
				  when LAS."MONTH" = Add_Months(L_MAX_MONTH, Decode(Nvl(A_MP_CONV, 1), 1, 0, 1)) then L_MONTHLY_EXP * Decode(Nvl(A_MP_CONV, 1), 0, 1, Nvl(A_MP_CONV, 1)) + L_ROUND
				  else 0
          end + Nvl(remeasurement.prior_month_end_depr_reserve,0) as begin_reserve,
				case WHEN LAS."MONTH" < L_MAX_MONTH AND LAS."MONTH" = L_START_MONTH THEN L_MONTHLY_EXP * Nvl(A_MP_CONV, 1)
          when LAS."MONTH" < Add_Months(L_MAX_MONTH, Decode(Nvl(A_MP_CONV, 1), 1, 0, 1)) AND LAS."MONTH" <> L_START_MONTH then L_MONTHLY_EXP
				  when LAS."MONTH" = Add_Months(L_MAX_MONTH, Decode(Nvl(A_MP_CONV, 1), 1, 0, 1)) then L_MONTHLY_EXP * Decode(Nvl(A_MP_CONV, 1), 0, 1, Nvl(A_MP_CONV, 1)) + L_ROUND
				  else 0
          end as depr_expense, 0 as depr_exp_alloc_adjust,
			sum( case WHEN LAS."MONTH" < L_MAX_MONTH AND LAS."MONTH" = L_START_MONTH THEN L_MONTHLY_EXP * Nvl(A_MP_CONV, 1)
          when LAS."MONTH" < Add_Months(L_MAX_MONTH, Decode(Nvl(A_MP_CONV, 1), 1, 0, 1)) AND LAS."MONTH" <> L_START_MONTH then L_MONTHLY_EXP
				  when LAS."MONTH" = Add_Months(L_MAX_MONTH, Decode(Nvl(A_MP_CONV, 1), 1, 0, 1)) then L_MONTHLY_EXP * Decode(Nvl(A_MP_CONV, 1), 0, 1, Nvl(A_MP_CONV, 1)) + L_ROUND
				  else 0
          end) over(partition by  las.ls_asset_id,  las.set_of_books_id,  las.revision order by  las.month) + Nvl(remeasurement.prior_month_end_depr_reserve,0) as end_reserve
		from ls_asset_schedule las
      JOIN ls_asset la ON las.ls_asset_id = la.ls_asset_id
      JOIN ls_ilr_payment_term ilrpt ON la.ilr_id = ilrpt.ilr_id
      left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON (las.ls_asset_id = remeasurement.ls_asset_id
          AND las.revision = remeasurement.revision
          AND las.set_of_books_id = remeasurement.set_of_books_id
          AND las.month >= Trunc(remeasurement.remeasurement_date, 'month'))
		where LAS.LS_ASSET_ID = A_LS_ASSET_ID
			and LAS.REVISION = A_REVISION
			and LAS.SET_OF_BOOKS_ID = A_SOB_ID
			and ilrpt.revision = A_REVISION
			and ilrpt.payment_term_id = 1
            and las.is_om = 0 -- wmd
			and las.month >= Greatest(trunc(ilrpt.payment_term_date, 'month'), Trunc(Nvl(L_REMEASUREMENT_DATE, To_Date('180001', 'yyyymm')),'month'));
        PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows inserted into LS_DEPR_FORECAST.');

    A_MSG := 'OK';
   end P_DEPR_SL;

   procedure P_DEPR_FERC( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_MSG out varchar2)
   is
   begin
      A_MSG := 'DELETING from depr_method_sle for current month through end of lease';
      delete from DEPR_METHOD_SLE
      where ASSET_ID = A_ASSET_ID
      and SET_OF_BOOKS_ID = A_SOB_ID;

    A_MSG := 'INSERTING depreciation expense for FERC Method';
    insert into DEPR_METHOD_SLE
      (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, DEPR_EXPENSE)
      select A_ASSET_ID, A_SOB_ID, LAS."MONTH", decode(las.is_om, 1, 0, LAS.PRINCIPAL_ACCRUAL)
       from LS_ASSET_SCHEDULE LAS
      where LAS.LS_ASSET_ID = A_LS_ASSET_ID
        and LAS.REVISION = A_REVISION
        and LAS.SET_OF_BOOKS_ID = A_SOB_ID;


      A_MSG := 'OK';
   end P_DEPR_FERC;


   procedure P_DEPR_SLE( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_LIFE number, A_MSG out varchar2)
   is
   begin
      P_DEPR_SLE( A_LS_ASSET_ID, A_REVISION, A_ASSET_ID, A_SOB_ID, A_MONTH, A_LIFE, A_MSG, 0);
   end P_DEPR_SLE;




   procedure P_DEPR_FERC_FCST( A_ILR_ID in number, A_REVISION in number )
   is
   begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_DEPR.P_DEPR_FERC_FCST');

    insert into LS_DEPR_FORECAST
		(LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, MONTH, begin_reserve, depr_expense, depr_exp_alloc_adjust, end_reserve)
		select las.ls_asset_id, las.set_of_books_id,  las.revision,  las.month,
			sum( las.principal_accrual) over(partition by  las.ls_asset_id,  las.set_of_books_id,  las.revision order by  las.month) -
				 las.principal_accrual + Nvl(remeasurement.prior_month_end_depr_reserve, 0) as begin_reserve,
			 las.principal_accrual as depr_expense, 0 as depr_exp_alloc_adjust,
			sum( las.principal_accrual) over(partition by  las.ls_asset_id,  las.set_of_books_id,  las.revision order by  las.month) + Nvl(remeasurement.prior_month_end_depr_reserve, 0) as end_reserve
		from ls_asset_schedule las
      JOIN ls_asset la ON las.ls_asset_id = la.ls_asset_id
      JOIN depr_group dg ON la.depr_group_id = dg.depr_group_id
      JOIN ls_ilr_options lio ON lio.ilr_id = la.ilr_id AND lio.revision = las.revision
      JOIN ls_fasb_cap_type_sob_map sob_map ON sob_map.lease_cap_type_id = lio.lease_cap_type_id AND sob_map.set_of_books_id = las.set_of_books_id
      left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON (las.ls_asset_id = remeasurement.ls_asset_id
        AND las.revision = remeasurement.revision
        AND las.set_of_books_id = remeasurement.set_of_books_id
        AND las.month >= Trunc(remeasurement.remeasurement_date, 'month')
        )
		where lower(dg.mid_period_method) = 'ferc'
		AND lio.ilr_id = A_ILR_ID
		AND lio.revision = A_REVISION
		and las.revision = A_REVISION
		and la.ilr_id = A_ILR_ID
		and las.is_om = 0
		and sob_map.fasb_cap_type_id <> 2  --fasb_cap_type_id 2 = Type B, which always uses SLE method, regardless of depr group setting.
    AND las.MONTH >= Nvl(Trunc(lio.remeasurement_date, 'month'), To_Date('180001', 'yyyymm'));

    PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows inserted into LS_DEPR_FORECAST.');

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	end P_DEPR_FERC_FCST;

	procedure P_GET_LEASE_DEPR( A_ILR_ID in number, A_REVISION in number)
	is
		L_DG_ID DEPR_GROUP.DEPR_GROUP_ID%TYPE;
		L_MSG varchar2(2000);
		L_CC_ID          NUMBER;
		L_DG_CC_IND      varchar2(100);

		type ASSET_REC is record(
			LS_ASSET_ID LS_ASSET.LS_ASSET_ID%type,
			COMPANY_ID COMPANY_SETUP.COMPANY_ID%type,
			GL_ACCOUNT_ID GL_ACCOUNT.GL_ACCOUNT_ID%type,
			MAJOR_LOCATION_ID MAJOR_LOCATION.MAJOR_LOCATION_ID%type,
			UTILITY_ACCOUNT_ID UTILITY_ACCOUNT.UTILITY_ACCOUNT_ID%type,
			BUS_SEGMENT_ID BUSINESS_SEGMENT.BUS_SEGMENT_ID%type,
			SUB_ACCOUNT_ID SUB_ACCOUNT.SUB_ACCOUNT_ID%type,
			VINTAGE number,
			ASSET_LOCATION_ID ASSET_LOCATION.ASSET_LOCATION_ID%type,
			LOCATION_TYPE_ID LOCATION_TYPE.LOCATION_TYPE_ID%type,
			RETIREMENT_UNIT_ID RETIREMENT_UNIT.RETIREMENT_UNIT_ID%type,
			PROPERTY_UNIT_ID PROPERTY_UNIT.PROPERTY_UNIT_ID%type,
			CC_VALUE LS_ASSET_CLASS_CODE.VALUE%type
		);

		type ASSET_TABLE is table of ASSET_REC index by pls_integer;
		L_ASSET_TABLE ASSET_TABLE;
	begin
      --start log back up again
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());

		L_MSG   := 'Finding depr group';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

		L_DG_CC_IND := pkg_pp_system_control.f_pp_system_control('Lease Depr Group Class Code');

		if nvl(L_DG_CC_IND, 'none') = 'none' then
		    L_CC_ID := -1;
		else
		    begin
		    select CLASS_CODE_ID
		    into L_CC_ID
		    from CLASS_CODE
		    where lower(trim(description)) = pkg_pp_system_control.f_pp_system_control('DEPR GROUP CLASS CODE');

		    exception when no_data_found then
		      L_CC_ID := -1;
		    end;
		end if;

		-- get attributes required to find depreciation group and calc depreciation
    /* WMD */

		select LA.LS_ASSET_ID, LA.COMPANY_ID, IA.CAP_ASSET_ACCOUNT_ID AS GL_ACCOUNT_ID,
			AL.MAJOR_LOCATION_ID, LA.UTILITY_ACCOUNT_ID, LA.BUS_SEGMENT_ID,
			LA.SUB_ACCOUNT_ID, TO_NUMBER(TO_CHAR(LI.EST_IN_SVC_DATE, 'YYYY')),
			LA.ASSET_LOCATION_ID, ML.LOCATION_TYPE_ID, LA.RETIREMENT_UNIT_ID,
			RU.PROPERTY_UNIT_ID, nvl(decode(L_DG_CC_IND, 'mla', LCC.VALUE, 'ilr', ICC.VALUE, 'asset', ACC.VALUE, 'NO CLASS CODE'), 'NO CLASS CODE')
		bulk collect
		into L_ASSET_TABLE
		from LS_ILR_ACCOUNT IA, LS_ASSET LA, ASSET_LOCATION AL, MAJOR_LOCATION ML,
			LS_ILR LI, RETIREMENT_UNIT RU, LS_LEASE LL,
			(select * from LS_MLA_CLASS_CODE where CLASS_CODE_ID = L_CC_ID) LCC,
			(select * from LS_ILR_CLASS_CODE where CLASS_CODE_ID = L_CC_ID) ICC,
			(select * from LS_ASSET_CLASS_CODE where CLASS_CODE_ID = L_CC_ID) ACC
		where IA.ILR_ID = A_ILR_ID
		and LI.ILR_ID = A_ILR_ID
         and LA.ILR_ID = A_ILR_ID
		and LA.ASSET_LOCATION_ID = AL.ASSET_LOCATION_ID
		and AL.MAJOR_LOCATION_ID = ML.MAJOR_LOCATION_ID
		and LA.RETIREMENT_UNIT_ID = RU.RETIREMENT_UNIT_ID
		and LL.LEASE_ID = LI.LEASE_ID
		and LL.LEASE_ID = LCC.LEASE_ID (+)
		and LI.ILR_ID = ICC.ILR_ID (+)
		and LA.LS_ASSET_ID = ACC.LS_ASSET_ID (+)
		;


		L_MSG   := 'Updating Depr Group on LS Asset';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      forall ndx in indices of L_ASSET_TABLE
			update ls_asset la
			set depr_group_id = PP_DEPR_PKG.F_FIND_DEPR_GROUP
									(
										L_ASSET_TABLE(ndx).COMPANY_ID,
										L_ASSET_TABLE(ndx).GL_ACCOUNT_ID,
										L_ASSET_TABLE(ndx).MAJOR_LOCATION_ID,
										L_ASSET_TABLE(ndx).UTILITY_ACCOUNT_ID,
										L_ASSET_TABLE(ndx).BUS_SEGMENT_ID,
										L_ASSET_TABLE(ndx).SUB_ACCOUNT_ID,
										-100, --SUBLEDGER_TYPE_ID
										L_ASSET_TABLE(ndx).VINTAGE,
										L_ASSET_TABLE(ndx).ASSET_LOCATION_ID,
										L_ASSET_TABLE(ndx).LOCATION_TYPE_ID,
										L_ASSET_TABLE(ndx).RETIREMENT_UNIT_ID,
										L_ASSET_TABLE(ndx).PROPERTY_UNIT_ID,
										L_CC_ID, --A_CLASS_CODE_ID
										L_ASSET_TABLE(ndx).CC_VALUE
									)
			where la.ls_asset_id = L_ASSET_TABLE(ndx).LS_ASSET_ID
			AND PP_DEPR_PKG.F_FIND_DEPR_GROUP
									(
										L_ASSET_TABLE(ndx).COMPANY_ID,
										L_ASSET_TABLE(ndx).GL_ACCOUNT_ID,
										L_ASSET_TABLE(ndx).MAJOR_LOCATION_ID,
										L_ASSET_TABLE(ndx).UTILITY_ACCOUNT_ID,
										L_ASSET_TABLE(ndx).BUS_SEGMENT_ID,
										L_ASSET_TABLE(ndx).SUB_ACCOUNT_ID,
										-100, --SUBLEDGER_TYPE_ID
										L_ASSET_TABLE(ndx).VINTAGE,
										L_ASSET_TABLE(ndx).ASSET_LOCATION_ID,
										L_ASSET_TABLE(ndx).LOCATION_TYPE_ID,
										L_ASSET_TABLE(ndx).RETIREMENT_UNIT_ID,
										L_ASSET_TABLE(ndx).PROPERTY_UNIT_ID,
										L_CC_ID, --A_CLASS_CODE_ID
										L_ASSET_TABLE(ndx).CC_VALUE
									) > 0
			;

      PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows on ls_asset updating with a depr group.');

	end P_GET_LEASE_DEPR;

	function f_lease_stg(A_ILR_ID in number, A_REVISION in number)
	return number
	is
      NUM_ROWS     number;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_DEPR.f_lease_stg');
		--
		insert into CPR_DEPR_CALC_STG
		(ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, INIT_LIFE, REMAINING_LIFE,
		 ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS,
		 DEPR_CALC_STATUS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH,
		 SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN,
		 RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS,
		 DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE,
		 YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE, PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB,
		 MONTH_RATE, COMPANY_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID,
		 DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR, SALVAGE_EXPENSE,
		 SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT,
		 IMPAIRMENT_EXPENSE_AMOUNT, END_OF_LIFE, NET_GROSS, OVER_DEPR_CHECK, RATE,
		 EFFECTIVE_DATE, SUBLEDGER_TYPE_ID, ENG_IN_SERVICE_YEAR, TRF_WEIGHT, MIN_MPC, TRUEUP_ADJ,
		 NET_TRF, ACTIVITY, ACTIVITY_3, BEG_RES_AMT, NET_IMP_AMT, EXISTS_TWO_MONTHS, EXISTS_ARO, HAS_CFNU,
		 HAS_NURV, HAS_NURV_LAST_MONTH, EXISTS_LAST_MONTH, NURV_ADJ,
		 IMPAIRMENT_ASSET_ACTIVITY_SALV, IMPAIRMENT_ASSET_BEGIN_BALANCE)
		with lease_assets as
		(
			select las.ls_asset_id, las.set_of_books_id, las.revision, las.month, las.end_capital_cost, la.depr_group_id, la.economic_life, /* WMD */
				dg.mid_period_conv, dg.mid_period_method, la.company_id, dg.depr_method_id,
				 case when la.ESTIMATED_RESIDUAL = 0 and lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual', la.COMPANY_ID))) = 'yes'
					then la.GUARANTEED_RESIDUAL_AMOUNT
					else round(la.ESTIMATED_RESIDUAL * la.FMV, 2) end / las.end_capital_cost as estimated_salvage, /* WMD */
				min("MONTH") over(partition by las.ls_asset_id, las.set_of_books_id, las.revision) as in_service,
				count(1) over(partition by las.ls_asset_id, las.set_of_books_id, las.revision) as init_life,
				count(1) over(partition by las.ls_asset_id, las.set_of_books_id, las.revision) -
				row_number() over(partition by las.ls_asset_id, las.set_of_books_id, las.revision order by las.month) + 1 as remaining_life
			from ls_asset_schedule las, ls_asset la, depr_group dg
			where la.depr_group_id = dg.depr_group_id
			and lower(dg.mid_period_method) not in ('sle', 'ferc', 'sl')
			and dg.subledger_type_id = -100
			and las.ls_asset_id = la.ls_asset_id
			and la.ilr_id = a_ilr_id
			and las.revision = a_revision
			and las.is_om = 0
			and las.end_capital_cost <> 0 /* WMD */
		), DEPR_METHOD_RATES_VIEW as
		 (select DD.DEPR_METHOD_ID as DEPR_METHOD_ID,
			 DD.SET_OF_BOOKS_ID,
			 DD.EFFECTIVE_DATE,
			 DD.RATE,
			 DD.OVER_DEPR_CHECK,
			 DD.NET_GROSS,
			 DD.END_OF_LIFE,
			 ROW_NUMBER() OVER(partition by DD.DEPR_METHOD_ID, DD.SET_OF_BOOKS_ID order by DD.EFFECTIVE_DATE desc) as THE_ROW
		  from DEPR_METHOD_RATES DD
		   where DD.EFFECTIVE_DATE <= sysdate)
		select la.ls_asset_id,
			 la.SET_OF_BOOKS_ID,
			 la."MONTH",
			 nvl(la.INIT_LIFE,0),
			 nvl(la.REMAINING_LIFE,0),
			 nvl(la.ESTIMATED_SALVAGE,0),
			 nvl(la.end_capital_cost,0), /* WMD */
			 0, 0, 2,0,0,
			 nvl(la.end_capital_cost,0), /* WMD */
			 0,0,0,0,0,0,0,0,0,
			 0,--A.DEPRECIATION_BASE,
			 0,--CURR_DEPR_EXPENSE
			 0,0,0,0,0,0,
			 null,
			 0,
			 la.COMPANY_ID,
			 lower(trim(la.MID_PERIOD_METHOD)),
			 la.mid_period_conv MID_PERIOD_CONV,
			 la.depr_group_id DEPR_GROUP_ID,
			 0,--DEPR_EXP_ALLOC_ADJUST,
			 la.depr_method_id DEPR_METHOD_ID,
			 0 TRUE_UP_CPR_DEPR,
			 0,--A.SALVAGE_EXPENSE,
			 0, 0,--SALVAGE_EXP_ALLOC_ADJUST,
			 0, 0,
			 c.END_OF_LIFE, c.NET_GROSS, c.OVER_DEPR_CHECK, nvl(c.RATE,0), c.EFFECTIVE_DATE,
			 -100,
			 la.in_service,
			 DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
											  la.COMPANY_ID),
					'no')),
				'no',
				1,
				0),
		   DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('CPR DEPR: Allow Exp in Month Added',
											  la.COMPANY_ID),
					'yes')),
				'no',
				0,
				-1
				),
			0,--TRUEUP_ADJ
			0,0,0,0,0,
			0,--EXISTS_TWO_MONTHS
			0,--EXISTS_ARO
			0, --HAS_CFNU
			0, --HAS_NURV
			0, --HAS_NURV_LAST_MONTH
			case when la.remaining_life = la.init_life then 0 else 1 end, --EXISTS_LAST_MONTH
			0, --NuRV_ADJ
			0, 0
		  from DEPR_METHOD_RATES_VIEW c, lease_assets la
		 where la.depr_method_id = c.depr_method_id
		 and la.set_of_books_id = c.set_of_books_id
		 and c.THE_ROW = 1;

       NUM_ROWS := sql%rowcount;

       PKG_PP_LOG.P_WRITE_MESSAGE(NUM_ROWS||' rows inserted into CPR_DEPR_CALC_STG.');

		 return NUM_ROWS;

		 PKG_PP_ERROR.REMOVE_MODULE_NAME;
	end f_lease_stg;

	procedure p_lease_calc
	is
	begin
		--
		PKG_DEPR_IND_CALC.P_DEPRCALC_LEASE();
	end p_lease_calc;

	procedure p_lease_results(A_ILR_ID in number, A_REVISION in number)
	is
	begin
		--
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_DEPR.p_lease_results');

		insert into LS_DEPR_FORECAST
		(LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, MONTH, begin_reserve, depr_expense, depr_exp_alloc_adjust, end_reserve)
		select c.asset_id, c.set_of_books_id, a_revision, c.gl_posting_mo_yr,
			c.beg_reserve_month, c.curr_depr_expense, c.depr_exp_alloc_adjust, c.depr_reserve
		from cpr_depr_calc_stg c
		;
      PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows inserted into LS_DEPR_FORECAST.');

		delete from cpr_depr_calc_stg;
      PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows deleted from cpr_depr_calc_stg.');

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	end p_lease_results;

  --**************************************************************************
  --                            P_fcst_lease
  --**************************************************************************
  PROCEDURE P_fcst_lease( a_ilr_id   IN NUMBER,
                          a_revision IN NUMBER )
  IS
  BEGIN
      DELETE FROM LS_DEPR_INFO_TMP;

      INSERT INTO LS_DEPR_INFO_TMP
                  (ilr_id,
                   ls_asset_id,
                   set_of_books_id,
                   revision,
                   month,
                   end_capital_cost,
                   depr_group_id,
                   economic_life,
                   set_of_books,
                   mid_period_conv,
                   mid_period_method,
                   fasb_cap_type_id)
      SELECT a_ilr_id,
             ls_asset_id,
             set_of_books_id,
             revision,
             month,
             end_capital_cost,
             depr_group_id,
             economic_life,
             set_of_books,
             mid_period_conv,
             mid_period_method,
             fasb_cap_type_id
      FROM   ( SELECT las.ls_asset_id,
                      las.set_of_books_id,
                      las.revision,
                      las.month,
                      las.end_capital_cost,
                      la.depr_group_id,
                      la.economic_life,/* WMD */
                      Row_number( )
                        over(
                          PARTITION BY las.ls_asset_id, las.set_of_books_id, las.revision
                          ORDER BY las.month)       AS the_row,
                      dg.mid_period_conv /* CJS 4/11/17 */,
                      sob.description               set_of_books,
                      Lower( dg.mid_period_method ) mid_period_method,
                      sob_map.fasb_cap_type_id
               FROM   LS_ASSET_SCHEDULE las,
                      LS_ASSET la,
                      DEPR_GROUP dg,
                      LS_ILR_OPTIONS lio,
                      LS_FASB_CAP_TYPE_SOB_MAP sob_map,
                      SET_OF_BOOKS sob
               WHERE  la.depr_group_id = dg.depr_group_id AND
                      las.ls_asset_id = la.ls_asset_id AND
                      la.ilr_id = a_ilr_id AND
                      las.revision = a_revision AND
                      lio.ilr_id = a_ilr_id AND
                      lio.revision = a_revision AND
                      sob_map.lease_cap_type_id = lio.lease_cap_type_id AND
                      sob_map.set_of_books_id = las.set_of_books_id AND
                      las.set_of_books_id = sob.set_of_books_id AND
                      las.revision = a_revision AND
                      la.ilr_id = a_ilr_id )
      WHERE  the_row = 1;

      P_fcst_lease_use_temp_table( a_ilr_id, a_revision );

      DELETE FROM LS_DEPR_INFO_TMP;

  END p_fcst_lease;

  --**************************************************************************
  --                            P_fcst_lease_use_temp_table
  --**************************************************************************
  PROCEDURE P_fcst_lease_use_temp_table( a_ilr_id   NUMBER,
                                         a_revision NUMBER )
  IS
    l_msg               VARCHAR2(2000);
    l_check             NUMBER;
    l_mid_period_method depr_group.mid_period_method%TYPE;
    l_init_life         NUMBER;
    l_proc_id           NUMBER;
    l_num               NUMBER;
  BEGIN
      pkg_pp_error.Set_module_name( 'PKG_LEASE_DEPR.P_FCST_LEASE' );
      --start log back up again
      pkg_pp_log.P_start_log( p_process_id => pkg_lease_common.F_getprocess( ) );

      pkg_pp_log.P_write_message( 'A_ILR_ID : '
                                  ||a_ilr_id );

      pkg_pp_log.P_write_message( 'A_REVISION : '
                                  ||a_revision );

      DELETE FROM LS_DEPR_FORECAST
      WHERE  revision = a_revision AND
             ls_asset_id IN
             ( SELECT la.ls_asset_id
               FROM   LS_ASSET la
               WHERE  la.ilr_id = a_ilr_id )
      AND MONTH >= Nvl((SELECT Trunc(remeasurement_date, 'month') FROM ls_ilr_options WHERE ilr_id = A_ILR_ID AND revision = A_REVISION), To_Date('180001','yyyymm'));

      pkg_pp_log.P_write_message( SQL%rowcount
                                  ||' rows deleted from LS_DEPR_FORECAST.' );

      -- calc ferc
      pkg_pp_log.P_write_message( 'FERC Calc' );

      pkg_pp_log.P_write_message( 'Entering P_DEPR_FERC_FCST' );

      P_depr_ferc_fcst( a_ilr_id, a_revision );

      -- calc sle
      pkg_pp_log.P_write_message( 'SLE Calc' );

      FOR l_calc_depr IN ( SELECT a_ilr_id,
                                  ls_asset_id,
                                  set_of_books_id,
                                  revision,
                                  month,
                                  end_capital_cost,
                                  depr_group_id,
                                  economic_life,
                                  set_of_books,
                                  mid_period_conv,
                                  mid_period_method
                           FROM   LS_DEPR_INFO_TMP
                           WHERE  ilr_id = a_ilr_id AND
                                  revision = a_revision AND
                                  fasb_cap_type_id = 2 --fasb_cap_type_id 2 = Type B, which always uses SLE method, regardless of depr group setting.
                          ) LOOP
          pkg_pp_log.P_write_message( 'Getting init life' );

          l_init_life := pkg_lease_common.F_get_init_life( l_calc_depr.ls_asset_id, l_calc_depr.revision, a_ilr_id, l_calc_depr.economic_life ).init_life;

          pkg_pp_log.P_write_message( 'init life : '
                                      ||l_init_life );

          pkg_pp_log.P_write_message( 'Entering P_DEPR_SLE for set of books: '
                                      ||l_calc_depr.set_of_books );

          P_depr_sle( l_calc_depr.ls_asset_id, l_calc_depr.revision, 0, l_calc_depr.set_of_books_id, l_calc_depr.month, l_init_life, l_msg, 1 );
      END LOOP;

      -- calc sl
      pkg_pp_log.P_write_message( 'SL Calc' );

      FOR l_calc_depr IN ( SELECT a_ilr_id,
                                  ls_asset_id,
                                  set_of_books_id,
                                  revision,
                                  month,
                                  end_capital_cost,
                                  depr_group_id,
                                  economic_life,
                                  set_of_books,
                                  mid_period_conv,
                                  mid_period_method
                           FROM   LS_DEPR_INFO_TMP
                           WHERE  ilr_id = a_ilr_id AND
                                  revision = a_revision AND
                                  fasb_cap_type_id <> 2 --fasb_cap_type_id 2 = Type B, which always uses SLE method, regardless of depr group setting.
                                  AND
                                  mid_period_method = 'sl' ) LOOP
          pkg_pp_log.P_write_message( 'Getting init life' );

          l_init_life := pkg_lease_common.F_get_init_life( l_calc_depr.ls_asset_id, l_calc_depr.revision, a_ilr_id, l_calc_depr.economic_life ).init_life;

          pkg_pp_log.P_write_message( 'init life : '
                                      ||l_init_life );

          pkg_pp_log.P_write_message( 'Entering P_DEPR_SL for set of books: '
                                      ||l_calc_depr.set_of_books );

          P_depr_sl( l_calc_depr.ls_asset_id, l_calc_depr.revision, 0, l_calc_depr.set_of_books_id, l_calc_depr.month, l_init_life, l_calc_depr.mid_period_conv, l_msg );
      END LOOP;

      -- calc rest of depr
      pkg_pp_log.P_write_message( 'Remaining Depr Methods Calc' );

      pkg_pp_log.P_write_message( 'Entering f_lease_stg' );

      l_num := F_lease_stg( a_ilr_id, a_revision );

      IF l_num > 0 THEN
        pkg_pp_log.P_write_message( 'Entering p_lease_calc' );

        P_lease_calc( );

        pkg_pp_log.P_write_message( 'Entering p_lease_results' );

        P_lease_results( a_ilr_id, a_revision );
      END IF;

      pkg_pp_error.remove_module_name;
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.Raise_error( 'ERROR', 'Failed Operation' );
  END p_fcst_lease_use_temp_table;


end PKG_LEASE_DEPR;
/

--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (5122, 0, 2017, 3, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2017.3.0.0_PKG_LEASE_DEPR.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
