/*
||=================================================================================================
|| Application: PowerPlant
|| Object Name: PKG_LESSOR_COMMON
|| Description:
||=================================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||=================================================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- --------------    ----------------------------------------------------------
|| 2017.1.0.0 11/01/2017 C.Shilling	   Create package
|| 2017.3.0.0 01/02/2018 Anand R	   PP-50272 Add Lessor version of F_BOOKJE and F_MC_BOOKJE
|| 2018.3.0.0 04/22/2018 B. Beck	   Add Asset Re-Recognition
||=================================================================================================
*/
CREATE OR REPLACE PACKAGE pkg_lessor_common AS
	G_PKG_VERSION varchar(35) := '2018.2.1.0';
	procedure p_startLog (a_process_description varchar2);
	
	-- function the return the process id for lessor calcs
	FUNCTION f_get_process_id(a_description pp_processes.description%TYPE)
		RETURN pp_processes.process_id%TYPE;
	
	--overlaoaded function with A_ORIG for the ORIGINATOR column on GL_TRANSACTION
   function F_BOOKJE(A_LSR_ILR_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_JE_METHOD_ID    in number,
                     A_AMOUNT_TYPE     in number,
                     A_REVERSAL_CONVENTION in number,
                     A_ORIG          in varchar2,
                     A_MSG           out varchar2) return number;

   -- Function to perform booking of LESSEE JEs (Multicurrencies)
   function F_BOOKJE(A_LSR_ILR_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
                        A_JE_METHOD_ID    in number,
                        A_AMOUNT_TYPE     in number,
                        A_REVERSAL_CONVENTION in number,
                        A_MSG           out varchar2) return number;


   -- Function to perform booking of LESSOR JEs (Multicurrencies)
   function F_MC_BOOKJE(A_LSR_ILR_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
                        A_JE_METHOD_ID    in number,
                        A_AMOUNT_TYPE     in number,
                        A_REVERSAL_CONVENTION in number,
                        A_EXCHANGE_RATE in number,
                        A_CURRENCY_FROM in number,
                        A_CURRENCY_TO   in number,
                        A_MSG           out varchar2) return number;

   --overlaoaded function with A_ORIG for the ORIGINATOR column on GL_TRANSACTION (Multicurrencies)
   function F_MC_BOOKJE(A_LSR_ILR_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
                        A_JE_METHOD_ID    in number,
                        A_AMOUNT_TYPE     in number,
                        A_REVERSAL_CONVENTION in number,
                        A_EXCHANGE_RATE in number,
                        A_CURRENCY_FROM in number,
                        A_CURRENCY_TO   in number,
                        A_ORIG          in varchar2,
                        A_MSG           out varchar2) return number;
		
	TYPE ilr_schedule_line IS RECORD (
		ilr_id				  lsr_ilr_schedule_sales_direct.ilr_id%TYPE,
		set_of_books_id		  lsr_ilr_schedule_sales_direct.set_of_books_id%TYPE,
		rownumber			  INTEGER,
		company_id			  lsr_ilr.company_id%type,
		month 				  lsr_ilr_schedule.month%type,
		principal_received	  lsr_ilr_schedule_sales_direct.principal_received%TYPE,
		interest_income	      lsr_ilr_schedule.interest_income_received%TYPE,
		interest_unguaranteed lsr_ilr_schedule_sales_direct.interest_unguaranteed_residual %TYPE,
		recognized_profit     lsr_ilr_schedule_direct_fin.recognized_profit %TYPE,
		initial_direct_cost   lsr_ilr_schedule.initial_direct_cost %TYPE,
		executory1 			  lsr_ilr_schedule.executory_paid1%TYPE,
		executory2 			  lsr_ilr_schedule.executory_paid2%TYPE,
		executory3 			  lsr_ilr_schedule.executory_paid3%TYPE,
		executory4 			  lsr_ilr_schedule.executory_paid4%TYPE,
		executory5 			  lsr_ilr_schedule.executory_paid5%TYPE,
		executory6 			  lsr_ilr_schedule.executory_paid6%TYPE,
		executory7 			  lsr_ilr_schedule.executory_paid7%TYPE,
		executory8 			  lsr_ilr_schedule.executory_paid8%TYPE,
		executory9 			  lsr_ilr_schedule.executory_paid9%TYPE,
		executory10			  lsr_ilr_schedule.executory_paid10%TYPE,
		contingent1			  lsr_ilr_schedule.contingent_paid1%TYPE,
		contingent2			  lsr_ilr_schedule.contingent_paid2%TYPE,
		contingent3			  lsr_ilr_schedule.contingent_paid3%TYPE,
		contingent4			  lsr_ilr_schedule.contingent_paid4%TYPE,
		contingent5			  lsr_ilr_schedule.contingent_paid5%TYPE,
		contingent6			  lsr_ilr_schedule.contingent_paid6%TYPE,
		contingent7			  lsr_ilr_schedule.contingent_paid7%TYPE,
		contingent8			  lsr_ilr_schedule.contingent_paid8%TYPE,
		contingent9			  lsr_ilr_schedule.contingent_paid9%TYPE,
		contingent10		  lsr_ilr_schedule.contingent_paid10%TYPE,
		accrued_rent		  lsr_ilr_schedule.accrued_rent%TYPE,
		deferred_rent		  lsr_ilr_schedule.deferred_rent%TYPE
	);
	
	
	
	TYPE ilr_schedule_line_table IS TABLE OF ilr_schedule_line INDEX BY PLS_INTEGER;
	
	
	TYPE asset_rerecognition IS RECORD (
		ldg_asset_id		  	pend_transaction.ldg_asset_id%TYPE,
		ldg_activity_id			pend_transaction.ldg_activity_id%TYPE,
		ldg_depr_group_id		pend_transaction.ldg_depr_group_id%TYPE,
		books_schema_id			pend_transaction.books_schema_id%TYPE,
		retirement_unit_id		pend_transaction.retirement_unit_id%TYPE,
		utility_account_id		pend_transaction.utility_account_id%TYPE, 
		bus_segment_id			pend_transaction.bus_segment_id%TYPE, 
		func_class_id			pend_transaction.func_class_id%TYPE, 
		sub_account_id			pend_transaction.sub_account_id%TYPE, 
		asset_location_id		pend_transaction.asset_location_id%TYPE, 
		gl_account_id			pend_transaction.gl_account_id%TYPE, 
		company_id				pend_transaction.company_id%TYPE,
		gl_posting_mo_yr		pend_transaction.gl_posting_mo_yr%TYPE,
		subledger_indicator		pend_transaction.subledger_indicator%TYPE, 
		activity_code			pend_transaction.activity_code%TYPE, 
		work_order_number		pend_transaction.work_order_number%TYPE, 
		posting_quantity		pend_transaction.posting_quantity%TYPE,
		user_id1				pend_transaction.user_id1%TYPE,
		posting_amount			pend_transaction.posting_amount%TYPE,
		in_service_year			pend_transaction.in_service_year%TYPE,
		description				pend_transaction.description%TYPE,
		long_description		pend_transaction.long_description%TYPE,
		property_group_id		pend_transaction.property_group_id%TYPE,
		retire_method_id		pend_transaction.retire_method_id%TYPE,
		posting_status			pend_transaction.posting_status%TYPE,
		cost_of_removal			pend_transaction.cost_of_removal%TYPE,
		salvage_cash			pend_transaction.salvage_cash%TYPE,
		salvage_returns			pend_transaction.salvage_returns%TYPE,
		reserve					pend_transaction.reserve%TYPE,
		misc_description		pend_transaction.misc_description%TYPE,
		ferc_activity_code		pend_transaction.ferc_activity_code%TYPE,
		serial_number			pend_transaction.serial_number%TYPE,
		reserve_credits			pend_transaction.reserve_credits%TYPE,
		gain_loss_reversal		pend_transaction.gain_loss_reversal%TYPE,
		disposition_code		pend_transaction.disposition_code%TYPE,
		asset_activity_id		cpr_activity.asset_activity_id%TYPE,
		orig_posting_amount 	pend_transaction.posting_amount%TYPE,
		set_of_books_id		  	set_of_books.set_of_books_id%TYPE
	);
	TYPE asset_rerecognition_table IS TABLE OF asset_rerecognition INDEX BY PLS_INTEGER;
	
	/*****************************************************************************
	* Procedure: p_rerecognize_cpr_assets
	* PURPOSE: Creates pending transactions for POST to pick up to re-recognize an asset after it had been
	*					de-recognized due to creating a Sales Type or Direct Finance Lease
	* PARAMETERS:
	*   a_asset_rerecognition_tbl: A table of ILR records to re-recognize
	*
	* RETURNS: NONE
	******************************************************************************/
	PROCEDURE p_rerecognize_cpr_assets(a_asset_rerecognition_tbl pkg_lessor_common.asset_rerecognition_table); 
	
	/*****************************************************************************
	* Procedure: f_approvedclassification_dfst
	* PURPOSE: Determines if this ILR is a reclassificationfrom DF -> ST or ST -> DF
	* PARAMETERS:
	*   a_ilr_id: The ID of the ILR we are currently building. Used to filter the prior schedule records.
	*   a_revision: The revision of the ILR we are currently building.
	*   a_set_of_books_id: The set of books id to check
	*   a_cap_type_id: The "new / current" cap type id
	*
	* RETURNS: number: 1 if is DF->ST or ST->DF 0 otherwise
	******************************************************************************/
	function f_approvedclassification_dfst(a_ilr_id NUMBER,
                                      a_revision NUMBER,
									  a_set_of_books_id NUMBER,
									  a_cap_type_id NUMBER)
	RETURN number;
	
END pkg_lessor_common;
/


CREATE OR REPLACE PACKAGE BODY pkg_lessor_common AS
	--**************************************************************
	--         functions
	--**************************************************************
	-- returns the process id for lease calc
	FUNCTION f_get_process_id(a_description pp_processes.description%TYPE)
		RETURN pp_processes.process_id%TYPE
	IS
		l_process_id pp_processes.process_id%TYPE;
	BEGIN
		SELECT process_id
		INTO l_process_id
		FROM pp_processes
		WHERE description = a_description;

		RETURN l_process_id;
	EXCEPTION
		WHEN No_Data_Found THEN
			Raise_Application_Error(-20000, 'The description passed to PKG_LESSOR_COMMON.F_GET_PROCESS_ID (''' || a_description || ''') was not found.');
	END f_get_process_id;
	
    --Wrapper for the overloaded version
   function F_BOOKJE(A_LSR_ILR_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_JE_METHOD_ID    in number,
                     A_AMOUNT_TYPE     in number,
                     A_REVERSAL_CONVENTION in number,
                     A_MSG           out varchar2) return number is

   begin

      return F_BOOKJE(A_LSR_ILR_ID, A_TRANS_TYPE, A_AMT, A_GL_ACCOUNT_ID, A_GAIN_LOSS, 
                      A_COMPANY_ID, A_MONTH, A_DR_CR, A_GL_JC, A_SOB_ID, A_JE_METHOD_ID, A_AMOUNT_TYPE,
                       A_REVERSAL_CONVENTION, '', A_MSG);
   exception
      when others then
         return -1;
   end F_BOOKJE;
   
   --**************************************************************************
   --                            F_BOOKJE
   --**************************************************************************
   function F_BOOKJE(A_LSR_ILR_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_JE_METHOD_ID    in number,
                     A_AMOUNT_TYPE     in number,
                     A_REVERSAL_CONVENTION in number,
                     A_ORIG          in varchar2,
                     A_MSG           out varchar2) return number is
      L_GL_ACCOUNT varchar2(2000);

   begin

      A_MSG       := 'Starting JE Creation for trans_type: ' || TO_CHAR(A_TRANS_TYPE);

         L_GL_ACCOUNT := PP_GL_TRANSACTION(A_TRANS_TYPE,
                                            '',
                                            '',
                                            '',
                                            '',
                                            A_GL_ACCOUNT_ID,
                                            A_GAIN_LOSS,
                                            '',
                                            A_JE_METHOD_ID,
                                            A_SOB_ID,
                                            A_AMOUNT_TYPE,
                                            A_REVERSAL_CONVENTION,
                                            A_LSR_ILR_ID);

         if LOWER(L_GL_ACCOUNT) like '%error%' then
            A_MSG := L_GL_ACCOUNT;
            return - 1;
         end if;


         A_MSG := 'Inserting into gl_transaction: ' || TO_CHAR(A_TRANS_TYPE);
         insert into GL_TRANSACTION
            (GL_TRANS_ID, "MONTH", COMPANY_NUMBER, GL_ACCOUNT, DEBIT_CREDIT_INDICATOR, AMOUNT,
             GL_JE_CODE, GL_STATUS_ID, DESCRIPTION, source, ORIGINATOR, COMMENTS, PEND_TRANS_ID,
             ASSET_ID, AMOUNT_TYPE, JE_METHOD_ID, TAX_ORIG_MONTH_NUMBER, TRANS_TYPE)
            select PWRPLANT1.NEXTVAL,
                   A_MONTH,
                   C.GL_COMPANY_NO,
                   L_GL_ACCOUNT,
                   A_DR_CR,
                   A_AMT * NVL(A_REVERSAL_CONVENTION,1),
                   A_GL_JC,
                   to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', A_COMPANY_ID)), 1)),
                   'TRANS TYPE: ' || TO_CHAR(A_TRANS_TYPE),
                   'LESSOR',
                   A_ORIG,
                   '',
                   '',
                   '',
                   A_AMOUNT_TYPE,
                   A_JE_METHOD_ID,
                   null,
				           A_TRANS_TYPE
              from COMPANY_SETUP C
             where C.COMPANY_ID = A_COMPANY_ID;

      return 1;
   exception
      when others then
		 a_msg:=SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         return -1;
   end F_BOOKJE;
	
   --Wrapper for the overloaded version (Multicurrencies)
   function F_MC_BOOKJE(A_LSR_ILR_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
                        A_JE_METHOD_ID    in number,
                        A_AMOUNT_TYPE     in number,
                        A_REVERSAL_CONVENTION in number,
                        A_EXCHANGE_RATE in number,
                        A_CURRENCY_FROM in number,
                        A_CURRENCY_TO   in number,
                        A_MSG           out varchar2) return number is

   begin

      return F_MC_BOOKJE(A_LSR_ILR_ID, A_TRANS_TYPE, A_AMT, A_GL_ACCOUNT_ID, A_GAIN_LOSS, 
                      A_COMPANY_ID, A_MONTH, A_DR_CR, A_GL_JC, A_SOB_ID, A_JE_METHOD_ID, A_AMOUNT_TYPE,
            A_REVERSAL_CONVENTION, A_EXCHANGE_RATE, A_CURRENCY_FROM, A_CURRENCY_TO, '', A_MSG);
   exception
      when others then
         return -1;
   end F_MC_BOOKJE;
   
   --**************************************************************************
   --                            F_MC_BOOKJE (Multicurrencies)
   --**************************************************************************
   function F_MC_BOOKJE(A_LSR_ILR_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_JE_METHOD_ID    in number,
                     A_AMOUNT_TYPE     in number,
                     A_REVERSAL_CONVENTION in number,
                     A_EXCHANGE_RATE in number,
                     A_CURRENCY_FROM in number,
                     A_CURRENCY_TO   in number,
                     A_ORIG          in varchar2,
                     A_MSG           out varchar2) return number is
      L_GL_ACCOUNT varchar2(2000);
      L_GL_TRANS_ID number;
	  L_CONTRACT_JES  varchar2(100);
	  L_JE_METHOD_SOB_CHECK number;

   begin

   	  A_MSG       := 'Getting Contract Currency JE System Control';
	  L_CONTRACT_JES:=lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_company('Lease: Contract Currency JEs', A_COMPANY_ID)));
	  
	  if L_CONTRACT_JES IS NULL then
		L_CONTRACT_JES := 'no';
	  end if;
	  
	  if L_CONTRACT_JES = 'yes' then
		A_MSG := 'Checking JE Method Set of Books' ;
		L_JE_METHOD_SOB_CHECK := PKG_LEASE_COMMON.F_JE_METHOD_SOB_CHECK(A_MSG);
		
		if L_JE_METHOD_SOB_CHECK > 0 then
		      A_MSG := 'You cannot book contract currency JEs with more than one set of books configured per JE Method';
			  return -1;
		end if;
	  end if;

   
      A_MSG       := 'Starting JE Creation for trans_type: ' || TO_CHAR(A_TRANS_TYPE);

         L_GL_ACCOUNT := PP_GL_TRANSACTION(A_TRANS_TYPE,
                                            '',
                                            '',
                                            '',
                                            '',
                                            A_GL_ACCOUNT_ID,
                                            A_GAIN_LOSS,
                                            '',
                                            A_JE_METHOD_ID,
                                            A_SOB_ID,
                                            A_AMOUNT_TYPE,
                                            A_REVERSAL_CONVENTION,
                                            A_LSR_ILR_ID);

         if LOWER(L_GL_ACCOUNT) like '%error%' then
            A_MSG := L_GL_ACCOUNT;
            return - 1;
         end if;

      select PWRPLANT1.NEXTVAL into L_GL_TRANS_ID from dual;

         A_MSG := 'Inserting into gl_transaction: ' || TO_CHAR(A_TRANS_TYPE);
         insert into GL_TRANSACTION
            (GL_TRANS_ID, "MONTH", COMPANY_NUMBER, GL_ACCOUNT, DEBIT_CREDIT_INDICATOR, AMOUNT,
             GL_JE_CODE, GL_STATUS_ID, DESCRIPTION, source, ORIGINATOR, COMMENTS, PEND_TRANS_ID,
             ASSET_ID, AMOUNT_TYPE, JE_METHOD_ID, TAX_ORIG_MONTH_NUMBER, TRANS_TYPE)
            select L_GL_TRANS_ID,
                   A_MONTH,
                   C.GL_COMPANY_NO,
                   L_GL_ACCOUNT,
                   A_DR_CR,
                   A_AMT * decode(L_CONTRACT_JES, 'yes', 1, A_EXCHANGE_RATE) * NVL(A_REVERSAL_CONVENTION,1),
                   A_GL_JC,
                   to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', A_COMPANY_ID)), 1)),
                   'TRANS TYPE: ' || TO_CHAR(A_TRANS_TYPE) || '; ILR ID: ' || TO_CHAR(A_LSR_ILR_ID),
                   'LESSOR',
                   A_ORIG,
                   '',
                   '',
                   '',
                   A_AMOUNT_TYPE,
                   A_JE_METHOD_ID,
                   null,
                   A_TRANS_TYPE
              from COMPANY_SETUP C
             where C.COMPANY_ID = A_COMPANY_ID;

         A_MSG := 'Inserting into ls_mc_gl_transaction_audit: ' || TO_CHAR(A_TRANS_TYPE);
     insert into LS_MC_GL_TRANSACTION_AUDIT
        (COMPANY_ID, GL_POSTING_MO_YR, GL_TRANS_ID, TRANS_TYPE, GL_JE_CODE,
      FROM_CURRENCY, TO_CURRENCY, EXCHANGE_RATE, CALCULATED_AMOUNT, TRANSLATED_AMOUNT, ILR_ID, LS_CURRENCY_TYPE_ID)
         select A_COMPANY_ID,
              A_MONTH,
          L_GL_TRANS_ID,
          A_TRANS_TYPE,
          A_GL_JC,
          A_CURRENCY_FROM,
          A_CURRENCY_TO,
          A_EXCHANGE_RATE,
          A_AMT * NVL(A_REVERSAL_CONVENTION,1),
          A_AMT * decode(L_CONTRACT_JES, 'yes', 1, A_EXCHANGE_RATE) * NVL(A_REVERSAL_CONVENTION,1),
		  A_LSR_ILR_ID,
		  decode(L_CONTRACT_JES, 'yes', 1, 2)
      from DUAL;

      return 1;
   exception
      when others then
     a_msg:=SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         return -1;
   end F_MC_BOOKJE;
 
   procedure p_startLog (a_process_description varchar2) is 
   begin
		--Start log for passed in process
		pkg_pp_log.p_start_log(pkg_lessor_common.f_get_Process_id(a_process_description));
	
   end p_startLog;
   

	/*****************************************************************************
	* Procedure: p_rerecognize_cpr_assets
	* PURPOSE: Creates pending transactions for POST to pick up to re-recognize an asset after it had been
	*					de-recognized due to creating a Sales Type or Direct Finance Lease
	* PARAMETERS:
	*   a_asset_rerecognition_tbl: A table of ILR records to re-recognize
	*
	* RETURNS: NONE
	******************************************************************************/
	PROCEDURE p_rerecognize_cpr_assets(a_asset_rerecognition_tbl pkg_lessor_common.asset_rerecognition_table)
	IS
		l_pend_trans_id 	pend_transaction.pend_trans_id%TYPE;
		l_ratio				NUMBER;
		l_pt_num_rows		NUMBER;
		l_pb_num_rows		NUMBER;
		l_round_err_rows	NUMBER;
		l_msg 				VARCHAR2(2000);
        L_GL_JE_CODE 		VARCHAR2(35);
		l_sum_basis_check	pend_transaction.posting_amount%TYPE;
		asset				pkg_lessor_common.asset_rerecognition;
	BEGIN	
		pkg_pp_log.p_write_message('Starting p_rerecognize_cpr_assets');

		L_MSG:='Retrieving L_GL_JE_CODE';
		select gl_je_code
		into L_GL_JE_CODE
		from standard_journal_entries, gl_je_control
		where standard_journal_entries.je_id = gl_je_control.je_id
		and upper(ltrim(rtrim(process_id))) = 'LSR RERECOGNITION';

		IF L_GL_JE_CODE IS NULL THEN
		   L_MSG := 'Error Retrieving L_GL_JE_CODE';
		   PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
		   RETURN ;
		END IF;

		l_pt_num_rows := 0;
		l_pb_num_rows := 0;
		l_round_err_rows := 0;
		
		--loop over eligible ILRs and their assets
		FOR i IN a_asset_rerecognition_tbl.FIRST .. a_asset_rerecognition_tbl.LAST LOOP
			asset := a_asset_rerecognition_tbl(i);
		
			--get new pend_trans_id
			l_pend_trans_id := pwrplant1.NEXTVAL;
			--insert into pend_transaction
			INSERT INTO pend_transaction (pend_trans_id, ldg_asset_id, ldg_activity_id, ldg_depr_group_id, books_schema_id, retirement_unit_id, utility_account_id,
				bus_segment_id, func_class_id, sub_account_id, asset_location_id, gl_account_id, company_id, gl_posting_mo_yr, subledger_indicator, activity_code,
				gl_je_code, work_order_number, posting_quantity, user_id1, posting_amount, in_service_year, description, long_description, property_group_id,
				retire_method_id, posting_status, cost_of_removal, salvage_cash, salvage_returns, reserve, misc_description, ferc_activity_code, reserve_credits,
				gain_loss_reversal, disposition_code)
			VALUES (l_pend_trans_id, asset.ldg_asset_id, asset.ldg_activity_id, asset.ldg_depr_group_id, asset.books_schema_id, asset.retirement_unit_id, asset.utility_account_id,
				asset.bus_segment_id, asset.func_class_id, asset.sub_account_id, asset.asset_location_id, asset.gl_account_id, asset.company_id, asset.gl_posting_mo_yr, asset.subledger_indicator, asset.activity_code,
				l_gl_je_code, asset.work_order_number, asset.posting_quantity * -1, asset.user_id1, asset.posting_amount, asset.in_service_year, asset.description, asset.long_description, asset.property_group_id,
				asset.retire_method_id, asset.posting_status, asset.cost_of_removal, asset.salvage_cash, asset.salvage_returns, asset.reserve, asset.misc_description, asset.ferc_activity_code, asset.reserve_credits,
				asset.gain_loss_reversal, asset.disposition_code);

			l_pt_num_rows := l_pt_num_rows + SQL%rowcount;

			l_ratio := 1;
			if asset.orig_posting_amount <> 0 then
				l_ratio := asset.posting_amount / asset.orig_posting_amount;
			end if;

			--insert into pend_basis
			INSERT INTO pend_basis(pend_trans_id, basis_1, basis_2, basis_3, basis_4, basis_5, basis_6, basis_7, basis_8, basis_9, basis_10,
				basis_11, basis_12, basis_13, basis_14, basis_15, basis_16, basis_17, basis_18, basis_19, basis_20,
				basis_21, basis_22, basis_23, basis_24, basis_25, basis_26, basis_27, basis_28, basis_29, basis_30,
				basis_31, basis_32, basis_33, basis_34, basis_35, basis_36, basis_37, basis_38, basis_39, basis_40,
				basis_41, basis_42, basis_43, basis_44, basis_45, basis_46, basis_47, basis_48, basis_49, basis_50,
				basis_51, basis_52, basis_53, basis_54, basis_55, basis_56, basis_57, basis_58, basis_59, basis_60,
				basis_61, basis_62, basis_63, basis_64, basis_65, basis_66, basis_67, basis_68, basis_69, basis_70)
			select 
				l_pend_trans_id,
				l_ratio * cab.basis_1, l_ratio * cab.basis_2, l_ratio * cab.basis_3, l_ratio * cab.basis_4, l_ratio * cab.basis_5, l_ratio * cab.basis_6, l_ratio * cab.basis_7, l_ratio * cab.basis_8,
				l_ratio * cab.basis_9, l_ratio * cab.basis_10, l_ratio * cab.basis_11, l_ratio * cab.basis_12, l_ratio * cab.basis_13, l_ratio * cab.basis_14, l_ratio * cab.basis_15, l_ratio * cab.basis_16,
				l_ratio * cab.basis_17, l_ratio * cab.basis_18, l_ratio * cab.basis_19, l_ratio * cab.basis_20, l_ratio * cab.basis_21, l_ratio * cab.basis_22, l_ratio * cab.basis_23, l_ratio * cab.basis_24,
				l_ratio * cab.basis_25, l_ratio * cab.basis_26, l_ratio * cab.basis_27, l_ratio * cab.basis_28, l_ratio * cab.basis_29, l_ratio * cab.basis_30, l_ratio * cab.basis_31, l_ratio * cab.basis_32,
				l_ratio * cab.basis_33, l_ratio * cab.basis_34, l_ratio * cab.basis_35, l_ratio * cab.basis_36, l_ratio * cab.basis_37, l_ratio * cab.basis_38, l_ratio * cab.basis_39, l_ratio * cab.basis_40,
				l_ratio * cab.basis_41, l_ratio * cab.basis_42, l_ratio * cab.basis_43, l_ratio * cab.basis_44, l_ratio * cab.basis_45, l_ratio * cab.basis_46, l_ratio * cab.basis_47, l_ratio * cab.basis_48,
				l_ratio * cab.basis_49, l_ratio * cab.basis_50, l_ratio * cab.basis_51, l_ratio * cab.basis_52, l_ratio * cab.basis_53, l_ratio * cab.basis_54, l_ratio * cab.basis_55, l_ratio * cab.basis_56,
				l_ratio * cab.basis_57, l_ratio * cab.basis_58, l_ratio * cab.basis_59, l_ratio * cab.basis_60, l_ratio * cab.basis_61, l_ratio * cab.basis_62, l_ratio * cab.basis_63, l_ratio * cab.basis_64,
				l_ratio * cab.basis_65, l_ratio * cab.basis_66, l_ratio * cab.basis_67, l_ratio * cab.basis_68, l_ratio * cab.basis_69, l_ratio * cab.basis_70
			from cpr_act_basis cab
			where cab.asset_id = asset.ldg_asset_id
			and cab.asset_activity_id = asset.asset_activity_id
			;

			l_pb_num_rows := l_pb_num_rows + SQL%rowcount;

			--check that the sum of the basis amounts equals the original posting_amount
			SELECT cost 
			INTO l_sum_basis_check
			FROM cpr_post_basis_amounts_view
			WHERE set_of_books_id = asset.set_of_books_id
			AND pend_trans_id = l_pend_trans_id;

			IF asset.posting_amount - l_sum_basis_check <> 0 THEN
				--apply the difference to basis_1 (this is how POST handles this situation)
				UPDATE pend_basis
				SET basis_1 = basis_1 + asset.posting_amount - l_sum_basis_check
				WHERE pend_trans_id = l_pend_trans_id;

				l_round_err_rows := l_round_err_rows + 1;
			END IF;
		END LOOP;

		pkg_pp_log.p_write_message(l_pt_num_rows || ' rows inserted into pend_transaction.');
		pkg_pp_log.p_write_message(l_pb_num_rows || ' rows inserted into pend_basis.');
		pkg_pp_log.p_write_message(l_round_err_rows || ' pend_basis rounding errors corrected.');
	EXCEPTION
		WHEN OTHERS THEN
		IF SQLCODE BETWEEN -20999 AND -20000 THEN
			ROLLBACK;
			pkg_pp_log.p_write_message(SQLERRM);
			RAISE;
			--PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
		ELSIF SQLCODE = 100 THEN
			ROLLBACK;
			pkg_pp_log.p_write_message('Error creating Lessor Termination Asset Re-recognition pending transactions - ' || SQLERRM || f_get_call_stack);
			Raise_Application_Error(-20000, SubStr('Error creating Lessor Termination Asset Re-recognition pending transactions - ORA-01403 - NO DATA FOUND' || Chr(10) || f_get_call_stack, 1, 2000));
		ELSE
			ROLLBACK;
			pkg_pp_log.p_write_message('Error creating Lessor Termination Asset Re-recognition pending transactions - ORA' || SQLCODE || ' - ' || SQLERRM || Chr(10) || f_get_call_stack);
			RAISE;
		END IF;
	END p_rerecognize_cpr_assets;
   
   
    /*****************************************************************************
	* Procedure: f_approvedclassification_dfst
	* PURPOSE: Determines if this ILR is a reclassificationfrom DF -> ST or ST -> DF
	* PARAMETERS:
	*   a_ilr_id: The ID of the ILR we are currently building. Used to filter the prior schedule records.
	*   a_revision: The revision of the ILR we are currently building.
	*   a_set_of_books_id: The set of books id to check
	*   a_cap_type_id: The "new / current" cap type id
	*
	* RETURNS: number: 1 if is DF->ST or ST->DF 0 otherwise, -1 if error
	******************************************************************************/
	function f_approvedclassification_dfst(a_ilr_id NUMBER,
                                      a_revision NUMBER,
									  a_set_of_books_id NUMBER,
									  a_cap_type_id NUMBER)
	RETURN number
	IS
		ll_approvedClassification number;
		ll_toCheckClassification number;
		ll_prior_approved_revision number;
		ld_approval_date date;
		ll_checkExists number;
	begin
		ll_approvedClassification := 0;
		ll_toCheckClassification := 0;

		--only do this for ILRS not being initiated
		select count(1)
		into ll_checkExists
		from lsr_ilr_options
		where lsr_ilr_options.ilr_id = a_ilr_id
		;

		if ll_checkExists > 0 then
			select sob.fasb_cap_type_id
			into ll_toCheckClassification
			from lsr_fasb_type_sob sob
			where sob.cap_type_id = a_cap_type_id
			and sob.set_of_books_id = a_set_of_books_id
			;
			
			-- get the approval date for this revision.
			-- if no approval date, then use system date
			select nvl(
			(
				select approval_date
				from lsr_ilr_approval
				where approval_status_id = 3
				and revision = a_revision
				and ilr_id = a_ilr_id
				), sysdate
			)
			into ld_approval_date
			from dual;
			
			-- find the latest approved revision less than this approval date.
			-- if none exist, then use this revision
			select nvl(
				(
				select max(revision) keep(dense_rank first order by approval_date desc)
				from lsr_ilr_approval
				where approval_status_id = 3
				and approval_date < ld_approval_date
				and ilr_id =  a_ilr_id
				), a_revision
			)
			into ll_prior_approved_revision
			from dual
			;
			
			if ll_prior_approved_revision <> a_revision then
				select nvl(
					(
					select lsr_fasb_type_sob.fasb_cap_type_id
					from lsr_fasb_type_sob, lsr_ilr_options
					where lsr_fasb_type_sob.cap_type_id = lsr_ilr_options.lease_cap_type_id
					and lsr_fasb_type_sob.set_of_books_id = a_set_of_books_id
					and lsr_ilr_options.ilr_id = a_ilr_id
					and lsr_ilr_options.revision = ll_prior_approved_revision
					), ll_toCheckClassification
				)
				into ll_approvedClassification
				from dual
				;
			end if;
		end if;

		if (ll_toCheckClassification + ll_approvedClassification) = 5 then
			return 1;
		else
			return 0;
		end if;
	
	exception
		when others then
			return -1;
	end f_approvedclassification_dfst;
   
END pkg_lessor_common;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (18444, 0, 2018, 2, 1, 0, 0, 'C:\BitBucketRepos\classic_pb\scripts\00_base\packages', 
    'PKG_LESSOR_COMMON.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
