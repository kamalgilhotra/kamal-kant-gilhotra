create or replace package PKG_PP_SEARCH as

	TYPE results_ids_t IS TABLE OF number(22,0) INDEX BY BINARY_INTEGER;
	SUBTYPE results_ids_st is results_ids_t;
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_PP_SEARCH
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   ||============================================================================
   */
	function F_SEARCH(a_searchString string, a_searchType string, a_results in out results_ids_st) return number;
end PKG_PP_SEARCH;
/

create or replace package body PKG_PP_SEARCH as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_PP_SEARCH
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 07/07/2013 B.Beck         Original Version
   ||============================================================================
   */

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************

   --**************************************************************************
   --                            F_SEARCH
   --**************************************************************************

   function F_SEARCH(a_searchString string, a_searchType string, a_results in out results_ids_st) return number is
		l_max_rows number;
		l_searchString varchar2(4000);
   begin
		l_max_rows := a_results.COUNT;

		/*lower the string here so we don't have to do it in every SQL statement*/
		l_searchString := lower(trim(a_searchString));

		/*handle empty string*/
		if l_searchString IS NULL then
			a_results.DELETE;
			return 0;
		end if;

		-- for Lessee, the searchTypes can be 'MLA', 'ILR', 'ASSETS', 'PAYMENTS', 'LESSOR
		if a_searchType = 'MLA' then
			select the_ids
			bulk collect into a_results
			from
			(
				select distinct i.lease_id as the_ids
				from ls_lease i, ls_lease_company lc, company c
				where
				(   i.lease_id = lc.lease_id and 
					lc.company_id = c.company_id)
				and
				(
					to_char(i.lease_id) like '%' || l_searchstring || '%'
					or lower(i.notes) like '%' || l_searchstring || '%'
					or lower(i.description) like '%' || l_searchstring || '%'
					or lower(i.long_description) like '%' || l_searchstring || '%'
					or lower(i.lease_number) like '%' || l_searchstring || '%'
					or i.lease_status_id in
						(select aa.lease_status_id from ls_lease_status aa
						where lower(aa.description) like '%' || l_searchstring || '%')
					or i.lease_group_id in
						(select aa.lease_group_id from ls_lease_group aa
						where lower(aa.description) like '%' || l_searchstring || '%')
					or i.lease_id in
						(select b.lease_id from company aa, ls_lease_company b
						where aa.company_id = b.company_id
						and (lower(aa.description) like '%' || l_searchstring || '%'
						or lower(aa.gl_company_no) like '%' || l_searchstring || '%'))
					or i.lease_id in
						(select aa.lease_id from ls_ilr aa, company c
						where (aa.company_id = c.company_id) and  
                        (lower(aa.notes) like '%' || l_searchstring || '%'
						or lower(aa.ilr_number) like '%' || l_searchstring || '%'
						or to_char(aa.ilr_id) like '%' || l_searchstring || '%'))
					or i.lease_id in
						(select aa.lease_id from ls_mla_class_code aa
						where lower(aa."VALUE") like '%' || l_searchstring || '%'
						)
					or i.lease_id in
						(select aa.lease_id from ls_lease_document aa
						where lower(aa.description) like '%' || l_searchstring || '%'
						or lower(aa."FILE_NAME") like '%' || l_searchstring || '%'
						)
				)
			)
			where rownum <= l_max_rows
			;
		elsif a_searchType = 'ILR' then
			--
			select the_ids
			bulk collect into a_results
			from
			(
				select distinct i.ilr_id as the_ids
				from ls_ilr i, company c
				where
				(i.company_id = c.company_id)
                and
				(
					to_char(i.ilr_id) like '%' || l_searchstring || '%'
					or lower(i.notes) like '%' || l_searchstring || '%'
					or lower(i.ilr_number) like '%' || l_searchstring || '%'
					or i.ilr_group_id in
						(select aa.ilr_group_id from ls_ilr_group aa
						where lower(aa.description) like '%' || l_searchstring || '%')
					or i.ilr_status_id in
						(select aa.ilr_status_id from ls_ilr_status aa
						where lower(aa.description) like '%' || l_searchstring || '%')
					or i.company_id in
						(select aa.company_id from company aa
						where lower(aa.description) like '%' || l_searchstring || '%'
						or lower(aa.gl_company_no) like '%' || l_searchstring || '%')
					or i.lease_id in
						(select aa.lease_id from ls_lease aa
						where lower(aa.notes) like '%' || l_searchstring || '%'
						or lower(aa.description) like '%' || l_searchstring || '%'
						or lower(aa.lease_number) like '%' || l_searchstring || '%'
						or to_char(aa.lease_id) like '%' || l_searchstring || '%')
					or i.ilr_id in
						(select aa.ilr_id from ls_ilr_class_code aa
						where lower(aa."VALUE") like '%' || l_searchstring || '%'
						)
					or i.ilr_id in
						(select aa.ilr_id from ls_ilr_document aa
						where lower(aa.description) like '%' || l_searchstring || '%'
						or lower(aa."FILE_NAME") like '%' || l_searchstring || '%'
						)
				)
			)
			where rownum <= l_max_rows
			;
		elsif a_searchType = 'ASSET' then
			--
			select the_ids
			bulk collect into a_results
			from
			(
				select distinct i.ls_asset_id as the_ids
				from ls_asset i, company c
				where
				(i.company_id = c.company_id)
				and
				(
					to_char(i.ls_asset_id) like '%' || l_searchstring || '%'
					or lower(i.notes) like '%' || l_searchstring || '%'
					or lower(i.description) like '%' || l_searchstring || '%'
					or lower(i.leased_asset_number) like '%' || l_searchstring || '%'
					or i.ls_asset_status_id in
						(select aa.ls_asset_status_id from ls_asset_status aa
						where lower(aa.description) like '%' || l_searchstring || '%'
						)
					or i.company_id in
						(select aa.company_id from company aa
						where lower(aa.description) like '%' || l_searchstring || '%'
						or lower(aa.gl_company_no) like '%' || l_searchstring || '%'
						)
					or i.ilr_id in
						(select aa.ilr_id from ls_ilr aa
						where lower(aa.notes) like '%' || l_searchstring || '%'
						or lower(aa.ilr_number) like '%' || l_searchstring || '%'
						or to_char(aa.ilr_id) like '%' || l_searchstring || '%'
						)
					or i.ls_asset_id in
						(select aa.ls_asset_id from ls_asset_class_code aa
						where lower(aa."VALUE") like '%' || l_searchstring || '%'
						)
					or i.ls_asset_id in
						(select aa.ls_asset_id from ls_asset_document aa
						where lower(aa.description) like '%' || l_searchstring || '%'
						or lower(aa."FILE_NAME") like '%' || l_searchstring || '%'
						)
					or i.utility_account_id in
						(select aa.utility_account_id from utility_account aa
						where lower(aa.description) like '%' || l_searchstring || '%'
						or to_char(aa.utility_account_id) like '%' || l_searchstring || '%'
						)
					or i.retirement_unit_id in
						(select aa.retirement_unit_id from retirement_unit aa
						where lower(aa.description) like '%' || l_searchstring || '%'
						or to_char(aa.retirement_unit_id) like '%' || l_searchstring || '%'
						or to_char(aa.external_retire_unit) like '%' || l_searchstring || '%'
						)
					or i.asset_location_id in
						(select aa.asset_location_id from asset_location aa
						where lower(aa.long_description) like '%' || l_searchstring || '%'
						or to_char(aa.asset_location_id) like '%' || l_searchstring || '%'
						or to_char(aa.ext_asset_location) like '%' || l_searchstring || '%'
						)
               or i.ilr_id in
                  (select ls_ilr.ilr_id from ls_ilr, ls_lease
                  where ls_ilr.lease_id = ls_lease.lease_id
                  and lower(ls_lease.description) like '%' || l_searchstring || '%'
                  )
				)
			)
			where rownum <= l_max_rows
			;
		elsif a_searchType = 'COMPONENT' then
			--
			select the_ids
			bulk collect into a_results
			from
			(
				select distinct component_id as the_ids
				from ls_component i, company c
				where 
				(i.company_id = c.company_id)
				and
				(
					lower(to_char(i.component_id)) like '%' || l_searchstring || '%'
				 or lower(i.description) like '%' || l_searchstring || '%'
				 or lower(i.long_description) like '%' || l_searchstring || '%'
				 or lower(i.serial_number) like '%' || l_searchstring || '%'
				 or lower(i.po_number) like '%' || l_searchstring || '%'
				 or lower(to_char(i.amount)) like '%' || l_searchstring || '%'
				 or lower(to_char(i.date_received,'MM/DD/YYYY MM/DD/YY MM/YY MM/YYYY MON MMYYYY')) like '%' || l_searchstring || '%'
				 or lower(to_char(i.interim_interest)) like '%' || l_searchstring || '%'
				 or i.ls_comp_status_id in (
					select ls_comp_status_id
					from ls_component_status
					where lower(description) like '%' || l_searchstring || '%'
					   or lower(long_description) like '%' || l_searchstring || '%'
				 )
				 or i.ls_asset_id in (
					select ls_asset_id
					from ls_asset
					where lower(description) like '%' || l_searchstring || '%'
					   or lower(long_description) like '%' || l_searchstring || '%'
				 )
             or i.ls_asset_id in (
               select a1.ls_asset_id
               from ls_asset a1, ls_ilr i1
               where a1.ilr_id = i1.ilr_id
                 and lower(i1.ilr_number) like '%' || l_searchstring || '%'
             )
             or i.ls_asset_id in (
               select ls_asset.ls_asset_id 
               from ls_asset, ls_ilr, ls_lease
               where ls_asset.ilr_id = ls_ilr.ilr_id
               and ls_ilr.lease_id = ls_lease.lease_id
               and lower(ls_lease.description) like '%' || l_searchstring || '%'
             )
				 or i.company_id in (
					select company_id
					from company
					where lower(description) like '%' || l_searchstring || '%'
					   or lower(long_description) like '%' || l_searchstring || '%'
				 )
				)
			)
			where rownum <= l_max_rows
			;
		elsif a_searchType = 'PAYMENT' then
			--
			select the_ids
			bulk collect into a_results
			from
			(
				select distinct i.ilr_id as the_ids
				from ls_ilr i, ls_payment_line p, ls_payment_hdr h, ls_asset a, company c
				where (i.ilr_id = a.ilr_id
				and p.payment_id = h.payment_id
				and a.ls_asset_id = p.ls_asset_id
				and a.company_id = c.company_id)
				and
				(
					to_char(h.gl_posting_mo_yr, 'yyyymm') like '%' || l_searchstring || '%'
					or to_char(i.ilr_id) like '%' || l_searchstring || '%'
					or lower(i.notes) like '%' || l_searchstring || '%'
					or lower(i.ilr_number) like '%' || l_searchstring || '%'
					or i.ilr_group_id in
						(select aa.ilr_group_id from ls_ilr_group aa
						where lower(aa.description) like '%' || l_searchstring || '%')
					or i.ilr_status_id in
						(select aa.ilr_status_id from ls_ilr_status aa
						where lower(aa.description) like '%' || l_searchstring || '%')
					or i.company_id in
						(select aa.company_id from company aa
						where lower(aa.description) like '%' || l_searchstring || '%'
						or lower(aa.gl_company_no) like '%' || l_searchstring || '%')
					or i.lease_id in
						(select aa.lease_id from ls_lease aa
						where lower(aa.notes) like '%' || l_searchstring || '%'
						or lower(aa.description) like '%' || l_searchstring || '%'
						or lower(aa.lease_number) like '%' || l_searchstring || '%'
						or to_char(aa.lease_id) like '%' || l_searchstring || '%')
					or i.ilr_id in
						(select aa.ilr_id from ls_ilr_class_code aa
						where lower(aa."VALUE") like '%' || l_searchstring || '%'
						)
					or i.ilr_id in
						(select aa.ilr_id from ls_ilr_document aa
						where lower(aa.description) like '%' || l_searchstring || '%'
						or lower(aa."FILE_NAME") like '%' || l_searchstring || '%'
						)
				)
			)
			where rownum <= l_max_rows
			;
		elsif a_searchType = 'LESSOR' then
			--
			select the_ids
			bulk collect into a_results
			from
			(
				select distinct l.lessor_id as the_ids
				from ls_lessor l
				where (to_char(l.lessor_id) like '%' || l_searchstring || '%'
					or lower(l.description) like '%' || l_searchstring || '%'
					or lower(l.address1) like '%' || l_searchstring || '%'
					or lower(l.external_lessor_number) like '%' || l_searchstring || '%'
					or lower(l.city) like '%' || l_searchstring || '%'
					or lower(l.county_id) like '%' || l_searchstring || '%'
					or lower(l.state_id) like '%' || l_searchstring || '%'
					or l.lease_group_id in
						(select aa.lease_group_id from ls_lease_group aa
						where lower(aa.description) like '%' || l_searchstring || '%')
				)
			)
			where rownum <= l_max_rows
			;
		end if;

		return a_results.COUNT;
   end F_SEARCH;

--**************************************************************************
--                            Initialize Package
--**************************************************************************
end PKG_PP_SEARCH;
/

--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (6882, 0, 2017, 3, 0, 1, 0, 'C:\plasticwks\powerplant\sql\packages', '2017.3.0.1_PKG_PP_SEARCH.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
