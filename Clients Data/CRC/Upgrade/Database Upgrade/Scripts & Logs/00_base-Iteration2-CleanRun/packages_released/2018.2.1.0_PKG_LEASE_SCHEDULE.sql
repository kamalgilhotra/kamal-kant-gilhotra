create or replace package pkg_lease_schedule as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: LEASE_SCHEDULE_PKG
   || Description:
   ||============================================================================
   || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 05/07/2013 B.Beck          Original Version
   || 10.4.1.0 06/21/2013 Brandon Beck    Added BPO and Term Penalty.
   ||                                     Also correct the end obligation for guaranteed residual
   || 10.4.1.0 07/05/2013 Brandon Beck    Load schedule tables from the calc tables
   || 10.4.1.0 07/15/2013 B.Beck          Update IRR and NPV onto ls_ilr
   || 10.4.1.1 10/02/2013 Kyle Peterson   Interim Interest
   || 10.4.2.0 01/07/2014 Kyle Peterson
   || 10.4.2.0 03/17/2014 Brandon Beck
   || 10.4.2.2 06/16/2014 Brandon Beck
   || 10.4.2.2 06/16/1014 Brandon Beck
   || 10.4.3.0 06/30/2014 Kyle Peterson  Refactor NPV calc
   || 2015.1   04/06/2015 Anand R        PP-42670 Operating Prepaid Lease Schedule
   || 2015.2.0 08/20/2015 Anand R        PP-44441 error when updating current lease cost on LS_ILR_SCHEDULE
   || 2015.2.0 09/30/2015 Andrew Scott   added some more logging to aid in maint-45012
   || 2016.1.0 02/10/2016 S. Byers       PP-45363 Added NPV_START_DATE to LS_ILR_STG
   || 2016.1.0 02/11/2016 S. Byers       PP-45365 Added F_BUILD_FORECAST
   || 2016.1.0 02/22/2016 Anand R        PP-45407 Added F_BUILD_SUMMARY_FORECAST
   || 2016.1.0 02/23/2016 Anand R        PP-45366 Added F_CONVERT_FORECAST
   || 2016.1.0 02/29/2016 S. Byers     PP-45508 Fixed call to F_COPY_REVISION in F_BUILD_FORECAST
   || 2016.1.0 03/08/2016 S. Byers       PP-45526 Trick the lease schedule logic into not checking for the IRR on summary forecasts
   || 2016.1.0 03/17/2016 Anand R        PP-45557 Update revision_built on ls_forecsast_version when revision is built
   || 2016.1.1 03/07/2017 J. Watkins     PP-47170 Incorrect revision grouping when creating forecasts
   || 2017.1.0 03/16/2017 David H        PP-47023 Add to initial measure included in schedule build
   || 2017.1.0 03/22/2017 David H.       PP-47023 and 47282 Added call to calculate variable payments during schedule build
   || 2017.1.0 06/26/2017 Jared S.     PP-48330 Added calculations for deferred rent
   || 2017.1.0 08/17/2017 Andrew H.      PP-48623 Add call to update currency_rate_default_dense after building schedule (in case max/min month changes)
   || 2017.1.0 08/30/2017 Anand R        PP-48861 Replace code block with view that uses materialzed asset schedule view
   || 2017.1.0 09/11/2017 Sisouphanh     PP-47249 Fixed F_POPULATE_SUMMARY_FORECAST to add the commit needed before selecting from materialzed view
   || 2017.1.0 11/28/2017 Anand R        PP-49763 Add nvl around lookback_revision to calculate IRR.
   || 2017.1.0 11/30/2017 Shane "C" Ward PP-49859 Update BPO Depreciation Calc
   || 2017.2.0 01/08/2018 Josh S     PP-50202 Add Liability
   || 2017.2.0 01/15/2018 Shane "C" Ward PP-50179 New Modified Retrospective Calc option for Forecast ILRs
   || 2017.2.0 01/25/2018 Shane "C" Ward PP-50220 Performance Enhancements and formatting
   || 2017.2.0 02/07/2018 Shane "C" Ward PP-50179 Add Source and In-Service Revision logic to Forecast ILRs
   || 2017.3.0 03/02/2018 Anand R        PP-50205 Add new Lessee tables to forecast build
   || 2017.3.0 03/28/2018 Shane "C" Ward PP-50475 Add Incentives and Initial Direct Costs Schedule Logic
   || 2017.3.0 03/12/2018 Josh S         PP-50199 Add remeasurements
   || 2017.3.0 03/23/2018 Josh S         PP-50691/PP-50740 Prepay Amortization and Deferred Rent/Prepaid Rent changes
   || 2017.3.0 04/12/2018 MaintOps (AJS) PP-50979 Prepaid Leases Using FERC Depreciation are Incorrect
   || 2017.4.0 06/14/2018 Shane "C" Ward PP-51378/PP-51537 Asset Partial Termination Remeasurement Logic
   || 2017.4.0 05/15/2018 Powers K       PP-51078 Add Right of Use Asset (ROU) to Schedule
   || 2017.4.0 06/12/2018 Powers K       PP-51540 Add new function F_ADD_PAYMENT_ADJUSTMENTS
   || 2017.4.0 06/25/2018 Shane "C" Ward PP-51712 Fixed Principal Calculation (Fix Floating Rates)
   || 2017.4.0 08/14/2018 Anand R        PP-52156 Fixes monthly prepaid lease with Expense As Incurred still calculating deferred rent
   || 2017.4.0 08/14/2018 Alex H         PP-52186 Correcting FASB cap type logic between the f_load_ilr functions
   || 2018.1.0 09/10/2018 Anand R        PP-52286 Correcting logic to calculate paid amount spread for Off balance sheet Straight line
   || 2018.1.0 09/11/2018 Anand R        PP-52295 Fix Interest paid for monthly spread for Off balance sheet As incurred
   || 2018.1.0 10/05/2018 S Byers        PP-52351 Fix various issues with Negative Payment Shift
   || 2018.1.0 10/25/2018 Shane "C" Ward PP-52257 Fix Escalations on Remeasurement. Add IFRS Remeasurement Logic
   || 2018.1.0 11/02/2018 K Powers       PP-52474 Add impairment fields
   || 2018.1.0.2 02/05/2019 C Yura         PP-53060 Fix In_service_revision number on ls_forecast_ilr_master
   ||==================================================================================================================================================
   */
   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   -- Function to process the asset (allocate, find NBV)
   function F_PROCESS_ASSETS(A_MONTH in date:=null) return varchar2;

   -- Loads the ilr_scheduling table for an entire LEASE
   function F_PROCESS_ILRS(A_LEASE_ID number) return varchar2;

   --Loads LS_ILR_STG all at once to avoid PK violations
   function F_MASS_PROCESS_ILRS(A_ILR_IDS   PKG_LEASE_IMPORT.NUM_ARRAY,
                                A_REVISIONS PKG_LEASE_IMPORT.NUM_ARRAY) return varchar2;

   -- Loads the ilr_scheduling table for a single ILR
   function F_PROCESS_ILR(A_ILR_ID   number,
                          A_REVISION number) return varchar2;

   -- Builds forecast revisions for all ILR's
   function F_PROCESS_ILRS_FCST(A_CAP_TYPE number) return varchar2;

   /*
   *  Loads from the calc staging table to the "real" tables
   */
   function F_SAVE_SCHEDULES(A_MONTH in date:=null) return varchar2;

   /*
   *  This function is used for Asset transfers to create new asset schedules based on the transfer
   */
   function F_PROCESS_ASSET_TRF(A_FROM_ASSET_ID     number,
                                A_TO_ASSET_ID       number,
                                A_FROM_ILR_REVISION number,
                                A_TO_ILR_REVISION   number,
                                A_PERCENT           number,
                                A_FROM_ILR_ID       number,
                                A_TO_ILR_ID         number,
                                A_MONTH             date:=null) return varchar2;

   function F_GET_II_BEGIN_DATE(A_ILR_ID   number,
                                A_REVISION number) return date;

   function F_MAKE_II(A_ILR_ID   number,
                      A_REVISION number) return number;

  function F_LOAD_ILR_SCHEDULE_STG(a_month in date:=null) return varchar2;

  function F_NET_PRESENT_VALUE return varchar2;

  function F_LOAD_ILR_STG(a_ilr_id number,
                a_revision number) return varchar2;
  function F_SYNC_ILR_SCHEDULE(A_ILR_ID IN NUMBER,
                               A_REVISION IN NUMBER) return varchar2;

   function F_BUILD_FORECAST(A_REVISION IN NUMBER) return varchar2;

   function F_BUILD_SUMMARY_FORECAST(A_REVISION IN NUMBER, A_RATE IN DECIMAL) return varchar2;

   function F_CONVERT_FORECAST(A_REVISION IN NUMBER) return varchar2;

   function F_POPULATE_SUMMARY_FORECAST(A_REVISION IN NUMBER, A_CUR_TYPE IN NUMBER, A_ACTUALS IN NUMBER:=null) return varchar2;

   -- Function to clear the staging tables used in the schedule calculation
   function F_CLEAR_STAGING_TABLES return varchar2;
   function F_CLEAR_STAGING_TABLES(A_CLEAR_CALC BOOLEAN) return varchar2;

   function F_LOAD_DEFERRED_RENT(A_SUMM_FCST IN BOOLEAN) return varchar2;

   FUNCTION F_BPO_SCHEDULE_EXTEND (A_LS_ASSET_ID IN NUMBER, A_ILR_ID IN NUMBER, A_REVISION IN NUMBER) RETURN VARCHAR2;

   FUNCTION F_ADD_PAYMENT_ADJUSTMENTS(A_PAYMENT_ID in number) return number;

   FUNCTION f_calc_variable_escalation_pct RETURN VARCHAR2;

   FUNCTION F_CLEAR_STAGING_TABLES_NO_LOG return varchar2;

   FUNCTION f_build_schedule_impaired(a_ilr_id in number,
										a_revision in number,
										a_impairment_date in number) return varchar2;
										
	FUNCTION f_copy_asset_schedule_trf(a_asset_id_from       	  IN NUMBER,
									   a_asset_curr_revision_from in number,
									   a_asset_revision_from 	  IN NUMBER,
									   a_asset_id_to         	  IN NUMBER,
									   a_asset_revision_to   	  IN NUMBER,
									   a_transfer_date       	  IN DATE,
									   a_interco_trans			  in boolean) RETURN VARCHAR2;
									   
	FUNCTION f_copy_ilr_schedule_trf(a_ilr_id_from       IN NUMBER,
									   a_ilr_curr_revision_from IN NUMBER,
									   a_ilr_revision_from in number,
									   a_ilr_id_to         IN NUMBER,
									   a_ilr_revision_to   IN NUMBER,
									   a_transfer_date       IN DATE,
									   a_interco_trans in boolean) RETURN VARCHAR2;
									   
	FUNCTION f_copy_asset_schedule_rows(a_month date) return varchar2;
	
	FUNCTION f_copy_ilr_schedule_rows(a_month date) return varchar2;
	
	FUNCTION f_copy_depr_forecast_rows(a_month date) return varchar2;

   procedure P_HANDLE_OM;

end PKG_LEASE_SCHEDULE;
/
create or replace package body pkg_lease_schedule as
 G_COPY_LS_ASSET_ID          LS_ASSET.LS_ASSET_ID%type;
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: LEASE_SCHEDULE_PKG
   || Description:
   ||============================================================================
   || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 05/07/2013 B.Beck          Original Version
   || 10.4.1.0 06/21/2013 Brandon Beck    Added BPO and Term Penalty.
   ||                                     Also correct the end obligation for guaranteed residual
   || 10.4.1.0 07/05/2013 Brandon Beck    Load schedule tables from the calc tables
   || 10.4.1.0 07/15/2013 B.Beck          Update IRR and NPV onto ls_ilr
   || 10.4.1.1 10/02/2013 Kyle Peterson   Interim Interest
   || 10.4.2.0 01/07/2014 Kyle Peterson
   || 10.4.2.0 03/17/2014 Brandon Beck
   || 10.4.2.2 06/16/2014 Brandon Beck
   || 10.4.2.2 06/16/1014 Brandon Beck
   || 10.4.3.0 06/30/2014 Kyle Peterson  Refactor NPV calc
   || 2015.1   04/06/2015 Anand R        PP-42670 Operating Prepaid Lease Schedule
   || 2015.2.0 08/20/2015 Anand R        PP-44441 error when updating current lease cost on LS_ILR_SCHEDULE
   || 2015.2.0 09/30/2015 Andrew Scott   added some more logging to aid in maint-45012
   || 2016.1.0 02/10/2016 S. Byers       PP-45363 Added NPV_START_DATE to LS_ILR_STG
   || 2016.1.0 02/11/2016 S. Byers       PP-45365 Added F_BUILD_FORECAST
   || 2016.1.0 02/22/2016 Anand R        PP-45407 Added F_BUILD_SUMMARY_FORECAST
   || 2016.1.0 02/23/2016 Anand R        PP-45366 Added F_CONVERT_FORECAST
   || 2016.1.0 02/29/2016 S. Byers     PP-45508 Fixed call to F_COPY_REVISION in F_BUILD_FORECAST
   || 2016.1.0 03/08/2016 S. Byers       PP-45526 Trick the lease schedule logic into not checking for the IRR on summary forecasts
   || 2016.1.0 03/17/2016 Anand R        PP-45557 Update revision_built on ls_forecsast_version when revision is built
   || 2016.1.1 03/07/2017 J. Watkins     PP-47170 Incorrect revision grouping when creating forecasts
   || 2017.1.0 03/16/2017 David H        PP-47023 Add to initial measure included in schedule build
   || 2017.1.0 03/22/2017 David H.       PP-47023 and 47282 Added call to calculate variable payments during schedule build
   || 2017.1.0 06/26/2017 Jared S.     PP-48330 Added calculations for deferred rent
   || 2017.1.0 08/17/2017 Andrew H.      PP-48623 Add call to update currency_rate_default_dense after building schedule (in case max/min month changes)
   || 2017.1.0 08/30/2017 Anand R        PP-48861 Replace code block with view that uses materialzed asset schedule view
   || 2017.1.0 09/11/2017 Sisouphanh     PP-47249 Fixed F_POPULATE_SUMMARY_FORECAST to add the commit needed before selecting from materialzed view
   || 2017.1.0 11/28/2017 Anand R        PP-49763 Add nvl around lookback_revision to calculate IRR.
   || 2017.1.0 11/30/2017 Shane "C" Ward PP-49859 Update BPO Depreciation Calc
   || 2017.2.0 01/08/2018 Josh S     PP-50202 Add Liability
   || 2017.2.0 01/15/2018 Shane "C" Ward PP-50179 New Modified Retrospective Calc option for Forecast ILRs
   || 2017.2.0 01/25/2018 Shane "C" Ward PP-50220 Performance Enhancements and formatting
   || 2017.2.0 02/07/2018 Shane "C" Ward PP-50179 Add Source and In-Service Revision logic to Forecast ILRs
   || 2017.3.0 03/02/2018 Anand R        PP-50205 Add new Lessee tables to forecast build
   || 2017.3.0 03/28/2018 Shane "C" Ward PP-50475 Add Incentives and Initial Direct Costs Schedule Logic
   || 2017.3.0 03/12/2018 Josh S         PP-50199 Add remeasurements
   || 2017.3.0 03/23/2018 Josh S         PP-50691/PP-50740 Prepay Amortization and Deferred Rent/Prepaid Rent changes
   || 2017.3.0 04/12/2018 MaintOps (AJS) PP-50979 Prepaid Leases Using FERC Depreciation are Incorrect
   || 2017.4.0 06/14/2018 Shane "C" Ward PP-51378/PP-51537 Asset Partial Termination Remeasurement Logic
   || 2017.4.0 05/15/2018 Powers K       PP-51078 Add Right of Use Asset (ROU) to Schedule
   || 2017.4.0 06/12/2018 Powers K       PP-51540 Add new function F_ADD_PAYMENT_ADJUSTMENTS
   || 2017.4.0 06/25/2018 Shane "C" Ward PP-51712 Fixed Principal Calculation (Fix Floating Rates)
   || 2017.4.0 06/25/2018 David Conway   PP-51542 Changed reference to IS_OM in F_LOAD_COMPONENTS, F_LOAD_ILRS_STG, and updated logic in P_HANDLE_OM to
   ||                       make use of LS_FASB_CAP_TYPE_SOB_MAP.FASB_CAP_TYPE_ID instead of the company system control.
   ||============================================================================
   */

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

   FUNCTION f_calc_variable_escalation_pct
   RETURN VARCHAR2
   IS
   L_SQLS VARCHAR2(4000);
   L_STATUS VARCHAR2(2000);
   BEGIN
    FOR L_VP IN (SELECT DISTINCT escalation FROM ls_ilr_schedule_stg WHERE escalation IS NOT NULL)
    LOOP
      SELECT DISTINCT
                  'merge INTO ls_ilr_schedule_stg stg '||
                  '  USING ( ' ||
                  '    WITH escalation AS ( ' ||
                  '      SELECT ' ||
                  '      ilr_id, ' ||
                  '      revision, ' ||
                  '      set_of_books_id, ' ||
                  '      MONTH, ' ||
                  '      Decode(escalation_month, 1, Lag(MONTH, months_to_escalate) OVER(PARTITION BY ilr_id, revision, set_of_books_id, payment_term_id ORDER BY id, month), NULL) base_month, ' ||
                  '      escalation_month, ' ||
                  '      escalation ' ||
                  '      FROM (select id, ilr_id, revision, set_of_books_id, month, escalation_month, payment_term_id, escalation, months_to_escalate from ls_ilr_schedule_stg '||
				  '				UNION '||
					'		SELECT -1 id, '||
					'	   remeasure.ilr_id, '||
					'	   remeasure.revision, '||
					'	   remeasure.set_of_books_id, '||
					'	   sched.month MONTH, '||
					'	   sched.escalation_month, '||
					'	   remeasure.payment_term_id, '||
					'	   '||L_VP.ESCALATION||' escalation, '||
					'	    Decode(ipt.escalation_freq_id, 4, 1, 3, 3, 2, 6, 1, 12) months_to_escalate '||
					' FROM ls_ilr_schedule sched, '||
					'	   ls_ilr i, '||
					'	   ls_ilr_payment_term ipt, '||
					'	   (SELECT s.ilr_id, s.revision, s.set_of_books_id, s.npv_start_date, MAX(payment_term_id) payment_term_id '||
					'		  FROM ls_ilr_payment_term_stg pt, '||
					'			   ls_ilr_stg s '||
					'		 WHERE pt.ilr_id = s.ilr_id '||
					'		   AND pt.revision = s.revision '||
					'		   AND pt.set_of_books_id = s.set_of_books_id '||
					'		   AND s.is_remeasurement = 1 '||
					'		   AND payment_term_date <= npv_start_date '||
					'		 GROUP BY s.ilr_id, s.revision, s.set_of_books_id, s.npv_start_date) remeasure '||
					'WHERE remeasure.ilr_id = i.ilr_id '||
					'  AND i.ilr_id = ipt.ilr_id '||
					'  AND i.current_revision = ipt.revision '||
					'  AND remeasure.payment_term_id = ipt.payment_Term_id '||
					'  AND i.ilr_id = sched.ilr_id '||
					'  AND i.current_revision = sched.revision '||
					'  AND remeasure.set_of_books_id = sched.set_of_books_id '||
					'  AND sched.month < remeasure.npv_start_date '||
				  '		 )stg ' ||
                  '      WHERE escalation IS NOT NULL ' ||
                  '    ) ' ||
                  '    SELECT a.ilr_id, a.revision, a.set_of_books_id, a.MONTH, a.escalation, ' ||
                  '    ' || vp.payment_formula_db || ' as escalation_pct' ||
                  '    FROM escalation a ' ||
                  '      JOIN ls_variable_payment vp ON a.escalation = vp.variable_payment_id ' ||
                  '    WHERE escalation_month = 1 ' ||
                  '    AND a.escalation = ' || L_VP.escalation ||
                  '  ) esc ' ||
                  '  ON (stg.ilr_id = esc.ilr_id AND stg.revision = esc.revision AND stg.set_of_books_id = esc.set_of_books_id AND stg.MONTH = esc.MONTH and stg.escalation = esc.escalation) ' ||
                  '  WHEN matched THEN ' ||
                  '  UPDATE SET stg.escalation_pct = esc.escalation_pct'
      INTO L_SQLS
      FROM ls_ilr_schedule_stg stg
        JOIN ls_variable_payment vp ON vp.variable_payment_id = stg.escalation
      WHERE escalation IS NOT NULL
      AND stg.escalation = L_VP.escalation;

      PKG_PP_LOG.P_WRITE_MESSAGE('Calculating escalation percent for variable_payment_id = ' || L_VP.escalation);

      EXECUTE IMMEDIATE L_SQLS;

      PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows updated on LS_ILR_SCHEDULE_STG');
    END LOOP;

   RETURN 'OK';

   EXCEPTION
    WHEN OTHERS THEN
      L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
      RETURN L_STATUS;
   END f_calc_variable_escalation_pct;

	/************************************************************************************************************
	** f_copy_ilr_schedule_trf
	**
	** Copies Schedules for Transfers
	** On FROM ILR it copies schedule to the new transfer revision up to tansfer date
	** On TO ILR it copies schedule to the new transfer revision 
	** 
	** Note: on Intraco tranfers ILR and Revision FROM and TO are the same
	**************************************************************************************************************/
	FUNCTION f_copy_ilr_schedule_trf(a_ilr_id_from       IN NUMBER,
									   a_ilr_curr_revision_from IN NUMBER,
									   a_ilr_revision_from in number,
									   a_ilr_id_to         IN NUMBER,
									   a_ilr_revision_to   IN NUMBER,
									   a_transfer_date       IN DATE,
									   a_interco_trans in boolean)
	  RETURN VARCHAR2 IS

	  l_status VARCHAR2(2000);
	BEGIN
	  pkg_pp_log.p_write_message('Deleting from ls_ilr_schedule for FROM ILR');
	  DELETE FROM ls_ilr_schedule
	  WHERE  ilr_id = a_ilr_id_from
	  AND    revision = a_ilr_revision_from;
	  
	  pkg_pp_log.p_write_message('Inserting into ls_ilr_schedule for FROM ILR');
		INSERT INTO ls_ilr_schedule
		  (ilr_id, revision, set_of_books_id, MONTH, residual_amount, term_penalty,
		   bpo_price, beg_capital_cost, end_capital_cost, beg_obligation,
		   end_obligation, beg_lt_obligation, end_lt_obligation, interest_accrual,
		   principal_accrual, interest_paid, principal_paid, executory_accrual1,
		   executory_accrual2, executory_accrual3, executory_accrual4,
		   executory_accrual5, executory_accrual6, executory_accrual7,
		   executory_accrual8, executory_accrual9, executory_accrual10,
		   executory_paid1, executory_paid2, executory_paid3, executory_paid4,
		   executory_paid5, executory_paid6, executory_paid7, executory_paid8,
		   executory_paid9, executory_paid10, contingent_accrual1,
		   contingent_accrual2, contingent_accrual3, contingent_accrual4,
		   contingent_accrual5, contingent_accrual6, contingent_accrual7,
		   contingent_accrual8, contingent_accrual9, contingent_accrual10,
		   contingent_paid1, contingent_paid2, contingent_paid3, contingent_paid4,
		   contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8,
		   contingent_paid9, contingent_paid10, user_id, time_stamp, is_om,
		   current_lease_cost, beg_deferred_rent, deferred_rent, end_deferred_rent,
		   beg_st_deferred_rent, end_st_deferred_rent, beg_liability, end_liability,
		   beg_lt_liability, end_lt_liability, principal_remeasurement,
		   liability_remeasurement, initial_direct_cost, incentive_amount,
		   beg_prepaid_rent, prepay_amortization, prepaid_rent, end_prepaid_rent,
		   idc_math_amount, incentive_math_amount, beg_net_rou_asset,
		   end_net_rou_asset, rou_asset_remeasurement, partial_term_gain_loss,
		   additional_rou_asset, executory_adjust, contingent_adjust,
		   beg_arrears_accrual, arrears_accrual, end_arrears_accrual,
		   remaining_principal, st_obligation_remeasurement,
		   lt_obligation_remeasurement, obligation_reclass,
		   unaccrued_interest_remeasure, unaccrued_interest_reclass, payment_month,
		   escalation_month, escalation_pct, impairment_activity, begin_accum_impair,
		   end_accum_impair, incentive_cap_amount)
		  SELECT a_ilr_id_from, a_ilr_revision_from, set_of_books_id, MONTH, residual_amount,
				 term_penalty, bpo_price, beg_capital_cost, end_capital_cost,
				 beg_obligation, end_obligation, beg_lt_obligation,
				 end_lt_obligation, interest_accrual, principal_accrual,
				 interest_paid, principal_paid, executory_accrual1,
				 executory_accrual2, executory_accrual3, executory_accrual4,
				 executory_accrual5, executory_accrual6, executory_accrual7,
				 executory_accrual8, executory_accrual9, executory_accrual10,
				 executory_paid1, executory_paid2, executory_paid3, executory_paid4,
				 executory_paid5, executory_paid6, executory_paid7, executory_paid8,
				 executory_paid9, executory_paid10, contingent_accrual1,
				 contingent_accrual2, contingent_accrual3, contingent_accrual4,
				 contingent_accrual5, contingent_accrual6, contingent_accrual7,
				 contingent_accrual8, contingent_accrual9, contingent_accrual10,
				 contingent_paid1, contingent_paid2, contingent_paid3,
				 contingent_paid4, contingent_paid5, contingent_paid6,
				 contingent_paid7, contingent_paid8, contingent_paid9,
				 contingent_paid10, user_id, time_stamp, is_om, current_lease_cost,
				 beg_deferred_rent, deferred_rent, end_deferred_rent,
				 beg_st_deferred_rent, end_st_deferred_rent, beg_liability,
				 end_liability, beg_lt_liability, end_lt_liability,
				 principal_remeasurement, liability_remeasurement,
				 initial_direct_cost, incentive_amount, beg_prepaid_rent,
				 prepay_amortization, prepaid_rent, end_prepaid_rent,
				 idc_math_amount, incentive_math_amount, beg_net_rou_asset,
				 end_net_rou_asset, rou_asset_remeasurement, partial_term_gain_loss,
				 additional_rou_asset, executory_adjust, contingent_adjust,
				 beg_arrears_accrual, arrears_accrual, end_arrears_accrual,
				 remaining_principal, st_obligation_remeasurement,
				 lt_obligation_remeasurement, obligation_reclass,
				 unaccrued_interest_remeasure, unaccrued_interest_reclass,
				 payment_month, escalation_month, escalation_pct,
				 impairment_activity, begin_accum_impair, end_accum_impair,
				 incentive_cap_amount
		  FROM   ls_ilr_schedule
		  WHERE  ilr_id = a_ilr_id_from
		  AND    revision = a_ilr_curr_revision_from
		  AND MONTH <= a_transfer_date;

	  if not a_interco_trans then --Transfers within ILR take place on one revision
		  pkg_pp_log.p_write_message('Deleting from ls_ilr_schedule for TO ILR');

		  DELETE FROM ls_ilr_schedule
		  WHERE  ilr_id = a_ilr_id_to
		  AND    revision = a_ilr_revision_to;
	  end if;
	  
	  pkg_pp_log.p_write_message('Inserting into ls_ilr_schedule for TO ILR');
	  
	  INSERT INTO ls_ilr_schedule
		  (ilr_id, revision, set_of_books_id, MONTH, residual_amount, term_penalty,
		   bpo_price, beg_capital_cost, end_capital_cost, beg_obligation,
		   end_obligation, beg_lt_obligation, end_lt_obligation, interest_accrual,
		   principal_accrual, interest_paid, principal_paid, executory_accrual1,
		   executory_accrual2, executory_accrual3, executory_accrual4,
		   executory_accrual5, executory_accrual6, executory_accrual7,
		   executory_accrual8, executory_accrual9, executory_accrual10,
		   executory_paid1, executory_paid2, executory_paid3, executory_paid4,
		   executory_paid5, executory_paid6, executory_paid7, executory_paid8,
		   executory_paid9, executory_paid10, contingent_accrual1,
		   contingent_accrual2, contingent_accrual3, contingent_accrual4,
		   contingent_accrual5, contingent_accrual6, contingent_accrual7,
		   contingent_accrual8, contingent_accrual9, contingent_accrual10,
		   contingent_paid1, contingent_paid2, contingent_paid3, contingent_paid4,
		   contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8,
		   contingent_paid9, contingent_paid10, user_id, time_stamp, is_om,
		   current_lease_cost, beg_deferred_rent, deferred_rent, end_deferred_rent,
		   beg_st_deferred_rent, end_st_deferred_rent, beg_liability, end_liability,
		   beg_lt_liability, end_lt_liability, principal_remeasurement,
		   liability_remeasurement, initial_direct_cost, incentive_amount,
		   beg_prepaid_rent, prepay_amortization, prepaid_rent, end_prepaid_rent,
		   idc_math_amount, incentive_math_amount, beg_net_rou_asset,
		   end_net_rou_asset, rou_asset_remeasurement, partial_term_gain_loss,
		   additional_rou_asset, executory_adjust, contingent_adjust,
		   beg_arrears_accrual, arrears_accrual, end_arrears_accrual,
		   remaining_principal, st_obligation_remeasurement,
		   lt_obligation_remeasurement, obligation_reclass,
		   unaccrued_interest_remeasure, unaccrued_interest_reclass, payment_month,
		   escalation_month, escalation_pct, impairment_activity, begin_accum_impair,
		   end_accum_impair, incentive_cap_amount)
		  SELECT a_ilr_id_to, a_ilr_revision_to, set_of_books_id, MONTH, residual_amount,
				 term_penalty, bpo_price, beg_capital_cost, end_capital_cost,
				 beg_obligation, end_obligation, beg_lt_obligation,
				 end_lt_obligation, interest_accrual, principal_accrual,
				 interest_paid, principal_paid, executory_accrual1,
				 executory_accrual2, executory_accrual3, executory_accrual4,
				 executory_accrual5, executory_accrual6, executory_accrual7,
				 executory_accrual8, executory_accrual9, executory_accrual10,
				 executory_paid1, executory_paid2, executory_paid3, executory_paid4,
				 executory_paid5, executory_paid6, executory_paid7, executory_paid8,
				 executory_paid9, executory_paid10, contingent_accrual1,
				 contingent_accrual2, contingent_accrual3, contingent_accrual4,
				 contingent_accrual5, contingent_accrual6, contingent_accrual7,
				 contingent_accrual8, contingent_accrual9, contingent_accrual10,
				 contingent_paid1, contingent_paid2, contingent_paid3,
				 contingent_paid4, contingent_paid5, contingent_paid6,
				 contingent_paid7, contingent_paid8, contingent_paid9,
				 contingent_paid10, user_id, time_stamp, is_om, current_lease_cost,
				 beg_deferred_rent, deferred_rent, end_deferred_rent,
				 beg_st_deferred_rent, end_st_deferred_rent, beg_liability,
				 end_liability, beg_lt_liability, end_lt_liability,
				 principal_remeasurement, liability_remeasurement,
				 initial_direct_cost, incentive_amount, beg_prepaid_rent,
				 prepay_amortization, prepaid_rent, end_prepaid_rent,
				 idc_math_amount, incentive_math_amount, beg_net_rou_asset,
				 end_net_rou_asset, rou_asset_remeasurement, partial_term_gain_loss,
				 additional_rou_asset, executory_adjust, contingent_adjust,
				 beg_arrears_accrual, arrears_accrual, end_arrears_accrual,
				 remaining_principal, st_obligation_remeasurement,
				 lt_obligation_remeasurement, obligation_reclass,
				 unaccrued_interest_remeasure, unaccrued_interest_reclass,
				 payment_month, escalation_month, escalation_pct,
				 impairment_activity, begin_accum_impair, end_accum_impair,
				 incentive_cap_amount
		  FROM   ls_ilr_schedule
		  WHERE  ilr_id = a_ilr_id_from
		  AND    revision = a_ilr_curr_revision_from
		  AND MONTH <= a_transfer_date;

	  RETURN 'OK';

	EXCEPTION
	  WHEN OTHERS THEN
		l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
		RETURN l_status;
	END f_copy_ilr_schedule_trf;
   
	/************************************************************************************************************
	** f_copy_asset_schedule_trf
	**
	** Copies Schedules for Transfers
	** On FROM Asset it copies schedule to the new transfer revision up to tansfer date
	** On TO Asset it copies schedule to the new transfer revision 
	** 
	** Note: on asset tranfers within the same ILR the revisions are the same asset id to and from are different
	**************************************************************************************************************/
	FUNCTION f_copy_asset_schedule_trf(a_asset_id_from       IN NUMBER,
									   a_asset_curr_revision_from IN NUMBER,
									   a_asset_revision_from in number,
									   a_asset_id_to         IN NUMBER,
									   a_asset_revision_to   IN NUMBER,
									   a_transfer_date       IN DATE,
									   a_interco_trans in boolean)
	  RETURN VARCHAR2 IS

	  l_status VARCHAR2(2000);
	BEGIN
	  pkg_pp_log.p_write_message('Deleting from ls_asset_schedule for FROM asset');
	  DELETE FROM ls_asset_schedule
	  WHERE  ls_asset_id = a_asset_id_from
	  AND    revision = a_asset_revision_from;
	  
	  pkg_pp_log.p_write_message('Deleting from ls_asset_schedule_tax for FROM asset');
	  DELETE FROM ls_asset_schedule_tax
	  WHERE  ls_asset_id = a_asset_id_from
	  AND    revision = a_asset_revision_from;
	  
	  pkg_pp_log.p_write_message('Deleting from ls_depr_forecast for FROM asset');
	  DELETE FROM ls_depr_forecast
	  WHERE  ls_asset_id = a_asset_id_from
	  AND    revision = a_asset_revision_from;
	  
	  pkg_pp_log.p_write_message('Inserting into ls_asset_schedule for FROM asset');
	  INSERT INTO ls_asset_schedule
		(ls_asset_id, revision, set_of_books_id, MONTH, residual_amount,
		 term_penalty, bpo_price, beg_capital_cost, end_capital_cost, current_lease_cost,
		 beg_obligation, end_obligation, beg_lt_obligation, end_lt_obligation,
		 beg_liability, end_liability, beg_lt_liability, end_lt_liability,
		 interest_accrual, principal_accrual, interest_paid, principal_paid,
		 executory_accrual1, executory_accrual2, executory_accrual3,
		 executory_accrual4, executory_accrual5, executory_accrual6,
		 executory_accrual7, executory_accrual8, executory_accrual9,
		 executory_accrual10, executory_paid1, executory_paid2, executory_paid3,
		 executory_paid4, executory_paid5, executory_paid6, executory_paid7,
		 executory_paid8, executory_paid9, executory_paid10, contingent_accrual1,
		 contingent_accrual2, contingent_accrual3, contingent_accrual4,
		 contingent_accrual5, contingent_accrual6, contingent_accrual7,
		 contingent_accrual8, contingent_accrual9, contingent_accrual10,
		 contingent_paid1, contingent_paid2, contingent_paid3, contingent_paid4,
		 contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8,
		 contingent_paid9, contingent_paid10, is_om, beg_deferred_rent,
		 deferred_rent, end_deferred_rent, beg_st_deferred_rent,
		 end_st_deferred_rent, beg_prepaid_rent, prepay_amortization,
		 prepaid_rent, end_prepaid_rent, incentive_amount, initial_direct_cost,
		 incentive_math_amount, idc_math_amount, beg_net_rou_asset,
		 end_net_rou_asset, rou_asset_remeasurement, partial_term_gain_loss,
		 principal_remeasurement, liability_remeasurement, beg_arrears_accrual,
		 arrears_accrual, end_arrears_accrual, additional_rou_asset,
		 executory_adjust, contingent_adjust, st_obligation_remeasurement,
		 lt_obligation_remeasurement, obligation_reclass,
		 unaccrued_interest_remeasure, unaccrued_interest_reclass,
		 begin_accum_impair, impairment_activity, end_accum_impair,
		 partial_month_percent, incentive_cap_amount)
		SELECT a_asset_id_from, a_asset_revision_from, set_of_books_id, MONTH,
			   residual_amount, term_penalty, bpo_price, beg_capital_cost,
			   end_capital_cost, current_lease_cost, beg_obligation, end_obligation,
			   beg_lt_obligation, end_lt_obligation, beg_liability,
			   end_liability, beg_lt_liability, end_lt_liability,
			   interest_accrual, principal_accrual, interest_paid,
			   principal_paid, executory_accrual1, executory_accrual2,
			   executory_accrual3, executory_accrual4, executory_accrual5,
			   executory_accrual6, executory_accrual7, executory_accrual8,
			   executory_accrual9, executory_accrual10, executory_paid1,
			   executory_paid2, executory_paid3, executory_paid4,
			   executory_paid5, executory_paid6, executory_paid7,
			   executory_paid8, executory_paid9, executory_paid10,
			   contingent_accrual1, contingent_accrual2, contingent_accrual3,
			   contingent_accrual4, contingent_accrual5, contingent_accrual6,
			   contingent_accrual7, contingent_accrual8, contingent_accrual9,
			   contingent_accrual10, contingent_paid1, contingent_paid2,
			   contingent_paid3, contingent_paid4, contingent_paid5,
			   contingent_paid6, contingent_paid7, contingent_paid8,
			   contingent_paid9, contingent_paid10, is_om, beg_deferred_rent,
			   deferred_rent, end_deferred_rent, beg_st_deferred_rent,
			   end_st_deferred_rent, beg_prepaid_rent, prepay_amortization,
			   prepaid_rent, end_prepaid_rent, incentive_amount,
			   initial_direct_cost, incentive_math_amount, idc_math_amount,
			   beg_net_rou_asset, end_net_rou_asset, rou_asset_remeasurement,
			   partial_term_gain_loss, principal_remeasurement,
			   liability_remeasurement, beg_arrears_accrual, arrears_accrual,
			   end_arrears_accrual, additional_rou_asset, executory_adjust,
			   contingent_adjust, st_obligation_remeasurement,
			   lt_obligation_remeasurement, obligation_reclass,
			   unaccrued_interest_remeasure, unaccrued_interest_reclass,
			   begin_accum_impair, impairment_activity, end_accum_impair,
			   partial_month_percent, incentive_cap_amount
		FROM   ls_asset_schedule
		WHERE  ls_asset_id = a_asset_id_from
		AND    revision = a_asset_curr_revision_from
		AND    MONTH <= a_transfer_date;
		
	  pkg_pp_log.p_write_message('Inserting into ls_asset_schedule_tax for FROM asset');
	  INSERT INTO ls_asset_schedule_tax
		(ls_asset_id, revision, set_of_books_id, schedule_month,
		 gl_posting_mo_yr, tax_local_id, vendor_id, tax_district_id, tax_base,
		 tax_rate, payment_amount, accrual_amount)
		SELECT a_asset_id_from, a_asset_revision_from, set_of_books_id, schedule_month,
			   gl_posting_mo_yr, tax_local_id, vendor_id, tax_district_id,
			   tax_base, tax_rate, payment_amount, accrual_amount
		FROM   ls_asset_schedule_tax
		WHERE  ls_asset_id = a_asset_id_from
		AND    revision = a_asset_curr_revision_from
		AND    schedule_month < a_transfer_date;

	  pkg_pp_log.p_write_message('Inserting into ls_depr_forecast for FROM asset');		
	  insert into ls_depr_forecast
		(LS_ASSET_ID, REVISION,	SET_OF_BOOKS_ID, MONTH, 
		BEGIN_RESERVE, DEPR_EXPENSE, DEPR_EXP_ALLOC_ADJUST,
		END_RESERVE,PARTIAL_MONTH_PERCENT, DEPR_GROUP_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, 
		DEPR_EXP_COMP_CURR, DEPR_EXP_AA_COMP_CURR, EXCHANGE_RATE
		)
		select a_asset_id_from, a_asset_revision_from, SET_OF_BOOKS_ID,	MONTH,
				BEGIN_RESERVE, DEPR_EXPENSE, DEPR_EXP_ALLOC_ADJUST,
				END_RESERVE, PARTIAL_MONTH_PERCENT, DEPR_GROUP_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV,
				DEPR_EXP_COMP_CURR, DEPR_EXP_AA_COMP_CURR, EXCHANGE_RATE
		FROM ls_depr_forecast ldf
		WHERE  ls_asset_id = a_asset_id_from
		AND    revision = a_asset_curr_revision_from
		AND    month < a_transfer_date;
	  
	  if not a_interco_trans then --Transfers within ILR take place on one revision
		  pkg_pp_log.p_write_message('Deleting from ls_asset_schedule for TO asset');

		  DELETE FROM ls_asset_schedule
		  WHERE  ls_asset_id = a_asset_id_to
		  AND    revision = a_asset_revision_to;
		  
		  pkg_pp_log.p_write_message('Deleting from ls_asset_schedule_tax for TO asset');
		  DELETE FROM ls_asset_schedule_tax
		  WHERE  ls_asset_id = a_asset_id_to
		  AND    revision = a_asset_revision_to;
		  
		  pkg_pp_log.p_write_message('Deleting from ls_depr_forecast for TO asset');
		  DELETE FROM ls_depr_forecast
		  WHERE  ls_asset_id = a_asset_id_to
		  AND    revision = a_asset_revision_to;
		  		  
		  pkg_pp_log.p_write_message('Deleting from ls_monthly_tax for TO asset');
		  DELETE FROM ls_monthly_tax WHERE ls_asset_id = a_asset_id_to;
		  
	  end if;
	  
	  pkg_pp_log.p_write_message('Inserting into ls_asset_schedule for TO asset');
	  INSERT INTO ls_asset_schedule
		(ls_asset_id, revision, set_of_books_id, MONTH, residual_amount,
		 term_penalty, bpo_price, beg_capital_cost, end_capital_cost, current_lease_cost,
		 beg_obligation, end_obligation, beg_lt_obligation, end_lt_obligation,
		 beg_liability, end_liability, beg_lt_liability, end_lt_liability,
		 interest_accrual, principal_accrual, interest_paid, principal_paid,
		 executory_accrual1, executory_accrual2, executory_accrual3,
		 executory_accrual4, executory_accrual5, executory_accrual6,
		 executory_accrual7, executory_accrual8, executory_accrual9,
		 executory_accrual10, executory_paid1, executory_paid2, executory_paid3,
		 executory_paid4, executory_paid5, executory_paid6, executory_paid7,
		 executory_paid8, executory_paid9, executory_paid10, contingent_accrual1,
		 contingent_accrual2, contingent_accrual3, contingent_accrual4,
		 contingent_accrual5, contingent_accrual6, contingent_accrual7,
		 contingent_accrual8, contingent_accrual9, contingent_accrual10,
		 contingent_paid1, contingent_paid2, contingent_paid3, contingent_paid4,
		 contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8,
		 contingent_paid9, contingent_paid10, is_om, beg_deferred_rent,
		 deferred_rent, end_deferred_rent, beg_st_deferred_rent,
		 end_st_deferred_rent, beg_prepaid_rent, prepay_amortization,
		 prepaid_rent, end_prepaid_rent, incentive_amount, initial_direct_cost,
		 incentive_math_amount, idc_math_amount, beg_net_rou_asset,
		 end_net_rou_asset, rou_asset_remeasurement, partial_term_gain_loss,
		 principal_remeasurement, liability_remeasurement, beg_arrears_accrual,
		 arrears_accrual, end_arrears_accrual, additional_rou_asset,
		 executory_adjust, contingent_adjust, st_obligation_remeasurement,
		 lt_obligation_remeasurement, obligation_reclass,
		 unaccrued_interest_remeasure, unaccrued_interest_reclass,
		 begin_accum_impair, impairment_activity, end_accum_impair,
		 partial_month_percent, incentive_cap_amount)
		SELECT a_asset_id_to, a_asset_revision_to, set_of_books_id, MONTH,
			   residual_amount, term_penalty, bpo_price, beg_capital_cost,
			   end_capital_cost, current_lease_cost, beg_obligation, end_obligation,
			   beg_lt_obligation, end_lt_obligation, beg_liability,
			   end_liability, beg_lt_liability, end_lt_liability,
			   interest_accrual, principal_accrual, interest_paid,
			   principal_paid, executory_accrual1, executory_accrual2,
			   executory_accrual3, executory_accrual4, executory_accrual5,
			   executory_accrual6, executory_accrual7, executory_accrual8,
			   executory_accrual9, executory_accrual10, executory_paid1,
			   executory_paid2, executory_paid3, executory_paid4,
			   executory_paid5, executory_paid6, executory_paid7,
			   executory_paid8, executory_paid9, executory_paid10,
			   contingent_accrual1, contingent_accrual2, contingent_accrual3,
			   contingent_accrual4, contingent_accrual5, contingent_accrual6,
			   contingent_accrual7, contingent_accrual8, contingent_accrual9,
			   contingent_accrual10, contingent_paid1, contingent_paid2,
			   contingent_paid3, contingent_paid4, contingent_paid5,
			   contingent_paid6, contingent_paid7, contingent_paid8,
			   contingent_paid9, contingent_paid10, is_om, beg_deferred_rent,
			   deferred_rent, end_deferred_rent, beg_st_deferred_rent,
			   end_st_deferred_rent, beg_prepaid_rent, prepay_amortization,
			   prepaid_rent, end_prepaid_rent, incentive_amount,
			   initial_direct_cost, incentive_math_amount, idc_math_amount,
			   beg_net_rou_asset, end_net_rou_asset, rou_asset_remeasurement,
			   partial_term_gain_loss, principal_remeasurement,
			   liability_remeasurement, beg_arrears_accrual, arrears_accrual,
			   end_arrears_accrual, additional_rou_asset, executory_adjust,
			   contingent_adjust, st_obligation_remeasurement,
			   lt_obligation_remeasurement, obligation_reclass,
			   unaccrued_interest_remeasure, unaccrued_interest_reclass,
			   begin_accum_impair, impairment_activity, end_accum_impair,
			   partial_month_percent, incentive_cap_amount
		FROM   ls_asset_schedule
		WHERE  ls_asset_id = a_asset_id_from
		AND    revision = a_asset_curr_revision_from
		AND    MONTH >= a_transfer_date;

	  pkg_pp_log.p_write_message('Inserting into ls_monthly_tax for TO asset');
	  INSERT INTO ls_monthly_tax
		(ls_asset_id, set_of_books_id, schedule_month, gl_posting_mo_yr,
		 tax_local_id, vendor_id, tax_district_id, tax_base, tax_rate,
		 payment_amount, accrual_amount, adjustment_amount)
		SELECT a_asset_id_to, set_of_books_id, schedule_month, gl_posting_mo_yr,
			   tax_local_id, vendor_id, tax_district_id, tax_base, tax_rate,
			   payment_amount, accrual_amount, adjustment_amount
		FROM   ls_monthly_tax
		WHERE  ls_asset_id = a_asset_id_from
		AND    schedule_month >= a_transfer_date;

	  pkg_pp_log.p_write_message('Inserting into ls_asset_schedule_tax for TO asset');
	  INSERT INTO ls_asset_schedule_tax
		(ls_asset_id, revision, set_of_books_id, schedule_month,
		 gl_posting_mo_yr, tax_local_id, vendor_id, tax_district_id, tax_base,
		 tax_rate, payment_amount, accrual_amount)
		SELECT a_asset_id_to, a_asset_revision_to, set_of_books_id, schedule_month,
			   gl_posting_mo_yr, tax_local_id, vendor_id, tax_district_id,
			   tax_base, tax_rate, payment_amount, accrual_amount
		FROM   ls_asset_schedule_tax
		WHERE  ls_asset_id = a_asset_id_from
		AND    revision = a_asset_curr_revision_from
		AND    schedule_month >= a_transfer_date;

	  pkg_pp_log.p_write_message('Inserting into ls_depr_forecast for TO asset');		
	  insert into ls_depr_forecast
		(LS_ASSET_ID, REVISION,	SET_OF_BOOKS_ID, MONTH, 
		BEGIN_RESERVE, DEPR_EXPENSE, DEPR_EXP_ALLOC_ADJUST,
		END_RESERVE,PARTIAL_MONTH_PERCENT, DEPR_GROUP_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV,
		DEPR_EXP_COMP_CURR, DEPR_EXP_AA_COMP_CURR, EXCHANGE_RATE
		)
		select a_asset_id_to, a_asset_revision_to, SET_OF_BOOKS_ID,	MONTH,
				BEGIN_RESERVE, DEPR_EXPENSE, DEPR_EXP_ALLOC_ADJUST,
				END_RESERVE, PARTIAL_MONTH_PERCENT, DEPR_GROUP_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV,
				DEPR_EXP_COMP_CURR, DEPR_EXP_AA_COMP_CURR, EXCHANGE_RATE
		FROM ls_depr_forecast ldf
		WHERE  ls_asset_id = a_asset_id_from
		AND    revision = a_asset_curr_revision_from
		AND    month >= a_transfer_date;

	  RETURN 'OK';

	EXCEPTION
	  WHEN OTHERS THEN
		l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
		RETURN l_status;
	END f_copy_asset_schedule_trf;


function f_copy_asset_schedule_rows(A_MONTH in date)
  return varchar2
  is
  begin

  PKG_PP_LOG.P_WRITE_MESSAGE('Deleting from ls_asset_schedule');

  delete from ls_asset_schedule
  where (ls_asset_id, revision, set_of_books_id) in
  (select la.ls_asset_id, revision, stg.set_of_books_id
   from ls_asset la, ls_ilr ilr, ls_ilr_stg stg
   where la.ilr_id = ilr.ilr_id
    and ilr.ilr_id = stg.ilr_id
    and la.ls_asset_id = nvl(g_copy_ls_asset_id, la.ls_asset_id));

   PKG_PP_LOG.P_WRITE_MESSAGE('Copying prior rows in from ls_asset_schedule');
  insert into ls_asset_schedule
    (LS_ASSET_ID,
    REVISION,
    SET_OF_BOOKS_ID,
    MONTH,
    RESIDUAL_AMOUNT,
    TERM_PENALTY,
    BPO_PRICE,
    BEG_CAPITAL_COST,
    END_CAPITAL_COST,
    BEG_OBLIGATION,
    END_OBLIGATION,
    BEG_LT_OBLIGATION,
    END_LT_OBLIGATION,
    BEG_LIABILITY,
    END_LIABILITY,
    BEG_LT_LIABILITY,
    END_LT_LIABILITY,
    INTEREST_ACCRUAL,
    PRINCIPAL_ACCRUAL,
    INTEREST_PAID,
    PRINCIPAL_PAID,
    EXECUTORY_ACCRUAL1,
    EXECUTORY_ACCRUAL2,
    EXECUTORY_ACCRUAL3,
    EXECUTORY_ACCRUAL4,
    EXECUTORY_ACCRUAL5,
    EXECUTORY_ACCRUAL6,
    EXECUTORY_ACCRUAL7,
    EXECUTORY_ACCRUAL8,
    EXECUTORY_ACCRUAL9,
    EXECUTORY_ACCRUAL10,
    EXECUTORY_PAID1,
    EXECUTORY_PAID2,
    EXECUTORY_PAID3,
    EXECUTORY_PAID4,
    EXECUTORY_PAID5,
    EXECUTORY_PAID6,
    EXECUTORY_PAID7,
    EXECUTORY_PAID8,
    EXECUTORY_PAID9,
    EXECUTORY_PAID10,
    CONTINGENT_ACCRUAL1,
    CONTINGENT_ACCRUAL2,
    CONTINGENT_ACCRUAL3,
    CONTINGENT_ACCRUAL4,
    CONTINGENT_ACCRUAL5,
    CONTINGENT_ACCRUAL6,
    CONTINGENT_ACCRUAL7,
    CONTINGENT_ACCRUAL8,
    CONTINGENT_ACCRUAL9,
    CONTINGENT_ACCRUAL10,
    CONTINGENT_PAID1,
    CONTINGENT_PAID2,
    CONTINGENT_PAID3,
    CONTINGENT_PAID4,
    CONTINGENT_PAID5,
    CONTINGENT_PAID6,
    CONTINGENT_PAID7,
    CONTINGENT_PAID8,
    CONTINGENT_PAID9,
    CONTINGENT_PAID10,
    IS_OM,
    BEG_DEFERRED_RENT,
    DEFERRED_RENT,
    END_DEFERRED_RENT,
    BEG_ST_DEFERRED_RENT,
    END_ST_DEFERRED_RENT,
    beg_prepaid_rent,
    prepay_amortization,
    prepaid_rent,
    end_prepaid_rent,
    INCENTIVE_AMOUNT,
    INITIAL_DIRECT_COST,
    INCENTIVE_MATH_AMOUNT,
    IDC_MATH_AMOUNT,
    beg_net_rou_asset,
    end_net_rou_asset,
    rou_asset_remeasurement,
    partial_term_gain_loss,
    principal_remeasurement,
    liability_remeasurement,
    beg_arrears_accrual,
    arrears_accrual,
    end_arrears_accrual,
    additional_rou_asset,
    executory_adjust,
    contingent_adjust,
    st_obligation_remeasurement,
    lt_obligation_remeasurement,
    obligation_reclass,
    unaccrued_interest_remeasure,
    unaccrued_interest_reclass,
    begin_accum_impair,
    impairment_activity,
    end_accum_impair,
	partial_month_percent,
	incentive_cap_amount
    )
    select
    la.ls_asset_id,
    stg.revision,
    a.SET_OF_BOOKS_ID,
    a.MONTH,
    a.RESIDUAL_AMOUNT,
    a.TERM_PENALTY,
    a.BPO_PRICE,
    a.BEG_CAPITAL_COST,
    a.END_CAPITAL_COST,
    Decode(remeasurement.om_to_cap_indicator, 1, 0, a.BEG_OBLIGATION),
    Decode(remeasurement.om_to_cap_indicator, 1, 0, a.END_OBLIGATION),
    decode(a.is_om, 1, 0, a.BEG_LT_OBLIGATION),
    decode(a.is_om,1,0,a.END_LT_OBLIGATION),
    a.BEG_LIABILITY,
    a.END_LIABILITY,
    decode(a.is_om, 1, 0, a.BEG_LT_LIABILITY),
    decode(a.is_om,1,0,a.END_LT_LIABILITY),
    a.INTEREST_ACCRUAL,
    a.PRINCIPAL_ACCRUAL,
    a.INTEREST_PAID,
    a.PRINCIPAL_PAID,
    a.EXECUTORY_ACCRUAL1,
    a.EXECUTORY_ACCRUAL2,
    a.EXECUTORY_ACCRUAL3,
    a.EXECUTORY_ACCRUAL4,
    a.EXECUTORY_ACCRUAL5,
    a.EXECUTORY_ACCRUAL6,
    a.EXECUTORY_ACCRUAL7,
    a.EXECUTORY_ACCRUAL8,
    a.EXECUTORY_ACCRUAL9,
    a.EXECUTORY_ACCRUAL10,
    a.EXECUTORY_PAID1,
    a.EXECUTORY_PAID2,
    a.EXECUTORY_PAID3,
    a.EXECUTORY_PAID4,
    a.EXECUTORY_PAID5,
    a.EXECUTORY_PAID6,
    a.EXECUTORY_PAID7,
    a.EXECUTORY_PAID8,
    a.EXECUTORY_PAID9,
    a.EXECUTORY_PAID10,
    a.CONTINGENT_ACCRUAL1,
    a.CONTINGENT_ACCRUAL2,
    a.CONTINGENT_ACCRUAL3,
    a.CONTINGENT_ACCRUAL4,
    a.CONTINGENT_ACCRUAL5,
    a.CONTINGENT_ACCRUAL6,
    a.CONTINGENT_ACCRUAL7,
    a.CONTINGENT_ACCRUAL8,
    a.CONTINGENT_ACCRUAL9,
    a.CONTINGENT_ACCRUAL10,
    a.CONTINGENT_PAID1,
    a.CONTINGENT_PAID2,
    a.CONTINGENT_PAID3,
    a.CONTINGENT_PAID4,
    a.CONTINGENT_PAID5,
    a.CONTINGENT_PAID6,
    a.CONTINGENT_PAID7,
    a.CONTINGENT_PAID8,
    a.CONTINGENT_PAID9,
    a.CONTINGENT_PAID10,
    a.IS_OM,
    a.BEG_DEFERRED_RENT,
    a.DEFERRED_RENT,
    a.END_DEFERRED_RENT,
    a.BEG_ST_DEFERRED_RENT,
    a.END_ST_DEFERRED_RENT,
    a.beg_prepaid_rent,
    a.prepay_amortization,
    a.prepaid_rent,
    a.end_prepaid_rent,
    A.INCENTIVE_AMOUNT,
    A.INITIAL_DIRECT_COST,
    A.INCENTIVE_MATH_AMOUNT,
    A.IDC_MATH_AMOUNT,
    a.beg_net_ROU_asset,
    a.end_net_ROU_asset,
    a.rou_asset_remeasurement,
    a.partial_term_gain_loss,
    a.principal_remeasurement,
    a.liability_remeasurement,
    a.beg_arrears_accrual,
    a.arrears_accrual,
    a.end_arrears_accrual,
    a.additional_rou_asset,
    a.executory_adjust,
    a.contingent_adjust,
    a.st_obligation_remeasurement,
    a.lt_obligation_remeasurement,
    a.obligation_reclass,
    a.unaccrued_interest_remeasure,
    a.unaccrued_interest_reclass,
    a.begin_accum_impair,
    a.impairment_activity,
    a.end_accum_impair,
	a.partial_month_percent,
	a.incentive_cap_amount
    from ls_asset_schedule a
    JOIN ls_asset la ON a.ls_asset_id = la.ls_asset_id AND a.revision = la.approved_revision
    JOIN ls_ilr_stg stg ON stg.set_of_books_id = a.set_of_books_id AND stg.ilr_id = la.ilr_id
    JOIN ls_ilr_options o ON o.ilr_id = stg.ilr_id AND o.revision = stg.revision
    left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON (
      a.ls_asset_id = remeasurement.ls_asset_id
      AND a.revision = remeasurement.current_revision
      AND a.set_of_books_id = remeasurement.set_of_books_id
      AND a.month < Trunc(remeasurement.remeasurement_date, 'month'))
    WHERE a.ls_asset_id = nvl(g_copy_ls_asset_id, a.ls_asset_id)
      AND a.month < Greatest(A_MONTH, Trunc(Nvl(o.remeasurement_date, To_Date('180001', 'yyyymm')), 'month'));

  PKG_PP_LOG.P_WRITE_MESSAGE('Deleting from ls_asset_schedule_tax');

  delete from ls_asset_schedule_tax
  where (ls_asset_id, revision, set_of_books_id) in
  (select la.ls_asset_id, revision, stg.set_of_books_id
   from ls_asset la, ls_ilr ilr, ls_ilr_stg stg
   where la.ilr_id = ilr.ilr_id
    and ilr.ilr_id = stg.ilr_id
    and la.ls_asset_id = nvl(g_copy_ls_asset_id, la.ls_asset_id));

   PKG_PP_LOG.P_WRITE_MESSAGE('Copying prior rows in from ls_asset_schedule');
  insert into ls_asset_schedule_tax (
    ls_asset_id, revision, set_of_books_id, schedule_month, gl_posting_mo_yr,
    tax_local_id, vendor_id, tax_district_id, tax_base, tax_rate, payment_amount)
  select a.ls_asset_id, stg.revision, a.set_of_books_id, a.schedule_month, a.gl_posting_mo_yr,
         a.tax_local_id, a.vendor_id, a.tax_district_id, a.tax_base, a.tax_rate, a.payment_amount
    from ls_asset_schedule_tax a
    JOIN ls_asset la ON a.ls_asset_id = la.ls_asset_id AND a.revision = la.approved_revision
    JOIN ls_ilr_stg stg ON stg.set_of_books_id = a.set_of_books_id AND stg.ilr_id = la.ilr_id
    JOIN ls_ilr_options o ON o.ilr_id = stg.ilr_id AND o.revision = stg.revision
    left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON (
      a.ls_asset_id = remeasurement.ls_asset_id
      AND a.revision = remeasurement.current_revision
      AND a.set_of_books_id = remeasurement.set_of_books_id
      AND a.schedule_month < Trunc(remeasurement.remeasurement_date, 'month'))
    WHERE a.ls_asset_id = nvl(g_copy_ls_asset_id, a.ls_asset_id)
      AND a.schedule_month < Greatest(A_MONTH, Trunc(Nvl(o.remeasurement_date, To_Date('180001', 'yyyymm')), 'month'));

  return 'OK';
  exception when others then
    PKG_PP_LOG.P_WRITE_MESSAGE('ERROR');
    PKG_PP_LOG.P_WRITE_MESSAGE('Error Copying Asset Schedule: ' || sqlerrm);
    return 'Error Copying Asset Schedule: ' || sqlerrm;
end f_copy_asset_schedule_rows;


function f_copy_ilr_schedule_rows(A_MONTH in date)
  return varchar2
  is
  counter number;
  v_ilr_id number:=null;
  begin

  select count(1)
  into counter
  from ls_asset
  where ls_asset_id = g_copy_ls_asset_id;

  if counter=1 then
    select ilr_id
    into v_ilr_id
    from ls_asset where ls_asset_id =  G_COPY_LS_ASSET_ID;
  end if;

  delete from ls_ilr_schedule
  where (ilr_id, revision, set_of_books_id) in
  (select ilr_id, revision, set_of_books_id
   from ls_ilr_stg);

   insert into LS_ILR_SCHEDULE
    (ilr_id,
    REVISION,
    SET_OF_BOOKS_ID,
    MONTH,
    RESIDUAL_AMOUNT,
    TERM_PENALTY,
    BPO_PRICE,
    BEG_CAPITAL_COST,
    END_CAPITAL_COST,
    BEG_OBLIGATION,
    END_OBLIGATION,
    BEG_LT_OBLIGATION,
    END_LT_OBLIGATION,
    BEG_LIABILITY,
    END_LIABILITY,
    BEG_LT_LIABILITY,
    END_LT_LIABILITY,
    INTEREST_ACCRUAL,
    PRINCIPAL_ACCRUAL,
    INTEREST_PAID,
    PRINCIPAL_PAID,
    EXECUTORY_ACCRUAL1,
    EXECUTORY_ACCRUAL2,
    EXECUTORY_ACCRUAL3,
    EXECUTORY_ACCRUAL4,
    EXECUTORY_ACCRUAL5,
    EXECUTORY_ACCRUAL6,
    EXECUTORY_ACCRUAL7,
    EXECUTORY_ACCRUAL8,
    EXECUTORY_ACCRUAL9,
    EXECUTORY_ACCRUAL10,
    EXECUTORY_PAID1,
    EXECUTORY_PAID2,
    EXECUTORY_PAID3,
    EXECUTORY_PAID4,
    EXECUTORY_PAID5,
    EXECUTORY_PAID6,
    EXECUTORY_PAID7,
    EXECUTORY_PAID8,
    EXECUTORY_PAID9,
    EXECUTORY_PAID10,
    CONTINGENT_ACCRUAL1,
    CONTINGENT_ACCRUAL2,
    CONTINGENT_ACCRUAL3,
    CONTINGENT_ACCRUAL4,
    CONTINGENT_ACCRUAL5,
    CONTINGENT_ACCRUAL6,
    CONTINGENT_ACCRUAL7,
    CONTINGENT_ACCRUAL8,
    CONTINGENT_ACCRUAL9,
    CONTINGENT_ACCRUAL10,
    CONTINGENT_PAID1,
    CONTINGENT_PAID2,
    CONTINGENT_PAID3,
    CONTINGENT_PAID4,
    CONTINGENT_PAID5,
    CONTINGENT_PAID6,
    CONTINGENT_PAID7,
    CONTINGENT_PAID8,
    CONTINGENT_PAID9,
    CONTINGENT_PAID10,
    IS_OM,
    BEG_DEFERRED_RENT,
    DEFERRED_RENT,
    END_DEFERRED_RENT,
    BEG_ST_DEFERRED_RENT,
    END_ST_DEFERRED_RENT,
    beg_prepaid_rent,
    prepay_amortization,
    prepaid_rent,
    end_prepaid_rent,
    INCENTIVE_AMOUNT,
	INCENTIVE_MATH_AMOUNT,
    INITIAL_DIRECT_COST,
	IDC_MATH_AMOUNT,
    beg_net_rou_asset,
    end_net_rou_asset,
    rou_asset_remeasurement,
    partial_term_gain_loss,
    principal_remeasurement,
    liability_remeasurement,
    beg_arrears_accrual,
    arrears_accrual,
    end_arrears_accrual,
    additional_rou_asset,
    executory_adjust,
    contingent_adjust,
    st_obligation_remeasurement,
    lt_obligation_remeasurement,
    obligation_reclass,
    unaccrued_interest_remeasure,
    unaccrued_interest_reclass,
	escalation_month,
	escalation_pct,
	impairment_activity,
	begin_accum_impair,
	end_accum_impair,
	incentive_cap_amount)
    select distinct
    stg.ilr_id,
    stg.revision,
    a.SET_OF_BOOKS_ID,
    a.MONTH,
    a.RESIDUAL_AMOUNT,
    a.TERM_PENALTY,
    a.BPO_PRICE,
    a.BEG_CAPITAL_COST,
    a.END_CAPITAL_COST,
    Decode(remeasurement.om_to_cap_indicator, 1, 0, a.BEG_OBLIGATION),
    Decode(remeasurement.om_to_cap_indicator, 1, 0, a.END_OBLIGATION),
    decode(a.is_om, 1, 0, a.BEG_LT_OBLIGATION),
    decode(a.is_om,1,0,a.END_LT_OBLIGATION),
    a.BEG_LIABILITY,
    a.END_LIABILITY,
    decode(a.is_om, 1, 0, a.BEG_LT_LIABILITY),
    decode(a.is_om,1,0,a.END_LT_LIABILITY),
    a.INTEREST_ACCRUAL,
    a.PRINCIPAL_ACCRUAL,
    a.INTEREST_PAID,
    a.PRINCIPAL_PAID,
    a.EXECUTORY_ACCRUAL1,
    a.EXECUTORY_ACCRUAL2,
    a.EXECUTORY_ACCRUAL3,
    a.EXECUTORY_ACCRUAL4,
    a.EXECUTORY_ACCRUAL5,
    a.EXECUTORY_ACCRUAL6,
    a.EXECUTORY_ACCRUAL7,
    a.EXECUTORY_ACCRUAL8,
    a.EXECUTORY_ACCRUAL9,
    a.EXECUTORY_ACCRUAL10,
    a.EXECUTORY_PAID1,
    a.EXECUTORY_PAID2,
    a.EXECUTORY_PAID3,
    a.EXECUTORY_PAID4,
    a.EXECUTORY_PAID5,
    a.EXECUTORY_PAID6,
    a.EXECUTORY_PAID7,
    a.EXECUTORY_PAID8,
    a.EXECUTORY_PAID9,
    a.EXECUTORY_PAID10,
    a.CONTINGENT_ACCRUAL1,
    a.CONTINGENT_ACCRUAL2,
    a.CONTINGENT_ACCRUAL3,
    a.CONTINGENT_ACCRUAL4,
    a.CONTINGENT_ACCRUAL5,
    a.CONTINGENT_ACCRUAL6,
    a.CONTINGENT_ACCRUAL7,
    a.CONTINGENT_ACCRUAL8,
    a.CONTINGENT_ACCRUAL9,
    a.CONTINGENT_ACCRUAL10,
    a.CONTINGENT_PAID1,
    a.CONTINGENT_PAID2,
    a.CONTINGENT_PAID3,
    a.CONTINGENT_PAID4,
    a.CONTINGENT_PAID5,
    a.CONTINGENT_PAID6,
    a.CONTINGENT_PAID7,
    a.CONTINGENT_PAID8,
    a.CONTINGENT_PAID9,
    a.CONTINGENT_PAID10,
    a.IS_OM,
    a.BEG_DEFERRED_RENT,
    a.DEFERRED_RENT,
    a.END_DEFERRED_RENT,
    a.BEG_ST_DEFERRED_RENT,
    a.END_ST_DEFERRED_RENT,
    a.beg_prepaid_rent,
    a.prepay_amortization,
    a.prepaid_rent,
    a.end_prepaid_rent,
    A.INCENTIVE_AMOUNT,
	INCENTIVE_MATH_AMOUNT,
    A.INITIAL_DIRECT_COST,
	IDC_MATH_AMOUNT,
    a.beg_net_rou_asset,
    a.end_net_rou_asset,
    a.rou_asset_remeasurement,
    a.partial_term_gain_loss,
    a.principal_remeasurement,
    a.liability_remeasurement,
    a.beg_arrears_accrual,
    a.arrears_accrual,
    a.end_arrears_accrual,
    a.additional_rou_asset,
    a.executory_adjust,
    a.contingent_adjust,
    a.st_obligation_remeasurement,
    a.lt_obligation_remeasurement,
    a.obligation_reclass,
    a.unaccrued_interest_remeasure,
    a.unaccrued_interest_reclass,
	a.escalation_month,
	a.escalation_pct,
	a.impairment_activity,
	a.begin_accum_impair,
	a.end_accum_impair,
	a.incentive_cap_amount
    from ls_ilr_schedule a
    JOIN ls_ilr ilr ON a.ilr_id = ilr.ilr_id AND ilr.current_revision = a.revision
    JOIN ls_ilr_stg stg ON a.set_of_books_id=stg.set_of_books_id AND ilr.ilr_id = stg.ilr_id
    JOIN ls_ilr_options o ON o.ilr_id = stg.ilr_id AND o.revision = stg.revision
    left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON (
      a.ilr_id = remeasurement.ilr_id
      AND a.revision = remeasurement.current_revision
      AND a.set_of_books_id = remeasurement.set_of_books_id
      AND a.month < Trunc(remeasurement.remeasurement_date,'month'))
    WHERE a.ilr_id = nvl(V_ILR_ID, a.ilr_id)
    AND a.month < Greatest(A_MONTH, Trunc(Nvl(o.remeasurement_date, To_Date('180001', 'yyyymm')), 'month'));

  return 'OK';
  exception when others then
    return 'Error Copying ILR Schedule: ' || sqlerrm;
end f_copy_ilr_schedule_rows;

function f_copy_depr_forecast_rows(A_MONTH in date)
  return varchar2
  is
  begin

  PKG_PP_LOG.P_WRITE_MESSAGE('Deleting from ls_depr_forecast');

  delete from ls_depr_forecast
  where (ls_asset_id, revision, set_of_books_id) in
  (select la.ls_asset_id, revision, stg.set_of_books_id
   from ls_asset la, ls_ilr ilr, ls_ilr_stg stg
   where la.ilr_id = ilr.ilr_id
    and ilr.ilr_id = stg.ilr_id
    and la.ls_asset_id = nvl(g_copy_ls_asset_id, la.ls_asset_id));

   PKG_PP_LOG.P_WRITE_MESSAGE('Copying prior rows in from ls_depr_forecast');
   -- Pull depr forecast for date < remeasurement date if its remeasurement otherwise < 1000 years from now
   insert into ls_depr_forecast
    (LS_ASSET_ID,
    REVISION,
    SET_OF_BOOKS_ID,
    MONTH,
    BEGIN_RESERVE,
    DEPR_EXPENSE,
    DEPR_EXP_ALLOC_ADJUST,
    END_RESERVE,
	PARTIAL_MONTH_PERCENT
    )
    select
    ldf.LS_ASSET_ID,
    stg.REVISION,
    stg.SET_OF_BOOKS_ID,
    ldf.MONTH,
    ldf.BEGIN_RESERVE,
    ldf.DEPR_EXPENSE,
    ldf.DEPR_EXP_ALLOC_ADJUST,
    ldf.END_RESERVE,
	ldf.PARTIAL_MONTH_PERCENT
    FROM ls_depr_forecast ldf, ls_asset la, ls_ilr_stg stg, ls_ilr_options o
    where ldf.ls_asset_id = la.ls_asset_id
      and ldf.ls_asset_id = nvl(g_copy_ls_asset_id, ldf.ls_asset_id)
      and ldf.set_of_books_id=stg.set_of_books_id
      and la.approved_revision = ldf.revision
      and la.ilr_id = stg.ilr_id
      and o.ilr_id = stg.ilr_id
      and o.revision = stg.revision
      AND ldf.month < Greatest(A_MONTH,
							  Trunc(decode(stg.is_remeasurement, 
												  1, Nvl(o.remeasurement_date, To_Date('180001', 'yyyymm')),
												  To_Date('299912', 'yyyymm')
										  ),'month')
								);
  return 'OK';
  exception when others then
    PKG_PP_LOG.P_WRITE_MESSAGE('ERROR');
    PKG_PP_LOG.P_WRITE_MESSAGE('Error Copying Depr Forecast: ' || sqlerrm);
    return 'Error Copying Depr Forecast: ' || sqlerrm;
end f_copy_depr_forecast_rows;

   function F_LONG_TERM_OBLIGATION return varchar2 is
  L_STATUS varchar2(2000);
  type STG_MONTHS is table of ls_ilr_asset_schedule_stg%rowtype;
  L_STG_MONTHS STG_MONTHS;
   begin

   L_STATUS := 'setting long term obligation and liability';

--L_STG_MONTHS will contain the shifted values
  select s2.* bulk collect into L_STG_MONTHS from ls_ilr_asset_schedule_stg s1, ls_ilr_asset_schedule_stg s2
  where s1.id < 0
  and s2.is_om = 0
  and exists
  (
    select 1
    from ls_ilr_asset_schedule_stg s2
    where s2.month = add_months(s1.month, 12 + s2.prepay_switch)
    and s1.ls_asset_id = s2.ls_asset_id
    and s1.set_of_books_id = s2.set_of_books_id
    and s1.revision = s2.revision
  );

  --Update the staging table with the shifted values
  forall ndx in indices of L_STG_MONTHS
    update ls_ilr_asset_schedule_stg s1
    set beg_lt_obligation = L_STG_MONTHS(ndx).beg_obligation,
        end_lt_obligation = L_STG_MONTHS(ndx).end_obligation,
        beg_lt_liability = L_STG_MONTHS(ndx).beg_liability,
        end_lt_liability = L_STG_MONTHS(ndx).end_liability
    where add_months(s1.month, 12 + L_STG_MONTHS(ndx).prepay_switch) = L_STG_MONTHS(ndx).month
    and s1.ls_asset_id = L_STG_MONTHS(ndx).ls_asset_id
    and s1.set_of_books_id = L_STG_MONTHS(ndx).set_of_books_id
    and s1.revision = L_STG_MONTHS(ndx).revision;

    update ls_ilr_asset_schedule_stg
    set beg_lt_obligation = 0,
        end_lt_obligation = 0,
        beg_lt_liability = 0,
        end_lt_liability = 0
    where is_om = 1;
  return 'OK';
  exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
    return l_status;
   end F_LONG_TERM_OBLIGATION;



   function F_LOAD_COMPONENTS(A_MONTH in date:=null) return varchar2 is
  L_STATUS varchar2(2000);
  begin
  L_STATUS := 'inserting component months into schedule';

  insert into ls_ilr_asset_schedule_stg
  (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, MONTH, AMOUNT,
  RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, PREPAY_SWITCH, PAYMENT_MONTH,
  MONTHS_TO_ACCRUE, RATE, NPV, CURRENT_LEASE_COST, BEG_CAPITAL_COST,
  END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION,
  BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
  UNPAID_BALANCE, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
  EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
  EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
  EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
  EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9,
  EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
  CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
  CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7,
  CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
  IS_OM, PAYMENT_TERM_TYPE_ID,
  BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT)
  select -1 * row_number() over(partition by c.ls_asset_id, c.set_of_books_id order by cal.month desc) as id,
    c.ilr_id,
    c.revision,
    c.ls_asset_id,
    c.set_of_books_id,
    cal.month,
    0 as amount,
    0 as residual_amount,
    0 as term_penalty,
    0 as bpo_price,
    c.prepay_switch as prepay_switch,
    0 as payment_month,
    1 as months_to_accrue,
    0 as rate,
    null as npv,
    null as current_lease_cost,
    case when month = comp_cap_month and comp_cap_month = A_MONTH then end_capital_cost else in_svc_capital_cost end  as beg_capital_cost,
    in_svc_capital_cost as end_capital_cost,
    in_svc_capital_cost as beg_obligation,
    in_svc_capital_cost as end_obligation,
    case when month = comp_cap_month and comp_cap_month = A_MONTH then beg_lt_obligation else in_svc_beg_lt_obligation end as beg_lt_obligation, /* Fix later */
    in_svc_end_lt_obligation as end_lt_obligation,
          in_svc_capital_cost as beg_liability,
    in_svc_capital_cost as end_liability,
    case when month = comp_cap_month and comp_cap_month = A_MONTH then beg_lt_liability else in_svc_beg_lt_liability end as beg_lt_liability,
    in_svc_end_lt_liability as end_lt_liability,
    null as unpaid_balance,
    0 as INTEREST_ACCRUAL,
    0 as PRINCIPAL_ACCRUAL,
    0 as INTEREST_PAID,
    0 as PRINCIPAL_PAID,
    0 as EXECUTORY_ACCRUAL1,
    0 as EXECUTORY_ACCRUAL2,
    0 as EXECUTORY_ACCRUAL3,
    0 as EXECUTORY_ACCRUAL4,
    0 as EXECUTORY_ACCRUAL5,
    0 as EXECUTORY_ACCRUAL6,
    0 as EXECUTORY_ACCRUAL7,
    0 as EXECUTORY_ACCRUAL8,
    0 as EXECUTORY_ACCRUAL9,
    0 as EXECUTORY_ACCRUAL10,
    0 as EXECUTORY_PAID1,
    0 as EXECUTORY_PAID2,
    0 as EXECUTORY_PAID3,
    0 as EXECUTORY_PAID4,
    0 as EXECUTORY_PAID5,
    0 as EXECUTORY_PAID6,
    0 as EXECUTORY_PAID7,
    0 as EXECUTORY_PAID8,
    0 as EXECUTORY_PAID9,
    0 as EXECUTORY_PAID10,
    0 as CONTINGENT_ACCRUAL1,
    0 as CONTINGENT_ACCRUAL2,
    0 as CONTINGENT_ACCRUAL3,
    0 as CONTINGENT_ACCRUAL4,
    0 as CONTINGENT_ACCRUAL5,
    0 as CONTINGENT_ACCRUAL6,
    0 as CONTINGENT_ACCRUAL7,
    0 as CONTINGENT_ACCRUAL8,
    0 as CONTINGENT_ACCRUAL9,
    0 as CONTINGENT_ACCRUAL10,
    0 as CONTINGENT_PAID1,
    0 as CONTINGENT_PAID2,
    0 as CONTINGENT_PAID3,
    0 as CONTINGENT_PAID4,
    0 as CONTINGENT_PAID5,
    0 as CONTINGENT_PAID6,
    0 as CONTINGENT_PAID7,
    0 as CONTINGENT_PAID8,
    0 as CONTINGENT_PAID9,
    0 as CONTINGENT_PAID10,
    is_om as IS_OM,
    2 as PAYMENT_TERM_TYPE_ID,
    0 as BEG_DEFERRED_RENT,
    0 as DEFERRED_RENT,
    0 as END_DEFERRED_RENT,
    0 as BEG_ST_DEFERRED_RENT,
    0 as END_ST_DEFERRED_RENT
    From
    (select distinct la.ls_asset_id, trunc(interim_interest_start_date,'month') comp_cap_month, in_svc.prepay_switch,
        in_svc.month as in_svc_date, ilr.ilr_id, in_svc.end_capital_cost as in_svc_capital_cost, in_svc.revision, in_svc.end_obligation as in_svc_end_obligation,
        in_svc.beg_lt_obligation as in_svc_beg_lt_obligation, in_svc.end_lt_obligation as in_svc_end_lt_obligation,
        nvl(current_sched.end_obligation,0) as end_obligation, nvl(current_sched.end_capital_cost,0) as end_capital_cost, nvl(current_sched.set_of_books_id,1) as set_of_books_id,
        nvl(current_sched.beg_lt_obligation,0) as beg_lt_obligation, nvl(current_sched.end_lt_obligation,0) as end_lt_obligation,
        case when fasb.fasb_cap_type_id in (4,5) then 1 else 0 end as is_om,
        in_svc.beg_lt_liability as in_svc_beg_lt_liability, in_svc.end_lt_liability as in_svc_end_lt_liability,
        nvl(current_sched.end_liability,0) as end_liability,
        nvl(current_sched.beg_lt_liability,0) as beg_lt_liability, nvl(current_sched.end_lt_liability,0) as end_lt_liability
    from ls_component_charge cc, ls_component lc, ls_asset la, ls_ilr ilr, ls_lease ll, ls_ilr_options ilro, ls_lease_cap_type lct,
    (select a.* from ls_ilr_asset_schedule_stg a where id = 1 ) in_svc,
    (select las.ls_asset_id, end_obligation, end_capital_cost, set_of_books_id, end_lt_obligation, beg_lt_obligation,
    end_liability, end_lt_liability, beg_lt_liability
    from ls_asset_schedule las, ls_asset la
    where las.ls_asset_id in (select ls_asset_id from ls_ilr_asset_schedule_stg)
      and la.approved_revision = las.revision
      and las.month = A_MONTH
      and las.ls_asset_id = la.ls_asset_id) current_sched,
    ls_fasb_cap_type_sob_map fasb
    where la.ls_asset_id=lc.ls_asset_id
    and lc.component_id = cc.component_id
    and la.ilr_id=ilr.ilr_id
    and ilr.lease_id = ll.lease_id
    and la.ls_asset_id in (select ls_asset_id from ls_ilr_asset_schedule_stg)
    and in_svc.ls_asset_id = la.ls_asset_id
    and la.ls_asset_id = current_sched.ls_asset_id (+)
    and ilro.ilr_id = ilr.ilr_id
    and ilro.revision = in_svc.revision
    and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
    and fasb.set_of_books_id = current_sched.set_of_books_id
    and fasb.lease_cap_type_id = ilro.lease_cap_type_id
    and trunc(interim_interest_start_date,'MONTH') = A_MONTH) c, pp_calendar cal
    where c.comp_cap_month>=A_MONTH
    and cal.month >= c.comp_cap_month
    and cal.month < c.in_svc_date;


    L_STATUS:=f_long_term_obligation;

    if L_STATUS <> 'OK' then
      return 'Error setting long term obligation and liability on component rows : ' || L_STATUS;
    end if;

    return 'OK';
  exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return l_status;
   end F_LOAD_COMPONENTS;

   --**************************************************************************
   --                            P_CHECK_RECALC_NPV
   -- Recalc NPV after determing IRR for any ILRs that have an IRR
   -- set the process_npv to be 0 where irr is null
   --**************************************************************************
   procedure P_CHECK_RECALC_NPV is

   begin
      -- do not re-calculate NPV if OM
      update LS_ILR_STG set PROCESS_NPV = 0 where IS_OM = 1;

      -- do not recalc NPV if IRR was not found
      update LS_ILR_STG
         set PROCESS_NPV = 0
       where IRR is null
         and PROCESS_NPV = 1;

      -- do not recalc NPV if ANNUAL IRR not between 0 and 100
      update LS_ILR_STG A
         set A.PROCESS_NPV = 0, A.VALIDATION_MESSAGE = 'IRR Calculation not between 0 and 100%'
       where A.PROCESS_NPV = 1
         and (POWER((1 + A.IRR), 12) - 1) not between 0 and 1
         and lower(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY( 'Lease IRR Validation Control',-1)))<>'no';

      update LS_ILR_SCHEDULE_STG S
         set PROCESS_NPV = 0
       where exists (select 1
                from LS_ILR_STG L
               where L.ILR_ID = S.ILR_ID
                 and L.REVISION = S.REVISION
                 and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                 and L.PROCESS_NPV = 0);

      -- set the rate to be the irr for the schedule
      update LS_ILR_SCHEDULE_STG S
         set RATE =
              (select L.IRR
                 from LS_ILR_STG L
                where L.ILR_ID = S.ILR_ID
                  and L.REVISION = S.REVISION
                  and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                  and L.PROCESS_NPV = 1)
       where exists (select 1
                from LS_ILR_STG L
               where L.ILR_ID = S.ILR_ID
                 and L.REVISION = S.REVISION
                 and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                 and L.PROCESS_NPV = 1);
   end P_CHECK_RECALC_NPV;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   --**************************************************************************
   --                            F_ASSET_ALLOC_NPV
   -- This function will allocate the NPV of the schedule down to the assets
   --**************************************************************************
   function F_ASSET_ALLOC_NPV return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      insert into LS_ILR_ASSET_STG
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, CURRENT_LEASE_COST, ALLOC_NPV,
          RESIDUAL_AMOUNT, RESIDUAL_NPV, NPV_MINUS_RESIDUAL_NPV, TERM_PENALTY, BPO_PRICE, ALLOC_DEFERRED_RENT, IS_OM, DEFERRED_RENT_SW, ALLOC_PREPAID_RENT, CONVERSION_BALANCE_MONTH)
         with LS_ASSET_VIEW as
          (select LA.ILR_ID,
                  LA.LS_ASSET_ID,
                  LA.FMV as CURRENT_LEASE_COST,
                  case when la.estimated_residual = 0 and lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Amort to Guaranteed Residual', la.COMPANY_ID))) = 'yes' then
                  NVL(LA.GUARANTEED_RESIDUAL_AMOUNT, 0)
                  ELSE
                    /* CJS 3/7/17 Add system control to account for capitalization of estimated residual */
                    Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', la.COMPANY_ID))), 'no', 0, round(la.ESTIMATED_RESIDUAL * la.FMV, 2))
                  end as RESIDUAL_AMOUNT,
                  LS.SET_OF_BOOKS_ID,
                  LS.REVISION,
                  NVL(RATIO_TO_REPORT(LA.FMV)
                      OVER(partition by LS.ILR_ID, LS.SET_OF_BOOKS_ID, LS.REVISION),
                      1) as PCT_SPREAD
             from LS_ASSET LA, LS_ILR_STG LS, LS_ILR_ASSET_MAP M
            where LA.ILR_ID = LS.ILR_ID
              and M.ILR_ID = LS.ILR_ID
              and M.REVISION = LS.REVISION
              and M.LS_ASSET_ID = LA.LS_ASSET_ID
        and la.ls_asset_status_id <> 4),
         LS_MONTHS_VIEW as
          (select Z.ILR_ID, Z.THE_LENGTH + 1 as THE_LENGTH, Z.RATE, Z.SET_OF_BOOKS_ID, Z.REVISION
             from (select MONTHS_BETWEEN(max(month)
                                         OVER(partition by ILR_ID, SET_OF_BOOKS_ID, REVISION),
                                         month) as THE_LENGTH,
                          ID,
                          month,
                          ILR_ID,
                          RATE,
                          SET_OF_BOOKS_ID,
                          REVISION
                     from LS_ILR_SCHEDULE_STG) Z
            where Z.ID = 1)
         select ROW_NUMBER() OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc, L.LS_ASSET_ID) as ID,
                S.ILR_ID,
                S.REVISION,
                L.LS_ASSET_ID,
                S.SET_OF_BOOKS_ID,
                L.CURRENT_LEASE_COST,
                ROUND(NVL(S.NPV,0) * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       NVL(S.NPV,0) - sum(ROUND(NVL(S.NPV,0) * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0) as ALLOC_PAYMENT_WITH_PLUG,
                L.RESIDUAL_AMOUNT,
                ROUND(L.RESIDUAL_AMOUNT / POWER(1 + M.RATE, M.THE_LENGTH - S.PREPAY_SWITCH), 2),
                ROUND(NVL(S.NPV,0) * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       NVL(S.NPV,0) - sum(ROUND(NVL(S.NPV,0) * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0) -
                ROUND(L.RESIDUAL_AMOUNT / POWER(1 + M.RATE, M.THE_LENGTH - S.PREPAY_SWITCH), 2),
                ROUND(S.TERM_PENALTY * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       S.TERM_PENALTY - sum(ROUND(S.TERM_PENALTY * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0),
                ROUND(S.BPO_PRICE * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       S.BPO_PRICE - sum(ROUND(S.BPO_PRICE * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0),
                ROUND(Nvl(S.DEFERRED_RENT_BALANCE, 0) * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       Nvl(S.DEFERRED_RENT_BALANCE, 0) - sum(ROUND(Nvl(S.DEFERRED_RENT_BALANCE, 0) * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0),
                S.IS_OM,
                S.DEFERRED_RENT_SW,
                ROUND(Nvl(S.PREPAID_RENT_BALANCE, 0) * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       Nvl(S.PREPAID_RENT_BALANCE, 0) - sum(ROUND(Nvl(S.PREPAID_RENT_BALANCE, 0) * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0),
                S.CONVERSION_BALANCE_MONTH
           from LS_ILR_STG S, LS_ASSET_VIEW L, LS_MONTHS_VIEW M
          where S.ILR_ID = L.ILR_ID
            and S.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
            and S.REVISION = L.REVISION
            and S.ILR_ID = M.ILR_ID
            and S.SET_OF_BOOKS_ID = M.SET_OF_BOOKS_ID
            and S.REVISION = M.REVISION;

    L_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ASSET_ALLOC_NPV;

   --**************************************************************************
   --                            F_ALLOCATE_ASSET_PAYMENTS
   -- This function will allocate the payments to the assets
   -- Spreads the payments based on pct current lease obligation
   --**************************************************************************
   function F_ALLOCATE_ASSET_PAYMENTS return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Starting Allocation of payment to Assets';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_SCHEDULE_STG
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, month, PREPAY_SWITCH, RATE,
          PAYMENT_MONTH, MONTHS_TO_ACCRUE, CURRENT_LEASE_COST, AMOUNT, RESIDUAL_AMOUNT,
          BEG_CAPITAL_COST, BEG_OBLIGATION, BEG_LIABILITY, TERM_PENALTY, BPO_PRICE, CONTINGENT_PAID1,
          CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6,
          CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, EXECUTORY_PAID1,
          EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6,
          EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, INCENTIVE_AMOUNT,
          INITIAL_DIRECT_COST, INCENTIVE_MATH_AMOUNT, IDC_MATH_AMOUNT, REMAINING_PRINCIPAL, IMPAIRMENT_ACTIVITY, IS_OM,
          PAYMENT_TERM_TYPE_ID, DEFERRED_RENT_SW, INTEREST_CONVENTION_ID, partial_month_percent,
	      incentive_cap_amount)
         with LS_ASSET_VIEW as
          (select LA.ILR_ID,
                  LA.LS_ASSET_ID,
                  LA.CURRENT_LEASE_COST,
                  LA.RESIDUAL_AMOUNT as RESIDUAL_AMOUNT,
                  LA.ALLOC_NPV,
                  nvl(RATIO_TO_REPORT(LA.NPV_MINUS_RESIDUAL_NPV) OVER(partition by LA.ILR_ID, LA.REVISION, LA.SET_OF_BOOKS_ID), 1/(count(*) over(partition by la.ilr_id, la.revision, la.set_of_books_id))) as pct_spread,
                  LA.TERM_PENALTY,
                  LA.BPO_PRICE,
                  LA.ALLOC_DEFERRED_RENT,
                  LA.ALLOC_PREPAID_RENT,
                  LA.REVISION,
                  LA.SET_OF_BOOKS_ID
             from LS_ILR_ASSET_STG LA)
         select ROW_NUMBER() OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, L.LS_ASSET_ID order by S.MONTH) as ID,
                S.ILR_ID,
                S.REVISION,
                L.LS_ASSET_ID,
                S.SET_OF_BOOKS_ID,
                S.MONTH,
                S.PREPAY_SWITCH,
                S.RATE,
                S.PAYMENT_MONTH,
                S.MONTHS_TO_ACCRUE,
                L.CURRENT_LEASE_COST,
                ROUND(S.AMOUNT * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.AMOUNT - sum(ROUND(S.AMOUNT * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0) as ALLOC_PAYMENT_WITH_PLUG,
                DECODE(S.RESIDUAL_AMOUNT, 0, 0, L.RESIDUAL_AMOUNT) as RESIDUAL,
                -- FERC Depr FASB CAP Types should not add/subtract prepaid/deferred rent from the capital cost
                L.ALLOC_NPV + decode(S.FASB_CAP_TYPE_ID, 3, 0, 6, 0, 1) * (L.ALLOC_PREPAID_RENT - L.ALLOC_DEFERRED_RENT), --BEG_CAPITAL_COST
                L.ALLOC_NPV, --BEG_OBLIGATION
                L.ALLOC_NPV, --BEG_LIABILITY
                DECODE(S.TERM_PENALTY, 0, 0, L.TERM_PENALTY),
                DECODE(S.BPO_PRICE, 0, 0, L.BPO_PRICE),
                ROUND(S.CONTINGENT_PAID1 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID1 - sum(ROUND(S.CONTINGENT_PAID1 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID2 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID2 - sum(ROUND(S.CONTINGENT_PAID2 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID3 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID3 - sum(ROUND(S.CONTINGENT_PAID3 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID4 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID4 - sum(ROUND(S.CONTINGENT_PAID4 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID5 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID5 - sum(ROUND(S.CONTINGENT_PAID5 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID6 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID6 - sum(ROUND(S.CONTINGENT_PAID6 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID7 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID7 - sum(ROUND(S.CONTINGENT_PAID7 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID8 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID8 - sum(ROUND(S.CONTINGENT_PAID8 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID9 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID9 - sum(ROUND(S.CONTINGENT_PAID9 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID10 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID10 - sum(ROUND(S.CONTINGENT_PAID10 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID1 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID1 - sum(ROUND(S.EXECUTORY_PAID1 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID2 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID2 - sum(ROUND(S.EXECUTORY_PAID2 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID3 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID3 - sum(ROUND(S.EXECUTORY_PAID3 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID4 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID4 - sum(ROUND(S.EXECUTORY_PAID4 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID5 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID5 - sum(ROUND(S.EXECUTORY_PAID5 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID6 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID6 - sum(ROUND(S.EXECUTORY_PAID6 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID7 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID7 - sum(ROUND(S.EXECUTORY_PAID7 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID8 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID8 - sum(ROUND(S.EXECUTORY_PAID8 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID9 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID9 - sum(ROUND(S.EXECUTORY_PAID9 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID10 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID10 - sum(ROUND(S.EXECUTORY_PAID10 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
				ROUND(S.INCENTIVE_AMOUNT * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.INCENTIVE_AMOUNT - sum(ROUND(S.INCENTIVE_AMOUNT * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
				ROUND(S.INITIAL_DIRECT_COST * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.INITIAL_DIRECT_COST - sum(ROUND(S.INITIAL_DIRECT_COST * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
				ROUND(S.INCENTIVE_MATH_AMOUNT * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.INCENTIVE_MATH_AMOUNT - sum(ROUND(S.INCENTIVE_MATH_AMOUNT * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
				ROUND(S.IDC_MATH_AMOUNT * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.IDC_MATH_AMOUNT - sum(ROUND(S.IDC_MATH_AMOUNT * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
				ROUND(S.REMAINING_PRINCIPAL * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.REMAINING_PRINCIPAL - sum(ROUND(S.REMAINING_PRINCIPAL * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
				ROUND(S.IMPAIRMENT_ACTIVITY * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.IMPAIRMENT_ACTIVITY - sum(ROUND(S.IMPAIRMENT_ACTIVITY * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                S.IS_OM,
                S.PAYMENT_TERM_TYPE_ID,
                S.DEFERRED_RENT_SW,
                S.INTEREST_CONVENTION_ID,
                s.partial_month_percent,
				ROUND(S.incentive_cap_amount * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.incentive_cap_amount - sum(ROUND(S.incentive_cap_amount * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0)
           from LS_ILR_SCHEDULE_STG S, LS_ASSET_VIEW L
          where S.ILR_ID = L.ILR_ID
            and S.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
            and S.REVISION = L.REVISION;

      -- make sure month with residual is set to 1
      L_STATUS := 'Setting the payment month for residual amounts';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      update LS_ILR_ASSET_SCHEDULE_STG S set S.PAYMENT_MONTH = 1 where RESIDUAL_AMOUNT <> 0;

      -- update the last payment month to be a 2
      update LS_ILR_ASSET_SCHEDULE_STG S
         set S.PAYMENT_MONTH = 2
       where (S.LS_ASSET_ID, S.MONTH) in (select A.LS_ASSET_ID, A.MONTH
                                            from (select B.LS_ASSET_ID,
                                                         B.MONTH,
                                                         ROW_NUMBER() OVER(partition by B.LS_ASSET_ID order by B.MONTH desc) as THE_ROW
                                                    from LS_ILR_ASSET_SCHEDULE_STG B
                                                   where B.PAYMENT_MONTH = 1) A
                                           where THE_ROW = 1);

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ALLOCATE_ASSET_PAYMENTS;

   --**************************************************************************
   --                            F_ALLOCATE_CONT_EXEC
   -- This function allocates the executory paid and contingent paid amounts
   -- To be accrued based on payment frequency
   --**************************************************************************
   function F_ALLOCATE_CONT_EXEC return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
      L_CHECK  number;
      L_SQLS   varchar2(32000);
   begin
      L_STATUS := 'Starting to determine contingent and executory accrual amounts';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

    L_CHECK := 0;
      select count(1)
        into L_CHECK
        from LS_ILR_ASSET_SCHEDULE_STG S2
       where S2.PAYMENT_MONTH in (1, 2)
         and (CONTINGENT_PAID1 <> 0 or CONTINGENT_PAID2 <> 0 or CONTINGENT_PAID3 <> 0 or
             CONTINGENT_PAID4 <> 0 or CONTINGENT_PAID5 <> 0 or CONTINGENT_PAID6 <> 0 or
             CONTINGENT_PAID7 <> 0 or CONTINGENT_PAID8 <> 0 or CONTINGENT_PAID9 <> 0 or
             CONTINGENT_PAID10 <> 0 or EXECUTORY_PAID1 <> 0 or EXECUTORY_PAID2 <> 0 or
             EXECUTORY_PAID3 <> 0 or EXECUTORY_PAID4 <> 0 or EXECUTORY_PAID5 <> 0 or
             EXECUTORY_PAID6 <> 0 or EXECUTORY_PAID7 <> 0 or EXECUTORY_PAID8 <> 0 or
             EXECUTORY_PAID9 <> 0 or EXECUTORY_PAID10 <> 0);

      if L_CHECK > 0 then
FOR I IN (
SELECT COL, replace(col, 'PAID', 'ACCRUAL') ACCRUAL_COL fROM
(SELECT SUM(NVL(CONTINGENT_PAID10, 0)) AMOUNT, 'CONTINGENT_PAID10' COL FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID9, 0)) AMOUNT, 'CONTINGENT_PAID9' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID8, 0)) AMOUNT, 'CONTINGENT_PAID8' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID7, 0)) AMOUNT, 'CONTINGENT_PAID7' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID6, 0)) AMOUNT, 'CONTINGENT_PAID6' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID5, 0)) AMOUNT, 'CONTINGENT_PAID5' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID4, 0)) AMOUNT, 'CONTINGENT_PAID4' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID3, 0)) AMOUNT, 'CONTINGENT_PAID3' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID2, 0)) AMOUNT, 'CONTINGENT_PAID2' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID1, 0)) AMOUNT, 'CONTINGENT_PAID1' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID10, 0)) AMOUNT, 'EXECUTORY_PAID10' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID9, 0)) AMOUNT, 'EXECUTORY_PAID9' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID8, 0)) AMOUNT, 'EXECUTORY_PAID8' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID7, 0)) AMOUNT, 'EXECUTORY_PAID7' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID6, 0)) AMOUNT, 'EXECUTORY_PAID6' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID5, 0)) AMOUNT, 'EXECUTORY_PAID5' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID4, 0)) AMOUNT, 'EXECUTORY_PAID4' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID3, 0)) AMOUNT, 'EXECUTORY_PAID3' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID2, 0)) AMOUNT, 'EXECUTORY_PAID2' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID1, 0)) AMOUNT, 'EXECUTORY_PAID1' FROM LS_ILR_ASSET_SCHEDULE_STG )
WHERE AMOUNT <> 0)
LOOP

L_SQLS:='
        MERGE INTO LS_ILR_ASSET_SCHEDULE_STG A
        USING (
         with ACCRUAL_MONTHS as
         (
           select ILR_ID, LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID,
            MONTH curr_month, add_months(MONTH, decode(PREPAY_SWITCH, 0, -1, 1) * (MONTHS_TO_ACCRUE - 1)) month_range,
            MONTHS_TO_ACCRUE, PREPAY_SWITCH,
            round(' || I.COL || ' / MONTHS_TO_ACCRUE, 2) as INTEREST_AMT,
            ' || I.COL || ' - ( MONTHS_TO_ACCRUE * round(' || I.COL || ' / MONTHS_TO_ACCRUE, 2)) as ROUNDER
           from LS_ILR_ASSET_SCHEDULE_STG
           where ' || I.COL || ' <> 0
           and MONTHS_TO_ACCRUE <> 1
           and MONTH is not null
           and (ILR_ID, REVISION, SET_OF_BOOKS_ID) in (select bb.ILR_ID, bb.REVISION, BB.SET_OF_BOOKS_ID from LS_ILR_STG bb)
         )
         select a.ID, a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID,
            a.MONTH, a.MONTHS_TO_ACCRUE, a.PREPAY_SWITCH,
            am.INTEREST_AMT + case when a.MONTH = am.CURR_MONTH then am.ROUNDER else 0 end ' || I.ACCRUAL_COL || ',
            am.ROUNDER
         from LS_ILR_ASSET_SCHEDULE_STG a, ACCRUAL_MONTHS am
         where a.month between LEAST(am.CURR_MONTH, am.MONTH_RANGE) and GREATEST(am.CURR_MONTH, am.MONTH_RANGE)
         and a.MONTHS_TO_ACCRUE <> 1
         and a.ILR_ID = am.ILR_ID
         and a.LS_ASSET_ID = am.LS_ASSET_ID
         and a.REVISION = am.REVISION
         and a.SET_OF_BOOKS_ID = am.SET_OF_BOOKS_ID
         and a.MONTH is not null
         ) B
         ON (A.ID = B.ID AND A.ILR_ID = B.ILR_ID AND A.LS_ASSET_ID = B.LS_ASSET_ID AND A.REVISION = B.REVISION AND A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID)
         WHEN MATCHED THEN UPDATE SET A.' || I.ACCRUAL_COL || ' = B.' || I.ACCRUAL_COL;

L_STATUS:=L_SQLS;
EXECUTE IMMEDIATE L_SQLS;

L_SQLS:='
      UPDATE LS_ILR_ASSET_SCHEDULE_STG
      SET ' || I.ACCRUAL_COL || ' = ' || I.COL || '
      WHERE MONTHS_TO_ACCRUE = 1
        AND MONTH IS NOT NULL
        AND (ILR_ID, REVISION, SET_OF_BOOKS_ID) IN (SELECT ILR_ID, REVISION, SET_OF_BOOKS_ID FROM LS_ILR_STG)';

L_STATUS:=L_SQLS;
EXECUTE IMMEDIATE L_SQLS;


END LOOP;

    end if;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ALLOCATE_CONT_EXEC;

   --**************************************************************************
   --                            F_ALLOCATE_TO_ASSETS
   -- This function will allocate the payments to the assets
   -- Spreads the payments based on pct current lease cost
   --**************************************************************************
   function F_ALLOCATE_TO_ASSETS return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Starting Allocation to Assets';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

      -- allocate the NPV of the ILR to the ASSETS underneath
      L_MSG := F_ASSET_ALLOC_NPV;

  if L_MSG <> 'OK' then
    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
    return l_msg;
  end if;

         -- Allocate the payments to the assets
         PKG_PP_LOG.P_WRITE_MESSAGE('ALLOCATE Payments');
         L_MSG := F_ALLOCATE_ASSET_PAYMENTS;

  if L_MSG <> 'OK' then
    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
    return l_msg;
  end if;

            -- allocate the executory and contingent paid amounts
            -- to be accrued based on payment frequency
            PKG_PP_LOG.P_WRITE_MESSAGE('ALLOCATE contingents and executory buckets');
    L_MSG := F_ALLOCATE_CONT_EXEC;

  if L_MSG <> 'OK' then
    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
    return l_msg;
         end if;

      return 'OK';

   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ALLOCATE_TO_ASSETS;



   procedure P_SPREAD_PRIN_ACCRUAL
   is
   type MONTHS_ACCRUAL_REC is record
   (
      ID                LS_ILR_ASSET_SCHEDULE_CALC_STG.ID%type,
      ILR_ID            LS_ILR_ASSET_SCHEDULE_CALC_STG.ILR_ID%type,
      LS_ASSET_ID       LS_ILR_ASSET_SCHEDULE_CALC_STG.LS_ASSET_ID%type,
      REVISION          LS_ILR_ASSET_SCHEDULE_CALC_STG.REVISION%type,
      SET_OF_BOOKS_ID   LS_ILR_ASSET_SCHEDULE_CALC_STG.SET_OF_BOOKS_ID%type,
      MONTH             LS_ILR_ASSET_SCHEDULE_CALC_STG.MONTH%type,
      PRINCIPAL_ACCRUAL LS_ILR_ASSET_SCHEDULE_CALC_STG.PRINCIPAL_ACCRUAL%type,
      MONTHS_TO_ACCRUE  LS_ILR_ASSET_SCHEDULE_CALC_STG.MONTHS_TO_ACCRUE%type,
      PREPAY_SWITCH     LS_ILR_ASSET_SCHEDULE_CALC_STG.PREPAY_SWITCH%type,
      PRINCIPAL_AMT     LS_ILR_ASSET_SCHEDULE_CALC_STG.PRINCIPAL_ACCRUAL%type,
      ROUNDER           LS_ILR_ASSET_SCHEDULE_CALC_STG.PRINCIPAL_ACCRUAL%type
   );

   type MONTHS_ACCRUAL_TAB is table of MONTHS_ACCRUAL_REC index by PLS_INTEGER;

   L_ACCRUAL_MONTHS MONTHS_ACCRUAL_TAB;
   v_count number;

   begin
      PKG_PP_LOG.P_WRITE_MESSAGE('Handle Principal Accrual');

    select *
    bulk collect
    into L_ACCRUAL_MONTHS
    from
    (
      -- figure out the payment months and how much accrual goes in each month
      -- Need amounts pulled for monthly leases, since prepaids are inadvertently shifted
      -- principal spread should align with p_handle_om spread
      -- this should spread principal amounts over the period incurred
      with ACCRUAL_MONTHS as
      (
        select a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID, 0 AS Monthly,
          a.MONTH curr_month,
          add_months(a.MONTH, decode(a.PREPAY_SWITCH, 0, -1, 1) * (a.MONTHS_TO_ACCRUE - 1) ) month_range,
          a.PRINCIPAL_ACCRUAL, a.months_to_accrue as months_to_accrue, a.PREPAY_SWITCH,
          case when a.payment_month = 2 and s.partial_month_percent < 1 then
            round(a.principal_paid, 2)
          else
            round((a.principal_paid) / a.months_to_accrue, 2)
          end as PRINCIPAL_AMT,
          round((a.principal_paid), 2) -
          case when a.payment_month = 2 and s.partial_month_percent < 1 then
            round((a.principal_paid), 2)
          else
            ( a.months_to_accrue * round((a.principal_paid) / a.months_to_accrue, 2))
          end as ROUNDER,
          a.PREPAY_SWITCH * greatest(a.payment_month - 1, 0) as is_last_month
        from LS_ILR_ASSET_SCHEDULE_CALC_STG a
        join ls_ilr_schedule_stg s on s.ilr_id = a.ilr_id
                                      and s.revision = a.revision
                                      and s.set_of_books_id = a.set_of_books_id
                                      and s.month = a.month
        where a.PRINCIPAL_PAID <> 0
        and a.MONTHS_TO_ACCRUE <> 1
        and a.MONTH is not null
        and (a.ILR_ID, a.REVISION, a.set_of_books_id) in (select bb.ILR_ID, bb.REVISION, bb.set_of_books_id from LS_ILR_STG bb)

        union all

        select a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID, 1 as Monthly,
          MONTH curr_month, add_months(MONTH, decode(PREPAY_SWITCH, 0, -1, 1) * (MONTHS_TO_ACCRUE - 1) ) month_range, a.PRINCIPAL_ACCRUAL, a.months_to_accrue as months_to_accrue, a.PREPAY_SWITCH,
          round(a.principal_paid, 2) as PRINCIPAL_AMT,
          0 as ROUNDER,
          a.PREPAY_SWITCH * greatest(a.payment_month - 1, 0) as is_last_month
        from LS_ILR_ASSET_SCHEDULE_CALC_STG a
        where a.PRINCIPAL_PAID <> 0
        and a.MONTHS_TO_ACCRUE = 1
        and a.MONTH is not null
        and (a.ILR_ID, a.REVISION, a.set_of_books_id) in (select bb.ILR_ID, bb.REVISION, bb.set_of_books_id from LS_ILR_STG bb)
      )
      -- FOR each month set the accrual amount and figure out the last month for the rounder.
      select
        a.ID, a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID,
        a.MONTH, a.PRINCIPAL_ACCRUAL, a.MONTHS_TO_ACCRUE, a.PREPAY_SWITCH,
        am.PRINCIPAL_AMT +
          case when a.MONTH = am.CURR_MONTH then am.ROUNDER else 0 end as principal_amt,
        am.ROUNDER
      from LS_ILR_ASSET_SCHEDULE_CALC_STG a, ACCRUAL_MONTHS am
      where a.month between LEAST(am.CURR_MONTH, am.MONTH_RANGE) and GREATEST(am.CURR_MONTH, am.MONTH_RANGE)
      and a.MONTHS_TO_ACCRUE <> 1
      and am.MONTHLY = 0
      and a.ILR_ID = am.ILR_ID
      and a.LS_ASSET_ID = am.LS_ASSET_ID
      and a.REVISION = am.REVISION
      and a.SET_OF_BOOKS_ID = am.SET_OF_BOOKS_ID
      and a.MONTH is not null
      and (a.ILR_ID, a.REVISION, a.set_of_books_id) in (select bb.ILR_ID, bb.REVISION, bb.set_of_books_id from LS_ILR_STG bb)

      union all

      select
        a.ID, a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID,
        a.MONTH, a.PRINCIPAL_ACCRUAL, a.MONTHS_TO_ACCRUE, a.PREPAY_SWITCH,
        am.PRINCIPAL_AMT,
        am.ROUNDER
      from LS_ILR_ASSET_SCHEDULE_CALC_STG a, ACCRUAL_MONTHS am
      where a.month = am.curr_month
      and a.MONTHS_TO_ACCRUE = 1
      and am.MONTHLY = 1
      and a.ILR_ID = am.ILR_ID
      and a.LS_ASSET_ID = am.LS_ASSET_ID
      and a.REVISION = am.REVISION
      and a.SET_OF_BOOKS_ID = am.SET_OF_BOOKS_ID
      and a.MONTH is not null
      and (a.ILR_ID, a.REVISION, a.set_of_books_id) in (select bb.ILR_ID, bb.REVISION, bb.set_of_books_id from LS_ILR_STG bb)
    )
      ;

      PKG_PP_LOG.P_WRITE_MESSAGE('Principal Accrual to Spread: '|| to_char(L_ACCRUAL_MONTHS.COUNT));

      forall ndx in indices of L_ACCRUAL_MONTHS
         update LS_ILR_ASSET_SCHEDULE_CALC_STG s
         set s.PRINCIPAL_ACCRUAL = L_ACCRUAL_MONTHS(ndx).PRINCIPAL_AMT
         where s.ID = L_ACCRUAL_MONTHS(ndx).ID
         and s.ILR_ID = L_ACCRUAL_MONTHS(ndx).ILR_ID
         and s.LS_ASSET_ID = L_ACCRUAL_MONTHS(ndx).LS_ASSET_ID
         and s.REVISION = L_ACCRUAL_MONTHS(ndx).REVISION
         and s.SET_OF_BOOKS_ID = L_ACCRUAL_MONTHS(ndx).SET_OF_BOOKS_ID
         ;

      -- Update the principal accrual to 0 if this is prepay and has a 0 payment in the first month
      update ls_ilr_asset_schedule_calc_stg
         set principal_accrual = 0
       where id = 1
         and amount = 0
         and principal_paid = 0
         and payment_month = 1
         and prepay_switch = 1;

   end P_SPREAD_PRIN_ACCRUAL;

   procedure P_HANDLE_OM
   is
   type MONTHS_ACCRUAL_REC is record
   (
      ID                LS_ILR_ASSET_SCHEDULE_CALC_STG.ID%type,
      ILR_ID            LS_ILR_ASSET_SCHEDULE_CALC_STG.ILR_ID%type,
      LS_ASSET_ID       LS_ILR_ASSET_SCHEDULE_CALC_STG.LS_ASSET_ID%type,
      REVISION          LS_ILR_ASSET_SCHEDULE_CALC_STG.REVISION%type,
      SET_OF_BOOKS_ID   LS_ILR_ASSET_SCHEDULE_CALC_STG.SET_OF_BOOKS_ID%type,
      MONTH             LS_ILR_ASSET_SCHEDULE_CALC_STG.MONTH%type,
      INTEREST_ACCRUAL  LS_ILR_ASSET_SCHEDULE_CALC_STG.INTEREST_ACCRUAL%type,
      MONTHS_TO_ACCRUE  LS_ILR_ASSET_SCHEDULE_CALC_STG.MONTHS_TO_ACCRUE%type,
      PREPAY_SWITCH     LS_ILR_ASSET_SCHEDULE_CALC_STG.PREPAY_SWITCH%type,
      INTEREST_AMT      LS_ILR_ASSET_SCHEDULE_CALC_STG.INTEREST_ACCRUAL%type,
      ROUNDER           LS_ILR_ASSET_SCHEDULE_CALC_STG.INTEREST_ACCRUAL%type
   );

   type MONTHS_ACCRUAL_TAB is table of MONTHS_ACCRUAL_REC index by PLS_INTEGER;

   L_ACCRUAL_MONTHS_SL MONTHS_ACCRUAL_TAB;
   L_ACCRUAL_MONTHS_INC MONTHS_ACCRUAL_TAB;

   v_count number;
   begin
   select count(1)
   into v_count
   from ls_ilr_stg
   where is_om = 1;

   if v_count = 0 then
    return;
   end if;
      PKG_PP_LOG.P_WRITE_MESSAGE('Handle Operating Leases');

  /* CJS 2/17/15 Adding portion to allow to do system control by company*/
  /* DJC 06/27/2018 Removed system control by company, now controlled by FASB_CAP_TYPE_ID */
      -- for OM records
      -- everything is interest expense not principal (move it).  Also no long term and short term obligations
      update LS_ILR_ASSET_SCHEDULE_CALC_STG
         set BEG_CAPITAL_COST = 0, END_CAPITAL_COST = 0,
             INTEREST_ACCRUAL = INTEREST_ACCRUAL + PRINCIPAL_ACCRUAL,
             INTEREST_PAID = INTEREST_PAID + PRINCIPAL_PAID, PRINCIPAL_ACCRUAL = 0,
             PRINCIPAL_PAID = 0, BEG_LT_OBLIGATION = 0, END_LT_OBLIGATION = 0,
             BEG_LT_LIABILITY = 0, END_LT_LIABILITY = 0, BEG_LIABILITY = 0, END_LIABILITY = 0, LIABILITY_REMEASUREMENT = 0,
			 obligation_reclass = 0, ST_OBLIGATION_REMEASUREMENT = 0, LT_OBLIGATION_REMEASUREMENT = 0,
			 PARTIAL_TERM_GAIN_LOSS = 0, UNACCRUED_INTEREST_RECLASS = 0
       where IS_OM = 1
         AND MONTHS_TO_ACCRUE <> 1;

       -- Handle the update for monthly differently to shift the interest_accrual back by the negative payment shift
       update LS_ILR_ASSET_SCHEDULE_CALC_STG A
          set INTEREST_ACCRUAL = (
              SELECT B.INTEREST_ACCRUAL + B.PRINCIPAL_ACCRUAL
                FROM LS_ILR_ASSET_SCHEDULE_CALC_STG B, LS_ILR_OPTIONS O
               WHERE B.ILR_ID = A.ILR_ID
                 AND B.REVISION = A.REVISION
                 AND B.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
         AND B.LS_ASSET_ID = A.LS_ASSET_ID
                 AND B.MONTH = ADD_MONTHS(NVL(A.MONTH,TO_DATE(299912,'YYYYMM')), NVL(O.SCHEDULE_PAYMENT_SHIFT,0))
                 AND B.ILR_ID = O.ILR_ID
                 AND B.REVISION = O.REVISION
                 AND B.ID > 0)
        where IS_OM = 1
         AND MONTHS_TO_ACCRUE = 1
         AND ID > 0
         AND EXISTS (
              SELECT 1
                FROM LS_ILR_ASSET_SCHEDULE_CALC_STG B, LS_ILR_OPTIONS O
               WHERE B.ILR_ID = A.ILR_ID
                 AND B.REVISION = A.REVISION
                 AND B.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
         AND B.LS_ASSET_ID = A.LS_ASSET_ID
                 AND B.MONTH = ADD_MONTHS(NVL(A.MONTH,TO_DATE(299912,'YYYYMM')), NVL(O.SCHEDULE_PAYMENT_SHIFT,0))
                 AND B.ILR_ID = O.ILR_ID
                 AND B.REVISION = O.REVISION
                 AND B.ID > 0);

       update LS_ILR_ASSET_SCHEDULE_CALC_STG A
         set (BEG_CAPITAL_COST, END_CAPITAL_COST, PRINCIPAL_ACCRUAL, PRINCIPAL_PAID,
              BEG_LT_OBLIGATION, END_LT_OBLIGATION, BEG_LT_LIABILITY, END_LT_LIABILITY,
              BEG_LIABILITY, END_LIABILITY, LIABILITY_REMEASUREMENT,
              INTEREST_PAID, obligation_reclass,ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT,
			 PARTIAL_TERM_GAIN_LOSS, UNACCRUED_INTEREST_RECLASS) = (
                 SELECT 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        B.INTEREST_PAID + B.PRINCIPAL_PAID, 0,0,0,0,0
                   FROM LS_ILR_ASSET_SCHEDULE_CALC_STG B, LS_ILR_OPTIONS O
                  WHERE B.ID = A.ID
                    AND B.ILR_ID = A.ILR_ID
                    AND B.REVISION = A.REVISION
                    AND B.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
          AND B.LS_ASSET_ID = A.LS_ASSET_ID
                    AND NVL(TO_NUMBER(TO_CHAR(B.MONTH,'YYYYMM')), B.ID) = NVL(TO_NUMBER(TO_CHAR(A.MONTH,'YYYYMM')), A.ID)
                    AND B.ILR_ID = O.ILR_ID
                    AND B.REVISION = O.REVISION)
       where IS_OM = 1
         AND MONTHS_TO_ACCRUE = 1
         AND EXISTS (
             SELECT 1
                   FROM LS_ILR_ASSET_SCHEDULE_CALC_STG B, LS_ILR_OPTIONS O
                  WHERE B.ID = A.ID
                    AND B.ILR_ID = A.ILR_ID
                    AND B.REVISION = A.REVISION
                    AND B.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
          AND B.LS_ASSET_ID = A.LS_ASSET_ID
                    AND NVL(TO_NUMBER(TO_CHAR(B.MONTH,'YYYYMM')), B.ID) = NVL(TO_NUMBER(TO_CHAR(A.MONTH,'YYYYMM')), A.ID)
                    AND B.ILR_ID = O.ILR_ID
                    AND B.REVISION = O.REVISION);

      -- spread the interest accrual to be correct
      PKG_PP_LOG.P_WRITE_MESSAGE('Spread Operating Lease Rental Accrual Amounts');

      select *
      bulk collect
      into L_ACCRUAL_MONTHS_SL
      from
    (
      with ACCRUAL_MONTHS as
      (
        select stg.ILR_ID, stg.LS_ASSET_ID, stg.REVISION, stg.SET_OF_BOOKS_ID, stg.PREPAY_SWITCH,
          max(MONTH) as month,
          sum(CASE WHEN PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL('Lease Non-Monthly Payment Spread') = 'no' THEN INTEREST_PAID ELSE PAID_SPREAD END
              - Nvl(remeasurement.prior_month_end_deferred_rent, 0) + Nvl(remeasurement.prior_month_end_prepaid_rent, 0)
              - CASE WHEN remeasurement.remeasurement_date IS NULL THEN
                  Decode(stg.MONTH, Nvl(lias.conversion_balance_month, npv_start_date), lias.alloc_deferred_rent, 0) + Decode(stg.MONTH, Nvl(lias.conversion_balance_month, npv_start_date), lias.alloc_prepaid_rent, 0)
                  ELSE
                  Decode(stg.MONTH, lias.conversion_balance_month, lias.alloc_deferred_rent, 0) + Decode(stg.MONTH, lias.conversion_balance_month, lias.alloc_prepaid_rent, 0)
                  end
              ) as interest_accrual,
          CASE WHEN lias.conversion_balance_MONTH IS NOT NULL
          THEN P.total_months + Nvl(MP2.mb,0)
          ELSE P.total_months + nvl(MP.mb,0) END - case when trunc(t.total_pmp) = t.total_pmp then 0 else (1 - lmp.last_pmp) end as MONTHS_TO_ACCRUE,
          round( sum(CASE WHEN PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL('Lease Non-Monthly Payment Spread') = 'no' THEN INTEREST_PAID ELSE PAID_SPREAD END
                  - Nvl(remeasurement.prior_month_end_deferred_rent, 0) + Nvl(remeasurement.prior_month_end_prepaid_rent, 0)
                  - CASE WHEN remeasurement.remeasurement_date IS NULL THEN
                 Decode(stg.MONTH, Nvl(lias.conversion_balance_month, npv_start_date), lias.alloc_deferred_rent, 0) - Decode(stg.MONTH, Nvl(lias.conversion_balance_month, npv_start_date), lias.alloc_prepaid_rent, 0)
                  ELSE
                  Decode(stg.MONTH, lias.conversion_balance_month, lias.alloc_deferred_rent, 0) - Decode(stg.MONTH, lias.conversion_balance_month, lias.alloc_prepaid_rent, 0)
                  end
                  ) / (CASE WHEN lias.conversion_balance_MONTH IS NOT NULL
                       THEN P.total_months + Nvl(MP2.mb,0)
                       ELSE P.total_months + nvl(MP.mb,0) END - case when trunc(t.total_pmp) = t.total_pmp then 0 else (1 - lmp.last_pmp) end), 2 ) as INTEREST_AMT,
          sum(CASE WHEN PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL('Lease Non-Monthly Payment Spread') = 'no' THEN INTEREST_PAID ELSE PAID_SPREAD END
              - Nvl(remeasurement.prior_month_end_deferred_rent, 0) + Nvl(remeasurement.prior_month_end_prepaid_rent, 0)
              - CASE WHEN remeasurement.remeasurement_date IS NULL THEN
                  Decode(stg.MONTH, Nvl(lias.conversion_balance_month, npv_start_date), lias.alloc_deferred_rent, 0) - Decode(stg.MONTH, Nvl(lias.conversion_balance_month, npv_start_date), lias.alloc_prepaid_rent, 0)
                  ELSE
                  Decode(stg.MONTH, lias.conversion_balance_month, lias.alloc_deferred_rent, 0) - Decode(stg.MONTH, lias.conversion_balance_month, lias.alloc_prepaid_rent, 0)
                  end
              ) - ( (CASE WHEN lias.conversion_balance_MONTH IS NOT NULL
                     THEN P.total_months + Nvl(MP2.mb,0)
                     ELSE P.total_months + nvl(MP.mb,0) END - case when trunc(t.total_pmp) = t.total_pmp then 0 else (1 - lmp.last_pmp) end) 
                       *  round( sum(CASE WHEN PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL('Lease Non-Monthly Payment Spread') = 'no' THEN INTEREST_PAID ELSE PAID_SPREAD END
                                        - Nvl(remeasurement.prior_month_end_deferred_rent, 0) + Nvl(remeasurement.prior_month_end_prepaid_rent, 0)
                                        - CASE WHEN remeasurement.remeasurement_date IS NULL THEN
                      Decode(stg.MONTH, Nvl(lias.conversion_balance_month, npv_start_date), lias.alloc_deferred_rent, 0) - Decode(stg.MONTH, Nvl(lias.conversion_balance_month, npv_start_date), lias.alloc_prepaid_rent, 0)
                      ELSE
                      Decode(stg.MONTH, lias.conversion_balance_month, lias.alloc_deferred_rent, 0) - Decode(stg.MONTH, lias.conversion_balance_month, lias.alloc_prepaid_rent, 0)
                      end
                                        ) / (CASE WHEN lias.conversion_balance_MONTH IS NOT NULL
                                             THEN P.total_months + Nvl(MP2.mb,0)
                                             ELSE P.total_months + nvl(MP.mb,0) END - case when trunc(t.total_pmp) = t.total_pmp then 0 else (1 - lmp.last_pmp) end), 2 ) ) as ROUNDER,
          lias.conversion_balance_month, npv_start_date
        from LS_ILR_ASSET_SCHEDULE_CALC_STG stg
        join ls_ilr_options ilro ON ilro.ilr_id = stg.ilr_id and ilro.revision = stg.revision
        join ls_fasb_cap_type_sob_map fasb ON fasb.set_of_books_id = stg.set_of_books_id
        JOIN ls_ilr_stg ilrs ON ilrs.ilr_id = stg.ilr_id AND ilrs.revision = stg.revision AND ilrs.set_of_books_id = stg.SET_of_books_id
              AND fasb.lease_cap_type_id = ilro.lease_cap_type_id
        JOIN (select ilr_id, revision, set_of_books_id, sum(number_of_terms * decode(payment_freq_id, 1, 12, 2, 6, 3, 3, 1)) total_months
                  from ls_ilr_payment_term_stg
                 group by ilr_id, revision, set_of_books_id) P on stg.ILR_ID = P.ILR_ID and stg.REVISION = P.REVISION and stg.SET_OF_BOOKS_ID = P.SET_OF_BOOKS_ID
        JOIN (select p.ilr_id, p.revision, p.set_of_books_id, min(p.payment_term_date) min_pay_term_date,
                     months_between(trunc(min(p.payment_term_date),'fmmonth'), trunc(i.npv_start_date,'fmmonth'))
                       + case p.payment_term_type_id when 4 then 1 else 0 end mb
                from ls_ilr_payment_term_stg p
                join ls_ilr_stg i on i.ilr_id = p.ilr_id 
                                 and i.revision = p.revision 
                                 and i.set_of_books_id = p.set_of_books_id
               group by p.ilr_id, p.revision, p.set_of_books_id, i.npv_start_date,
                        case p.payment_term_type_id when 4 then 1 else 0 end) MP on stg.ILR_ID = MP.ILR_ID and stg.REVISION = MP.REVISION and stg.SET_OF_BOOKS_ID = MP.SET_OF_BOOKS_ID
        JOIN (select a.ls_asset_id, p.ilr_id, p.revision, p.set_of_books_id, min_pay_term_date, 
                     months_between(min_pay_term_date, a.conversion_balance_month) 
                       + case p.payment_term_type_id when 4 then 1 else 0 end mb
                from (select p.ilr_id, p.revision, p.set_of_books_id, min(p.payment_term_date) min_pay_term_date,
                             p.payment_term_type_id 
                        from ls_ilr_payment_term_stg p 
                       group by p.ilr_id, p.revision, p.set_of_books_id, p.payment_term_type_id) p
                join ls_ilr_asset_stg a on a.ilr_id = p.ilr_id 
                                       and a.revision = p.revision 
                                       and a.set_of_books_id = p.set_of_books_id) MP2 on stg.ILR_ID = MP2.ILR_ID 
                                                                                     and stg.REVISION = MP2.REVISION 
                                                                                     and stg.SET_OF_BOOKS_ID = MP2.SET_OF_BOOKS_ID 
                                                                                     and stg.LS_ASSET_ID = MP2.LS_ASSET_ID
        join (select ilr_id, revision, set_of_books_id,
                     partial_month_percent last_pmp
                from LS_ILR_ASSET_SCHEDULE_CALC_STG
               where month = (select max(month) from LS_ILR_ASSET_SCHEDULE_CALC_STG)) lmp on lmp.ilr_id = stg.ilr_id 
                                                                                         and lmp.revision = stg.revision 
                                                                                         and lmp.set_of_books_id = stg.set_of_books_id
        join (select ilr_id, revision, set_of_books_id, sum(partial_month_percent) total_pmp
                from LS_ILR_ASSET_SCHEDULE_CALC_STG
               group by ilr_id, revision, set_of_books_id) t on t.ilr_id = stg.ilr_id 
                                                            and t.revision = stg.revision 
                                                            and t.set_of_books_id = stg.set_of_books_id
        left OUTER JOIN LS_ILR_ASSET_STG lias ON stg.ILR_ID = lias.ILR_ID
                AND stg.REVISION = lias.REVISION
                AND stg.LS_ASSET_ID = lias.LS_ASSET_ID
                AND stg.SET_OF_BOOKS_ID = lias.SET_OF_BOOKS_ID
        left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON stg.ilr_id = remeasurement.ilr_id
          AND stg.revision = remeasurement.revision
          AND stg.ls_asset_id = remeasurement.ls_asset_id
          AND stg.set_of_books_id = remeasurement.set_of_books_id
          AND stg.month = Trunc(remeasurement.remeasurement_date,'month')
        where stg.IS_OM = 1
          and fasb.fasb_cap_type_id = 4 and nvl(stg.deferred_rent_sw,0) != 0
        AND stg.MONTH >= Nvl(lias.conversion_balance_month, To_Date('180001', 'yyyymm'))
        and MONTH is not null
        group by stg.ILR_ID, stg.LS_ASSET_ID, stg.REVISION, stg.SET_OF_BOOKS_ID, stg.PREPAY_SWITCH,
                 CASE WHEN lias.conversion_balance_MONTH IS NOT NULL
                                                                THEN P.total_months + Nvl(MP2.mb,0)
                                                                ELSE P.total_months + nvl(MP.mb,0) END - case when trunc(t.total_pmp) = t.total_pmp then 0 else (1 - lmp.last_pmp) end,
                 lias.conversion_balance_month, (stg.PREPAY_SWITCH - 1) * nvl(ilro.schedule_payment_shift,0), npv_start_date
      )
      select a.ID, a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID,
        a.MONTH, a.INTEREST_ACCRUAL, a.MONTHS_TO_ACCRUE, a.PREPAY_SWITCH,
        CASE
          WHEN a.MONTH < am.conversion_balance_month THEN a.interest_paid
        ELSE
          case when conversion_balance_month is null and npv_start_date is null THEN
            Round(am.INTEREST_AMT * a.partial_month_percent,2) + case when a.ID = am.MONTHS_TO_ACCRUE+Decode(a.payment_term_type_id,4,1,0) then am.ROUNDER
            + (am.INTEREST_AMT - Round((am.INTEREST_AMT * a.partial_month_percent),2) - Round((am.INTEREST_AMT * (1 - a.partial_month_percent)),2)) else 0 END
          when a.month <= add_months(nvl(am.conversion_balance_month,npv_start_date),am.months_to_accrue+Decode(a.payment_term_type_id,4,1,0)-1) then
            Round(am.INTEREST_AMT * a.partial_month_percent,2) + case when a.month = add_months(nvl(am.conversion_balance_month,npv_start_date),am.months_to_accrue+ Decode(a.payment_term_type_id,4,1,0)-1)
            then am.ROUNDER +
            (am.INTEREST_AMT - Round((am.INTEREST_AMT * a.partial_month_percent),2) - Round((am.INTEREST_AMT * (1 - a.partial_month_percent)),2)) else 0 END
          else
            0
          end
        END AS INTEREST_AMT,
        am.ROUNDER +
        CASE
          WHEN a.MONTH < am.conversion_balance_month THEN 0
        ELSE
          case when conversion_balance_month is null and npv_start_date is null THEN
            0 + case when a.ID = am.MONTHS_TO_ACCRUE+Decode(a.payment_term_type_id,4,1,0) then 
            (am.INTEREST_AMT - Round((am.INTEREST_AMT * a.partial_month_percent),2) - Round((am.INTEREST_AMT * (1 - a.partial_month_percent)),2)) else 0 END
          when a.month <= add_months(nvl(am.conversion_balance_month,npv_start_date),am.months_to_accrue+Decode(a.payment_term_type_id,4,1,0)-1) then
            0 + case when a.month = add_months(nvl(am.conversion_balance_month,npv_start_date),am.months_to_accrue+ Decode(a.payment_term_type_id,4,1,0)-1)
            then 
            (am.INTEREST_AMT - Round((am.INTEREST_AMT * a.partial_month_percent),2) - Round((am.INTEREST_AMT * (1 - a.partial_month_percent)),2)) else 0 END
          else
            0
          end
        END
         AS ROUNDER
      from LS_ILR_ASSET_SCHEDULE_CALC_STG a, ACCRUAL_MONTHS am
      where a.ILR_ID = am.ILR_ID
      and a.LS_ASSET_ID = am.LS_ASSET_ID
      and a.REVISION = am.REVISION
      and a.SET_OF_BOOKS_ID = am.SET_OF_BOOKS_ID
      and a.IS_OM = 1
      and a.MONTH is not null
      and a.month <= am.month
    );

    PKG_PP_LOG.P_WRITE_MESSAGE('Interest Accrual to Respread (Straight-Line): '|| to_char(L_ACCRUAL_MONTHS_SL.COUNT));
    forall ndx in indices of L_ACCRUAL_MONTHS_SL
       update LS_ILR_ASSET_SCHEDULE_CALC_STG s
       set s.INTEREST_ACCRUAL = L_ACCRUAL_MONTHS_SL(ndx).INTEREST_AMT
       where s.ID = L_ACCRUAL_MONTHS_SL(ndx).ID
       and s.ILR_ID = L_ACCRUAL_MONTHS_SL(ndx).ILR_ID
       and s.LS_ASSET_ID = L_ACCRUAL_MONTHS_SL(ndx).LS_ASSET_ID
       and s.REVISION = L_ACCRUAL_MONTHS_SL(ndx).REVISION
       and s.SET_OF_BOOKS_ID = L_ACCRUAL_MONTHS_SL(ndx).SET_OF_BOOKS_ID
       ;

    select *
      bulk collect
      into L_ACCRUAL_MONTHS_INC
      from
      (
         with ACCRUAL_MONTHS as
         (
           select stg.ILR_ID, stg.LS_ASSET_ID, stg.REVISION, stg.SET_OF_BOOKS_ID,
            add_months(stg.MONTH, -1 * nvl(ilro.schedule_payment_shift,0)) curr_month,
            add_months(add_months(stg.MONTH, -1 * nvl(ilro.schedule_payment_shift,0)),
                       decode(stg.PREPAY_SWITCH, 0, -1, 1) * (stg.MONTHS_TO_ACCRUE - 1) ) month_range,
            stg.INTEREST_ACCRUAL, stg.MONTHS_TO_ACCRUE, stg.PREPAY_SWITCH,
            round(stg.INTEREST_PAID / stg.MONTHS_TO_ACCRUE, 2) as INTEREST_AMT,
            stg.INTEREST_PAID - ( stg.MONTHS_TO_ACCRUE * round(stg.INTEREST_PAID / stg.MONTHS_TO_ACCRUE, 2)) as ROUNDER
           from LS_ILR_ASSET_SCHEDULE_CALC_STG stg
            join ls_ilr_options ilro ON ilro.ilr_id = stg.ilr_id and ilro.revision = stg.revision
            join ls_fasb_cap_type_sob_map fasb ON fasb.set_of_books_id = stg.set_of_books_id
                  AND fasb.lease_cap_type_id = ilro.lease_cap_type_id
           where stg.IS_OM = 1
             and stg.MONTH is not null
             and ( fasb.fasb_cap_type_id = 5 or nvl(stg.deferred_rent_sw,0) = 0 )
          and stg.payment_month <> 0
         )
         select a.ID, a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID,
            a.MONTH, a.INTEREST_ACCRUAL, a.MONTHS_TO_ACCRUE, a.PREPAY_SWITCH,
            am.INTEREST_AMT + case when a.MONTH = am.CURR_MONTH then am.ROUNDER else 0 end,
            am.ROUNDER
         from LS_ILR_ASSET_SCHEDULE_CALC_STG a, ACCRUAL_MONTHS am
         where a.month between LEAST(am.CURR_MONTH, am.MONTH_RANGE) and GREATEST(am.CURR_MONTH, am.MONTH_RANGE)
         and a.ILR_ID = am.ILR_ID
         and a.LS_ASSET_ID = am.LS_ASSET_ID
         and a.REVISION = am.REVISION
         and a.SET_OF_BOOKS_ID = am.SET_OF_BOOKS_ID
         and a.IS_OM = 1
         and a.MONTH is not null
      )
      ;

      PKG_PP_LOG.P_WRITE_MESSAGE('Interest Accrual to Respread (As Incurred): '|| to_char(L_ACCRUAL_MONTHS_INC.COUNT));
      forall ndx in indices of L_ACCRUAL_MONTHS_INC
         update LS_ILR_ASSET_SCHEDULE_CALC_STG s
         set s.INTEREST_ACCRUAL = L_ACCRUAL_MONTHS_INC(ndx).INTEREST_AMT
         where s.ID = L_ACCRUAL_MONTHS_INC(ndx).ID
         and s.ILR_ID = L_ACCRUAL_MONTHS_INC(ndx).ILR_ID
         and s.LS_ASSET_ID = L_ACCRUAL_MONTHS_INC(ndx).LS_ASSET_ID
         and s.REVISION = L_ACCRUAL_MONTHS_INC(ndx).REVISION
         and s.SET_OF_BOOKS_ID = L_ACCRUAL_MONTHS_INC(ndx).SET_OF_BOOKS_ID
         ;

      -- zero out the interest_accrual for the last month of a negative shifted arrears ILR
       update LS_ILR_ASSET_SCHEDULE_CALC_STG s
         set INTEREST_ACCRUAL = 0
       where IS_OM = 1
         and PREPAY_SWITCH = 0
         and exists (
             select 1 from (select ilr_id, revision, max(MONTH) max_month from LS_ILR_ASSET_SCHEDULE_CALC_STG
                             group by ilr_id, revision) x, ls_ilr_options o
              where s.month = x.max_month
                and s.ilr_id = x.ilr_id
                and s.revision = x.revision
                and x.ilr_id = o.ilr_id
                and x.revision = o.revision
                and nvl(o.schedule_payment_shift,0) > 0);

   end P_HANDLE_OM;

   procedure P_HANDLE_DEFERRED_RENT is
      L_MSG varchar2(2000);
   begin
   --Moving this to separate procedure, since not just isolated to operating leases, won't always run in P_HANDLE_OM
   L_MSG:='Calculating deferred rent/rolling deferred rent forward';
   PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

   merge into LS_ILR_ASSET_SCHEDULE_CALC_STG A
  using (
           with LP as (
               select L2.ILR_ID,
                       L2.PAYMENT_TERM_ID,
                       L2.REVISION,
                       L2.SET_OF_BOOKS_ID,
                       ROW_NUMBER() OVER(partition by L2.ILR_ID, L2.SET_OF_BOOKS_ID, L2.REVISION order by L2.PAYMENT_TERM_DATE desc) as THE_MAX,
                       L2.PAYMENT_FREQ_ID,
                       L2.PAYMENT_TERM_DATE
                 from LS_ILR_PAYMENT_TERM_STG L2)
            select ID,
            ILR_ID,
            REVISION,
            LS_ASSET_ID,
            SET_OF_BOOKS_ID,
            month,
            INT_ACCRUAL,
            INT_PAID,
            IS_OM,
            BEG_DEFERRED_RENT,
            DEFERRED_RENT,
            END_DEFERRED_RENT,
            BEG_ST_DEFERRED_RENT,
            END_ST_DEFERRED_RENT,
            PREPAY_AMORTIZATION,
            BEG_PREPAID_RENT,
            PREPAID_RENT,
            END_PREPAID_RENT,
            Nvl(DEFERRED_RENT_SW, 1) AS DEFERRED_RENT_SW,
            BEG_ARREARS_ACCRUAL,
            ARREARS_ACCRUAL,
            END_ARREARS_ACCRUAL
        from LS_ILR_ASSET_SCHEDULE_CALC_STG A
          JOIN LS_ILR_OPTIONS O ON A.ILR_ID = O.ILR_ID AND A.REVISION = O.REVISION
          JOIN LS_ILR_ASSET_STG B ON A.ILR_ID = B.ILR_ID AND A.REVISION = B.REVISION AND A.LS_ASSET_ID = B.LS_ASSET_ID AND A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID
          JOIN LP ON A.ILR_ID = LP.ILR_ID
               AND A.REVISION = LP.REVISION
               AND A.SET_OF_BOOKS_ID = LP.SET_OF_BOOKS_ID
          JOIN (SELECT DISTINCT ilr_id, revision, set_of_books_id,
                       First_Value(MONTH) OVER(PARTITION BY ilr_id, revision, set_of_books_id) first_pay_month
                  FROM LS_ILR_SCHEDULE_STG) M ON A.ILR_ID = M.ILR_ID
               AND A.REVISION = M.REVISION
               AND A.SET_OF_BOOKS_ID = M.SET_OF_BOOKS_ID
          left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON A.ilr_id = remeasurement.ilr_id
              AND A.revision = remeasurement.revision
              AND A.ls_asset_id = remeasurement.ls_asset_id
              AND A.set_of_books_id = remeasurement.set_of_books_id
              AND A.month = Trunc(remeasurement.remeasurement_date,'month')
        where LP.THE_MAX = 1
        MODEL PARTITION BY (A.ILR_ID,
                            A.LS_ASSET_ID,
                            A.REVISION,
                            A.SET_OF_BOOKS_ID)
        DIMENSION BY (ROW_NUMBER() OVER(partition by A.ILR_ID, A.LS_ASSET_ID, A.REVISION, A.SET_OF_BOOKS_ID order by A.month) as ID)
        MEASURES(INTEREST_ACCRUAL as INT_ACCRUAL,
                 INTEREST_PAID as INT_PAID,
                 A.month,
                 MONTHS_BETWEEN(max(A.month) OVER(partition by A.ILR_ID, A.LS_ASSET_ID, A.REVISION, A.SET_OF_BOOKS_ID), A.month) as MONTH_ID,
                 A.IS_OM,
                 BEG_DEFERRED_RENT,
                 DEFERRED_RENT,
                 A.END_DEFERRED_RENT,
                 BEG_ST_DEFERRED_RENT,
                 0 as ST_DEFERRED_RENT,
                 END_ST_DEFERRED_RENT,
                 PAYMENT_MONTH,
                 a.PREPAY_SWITCH,
                 PAID_SPREAD,
                 REMEASUREMENT.PRIOR_MONTH_END_DEFERRED_RENT AS REMEASURED_DEFERRED_RENT,
                 REMEASUREMENT.IS_REMEASUREMENT,
                 Nvl(B.DEFERRED_RENT_SW, 1) AS DEFERRED_RENT_SW,
                 PREPAY_AMORTIZATION,
                 BEG_PREPAID_RENT,
                 PREPAID_RENT,
                 END_PREPAID_RENT,
                 REMEASUREMENT.PRIOR_MONTH_END_PREPAID_RENT AS REMEASURED_PREPAID_RENT,
                 0 AS NET_DEFERRED_PREPAID,
                 0 AS CALC_END_PREPAID_RENT,
                 0 AS CALC_BEG_PREPAID_RENT,
                 (SELECT company_id FROM ls_ilr WHERE ilr_id = A.ILR_ID) AS COMPANY_ID,
                 nvl(B.CONVERSION_BALANCE_MONTH,to_date(1900,'yyyy')) as CONVERSION_BALANCE_MONTH,
                 B.ALLOC_DEFERRED_RENT,
                 B.ALLOC_PREPAID_RENT,
                 0 AS pay_group,
                 0 AS conv_pay_term,
                 BEG_ARREARS_ACCRUAL,
                 ARREARS_ACCRUAL,
                 END_ARREARS_ACCRUAL,
                 NVL(O.SCHEDULE_PAYMENT_SHIFT,0) as NEG_PAY_SHIFT,
                 MOD(DENSE_RANK()
                     OVER(PARTITION BY A.ILR_ID, A.REVISION, A.SET_OF_BOOKS_ID ORDER BY A.MONTH) - 1
                     + case when A.PREPAY_SWITCH = 0 AND A.MONTH <= ADD_MONTHS(M.FIRST_PAY_MONTH, DECODE(LP.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) THEN
                         MONTHS_BETWEEN(M.FIRST_PAY_MONTH, LP.PAYMENT_TERM_DATE)
                       else
                         0
                       end,
                     A.MONTHS_TO_ACCRUE) as TERM_COUNT
                 )
      IGNORE NAV RULES UPSERT ITERATE(100000) UNTIL ITERATION_NUMBER = MONTH_ID [ 1 ]
      (
              -- iteration_number + 1 = the current month processing.
              -- iteration_number = prior month

              DEFERRED_RENT [ 0 ] = 0, BEG_DEFERRED_RENT [ 0 ] = 0, END_DEFERRED_RENT [ 0 ] = 0,

              BEG_DEFERRED_RENT [ 1 ] = END_DEFERRED_RENT [ 0 ],

              BEG_PREPAID_RENT [ 0 ] = 0, PREPAY_AMORTIZATION [ 0 ] = 0, PREPAID_RENT [ 0 ] = 0, END_PREPAID_RENT [ 0 ] = 0,

              BEG_ST_DEFERRED_RENT [ 0 ] = 0, ST_DEFERRED_RENT [ 0 ] = 0, END_ST_DEFERRED_RENT [ 0 ] = 0,

              BEG_ARREARS_ACCRUAL [ 0 ] = 0, ARREARS_ACCRUAL [ 0 ] = 0, END_ARREARS_ACCRUAL [ 0 ] = 0,

              INT_PAID [ 0 ] = 0,

              --If there's a balance loaded in the first month, set that, otherwise starting balance is 0
              BEG_DEFERRED_RENT [ 1 ] = CASE
                                          WHEN MONTH [ 1 ] = CONVERSION_BALANCE_MONTH [ CV(ID) ] THEN
                                            ALLOC_DEFERRED_RENT [ CV(ID) ]
                                          ELSE
                                            0
                                        END,

              --If there's a balance loaded in the first month, set that, otherwise starting balance is 0
              BEG_PREPAID_RENT [ 1 ] = CASE
                                          WHEN MONTH [ 1 ] = CONVERSION_BALANCE_MONTH [ CV(ID) ] THEN
                                            ALLOC_PREPAID_RENT [ CV(ID) ]
                                          ELSE
                                            0
                                       END,

              --If there's a balance loaded in the first month, set that, otherwise starting balance is 0
              CALC_BEG_PREPAID_RENT [ 1 ] = CASE
                                              WHEN MONTH [ 1 ] = CONVERSION_BALANCE_MONTH [ CV(ID) ] THEN
                                                ALLOC_PREPAID_RENT [ CV(ID) ]
                                              ELSE
                                                0
                                            END,

              --If there's a balance loaded in the first month, set the MONTH [ 0 ] amount because the NET_DEFERRED_PREPAID calculation further below compares to previous month
              NET_DEFERRED_PREPAID [ 0 ] = CASE
                                            WHEN MONTH [ 0 ] = CONVERSION_BALANCE_MONTH [ CV(ID) ] THEN
                                              ALLOC_PREPAID_RENT [ CV(ID) + 1] - ALLOC_DEFERRED_RENT [ CV(ID) + 1]
                                            ELSE
                                              0
                                           END,

              -- CJS temporary for testing purposes to avoid skewing ST numbers until they're correct
              -- ST_DEFERRED_RENT [ ITERATION_NUMBER - 11 ] = INT_PAID [ ITERATION_NUMBER + 1 ] - INT_PAID [ ITERATION_NUMBER - 11 ],

              BEG_ST_DEFERRED_RENT [ ITERATION_NUMBER - 11 ] = END_ST_DEFERRED_RENT [ CV(ID) - 1 ],

              END_ST_DEFERRED_RENT [ ITERATION_NUMBER - 11 ] = ST_DEFERRED_RENT [ CV(ID) ] - ST_DEFERRED_RENT [ CV(ID) - 11] + BEG_ST_DEFERRED_RENT [ CV(ID) ],

              -- If there's a conversion balance, don't calculate deferred or prepaid before the conversion month
              NET_DEFERRED_PREPAID [ ITERATION_NUMBER + 1 ] = CASE
                                                                WHEN IS_OM [ CV(ID) ] = 1 AND DEFERRED_RENT_SW [ CV(ID) ] = 1 AND MONTH [ CV(ID)] >= CONVERSION_BALANCE_MONTH [ CV(ID) ] THEN
                                                                  NET_DEFERRED_PREPAID [ CV(ID) - 1 ]
                                                                  + Decode(
                                                                            lower(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Lease Non-Monthly Payment Spread',COMPANY_ID [ CV(ID) ]))),
                                                                            'yes',
                                                                            PAID_SPREAD [ CV(ID) ],
                                                                            INT_PAID [ CV(ID) ]
                                                                          )
                                                                  - INT_ACCRUAL [ CV(ID) ]
                                                                ELSE
                                                                  0
                                                              END,

              -- Conversion balances are beginning balances effective in CONVERSION_BALANCE_MONTH, so if there's a balance set the end balance of the prior month to the loaded balance to roll forward
              NET_DEFERRED_PREPAID [ ITERATION_NUMBER + 1 ] = CASE
                                                                WHEN IS_OM [ CV(ID) ] = 1 AND DEFERRED_RENT_SW [ CV(ID) ] = 1 AND MONTH [ CV(ID)] = Add_Months(CONVERSION_BALANCE_MONTH [ CV(ID) ], -1) THEN
                                                                  ALLOC_PREPAID_RENT [ CV(ID) ] - ALLOC_DEFERRED_RENT [ CV(ID) ]
                                                                ELSE
                                                                  NET_DEFERRED_PREPAID [ CV(ID) ]
                                                              END,

              NET_DEFERRED_PREPAID [ 1 ] = DECODE(IS_OM [ CV(ID) ] * DEFERRED_RENT_SW [ CV(ID) ],
                                          1,
                                          Decode(IS_REMEASUREMENT [ 1 ],
                                                  1,
                                                  (-REMEASURED_DEFERRED_RENT [ 1 ] + REMEASURED_PREPAID_RENT [ 1 ])
                                                        +  Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Lease Non-Monthly Payment Spread',COMPANY_ID [ CV(ID) ]))),
                                                                            'yes',
                                                                            PAID_SPREAD [ CV(ID) ],
                                                                            INT_PAID [ CV(ID) ]
                                                                  )
                                                        - INT_ACCRUAL [ CV(ID) ],
                                                  NET_DEFERRED_PREPAID [ 1 ]),
                                          0),
              PREPAY_AMORTIZATION [ ITERATION_NUMBER + 1 ] = CASE WHEN lower(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Lease Non-Monthly Payment Spread',COMPANY_ID [ CV(ID) ]))) = 'yes' THEN
                                                               CASE WHEN IS_OM [ CV(ID) ] = 1 AND PREPAY_SWITCH [ CV(ID) ] = 1
                                                                    AND (NEG_PAY_SHIFT [ CV(ID) ] = 0
                                                                         or (NEG_PAY_SHIFT [ CV(ID) ] > 0 AND TERM_COUNT [ CV(ID) ] > NEG_PAY_SHIFT [ CV(ID) ])) THEN
                                                                      INT_PAID [ CV(ID) ] - PAID_SPREAD [ CV(ID) ]
                                                                    WHEN IS_OM [ CV(ID) ] = 1 AND PREPAY_SWITCH [ CV(ID) ] = 1
                                                                    AND NEG_PAY_SHIFT [ CV(ID) ] > 0 AND TERM_COUNT [ CV(ID) ] = NEG_PAY_SHIFT [ CV(ID) ] THEN
                                                                      INT_PAID [ CV(ID) ] - PAID_SPREAD [ CV(ID) ] + END_ARREARS_ACCRUAL [ CV(ID) - 1 ]
                                                               ELSE
                                                                 0
                                                               END
                                                             ELSE
                                                               0
                                                             END,

              -- If there's a conversion balance, don't calculate prepay amortization before the conversion month
              PREPAY_AMORTIZATION [ ITERATION_NUMBER + 1 ] = CASE WHEN MONTH [ CV(ID)] < CONVERSION_BALANCE_MONTH [ CV(ID) ] THEN 0 else PREPAY_AMORTIZATION [ CV(ID) ] end,

              PAY_GROUP [ ITERATION_NUMBER + 1 ] = PREPAY_AMORTIZATION [ CV(ID) - 1 ] + PAYMENT_MONTH [ CV(ID) ],

              CONV_PAY_TERM [ ITERATION_NUMBER + 1 ] = CASE
                                                        WHEN MONTH [ CV(ID)] >= CONVERSION_BALANCE_MONTH [ CV(ID) ] THEN
                                                          PAY_GROUP [ CV(ID) ]
                                                        ELSE
                                                          0
                                                       END,

              PREPAY_AMORTIZATION [ ITERATION_NUMBER + 1 ] = Decode(CONV_PAY_TERM [ CV(ID) ], 0, 0, PREPAY_AMORTIZATION [ CV(ID) ]),

              ARREARS_ACCRUAL [ ITERATION_NUMBER + 1 ] = CASE WHEN IS_OM [ CV(ID) ] = 1 AND PREPAY_SWITCH [ CV(ID) ] = 0
                                                                   AND lower(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Lease Non-Monthly Payment Spread',COMPANY_ID [ CV(ID) ]))) = 'yes' THEN
                                                                PAID_SPREAD [ CV(ID) ] - INT_PAID [ CV(ID) ]
                                                              WHEN IS_OM [ CV(ID) ] = 1 AND PREPAY_SWITCH [ CV(ID) ] = 1 AND NEG_PAY_SHIFT [ CV(ID) ] > 0
                                                                   AND TERM_COUNT[ CV(ID) ] < NEG_PAY_SHIFT[ CV(ID) ]
                                                                   AND lower(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Lease Non-Monthly Payment Spread',COMPANY_ID [ CV(ID) ]))) = 'yes' THEN
                                                                INT_PAID [ CV(ID) ] - PAID_SPREAD [ CV(ID) ]
                                                              WHEN IS_OM [ CV(ID) ] = 1 AND PREPAY_SWITCH [ CV(ID) ] = 1 AND NEG_PAY_SHIFT [ CV(ID) ] > 0
                                                                   AND TERM_COUNT[ CV(ID) ] = NEG_PAY_SHIFT[ CV(ID) ]
                                                                   AND lower(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Lease Non-Monthly Payment Spread',COMPANY_ID [ CV(ID) ]))) = 'yes' THEN
                                                                -1 * END_ARREARS_ACCRUAL [ CV(ID) - 1 ]
                                                              ELSE
                                                                0
                                                              END,

              BEG_ARREARS_ACCRUAL [ ITERATION_NUMBER + 1 ] = END_ARREARS_ACCRUAL [ CV(ID) - 1 ],

              END_ARREARS_ACCRUAL [ ITERATION_NUMBER + 1 ] = BEG_ARREARS_ACCRUAL [ CV(ID) ] + ARREARS_ACCRUAL [ CV(ID) ],

              -- MONTH[1] is specifically handled above, so only roll forward balances for other months
              BEG_DEFERRED_RENT [ ITERATION_NUMBER + 1 ] = Decode( CV(ID), 1, BEG_DEFERRED_RENT [ 1 ], END_DEFERRED_RENT [ CV(ID) - 1 ]),
              BEG_DEFERRED_RENT [ 1 ] = DECODE(IS_OM [ CV(ID) ] * DEFERRED_RENT_SW [ CV(ID) ], 1, Decode(IS_REMEASUREMENT [ 1 ], 1, REMEASURED_DEFERRED_RENT [ 1 ], BEG_DEFERRED_RENT [ 1 ]), 0),
              END_DEFERRED_RENT [ ITERATION_NUMBER + 1 ] = Decode(Sign(NET_DEFERRED_PREPAID [ CV(ID) ]), -1, -NET_DEFERRED_PREPAID [ CV(ID) ], 0),
              DEFERRED_RENT [ ITERATION_NUMBER + 1 ] = DECODE(IS_OM [ CV(ID) ] * DEFERRED_RENT_SW [ CV(ID) ], 1, END_DEFERRED_RENT [ CV(ID) ] - BEG_DEFERRED_RENT [ CV(ID) ], 0),

              -- If there's a conversion balance, don't calculate deferred rent before the conversion month
              DEFERRED_RENT [ ITERATION_NUMBER + 1 ] = CASE WHEN MONTH [ CV(ID)] < CONVERSION_BALANCE_MONTH [ CV(ID) ] THEN 0 else DEFERRED_RENT [ CV(ID) ] end,

              -- MONTH[1] is specifically handled above, so only roll forward balances for other months
              CALC_BEG_PREPAID_RENT [ ITERATION_NUMBER + 1 ] = Decode( CV(ID), 1, CALC_BEG_PREPAID_RENT [ 1 ], CALC_END_PREPAID_RENT [ CV(ID) - 1 ]),
              CALC_BEG_PREPAID_RENT [ 1 ] = Decode(IS_REMEASUREMENT [ 1 ], 1, REMEASURED_PREPAID_RENT [ 1 ], CALC_BEG_PREPAID_RENT [ 1 ]),
              CALC_END_PREPAID_RENT [ ITERATION_NUMBER + 1 ] = Decode(Sign(NET_DEFERRED_PREPAID [ CV(ID) ]), 1, NET_DEFERRED_PREPAID [ CV(ID) ], 0),

              PREPAID_RENT [ ITERATION_NUMBER + 1 ] = DECODE(IS_OM [ CV(ID) ] * DEFERRED_RENT_SW [ CV(ID) ], 1, CALC_END_PREPAID_RENT [ CV(ID) ] - CALC_BEG_PREPAID_RENT [ CV(ID) ], 0),

              -- If there's a conversion balance, don't calculate prepaid rent before the conversion month
              PREPAID_RENT [ ITERATION_NUMBER + 1 ] = CASE WHEN MONTH [ CV(ID)] < CONVERSION_BALANCE_MONTH [ CV(ID) ] THEN 0 else PREPAID_RENT [ CV(ID) ] end,

              -- MONTH[1] is specifically handled above, so only roll forward balances for other months
              BEG_PREPAID_RENT [ ITERATION_NUMBER + 1 ] = Decode( CV(ID), 1, BEG_PREPAID_RENT [ 1 ], END_PREPAID_RENT [ CV(ID) - 1 ]),
              BEG_PREPAID_RENT [ 1 ] = DECODE(IS_OM [ CV(ID) ] * DEFERRED_RENT_SW [ CV(ID) ], 1, Decode(IS_REMEASUREMENT [ 1 ], 1, REMEASURED_PREPAID_RENT [ 1 ], BEG_PREPAID_RENT [ 1 ]), 0),

              -- If there's a conversion balance use that, otherwise calculate from beginning balance + activity
              END_PREPAID_RENT [ ITERATION_NUMBER + 1 ] = CASE
                                                            WHEN IS_OM [ CV(ID) ] = 1 AND DEFERRED_RENT_SW [ CV(ID) ] = 1 AND MONTH [ CV(ID)] = Add_Months(CONVERSION_BALANCE_MONTH [ CV(ID) ], -1) THEN
                                                              ALLOC_PREPAID_RENT [ CV(ID) ]
                                                            ELSE
                                                              BEG_PREPAID_RENT [ CV(ID) ] + PREPAY_AMORTIZATION [ CV(ID) ] + PREPAID_RENT [ CV(ID) ]
                                                          END
              )
      ) B
    ON (A.ID = B.ID and A.ILR_ID = B.ILR_ID and A.REVISION = B.REVISION and A.LS_ASSET_ID = B.LS_ASSET_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.MONTH = B.MONTH)
    when matched then update set
        A.BEG_DEFERRED_RENT = B.BEG_DEFERRED_RENT,
        A.DEFERRED_RENT = B.DEFERRED_RENT,
        A.END_DEFERRED_RENT = B.END_DEFERRED_RENT,
        A.BEG_ST_DEFERRED_RENT = B.BEG_ST_DEFERRED_RENT,
        A.END_ST_DEFERRED_RENT = B.END_ST_DEFERRED_RENT,
        A.BEG_PREPAID_RENT = B.BEG_PREPAID_RENT,
        A.PREPAY_AMORTIZATION = B.PREPAY_AMORTIZATION,
        A.PREPAID_RENT = B.PREPAID_RENT,
        A.END_PREPAID_RENT = B.END_PREPAID_RENT,
        A.BEG_ARREARS_ACCRUAL = B.BEG_ARREARS_ACCRUAL,
        A.ARREARS_ACCRUAL = B.ARREARS_ACCRUAL,
        A.END_ARREARS_ACCRUAL = B.END_ARREARS_ACCRUAL
        ;

   end P_HANDLE_DEFERRED_RENT;

   --**************************************************************************
   --                            F_CALC_ASSET_SCHEDULE
   -- This function will Build the asset schedule out
   --**************************************************************************
   function F_CALC_ASSET_SCHEDULE return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
   begin
      L_STATUS := 'Undo prior asset schedule calculations';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_ASSET_SCHEDULE_CALC_STG
       where (ILR_ID, REVISION, set_of_books_id) in (select A.ILR_ID, A.REVISION, a.set_of_books_id from ls_ilr_stg A);
      L_STATUS := 'Starting to build asset schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

      insert into LS_ILR_ASSET_SCHEDULE_CALC_STG S
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, month, AMOUNT, RESIDUAL_AMOUNT,
          PREPAY_SWITCH, PAYMENT_MONTH, MONTHS_TO_ACCRUE, RATE, NPV, BEG_CAPITAL_COST,
          END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LIABILITY, END_LIABILITY, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL,
          INTEREST_PAID, PRINCIPAL_PAID, PENNY_ROUNDER, PENNY_PRIN_ROUNDER, PENNY_INT_ROUNDER,
          PENNY_END_ROUNDER, PRINCIPAL_ACCRUED, INTEREST_ACCRUED, PRIN_ROUND_ACCRUED,
          INT_ROUND_ACCRUED, TERM_PENALTY, BPO_PRICE, BEG_LT_OBLIGATION, END_LT_OBLIGATION, BEG_LT_LIABILITY, END_LT_LIABILITY,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, PAYMENT_TERM_TYPE_ID, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT,
          BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
          PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST, INCENTIVE_MATH_AMOUNT, IDC_MATH_AMOUNT,
          DEFERRED_RENT_SW, INTEREST_CONVENTION_ID, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
          PARTIAL_MONTH_PERCENT, ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
		  IMPAIRMENT_ACTIVITY, BEG_ACCUM_IMPAIR, END_ACCUM_IMPAIR,
          UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS, incentive_cap_amount)
    WITH prelim AS
        (SELECT ilr_id,
                ls_asset_id,
                revision,
                set_of_books_id,
                MONTH,
                row_number() OVER (PARTITION BY ilr_id, ls_asset_id, revision, set_of_books_id ORDER BY MONTH) AS ID,
                months_between(MAX(MONTH) OVER(PARTITION BY ilr_id, ls_asset_id, revision, set_of_books_id), MONTH) AS month_id
			FROM ls_ilr_asset_schedule_stg),
         mid_period_pay_month as
         (select p.ilr_id, p.revision, p.set_of_books_id, t.payment_term_id,
                    case when neg_pay_shift = 0 then
                      p.first_pay_month
                    else
                      case when p.first_pay_month = decode(p.prepay_switch, 0, decode(f.payment_freq_id, 1, 12, 2, 6, 3, 3, 1), 0) + p.neg_pay_shift then
                        1
                      else
                        p.first_pay_month
                      end
                    end first_pay_month
               from (
                SELECT s.ilr_id, s.revision, s.set_of_books_id, nvl(o.schedule_payment_shift, 0) neg_pay_shift, s.prepay_switch,
                       min(s.id) first_pay_month, min(s.payment_term_id) first_pay_term_id
                  FROM ls_ilr_schedule_stg s
                  join ls_ilr_options o on s.ilr_id = o.ilr_id and s.revision = o.revision
                 WHERE s.payment_month <> 0
                   group by s.ilr_id, s.revision, s.set_of_books_id, nvl(o.schedule_payment_shift, 0), s.prepay_switch
                    ) p
               join ls_ilr_payment_term_stg t on t.ilr_id = p.ilr_id
                                             and t.revision = t.revision
                                             and t.set_of_books_id = p.set_of_books_id
               join ls_ilr_payment_term_stg f on f.ilr_id = p.ilr_id
                                             and f.revision = t.revision
                                             and f.set_of_books_id = p.set_of_books_id
                                             and f.payment_term_id = p.first_pay_term_id)
         select ID,
                ILR_ID,
                REVISION,
                LS_ASSET_ID,
                SET_OF_BOOKS_ID,
                month,
                AMT,
                RES_AMT,
                PREPAY_SWITCH,
                PAYMENT_MONTH,
                MONTHS_TO_ACCRUE,
                RATE,
                NPV,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                BEG_LIABILITY,
                END_LIABILITY,
                INT_ACCRUAL,
                PRIN_ACCRUAL,
                INT_PAID,
                PRIN_PAID,
                ADD_A_PENNY,
                ADD_A_PENNY_PRIN_ACCRUAL,
                ADD_A_PENNY_INT_ACCRUAL,
                ADD_A_PENNY_END,
                PRIN_ACCRUED,
                INT_ACCRUED,
                PRIN_ACCRUED_ROUND,
                INT_ACCRUED_ROUND,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                BEG_LT_LIABILITY,
                END_LT_LIABILITY,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                IS_OM,
                PAYMENT_TERM_TYPE_ID,
                BEG_DEFERRED_RENT,
                DEFERRED_RENT,
                END_DEFERRED_RENT,
                BEG_ST_DEFERRED_RENT,
                END_ST_DEFERRED_RENT,
                PRINCIPAL_REMEASUREMENT,
                LIABILITY_REMEASUREMENT,
                INCENTIVE_AMOUNT,
                INITIAL_DIRECT_COST,
                math_incentive_amount,
                math_init_direct_cost,
                DEFERRED_RENT_SW,
                INTEREST_CONVENTION_ID,
                ROU_ASSET_REMEASUREMENT,
                PARTIAL_TERM_GAIN_LOSS,
                ADDITIONAL_ROU_ASSET,
                partial_month_percent,
                ST_OBLIGATION_REMEASUREMENT,
                LT_OBLIGATION_REMEASUREMENT,
                OBLIGATION_RECLASS,
                IMPAIRMENT_ACTIVITY,
                BEG_ACCUM_IMPAIR,
                END_ACCUM_IMPAIR,
                UNACCRUED_INTEREST_REMEASURE,
                round(UNACCRUED_INTEREST_RECLASS,2) - (ROUND( END_LIABILITY , 2) - ROUND( END_LT_LIABILITY , 2)
                                                        - (ROUND( BEG_LIABILITY , 2) - ROUND( BEG_LT_LIABILITY , 2))
                                                        - ROUND( INT_ACCRUAL , 2)
                                                        + ROUND( INT_PAID , 2)
                                                        + ROUND( PRIN_PAID , 2)
                                                        - ROUND( OBLIGATION_RECLASS , 2)
                                                        - ROUND( ST_OBLIGATION_REMEASUREMENT , 2)
                                                        + ROUND( UNACCRUED_INTEREST_RECLASS , 2)
                                                        + ROUND( UNACCRUED_INTEREST_REMEASURE , 2)) as UNACCRUED_INTEREST_RECLASS,
														incentive_cap_amount
                     FROM (SELECT ls_ilr_asset_schedule_stg.ilr_id,
                        ls_ilr_asset_schedule_stg.ls_asset_id,
                        ls_ilr_asset_schedule_stg.revision,
                        ls_ilr_asset_schedule_stg.set_of_books_id,
                        ls_ilr_asset_schedule_stg.MONTH,
                        prelim.ID,
                        month_id,
                        beg_obligation,
                        amount AS amt,
                        residual_amount AS res_amt,
                        prepay_switch,
                        payment_month,
                        months_to_accrue,
                        rate,
                        beg_capital_cost,
                        amount AS amount,
                        interest_accrual AS int_accrual,
                        interest_paid AS int_paid,
                        principal_accrual AS prin_accrual,
                        principal_paid AS prin_paid,
                        end_capital_cost,
                        end_obligation,
                        npv,
                        term_penalty,
                        bpo_price,
                        beg_lt_obligation,
                        end_lt_obligation,
                        contingent_accrual1,
                        contingent_accrual2,
                        contingent_accrual3,
                        contingent_accrual4,
                        contingent_accrual5,
                        contingent_accrual6,
                        contingent_accrual7,
                        contingent_accrual8,
                        contingent_accrual9,
                        contingent_accrual10,
                        executory_accrual1,
                        executory_accrual2,
                        executory_accrual3,
                        executory_accrual4,
                        executory_accrual5,
                        executory_accrual6,
                        executory_accrual7,
                        executory_accrual8,
                        executory_accrual9,
                        executory_accrual10,
                        contingent_paid1,
                        contingent_paid2,
                        contingent_paid3,
                        contingent_paid4,
                        contingent_paid5,
                        contingent_paid6,
                        contingent_paid7,
                        contingent_paid8,
                        contingent_paid9,
                        contingent_paid10,
                        executory_paid1,
                        executory_paid2,
                        executory_paid3,
                        executory_paid4,
                        executory_paid5,
                        executory_paid6,
                        executory_paid7,
                        executory_paid8,
                        executory_paid9,
                        executory_paid10,
                        is_om,
                        payment_term_type_id,
                        amount AS prin_fixed,
                        beg_deferred_rent,
                        deferred_rent,
                        end_deferred_rent,
                        beg_st_deferred_rent,
                        end_st_deferred_rent,
                        BEG_LIABILITY,
                        END_LIABILITY,
                        BEG_LT_LIABILITY,
                        END_LT_LIABILITY,
                        PRINCIPAL_REMEASUREMENT,
                        LIABILITY_REMEASUREMENT,
                        PRIOR_MONTH_END_OBLIGATION,
                        PRIOR_MONTH_END_LIABILITY,
                        PRIOR_MONTH_END_LT_OBLIGATION,
                        PRIOR_MONTH_END_LT_LIABILITY,
                        PRIOR_MONTH_END_CAPITAL_COST,
                        IS_REMEASUREMENT,
                        Nvl(incentive_amount,0) incentive_amount,
                        Nvl(initial_direct_cost,0) initial_direct_cost,
                        Nvl(incentive_math_amount,0) math_incentive_amount,
                        Nvl(idc_math_amount,0) math_init_direct_cost,
                        INTEREST_CONVENTION_ID,
                        OM_TO_CAP_INDICATOR,
                        ls_ilr_asset_schedule_stg.DEFERRED_RENT_SW,
                        PRIOR_MONTH_END_DEFERRED_RENT,
                        PRIOR_MONTH_END_PREPAID_RENT,
                        PRIOR_MONTH_END_ARREARS_ACCR,
                        PRIOR_MONTH_NET_ROU_ASSET,
                        nvl(REMEASUREMENT.REMEASUREMENT_TYPE,0) REMEASUREMENT_TYPE,
                        REMEASUREMENT.ORIGINAL_ASSET_QUANTITY,
                        REMEASUREMENT.CURRENT_ASSET_QUANTITY,
                        PARTIAL_MONTH_PERCENT,
                        UNPAID_ACCRUED_INTEREST,
                        MID_PERIOD_PAY_MONTH.FIRST_PAY_MONTH FIRST_PAY_MONTH,
                        ST_OBLIGATION_REMEASUREMENT,
                        LT_OBLIGATION_REMEASUREMENT,
                        OBLIGATION_RECLASS,
                        UNACCRUED_INTEREST_REMEASURE,
                        UNACCRUED_INTEREST_RECLASS,
                        LIOP.REMEASURE_CALC_GAIN_LOSS,
						            IMPAIRMENT_ACTIVITY,
						            PRIOR_MONTH_END_ACCUM_IMPAIR,
                        fmap.fasb_cap_type_id,
                        M.MAX_ID,
						incentive_cap_amount
                 FROM ls_ilr_asset_schedule_stg
                 JOIN prelim ON ls_ilr_asset_schedule_stg.ilr_id = prelim.ilr_id
                      AND ls_ilr_asset_schedule_stg.ls_asset_id = prelim.ls_asset_id
                      AND ls_ilr_asset_schedule_stg.revision = prelim.revision
                      AND ls_ilr_asset_schedule_stg.set_of_books_id = prelim.set_of_books_id
                      AND ls_ilr_asset_schedule_stg.MONTH = prelim.MONTH
                 left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON LS_ILR_ASSET_SCHEDULE_STG.ilr_id = remeasurement.ilr_id
                      AND LS_ILR_ASSET_SCHEDULE_STG.revision = remeasurement.revision
                      AND LS_ILR_ASSET_SCHEDULE_STG.ls_asset_id = remeasurement.ls_asset_id
                      AND LS_ILR_ASSET_SCHEDULE_STG.set_of_books_id = remeasurement.set_of_books_id
                      AND LS_ILR_ASSET_SCHEDULE_STG.month = Trunc(remeasurement.remeasurement_date, 'month')
                 JOIN (select ilr_id, revision, set_of_books_id, month, payment_term_id
                         from ls_ilr_schedule_stg) stg on ls_ilr_asset_schedule_stg.ilr_id = stg.ilr_id
                      AND ls_ilr_asset_schedule_stg.revision = stg.revision
                      AND ls_ilr_asset_schedule_stg.set_of_books_id = stg.set_of_books_id
                      AND ls_ilr_asset_schedule_stg.MONTH = stg.MONTH
                 JOIN LS_ILR_OPTIONS liop ON ls_ilr_asset_schedule_stg.ilr_id = liop.ilr_id
                      AND ls_ilr_asset_schedule_stg.revision = liop.revision
                 JOIN LS_FASB_CAP_TYPE_SOB_MAP fmap on fmap.lease_cap_type_id = liop.lease_cap_type_id
                                                   and fmap.set_of_books_id = ls_ilr_asset_schedule_stg.set_of_books_id
                 join (select ilr_id, revision, set_of_books_id, max(id) max_id
                         from ls_ilr_schedule_stg
                        group by ilr_id, revision, set_of_books_id) m on m.ilr_id = stg.ilr_id
                                                                     and m.revision = stg.revision
                                                                     and m.set_of_books_id = stg.set_of_books_id
                 LEFT OUTER JOIN mid_period_pay_month on ls_ilr_asset_schedule_stg.ilr_id = mid_period_pay_month.ilr_id
                      AND ls_ilr_asset_schedule_stg.revision = mid_period_pay_month.revision
                      AND ls_ilr_asset_schedule_stg.set_of_books_id = mid_period_pay_month.set_of_books_id
                      AND stg.payment_term_id = mid_period_pay_month.payment_term_id)
           MODEL PARTITION BY(ilr_id,
                              ls_asset_id,
                              revision,
                              set_of_books_id)
           DIMENSION BY(ID)
           MEASURES(beg_obligation,
                    amt,
                    res_amt,
                    prepay_switch,
                    payment_month,
                    months_to_accrue,
                    0 AS add_a_penny,
                    0 AS add_a_penny_end,
                    0 AS add_a_penny_prin_accrual,
                    0 AS add_a_penny_int_accrual,
                    rate,
                    beg_capital_cost,
                    amount,
                    int_accrual,
                    0 AS int_accrued,
                    int_paid,
                    prin_accrual,
                    0 AS prin_accrued,
                    prin_paid,
                    end_capital_cost,
                    end_obligation,
                    MONTH,
                    npv,
                    0 AS prin_accrued_round,
                    0 AS int_accrued_round,
                    month_id,
                    term_penalty,
                    bpo_price,
                    beg_lt_obligation,
                    end_lt_obligation,
                    contingent_accrual1,
                    contingent_accrual2,
                    contingent_accrual3,
                    contingent_accrual4,
                    contingent_accrual5,
                    contingent_accrual6,
                    contingent_accrual7,
                    contingent_accrual8,
                    contingent_accrual9,
                    contingent_accrual10,
                    executory_accrual1,
                    executory_accrual2,
                    executory_accrual3,
                    executory_accrual4,
                    executory_accrual5,
                    executory_accrual6,
                    executory_accrual7,
                    executory_accrual8,
                    executory_accrual9,
                    executory_accrual10,
                    contingent_paid1,
                    contingent_paid2,
                    contingent_paid3,
                    contingent_paid4,
                    contingent_paid5,
                    contingent_paid6,
                    contingent_paid7,
                    contingent_paid8,
                    contingent_paid9,
                    contingent_paid10,
                    executory_paid1,
                    executory_paid2,
                    executory_paid3,
                    executory_paid4,
                    executory_paid5,
                    executory_paid6,
                    executory_paid7,
                    executory_paid8,
                    executory_paid9,
                    executory_paid10,
                    is_om,
                    payment_term_type_id,
                    prin_fixed,
                    beg_deferred_rent,
                    deferred_rent,
                    end_deferred_rent,
                    beg_st_deferred_rent,
                    end_st_deferred_rent,
                    BEG_LIABILITY,
                    END_LIABILITY,
                    BEG_LT_LIABILITY,
                    END_LT_LIABILITY,
                    PRINCIPAL_REMEASUREMENT,
                    LIABILITY_REMEASUREMENT,
                    PRIOR_MONTH_END_OBLIGATION AS REMEASURED_BEG_OBLIGATION,
                    PRIOR_MONTH_END_LIABILITY AS REMEASURED_BEG_LIABILITY,
                    PRIOR_MONTH_END_LT_OBLIGATION AS REMEASURED_BEG_LT_OBLIGATION,
                    PRIOR_MONTH_END_LT_LIABILITY AS REMEASURED_BEG_LT_LIABILITY,
                    PRIOR_MONTH_END_CAPITAL_COST AS REMEASURED_BEG_CAPITAL_COST,
                    IS_REMEASUREMENT,
                    INCENTIVE_AMOUNT,
                    INITIAL_DIRECT_COST,
                    math_incentive_amount,
                    math_init_direct_cost,
                    INTEREST_CONVENTION_ID,
                    OM_TO_CAP_INDICATOR,
                    DEFERRED_RENT_SW,
                    PRIOR_MONTH_END_DEFERRED_RENT,
                    PRIOR_MONTH_END_PREPAID_RENT,
                    PRIOR_MONTH_NET_ROU_ASSET AS REMEASURED_NET_ROU_ASSET,
                    0 AS PCT_CHANGE_IN_LIABILITY,
                    0 as ROU_ASSET_REMEASUREMENT,
                    0 as PARTIAL_TERM_GAIN_LOSS,
                    PRIOR_MONTH_END_ARREARS_ACCR,
                    REMEASUREMENT_TYPE,
                    ORIGINAL_ASSET_QUANTITY,
                    CURRENT_ASSET_QUANTITY,
                    0 AS ADDITIONAL_ROU_ASSET,
                    PARTIAL_MONTH_PERCENT,
                    PRIOR_MONTH_END_LIABILITY - Decode(OM_TO_CAP_INDICATOR, 0, PRIOR_MONTH_END_OBLIGATION, 0) AS REM_DIFF,
                    0 AS PAY_GROUP,
                    0 AS NEW_NPV,
                    FIRST_PAY_MONTH,
                    0 ST_OBLIGATION_REMEASUREMENT,
                    0 LT_OBLIGATION_REMEASUREMENT,
                    0 OBLIGATION_RECLASS,
                    0 UNACCRUED_INTEREST_REMEASURE,
                    0 UNACCRUED_INTEREST_RECLASS,
                    case when REMEASUREMENT_TYPE = 3 then
                      0
                    else
                      NVL(REMEASURE_CALC_GAIN_LOSS,1)
                    end REMEASURE_CALC_GAIN_LOSS,
                    IMPAIRMENT_ACTIVITY,
                    PRIOR_MONTH_END_ACCUM_IMPAIR,
                    0 BEG_ACCUM_IMPAIR,
                    0 END_ACCUM_IMPAIR,
                    FASB_CAP_TYPE_ID,
                    MAX_ID,
					incentive_cap_amount
                    )
         IGNORE NAV RULES UPSERT ITERATE(100000) UNTIL ITERATION_NUMBER = MONTH_ID [ 1 ]
         (
             --
             -- iteration_number + 1 = the current month processing.
             -- iteration_number = prior month
             -- iteration_number - 11 = 12 months prior
             --
             -- if prepaid, the first months amount goes into the period zero accrual
             PAYMENT_MONTH [ 0 ] = 1,
             PRIN_ACCRUAL [ 0 ] = DECODE(PREPAY_SWITCH [ 1 ], 1, AMOUNT [ 1 ], 0),
             PRIN_ACCRUED [ 0 ] = DECODE(PREPAY_SWITCH [ 1 ], 1, AMOUNT [ 1 ], 0),
             INT_ACCRUAL [ 0 ] = 0,
             INT_ACCRUED [ 0 ] = 0,
             PRIN_ACCRUED_ROUND [ 0 ] = DECODE(PREPAY_SWITCH [ 1 ], 1, AMOUNT [ 1 ], 0),
             INT_ACCRUED_ROUND [ 0 ] = 0,

             PAY_GROUP [ ITERATION_NUMBER + 1 ] = PAY_GROUP[ CV(ID) - 1 ] + PAYMENT_MONTH[ CV(ID) ],

            -- IFF the current processing month is a paymeny month AND it is prepaid,
            -- then reset the accural amount for last month to be based on current months payment amount (minus last months interest)
            -- ELSE lease prin_accrual for last month alone
            PRIN_ACCRUAL[ ITERATION_NUMBER ] = DECODE(PAYMENT_MONTH [ CV(ID) + 1 ], 0, PRIN_ACCRUAL[ CV(ID) ],
                                          case when PAYMENT_TERM_TYPE_ID[ CV(ID) + 1] = 3 then
                                              PRIN_ACCRUAL[CV(ID)]
                                          else
                                            DECODE(PREPAY_SWITCH[CV(ID) + 1], 1, AMOUNT[ CV(ID) + 1 ] - INT_ACCRUAL[CV(ID)], PRIN_ACCRUAL[CV(ID)] )
                                          end
                                          ),

            RATE [ ITERATION_NUMBER + 1 ] = CASE
                                              WHEN INTEREST_CONVENTION_ID [ CV(ID) ] = 1 THEN
                                              --Interest Method (interest_convention_id = 1)
                                              --Set the rate to be based on my number of months accrued (to account for monthly interest accrued during periods)
                                              --((1+(Rate for NPV)))^(Months to Accrue)-1)/(Months to Accrue)
                                                (POWER(1 + RATE [ CV(ID) ], MONTHS_TO_ACCRUE [ CV(ID) ]) - 1) / MONTHS_TO_ACCRUE [ CV(ID) ]
                                              WHEN INTEREST_CONVENTION_ID [ CV(ID) ] = 2 THEN
                                              --Compounding Interest (interest_convention_id = 2)
                                              --Use the existing rate used for calculating NPV
                                                RATE [ CV(ID) ]
                                            END,

            -- If this is the month after the payment, then accrued amount is equal to 0 if arrears or accrual amount of prior period if prepaid.
            --  All other months are equal to prior accrued amount + prior accrual
            INT_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - 1 ], 0, INT_ACCRUED [ CV(ID) - 1 ] + INT_ACCRUAL [ CV(ID) - 1 ],
                                          PREPAY_SWITCH [ CV(ID) ] * INT_ACCRUAL [ CV(ID) - 1 ]),
            PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - 1 ], 0, PRIN_ACCRUED [ CV(ID) - 1 ] + PRIN_ACCRUAL [ CV(ID) - 1 ],
                                             PREPAY_SWITCH [ CV(ID) ] * PRIN_ACCRUAL [ CV(ID) - 1 ]),
            INT_ACCRUED_ROUND [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - 1 ], 0,
                                                INT_ACCRUED_ROUND [ CV(ID) - 1 ] + ROUND(INT_ACCRUAL [ CV(ID) - 1 ], 2),
                                                PREPAY_SWITCH [ CV(ID) ] * ROUND(INT_ACCRUAL [ CV(ID) - 1 ], 2)),
            PRIN_ACCRUED_ROUND [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - 1 ], 0,
                                                PRIN_ACCRUED_ROUND [ CV(ID) - 1 ] + ROUND(PRIN_ACCRUAL [ CV(ID) - 1 ], 2),
                                                PREPAY_SWITCH [ CV(ID) ] * ROUND(PRIN_ACCRUAL [ CV(ID) - 1 ], 2)),

            -- if prepaid, the end balance is used to determine the accrual
            INT_ACCRUAL [ ITERATION_NUMBER + 1 ] =
               case when PAYMENT_TERM_TYPE_ID[CV(ID)] = 1 then
                  (last_day(PKG_LEASE_SCHEDULE.F_GET_II_BEGIN_DATE(CV(ILR_ID), CV(REVISION))) - PKG_LEASE_SCHEDULE.F_GET_II_BEGIN_DATE(CV(ILR_ID), CV(REVISION)))/
                   (last_day(PKG_LEASE_SCHEDULE.F_GET_II_BEGIN_DATE(CV(ILR_ID), CV(REVISION))) - last_day(add_months(PKG_LEASE_SCHEDULE.F_GET_II_BEGIN_DATE(CV(ILR_ID), CV(REVISION)), -1)))
                   * RATE [ CV(ID) ]
                   * (BEG_OBLIGATION [ CV(ID) ] - DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, PREPAY_SWITCH [ CV(ID) ] * PRIN_ACCRUED [ CV(ID) ]))
               else RATE [ CV(ID) ] *
                  CASE
                    WHEN INTEREST_CONVENTION_ID [ CV(ID) ] = 1 THEN
                    --Interest Method (interest_convention_id = 1)
                    --Interest Accrual = (Beg Obligation - If Prepay(Principal Accrual)) * Interest Rate
                      (BEG_OBLIGATION [ CV(ID) ] - DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, PREPAY_SWITCH [ CV(ID) ] * PRIN_ACCRUED [ CV(ID) ]))
                    WHEN INTEREST_CONVENTION_ID [ CV(ID) ] = 2 THEN
                    --Compounding Interest (interest_convention_id = 2)
                    --Interest Accrual = (Beg Liability - If Prepay(Total Payment))* Interest Rate
                      (BEG_LIABILITY [ CV(ID) ] - DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, PREPAY_SWITCH [ CV(ID) ] * PRIN_FIXED [ CV(ID) ]))
                  END * PARTIAL_MONTH_PERCENT [ CV(ID) ]
               end,
            AMOUNT[ ITERATION_NUMBER + 1] = case
               when PKG_LEASE_SCHEDULE.F_MAKE_II(CV(ILR_ID),CV(REVISION)) = 1 and PAYMENT_TERM_TYPE_ID[CV(ID)] = 1 then
                  INT_ACCRUAL[ CV(ID) ]
               else
                  AMOUNT[ CV(ID)]
               end,

                 PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = case
                  when PAYMENT_TERM_TYPE_ID[ CV(ID) ] = 3 then
                    PRIN_FIXED[ CV(ID) ]
                  else
               -- if this is a payment month... and it is prepaid... then use just negative interest.
               DECODE(PAYMENT_MONTH [CV(ID)], 0, AMOUNT [CV(ID)],
                     DECODE(PREPAY_SWITCH[CV(ID)], 1, 0, AMOUNT [CV(ID)]) )
               - INT_ACCRUAL [CV(ID)]
                  end,
                -- for prepaid, the accrual from the prior period is used for the payment amount
                -- for prin, the first period will include the full first payment amount
                 INT_PAID [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0,(1 - PREPAY_SWITCH [ CV(ID) ]) * INT_ACCRUAL [ CV(ID) ] + INT_ACCRUED [ CV(ID) ]) ,
                 PRIN_PAID [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0,(1 - PREPAY_SWITCH [ CV(ID) ]) * PRIN_ACCRUAL [ CV(ID) ] + PRIN_ACCRUED [ CV(ID) ]),

                -- after setting the paid amount, set the accrued amounts to be zero
                 PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, PRIN_ACCRUED [ CV(ID) ], 0),
                 INT_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, INT_ACCRUED [ CV(ID) ], 0),

                -- TRUEUP rounding that may come into play around accrued balanced <> paid balances
                -- take the prior months accrued rounding.  And add the accrual (if arrears).  Else do not add anything
                 ADD_A_PENNY_PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, DECODE(CV(ID), 1, 0, ROUND(PRIN_PAID [ CV(ID) ], 2) - PRIN_ACCRUED_ROUND [ CV(ID) ] - (1 - PREPAY_SWITCH [ CV(ID) ]) * ROUND(PRIN_ACCRUAL [ CV(ID) ], 2))),
                 ADD_A_PENNY_INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, DECODE(CV(ID), 1, 0, ROUND(INT_PAID [ CV(ID) ], 2) - INT_ACCRUED_ROUND [ CV(ID) ] - (1 - PREPAY_SWITCH [ CV(ID) ]) * ROUND(INT_ACCRUAL [ CV(ID) ], 2))),

                -- trueup principal accrual for rounding between periods for arrears
                 PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUAL [ CV(ID) ] + ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY_PRIN_ACCRUAL [ CV(ID) ]),
                 INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = INT_ACCRUAL [ CV(ID) ] + ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY_INT_ACCRUAL [ CV(ID) ]),

                -- prepaids update prior month accrual.  And current month accrued
                 PRIN_ACCRUAL [ ITERATION_NUMBER ] = PRIN_ACCRUAL [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_PRIN_ACCRUAL [ CV(ID) + 1 ]),
                 PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUED [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_PRIN_ACCRUAL [ CV(ID) ]),
                 INT_ACCRUAL [ ITERATION_NUMBER ] = INT_ACCRUAL [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_INT_ACCRUAL [ CV(ID) + 1 ]),
                 INT_ACCRUED [ ITERATION_NUMBER + 1 ] = INT_ACCRUED [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_INT_ACCRUAL [ CV(ID) ]),

                --If there's unpaid accrued interest from a mid period remeasurement (exclude monthly payment terms), plug the interest and accrual payments
                INT_PAID [ ITERATION_NUMBER + 1 ] = CASE WHEN PAY_GROUP[ CV(ID) ] = 1 AND PAYMENT_MONTH[ CV(ID) ] = 1 AND NVL(FIRST_PAY_MONTH[ CV(ID) ], 1) <> 1 AND MONTHS_TO_ACCRUE[ CV(ID) ] <> 1 THEN
                                                      INT_PAID[ CV(ID) ] + REM_DIFF[ 1 ]
                                                    WHEN PAY_GROUP[ CV(ID) ] = 1 AND PAYMENT_MONTH[ CV(ID) ] = 1 AND IS_REMEASUREMENT[ CV(ID) ] = 1 THEN
                                                      INT_PAID[ CV(ID) ] + REM_DIFF[ 1 ]
                                                    ELSE
                                                      INT_PAID[ CV(ID) ]
                                                     END,

                PRIN_PAID [ ITERATION_NUMBER + 1 ] = CASE WHEN PAY_GROUP[ CV(ID) ] = 1 AND PAYMENT_MONTH[ CV(ID) ] = 1 AND NVL(FIRST_PAY_MONTH[ CV(ID) ], 1) <> 1 AND MONTHS_TO_ACCRUE[ CV(ID) ] <> 1 THEN
                                                        PRIN_PAID[ CV(ID) ] - REM_DIFF[ 1 ]
                                                      WHEN PAY_GROUP[ CV(ID) ] = 1 AND PAYMENT_MONTH[ CV(ID) ] = 1 AND IS_REMEASUREMENT[ CV(ID) ] = 1 THEN
                                                        PRIN_PAID[ CV(ID) ] - REM_DIFF[ 1 ]
                                                      ELSE
                                                        PRIN_PAID[ CV(ID) ]
                                                      END,

                -- end capital cost, obligation, and liability
                 END_OBLIGATION [ ITERATION_NUMBER + 1 ] = BEG_OBLIGATION [ CV(ID) ] - PRIN_PAID [ CV(ID) ],

                END_OBLIGATION [ ITERATION_NUMBER + 1 ] = CASE WHEN REM_DIFF [ 1 ] <> 0 AND CV(ID) = 1 AND PAY_GROUP [ CV(ID) ] = 0 THEN
                                                            END_OBLIGATION [ CV(ID) ] - REM_DIFF [ 1 ]
                                                          WHEN REM_DIFF [ 1 ] <> 0 AND CV(ID) = 1 AND IS_REMEASUREMENT[ CV(ID) ] = 1 THEN
                                                            END_OBLIGATION [ CV(ID) ] - NVL(REM_DIFF [ 1 ], 0)
                                                          ELSE
                                                            END_OBLIGATION [ CV(ID) ]
                                                          END,

                 END_LIABILITY [ ITERATION_NUMBER + 1 ] = BEG_LIABILITY [ CV(ID) ] - PRIN_PAID [ CV(ID) ] + INT_ACCRUAL [ CV(ID) ] - INT_PAID [ CV(ID) ],

                -- check if a penny needs to be added to principal and subtracted from interest
                -- if the last month.  Make sure end obligation is zero
                 ADD_A_PENNY [ ITERATION_NUMBER + 1 ] = ROUND(BEG_OBLIGATION [ CV(ID) ], 2) - ROUND(PRIN_PAID [ CV(ID) ], 2) - ROUND(END_OBLIGATION [ CV(ID) ], 2)
                                                        - CASE WHEN CV(ID) = 1 AND IS_REMEASUREMENT [ CV(ID) ] = 1 and REM_DIFF [ 1 ] <> 0 THEN
                                                            REM_DIFF [ 1 ]
                                                           ELSE
                                                             0
                                                           END,

                -- apply from current month accrual if arrears
                 INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = INT_ACCRUAL [ CV(ID) ] - ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY [ CV(ID) ]),
                 PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUAL [ CV(ID) ] + ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY [ CV(ID) ]),

                -- for prepaid, apply to prior period accrual and current month accrued
                 INT_ACCRUAL [ ITERATION_NUMBER ] = INT_ACCRUAL [ CV(ID) ] - (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY [ CV(ID) + 1 ]),
                 PRIN_ACCRUAL [ ITERATION_NUMBER ] = PRIN_ACCRUAL [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY [ CV(ID) + 1 ]),
                 INT_ACCRUED [ ITERATION_NUMBER + 1 ] = INT_ACCRUED [ CV(ID) ] - (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY [ CV(ID) ]),
                 PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUED [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY [ CV(ID) ]),

                -- Apply to current period payment
                 INT_PAID [ ITERATION_NUMBER + 1 ] = INT_PAID [ CV(ID) ] - ADD_A_PENNY [ CV(ID) ],
                 PRIN_PAID [ ITERATION_NUMBER + 1 ] = PRIN_PAID [ CV(ID) ] + ADD_A_PENNY [ CV(ID) ],

                -- in the month where end obligation should go to zero (payment_month = 2).
                -- MAKE sure this happens by applying rounding
                -- check beginning balance minus payment minus res - bpo - termination
                 ADD_A_PENNY_END [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, ROUND(BEG_OBLIGATION [ CV(ID) ], 2) - ROUND(PRIN_PAID [ CV(ID) ], 2) - RES_AMT [ MAX_ID [ CV(ID) ] ] - BPO_PRICE [ MAX_ID [ CV(ID) ] ] - TERM_PENALTY [ MAX_ID [ CV(ID) ] ], 0),

                -- if arrears, then modify accrual for current month
                 INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = INT_ACCRUAL [ CV(ID) ] - ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY_END [ CV(ID) ]),
                 PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUAL [ CV(ID) ] + ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY_END [ CV(ID) ]),

                -- for prepaid, apply to prior period accrual and current month accrued
                 INT_ACCRUAL [ ITERATION_NUMBER ] = INT_ACCRUAL [ CV(ID) ] - (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_END [ CV(ID) + 1 ]),
                 PRIN_ACCRUAL [ ITERATION_NUMBER ] = PRIN_ACCRUAL [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_END [ CV(ID) + 1 ]),
                 INT_ACCRUED [ ITERATION_NUMBER + 1 ] = INT_ACCRUED [ CV(ID) ] - (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_END [ CV(ID) ]),
                 PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUED [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_END [ CV(ID) ]),

                -- Apply to current period payment
                 INT_PAID [ ITERATION_NUMBER + 1 ] = INT_PAID [ CV(ID) ] - ADD_A_PENNY_END [ CV(ID) ],
                 PRIN_PAID [ ITERATION_NUMBER + 1 ] = PRIN_PAID [ CV(ID) ] + ADD_A_PENNY_END [ CV(ID) ],

                -- set end obligation to be residual + bpo + termination if last month.
                 END_OBLIGATION [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, RES_AMT [ CV(ID) ] + BPO_PRICE [ CV(ID) ] + TERM_PENALTY [ CV(ID) ], END_OBLIGATION [ CV(ID) ]),

                 BEG_OBLIGATION [ ITERATION_NUMBER + 2 ] = END_OBLIGATION [ CV(ID) - 1 ],

                 -- set end liability to be residual + bpo + termination if last month.
                 END_LIABILITY [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, RES_AMT [ CV(ID) ] + BPO_PRICE [ CV(ID) ] + TERM_PENALTY [ CV(ID) ], END_LIABILITY [ CV(ID) ]),

                 BEG_LIABILITY [ ITERATION_NUMBER + 2 ] = END_LIABILITY [ CV(ID) - 1 ],

                -- if last payment month and it is a prepaid, then set the accrual to be the prepaid payment amount (stored in prin_accrued for period 0)
                 PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, DECODE(PREPAY_SWITCH [ CV(ID) ], 1, PRIN_ACCRUED [ 0 ], PRIN_ACCRUAL [ CV(ID) ]), PRIN_ACCRUAL [ CV(ID) ]),
                 INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, DECODE(PREPAY_SWITCH [ CV(ID) ], 1, 0, INT_ACCRUAL [ CV(ID) ]), INT_ACCRUAL [ CV(ID) ]),

                -- end lt obligation is equal to the end obligation in 12 months
                 BEG_LT_OBLIGATION [ ITERATION_NUMBER + 1 ] = 0,
                 END_LT_OBLIGATION [ ITERATION_NUMBER + 1 ] = 0,
                 BEG_LT_OBLIGATION [ ITERATION_NUMBER - 11 ] = BEG_OBLIGATION [ ITERATION_NUMBER + 1 ],
                 END_LT_OBLIGATION [ ITERATION_NUMBER - 11 ] = Decode(PAYMENT_MONTH [ ITERATION_NUMBER + 1 ], 2, END_OBLIGATION [ ITERATION_NUMBER + 1 ] - (RES_AMT  [ ITERATION_NUMBER + 1  ] + BPO_PRICE  [ ITERATION_NUMBER + 1 ] + TERM_PENALTY  [ ITERATION_NUMBER + 1 ]), END_OBLIGATION [ ITERATION_NUMBER + 1 ]),
                 BEG_LT_LIABILITY [ ITERATION_NUMBER + 1 ] = 0,
                 END_LT_LIABILITY [ ITERATION_NUMBER + 1 ] = 0,
                 BEG_LT_LIABILITY [ ITERATION_NUMBER - 11 ] = BEG_LIABILITY [ ITERATION_NUMBER + 1 ],
                 END_LT_LIABILITY [ ITERATION_NUMBER - 11 ] =  Decode(PAYMENT_MONTH [ ITERATION_NUMBER + 1 ], 2, END_LIABILITY [ITERATION_NUMBER +1    ] - (RES_AMT [ ITERATION_NUMBER +1 ]    + BPO_PRICE [ ITERATION_NUMBER +   1 ] + TERM_PENALTY [ ITERATION_NUMBER + 1 ]), END_LIABILITY [ ITERATION_NUMBER + 1 ]),

                 DEFERRED_RENT [ 0 ] = 0, BEG_DEFERRED_RENT [ 0 ] = 0, END_DEFERRED_RENT [ 0 ] = 0, BEG_ST_DEFERRED_RENT [ 0 ] = 0, END_ST_DEFERRED_RENT [ 0 ] = 0,

                 BEG_ST_DEFERRED_RENT [ ITERATION_NUMBER + 1 ] = 0, END_ST_DEFERRED_RENT [ ITERATION_NUMBER + 1 ] = 0,

                 --Remeasurements
                 PRINCIPAL_REMEASUREMENT [ ITERATION_NUMBER + 1 ] = 0,
                 LIABILITY_REMEASUREMENT [ ITERATION_NUMBER + 1 ] = 0,

                 PRINCIPAL_REMEASUREMENT [ 1 ] = CASE WHEN IS_REMEASUREMENT [ 1 ] = 1 AND OM_TO_CAP_INDICATOR [ 1 ] = 1 THEN
                                                  END_OBLIGATION [ 1 ] + PRIN_PAID [ 1 ]
                                                 ELSE
                                                  CASE
                                                    WHEN IS_REMEASUREMENT [ 1 ] = 1 AND ITERATION_NUMBER = 1 THEN
                                                      BEG_OBLIGATION [ 1 ] - Decode(IS_OM [ CV(ID) ], 1, PRIN_PAID [ CV(ID) ], 0) - REMEASURED_BEG_OBLIGATION [ 1 ]
                                                    ELSE
                                                      PRINCIPAL_REMEASUREMENT [ CV(ID) ]
                                                  END
                                                 END,

                 LIABILITY_REMEASUREMENT [ 1 ] = CASE WHEN IS_REMEASUREMENT [ 1 ] = 1 AND OM_TO_CAP_INDICATOR [ 1 ] = 1 THEN
                                                  END_LIABILITY [ 1 ] + PRIN_PAID [ 1 ] - INT_ACCRUAL [ 1 ] + INT_PAID [ 1 ]
                                                 ELSE
                                                  CASE
                                                    WHEN IS_REMEASUREMENT [ 1 ] = 1 AND ITERATION_NUMBER = 1 AND remeasurement_type[ 1 ] <> 2 THEN
                                                      --Cap Remeasure
                                                      BEG_OBLIGATION [ 1 ] - Decode(IS_OM [ CV(ID) ], 1, PRIN_PAID [ CV(ID) ], 0) - REMEASURED_BEG_LIABILITY [ 1 ]
                                                    WHEN IS_REMEASUREMENT [ 1 ] = 1 AND ITERATION_NUMBER = 1 AND remeasurement_type[ 1 ] = 2 THEN
                                                      --Cap Remeasure Partial Quantity
                                                      -1 * (REMEASURED_BEG_LIABILITY [ 1 ] * (1 - (current_asset_quantity [ 1 ] / original_asset_quantity [ 1 ] )))
                                                    ELSE
                                                      LIABILITY_REMEASUREMENT [ CV(ID) ]
                                                  END
                                                 END,

                NEW_NPV [ 1 ] = CASE WHEN ITERATION_NUMBER = 1 THEN
                                  BEG_OBLIGATION [ 1 ]
                                ELSE
                                  NEW_NPV [ CV(ID) ]
                                END,

                PCT_CHANGE_IN_LIABILITY [iteration_number + 1] = 0,
                ROU_ASSET_REMEASUREMENT [iteration_number + 1] = 0,
                PARTIAL_TERM_GAIN_LOSS [iteration_number + 1] = 0,

                pct_change_in_liability [1] = case when liability_remeasurement[ 1 ] < 0 and remeasured_beg_liability[1] <> 0 then
                        (LIABILITY_REMEASUREMENT[1]) / (REMEASURED_BEG_LIABILITY [1])
                        else
                        0
                        end,
                ROU_ASSET_REMEASUREMENT [ 1 ] = CASE WHEN LIABILITY_REMEASUREMENT [ 1 ] < 0 and REMEASUREMENT_TYPE [ 1 ] in (0, 1) and REMEASURE_CALC_GAIN_LOSS [ 1 ] = 1 THEN
                                                   REMEASURED_NET_ROU_ASSET [1] * PCT_CHANGE_IN_LIABILITY [1]
                                                  WHEN remeasurement_type [ 1 ] = 2 THEN
                                                   -1 * REMEASURED_NET_ROU_ASSET [1] * ( 1 - (current_asset_quantity [ 1 ] / original_asset_quantity [ 1 ] ))
                                                 END,
                BEG_OBLIGATION [ 1 ] = Decode(OM_TO_CAP_INDICATOR [ CV(ID) ], 1, 0, BEG_OBLIGATION [ 1 ]),
                BEG_LIABILITY [ 1 ] = Decode(OM_TO_CAP_INDICATOR [ CV(ID) ], 1, 0, BEG_LIABILITY [ 1 ]),

                BEG_OBLIGATION [ 1 ] = BEG_OBLIGATION [ 1 ] - Decode(IS_REMEASUREMENT [ 1 ] * ITERATION_NUMBER, 1, Decode(IS_OM [ CV(ID) ], 1, PRIN_PAID [ CV(ID) ], 0) + PRINCIPAL_REMEASUREMENT [ 1 ], 0),

                ADDITIONAL_ROU_ASSET [ 1 ] = CASE WHEN REMEASUREMENT_TYPE [ 1 ] = 2 AND ITERATION_NUMBER = 1 THEN
                                              NEW_NPV [ 1 ] - (BEG_OBLIGATION [ 1 ] + LIABILITY_REMEASUREMENT [ 1 ] + REM_DIFF[ 1 ])
                                            ELSE
                                              ADDITIONAL_ROU_ASSET [ CV(ID) ]
                                            END,
                PARTIAL_TERM_GAIN_LOSS [ 1 ] = CASE WHEN REMEASUREMENT_TYPE [ 1 ] = 2 THEN
                                                 PRINCIPAL_REMEASUREMENT [ 1 ] - ADDITIONAL_ROU_ASSET [ 1 ] - ROU_ASSET_REMEASUREMENT [ 1 ]
                                               WHEN REMEASUREMENT_TYPE [ 1 ] in (0, 1) and liability_remeasurement[ 1 ] < 0 and REMEASURE_CALC_GAIN_LOSS [ 1 ] = 1 THEN
                                                 LIABILITY_REMEASUREMENT [1] - ROU_ASSET_REMEASUREMENT[1]
                                               ELSE
                                                 PARTIAL_TERM_GAIN_LOSS [ CV(ID) ]
                                               END,
                --If the liability and principal remeasurements are different, force them to be equal. The difference is unpaid accrued interest, which is plugged in the payments.
                PRINCIPAL_REMEASUREMENT [ 1 ] = CASE WHEN REM_DIFF [ 1 ] <> 0 AND ITERATION_NUMBER = 1 THEN
                                                  PRINCIPAL_REMEASUREMENT [ 1 ] - REM_DIFF [ 1 ]
                                                ELSE
                                                  PRINCIPAL_REMEASUREMENT[1]
                                                END,

                 BEG_LIABILITY [ 1 ] = BEG_LIABILITY [ 1 ] - Decode(IS_REMEASUREMENT [ 1 ] * ITERATION_NUMBER, 1, Decode(IS_OM [ CV(ID) ], 1, PRIN_PAID [ CV(ID) ], 0) + LIABILITY_REMEASUREMENT [ 1 ] + ADDITIONAL_ROU_ASSET [ 1 ], 0),

                 BEG_LT_OBLIGATION [ 1 ] = Decode(CV(ID) * IS_REMEASUREMENT [ 1 ], 1, REMEASURED_BEG_LT_OBLIGATION [ 1 ], BEG_LT_OBLIGATION [ 1 ]),
                 BEG_LT_LIABILITY [ 1 ] = Decode(CV(ID) * IS_REMEASUREMENT [ 1 ], 1, REMEASURED_BEG_LT_LIABILITY [ 1 ], BEG_LT_lIABILITY [ 1 ]),

                 BEG_CAPITAL_COST [ 1 ] = Decode(IS_REMEASUREMENT [ 1 ], 1, REMEASURED_BEG_CAPITAL_COST [ 1 ], BEG_CAPITAL_COST [ CV(ID) ]),
                 END_CAPITAL_COST [ 1 ] = CASE WHEN IS_REMEASUREMENT [1] = 1 THEN
                                            CASE WHEN (pct_change_in_liability [1] < 0 or remeasurement_type [1] in (1, 2)) and REMEASURE_CALC_GAIN_LOSS [ 1 ] = 1 THEN
                                              --Partial Termination REMEASUREMENT
                                              Round(BEG_CAPITAL_COST [ 1 ], 2) + Round(ROU_ASSET_REMEASUREMENT[ 1 ], 2) + Round(ADDITIONAL_ROU_ASSET [ 1 ], 2)
                                            else
                                              --Normal REMEASUREMENT
                                              BEG_CAPITAL_COST [ 1 ]
                                              + LIABILITY_REMEASUREMENT [ 1 ]
                                              -- Don't include incentives, prepaid/deferred rent amounts if the FASB_CAP_TYPE_ID is 3 or 6 (FERC)
                                              + decode(FASB_CAP_TYPE_ID [ CV(ID) ], 3, 0, 6, 0, 1) *
                                                (
                                                 Decode(OM_TO_CAP_INDICATOR [ 1 ], 1, PRIOR_MONTH_END_PREPAID_RENT [ 1 ] - PRIOR_MONTH_END_DEFERRED_RENT [ 1 ] - PRIOR_MONTH_END_ARREARS_ACCR [ 1 ] - incentive_cap_amount[1], 0)
                                                )
                                            end
                                          else
                                            BEG_CAPITAL_COST [ CV(ID) ]
                                          end,
                 BEG_CAPITAL_COST [ ITERATION_NUMBER + 1 ] = Decode(IS_REMEASUREMENT [ 1 ], 1, END_CAPITAL_COST [ CV(ID) - 1 ],
                                                                    BEG_CAPITAL_COST [ CV(ID) ]
                                                                    -- Don't include incentives, prepaid/deferred rent amounts if the FASB_CAP_TYPE_ID is 3 or 6 (FERC)
                                                                    + decode(FASB_CAP_TYPE_ID [ CV(ID) ], 3, 0, 6, 0, 1) * (math_init_direct_cost[ 1 ] - incentive_cap_amount [ 1 ])),
                 END_CAPITAL_COST [ ITERATION_NUMBER + 1 ] = Decode(IS_REMEASUREMENT [ 1 ], 1, BEG_CAPITAL_COST [ ITERATION_NUMBER + 1 ], BEG_CAPITAL_COST [ CV(ID) ] ),

                 INCENTIVE_AMOUNT[ITERATION_NUMBER + 1] = INCENTIVE_AMOUNT[CV(ID)],
                 INITIAL_DIRECT_COST[ITERATION_NUMBER + 1] = INITIAL_DIRECT_COST[CV(ID)],

                 BEG_ACCUM_IMPAIR[ 1 ] = Decode(IS_REMEASUREMENT[1], 1, PRIOR_MONTH_END_ACCUM_IMPAIR[1], 0),
                 END_ACCUM_IMPAIR[ 0 ] = Decode(IS_REMEASUREMENT[1], 1, PRIOR_MONTH_END_ACCUM_IMPAIR[1], 0),

                 BEG_ACCUM_IMPAIR[ ITERATION_NUMBER + 1 ] = END_ACCUM_IMPAIR [ ITERATION_NUMBER ],
                 END_ACCUM_IMPAIR[ ITERATION_NUMBER + 1 ] = BEG_ACCUM_IMPAIR [ ITERATION_NUMBER +  1 ] + IMPAIRMENT_ACTIVITY [ CV(ID) ],

                 -- Short Term/Long Term Obligation Remeasurement
                 LT_OBLIGATION_REMEASUREMENT [ 1 ] = CASE WHEN IS_REMEASUREMENT [ 1 ] = 1 THEN
                                                       BEG_OBLIGATION [ CV(ID) + 12 ] - BEG_LT_OBLIGATION [ CV(ID) ]
                                                     ELSE
                                                       0
                                                     END,

                 ST_OBLIGATION_REMEASUREMENT [ 1 ] = CASE WHEN IS_REMEASUREMENT [ 1 ] = 1 THEN
                                                       LIABILITY_REMEASUREMENT [ CV(ID) ] + ADDITIONAL_ROU_ASSET [ CV(ID) ] - LT_OBLIGATION_REMEASUREMENT [ CV(ID) ]
                                                     ELSE
                                                       0
                                                     END,

                 -- Obligation Reclass
                 OBLIGATION_RECLASS [ ITERATION_NUMBER - 11 ] = BEG_LT_OBLIGATION [ CV(ID) ] + NVL(LT_OBLIGATION_REMEASUREMENT [ CV(ID) ],0) - END_LT_OBLIGATION [ CV(ID) ],

                 -- Unaccrued Interest Remeasurement
                 UNACCRUED_INTEREST_REMEASURE [ 1 ] = CASE WHEN IS_REMEASUREMENT [ 1 ] = 1 THEN
                                                        (BEG_LIABILITY [ CV(ID) + 12 ] - BEG_LT_LIABILITY [ CV(ID) ]) - (BEG_OBLIGATION [ CV(ID) + 12 ] - BEG_LT_OBLIGATION [ CV(ID) ])
                                                      ELSE
                                                        0
                                                      END,

                 -- Unaccrued Interest Reclass
                 UNACCRUED_INTEREST_RECLASS [ ITERATION_NUMBER - 11 ] = (BEG_LT_OBLIGATION [ CV(ID) ] + NVL(LT_OBLIGATION_REMEASUREMENT [ CV(ID) ],0) - END_LT_OBLIGATION [ CV(ID) ])
                                                                                - (BEG_LT_LIABILITY [ CV(ID) ] + NVL(LT_OBLIGATION_REMEASUREMENT [ CV(ID) ],0) + NVL(UNACCRUED_INTEREST_REMEASURE [ CV(ID) ], 0) - END_LT_LIABILITY[ CV(ID) ])

                 );

    L_STATUS:='Calculating Paid Spread';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

    -- Get the distinct list of schedule payment shift values and loop over each to do a separate merge
    for stg in (select distinct o.schedule_payment_shift
                    from ls_ilr_options o
                    join ls_ilr_stg s on s.ilr_id = o.ilr_id and s.revision = o.revision)
    loop
      merge into LS_ILR_ASSET_SCHEDULE_CALC_STG A
      using (
          with N as (select ROWNUM as THE_ROW from DUAL connect by level <= 10000),
               orig_mta as (
                 select ROW_NUMBER() OVER(partition by P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)),
                        P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID, P.PAYMENT_TERM_ID, P.PAYMENT_TERM_DATE, P.PAYMENT_TERM_TYPE_ID,
                        ADD_MONTHS(trunc( P.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1) MONTH,
                        DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) as MONTHS_TO_ACCRUE,
                        P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) NUM_MONTHS
                   from LS_ILR_PAYMENT_TERM_STG P
                   cross join N
                   join ls_ilr_options o on P.ilr_id = o.ilr_id and P.revision = o.revision
                  where N.THE_ROW <= (P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))
                                        + 1 * decode(P.payment_term_type_id,4,1,0)
                  AND (Decode(P.PAYMENT_TERM_TYPE_ID, 4, P.PAYMENT_TERM_ID, 1) = 1
                       OR
                       (P.PAYMENT_TERM_ID <> 1 AND trunc(p.PAYMENT_TERM_DATE, 'month') <> ADD_MONTHS(trunc( P.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1))
                      )
               ),
               schedule as (
                 select a.id, a.ilr_id, a.revision, a.ls_asset_id, a.set_of_books_id,
                        LEAD(Nvl(PRINCIPAL_PAID,0) + Nvl(INTEREST_PAID,0), nvl(O.SCHEDULE_PAYMENT_SHIFT,0), 0)
                             OVER (ORDER BY A.ILR_ID, A.REVISION, A.LS_ASSET_ID, A.SET_OF_BOOKS_ID, A.MONTH) as TOT_PAID,
                        a.MONTH,
                        NVL(LEAD(CASE WHEN A.ID < 1 THEN 0 ELSE PAYMENT_MONTH END, nvl(O.SCHEDULE_PAYMENT_SHIFT,0), 0)
                                 OVER (ORDER BY A.ILR_ID, A.REVISION, A.LS_ASSET_ID, A.SET_OF_BOOKS_ID, A.MONTH),0) AS PAYMENT_MONTH,
                        a.MONTHS_TO_ACCRUE,
                        a.PREPAY_SWITCH,
                        nvl(O.SCHEDULE_PAYMENT_SHIFT,0) NEG_PAY_SHIFT
                   from LS_ILR_ASSET_SCHEDULE_CALC_STG A, LS_ILR_ASSET_STG B, ls_ilr_options o
                  where A.ILR_ID = B.ILR_ID
                    and A.REVISION = B.REVISION
                    and A.LS_ASSET_ID = B.LS_ASSET_ID
                    and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID
                    and a.ilr_id = o.ilr_id
                    and a.revision = o.revision
                    and nvl(o.SCHEDULE_PAYMENT_SHIFT,0) = stg.schedule_payment_shift
                       )
          select ID,
                  ILR_ID,
                  REVISION,
                  LS_ASSET_ID,
                  SET_OF_BOOKS_ID,
                  MONTH,
                  PAID_SPREAD_NEW, INT_STORED, TOT_PAID, FUTURE_PAID, TEST_PM, PAYMENT_MONTH, FP_INDEX, MONTHS_REM_ACCRUE
            from schedule s
          join orig_mta m on s.ilr_id = m.ilr_id and s.revision = m.revision and s.set_of_books_id = m.set_of_books_id and s.month = m.month
          MODEL partition by(s.ILR_ID, s.LS_ASSET_ID, s.REVISION, s.SET_OF_BOOKS_ID)
          DIMENSION by(ID AS ID)
          MEASURES(
          TOT_PAID,
          s.MONTH,
          MONTHS_BETWEEN(max(s.month) OVER(partition by s.ILR_ID, s.LS_ASSET_ID, s.REVISION, s.SET_OF_BOOKS_ID), s.month) as MONTH_ID,
          s.PAYMENT_MONTH,
          0 as INT_STORED,
          m.MONTHS_TO_ACCRUE,
          0 AS FUTURE_PAID,
          0 as MONTHS_REM_ACCRUE,
          s.PREPAY_SWITCH,
          0 as PAID_SPREAD,
          0 as PAID_ACCRUED,
          0 as PAID_ACCRUED_ROUND,
          0 as ADD_A_PENNY_PAID_SPREAD,
          0 AS PAID_SPREAD_NEW,
          s.NEG_PAY_SHIFT,
          0 AS FP_INDEX,
          0 TEST_PM
          )
          IGNORE NAV RULES UPSERT ITERATE(100000) UNTIL ITERATION_NUMBER = MONTH_ID [ 1 ]
          (
            TOT_PAID [ 0 ] = 0, PAID_ACCRUED [ 0 ] = 0, PAID_ACCRUED_ROUND [ 0 ] = 0,

            --Need months to accrue + 1 in the 0 spot for arrears leases
            MONTHS_REM_ACCRUE [ 0 ] = (1 - PREPAY_SWITCH [ 1 ]) * (MONTHS_TO_ACCRUE [ 1 ] + 1),

            --For arrears, if last month was a payment, start back over on the remaining months to accrue
            --For prepaid, if this month is a payment, start back over on the remaining months to accrue
            --For both, if it's the last month (2) and there is actually a payment, restart months to accrue, if no payment, continue decrementing
            --Normal month, just decrement months to accrue from last month
            MONTHS_REM_ACCRUE [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - (1 - PREPAY_SWITCH [ CV(ID) ]) ], 1,
                    MONTHS_TO_ACCRUE [ CV(ID) ], 2,
                        DECODE(TOT_PAID [ CV(ID) ], 0,
                            MONTHS_REM_ACCRUE [ CV(ID) - 1 ] - 1, MONTHS_TO_ACCRUE [ CV(ID) ]),
                    MONTHS_REM_ACCRUE [ CV(ID) - 1 ] - 1),

            --For arrears, need to be able to reference the payment amount for the period in non-payment months; for monthlys, just use that months rent
            FP_INDEX [ ITERATION_NUMBER + 1 ] = CV(ID) + DECODE(MONTHS_TO_ACCRUE [ CV(ID) ], 1, 0, MONTHS_REM_ACCRUE [ CV(ID) ] - (1 - PREPAY_SWITCH [ CV(ID) ])),

            FUTURE_PAID [ ITERATION_NUMBER + 1 ] = CASE WHEN NEG_PAY_SHIFT [ CV(ID) ] > 0 THEN
                                                    CASE WHEN CV(ID) <= FP_INDEX [ CV(ID) ] THEN
                                                      TOT_PAID [ FP_INDEX [ CV(ID) ] ]
                                                    ELSE
                                                      0
                                                    END
                                                  ELSE
                                                    TOT_PAID [ CV(ID) + DECODE(MONTHS_TO_ACCRUE [ CV(ID) ], 1, 0, MONTHS_REM_ACCRUE [ CV(ID) ] - (1 - PREPAY_SWITCH [ CV(ID) ])) ]
                                                  END,

            --if prepaid, find the amount paid for the period and use for reference; if arrears, use the future_paid column; account for final month (2)
            INT_STORED [ ITERATION_NUMBER + 1 ] = DECODE(PREPAY_SWITCH [ CV(ID) ],
                                                         1,
                                                         DECODE(PAYMENT_MONTH [ CV(ID) ],
                                                                1, TOT_PAID [ CV(ID) ],
                                                                2, DECODE(TOT_PAID [ CV(ID) ],
                                                                          0 , INT_STORED [ CV(ID) ],
                                                                          TOT_PAID [ CV(ID) ]),
                                                                INT_STORED [ CV(ID) - 1 ]),
                                                         FUTURE_PAID [ CV(ID) ]),

            PAID_SPREAD [ ITERATION_NUMBER + 1 ] = case when MONTHS_TO_ACCRUE [ CV(ID) ] is null or MONTHS_TO_ACCRUE [ CV(ID) ] = 0
                                                        then 0
                                                        else INT_STORED [ CV(ID) ] / MONTHS_TO_ACCRUE [ CV(ID) ] end,

            PAID_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - (1 - PREPAY_SWITCH [ CV(ID) ]) ], 1,
                                              PAID_SPREAD [ CV(ID) ], 2, DECODE(TOT_PAID [ CV(ID) ], 0, PAID_ACCRUED [ CV(ID) - 1 ] + PAID_SPREAD [ CV(ID) ],
                                              PAID_SPREAD [ CV(ID) ]), PAID_ACCRUED [ CV(ID) - 1 ] + PAID_SPREAD [ CV(ID) ]),

            PAID_ACCRUED_ROUND [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - (1 - PREPAY_SWITCH [ CV(ID) ]) ], 1,
                                              ROUND(PAID_SPREAD [ CV(ID) ], 2), 2, DECODE(TOT_PAID [ CV(ID) ], 0, PAID_ACCRUED_ROUND [ CV(ID) - 1 ] + ROUND(PAID_SPREAD [ CV(ID) ], 2),
                                              ROUND(PAID_SPREAD [ CV(ID) ], 2)), PAID_ACCRUED_ROUND [ CV(ID) - 1 ] + ROUND(PAID_SPREAD [ CV(ID) ], 2)),

            ADD_A_PENNY_PAID_SPREAD [ ITERATION_NUMBER + 1 ] = DECODE(MONTHS_REM_ACCRUE [ CV(ID) ], 1, ROUND(INT_STORED [ CV(ID) ], 2) - DECODE(MONTHS_TO_ACCRUE [ CV(ID) ], 1, 0, PAID_ACCRUED_ROUND [ CV(ID) - 1 ]) - ROUND(PAID_SPREAD [ CV(ID) ], 2), 0),

            PAID_SPREAD [ ITERATION_NUMBER + 1 ] = PAID_SPREAD [ CV(ID) ]  + ADD_A_PENNY_PAID_SPREAD [ CV(ID) ],

            PAID_ACCRUED [ ITERATION_NUMBER + 1 ] = PAID_ACCRUED [ CV(ID) ] + ADD_A_PENNY_PAID_SPREAD [ CV(ID) ],

            PAID_SPREAD_NEW[ITERATION_NUMBER + 1] = PAID_SPREAD[ CV(ID) ]
          )
      ) B
      ON (A.ID = B.ID and A.ILR_ID = B.ILR_ID and A.REVISION = B.REVISION and A.LS_ASSET_ID = B.LS_ASSET_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.MONTH = B.MONTH)
      when matched then update set
          a.paid_spread = b.paid_spread_new;

    end loop;

    L_STATUS:='HANDLE OM';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    P_HANDLE_OM;
    L_STATUS:='HANDLE DEFERRED RENT';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    P_HANDLE_DEFERRED_RENT;
    L_STATUS:='SPREAD PRINCIPAL ACCRUAL';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    P_SPREAD_PRIN_ACCRUAL;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_CALC_ASSET_SCHEDULE;

   --**************************************************************************
   --                            F_LOAD_ASSET_SCHEDULE
   -- This function will Build the asset schedule out
   --**************************************************************************
   function F_LOAD_ASSET_SCHEDULE(A_MONTH in date:=null)  return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin

      L_STATUS := 'Starting to load asset schedule';
    L_MSG := 'OK';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      merge into LS_ILR_ASSET_SCHEDULE_STG S
      using (select * from LS_ILR_ASSET_SCHEDULE_CALC_STG) B
      on (B.ILR_ID = S.ILR_ID and B.LS_ASSET_ID = S.LS_ASSET_ID and B.REVISION = S.REVISION and B.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID and B.MONTH = S.MONTH)
      when matched then
         update
            set S.BEG_CAPITAL_COST = ROUND(B.BEG_CAPITAL_COST, 2),
                S.END_CAPITAL_COST = ROUND(B.END_CAPITAL_COST, 2),
                S.BEG_OBLIGATION = ROUND(B.BEG_OBLIGATION, 2),
                S.END_OBLIGATION = ROUND(B.END_OBLIGATION, 2),
                S.BEG_LT_OBLIGATION = ROUND(B.BEG_LT_OBLIGATION, 2),
                S.END_LT_OBLIGATION = ROUND(B.END_LT_OBLIGATION, 2),
                S.BEG_LIABILITY = ROUND(B.BEG_LIABILITY, 2),
                S.END_LIABILITY = ROUND(B.END_LIABILITY, 2),
                S.BEG_LT_LIABILITY = ROUND(B.BEG_LT_LIABILITY, 2),
                S.END_LT_LIABILITY = ROUND(B.END_LT_LIABILITY, 2),
                S.INTEREST_ACCRUAL = ROUND(B.INTEREST_ACCRUAL, 2),
                S.PRINCIPAL_ACCRUAL = ROUND(B.PRINCIPAL_ACCRUAL, 2),
                S.INTEREST_PAID = ROUND(B.INTEREST_PAID, 2),
                S.PRINCIPAL_PAID = ROUND(B.PRINCIPAL_PAID, 2),
                S.CONTINGENT_PAID1 = ROUND(B.CONTINGENT_PAID1, 2),
                S.CONTINGENT_PAID2 = ROUND(B.CONTINGENT_PAID2, 2),
                S.CONTINGENT_PAID3 = ROUND(B.CONTINGENT_PAID3, 2),
                S.CONTINGENT_PAID4 = ROUND(B.CONTINGENT_PAID4, 2),
                S.CONTINGENT_PAID5 = ROUND(B.CONTINGENT_PAID5, 2),
                S.CONTINGENT_PAID6 = ROUND(B.CONTINGENT_PAID6, 2),
                S.CONTINGENT_PAID7 = ROUND(B.CONTINGENT_PAID7, 2),
                S.CONTINGENT_PAID8 = ROUND(B.CONTINGENT_PAID8, 2),
                S.CONTINGENT_PAID9 = ROUND(B.CONTINGENT_PAID9, 2),
                S.CONTINGENT_PAID10 = ROUND(B.CONTINGENT_PAID10, 2),
                S.EXECUTORY_PAID1 = ROUND(B.EXECUTORY_PAID1, 2),
                S.EXECUTORY_PAID2 = ROUND(B.EXECUTORY_PAID2, 2),
                S.EXECUTORY_PAID3 = ROUND(B.EXECUTORY_PAID3, 2),
                S.EXECUTORY_PAID4 = ROUND(B.EXECUTORY_PAID4, 2),
                S.EXECUTORY_PAID5 = ROUND(B.EXECUTORY_PAID5, 2),
                S.EXECUTORY_PAID6 = ROUND(B.EXECUTORY_PAID6, 2),
                S.EXECUTORY_PAID7 = ROUND(B.EXECUTORY_PAID7, 2),
                S.EXECUTORY_PAID8 = ROUND(B.EXECUTORY_PAID8, 2),
                S.EXECUTORY_PAID9 = ROUND(B.EXECUTORY_PAID9, 2),
                S.EXECUTORY_PAID10 = ROUND(B.EXECUTORY_PAID10, 2),
                S.CONTINGENT_ACCRUAL1 = ROUND(B.CONTINGENT_ACCRUAL1, 2),
                S.CONTINGENT_ACCRUAL2 = ROUND(B.CONTINGENT_ACCRUAL2, 2),
                S.CONTINGENT_ACCRUAL3 = ROUND(B.CONTINGENT_ACCRUAL3, 2),
                S.CONTINGENT_ACCRUAL4 = ROUND(B.CONTINGENT_ACCRUAL4, 2),
                S.CONTINGENT_ACCRUAL5 = ROUND(B.CONTINGENT_ACCRUAL5, 2),
                S.CONTINGENT_ACCRUAL6 = ROUND(B.CONTINGENT_ACCRUAL6, 2),
                S.CONTINGENT_ACCRUAL7 = ROUND(B.CONTINGENT_ACCRUAL7, 2),
                S.CONTINGENT_ACCRUAL8 = ROUND(B.CONTINGENT_ACCRUAL8, 2),
                S.CONTINGENT_ACCRUAL9 = ROUND(B.CONTINGENT_ACCRUAL9, 2),
                S.CONTINGENT_ACCRUAL10 = ROUND(B.CONTINGENT_ACCRUAL10, 2),
                S.EXECUTORY_ACCRUAL1 = ROUND(B.EXECUTORY_ACCRUAL1, 2),
                S.EXECUTORY_ACCRUAL2 = ROUND(B.EXECUTORY_ACCRUAL2, 2),
                S.EXECUTORY_ACCRUAL3 = ROUND(B.EXECUTORY_ACCRUAL3, 2),
                S.EXECUTORY_ACCRUAL4 = ROUND(B.EXECUTORY_ACCRUAL4, 2),
                S.EXECUTORY_ACCRUAL5 = ROUND(B.EXECUTORY_ACCRUAL5, 2),
                S.EXECUTORY_ACCRUAL6 = ROUND(B.EXECUTORY_ACCRUAL6, 2),
                S.EXECUTORY_ACCRUAL7 = ROUND(B.EXECUTORY_ACCRUAL7, 2),
                S.EXECUTORY_ACCRUAL8 = ROUND(B.EXECUTORY_ACCRUAL8, 2),
                S.EXECUTORY_ACCRUAL9 = ROUND(B.EXECUTORY_ACCRUAL9, 2),
                S.EXECUTORY_ACCRUAL10 = ROUND(B.EXECUTORY_ACCRUAL10, 2),
                S.BEG_DEFERRED_RENT = ROUND(B.BEG_DEFERRED_RENT, 2),
                S.DEFERRED_RENT = ROUND(B.DEFERRED_RENT, 2),
                S.END_DEFERRED_RENT = ROUND(B.END_DEFERRED_RENT, 2),
                S.BEG_ST_DEFERRED_RENT = ROUND(B.BEG_ST_DEFERRED_RENT, 2),
                S.END_ST_DEFERRED_RENT = ROUND(B.END_ST_DEFERRED_RENT, 2),
                S.LIABILITY_REMEASUREMENT = ROUND(B.LIABILITY_REMEASUREMENT, 2),
                S.PRINCIPAL_REMEASUREMENT = ROUND(B.PRINCIPAL_REMEASUREMENT, 2),
                S.INCENTIVE_AMOUNT = ROUND(B.INCENTIVE_AMOUNT, 2),
                S.INITIAL_DIRECT_COST = ROUND(B.INITIAL_DIRECT_COST, 2),
                S.INCENTIVE_MATH_AMOUNT = ROUND(B.INCENTIVE_MATH_AMOUNT, 2),
                S.IDC_MATH_AMOUNT = ROUND(B.IDC_MATH_AMOUNT, 2),
                S.PREPAY_AMORTIZATION = ROUND(B.PREPAY_AMORTIZATION, 2),
                S.BEG_PREPAID_RENT = ROUND(B.BEG_PREPAID_RENT, 2),
                S.PREPAID_RENT = ROUND(B.PREPAID_RENT, 2),
                S.END_PREPAID_RENT = ROUND(B.END_PREPAID_RENT, 2),
                S.ROU_ASSET_REMEASUREMENT = ROUND(B.ROU_ASSET_REMEASUREMENT, 2),
                S.PARTIAL_TERM_GAIN_LOSS = ROUND(B.PARTIAL_TERM_GAIN_LOSS, 2),
                S.BEG_ARREARS_ACCRUAL = ROUND(B.BEG_ARREARS_ACCRUAL, 2),
                S.ARREARS_ACCRUAL = ROUND(B.ARREARS_ACCRUAL, 2),
                S.END_ARREARS_ACCRUAL = ROUND(B.END_ARREARS_ACCRUAL, 2),
                S.ADDITIONAL_ROU_ASSET = ROUND(B.ADDITIONAL_ROU_ASSET, 2),
                S.ST_OBLIGATION_REMEASUREMENT = ROUND(B.ST_OBLIGATION_REMEASUREMENT, 2),
                S.LT_OBLIGATION_REMEASUREMENT = ROUND(B.LT_OBLIGATION_REMEASUREMENT, 2),
                S.OBLIGATION_RECLASS = ROUND(B.OBLIGATION_RECLASS, 2),
                S.UNACCRUED_INTEREST_RECLASS = ROUND(B.UNACCRUED_INTEREST_RECLASS, 2),
                S.UNACCRUED_INTEREST_REMEASURE = ROUND(B.UNACCRUED_INTEREST_REMEASURE, 2),
				S.BEG_ACCUM_IMPAIR = ROUND(B.BEG_ACCUM_IMPAIR, 2),
				S.END_ACCUM_IMPAIR = ROUND(B.END_ACCUM_IMPAIR, 2),
				S.incentive_cap_amount = ROUND(B.incentive_cap_amount, 2);

    -- Process Components prior to save
    L_MSG := F_LOAD_COMPONENTS(A_MONTH);
    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

    return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_LOAD_ASSET_SCHEDULE;

   --**************************************************************************
   --                            F_PROCESS_ASSETS
   -- This function will process the assets under the ILRS
   -- IT will allocate the payments to the assets
   -- then calculate NBV for the asset
   --**************************************************************************
   function F_PROCESS_ASSETS(A_MONTH in date:=null) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('PROCESSING Assets');

      L_STATUS := 'CALLING F_ALLOCATE_TO_ASSETS';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := F_ALLOCATE_TO_ASSETS;
      if L_MSG = 'OK' then

         L_STATUS := 'CALLING F_CALC_ASSET_SCHEDULE';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_ASSET_SCHEDULE;

         if L_MSG = 'OK' then

            L_STATUS := 'CALLING F_LOAD_ASSET_SCHEDULE';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            L_MSG := F_LOAD_ASSET_SCHEDULE(A_MONTH);

            if L_MSG <> 'OK' THEN
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              return L_MSG;
            end if;


            -- Calculate VP here- all asset schedules built and in staging table
            -- so formulas requiring schedule fields will be able to calc
            /*L_STATUS := 'CALLING F_CALC_ILR_VAR_PAYMENTS';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            L_MSG := PKG_LEASE_VAR_PAYMENTS.F_CALC_ALL_VAR_PAYMENTS;
            if L_MSG <> 'OK' THEN
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              return L_MSG;
            end if;*/

            L_MSG := 'CALLING F_COPY_ASSET_SCHEDULE_ROWS';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            L_MSG:=F_COPY_ASSET_SCHEDULE_ROWS(A_MONTH);
            if L_MSG<>'OK' THEN
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              return L_MSG;
            end if;

            L_MSG := 'CALLING F_COPY_ILR_SCHEDULE_ROWS';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            L_MSG:=F_COPY_ILR_SCHEDULE_ROWS(A_MONTH);
            if L_MSG<>'OK' THEN
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              return L_MSG;
            end if;

            L_MSG := 'CALLING F_COPY_DEPR_FORECAST_ROWS';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            L_MSG:=F_COPY_DEPR_FORECAST_ROWS(A_MONTH);
            if L_MSG<>'OK' THEN
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            return L_MSG;
         end if;

            return L_MSG;
      end if;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ASSETS;

   --**************************************************************************
   --                            F_CALC_IRR
   -- This function will calculate the internal rate of return
   -- For ILRs where NPV > FMV (net present value is greater than fair market value)
   --**************************************************************************
   function F_CALC_IRR return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Starting IRR Calculation';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      merge into LS_ILR_STG S
      using (select ILR_ID, REVISION, SET_OF_BOOKS_ID, 1 / X - 1 as THE_IRR
               from (select *
                       from (select ILR_ID, REVISION, SET_OF_BOOKS_ID, month, sum(AMOUNT) as AMOUNT
                               from (select L.ILR_ID,
                                            S.REVISION,
                                            S.SET_OF_BOOKS_ID,
                                            S.MONTH,
                                            S.AMOUNT + S.RESIDUAL_AMOUNT + S.BPO_PRICE + S.TERM_PENALTY as AMOUNT
                                       from LS_ILR_SCHEDULE_STG S, LS_ILR_STG L
                                      where L.ILR_ID = S.ILR_ID
                                        and L.REVISION = S.REVISION
                                        and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                                        and L.NPV > L.FMV
                                        and L.IS_OM = 0
                                        and L.REVISION NOT IN (SELECT REVISION FROM LS_FORECAST_VERSION WHERE FORECAST_TYPE_ID = 2 UNION SELECT nvl(LOOKBACK_REVISION,0) FROM LS_FORECAST_VERSION WHERE FORECAST_TYPE_ID = 2)
                                        AND L.CONSIDER_FMV = 1
                                     union all
                                     select L.ILR_ID,
                                            S.REVISION,
                                            S.SET_OF_BOOKS_ID,
                                            min(ADD_MONTHS(S.MONTH, L.PREPAY_SWITCH - 1)),
                                            -1 * L.FMV
                                       from LS_ILR_SCHEDULE_STG S, LS_ILR_STG L
                                      where L.ILR_ID = S.ILR_ID
                                        and L.REVISION = S.REVISION
                                        and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                                        and L.NPV > L.FMV
                                        and L.IS_OM = 0
                                        and L.REVISION NOT IN (SELECT REVISION FROM LS_FORECAST_VERSION WHERE FORECAST_TYPE_ID = 2 UNION SELECT nvl(LOOKBACK_REVISION,0) FROM LS_FORECAST_VERSION WHERE FORECAST_TYPE_ID = 2)
                                        AND L.CONSIDER_FMV = 1
                                      group by L.ILR_ID, L.FMV, S.REVISION, S.SET_OF_BOOKS_ID)
                              group by ILR_ID, month, REVISION, SET_OF_BOOKS_ID
                              order by 1, 2) MODEL partition by(ILR_ID, REVISION, SET_OF_BOOKS_ID) DIMENSION by(ROW_NUMBER() OVER(partition by ILR_ID, REVISION, SET_OF_BOOKS_ID order by month) as THE_ROW) MEASURES(MONTHS_BETWEEN(month, FIRST_VALUE(month) OVER(partition by ILR_ID, REVISION, SET_OF_BOOKS_ID order by month)) as month, AMOUNT S, 0 SS, 0 F_A, 0 F_B, 0 F_X, 0 A, 1 B, 0 X, 0 ITER) RULES ITERATE(10000) UNTIL(ABS(F_X [ 1 ]) < POWER(10, -20))(SS [ any ] = S [ CV() ] * POWER(A [ 1 ], month [ CV() ]), F_A [ 1 ] = sum(SS) [ any ], SS [ any ] = S [ CV() ] * POWER(B [ 1 ], month [ CV() ]), F_B [ 1 ] = sum(SS) [ any ], X [ 1 ] = A [ 1 ] - F_A [ 1 ] * (B [ 1 ] - A [ 1 ]) / (F_B [ 1 ] - F_A [ 1 ]), SS [ any ] = S [ CV() ] * POWER(X [ 1 ], month [ CV() ]), F_X [ 1 ] = sum(SS) [ any ], A [ 1 ] = DECODE(SIGN(F_A [ 1 ] * F_X [ 1 ]), 1, X [ 1 ], A [ 1 ]), B [ 1 ] = DECODE(SIGN(F_A [ 1 ] * F_X [ 1 ]), 1, B [ 1 ], X [ 1 ]), ITER [ 1 ] = ITERATION_NUMBER + 1)) B
              where B.THE_ROW = 1
                and B.X <> 0) I
      on (I.ILR_ID = S.ILR_ID and I.REVISION = S.REVISION and I.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID)
      when matched then
         update set S.IRR = I.THE_IRR;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
     PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         return L_STATUS;
   end F_CALC_IRR;

   --**************************************************************************
   --                            F_NET_PRESENT_VALUE
   -- Determine the NET PRESENT VALUE of the future cash flow
   -- Based on the rate (either the discount rate entered or the IRR)
   --**************************************************************************
   function F_NET_PRESENT_VALUE return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
    type NPV_REC is record (
    ID number(22,0),
    ILR_ID LS_ILR_SCHEDULE_STG.ILR_ID%type,
    REVISION LS_ILR_SCHEDULE_STG.REVISION%type,
    SET_OF_BOOKS_ID LS_ILR_SCHEDULE_STG.SET_OF_BOOKS_ID%type,
    MONTH LS_ILR_SCHEDULE_STG.MONTH%type,
    NPV LS_ILR_SCHEDULE_STG.NPV%type,
    PAYMENT_TERM_TYPE_ID LS_ILR_SCHEDULE_STG.PAYMENT_TERM_TYPE_ID%type);
    type NPV_TABLE is table of NPV_REC index by pls_integer;
    L_NPV_REC_TABLE NPV_TABLE;


   begin
      L_STATUS := 'Starting NBV by payment';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

      select ID, ILR_ID, REVISION, SET_OF_BOOKS_ID, month, NPV, PAYMENT_TERM_TYPE_ID
      bulk collect
      into L_NPV_REC_TABLE
      from LS_ILR_SCHEDULE_STG stg,
            (select p.ilr_id, p.revision, p.set_of_books_id, t.payment_term_id,
                    p.first_pay_month
               from (
                SELECT s.ilr_id, s.revision, s.set_of_books_id, nvl(o.schedule_payment_shift, 0) neg_pay_shift, s.prepay_switch,
                       min(s.id) first_pay_month, min(s.payment_term_id) first_pay_term_id
                  FROM ls_ilr_schedule_stg s
                  join ls_ilr_options o on s.ilr_id = o.ilr_id and s.revision = o.revision
                 WHERE s.payment_month <> 0
                   group by s.ilr_id, s.revision, s.set_of_books_id, nvl(o.schedule_payment_shift, 0), s.prepay_switch
                    ) p
               join ls_ilr_payment_term_stg t on t.ilr_id = p.ilr_id
                                             and t.revision = t.revision
                                             and t.set_of_books_id = p.set_of_books_id
             )  mid_period_pay_month
      WHERE stg.ilr_id = mid_period_pay_month.ilr_id
      AND stg.revision = mid_period_pay_month.revision
      AND stg.set_of_books_id = mid_period_pay_month.set_of_books_id
      and stg.payment_term_id = mid_period_pay_month.payment_term_id
      MODEL partition by(stg.ilr_id, stg.revision, stg.set_of_books_id)
      DIMENSION by(ROW_NUMBER() OVER(partition by stg.ilr_id, stg.revision, stg.set_of_books_id order by month) as ID)
      MEASURES(AMOUNT,
            0 NPV,
            RATE,
            month,
            PREPAY_SWITCH,
            RESIDUAL_AMOUNT,
            BPO_PRICE,
            TERM_PENALTY,
            PAYMENT_TERM_TYPE_ID,
            INTEREST_CONVENTION_ID,
            FIRST_PAY_MONTH,
            0 AS FUTURE_NPV,
            0 AS MID_PERIOD_NPV,
            0 AS MONTHLY_INTEREST_RATE,
            MONTHS_TO_ACCRUE,
            FIRST_VALUE(STG.PARTIAL_MONTH_PERCENT) OVER (ORDER BY STG.ILR_ID, STG.REVISION, STG.SET_OF_BOOKS_ID) AS FIRST_PARTIAL_MONTH_PCT,
            LAST_VALUE(STG.PARTIAL_MONTH_PERCENT) OVER (ORDER BY STG.ILR_ID, STG.REVISION, STG.SET_OF_BOOKS_ID) AS LAST_PARTIAL_MONTH_PCT,
            LAST_VALUE(STG.ID) OVER () MAX_ID
            )
      RULES(NPV [ any ] = case
            when PAYMENT_TERM_TYPE_ID[CV(ID)] = 3 then
              (AMOUNT [ CV(ID) ] + RESIDUAL_AMOUNT [ CV(ID) ] + BPO_PRICE [
              CV(ID) ] + TERM_PENALTY [ CV(ID) ] )
            else
              (AMOUNT [ CV(ID) ] + RESIDUAL_AMOUNT [ CV(ID) ] + BPO_PRICE [
              CV(ID) ] + TERM_PENALTY [ CV(ID) ] ) /
              POWER(1 + RATE [ CV(ID) ], CV(ID) - PREPAY_SWITCH [ CV(ID) ]
                                          - CASE WHEN INTEREST_CONVENTION_ID[CV(ID)] = 1
                                                  AND MONTHS_TO_ACCRUE[CV(ID)] <> 1
                                                  AND ((PREPAY_SWITCH[CV(ID)] = 1 AND FIRST_PAY_MONTH[CV(ID)] <> 1)
                                                      OR
                                                      (PREPAY_SWITCH[CV(ID)] = 0 AND FIRST_PAY_MONTH[CV(ID)] <> MONTHS_TO_ACCRUE[1])
                                                      )
                                            THEN
                                              (FIRST_PAY_MONTH[CV(ID)] - PREPAY_SWITCH[CV(ID)])
                                            ELSE
                                              0
                                            END
                                          + CASE WHEN PAYMENT_TERM_TYPE_ID[CV(ID)] = 4
                                                  AND CV(ID) < MAX_ID[CV(ID)]
                                            THEN
                                              FIRST_PARTIAL_MONTH_PCT[CV(ID)] - 1
                                            WHEN PAYMENT_TERM_TYPE_ID[CV(ID)] = 4
                                                  AND CV(ID) = MAX_ID[CV(ID)]
                                            THEN
                                              FIRST_PARTIAL_MONTH_PCT[CV(ID)] + LAST_PARTIAL_MONTH_PCT[CV(ID)] - 2
                                            ELSE
                                              0
                                            END
                                          + CASE WHEN PAYMENT_TERM_TYPE_ID[CV(ID)] = 4
                                                  AND CV(ID) = 1
                                                  AND PREPAY_SWITCH[CV(ID)] = 1
                                            THEN
                                              LAST_PARTIAL_MONTH_PCT[CV(ID)]
                                            ELSE
                                              0
                                            END
                    )
            END,

            FUTURE_NPV[ANY] = Sum(NPV) OVER() - AMOUNT[CV(ID)],

            MONTHLY_INTEREST_RATE[ANY] = (Power(1 + RATE[CV(ID)], MONTHS_TO_ACCRUE[CV(ID)]) - 1) / MONTHS_TO_ACCRUE[CV(ID)],

            MID_PERIOD_NPV[ANY] = Decode(CV(ID),
                                          FIRST_PAY_MONTH[CV(ID)],
                                          (AMOUNT[CV(ID)] - ((FIRST_PAY_MONTH[CV(ID)] - PREPAY_SWITCH[CV(ID)]) * FUTURE_NPV[CV(ID)] * MONTHLY_INTEREST_RATE[CV(ID)]))
                                          / (1 + ((FIRST_PAY_MONTH[CV(ID)] - PREPAY_SWITCH[CV(ID)]) * MONTHLY_INTEREST_RATE[CV(ID)])),
                                          0
                                          ),

            NPV[ANY] = CASE WHEN INTEREST_CONVENTION_ID[CV(ID)] = 1
                            AND CV(ID) = FIRST_PAY_MONTH[CV(ID)]
                            AND MONTHS_TO_ACCRUE[CV(ID)] <> 1
                            AND ((PREPAY_SWITCH[CV(ID)] = 1 AND FIRST_PAY_MONTH[CV(ID)] <> 1)
                                OR
                                (PREPAY_SWITCH[CV(ID)] = 0 AND FIRST_PAY_MONTH[CV(ID)] <> MONTHS_TO_ACCRUE[CV(ID)])
                                )
                      THEN
                        MID_PERIOD_NPV[CV(ID)]
                      ELSE
                        NPV[CV(ID)]
                      END
        );

    forall i in indices of L_NPV_REC_TABLE
      update LS_ILR_SCHEDULE_STG LISS
      set LISS.NPV = L_NPV_REC_TABLE(i).NPV
      where LISS.ILR_ID = L_NPV_REC_TABLE(i).ILR_ID
      and LISS.MONTH = L_NPV_REC_TABLE(i).MONTH
      and LISS.REVISION = L_NPV_REC_TABLE(i).REVISION
      and LISS.SET_OF_BOOKS_ID = L_NPV_REC_TABLE(i).SET_OF_BOOKS_ID
      and LISS.PROCESS_NPV = 1;

      L_STATUS := 'Updating NBV for ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      update LS_ILR_STG S
         set NPV =
              (select ROUND(sum(B.NPV), 2)
                 from LS_ILR_SCHEDULE_STG B
                where B.ILR_ID = S.ILR_ID
                  and B.REVISION = S.REVISION
                  and B.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID)
       where PROCESS_NPV = 1;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_NET_PRESENT_VALUE;

   --***************************************************************************
      -- **                            F_SET_IFRS_REMEASURE
    -- ** Runs statement consulting IFRS Escalation Setup and Escalation Assignment to set remeasurement and ifrs_escalation flags on ls_ilr_stg
    -- ***********************************************
   function F_SET_IFRS_REMEASURE return varchar2 is
    L_MSG    varchar2(2000);
    L_STATUS VARCHAR2(2000);
   begin
   	 -- Set NPV Start date to Null where were doing an IFRS Remeasurement and Set of Books isnt included
	 -- Also set Include Escalation in Payment Flag based on Set of Books set up to Escalation (set to 0 for non-mapped, at this point all should = 1 at this point)
	MERGE INTO ls_ilr_stg s
	USING (SELECT distinct t.ilr_id,
				  t.revision,
				  m.set_of_books_id,
				  m.include_in_esc
			 FROM ls_ilr_payment_term t,
				  ls_vp_escalation_sob_map m,
				  ls_ilr_options o,
				  ls_ilr i,
				  ls_ilr_stg stg,
				  (SELECT control_value,
						  company_id
					 FROM pp_system_control_companies
					WHERE upper(control_name) = 'IFRS SET OF BOOKS REMEASUREMENT') sys
			WHERE t.ilr_id = o.ilr_id
			  AND t.revision = o.revision
			  AND t.ilr_id = i.ilr_id
			  AND t.escalation = m.variable_payment_id
			  AND stg.ilr_id = i.ilr_id
			  AND stg.revision = t.revision
			  AND stg.set_of_books_id = m.set_of_books_id
			  AND i.company_id = sys.company_id
			  AND lower(sys.control_value) = 'yes'
			  AND nvl(o.remeasurement_date, to_date('011800', 'mmyyyy')) < --Try to filter out ILRs that have an IFRS escalation only in the early terms and not in the later (remeasurement date onward)
				  add_months(t.payment_term_date, decode(t.payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1) * t.number_of_terms)) b
	ON (s.ilr_id = b.ilr_id AND s.revision = b.revision AND s.set_of_books_id = b.set_of_books_id)
	WHEN MATCHED THEN
	  UPDATE
		 SET s.npv_start_date  = decode(s.is_remeasurement, 1, decode(b.include_in_esc, 0, NULL, s.npv_start_date), s.npv_start_date),
			 s.include_esc_in_pay = b.include_in_esc,
			 s.is_remeasurement  = decode(s.is_remeasurement, 1, b.include_in_esc, s.is_remeasurement);


    RETURN 'OK';

    exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;

	end f_set_ifrs_remeasure;

   --***************************************************************************
      -- **                            F_LOAD_ILR_IDC_INC_STG
    -- ** IDC - Always get's spread across length of schedule
    -- **
    -- ** Incentive - Incentives occurring before commencement of lease are treated like IDC (except negative in calcs) and spread across length of schedules
    -- **     -Incentives occuring after commencement are grouped into the most recent (prior) payment and subtracted out of the total Payment Amount
    -- ***********************************************
   function F_LOAD_ILR_IDC_INC_STG return varchar2 is
    L_MSG    varchar2(2000);
    L_STATUS VARCHAR2(2000);
   begin
    l_msg := 'Begin IDC and Incentive Logic';
    pkg_pp_log.p_write_message(l_msg);

	l_status := 'Clear IDC and Incentive Staging';
    pkg_pp_log.p_write_message(l_status);

	delete from ls_ilr_idc_stg;
	delete from ls_ilr_inc_stg;

     /* ********************************************
      **  Begin IDC and Incentive Logic
      **
      **  IDC - Only get's spread for Cap, not OM
      **
      **  Incentive - Incentives occurring before commencement of lease are treated like IDC (except negative in calcs) and spread across length of schedules
      **      -Incentives occuring after commencement are grouped into the most recent (prior) payment and subtracted out of the total Payment Amount
      ***********************************************/
      --Spread IDC across the Payments
      L_STATUS := 'Insert IDC information to LS_ILR_IDC_STG';
      INSERT INTO LS_ILR_IDC_STG
            (ilr_id,
            revision,
            date_incurred,
            initial_direct_cost)
      SELECT stg.ilr_id,
        stg.revision,
        stg.min_month,
        idc.idc_sum
      FROM   ( SELECT s.ilr_id,
              s.revision,
              Min( month ) min_month
          FROM   LS_ILR_SCHEDULE_STG s
          GROUP  BY s.ilr_id,s.revision ) stg,
        ( SELECT c.ilr_id,
              c.revision,
              SUM( c.amount ) idc_sum
          FROM   LS_ILR_INITIAL_DIRECT_COST c
          GROUP  BY c.ilr_id,c.revision ) idc
      WHERE  stg.ilr_id = idc.ilr_id AND
        stg.revision = idc.revision;

      --Set math amounts used by calc to be applied to ROU
      l_status := 'Setting calculation amounts for IDC';
      merge INTO ls_ilr_schedule_stg a USING
      (SELECT ilr_id, revision,date_incurred, initial_direct_cost FROM ls_ilr_idc_stg) b
      ON (a.ilr_id = b.ilr_id AND a.revision = b.revision AND a.MONTH = b.date_incurred)
      WHEN matched THEN
      UPDATE SET a.idc_math_amount = b.initial_direct_cost;

      L_STATUS := 'Update ls_ilr_schedule_stg with Initial Direct Costs for Non-OM';
     merge INTO LS_ILR_SCHEDULE_STG a
      USING ( SELECT z.ilr_id,
            z.revision,
			(z.initial_direct_cost - accrued.accrued_idc )/num_periods idc_amount,
            accrued.set_of_books_id,
             accrued.switch_to_cap,
             accrued.remeasurement_date
        FROM (SELECT c.ilr_id,
                  c.revision,
                  c.initial_direct_cost,
             s.num_periods,
             s.set_of_books_id
              FROM ls_ilr_idc_stg c,
          ( SELECT ilr_id,
                      revision,
                      set_of_books_id,
                      Count( month ) num_periods
                  FROM   LS_ILR_SCHEDULE_STG
                  GROUP  BY ilr_id,revision,set_of_books_id ) s
                   WHERE  c.ilr_id = s.ilr_id AND
                  c.revision = s.revision ) z,
                  v_ls_ilr_idc_inc_accrued_fx_vw accrued
       WHERE z.ilr_id = accrued.ilr_id(+)
         AND z.revision = accrued.revision(+)
       AND z.set_of_books_id = accrued.set_of_books_id(+)) b
      ON (b.ilr_id = a.ilr_id AND b.revision = a.revision AND b.set_of_books_id = a.set_of_books_id and a.is_om = 0 and a.fasb_cap_type_id not in (3,6)
	     and not exists (select 1 from v_ls_ilr_idc_inc_accrued_fx_vw vw 
                      where vw.ilr_id = a.ilr_id 
                      and vw.set_of_books_id = a.set_of_books_id 
                      and vw.revision = a.revision
                      and b.switch_to_cap = 1
                      and b.remeasurement_date is not null
                      and sign(a.revision) = 1)) ---Don't spread for off-to-on remeasurements unless its a forecast           
      WHEN matched THEN
      UPDATE SET a.initial_direct_cost = b.idc_amount;

      L_STATUS := 'Update ls_ilr_schedule_stg with Initial Direct Costs for OM';
      merge INTO LS_ILR_SCHEDULE_STG a
      USING ( select s.ilr_id,
                     s.revision,
                     s.date_incurred,
                     s.initial_direct_cost,
                i.est_in_svc_date
                from LS_ILR_IDC_STG s,
                LS_ILR i
                where s.ilr_id = i.ilr_id) b
        ON (b.ilr_id = a.ilr_id AND b.revision = a.revision and a.month = b.est_in_svc_date and (a.is_om = 1 or a.fasb_cap_type_id in (3,6)))
      WHEN matched THEN
      UPDATE SET a.initial_direct_cost = b.initial_direct_cost;

      L_STATUS := 'Insert Incentive information to LS_ILR_INC_STG (Spread)';
      INSERT INTO ls_ilr_inc_stg (ilr_id, revision, date_incurred, incentive_amount, is_at_commencement)
      SELECT c.ilr_id,
          c.revision,
          Nvl(s.npv_start_date, min_pay_term),
          SUM( c.amount ) incentive_sum,
          1
      FROM   LS_ILR_INCENTIVE c,
          ( SELECT stg.ilr_id,
              stg.revision,
              Min( payment_term_date ) min_pay_term,
              stg.npv_start_date
          FROM   LS_ILR_STG stg, ls_ilr_payment_term term
          WHERE stg.ilr_id = term.ilr_id
          AND stg.revision = term.revision
          GROUP  BY stg.ilr_id, stg.revision, stg.npv_start_date ) s
      WHERE  c.ilr_id = s.ilr_id AND
          c.revision = s.revision AND
          trunc(c.date_incurred, 'fmmonth') <= trunc(s.min_pay_term, 'fmmonth')
      GROUP  BY c.ilr_id,c.revision, Nvl(s.npv_start_date, min_pay_term);

      --Spread Incentives prior or on commencement in the first month on schedule
      L_STATUS := 'Update ls_ilr_schedule_stg with Incentives (Spread)';
      merge INTO LS_ILR_SCHEDULE_STG a
      USING (      SELECT c.ilr_id,
            c.revision,
            ( c.incentive_amount - nvl(accrued.accrued_incentive,0) ) / n.num_periods incentive_amount,
            n.set_of_books_id
          FROM   ls_ilr_INC_STG c,
            ( SELECT ilr_id,
                  revision,
                  set_of_books_id,
                  Count( month ) num_periods
              FROM   ls_ILR_SCHEDULE_STG
              GROUP  BY ilr_id,revision,set_of_books_id ) n,
            ( SELECT * FROM v_ls_ilr_idc_inc_accrued_fx_vw
            WHERE switch_to_cap = 1
            OR remeasurement_date IS NOT null) accrued --Pull accrued if we're switching cap and/or it's a remeasurement
          WHERE  c.ilr_id = n.ilr_id AND
            c.revision = n.revision AND
            n.ilr_id = accrued.ilr_id (+) AND
            n.set_of_books_id = accrued.set_of_books_id (+)) b
      ON (b.ilr_id = a.ilr_id AND b.revision = a.revision AND b.set_of_books_id = a.set_of_books_id)
      WHEN matched THEN
      UPDATE SET a.incentive_amount = b.incentive_amount;

      L_STATUS := 'Insert Incentive information to LS_ILR_INC_STG (On Payment Periods) [Annual/Semi-Annual/Quarterly]'; -- No need to check anything that might have accrued since these expense on their payment period
      INSERT INTO ls_ilr_inc_stg (ilr_id, revision, date_incurred, incentive_amount, is_at_commencement)
          SELECT c.ilr_id,
             c.revision,
             stg.month,
             Sum(c.amount),
             0
        FROM   LS_ILR_INCENTIVE c,
             ( SELECT DISTINCT s.ilr_id,
                     s.revision,
                     month,
                     months_to_accrue,
                     prepay_switch
             FROM   LS_ILR_SCHEDULE_STG s
             WHERE  s.payment_month = 1 ) stg,
               ( SELECT stg.ilr_id,
                        stg.revision,
                        Min( payment_term_date ) min_pay_term,
                        stg.npv_start_date
                    FROM   LS_ILR_STG stg, ls_ilr_payment_term term
                    WHERE stg.ilr_id = term.ilr_id
                    AND stg.revision = term.revision
                    GROUP  BY stg.ilr_id, stg.revision, stg.npv_start_date ) m
        WHERE  c.ilr_id = stg.ilr_id AND
             c.revision = stg.revision AND
             stg.months_to_accrue in (12, 3, 6) and
             trunc(c.date_incurred, 'fmmonth') >= add_months(stg.MONTH, (stg.prepay_switch - 1) * (months_to_accrue - 1)) and
             trunc(c.date_incurred, 'fmmonth') <= Add_Months(stg.MONTH, stg.prepay_switch * (months_to_accrue - 1))  and
               c.ilr_id = m.ilr_id AND
               c.revision = m.revision AND
              trunc(c.date_incurred, 'fmmonth') > trunc(m.min_pay_term, 'fmmonth')
         GROUP BY c.ilr_id, c.revision, stg.month;

      L_STATUS := 'Insert Incentive information to LS_ILR_INC_STG (On Payment Periods) [Monthly]'; -- No need to check anything that might have accrued since these expense on their payment period
      INSERT INTO ls_ilr_inc_stg (ilr_id, revision, date_incurred, incentive_amount, is_at_commencement)
          SELECT c.ilr_id,
               c.revision,
               stg.month,
               Sum(c.amount),
               0
          FROM   LS_ILR_INCENTIVE c,
               ( SELECT DISTINCT s.ilr_id,
                       s.revision,
                       month,
                       months_to_accrue
               FROM   LS_ILR_SCHEDULE_STG s
               WHERE  s.payment_month = 1 ) stg,
               ( SELECT stg.ilr_id,
                        stg.revision,
                        Min( payment_term_date ) min_pay_term,
                        stg.npv_start_date
                    FROM   LS_ILR_STG stg, ls_ilr_payment_term term
                    WHERE stg.ilr_id = term.ilr_id
                    AND stg.revision = term.revision
                    GROUP  BY stg.ilr_id, stg.revision, stg.npv_start_date ) m
          WHERE  c.ilr_id = stg.ilr_id AND
               c.revision = stg.revision AND
               stg.months_to_accrue = 1 and
               trunc(c.date_incurred, 'fmmonth') = trunc(stg.MONTH, 'fmmonth') and
               c.ilr_id = m.ilr_id AND
               c.revision = m.revision AND
               trunc(c.date_incurred, 'fmmonth') > trunc(m.min_pay_term, 'fmmonth')
         GROUP BY c.ilr_id, c.revision, stg.month
              ;
      --Fill in rest of Incentives based on Month they occur in
      L_STATUS := 'Update ls_ilr_schedule_stg with Incentives (On Payment Periods)';
      merge INTO LS_ILR_SCHEDULE_STG a
      USING ( SELECT s.ilr_id,
             s.revision,
             SUM( incentive_amount ) incentive_amount,
             trunc(s.date_incurred,'fmmonth') date_incurred
        FROM   LS_ILR_INC_STG s
        WHERE  ( ilr_id, revision, date_incurred ) NOT IN (   SELECT stg.ilr_id,
                                       stg.revision,
                                       Min( payment_term_date )
                                  FROM   ls_ilr_stg stg, ls_ilr_payment_term t
                                    WHERE stg.ilr_id = t.ilr_id
                                    AND stg.revision = t.revision
                                  GROUP  BY stg.ilr_id,stg.revision)
        and s.is_at_commencement = 0
        GROUP  BY s.ilr_id,s.revision,s.date_incurred ) b
      ON (b.ilr_id = a.ilr_id AND b.revision = a.revision AND b.date_incurred = a.month)
      WHEN matched THEN
      UPDATE SET a.incentive_amount = Nvl(a.incentive_amount, 0)
                      + b.incentive_amount,
            a.amount = Nvl( a.amount, 0 ) - Nvl( b.incentive_amount, 0 );

      l_status := 'Setting calculation amounts for Incentive';
      merge INTO ls_ilr_schedule_stg a USING
      (SELECT ilr_id, revision,date_incurred, sum(incentive_amount) incentive_amount  FROM ls_ilr_inc_stg where is_at_commencement = 0 group by ilr_id, revision,date_incurred) b
      ON (a.ilr_id = b.ilr_id AND a.revision = b.revision AND a.MONTH = b.date_incurred)
      WHEN matched THEN
      UPDATE SET a.incentive_math_amount = b.incentive_amount;

      -- plug the Initial Direct Cost and Incentive amount in the last month with any rounding differences
      -- if the sum of the amounts do not equal the total
      l_status := 'Plug initial direct cost in the last month to account for rounding issues';
      merge into ls_ilr_schedule_stg a
      using (
                 select s.ilr_id,
                        s.revision,
                        s.schedule_idc - i.initial_direct_cost plug_amount,
                        s.set_of_books_id,
                        m.max_month
                   from (
                           select ilr_id,
                                  revision,
                                  set_of_books_id,
                                  sum(initial_direct_cost) schedule_idc
                             from ls_ilr_schedule_stg
                            group by ilr_id, revision, set_of_books_id) s,
                        (
                           select ilr_id, revision, max(month) max_month
                             from ls_ilr_schedule_stg
                            group by ilr_id, revision
                        ) m,
                        (select idc.ilr_id,
                                idc.revision,
                                acc.set_of_books_id,
                                 Sum(idc.initial_direct_cost) - acc.accrued_idc initial_direct_cost
                           from ls_ilr_idc_stg idc,
                           v_ls_ilr_idc_inc_accrued_fx_vw acc
                           WHERE idc.ilr_id = acc.ilr_id
                           AND idc.revision = acc.revision
                          group by idc.ilr_id, idc.revision, acc.set_of_books_id,acc.accrued_idc ) i
                where s.ilr_id = i.ilr_id
                  and s.revision = i.revision
              and s.set_of_books_id = i.set_of_books_id
                  and s.ilr_id = m.ilr_id
                  and s.revision = m.revision
            ) b
      on (a.ilr_id = b.ilr_id and a.revision = b.revision and a.month = b.max_month and a.set_of_books_id = b.set_of_books_id and a.is_om = 0) -- Only need to Plug if we're on a Capital lease since IDC isn't Spread for OM
      when matched then
        update set a.initial_direct_cost = nvl(a.initial_direct_cost,0) - b.plug_amount;

     l_status := 'Plug incentive amount in the last month to account for rounding issues';
      merge into ls_ilr_schedule_stg a
      using (
                 select s.ilr_id,
                        s.revision,
                        s.schedule_inc - i.incentive_amount plug_amount,
                        s.set_of_books_id,
                        m.max_month
                   from (
                           select ilr_id,
                                  revision,
                                  set_of_books_id,
                                  sum(incentive_amount) schedule_inc
                             from ls_ilr_schedule_stg
                            group by ilr_id, revision, set_of_books_id) s,
                        (
                           select ilr_id, revision, max(month) max_month
                             from ls_ilr_schedule_stg
                            group by ilr_id, revision
                        ) m,
                        ( select inc.ilr_id,
                                inc.revision,
                                sum(inc.incentive_amount) - acc.accrued_incentive incentive_amount,
                                acc.set_of_books_id
                           from LS_ILR_INC_STG inc,
                           v_ls_ilr_idc_inc_accrued_fx_Vw acc
                           WHERE inc.ilr_id = acc.ilr_id
                           AND inc.revision = acc.revision
                          group by inc.ilr_id, inc.revision, acc.set_of_books_id, acc.accrued_incentive) i
                where s.ilr_id = i.ilr_id
                  and s.revision = i.revision
              and s.set_of_books_id = i.set_of_books_id
                  and s.ilr_id = m.ilr_id
                  and s.revision = m.revision
            ) b
      on (a.ilr_id = b.ilr_id and a.revision = b.revision and a.month = b.max_month and a.set_of_books_id = b.set_of_books_id)
      when matched then
        update set a.incentive_amount = nvl(a.incentive_amount,0) - b.plug_amount;

      --If remeasurement resum IDC_math_amount and incentive at npv_start_date
      l_status := 'Catch up Accrual of IDC and Incentive';
      merge INTO ls_ilr_schedule_stg a USING
      (
       SELECT s.ilr_id,

                       Nvl(s.npv_start_date, min_date.min_pay_date) start_date,
                      s.set_of_books_id,
                      s.revision,
                      Sum(initial_direct_cost) idc_math_amount,
                      Sum(stg.incentive_amount) - nvl(inc.incentive_amount,0) incentive_math_amount,
					  o.remeasurement_date
                  FROM   ls_ILR_STG s,
                      (SELECT * FROM v_ls_ilr_idc_inc_accrued_fx_vw
                      ) o,
                      ls_ilr_schedule_stg stg,
                      (SELECT incent.ilr_id, incent.revision, s.set_of_books_id, Sum(incentive_amount) incentive_amount
                        FROM ls_ilr_inc_stg incent, ls_ilr_stg s
                        WHERE incent.is_at_commencement = 0
                        AND incent.ilr_id = s.ilr_id
                        AND incent.revision = s.revision
                        GROUP BY incent.ilr_id, incent.revision, s.set_of_books_id) inc,
            (SELECT Min(payment_term_date) min_pay_date, ps.ilr_id, ps.revision FROM ls_ilr_payment_term p, ls_ilr_stg ps
                        WHERE ps.ilr_id = p.ilr_id
                        AND ps.revision = p.revision
                        GROUP BY ps.ilr_Id, ps.revision) min_date
                  WHERE  s.ilr_id = o.ilr_id AND
                      s.revision = o.revision AND
                      s.set_of_books_id = o.set_of_books_id AND
                      stg.ilr_id = s.ilr_id AND
                      stg.revision = s.revision AND
                      stg.set_of_books_id = s.set_of_books_id AND
                      inc.ilr_id (+)= s.ilr_id AND
                      inc.revision (+)= s.revision AND
                      inc.set_of_books_id (+)= s.set_of_books_id AND
                      min_date.ilr_id = stg.ilr_id AND
                      min_date.revision = stg.revision AND
                      (o.switch_to_cap = 1 or o.remeasurement_date is null)
                      GROUP BY s.ilr_id,
                      Nvl(s.npv_start_date, min_date.min_pay_date),
                      s.set_of_books_id,
                      s.revision,
                      inc.incentive_amount,
					  o.remeasurement_date) b ON (a.ilr_id = b.ilr_id AND a.revision = b.revision AND a.MONTH = b.start_date AND a.set_of_books_id = b.set_of_books_id)
      WHEN matched THEN
                UPDATE SET a.incentive_cap_amount = nvl(b.incentive_math_amount,0),
                 a.idc_math_amount = nvl(b.idc_math_amount,0);

	  --Using field to spread and determining math amount, but zero'ing out after, should remove from front end later
      update ls_ilr_schedule_stg set initial_direct_cost = 0;

	  --Remove math amount where it shouldn't impact capital cost
	  update ls_ilr_schedule_stg stg set idc_math_amount = 0 
	    where not exists 
		(select 1 from v_ls_ilr_idc_inc_accrued_fx_vw idc 
		   where stg.ilr_id = idc.ilr_id 
		   and stg.revision = idc.revision
		   and stg.set_of_books_id = idc.set_of_books_id
		   and (idc.switch_to_cap = 1 or idc.remeasurement_date is null));

      RETURN 'OK';

    exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;

   end F_LOAD_ILR_IDC_INC_STG;

   --**************************************************************************
   --                            F_LOAD_ILR_STG
   -- Loads the ILR_STG table for a single ILR ID
   --**************************************************************************
   function F_LOAD_ILR_STG(A_ILR_ID   number,
                           A_REVISION number) return varchar2 is
      L_MSG    varchar2(2000);
      L_SQLS   varchar2(2000);
      L_IS_CAP number(1, 0);
    L_IS_CAP2 number(1, 0);
   begin
      for L_SOBS in (select L.ILR_ID as ILR_ID,
                            LO.REVISION as REVISION,
                            LL.PRE_PAYMENT_SW as PREPAY_SWITCH,
                            CASE
                              WHEN LLCT.RATE_CONVENTION_ID = 2 THEN
                                --Effective Period Interest: (1+Discount Rate)^(1/(days in year/30))-1
                                Power(1 + ((LO.INCEPTION_AIR/100) / decode(PT.payment_freq_id, 1, 1, 2, 2, 3, 4, 12)),
                                      decode(PT.payment_freq_id, 1, 1, 2, 2, 3, 4, 12) / (nvl(LL.DAYS_IN_YEAR, 360)/30)) - 1
                              ELSE
                                --rate_convention_id = 1
                                --Simple Rate: Discount Rate / (days in year/30)
                                (LO.INCEPTION_AIR/100) / (nvl(LL.DAYS_IN_YEAR, 360)/30)
                            END AS DISCOUNT_RATE,
                            sum(NVL(LA.FMV, 0)) as FMV,
                            sum(
                            case when la.estimated_residual = 0 and lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Amort to Guaranteed Residual', la.COMPANY_ID))) = 'yes' then
                              NVL(LA.GUARANTEED_RESIDUAL_AMOUNT, 0)
                            else
                              /* CJS 3/7/17 Add system control to account for capitalization of estimated residual */
                              Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', la.COMPANY_ID))), 'no', 0, round(la.ESTIMATED_RESIDUAL * la.FMV, 2))
                            end
                            ) as RESIDUAL_AMOUNT,
                            NVL(LO.PURCHASE_OPTION_AMT, 0) as BPO_PRICE,
                            NVL(LO.TERMINATION_AMT, 0) as TERM_PENALTY,
                            C.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                            FASB.BOOK_SUMMARY_ID as BOOK_SUMMARY_ID,
                            L.LEASE_ID,
                            LO.DEFERRED_RENT_SW,
                            Nvl(FASB.CONSIDER_FMV, 1) AS CONSIDER_FMV,
                            LLCT.INTEREST_CONVENTION_ID,
                            FASB.FASB_CAP_TYPE_ID AS FASB_CAP_TYPE_ID
                       from LS_ILR               L,
                            LS_LEASE             LL,
                            LS_ASSET             LA,
                            LS_ILR_OPTIONS       LO,
                            COMPANY_SET_OF_BOOKS C,
                            LS_LEASE_CAP_TYPE    LLCT,
                            LS_ILR_ASSET_MAP M,
                            LS_FASB_CAP_TYPE_SOB_MAP FASB,
                            (select distinct ilr_id, revision, first_value(payment_freq_id) over (order by payment_term_date) payment_freq_id
                               from ls_ilr_payment_term
                              where ilr_id = A_ILR_ID
                                and revision = A_REVISION) PT
                      where L.LEASE_ID = LL.LEASE_ID
                        and M.LS_ASSET_ID = LA.LS_ASSET_ID
                        and M.ILR_ID = A_ILR_ID
                        and M.REVISION = A_REVISION
                        and L.ILR_ID = LA.ILR_ID(+)
                        and LA.LS_ASSET_STATUS_ID <> 4
                        and L.ILR_ID = A_ILR_ID
                        and LO.ILR_ID = L.ILR_ID
                        and LO.REVISION = A_REVISION
                        and L.COMPANY_ID = C.COMPANY_ID
                        and LO.LEASE_CAP_TYPE_ID = LLCT.LS_LEASE_CAP_TYPE_ID
                        and LLCT.LS_LEASE_CAP_TYPE_ID = FASB.LEASE_CAP_TYPE_ID
                        and FASB.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
                        and LO.ILR_ID = PT.ILR_ID
                        and LO.REVISION = PT.REVISION
                        and (A_REVISION > 0
                             or
                             c.set_of_books_id in (
                                 select decode(set_of_books_id, -1, c.set_of_books_id, set_of_books_id)
                                   from ls_forecast_version where revision = A_REVISION)
                             )
                      group by L.ILR_ID,
                               LL.PRE_PAYMENT_SW,
                               LO.PURCHASE_OPTION_AMT,
                               LO.TERMINATION_AMT,
                               LO.REVISION,
                               C.SET_OF_BOOKS_ID,
                               FASB.BOOK_SUMMARY_ID,
                               L.LEASE_ID,
                               LO.DEFERRED_RENT_SW,
                               FASB.CONSIDER_FMV,
                               CASE
                                WHEN LLCT.RATE_CONVENTION_ID = 2 THEN
                                  --Effective Period Interest: (1+Discount Rate)^(1/(days in year/30))-1
                                  Power(1 + ((LO.INCEPTION_AIR/100) / decode(PT.payment_freq_id, 1, 1, 2, 2, 3, 4, 12)),
                                      decode(PT.payment_freq_id, 1, 1, 2, 2, 3, 4, 12) / (nvl(LL.DAYS_IN_YEAR, 360)/30)) - 1
                                ELSE
                                  --rate_convention_id = 1
                                  --Simple Rate: Discount Rate / (days in year/30)
                                  (LO.INCEPTION_AIR/100) / (nvl(LL.DAYS_IN_YEAR, 360)/30)
                               END,
                               LLCT.INTEREST_CONVENTION_ID,
                               FASB.FASB_CAP_TYPE_ID)
      loop
         L_MSG := 'PROCESSING set_of_books: ' || L_SOBS.SET_OF_BOOKS_ID;

     L_IS_CAP := CASE WHEN L_SOBS.FASB_CAP_TYPE_ID IN (1,2,3,6) THEN 1 ELSE 0 END;

     select decode(l.lease_type_id, 3, 1, L_IS_CAP)
     into L_IS_CAP2
     from ls_lease l
     where l.lease_id = L_SOBS.LEASE_ID
     ;

         insert into LS_ILR_STG
            (ILR_ID, REVISION, PREPAY_SWITCH, DISCOUNT_RATE, FMV, RESIDUAL_AMOUNT, PROCESS_NPV,
             BPO_PRICE, TERM_PENALTY, SET_OF_BOOKS_ID, IS_OM, DEFERRED_RENT_SW, CONSIDER_FMV, INTEREST_CONVENTION_ID, INCLUDE_ESC_IN_PAY)
         values
            (L_SOBS.ILR_ID, L_SOBS.REVISION, L_SOBS.PREPAY_SWITCH, L_IS_CAP2 * nvl(L_SOBS.DISCOUNT_RATE,0),
             L_SOBS.FMV, L_SOBS.RESIDUAL_AMOUNT, 1, L_SOBS.BPO_PRICE, L_SOBS.TERM_PENALTY,
             L_SOBS.SET_OF_BOOKS_ID, 1 - L_IS_CAP, L_SOBS.DEFERRED_RENT_SW, L_SOBS.CONSIDER_FMV, L_SOBS.INTEREST_CONVENTION_ID, 1);

         -- If this is a remeasurement, only calculate from the effective remeasurement date
         UPDATE ls_ilr_stg stg
         SET (NPV_START_DATE, IS_REMEASUREMENT) = (
          SELECT Trunc(remeasurement_date, 'month'), 1
          FROM ls_ilr_options o
          WHERE stg.ilr_id = o.ilr_id
          AND stg.revision = o.revision
          AND o.ilr_id = L_SOBS.ILR_ID
          AND o.revision = L_SOBS.REVISION
         )
         WHERE stg.ilr_id = L_SOBS.ILR_ID
         AND stg.revision = L_SOBS.REVISION
         AND EXISTS (SELECT 1
                     FROM ls_ilr_options o2
                     WHERE o2.ilr_id = L_SOBS.ILR_ID
                     AND o2.revision = L_SOBS.REVISION
                     AND o2.remeasurement_date IS NOT NULL
                     )
         -- Can only remeasure if there's another revision that's approved
         AND EXISTS (SELECT 1
                     FROM ls_ilr_approval a
                     WHERE a.ilr_id = L_SOBS.ILR_ID
                     AND a.revision <> L_SOBS.REVISION
                     AND a.approval_status_id IN (3,6)
                     );

      end loop;

	 -- Set NPV Start date to Null where were doing an IFRS Remeasurement and Set of Books isnt included
	 -- Also set Include Escalation in Payment Flag based on Set of Books set up to Escalation (set to 0 for non-mapped, at this point all should = 1 at this point)
	  l_msg := f_set_ifrs_remeasure;

	  if l_msg <> 'OK' then
		return l_msg;
	  end if;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR('ERROR loading ls_ilr_stg: ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_LOAD_ILR_STG;


   --**************************************************************************
   --                            F_LOAD_ILRS_STG
   -- Loads the ILR_STG table for a single LEASE ID
   --**************************************************************************
   function F_LOAD_ILRS_STG(A_LEASE_ID number) return varchar2 is
      L_MSG    varchar2(2000);
      L_SQLS   varchar2(2000);
      L_IS_CAP number(1, 0);
    L_IS_CAP2 number(1,0);

   begin
      for L_SOBS in (select L.ILR_ID as ILR_ID,
                            LO.REVISION as REVISION,
                            LL.PRE_PAYMENT_SW as PREPAY_SWITCH,
                            CASE
                              WHEN LLCT.RATE_CONVENTION_ID = 2 THEN
                                --Effective Period Interest: (1+Discount Rate)^(1/(days in year/30))-1
                                Power(1 + (LO.INCEPTION_AIR/100),1 / (nvl(LL.DAYS_IN_YEAR, 360)/30)) - 1
                              ELSE
                                --rate_convention_id = 1
                                --Simple Rate: Discount Rate / (days in year/30)
                                (LO.INCEPTION_AIR/100) / (nvl(LL.DAYS_IN_YEAR, 360)/30)
                            END AS DISCOUNT_RATE,
                            sum(NVL(LA.FMV, 0)) as FMV,
                            sum(
                            case when la.estimated_residual = 0 and lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Amort to Guaranteed Residual', la.COMPANY_ID))) = 'yes' then
                              NVL(LA.GUARANTEED_RESIDUAL_AMOUNT, 0)
                            ELSE
                              /* CJS 3/7/17 Add system control to account for capitalization of estimated residual */
                              Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', la.COMPANY_ID))), 'no', 0, round(la.ESTIMATED_RESIDUAL * la.FMV, 2))
                            end
                            ) as RESIDUAL_AMOUNT,
                            NVL(LO.PURCHASE_OPTION_AMT, 0) as BPO_PRICE,
                            NVL(LO.TERMINATION_AMT, 0) as TERM_PENALTY,
                            C.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                            FASB.BOOK_SUMMARY_ID as BOOK_SUMMARY_ID,
                            LO.DEFERRED_RENT_SW,
                            Nvl(FASB.CONSIDER_FMV, 1) AS CONSIDER_FMV,
                            LLCT.INTEREST_CONVENTION_ID,
                            LO.LEASE_CAP_TYPE_ID AS LEASE_CAP_TYPE_ID,
                            FASB.FASB_CAP_TYPE_ID AS FASB_CAP_TYPE_ID,
                            L.LEASE_ID
                       from LS_ILR               L,
                            LS_LEASE             LL,
                            LS_ASSET             LA,
                            LS_ILR_OPTIONS       LO,
                            COMPANY_SET_OF_BOOKS C,
                            LS_LEASE_CAP_TYPE    LLCT,
                            LS_FASB_CAP_TYPE_SOB_MAP FASB
                      where L.LEASE_ID = LL.LEASE_ID
                        and L.ILR_ID = LA.ILR_ID(+)
                        and LA.LS_ASSET_STATUS_ID <> 4
                        and LL.LEASE_ID = A_LEASE_ID
                        and LO.ILR_ID = L.ILR_ID
                        and LO.REVISION = L.CURRENT_REVISION
                        and L.COMPANY_ID = C.COMPANY_ID
                        and LO.LEASE_CAP_TYPE_ID = LLCT.LS_LEASE_CAP_TYPE_ID
                        and LLCT.LS_LEASE_CAP_TYPE_ID = FASB.LEASE_CAP_TYPE_ID
                        and FASB.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
                      group by L.ILR_ID,
                               LL.PRE_PAYMENT_SW,
                               LO.INCEPTION_AIR / (100 * (nvl(LL.DAYS_IN_YEAR, 360)/30 )), /* CJS */
                               LO.PURCHASE_OPTION_AMT,
                               LO.TERMINATION_AMT,
                               LO.REVISION,
                               C.SET_OF_BOOKS_ID,
                               FASB.BOOK_SUMMARY_ID,
                               LO.DEFERRED_RENT_SW,
                               FASB.CONSIDER_FMV,
                               CASE
                                WHEN LLCT.RATE_CONVENTION_ID = 2 THEN
                                  --Effective Period Interest: (1+Discount Rate)^(1/(days in year/30))-1
                                  Power(1 + (LO.INCEPTION_AIR/100),1 / (nvl(LL.DAYS_IN_YEAR, 360)/30)) - 1
                                ELSE
                                  --rate_convention_id = 1
                                  --Simple Rate: Discount Rate / (days in year/30)
                                  (LO.INCEPTION_AIR/100) / (nvl(LL.DAYS_IN_YEAR, 360)/30)
                               END,
                               LLCT.INTEREST_CONVENTION_ID,
                               LO.LEASE_CAP_TYPE_ID,
                               FASB.FASB_CAP_TYPE_ID,
                               L.LEASE_ID
)
      loop
         L_IS_CAP := CASE WHEN L_SOBS.FASB_CAP_TYPE_ID IN (1,2,3,6) THEN 1 ELSE 0 END;

     select decode(l.lease_type_id, 3, 1, L_IS_CAP)
     into L_IS_CAP2
     from ls_lease l
     where l.lease_id = L_SOBS.LEASE_ID
     ;

         insert into LS_ILR_STG
            (ILR_ID, REVISION, PREPAY_SWITCH, DISCOUNT_RATE, FMV, RESIDUAL_AMOUNT, PROCESS_NPV,
             BPO_PRICE, TERM_PENALTY, SET_OF_BOOKS_ID, IS_OM, DEFERRED_RENT_SW, CONSIDER_FMV, INTEREST_CONVENTION_ID)
         values
            (L_SOBS.ILR_ID, L_SOBS.REVISION, L_SOBS.PREPAY_SWITCH, L_IS_CAP2 * L_SOBS.DISCOUNT_RATE,
             L_SOBS.FMV, L_SOBS.RESIDUAL_AMOUNT, 1, L_SOBS.BPO_PRICE, L_SOBS.TERM_PENALTY,
             L_SOBS.SET_OF_BOOKS_ID, 1 - L_IS_CAP, L_SOBS.DEFERRED_RENT_SW, L_SOBS.CONSIDER_FMV, L_SOBS.INTEREST_CONVENTION_ID);

         -- If this is a remeasurement, only calcualte from the effective remeasurement date
         UPDATE ls_ilr_stg stg
         SET (NPV_START_DATE, IS_REMEASUREMENT) = (
          SELECT Trunc(remeasurement_date, 'month'), 1
          FROM ls_ilr_options o
          WHERE stg.ilr_id = o.ilr_id
          AND stg.revision = o.revision
          AND o.ilr_id = L_SOBS.ILR_ID
          AND o.revision = L_SOBS.REVISION
         )
         WHERE stg.ilr_id = L_SOBS.ILR_ID
         AND stg.revision = L_SOBS.REVISION
         AND EXISTS (SELECT 1
                     FROM ls_ilr_options o2
                     WHERE o2.ilr_id = L_SOBS.ILR_ID
                     AND o2.revision = L_SOBS.REVISION
                     AND o2.remeasurement_date IS NOT NULL
                     )
         -- Can only remeasure if there's another revision that's approved
         AND EXISTS (SELECT 1
                     FROM ls_ilr_approval a
                     WHERE a.ilr_id = L_SOBS.ILR_ID
                     AND a.revision <> L_SOBS.REVISION
                     AND a.approval_status_id IN (3,6)
                     );

      end loop;


	 -- Set NPV Start date to Null where were doing an IFRS Remeasurement and Set of Books isnt included
	 -- Also set Include Escalation in Payment Flag based on Set of Books set up to Escalation (set to 0 for non-mapped, at this point all should = 1 at this point)
	  l_msg := f_set_ifrs_remeasure;

	  if l_msg <> 'OK' then
		return l_msg;
	  end if;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR('ERROR loading ls_ilr_stg: ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_LOAD_ILRS_STG;

   --**************************************************************************
   --                            F_LOAD_ILR_SCHEDULE_STG
   -- Builds out the cash flow based on payment terms
   -- Take into consideration whether or not the payment is
   -- Paid at the beginning of the period or at the end of the period
   --**************************************************************************
   function F_LOAD_ILR_SCHEDULE_STG(A_MONTH in date:=null) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
      L_ESCALATIONS_EXIST NUMBER;
      L_SCHEDULE_PAYMENT_SHIFT_COUNT NUMBER;
      L_MONTH_CHECK NUMBER;
	  L_REMEASURE_COUNT NUMBER;
	  L_SPLIT_ESC_OUT_COUNT NUMBER;
	  L_IFRS_BUCKET NUMBER;
	  L_SQLS varchar2(4000);
   begin
      L_STATUS := 'Loading ls_ilr_schedule_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

      insert into LS_ILR_SCHEDULE_STG
         (ID, ILR_ID, REVISION, SET_OF_BOOKS_ID, month, AMOUNT, RESIDUAL_AMOUNT, RATE,
          PREPAY_SWITCH, PROCESS_NPV, PAYMENT_MONTH, MONTHS_TO_ACCRUE, BPO_PRICE, TERM_PENALTY,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, PAYMENT_TERM_TYPE_ID, DEFERRED_RENT_SW, INTEREST_CONVENTION_ID,
          PAYMENT_TERM_ID, ESCALATION, ESCALATION_PCT, ESCALATION_MONTH, MONTHS_TO_ESCALATE,
          PARTIAL_MONTH_PERCENT, FASB_CAP_TYPE_ID)
         with N as
          (
           -- create a "table" with 10000 rows
           select ROWNUM as THE_ROW from DUAL connect by level <= 10000),
         LP as
          (
           -- find the payment_term_id with the latest payment_term_date
           select L2.ILR_ID,
                   L2.PAYMENT_TERM_ID,
                   L2.REVISION,
                   L1.SET_OF_BOOKS_ID,
                   ROW_NUMBER() OVER(partition by L2.ILR_ID, L2.SET_OF_BOOKS_ID, L2.REVISION order by L2.PAYMENT_TERM_DATE desc) as THE_MAX
             from LS_ILR_PAYMENT_TERM_STG L2, LS_ILR_STG L1
            where L1.ILR_ID = L2.ILR_ID
              and L1.REVISION = L2.REVISION
              and L1.SET_OF_BOOKS_ID = L2.SET_OF_BOOKS_ID)
         select ROW_NUMBER() OVER(partition by P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)),
                P.ILR_ID,
                L.REVISION,
                L.SET_OF_BOOKS_ID,
                ADD_MONTHS(trunc( P.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1),
                case
                   -- Last Payment
                   when p.payment_term_type_id = 4
                        and N.THE_ROW = (P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) + 1)
                        and P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID
                        then
                        round((1 - ((DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)
                               - (to_number(to_char(p.payment_term_date - 1,'dd')) /30))
                               / DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)))
                               * p.paid_amount, 2)
                   -- First Payment
                   when p.payment_term_type_id = 4
                        and N.THE_ROW = DECODE(L.PREPAY_SWITCH, 1, 1, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))
                        and P.PAYMENT_TERM_ID = FIRST_VALUE(P.PAYMENT_TERM_ID) OVER ()
                        and mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) = DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        round(((DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)
                               - (to_number(to_char(p.payment_term_date - 1,'dd')) /30))
                               / DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) * p.paid_amount,2)
                   -- Payment Term Seams
                   when p.payment_term_type_id = 4
                        and P.PAYMENT_TERM_ID <> LAG(P.PAYMENT_TERM_ID, DECODE(L.PREPAY_SWITCH, 1, 1, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))) OVER (PARTITION BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID ORDER BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID, P.PAYMENT_TERM_ID)
                        and mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH)
                        then
                          round(
                                (1 - ((Decode(LAG(P.PAYMENT_FREQ_ID, DECODE(L.PREPAY_SWITCH, 1, 1, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))) OVER (PARTITION BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID ORDER BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID),1, 12, 2, 6, 3, 3, 1)
                                 - (to_number(to_char(p.payment_term_date - 1,'dd')) /30))
                                 / Decode(LAG(P.PAYMENT_FREQ_ID, DECODE(L.PREPAY_SWITCH, 1, 1, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))) OVER (PARTITION BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID ORDER BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID),1, 12, 2, 6, 3, 3, 1))
                                ) * LAG(p.paid_amount, DECODE(L.PREPAY_SWITCH, 1, 1, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)))
                                    OVER (PARTITION BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID ORDER BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID)
                                +
                                ((DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)
                                 - (to_number(to_char(p.payment_term_date - 1,'dd')) /30))
                                 / DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)
                                ) * p.paid_amount
                                , 2)
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        round(p.paid_amount,2)
                   else
                    0
                end as PAID_AMOUNT,
                case
                   when N.THE_ROW =
                        P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) + Decode(p.payment_term_type_id, 4, 1, 0) and
                        P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                    L.RESIDUAL_AMOUNT
                   else
                    0
                end as RESIDUAL_AMOUNT,
                L.DISCOUNT_RATE,
                L.PREPAY_SWITCH,
                1,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                     1
                   -- Last (extra) Payment for Partial Month
                   when p.payment_term_type_id = 4
                        and N.THE_ROW = (P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) + 1)
                        and P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID
                        then
                     1
                   else
                    0
                end as PAYMENT_MONTH,
                DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) as MONTHS_TO_ACCRUE,
                case
                   when N.THE_ROW =
                        P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) + Decode(p.payment_term_type_id, 4, 1, 0) and
                        P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                    L.BPO_PRICE
                   else
                    0
                end as BPO_PRICE,
                case
                   when N.THE_ROW =
                        P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) + Decode(p.payment_term_type_id, 4, 1, 0) and
                        P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                    L.TERM_PENALTY
                   else
                    0
                end as TERM_PENALTY,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_1, 0)
                   else
                    0
                end as CONTINGENT_PAID1,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_2, 0)
                   else
                    0
                end as CONTINGENT_PAID2,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_3, 0)
                   else
                    0
                end as CONTINGENT_PAID3,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_4, 0)
                   else
                    0
                end as CONTINGENT_PAID4,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_5, 0)
                   else
                    0
                end as CONTINGENT_PAID5,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_6, 0)
                   else
                    0
                end as CONTINGENT_PAID6,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_7, 0)
                   else
                    0
                end as CONTINGENT_PAID7,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_8, 0)
                   else
                    0
                end as CONTINGENT_PAID8,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_9, 0)
                   else
                    0
                end as CONTINGENT_PAID9,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_10, 0)
                   else
                    0
                end as CONTINGENT_PAID10,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_1, 0)
                   else
                    0
                end as EXECUTORY_PAID1,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_2, 0)
                   else
                    0
                end as EXECUTORY_PAID2,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_3, 0)
                   else
                    0
                end as EXECUTORY_PAID3,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_4, 0)
                   else
                    0
                end as EXECUTORY_PAID4,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_5, 0)
                   else
                    0
                end as EXECUTORY_PAID5,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_6, 0)
                   else
                    0
                end as EXECUTORY_PAID6,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_7, 0)
                   else
                    0
                end as EXECUTORY_PAID7,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_8, 0)
                   else
                    0
                end as EXECUTORY_PAID8,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_9, 0)
                   else
                    0
                end as EXECUTORY_PAID9,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_10, 0)
                   else
                    0
                end as EXECUTORY_PAID10,
                L.IS_OM,
                P.PAYMENT_TERM_TYPE_ID,
                L.DEFERRED_RENT_SW,
                L.INTEREST_CONVENTION_ID,
                P.PAYMENT_TERM_ID,
                ESCALATION,
                ESCALATION_PCT,
                CASE WHEN
                  CASE WHEN ROW_NUMBER() OVER(partition by p.ILR_ID, p.REVISION, p.SET_OF_BOOKS_ID, p.payment_term_id order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)) - 1
                            <=  DECODE(escalation_freq_id, 1, 12, 2, 6, 3, 3, 1) + Decode(prepay_switch, 0, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) - 1, 0)
                       THEN
                        Mod(
                          ROW_NUMBER() OVER(partition by p.ILR_ID, p.REVISION, p.SET_OF_BOOKS_ID, p.payment_term_id order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)) - 1,
                          DECODE(escalation_freq_id, 1, 12, 2, 6, 3, 3, 1) + Decode(prepay_switch, 0, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) - 1, 0)
                        )
                       ELSE
                        Mod(
                          ROW_NUMBER() OVER(partition by p.ILR_ID, p.REVISION, p.SET_OF_BOOKS_ID, p.payment_term_id order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)) - 1
                          - (DECODE(escalation_freq_id, 1, 12, 2, 6, 3, 3, 1) + Decode(prepay_switch, 0, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) - 1, 0)),
                          DECODE(escalation_freq_id, 1, 12, 2, 6, 3, 3, 1)
                        )
                  END = 0
                  AND ROW_NUMBER() OVER(partition by p.ILR_ID, p.REVISION, p.SET_OF_BOOKS_ID, p.payment_term_id order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)) <> 1
                THEN
                  1
                ELSE
                  0
              END ESCALATION_MONTH,
              DECODE(P.ESCALATION_FREQ_ID, 1, 12, 2, 6, 3, 3, 1),
                case
                   when p.payment_term_type_id = 4
                        and N.THE_ROW = P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) + 1
                        and P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                        round(to_number(to_char(p.payment_term_date - 1,'dd'))/30,8)
                   when p.payment_term_type_id = 4 and N.THE_ROW  = 1 and nvl(L.IS_REMEASUREMENT,0) = 0 
                        and P.PAYMENT_TERM_ID = FIRST_VALUE(P.PAYMENT_TERM_ID) OVER () then
                        round((30- to_number(to_char(p.payment_term_date,'dd')) + 1)/30,8)
                   else
                    1
                end as partial_month_percent,
                M.FASB_CAP_TYPE_ID
           from LS_ILR_PAYMENT_TERM_STG P, LS_ILR_STG L, LP, N, LS_ILR_OPTIONS O, LS_FASB_CAP_TYPE_SOB_MAP M
          where P.ILR_ID = L.ILR_ID
            and P.REVISION = L.REVISION
            and LP.ILR_ID = L.ILR_ID
            and LP.REVISION = L.REVISION
            and LP.THE_MAX = 1
            and LP.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
            and P.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
            and N.THE_ROW <= (P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))
                              + case when P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then 1 else 0 end * decode(p.payment_term_type_id,4,1,0)
            AND ADD_MONTHS(TRUNC(P.PAYMENT_TERM_DATE,'MM'),THE_ROW-1) >= nvl(L.NPV_START_DATE, to_date(0001,'yyyy'))
            AND P.ILR_ID = O.ILR_ID
            AND P.REVISION = O.REVISION
            AND O.LEASE_CAP_TYPE_ID = M.LEASE_CAP_TYPE_ID
            AND M.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
          order by 2;

      -- Determine whether the ILR(s) have a negative payment shift
      select count(*)
        into L_SCHEDULE_PAYMENT_SHIFT_COUNT
        from ls_ilr_options o
        join ls_ilr_stg s on s.ilr_id = o.ilr_id
                         and s.revision = o.revision
       where nvl(o.schedule_payment_shift,0) > 0;


      if L_SCHEDULE_PAYMENT_SHIFT_COUNT > 0 then
        -- Check to see if the npv start date is greater than the first payment term date
        -- which would indicate this is a remeasurement or transition
        select count(1)
          into L_MONTH_CHECK
          from LS_ILR_STG L
          join (
                 select ilr_id, revision, set_of_books_id, min(payment_term_date) payment_term_date
                   from ls_ilr_payment_term_stg
                  group by ilr_id, revision, set_of_books_id
               ) P on L.ILR_ID = P.ILR_ID
                  and L.REVISION = P.REVISION
                  and L.SET_OF_BOOKS_ID = P.SET_OF_BOOKS_ID
         where nvl(L.NPV_START_DATE, to_date(0001,'yyyy')) > P.PAYMENT_TERM_DATE;

        if L_MONTH_CHECK > 0 then
          -- Add the records prior to the npv start date with negative id's so that the payments
          -- can be escalated and shifted correctly
          insert into LS_ILR_SCHEDULE_STG
             (ID, ILR_ID, REVISION, SET_OF_BOOKS_ID, month, AMOUNT, RESIDUAL_AMOUNT, RATE,
              PREPAY_SWITCH, PROCESS_NPV, PAYMENT_MONTH, MONTHS_TO_ACCRUE, BPO_PRICE, TERM_PENALTY,
              CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
              CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
              EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
              EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
              IS_OM, PAYMENT_TERM_TYPE_ID, DEFERRED_RENT_SW, INTEREST_CONVENTION_ID,
              PAYMENT_TERM_ID, ESCALATION, ESCALATION_PCT, ESCALATION_MONTH, MONTHS_TO_ESCALATE,
              PARTIAL_MONTH_PERCENT, FASB_CAP_TYPE_ID)
             with N as
              (
               -- create a "table" with 10000 rows
               select ROWNUM as THE_ROW from DUAL connect by level <= 10000),
             MP as (
                select L.ILR_ID, L.REVISION, L.SET_OF_BOOKS_ID,
                       months_between(P.PAYMENT_TERM_DATE, nvl(L.NPV_START_DATE, to_date(0001,'yyyy'))) id_shift
                  from LS_ILR_STG L
                  join (
                         select ilr_id, revision, set_of_books_id, min(payment_term_date) payment_term_date
                           from ls_ilr_payment_term_stg
                          group by ilr_id, revision, set_of_books_id
                       ) P on L.ILR_ID = P.ILR_ID
                          and L.REVISION = P.REVISION
                          and L.SET_OF_BOOKS_ID = P.SET_OF_BOOKS_ID
                 where nvl(L.NPV_START_DATE, to_date(0001,'yyyy')) > P.PAYMENT_TERM_DATE
             ),
             LP as
              (
               -- find the payment_term_id with the latest payment_term_date
               select L2.ILR_ID,
                       L2.PAYMENT_TERM_ID,
                       L2.REVISION,
                       L1.SET_OF_BOOKS_ID,
                       ROW_NUMBER() OVER(partition by L2.ILR_ID, L2.SET_OF_BOOKS_ID, L2.REVISION order by L2.PAYMENT_TERM_DATE desc) as THE_MAX
                 from LS_ILR_PAYMENT_TERM_STG L2, LS_ILR_STG L1
                where L1.ILR_ID = L2.ILR_ID
                  and L1.REVISION = L2.REVISION
                  and L1.SET_OF_BOOKS_ID = L2.SET_OF_BOOKS_ID)
             select MP.ID_SHIFT + ROW_NUMBER() OVER(partition by P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)),
                    P.ILR_ID,
                    L.REVISION,
                    L.SET_OF_BOOKS_ID,
                    ADD_MONTHS(trunc( P.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1),
                    case
                       -- Last Payment
                       when p.payment_term_type_id = 4
                            and N.THE_ROW = (P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) + 1)
                            and P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID
                            then
                            round((1 - ((DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)
                                   - (to_number(to_char(p.payment_term_date - 1,'dd')) /30))
                                   / DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)))
                                   * p.paid_amount, 2)
                       -- First Payment
                       when p.payment_term_type_id = 4
                            and N.THE_ROW = DECODE(L.PREPAY_SWITCH, 1, 1, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))
                            and P.PAYMENT_TERM_ID = FIRST_VALUE(P.PAYMENT_TERM_ID) OVER ()
                            and mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) = DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                            round(((DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)
                                   - (to_number(to_char(p.payment_term_date - 1,'dd')) /30))
                                   / DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) * p.paid_amount,2)
                       -- Payment Term Seams
                       when p.payment_term_type_id = 4
                            and P.PAYMENT_TERM_ID <> LAG(P.PAYMENT_TERM_ID, DECODE(L.PREPAY_SWITCH, 1, 1, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))) OVER (PARTITION BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID ORDER BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID)
                            and mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                                DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH)
                            then
                              round(
                                    (1 - ((Decode(LAG(P.PAYMENT_FREQ_ID, DECODE(L.PREPAY_SWITCH, 1, 1, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))) OVER (PARTITION BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID ORDER BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID),1, 12, 2, 6, 3, 3, 1)
                                     - (to_number(to_char(p.payment_term_date - 1,'dd')) /30))
                                     / Decode(LAG(P.PAYMENT_FREQ_ID, DECODE(L.PREPAY_SWITCH, 1, 1, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))) OVER (PARTITION BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID ORDER BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID),1, 12, 2, 6, 3, 3, 1))
                                    ) * LAG(p.paid_amount, DECODE(L.PREPAY_SWITCH, 1, 1, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)))
                                        OVER (PARTITION BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID ORDER BY P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID)
                                    +
                                    ((DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)
                                     - (to_number(to_char(p.payment_term_date - 1,'dd')) /30))
                                     / DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)
                                    ) * p.paid_amount
                                    , 2)
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                            round(p.paid_amount,2)
                       else
                        0
                    end as PAID_AMOUNT,
                    case
                       when N.THE_ROW =
                            P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) + Decode(p.payment_term_type_id, 4, 1, 0) and
                            P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                        L.RESIDUAL_AMOUNT
                       else
                        0
                    end as RESIDUAL_AMOUNT,
                    L.DISCOUNT_RATE,
                    L.PREPAY_SWITCH,
                    1,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                         1
                       -- Last (extra) Payment for Partial Month
                       when p.payment_term_type_id = 4
                            and N.THE_ROW = (P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) + 1)
                            and P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID
                            then
                         1
                       else
                        0
                    end as PAYMENT_MONTH,
                    DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) as MONTHS_TO_ACCRUE,
                    case
                       when N.THE_ROW =
                            P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) + Decode(p.payment_term_type_id, 4, 1, 0) and
                            P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                        L.BPO_PRICE
                       else
                        0
                    end as BPO_PRICE,
                    case
                       when N.THE_ROW =
                            P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) + Decode(p.payment_term_type_id, 4, 1, 0) and
                            P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                        L.TERM_PENALTY
                       else
                        0
                    end as TERM_PENALTY,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.C_BUCKET_1, 0)
                       else
                        0
                    end as CONTINGENT_PAID1,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.C_BUCKET_2, 0)
                       else
                        0
                    end as CONTINGENT_PAID2,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.C_BUCKET_3, 0)
                       else
                        0
                    end as CONTINGENT_PAID3,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.C_BUCKET_4, 0)
                       else
                        0
                    end as CONTINGENT_PAID4,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.C_BUCKET_5, 0)
                       else
                        0
                    end as CONTINGENT_PAID5,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.C_BUCKET_6, 0)
                       else
                        0
                    end as CONTINGENT_PAID6,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.C_BUCKET_7, 0)
                       else
                        0
                    end as CONTINGENT_PAID7,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.C_BUCKET_8, 0)
                       else
                        0
                    end as CONTINGENT_PAID8,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.C_BUCKET_9, 0)
                       else
                        0
                    end as CONTINGENT_PAID9,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.C_BUCKET_10, 0)
                       else
                        0
                    end as CONTINGENT_PAID10,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.E_BUCKET_1, 0)
                       else
                        0
                    end as EXECUTORY_PAID1,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.E_BUCKET_2, 0)
                       else
                        0
                    end as EXECUTORY_PAID2,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.E_BUCKET_3, 0)
                       else
                        0
                    end as EXECUTORY_PAID3,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.E_BUCKET_4, 0)
                       else
                        0
                    end as EXECUTORY_PAID4,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.E_BUCKET_5, 0)
                       else
                        0
                    end as EXECUTORY_PAID5,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.E_BUCKET_6, 0)
                       else
                        0
                    end as EXECUTORY_PAID6,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.E_BUCKET_7, 0)
                       else
                        0
                    end as EXECUTORY_PAID7,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.E_BUCKET_8, 0)
                       else
                        0
                    end as EXECUTORY_PAID8,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.E_BUCKET_9, 0)
                       else
                        0
                    end as EXECUTORY_PAID9,
                    case
                       when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                            DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                        NVL(P.E_BUCKET_10, 0)
                       else
                        0
                    end as EXECUTORY_PAID10,
                    L.IS_OM,
                    P.PAYMENT_TERM_TYPE_ID,
                    L.DEFERRED_RENT_SW,
                    L.INTEREST_CONVENTION_ID,
                    P.PAYMENT_TERM_ID,
                    ESCALATION,
                    ESCALATION_PCT,
                    CASE WHEN
                      CASE WHEN ROW_NUMBER() OVER(partition by p.ILR_ID, p.REVISION, p.SET_OF_BOOKS_ID, p.payment_term_id order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)) - 1
                                <=  DECODE(escalation_freq_id, 1, 12, 2, 6, 3, 3, 1) + Decode(prepay_switch, 0, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) - 1, 0)
                           THEN
                            Mod(
                              ROW_NUMBER() OVER(partition by p.ILR_ID, p.REVISION, p.SET_OF_BOOKS_ID, p.payment_term_id order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)) - 1,
                              DECODE(escalation_freq_id, 1, 12, 2, 6, 3, 3, 1) + Decode(prepay_switch, 0, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) - 1, 0)
                            )
                           ELSE
                            Mod(
                              ROW_NUMBER() OVER(partition by p.ILR_ID, p.REVISION, p.SET_OF_BOOKS_ID, p.payment_term_id order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)) - 1
                              - (DECODE(escalation_freq_id, 1, 12, 2, 6, 3, 3, 1) + Decode(prepay_switch, 0, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) - 1, 0)),
                              DECODE(escalation_freq_id, 1, 12, 2, 6, 3, 3, 1)
                            )
                      END = 0
                      AND ROW_NUMBER() OVER(partition by p.ILR_ID, p.REVISION, p.SET_OF_BOOKS_ID, p.payment_term_id order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)) <> 1
                    THEN
                      1
                    ELSE
                      0
                  END ESCALATION_MONTH,
                  DECODE(P.ESCALATION_FREQ_ID, 1, 12, 2, 6, 3, 3, 1),
                    case
                       when p.payment_term_type_id = 4
                            and N.THE_ROW = P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) + 1
                            and P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                            round(to_number(to_char(p.payment_term_date - 1,'dd'))/30,8)
                       when p.payment_term_type_id = 4 and N.THE_ROW  = 1
                            and P.PAYMENT_TERM_ID = FIRST_VALUE(P.PAYMENT_TERM_ID) OVER () then
                            round((30- to_number(to_char(p.payment_term_date,'dd')) + 1)/30,8)
                       else
                        1
                    end as partial_month_percent,
                    M.FASB_CAP_TYPE_ID
               from LS_ILR_PAYMENT_TERM_STG P, LS_ILR_STG L, LP, N, MP, LS_ILR_OPTIONS O, LS_FASB_CAP_TYPE_SOB_MAP M
              where P.ILR_ID = L.ILR_ID
                and P.REVISION = L.REVISION
                and LP.ILR_ID = L.ILR_ID
                and LP.REVISION = L.REVISION
                and LP.THE_MAX = 1
                and LP.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
                and P.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
                and N.THE_ROW <= (P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))
                                  + case when P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then 1 else 0 end * decode(p.payment_term_type_id,4,1,0)
                AND ADD_MONTHS(TRUNC(P.PAYMENT_TERM_DATE,'MM'),THE_ROW-1) < nvl(L.NPV_START_DATE, to_date(0001,'yyyy'))
                and L.ILR_ID = MP.ILR_ID
                and L.REVISION = MP.REVISION
                and L.SET_OF_BOOKS_ID = MP.SET_OF_BOOKS_ID
                AND P.ILR_ID = O.ILR_ID
                AND P.REVISION = O.REVISION
                AND O.LEASE_CAP_TYPE_ID = M.LEASE_CAP_TYPE_ID
                AND M.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
              order by 2;
        end if;
      end if;

      L_STATUS := 'Checking for Escalations';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

      SELECT Count(1)
      INTO L_ESCALATIONS_EXIST
      FROM LS_ILR_SCHEDULE_STG
      WHERE escalation IS NOT NULL
      OR (escalation IS NULL AND escalation_pct <> 0);

      IF L_ESCALATIONS_EXIST > 0 THEN -- Begin All Escalations logic

		--See if any of the ILRs are set up to break out the escalation into a contingent bucket
		--Currently this will return the set of books that are not mapped for IFRS Remeasurement
	  	l_status := 'Retrieving check for ILRs with IFRS Remeasurement';
		select count(1)
		into l_split_esc_out_count
		from ls_ilr_stg s
		where include_esc_in_pay = 0;

		--If we are doing a remeasurement then we need to make sure we have the right amount to base our escalation off of
		--ex. middle of pay period remeasurement should get the last escalated payment amount and escalate from there
		L_STATUS := 'Checking for Remeasurements with Escalations';

		select count(1)
		into l_remeasure_count
		from ls_ilr_stg s
		where is_remeasurement = 1;

		if l_remeasure_count > 0 then
			L_STATUS := 'Updating base escalation amounts for Mid Payment Term Remeasurement';
			--Get Prior Escalated Amount from Prior Schedule
			MERGE INTO ls_ilr_schedule_stg a
			USING (
			  WITH remeasure_term AS -- Get the last payment date to pull based on the current revision and remeasurement date (npv start date)
			   (SELECT pt.ilr_id,
					   pt.revision,
					   MIN(pt.payment_term_id) payment_term_id,
					   MIN(payment_term_date) payment_term_date,
					   MAX(stg.month) payment_month
				  FROM ls_ilr_payment_term     pt,
					   ls_ilr_stg          s,
					   ls_ilr_schedule_stg stg
				 WHERE pt.ilr_id = s.ilr_id
				   AND pt.revision = s.revision
				   AND s.ilr_id = stg.ilr_id
				   AND s.revision = stg.revision
				   AND stg.payment_month = 1
				   AND stg.month <= s.npv_start_date
				   AND s.npv_start_date BETWEEN payment_term_date AND add_months(payment_term_date, (number_of_terms * decode(payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1)) - 1)
				 GROUP BY pt.ilr_id,
						  pt.revision),
			  last_payment AS --Get prior escalated payment if the remeasurement date is within a pay period that isnt the first or second payment
								-- Exclude second payment since would be based off of the amount already in schedule stg
			   (SELECT stg.ilr_id,
					   stg.set_of_books_id,
					   i.current_revision current_revision,
					   rt.payment_month,
					   rt.revision,
					   rt.payment_term_id,
					   MAX(MONTH) last_payment_month
				  FROM ls_ilr_schedule s,
					   ls_ilr_options  o,
					   ls_ilr          i,
					   ls_ilr_stg  stg,
					   remeasure_term  rt
				 WHERE i.ilr_id = stg.ilr_id
				   AND i.current_revision = s.revision
				   AND o.ilr_id = stg.ilr_id
				   AND o.revision = stg.revision
				   AND stg.ilr_id = s.ilr_id
				   AND stg.set_of_books_id = s.set_of_books_id
				   AND stg.ilr_id = rt.ilr_id(+)
				   AND stg.revision = rt.revision(+)
				   AND s.payment_month = 1
				   AND s.escalation_month = 1
				   AND s.month > rt.payment_term_date
				   AND s.month < o.remeasurement_date
				   AND stg.is_remeasurement = 1 -- Filter to where doing remeasurements
				 GROUP BY stg.ilr_id,
						  stg.set_of_books_id,
						  i.current_revision,
						  rt.payment_month,
						  rt.revision,
						  rt.payment_term_id,
						  nvl(rt.payment_term_date, o.remeasurement_date)
				HAVING MAX(MONTH) >= nvl(rt.payment_term_date, o.remeasurement_date))
			  SELECT lp.ilr_id,
					 lp.set_of_books_id,
					 lp.current_revision,
					 lp.revision,
					 lp.last_payment_month,
					 lp.payment_month,
					 lp.payment_term_id,
					 s.month,
					 s.principal_paid,
					 s.interest_paid,
					 s.escalation_month,
					 s.escalation_pct
				FROM last_payment    lp,
					 ls_ilr_schedule s
			   WHERE lp.ilr_id = s.ilr_id
				 AND lp.set_of_books_id = s.set_of_books_id
				 AND lp.current_revision = s.revision
				 AND lp.last_payment_month = MONTH) b ON (a.ilr_id = b.ilr_id AND a.revision = b.revision and a.set_of_books_id = b.set_of_books_id AND a.payment_term_id = b.payment_term_id) WHEN MATCHED THEN
				UPDATE SET a.amount = Nvl(b.principal_paid,0) + Nvl(b.interest_paid,0);

				L_STATUS := 'Updating Escalation flag for Remeasurement Month';
				--Update escalation_month flag so that we calculate a new escalation in the remeasurement month
				UPDATE ls_ilr_schedule_stg
				   SET escalation_month = 1
				 WHERE (ilr_id, revision, set_of_books_id, MONTH) IN (SELECT stg.ilr_id,
																			 stg.revision,
																			 sched.set_of_books_id,
																			 sched.month
																		FROM ls_ilr_schedule sched,
																			 ls_ilr_stg      stg,
																			 ls_ilr          i
																	   WHERE i.ilr_id = stg.ilr_id
																		 AND i.current_revision = sched.revision
																		 AND i.ilr_id = sched.ilr_id
																		 AND sched.set_of_books_id = stg.set_of_books_id
																		 AND stg.is_remeasurement = 1
																		 AND stg.npv_start_date = sched.month);

		end if; -- End Escalation Remeasurement Logic


        L_STATUS := 'Calculating Variable Escalation';
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
        L_STATUS := F_CALC_VARIABLE_ESCALATION_PCT();
        IF L_STATUS <> 'OK' then
          PKG_PP_LOG.P_WRITE_MESSAGE('Error calculating variable escalation: ' || L_STATUS);
          return L_STATUS;
        END IF;

        L_STATUS := 'Escalating payments in ls_ilr_schedule_stg';
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
        merge INTO LS_ILR_SCHEDULE_STG stg
        USING (
          SELECT
          ilr_id,
          revision,
          set_of_books_id,
          MONTH,
          escalated_amount,
		  escalation_amount
          FROM LS_ILR_SCHEDULE_STG stg
		  JOIN LS_ILR_STG s on s.ilr_id = stg.ilr_id and s.revision = stg.revision and s.set_of_books_id = stg.set_of_books_id
          MODEL PARTITION BY (stg.ILR_ID,
                              stg.REVISION,
                              stg.SET_OF_BOOKS_ID,
                              stg.payment_term_id)
          DIMENSION BY (ROW_NUMBER() OVER(partition by stg.ILR_ID, stg.REVISION, stg.SET_OF_BOOKS_ID, stg.payment_term_id order by stg.month) as ID)
          MEASURES( MONTHS_BETWEEN(max(stg.month) OVER(partition by stg.ILR_ID, stg.REVISION, stg.SET_OF_BOOKS_ID, stg.payment_term_id), stg.month) as MONTH_ID,
                    MONTH,
                    amount,
                    payment_month,
                    months_to_accrue,
                    escalation,
                    escalation_month,
                    escalation_pct,
                    months_to_escalate,
					is_remeasurement,
                    0 AS escal_non_escalation_month,
                    0 AS escalated_amount,
                    0 AS new_escalation_pct,
					0 as escalation_amount
                    )
          IGNORE NAV RULES UPSERT ITERATE(100000) UNTIL ITERATION_NUMBER = MONTH_ID [ 1 ]
          (
                ESCALATION_PCT[ITERATION_NUMBER + 1] = Decode(ESCALATION_MONTH[CV()], 0, 0, ESCALATION_PCT[CV()]),
                ESCAL_NON_ESCALATION_MONTH [ITERATION_NUMBER + 1] = CASE WHEN CV(ID) > MONTHS_TO_ESCALATE[CV()] or IS_REMEASUREMENT[1] = 1 THEN
																		Decode(PAYMENT_MONTH [CV()], 1, Decode(ESCALATION_MONTH [CV()], 0, 1, 0), 0)
																	ELSE
																		0
																	END,
                ESCALATION_PCT[ITERATION_NUMBER + 1] = Decode(ESCAL_NON_ESCALATION_MONTH[CV()], 1, ESCALATION_PCT[CV() - MONTHS_TO_ACCRUE[CV()]], ESCALATION_PCT[CV()]),
                NEW_ESCALATION_PCT[ITERATION_NUMBER + 1] = CASE WHEN CV(ID) < = MONTHS_TO_ESCALATE[CV()] THEN
                                                              1
                                                            ELSE
                                                              NEW_ESCALATION_PCT[CV() - MONTHS_TO_ESCALATE[CV()]]
                                                            END * (1 + ESCALATION_PCT[CV()]),
                ESCALATED_AMOUNT[ITERATION_NUMBER + 1] = NEW_ESCALATION_PCT[CV()] * AMOUNT[CV()],

				--Store off Amount Payment Escalated by for IFRS
				ESCALATION_AMOUNT[ITERATION_NUMBER + 1] = ESCALATED_AMOUNT[CV()] - AMOUNT[CV(ID)]
          )
        ) compounded_stg
        ON (stg.ilr_id = compounded_stg.ilr_id AND stg.revision = compounded_stg.revision AND stg.set_of_books_id = compounded_stg.set_of_books_id AND stg.MONTH = compounded_stg.MONTH)
        WHEN matched THEN
        UPDATE SET stg.amount = compounded_stg.escalated_amount, stg.escalation_amt = compounded_stg.escalation_amount;

		--If the escalation is supposed to be in a bucket, throw the escalated amount (Payment*Escalation_pct) into the Contingent bucket marked for it
		if l_split_esc_out_count > 0 then
			L_STATUS := 'Retrieving IFRS Escalation Contingent Bucket';

			begin

			SELECT bucket_number
			into l_ifrs_bucket
			FROM ls_rent_bucket_admin
			WHERE incl_in_ifrs_remeasure = 1
			AND Upper(rent_type) = 'CONTINGENT';

			exception
				when NO_DATA_FOUND then
				   l_status := 'Error performing Escalation for IFRS Remeasurement, no Contingent Bucket defined as "Include in IFRS Remeasurement"';
				   return l_status;
			end;

			--Where we are doing an IFRS remeasurement and Set of Books is not included, we need to move the Escalation Amount out of the Payment (Amount) and into its Contingent Bucket
			--Dynamic SQL string since we dont know which bucket were updating till run time
			l_sqls := 'UPDATE ls_ilr_schedule_stg stg '||
						'   SET amount = amount - escalation_amt, contingent_paid'||l_ifrs_bucket||' = escalation_amt'||
						' WHERE (ilr_id, revision, set_of_books_id) IN'||
						' (SELECT distinct t.ilr_id,'||
						'			   t.revision,'||
						'			   m.set_of_books_id'||
						'		  FROM ls_ilr_payment_term t,'||
						'			   ls_vp_escalation_sob_map m,'||
						'			   ls_ilr_options o,'||
						'			   ls_ilr_stg stg,'||
						'	 ls_ilr i,'||
						'			   (SELECT control_value,'||
						'					   company_id'||
						'				  FROM pp_system_control_companies'||
						'				 WHERE upper(control_name) = ''IFRS SET OF BOOKS REMEASUREMENT'') sys'||
						'		 WHERE t.ilr_id = stg.ilr_id'||
						'		   AND t.revision = stg.revision'||
						'		   AND t.ilr_id = o.ilr_id'||
						'		   AND t.revision = o.revision'||
						'		   AND t.escalation = m.variable_payment_id'||
						'   AND i.ilr_id = stg.ilr_id'||
						'		   AND i.company_id = sys.company_id'||
						'		   AND lower(sys.control_value) = ''yes'''||
						'		   AND m.set_of_books_id = stg.set_of_books_id'||
						'		   AND stg.include_esc_in_pay = 0)';

			 execute immediate l_sqls;
		end if; --End IFRS Escalation check
      END IF; -- End All Escalations Check

      -- Apply negative payment shift logic
      -- If ILR is using monthly payments or paying in arrears, extra months will need to be added to the schedule
      L_STATUS := 'Checking for Schedule (Negative) Payment Shift';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

      if L_SCHEDULE_PAYMENT_SHIFT_COUNT > 0 then
        for L_SHIFT_ILRS in(select distinct s.ilr_id, o.schedule_payment_shift, s.revision from ls_ilr_options o
                   join ls_ilr_stg s on s.ilr_id = o.ilr_id
                   and s.revision = o.revision
                   where nvl(o.schedule_payment_shift,0) > 0)
        loop

		  --Shift Payments and Flags for Payment Month and Escalation
          merge into ls_ilr_schedule_stg a
          using (
             with N as (select ROWNUM as THE_ROW from DUAL connect by level <= 10000),
             pmp as (
              select distinct ilr_id, revision, set_of_books_id,
                     first_value(id) over(order by ilr_id, revision, set_of_books_id) fv_id,
                     first_value(partial_month_percent) over(order by ilr_id, revision, set_of_books_id) fv_partial_month_percent,
                     last_value(id) over(order by ilr_id, revision, set_of_books_id) lv_id,
                     last_value(partial_month_percent) over(order by ilr_id, revision, set_of_books_id) lv_partial_month_percent
                from ls_ilr_schedule_stg),
             max_month as (
              select nvl(o.schedule_payment_shift,0) schedule_payment_shift,
                     max(s.month) lm,
                     months_between(add_months(max(s.month), decode(s.prepay_switch, 0, 1, 0) * nvl(o.schedule_payment_shift,0)),max(s.month)) - 1 mb
                from ls_ilr_schedule_stg s, ls_ilr_options o
               where s.ilr_id = o.ilr_id
                 and s.revision = o.revision
                 and s.revision = L_SHIFT_ILRS.revision
                 and s.ilr_id = L_SHIFT_ILRS.ilr_id
               group by nvl(o.schedule_payment_shift,0), decode(s.prepay_switch, 0, 1, 0) * nvl(o.schedule_payment_shift,0)),
             extra_months as (
              select ROWNUM as THE_ROW from DUAL
             connect by level <= (select mb from max_month)),
             pre_shift_mta as (
               select ROW_NUMBER() OVER(partition by P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)),
                      P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID, P.PAYMENT_TERM_ID, P.PAYMENT_TERM_DATE, P.PAYMENT_TERM_TYPE_ID,
                      ADD_MONTHS(trunc( P.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1) MONTH,
                      DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) as MONTHS_TO_ACCRUE,
                      P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) NUM_MONTHS
                 from (select ilr_id, revision, set_of_books_id, payment_term_id, payment_term_date,
                              payment_freq_id, number_of_terms, payment_term_type_id
                        from LS_ILR_PAYMENT_TERM_STG
                       where (ilr_id, revision, set_of_books_id, payment_term_date) in (
                             select ilr_id, revision, set_of_books_id, min(payment_term_date)
                               from ls_ilr_payment_term_stg
                              group by ilr_id, revision, set_of_books_id)) P
                 cross join N
                 join ls_ilr_options o on P.ilr_id = o.ilr_id and P.revision = o.revision
                where N.THE_ROW <= nvl(o.schedule_payment_shift,0)
             ),
             shifted_mta as (
               select ROW_NUMBER() OVER(partition by P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)),
                      P.ILR_ID, P.REVISION, P.SET_OF_BOOKS_ID, P.PAYMENT_TERM_ID, P.PAYMENT_TERM_DATE, P.PAYMENT_TERM_TYPE_ID,
                      ADD_MONTHS(trunc( P.PAYMENT_TERM_DATE, 'MM' ), nvl(o.schedule_payment_shift,0) + THE_ROW - 1) MONTH,
                      DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) as MONTHS_TO_ACCRUE,
                      P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) NUM_MONTHS
                 from LS_ILR_PAYMENT_TERM_STG P
                 cross join N
                 join ls_ilr_options o on P.ilr_id = o.ilr_id and P.revision = o.revision
                where N.THE_ROW <= (P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1))
                                      + 1 * decode(P.payment_term_type_id,4,1,0)
             ),
             shift as (
              select s.ID + nvl(o.schedule_payment_shift,0) as ID, s.ILR_ID, s.REVISION, s.SET_OF_BOOKS_ID,
                     add_months(s.month, nvl(o.schedule_payment_shift,0)) shift_month,
                     s.AMOUNT, s.RATE,
                     s.PREPAY_SWITCH, s.PROCESS_NPV, s.PAYMENT_MONTH, s.ESCALATION_MONTH, s.ESCALATION_PCT,
                     s.CONTINGENT_PAID1, s.CONTINGENT_PAID2, s.CONTINGENT_PAID3, s.CONTINGENT_PAID4, s.CONTINGENT_PAID5,
                     s.CONTINGENT_PAID6, s.CONTINGENT_PAID7, CONTINGENT_PAID8, s.CONTINGENT_PAID9, s.CONTINGENT_PAID10,
                     s.EXECUTORY_PAID1, s.EXECUTORY_PAID2, s.EXECUTORY_PAID3, s.EXECUTORY_PAID4, s.EXECUTORY_PAID5,
                     s.EXECUTORY_PAID6, s.EXECUTORY_PAID7, s.EXECUTORY_PAID8, s.EXECUTORY_PAID9, s.EXECUTORY_PAID10,
                     s.IS_OM, s.DEFERRED_RENT_SW, s.INTEREST_CONVENTION_ID,
                     s.PARTIAL_MONTH_PERCENT
                from ls_ilr_schedule_stg s, ls_ilr_options o
               where s.ilr_id = o.ilr_id
                 and s.revision = o.revision
                 and s.revision = L_SHIFT_ILRS.revision
                 and s.ilr_id = L_SHIFT_ILRS.ilr_id
                 and s.payment_month <> 0
              union
              select s.ID + e.THE_ROW, s.ILR_ID, s.REVISION, s.SET_OF_BOOKS_ID, add_months(s.month, e.THE_ROW),
                     0, s.RATE,
                     s.PREPAY_SWITCH, s.PROCESS_NPV, 0, 0, 0,
                     0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0,
                     s.IS_OM, s.DEFERRED_RENT_SW, s.INTEREST_CONVENTION_ID,
                     1
                from ls_ilr_schedule_stg s, max_month m, extra_months e
               where e.the_row <= m.mb
                 and s.month = m.lm
                 and s.revision = L_SHIFT_ILRS.revision
                 and s.ilr_id = L_SHIFT_ILRS.ilr_id
              union
              select s.ID, s.ILR_ID, s.REVISION, s.SET_OF_BOOKS_ID, s.month,
                     0, s.RATE,
                     s.PREPAY_SWITCH, s.PROCESS_NPV, 0, 0, 0,
                     0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0,
                     s.IS_OM, s.DEFERRED_RENT_SW, s.INTEREST_CONVENTION_ID,
                     1
                from ls_ilr_schedule_stg s, ls_ilr_options o
               where s.ilr_id = o.ilr_id
                 and s.revision = o.revision
                 and s.revision = L_SHIFT_ILRS.revision
                 and s.ilr_id = L_SHIFT_ILRS.ilr_id
              minus
              select s.ID + nvl(o.schedule_payment_shift,0), s.ILR_ID, s.REVISION, s.SET_OF_BOOKS_ID,
                     add_months(s.month, nvl(o.schedule_payment_shift,0)),
                     0, s.RATE,
                     s.PREPAY_SWITCH, s.PROCESS_NPV, 0, 0, 0,
                     0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0,
                     s.IS_OM, s.DEFERRED_RENT_SW, s.INTEREST_CONVENTION_ID,
                     1
                from ls_ilr_schedule_stg s, ls_ilr_options o
               where s.ilr_id = o.ilr_id
                 and s.revision = o.revision
                 and payment_month <> 0
                 and s.revision = L_SHIFT_ILRS.revision
                 and s.ilr_id = L_SHIFT_ILRS.ilr_id
                 )
             select s.ID, s.ILR_ID, s.REVISION, s.SET_OF_BOOKS_ID, s.shift_month,
               a.payment_term_date tm, add_months(a.payment_term_date, a.num_months - 1) nm,
               s.AMOUNT, s.RATE,
               s.PREPAY_SWITCH, s.PROCESS_NPV, s.PAYMENT_MONTH, a.MONTHS_TO_ACCRUE, s.ESCALATION_MONTH, s.ESCALATION_PCT,
               s.CONTINGENT_PAID1, s.CONTINGENT_PAID2, s.CONTINGENT_PAID3, s.CONTINGENT_PAID4, s.CONTINGENT_PAID5,
               s.CONTINGENT_PAID6, s.CONTINGENT_PAID7, CONTINGENT_PAID8, s.CONTINGENT_PAID9, s.CONTINGENT_PAID10,
               s.EXECUTORY_PAID1, s.EXECUTORY_PAID2, s.EXECUTORY_PAID3, s.EXECUTORY_PAID4, s.EXECUTORY_PAID5,
               s.EXECUTORY_PAID6, s.EXECUTORY_PAID7, s.EXECUTORY_PAID8, s.EXECUTORY_PAID9, s.EXECUTORY_PAID10,
               s.IS_OM, a.PAYMENT_TERM_TYPE_ID, s.DEFERRED_RENT_SW, s.INTEREST_CONVENTION_ID,
               a.PAYMENT_TERM_ID,
               case when s.id = pmp.fv_id
                    then pmp.fv_partial_month_percent
                    when s.id = pmp.lv_id + (1 - s.PREPAY_SWITCH) * m.schedule_payment_shift
                    then pmp.lv_partial_month_percent
                    else s.PARTIAL_MONTH_PERCENT end partial_month_percent
             from shift s
             join pmp on s.ilr_id = pmp.ilr_id
                  and s.revision = pmp.revision
                  and s.revision = L_SHIFT_ILRS.revision
                  and s.ilr_id = L_SHIFT_ILRS.ilr_id
                  and s.set_of_books_id = pmp.set_of_books_id
            cross join max_month m
            join shifted_mta a on a.ilr_id = s.ilr_id
                             and a.revision = s.revision
                             and a.set_of_books_id = s.set_of_books_id
                             and a.month = s.shift_month
          union
           select s.ID, s.ILR_ID, s.REVISION, s.SET_OF_BOOKS_ID, s.shift_month,
               a.payment_term_date tm, add_months(a.payment_term_date, a.num_months - 1) nm,
               s.AMOUNT, s.RATE,
               s.PREPAY_SWITCH, s.PROCESS_NPV, s.PAYMENT_MONTH, a.MONTHS_TO_ACCRUE, s.ESCALATION_MONTH, s.ESCALATION_PCT,
               s.CONTINGENT_PAID1, s.CONTINGENT_PAID2, s.CONTINGENT_PAID3, s.CONTINGENT_PAID4, s.CONTINGENT_PAID5,
               s.CONTINGENT_PAID6, s.CONTINGENT_PAID7, CONTINGENT_PAID8, s.CONTINGENT_PAID9, s.CONTINGENT_PAID10,
               s.EXECUTORY_PAID1, s.EXECUTORY_PAID2, s.EXECUTORY_PAID3, s.EXECUTORY_PAID4, s.EXECUTORY_PAID5,
               s.EXECUTORY_PAID6, s.EXECUTORY_PAID7, s.EXECUTORY_PAID8, s.EXECUTORY_PAID9, s.EXECUTORY_PAID10,
               s.IS_OM, a.PAYMENT_TERM_TYPE_ID, s.DEFERRED_RENT_SW, s.INTEREST_CONVENTION_ID,
               a.PAYMENT_TERM_ID,
               case when s.id = pmp.fv_id
                    then pmp.fv_partial_month_percent
                    when s.id = pmp.lv_id + (1 - s.PREPAY_SWITCH) * m.schedule_payment_shift
                    then pmp.lv_partial_month_percent
                    else s.PARTIAL_MONTH_PERCENT end partial_month_percent
            from shift s
            join pmp on s.ilr_id = pmp.ilr_id
                  and s.revision = pmp.revision
                  and s.revision = L_SHIFT_ILRS.revision
                  and s.ilr_id = L_SHIFT_ILRS.ilr_id
                  and s.set_of_books_id = pmp.set_of_books_id
            cross join max_month m
            join pre_shift_mta a on a.ilr_id = s.ilr_id
                              and a.revision = s.revision
                              and a.set_of_books_id = s.set_of_books_id
                              and a.month = s.shift_month
            ) b
             on (a.id = b.id
                 and a.ilr_id = b.ilr_id
                 and a.revision = b.revision
                 and a.set_of_books_id = b.set_of_books_id
                 and a.month = b.shift_month)
             when matched then
               update set a.amount = b.amount,
                          a.payment_month = b.payment_month,
						  a.escalation_month = b.escalation_month,
						  a.escalation_pct = b.escalation_pct,
                          a.months_to_accrue = b.months_to_accrue,
                          a.payment_term_id = b.payment_term_id,
                          a.contingent_paid1 = b.contingent_paid1,
                          a.contingent_paid2 = b.contingent_paid2,
                          a.contingent_paid3 = b.contingent_paid3,
                          a.contingent_paid4 = b.contingent_paid4,
                          a.contingent_paid5 = b.contingent_paid5,
                          a.contingent_paid6 = b.contingent_paid6,
                          a.contingent_paid7 = b.contingent_paid7,
                          a.contingent_paid8 = b.contingent_paid8,
                          a.contingent_paid9 = b.contingent_paid9,
                          a.contingent_paid10 = b.contingent_paid10,
                          a.executory_paid1 = b.executory_paid1,
                          a.executory_paid2 = b.executory_paid2,
                          a.executory_paid3 = b.executory_paid3,
                          a.executory_paid4 = b.executory_paid4,
                          a.executory_paid5 = b.executory_paid5,
                          a.executory_paid6 = b.executory_paid6,
                          a.executory_paid7 = b.executory_paid7,
                          a.executory_paid8 = b.executory_paid8,
                          a.executory_paid9 = b.executory_paid9,
                          a.executory_paid10 = b.executory_paid10,
                          a.partial_month_percent = b.partial_month_percent
             when not matched then
               insert (ID, ILR_ID, REVISION, SET_OF_BOOKS_ID, month, AMOUNT, RESIDUAL_AMOUNT, RATE,
                       PREPAY_SWITCH, PROCESS_NPV, PAYMENT_MONTH, MONTHS_TO_ACCRUE, BPO_PRICE, TERM_PENALTY,
                       CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
                       CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
                       EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
                       EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
                       IS_OM, PAYMENT_TERM_TYPE_ID, DEFERRED_RENT_SW, INTEREST_CONVENTION_ID,
                       PAYMENT_TERM_ID, ESCALATION, ESCALATION_PCT, ESCALATION_MONTH, MONTHS_TO_ESCALATE,
                       PARTIAL_MONTH_PERCENT)
               values (b.ID, b.ILR_ID, b.REVISION, b.SET_OF_BOOKS_ID, b.shift_month, b.AMOUNT, 0, b.RATE,
                       b.PREPAY_SWITCH, b.PROCESS_NPV, b.PAYMENT_MONTH, b.MONTHS_TO_ACCRUE, 0, 0,
                       b.CONTINGENT_PAID1, b.CONTINGENT_PAID2, b.CONTINGENT_PAID3, b.CONTINGENT_PAID4, b.CONTINGENT_PAID5,
                       b.CONTINGENT_PAID6, b.CONTINGENT_PAID7, b.CONTINGENT_PAID8, b.CONTINGENT_PAID9, b.CONTINGENT_PAID10,
                       b.EXECUTORY_PAID1, b.EXECUTORY_PAID2, b.EXECUTORY_PAID3, b.EXECUTORY_PAID4, b.EXECUTORY_PAID5,
                       b.EXECUTORY_PAID6, b.EXECUTORY_PAID7, b.EXECUTORY_PAID8, b.EXECUTORY_PAID9, b.EXECUTORY_PAID10,
                       b.IS_OM, b.PAYMENT_TERM_TYPE_ID, b.DEFERRED_RENT_SW, b.INTEREST_CONVENTION_ID,
                       b.PAYMENT_TERM_ID, 0, b.ESCALATION_PCT, b.ESCALATION_MONTH, 0,
                       b.PARTIAL_MONTH_PERCENT);
        end loop;

        -- Remove any extra records added with a negative id
        delete from ls_ilr_schedule_stg
         where id < 1;
      end if;

      -- Load Incentives and Indirect Costs
      L_STATUS := F_LOAD_ILR_IDC_INC_STG();

      IF L_STATUS <> 'OK' then
        PKG_PP_LOG.P_WRITE_MESSAGE('ILR Incentives and Indirect Costs not loaded correctly: ' || L_STATUS);
        return L_STATUS;
      END IF;

	  -- Set Impairment Activity based on whats in ls_ilr_impair
	UPDATE ls_ilr_schedule_stg stg
	   SET impairment_activity =
		   (SELECT impairment_activity
			  FROM ls_ilr_impair i
			 WHERE stg.ilr_id = i.ilr_id
			   AND stg.set_of_books_id = i.set_of_books_id
			   AND stg.month = i.month)
	 WHERE EXISTS (SELECT 1
			  FROM ls_ilr_impair i
			 WHERE stg.ilr_id = i.ilr_id
			   AND stg.set_of_books_id = i.set_of_books_id
			   AND stg.month = i.month);


    --If Floating Rates and Lease Type = 5 (Fixed Principal with Floating Rates) recalc amount column based on effective dated floating rate for beginning of schedule
      L_STATUS := 'Checking for Fixed Principal Floating Rates Calc';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

    MERGE INTO LS_ILR_SCHEDULE_STG A
    USING (
      WITH FIRST_TERM AS
       (SELECT DISTINCT S1.ILR_ID,
              S1.REVISION,
              NVL(S1.NPV_START_DATE,
                PT.MIN_PAY) MIN_DATE
        FROM LS_ILR_STG S1,
           (SELECT ILR_ID,
               REVISION,
               MIN(PAYMENT_TERM_DATE) MIN_PAY
            FROM LS_ILR_PAYMENT_TERM_STG
           GROUP BY ILR_ID,
                REVISION) PT
       WHERE S1.ILR_ID = PT.ILR_ID
         AND S1.REVISION = PT.REVISION),

      FLOATING_RATE_INFO AS
       (SELECT FIRST_TERM.ILR_ID ILR_ID,
           G.USE_FLOATING_RATE,
           L.LEASE_ID,
           L.DAYS_IN_YEAR,
           L.DAYS_IN_MONTH_SW,
           MAX(LFR.EFFECTIVE_DATE) EFFECTIVE_DATE
        FROM LS_ILR_OPTIONS          O,
           LS_ILR_GROUP            G,
           LS_ILR                  I,
           LS_LEASE                L,
           LS_LEASE_FLOATING_RATES LFR,
           FIRST_TERM
       WHERE FIRST_TERM.ILR_ID = O.ILR_ID
         AND FIRST_TERM.REVISION = O.REVISION
         AND FIRST_TERM.ILR_ID = I.ILR_ID
         AND I.ILR_GROUP_ID = G.ILR_GROUP_ID
         AND I.LEASE_ID = L.LEASE_ID
         AND L.LEASE_ID = LFR.LEASE_ID
         AND LFR.EFFECTIVE_DATE < FIRST_TERM.MIN_DATE
         AND L.LEASE_TYPE_ID = 5
         AND G.USE_FLOATING_RATE = 1
       GROUP BY FIRST_TERM.ILR_ID,
            G.USE_FLOATING_RATE,
            L.LEASE_ID,
            L.DAYS_IN_YEAR,
            L.DAYS_IN_MONTH_SW),

      PRINCIPAL_ACCRUAL AS
       (SELECT PAYMENT_MONTHS.ILR_ID,
           PAYMENT_MONTHS.REVISION,
           PAYMENT_MONTHS.SET_OF_BOOKS_ID,
           PAYMENT_MONTHS.MONTH,
           PAYMENT_MONTHS.AMOUNT,
           RATES.RATE,
           PAYMENT_MONTHS.PAYMENT_MONTH,
           FR_INFO.DAYS_IN_YEAR,
           FR_INFO.DAYS_IN_MONTH_SW
        FROM FLOATING_RATE_INFO      FR_INFO,
           LS_ILR_SCHEDULE_STG     PAYMENT_MONTHS,
           LS_LEASE_FLOATING_RATES RATES
       WHERE FR_INFO.ILR_ID = PAYMENT_MONTHS.ILR_ID
         AND RATES.LEASE_ID = FR_INFO.LEASE_ID
         AND RATES.EFFECTIVE_DATE = FR_INFO.EFFECTIVE_DATE
         AND PAYMENT_MONTHS.PAYMENT_MONTH = 1),

      DAYS_IN_MONTH AS
       (SELECT B.ILR_ID,
           B.REVISION,
           B.SET_OF_BOOKS_ID,
           B.MONTH,
           SUM(DAYS_IN_MONTH) DAYS_IN_PAY_PERIOD
        FROM (SELECT DAYS.ILR_ID,
               DAYS.REVISION,
               DAYS.SET_OF_BOOKS_ID,
               DAYS.MONTH,
               DAYS.PAYMENT_MONTH,
               LEASE.PRE_PAYMENT_SW,
               (DECODE(DAYS_IN_MONTH_SW,
                   0,
                   EXTRACT(DAY FROM LAST_DAY(Decode(pre_payment_sw, 1, MONTH, Add_Months(MONTH, -1)))),
                   30)) DAYS_IN_MONTH
            FROM LS_ILR_SCHEDULE_STG DAYS,
               LS_ILR              ILR,
               LS_LEASE            LEASE
           WHERE DAYS.ILR_ID = ILR.ILR_ID
             AND ILR.LEASE_ID = LEASE.LEASE_ID) A,
           (SELECT ILR_ID,
               REVISION,
               SET_OF_BOOKS_ID,
               MONTH,
               PAYMENT_MONTH,
               MONTHS_TO_ACCRUE
            FROM LS_ILR_SCHEDULE_STG
           WHERE PAYMENT_MONTH = 1) B
       WHERE A.ILR_ID = B.ILR_ID
         AND A.REVISION = B.REVISION
         AND A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID
        AND a.MONTH BETWEEN Decode(b.months_to_accrue, 1, b.MONTH,
                                Decode(a.pre_payment_sw, 1, b.MONTH, Add_Months(b.MONTH, - b.months_to_accrue + 1)))
        AND Decode(b.months_to_accrue, 1, b.MONTH,
                          Decode(a.pre_payment_sw, 0, b.MONTH, Add_Months(b.MONTH, b.months_to_accrue - 1)))
       GROUP BY B.ILR_ID,
            B.REVISION,
            B.SET_OF_BOOKS_ID,
            B.MONTH)
      --End With Clauses
      SELECT SCHED.ILR_ID,
         SCHED.REVISION,
         SCHED.SET_OF_BOOKS_ID,
         SCHED.MONTH,
         DAYS_IN_MONTH.DAYS_IN_PAY_PERIOD,
         (PRIN_ACCRUAL.RATE) / (PRIN_ACCRUAL.DAYS_IN_YEAR / DAYS_IN_PAY_PERIOD) NEW_RATE,
         SUM(PRIN_ACCRUAL.AMOUNT) REMAINING_PRINCIPAL
      FROM PRINCIPAL_ACCRUAL   PRIN_ACCRUAL,
         DAYS_IN_MONTH,
         LS_ILR_SCHEDULE_STG SCHED
       WHERE PRIN_ACCRUAL.ILR_ID = SCHED.ILR_ID
       AND PRIN_ACCRUAL.REVISION = SCHED.REVISION
       AND PRIN_ACCRUAL.SET_OF_BOOKS_ID = SCHED.SET_OF_BOOKS_ID
       AND PRIN_ACCRUAL.PAYMENT_MONTH = SCHED.PAYMENT_MONTH
       AND SCHED.MONTH = DAYS_IN_MONTH.MONTH
       AND SCHED.ILR_ID = DAYS_IN_MONTH.ILR_ID
       AND SCHED.REVISION = DAYS_IN_MONTH.REVISION
       AND SCHED.SET_OF_BOOKS_ID = DAYS_IN_MONTH.SET_OF_BOOKS_ID
       AND SCHED.MONTH <= PRIN_ACCRUAL.MONTH
       GROUP BY SCHED.ILR_ID,
          SCHED.REVISION,
          SCHED.SET_OF_BOOKS_ID,
          SCHED.MONTH,
          PRIN_ACCRUAL.RATE,
          PRIN_ACCRUAL.DAYS_IN_YEAR,
          PRIN_ACCRUAL.DAYS_IN_MONTH_SW,
          DAYS_IN_MONTH.DAYS_IN_PAY_PERIOD) B
        ON (A.ILR_ID = B.ILR_ID AND A.REVISION = B.REVISION AND A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID AND A.MONTH = B.MONTH) WHEN MATCHED THEN
      UPDATE
         SET A.AMOUNT =
           (B.REMAINING_PRINCIPAL * B.NEW_RATE) + A.AMOUNT,
           A.REMAINING_PRINCIPAL = B.REMAINING_PRINCIPAL;

        return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_LOAD_ILR_SCHEDULE_STG;

   --**************************************************************************
   --                            F_CALC_SCHEDULES
   --**************************************************************************
   function F_CALC_SCHEDULES return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      --call the function to load the ILR payment term staging table
      L_STATUS := 'Calling F_LOAD_ILR_PAYMENT_TERM_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := PKG_LEASE_VAR_PAYMENTS.F_LOAD_ILR_PAYMENT_TERM_STG;
      IF L_MSG <> 'OK' then
        PKG_PP_LOG.P_WRITE_MESSAGE('LS_ILR_PAYMENT_TERM_STG not loaded correctly: ' || L_MSG);
        return L_MSG;
      END IF;

      --call the function to calculate any additions to the initial measure for
      L_STATUS := 'CALLING F_CALC_INITIAL_AMT';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := PKG_LEASE_VAR_PAYMENTS.F_CALC_INITIAL_AMT;
      IF L_MSG <> 'OK' then
        PKG_PP_LOG.P_WRITE_MESSAGE('Initial measure not calculated correctly: ' || L_MSG);

        --if the initial measure amount calc failed, clear the staging tables
        L_STATUS := F_CLEAR_STAGING_TABLES;
        if L_STATUS <> 'OK' then
          PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
        end if;

        return L_MSG;
      END IF;

      --proceed with the calc with updated initial measure amounts
      L_STATUS := 'CALLING F_LOAD_ILR_SCHEDULE_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := F_LOAD_ILR_SCHEDULE_STG;
      if L_MSG = 'OK' then
         -- Determine the Net Present Value based on the
         -- entered discount rate and the cash flow
         L_STATUS := 'CALLING F_NET_PRESENT_VALUE';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_NET_PRESENT_VALUE;
         if L_MSG = 'OK' then
            -- Call the function to calculate internal rate of return
            -- the function will only update ILRs where NPV > FMV
            L_STATUS := 'CALLING F_CALC_IRR';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            L_MSG := F_CALC_IRR;
            if L_MSG = 'OK' then
               L_STATUS := 'DO NOT REPROCESS NPV FOR SOME ILRs';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
               P_CHECK_RECALC_NPV;

               L_STATUS := 'CALLING F_NET_PRESENT_VALUE AFTER IRR';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
               L_MSG := F_NET_PRESENT_VALUE;
               if L_MSG = 'OK' then
            L_STATUS := 'CALLING F_LOAD_DEFERRED_RENT';
          PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
          L_MSG := F_LOAD_DEFERRED_RENT(FALSE);
                  return L_MSG;
               end if;
            end if;
         end if;
      end if;

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_CALC_SCHEDULES;

   --**************************************************************************
   --                            F_PROCESS_ILR
   --**************************************************************************
   function F_PROCESS_ILR(A_ILR_ID   number,
                          A_REVISION number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING ilr_id: ' || TO_CHAR(A_ILR_ID));
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING revision: ' || TO_CHAR(A_REVISION));

      L_STATUS := 'CALLING F_LOAD_ILR_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := F_LOAD_ILR_STG(A_ILR_ID, A_REVISION);
      if L_MSG = 'OK' then
         L_STATUS := 'CALLING F_CALC_SCHEDULES';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_SCHEDULES;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ILR;

   --**************************************************************************
   --                            F_PROCESS_ILRS
   --**************************************************************************
   function F_PROCESS_ILRS(A_LEASE_ID number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING lease_id: ' || TO_CHAR(A_LEASE_ID));

      L_STATUS := 'CALLING F_LOAD_ILRS_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := F_LOAD_ILRS_STG(A_LEASE_ID);
      if L_MSG = 'OK' then
         L_STATUS := 'CALLING F_CALC_SCHEDULES';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_SCHEDULES;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ILRS;

   --**************************************************************************
   --                            F_MASS_PROCESS_ILRS
   --**************************************************************************
   function F_MASS_PROCESS_ILRS(A_ILR_IDS   PKG_LEASE_IMPORT.NUM_ARRAY,
                                A_REVISIONS PKG_LEASE_IMPORT.NUM_ARRAY) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
      L_IRR NUMBER(22,4);
	  
	  L_DEFAULT_RATE number;
	  L_TERM_LENGTH number;
	  L_ILR_ID varchar2(20);
	  L_EST_IN_SVC_DATE date;
	  L_BORROWING_CURR number;
	  L_DEFAULT_RATE_FLAG number;
	  L_REVISION number;
	  
	  disc_rate_error exception;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());

      for I in 1 .. A_ILR_IDS.COUNT
      loop
		L_STATUS:= 'Checking for Default Rate Flag on ILR';
		L_ILR_ID := A_ILR_IDS(I);
		L_REVISION := A_REVISIONS(I);

		select ls_ilr.est_in_svc_date, ls_ilr_options.borrowing_curr_id, ls_ilr_options.default_rate_flag
		  into L_EST_IN_SVC_DATE, L_BORROWING_CURR, L_DEFAULT_RATE_FLAG
		  from ls_ilr_options 
		  join ls_ilr on ls_ilr_options.ilr_Id = ls_ilr.ilr_id
		 where ls_ilr.ilr_id = L_ILR_ID and
			   ls_ilr_options.revision = L_REVISION;

		IF L_DEFAULT_RATE_FLAG = 1 THEN
		
			L_STATUS:= 'Updating Default Discount Rate';
			L_TERM_LENGTH := PKG_LEASE_ILR.F_PULL_ILR_PAYMENT_TERM_MONTHS(L_ILR_ID, L_REVISION);
			
			if L_TERM_LENGTH IS NULL then
			  RAISE disc_rate_error;
			end if;

			L_STATUS:= 'Updating Default Discount Rate - Getting Discount Rate for ILR';
			L_DEFAULT_RATE := PKG_LEASE_ILR.F_GET_DEFAULT_DISC_RATE(L_ILR_ID, L_TERM_LENGTH, L_EST_IN_SVC_DATE, NULL, L_BORROWING_CURR);
			
			--Multiply 100 since ls_ilr_options stores the decimal as a casted percentage
			L_DEFAULT_RATE := L_DEFAULT_RATE*100;
			
			L_STATUS:= 'Updating Default Discount Rate - Setting Discount Rate for ILR';
			UPDATE ls_ilr_options
				SET inception_air = L_DEFAULT_RATE
			  WHERE ls_ilr_options.ilr_id = L_ILR_ID AND
					ls_ilr_options.revision = L_REVISION;
		END IF;

        L_MSG := pkg_lease_schedule.f_process_ilr(A_ILR_IDS(I), A_REVISIONS(I));

        IF L_MSG <> 'OK' THEN
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            ROLLBACK;
            RETURN L_MSG;
        END IF;

        L_STATUS := 'Calling f_process_assets';
        L_MSG:=pkg_lease_schedule.f_process_assets;

        IF L_MSG <> 'OK' THEN
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            ROLLBACK;
            RETURN L_MSG;
        END IF;

        L_STATUS := 'Calling F_CALC_ALL_VAR_PAYMENTS';
        L_MSG:=PKG_LEASE_VAR_PAYMENTS.F_CALC_ALL_VAR_PAYMENTS(null);

        select nvl(100 * (POWER((1 + IRR), 12) - 1), 0) into L_IRR
          from LS_ILR_STG
        where ILR_ID = A_ILR_IDS(i)
          and REVISION = A_REVISIONS(i)
          and SET_OF_BOOKS_ID = 1;
		
		if L_IRR > 200 and lower(trim(pkg_pp_system_control.f_pp_system_control('Lease IRR Validation Control'))) = 'yes' then
            RAISE_APPLICATION_ERROR(-20000, 'Schedule cannot be built. The given FMV and NPV would cause an internal rate of return over 200%. Update ILR information accordingly or turn off Lease IRR Validation Control.');
            rollback;
            RETURN L_MSG;
        end if;


        L_STATUS := 'Calling f_save_schedules';
        L_MSG := pkg_lease_schedule.f_save_schedules;

        IF L_MSG <> 'OK' THEN
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            ROLLBACK;
            RETURN L_MSG;
        END IF;

        -- Clear staging tables
        L_STATUS := 'Calling f_clear_staging_tables';
        L_MSG := pkg_lease_schedule.F_CLEAR_STAGING_TABLES(TRUE);

        IF L_MSG <> 'OK' THEN
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            ROLLBACK;
            RETURN L_MSG;
        END IF;
      end loop;

      return L_MSG;
   exception
	  when disc_rate_error then
		 L_STATUS := L_MSG || ' - Unable to confirm default discount rate';
	     return L_MSG;
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_MASS_PROCESS_ILRS;

   --**************************************************************************
  --                            F_PROCESS_MOD_RETRO_FCST
  -- This function will recalculate NBV of the asset at Conversion Date
  -- and calculate Earnings on Asset at Conversion (NBV - Obligation)
  -- SCW 1/10/18
  --**************************************************************************
  FUNCTION F_process_mod_retro_fcst ( a_revision        NUMBER,
                    a_conversion_date DATE )
  RETURN VARCHAR2
  IS
    l_msg    VARCHAR2(2000);
    l_status VARCHAR2(2000);

  BEGIN
    l_status := 'Starting Modified Retrospective Calculations for Revision '
          ||a_revision
          ||' Conversion Date'
          ||To_char( a_conversion_date, 'mm/dd/yyyy' );

    pkg_pp_log.P_write_message( l_status );

    l_msg := 'Deleting from LS_ILR_ASSET_SCHEDULE_STG: ';

    pkg_pp_log.P_write_message( l_msg );

    DELETE FROM LS_ILR_ASSET_SCHEDULE_STG
    WHERE  revision IN ( a_revision );

    l_msg := 'Deleting from LS_ILR_ASSET_SCHEDULE_CALC_STG: ';

    pkg_pp_log.P_write_message( l_msg );

    DELETE FROM LS_ILR_ASSET_SCHEDULE_CALC_STG
    WHERE  revision IN ( a_revision );

    l_msg := 'Deleting from LS_ILR_ASSET_STG: ';

    pkg_pp_log.P_write_message( l_msg );

    DELETE FROM LS_ILR_ASSET_STG
    WHERE  revision IN ( a_revision );

    l_status := 'CLEARING OUT ls_ilr_schedule_stg';

    pkg_pp_log.P_write_message( l_status );

    DELETE FROM LS_ILR_SCHEDULE_STG;

    l_status := 'Updating NPV Start Date for Modified Retrospective Obligation Calculation';

    pkg_pp_log.P_write_message( l_status );

    UPDATE LS_ILR_STG
    SET    npv_start_date = a_conversion_date,
         process_npv = 1
    WHERE  revision = a_revision;

    --Calc the ILR schedules in the temp tables
    l_status := 'Calling F_CALC_SCHEDULES';

    pkg_pp_log.P_write_message( l_status );

    l_msg := F_calc_schedules( );

    IF l_msg <> 'OK' THEN
      pkg_pp_log.P_write_message( l_msg );

      RETURN l_msg;
    END IF;

    --Calc the Asset Amounts from the ILR Schedules - Not using f_process_assets since it automatically inserts into the actual schedule tables
    l_status := 'CALLING F_ALLOCATE_TO_ASSETS';

    pkg_pp_log.P_write_message( l_status );

    l_msg := f_allocate_to_assets;

    IF l_msg = 'OK' THEN
      l_status := 'CALLING F_CALC_ASSET_SCHEDULE';

      pkg_pp_log.P_write_message( l_status );

      l_msg := f_calc_asset_schedule;

      IF l_msg = 'OK' THEN
        l_status := 'CALLING F_LOAD_ASSET_SCHEDULE';

        pkg_pp_log.P_write_message( l_status );

        l_msg := F_load_asset_schedule( a_conversion_date );

        IF l_msg <> 'OK' THEN
          pkg_pp_log.P_write_message( l_msg );
          RETURN l_msg;
        END IF;
      END IF;
    END IF;

    --Wipe out fields that we need to replace with mod retro calc results
    pkg_pp_log.p_write_message('Clear Asset Schedule Obligation/Payments/Interest prior to Conversion Date');
    UPDATE LS_ASSET_SCHEDULE
    SET    beg_obligation = NULL,
         end_obligation = NULL,
         interest_paid = NULL,
         interest_accrual = NULL,
         principal_paid = NULL,
         principal_accrual = NULL,
         beg_lt_obligation = NULL,
         end_lt_obligation = NULL,
         executory_paid1 = NULL,
         executory_accrual1 = NULL,
         contingent_paid1 = NULL,
         contingent_accrual1 = NULL,
         executory_paid2 = NULL,
         executory_accrual2 = NULL,
         contingent_paid2 = NULL,
         contingent_accrual2 = NULL,
         executory_paid3 = NULL,
         executory_accrual3 = NULL,
         contingent_paid3 = NULL,
         contingent_accrual3 = NULL,
         executory_paid4 = NULL,
         executory_accrual4 = NULL,
         contingent_paid4 = NULL,
         contingent_accrual4 = NULL,
         executory_paid5 = NULL,
         executory_accrual5 = NULL,
         contingent_paid5 = NULL,
         contingent_accrual5 = NULL,
         executory_paid6 = NULL,
         executory_accrual6 = NULL,
         contingent_paid6 = NULL,
         contingent_accrual6 = NULL,
         executory_paid7 = NULL,
         executory_accrual7 = NULL,
         contingent_paid7 = NULL,
         contingent_accrual7 = NULL,
         executory_paid8 = NULL,
         executory_accrual8 = NULL,
         contingent_paid8 = NULL,
         contingent_accrual8 = NULL,
         executory_paid9 = NULL,
         executory_accrual9 = NULL,
         contingent_paid9 = NULL,
         contingent_accrual9 = NULL,
         executory_paid10 = NULL,
         executory_accrual10 = NULL,
         contingent_paid10 = NULL,
         contingent_accrual10 = NULL,
         obligation_reclass = NULL,
         unaccrued_interest_reclass = -1 * (BEG_LT_LIABILITY + NVL(UNACCRUED_INTEREST_REMEASURE, 0) - END_LT_LIABILITY)
    WHERE  revision = a_revision AND
         month < a_conversion_date;

    --Wipe out Interest since we will repopulate based on conversion date
    pkg_pp_log.p_write_message('Clear ILR Schedule Obligation/Payments/Interest prior to Conversion Date');
    UPDATE LS_ILR_SCHEDULE
    SET    beg_obligation = NULL,
         end_obligation = NULL,
         interest_paid = NULL,
         interest_accrual = NULL,
         principal_paid = NULL,
         principal_accrual = NULL,
         beg_lt_obligation = NULL,
         end_lt_obligation = NULL,
         executory_paid1 = NULL,
         executory_accrual1 = NULL,
         contingent_paid1 = NULL,
         contingent_accrual1 = NULL,
         executory_paid2 = NULL,
         executory_accrual2 = NULL,
         contingent_paid2 = NULL,
         contingent_accrual2 = NULL,
         executory_paid3 = NULL,
         executory_accrual3 = NULL,
         contingent_paid3 = NULL,
         contingent_accrual3 = NULL,
         executory_paid4 = NULL,
         executory_accrual4 = NULL,
         contingent_paid4 = NULL,
         contingent_accrual4 = NULL,
         executory_paid5 = NULL,
         executory_accrual5 = NULL,
         contingent_paid5 = NULL,
         contingent_accrual5 = NULL,
         executory_paid6 = NULL,
         executory_accrual6 = NULL,
         contingent_paid6 = NULL,
         contingent_accrual6 = NULL,
         executory_paid7 = NULL,
         executory_accrual7 = NULL,
         contingent_paid7 = NULL,
         contingent_accrual7 = NULL,
         executory_paid8 = NULL,
         executory_accrual8 = NULL,
         contingent_paid8 = NULL,
         contingent_accrual8 = NULL,
         executory_paid9 = NULL,
         executory_accrual9 = NULL,
         contingent_paid9 = NULL,
         contingent_accrual9 = NULL,
         executory_paid10 = NULL,
         executory_accrual10 = NULL,
         contingent_paid10 = NULL,
         contingent_accrual10 = NULL,
         obligation_reclass = NULL,
         unaccrued_interest_reclass = -1 * (BEG_LT_LIABILITY + NVL(UNACCRUED_INTEREST_REMEASURE, 0) - END_LT_LIABILITY)
    WHERE  revision = a_revision AND
         month < a_conversion_date;

    --Merge the rest of the dataset in after the conversion date for obligation and interest
    pkg_pp_log.p_write_message('Update ILR Schedule Obligation/Payments/Interest from Conversion Date forward');
    merge INTO LS_ILR_SCHEDULE a
    USING ( SELECT ilr_id,
             revision,
             set_of_books_id,
             month,
             SUM ( beg_obligation )    beg_obligation,
             SUM ( end_obligation )    end_obligation,
             SUM ( interest_accrual )  interest_accrual,
             SUM ( principal_accrual ) principal_accrual,
             SUM ( interest_paid )     interest_paid,
             SUM ( principal_paid )    principal_paid,
             SUM ( beg_lt_obligation ) beg_lt_obligation,
             SUM ( end_lt_obligation ) end_lt_obligation,
             SUM ( obligation_reclass ) obligation_reclass,
             SUM ( unaccrued_interest_reclass ) unaccrued_interest_reclass
        FROM   LS_ILR_ASSET_SCHEDULE_STG
        WHERE  month > = a_conversion_date AND
             revision = a_revision
        GROUP  BY ilr_id,revision,month,set_of_books_id,is_om ) b
    ON ( a.ilr_id = b.ilr_id AND a.revision = b.revision AND a.set_of_books_id = b.set_of_books_id AND a.month = b.month )
    WHEN matched THEN
      UPDATE SET a.beg_obligation = b.beg_obligation,
           a.end_obligation = b.end_obligation,
           a.beg_lt_obligation = b.beg_lt_obligation,
           a.end_lt_obligation = b.end_lt_obligation,
           a.interest_accrual = b.interest_accrual,
           a.principal_accrual = b.principal_accrual,
           a.interest_paid = b.interest_paid,
           a.principal_paid = b.principal_paid,
           a.obligation_reclass = b.obligation_reclass,
           a.unaccrued_interest_reclass = b.unaccrued_interest_reclass
    WHEN NOT matched THEN
      INSERT ( a.ilr_id,
           a.revision,
           a.set_of_books_id,
           a.month,
           a.beg_obligation,
           a.end_obligation,
           a.beg_lt_obligation,
           a.end_lt_obligation,
           a.interest_accrual,
           a.principal_accrual,
           a.interest_paid,
           a.principal_paid,
           a.obligation_reclass,
           a.unaccrued_interest_reclass)
      VALUES ( b.ilr_id,
           b.revision,
           b.set_of_books_id,
           b.month,
           b.beg_obligation,
           b.end_obligation,
           b.beg_lt_obligation,
           b.end_lt_obligation,
           b.interest_accrual,
           b.principal_accrual,
           b.interest_paid,
           b.principal_paid,
           b.obligation_reclass,
           b.unaccrued_interest_reclass);

    -- Update the asset schedule
    pkg_pp_log.p_write_message('Update Asset Schedule Obligation/Payments/Interest from Conversion Date forward');
    merge INTO LS_ASSET_SCHEDULE a
    USING ( SELECT ls_asset_id,
                   revision,
                   set_of_books_id,
                   month,
                   beg_obligation,
                   end_obligation,
                   interest_accrual,
                   principal_accrual,
                   interest_paid,
                   principal_paid,
                   beg_lt_obligation,
                   end_lt_obligation,
                   obligation_reclass,
                   unaccrued_interest_reclass
              FROM LS_ILR_ASSET_SCHEDULE_STG
             WHERE month > = a_conversion_date
               AND revision = a_revision
          ) b
    ON ( a.ls_asset_id = b.ls_asset_id AND a.revision = b.revision AND a.set_of_books_id = b.set_of_books_id AND a.month = b.month )
    WHEN matched THEN
      UPDATE SET a.beg_obligation = b.beg_obligation,
           a.end_obligation = b.end_obligation,
           a.beg_lt_obligation = b.beg_lt_obligation,
           a.end_lt_obligation = b.end_lt_obligation,
           a.interest_accrual = b.interest_accrual,
           a.principal_accrual = b.principal_accrual,
           a.interest_paid = b.interest_paid,
           a.principal_paid = b.principal_paid,
           a.obligation_reclass = b.obligation_reclass,
           a.unaccrued_interest_reclass = b.unaccrued_interest_reclass
    WHEN NOT matched THEN
      INSERT ( a.ls_asset_id,
           a.revision,
           a.set_of_books_id,
           a.month,
           a.beg_obligation,
           a.end_obligation,
           a.beg_lt_obligation,
           a.end_lt_obligation,
           a.interest_accrual,
           a.principal_accrual,
           a.interest_paid,
           a.principal_paid,
           a.obligation_reclass,
           a.unaccrued_interest_reclass)
      VALUES ( b.ls_asset_id,
           b.revision,
           b.set_of_books_id,
           b.month,
           b.beg_obligation,
           b.end_obligation,
           b.beg_lt_obligation,
           b.end_lt_obligation,
           b.interest_accrual,
           b.principal_accrual,
           b.interest_paid,
           b.principal_paid,
           b.obligation_reclass,
           b.unaccrued_interest_reclass);

    --Calculate NPV of Payments at Conversion for Liability
    --Calculate NBV of Asset at Conversion and subtract end liability from prior step to get Earnings
    -- Retained Earnings = Beginning Liability - Beginning Net ROU Asset
    pkg_pp_log.p_write_message('Calculating Retained Earnings');
    merge INTO LS_ASSET_FCST_EARNINGS a
    USING ( SELECT cost.ls_asset_id,
                   cost.revision,
                   cost.set_of_books_id,
                   a_conversion_date  MONTH,
                   cost.beg_liability - cost.beg_net_rou_asset asset_earnings_diff
                   --(cost.end_obligation - (cost.end_capital_cost - Nvl( depr.depr_expense, 0 ))) asset_earnings_diff
              FROM LS_ASSET_SCHEDULE cost
             WHERE cost.revision = a_revision
               AND cost.month = a_conversion_date
          ) b
    ON (a.ls_asset_id = b.ls_asset_id AND a.revision = b.revision AND a.set_of_books_id = b.set_of_books_id)
    WHEN matched THEN
      UPDATE SET a.earnings_amount = b.asset_earnings_diff,
                 a.month = b.month
    WHEN NOT matched THEN
      INSERT (a.ls_asset_id,
          a.revision,
          a.set_of_books_id,
          a.month,
          a.earnings_amount)
      VALUES (b.ls_asset_id,
          b.revision,
          b.set_of_books_id,
          b.month,
          b.asset_earnings_diff);

    RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
         l_status := Substr( l_status
                   || ': '
                   || SQLERRM, 1, 2000 );

         RETURN l_status;
  END f_process_mod_retro_fcst;

   --**************************************************************************
   --                            F_PROCESS_ILRS_FCST
   -- Deprecated Function - SCW
   --**************************************************************************
   function F_PROCESS_ILRS_FCST(A_CAP_TYPE number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING ILR FORECAST');

    pkg_pp_log.p_write_message('Clearing 9999');

      -- Delete anything related to revision 9999's
      delete from LS_ASSET_SCHEDULE where REVISION = 9999;
      delete from LS_ILR_SCHEDULE where REVISION = 9999;
      delete from LS_ILR_PAYMENT_TERM where REVISION = 9999;
      delete from LS_ILR_ASSET_MAP where REVISION = 9999;
      delete from LS_ILR_OPTIONS where REVISION = 9999;
      delete from LS_ILR_APPROVAL where REVISION = 9999;

      -- Create revision 10,000's for all In-Service ILR's
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select ILR_ID, 9999, 4, 1
           from LS_ILR_APPROVAL
          where ILR_ID in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
            and REVISION =
                (select CURRENT_REVISION from LS_ILR where ILR_ID = LS_ILR_APPROVAL.ILR_ID);

      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT, PAYMENT_SHIFT, DEPR_CALC_METHOD)
         select ILR_ID,
                9999,
                PURCHASE_OPTION_TYPE_ID,
                PURCHASE_OPTION_AMT,
                RENEWAL_OPTION_TYPE_ID,
                CANCELABLE_TYPE_ID,
                ITC_SW,
                PARTIAL_RETIRE_SW,
                SUBLET_SW,
                MUNI_BO_SW,
                INCEPTION_AIR,
                A_CAP_TYPE,
                TERMINATION_AMT,
                PAYMENT_SHIFT,
                NVL(DEPR_CALC_METHOD,0)
           from LS_ILR_OPTIONS
          where ILR_ID in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
            and REVISION =
                (select CURRENT_REVISION from LS_ILR where ILR_ID = LS_ILR_OPTIONS.ILR_ID);

      insert into LS_ILR_ASSET_MAP
         (ILR_ID, LS_ASSET_ID, REVISION)
         select ILR_ID, LS_ASSET_ID, 9999
           from LS_ILR_ASSET_MAP
          where ILR_ID in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
            and REVISION =
                (select CURRENT_REVISION from LS_ILR where ILR_ID = LS_ILR_ASSET_MAP.ILR_ID);

      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE, PAYMENT_FREQ_ID,
          NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT, CURRENCY_TYPE_ID,
          C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6, C_BUCKET_7,
          C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3, E_BUCKET_4,
          E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10, REVISION)
         select ILR_ID,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10,
                9999
           from LS_ILR_PAYMENT_TERM
          where ILR_ID in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
            and REVISION =
                (select CURRENT_REVISION from LS_ILR where ILR_ID = LS_ILR_PAYMENT_TERM.ILR_ID);

      -- Build schedules
      L_STATUS := 'CALLING F_LOAD_ILR_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      for ILR in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
      loop
         L_MSG := F_LOAD_ILR_STG(ILR.ILR_ID, 9999);
         exit when L_MSG <> 'OK';
      end loop;
      if L_MSG = 'OK' then
         L_STATUS := 'CALLING F_CALC_SCHEDULES';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_SCHEDULES;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

        return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ILRS_FCST;

   --**************************************************************************
   --                            F_EXECUTE_IMMEDIATE
   --**************************************************************************
   function F_EXECUTE_IMMEDIATE(A_SQLS in varchar2, A_START_LOG in number:=null) return varchar2
   is
     counter number;
   begin
     if nvl(a_start_log,0) = 1 then
       PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
     end if;

     if lower(trim(pkg_pp_system_control.f_pp_system_control('Lease Debug Execute Immediate'))) = 'yes' then
       pkg_pp_log.p_write_message(a_sqls);
     end if;

     execute immediate A_SQLS;

     return 'OK';

     exception when others then
       return 'Error Executing SQL: ' || sqlerrm || ' ' || sqlcode || chr(13) || chr(10) || a_sqls;
   end f_execute_immediate;

   --**************************************************************************
   --                            F_CALC_TAXES
   -- This function calculates the taxes related to the schedule from the stg tables
   --**************************************************************************
   function F_CALC_TAXES return varchar2 is
     L_COUNTER NUMBER;
     L_BUCKET_COUNTER NUMBER;
     L_BUCKET  LS_RENT_BUCKET_ADMIN%rowtype;
     L_STATUS VARCHAR2(30000);
	   L_SQLS varchar2(30000);
   begin
     -- Check to see whether anything is configured for tax
     select count(1)
       into L_COUNTER
       from ls_tax_local;

     if L_COUNTER > 0 then
       -- Validate that there is only one tax bucket
       select count(*)
         into L_BUCKET_COUNTER
         from ls_rent_bucket_admin
        where tax_expense_bucket = 1;

       if L_BUCKET_COUNTER <> 1 then
         return 'Error: there must be exactly one bucket defined as the tax expense bucket';
       end if;

       -- Get the tax bucket information
       select *
         into L_BUCKET
         from LS_RENT_BUCKET_ADMIN
        where TAX_EXPENSE_BUCKET = 1;

       -- Clear the staging table
       delete from ls_asset_schedule_tax
        where (ls_asset_id, revision, set_of_books_id, schedule_month) in (
                select ls_asset_id, revision, set_of_books_id, month from ls_ilr_asset_schedule_calc_stg);

       -- Insert records based on payments to get the tax base and tax payment amount
       insert into ls_asset_schedule_tax (
          ls_asset_id, revision, set_of_books_id, schedule_month, gl_posting_mo_yr,
          tax_local_id, vendor_id, tax_district_id, tax_base, tax_rate, payment_amount)
        with ilr_asset as (
               select /*+ MATERIALIZE */ i.ilr_id, i.ilr_number, i.lease_id,
                      -1 * nvl(o.payment_shift,0) payment_shift,
                      a.ls_asset_id, s.revision, a.ls_asset_status_id, a.retirement_date,
                      a.tax_asset_location_id, m.tax_local_id, a.company_id,
                      l.pay_lessor,
                      a.actual_purchase_amount,
                      nvl(a.actual_termination_amount,0) actual_termination_amount,
                      nvl(a.guaranteed_residual_amount,0) -  nvl(a.actual_residual_amount, nvl(a.guaranteed_residual_amount,0)) net_residual
                 from (select distinct ilr_id, revision from ls_ilr_stg) s
                 join ls_ilr i on i.ilr_id = s.ilr_id
                 join ls_asset a on a.ilr_id = i.ilr_id
                 join ls_ilr_options o on o.ilr_id = i.ilr_id
                                      and o.revision = s.revision
                 join ls_asset_tax_map m on m.ls_asset_id = a.ls_asset_id
                 join ls_tax_local l on l.tax_local_id = m.tax_local_id
                where m.status_code_id = 1
             )
        select t.ls_asset_id,
               t.revision,
               t.set_of_books_id,
               t.month schedule_month,
               add_months(t.month, t.shift) gl_posting_month,
               t.tax_local_id,
               t.vendor_id,
               t.tax_district_id,
               t.amount tax_base,
               t.rate payment_rate,
               t.rate * t.amount payment_amount
          from (
                  select /*+ NO_MERGE */ las.month,
                         r.rate,
                         r.ls_asset_id,
                         r.revision,
                         r.tax_local_id,
                         las.set_of_books_id,
                         r.tax_district_id,
                         v.payment_pct * (
                           las.interest_paid + las.principal_paid
                           + nvl(b.cont1 * las.contingent_paid1, 0) + nvl(b.cont2 * las.contingent_paid2, 0)
                           + nvl(b.cont3 * las.contingent_paid3, 0) + nvl(b.cont4 * las.contingent_paid4, 0)
                           + nvl(b.cont5 * las.contingent_paid5, 0) + nvl(b.cont6 * las.contingent_paid6, 0)
                           + nvl(b.cont7 * las.contingent_paid7, 0) + nvl(b.cont8 * las.contingent_paid8, 0)
                           + nvl(b.cont9 * las.contingent_paid9, 0) + nvl(b.cont10 * las.contingent_paid10, 0)
                           + nvl(b.exec1 * las.executory_paid1, 0) + nvl(b.exec2 * las.executory_paid2, 0)
                           + nvl(b.exec3 * las.executory_paid3, 0) + nvl(b.exec4 * las.executory_paid4, 0)
                           + nvl(b.exec5 * las.executory_paid5, 0) + nvl(b.exec6 * las.executory_paid6, 0)
                           + nvl(b.exec7 * las.executory_paid7, 0) + nvl(b.exec8 * las.executory_paid8, 0)
                           + nvl(b.exec9 * las.executory_paid9, 0) + nvl(b.exec10 * las.executory_paid10, 0)
                           + nvl(las.residual_amount, 0) + nvl(las.term_penalty, 0)
                           + nvl(las.bpo_price, 0)) amount,
                         a.payment_shift shift,
                         decode(a.pay_lessor, 0, -1, v.vendor_id) vendor_id
                    from ls_ilr_asset_schedule_calc_stg las
                    join ilr_asset a on a.ls_asset_id = las.ls_asset_id
                                    and a.revision = las.revision
                    join ls_lease l on l.lease_id = a.lease_id
                    join ls_lease_options llo on llo.lease_id = a.lease_id AND llo.revision = l.current_revision
                    join ls_lease_vendor v on v.lease_id = a.lease_id
                                          and v.company_id = a.company_id
                    join (
                            select sum(rates.rate) rate,
                                   rates.ls_asset_id ls_asset_id,
                                   rates.tax_local_id tax_local_id,
                                   rates.revision revision,
                                   rates.tax_loc_id as tax_district_id,
                                   rates.effective_date eff_date,
                                   rates.next_date next_eff_date
                              from (
                                      select /*+ NO_MERGE */ r.rate, r.effective_date, r.next_date, r.tax_local_id, r.state_id tax_loc_id,
                                             a.ls_asset_id, a.revision
                                        from ilr_asset a
                                        join (
                                               select s.tax_local_id, s.rate, trim(l.state_id) state_id, s.effective_date,
                                                      l.asset_location_id,
                                                      nvl(lead(s.effective_date)
                                                            over (partition by s.tax_local_id, trim(s.state_id), l.asset_location_id order by s.effective_date),
                                                          to_date('999912','YYYYMM')) next_date
                                                 from ls_tax_state_rates s
                                                 join asset_location l on l.state_id = s.state_id
                                             ) r on r.tax_local_id = a.tax_local_id
                                                              and r.asset_location_id = a.tax_asset_location_id
                                      union all
                                      select /*+ NO_MERGE */ r.rate, r.effective_date, r.next_date, r.tax_local_id, r.tax_district_id tax_loc_id,
                                             a.ls_asset_id, a.revision
                                        from ilr_asset a
                                        join (
                                               select d.tax_local_id, d.rate, to_char(l.tax_district_id) tax_district_id, d.effective_date,
                                                      l.asset_location_id,
                                                      nvl(lead(d.effective_date)
                                                            over (partition by d.tax_local_id, to_char(l.tax_district_id), l.asset_location_id order by d.effective_date),
                                                          to_date('999912','YYYYMM')) next_date
                                                 from ls_tax_district_rates d
                                                 join ls_location_tax_district l on l.tax_district_id = d.ls_tax_district_id
                                             ) r on r.tax_local_id = a.tax_local_id
                                                              and r.asset_location_id = a.tax_asset_location_id
                                   ) rates
                             group by rates.ls_asset_id, rates.tax_local_id, rates.revision, rates.tax_loc_id,
                                      rates.effective_date, rates.next_date
                         ) r on r.ls_asset_id = las.ls_asset_id
                            and r.revision = las.revision
                            and r.tax_local_id = a.tax_local_id
                            and case
                                 when llo.tax_rate_option_id = 1 then
                                  last_day(add_months(las.month, nvl(a.payment_shift,0)))
                                 when llo.tax_rate_option_id = 2 then
                                  l.master_agreement_date
                                end between trunc(r.eff_date,'day') and trunc(r.next_eff_date,'day')
                    cross join ls_tax_basis_buckets b
              ) t;

       -- Calculate the accrual amount by straight-lining the tax payment over the payment period
       merge into ls_asset_schedule_tax tax
       using (
                with tax_eff_date as (
                       select t.ls_asset_id, t.revision, t.set_of_books_id, min(t.schedule_month) first_tax_month,
                              t.tax_local_id, t.vendor_id, t.tax_district_id
                         from ls_asset_schedule_tax t
                         join ls_ilr_asset_schedule_calc_stg s on s.ls_asset_id = t.ls_asset_id
                                                              and s.revision = t.revision
                                                              and s.set_of_books_id = t.set_of_books_id
                                                              and s.month = t.schedule_month
                        group by t.ls_asset_id, t.revision, t.set_of_books_id,
                                 t.tax_local_id, t.vendor_id, t.tax_district_id
                     ),
                     accrual_months as (
                       select t.ls_asset_id, t.revision, t.set_of_books_id, t.schedule_month payment_month,
                              t.tax_local_id, t.vendor_id, t.tax_district_id,
                              add_months(t.schedule_month, decode(s.prepay_switch, 0, -1, 1) * (s.months_to_accrue -1)) accrue_to_month,
                              round(t.payment_amount / case when s.prepay_switch = 0
                                                             and d.first_tax_month between add_months(t.schedule_month,
                                                                                                      -1 * (s.months_to_accrue -1))
                                                                                       and t.schedule_month then
                                                         months_between(t.schedule_month, d.first_tax_month) + 1
                                                       else
                                                         s.months_to_accrue
                                                       end, 2) as accrual_amount,
                              t.payment_amount
                                - ( case when s.prepay_switch = 0
                                          and d.first_tax_month between add_months(t.schedule_month,
                                                                                   -1 * (s.months_to_accrue -1))
                                                                    and t.schedule_month then
                                      months_between(t.schedule_month, d.first_tax_month) + 1
                                    else
                                      s.months_to_accrue
                                    end
                                    * round(t.payment_amount / case when s.prepay_switch = 0
                                                                     and d.first_tax_month between add_months(t.schedule_month,
                                                                                                              -1 * (s.months_to_accrue -1))
                                                                                               and t.schedule_month then
                                                                 months_between(t.schedule_month, d.first_tax_month) + 1
                                                               else
                                                                 s.months_to_accrue
                                                               end, 2)) as ROUNDER
                         from ls_asset_schedule_tax t
                         join ls_ilr_asset_schedule_calc_stg s on s.ls_asset_id = t.ls_asset_id
                                                              and s.revision = t.revision
                                                              and s.set_of_books_id = t.set_of_books_id
                                                              and s.month = t.schedule_month
                         join tax_eff_date d on d.ls_asset_id = t.ls_asset_id
                                            and d.revision = t.revision
                                            and d.set_of_books_id = t.set_of_books_id
                                            and d.tax_local_id = t.tax_local_id
                                            and d.vendor_id = t.vendor_id
                                            and d.tax_district_id = t.tax_district_id
                        where t.payment_amount <> 0
                     )
                     select t.ls_asset_id, t.revision, t.set_of_books_id, t.schedule_month,
                            t.tax_local_id, t.vendor_id, t.tax_district_id,
                            t.payment_amount,
                            am.accrual_amount +
                              case when t.schedule_month = greatest(am.payment_month, am.accrue_to_month) then
                                am.rounder
                              else
                                0
                              end accrual_amount
                       from ls_asset_schedule_tax t
                       join accrual_months am on am.ls_asset_id = t.ls_asset_id
                                             and am.revision = t.revision
                                             and am.set_of_books_id = t.set_of_books_id
                                             and am.tax_local_id = t.tax_local_id
                                             and am.vendor_id = t.vendor_id
                                             and am.tax_district_id = t.tax_district_id
                                             and t.schedule_month between least(am.payment_month, am.accrue_to_month)
                                                                      and greatest(am.payment_month, am.accrue_to_month)
             ) x
       on (    x.ls_asset_id = tax.ls_asset_id
           and x.revision = tax.revision
           and x.set_of_books_id = tax.set_of_books_id
           and x.schedule_month = tax.schedule_month
           and x.tax_local_id = tax.tax_local_id
           and x.vendor_id = tax.vendor_id
           and x.tax_district_id = tax.tax_district_id)
       when matched then
          update set tax.accrual_amount = x.accrual_amount;

       -- Update the appropriate buckets on the schedule with the total amounts for the schedule months
       -- Asset Schedule
       L_SQLS:= '
       merge into ls_asset_schedule a
       using (
                select t.ls_asset_id, t.revision, t.set_of_books_id, t.schedule_month,
                       sum(t.payment_amount) payment_amount,
                       sum(t.accrual_amount) accrual_amount
                  from ls_asset_schedule_tax t
                  join ls_ilr_asset_schedule_calc_stg s on s.ls_asset_id = t.ls_asset_id
                                                       and s.revision = t.revision
                                                       and s.set_of_books_id = t.set_of_books_id
                                                       and s.month = t.schedule_month
                 group by t.ls_asset_id, t.revision, t.set_of_books_id, t.schedule_month
             ) s
       on (    a.ls_asset_id = s.ls_asset_id
           and a.revision = s.revision
           and a.set_of_books_id = s.set_of_books_id
           and a.month = s.schedule_month
          )
       when matched then
         update set a.' ||L_BUCKET.RENT_TYPE || '_paid' || to_char(L_BUCKET.BUCKET_NUMBER) || ' = s.payment_amount,
                    a.' ||L_BUCKET.RENT_TYPE || '_accrual' || to_char(L_BUCKET.BUCKET_NUMBER) || ' = s.accrual_amount';

       L_STATUS := F_EXECUTE_IMMEDIATE(L_SQLS);

       IF L_STATUS <> 'OK' THEN
         RETURN L_STATUS;
       END IF;

       -- ILR Schedule
       L_SQLS:= '
       merge into ls_ilr_schedule a
       using (
                select s.ilr_id, t.revision, t.set_of_books_id, t.schedule_month,
                       sum(t.payment_amount) payment_amount,
                       sum(t.accrual_amount) accrual_amount
                  from ls_asset_schedule_tax t
                  join ls_ilr_asset_schedule_calc_stg s on s.ls_asset_id = t.ls_asset_id
                                                       and s.revision = t.revision
                                                       and s.set_of_books_id = t.set_of_books_id
                                                       and s.month = t.schedule_month
                 group by s.ilr_id, t.revision, t.set_of_books_id, t.schedule_month
             ) s
       on (    a.ilr_id = s.ilr_id
           and a.revision = s.revision
           and a.set_of_books_id = s.set_of_books_id
           and a.month = s.schedule_month
          )
       when matched then
         update set a.' ||L_BUCKET.RENT_TYPE || '_paid' || to_char(L_BUCKET.BUCKET_NUMBER) || ' = s.payment_amount,
                    a.' ||L_BUCKET.RENT_TYPE || '_accrual' || to_char(L_BUCKET.BUCKET_NUMBER) || ' = s.accrual_amount';

       L_STATUS := F_EXECUTE_IMMEDIATE(L_SQLS);

       IF L_STATUS <> 'OK' THEN
         RETURN L_STATUS;
       END IF;
     end if;

     RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
         l_status := Substr( l_status
                   || ': '
                   || SQLERRM, 1, 2000 );

         RETURN l_status;

   end F_CALC_TAXES;

   --**************************************************************************
   --                            F_SAVE_SCHEDULES
   -- This function saves the scehdules frm the calc table to the stg tables
   --**************************************************************************
   function F_SAVE_SCHEDULES(A_MONTH in date:=null) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
      L_LEASE_TYPE number;
      L_COMPANY_ID number;
      L_FLOAT_RATE number;
      L_RTN varchar2(2000);
      L_MONTH date;
      L_COUNT NUMBER(10,0);

	cursor STAGED_ILRS IS
		select ilr_id, revision, set_of_books_id from ls_ilr_stg order by 1, 2, 3;
   begin

	  PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('SAVING SCHEDULE');

	  --Delete by Set of Books since Remeasurement Date can be populated by Set of Books in ls_ilr_stg so would need to remove different datasets by ILR and SOB
	  for l_ilr_rec in STAGED_ILRS
	  loop
		l_status := 'Deleting prior schedule for ILR ID: '||to_char(l_ilr_rec.ilr_id)||'    Revision: ' ||to_char(l_ilr_rec.revision)||'    Set of Books: '||to_char(l_ilr_rec.set_of_books_id);
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		DELETE FROM ls_ilr_schedule
		 WHERE ilr_id = l_ilr_rec.ilr_id
		   AND revision = l_ilr_rec.revision
		   AND set_of_books_id = l_ilr_rec.set_of_books_id
		   AND MONTH >=
			   decode(sign(revision),
					  -1,
					  nvl(a_month, to_date(180001, 'YYYYMM')),
					  greatest(nvl(a_month, to_date(180001, 'YYYYMM')),
							   (SELECT DISTINCT trunc(nvl(decode(is_impairment, --If impairment revision we are building from Impairment date forward
																  1,
																  impairment_date,
																  decode(is_remeasurement,
																		 1,
																		 remeasurement_date,
																		 to_date(180001,
																				 'YYYYMM'))),
														   to_date(180001, 'YYYYMM')),
													   'month') --is_remeasurement on ls_ilr_stg overrides remeasurement date if = 0
								  FROM ls_ilr_options o, ls_ilr_stg stg
								 WHERE stg.ilr_id = l_ilr_rec.ilr_id
								   and stg.revision = l_ilr_rec.revision
								   and stg.set_of_books_id = l_ilr_rec.set_of_books_id
								   and o.ilr_id = stg.ilr_id
								   AND o.revision = stg.revision)));

			   PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows deleted from LS_ILR_SCHEDULE for Set of Books'||to_char(l_ilr_rec.set_of_books_id));
		end loop;


		L_STATUS := 'LOADING ls_ilr_schedule';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		insert into LS_ILR_SCHEDULE
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
          INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
          PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST,
          PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, INCENTIVE_MATH_AMOUNT,
          IDC_MATH_AMOUNT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
          BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL,
          ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
          UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS, IMPAIRMENT_ACTIVITY, BEGIN_ACCUM_IMPAIR, END_ACCUM_IMPAIR,
		  incentive_cap_amount)
         select A.ILR_ID,
                A.REVISION,
                A.SET_OF_BOOKS_ID,
                A.month,
                sum(RESIDUAL_AMOUNT),
                sum(TERM_PENALTY),
                sum(BPO_PRICE),
                sum(BEG_CAPITAL_COST),
                sum(END_CAPITAL_COST),
                sum(BEG_OBLIGATION),
                sum(END_OBLIGATION),
                sum(BEG_LT_OBLIGATION),
                sum(END_LT_OBLIGATION),
                sum(BEG_LIABILITY),
                sum(END_LIABILITY),
                sum(BEG_LT_LIABILITY),
                sum(END_LT_LIABILITY),
                sum(INTEREST_ACCRUAL),
                sum(PRINCIPAL_ACCRUAL),
                sum(INTEREST_PAID),
                sum(PRINCIPAL_PAID),
                sum(CONTINGENT_ACCRUAL1),
                sum(CONTINGENT_ACCRUAL2),
                sum(CONTINGENT_ACCRUAL3),
                sum(CONTINGENT_ACCRUAL4),
                sum(CONTINGENT_ACCRUAL5),
                sum(CONTINGENT_ACCRUAL6),
                sum(CONTINGENT_ACCRUAL7),
                sum(CONTINGENT_ACCRUAL8),
                sum(CONTINGENT_ACCRUAL9),
                sum(CONTINGENT_ACCRUAL10),
                sum(EXECUTORY_ACCRUAL1),
                sum(EXECUTORY_ACCRUAL2),
                sum(EXECUTORY_ACCRUAL3),
                sum(EXECUTORY_ACCRUAL4),
                sum(EXECUTORY_ACCRUAL5),
                sum(EXECUTORY_ACCRUAL6),
                sum(EXECUTORY_ACCRUAL7),
                sum(EXECUTORY_ACCRUAL8),
                sum(EXECUTORY_ACCRUAL9),
                sum(EXECUTORY_ACCRUAL10),
                sum(CONTINGENT_PAID1),
                sum(CONTINGENT_PAID2),
                sum(CONTINGENT_PAID3),
                sum(CONTINGENT_PAID4),
                sum(CONTINGENT_PAID5),
                sum(CONTINGENT_PAID6),
                sum(CONTINGENT_PAID7),
                sum(CONTINGENT_PAID8),
                sum(CONTINGENT_PAID9),
                sum(CONTINGENT_PAID10),
                sum(EXECUTORY_PAID1),
                sum(EXECUTORY_PAID2),
                sum(EXECUTORY_PAID3),
                sum(EXECUTORY_PAID4),
                sum(EXECUTORY_PAID5),
                sum(EXECUTORY_PAID6),
                sum(EXECUTORY_PAID7),
                sum(EXECUTORY_PAID8),
                sum(EXECUTORY_PAID9),
                sum(EXECUTORY_PAID10),
                A.IS_OM,
                SUM(BEG_DEFERRED_RENT),
                SUM(DEFERRED_RENT),
                SUM(END_DEFERRED_RENT),
                SUM(BEG_ST_DEFERRED_RENT),
                SUM(END_ST_DEFERRED_RENT),
                Sum(PRINCIPAL_REMEASUREMENT),
                Sum(LIABILITY_REMEASUREMENT),
                Sum(INCENTIVE_AMOUNT),
                Sum(INITIAL_DIRECT_COST),
                Sum(PREPAY_AMORTIZATION),
                Sum(BEG_PREPAID_RENT),
                Sum(PREPAID_RENT),
                Sum(END_PREPAID_RENT),
                SUM(INCENTIVE_MATH_AMOUNT),
                SUM(IDC_MATH_AMOUNT),
                SUM(ROU_ASSET_REMEASUREMENT),
                SUM(PARTIAL_TERM_GAIN_LOSS),
                SUM(ADDITIONAL_ROU_ASSET),
                Sum(BEG_ARREARS_ACCRUAL),
                Sum(ARREARS_ACCRUAL),
                Sum(END_ARREARS_ACCRUAL),
                Sum(REMAINING_PRINCIPAL),
                Sum(ST_OBLIGATION_REMEASUREMENT),
                Sum(LT_OBLIGATION_REMEASUREMENT),
                Sum(OBLIGATION_RECLASS),
                Sum(UNACCRUED_INTEREST_REMEASURE),
                Sum(UNACCRUED_INTEREST_RECLASS),
				Sum(IMPAIRMENT_ACTIVITY),
				Sum(BEG_ACCUM_IMPAIR),
				Sum(END_ACCUM_IMPAIR),
				Sum(incentive_cap_amount)
           from LS_ILR_ASSET_SCHEDULE_STG A
		   join LS_ILR_OPTIONS O on a.ilr_id = o.ilr_id
							  and a.revision = o.revision
		where month >= nvl(decode(o.is_impairment, 1, o.impairment_date, A_MONTH), TO_DATE(180001,'YYYYMM'))
          group by A.ILR_ID, A.REVISION, A.month, A.SET_OF_BOOKS_ID, A.IS_OM;

		PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows inserted into LS_ILR_SCHEDULE');

		L_STATUS:='Setting ILR beginning capital to 0';

		update ls_ilr_schedule z
		  set beg_capital_cost = 0
		  where (ilr_id, revision, set_of_books_id) in (
		 select ilr_id, revision, set_of_books_id
		from ls_ilr_asset_schedule_stg )
		and exists
		  (select 1 from
		  (select ilr_id, revision, set_of_books_id, month, row_number() over(partition by ilr_id, revision, set_of_books_id order by month) the_row
		  from ls_ilr_schedule ilrs
		  where (ilr_id, revision, set_of_books_id) in
			(select ilr_id, revision, set_of_books_id
			 from ls_ilr_asset_schedule_stg))
		  where the_row = 1
		and ilr_id = z.ilr_id
		and revision = z.revision
		and set_of_books_id = z.set_of_books_id
		and month = z.month);

		PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows updated on LS_ILR_SCHEDULE');

		L_STATUS := 'DELETING ls_asset_schedule';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

		--Delete by Set of Books since Remeasurement Date can be populated by Set of Books in ls_ilr_stg so would need to remove different datasets by ILR and SOB
		for l_ilr_rec in STAGED_ILRS
		loop
			l_status := 'Deleting prior Asset Schedule for ILR ID: '||to_char(l_ilr_rec.ilr_id)||'    Revision: ' ||to_char(l_ilr_rec.revision)||'    Set of Books: '||to_char(l_ilr_rec.set_of_books_id);
			DELETE FROM ls_asset_schedule
			 WHERE (ls_asset_id, revision, set_of_books_id) IN
				   (SELECT a.ls_asset_id, a.revision, a.set_of_books_id
					  FROM ls_ilr_asset_stg a
					 WHERE a.ilr_id = l_ilr_rec.ilr_id
					   AND a.revision = l_ilr_rec.revision
					   AND a.set_of_books_id = l_ilr_rec.set_of_books_id)
			   AND MONTH >=
				   decode(sign(revision),
						  -1,
						  nvl(a_month, to_date(180001, 'YYYYMM')),
						  greatest(nvl(a_month, to_date(180001, 'YYYYMM')),
								   (SELECT DISTINCT trunc(nvl(decode(is_impairment, --If impairment revision we are building from Impairment date forward
																	  1,
																	  impairment_date,
																	  decode(is_remeasurement,
																			 1,
																			 remeasurement_date,
																			 to_date(180001,
																					 'YYYYMM'))),
															   to_date(180001, 'YYYYMM')),
														   'month') --is_remeasurement on ls_ilr_stg overrides remeasurement date if = 0
									  FROM ls_ilr_options o, ls_ilr_stg stg
									 WHERE stg.ilr_id = l_ilr_rec.ilr_id
									   AND stg.revision = l_ilr_rec.revision
									   AND stg.set_of_books_id = l_ilr_rec.set_of_books_id
									   AND o.ilr_id = stg.ilr_id
									   AND o.revision = stg.revision)));

			PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows deleted from LS_ASSET_SCHEDULE for Set of Books'||to_char(l_ilr_rec.set_of_books_id));
		end loop;

		L_STATUS := 'LOADING ls_asset_schedule';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		insert into LS_ASSET_SCHEDULE
         (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
          INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
          PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST, INCENTIVE_MATH_AMOUNT, IDC_MATH_AMOUNT,
          PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
          PARTIAL_MONTH_PERCENT, BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL,
          REMAINING_PRINCIPAL, ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
          UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS, IMPAIRMENT_ACTIVITY, BEGIN_ACCUM_IMPAIR, END_ACCUM_IMPAIR,
		  incentive_cap_amount)
         select A.LS_ASSET_ID,
                A.REVISION,
                A.SET_OF_BOOKS_ID,
                A.month,
                RESIDUAL_AMOUNT,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                BEG_LIABILITY,
                END_LIABILITY,
                BEG_LT_LIABILITY,
                END_LT_LIABILITY,
                INTEREST_ACCRUAL,
                PRINCIPAL_ACCRUAL,
                INTEREST_PAID,
                PRINCIPAL_PAID,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                IS_OM,
                BEG_DEFERRED_RENT,
                DEFERRED_RENT,
                END_DEFERRED_RENT,
                BEG_ST_DEFERRED_RENT,
                END_ST_DEFERRED_RENT,
                PRINCIPAL_REMEASUREMENT,
                LIABILITY_REMEASUREMENT,
                INCENTIVE_AMOUNT,
                INITIAL_DIRECT_COST,
                INCENTIVE_MATH_AMOUNT,
                IDC_MATH_AMOUNT,
                PREPAY_AMORTIZATION,
                BEG_PREPAID_RENT,
                PREPAID_RENT,
                END_PREPAID_RENT,
                ROU_ASSET_REMEASUREMENT,
                PARTIAL_TERM_GAIN_LOSS,
                ADDITIONAL_ROU_ASSET,
                partial_month_percent,
                BEG_ARREARS_ACCRUAL,
                ARREARS_ACCRUAL,
                END_ARREARS_ACCRUAL,
                REMAINING_PRINCIPAL,
                ST_OBLIGATION_REMEASUREMENT,
                LT_OBLIGATION_REMEASUREMENT,
                OBLIGATION_RECLASS,
                UNACCRUED_INTEREST_REMEASURE,
                UNACCRUED_INTEREST_RECLASS,
				IMPAIRMENT_ACTIVITY,
				BEG_ACCUM_IMPAIR,
				END_ACCUM_IMPAIR,
				incentive_cap_amount
           from LS_ILR_ASSET_SCHEDULE_STG A
		   join LS_ILR_OPTIONS O on a.ilr_id = o.ilr_id
								and a.revision = o.revision
			where month >= nvl(decode(is_impairment, 1, impairment_date, A_MONTH), TO_DATE(180001,'YYYYMM'));
      PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows inserted into from LS_ASSET_SCHEDULE');

      /* WMD */
      L_STATUS:='Setting Asset beginning capital to 0';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      update ls_asset_schedule z
          set beg_capital_cost = 0
          where (ls_asset_id, revision, set_of_books_id) in (
         select ls_asset_id, revision, set_of_books_id
      from ls_ilr_asset_schedule_stg )
      and exists
          (select 1 from
          (select ls_asset_id, revision, set_of_books_id, month, row_number() over(partition by ls_asset_id, revision, set_of_books_id order by month) the_row
          from ls_asset_schedule las
          where (ls_asset_id, revision, set_of_books_id) in (
         select ls_asset_id, revision, set_of_books_id
      from ls_ilr_asset_schedule_stg z))
      where the_row = 1
      and ls_asset_id = z.ls_asset_id
      and revision = z.revision
      and set_of_books_id = z.set_of_books_id
      and month = z.month);

       PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows updated on LS_ASSET_SCHEDULE');

      for L_ILR in (select distinct ilr_id, revision from ls_ilr_stg) LOOP

         L_STATUS:='Checking to see if ILR is sinking fund';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

        select ll.lease_type_id, ilr.company_id, ig.use_floating_rate
        into L_LEASE_TYPE, L_COMPANY_ID, L_FLOAT_RATE
        from ls_lease ll, ls_ilr ilr, ls_ilr_group ig
        where ilr.lease_id = ll.lease_id
          and ilr.ilr_id = L_ILR.ilr_id
      and ilr.ilr_group_id = ig.ilr_group_id;

        if A_MONTH is null then
          select min(PAYMENT_TERM_DATE)
          into L_MONTH
          from LS_ILR_PAYMENT_TERM
          where ilr_id = L_ILR.ILR_ID
            AND revision = L_ILR.REVISION;
        else
         L_MONTH:= A_MONTH;
        end if;

        if L_LEASE_TYPE = 3 then
      L_STATUS:='Calling Sinking fund schedules calc';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
          L_STATUS:=PKG_LEASE_CALC.F_FLOATING_RATE_SF_ACCRUAL(-1, L_COMPANY_ID, L_MONTH, L_ILR.ILR_ID, L_ILR.REVISION);
          IF L_STATUS <> 'OK' THEN
            PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
            PKG_PP_LOG.P_WRITE_MESSAGE('Error building sinking fund schedules: ' || L_STATUS);
            RETURN L_STATUS;
          END IF;
        elsif L_LEASE_TYPE = 5 and L_FLOAT_RATE = 1 then
      L_STATUS := 'Calling Fixed Principal Floating Interest schedules calc';
      L_STATUS := PKG_LEASE_CALC.F_FLOATING_RATE_FIXED_PRIN_PAY(-1, L_COMPANY_ID, L_MONTH, L_ILR.ILR_ID, L_ILR.REVISION);
      IF L_STATUS <> 'OK' then
      IF L_STATUS <> 'NO BUCKET' then
        PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
        PKG_PP_LOG.P_WRITE_MESSAGE('Error building Fixed Principal with Floating Rates schedules: ' || L_STATUS);
        RETURN L_STATUS;
      else
      --  If no bucket defined for calc, log it and keep going.
        pkg_pp_log.p_write_message('No Floating Rates bucket defined for Floating Rates Calculation for ILR ID: '||to_char(l_ilr.ilr_id));
      end if;
          END IF;

    elsif L_LEASE_TYPE <> 3 and L_FLOAT_RATE = 1 then
    /* CJS 4/23/15 Call floating rate by ILR for variable leases */
      L_STATUS:='Calling floating rate schedules calc';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_STATUS:=PKG_LEASE_CALC.F_FLOATING_RATE_PAYMENT(-1, L_COMPANY_ID, L_MONTH, L_ILR.ILR_ID, L_ILR.REVISION);
      IF L_STATUS <> 'OK' THEN
            PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
            PKG_PP_LOG.P_WRITE_MESSAGE('Error building floating rate schedules: ' || L_STATUS);
            RETURN L_STATUS;
          END IF;
        end if;
      end loop;

      -- update term penalty on ls_asset
      L_STATUS := 'UPDATING asset termination penalty amount';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      update LS_ASSET A
         set A.TERMINATION_PENALTY_AMOUNT =
              (select min(M.TERM_PENALTY) from LS_ILR_ASSET_STG M where M.LS_ASSET_ID = A.LS_ASSET_ID)
       where exists (select 1 from LS_ILR_ASSET_STG M where M.LS_ASSET_ID = A.LS_ASSET_ID);

      -- update the ls_ilr_amounts_set_of_books
      L_STATUS := 'Deleting from ls_ilr_amounts_set_of_books';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_AMOUNTS_SET_OF_BOOKS
       where (ILR_ID, REVISION, set_of_books_id) in (select A.ILR_ID, A.REVISION, a.set_of_books_id from LS_ILR_stg A);

    L_STATUS:='Inserting into ls_ilr_amounts_set_of_books';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          IS_OM, CURRENT_LEASE_COST)
         select L.ILR_ID,
                L.REVISION,
                L.SET_OF_BOOKS_ID,
                NVL(L.NPV, 0),
                NVL(100 * (POWER((1 + L.IRR), 12) - 1), 0),
                sum(A.END_CAPITAL_COST),
                L.IS_OM,
                L.FMV
           from LS_ILR_ASSET_SCHEDULE_STG A, LS_ILR_STG L
          where A.ID = 1
            and L.ILR_ID = A.ILR_ID
            and L.REVISION = A.REVISION
            and L.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
          group by L.ILR_ID,
                   L.REVISION,
                   L.SET_OF_BOOKS_ID,
                   NVL(L.NPV, 0),
                   NVL(100 * (POWER((1 + L.IRR), 12) - 1), 0),
                   L.IS_OM,
                   L.FMV;

      L_STATUS:='Inserting missing amounts into ls_ilr_amounts_set_of_books';
      insert into ls_ilr_amounts_set_of_books
      (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          IS_OM, CURRENT_LEASE_COST)
      select a.ILR_ID, stg.REVISION, a.SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          a.IS_OM, CURRENT_LEASE_COST
      from ls_ilr_amounts_set_of_books a, ls_ilr ilr, ls_ilr_stg stg
      where a.ilr_id = stg.ilr_id
        and a.set_of_books_id = stg.set_of_books_id
        and a.ilr_id = ilr.ilr_id
        and a.revision = ilr.current_revision
        and not exists (select 1 from ls_ilr_amounts_set_of_books where ilr_id = a.ilr_id and revision = stg.revision and set_of_books_id = a.set_of_books_id);

      L_STATUS:='Updating Current Lease Cost on LS_ASSET_SCHEDULE';
      update
      (select a.ls_asset_id, a.revision, a.month, a.current_lease_cost, lg.require_components, set_of_books_id
      from ls_asset_schedule a, ls_asset la, ls_ilr ilr, ls_lease ll, ls_lease_group lg
      where a.ls_asset_id = la.ls_asset_id
        and la.ilr_id = ilr.ilr_id
        and ilr.lease_id = ll.lease_id
        and ll.lease_group_id = lg.lease_group_id) las
      set current_lease_cost =
      case when las.require_components = 1 then
      (select sum(cc.amount)
      from ls_component_charge cc, ls_component lc, ls_asset la, ls_ilr ilr, ls_lease ll
      where las.ls_asset_id = la.ls_asset_id
        and cc.component_id = lc.component_id
        and lc.ls_asset_id = la.ls_asset_id
        and la.ilr_id = ilr.ilr_id
        and ilr.lease_id = ll.lease_id
        and cc.interim_interest_start_date is not null
        and las.month >= add_months(trunc(cc.interim_interest_start_date, 'month'), case when extract(day from cc.interim_interest_start_date) >= ll.cut_off_day then 1 else 0 end))
      else
        (select fmv
        from ls_asset la
        where la.ls_asset_id = las.ls_asset_id)
      end
      where (ls_asset_id, revision, set_of_books_id) in (select ls_asset_id, revision, set_of_books_id from ls_ilr_asset_schedule_stg);

      L_STATUS:='Updating Current Lease Cost on LS_ILR_SCHEDULE';
      update
      (select i.ilr_id, i.revision, i.month, i.current_lease_cost, lg.require_components, set_of_books_id
      from ls_ilr_schedule i, ls_ilr ilr, ls_lease ll, ls_lease_group lg
      where i.ilr_id = ilr.ilr_id
        and ilr.lease_id = ll.lease_id
        and ll.lease_group_id = lg.lease_group_id) lis
      set current_lease_cost =
      case when lis.require_components = 1 then
      (select sum(cc.amount)
      from ls_component_charge cc, ls_component lc, ls_asset la, ls_ilr ilr, ls_lease ll
      where lis.ilr_id = ilr.ilr_id
        and cc.component_id = lc.component_id
        and lc.ls_asset_id = la.ls_asset_id
        and la.ilr_id = ilr.ilr_id
        and ilr.lease_id = ll.lease_id
        and cc.interim_interest_start_date is not null
        and lis.month >= add_months(trunc(cc.interim_interest_start_date, 'month'), case when extract(day from cc.interim_interest_start_date) >= ll.cut_off_day then 1 else 0 end))
      else
        (select fmv
        from LS_ILR_STG STG
        WHERE STG.ILR_ID = LIS.ILR_ID
      and stg.set_of_books_id = lis.set_of_books_id
          AND STG.REVISION = LIS.REVISION)
      end
      where (ilr_id, revision, set_of_books_id) in (select ilr_id, revision, set_of_books_id from ls_ilr_stg);

      L_STATUS:='Setting residual amount to the guarantee';
      update ls_asset_schedule a
      set residual_amount =
      (select nvl(guaranteed_residual_amount,0)
       from ls_asset la
       where la.ls_asset_id = a.ls_asset_id)
      where month = (
        select max(month)
        from ls_asset_schedule
        where ls_asset_id = a.ls_asset_id
        and set_of_books_id = a.set_of_books_id
        and revision = a.revision)
       and (ls_asset_id, revision, set_of_books_id) in (select ls_asset_id, revision, set_of_books_id from ls_ilr_asset_schedule_stg);
       L_STATUS:='Setting ILR schedule residual amount to the guarantee';
       merge into (select * from ls_ilr_schedule where (ilr_id, revision, set_of_books_id) in (select ilr_id, revision, set_of_books_id from ls_ilr_asset_schedule_stg)) a
      using (select la.ilr_id, las.revision, month, las.set_of_books_id, sum(nvl(residual_amount,0)) total_resid
       from ls_asset_schedule las, ls_asset la
       where la.ls_asset_id = las.ls_asset_id
        and la.approved_revision = las.revision
        and nvl(las.residual_amount,0) <> 0
        and (la.ls_asset_id, las.revision, las.set_of_books_id) in (select ls_asset_id, revision, set_of_books_id from ls_ilr_asset_schedule_stg)
       group by la.ilr_id, las.revision, las.month, las.set_of_books_id) b
    on (a.ilr_id = b.ilr_id and a.revision = b.revision and a.month = b.month and a.set_of_books_id = b.set_of_books_id)
    when matched then update set a.residual_amount = b.total_resid;

    -- Find the Depr Group and assign to the Asset since the BPO Extend and Depr Calc depend on it
    L_STATUS := 'UPDATING depr group.';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    for L_ILR in (select distinct ilr_id, revision from ls_ilr_stg) loop

        PKG_PP_LOG.P_WRITE_MESSAGE('Entering P_GET_LEASE_DEPR');
        PKG_LEASE_DEPR.P_GET_LEASE_DEPR(L_ILR.ILR_ID, L_ILR.REVISION);

    end loop;

  L_STATUS:='Starting the BPO function.';
    FOR I IN (
              SELECT DISTINCT LS_ASSET_ID, ILR_ID, REVISION
              FROM LS_ILR_ASSET_SCHEDULE_STG
             )
    LOOP

      L_RTN := F_BPO_SCHEDULE_EXTEND(I.LS_ASSET_ID, I.ILR_ID, I.REVISION);

      IF L_RTN <> 'OK' THEN
        PKG_PP_LOG.P_WRITE_MESSAGE('Error extending schedules for LS_ASSET_ID: ' || I.LS_ASSET_ID || ' ILR_ID: ' || I.ILR_ID || ' Revision: ' || I.REVISION);
        ROLLBACK;
      END IF;

    END LOOP;

    L_STATUS:='The BPO function ended.';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

	L_STATUS :='Updating Partial Termination Flag on ILR Options';
	PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);


    UPDATE LS_ILR_OPTIONS a
    SET    partial_termination = 1, remeasurement_type = 1
    WHERE  ( ilr_id, revision ) IN
         ( SELECT stg.ilr_id,
            stg.revision
         FROM   LS_ILR_SCHEDULE sched,
            LS_ILR_STG stg
         WHERE  Nvl( liability_remeasurement, 0 ) < 0 AND
            sched.ilr_id = stg.ilr_id AND
            sched.revision = stg.revision AND
            sched.set_of_books_id = stg.set_of_books_id )
    and nvl(remeasurement_type,0) not in (2, 3)
    and nvl(remeasure_calc_gain_loss,1) = 1;

    L_STATUS :='Updating Partial Termination Flag on ILR Options when Gain/Loss should not be calculated';
	PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    -- Set to a regular remeasurement when Gain/Loss should not be calculated
    UPDATE LS_ILR_OPTIONS a
    SET    partial_termination = null, remeasurement_type = 0
    WHERE  ( ilr_id, revision ) IN
         ( SELECT stg.ilr_id,
            stg.revision
         FROM   LS_ILR_SCHEDULE sched,
            LS_ILR_STG stg
         WHERE  Nvl( liability_remeasurement, 0 ) < 0 AND
            sched.ilr_id = stg.ilr_id AND
            sched.revision = stg.revision AND
            sched.set_of_books_id = stg.set_of_books_id )
    and nvl(remeasurement_type,0) not in (2,3)
    and nvl(remeasure_calc_gain_loss,1) = 0;

	L_STATUS :='Updating Partial Termination Flag on ILR Options (2)';
	PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

	UPDATE LS_ILR_OPTIONS a
	SET partial_termination = null, remeasurement_type = 0
	WHERE ( ilr_id, revision ) IN
		( SELECT stg.ilr_id,
				 stg.revision
		FROM LS_ILR_SCHEDULE sched,
			 LS_ILR_STG stg
		WHERE Nvl( liability_remeasurement, 0 ) > 0 AND
			sched.ilr_id = stg.ilr_id AND
			sched.revision = stg.revision AND
			sched.set_of_books_id = stg.set_of_books_id )
	and nvl(remeasurement_type,0) not in (2,3)
	and nvl(remeasure_calc_gain_loss,1) = 1;

    L_STATUS := 'UPDATING depr group and depr forecast. Beginning depr logging.';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

    l_status := 'Clearing LS_DEPR_INFO_TMP';
    pkg_pp_log.p_write_message(l_status);
    delete from LS_DEPR_INFO_TMP;

    l_status := 'Populating LS_DEPR_INFO_TMP';
    INSERT INTO LS_DEPR_INFO_TMP(ilr_id,
                                  ls_asset_id,
                                  set_of_books_id,
                                  revision,
                                  MONTH,
                                  end_capital_cost,
                                  depr_group_id,
                                  economic_life,
                                  set_of_books,
                                  mid_period_conv,
                                  mid_period_method,
                                  fasb_cap_type_id)
    SELECT  ilr_id,
            ls_asset_id,
            set_of_books_id,
            revision,
            month,
            end_capital_cost,
            depr_group_id,
            economic_life,
            set_of_books,
            mid_period_conv,
            mid_period_method,
            fasb_cap_type_id
      from
      (
        SELECT  la.ilr_id,
                las.ls_asset_id,
                las.set_of_books_id,
                las.revision,
                las.MONTH,
                las.end_capital_cost,
                la.depr_group_id,
                la.economic_life, /* WMD */
                row_number() OVER(PARTITION BY las.ls_asset_id, las.set_of_books_id, las.revision ORDER BY las.MONTH) AS the_row,
                dg.mid_period_conv /* CJS 4/11/17 */,
                sob.DESCRIPTION set_of_books,
                LOWER(dg.mid_period_method) mid_period_method,
                sob_map.fasb_cap_type_id
        from ls_asset_schedule las,
              ls_asset la,
              depr_group dg,
              ls_ilr_options lio,
              ls_fasb_cap_type_sob_map sob_map,
              set_of_books sob
        where la.depr_group_id = dg.depr_group_id
        and las.ls_asset_id = la.ls_asset_id
        AND lio.ilr_id = la.ilr_id
        AND lio.revision = las.revision
        AND sob_map.lease_cap_type_id = lio.lease_cap_type_id
        AND sob_map.set_of_books_id = las.set_of_books_id
        and las.set_of_books_id = sob.set_of_books_id
        and (la.ilr_id, las.revision) in (SELECT ilr_id, revision
                    FROM ls_ilr_stg)
		and (lio.ilr_id, lio.revision, sob_map.set_of_books_id) not in (SELECT distinct t.ilr_id, -- Exclude for IFRS Remeasurement where SOB is not included
																					  t.revision,
																					  m.set_of_books_id
																				 FROM ls_ilr_payment_term t,
																					  ls_vp_escalation_sob_map m,
																					  ls_ilr_options o,
																					  ls_ilr i,
																					  ls_Ilr_stg stg,
																					  (SELECT control_value,
																							  company_id
																						 FROM pp_system_control_companies
																						WHERE upper(control_name) = 'IFRS SET OF BOOKS REMEASUREMENT') sys
																				WHERE o.ilr_id = stg.ilr_id
																				  AND o.revision = stg.revision
																				  AND t.ilr_id = o.ilr_id
																				  AND t.revision = o.revision
																				  AND t.ilr_id = i.ilr_id
																				  AND t.escalation = m.variable_payment_id
																				  and m.set_of_books_id = stg.set_of_books_id
																				  AND i.company_id = sys.company_id
																				  AND lower(sys.control_value) = 'yes'
																				  AND m.include_in_esc = 0
																				  AND o.remeasurement_type = 3)
        )
      where the_row = 1;

    for L_ILR in (select distinct ilr_id, revision from ls_ilr_stg) loop

        pkg_pp_log.p_write_message('Entering P_FCST_LEASE_USE_TEMP_TABLE');
        PKG_LEASE_DEPR.P_FCST_LEASE_USE_TEMP_TABLE(L_ILR.ILR_ID, L_ILR.REVISION);
        PKG_PP_LOG.P_WRITE_MESSAGE('Finished P_FCST_LEASE_USE_TEMP_TABLE');

        --start log back up again
        PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());

    end loop;

    l_status := 'Clearing LS_DEPR_INFO_TMP';
    pkg_pp_log.p_write_message(l_status);
    delete from LS_DEPR_INFO_TMP;

    L_STATUS:='Updating BEG/END ROU Asset LS_ASSET_SCHEDULE';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

    merge INTO LS_ASSET_SCHEDULE z
        USING ( SELECT distinct ls_asset_id,revision,set_of_books_id,begin_reserve,end_reserve,month
            FROM   LS_DEPR_FORECAST ldf
            where (ldf.ls_asset_id, ldf.revision, ldf.set_of_books_id) in (select ls_asset_id, revision, set_of_books_id
            from ls_ilr_asset_schedule_stg) ) a
        ON ( z.ls_asset_id = a.ls_asset_id AND z.revision = a.revision AND z.set_of_books_id = a.set_of_books_id AND z.month=a.month)
        WHEN matched THEN
          UPDATE SET z.beg_net_ROU_asset = nvl(z.beg_capital_cost,0) - nvl(a.begin_reserve,0) - nvl(z.begin_accum_impair,0),
                     z.end_net_ROU_asset = nvl(z.end_capital_cost,0) - nvl(a.end_reserve,0) - nvl(z.end_accum_impair,0);

    update ls_asset_schedule las
      set beg_net_rou_asset = nvl(beg_capital_cost,0)
    where beg_net_rou_asset is null
    and (las.ls_asset_id, las.revision, las.set_of_books_id) in (select ls_asset_id, revision, set_of_books_id from ls_ilr_asset_schedule_stg);

    update ls_asset_schedule las
      set end_net_rou_asset = nvl(end_capital_cost,0)
    where end_net_rou_asset is null
    and (las.ls_asset_id, las.revision, las.set_of_books_id) in (select ls_asset_id,revision,set_of_books_id from ls_ilr_asset_schedule_stg);

    L_STATUS:='Updating BEG/END ROU Asset LS_ILR_SCHEDULE';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

    merge INTO LS_ILR_SCHEDULE z
        USING ( SELECT la.ilr_id as ilr_id,ldf.revision as revision,ldf.set_of_books_id as set_of_books_id,ldf.month as month,
                sum(ldf.begin_reserve) as beg_res,sum(ldf.end_reserve) as end_res
            FROM   LS_DEPR_FORECAST ldf,ls_asset la
            where (ldf.ls_asset_id, ldf.revision, ldf.set_of_books_id) in (select ls_asset_id, revision, set_of_books_id
            from ls_ilr_asset_schedule_stg)
            and (la.ilr_id, ldf.revision, ldf.set_of_books_id) in (select ilr_id,revision, set_of_books_id
            from ls_ilr_schedule_stg)
            and ldf.ls_asset_id = la.ls_asset_id
            group by la.ilr_id,ldf.revision,ldf.set_of_books_id,month) a
        ON ( z.ilr_id = a.ilr_id AND z.revision = a.revision AND z.set_of_books_id = a.set_of_books_id AND z.month=a.month)
        WHEN matched THEN
          UPDATE SET z.beg_net_ROU_asset = nvl(z.beg_capital_cost,0) - nvl(a.beg_res,0) - nvl(z.begin_accum_impair,0),
                     z.end_net_ROU_asset = nvl(z.end_capital_cost,0) - nvl(a.end_res,0) - nvl(z.end_accum_impair, 0);

      update LS_ILR_SCHEDULE lis
      set beg_net_ROU_asset = nvl(beg_capital_cost,0)
      where beg_net_ROU_asset is null
      and (lis.ilr_id, lis.revision, lis.set_of_books_id) in (select ilr_id,revision, set_of_books_id from ls_ilr_schedule_stg);

      update LS_ILR_SCHEDULE lis
      set end_net_ROU_asset = nvl(end_capital_cost,0)
      where end_net_ROU_asset is null
      and (lis.ilr_id, lis.revision, lis.set_of_books_id) in (select ilr_id,revision, set_of_books_id from ls_ilr_schedule_stg);

    L_STATUS :='Updating Payment Month and Escalation Month Flags on Schedule';
	UPDATE ls_ilr_schedule sched
	   SET (sched.payment_month, sched.escalation_month, sched.escalation_pct) =
		   (SELECT payment_month,
				   escalation_month,
				   escalation_pct
			  FROM ls_ilr_schedule_stg stg
			 WHERE sched.ilr_id = stg.ilr_id
			   AND sched.revision = stg.revision
			   AND sched.set_of_books_id = stg.set_of_books_id
			   AND sched.month = stg.month)
	 WHERE exists (SELECT 1 FROM ls_ilr_schedule_stg stg2
						where stg2.ilr_id = sched.ilr_id
						and stg2.revision = sched.revision
						and stg2.set_of_books_id = sched.set_of_books_id
						and stg2.month = sched.month);


    -- Calculate Taxes
    PKG_PP_LOG.P_WRITE_MESSAGE('Starting Tax Calculation');
    L_RTN := F_CALC_TAXES();

    if L_RTN <> 'OK' then
      PKG_PP_LOG.P_WRITE_MESSAGE(L_RTN);
      return L_RTN;
    end if;


      --Refresh dense currency rates in case there is a new min/max month on the schedule tables
      --(which determine the months for which dense currency rates will be generated)
      SELECT Count(1)
      INTO L_COUNT
          FROM (
                SELECT 1
                FROM (
                      SELECT MAX(TRUNC(MONTH, 'MONTH')) as max_month
                      FROM ls_asset_schedule
                      UNION ALL
                      SELECT MAX(TRUNC(MONTH, 'MONTH')) AS max_month
                      FROM ls_ilr_schedule
                     )
                WHERE max_month > (SELECT MAX(TRUNC(exchange_date, 'MONTH'))
                                   FROM currency_rate_default_dense)
               UNION ALL
                SELECT 1
                FROM (SELECT MIN(TRUNC(MONTH, 'MONTH')) AS min_month
                      FROM ls_asset_schedule
                      UNION ALL
                      SELECT MIN(TRUNC(MONTH, 'MONTH')) AS min_month
                      FROM ls_ilr_schedule
                     )
                WHERE min_month < (SELECT MIN(TRUNC(exchange_date, 'MONTH'))
                                   FROM currency_rate_default_dense)
               );

          if L_COUNT > 0 THEN
        L_STATUS:='Refreshing Currency Rates';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    p_refresh_curr_rate_dflt_dense;
          END IF;

        L_STATUS:='Currency function ended.';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_SAVE_SCHEDULES;

	FUNCTION f_process_asset_trf(a_from_asset_id     NUMBER,
								 a_to_asset_id       NUMBER,
								 a_from_ilr_revision NUMBER,
								 a_to_ilr_revision   NUMBER,
								 a_percent           NUMBER,
								 a_from_ilr_id       NUMBER,
								 a_to_ilr_id         NUMBER,
								 a_month             DATE := NULL)
	  RETURN VARCHAR2 IS
	  l_msg    VARCHAR2(2000);
	  l_status VARCHAR2(2000);
	BEGIN
	  pkg_pp_log.p_start_log(p_process_id => pkg_lease_common.f_getprocess());
	  pkg_pp_log.p_write_message('Processing Asset Transfer');
	  pkg_pp_log.p_write_message('   FROM ASSET: ' || to_char(a_from_asset_id));
	  pkg_pp_log.p_write_message('   TO ASSET: ' || to_char(a_to_asset_id));
	  pkg_pp_log.p_write_message('   FROM ILR: ' || to_char(a_from_ilr_id));
	  pkg_pp_log.p_write_message('   TO ILR: ' || to_char(a_to_ilr_id));
	  pkg_pp_log.p_write_message('   FROM ILR Revision: ' || to_char(a_from_ilr_revision));
	  pkg_pp_log.p_write_message('   TO ILR Revision: ' || to_char(a_to_ilr_revision));
	  pkg_pp_log.p_write_message('   PERCENT: ' || to_char(a_percent));
	  pkg_pp_log.p_write_message('   In Service Month: ' || to_char(a_month, 'YYYYMM'));

	EXCEPTION
	  WHEN OTHERS THEN
		l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
		pkg_pp_log.p_write_message(l_status);

		RETURN l_status;
	END f_process_asset_trf;

   function F_GET_II_BEGIN_DATE(A_ILR_ID   number,
                                A_REVISION number) return date is
      RTN_DATE date;
   begin
      select INTERIM_INTEREST_BEGIN_DATE
        into RTN_DATE
        from LS_ILR_PAYMENT_TERM
       where ILR_ID = A_ILR_ID
         and REVISION = A_REVISION
         and PAYMENT_TERM_TYPE_ID = 1;

      return RTN_DATE;

   exception
      when NO_DATA_FOUND then
         return 'JUL-4-1776';
      when TOO_MANY_ROWS then
         return 'JUL-4-1776';

   end F_GET_II_BEGIN_DATE;

   function F_MAKE_II(A_ILR_ID   number,
                      A_REVISION number) return number is
      RTN number;
   begin
      select MAKE_II_PAYMENT
        into RTN
        from LS_ILR_PAYMENT_TERM
       where ILR_ID = A_ILR_ID
         and REVISION = A_REVISION
         and PAYMENT_TERM_TYPE_ID = 1;

      return RTN;

   end F_MAKE_II;

function F_SYNC_ILR_SCHEDULE(A_ILR_ID IN NUMBER, A_REVISION IN NUMBER) RETURN VARCHAR2
  is
  location varchar2(30000);
  begin

  PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
  LOCATION:='Deleting existing ILR Schedule';
  PKG_PP_LOG.P_WRITE_MESSAGE(LOCATION);

  delete from ls_ilr_schedule
  where ilr_id = A_ILR_ID
    and revision = A_REVISION;

  LOCATION:='Merging all asset schedules into ILR schedule';
  pkg_pp_log.p_write_message('Loading ILR:'||a_ilr_id||' Revision: '|| a_revision);
  merge into ls_ilr_schedule a
  using (
  select    la.ilr_id,
            A_REVISION as revision,
            las.set_of_books_id,
            las.month,
            las.is_om,
            sum(nvl(las.RESIDUAL_AMOUNT,0)) as RESIDUAL_AMOUNT,
            sum(nvl(las.TERM_PENALTY,0)) as TERM_PENALTY,
            sum(nvl(las.BPO_PRICE,0)) as BPO_PRICE,
            sum(nvl(las.BEG_CAPITAL_COST,0)) as BEG_CAPITAL_COST,
            sum(nvl(las.END_CAPITAL_COST,0)) as END_CAPITAL_COST,
            sum(nvl(las.BEG_OBLIGATION,0)) as BEG_OBLIGATION,
            sum(nvl(las.END_OBLIGATION,0)) as END_OBLIGATION,
            sum(nvl(las.BEG_LT_OBLIGATION,0)) as BEG_LT_OBLIGATION,
            sum(nvl(las.END_LT_OBLIGATION,0)) as END_LT_OBLIGATION,
            sum(nvl(las.BEG_LIABILITY,0)) as BEG_LIABILITY,
            sum(nvl(las.END_LIABILITY,0)) as END_LIABILITY,
            sum(nvl(las.BEG_LT_LIABILITY,0)) as BEG_LT_LIABILITY,
            sum(nvl(las.END_LT_LIABILITY,0)) as END_LT_LIABILITY,
            sum(nvl(las.INTEREST_ACCRUAL,0)) as INTEREST_ACCRUAL,
            sum(nvl(las.PRINCIPAL_ACCRUAL,0)) as PRINCIPAL_ACCRUAL,
            sum(nvl(las.INTEREST_PAID,0)) as INTEREST_PAID,
            sum(nvl(las.PRINCIPAL_PAID,0)) as PRINCIPAL_PAID,
            sum(nvl(las.EXECUTORY_ACCRUAL1,0)) as EXECUTORY_ACCRUAL1,
            sum(nvl(las.EXECUTORY_ACCRUAL2,0)) as EXECUTORY_ACCRUAL2,
            sum(nvl(las.EXECUTORY_ACCRUAL3,0)) as EXECUTORY_ACCRUAL3,
            sum(nvl(las.EXECUTORY_ACCRUAL4,0)) as EXECUTORY_ACCRUAL4,
            sum(nvl(las.EXECUTORY_ACCRUAL5,0)) as EXECUTORY_ACCRUAL5,
            sum(nvl(las.EXECUTORY_ACCRUAL6,0)) as EXECUTORY_ACCRUAL6,
            sum(nvl(las.EXECUTORY_ACCRUAL7,0)) as EXECUTORY_ACCRUAL7,
            sum(nvl(las.EXECUTORY_ACCRUAL8,0)) as EXECUTORY_ACCRUAL8,
            sum(nvl(las.EXECUTORY_ACCRUAL9,0)) as EXECUTORY_ACCRUAL9,
            sum(nvl(las.EXECUTORY_ACCRUAL10,0)) as EXECUTORY_ACCRUAL10,
            sum(nvl(las.EXECUTORY_PAID1,0)) as EXECUTORY_PAID1,
            sum(nvl(las.EXECUTORY_PAID2,0)) as EXECUTORY_PAID2,
            sum(nvl(las.EXECUTORY_PAID3,0)) as EXECUTORY_PAID3,
            sum(nvl(las.EXECUTORY_PAID4,0)) as EXECUTORY_PAID4,
            sum(nvl(las.EXECUTORY_PAID5,0)) as EXECUTORY_PAID5,
            sum(nvl(las.EXECUTORY_PAID6,0)) as EXECUTORY_PAID6,
            sum(nvl(las.EXECUTORY_PAID7,0)) as EXECUTORY_PAID7,
            sum(nvl(las.EXECUTORY_PAID8,0)) as EXECUTORY_PAID8,
            sum(nvl(las.EXECUTORY_PAID9,0)) as EXECUTORY_PAID9,
            sum(nvl(las.EXECUTORY_PAID10,0)) as EXECUTORY_PAID10,
            sum(nvl(las.CONTINGENT_ACCRUAL1,0)) as CONTINGENT_ACCRUAL1,
            sum(nvl(las.CONTINGENT_ACCRUAL2,0)) as CONTINGENT_ACCRUAL2,
            sum(nvl(las.CONTINGENT_ACCRUAL3,0)) as CONTINGENT_ACCRUAL3,
            sum(nvl(las.CONTINGENT_ACCRUAL4,0)) as CONTINGENT_ACCRUAL4,
            sum(nvl(las.CONTINGENT_ACCRUAL5,0)) as CONTINGENT_ACCRUAL5,
            sum(nvl(las.CONTINGENT_ACCRUAL6,0)) as CONTINGENT_ACCRUAL6,
            sum(nvl(las.CONTINGENT_ACCRUAL7,0)) as CONTINGENT_ACCRUAL7,
            sum(nvl(las.CONTINGENT_ACCRUAL8,0)) as CONTINGENT_ACCRUAL8,
            sum(nvl(las.CONTINGENT_ACCRUAL9,0)) as CONTINGENT_ACCRUAL9,
            sum(nvl(las.CONTINGENT_ACCRUAL10,0)) as CONTINGENT_ACCRUAL10,
            sum(nvl(las.CONTINGENT_PAID1,0)) as CONTINGENT_PAID1,
            sum(nvl(las.CONTINGENT_PAID2,0)) as CONTINGENT_PAID2,
            sum(nvl(las.CONTINGENT_PAID3,0)) as CONTINGENT_PAID3,
            sum(nvl(las.CONTINGENT_PAID4,0)) as CONTINGENT_PAID4,
            sum(nvl(las.CONTINGENT_PAID5,0)) as CONTINGENT_PAID5,
            sum(nvl(las.CONTINGENT_PAID6,0)) as CONTINGENT_PAID6,
            sum(nvl(las.CONTINGENT_PAID7,0)) as CONTINGENT_PAID7,
            sum(nvl(las.CONTINGENT_PAID8,0)) as CONTINGENT_PAID8,
            sum(nvl(las.CONTINGENT_PAID9,0)) as CONTINGENT_PAID9,
            sum(nvl(las.CONTINGENT_PAID10,0)) as CONTINGENT_PAID10,
            SUM(NVL(LAS.CURRENT_LEASE_COST,0)) AS CURRENT_LEASE_COST,
            SUM(NVL(las.BEG_DEFERRED_RENT,0)) as BEG_DEFERRED_RENT,
            SUM(NVL(las.DEFERRED_RENT,0)) as DEFERRED_RENT,
            SUM(NVL(las.END_DEFERRED_RENT,0)) as END_DEFERRED_RENT,
            SUM(NVL(las.BEG_ST_DEFERRED_RENT,0)) as BEG_ST_DEFERRED_RENT,
            SUM(NVL(las.END_ST_DEFERRED_RENT,0)) as END_ST_DEFERRED_RENT,
            SUM(NVL(las.INCENTIVE_AMOUNT,0)) as INCENTIVE_AMOUNT,
            SUM(NVL(las.INITIAL_DIRECT_COST,0)) as INITIAL_DIRECT_COST,
            SUM(NVL(las.PRINCIPAL_REMEASUREMENT,0)) as PRINCIPAL_REMEASUREMENT,
            SUM(NVL(las.LIABILITY_REMEASUREMENT,0)) as LIABILITY_REMEASUREMENT,
            SUM(NVL(las.BEG_PREPAID_RENT,0)) as BEG_PREPAID_RENT,
            SUM(NVL(las.PREPAY_AMORTIZATION,0)) as PREPAY_AMORTIZATION,
            SUM(NVL(las.PREPAID_RENT,0)) as PREPAID_RENT,
            SUM(NVL(las.END_PREPAID_RENT,0)) as END_PREPAID_RENT,
            SUM(NVL(las.BEG_ARREARS_ACCRUAL,0)) as BEG_ARREARS_ACCRUAL,
            SUM(NVL(las.ARREARS_ACCRUAL,0)) as ARREARS_ACCRUAL,
            SUM(NVL(las.END_ARREARS_ACCRUAL,0)) as END_ARREARS_ACCRUAL,
			SUM(NVL(las.INCENTIVE_MATH_AMOUNT,0)) as INCENTIVE_MATH_AMOUNT,
			SUM(NVL(las.incentive_cap_amount,0)) as incentive_cap_amount,
			SUM(NVL(las.BEG_NET_ROU_ASSET,0)) as BEG_NET_ROU_ASSET,
			SUM(NVL(las.END_NET_ROU_ASSET,0)) as END_NET_ROU_ASSET
  from ls_asset la, ls_asset_schedule las
  where la.ls_asset_id = las.ls_asset_id
    and la.ilr_id = A_ILR_ID
    and las.revision = A_REVISION
  group by la.ilr_id,
            A_REVISION,
            las.set_of_books_id,
            las.month,
            las.is_om) b
  on (a.ilr_id = b.ilr_id and a.revision = b.revision and a.set_of_books_id = b.set_of_books_id and a.month = b.month)
  when matched then update set
    a.RESIDUAL_AMOUNT = b.RESIDUAL_AMOUNT,
    a.TERM_PENALTY = b.TERM_PENALTY,
    a.BPO_PRICE = b.BPO_PRICE,
    a.BEG_CAPITAL_COST = b.BEG_CAPITAL_COST,
    a.END_CAPITAL_COST = b.END_CAPITAL_COST,
    a.BEG_OBLIGATION = b.BEG_OBLIGATION,
    a.END_OBLIGATION = b.END_OBLIGATION,
    a.BEG_LT_OBLIGATION = b.BEG_LT_OBLIGATION,
    a.END_LT_OBLIGATION = b.END_LT_OBLIGATION,
    a.BEG_LIABILITY = b.BEG_LIABILITY,
    a.END_LIABILITY = b.END_LIABILITY,
    a.BEG_LT_LIABILITY = b.BEG_LT_LIABILITY,
    a.END_LT_LIABILITY = b.END_LT_LIABILITY,
    a.INTEREST_ACCRUAL = b.INTEREST_ACCRUAL,
    a.PRINCIPAL_ACCRUAL = b.PRINCIPAL_ACCRUAL,
    a.INTEREST_PAID = b.INTEREST_PAID,
    a.PRINCIPAL_PAID = b.PRINCIPAL_PAID,
    a.EXECUTORY_ACCRUAL1 = b.EXECUTORY_ACCRUAL1,
    a.EXECUTORY_ACCRUAL2 = b.EXECUTORY_ACCRUAL2,
    a.EXECUTORY_ACCRUAL3 = b.EXECUTORY_ACCRUAL3,
    a.EXECUTORY_ACCRUAL4 = b.EXECUTORY_ACCRUAL4,
    a.EXECUTORY_ACCRUAL5 = b.EXECUTORY_ACCRUAL5,
    a.EXECUTORY_ACCRUAL6 = b.EXECUTORY_ACCRUAL6,
    a.EXECUTORY_ACCRUAL7 = b.EXECUTORY_ACCRUAL7,
    a.EXECUTORY_ACCRUAL8 = b.EXECUTORY_ACCRUAL8,
    a.EXECUTORY_ACCRUAL9 = b.EXECUTORY_ACCRUAL9,
    a.EXECUTORY_ACCRUAL10 = b.EXECUTORY_ACCRUAL10,
    a.EXECUTORY_PAID1 = b.EXECUTORY_PAID1,
    a.EXECUTORY_PAID2 = b.EXECUTORY_PAID2,
    a.EXECUTORY_PAID3 = b.EXECUTORY_PAID3,
    a.EXECUTORY_PAID4 = b.EXECUTORY_PAID4,
    a.EXECUTORY_PAID5 = b.EXECUTORY_PAID5,
    a.EXECUTORY_PAID6 = b.EXECUTORY_PAID6,
    a.EXECUTORY_PAID7 = b.EXECUTORY_PAID7,
    a.EXECUTORY_PAID8 = b.EXECUTORY_PAID8,
    a.EXECUTORY_PAID9 = b.EXECUTORY_PAID9,
    a.EXECUTORY_PAID10 = b.EXECUTORY_PAID10,
    a.CONTINGENT_ACCRUAL1 = b.CONTINGENT_ACCRUAL1,
    a.CONTINGENT_ACCRUAL2 = b.CONTINGENT_ACCRUAL2,
    a.CONTINGENT_ACCRUAL3 = b.CONTINGENT_ACCRUAL3,
    a.CONTINGENT_ACCRUAL4 = b.CONTINGENT_ACCRUAL4,
    a.CONTINGENT_ACCRUAL5 = b.CONTINGENT_ACCRUAL5,
    a.CONTINGENT_ACCRUAL6 = b.CONTINGENT_ACCRUAL6,
    a.CONTINGENT_ACCRUAL7 = b.CONTINGENT_ACCRUAL7,
    a.CONTINGENT_ACCRUAL8 = b.CONTINGENT_ACCRUAL8,
    a.CONTINGENT_ACCRUAL9 = b.CONTINGENT_ACCRUAL9,
    a.CONTINGENT_ACCRUAL10 = b.CONTINGENT_ACCRUAL10,
    a.CONTINGENT_PAID1 = b.CONTINGENT_PAID1,
    a.CONTINGENT_PAID2 = b.CONTINGENT_PAID2,
    a.CONTINGENT_PAID3 = b.CONTINGENT_PAID3,
    a.CONTINGENT_PAID4 = b.CONTINGENT_PAID4,
    a.CONTINGENT_PAID5 = b.CONTINGENT_PAID5,
    a.CONTINGENT_PAID6 = b.CONTINGENT_PAID6,
    a.CONTINGENT_PAID7 = b.CONTINGENT_PAID7,
    a.CONTINGENT_PAID8 = b.CONTINGENT_PAID8,
    a.CONTINGENT_PAID9 = b.CONTINGENT_PAID9,
    a.CONTINGENT_PAID10 = b.CONTINGENT_PAID10,
    a.CURRENT_LEASE_COST = b.CURRENT_LEASE_COST,
    a.BEG_DEFERRED_RENT = b.BEG_DEFERRED_RENT,
    a.DEFERRED_RENT = b.DEFERRED_RENT,
    a.END_DEFERRED_RENT = b.END_DEFERRED_RENT,
    a.BEG_ST_DEFERRED_RENT = b.BEG_ST_DEFERRED_RENT,
    a.END_ST_DEFERRED_RENT = b.END_ST_DEFERRED_RENT,
    a.INCENTIVE_AMOUNT = b.INCENTIVE_AMOUNT,
    a.INITIAL_DIRECT_COST = b.INITIAL_DIRECT_COST,
    a.PRINCIPAL_REMEASUREMENT = b.PRINCIPAL_REMEASUREMENT,
    a.LIABILITY_REMEASUREMENT = b.LIABILITY_REMEASUREMENT,
    a.BEG_PREPAID_RENT = b.BEG_PREPAID_RENT,
    a.PREPAY_AMORTIZATION = b.PREPAY_AMORTIZATION,
    a.PREPAID_RENT = b.PREPAID_RENT,
    a.END_PREPAID_RENT = b.END_PREPAID_RENT,
    a.BEG_ARREARS_ACCRUAL = b.BEG_ARREARS_ACCRUAL,
    a.ARREARS_ACCRUAL = b.ARREARS_ACCRUAL,
    a.END_ARREARS_ACCRUAL = b.END_ARREARS_ACCRUAL,
    a.INCENTIVE_MATH_AMOUNT = b.INCENTIVE_MATH_AMOUNT,
    a.incentive_cap_amount = b.incentive_cap_amount,
	a.BEG_NET_ROU_ASSET = b.BEG_NET_ROU_ASSET,
	a.END_NET_ROU_ASSET = b.END_NET_ROU_ASSET
  when not matched then insert
  (ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST,
  END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION,
  BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
  INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3,
  EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9,
  EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6,
  EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3,
  CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9,
  CONTINGENT_ACCRUAL10, CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
  CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, IS_OM, CURRENT_LEASE_COST,
  BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST,
  PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, PREPAID_RENT, END_PREPAID_RENT,
  BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL,INCENTIVE_MATH_AMOUNT,incentive_cap_amount, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET)
  values (
    b.ILR_ID,
    b.REVISION,
    b.SET_OF_BOOKS_ID,
    b.MONTH,
    b.RESIDUAL_AMOUNT,
    b.TERM_PENALTY,
    b.BPO_PRICE,
    b.BEG_CAPITAL_COST,
    b.END_CAPITAL_COST,
    b.BEG_OBLIGATION,
    b.END_OBLIGATION,
    b.BEG_LT_OBLIGATION,
    b.END_LT_OBLIGATION,
    b.BEG_LIABILITY,
    b.END_LIABILITY,
    b.BEG_LT_LIABILITY,
    b.END_LT_LIABILITY,
    b.INTEREST_ACCRUAL,
    b.PRINCIPAL_ACCRUAL,
    b.INTEREST_PAID,
    b.PRINCIPAL_PAID,
    b.EXECUTORY_ACCRUAL1,
    b.EXECUTORY_ACCRUAL2,
    b.EXECUTORY_ACCRUAL3,
    b.EXECUTORY_ACCRUAL4,
    b.EXECUTORY_ACCRUAL5,
    b.EXECUTORY_ACCRUAL6,
    b.EXECUTORY_ACCRUAL7,
    b.EXECUTORY_ACCRUAL8,
    b.EXECUTORY_ACCRUAL9,
    b.EXECUTORY_ACCRUAL10,
    b.EXECUTORY_PAID1,
    b.EXECUTORY_PAID2,
    b.EXECUTORY_PAID3,
    b.EXECUTORY_PAID4,
    b.EXECUTORY_PAID5,
    b.EXECUTORY_PAID6,
    b.EXECUTORY_PAID7,
    b.EXECUTORY_PAID8,
    b.EXECUTORY_PAID9,
    b.EXECUTORY_PAID10,
    b.CONTINGENT_ACCRUAL1,
    b.CONTINGENT_ACCRUAL2,
    b.CONTINGENT_ACCRUAL3,
    b.CONTINGENT_ACCRUAL4,
    b.CONTINGENT_ACCRUAL5,
    b.CONTINGENT_ACCRUAL6,
    b.CONTINGENT_ACCRUAL7,
    b.CONTINGENT_ACCRUAL8,
    b.CONTINGENT_ACCRUAL9,
    b.CONTINGENT_ACCRUAL10,
    b.CONTINGENT_PAID1,
    b.CONTINGENT_PAID2,
    b.CONTINGENT_PAID3,
    b.CONTINGENT_PAID4,
    b.CONTINGENT_PAID5,
    b.CONTINGENT_PAID6,
    b.CONTINGENT_PAID7,
    b.CONTINGENT_PAID8,
    b.CONTINGENT_PAID9,
    b.CONTINGENT_PAID10,
    b.IS_OM,
    b.CURRENT_LEASE_COST,
    b.BEG_DEFERRED_RENT,
    b.DEFERRED_RENT,
    b.END_DEFERRED_RENT,
    b.BEG_ST_DEFERRED_RENT,
    b.END_ST_DEFERRED_RENT,
    b.INCENTIVE_AMOUNT,
    b.INITIAL_DIRECT_COST,
    b.PRINCIPAL_REMEASUREMENT,
    b.LIABILITY_REMEASUREMENT,
    b.BEG_PREPAID_RENT,
    b.PREPAY_AMORTIZATION,
    b.PREPAID_RENT,
    b.END_PREPAID_RENT,
    b.BEG_ARREARS_ACCRUAL,
    b.ARREARS_ACCRUAL,
    b.END_ARREARS_ACCRUAL,
	b.INCENTIVE_MATH_AMOUNT,
	b.INCENTIVE_CAP_AMOUNT,
	b.BEG_NET_ROU_ASSET,
	b.END_NET_ROU_ASSET
    );

    return 'OK';

  EXCEPTION
    WHEN Others THEN
    return 'Error syncing ILR schedule to assets: ' || sqlerrm;
  end F_SYNC_ILR_SCHEDULE;

  function F_BUILD_FORECAST(A_REVISION number) return varchar2 is
      LOOKBACK_REVISION number;
      B_HAS_LOOKBACK boolean;
      FORECAST_TYPE_ID number;
      EFFECTIVE_DATE date;
      LOOKBACK_DATE date;
      NULL_DATE date := null;
      MSG varchar2(2000);
    L_CUR_TYPE number;
      L_STATUS varchar2(2000);
      L_RTN number;
      location varchar2(30000);
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      LOCATION:='Entering F_BUILD_FORECAST';
      PKG_PP_LOG.P_WRITE_MESSAGE(LOCATION);

      -- Get the lookback revision
      select LOOKBACK_REVISION, FORECAST_TYPE_ID, CONVERSION_DATE, LOOKBACK_DATE, LS_CURRENCY_TYPE_ID
        into LOOKBACK_REVISION, FORECAST_TYPE_ID, EFFECTIVE_DATE, LOOKBACK_DATE, L_CUR_TYPE
        from LS_FORECAST_VERSION
       where REVISION = A_REVISION;

      if LOOKBACK_REVISION is null then
         B_HAS_LOOKBACK := FALSE;
      else
         B_HAS_LOOKBACK := TRUE;
      end if;

      -- Delete from several tables
      MSG := 'Deleting from LS_ILR_ASSET_SCHEDULE_STG: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ILR_ASSET_SCHEDULE_STG
       where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_STG: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ILR_STG
        where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_ASSET_STG: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ILR_ASSET_STG
        where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_SCHEDULE_STG: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ILR_SCHEDULE_STG
        where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_ASSET_SCHEDULE_CALC_STG: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ILR_ASSET_SCHEDULE_CALC_STG
        where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_INC_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from ls_ilr_inc_stg
       where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_IDC_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from ls_ilr_idc_stg
       where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_PAYMENT_TERM_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from ls_ilr_payment_term_stg
       where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Delting from LS_ILR_SCHEDULE_BUCKET_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from ls_ilr_schedule_bucket_stg;

      MSG := 'Deleting from LS_ASSET_SCHEDULE: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ASSET_SCHEDULE
        where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_SCHEDULE: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ILR_SCHEDULE
        where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_FORECAST_SUMMARY: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_SUMMARY_FORECAST
        where revision in (A_REVISION, LOOKBACK_REVISION);

      -- Loop over LS_FORECAST_ILR_MASTER to load each ILR
      MSG := 'Looping over LS_FORECAST_ILR_MASTER to load each ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      for i in (select distinct ilr_id from ls_forecast_ilr_master where revision = A_REVISION)
      loop
        L_STATUS := F_LOAD_ILR_STG(i.ilr_id, A_REVISION);
        if L_STATUS <> 'OK' then
           return L_STATUS;
        end if;
      end loop;

      -- Run an update statement on ls_ilr_stg and set the npv_start_date column equal to the effective month of the lookback revision.
    -- If this is a mod retro forecast (4), conversion date/effective date will be populated at this point, but we don't want it to be set yet
      MSG := 'Updating ls_ilr_stg.npv_start_date: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
    if forecast_type_id <> 4 then
      update ls_ilr_stg
         set npv_start_date = EFFECTIVE_DATE
       where revision = A_REVISION;
    else
      update ls_ilr_stg
      set npv_start_date = null
      where revision = a_revision;
    end if;

      -- Call F_CALC_SCHEDULES
      MSG := 'Calling F_CALC_SCHEDULES';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);

      L_STATUS := F_CALC_SCHEDULES();
      if L_STATUS <> 'OK' then
        return L_STATUS;
      end if;

      if FORECAST_TYPE_ID = 2 then
        MSG := 'Loading Deferred Rent for Summary Forecast';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        L_STATUS := F_LOAD_DEFERRED_RENT(TRUE);
        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;
      end if;

      -- Call F_PROCESS_ASSETS and F_SAVE_SCHEDULES
      --   Only pass in the effective date if we are not doing a Full Retrospective (3) or Modified Retrospective (4)
      MSG := 'Calling F_PROCESS_ASSETS and F_SAVE_SCHEDULES';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      if FORECAST_TYPE_ID in (3,4) then
        L_STATUS := F_PROCESS_ASSETS(NULL_DATE);
        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;

        L_STATUS := PKG_LEASE_VAR_PAYMENTS.F_CALC_ALL_VAR_PAYMENTS(NULL_DATE);
        if L_STATUS <> 'OK' then
          return L_STATUS;
         end if;

        L_STATUS := F_SAVE_SCHEDULES(NULL_DATE);
        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;

      else -- Not Full Retro or Mod Retro Forecast Type, process with effective date
        L_STATUS := F_PROCESS_ASSETS(EFFECTIVE_DATE);
        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;

        L_STATUS := PKG_LEASE_VAR_PAYMENTS.F_CALC_ALL_VAR_PAYMENTS(EFFECTIVE_DATE);
        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;

        L_STATUS := F_SAVE_SCHEDULES(EFFECTIVE_DATE);
        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;

      end if;

      --Call Mod Retro Function here
      if forecast_type_id = 4 then
        msg := F_PROCESS_MOD_RETRO_FCST(A_REVISION, EFFECTIVE_DATE);

        if msg <> 'OK' THEN
          PKG_PP_LOG.P_WRITE_MESSAGE(msg);
          return msg;
        end if;

        PKG_PP_LOG.P_WRITE_MESSAGE('End Modified Retrospective Calculations');
      END IF;

      -- clear the staging tables
      MSG := F_CLEAR_STAGING_TABLES(TRUE);
      if MSG <> 'OK' then
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      end if;

      -- If there is a lookback period on the forecast version, we need to copy all
      -- of our ILR's and build the schedules for the lookback period as well
      if B_HAS_LOOKBACK then
        MSG := 'Deleting from LS_DEPR_FORECAST for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_DEPR_FORECAST
          where revision = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_ASSET_MAP for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_ASSET_MAP
          where revision = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_OPTIONS for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_OPTIONS
          where revision = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_RENEWALS_OPTIONS for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_RENEWAL_OPTIONS
          where ILR_RENEWAL_ID IN (SELECT ILR_RENEWAL_ID FROM LS_ILR_RENEWAL
                                   WHERE REVISION = LOOKBACK_REVISION);

        MSG := 'Deleting from LS_ILR_RENEWALS for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_RENEWAL
          WHERE REVISION = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_PURCHASE_OPTIONS for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_PURCHASE_OPTIONS
          WHERE REVISION = LOOKBACK_REVISION;


        MSG := 'Deleting from LS_ILR_TERMINATION_OPTIONS for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_TERMINATION_OPTIONS
          WHERE REVISION = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_PAYMENT_TERM_VAR_PAYMNT for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_PAYMENT_TERM_VAR_PAYMNT
          where revision = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_PAYMENT_TERM for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_PAYMENT_TERM
          where revision = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_APPROVAL for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_APPROVAL
          where revision = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_AMOUNTS_SET_OF_BOOKS for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_AMOUNTS_SET_OF_BOOKS
          where revision = LOOKBACK_REVISION;

        -- Call PKG_LEASE_ILR.F_COPYREVISION(A_ILR_ID, Our master revision passed into the SSP, The lookback period revision)
        MSG := 'Copying ILRs to Lookback Revision';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        for i in (select distinct ilr_id from ls_forecast_ilr_master where revision = A_REVISION)
        loop
          L_RTN := PKG_LEASE_ILR.F_COPYREVISION(i.ilr_id, A_REVISION, LOOKBACK_REVISION);

          if L_RTN = -1 then
             return 'ERROR in PKG_LEASE_ILF.F_COPY_REVISION';
          end if;
        end loop;

        MSG := 'Deleting from LS_FORECAST_ILR_MASTER for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_FORECAST_ILR_MASTER
          where revision = LOOKBACK_REVISION;

        -- Copy the ILRS into ls_forecast_ilr_master for the lookback_revision
        MSG := 'Copying the ILRs to LS_FORECAST_ILR_MASTER for the Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        insert into ls_forecast_ilr_master (revision, ilr_id, from_cap_type_id, to_cap_type_id)
        select LOOKBACK_REVISION, ilr_id, from_cap_type_id, to_cap_type_id
          from ls_forecast_ilr_master
         where revision = A_REVISION;

        -- Loop over our ls_forecast_ilr_master table again and call PKG_LEASE_SCHEDULE.F_LOAD_ILR_STG for the ILR and Lookback revision.
        MSG := 'Looping over LS_FORECAST_ILR_MASTER to load each ILR for Lookback Revision';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        for i in (select distinct ilr_id from ls_forecast_ilr_master where revision = LOOKBACK_REVISION)
        loop
          L_STATUS := F_LOAD_ILR_STG(i.ilr_id, LOOKBACK_REVISION);

          if L_STATUS <> 'OK' then
             return L_STATUS;
          end if;
        end loop;

        -- Run an update statement on ls_ilr_stg and set the npv_start_date column equal to the effective month of the lookback revision.
        MSG := 'Updating ls_ilr_stg.npv_start_date for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        update ls_ilr_stg
           set npv_start_date = LOOKBACK_DATE
         where revision = LOOKBACK_REVISION;

        -- Call PKG_LEASE_SCHEDULE.F_CALC_SCHEDULE() -- This function does not take in any arguments and will process all ILR's that we staged
        MSG := 'Calling F_CALC_SCHEDULES for Lookback Revision';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        L_STATUS := F_CALC_SCHEDULES();

        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;

        if FORECAST_TYPE_ID = 2 then
          MSG := 'Loading Deferred Rent for Summary Forecast';
          PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
          L_STATUS := F_LOAD_DEFERRED_RENT(TRUE);

          if L_STATUS <> 'OK' then
            return L_STATUS;
          end if;
        end if;

        -- Call PKG_LEASE_SCHEDULE.F_PROCESS_ASSETS(Effective month of the lookback revision).
        MSG := 'Calling F_PROCESS_ASSETS for Lookback Revision';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        L_STATUS := F_PROCESS_ASSETS(LOOKBACK_DATE);

        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;

        -- Call PKG_LEASE_SCHEDULE.F_SAVE_SCHEDULES(Effective month of the lookback revision).
        MSG := 'Calling F_SAVE_SCHEDULES for Lookback Revision';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        L_STATUS := F_SAVE_SCHEDULES(LOOKBACK_DATE);

        -- clear the staging tables
        MSG := F_CLEAR_STAGING_TABLES;
         if MSG <> 'OK' then
           PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
         end if;

        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;


      end if;

      if FORECAST_TYPE_ID = 2 then
        -- Insert into ls_summary_forecast table for both the forecast and lookback revision
        --  Forecast Revision
        MSG := 'Inserting into ls_summary_forecast for forecast revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
          msg:=f_populate_summary_forecast(A_REVISION, L_CUR_TYPE);
          if msg <> 'OK' then
            PKG_PP_LOG.P_WRITE_MESSAGE(msg);
            return msg;
          end if;

        --  Lookback Revision
        if not LOOKBACK_REVISION is null then
          MSG := 'Inserting into ls_summary_forecast for lookback revision: ';
          PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
          msg:=f_populate_summary_forecast(lookback_revision, L_CUR_TYPE);
          if msg <> 'OK' then
            PKG_PP_LOG.P_WRITE_MESSAGE(msg);
            return msg;
          end if;
        end if;

        -- Populate Actuals into the Forecast Summary Table
        MSG := 'Inserting into ls_summary_forecast for actuals: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        msg:=f_populate_summary_forecast(A_REVISION, L_CUR_TYPE, 1);
        if msg <> 'OK' then
          PKG_PP_LOG.P_WRITE_MESSAGE(msg);
          return msg;
        end if;
      end if;

        -- Update revision_built to 1
        MSG := 'Update ls_forecast_revision.revision_built to 1 ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);

        update LS_FORECAST_VERSION
        set REVISION_BUILT = 1
        where REVISION = A_REVISION;

    msg := 'Setting Source Revision ';
    PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
    IF forecast_type_id <> 2 THEN
      --Set source revision on the forecast ILR to the current revision of the Historic/Original ILR Revision for auditing/reporting
      merge INTO LS_FORECAST_ILR_MASTER z
      USING ( SELECT ilr_id,
               current_revision
          FROM   LS_ILR ) a
      ON ( z.ilr_id = a.ilr_id AND z.revision = a_revision )
      WHEN matched THEN
        UPDATE SET source_revision = a.current_revision;
    END IF;

      return 'OK';

   EXCEPTION
      WHEN OTHERS THEN
        L_STATUS := 'ERROR: Building Forecast: '||MSG||sqlerrm;
        return L_STATUS;
   end F_BUILD_FORECAST;

   function F_BUILD_SUMMARY_FORECAST(A_REVISION number, A_RATE decimal) return varchar2 is
     MSG varchar2(2000);
     L_STATUS varchar2(2000);

     begin

     MSG := 'Starting : F_BUILD_SUMMARY_FORECAST ';
     PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
     for i in ( select to_cap_type_id, pre_payment_sw, ilr_id, company_id, revision
                   from (
                         select a.revision, ilr.ilr_id, a.to_cap_type_id, ll.pre_payment_sw, ilr.company_id, row_number() over(partition by a.to_cap_type_id, ll.pre_payment_sw, ilr.company_id order by ilr.ilr_id) the_row
                         from ls_forecast_ilr_master a, ls_ilr ilr, ls_lease ll
                         where a.revision = A_REVISION
                         and a.ilr_id = ilr.ilr_id
                         and ilr.lease_id = ll.lease_id)
                   where the_row =1  )
     loop
       --Merge into ls_ilr_payment_term the total payment amounts by month for each grouping
       MSG := 'Merge into ls_ilr_payment_term : ';

       merge into ls_ilr_payment_term a using (
            with N as (select ROWNUM as THE_ROW from DUAL connect by level <= 10000)
            select i.ilr_id as ilr_id, i.revision as revision, ADD_MONTHS(trunc( ilrpt.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1) as month,
            sum(case when mod(N.THE_ROW, DECODE(ilrpt.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(ilrpt.PAYMENT_FREQ_ID, 4, 0, ll.pre_payment_sw) then
                    ilrpt.PAID_AMOUNT
                   else
                    0
                end) as PAID_AMOUNT,
            (select max(payment_term_id) from ls_ilr_payment_term where ilr_id = i.ilr_id and revision = i.revision) as max_payment_term_id,
            row_number() over(order by ADD_MONTHS(trunc( ilrpt.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1)) adder
                from ls_forecast_ilr_master a, ls_ilr_payment_term ilrpt, ls_lease ll, ls_ilr ilr, n
                where a.ilr_id = ilrpt.ilr_id
          and a.revision = i.revision
                and a.revision = ilrpt.revision
                and a.to_cap_type_id = i.to_cap_type_id
                and a.ilr_id = ilr.ilr_id
                and ilr.company_id = i.company_id
                and ilr.lease_id = ll.lease_id
                and ll.pre_payment_sw = i.pre_payment_sw
                and N.THE_ROW <= ilrpt.NUMBER_OF_TERMS * DECODE(ilrpt.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)
                and ilrpt.payment_term_type_id = 2
                group by i.ilr_id, i.revision, ADD_MONTHS(trunc( ilrpt.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1)
             ) b
        on (a.ilr_id = b.ilr_id and a.revision = b.revision and a.payment_term_date = b.month)
        when matched then update set payment_freq_id = 4 /* monthly */,
                             number_of_terms = 1,
                             paid_amount = b.paid_amount,
                             contingent_amount = 0
        when not matched then insert (ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, paid_amount)
                              values (b.ilr_id, b.revision, b.max_payment_term_id + adder, 2, b.month, 4 /* monthly */, 1, b.paid_amount);

       --Insert a $0 row for any months in the middle of the payment term that may not have had a payment
       MSG := 'Insert into ls_ilr_payment_term : ';
       insert into ls_ilr_payment_term
       (ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, paid_amount)
       select i.ilr_id,  i.revision,  max_payment_term + rownum, 2, month, 4, 1, 0
       from
          (with N as (select ROWNUM as THE_ROW from DUAL connect by level <= 10000)
             select add_months(min_date, the_row) month, max_payment_term
             from
             (select min(payment_term_date) min_date, max(payment_term_date) max_date, max(payment_term_id) max_payment_term
              from ls_ilr_payment_term ilrpt
               where ilr_id = i.ilr_id
               and revision = i.revision ), n
               where n.the_row < months_between(max_date, min_date)) a
       where not exists (select 1 from ls_ilr_payment_term where ilr_id = i.ilr_id and revision = i.revision and payment_term_date = a.month);

       --Re-Sort the ilr_payment_term table:
       MSG := 'Re sort - Merge into ls_ilr_payment_term : ';

       merge into ls_ilr_payment_term a using(
         select row_number() over(partition by ilr_id, revision order by payment_term_date) the_row, a.*
         from ls_ilr_payment_term a
         where ilr_id =  i.ilr_id
         and revision = i.revision
         ) b
       on (a.ilr_id = b.ilr_id and a.payment_term_date = b.payment_term_date and a.revision = b.revision)
       when matched then update set payment_term_id = b.the_row ;

       --Delete out the payment term records for the other ILR's that have now been consolidated into another ILR:
       MSG := 'Delete from ls_ilr_payment_term : ';

       delete from ls_ilr_payment_term
       where ilr_id <> i.ilr_id
       and (ilr_id, revision) in
         (select a.ilr_id, a.revision
          from ls_forecast_ilr_master a, ls_ilr ilr, ls_lease ll
          where a.ilr_id = ilr.ilr_id
            and ilr.lease_id = ll.lease_id
            and ll.pre_payment_sw =  i.pre_payment_sw
            and a.to_cap_type_id = i.to_cap_type_id
            and revision = i.revision
            and company_id = i.company_id);

     end loop;

     --Update ls_ilr_options.inception_air with passed in rate:
       MSG := 'Update ls_ilr_options.inception_air : ';
       update ls_ilr_options
       set inception_air = A_RATE
       where revision = A_REVISION;

     MSG := 'F_BUILD_SUMMARY_FORECAST complete successfully';
     PKG_PP_LOG.P_WRITE_MESSAGE(MSG);

     return 'OK';

     EXCEPTION
      WHEN OTHERS THEN
        L_STATUS := 'ERROR: Building Summary Forecast: '||MSG||sqlerrm;
        return L_STATUS;

    end F_BUILD_SUMMARY_FORECAST;

    function F_CONVERT_FORECAST(A_REVISION IN NUMBER) return varchar2 IS
      msg varchar(2000);
      new_revision number;
      l_return number;
      begin
        msg := 'Starting : F_CONVERT_FORECAST ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        for i in (select * from ls_forecast_ilr_master where revision = A_REVISION)
          loop
            new_revision := PKG_LEASE_ILR.F_COPYREVISION(i.ilr_id, A_REVISION);

      msg := 'Recording In Service Revision';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      merge INTO LS_FORECAST_ILR_MASTER z
      USING ( SELECT ilr_id,
               new_revision current_revision
              FROM   LS_ILR
              where ilr_id = i.ilr_id  ) a
      ON ( z.ilr_id = a.ilr_id AND z.revision = a_revision)
      WHEN matched THEN
        UPDATE SET in_service_revision = a.current_revision;

            l_return := PKG_LEASE_CALC.F_SEND_ILR_NO_COMMIT(i.ilr_id, new_revision, MSG);
            if l_return = -1 then
              msg := 'Error: PKG_LEASE_CALC.F_SEND_ILR_NO_COMMIT: ' || msg ;
              return msg;
            end if;

            l_return := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(i.ilr_id, new_revision, MSG, TRUE);
            if l_return = -1 then
              msg := 'Error: PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT: ' || msg ;
              return msg;
            end if;
          end loop;

         MSG := 'F_CONVERT_FORECAST completed successfully';
         PKG_PP_LOG.P_WRITE_MESSAGE(MSG);

      return 'OK';

      EXCEPTION
      WHEN OTHERS THEN
        msg := 'ERROR: Convert Forecast to Service: '||MSG||sqlerrm;
        return msg;

      end F_CONVERT_FORECAST;

  function F_POPULATE_SUMMARY_FORECAST(A_REVISION IN NUMBER, A_CUR_TYPE IN number, A_ACTUALS IN number:=null) return varchar2
  is
  msg varchar2(5000);
  begin

  msg:='Inserting into ls_summary_forecast for revision ' || a_revision;
    pkg_pp_log.p_write_message(msg);
  insert into ls_summary_forecast (
             COMPANY_ID, REVISION, SET_OF_BOOKS_ID, "MONTH", TYPE_A_EXPENSE, TYPE_B_EXPENSE, RESIDUAL_AMOUNT, TERM_PENALTY,
             BPO_PRICE, BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION,
             BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
             INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
             EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
             EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
             EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
             CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
             CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4,
             CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, current_lease_cost,
             begin_reserve, end_reserve, is_actuals, rental_expense, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT,
             BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
             PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, PREPAID_RENT, END_PREPAID_RENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST,
             BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL)
  SELECT
    company_id,
    revision,
    set_of_books_id,
    MONTH,
    Sum(type_a_expense) type_a_expense,
    Sum(type_b_expense) type_b_expense,
    Sum(residual_amount) residual_amount,
    Sum(term_penalty) term_penalty,
    Sum(bpo_price) bpo_price,
    Sum(beg_capital_cost) beg_capital_cost,
    Sum(end_capital_cost) end_capital_cost,
    Sum(beg_obligation) beg_obligation,
    Sum(end_obligation) end_obligation,
    Sum(beg_lt_obligation) beg_lt_obligation,
    Sum(end_lt_obligation) end_lt_obligation,
    Sum(beg_liability) beg_liability,
    Sum(end_liability) end_liability,
    Sum(beg_lt_liability) beg_lt_liability,
    Sum(end_lt_liability) end_lt_liability,
    Sum(interest_accrual) interest_accrual,
    Sum(principal_accrual) principal_accrual,
    Sum(interest_paid) interest_paid,
    Sum(principal_paid) principal_paid,
    Sum(executory_accrual1) executory_accrual1,
    Sum(executory_accrual2) executory_accrual2,
    Sum(executory_accrual3) executory_accrual3,
    Sum(executory_accrual4) executory_accrual4,
    Sum(executory_accrual5) executory_accrual5,
    Sum(executory_accrual6) executory_accrual6,
    Sum(executory_accrual7) executory_accrual7,
    Sum(executory_accrual8) executory_accrual8,
    Sum(executory_accrual9) executory_accrual9,
    Sum(executory_accrual10) executory_accrual10,
    Sum(executory_paid1) executory_paid1,
    Sum(executory_paid2) executory_paid2,
    Sum(executory_paid3) executory_paid3,
    Sum(executory_paid4) executory_paid4,
    Sum(executory_paid5) executory_paid5,
    Sum(executory_paid6) executory_paid6,
    Sum(executory_paid7) executory_paid7,
    Sum(executory_paid8) executory_paid8,
    Sum(executory_paid9) executory_paid9,
    Sum(executory_paid10) executory_paid10,
    Sum(contingent_accrual1) contingent_accrual1,
    Sum(contingent_accrual2) contingent_accrual2,
    Sum(contingent_accrual3) contingent_accrual3,
    Sum(contingent_accrual4) contingent_accrual4,
    Sum(contingent_accrual5) contingent_accrual5,
    Sum(contingent_accrual6) contingent_accrual6,
    Sum(contingent_accrual7) contingent_accrual7,
    Sum(contingent_accrual8) contingent_accrual8,
    Sum(contingent_accrual9) contingent_accrual9,
    Sum(contingent_accrual10) contingent_accrual10,
    Sum(contingent_paid1) contingent_paid1,
    Sum(contingent_paid2) contingent_paid2,
    Sum(contingent_paid3) contingent_paid3,
    Sum(contingent_paid4) contingent_paid4,
    Sum(contingent_paid5) contingent_paid5,
    Sum(contingent_paid6) contingent_paid6,
    Sum(contingent_paid7) contingent_paid7,
    Sum(contingent_paid8) contingent_paid8,
    Sum(contingent_paid9) contingent_paid9,
    Sum(contingent_paid10) contingent_paid10,
    Sum(current_lease_cost) current_lease_cost,
    Sum(begin_reserve) begin_reserve,
    Sum(end_reserve) end_reserve,
    is_actuals,
    Sum(rental_expense) rental_expense,
    Sum(beg_deferred_rent),
    Sum(deferred_rent),
    Sum(end_deferred_rent),
    Sum(beg_st_deferred_rent),
    Sum(end_st_deferred_rent),
    Sum(principal_remeasurement),
    Sum(liability_remeasurement),
    Sum(beg_prepaid_rent),
    Sum(prepay_amortization),
    Sum(prepaid_rent),
    Sum(end_prepaid_rent),
    Sum(incentive_amount),
    Sum(initial_direct_cost),
    Sum(beg_arrears_accrual),
    Sum(arrears_accrual),
    Sum(end_arrears_accrual)
  FROM
  (
    SELECT
      a.company_id,
      CASE
        WHEN A_ACTUALS = 1 THEN A_REVISION
        ELSE a.revision
      END
      revision,
             a.set_of_books_id,
      a.MONTH,
      CASE
        WHEN a.is_om = 1 THEN 0
        ELSE Nvl(
          CASE WHEN ls_map.fasb_cap_type_id in (1,3) THEN
            Nvl(a.interest_accrual,0) + Nvl(a.depr_expense,0) + Nvl(a.depr_exp_alloc_adjust,0)
          END,
          0
        )
        END
      AS type_a_expense,
      CASE
        WHEN a.is_om = 1 THEN 0
        ELSE Nvl(
          CASE WHEN ls_map.fasb_cap_type_id in (2,6) THEN
            Nvl(a.interest_accrual,0) + Nvl(a.depr_expense,0) + Nvl(a.depr_exp_alloc_adjust,0)
          END,
          0
        )
      END AS type_b_expense,
      Nvl(a.residual_amount,0) residual_amount,
      Nvl(a.term_penalty,0) term_penalty,
      Nvl(a.bpo_price,0) bpo_price,
      Nvl(a.beg_capital_cost,0) beg_capital_cost,
      Nvl(a.end_capital_cost,0) end_capital_cost,
      CASE
        WHEN a.is_om = 0 THEN Nvl(a.beg_obligation,0)
        ELSE 0
      END beg_obligation,
      CASE
        WHEN a.is_om = 0 THEN Nvl(a.end_obligation,0)
        ELSE 0
      END end_obligation,
      CASE
        WHEN a.is_om = 0 THEN Nvl(a.beg_lt_obligation,0)
        ELSE 0
      END beg_lt_obligation,
      CASE
        WHEN a.is_om = 0 THEN Nvl(a.end_lt_obligation,0)
        ELSE 0
      END end_lt_obligation,
      CASE
        WHEN a.is_om = 0 THEN Nvl(a.beg_liability,0)
        ELSE 0
      END beg_liability,
      CASE
        WHEN a.is_om = 0 THEN Nvl(a.end_liability,0)
        ELSE 0
      END end_liability,
      CASE
        WHEN a.is_om = 0 THEN Nvl(a.beg_lt_liability,0)
        ELSE 0
      END beg_lt_liability,
      CASE
        WHEN a.is_om = 0 THEN Nvl(a.end_lt_liability,0)
        ELSE 0
      END end_lt_liability,
      CASE
        WHEN a.is_om = 0 THEN Nvl(a.interest_accrual,0)
        ELSE 0
      END interest_accrual,
      Nvl(a.principal_accrual,0) principal_accrual,
      Nvl(a.interest_paid,0) interest_paid,
      Nvl(a.principal_paid,0) principal_paid,
      Nvl(a.executory_accrual1,0) executory_accrual1,
      Nvl(a.executory_accrual2,0) executory_accrual2,
      Nvl(a.executory_accrual3,0) executory_accrual3,
      Nvl(a.executory_accrual4,0) executory_accrual4,
      Nvl(a.executory_accrual5,0) executory_accrual5,
      Nvl(a.executory_accrual6,0) executory_accrual6,
      Nvl(a.executory_accrual7,0) executory_accrual7,
      Nvl(a.executory_accrual8,0) executory_accrual8,
      Nvl(a.executory_accrual9,0) executory_accrual9,
      Nvl(a.executory_accrual10,0) executory_accrual10,
      Nvl(a.executory_paid1,0) executory_paid1,
      Nvl(a.executory_paid2,0) executory_paid2,
      Nvl(a.executory_paid3,0) executory_paid3,
      Nvl(a.executory_paid4,0) executory_paid4,
      Nvl(a.executory_paid5,0) executory_paid5,
      Nvl(a.executory_paid6,0) executory_paid6,
      Nvl(a.executory_paid7,0) executory_paid7,
      Nvl(a.executory_paid8,0) executory_paid8,
      Nvl(a.executory_paid9,0) executory_paid9,
      Nvl(a.executory_paid10,0) executory_paid10,
      Nvl(a.contingent_accrual1,0) contingent_accrual1,
      Nvl(a.contingent_accrual2,0) contingent_accrual2,
      Nvl(a.contingent_accrual3,0) contingent_accrual3,
      Nvl(a.contingent_accrual4,0) contingent_accrual4,
      Nvl(a.contingent_accrual5,0) contingent_accrual5,
      Nvl(a.contingent_accrual6,0) contingent_accrual6,
      Nvl(a.contingent_accrual7,0) contingent_accrual7,
      Nvl(a.contingent_accrual8,0) contingent_accrual8,
      Nvl(a.contingent_accrual9,0) contingent_accrual9,
      Nvl(a.contingent_accrual10,0) contingent_accrual10,
      Nvl(a.contingent_paid1,0) contingent_paid1,
      Nvl(a.contingent_paid2,0) contingent_paid2,
      Nvl(a.contingent_paid3,0) contingent_paid3,
      Nvl(a.contingent_paid4,0) contingent_paid4,
      Nvl(a.contingent_paid5,0) contingent_paid5,
      Nvl(a.contingent_paid6,0) contingent_paid6,
      Nvl(a.contingent_paid7,0) contingent_paid7,
      Nvl(a.contingent_paid8,0) contingent_paid8,
      Nvl(a.contingent_paid9,0) contingent_paid9,
      Nvl(a.contingent_paid10,0) contingent_paid10,
      Nvl(a.current_lease_cost,0) current_lease_cost,
      Nvl(a.begin_reserve,0) begin_reserve,
      Nvl(a.end_reserve,0) end_reserve,
      Nvl(A_ACTUALS,0) is_actuals,
      CASE
        WHEN a.is_om = 1 THEN Nvl(a.interest_accrual,0)
        ELSE 0
      END AS rental_expense,
      Nvl(a.BEG_DEFERRED_RENT,0) beg_deferred_rent,
      Nvl(a.DEFERRED_RENT,0) deferred_rent,
      Nvl(a.END_DEFERRED_RENT,0) end_deferred_rent,
      Nvl(a.BEG_ST_DEFERRED_RENT,0) beg_st_deferred_rent,
      Nvl(a.END_ST_DEFERRED_RENT,0) end_st_deferred_rent,
      Nvl(a.principal_remeasurement,0) principal_remeasurement,
      Nvl(a.liability_remeasurement,0) liability_remeasurement,
      Nvl(a.beg_prepaid_rent,0) beg_prepaid_rent,
      Nvl(a.prepay_amortization,0) prepay_amortization,
      Nvl(a.prepaid_rent,0) prepaid_rent,
      Nvl(a.end_prepaid_rent,0) end_prepaid_rent,
      nvl(a.incentive_amount, 0) incentive_amount,
      nvl(a.initial_direct_cost, 0) initial_direct_cost,
      nvl(a.beg_arrears_accrual, 0) beg_arrears_accrual,
      nvl(a.arrears_accrual, 0) arrears_accrual,
      nvl(a.end_arrears_accrual, 0) end_arrears_accrual
    FROM
      v_ls_asset_schedule_fx_vw a,
      ls_ilr ilr,
      ls_ilr_options ilro,
      ls_lease_cap_type lct,
      ls_forecast_ilr_master master,
      ls_fasb_cap_type_sob_map ls_map
    WHERE a.revision =
      CASE
        WHEN A_ACTUALS = 1 THEN a.approved_revision
        ELSE A_REVISION
      END
    AND a.ilr_id = ilro.ilr_id
    AND a.ilr_id = ilr.ilr_id
    AND ilro.revision =
      CASE
        WHEN A_ACTUALS = 1 THEN ilr.current_revision
        ELSE A_REVISION
      END
    AND ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
    AND lct.ls_lease_cap_type_id = ls_map.lease_cap_type_id
    AND ls_map.set_of_books_id = a.set_of_books_id
    AND a.set_of_books_id IN (
      SELECT Decode(
          set_of_books_id,
          -1,
          a.set_of_books_id,
          set_of_books_id
        )
      FROM ls_forecast_version
      WHERE revision = A_REVISION
    )
    AND a.ilr_id = master.ilr_id
    AND master.revision = A_REVISION
    AND a.ls_cur_type = A_CUR_TYPE
  ) x
  GROUP BY company_id, revision, set_of_books_id, month, is_actuals;


  return 'OK';
  exception when others then
    return 'ERROR: Populating Summary Forecast: '||MSG||' ' || sqlerrm;
  end F_POPULATE_SUMMARY_FORECAST;

  --Overloaded function signature to maintain the previous calls
  function F_CLEAR_STAGING_TABLES return varchar2
  is
  begin
      --call the overloaded function with default parameter
     return F_CLEAR_STAGING_TABLES(FALSE);
  end F_CLEAR_STAGING_TABLES;

 function F_CLEAR_STAGING_TABLES(A_CLEAR_CALC IN BOOLEAN) return varchar2
    is
    L_STATUS                      varchar2(2000);

  begin

      L_STATUS := 'CLEARING OUT ls_ilr_asset_schedule_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_ASSET_SCHEDULE_STG;

      L_STATUS := 'CLEARING OUT ls_ilr_asset_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_ASSET_STG;

      L_STATUS := 'CLEARING OUT ls_ilr_inc_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from ls_ilr_inc_stg;

      L_STATUS := 'CLEARING OUT ls_ilr_idc_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from ls_ilr_idc_stg;

      L_STATUS := 'CLEARING OUT ls_ilr_schedule_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_SCHEDULE_STG;

      L_STATUS := 'CLEARING OUT ls_ilr_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_STG;

	  if(A_CLEAR_CALC) then
	      L_STATUS := 'CLEARING OUT ls_ilr_asset_schedule_calc_stg';
          PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
          delete from ls_ilr_asset_schedule_calc_stg;

	      L_STATUS := 'CLEARING OUT ls_ilr_payment_term_stg';
          PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
          delete from ls_ilr_payment_term_stg;

	      L_STATUS := 'CLEARING OUT ls_ilr_schedule_bucket_stg';
          PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
          delete from ls_ilr_schedule_bucket_stg;
	  end if;

      return 'OK';

  exception when others then
    return 'ERROR: Clearing staging tables: ' || sqlerrm;

  end F_CLEAR_STAGING_TABLES;

   --**************************************************************************
   --                            F_LOAD_DEFERRED_RENT
   -- Load deferred rent balance onto the schedule building tables
   -- If this is a summary forecast, need to load the deferred rent appropriately
   --**************************************************************************
   function F_LOAD_DEFERRED_RENT(A_SUMM_FCST IN BOOLEAN) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin

      if not A_SUMM_FCST THEN

        L_STATUS := 'Loading Conversion Deferred and Prepaid Rent Balances';
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

        merge into LS_ILR_STG S
        using (select ilr_id,
                      revision,
                      set_of_books_id,
                      MONTH,
                      beg_deferred_rent,
                      beg_prepaid_rent
                      from ls_ilr_load_balances) B
        on (B.ILR_ID = S.ILR_ID and B.REVISION = S.REVISION and B.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID)
        when matched then
          update
              set S.DEFERRED_RENT_BALANCE = B.BEG_DEFERRED_RENT,
                  S.PREPAID_RENT_BALANCE = B.BEG_PREPAID_RENT,
                  S.CONVERSION_BALANCE_MONTH = B.MONTH;

        L_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

        L_STATUS := 'Loading Deferred Rent and Prepaid Rent Balance';
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
        update LS_ILR_STG STG
        set (DEFERRED_RENT_BALANCE, PREPAID_RENT_BALANCE, CONVERSION_BALANCE_MONTH) = (
          select Nvl(BEG_DEFERRED_RENT + BEG_ARREARS_ACCRUAL, 0), Nvl(BEG_PREPAID_RENT, 0), NULL
          from LS_ILR_SCHEDULE S, LS_ILR ILR
          where S.ILR_ID = ILR.ILR_ID
          and ILR.CURRENT_REVISION = S.REVISION
          and S.ILR_ID = STG.ILR_ID
          and S.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
          and S.MONTH = STG.NPV_START_DATE)
        WHERE EXISTS (
          select 1
          from LS_ILR_SCHEDULE S, LS_ILR ILR
          where S.ILR_ID = ILR.ILR_ID
          and ILR.CURRENT_REVISION = S.REVISION
          and S.ILR_ID = STG.ILR_ID
          and S.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
          and S.MONTH = STG.NPV_START_DATE
        );

        L_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

      else

        -- we've already loaded in deferred rent for each ILR in the forecast,
        --so pull them onto the ILR that's being used for summarization (has payment term records)
        L_STATUS := 'Loading Summary Forecast Deferred Rent';
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
        update LS_ILR_STG STG
        set (DEFERRED_RENT_BALANCE, PREPAID_RENT_BALANCE) = (
          select Nvl(DEFERRED_RENT, 0), Nvl(PREPAID_RENT, 0)
          from
            (select S.REVISION, S.SET_OF_BOOKS_ID, ILR.COMPANY_ID, ILRO.LEASE_CAP_TYPE_ID,
                S.PREPAY_SWITCH, sum(DEFERRED_RENT_BALANCE) DEFERRED_RENT, sum(PREPAID_RENT_BALANCE) PREPAID_RENT
            from LS_ILR_STG S, LS_ILR ILR, LS_ILR_OPTIONS ILRO
            where S.ILR_ID = ILR.ILR_ID
            and S.ILR_ID = ILRO.ILR_ID
            and S.REVISION = ILRO.REVISION
            group by S.REVISION, S.SET_OF_BOOKS_ID, ILR.COMPANY_ID,
                ILRO.LEASE_CAP_TYPE_ID, S.PREPAY_SWITCH) AMTS,
            (select S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, ILR.COMPANY_ID,
                ILRO.LEASE_CAP_TYPE_ID, S.PREPAY_SWITCH
            from LS_ILR_STG S, LS_ILR ILR, LS_ILR_OPTIONS ILRO, LS_ILR_PAYMENT_TERM PT
            where S.ILR_ID = ILR.ILR_ID
            and S.ILR_ID = ILRO.ILR_ID
            and S.REVISION = ILRO.REVISION
            and S.ILR_ID = PT.ILR_ID
            and S.REVISION = PT.REVISION
            and PT.PAYMENT_TERM_ID = 1) ILRS
          where AMTS.REVISION = ILRS.REVISION
          and AMTS.SET_OF_BOOKS_ID = ILRS.SET_OF_BOOKS_ID
          and AMTS.COMPANY_ID = ILRS.COMPANY_ID
          and AMTS.LEASE_CAP_TYPE_ID = ILRS.LEASE_CAP_TYPE_ID
          and AMTS.PREPAY_SWITCH = ILRS.PREPAY_SWITCH
          and ILRS.ILR_ID = STG.ILR_ID
          and ILRS.REVISION = STG.REVISION
          and ILRS.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
          );

      end if;


      return 'OK';

  exception when others then
    return 'ERROR: Loading deferred rent: ' || sqlerrm;

  end F_LOAD_DEFERRED_RENT;
  -- KRD 4/6/17 - Function to extend schedules for assets with BPOs out to the last month of the depreciable life
  -- KRD 10/30/17 - Copy and pasted the function from Jared's package in here
  FUNCTION F_BPO_SCHEDULE_EXTEND (
                                  A_LS_ASSET_ID IN NUMBER,
                                  A_ILR_ID      IN NUMBER,
                                  A_REVISION    IN NUMBER
                                 )
  RETURN VARCHAR2
  IS

  L_STATUS              VARCHAR2(1000);
  L_MAX_MONTH           DATE;
  L_MID_PERIOD_CONV     NUMBER;
  L_BPO_CHECK     NUMBER;

  BEGIN


    L_STATUS := 'Checking for purchase option';
  select PURCHASE_OPTION_TYPE_ID
  into L_BPO_CHECK
  from LS_ILR_OPTIONS
  where ILR_ID = A_ILR_ID
  and REVISION = A_REVISION;

    L_STATUS := 'Extending schedules for Asset: ' || A_LS_ASSET_ID || '  ILR: ' || A_ILR_ID || '  Revision: ' || A_REVISION;
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);


    L_STATUS := 'Calculating final month on amortization schedule';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

-- CJS 9/28/17 Need month - 1 for max month
-- CJS 10/3/17 Account for both purchase option extension and just MPM extension
-- KRD 12/11/17 - Change logic to use max Payment Start Date + Number of Months instead of pulling the current
--                  maximum schedule month which may have already been extended
    SELECT Decode(L_BPO_CHECK, 1, z.last_month, Add_Months(z.first_month, la.economic_life - 1))
    INTO L_MAX_MONTH
    FROM ls_asset la,
        (
          SELECT la.ls_asset_id,
                term.payment_term_date AS first_month,
                Add_Months(term2.payment_term_date, term2.number_of_terms * Decode(term2.payment_freq_id,1,12,2,6,3,3,1) - 1) AS last_month
          FROM ls_asset la,
              ls_ilr_payment_term term,
              ls_ilr_payment_term term2
          WHERE la.ilr_id = term.ilr_id
          AND   la.ilr_id = term2.ilr_id
          AND   la.ls_asset_id = A_LS_ASSET_ID
          AND   term.revision = A_REVISION
          AND   term2.revision = A_REVISION
          AND   term.payment_term_id = (SELECT Min(payment_term_id) FROM ls_ilr_payment_term WHERE ilr_id = term.ilr_id AND revision = A_REVISION)
          AND   term2.payment_term_id = (SELECT Max(payment_term_id) FROM ls_ilr_payment_term WHERE ilr_id = term.ilr_id AND revision = A_REVISION)
        ) z
    WHERE la.ls_asset_id = z.ls_asset_id;

    L_STATUS := 'Getting Mid Period Convention on Asset';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

  SELECT To_number( Nvl( mid_period_conv, -1 ) )
  INTO   l_mid_period_conv
  FROM   ( SELECT Nvl( dg.mid_period_conv, -1 ) mid_period_conv
       FROM   LS_ASSET la,
          DEPR_GROUP dg
       WHERE  la.depr_group_id = dg.depr_group_id AND
          la.ls_asset_id = a_ls_asset_id AND
          la.depr_group_id IS NOT NULL
       UNION
       SELECT 1
       FROM   LS_ASSET
       WHERE  depr_group_id IS NULL AND
          ls_asset_id = a_ls_asset_id );

    if L_BPO_CHECK = 1 and L_MID_PERIOD_CONV in (-1,1) then
        return 'OK';
    end if;

    L_STATUS := 'Inserting new schedule rows';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

-- KRD 4/17/17 - Adding decode over mid period conv to determine if we add 1 extra month at the end
--               NVL will handle cases where this function is called from F_SAVE_SCHEDULES, otherwise it will be called again
--                from F_ADD_ASSET after the DG has been found
    INSERT INTO LS_ASSET_SCHEDULE (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST, END_CAPITAL_COST,
                                  BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION,
                                  BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
                                  INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID,
                                  PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
                                  EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4,
                                  EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
                                  CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9,
                                  CONTINGENT_ACCRUAL10, CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7,
                                  CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, IS_OM, CURRENT_LEASE_COST,
                                  BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
                                  PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, PREPAID_RENT, END_PREPAID_RENT,
                                  BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, partial_month_percent,
                                  ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS, UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS, BEGIN_ACCUM_IMPAIR, END_ACCUM_IMPAIR, IMPAIRMENT_ACTIVITY)
      SELECT las.ls_asset_id as LS_ASSET_ID,
            las.revision as REVISION,
            las.set_of_books_id as SET_OF_BOOKS_ID,
            add_months(las.month, n.the_row) as MONTH,
            0 as RESIDUAL_AMOUNT,
            0 as TERM_PENALTY,
            0 as BPO_PRICE,
            las.END_CAPITAL_COST as BEG_CAPITAL_COST,
            las.END_CAPITAL_COST as END_CAPITAL_COST,
            0 as BEG_OBLIGATION,
            0 as END_OBLIGATION,
            0 as BEG_LT_OBLIGATION,
            0 as END_LT_OBLIGATION,
            0 as BEG_LIABILITY,
            0 as END_LIABILITY,
            0 as BEG_LT_LIABILITY,
            0 as END_LT_LIABILITY,
            0 as INTEREST_ACCRUAL,
            0 as PRINCIPAL_ACCRUAL,
            0 as INTEREST_PAID,
            0 as PRINCIPAL_PAID,
            0 as EXECUTORY_ACCRUAL1,
            0 as EXECUTORY_ACCRUAL2,
            0 as EXECUTORY_ACCRUAL3,
            0 as EXECUTORY_ACCRUAL4,
            0 as EXECUTORY_ACCRUAL5,
            0 as EXECUTORY_ACCRUAL6,
            0 as EXECUTORY_ACCRUAL7,
            0 as EXECUTORY_ACCRUAL8,
            0 as EXECUTORY_ACCRUAL9,
            0 as EXECUTORY_ACCRUAL10,
            0 as EXECUTORY_PAID1,
            0 as EXECUTORY_PAID2,
            0 as EXECUTORY_PAID3,
            0 as EXECUTORY_PAID4,
            0 as EXECUTORY_PAID5,
            0 as EXECUTORY_PAID6,
            0 as EXECUTORY_PAID7,
            0 as EXECUTORY_PAID8,
            0 as EXECUTORY_PAID9,
            0 as EXECUTORY_PAID10,
            0 as CONTINGENT_ACCRUAL1,
            0 as CONTINGENT_ACCRUAL2,
            0 as CONTINGENT_ACCRUAL3,
            0 as CONTINGENT_ACCRUAL4,
            0 as CONTINGENT_ACCRUAL5,
            0 as CONTINGENT_ACCRUAL6,
            0 as CONTINGENT_ACCRUAL7,
            0 as CONTINGENT_ACCRUAL8,
            0 as CONTINGENT_ACCRUAL9,
            0 as CONTINGENT_ACCRUAL10,
            0 as CONTINGENT_PAID1,
            0 as CONTINGENT_PAID2,
            0 as CONTINGENT_PAID3,
            0 as CONTINGENT_PAID4,
            0 as CONTINGENT_PAID5,
            0 as CONTINGENT_PAID6,
            0 as CONTINGENT_PAID7,
            0 as CONTINGENT_PAID8,
            0 as CONTINGENT_PAID9,
            0 as CONTINGENT_PAID10,
            LAS.IS_OM as IS_OM,
            las.CURRENT_LEASE_COST as CURRENT_LEASE_COST,
            0 as BEG_DEFERRED_RENT,
            0 as DEFERRED_RENT,
            0 as END_DEFERRED_RENT,
            0 as BEG_ST_DEFERRED_RENT,
            0 as END_ST_DEFERRED_RENT,
            0 as PRINCIPAL_REMEASUREMENT,
            0 as LIABILITY_REMEASUREMENT,
            0 as BEG_PREPAID_RENT,
            0 as PREPAY_AMORTIZATION,
            0 as PREPAID_RENT,
            0 as END_PREPAID_RENT,
            0 AS BEG_ARREARS_ACCRUAL,
            0 AS ARREARS_ACCRUAL,
            0 AS END_ARREARS_ACCRUAL,
            las.partial_month_percent as partial_month_percent,
            0 as ST_OBLIGATION_REMEASUREMENT,
            0 as LT_OBLIGATION_REMEASUREMENT,
            0 as OBLIGATION_RECLASS,
            0 as UNACCRUED_INTEREST_REMEASURE,
            0 as UNACCRUED_INTEREST_RECLASS,
			las.BEGIN_ACCUM_IMPAIR,
			LAS.END_ACCUM_IMPAIR,
			LAS.IMPAIRMENT_ACTIVITY
      FROM ls_asset la,
           (
            SELECT la.ls_asset_id, las.revision, las.set_of_books_id, las.is_om, las.end_capital_cost, las.current_lease_cost,
				begin_accum_impair, end_accum_impair, impairment_activity, las.partial_month_percent, Max(las.month) month,
                row_number() over (partition by la.ls_asset_id, las.revision, las.set_of_books_id order by max(las.month) desc) the_row2
            FROM ls_asset_schedule las,
                 ls_asset la
            WHERE la.ls_asset_id = las.ls_asset_id
            AND las.revision = A_REVISION
            AND la.ls_asset_id = A_LS_ASSET_ID
            GROUP BY la.ls_asset_id, las.revision, las.set_of_books_id, las.is_om, las.end_capital_cost, las.current_lease_cost, las.partial_month_percent, begin_accum_impair, end_accum_impair, impairment_activity
           ) las,
           (
            SELECT ROWNUM AS the_row
            FROM dual
            CONNECT BY LEVEL <= 10000
           ) n
      WHERE la.ls_asset_id = las.ls_asset_id
    AND   las.the_row2 = 1
      AND   Add_Months(las.MONTH, n.the_row) <= Decode(L_MID_PERIOD_CONV, 1, L_MAX_MONTH, -1, L_MAX_MONTH, Add_Months(L_MAX_MONTH,1));

    PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted to LS_ASSET_SCHEDULE');

    INSERT INTO ls_ilr_Schedule (ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST,
                                END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION,
                                BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
                                INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
                                EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7,
                                EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2,
                                EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
                                EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3,
                                CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
                                CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3,
                                CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9,
                                CONTINGENT_PAID10, IS_OM, CURRENT_LEASE_COST,
                                BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
                                PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, PREPAID_RENT, END_PREPAID_RENT,
                                BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL,
                                ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS, UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS,
								BEGIN_ACCUM_IMPAIR, END_ACCUM_IMPAIR, IMPAIRMENT_ACTIVITY)
      SELECT ilrs.ilr_id,
            ilrs.revision,
            ilrs.set_of_books_id,
            add_months(ilrs.month, n.the_row) as MONTH,
            0 as RESIDUAL_AMOUNT,
            0 as TERM_PENALTY,
            0 as BPO_PRICE,
            ilrs.END_CAPITAL_COST as BEG_CAPITAL_COST,
            ilrs.END_CAPITAL_COST as END_CAPITAL_COST,
            0 as BEG_OBLIGATION,
            0 as END_OBLIGATION,
            0 as BEG_LT_OBLIGATION,
            0 as END_LT_OBLIGATION,
            0 as BEG_LIABILITY,
            0 as END_LIABILITY,
            0 as BEG_LT_LIABILITY,
            0 as END_LT_LIABILITY,
            0 as INTEREST_ACCRUAL,
            0 as PRINCIPAL_ACCRUAL,
            0 as INTEREST_PAID,
            0 as PRINCIPAL_PAID,
            0 as EXECUTORY_ACCRUAL1,
            0 as EXECUTORY_ACCRUAL2,
            0 as EXECUTORY_ACCRUAL3,
            0 as EXECUTORY_ACCRUAL4,
            0 as EXECUTORY_ACCRUAL5,
            0 as EXECUTORY_ACCRUAL6,
            0 as EXECUTORY_ACCRUAL7,
            0 as EXECUTORY_ACCRUAL8,
            0 as EXECUTORY_ACCRUAL9,
            0 as EXECUTORY_ACCRUAL10,
            0 as EXECUTORY_PAID1,
            0 as EXECUTORY_PAID2,
            0 as EXECUTORY_PAID3,
            0 as EXECUTORY_PAID4,
            0 as EXECUTORY_PAID5,
            0 as EXECUTORY_PAID6,
            0 as EXECUTORY_PAID7,
            0 as EXECUTORY_PAID8,
            0 as EXECUTORY_PAID9,
            0 as EXECUTORY_PAID10,
            0 as CONTINGENT_ACCRUAL1,
            0 as CONTINGENT_ACCRUAL2,
            0 as CONTINGENT_ACCRUAL3,
            0 as CONTINGENT_ACCRUAL4,
            0 as CONTINGENT_ACCRUAL5,
            0 as CONTINGENT_ACCRUAL6,
            0 as CONTINGENT_ACCRUAL7,
            0 as CONTINGENT_ACCRUAL8,
            0 as CONTINGENT_ACCRUAL9,
            0 as CONTINGENT_ACCRUAL10,
            0 as CONTINGENT_PAID1,
            0 as CONTINGENT_PAID2,
            0 as CONTINGENT_PAID3,
            0 as CONTINGENT_PAID4,
            0 as CONTINGENT_PAID5,
            0 as CONTINGENT_PAID6,
            0 as CONTINGENT_PAID7,
            0 as CONTINGENT_PAID8,
            0 as CONTINGENT_PAID9,
            0 as CONTINGENT_PAID10,
            ilrs.is_om,
            ilrs.current_lease_cost,
            0 as BEG_DEFERRED_RENT,
            0 as DEFERRED_RENT,
            0 as END_DEFERRED_RENT,
            0 as BEG_ST_DEFERRED_RENT,
            0 as END_ST_DEFERRED_RENT,
            0 as PRINCIPAL_REMEASUREMENT,
            0 as LIABILITY_REMEASUREMENT,
            0 as BEG_PREPAID_RENT,
            0 as PREPAY_AMORTIZATION,
            0 as PREPAID_RENT,
            0 as END_PREPAID_RENT,
            0 AS BEG_ARREARS_ACCRUAL,
            0 AS ARREARS_ACCRUAL,
            0 AS END_ARREARS_ACCRUAL,
            0 as ST_OBLIGATION_REMEASUREMENT,
            0 as LT_OBLIGATION_REMEASUREMENT,
            0 as OBLIGATION_RECLASS,
            0 as UNACCRUED_INTEREST_REMEASURE,
            0 as UNACCRUED_INTEREST_RECLASS,
			ilrs.begin_accum_impair,
			ilrs.end_accum_impair,
			ilrs.impairment_activity
      FROM (
            SELECT ilr_id, revision, set_of_books_id, is_om, end_capital_cost, current_lease_cost,
				begin_accum_impair, end_accum_impair, impairment_activity, Max(MONTH) AS MONTH,
                row_number() over (partition by ilr_id, revision, set_of_books_id order by max(month) desc) the_row2
            FROM ls_ilr_schedule
            WHERE revision = A_REVISION
            AND   ilr_id = A_ILR_ID
            GROUP BY ilr_id, revision, set_of_books_id, is_om, end_capital_cost, current_lease_cost, begin_accum_impair, end_accum_impair, impairment_activity
           ) ilrs,
           (
            SELECT ROWNUM AS the_row
            FROM dual CONNECT BY LEVEL <= 10000
           ) n
      WHERE Add_Months(ilrs.MONTH, n.the_row) <= Decode(L_MID_PERIOD_CONV, 1, L_MAX_MONTH, -1, L_MAX_MONTH, Add_Months(L_MAX_MONTH,1))
      AND   ilrs.the_row2 = 1;


    PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted to LS_ILR_SCHEDULE');

    PKG_PP_LOG.P_WRITE_MESSAGE('Successfully extended schedules');

  RETURN 'OK';
    EXCEPTION WHEN OTHERS THEN
    RETURN 'SQL ERROR AT LOCATION: '||L_STATUS||' - ' || sqlerrm;

  END F_BPO_SCHEDULE_EXTEND;

  --*******************************************************************************************
   --                            F_ADD_PAYMENT_ADJUSTMENTS
   --
   -- 2017.4.0 06/27/2016 Powers       PP-51540 Joined ILR merge with LS_PAYMENT_HDR for speed
   --******************************************************************************************
   function F_ADD_PAYMENT_ADJUSTMENTS(A_PAYMENT_ID in number) return number is

      RTN number;
      A_STATUS VARCHAR2(100);
      sqls VARCHAR2(4000);
      v_Rows_Updated NUMBER;

   -- First check for any adjustments and if so, start looping
   begin

     PKG_PP_LOG.P_WRITE_MESSAGE('Starting F_ADD_PAYMENT_ADJUSTMENTS'||to_char(a_payment_id));

     v_Rows_Updated:=0;

     BEGIN
       FOR loop_buckets IN

          -- Look for any adjustments and map to the correct bucket
          (select lpl.payment_id payment_id,lpl.payment_type_id payment_type_id,las.ls_asset_id asset_id,
                  lpl.set_of_books_id sob_id,li.current_revision rev,to_char(month,'YYYYMM') the_month,
                  decode(lpt.description,'Executory 1','executory_paid1','Executory 2','executory_paid2',
                                         'Executory 3','executory_paid3','Executory 4','executory_paid4',
                                         'Executory 5','executory_paid5','Executory 6','executory_paid6',
                                         'Executory 7','executory_paid7','Executory 8','executory_paid8',
                                         'Executory 9','executory_paid9','Executory 10','executory_paid10',
                                         'Contingent 1','contingent_paid1','Contingent 2','contingent_paid2',
                                         'Contingent 3','contingent_paid3','Contingent 4','contingent_paid4',
                                         'Contingent 5','contingent_paid5','Contingent 6','contingent_paid6',
                                         'Contingent 7','contingent_paid7','Contingent 8','contingent_paid8',
                                         'Contingent 9','contingent_paid9','Contingent 10','contingent_paid10',
                                         ' ') bucket, sum(adjustment_amount) amt
                from ls_payment_line lpl, ls_payment_hdr lph, ls_ilr li, ls_asset_schedule las, ls_asset la, ls_payment_type lpt
                WHERE las.ls_asset_id = lpl.ls_asset_id
                and las.revision = li.current_revision
                and las.set_of_books_id = lpl.set_of_books_id
                and lpl.payment_id = lph.payment_id
                and case when lph.ls_asset_id = -1 then -1 else las.ls_asset_id end = lph.ls_asset_id
                and la.ls_asset_id = lpl.ls_asset_id
                and la.ilr_id = li.ilr_id
                and lpt.payment_type_id = lpl.payment_type_id
                and lph.gl_posting_mo_yr = lph.gl_posting_mo_yr
                and lph.gl_posting_mo_yr = las.month
                and lph.payment_status_id = 3
                and lpl.payment_type_id between 3 and 22
                and lpl.adjustment_amount <> 0
                and lpl.payment_id = A_PAYMENT_ID
                and case when lph.ilr_id = -1 then -1 else li.ilr_id end = lph.ilr_id
                group by lpl.payment_id,lpl.payment_type_id,las.ls_asset_id,lpl.set_of_books_id,li.current_revision,to_char(month,'YYYYMM'),lpt.description  )

          LOOP

          sqls:= 'update ls_asset_schedule set '||loop_buckets.bucket||'=nvl('||loop_buckets.bucket||',0) +nvl('||to_char(loop_buckets.amt)||',0) where ls_asset_id='||to_char(loop_buckets.asset_id)||
                    ' and revision='||to_char(loop_buckets.rev)||' and set_of_books_id='||to_char(loop_buckets.sob_id)||
                    ' and to_char(month,''YYYYMM'')='''||loop_buckets.the_month||'''';

          BEGIN

            EXECUTE IMMEDIATE sqls;

            v_Rows_Updated := v_Rows_Updated + nvl(sql%rowcount,0);

            EXCEPTION
                WHEN OTHERS THEN
                    PKG_PP_LOG.P_WRITE_MESSAGE('Error executing F_ADD_PAYMENT_ADJUSTMENT - '||SQLCODE||' -ERROR- '||SQLERRM);
                    return -1;
          END;

           PKG_PP_LOG.P_WRITE_MESSAGE(to_char(nvl(v_Rows_Updated,0))|| ' rows merged to LS_ASSET_SCHEDULE');

           END LOOP loop_buckets;
        END;

        --If nothing to process, then return
        if v_Rows_Updated = 0 then
          return 1;
        end if;

    -- Summate adjustments by contingent/executory buckets (aka, 3-12 and 13 to 22)
     merge into ls_asset_schedule a
      using (select li.current_revision ,lpl.ls_asset_id, lpl.gl_posting_mo_yr, lpl.set_of_books_id,sum(nvl(lpl.adjustment_amount,0)) as adjustment_amount
            from ls_payment_line lpl,ls_payment_hdr lph,ls_ilr li, ls_lease ll
            where case when lph.ilr_id = -1 then -1 else li.ilr_id end = lph.ilr_id
            and lpl.payment_id = lph.payment_id
            and lph.payment_status_id = 3
            and case when lph.ls_asset_id = -1 then -1 else lpl.ls_asset_id end = lph.ls_asset_id
            and lpl.payment_type_id between 3 and 12
            and lph.gl_posting_mo_yr = lpl.gl_posting_mo_yr
      and ll.lease_id = lph.lease_id
      and ll.lease_id = li.lease_id
            and lpl.adjustment_amount <> 0
            and lpl.payment_id = A_PAYMENT_ID
            group by li.current_revision ,lpl.ls_asset_id, lpl.gl_posting_mo_yr, lpl.set_of_books_id) b
      on ( a.ls_asset_id = b.ls_asset_id and
         a.month = b.gl_posting_mo_yr and
         a.set_of_books_id = b.set_of_books_id and
         a.revision = b.current_revision)
      when matched then
            update set a.executory_adjust = nvl(a.executory_adjust,0) + nvl(b.adjustment_amount,0);

      PKG_PP_LOG.P_WRITE_MESSAGE(to_char(SQL%ROWCOUNT) || ' rows merged to LS_ASSET_SCHEDULE for Executory Adjustments');

    merge into ls_asset_schedule a
      using (select li.current_revision ,lpl.ls_asset_id, lpl.gl_posting_mo_yr, lpl.set_of_books_id,sum(nvl(lpl.adjustment_amount,0)) as adjustment_amount
            from ls_payment_line lpl,ls_payment_hdr lph,ls_ilr li, ls_lease ll
            where case when lph.ilr_id = -1 then -1 else li.ilr_id end = lph.ilr_id
            and lpl.payment_id = lph.payment_id
            and lph.payment_status_id = 3
            and case when lph.ls_asset_id = -1 then -1 else lpl.ls_asset_id end = lph.ls_asset_id
            and lpl.payment_type_id between 13 and 22
            and lph.gl_posting_mo_yr = lpl.gl_posting_mo_yr
      and ll.lease_id = lph.lease_id
      and ll.lease_id = li.lease_id
            and lpl.adjustment_amount <> 0
            and lpl.payment_id = A_PAYMENT_ID
            group by li.current_revision ,lpl.ls_asset_id, lpl.gl_posting_mo_yr, lpl.set_of_books_id) b
      on ( a.ls_asset_id = b.ls_asset_id and
         a.month = b.gl_posting_mo_yr and
         a.set_of_books_id = b.set_of_books_id and
         a.revision = b.current_revision)
      when matched then
            update set a.contingent_adjust = nvl(a.contingent_adjust,0) + nvl(b.adjustment_amount,0);

      PKG_PP_LOG.P_WRITE_MESSAGE(to_char(SQL%ROWCOUNT) || ' rows merged to LS_ASSET_SCHEDULE for Contingent Adjustments');

      PKG_PP_LOG.P_WRITE_MESSAGE('Rolling up sums into LS_ILR_SCHEDULE');

    merge into ls_ilr_schedule a
      using (select la.ilr_id, li.current_revision,las.month,las.set_of_books_id,
            sum(contingent_adjust) as contingent_adjust,
            sum(executory_adjust) as executory_adjust,
            sum(executory_paid1) as executory_paid1,
            sum(executory_paid2) as executory_paid2,
            sum(executory_paid3) as executory_paid3,
            sum(executory_paid4) as executory_paid4,
            sum(executory_paid5) as executory_paid5,
            sum(executory_paid6) as executory_paid6,
            sum(executory_paid7) as executory_paid7,
            sum(executory_paid8) as executory_paid8,
            sum(executory_paid9) as executory_paid9,
            sum(executory_paid10) as executory_paid10,
            sum(contingent_paid1) as contingent_paid1,
            sum(contingent_paid2) as contingent_paid2,
            sum(contingent_paid3) as contingent_paid3,
            sum(contingent_paid4) as contingent_paid4,
            sum(contingent_paid5) as contingent_paid5,
            sum(contingent_paid6) as contingent_paid6,
            sum(contingent_paid7) as contingent_paid7,
            sum(contingent_paid8) as contingent_paid8,
            sum(contingent_paid9) as contingent_paid9,
            sum(contingent_paid10) as contingent_paid10
          from ls_asset_schedule las, ls_asset la, ls_ilr li
          where la.ilr_id = li.ilr_id
          and las.revision = li.current_revision
          and la.ls_asset_id = las.ls_asset_id
          AND EXISTS (SELECT 1
                      FROM ls_payment_line lpl, ls_payment_hdr lph
                      WHERE lpl.payment_id = lph.payment_id
                      AND lph.gl_posting_mo_yr = las.month
                      AND lph.payment_id = A_PAYMENT_ID
                      and case when lph.ls_asset_id = -1 then -1 else las.ls_asset_id end = lph.ls_asset_id
                      AND lpl.ls_asset_id = las.ls_asset_id
                      AND lpl.ls_asset_id = la.ls_asset_id
                      AND lph.payment_status_id = 3 )
          and ((las.executory_adjust <> 0 or las.contingent_adjust <> 0))
          group by la.ilr_id, li.current_revision,las.month,las.set_of_books_id) b
      on ( a.ilr_id = b.ilr_id and
         a.month = b.month and
         a.set_of_books_id = b.set_of_books_id and
         a.revision = b.current_revision)
      when matched then
            update set a.contingent_adjust = nvl(b.contingent_adjust,0), a.executory_adjust = nvl(b.executory_adjust,0),
            a.executory_paid1 = nvl(b.executory_paid1,0),a.executory_paid2 = nvl(b.executory_paid2,0),a.executory_paid3 = nvl(b.executory_paid3,0),
            a.executory_paid4 = nvl(b.executory_paid4,0),a.executory_paid5 = nvl(b.executory_paid5,0),a.executory_paid6 = nvl(b.executory_paid6,0),
            a.executory_paid7 = nvl(b.executory_paid7,0),a.executory_paid8 = nvl(b.executory_paid8,0),a.executory_paid9 = nvl(b.executory_paid9,0),
            a.executory_paid10 = nvl(b.executory_paid10,0),
            a.contingent_paid1 = nvl(b.contingent_paid1,0),a.contingent_paid2 = nvl(b.contingent_paid2,0),a.contingent_paid3 = nvl(b.contingent_paid3,0),
            a.contingent_paid4 = nvl(b.contingent_paid4,0),a.contingent_paid5 = nvl(b.contingent_paid5,0),a.contingent_paid6 = nvl(b.contingent_paid6,0),
            a.contingent_paid7 = nvl(b.contingent_paid7,0),a.contingent_paid8 = nvl(b.contingent_paid8,0),a.contingent_paid9 = nvl(b.contingent_paid9,0),
            a.contingent_paid10 = nvl(b.contingent_paid10,0) ;

      PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows summated and merged to LS_ILR_SCHEDULE');

      commit;

      return 1;

   exception
      when others then
         rollback;
         PKG_PP_LOG.P_WRITE_MESSAGE('Error executing F_ADD_PAYMENT_ADJUSTMENT ('||to_char(a_payment_id)||' - '||SQLCODE||' -ERROR- '||SQLERRM);
         return -1;
   end F_ADD_PAYMENT_ADJUSTMENTS;

  function F_CLEAR_STAGING_TABLES_NO_LOG return varchar2
  --Function used for debugging new dev. An easy way to wipe all schedule staging tables when failed schedule calc
  -- THIS SHOULD NEVER BE CALLED BY SCHEDULE BUILD LOGIC
    is
    L_STATUS                      varchar2(2000);

  begin

      L_STATUS := 'CLEARING OUT ls_ilr_asset_schedule_stg';
      delete from LS_ILR_ASSET_SCHEDULE_STG;

      L_STATUS := 'CLEARING OUT ls_ilr_asset_stg';
      delete from LS_ILR_ASSET_STG;

      L_STATUS := 'CLEARING OUT ls_ilr_inc_stg';
      delete from ls_ilr_inc_stg;

      L_STATUS := 'CLEARING OUT ls_ilr_idc_stg';
      delete from ls_ilr_idc_stg;

      L_STATUS := 'CLEARING OUT ls_ilr_schedule_stg';
      delete from LS_ILR_SCHEDULE_STG;

      L_STATUS := 'CLEARING OUT ls_ilr_stg';
      delete from LS_ILR_STG;

	  L_STATUS := 'CLEARING OUT ls_ilr_asset_schedule_calc_stg';
	  delete from ls_ilr_asset_schedule_calc_stg;

	  L_STATUS := 'CLEARING OUT ls_ilr_payment_term_stg';
	  delete from ls_ilr_payment_term_stg;

	  L_STATUS := 'CLEARING OUT ls_ilr_schedule_bucket_stg';
	  delete from ls_ilr_schedule_bucket_stg;


      return 'OK';

  exception when others then
    return 'ERROR: Clearing staging tables: ' || sqlerrm;

  end F_CLEAR_STAGING_TABLES_NO_LOG;

   --**************************************************************************
   --                            F_BUILD_SCHEDULE_IMPAIRED
   -- Specific schedule build function for Impairment Revisions of ILRs
   --**************************************************************************
  
	FUNCTION f_build_schedule_impaired(a_ilr_id          IN NUMBER,
									   a_revision        IN NUMBER,
									   a_impairment_date IN NUMBER)
	  RETURN VARCHAR2 IS
	  l_status VARCHAR2(4000);
	  l_msg    VARCHAR2(4000);
	  
	  l_bpo_check number(1,0);
	  l_mid_period_conv number;
	  l_impairment_date date;
	  
	BEGIN
	  pkg_pp_log.p_start_log(p_process_id => pkg_lease_common.f_getprocess());
	  pkg_pp_log.p_write_message('Starting build of Impaired Schedule');

	  l_impairment_date := to_date(a_impairment_date, 'yyyymm');
	  
	  --Load ls_ilr_stg for the ILR being processed
	  INSERT INTO ls_ilr_stg
		(ilr_id,
		 revision,
		 set_of_books_id)
		SELECT ilr_id, a_revision, set_of_books_id
		FROM   ls_ilr ilr
		JOIN   company_set_of_books csob ON ilr.company_id = csob.company_id
		WHERE  ilr_id = a_ilr_id;

	  --Copy all schedule rows
	  l_status := 'Copying Asset Schedule Rows from Approved Revision';
	  l_msg    := f_copy_asset_schedule_rows(to_date(299912, 'yyyymm'));

	  IF l_msg <> 'OK' THEN
		pkg_pp_log.p_write_message(l_msg);
		  RETURN l_msg;
	  END IF;

	  l_status := 'Copying ILR Schedule Rows from Approved Revision';
	  l_msg    := f_copy_ilr_schedule_rows(to_date(299912, 'yyyymm'));

	  IF l_msg <> 'OK' THEN
		pkg_pp_log.p_write_message(l_msg);
		  RETURN l_msg;
	  END IF;

	  l_status := 'Copying Depreciation records from Approved Revision';
	  l_msg    := f_copy_depr_forecast_rows(to_date(299912, 'yyyymm'));

	  IF l_msg <> 'OK' THEN
		pkg_pp_log.p_write_message(l_msg);
		  RETURN l_msg;
	  END IF;

	  --Create balance rollforward for accumulated impairment
	  --Activity set to 0 for update afterwards
	  insert into ls_ilr_impair_stg
	  (ilr_id, revision, set_of_books_id, month, impairment_activity, beginning_impairment, ending_impairment)
	  select ilr_id, revision, set_of_books_id, month, 0, 0, 0
	  from ls_ilr_schedule
	  where ilr_id = a_ilr_id
	  and revision = a_revision;
	  
	  l_status := 'Setting Impairment Activity';
	  --Set Impairment Activity on stg
	  UPDATE ls_ilr_impair_stg sched
			   SET impairment_activity =
				   (SELECT impairment_activity
					  FROM ls_ilr_impair i
					 WHERE sched.ilr_id = i.ilr_id
					   AND sched.set_of_books_id = i.set_of_books_id
					   AND sched.month = i.month)
			 WHERE EXISTS (SELECT 1
					  FROM ls_ilr_impair i
					 WHERE sched.ilr_id = i.ilr_id
					   AND sched.set_of_books_id = i.set_of_books_id
					   AND sched.month = i.month);
					   
					   
	  --Set starting balance from first impairment
	  l_status := 'Setting Impairment Starting Balance';
		UPDATE ls_ilr_impair_stg stg SET stg.ending_impairment = 
		(SELECT ending_impairment FROM (
		SELECT ilr_id, set_of_books_id, MONTH, Sum(impairment_activity) 
		OVER (PARTITION BY ilr_id, set_of_books_id ORDER BY MONTH RANGE unbounded preceding)AS ending_impairment
		FROM ls_ilr_impair_stg) a
		WHERE a.ilr_id = stg.ilr_id
		AND a.set_of_books_id = stg.set_of_books_id
		AND a.MONTH = stg.MONTH);
	  
	  -- set beginning balances for impairments ending - activity starting from the month after the first impairment
	  l_status := 'Building Impairment Balance';
		UPDATE ls_ilr_impair_stg s1
		SET beginning_impairment = ending_impairment - impairment_activity
		WHERE (set_of_books_id, MONTH) in
		(SELECT set_of_books_id, MONTH FROM ls_ilr_impair_stg s2 
		WHERE MONTH > (SELECT Min(MONTH) FROM ls_ilr_impair imp
		WHERE imp.ilr_id = s2.ilr_id
		AND s2.set_of_books_id = imp.set_of_books_id));
		
	  l_status := 'Setting Impairment Amounts on ILR Schedule';
	  update ls_ilr_schedule sched set (impairment_activity, begin_accum_impair, end_accum_impair) =
	  (select impairment_activity, beginning_impairment, ending_impairment
	  from ls_ilr_impair_stg stg
	  where stg.ilr_id = sched.ilr_Id
	  and stg.revision = sched.revision
	  and stg.set_of_books_id = sched.set_of_books_id
	  and stg.month = sched.month)
	  where ilr_id = a_ilr_id
	  and revision = a_revision
	  and month >= l_impairment_date;
	  
		
	  --Allocate Impairments to Asset Schedule based on copied schedules next Paid split after Impairment Date
	  l_status := 'Allocating Impairment to Assets';
		MERGE INTO ls_asset_schedule z
		USING (
		  WITH next_payment AS
		   (SELECT MIN(MONTH) pay_month --Pull the next payment date on the ILR schedule >= impairment date
			FROM   ls_ilr_schedule
			WHERE  ilr_id = a_ilr_id
			AND    revision = a_revision
			AND    (nvl(principal_paid, 0) <> 0 OR nvl(interest_paid, 0) <> 0)
			AND    MONTH >= l_impairment_date),
		  asset_allocation AS
		   (SELECT asset.ls_asset_id,
				   asset.set_of_books_id,
				   asset.revision,
				   (nvl(asset.interest_paid, 0) + nvl(asset.principal_paid, 0)) /
				   (nvl(ilr.principal_paid, 0) + nvl(ilr.interest_paid, 0)) allocation
			FROM   (SELECT ilr_id, ls_asset_id, revision
					FROM   ls_ilr_asset_map
					WHERE  ilr_id = a_ilr_id
					AND    revision = a_revision) MAP
			JOIN   ls_asset_schedule asset ON (map.ls_asset_id = asset.ls_asset_id AND
											  map.revision = asset.revision)
			JOIN   ls_ilr_schedule ilr ON (map.ilr_id = ilr.ilr_id AND
										  map.revision = ilr.revision AND
										  ilr.set_of_books_id =
										  asset.set_of_books_id)
			JOIN   next_payment pay ON (pay.pay_month = asset.month AND
									   ilr.month = pay.pay_month))
		  SELECT alloc.ls_asset_id,
				 alloc.set_of_books_id,
				 alloc.revision,
				 impair.month,
				 impair.impairment_activity * alloc.allocation impairment_activity,
				 impair.beginning_impairment * alloc.allocation beginning_impairment,
				 impair.ending_impairment * alloc.allocation ending_impairment
		  FROM   asset_allocation alloc
		  JOIN ls_ilr_impair_stg impair ON (impair.set_of_books_id = alloc.set_of_books_Id)) a 
		  ON (z.ls_asset_id = a.ls_asset_id 
				AND z.set_of_books_id = a.set_of_books_id 
				AND z.month = a.month
				and z.revision = a.revision) 
		 WHEN
		   MATCHED THEN
			UPDATE
			SET    z.impairment_activity = a.impairment_activity,
				   z.begin_accum_impair  = a.beginning_impairment,
				   z.end_accum_impair    = a.ending_impairment;

	  --Recalculate Depr with Impairment included
	  l_status := 'Calculating Depreciation for Impairment';
	  pkg_lease_depr.p_fcst_lease(a_ilr_id, a_revision);
	  --Restart log since Depr does some stuff that isnt tracked in this log
	  pkg_pp_log.p_start_log(p_process_id => pkg_lease_common.f_getprocess());

	  --Update Beg and End ROU Asset including Impairment
	  l_status := 'Updating BEG/END ROU Asset for Impairment on Asset Schedule';
	  pkg_pp_log.p_write_message(l_status);

	  MERGE INTO ls_asset_schedule z
	  USING (SELECT DISTINCT ls_asset_id,
							 revision,
							 set_of_books_id,
							 begin_reserve,
							 end_reserve,
							 MONTH
			 FROM   ls_depr_forecast ldf
			 WHERE  (ldf.ls_asset_id, ldf.revision, ldf.set_of_books_id) IN
					(SELECT map.ls_asset_id, stg.revision, stg.set_of_books_id
					 FROM   ls_ilr_asset_map map
					 join ls_ilr_stg stg on stg.ilr_id = map.ilr_id and stg.revision = map.revision)) a
	  ON (z.ls_asset_id = a.ls_asset_id AND z.revision = a.revision AND z.set_of_books_id = a.set_of_books_id AND z.month = a.month)
	  WHEN MATCHED THEN
		UPDATE
		SET    z.beg_net_rou_asset = nvl(z.beg_capital_cost, 0) -
									 nvl(a.begin_reserve, 0) -
									 nvl(z.begin_accum_impair, 0),
			   z.end_net_rou_asset = nvl(z.end_capital_cost, 0) -
									 nvl(a.end_reserve, 0) -
									 nvl(z.end_accum_impair, 0);

	  l_status := 'Updating BEG/END ROU Asset for Impairment on ILR Schedule';
	  pkg_pp_log.p_write_message(l_status);

	  MERGE INTO ls_ilr_schedule z
	  USING (SELECT la.ilr_id AS ilr_id,
					ldf.revision AS revision,
					ldf.set_of_books_id AS set_of_books_id,
					ldf.month AS MONTH,
					SUM(ldf.begin_reserve) AS beg_res,
					SUM(ldf.end_reserve) AS end_res
			 FROM   ls_depr_forecast ldf, ls_asset la
			 WHERE  (ldf.ls_asset_id, ldf.revision) IN
					(SELECT ls_asset_id, revision
					 FROM   ls_ilr_asset_map 
					 where ilr_id = a_ilr_id
					 and revision = a_revision)
			 AND    (la.ilr_id, ldf.revision, ldf.set_of_books_id) IN
					(SELECT ilr_id, revision, set_of_books_id
					  FROM   ls_ilr_stg)
			 AND    ldf.ls_asset_id = la.ls_asset_id
			 GROUP  BY la.ilr_id, ldf.revision, ldf.set_of_books_id, MONTH) a
	  ON (z.ilr_id = a.ilr_id AND z.revision = a.revision AND z.set_of_books_id = a.set_of_books_id AND z.month = a.month)
	  WHEN MATCHED THEN
		UPDATE
		SET    z.beg_net_rou_asset = nvl(z.beg_capital_cost, 0) -
									 nvl(a.beg_res, 0) -
									 nvl(z.begin_accum_impair, 0),
			   z.end_net_rou_asset = nvl(z.end_capital_cost, 0) -
									 nvl(a.end_res, 0) -
									 nvl(z.end_accum_impair, 0);

	  --Clear calc tables
	  delete from ls_ilr_stg;
	  delete from ls_depr_info_tmp;
	  delete from ls_ilr_impair_stg;

	  RETURN 'OK';
	  
	exception when others then		
		return 'Error '||l_status||': ' || sqlerrm;
	END f_build_schedule_impaired;
	
end;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (18522, 0, 2018, 2, 1, 0, 0, 'C:\BitBucketRepos\classic_pb\scripts\00_base\packages', 
    'PKG_LEASE_SCHEDULE.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
