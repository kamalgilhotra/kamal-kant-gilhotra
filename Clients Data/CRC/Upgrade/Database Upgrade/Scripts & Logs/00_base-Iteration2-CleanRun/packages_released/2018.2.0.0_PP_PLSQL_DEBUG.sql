
-- Create Package
create or replace package PP_PLSQL_DEBUG is

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PP_PLSQL_DEBUG
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 06/21/2013 Roger Roach
   ||
   ||============================================================================
   */
   G_PKG_VERSION varchar(35) := '2018.2.0.0';
   
   type RESULTS_T is table of varchar2(200);

   procedure ABORT;

   procedure BACKTRACE;

   -- highly expermiental
   procedure CURRENT_PRG;

   procedure BREAKPOINTS;

   procedure REFRESH;

   procedure CONTINUE_(BREAK_FLAGS in number);

   procedure CONTINUE;

   procedure BREAK_ON_NEXT_LINE;

   procedure DELETE_BP(BREAKPOINT in binary_integer);

   procedure PRINT_VAR(name in varchar2);
   function GET_VAR(name  in varchar2,
                    A_VAL out varchar2) return number;

   procedure START_DEBUGGER(DEBUG_SESSION_ID in varchar2);

   function START_DEBUGEE return varchar2;

   procedure DEBUG_PROCEDURE_ON(name in varchar2);

   procedure DEBUG_PROCEDURE_OFF(name in varchar2);

   procedure PRINT_PROGINFO(PRGINFO DBMS_DEBUG.PROGRAM_INFO);

   procedure PRINT_RUNTIME_INFO(RUNINFO DBMS_DEBUG.RUNTIME_INFO);

   procedure PRINT_SOURCE(RUNINFO      DBMS_DEBUG.RUNTIME_INFO,
                          LINES_BEFORE number default 0,
                          LINES_AFTER  number default 0);

   procedure PRINT_RUNTIME_INFO_WITH_SOURCE(RUNINFO DBMS_DEBUG.RUNTIME_INFO
                                            --v_lines_before in number,
                                            --v_lines_after  in number,
                                            -- v_lines_width  in number
                                            );

   procedure SELF_CHECK;

   procedure SET_BREAKPOINT(P_LINE     in number,
                            P_NAME     in varchar2 default null,
                            P_OWNER    in varchar2 default null,
                            P_CURSOR   in boolean default false,
                            P_TOPLEVEL in boolean default false,
                            P_BODY     in boolean default false,
                            P_TRIGGER  in boolean default false);

   procedure QUIT_DB;

   procedure EXECUTE_SQL(SQLS varchar2);

   procedure EXECUTE_SQL2(SQLS varchar2);

   procedure STEP;

   procedure STEP_INTO;

   procedure STEP_OUT;

   function STR_FOR_NAMESPACE(NSP in binary_integer) return varchar2;

   function STR_FOR_REASON_IN_RUNTIME_INFO(RSN in binary_integer) return varchar2;

   procedure WAIT_UNTIL_RUNNING;

   procedure IS_RUNNING;

   procedure VERSION;

   procedure DETACH;

   function LIBUNITTYPE_AS_STRING(LUT binary_integer) return varchar2;

   function BP_STATUS_AS_STRING(BPS binary_integer) return varchar2;

   function GENERAL_ERROR(E in binary_integer) return varchar2;

   -- the following vars are used whenever continue returnes and shows
   -- the lines arount line
   CONT_LINES_BEFORE_ number;

   CONT_LINES_AFTER_ number;
   --  cont_lines_width_  number;

   -- Store the current line of execution
   CUR_LINE_ DBMS_DEBUG.RUNTIME_INFO;

end PP_PLSQL_DEBUG;
/

create or replace package body PP_PLSQL_DEBUG is
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PP_PLSQL_DEBUG
   || Description:
   ||============================================================================
   || Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     10-09-2012 Roger Roach    Initial coding  *
   || 1.1     10-22.2012 ROger Roach    Added the procedure execute_sql
   || 1.2     10-24.2012 ROger Roach    Added the plsql profiler
   */


   --****************************************************************************
   --                            abort
   --****************************************************************************
   procedure ABORT is

      RUNINFO DBMS_DEBUG.RUNTIME_INFO;
      RET     binary_integer;

   begin
      CONTINUE_(DBMS_DEBUG.ABORT_EXECUTION);
   end ABORT;


   --****************************************************************************
   --                            BREAK_ON_NEXT_LINE
   --****************************************************************************
   procedure BREAK_ON_NEXT_LINE as

   begin
      CONTINUE_(DBMS_DEBUG.BREAK_ANY_CALL);
   end BREAK_ON_NEXT_LINE;


   --****************************************************************************
   --                            BACKTRACE
   --****************************************************************************
   procedure BACKTRACE is

      PKGS DBMS_DEBUG.BACKTRACE_TABLE;
      I    number;

   begin
      DBMS_DEBUG.PRINT_BACKTRACE(PKGS);
      I := PKGS.FIRST();
      DBMS_OUTPUT.PUT_LINE('backtrace');
      while I is not null
      loop
         DBMS_OUTPUT.PUT_LINE('  ' || I || ': ' || PKGS(I).NAME || ' (' || PKGS(I).LINE# || ')');
         I := PKGS.NEXT(I);
      end loop;
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('  backtrace exception: ' || sqlcode);
         DBMS_OUTPUT.PUT_LINE('                       ' || sqlerrm(sqlcode));
   end; -- backtrace


   --****************************************************************************
   --                            BREAKPOINTS
   --****************************************************************************
   procedure BREAKPOINTS is

      BRKPTS DBMS_DEBUG.BREAKPOINT_TABLE;
      I      number;
      V_LINE number;

   begin
      DBMS_DEBUG.SHOW_BREAKPOINTS(BRKPTS);
      I := BRKPTS.FIRST();
      DBMS_OUTPUT.PUT_LINE('');
      while I is not null
      loop
         if V_LINE is not null then
            DBMS_OUTPUT.PUT(TO_CHAR(V_LINE, '99999'));
            null;
         else
            DBMS_OUTPUT.PUT('      ');
         end if;

         DBMS_OUTPUT.PUT(' ');

         DBMS_OUTPUT.PUT(TO_CHAR(I, '999') || ': ');
         DBMS_OUTPUT.PUT(RPAD(COALESCE(BRKPTS(I).NAME, ' '), 31));

         DBMS_OUTPUT.PUT(RPAD(COALESCE(BRKPTS(I).OWNER, ' '), 31));

         V_LINE := BRKPTS(I).LINE#;

         DBMS_OUTPUT.PUT(' ');
         DBMS_OUTPUT.PUT(LIBUNITTYPE_AS_STRING(BRKPTS(I).LIBUNITTYPE));
         DBMS_OUTPUT.PUT(' ');
         DBMS_OUTPUT.PUT(BP_STATUS_AS_STRING(BRKPTS(I).STATUS));

         DBMS_OUTPUT.PUT_LINE('');
         I := BRKPTS.NEXT(I);
      end loop;
   end BREAKPOINTS;

   /*
       dbms_debug.continue can be called with the following breakflags:
        o  break_next_line       ( Break at next source line (step over calls) )
        o  break_any_call        ( Break at next source line (step into calls) )
        o  break_any_return
        o  break_return
        o  break_exception
        o  break_handler
        o  abort_execution

        As the user of adp_debug might want to use continue with variying breakflags, continue_ (with the
        underscore) is the generic wrapper. (I hope this makes sense)
   */


   --****************************************************************************
   --                            LIBUNITTYPE_AS_STRING
   --****************************************************************************
   function LIBUNITTYPE_AS_STRING(LUT binary_integer) return varchar2 is

   begin

      if LUT = DBMS_DEBUG.LIBUNITTYPE_CURSOR then
         return 'Cursor';
      end if;
      if LUT = DBMS_DEBUG.LIBUNITTYPE_PROCEDURE then
         return 'Proc';
      end if;
      if LUT = DBMS_DEBUG.LIBUNITTYPE_FUNCTION then
         return 'Func';
      end if;
      if LUT = DBMS_DEBUG.LIBUNITTYPE_FUNCTION then
         return 'Func';
      end if;
      if LUT = DBMS_DEBUG.LIBUNITTYPE_PACKAGE then
         return 'Pkg';
      end if;
      if LUT = DBMS_DEBUG.LIBUNITTYPE_PACKAGE_BODY then
         return 'Pkg Bd';
      end if;
      if LUT = DBMS_DEBUG.LIBUNITTYPE_TRIGGER then
         return 'Trig';
      end if;
      if LUT = DBMS_DEBUG.LIBUNITTYPE_UNKNOWN then
         return 'Unk';
      end if;

      return '???';

   end LIBUNITTYPE_AS_STRING;


   --****************************************************************************
   --                            BP_STATUS_AS_STRING
   --****************************************************************************
   -- "User friendly" name for breakpoint_status_*
   function BP_STATUS_AS_STRING(BPS binary_integer) return varchar2 is

   begin

      if BPS = DBMS_DEBUG.BREAKPOINT_STATUS_UNUSED then
         return 'unused';
      end if;
      if BPS = DBMS_DEBUG.BREAKPOINT_STATUS_ACTIVE then
         return 'active';
      end if;
      if BPS = DBMS_DEBUG.BREAKPOINT_STATUS_DISABLED then
         return 'disabled';
      end if;
      if BPS = DBMS_DEBUG.BREAKPOINT_STATUS_REMOTE then
         return 'remote';
      end if;

      return '???';

   end BP_STATUS_AS_STRING;


   --****************************************************************************
   --                            CONTINUE_
   --****************************************************************************
   procedure CONTINUE_(BREAK_FLAGS in number) is

      RET   binary_integer;
      V_ERR varchar2(100);

   begin
      DBMS_OUTPUT.PUT_LINE('');

      RET := DBMS_DEBUG.CONTINUE(CUR_LINE_,
                                 BREAK_FLAGS,
                                 0 + DBMS_DEBUG.INFO_GETLINEINFO + DBMS_DEBUG.INFO_GETBREAKPOINT +
                                 DBMS_DEBUG.INFO_GETSTACKDEPTH + DBMS_DEBUG.INFO_GETOERINFO + 0);

      if RET = DBMS_DEBUG.SUCCESS then
         DBMS_OUTPUT.PUT_LINE('  reason for break: ' ||
                              STR_FOR_REASON_IN_RUNTIME_INFO(CUR_LINE_.REASON));
         if CUR_LINE_.REASON = DBMS_DEBUG.REASON_KNL_EXIT then
            return;
         end if;
         if CUR_LINE_.REASON = DBMS_DEBUG.REASON_EXIT then
            return;
         end if;
         --print_runtime_info_with_source(cur_line_,cont_lines_before_, cont_lines_after_,cont_lines_width_);
         PRINT_SOURCE(CUR_LINE_, CONT_LINES_BEFORE_, CONT_LINES_AFTER_);
      elsif RET = DBMS_DEBUG.ERROR_TIMEOUT then
         DBMS_OUTPUT.PUT_LINE('  continue: error_timeout');
      elsif RET = DBMS_DEBUG.ERROR_COMMUNICATION then
         DBMS_OUTPUT.PUT_LINE('  continue: error_communication');
      else
         V_ERR := GENERAL_ERROR(RET);
         DBMS_OUTPUT.PUT_LINE('  continue: general error' || V_ERR);
      end if;
   end CONTINUE_;


   --****************************************************************************
   --                            DETACH
   --****************************************************************************
   procedure DETACH is

   begin
      DBMS_DEBUG.DETACH_SESSION;
   end DETACH;


   --****************************************************************************
   --                            CONTINUE
   --****************************************************************************
   /*
       continue (calling continue_ with break_flags = 0 ) will run until
       the program hits a breakpoint
   */

   procedure CONTINUE is
   begin
      CONTINUE_(0);
   end CONTINUE;


   --****************************************************************************
   --                            DELETE_BP
   --****************************************************************************
   procedure DELETE_BP(BREAKPOINT in binary_integer) is

      RET binary_integer;

   begin
      RET := DBMS_DEBUG.DELETE_BREAKPOINT(BREAKPOINT);

      if RET = DBMS_DEBUG.SUCCESS then
         DBMS_OUTPUT.PUT_LINE('  breakpoint deleted');
      elsif RET = DBMS_DEBUG.ERROR_NO_SUCH_BREAKPT then
         DBMS_OUTPUT.PUT_LINE('  No such breakpoint exists');
      elsif RET = DBMS_DEBUG.ERROR_IDLE_BREAKPT then
         DBMS_OUTPUT.PUT_LINE('  Cannot delete an unused breakpoint');
      elsif RET = DBMS_DEBUG.ERROR_STALE_BREAKPT then
         DBMS_OUTPUT.PUT_LINE('  The program unit was redefined since the breakpoint was set');
      else
         DBMS_OUTPUT.PUT_LINE('  Unknown error');
      end if;
   end; -- delete_bp


   --****************************************************************************
   --                            PRINT_VAR
   --****************************************************************************
   procedure PRINT_VAR(name in varchar2) is

      RET   binary_integer;
      VAL   varchar2(4000);
      FRAME number;

   begin
      FRAME := 0;
      RET   := DBMS_DEBUG.GET_VALUE(name, FRAME, VAL, null);

      if RET = DBMS_DEBUG.SUCCESS then
         DBMS_OUTPUT.PUT_LINE('  ' || name || ' = ' || VAL);
      elsif RET = DBMS_DEBUG.ERROR_BOGUS_FRAME then
         DBMS_OUTPUT.PUT_LINE('  print_var: frame does not exist');
      elsif RET = DBMS_DEBUG.ERROR_NO_DEBUG_INFO then
         DBMS_OUTPUT.PUT_LINE('  print_var: Entrypoint has no debug info');
      elsif RET = DBMS_DEBUG.ERROR_NO_SUCH_OBJECT then
         DBMS_OUTPUT.PUT_LINE('  print_var: variable ' || name || ' does not exist in in frame ' ||
                              FRAME);
      elsif RET = DBMS_DEBUG.ERROR_UNKNOWN_TYPE then
         DBMS_OUTPUT.PUT_LINE('  print_var: The type information in the debug information is illegible');
      elsif RET = DBMS_DEBUG.ERROR_NULLVALUE then
         DBMS_OUTPUT.PUT_LINE('  ' || name || ' = NULL');
      elsif RET = DBMS_DEBUG.ERROR_INDEXED_TABLE then
         DBMS_OUTPUT.PUT_LINE('  print_var: The object is a table, but no index was provided.');
      else
         DBMS_OUTPUT.PUT_LINE('  print_var: unknown error');
      end if;

   end PRINT_VAR;


   --****************************************************************************
   --                            GET_VAR
   --****************************************************************************
   function GET_VAR(name  in varchar2,
                    A_VAL out varchar2) return number is

      RET   binary_integer;
      VAL   varchar2(4000);
      FRAME number;

   begin
      FRAME := 0;
      RET   := DBMS_DEBUG.GET_VALUE(name, FRAME, VAL, null);

      if RET = DBMS_DEBUG.SUCCESS then
         A_VAL := VAL;
         return 0;
      elsif RET = DBMS_DEBUG.ERROR_BOGUS_FRAME then
         A_VAL := 'print_var: frame does not exist';
      elsif RET = DBMS_DEBUG.ERROR_NO_DEBUG_INFO then
         A_VAL := 'print_var: Entrypoint has no debug info';
      elsif RET = DBMS_DEBUG.ERROR_NO_SUCH_OBJECT then
         A_VAL := 'print_var: variable ' || name || ' does not exist in in frame ' || FRAME;
      elsif RET = DBMS_DEBUG.ERROR_UNKNOWN_TYPE then
         A_VAL := 'print_var: The type information in the debug information is illegible';
      elsif RET = DBMS_DEBUG.ERROR_NULLVALUE then
         A_VAL := 'NULL';
      elsif RET = DBMS_DEBUG.ERROR_INDEXED_TABLE then
         A_VAL := 'print_var: The object is a table, but no index was provided.';
      else
         A_VAL := 'print_var: unknown error';
      end if;
      return - 1;
   end GET_VAR;


   --****************************************************************************
   --                            START_DEBUGGER
   --****************************************************************************
   /*
      This is the first call the debugging session must make. It, in turn, calls
      dbms_debug.attach_session.

      After attaching to the session, it waits for the first event (wait_until_running), which is interpreter starting.
   */
   procedure START_DEBUGGER(DEBUG_SESSION_ID in varchar2) is

   begin
      DBMS_DEBUG.ATTACH_SESSION(DEBUG_SESSION_ID);
      CONT_LINES_BEFORE_ := 5;
      CONT_LINES_AFTER_  := 100;
      --cont_lines_width_  := 100;

      WAIT_UNTIL_RUNNING;
   end START_DEBUGGER;


   --****************************************************************************
   --                            START_DEBUGEE
   --****************************************************************************
   /* This is the first call the debugged session must make.

      The return value must be passed to the debugging session and used in start_debugger
   */
   function START_DEBUGEE return varchar2 as

      DEBUG_SESSION_ID varchar2(20);

   begin
      --select dbms_debug.initialize into debug_session_id from dual;
      DEBUG_SESSION_ID := DBMS_DEBUG.INITIALIZE;
      DBMS_DEBUG.DEBUG_ON;
      return DEBUG_SESSION_ID;
   end START_DEBUGEE;

   --****************************************************************************
   --                            DEBUG_PROCEDURE_ON
   --****************************************************************************
   procedure DEBUG_PROCEDURE_ON(name in varchar2) as

      DEBUG_SESSION_ID varchar2(40);
      enable           varchar2(10);
      SQL1             varchar2(100);

   begin
      begin
         select LOWER(enable)
           into enable
           from PP_PLSQL_DEBUG_ENABLE
          where procedure = LOWER(name)
            and USERS = LOWER(SYS_CONTEXT('USERENV', 'SESSION_USER'));
      exception
         when others then
            return;
      end;

      if enable = 'yes' then
         SQL1 := 'alter session set plsql_debug = true';
         execute immediate SQL1;
         DEBUG_SESSION_ID := DBMS_DEBUG.INITIALIZE;
         DBMS_APPLICATION_INFO.SET_CLIENT_INFO(DEBUG_SESSION_ID);
         DBMS_DEBUG.DEBUG_ON(true, true);
      elsif enable = 'profiler' then
         DBMS_PROFILER.START_PROFILER(sysdate, name);
      end if;
      return;

   end DEBUG_PROCEDURE_ON;


   --****************************************************************************
   --                            REFRESH
   --****************************************************************************
   procedure REFRESH as

   begin
      PRINT_SOURCE(CUR_LINE_, CONT_LINES_BEFORE_, CONT_LINES_AFTER_);
   end;


   --****************************************************************************
   --                            DEBUG_PROCEDURE_OFF
   --****************************************************************************
   procedure DEBUG_PROCEDURE_OFF(name in varchar2) as

      enable varchar2(10);
      SQL1   varchar2(100);

   begin
      begin
         select LOWER(enable)
           into enable
           from PP_PLSQL_DEBUG_ENABLE
          where procedure = LOWER(name)
            and USERS = LOWER(SYS_CONTEXT('USERENV', 'SESSION_USER'));
      exception
         when others then
            return;
      end;

      if enable = 'yes' then
         DBMS_DEBUG.DEBUG_OFF;
         SQL1 := 'alter session set plsql_debug = false';
         execute immediate SQL1;
         DBMS_APPLICATION_INFO.SET_CLIENT_INFO('');
      elsif enable = 'profiler' then
         DBMS_PROFILER.STOP_PROFILER;
      end if;
      return;
   end DEBUG_PROCEDURE_OFF;


   --****************************************************************************
   --                            QUIT_DB
   --****************************************************************************
   procedure QUIT_DB as

      COLL SYS.DBMS_DEBUG_VC2COLL; -- results (unused)
      ERRM varchar2(100);

   begin
      DBMS_DEBUG.EXECUTE('dbms_debug.debug_off', -1, 0, COLL, ERRM);
      DBMS_OUTPUT.PUT_LINE(ERRM);
   end QUIT_DB;


   --****************************************************************************
   --                            EXECUTE_SQL
   --****************************************************************************
   procedure EXECUTE_SQL(SQLS varchar2) as

      COLL SYS.DBMS_DEBUG_VC2COLL; -- results (unused)
      ERRM varchar2(2000);
      VAL  binary_integer;
      I    number(22, 0);

   begin
      COLL := SYS.DBMS_DEBUG_VC2COLL();
      DBMS_DEBUG.EXECUTE(SQLS, -1, 1, COLL, ERRM);
      DBMS_OUTPUT.PUT_LINE(ERRM);
      VAL := COLL.FIRST;
      while (VAL is not null)
      loop
         DBMS_OUTPUT.PUT_LINE(COLL(VAL));
         VAL := COLL.NEXT(VAL);
      end loop;
   end EXECUTE_SQL;


   --****************************************************************************
   --                            EXECUTE_SQL2
   --****************************************************************************
   procedure EXECUTE_SQL2(SQLS varchar2) as

      COLL SYS.DBMS_DEBUG_VC2COLL; -- results (unused)
      ERRM varchar2(100);

   begin
      DBMS_DEBUG.EXECUTE(SQLS, -1, 0, COLL, ERRM);
      DBMS_OUTPUT.PUT_LINE(ERRM);
   end EXECUTE_SQL2;


   --****************************************************************************
   --                            PRINT_PROGINFO
   --****************************************************************************
   procedure PRINT_PROGINFO(PRGINFO DBMS_DEBUG.PROGRAM_INFO) as

   begin
      DBMS_OUTPUT.PUT_LINE('  Namespace:  ' || STR_FOR_NAMESPACE(PRGINFO.NAMESPACE));
      DBMS_OUTPUT.PUT_LINE('  Name:       ' || PRGINFO.NAME);
      DBMS_OUTPUT.PUT_LINE('  owner:      ' || PRGINFO.OWNER);
      DBMS_OUTPUT.PUT_LINE('  dblink:     ' || PRGINFO.DBLINK);
      DBMS_OUTPUT.PUT_LINE('  Line#:      ' || PRGINFO.LINE#);
      DBMS_OUTPUT.PUT_LINE('  lib unit:   ' || PRGINFO.LIBUNITTYPE);
      DBMS_OUTPUT.PUT_LINE('  entrypoint: ' || PRGINFO.ENTRYPOINTNAME);
   end PRINT_PROGINFO;


   --****************************************************************************
   --                            PRINT_RUNTIME_INFO
   --****************************************************************************
   procedure PRINT_RUNTIME_INFO(RUNINFO DBMS_DEBUG.RUNTIME_INFO) as

      --rsnt varchar2(40);

   begin
      --rsnt := str_for_reason_in_runtime_info(runinfo.reason);
      DBMS_OUTPUT.PUT_LINE('');
      DBMS_OUTPUT.PUT_LINE('Runtime Info');
      DBMS_OUTPUT.PUT_LINE('Prg Name:      ' || RUNINFO.PROGRAM.NAME);
      DBMS_OUTPUT.PUT_LINE('Line:          ' || RUNINFO.LINE#);
      DBMS_OUTPUT.PUT_LINE('Terminated:    ' || RUNINFO.TERMINATED);
      DBMS_OUTPUT.PUT_LINE('Breakpoint:    ' || RUNINFO.BREAKPOINT);
      DBMS_OUTPUT.PUT_LINE('Stackdepth     ' || RUNINFO.STACKDEPTH);
      DBMS_OUTPUT.PUT_LINE('Interpr depth: ' || RUNINFO.INTERPRETERDEPTH);
      --dbms_output.put_line('Reason         ' || rsnt);
      DBMS_OUTPUT.PUT_LINE('Reason:        ' || STR_FOR_REASON_IN_RUNTIME_INFO(RUNINFO.REASON));

      PRINT_PROGINFO(RUNINFO.PROGRAM);
   end PRINT_RUNTIME_INFO;


   --****************************************************************************
   --                            PRINT_SOURCE
   --****************************************************************************
   procedure PRINT_SOURCE(RUNINFO      DBMS_DEBUG.RUNTIME_INFO,
                          LINES_BEFORE number default 0,
                          LINES_AFTER  number default 0) is

      FIRST_LINE binary_integer;
      LAST_LINE  binary_integer;

      PREFIX varchar2(99);
      SUFFIX varchar2(4000);

      --source_lines                 vc2_table;
      SOURCE_LINES DBMS_DEBUG.VC2_TABLE;

      CUR_LINE      binary_integer;
      CUR_REAL_LINE number;

   begin

      FIRST_LINE := GREATEST(RUNINFO.LINE# - CONT_LINES_BEFORE_, 1);
      LAST_LINE  := RUNINFO.LINE# + CONT_LINES_AFTER_;

      if FIRST_LINE is null or LAST_LINE is null then
         DBMS_OUTPUT.PUT_LINE('first_line or last_line is null');
         PRINT_RUNTIME_INFO(RUNINFO);
         return;
      end if;

      if RUNINFO.PROGRAM.NAME is not null and RUNINFO.PROGRAM.OWNER is not null then

         DBMS_OUTPUT.PUT_LINE('');
         DBMS_OUTPUT.PUT_LINE('  ' || RUNINFO.PROGRAM.OWNER || '.' || RUNINFO.PROGRAM.NAME);

         --select
         --   cast(multiset(
         for R in (select
                   -- 90 is the length in dbms_debug.vc2_table....
                    ROWNUM LINE, SUBSTR(TEXT, 1, 90) TEXT
                     from ALL_SOURCE
                    where name = RUNINFO.PROGRAM.NAME
                      and OWNER = RUNINFO.PROGRAM.OWNER
                      and type <> 'PACKAGE'
                      and LINE >= FIRST_LINE
                      and LINE <= LAST_LINE
                    order by LINE) -- as vc2_table)
         loop
            --        into
            --          source_lines
            --        from
            --          dual;
            SOURCE_LINES(R.LINE) := R.TEXT;

         end loop;

      else

         DBMS_DEBUG.SHOW_SOURCE(FIRST_LINE, LAST_LINE, SOURCE_LINES);

         --      select
         --        cast(
         --          multiset(
         --            select culumn_value from
         --              table(
         --                cast(source_lines_dbms as dbms_debug.vc2_table)
         --              )
         --        )as vc2_table)
         --      into
         --        source_lines
         --      from
         --        dual;

      end if;

      DBMS_OUTPUT.PUT_LINE('');
      CUR_LINE := SOURCE_LINES.FIRST();
      while CUR_LINE is not null
      loop
         CUR_REAL_LINE := CUR_LINE + FIRST_LINE - 1;

         -- for r in (select column_value text from table(source_lines)) loop
         PREFIX := TO_CHAR(CUR_REAL_LINE, '9999');

         if CUR_REAL_LINE = RUNINFO.LINE# then
            PREFIX := PREFIX || ' -> ';
         else
            PREFIX := PREFIX || '    ';
         end if;

         -- TODO, most probably superfluos, 90 is the max width.... (ts, ts)
         --if length(r.text) > v_lines_width then
         --  suffix := substr(r.text,1,v_lines_width);
         --else
         --  suffix := r.text;
         --end if;

         SUFFIX := SOURCE_LINES(CUR_LINE);
         SUFFIX := TRANSLATE(SUFFIX, CHR(10), ' ');
         SUFFIX := TRANSLATE(SUFFIX, CHR(13), ' ');

         --dbms_output.put_line(prefix || suffix);
         DBMS_OUTPUT.PUT_LINE(PREFIX || SUFFIX);

         --    line_printed := 'Y';

         CUR_LINE := SOURCE_LINES.NEXT(CUR_LINE);
         --cur_line := cur_line + 1;
      end loop;

      DBMS_OUTPUT.PUT_LINE('');

   end PRINT_SOURCE;


   --****************************************************************************
   --                        PRINT_RUNTIME_INFO_WITH_SOURCE
   --****************************************************************************
   procedure PRINT_RUNTIME_INFO_WITH_SOURCE(RUNINFO DBMS_DEBUG.RUNTIME_INFO) is

   begin

      PRINT_RUNTIME_INFO(RUNINFO);

      --dbms_output.put_line('line#: ' || runinfo.line#);
      --dbms_output.put_line(' -   : ' || (runinfo.line# - cont_lines_before_));

      --dbms_output.put_line('first_line: ' || first_line);
      --dbms_output.put_line('last_line:  ' || last_line);

      PRINT_SOURCE(RUNINFO);

   end PRINT_RUNTIME_INFO_WITH_SOURCE;


   --****************************************************************************
   --                            SELF_CHECK
   --****************************************************************************
   procedure SELF_CHECK as

      RET binary_integer;

   begin
      DBMS_DEBUG.SELF_CHECK(5);
   exception
      when DBMS_DEBUG.PIPE_CREATION_FAILURE then
         DBMS_OUTPUT.PUT_LINE('  self_check: pipe_creation_failure');
      when DBMS_DEBUG.PIPE_SEND_FAILURE then
         DBMS_OUTPUT.PUT_LINE('  self_check: pipe_send_failure');
      when DBMS_DEBUG.PIPE_RECEIVE_FAILURE then
         DBMS_OUTPUT.PUT_LINE('  self_check: pipe_receive_failure');
      when DBMS_DEBUG.PIPE_DATATYPE_MISMATCH then
         DBMS_OUTPUT.PUT_LINE('  self_check: pipe_datatype_mismatch');
      when DBMS_DEBUG.PIPE_DATA_ERROR then
         DBMS_OUTPUT.PUT_LINE('  self_check: pipe_data_error');
      when others then
         DBMS_OUTPUT.PUT_LINE('  self_check: unknown error');
   end SELF_CHECK;


   --****************************************************************************
   --                            SET_BREAKPOINT
   --
   -- Out of the four parameters: P_CURSOR, P_TOPLEVEL, P_BODY, P_TRIGGER
   -- at most one should be set to zero. They set the proginfo.namespace
   --
   --****************************************************************************
   procedure SET_BREAKPOINT(P_LINE     in number,
                            P_NAME     in varchar2 default null,
                            P_OWNER    in varchar2 default null,
                            P_CURSOR   in boolean default false,
                            P_TOPLEVEL in boolean default false,
                            P_BODY     in boolean default false,
                            P_TRIGGER  in boolean default false) as

      PROGINFO DBMS_DEBUG.PROGRAM_INFO;
      RET      binary_integer;
      BP       binary_integer;

   begin

      if P_CURSOR then
         PROGINFO.NAMESPACE := DBMS_DEBUG.NAMESPACE_CURSOR;
      elsif P_TOPLEVEL then
         PROGINFO.NAMESPACE := DBMS_DEBUG.NAMESPACE_PKGSPEC_OR_TOPLEVEL;
      elsif P_BODY then
         PROGINFO.NAMESPACE := DBMS_DEBUG.NAMESPACE_PKG_BODY;
      elsif P_TRIGGER then
         PROGINFO.NAMESPACE := DBMS_DEBUG.NAMESPACE_TRIGGER;
      else
         PROGINFO.NAMESPACE := null;
      end if;

      PROGINFO.NAME           := P_NAME;
      PROGINFO.OWNER          := P_OWNER;
      PROGINFO.DBLINK         := null;
      PROGINFO.ENTRYPOINTNAME := null;

      RET := DBMS_DEBUG.SET_BREAKPOINT(PROGINFO, P_LINE, BP);

      if RET = DBMS_DEBUG.SUCCESS then
         DBMS_OUTPUT.PUT_LINE('  breakpoint set: ' || BP);
      elsif RET = DBMS_DEBUG.ERROR_ILLEGAL_LINE then
         DBMS_OUTPUT.PUT_LINE('  set_breakpoint: error_illegal_line');
      elsif RET = DBMS_DEBUG.ERROR_BAD_HANDLE then
         DBMS_OUTPUT.PUT_LINE('  set_breakpoint: error_bad_handle');
      else
         DBMS_OUTPUT.PUT_LINE('  set_breakpoint: unknown error (' || RET || ')');
      end if;

   end SET_BREAKPOINT;


   --****************************************************************************
   --                            STEP
   --****************************************************************************
   procedure STEP is

   begin
      CONTINUE_(DBMS_DEBUG.BREAK_NEXT_LINE);
   end STEP;


   --****************************************************************************
   --                            STEP_INTO
   --****************************************************************************
   procedure STEP_INTO is

   begin
      CONTINUE_(DBMS_DEBUG.BREAK_ANY_CALL);
   end STEP_INTO;


   --****************************************************************************
   --                            STEP_OUT
   --****************************************************************************
   procedure STEP_OUT is

   begin
      CONTINUE_(DBMS_DEBUG.BREAK_ANY_RETURN);
   end STEP_OUT;


   --****************************************************************************
   --                            STR_FOR_NAMESPACE
   --****************************************************************************
   function STR_FOR_NAMESPACE(NSP in binary_integer) return varchar2 is

      NSPS varchar2(40);

   begin
      if NSP = DBMS_DEBUG.NAMESPACE_CURSOR then
         NSPS := 'Cursor (anonymous block)';
      elsif NSP = DBMS_DEBUG.NAMESPACE_PKGSPEC_OR_TOPLEVEL then
         NSPS := 'package, proc, func or obj type';
      elsif NSP = DBMS_DEBUG.NAMESPACE_PKG_BODY then
         NSPS := 'package body or type body';
      elsif NSP = DBMS_DEBUG.NAMESPACE_TRIGGER then
         NSPS := 'Triggers';
      else
         NSPS := 'Unknown namespace';
      end if;

      return NSPS;
   end STR_FOR_NAMESPACE;


   --****************************************************************************
   --                            STR_FOR_REASON_IN_RUNTIME_INFO
   --****************************************************************************
   function STR_FOR_REASON_IN_RUNTIME_INFO(RSN in binary_integer) return varchar2 is

      RSNT varchar2(40);

   begin
      if RSN = DBMS_DEBUG.REASON_NONE then
         RSNT := 'none';
      elsif RSN = DBMS_DEBUG.REASON_INTERPRETER_STARTING then
         RSNT := 'Interpreter is starting.';
      elsif RSN = DBMS_DEBUG.REASON_BREAKPOINT then
         RSNT := 'Hit a breakpoint';
      elsif RSN = DBMS_DEBUG.REASON_ENTER then
         RSNT := 'Procedure entry';
      elsif RSN = DBMS_DEBUG.REASON_RETURN then
         RSNT := 'Procedure is about to return';
      elsif RSN = DBMS_DEBUG.REASON_FINISH then
         RSNT := 'Procedure is finished';
      elsif RSN = DBMS_DEBUG.REASON_LINE then
         RSNT := 'Reached a new line';
      elsif RSN = DBMS_DEBUG.REASON_INTERRUPT then
         RSNT := 'An interrupt occurred';
      elsif RSN = DBMS_DEBUG.REASON_EXCEPTION then
         RSNT := 'An exception was raised';
      elsif RSN = DBMS_DEBUG.REASON_EXIT then
         RSNT := 'Interpreter is exiting (old form)';
      elsif RSN = DBMS_DEBUG.REASON_KNL_EXIT then
         RSNT := 'Kernel is exiting';
      elsif RSN = DBMS_DEBUG.REASON_HANDLER then
         RSNT := 'Start exception-handler';
      elsif RSN = DBMS_DEBUG.REASON_TIMEOUT then
         RSNT := 'A timeout occurred';
      elsif RSN = DBMS_DEBUG.REASON_INSTANTIATE then
         RSNT := 'Instantiation block';
      elsif RSN = DBMS_DEBUG.REASON_ABORT then
         RSNT := 'Interpreter is aborting';
      else
         RSNT := 'Unknown reason';
      end if;
      return RSNT;
   end STR_FOR_REASON_IN_RUNTIME_INFO;


   --****************************************************************************
   --                            WAIT_UNTIL_RUNNING
   --****************************************************************************
   procedure WAIT_UNTIL_RUNNING as

      RUNINFO DBMS_DEBUG.RUNTIME_INFO;
      RET     binary_integer;
      V_ERR   varchar2(100);

   begin
      RET := DBMS_DEBUG.SYNCHRONIZE(RUNINFO,
                                    0 /*+
                                          dbms_debug.info_getstackdepth +
                                          dbms_debug.info_getbreakpoint +
                                          dbms_debug.info_getlineinfo   +
                                          dbms_debug.info_getoerinfo    +
                                          0 */);

      if RET = DBMS_DEBUG.SUCCESS then
         PRINT_RUNTIME_INFO(RUNINFO);
      elsif RET = DBMS_DEBUG.ERROR_TIMEOUT then
         DBMS_OUTPUT.PUT_LINE('  synchronize: error_timeout');
      elsif RET = DBMS_DEBUG.ERROR_COMMUNICATION then
         DBMS_OUTPUT.PUT_LINE('  synchronize: error_communication');
      else
         V_ERR := GENERAL_ERROR(RET);
         DBMS_OUTPUT.PUT_LINE('  synchronize: general error' || V_ERR);
         --dbms_output.put_line('  synchronize: unknown error');
      end if;

   end WAIT_UNTIL_RUNNING;


   --****************************************************************************
   --                            IS_RUNNING
   --****************************************************************************
   procedure IS_RUNNING is

   begin
      if DBMS_DEBUG.TARGET_PROGRAM_RUNNING then
         DBMS_OUTPUT.PUT_LINE('  target (debugee) is running');
      else
         DBMS_OUTPUT.PUT_LINE('  target (debugee) is not running');
      end if;
   end IS_RUNNING;


   --****************************************************************************
   --                            GENERAL_ERROR
   --****************************************************************************
   function GENERAL_ERROR(E in binary_integer) return varchar2 is

   begin

      if E = DBMS_DEBUG.ERROR_UNIMPLEMENTED then
         return 'unimplemented';
      end if;
      if E = DBMS_DEBUG.ERROR_DEFERRED then
         return 'deferred';
      end if;
      if E = DBMS_DEBUG.ERROR_EXCEPTION then
         return 'probe exception';
      end if;
      if E = DBMS_DEBUG.ERROR_COMMUNICATION then
         return 'communication error';
      end if;
      if E = DBMS_DEBUG.ERROR_UNIMPLEMENTED then
         return 'unimplemented';
      end if;
      if E = DBMS_DEBUG.ERROR_TIMEOUT then
         return 'timeout';
      end if;

      return '???';

   end GENERAL_ERROR;


   --****************************************************************************
   --                            VERSION
   --****************************************************************************
   procedure VERSION as

      MAJOR binary_integer;
      MINOR binary_integer;

   begin
      DBMS_DEBUG.PROBE_VERSION(MAJOR, MINOR);
      DBMS_OUTPUT.PUT_LINE('  probe version is: ' || MAJOR || '.' || MINOR);
   end VERSION;


   --****************************************************************************
   --                            CURRENT_PRG
   --****************************************************************************
   procedure CURRENT_PRG is

      RI  DBMS_DEBUG.RUNTIME_INFO;
      PI  DBMS_DEBUG.PROGRAM_INFO;
      RET binary_integer;

   begin

      RET := DBMS_DEBUG.GET_RUNTIME_INFO(0 + DBMS_DEBUG.INFO_GETLINEINFO +
                                         DBMS_DEBUG.INFO_GETBREAKPOINT +
                                         DBMS_DEBUG.INFO_GETSTACKDEPTH + DBMS_DEBUG.INFO_GETOERINFO + 0,
                                         RI);

      PI := RI.PROGRAM;

      PRINT_PROGINFO(PI);
   end CURRENT_PRG;


--****************************************************************************
--                            INITIALIZE PACKAGE
--****************************************************************************
begin

   CONT_LINES_BEFORE_ := 5;
   CONT_LINES_AFTER_  := 5;

end PP_PLSQL_DEBUG;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (16306, 0, 2018, 2, 0, 0, 0, 'c:\plasticwks\powerplant\sql\packages', 
    'PP_PLSQL_DEBUG.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
