/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   PKG_TAX_PLANT_RECON.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Revised By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2016.1.0.0 03/10/2016 Charlie Shilling     Create package
|| 2016.1.0.0 03/17/2016 David Haupt          Add F_CALC_FORM_DATA
|| 2016.1.0.0 04/07/2016 Anand R              PP-45625
|| 2016.1.0.0 06/16/2016 Anand R              PP-45370
|| 2016.1.0.0 07/07/2016 Jared Watkins	      Change text values per product decisions
|| 2016.1.0.0 10/5/2016  Anand R              Add prev statutory and grossup rates
|| 2016.1.0.0 11/17/2016 Anand R              PP-46789 - resolve issue with form rates
|| 2017.1.0.0 03/21/2017 Sarah Byers          PP-47373 - performance enhancements
||========================================================================================
*/
CREATE OR REPLACE PACKAGE pkg_tax_plant_recon AS
  G_PKG_VERSION varchar(35) := '2018.2.0.0';
	--public procedures
	FUNCTION f_refresh_form_data(a_job_no NUMBER) RETURN NUMBER;
  FUNCTION f_calc_form_data(a_job_no NUMBER) RETURN NUMBER;

END pkg_tax_plant_recon;
/
CREATE OR REPLACE PACKAGE BODY pkg_tax_plant_recon AS

	--private global variables
	g_start_time 		TIMESTAMP;
	g_version			NUMBER(22,0);
	g_job_no			NUMBER(22,0);
	g_line_no			NUMBER(22,0);

	--private function/procedure declarations
  FUNCTION f_get_version RETURN VARCHAR2;
  FUNCTION f_load_form_rates(a_tax_pr_form_id NUMBER) RETURN NUMBER;
  FUNCTION f_load_asset_activity(a_tax_pr_form_id NUMBER) RETURN NUMBER;
  FUNCTION f_load_tax_asset_activity(a_tax_pr_form_id NUMBER) RETURN NUMBER;
  FUNCTION f_load_depr_reserve_activity(a_tax_pr_form_id NUMBER) RETURN NUMBER;
  FUNCTION f_load_tax_reserve_activity(a_tax_pr_form_id NUMBER) RETURN NUMBER;
  FUNCTION f_load_tax_reserve_summary(a_tax_pr_form_id NUMBER) RETURN NUMBER;
  FUNCTION f_load_tax_book_basis_summary(a_tax_pr_form_id NUMBER) RETURN NUMBER;
  FUNCTION f_load_tax_temp_diff(a_tax_pr_form_id NUMBER) RETURN NUMBER;
  FUNCTION f_load_input_gl_data(a_tax_pr_form_id NUMBER) RETURN NUMBER;
  FUNCTION f_load_input_adj_data(a_tax_pr_form_id NUMBER) RETURN NUMBER;
  FUNCTION f_calc_subtotals(a_tax_pr_form_id NUMBER) RETURN NUMBER;
  PROCEDURE write_log(a_job_no NUMBER, a_error_type NUMBER, a_code NUMBER, a_msg VARCHAR2);

	--=============================================================================
	-- Function F_REFRESH_FORM_DATA
	--
	-- Description: Main function that calls the other functions to update form data
	--=============================================================================
	FUNCTION f_refresh_form_data(a_job_no NUMBER)
		 RETURN NUMBER
	IS
		rtn_code 				NUMBER;
		l_tax_pr_form_id		NUMBER(22,0);
		l_refresh_plant			NUMBER;
		l_refresh_depr			NUMBER;
		l_refresh_tax			NUMBER;
		lb_load_asset_data 		BOOLEAN;
		lb_load_depr_data 		BOOLEAN;
		lb_load_powertax_data	BOOLEAN;
    
    ANALYZE_CODE pls_integer;

		--=============================================================================
		-- Procedure P_UDPATE_REFRESH_DATE
		--
		-- Description: Updates the refresh date for a particular tax_pr_form_id and form_source_id
		--=============================================================================
		PROCEDURE p_update_refresh_date(a_tax_pr_form_id NUMBER, a_form_source_id NUMBER)
		IS
		BEGIN
			MERGE INTO tax_plant_recon_form_refresh a
			USING (
				SELECT a_tax_pr_form_id AS tax_pr_form_id, a_form_source_id AS form_source_id, SYSDATE AS refresh_date
				FROM dual
			) b
			ON (a.tax_pr_form_id = b.tax_pr_form_id
				AND a.form_source_id = b.form_source_id)
			WHEN MATCHED THEN
		 		UPDATE
				SET a.refresh_date = b.refresh_date
			WHEN NOT MATCHED THEN
				INSERT (tax_pr_form_id, form_source_id, refresh_date)
				VALUES (b.tax_pr_form_id, b.form_source_id, b.refresh_date);
		END p_update_refresh_date;

		FUNCTION f_bool_to_char(a_boolean BOOLEAN) RETURN VARCHAR2
		IS
		BEGIN
			IF a_boolean = FALSE THEN
				RETURN 'FALSE';
			ELSIF a_boolean = TRUE THEN
				RETURN 'TRUE';
			ELSIF a_boolean = NULL THEN
				RETURN 'NULL';
			ELSE
				Raise_Application_Error(-20005, 'Invalid boolean value passed to f_bool_to_char.');
			END IF;
		END f_bool_to_char;
	BEGIN
		--start logging
		g_start_time := current_timestamp;
		g_job_no  := a_job_no;
		g_line_no := 1;
		dbms_output.enable(100000);
		write_log(g_job_no, 0, 0, 'Plant Reconciliation Refresh Data  Started Version=' || f_get_version());
		--set_session_parameter();

		rtn_code := AUDIT_TABLE_PKG.setcontext('audit','no');

		--get job parameters from table
		write_log(g_job_no, 0, 0, 'Getting job parameters for job_no ' || a_job_no || '.');
		BEGIN
			SELECT tax_pr_form_id, refresh_plant, refresh_depr, refresh_tax, tax_version_id
			INTO l_tax_pr_form_id, l_refresh_plant, l_refresh_depr, l_refresh_tax, g_version
			FROM tax_job_pr_form_params
			WHERE job_no = a_job_no;
		EXCEPTION
			WHEN No_Data_Found THEN
				Raise_Application_Error(-20000, 'No data found in TAX_JOB_PR_FORM_PARAMS for job_no ' || a_job_no || '.');
			WHEN Too_Many_Rows THEN
				Raise_Application_Error(-20001, 'Multiple rows found in TAX_JOB_PR_FORM_PARAMS for job_no ' || a_job_no || '. Check that the primary key is enabled.');
		END;

		--validate inputs
		IF l_tax_pr_form_id = NULL THEN
			Raise_Application_Error(-20005, 'The tax_pr_form_id on tax_job_pr_form_parmas is NULL. Cannot continue processing. Job Number: ' || a_job_no);
		END IF;
		IF g_version = NULL THEN
			Raise_Application_Error(-20006, 'The tax_version on tax_job_pr_form_parmas is NULL. Cannot continue processing. Job Number: ' || a_job_no);
		END IF;
		IF l_refresh_plant = 1 THEN
			lb_load_asset_data := TRUE;
		ELSIF l_refresh_plant = 0 THEN
			lb_load_asset_data := FALSE;
		ELSE
			Raise_Application_Error(-20002, 'Invalid value found in TAX_JOB_PR_FORM_PARAMS for REFRESH_PLANT: ' || l_refresh_plant || '. Valid valuses are 0 for FALSE or 1 for TRUE.');
		END IF;
		IF l_refresh_depr = 1 THEN
			lb_load_depr_data := TRUE;
		ELSIF l_refresh_depr = 0 THEN
			lb_load_depr_data := FALSE;
		ELSE
			Raise_Application_Error(-20003, 'Invalid value found in TAX_JOB_PR_FORM_PARAMS for REFRESH_DEPR: ' || l_refresh_depr || '. Valid valuses are 0 for FALSE or 1 for TRUE.');
		END IF;
		IF l_refresh_tax = 1 THEN
			lb_load_powertax_data := TRUE;
		ELSIF l_refresh_tax = 0 THEN
			lb_load_powertax_data := FALSE;
		ELSE
			Raise_Application_Error(-20004, 'Invalid value found in TAX_JOB_PR_FORM_PARAMS for REFRESH_TAX: ' || l_refresh_tax || '. Valid valuses are 0 for FALSE or 1 for TRUE.');
		END IF;

		write_log(g_job_no, 0, 0, '	tax_pr_form_id: ' || l_tax_pr_form_id);
		write_log(g_job_no, 0, 0, '	tax_version_id: ' || g_version);
		write_log(g_job_no, 0, 0, '	load_asset_data: ' || f_bool_to_char(lb_load_asset_data));
		write_log(g_job_no, 0, 0, '	load_depr_data: ' || f_bool_to_char(lb_load_depr_data));
		write_log(g_job_no, 0, 0, '	load_powertax_data: ' || f_bool_to_char(lb_load_powertax_data));

		--call functions to do real work.
		IF lb_load_asset_data THEN
			write_log(g_job_no, 0, 0, 'Refreshing plant asset data.');
			IF f_load_asset_activity(l_tax_pr_form_id) = -1 THEN
				ROLLBACK;
				RETURN -1;
			END IF;

			p_update_refresh_date(l_tax_pr_form_id, 1);
		END IF;

		IF lb_load_depr_data THEN
			write_log(g_job_no, 0, 0, 'Refreshing plant depreciation data.');
			IF f_load_depr_reserve_activity(l_tax_pr_form_id) = -1 THEN
				ROLLBACK;
				RETURN -1;
			END IF;

			p_update_refresh_date(l_tax_pr_form_id, 2);
		END IF;

		IF lb_load_powertax_data THEN
			write_log(g_job_no, 0, 0, 'Refreshing PowerTax data.');
			IF f_load_form_rates(l_tax_pr_form_id) = -1 THEN
				ROLLBACK;
				RETURN -1;
			END IF;
			IF f_load_tax_asset_activity(l_tax_pr_form_id) = -1 THEN
				ROLLBACK;
				RETURN -1;
			END IF;
			IF f_load_tax_reserve_activity(l_tax_pr_form_id) = -1 THEN
				ROLLBACK;
				RETURN -1;
			END IF;
			IF f_load_tax_reserve_summary(l_tax_pr_form_id) = -1 THEN
				ROLLBACK;
				RETURN -1;
			END IF;
			IF f_load_tax_book_basis_summary(l_tax_pr_form_id) = -1 THEN
				ROLLBACK;
				RETURN -1;
			END IF;
			IF f_load_tax_temp_diff(l_tax_pr_form_id) = -1 THEN
				ROLLBACK;
				RETURN -1;
			END IF;

			p_update_refresh_date(l_tax_pr_form_id, 3);
		END IF;

		write_log(g_job_no, 0, 0, 'Refresh complete, beginning calculation.');
		COMMIT;
    
    -- Gather stats for tax_plant_recon_form_% tables
    write_log(g_job_no, 0, 0, 'Gathering Table Stats.');
    ANALYZE_CODE := ANALYZE_TABLE('TAX_PLANT_RECON_FORM_SUMM_ID');
    ANALYZE_CODE := ANALYZE_TABLE('TAX_PLANT_RECON_FORM_RES_SUMM');
    ANALYZE_CODE := ANALYZE_TABLE('TAX_PLANT_RECON_FORM_SUMMARY');
    ANALYZE_CODE := ANALYZE_TABLE('TAX_PLANT_RECON_FORM_RATES');
    ANALYZE_CODE := ANALYZE_TABLE('TAX_PLANT_RECON_FORM_GL_ACCT');
    ANALYZE_CODE := ANALYZE_TABLE('TAX_PLANT_RECON_FORM_ADJ');
    ANALYZE_CODE := ANALYZE_TABLE('TAX_PLANT_RECON_FORM_STATUS');
    ANALYZE_CODE := ANALYZE_TABLE('TAX_PLANT_RECON_FORM_REFRESH');

    --call the calculate sub-functions instead of the main calc function
    write_log(g_job_no, 0, 0, 'Summarizing form: ' || l_tax_pr_form_id || '.');

    --call functions to do the actual work
    if f_load_input_gl_data(l_tax_pr_form_id) = -1 then
      rollback;
      return -1;
    end if;
    if f_load_input_adj_data(l_tax_pr_form_id) = -1 then
      rollback;
      return -1;
    end if;
    if f_calc_subtotals(l_tax_pr_form_id) = -1 then
      rollback;
      return -1;
    end if;

    write_log(g_job_no, 0, 0, 'Calculation complete.');
    COMMIT;
    
    return 0;
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
    		write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_REFRESH_FORM_DATA. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
    		RETURN -1;
	END f_refresh_form_data;

  --=============================================================================
	-- Function F_CALC_FORM_DATA
	--
	-- Description: Main function that calls the other functions to calculate form data
	--=============================================================================
	FUNCTION f_calc_form_data(a_job_no NUMBER)
    RETURN NUMBER
	IS
		rtn_code 				NUMBER;
    l_tax_pr_form_id		NUMBER(22,0);
  BEGIN
    --start logging
		g_start_time := current_timestamp;
		g_job_no  := a_job_no;
		g_line_no := 1;
		dbms_output.enable(100000);
		write_log(g_job_no, 0, 0, 'Plant Reconciliation Calculate Data  Started Version=' || f_get_version());
		--set_session_parameter();

		rtn_code := AUDIT_TABLE_PKG.setcontext('audit','no');

		--get job parameters from table
		write_log(g_job_no, 0, 0, 'Getting job parameters for job_no ' || a_job_no || '.');
		BEGIN
			SELECT tax_pr_form_id
			INTO l_tax_pr_form_id
			FROM tax_job_pr_form_params
			WHERE job_no = a_job_no;
		EXCEPTION
			WHEN No_Data_Found THEN
				Raise_Application_Error(-20000, 'No data found in TAX_JOB_PR_FORM_PARAMS for job_no ' || a_job_no || '.');
			WHEN Too_Many_Rows THEN
				Raise_Application_Error(-20001, 'Multiple rows found in TAX_JOB_PR_FORM_PARAMS for job_no ' || a_job_no || '. Check that the primary key is enabled.');
		END;

		--check that the id is not null
		if l_tax_pr_form_id is null then
		  Raise_Application_Error(-20002, 'Invalid form ID value found in TAX_JOB_PR_FORM_PARAMS for job_no ' || a_job_no || '. Check that the form ID is correct.');
		end if;

    write_log(g_job_no, 0, 0, 'Summarizing form: ' || l_tax_pr_form_id || '.');

    --call functions to do the actual work
    if f_load_input_gl_data(l_tax_pr_form_id) = -1 then
      rollback;
      return -1;
    end if;
    if f_load_input_adj_data(l_tax_pr_form_id) = -1 then
      rollback;
      return -1;
    end if;
    if f_calc_subtotals(l_tax_pr_form_id) = -1 then
      rollback;
      return -1;
    end if;

    write_log(g_job_no, 0, 0, 'Calculation complete.');
    COMMIT;
    return 0;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
        write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_CALC_FORM_DATA. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
        RETURN -1;
  END f_calc_form_data;
  
  --=============================================================================
	-- Function F_GET_VERSION
	--
	-- Description: Returns the current version of the package
	--=============================================================================
	FUNCTION f_get_version
		RETURN VARCHAR2
	IS
	BEGIN
		RETURN '2016.1.2.0';
	END f_get_version;

  --=============================================================================
	-- Function F_LOAD_FORM_RATES
	--
	-- Description: Populates tax_plan_recon_form_rates
	--=============================================================================
	FUNCTION f_load_form_rates(a_tax_pr_form_id NUMBER)
		 RETURN NUMBER
	IS
	BEGIN
    write_log(g_job_no, 0, 0, '-- Load Form Rates');
		MERGE INTO tax_plant_recon_form_rates a
		USING (
    WITH v as (
                SELECT tax_year_version_id, tax_year,
                       To_Date(To_Char(Floor(tax_year))||To_Char(1),'yyyymm') beg_date,
                       To_Date(To_Char(Floor(tax_year))||To_Char(months),'yyyymm') end_date
                  FROM tax_year_version
                 WHERE months <> 0
                ),
          
          tax_record_view as (
              select /*+ materialize */ c.company_id, c.tax_record_id, v.tax_year, v.beg_date, v.end_date
                from tax_record_control c, tax_plant_recon_form f, v
               where c.version_id = g_version
                 and c.company_id = f.company_id
                 and f.tax_pr_form_id = a_tax_pr_form_id
                 and f.tax_year_version_id = v.tax_year_version_id
                 ),
          v1 as (
            SELECT /*+ materialize */ n.jurisdiction_id, 
                   Min(d.def_income_tax_rate_id) def_income_tax_rate_id, Max(d.effective_date) end_date
              FROM deferred_income_tax_rates d, normalization_schema n, deferred_income_tax i,
                   tax_record_view a
             WHERE (d.def_income_tax_rate_id, d.effective_date) IN (
                      SELECT def_income_tax_rate_id, Max(effective_date)
                      FROM deferred_income_tax_rates d2
                      WHERE d2.effective_date <= a.end_date
                      GROUP BY def_income_tax_rate_id)
                AND d.def_income_tax_rate_id = n.def_income_tax_rate_id
                AND a.tax_record_id = i.tax_record_id
                AND n.normalization_id = i.normalization_id
                AND a.tax_year = i.tax_year
                GROUP BY n.jurisdiction_id
             ),
      v2 as  (
            SELECT /*+ materialize */ n.jurisdiction_id, 
                   Min(d.def_income_tax_rate_id) def_income_tax_rate_id, Max(d.effective_date) end_date
              FROM deferred_income_tax_rates d, normalization_schema n, deferred_income_tax i,
                   tax_record_view a
             WHERE (d.def_income_tax_rate_id, d.effective_date) IN (
                      SELECT def_income_tax_rate_id, Max(effective_date)
                      FROM deferred_income_tax_rates d2
                      WHERE d2.effective_date < a.beg_date
                      GROUP BY def_income_tax_rate_id)
                AND d.def_income_tax_rate_id = n.def_income_tax_rate_id
                AND a.tax_record_id = i.tax_record_id
                AND n.normalization_id = i.normalization_id
                AND a.tax_year = i.tax_year
                GROUP BY n.jurisdiction_id
        )
		SELECT /*+ NO_MERGE */ DISTINCT a_tax_pr_form_id tax_pr_form_id,
                v1.jurisdiction_id, v1.def_income_tax_rate_id def_income_tax_rate_id, v1.end_date effective_date,
                v2.def_income_tax_rate_id prev_def_income_tax_rate_id, v2.end_date prev_effective_date
        FROM v1, v2, tax_plant_recon_form_jur j, tax_plant_recon_form f
        where f.tax_pr_form_id = a_tax_pr_form_id
          AND j.tax_pr_form_id = f.tax_pr_form_id
          and v1.jurisdiction_id = j.jurisdiction_id
          and v1.jurisdiction_id = v2.jurisdiction_id (+)
		) b
		ON (a.tax_pr_form_id = b.tax_pr_form_id
			AND a.def_income_tax_rate_id = b.def_income_tax_rate_id)
		WHEN MATCHED THEN
			UPDATE
			SET a.effective_date = b.effective_date,
      a.prev_def_income_tax_rate_id = b.prev_def_income_tax_rate_id,
      a.prev_effective_date = b.prev_effective_date
		WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, jurisdiction_id, def_income_tax_rate_id, effective_date, prev_def_income_tax_rate_id, prev_effective_date )
			VALUES (b.tax_pr_form_id, b.jurisdiction_id, b.def_income_tax_rate_id, b.effective_date, b.prev_def_income_tax_rate_id, b.prev_effective_date);

		RETURN 0;
	EXCEPTION
		WHEN OTHERS THEN
    		write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_LOAD_FORM_RATES. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
    		RETURN -1;
	END f_load_form_rates;

  --=============================================================================
	-- Function F_LOAD_ASSET_ACTIVITY
	--
	-- Description: Populates 
	--=============================================================================
	FUNCTION f_load_asset_activity(a_tax_pr_form_id NUMBER)
		 RETURN NUMBER
	IS
		l_description  			tax_plant_recon_form_summ_id.description%TYPE := 'PowerPlan CPR Asset Actuals';
		l_tax_pr_section_id 	tax_plant_recon_form_summ_id.tax_pr_section_id%TYPE := 3;
		l_recon_source			tax_plant_recon_form_summ_id.recon_source%TYPE := 3;
		l_tax_account_type		tax_plant_recon_form_summ_id.tax_account_type%TYPE := 1;
	BEGIN
    write_log(g_job_no, 0, 0, '-- Load Asset Activity ID');
		MERGE INTO tax_plant_recon_form_summ_id a
		USING (
			SELECT a_tax_pr_form_id AS tax_pr_form_id,
				Nvl(Max(tax_pr_summary_id),0) + 1 AS tax_pr_summary_id,
				l_description AS description,
				l_tax_pr_section_id AS tax_pr_section_id,
				NULL AS jurisdiction_id,
				l_recon_source AS recon_source,
				l_tax_account_type AS tax_account_type,
				1 AS sort_order,
				0 AS subtotal
			FROM tax_plant_recon_form_summ_id
			WHERE tax_pr_form_id = a_tax_pr_form_id
		) b
		ON (a.tax_pr_form_id = b.tax_pr_form_id
			AND a.description = b.description
			AND a.tax_pr_section_id = b.tax_pr_section_id)
		WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type,
   				sort_order, subtotal)
			VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source, b.tax_account_type,
   				b.sort_order, b.subtotal);

    write_log(g_job_no, 0, 0, '-- Load Asset Activity Summary');
		MERGE INTO tax_plant_recon_form_summary a
		USING (
			SELECT s.tax_pr_form_id,
				s.tax_pr_summary_id,
				Sum(Decode(To_Char(summ.gl_posting_mo_yr, 'yyyymm'),
					v.beg_month,
					Nvl(summ.beginning_balance, 0), 0)) beg_balance,
				Sum(Nvl(summ.additions, 0)) additions,
				Sum(Nvl(summ.adjustments, 0)) adjustments,
				Sum(Nvl(summ.transfers_in, 0) + Nvl(summ.transfers_out, 0)) transfers,
				Sum(Nvl(summ.retirements, 0)) retirements,
				0 book_provision,
				0 actual_cor,
				0 actual_salvage,
				0 other,
				Sum(Decode(To_Char(summ.gl_posting_mo_yr, 'yyyymm'), v.end_month, Nvl(summ.ending_balance, 0), 0)) end_balance
			FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form f, account_summary summ, company_setup cs,
				(
				SELECT tax_year_version_id,
            				To_Char(Floor(tax_year))||'01' beg_month,
            				To_Char(Floor(tax_year))||LPad(To_Char(months),2,'0') end_month
    				FROM tax_year_version
    				WHERE months <> 0
				) v
			WHERE f.tax_pr_form_id = a_tax_pr_form_id
			AND f.tax_year_version_id = v.tax_year_version_id
			AND f.tax_pr_form_id = s.tax_pr_form_id
			AND s.tax_pr_section_id = l_tax_pr_section_id
			AND s.recon_source = l_recon_source
			AND s.tax_account_type = l_tax_account_type
			AND s.description = l_description
			AND summ.company_id = cs.company_id
			AND summ.set_of_books_id = 1
			AND To_Char(summ.gl_posting_mo_yr, 'yyyymm') >= v.beg_month
			AND To_Char(summ.gl_posting_mo_yr, 'yyyymm') <= v.end_month
			AND Nvl(cs.powertax_company_id, cs.company_id) = f.company_id
			GROUP BY s.tax_pr_form_id, s.tax_pr_summary_id
		) b
		ON (a.tax_pr_form_id = b.tax_pr_form_id
			AND a.tax_pr_summary_id = b.tax_pr_summary_id)
		WHEN MATCHED THEN
			UPDATE
			SET a.beg_balance = b.beg_balance,
				a.additions = b.additions,
				a.adjustments = b.adjustments,
				a.transfers = b.transfers,
				a.retirements = b.retirements,
				a.book_provision = b.book_provision,
				a.actual_cor = b.actual_cor,
				a.actual_salvage = b.actual_salvage,
				a.other = b.other,
				a.end_balance = b.end_balance
		WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements,
  	  			book_provision, actual_cor, actual_salvage, other, end_balance)
			VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements,
  	  			b.book_provision, b.actual_cor, b.actual_salvage, b.other, b.end_balance);

		RETURN 0;
	EXCEPTION
		WHEN OTHERS THEN
    		write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_LOAD_ASSET_ACTIVITY. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
    		RETURN -1;
	END f_load_asset_activity;

  --=============================================================================
	-- Function F_LOAD_TAX_ASSET_ACTIVITY
	--
	-- Description: Populates 
	--=============================================================================
	FUNCTION f_load_tax_asset_activity(a_tax_pr_form_id NUMBER)
		 RETURN NUMBER
	IS
		--this is the PowerTax Asset Actuals value
		l_description  			tax_plant_recon_form_summ_id.description%TYPE := 'PowerTax Book Basis Balance';
		l_tax_pr_section_id 	tax_plant_recon_form_summ_id.tax_pr_section_id%TYPE := 3;
		l_recon_source			tax_plant_recon_form_summ_id.recon_source%TYPE := 2;
		l_tax_account_type		tax_plant_recon_form_summ_id.tax_account_type%TYPE := 1;
	BEGIN
    write_log(g_job_no, 0, 0, '-- Load Tax Asset Activity ID');
		MERGE INTO tax_plant_recon_form_summ_id a
		USING (
			SELECT a_tax_pr_form_id AS tax_pr_form_id,
				Nvl(Max(tax_pr_summary_id),0) + 1 AS tax_pr_summary_id,
				l_description AS description,
				l_tax_pr_section_id AS tax_pr_section_id,
				NULL AS jurisdiction_id,
				l_recon_source AS recon_source,
				l_tax_account_type AS tax_account_type,
				4 AS sort_order,
				0 AS subtotal
			FROM tax_plant_recon_form_summ_id
			WHERE tax_pr_form_id = a_tax_pr_form_id
		) b
		ON (a.tax_pr_form_id = b.tax_pr_form_id
			AND a.description = b.description
			AND a.tax_pr_section_id = b.tax_pr_section_id)
		WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type,
   				sort_order, subtotal)
			VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source, b.tax_account_type,
   				b.sort_order, b.subtotal);

    write_log(g_job_no, 0, 0, '-- Load Tax Asset Activity Summary');
		MERGE INTO tax_plant_recon_form_summary a
		USING (
      WITH v as (
          SELECT tax_year_version_id, tax_year
              FROM tax_year_version
              WHERE months <> 0
          ),
      tax_record_view as (
          select /*+ materialize */ c.company_id, c.tax_record_id, v.tax_year
            from tax_record_control c, tax_plant_recon_form f, v
           where c.version_id = g_version
             and c.company_id = f.company_id
             and f.tax_pr_form_id = a_tax_pr_form_id
             and f.tax_year_version_id = v.tax_year_version_id
             ),
      tax_depr_view as (
          select /*+ materialize */ d.tax_book_id tax_book_id,
                 sum(d.book_balance) book_balance,
                 sum(nvl(x.book_balance,0)) book_balance_transfer,
                 sum(d.book_balance_end) book_balance_end
            from tax_record_view r, tax_depreciation d, tax_depreciation_transfer x
           where d.tax_record_id = r.tax_record_id
             and d.tax_year = r.tax_year 
             and d.tax_record_id = x.tax_record_id (+)
             and d.tax_year = x.tax_year (+)
             and d.tax_book_id = x.tax_book_id (+)
           group by d.tax_book_id
           ),
     basis_amounts_view_additions as (
          SELECT /*+ materialize */ tax_record_view.company_id company_id,
                      tax_record_view.tax_year tax_year,
                      tax_include_activity.tax_book_id tax_book_id,
                      Sum(basis_amounts.amount) basis_amounts_additions
              FROM tax_record_view,
                      basis_amounts,
                      tax_include_activity
              WHERE basis_amounts.tax_year = tax_record_view.tax_year
              AND basis_amounts.tax_record_id = tax_record_view.tax_record_id
              AND basis_amounts.tax_include_id = tax_include_activity.tax_include_id
              and basis_amounts.tax_activity_code_id not in (2, 4, 12)
              GROUP BY tax_record_view.company_id,
                          tax_record_view.tax_year,
                          tax_include_activity.tax_book_id
          ),
     basis_amounts_view_disp as (
          SELECT /*+ materialize */ tax_record_view.company_id company_id,
                      tax_record_view.tax_year tax_year,
                      tax_include_activity.tax_book_id tax_book_id,
                      Sum(-1 * basis_amounts.amount) basis_amounts_dispositions
              FROM tax_record_view,
                      basis_amounts,
                      tax_include_activity
              WHERE basis_amounts.tax_year = tax_record_view.tax_year
              AND basis_amounts.tax_record_id = tax_record_view.tax_record_id
              AND basis_amounts.tax_include_id = tax_include_activity.tax_include_id
              and basis_amounts.tax_activity_code_id in (2, 4, 12)
              GROUP BY tax_record_view.company_id,
                          tax_record_view.tax_year,
                          tax_include_activity.tax_book_id
          )
   SELECT /*+ NO_MERGE */ s.tax_pr_form_id,
          s.tax_pr_summary_id,
          t.book_balance beg_balance,
          Nvl(a.basis_amounts_additions, 0) additions,
          0 adjustments,
          t.book_balance_transfer transfers,
          Nvl(d.basis_amounts_dispositions, 0) retirements,
          0 book_provision,
          0 actual_cor,
          0 actual_salvage,
          0 other,
          t.book_balance_end end_balance
      FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form f,
          tax_depr_view t, (select tbs.x x, max(y.tax_book_id) tax_book_id from tax_book_schema tbs, tax_depr_view y
                             where tbs.tax_book_id = y.tax_book_id
                             group by tbs.x) z,
          basis_amounts_view_additions a,
          basis_amounts_view_disp d,
          jurisdiction j
      WHERE f.tax_pr_form_id = a_tax_pr_form_id
      AND f.tax_pr_form_id = s.tax_pr_form_id
      AND s.description = l_description
      AND s.tax_pr_section_id = l_tax_pr_section_id
      AND s.recon_source = l_recon_source
			AND s.tax_account_type = l_tax_account_type
      AND t.tax_book_id = a.tax_book_id (+)
      and t.tax_book_id = d.tax_book_id (+)
      and t.tax_book_id = z.tax_book_id
      and z.x = 10 -- only want federal
      and j.tax_book_id = t.tax_book_id
      and lower(trim(j.description)) = 'federal'
		) b
		ON (a.tax_pr_form_id = b.tax_pr_form_id
			AND a.tax_pr_summary_id = b.tax_pr_summary_id)
		WHEN MATCHED THEN
			UPDATE
			SET a.beg_balance = b.beg_balance,
				a.additions = b.additions,
				a.adjustments = b.adjustments,
				a.transfers = b.transfers,
				a.retirements = b.retirements,
				a.book_provision = b.book_provision,
				a.actual_cor = b.actual_cor,
				a.actual_salvage = b.actual_salvage,
				a.other = b.other,
				a.end_balance = b.end_balance
		WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements,
  				book_provision, actual_cor, actual_salvage, other, end_balance)
			VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements,
  				b.book_provision, b.actual_cor, b.actual_salvage, b.other, b.end_balance);

		RETURN 0;
	EXCEPTION
		WHEN OTHERS THEN
    		write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_LOAD_TAX_ASSET_ACTIVITY. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
    		RETURN -1;
	END f_load_tax_asset_activity;

  --=============================================================================
	-- Function F_LOAD_DEPR_RESERVE_ACTIVITY
	--
	-- Description: Populates 
	--=============================================================================
	FUNCTION f_load_depr_reserve_activity(a_tax_pr_form_id NUMBER)
		 RETURN NUMBER
	IS
		l_description  			tax_plant_recon_form_summ_id.description%TYPE := 'PowerPlan CPR Depr Reserve Balance';
		l_tax_pr_section_id 	tax_plant_recon_form_summ_id.tax_pr_section_id%TYPE := 5;
		l_recon_source			tax_plant_recon_form_summ_id.recon_source%TYPE := 3;
		l_tax_account_type		tax_plant_recon_form_summ_id.tax_account_type%TYPE := 2;
	BEGIN
    write_log(g_job_no, 0, 0, '-- Load Depr Reserve Activity ID');
		MERGE INTO tax_plant_recon_form_summ_id a
		USING (
			SELECT a_tax_pr_form_id AS tax_pr_form_id,
				Nvl(Max(tax_pr_summary_id),0) + 1 AS tax_pr_summary_id,
				l_description AS description,
				l_tax_pr_section_id AS tax_pr_section_id,
				NULL AS jurisdiction_id,
				l_recon_source AS recon_source,
				l_tax_account_type AS tax_account_type,
				1 AS sort_order,
				0 AS subtotal
			FROM tax_plant_recon_form_summ_id
			WHERE tax_pr_form_id = a_tax_pr_form_id
		) b
		ON (a.tax_pr_form_id = b.tax_pr_form_id
			AND a.description = b.description
			AND a.tax_pr_section_id = b.tax_pr_section_id)
		WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type,
   				sort_order, subtotal)
			VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source, b.tax_account_type,
   				b.sort_order, b.subtotal);

		write_log(g_job_no, 0, 0, '-- Load Depr Reserve Activity Summary');
    MERGE INTO tax_plant_recon_form_summary a
		USING (
			SELECT s.tax_pr_form_id,
        		s.tax_pr_summary_id,
        		Sum(Decode(To_Char(dv.gl_post_mo_yr, 'yyyymm'), v.beg_month, dv.begin_reserve, 0)) beg_balance,
        		0 additions,
        		0 adjustments,
        		0 transfers,
        		Sum(dv.retirements) retirements,
        		Sum(dv.provision) book_provision,
        		Sum(dv.cost_of_removal) actual_cor,
        		Sum(dv.salv_credits) actual_salvage,
        		Sum(dv.transfers + dv.gain_loss) other,
        		Sum(Decode(To_Char(dv.gl_post_mo_yr, 'yyyymm'), v.end_month, dv.end_reserve, 0)) end_balance
  			FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form f, depr_total_report_v dv, depr_group dg, company_setup cs,
        		(
            		SELECT tax_year_version_id,
                    		To_Char(Floor(tax_year)) || '01' beg_month,
                    		To_Char(Floor(tax_year)) || LPad(To_Char(months),2,'0') end_month
            		FROM tax_year_version
            		WHERE months <> 0
        		) v
 			WHERE f.tax_pr_form_id = a_tax_pr_form_id
    		AND f.tax_year_version_id = v.tax_year_version_id
    		AND f.tax_pr_form_id = s.tax_pr_form_id
    		AND s.tax_pr_section_id = l_tax_pr_section_id
    		AND s.recon_source = l_recon_source
    		AND s.tax_account_type = l_tax_account_type
			AND s.description = l_description
    		AND dv.depr_group_id = dg.depr_group_id
    		AND dg.company_id = cs.company_id
    		AND dv.set_of_books_id = 1
    		AND To_Number(To_Char(dv.gl_post_mo_yr, 'yyyymm')) >= To_Number(v.beg_month)
    		AND To_Number(To_Char(dv.gl_post_mo_yr, 'yyyymm')) <= To_Number(v.end_month)
    		AND Nvl(cs.powertax_company_id, cs.company_id) = f.company_id
 			GROUP BY s.tax_pr_form_id, s.tax_pr_summary_id
		) b
		ON (a.tax_pr_form_id = b.tax_pr_form_id
			AND a.tax_pr_summary_id = b.tax_pr_summary_id)
		WHEN MATCHED THEN
			UPDATE
			SET a.beg_balance = b.beg_balance,
				a.additions = b.additions,
				a.adjustments = b.adjustments,
				a.transfers = b.transfers,
				a.retirements = b.retirements,
				a.book_provision = b.book_provision,
				a.actual_cor = b.actual_cor,
				a.actual_salvage = b.actual_salvage,
				a.other = b.other,
				a.end_balance = b.end_balance
		WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements,
  				book_provision, actual_cor, actual_salvage, other, end_balance)
			VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements,
  				b.book_provision, b.actual_cor, b.actual_salvage, b.other, b.end_balance);

		RETURN 0;
	EXCEPTION
		WHEN OTHERS THEN
    		write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_LOAD_DEPR_RESERVE_ACTIVITY. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
    		RETURN -1;
	END f_load_depr_reserve_activity;

  --=============================================================================
	-- Function F_LOAD_TAX_RESERVE_ACTIVITY
	--
	-- Description: Populates 
	--=============================================================================
	FUNCTION f_load_tax_reserve_activity(a_tax_pr_form_id NUMBER)
		 RETURN NUMBER
	IS
		l_description_reserve		tax_plant_recon_form_summ_id.description%TYPE := 'PowerTax Reserve Balance';
		l_tax_pr_section_id_reserve	tax_plant_recon_form_summ_id.tax_pr_section_id%TYPE := 5;

		l_description_book			tax_plant_recon_form_summ_id.description%TYPE := 'Deferred Tax Book Basis Balance';
		l_tax_pr_section_id_book	tax_plant_recon_form_summ_id.tax_pr_section_id%TYPE := 7;

		l_recon_source				tax_plant_recon_form_summ_id.recon_source%TYPE := 2;
		l_tax_account_type			tax_plant_recon_form_summ_id.tax_account_type%TYPE := 2;
	BEGIN
    write_log(g_job_no, 0, 0, '-- Load Tax Reserve Activity ID');
		MERGE INTO tax_plant_recon_form_summ_id a
		USING (
			SELECT a_tax_pr_form_id AS tax_pr_form_id,
    			Nvl(Max(tax_pr_summary_id),0) + 1 AS tax_pr_summary_id,
    			l_description_reserve AS description,
    			l_tax_pr_section_id_reserve AS tax_pr_section_id,
    			NULL AS jurisdiction_id,
    			l_recon_source AS recon_source,
    			l_tax_account_type AS tax_account_type,
    			4 AS sort_order,
    			0 AS subtotal
			FROM tax_plant_recon_form_summ_id
			WHERE tax_pr_form_id = a_tax_pr_form_id
			UNION
			SELECT a_tax_pr_form_id,
    			Nvl(Max(tax_pr_summary_id),0) + 2, --add 2 instead of 1 to get a different ID than the upper part of the UNION
    			l_description_book AS description,
    			l_tax_pr_section_id_book AS tax_pr_section_id,
    			NULL,
    			l_recon_source AS recon_source,
    			l_tax_account_type AS tax_account_type,
    			8 AS sort_order,
    			0 AS subtotal
			FROM tax_plant_recon_form_summ_id
			WHERE tax_pr_form_id = a_tax_pr_form_id
		) b
		ON (a.tax_pr_form_id = b.tax_pr_form_id
			AND a.description = b.description
			AND a.tax_pr_section_id = b.tax_pr_section_id)
		WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type,
   				sort_order, subtotal)
			VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source, b.tax_account_type,
   				b.sort_order, b.subtotal);

		write_log(g_job_no, 0, 0, '-- Load Tax Reserve Activity Summary');
    MERGE INTO tax_plant_recon_form_summary a
		USING (
      WITH v as (
          SELECT tax_year_version_id, tax_year
              FROM tax_year_version
              WHERE months <> 0
          ),
      tax_record_view as (
          select /*+ materialize */ c.company_id, c.tax_record_id, v.tax_year
            from tax_record_control c, tax_plant_recon_form f, v
           where c.version_id = g_version
             and c.company_id = f.company_id
             and f.tax_pr_form_id = a_tax_pr_form_id
             and f.tax_year_version_id = v.tax_year_version_id
             ),
       amort_1 as (
                   SELECT  /*+ materialize */ deferred_income_tax.tax_year tax_year,
                           tax_record_view.company_id company_id,
                           Sum(tax_depreciation.accum_reserve - tax_depreciation.accum_ordinary_retires) beg_balance,
                           Sum(tax_depreciation.accum_reserve_end - tax_depreciation.accum_ordin_retires_end) end_balance,
                           Sum(tax_depreciation.tax_balance_end - tax_depreciation.accum_ordin_retires_end) fin_book_basis_end
                      FROM tax_record_view,
                           normalization_schema,
                           deferred_income_tax,
                           tax_depreciation,
                           tax_book to_tax_book
                       WHERE normalization_schema.normalization_id = deferred_income_tax.normalization_id
                        AND deferred_income_tax.tax_record_id = tax_record_view.tax_record_id
                        AND tax_depreciation.tax_book_id = normalization_schema.norm_to_tax
                        AND tax_depreciation.tax_year = deferred_income_tax.tax_year
                        AND tax_depreciation.tax_record_id = deferred_income_tax.tax_record_id
                        and tax_depreciation.tax_year = tax_record_view.tax_year
                        AND normalization_schema.amortization_type_id = 1
                        AND normalization_schema.norm_to_tax = to_tax_book.tax_book_id
                        and to_tax_book.report_indicator = 2
                        AND normalization_schema.jurisdiction_id IN (
                                SELECT jurisdiction_id
                                  FROM jurisdiction
                                 WHERE tax_book_id = 10
                                   AND lower(trim(description)) = 'federal')
                       GROUP BY deferred_income_tax.tax_year, tax_record_view.company_id
                      ),
           amort_0 as (
                   SELECT  /*+ materialize */ deferred_income_tax.tax_year tax_year,
                         tax_record_view.company_id company_id,
                         Sum(tax_book_reconcile.basis_amount_beg * - 1 - deferred_income_tax.norm_diff_balance_beg) beg_balance,
                         Sum(tax_book_reconcile.basis_amount_end * - 1 - deferred_income_tax.norm_diff_balance_end) end_balance,
                         Sum(tax_book_reconcile.basis_amount_end * - 1) fin_book_basis_end
                      FROM normalization_schema,
                         tax_record_view,
                         deferred_income_tax,
                         tax_book_reconcile,
                         tax_include_activity,
                         jurisdiction
                     WHERE normalization_schema.normalization_id = deferred_income_tax.normalization_id
                      AND deferred_income_tax.tax_record_id = tax_record_view.tax_record_id
                      AND normalization_schema.amortization_type_id = 0
                      AND tax_book_reconcile.reconcile_item_id = normalization_schema.reconcile_item_id
                      AND tax_book_reconcile.tax_include_id = tax_include_activity.tax_include_id
                      AND tax_include_activity.tax_book_id = jurisdiction.tax_book_id
                      AND normalization_schema.jurisdiction_id = jurisdiction.jurisdiction_id
                      AND tax_book_reconcile.tax_year = deferred_income_tax.tax_year
                      AND tax_book_reconcile.tax_record_id = tax_record_view.tax_record_id
                      AND tax_record_view.tax_year = tax_book_reconcile.tax_year
                      AND jurisdiction.tax_book_id = 10
                      AND lower(trim(jurisdiction.description)) = 'federal'
                     GROUP BY deferred_income_tax.tax_year,
                         tax_record_view.company_id
                      )
			SELECT /*+ NO_MERGE */ s.tax_pr_form_id,
    			s.tax_pr_summary_id,
				Sum(Decode(s.tax_pr_section_id, 5, a.beg_balance, 0)) beg_balance,
				0 additions,
				0 adjustments,
				0 transfers,
				0 retirements,
				0 book_provision,
				0 actual_cor,
				0 actual_salvage,
				0 other,
				Sum(Decode(s.tax_pr_section_id, 5, a.end_balance, 7, a.fin_book_basis_end)) end_balance
			FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form f,
    			(
    				select tax_year, company_id, beg_balance, end_balance, fin_book_basis_end
              from amort_1
            union
            select tax_year, company_id, beg_balance, end_balance, fin_book_basis_end
              from amort_0
    			) a
			WHERE f.tax_pr_form_id = a_tax_pr_form_id
			AND f.tax_pr_form_id = s.tax_pr_form_id
			AND s.tax_pr_section_id IN (l_tax_pr_section_id_reserve, l_tax_pr_section_id_book)
			AND s.recon_source = l_recon_source
			AND s.tax_account_type = l_tax_account_type
			AND s.description IN (l_description_reserve, l_description_book)
			AND f.company_id = a.company_id
			GROUP BY s.tax_pr_form_id,
    			s.tax_pr_summary_id
		) b
		ON (a.tax_pr_form_id = b.tax_pr_form_id
			AND a.tax_pr_summary_id = b.tax_pr_summary_id)
		WHEN MATCHED THEN
			UPDATE
			SET a.beg_balance = b.beg_balance,
				a.additions = b.additions,
				a.adjustments = b.adjustments,
				a.transfers = b.transfers,
				a.retirements = b.retirements,
				a.book_provision = b.book_provision,
				a.actual_cor = b.actual_cor,
				a.actual_salvage = b.actual_salvage,
				a.other = b.other,
				a.end_balance = b.end_balance
		WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements,
  				book_provision, actual_cor, actual_salvage, other, end_balance)
			VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements,
  				b.book_provision, b.actual_cor, b.actual_salvage, b.other, b.end_balance);

		RETURN 0;
	EXCEPTION
		WHEN OTHERS THEN
    		write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_LOAD_TAX_RESERVE_ACTIVITY. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
    		RETURN -1;
	END f_load_tax_reserve_activity;

  --=============================================================================
	-- Function F_LOAD_TAX_RESERVE_SUMMARY
	--
	-- Description: Populates 
	--=============================================================================
	FUNCTION f_load_tax_reserve_summary(a_tax_pr_form_id NUMBER)
		 RETURN NUMBER
	IS
	BEGIN
    write_log(g_job_no, 0, 0, '-- Load Tax Reserve Summary');
		MERGE INTO tax_plant_recon_form_res_summ a
		USING (
      WITH v as (
            SELECT tax_year_version_id, tax_year
              FROM tax_year_version
             WHERE months <> 0
                ),
          tax_record_view as (
            select /*+ materialize */ c.company_id, c.tax_record_id, v.tax_year
              from tax_record_control c, tax_plant_recon_form f, v
             where c.version_id = g_version
               and c.company_id = f.company_id
               and f.tax_pr_form_id = a_tax_pr_form_id
               and f.tax_year_version_id = v.tax_year_version_id
                             ),
          tax_depr_view as (
            select /*+ materialize */ td.tax_book_id,
                   Sum(td.accum_reserve) beg_balance,
                   Sum(td.depreciation) depr_expense,
                   Sum(-(td.retire_res_impact - td.ex_retire_res_impact) - ( td.ex_retire_res_impact)) reserve_retire,
                   Sum(td.transfer_res_impact) transfers,
                   Sum(td.salvage_res_impact) salvage_impacts,
                   Sum(-td.cor_res_impact) cor_impacts,
                   Sum(td.accum_reserve_end - (td.accum_reserve + td.depreciation - td.retire_res_impact + td.transfer_res_impact - td.cor_res_impact + td.accum_reserve_adjust)) other_bonus,
                   Sum(td.accum_reserve_end) end_balance
              from tax_depreciation td, tax_record_view r
             where td.tax_record_id = r.tax_record_id
               and td.tax_year = r.tax_year
              group by td.tax_book_id
                           )
			SELECT /*+ NO_MERGE */ f.tax_pr_form_id,
    			j.jurisdiction_id,
    			tax_depr_view.beg_balance beg_balance,
    			tax_depr_view.depr_expense depr_expense,
    			tax_depr_view.reserve_retire reserve_retire,
    			tax_depr_view.transfers transfers,
    			tax_depr_view.salvage_impacts salvage_impacts,
    			tax_depr_view.cor_impacts cor_impacts,
    			tax_depr_view.other_bonus other_bonus,
    			tax_depr_view.end_balance end_balance
			FROM tax_plant_recon_form f, tax_plant_recon_form_jur fj, jurisdiction j,
    			 tax_depr_view, (select tbs.x x, max(y.tax_book_id) tax_book_id from tax_book_schema tbs, tax_depr_view y
                            where tbs.tax_book_id = y.tax_book_id
                            group by tbs.x) z
			WHERE f.tax_pr_form_id = a_tax_pr_form_id
			  AND f.tax_pr_form_id = fj.tax_pr_form_id
			  AND fj.jurisdiction_id = j.jurisdiction_id
        AND tax_depr_view.tax_book_id = z.tax_book_id
			  AND j.tax_book_id = z.x
		) b
		ON (a.tax_pr_form_id = b.tax_pr_form_id
			AND a.jurisdiction_id = b.jurisdiction_id)
		WHEN MATCHED THEN
			UPDATE
			SET a.beg_balance = b.beg_balance,
				a.depr_expense = b.depr_expense,
				a.transfers = b.transfers,
				a.reserve_retire = b.reserve_retire,
				a.salvage_impacts = b.salvage_impacts,
				a.cor_impacts = b.cor_impacts,
				a.other_bonus = b.other_bonus,
				a.end_balance = b.end_balance
		WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, jurisdiction_id, beg_balance, depr_expense, transfers, reserve_retire,
    			salvage_impacts, cor_impacts, other_bonus, end_balance)
			VALUES (b.tax_pr_form_id, b.jurisdiction_id, b.beg_balance, b.depr_expense, b.transfers, b.reserve_retire,
    			b.salvage_impacts, b.cor_impacts, b.other_bonus, b.end_balance);

		RETURN 0;
	EXCEPTION
		WHEN OTHERS THEN
    		write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_LOAD_TAX_RESERVE_SUMMARY. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
    		RETURN -1;
	END f_load_tax_reserve_summary;

  --=============================================================================
	-- Function F_LOAD_TAX_BOOK_BASIS_SUMMARY
	--
	-- Description: Populates 
	--=============================================================================
	FUNCTION f_load_tax_book_basis_summary(a_tax_pr_form_id NUMBER)
		 RETURN NUMBER
	IS
		l_description				tax_plant_recon_form_summ_id.description%TYPE;
		l_tax_pr_section_id			tax_plant_recon_form_summ_id.tax_pr_section_id%TYPE := 4;
		l_recon_source				tax_plant_recon_form_summ_id.recon_source%TYPE := 2;
	BEGIN
    -- Load the IDs
    -- PowerTax Book Basis
    l_description := 'PowerTax Book Basis';
    write_log(g_job_no, 0, 0, '-- Load PowerTax Book Basis ID');
    MERGE INTO tax_plant_recon_form_summ_id a
    USING (
      SELECT tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source,
        tax_account_type,
        sort_order,
        subtotal
      FROM (
        SELECT a_tax_pr_form_id AS tax_pr_form_id,
            Nvl(Max(tax_pr_summary_id),0) AS max_tax_pr_summary_id,
            l_description AS description,
            l_tax_pr_section_id AS tax_pr_section_id,
            j.jurisdiction_id AS jurisdiction_id,
            l_recon_source AS recon_source,
            NULL AS tax_account_type,
            1 AS sort_order,
            0 AS subtotal
        FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j
        WHERE s.tax_pr_form_id = a_tax_pr_form_id
        AND s.tax_pr_form_id = j.tax_pr_form_id
        GROUP BY j.jurisdiction_id
      )
    ) b
    ON (a.tax_pr_form_id = b.tax_pr_form_id
      AND a.description = b.description
      AND a.tax_pr_section_id = b.tax_pr_section_id)
    WHEN NOT MATCHED THEN
      INSERT (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type,
          sort_order, subtotal)
      VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source, b.tax_account_type,
          b.sort_order, b.subtotal);
    
    -- PowerTax Book Basis
    l_description := 'PowerTax Book Basis';
    write_log(g_job_no, 0, 0, '-- Load PowerTax Book Basis Summary');
    MERGE INTO tax_plant_recon_form_summary a
    USING (
      WITH v as (
          SELECT tax_year_version_id, tax_year
              FROM tax_year_version
              WHERE months <> 0
          ),
      tax_record_view as (
          select /*+ materialize */ c.company_id, c.tax_record_id, v.tax_year
            from tax_record_control c, tax_plant_recon_form f, v
           where c.version_id = g_version
             and c.company_id = f.company_id
             and f.tax_pr_form_id = a_tax_pr_form_id
             and f.tax_year_version_id = v.tax_year_version_id
             ),
      tax_depr_view as (
          select /*+ materialize */ d.tax_book_id tax_book_id,
                 sum(d.book_balance) book_balance,
                 sum(nvl(x.book_balance,0)) book_balance_transfer,
                 sum(d.book_balance_end) book_balance_end
            from tax_record_view r, tax_depreciation d, tax_depreciation_transfer x
           where d.tax_record_id = r.tax_record_id
             and d.tax_year = r.tax_year 
             and d.tax_record_id = x.tax_record_id (+)
             and d.tax_year = x.tax_year (+)
             and d.tax_book_id = x.tax_book_id (+)
           group by d.tax_book_id
           ),
     basis_amounts_view_additions as (
          SELECT /*+ materialize */ tax_record_view.company_id company_id,
                      tax_record_view.tax_year tax_year,
                      tax_include_activity.tax_book_id tax_book_id,
                      Sum(basis_amounts.amount) basis_amounts_additions
              FROM tax_record_view,
                      basis_amounts,
                      tax_include_activity
              WHERE basis_amounts.tax_year = tax_record_view.tax_year
              AND basis_amounts.tax_record_id = tax_record_view.tax_record_id
              AND basis_amounts.tax_include_id = tax_include_activity.tax_include_id
              and basis_amounts.tax_activity_code_id not in (2, 4, 12)
              GROUP BY tax_record_view.company_id,
                          tax_record_view.tax_year,
                          tax_include_activity.tax_book_id
          ),
     basis_amounts_view_disp as (
          SELECT /*+ materialize */ tax_record_view.company_id company_id,
                      tax_record_view.tax_year tax_year,
                      tax_include_activity.tax_book_id tax_book_id,
                      Sum(-1 * basis_amounts.amount) basis_amounts_dispositions
              FROM tax_record_view,
                      basis_amounts,
                      tax_include_activity
              WHERE basis_amounts.tax_year = tax_record_view.tax_year
              AND basis_amounts.tax_record_id = tax_record_view.tax_record_id
              AND basis_amounts.tax_include_id = tax_include_activity.tax_include_id
              and basis_amounts.tax_activity_code_id in (2, 4, 12)
              GROUP BY tax_record_view.company_id,
                          tax_record_view.tax_year,
                          tax_include_activity.tax_book_id
          )
   SELECT /*+ NO_MERGE */ s.tax_pr_form_id,
          s.tax_pr_summary_id,
          t.book_balance beg_balance,
          Nvl(a.basis_amounts_additions, 0) additions,
          0 adjustments,
          t.book_balance_transfer transfers,
          Nvl(d.basis_amounts_dispositions, 0) retirements,
          0 book_provision,
          0 actual_cor,
          0 actual_salvage,
          0 other,
          t.book_balance_end end_balance
      FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form f, jurisdiction j, 
          tax_depr_view t, (select tbs.x x, max(y.tax_book_id) tax_book_id from tax_book_schema tbs, tax_depr_view y
                             where tbs.tax_book_id = y.tax_book_id
                             group by tbs.x) z,
          basis_amounts_view_additions a,
          basis_amounts_view_disp d
      WHERE f.tax_pr_form_id = a_tax_pr_form_id
      AND f.tax_pr_form_id = s.tax_pr_form_id
      AND s.description = l_description
      AND s.tax_pr_section_id = l_tax_pr_section_id
      AND s.recon_source = l_recon_source
      AND s.sort_order = 1
      and s.jurisdiction_id = j.jurisdiction_id
      AND t.tax_book_id = a.tax_book_id (+)
      and t.tax_book_id = d.tax_book_id (+)
      and t.tax_book_id = z.tax_book_id
      and z.x = j.tax_book_id
    ) b
    ON (a.tax_pr_form_id = b.tax_pr_form_id
      AND a.tax_pr_summary_id = b.tax_pr_summary_id)
    WHEN MATCHED THEN
      UPDATE
      SET a.beg_balance = b.beg_balance,
        a.additions = b.additions,
        a.adjustments = b.adjustments,
        a.transfers = b.transfers,
        a.retirements = b.retirements,
        a.book_provision = b.book_provision,
        a.actual_cor = b.actual_cor,
        a.actual_salvage = b.actual_salvage,
        a.other = b.other,
        a.end_balance = b.end_balance
    WHEN NOT MATCHED THEN
      INSERT (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements,
          book_provision, actual_cor, actual_salvage, other, end_balance)
      VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements,
          b.book_provision, b.actual_cor, b.actual_salvage, b.other, b.end_balance);
    
    -- Total Book to Tax Basis Difference
    l_description := 'Total Book to Tax Basis Difference';
    write_log(g_job_no, 0, 0, '-- Load Total Book to Tax Basis Difference ID');
    MERGE INTO tax_plant_recon_form_summ_id a
    USING (
      SELECT tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source,
        tax_account_type,
        sort_order,
        subtotal
      FROM (
        SELECT a_tax_pr_form_id AS tax_pr_form_id,
            Nvl(Max(tax_pr_summary_id),0) AS max_tax_pr_summary_id,
            l_description AS description,
            l_tax_pr_section_id AS tax_pr_section_id,
            j.jurisdiction_id AS jurisdiction_id,
            l_recon_source AS recon_source,
            NULL AS tax_account_type,
            2 AS sort_order,
            0 AS subtotal
        FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j
        WHERE s.tax_pr_form_id = a_tax_pr_form_id
        AND s.tax_pr_form_id = j.tax_pr_form_id
        GROUP BY j.jurisdiction_id
      )
    ) b
    ON (a.tax_pr_form_id = b.tax_pr_form_id
      AND a.description = b.description
      AND a.tax_pr_section_id = b.tax_pr_section_id)
    WHEN NOT MATCHED THEN
      INSERT (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type,
          sort_order, subtotal)
      VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source, b.tax_account_type,
          b.sort_order, b.subtotal);
    
    -- Total Book to Tax Basis Difference
    l_description := 'Total Book to Tax Basis Difference';
    write_log(g_job_no, 0, 0, '-- Load Total Book to Tax Basis Difference Summary');
    MERGE INTO tax_plant_recon_form_summary a
    USING (
      WITH v as (
              SELECT tax_year_version_id, tax_year
                  FROM tax_year_version
                  WHERE months <> 0
                ),
           tax_record_view as (
              select /*+ materialize */ c.company_id, c.tax_record_id, v.tax_year
                from tax_record_control c, tax_plant_recon_form f, v
               where c.version_id = g_version
                 and c.company_id = f.company_id
                 and f.tax_pr_form_id = a_tax_pr_form_id
                 and f.tax_year_version_id = v.tax_year_version_id
             ),
             tax_depr_view as (
             select /*+ materialize */ d.tax_book_id, d.tax_year
               from tax_depreciation d, tax_record_view r
              where d.tax_record_id = r.tax_record_id
                and d.tax_year = r.tax_year
             ),
           reconcile_view as (
                  SELECT /*+ materialize */ tax_include_activity.tax_book_id,
                      Sum(tax_book_reconcile.basis_amount_beg) begin_m_item,
                      Sum(Nvl(tax_book_reconcile_transfer.basis_amount_beg, 0)) m_item_transfer,
                      Sum(tax_book_reconcile.basis_amount_activity) m_item_additions,
                      Sum(tax_book_reconcile.basis_amount_end - (Nvl(tax_book_reconcile_transfer.basis_amount_beg, 0) + tax_book_reconcile.basis_amount_beg)
                          - tax_book_reconcile.basis_amount_activity) m_item_dispositions,
                      Sum(tax_book_reconcile.basis_amount_end) end_m_item
                      FROM tax_record_view,
                            tax_book_reconcile,
                            tax_include_activity,
                            tax_reconcile_item,
                            tax_book_reconcile_transfer
                      WHERE tax_record_view.tax_year = tax_book_reconcile.tax_year
                        AND tax_record_view.tax_record_id = tax_book_reconcile.tax_record_id
                        AND tax_book_reconcile.tax_include_id = tax_include_activity.tax_include_id
                        AND tax_book_reconcile.reconcile_item_id = tax_reconcile_item.reconcile_item_id
                        AND tax_book_reconcile.tax_record_id = tax_book_reconcile_transfer.tax_record_id (+)
                        AND tax_book_reconcile.tax_include_id = tax_book_reconcile_transfer.tax_include_id (+)
                        AND tax_book_reconcile.reconcile_item_id = tax_book_reconcile_transfer.reconcile_item_id (+)
                        AND tax_book_reconcile.tax_year = tax_book_reconcile_transfer.tax_year (+) 
                      GROUP BY tax_include_activity.tax_book_id
              ) 
      SELECT /*+ NO_MERGE */ s.tax_pr_form_id,
          s.tax_pr_summary_id,
          Nvl(reconcile_view.begin_m_item, 0) beg_balance,
          Nvl(reconcile_view.m_item_additions, 0) additions,
          0 adjustments,
          Nvl(reconcile_view.m_item_transfer, 0) transfers,
          Nvl(reconcile_view.m_item_dispositions, 0) retirements,
          0 book_provision,
          0 actual_cor,
          0 actual_salvage,
          0 other,
          Nvl(reconcile_view.end_m_item, 0) end_balance
      FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form f, jurisdiction j,
           reconcile_view, (select tbs.x x, max(y.tax_book_id) tax_book_id from tax_book_schema tbs, tax_depr_view y
                            where tbs.tax_book_id = y.tax_book_id
                            group by tbs.x) z 
      WHERE f.tax_pr_form_id = a_tax_pr_form_id
        AND f.tax_pr_form_id = s.tax_pr_form_id
        AND s.description = l_description
        AND s.tax_pr_section_id = l_tax_pr_section_id
        AND s.recon_source = l_recon_source
        AND s.sort_order = 2
        AND s.jurisdiction_id = j.jurisdiction_id
        AND reconcile_view.tax_book_id = z.tax_book_id
        AND j.tax_book_id = z.x
    ) b
    ON (a.tax_pr_form_id = b.tax_pr_form_id
      AND a.tax_pr_summary_id = b.tax_pr_summary_id)
    WHEN MATCHED THEN
      UPDATE
      SET a.beg_balance = b.beg_balance,
        a.additions = b.additions,
        a.adjustments = b.adjustments,
        a.transfers = b.transfers,
        a.retirements = b.retirements,
        a.book_provision = b.book_provision,
        a.actual_cor = b.actual_cor,
        a.actual_salvage = b.actual_salvage,
        a.other = b.other,
        a.end_balance = b.end_balance
    WHEN NOT MATCHED THEN
      INSERT (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements,
          book_provision, actual_cor, actual_salvage, other, end_balance)
      VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements,
          b.book_provision, b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    
    -- Adds: Ordinary Retirements
    l_description := 'Adds: Ordinary Retirements';
    write_log(g_job_no, 0, 0, '-- Load Adds: Ordinary Retirements ID');
    MERGE INTO tax_plant_recon_form_summ_id a
    USING (
      SELECT tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source,
        tax_account_type,
        sort_order,
        subtotal
      FROM (
        SELECT a_tax_pr_form_id AS tax_pr_form_id,
            Nvl(Max(tax_pr_summary_id),0) AS max_tax_pr_summary_id,
            l_description AS description,
            l_tax_pr_section_id AS tax_pr_section_id,
            j.jurisdiction_id AS jurisdiction_id,
            l_recon_source AS recon_source,
            NULL AS tax_account_type,
            3 AS sort_order,
            0 AS subtotal
        FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j
        WHERE s.tax_pr_form_id = a_tax_pr_form_id
        AND s.tax_pr_form_id = j.tax_pr_form_id
        GROUP BY j.jurisdiction_id
      )
    ) b
    ON (a.tax_pr_form_id = b.tax_pr_form_id
      AND a.description = b.description
      AND a.tax_pr_section_id = b.tax_pr_section_id)
    WHEN NOT MATCHED THEN
      INSERT (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type,
          sort_order, subtotal)
      VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source, b.tax_account_type,
          b.sort_order, b.subtotal);
    
    -- Adds: Ordinary Retirements
    l_description := 'Adds: Ordinary Retirements';
    write_log(g_job_no, 0, 0, '-- Load Adds: Ordinary Retirements Summary');
    MERGE INTO tax_plant_recon_form_summary a
    USING (
      WITH v as (
              SELECT tax_year_version_id, tax_year
                  FROM tax_year_version
                  WHERE months <> 0
                ),
           tax_record_view as (
              select /*+ materialize */ c.company_id, c.tax_record_id, v.tax_year
                from tax_record_control c, tax_plant_recon_form f, v
               where c.version_id = g_version
                 and c.company_id = f.company_id
                 and f.tax_pr_form_id = a_tax_pr_form_id
                 and f.tax_year_version_id = v.tax_year_version_id
                 ),
           tax_depr_view as (
              SELECT /*+ materialize */ tax_depreciation.tax_book_id,
                      Sum( tax_depreciation.accum_ordinary_retires ) accum_ordinary_retires,
                      Sum( Nvl( tax_depreciation_transfer.accum_ordinary_retires, 0 ) ) accum_ord_rets_transfer,
                      Sum( tax_depreciation.accum_ordin_retires_end ) accum_ordin_retires_end
                  FROM tax_depreciation,
                      tax_record_view,
                      tax_depreciation_transfer
                  WHERE tax_depreciation.tax_record_id = tax_record_view.tax_record_id
                    AND tax_depreciation.tax_year = tax_record_view.tax_year
                    AND tax_depreciation.tax_record_id = tax_depreciation_transfer.tax_record_id (+)
                    AND tax_depreciation.tax_book_id = tax_depreciation_transfer.tax_book_id (+)
                    AND tax_depreciation.tax_year = tax_depreciation_transfer.tax_year (+)
                  GROUP BY tax_depreciation.tax_book_id
              ) 
      SELECT /*+ NO_MERGE */ s.tax_pr_form_id,
          s.tax_pr_summary_id,
          tax_depr_view.accum_ordinary_retires beg_balance,
          0 additions,
          0 adjustments,
          tax_depr_view.accum_ord_rets_transfer transfers,
          tax_depr_view.accum_ordin_retires_end - (tax_depr_view.accum_ordinary_retires + tax_depr_view.accum_ord_rets_transfer) retirements,
          0 book_provision,
          0 actual_cor,
          0 actual_salvage,
          0 other,
          tax_depr_view.accum_ordin_retires_end end_balance
      FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form f, jurisdiction j,
           tax_depr_view, (select tbs.x x, max(y.tax_book_id) tax_book_id from tax_book_schema tbs, tax_depr_view y
                             where tbs.tax_book_id = y.tax_book_id
                             group by tbs.x) z
      WHERE f.tax_pr_form_id = a_tax_pr_form_id
      AND f.tax_pr_form_id = s.tax_pr_form_id
      AND s.description = l_description
      AND s.tax_pr_section_id = l_tax_pr_section_id
      AND s.recon_source = l_recon_source
      AND s.sort_order = 3
      AND s.jurisdiction_id = j.jurisdiction_id
      AND tax_depr_view.tax_book_id = z.tax_book_id
      AND z.x = j.tax_book_id
    ) b
    ON (a.tax_pr_form_id = b.tax_pr_form_id
      AND a.tax_pr_summary_id = b.tax_pr_summary_id)
    WHEN MATCHED THEN
      UPDATE
      SET a.beg_balance = b.beg_balance,
        a.additions = b.additions,
        a.adjustments = b.adjustments,
        a.transfers = b.transfers,
        a.retirements = b.retirements,
        a.book_provision = b.book_provision,
        a.actual_cor = b.actual_cor,
        a.actual_salvage = b.actual_salvage,
        a.other = b.other,
        a.end_balance = b.end_balance
    WHEN NOT MATCHED THEN
      INSERT (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements,
          book_provision, actual_cor, actual_salvage, other, end_balance)
      VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements,
          b.book_provision, b.actual_cor, b.actual_salvage, b.other, b.end_balance);
    
    -- PowerTax JURISDICTION Tax Basis
    write_log(g_job_no, 0, 0, '-- Load PowerTax JURISDICTION Tax Basis ID');
    MERGE INTO tax_plant_recon_form_summ_id a
    USING (
      SELECT tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source,
        tax_account_type,
        sort_order,
        subtotal
      FROM (
        SELECT a_tax_pr_form_id AS tax_pr_form_id,
            Nvl(Max(tax_pr_summary_id),0) AS max_tax_pr_summary_id,
            'PowerTax '||d.description||' Tax Basis' AS description,
            l_tax_pr_section_id AS tax_pr_section_id,
            j.jurisdiction_id AS jurisdiction_id,
            l_recon_source AS recon_source,
            NULL AS tax_account_type,
            4 AS sort_order,
            0 AS subtotal
        FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction d
        WHERE s.tax_pr_form_id = a_tax_pr_form_id
        AND s.tax_pr_form_id = j.tax_pr_form_id
          AND j.jurisdiction_id = d.jurisdiction_id
        GROUP BY j.jurisdiction_id, d.description
      )
    ) b
    ON (a.tax_pr_form_id = b.tax_pr_form_id
      AND a.description = b.description
      AND a.tax_pr_section_id = b.tax_pr_section_id)
    WHEN NOT MATCHED THEN
      INSERT (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type,
          sort_order, subtotal)
      VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source, b.tax_account_type,
          b.sort_order, b.subtotal);
          
    -- PowerTax JURISDICTION Tax Basis
    write_log(g_job_no, 0, 0, '-- Load PowerTax JURISDICTION Tax Basis Summary');
    MERGE INTO tax_plant_recon_form_summary a
    USING (
      WITH v as (
              SELECT tax_year_version_id, tax_year
                  FROM tax_year_version
                  WHERE months <> 0
                ),
           tax_record_view as (
              select /*+ materialize */ c.company_id, c.tax_record_id, v.tax_year
                from tax_record_control c, tax_plant_recon_form f, v
               where c.version_id = g_version
                 and c.company_id = f.company_id
                 and f.tax_pr_form_id = a_tax_pr_form_id
                 and f.tax_year_version_id = v.tax_year_version_id
                 ),
           tax_depr_view as (
              SELECT /*+ materialize */ tax_depreciation.tax_book_id,
                      Sum( tax_depreciation.tax_balance ) tax_balance,
                      Sum( Nvl( tax_depreciation_transfer.tax_balance, 0 ) ) tax_balance_transfer,
                      Sum( tax_depreciation.tax_balance_end ) tax_balance_end,
                      Sum( tax_depreciation.additions ) additions,
                      Sum( tax_depreciation.adjustments ) adjustments,
                      Sum( tax_depreciation.retirements ) retirements,
                      Sum( tax_depreciation.extraordinary_retires ) extraordinary_retires,
                      Sum( tax_depreciation.accum_ordinary_retires ) accum_ordinary_retires,
                      Sum( Nvl( tax_depreciation_transfer.accum_ordinary_retires, 0 ) ) accum_ord_rets_transfer,
                      Sum( tax_depreciation.accum_ordin_retires_end ) accum_ordin_retires_end
                  FROM tax_depreciation,
                      tax_record_view,
                      tax_depreciation_transfer
                  WHERE tax_depreciation.tax_record_id = tax_record_view.tax_record_id
                  AND tax_depreciation.tax_year = tax_record_view.tax_year
                  AND tax_depreciation.tax_record_id = tax_depreciation_transfer.tax_record_id (+)
                  AND tax_depreciation.tax_book_id = tax_depreciation_transfer.tax_book_id (+)
                  AND tax_depreciation.tax_year = tax_depreciation_transfer.tax_year (+)
                  GROUP BY tax_depreciation.tax_book_id
              ) 
      SELECT /*+ NO_MERGE */ s.tax_pr_form_id,
          s.tax_pr_summary_id,
          tax_depr_view.tax_balance beg_balance,
          tax_depr_view.additions + tax_depr_view.adjustments additions,
          0 adjustments,
          tax_depr_view.tax_balance_transfer transfers,
          -1 * (tax_depr_view.retirements + tax_depr_view.extraordinary_retires
                      - (tax_depr_view.accum_ordin_retires_end - (tax_depr_view.accum_ordinary_retires + tax_depr_view.accum_ord_rets_transfer))) retirements,
          0 book_provision,
          0 actual_cor,
          0 actual_salvage,
          0 other,
          tax_depr_view.tax_balance_end end_balance
      FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form f, jurisdiction j,
           tax_depr_view, (select tbs.x x, max(y.tax_book_id) tax_book_id from tax_book_schema tbs, tax_depr_view y
                             where tbs.tax_book_id = y.tax_book_id
                             group by tbs.x) z
      WHERE f.tax_pr_form_id = a_tax_pr_form_id
      AND f.tax_pr_form_id = s.tax_pr_form_id
      AND s.description = 'PowerTax '||j.description||' Tax Basis'
      AND s.tax_pr_section_id = l_tax_pr_section_id
      AND s.recon_source = l_recon_source
      AND s.sort_order = 4
      AND s.jurisdiction_id = j.jurisdiction_id
      AND tax_depr_view.tax_book_id = z.tax_book_id
      AND z.x = j.tax_book_id
    ) b
    ON (a.tax_pr_form_id = b.tax_pr_form_id
      AND a.tax_pr_summary_id = b.tax_pr_summary_id)
    WHEN MATCHED THEN
      UPDATE
      SET a.beg_balance = b.beg_balance,
        a.additions = b.additions,
        a.adjustments = b.adjustments,
        a.transfers = b.transfers,
        a.retirements = b.retirements,
        a.book_provision = b.book_provision,
        a.actual_cor = b.actual_cor,
        a.actual_salvage = b.actual_salvage,
        a.other = b.other,
        a.end_balance = b.end_balance
    WHEN NOT MATCHED THEN
      INSERT (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements,
          book_provision, actual_cor, actual_salvage, other, end_balance)
      VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements,
          b.book_provision, b.actual_cor, b.actual_salvage, b.other, b.end_balance);

		RETURN 0;
	EXCEPTION
		WHEN OTHERS THEN
    		write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_LOAD_TAX_BOOK_BASIS_SUMMARY. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
    		RETURN -1;
	END f_load_tax_book_basis_summary;

  --=============================================================================
	-- Function F_LOAD_TAX_TEMP_DIFF
	--
	-- Description: Populates 
	--=============================================================================
	FUNCTION f_load_tax_temp_diff(a_tax_pr_form_id NUMBER)
		 RETURN NUMBER
	IS
		l_description_prefix	tax_plant_recon_form_summ_id.description%TYPE := 'PowerTax Temp Diff - ';
		l_tax_pr_section_id 	tax_plant_recon_form_summ_id.tax_pr_section_id%TYPE := 6;
		l_recon_source			tax_plant_recon_form_summ_id.recon_source%TYPE := 2;
	BEGIN
    write_log(g_job_no, 0, 0, '-- Load PowerTax Temp Difference ID');
		MERGE INTO tax_plant_recon_form_summ_id a
		USING (
			SELECT tax_pr_form_id,
				max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
				description,
				tax_pr_section_id,
				jurisdiction_id,
				recon_source,
				tax_account_type,
				sort_order,
				subtotal
			FROM (
				SELECT a_tax_pr_form_id AS tax_pr_form_id,
						Nvl(Max(tax_pr_summary_id),0) AS max_tax_pr_summary_id,
						l_description_prefix || d.description AS description,
						l_tax_pr_section_id AS tax_pr_section_id,
						j.jurisdiction_id AS jurisdiction_id,
						l_recon_source AS recon_source,
						NULL AS tax_account_type,
						8 AS sort_order,
						0 AS subtotal
				FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction d
				WHERE s.tax_pr_form_id = a_tax_pr_form_id
				AND s.tax_pr_form_id = j.tax_pr_form_id
				AND j.jurisdiction_id = d.jurisdiction_id
				GROUP BY j.jurisdiction_id, d.description
			)
		) b
		ON (a.tax_pr_form_id = b.tax_pr_form_id
			AND a.description = b.description
			AND a.tax_pr_section_id = b.tax_pr_section_id)
		WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type,
   				sort_order, subtotal)
			VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source, b.tax_account_type,
   				b.sort_order, b.subtotal);

		write_log(g_job_no, 0, 0, '-- Load PowerTax Temp Difference Summary');
    MERGE INTO tax_plant_recon_form_summary a
		USING (
      WITH v as (
              SELECT tax_year_version_id, tax_year
                  FROM tax_year_version
                  WHERE months <> 0
                 ),
          tax_record_view as (
              select /*+ materialize */ c.company_id, c.tax_record_id, v.tax_year
                from tax_record_control c, tax_plant_recon_form f, v
               where c.version_id = g_version
                 and c.company_id = f.company_id
                 and f.tax_pr_form_id = a_tax_pr_form_id
                 and f.tax_year_version_id = v.tax_year_version_id
                 ),
          dit_view as (
               select /*+ materialize */ n.jurisdiction_id,
                      sum(dit.norm_diff_balance_beg) beg_balance,
                      sum(dit.norm_diff_balance_end) end_balance
                 from deferred_income_tax dit, normalization_schema n, tax_record_view r
                where dit.tax_record_id = r.tax_record_id
                  and dit.tax_year = r.tax_year
                  and dit.normalization_id = n.normalization_id
                group by n.jurisdiction_id
                      )
			SELECT /*+ NO_MERGE */ s.tax_pr_form_id,
    			s.tax_pr_summary_id,
    			dit_view.beg_balance beg_balance,
    			dit_view.end_balance end_balance
			FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form f, jurisdiction j,
    			 dit_view
			WHERE f.tax_pr_form_id = a_tax_pr_form_id
			AND f.tax_pr_form_id = s.tax_pr_form_id
			AND s.description = l_description_prefix || j.description
			AND s.tax_pr_section_id = l_tax_pr_section_id
			AND s.recon_source = l_recon_source
			AND s.jurisdiction_id = j.jurisdiction_id
			AND dit_view.jurisdiction_id = j.jurisdiction_id
		) b
		ON (a.tax_pr_form_id = b.tax_pr_form_id
			AND a.tax_pr_summary_id = b.tax_pr_summary_id)
		WHEN MATCHED THEN
			UPDATE
			SET a.beg_balance = b.beg_balance,
				a.end_balance = b.end_balance
		WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, tax_pr_summary_id, beg_balance, end_balance, additions, adjustments, transfers, retirements, book_provision, actual_cor, actual_salvage, other)
			VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.end_balance, 0, 0, 0, 0, 0, 0, 0, 0);

		RETURN 0;
	EXCEPTION
		WHEN OTHERS THEN
    		write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_LOAD_TAX_TEMP_DIFF. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
    		RETURN -1;
	END f_load_tax_temp_diff;

  --=============================================================================
	-- Function F_LOAD_INPUT_ADJ_DATA
	--
	-- Description: Populates 
	--=============================================================================
  FUNCTION f_load_input_adj_data(a_tax_pr_form_id NUMBER)
    RETURN NUMBER
  IS

  BEGIN
    write_log(g_job_no, 0, 0, 'Loading input adj data');
    MERGE INTO tax_plant_recon_form_summ_id a
    USING (
      select a_tax_pr_form_id tax_pr_form_id,
           nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
           'Total GL to CPR Asset Recon Adj' description,
           3 tax_pr_section_id,
           null jurisdiction,
           1 recon_source,
           1 tax_account_type,
           2 sort_order,
           1 subtotal
        from tax_plant_recon_form_summ_id
      where tax_pr_form_id = a_tax_pr_form_id
      union
      select a_tax_pr_form_id tax_pr_form_id,
            nvl(max(tax_pr_summary_id),0) + 2 tax_pr_summary_id,
            'Total GL to CPR Depr Recon Adj' description,
            5 tax_pr_section_id,
            null jurisdiction,
            1 recon_source,
            2 tax_account_type,
            2 sort_order,
            1 subtotal
        from tax_plant_recon_form_summ_id
      where tax_pr_form_id = a_tax_pr_form_id
      union
      select a_tax_pr_form_id tax_pr_form_id,
            nvl(max(tax_pr_summary_id),0) + 3 tax_pr_summary_id,
            'Federal CPR to PowerTax Recon Adj' description,
            3 tax_pr_section_id,
            null jurisdiction,
            2 recon_source,
            1 tax_account_type,
            2 sort_order,
            1 subtotal
        from tax_plant_recon_form_summ_id
      where tax_pr_form_id = a_tax_pr_form_id
      union
      select a_tax_pr_form_id tax_pr_form_id,
            nvl(max(tax_pr_summary_id),0) + 4 tax_pr_summary_id,
            'Federal CPR Depr to PwrTax Recon Adj' description,
            5 tax_pr_section_id,
            null jurisdiction,
            2 recon_source,
            2 tax_account_type,
            2 sort_order,
            1 subtotal
        from tax_plant_recon_form_summ_id
      where tax_pr_form_id = a_tax_pr_form_id
      union
      select a_tax_pr_form_id tax_pr_form_id,
            nvl(max(tax_pr_summary_id),0) + 5 tax_pr_summary_id,
            'Total Accumulated Deferred Income Tax' description,
            7 tax_pr_section_id,
            null jurisdiction,
            2 recon_source,
            null tax_account_type,
            2 sort_order,
            1 subtotal
        from tax_plant_recon_form_summ_id
      where tax_pr_form_id = a_tax_pr_form_id
      ) b
      ON (a.tax_pr_form_id = b.tax_pr_form_id
        AND a.description = b.description
        and a.tax_pr_section_id = b.tax_pr_section_id)
      WHEN NOT MATCHED THEN
			INSERT (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type,
   				sort_order, subtotal)
			VALUES (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
   				b.sort_order, b.subtotal);

    MERGE into tax_plant_recon_form_summary a
    USING (
      select amt.tax_pr_form_id tax_pr_form_id,
           id.tax_pr_summary_id tax_pr_summary_id,
           sum(amt.beg_balance) beg_balance,
           sum(amt.additions) additions,
           sum(amt.adjustments) adjustments,
           sum(amt.transfers) transfers,
           sum(amt.retirements) retirements,
           0 book_provision,
           0 actual_cor,
           0 actual_salvage,
           0 other,
           sum(amt.end_balance) end_balance
       from tax_plant_recon_adj adj, tax_plant_recon_form_adj amt, tax_plant_recon_form_summ_id id, jurisdiction j
      where amt.tax_pr_form_id = a_tax_pr_form_id
       and amt.tax_pr_form_id = id.tax_pr_form_id
       and amt.tax_pr_adj_id = adj.tax_pr_adj_id
       and adj.tax_pr_section_id = id.tax_pr_section_id
       and amt.jurisdiction_id = j.jurisdiction_id (+)
       and id.tax_pr_section_id = 3
       and adj.recon_source = id.recon_source
       and id.tax_account_type = 1
       and id.sort_order = 2
       and (lower(j.description) = 'federal'
	    or j.description is null)
      group by amt.tax_pr_form_id, id.tax_pr_summary_id
    union
    select amt.tax_pr_form_id tax_pr_form_id,
           id.tax_pr_summary_id tax_pr_summary_id,
           sum(amt.beg_balance) beg_balance,
           0 additions,
           0 adjustments,
           0 transfers,
           sum(amt.retirements) retirements,
           sum(amt.book_provision) book_provision,
           sum(amt.actual_cor) actual_cor,
           sum(amt.actual_salvage) actual_salvage,
           sum(amt.other) other,
           sum(amt.end_balance) end_balance
      from tax_plant_recon_adj adj, tax_plant_recon_form_adj amt, tax_plant_recon_form_summ_id id, jurisdiction j
      where amt.tax_pr_form_id = a_tax_pr_form_id
       and amt.tax_pr_form_id = id.tax_pr_form_id
       and amt.tax_pr_adj_id = adj.tax_pr_adj_id
       and adj.tax_pr_section_id = id.tax_pr_section_id
       and amt.jurisdiction_id = j.jurisdiction_id (+)
       and id.tax_pr_section_id = 5
       and adj.recon_source = id.recon_source
       and id.tax_account_type = 2
       and id.sort_order = 2
       and (lower(j.description) = 'federal'
	    or j.description is null)
      group by amt.tax_pr_form_id, id.tax_pr_summary_id
    union
    select amt.tax_pr_form_id tax_pr_form_id,
           id.tax_pr_summary_id tax_pr_summary_id,
           sum(amt.beg_balance) beg_balance,
           0 additions,
           0 adjustments,
           0 transfers,
           0 retirements,
           0 book_provision,
           0 actual_cor,
           0 actual_salvage,
           sum(amt.other) other,
           sum(amt.end_balance) end_balance
      from tax_plant_recon_adj adj, tax_plant_recon_form_adj amt, tax_plant_recon_form_summ_id id
      where amt.tax_pr_form_id = a_tax_pr_form_id
       and amt.tax_pr_form_id = id.tax_pr_form_id
       and amt.tax_pr_adj_id = adj.tax_pr_adj_id
       and adj.tax_pr_section_id = id.tax_pr_section_id
       and id.tax_pr_section_id = 7
       and adj.recon_source = id.recon_source
       and nvl(id.tax_account_type,2) = 2
       and id.sort_order = 2
     group by amt.tax_pr_form_id, id.tax_pr_summary_id) b
     on (a.tax_pr_form_id = b.tax_pr_form_id
        	and a.tax_pr_summary_id = b.tax_pr_summary_id)
     when matched then
       update
       set a.beg_balance = b.beg_balance,
       a.additions = b.additions,
       a.adjustments = b.adjustments,
       a.transfers = b.transfers,
       a.retirements = b.retirements,
       a.book_provision = b.book_provision,
       a.actual_cor = b.actual_cor,
       a.actual_salvage = b.actual_salvage,
       a.other = b.other,
       a.end_balance = b.end_balance
     when not matched then
       insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements,
              book_provision, actual_cor, actual_salvage, other, end_balance)
       values( b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements,
               b.book_provision, b.actual_cor, b.actual_salvage, b.other, b.end_balance);

      --initialize rows in the summary table to 0 if no GL accounts or adjustments have been added yet
    write_log(g_job_no, 0, 0, 'Initializing rows for blank manual sections');
    insert into tax_plant_recon_form_summary
    (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
    select a.tax_pr_form_id, a.tax_pr_summary_id, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    from ( SELECT a_tax_pr_form_id tax_pr_form_id, tax_pr_summary_id
             FROM tax_plant_recon_form_summ_id
             WHERE tax_pr_form_id = a_tax_pr_form_id
               MINUS
             SELECT a_tax_pr_form_id tax_pr_form_id, tax_pr_summary_id
             FROM tax_plant_recon_form_summary
             WHERE tax_pr_form_id = a_tax_pr_form_id
           ) a;
    write_log(g_job_no, 0, 0, 'Number of rows initialized: ' || to_char(sql%rowcount));
  return 0;

  EXCEPTION
      WHEN OTHERS THEN
        write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_LOAD_INPUT_ADJ_DATA. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
        RETURN -1;
  END f_load_input_adj_data;

  --=============================================================================
	-- Function F_LOAD_INPUT_GL_DATA
	--
	-- Description: Populates 
	--=============================================================================
  function f_load_input_gl_data(a_tax_pr_form_id number)
    return number
  is

  begin
  write_log(g_job_no, 0, 0, 'Loading input GL data');
    merge into tax_plant_recon_form_summ_id a
    using(
    	 select a_tax_pr_form_id tax_pr_form_id,
             nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
             'Asset Related GL Account Balance' description,
             3 tax_pr_section_id,
             null jurisdiction,
             1 recon_source,
             1 tax_account_type,
             4 sort_order,
             1 subtotal
        from tax_plant_recon_form_summ_id
       where tax_pr_form_id = a_tax_pr_form_id
      union
      select a_tax_pr_form_id tax_pr_form_id,
             nvl(max(tax_pr_summary_id),0) + 2 tax_pr_summary_id,
             'Depr Related GL Account Balance' description,
             5 tax_pr_section_id,
             null jurisdiction,
             1 recon_source,
             2 tax_account_type,
             4 sort_order,
             1 subtotal
        from tax_plant_recon_form_summ_id
       where tax_pr_form_id = a_tax_pr_form_id
      union
       select a_tax_pr_form_id tax_pr_form_id,
             nvl(max(tax_pr_summary_id),0) + 3 tax_pr_summary_id,
             'Total GL Asset Balance' description,
             7 tax_pr_section_id,
             null jurisdiction,
             1 recon_source,
             1 tax_account_type,
             10 sort_order,
             1 subtotal
        from tax_plant_recon_form_summ_id
       where tax_pr_form_id = a_tax_pr_form_id) b
    ON (a.tax_pr_form_id = b.tax_pr_form_id
        AND a.description = b.description
        and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
    	select gl.tax_pr_form_id tax_pr_form_id,
       id.tax_pr_summary_id tax_pr_summary_id,
       sum(gl.beg_balance) beg_balance,
       0 additions,
       0 adjustments,
       0 transfers,
       0 retirements,
       0 book_provision,
       0 actual_cor,
       0 actual_salvage,
       0 other,
       sum(gl.end_balance) end_balance
      from tax_plant_recon_gl_account acct, tax_plant_recon_form_gl_acct gl, tax_plant_recon_form_summ_id id
      where gl.tax_pr_form_id = a_tax_pr_form_id
       and gl.tax_pr_form_id = id.tax_pr_form_id
       and gl.tax_pr_gl_account_id = acct.tax_pr_gl_account_id
       and acct.tax_account_type = id.tax_account_type
       and id.tax_pr_section_id in (3,5,7)
       and id.recon_source = 1
       and id.sort_order in (4,10)
      group by gl.tax_pr_form_id, id.tax_pr_summary_id ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    return 0;
    exception
      when others then
        write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_LOAD_INPUT_GL_DATA. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
        return -1;
  end f_load_input_gl_data;

  --=============================================================================
	-- Function F_CALC_SUBTOTALS
	--
	-- Description: Populates 
	--=============================================================================
  function f_calc_subtotals(a_tax_pr_form_id number)
    return number
  is
  begin
    write_log(g_job_no, 0, 0, 'Calculating subtotals');
    --PPE Reconciled to the GL
    write_log(g_job_no, 0, 0, 'Depr Related GL Account Balance');
    merge into tax_plant_recon_form_summ_id a
    using (
    	select a_tax_pr_form_id tax_pr_form_id,
       nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
       'Reconciled GL Book Balance' description,
       3 tax_pr_section_id,
       null jurisdiction,
       1 recon_source,
       1 tax_account_type,
       3 sort_order,
       1 subtotal
     from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id
      union
     select a_tax_pr_form_id tax_pr_form_id,
       nvl(max(tax_pr_summary_id),0) + 2 tax_pr_summary_id,
       'Reconciled GL Reserve Balance' description,
       5 tax_pr_section_id,
       null jurisdiction,
       1 recon_source,
       2 tax_account_type,
       3 sort_order,
       1 subtotal
     from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id) b
    ON (a.tax_pr_form_id = b.tax_pr_form_id
        AND a.description = b.description
        and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(s.beg_balance) beg_balance,
       sum(s.additions) additions,
       sum(s.adjustments) adjustments,
       sum(s.transfers) transfers,
       sum(s.retirements) retirements,
       sum(s.book_provision) book_provision,
       sum(s.actual_cor) actual_cor,
       sum(s.actual_salvage) actual_salvage,
       sum(s.other) other,
       sum(s.end_balance) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id in (3, 5)
       and id.recon_source in (1, 3)
       and id.sort_order in (1, 2)
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = id.tax_pr_section_id
       and sub_id.recon_source = 1
       and sub_id.tax_account_type = id.tax_account_type
       and sub_id.sort_order = 3
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
     on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
     when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --Total Asset Balance
    write_log(g_job_no, 0, 0, 'Reconciled CPR Book Balance');
    merge into tax_plant_recon_form_summ_id a
    using (
     select a_tax_pr_form_id tax_pr_form_id,
       nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
       'Reconciled CPR Book Balance' description,
       3 tax_pr_section_id,
       null jurisdiction,
       2 recon_source,
       1 tax_account_type,
       3 sort_order,
       1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(s.beg_balance) beg_balance,
       sum(s.additions) additions,
       sum(s.adjustments) adjustments,
       sum(s.transfers) transfers,
       sum(s.retirements) retirements,
       sum(s.book_provision) book_provision,
       sum(s.actual_cor) actual_cor,
       sum(s.actual_salvage) actual_salvage,
       sum(s.other) other,
       sum(s.end_balance) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 3
       and id.recon_source in (2, 3)
       and id.sort_order in (1, 2)
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = id.tax_pr_section_id
       and sub_id.recon_source = 2
       and sub_id.tax_account_type = id.tax_account_type
       and sub_id.sort_order = 3
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --Total Reserve Balance
    write_log(g_job_no, 0, 0, 'Reconciled CPR Reserve Balance');
    merge into tax_plant_recon_form_summ_id a
    using (
     select a_tax_pr_form_id tax_pr_form_id,
       nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
       'Reconciled CPR Reserve Balance' description,
       5 tax_pr_section_id,
       null jurisdiction,
       2 recon_source,
       2 tax_account_type,
       3 sort_order,
       1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(s.beg_balance) beg_balance,
       sum(s.additions) additions,
       sum(s.adjustments) adjustments,
       sum(s.transfers) transfers,
       sum(s.retirements) retirements,
       sum(s.book_provision) book_provision,
       sum(s.actual_cor) actual_cor,
       sum(s.actual_salvage) actual_salvage,
       sum(s.other) other,
       sum(s.end_balance) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 5
       and id.recon_source in (2, 3)
       and id.sort_order in (1, 2)
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = id.tax_pr_section_id
       and sub_id.recon_source = 2
       and sub_id.tax_account_type = id.tax_account_type
       and sub_id.sort_order = 3
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --Difference (PPE Recon)
    write_log(g_job_no, 0, 0, 'Difference (PPE Recon)');
    merge into tax_plant_recon_form_summ_id a
    using (
     select a_tax_pr_form_id tax_pr_form_id,
           nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
           'Difference' description,
           3 tax_pr_section_id,
           null jurisdiction,
           1 recon_source,
           1 tax_account_type,
           5 sort_order,
           1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id
     union
     select a_tax_pr_form_id tax_pr_form_id,
           nvl(max(tax_pr_summary_id),0) + 2 tax_pr_summary_id,
           'Difference' description,
           3 tax_pr_section_id,
           null jurisdiction,
           2 recon_source,
           1 tax_account_type,
           5 sort_order,
           1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id
     union
     select a_tax_pr_form_id tax_pr_form_id,
           nvl(max(tax_pr_summary_id),0) + 3 tax_pr_summary_id,
           'Difference' description,
           5 tax_pr_section_id,
           null jurisdiction,
           1 recon_source,
           2 tax_account_type,
           5 sort_order,
           1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id
     union
     select a_tax_pr_form_id tax_pr_form_id,
           nvl(max(tax_pr_summary_id),0) + 4 tax_pr_summary_id,
           'Difference' description,
           5 tax_pr_section_id,
           null jurisdiction,
           2 recon_source,
           2 tax_account_type,
           5 sort_order,
           1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);

     merge into tax_plant_recon_form_summary a
     using (
      select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(decode(id.sort_order, 4, -1 * s.beg_balance, s.beg_balance)) beg_balance,
       sum(decode(id.sort_order, 4, -1 * s.additions, s.additions)) additions,
       sum(decode(id.sort_order, 4, -1 * s.adjustments, s.adjustments)) adjustments,
       sum(decode(id.sort_order, 4, -1 * s.transfers, s.transfers)) transfers,
       sum(decode(id.sort_order, 4, -1 * s.retirements, s.retirements)) retirements,
       sum(decode(id.sort_order, 4, -1 * s.book_provision, s.book_provision)) book_provision,
       sum(decode(id.sort_order, 4, -1 * s.actual_cor, s.actual_cor)) actual_cor,
       sum(decode(id.sort_order, 4, -1 * s.actual_salvage, s.actual_salvage)) actual_salvage,
       sum(decode(id.sort_order, 4, -1 * s.other, s.other)) other,
       sum(decode(id.sort_order, 4, -1 * s.end_balance, s.end_balance)) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id in (3,5)
       and id.recon_source in (1, 2)
       and id.sort_order in (3, 4)
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = id.tax_pr_section_id
       and sub_id.recon_source = id.recon_source
       and sub_id.tax_account_type = id.tax_account_type
       and sub_id.sort_order = 5
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --JURISDICTION Tax Balance
    write_log(g_job_no, 0, 0, 'JURISDICTION Tax Balance');
    merge into tax_plant_recon_form_summ_id a
    using (
     /*select a_tax_pr_form_id tax_pr_form_id,
         nvl(max(tax_pr_summary_id),0) + ROWNUM tax_pr_summary_id,
         jur.description||' Tax Balance' description,
         6 tax_pr_section_id,
         j.jurisdiction_id jurisdiction_id,
         2 recon_source_id,
         null tax_account_type,
         1 sort_order,
         1 subtotal
      from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
     where s.tax_pr_form_id = a_tax_pr_form_id
        and s.tax_pr_form_id = j.tax_pr_form_id
        and jur.jurisdiction_id = j.jurisdiction_id
        group by jur.description||' Tax Balance', j.jurisdiction_id*/
        SELECT tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source,
        tax_account_type,
        sort_order,
        subtotal
        FROM (
          SELECT a_tax_pr_form_id AS tax_pr_form_id,
              Nvl(Max(tax_pr_summary_id),0) AS max_tax_pr_summary_id,
              'PowerTax '||jur.description||' Tax Basis' AS description,
              6 AS tax_pr_section_id,
              j.jurisdiction_id AS jurisdiction_id,
              2 AS recon_source,
              NULL AS tax_account_type,
              1 AS sort_order,
              1 AS subtotal
          FROM tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
          WHERE s.tax_pr_form_id = a_tax_pr_form_id
          AND s.tax_pr_form_id = j.tax_pr_form_id
          and j.jurisdiction_id = jur.jurisdiction_id
          GROUP BY 'PowerTax '||jur.description||' Tax Basis',j.jurisdiction_id
        )
      ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(s.beg_balance) beg_balance,
       0 additions,
       0 adjustments,
       0 transfers,
       0 retirements,
       0 book_provision,
       0 actual_cor,
       0 actual_salvage,
       0 other,
       sum(s.end_balance) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 4
       and id.recon_source = 2
       and id.sort_order = 4
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and id.jurisdiction_id = sub_id.jurisdiction_id
       and sub_id.tax_pr_section_id = 6
       and sub_id.sort_order = 1
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --JURISDICTION Tax Reserve
    write_log(g_job_no, 0, 0, 'JURISDICTION Tax Reserve');
    merge into tax_plant_recon_form_summ_id a
    using (
    /* select a_tax_pr_form_id tax_pr_form_id,
         max(nvl(s.tax_pr_summary_id,0)) + 1 tax_pr_summary_id,
         jur.description||' Tax Reserve' description,
         6 tax_pr_section_id,
         j.jurisdiction_id jurisdiction_id,
         2 recon_source_id,
         null tax_account_type,
         2 sort_order,
         1 subtotal
      from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
     where s.tax_pr_form_id = a_tax_pr_form_id
        and s.tax_pr_form_id = j.tax_pr_form_id
        and jur.jurisdiction_id = j.jurisdiction_id
        group by jur.description||' Tax Reserve', j.jurisdiction_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);*/
    select tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source_id,
        tax_account_type,
        sort_order,
        subtotal
        from (
          select a_tax_pr_form_id tax_pr_form_id,
           max(nvl(s.tax_pr_summary_id,0)) max_tax_pr_summary_id,
           'PowerTax '||jur.description||' Tax Reserve' description,
           6 tax_pr_section_id,
           j.jurisdiction_id jurisdiction_id,
           2 recon_source_id,
           null tax_account_type,
           2 sort_order,
           1 subtotal
        from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
       where s.tax_pr_form_id = a_tax_pr_form_id
          and s.tax_pr_form_id = j.tax_pr_form_id
          and jur.jurisdiction_id = j.jurisdiction_id
          group by 'PowerTax '||jur.description||' Tax Reserve', j.jurisdiction_id
        )
      ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(s.beg_balance) beg_balance,
       0 additions,
       0 adjustments,
       0 transfers,
       0 retirements,
       0 book_provision,
       0 actual_cor,
       0 actual_salvage,
       0 other,
       sum(s.end_balance) end_balance
      from tax_plant_recon_form_res_summ s, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and s.jurisdiction_id = sub_id.jurisdiction_id
       and sub_id.tax_pr_section_id = 6
       and sub_id.sort_order = 2
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --JURISDICTION Net Tax Basis
    write_log(g_job_no, 0, 0, 'JURISDICTION Net Tax Basis');
    merge into tax_plant_recon_form_summ_id a
    using (
     /*select a_tax_pr_form_id tax_pr_form_id,
         max(nvl(s.tax_pr_summary_id,0)) + 1 tax_pr_summary_id,
         jur.description||' Net Tax Basis' description,
         6 tax_pr_section_id,
         j.jurisdiction_id jurisdiction_id,
         2 recon_source_id,
         null tax_account_type,
         3 sort_order,
         1 subtotal
      from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
     where s.tax_pr_form_id = a_tax_pr_form_id
        and s.tax_pr_form_id = j.tax_pr_form_id
        and jur.jurisdiction_id = j.jurisdiction_id
        group by jur.description||' Net Tax Basis', j.jurisdiction_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);*/
     select tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source_id,
        tax_account_type,
        sort_order,
        subtotal
        from (
          select a_tax_pr_form_id tax_pr_form_id,
             max(nvl(s.tax_pr_summary_id,0)) max_tax_pr_summary_id,
             jur.description||' Net Tax Basis' description,
             6 tax_pr_section_id,
             j.jurisdiction_id jurisdiction_id,
             2 recon_source_id,
             null tax_account_type,
             3 sort_order,
             1 subtotal
        from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
       where s.tax_pr_form_id = a_tax_pr_form_id
          and s.tax_pr_form_id = j.tax_pr_form_id
          and jur.jurisdiction_id = j.jurisdiction_id
          group by jur.description||' Net Tax Basis', j.jurisdiction_id
        )
      ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(decode(id.sort_order, 2, -1 * s.beg_balance, s.beg_balance)) beg_balance,
       sum(decode(id.sort_order, 2, -1 * s.additions, s.additions)) additions,
       sum(decode(id.sort_order, 2, -1 * s.adjustments, s.adjustments)) adjustments,
       sum(decode(id.sort_order, 2, -1 * s.transfers, s.transfers)) transfers,
       sum(decode(id.sort_order, 2, -1 * s.retirements, s.retirements)) retirements,
       sum(decode(id.sort_order, 2, -1 * s.book_provision, s.book_provision)) book_provision,
       sum(decode(id.sort_order, 2, -1 * s.actual_cor, s.actual_cor)) actual_cor,
       sum(decode(id.sort_order, 2, -1 * s.actual_salvage, s.actual_salvage)) actual_salvage,
       sum(decode(id.sort_order, 2, -1 * s.other, s.other)) other,
       sum(decode(id.sort_order, 2, -1 * s.end_balance, s.end_balance)) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 6
       and id.sort_order in (1, 2)
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = id.tax_pr_section_id
       and sub_id.jurisdiction_id = id.jurisdiction_id
       and sub_id.sort_order = 3
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --JURISDICTION Book Balance
    write_log(g_job_no, 0, 0, 'JURISDICTION Book Balance');
    merge into tax_plant_recon_form_summ_id a
    using (
     /*select a_tax_pr_form_id tax_pr_form_id,
         max(nvl(s.tax_pr_summary_id,0)) + 1 tax_pr_summary_id,
         jur.description||' Book Balance' description,
         6 tax_pr_section_id,
         j.jurisdiction_id jurisdiction_id,
         2 recon_source_id,
         null tax_account_type,
         4 sort_order,
         1 subtotal
      from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
     where s.tax_pr_form_id = a_tax_pr_form_id
        and s.tax_pr_form_id = j.tax_pr_form_id
        and jur.jurisdiction_id = j.jurisdiction_id
        group by jur.description||' Book Balance', j.jurisdiction_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);*/
     select tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source_id,
        tax_account_type,
        sort_order,
        subtotal
        from (
          select a_tax_pr_form_id tax_pr_form_id,
             max(nvl(s.tax_pr_summary_id,0)) max_tax_pr_summary_id,
             --jur.description||' Book Balance' description,
			 jur.description||' Reconciled CPR Book Balance' description,
             6 tax_pr_section_id,
             j.jurisdiction_id jurisdiction_id,
             2 recon_source_id,
             null tax_account_type,
             4 sort_order,
             1 subtotal
        from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
       where s.tax_pr_form_id = a_tax_pr_form_id
          and s.tax_pr_form_id = j.tax_pr_form_id
          and jur.jurisdiction_id = j.jurisdiction_id
          group by jur.description||' Reconciled CPR Book Balance', j.jurisdiction_id
        )
      ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       (s.beg_balance + nvl(adjs.beg_balance,0)) beg_balance,
       (s.additions + nvl(adjs.additions,0)) additions,
       (s.adjustments + nvl(adjs.adjustments,0)) adjustments,
       (s.transfers + nvl(adjs.transfers,0)) transfers,
       (s.retirements + nvl(adjs.retirements,0)) retirements,
       0 book_provision,
       0 actual_cor,
       0 actual_salvage,
       0 other,
       ( s.end_balance + nvl(adjs.end_balance,0)) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id,
      ( select jurisdiction_id,
			sum(beg_balance) beg_balance,
			sum(additions) additions,
			sum(adjustments) adjustments,
			sum(transfers) transfers,
			sum(retirements) retirements,
			sum(end_balance) end_balance
		 from tax_plant_recon_form_adj a, tax_plant_recon_adj adj
        where a.tax_pr_form_id = a_tax_pr_form_id
              and a.tax_pr_adj_id = adj.tax_pr_adj_id
              and adj.tax_pr_section_id = 3 and recon_source = 2
          group by jurisdiction_id ) adjs
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 3
       and id.recon_source = 3
       and id.tax_account_type = 1
       and id.sort_order = 1
       and id.subtotal = 0
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = 6
       and sub_id.sort_order = 4
       and sub_id.jurisdiction_id = adjs.jurisdiction_id(+)
     /*group by s.tax_pr_form_id, sub_id.tax_pr_summary_id, s.beg_balance, s.end_balance*/) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --JURISDICTION Book Reserve
    write_log(g_job_no, 0, 0, 'JURISDICTION Book Reserve');
    merge into tax_plant_recon_form_summ_id a
    using (
     /*select a_tax_pr_form_id tax_pr_form_id,
         max(nvl(s.tax_pr_summary_id,0)) + 1 tax_pr_summary_id,
         jur.description||' Book Reserve' description,
         6 tax_pr_section_id,
         j.jurisdiction_id jurisdiction_id,
         2 recon_source_id,
         null tax_account_type,
         5 sort_order,
         1 subtotal
      from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
     where s.tax_pr_form_id = a_tax_pr_form_id
        and s.tax_pr_form_id = j.tax_pr_form_id
        and jur.jurisdiction_id = j.jurisdiction_id
        group by jur.description||' Book Reserve', j.jurisdiction_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);*/
     select tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source_id,
        tax_account_type,
        sort_order,
        subtotal
        from (
          select a_tax_pr_form_id tax_pr_form_id,
             max(nvl(s.tax_pr_summary_id,0)) max_tax_pr_summary_id,
             --jur.description||' Book Reserve' description,
			 jur.description||' Reconciled CPR Reserve Balance' description,
             6 tax_pr_section_id,
             j.jurisdiction_id jurisdiction_id,
             2 recon_source_id,
             null tax_account_type,
             5 sort_order,
             1 subtotal
        from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
       where s.tax_pr_form_id = a_tax_pr_form_id
          and s.tax_pr_form_id = j.tax_pr_form_id
          and jur.jurisdiction_id = j.jurisdiction_id
          group by jur.description||' Reconciled CPR Reserve Balance', j.jurisdiction_id
        )
      ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       (s.beg_balance + nvl(adjs.beg_balance,0)) beg_balance,
       0 additions,
       0 adjustments,
       0 transfers,
       (s.retirements + nvl(adjs.retirements,0)) retirements,
       (s.book_provision + nvl(adjs.book_provision,0)) book_provision,
       (s.actual_cor + nvl(adjs.actual_cor,0)) actual_cor,
       (s.actual_salvage + nvl(adjs.actual_salvage,0)) actual_salvage,
       (s.other + nvl(adjs.other,0)) other,
       (s.end_balance + nvl(adjs.end_balance,0)) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id,
      ( select jurisdiction_id,
			   sum(beg_balance) beg_balance,
			   sum(retirements) retirements,
			   sum(book_provision) book_provision,
			   sum(actual_cor) actual_cor,
			   sum(actual_salvage) actual_salvage,
			   sum(other) other,
			   sum(end_balance) end_balance
		 from tax_plant_recon_form_adj a, tax_plant_recon_adj adj
        where a.tax_pr_form_id = a_tax_pr_form_id
              and a.tax_pr_adj_id = adj.tax_pr_adj_id
              and adj.tax_pr_section_id = 5 and recon_source = 2
          group by jurisdiction_id ) adjs
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 5
       and id.recon_source = 3
       and id.tax_account_type = 2
       and id.sort_order = 1
       and id.subtotal = 0
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = 6
       and sub_id.sort_order = 5
       and sub_id.jurisdiction_id = adjs.jurisdiction_id(+)
    /* group by s.tax_pr_form_id, sub_id.tax_pr_summary_id, s.beg_balance, s.end_balance*/) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --JURISDICTION Net Book Basis
    write_log(g_job_no, 0, 0, 'JURISDICTION Net Book Basis');
    merge into tax_plant_recon_form_summ_id a
    using (
     /*select a_tax_pr_form_id tax_pr_form_id,
         max(nvl(s.tax_pr_summary_id,0)) + 1 tax_pr_summary_id,
         jur.description||' Net Book Basis' description,
         6 tax_pr_section_id,
         j.jurisdiction_id jurisdiction_id,
         2 recon_source_id,
         null tax_account_type,
         6 sort_order,
         1 subtotal
      from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
     where s.tax_pr_form_id = a_tax_pr_form_id
        and s.tax_pr_form_id = j.tax_pr_form_id
        and jur.jurisdiction_id = j.jurisdiction_id
        group by jur.description||' Net Book Basis', j.jurisdiction_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);*/
     select tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source_id,
        tax_account_type,
        sort_order,
        subtotal
        from (
          select a_tax_pr_form_id tax_pr_form_id,
             max(nvl(s.tax_pr_summary_id,0)) max_tax_pr_summary_id,
             jur.description||' Net Book Basis' description,
             6 tax_pr_section_id,
             j.jurisdiction_id jurisdiction_id,
             2 recon_source_id,
             null tax_account_type,
             6 sort_order,
             1 subtotal
        from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
       where s.tax_pr_form_id = a_tax_pr_form_id
          and s.tax_pr_form_id = j.tax_pr_form_id
          and jur.jurisdiction_id = j.jurisdiction_id
          group by jur.description||' Net Book Basis', j.jurisdiction_id
        )
      ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);

     merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(decode(id.sort_order, 5, -1 * s.beg_balance, s.beg_balance)) beg_balance,
       sum(decode(id.sort_order, 5, -1 * s.additions, s.additions)) additions,
       sum(decode(id.sort_order, 5, -1 * s.adjustments, s.adjustments)) adjustments,
       sum(decode(id.sort_order, 5, -1 * s.transfers, s.transfers)) transfers,
       sum(decode(id.sort_order, 5, -1 * s.retirements, s.retirements)) retirements,
       sum(decode(id.sort_order, 5, -1 * s.book_provision, s.book_provision)) book_provision,
       sum(decode(id.sort_order, 5, -1 * s.actual_cor, s.actual_cor)) actual_cor,
       sum(decode(id.sort_order, 5, -1 * s.actual_salvage, s.actual_salvage)) actual_salvage,
       sum(decode(id.sort_order, 5, -1 * s.other, s.other)) other,
       sum(decode(id.sort_order, 5, -1 * s.end_balance, s.end_balance)) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 6
       and id.sort_order in (4, 5)
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = id.tax_pr_section_id
       and sub_id.jurisdiction_id = id.jurisdiction_id
       and sub_id.sort_order = 6
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --Temp Diff - JURISDICTION
    write_log(g_job_no, 0, 0, 'Temp Diff - JURISDICTION');
    merge into tax_plant_recon_form_summ_id a
    using (
     /*select a_tax_pr_form_id tax_pr_form_id,
         max(nvl(s.tax_pr_summary_id,0)) + 1 tax_pr_summary_id,
         'Temp Diff - '||jur.description description,
         6 tax_pr_section_id,
         j.jurisdiction_id jurisdiction_id,
         2 recon_source_id,
         null tax_account_type,
         7 sort_order,
         1 subtotal
      from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
     where s.tax_pr_form_id = a_tax_pr_form_id
        and s.tax_pr_form_id = j.tax_pr_form_id
        and jur.jurisdiction_id = j.jurisdiction_id
        group by 'Temp Diff - '||jur.description, j.jurisdiction_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);*/
     select tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source_id,
        tax_account_type,
        sort_order,
        subtotal
        from (
          select a_tax_pr_form_id tax_pr_form_id,
             max(nvl(s.tax_pr_summary_id,0)) max_tax_pr_summary_id,
             'Temp Diff - '||jur.description description,
             6 tax_pr_section_id,
             j.jurisdiction_id jurisdiction_id,
             2 recon_source_id,
             null tax_account_type,
             7 sort_order,
             1 subtotal
        from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
       where s.tax_pr_form_id = a_tax_pr_form_id
          and s.tax_pr_form_id = j.tax_pr_form_id
          and jur.jurisdiction_id = j.jurisdiction_id
          group by 'Temp Diff - '||jur.description, j.jurisdiction_id
        )
      ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(decode(id.sort_order, 6, -1 * s.beg_balance, s.beg_balance)) beg_balance,
       sum(decode(id.sort_order, 6, -1 * s.additions, s.additions)) additions,
       sum(decode(id.sort_order, 6, -1 * s.adjustments, s.adjustments)) adjustments,
       sum(decode(id.sort_order, 6, -1 * s.transfers, s.transfers)) transfers,
       sum(decode(id.sort_order, 6, -1 * s.retirements, s.retirements)) retirements,
       0 book_provision,
       0 actual_cor,
       0 actual_salvage,
       0 other,
       sum(decode(id.sort_order, 6, -1 * s.end_balance, s.end_balance)) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 6
       and id.sort_order in (3, 6)
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = id.tax_pr_section_id
       and sub_id.jurisdiction_id = id.jurisdiction_id
       and sub_id.sort_order = 7
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --Difference (Net Book vs Net Tax Tie Out)
    write_log(g_job_no, 0, 0, 'Difference (Net Book vs Net Tax Tie Out)');
    merge into tax_plant_recon_form_summ_id a
    using (
     /*select a_tax_pr_form_id tax_pr_form_id,
         max(nvl(s.tax_pr_summary_id,0)) + 1 tax_pr_summary_id,
         'Difference - '||jur.description description,
         6 tax_pr_section_id,
         j.jurisdiction_id jurisdiction_id,
         2 recon_source_id,
         null tax_account_type,
         9 sort_order,
         1 subtotal
      from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
     where s.tax_pr_form_id = a_tax_pr_form_id
        and s.tax_pr_form_id = j.tax_pr_form_id
        and jur.jurisdiction_id = j.jurisdiction_id
        group by 'Difference - '||jur.description, j.jurisdiction_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);*/
     select tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source_id,
        tax_account_type,
        sort_order,
        subtotal
        from (
          select a_tax_pr_form_id tax_pr_form_id,
             max(nvl(s.tax_pr_summary_id,0)) max_tax_pr_summary_id,
             'Difference - '||jur.description description,
             6 tax_pr_section_id,
             j.jurisdiction_id jurisdiction_id,
             2 recon_source_id,
             null tax_account_type,
             9 sort_order,
             1 subtotal
        from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
       where s.tax_pr_form_id = a_tax_pr_form_id
          and s.tax_pr_form_id = j.tax_pr_form_id
          and jur.jurisdiction_id = j.jurisdiction_id
          group by 'Difference - '||jur.description, j.jurisdiction_id
        )
      ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(s.beg_balance) beg_balance,
       sum(s.additions) additions,
       sum(s.adjustments) adjustments,
       sum(s.transfers) transfers,
       sum(s.retirements) retirements,
       sum(s.book_provision) book_provision,
       sum(s.actual_cor) actual_cor,
       sum(s.actual_salvage) actual_salvage,
       sum(s.other) other,
       sum(s.end_balance) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 6
       and id.sort_order in (7, 8)
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = id.tax_pr_section_id
       and sub_id.jurisdiction_id = id.jurisdiction_id
       and sub_id.sort_order = 9
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --Temp Diff - JURISDICTION (Less Perm Related Items)
    write_log(g_job_no, 0, 0, 'Temp Diff - JURISDICTION (Less Perm Related Items)');
    merge into tax_plant_recon_form_summ_id a
    using (
     /*select a_tax_pr_form_id tax_pr_form_id,
         max(nvl(s.tax_pr_summary_id,0)) + 1 tax_pr_summary_id,
         'Temp Diff - '||jur.description||' (Less Perm Related Items)' description,
         6 tax_pr_section_id,
         j.jurisdiction_id jurisdiction_id,
         2 recon_source_id,
         null tax_account_type,
         10 sort_order,
         1 subtotal
      from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
     where s.tax_pr_form_id = a_tax_pr_form_id
        and s.tax_pr_form_id = j.tax_pr_form_id
        and jur.jurisdiction_id = j.jurisdiction_id
        group by 'Temp Diff - '||jur.description||' (Less Perm Related Items)', j.jurisdiction_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);*/
     select tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source_id,
        tax_account_type,
        sort_order,
        subtotal
        from (
          select a_tax_pr_form_id tax_pr_form_id,
             max(nvl(s.tax_pr_summary_id,0)) max_tax_pr_summary_id,
             'Temp Diff - '||jur.description||' (Less Perm Related Items)' description,
             6 tax_pr_section_id,
             j.jurisdiction_id jurisdiction_id,
             2 recon_source_id,
             null tax_account_type,
             10 sort_order,
             1 subtotal
        from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
       where s.tax_pr_form_id = a_tax_pr_form_id
          and s.tax_pr_form_id = j.tax_pr_form_id
          and jur.jurisdiction_id = j.jurisdiction_id
          group by 'Temp Diff - '||jur.description||' (Less Perm Related Items)', j.jurisdiction_id
        )
      ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(decode(id.sort_order, 9, -1 * s.beg_balance, s.beg_balance)) beg_balance,
       0 additions,
       0 adjustments,
       0 transfers,
       0 retirements,
       0 book_provision,
       0 actual_cor,
       0 actual_salvage,
       0 other,
       sum(decode(id.sort_order, 9, -1 * s.end_balance, s.end_balance)) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 6
       and id.sort_order in (7, 9)
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = id.tax_pr_section_id
       and sub_id.jurisdiction_id = id.jurisdiction_id
       and sub_id.sort_order = 10
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --JURISDICTION Requirement
    write_log(g_job_no, 0, 0, 'Jurisdiction Requirement');
    merge into tax_plant_recon_form_summ_id a
    using (
     /*select a_tax_pr_form_id tax_pr_form_id,
         max(nvl(s.tax_pr_summary_id,0)) + 1 tax_pr_summary_id,
         jur.description||' Requirement' description,
         6 tax_pr_section_id,
         j.jurisdiction_id jurisdiction_id,
         2 recon_source_id,
         null tax_account_type,
         12 sort_order,
         1 subtotal
      from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
     where s.tax_pr_form_id = a_tax_pr_form_id
        and s.tax_pr_form_id = j.tax_pr_form_id
        and jur.jurisdiction_id = j.jurisdiction_id
        group by jur.description||' Requirement', j.jurisdiction_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);*/
     select tax_pr_form_id,
        max_tax_pr_summary_id + ROWNUM AS tax_pr_summary_id,
        description,
        tax_pr_section_id,
        jurisdiction_id,
        recon_source_id,
        tax_account_type,
        sort_order,
        subtotal
        from (
          select a_tax_pr_form_id tax_pr_form_id,
             max(nvl(s.tax_pr_summary_id,0)) max_tax_pr_summary_id,
             jur.description||' Requirement' description,
             6 tax_pr_section_id,
             j.jurisdiction_id jurisdiction_id,
             2 recon_source_id,
             null tax_account_type,
             12 sort_order,
             1 subtotal
        from tax_plant_recon_form_summ_id s, tax_plant_recon_form_jur j, jurisdiction jur
       where s.tax_pr_form_id = a_tax_pr_form_id
          and s.tax_pr_form_id = j.tax_pr_form_id
          and jur.jurisdiction_id = j.jurisdiction_id
          group by jur.description||' Requirement', j.jurisdiction_id
        )
      ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction_id, b.recon_source_id, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       s.beg_balance * r.prev_statutory_rate beg_balance,
       0 additions,
       0 adjustments,
       0 transfers,
       0 retirements,
       0 book_provision,
       0 actual_cor,
       0 actual_salvage,
       0 other,
       s.end_balance * r.statutory_rate end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id,
           (
                 select v1.jurisdiction_id jurisdiction_id, v1.statutory_rate statutory_rate, nvl(v2.prev_statutory_rate, 0) prev_statutory_rate
                 from
                 (select a.jurisdiction_id, nvl(b.statutory_rate, 0) statutory_rate
                      from tax_plant_recon_form_rates a, deferred_income_tax_rates b
                      where a.def_income_tax_rate_id = b.def_income_tax_rate_id
                      and a.effective_date = b.effective_date
                      and a.tax_pr_form_id = a_tax_pr_form_id) v1,
                 (select a.jurisdiction_id, nvl(b.statutory_rate, 0) prev_statutory_rate
                      from tax_plant_recon_form_rates a, deferred_income_tax_rates b
                      where a.prev_def_income_tax_rate_id = b.def_income_tax_rate_id
                      and a.prev_effective_date = b.effective_date
                      and a.tax_pr_form_id = a_tax_pr_form_id)v2
                 where v1.jurisdiction_id = v2.jurisdiction_id(+)
           ) r
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 6
       and id.sort_order = 10
       and id.jurisdiction_id = r.jurisdiction_id
       and id.jurisdiction_id = sub_id.jurisdiction_id
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = 6
       and sub_id.sort_order = 12
     /*group by s.tax_pr_form_id, sub_id.tax_pr_summary_id, s.beg_balance, s.end_balance, r.statutory_*/) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --Total Requirement Fed and State
    write_log(g_job_no, 0, 0, 'Total Requirement Fed and State');
    merge into tax_plant_recon_form_summ_id a
    using (
     select a_tax_pr_form_id tax_pr_form_id,
       nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
       'Total Requirement Fed and State' description,
       7 tax_pr_section_id,
       null jurisdiction,
       2 recon_source,
       null tax_account_type,
       1 sort_order,
       1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);


    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(s.beg_balance) beg_balance,
       0 additions,
       0 adjustments,
       0 transfers,
       0 retirements,
       0 book_provision,
       0 actual_cor,
       0 actual_salvage,
       0 other,
       sum(s.end_balance) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 6
       and id.sort_order = 12
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = 7
       and sub_id.sort_order = 1
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);


    --Statutory vs Income Stmt Deferreds
    write_log(g_job_no, 0, 0, 'Statutory vs Income Stmt Deferreds');
    merge into tax_plant_recon_form_summ_id a
    using (
     select a_tax_pr_form_id tax_pr_form_id,
          nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
          'Statutory vs Income Stmt Deferreds' description,
          7 tax_pr_section_id,
          null jurisdiction,
          2 recon_source,
          null tax_account_type,
          3 sort_order,
          1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);


    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(decode(id.sort_order, 2, -1 * s.beg_balance, s.beg_balance)) beg_balance,
       0 additions,
       0 adjustments,
       0 transfers,
       0 retirements,
       0 book_provision,
       0 actual_cor,
       0 actual_salvage,
       0 other,
       sum(decode(id.sort_order, 2, -1 * s.end_balance, s.end_balance)) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 7
       and id.sort_order in (1, 2)
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = id.tax_pr_section_id
       and sub_id.sort_order = 3
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);


    --Regulatory Requirement - Increment
    write_log(g_job_no, 0, 0, 'Regulatory Requirement - Increment');
    merge into tax_plant_recon_form_summ_id a
    using (
     select a_tax_pr_form_id tax_pr_form_id,
          nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
          'Regulatory Requirement - Increment' description,
          7 tax_pr_section_id,
          null jurisdiction,
          2 recon_source,
          null tax_account_type,
          4 sort_order,
          1 subtotal
       from tax_plant_recon_form_summ_id
      where tax_pr_form_id = a_tax_pr_form_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);


    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
       sub_id.tax_pr_summary_id tax_pr_summary_id,
       sum(decode(id.sort_order, 2, -1 * s.beg_balance, s.beg_balance)) beg_balance,
       0 additions,
       0 adjustments,
       0 transfers,
       0 retirements,
       0 book_provision,
       0 actual_cor,
       0 actual_salvage,
       0 other,
       sum(decode(id.sort_order, 2, -1 * s.end_balance, s.end_balance)) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 7
       and id.sort_order in (1, 2)
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = id.tax_pr_section_id
       and sub_id.sort_order = 4
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);


    --Regulatory Requirement - Grossup
    write_log(g_job_no, 0, 0, 'Regulatory Requirement - Grossup');
    merge into tax_plant_recon_form_summ_id a
    using (
     select a_tax_pr_form_id tax_pr_form_id,
          nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
          'Regulatory Requirement - Grossup' description,
          7 tax_pr_section_id,
          null jurisdiction,
          2 recon_source,
          null tax_account_type,
          5 sort_order,
          1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);


    merge into tax_plant_recon_form_summary a
    using (
     WITH v as (
                      select tax_year_version_id,
                           tax_year,
                           To_Date(To_Char(Floor(tax_year))||To_Char(1),'yyyymm') beg_date,
                           to_date(to_char(floor(tax_year))||to_char(months),'yyyymm') end_date
                        from tax_year_version
                       where months <> 0
               ),
          incr as (
                  SELECT beg_balance, end_balance
                  FROM tax_plant_recon_form_summary ts, tax_plant_recon_form_summ_id id2
                  WHERE ts.tax_pr_form_id = id2.tax_pr_form_id
                  AND ts.tax_pr_summary_id = id2.tax_pr_summary_id
                  AND ts.tax_pr_form_id = a_tax_pr_form_id
                  AND id2.tax_pr_section_id = 7
                  AND id2.sort_order = 4
                     ),
         tax_record_view as (
          select /*+ materialize */ c.company_id, c.tax_record_id, v.tax_year, v.beg_date, v.end_date
            from tax_record_control c, tax_plant_recon_form f, v
           where c.version_id = g_version
             and c.company_id = f.company_id
             and f.tax_pr_form_id = a_tax_pr_form_id
             and f.tax_year_version_id = v.tax_year_version_id
             ),
          r as (
                 select /*+ materialize */ n.jurisdiction_id, 
                        max(d.def_income_tax_rate_id) def_income_tax_rate_id, 
                        max(d.effective_date) end_date, 
                        max(d.grossup_rate) grossup_rate
                  from tax_record_view a, deferred_income_tax_rates d, normalization_schema n, deferred_income_tax i
                 where d.effective_date <= a.end_date
                  and (d.def_income_tax_rate_id, d.effective_date) in (
                      select def_income_tax_rate_id, max(effective_date)
                        from deferred_income_tax_rates
                       group by def_income_tax_rate_id)
                  and d.def_income_tax_rate_id = n.def_income_tax_rate_id
                  and a.tax_record_id = i.tax_record_id
                  and n.normalization_id = i.normalization_id
                  and a.tax_year = i.tax_year
                 group by n.jurisdiction_id
            ),
          r2 as (
                 select /*+ materialize */ n.jurisdiction_id, max(d.def_income_tax_rate_id) def_income_tax_rate_id, max(d.effective_date) end_date, max(d.grossup_rate) grossup_rate
                  from tax_record_view a, deferred_income_tax_rates d, normalization_schema n, deferred_income_tax i
                 where (d.def_income_tax_rate_id, d.effective_date) in (
                      select def_income_tax_rate_id, max(effective_date)
                        from deferred_income_tax_rates d2
					              where d2.effective_date < a.beg_date
                       group by def_income_tax_rate_id)
                  and d.def_income_tax_rate_id = n.def_income_tax_rate_id
                  and a.tax_record_id = i.tax_record_id
                  and n.normalization_id = i.normalization_id
                  and a.tax_year = i.tax_year
                 group by n.jurisdiction_id
            ) 
     select /*+ NO_MERGE */ s.tax_pr_form_id tax_pr_form_id,
          id.tax_pr_summary_id tax_pr_summary_id,
          incr.beg_balance * r2.grossup_rate beg_balance,
          0 additions,
          0 adjustments,
          0 transfers,
          0 retirements,
          0 book_provision,
          0 actual_cor,
          0 actual_salvage,
          0 other,
          incr.end_balance * r.grossup_rate end_balance
       from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_jur j, jurisdiction jur,
            r,
            r2,
		        incr
      where s.tax_pr_form_id = a_tax_pr_form_id
        and s.tax_pr_form_id = id.tax_pr_form_id
        and s.tax_pr_form_id = j.tax_pr_form_id
        and j.jurisdiction_id = jur.jurisdiction_id
        and jur.tax_book_id = 10
        AND lower(trim(jur.description)) = 'federal'
        and j.jurisdiction_id = r.jurisdiction_id
        and j.jurisdiction_id = r2.jurisdiction_id
        --and s.tax_pr_summary_id = id.tax_pr_summary_id
        and id.tax_pr_section_id = 7
        and id.sort_order = 5
      group by s.tax_pr_form_id, id.tax_pr_summary_id, incr.beg_balance * r2.grossup_rate, incr.end_balance * r.grossup_rate) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);


    --Total Regulatory Account
    write_log(g_job_no, 0, 0, 'Total Regulatory Account');
    merge into tax_plant_recon_form_summ_id a
    using (
     select a_tax_pr_form_id tax_pr_form_id,
          nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
          'Total Regulatory Account' description,
          7 tax_pr_section_id,
          null jurisdiction,
          2 recon_source,
          null tax_account_type,
          6 sort_order,
          1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);


    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
         sub_id.tax_pr_summary_id tax_pr_summary_id,
         sum(s.beg_balance) beg_balance,
         0 additions,
         0 adjustments,
         0 transfers,
         0 retirements,
         0 book_provision,
         0 actual_cor,
         0 actual_salvage,
         0 other,
         sum(s.end_balance) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 7
       and id.sort_order in (4, 5)
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = id.tax_pr_section_id
       and sub_id.sort_order = 6
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);


    --Recalc - Grossup/Total Reg
    write_log(g_job_no, 0, 0, 'Recalc - Grossup/Total Reg');
    merge into tax_plant_recon_form_summ_id a
    using (
     select a_tax_pr_form_id tax_pr_form_id,
        nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
        'Recalc - Grossup/Total Reg' description,
        7 tax_pr_section_id,
        null jurisdiction,
        2 recon_source,
        null tax_account_type,
        7 sort_order,
        1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);


    merge into tax_plant_recon_form_summary a
    using (
     select sub_id.tax_pr_form_id tax_pr_form_id,
          sub_id.tax_pr_summary_id tax_pr_summary_id,
          decode(s_6.beg_balance, 0, 0, s_5.beg_balance / s_6.beg_balance) beg_balance,
          0 additions,
          0 adjustments,
          0 transfers,
          0 retirements,
          0 book_provision,
          0 actual_cor,
          0 actual_salvage,
          0 other,
          decode(s_6.end_balance, 0, 0, s_5.end_balance / s_6.end_balance) end_balance
       from tax_plant_recon_form_summary s_5, tax_plant_recon_form_summ_id id_5,
            tax_plant_recon_form_summary s_6, tax_plant_recon_form_summ_id id_6,
            tax_plant_recon_form_summ_id sub_id
      where s_5.tax_pr_form_id = a_tax_pr_form_id
        and s_5.tax_pr_form_id = id_5.tax_pr_form_id
        and s_5.tax_pr_summary_id = id_5.tax_pr_summary_id
        and id_5.tax_pr_section_id = 7
        and id_5.sort_order = 5
        and s_5.tax_pr_form_id = s_6.tax_pr_form_id
        and s_6.tax_pr_form_id = id_6.tax_pr_form_id
        and s_6.tax_pr_summary_id = id_6.tax_pr_summary_id
        and id_6.tax_pr_section_id = 7
        and id_6.sort_order = 6
        and sub_id.tax_pr_form_id = id_6.tax_pr_form_id
        and sub_id.tax_pr_section_id = id_6.tax_pr_section_id
        and sub_id.tax_pr_form_id = id_5.tax_pr_form_id
        and sub_id.tax_pr_section_id = id_5.tax_pr_section_id
        and sub_id.sort_order = 7
      /*group by sub_id.tax_pr_form_id, sub_id.tax_pr_summary_id*/) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);


    --PowerTax Book Ending
    write_log(g_job_no, 0, 0, 'PowerTax Book Ending');
    merge into tax_plant_recon_form_summ_id a
    using (
     select a_tax_pr_form_id tax_pr_form_id,
           nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
           'PowerTax Book Basis Balance' description,
           7 tax_pr_section_id,
           null jurisdiction,
           2 recon_source,
           null tax_account_type,
           9 sort_order,
           1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);


    merge into tax_plant_recon_form_summary a
    using (
     select s.tax_pr_form_id tax_pr_form_id,
         sub_id.tax_pr_summary_id tax_pr_summary_id,
         sum(s.beg_balance) beg_balance,
         0 additions,
         0 adjustments,
         0 transfers,
         0 retirements,
         0 book_provision,
         0 actual_cor,
         0 actual_salvage,
         0 other,
         sum(s.end_balance) end_balance
      from tax_plant_recon_form_summary s, tax_plant_recon_form_summ_id id, tax_plant_recon_form_summ_id sub_id
     where s.tax_pr_form_id = a_tax_pr_form_id
       and s.tax_pr_form_id = id.tax_pr_form_id
       and s.tax_pr_summary_id = id.tax_pr_summary_id
       and id.tax_pr_section_id = 3
       and id.recon_source = 2
       and id.tax_account_type = 1
       and id.sort_order = 4
       and s.tax_pr_form_id = sub_id.tax_pr_form_id
       and sub_id.tax_pr_section_id = 7
       and sub_id.sort_order = 9
	   and sub_id.subtotal = 1
     group by s.tax_pr_form_id, sub_id.tax_pr_summary_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    --Out of Balance
    write_log(g_job_no, 0, 0, 'Out of Balance');
    merge into tax_plant_recon_form_summ_id a
    using (
     select a_tax_pr_form_id tax_pr_form_id,
          nvl(max(tax_pr_summary_id),0) + 1 tax_pr_summary_id,
          'Out of Balance' description,
          7 tax_pr_section_id,
          null jurisdiction,
          2 recon_source,
          null tax_account_type,
          11 sort_order,
          1 subtotal
      from tax_plant_recon_form_summ_id
     where tax_pr_form_id = a_tax_pr_form_id) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
      and a.description = b.description
      and a.tax_pr_section_id = b.tax_pr_section_id)
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, description, tax_pr_section_id, jurisdiction_id, recon_source, tax_account_type, sort_order, subtotal)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.description, b.tax_pr_section_id, b.jurisdiction, b.recon_source, b.tax_account_type,
             b.sort_order, b.subtotal);

    merge into tax_plant_recon_form_summary a
    using (
     select a_tax_pr_form_id tax_pr_form_id,
         id.tax_pr_summary_id tax_pr_summary_id,
         0 beg_balance,
         0 additions,
         0 adjustments,
         0 transfers,
         0 retirements,
         0 book_provision,
         0 actual_cor,
         0 actual_salvage,
         0 other,
         Nvl(r.total_balance ,0) end_balance
           from tax_plant_recon_form_summ_id id,
                    (
                SELECT a_tax_pr_form_id AS tax_pr_form_id, Abs(gl.end_balance-be.end_balance) + Abs(gl.end_balance-fbb.end_balance) total_balance
                    FROM
                        (SELECT end_balance FROM tax_plant_recon_form_summary
                      WHERE tax_pr_form_id = a_tax_pr_form_id
                      AND tax_pr_summary_id = (SELECT tax_pr_summary_id FROM tax_plant_recon_form_summ_id
                                                WHERE tax_pr_form_id = a_tax_pr_form_id
                                                AND Lower(description) LIKE 'total gl asset balance') )gl,
                        (SELECT end_balance FROM tax_plant_recon_form_summary
                      WHERE tax_pr_form_id = a_tax_pr_form_id
                      AND tax_pr_summary_id = (SELECT tax_pr_summary_id FROM tax_plant_recon_form_summ_id
                                                WHERE tax_pr_form_id = a_tax_pr_form_id
                                                AND Lower(description) LIKE 'powertax book basis balance'
                                                and tax_pr_section_id = 7) ) be,
                                                --hardcoded fix to resolve issue with multiple sections using the same description
                                                --this was causing multiple rows to be returned here, which breaks the MERGE statement
                      (SELECT end_balance FROM tax_plant_recon_form_summary
                      WHERE tax_pr_form_id = a_tax_pr_form_id
                      AND tax_pr_summary_id = (SELECT tax_pr_summary_id FROM tax_plant_recon_form_summ_id
                                                WHERE tax_pr_form_id = a_tax_pr_form_id
                                                AND Lower(description) LIKE 'deferred tax book basis balance') ) fbb  )r
                WHERE id.tax_pr_form_id = r.tax_pr_form_id --(+)
                AND id.tax_pr_summary_id = (SELECT tax_pr_summary_id
                                             FROM tax_plant_recon_form_summ_id
                                             WHERE Lower(description) LIKE 'out of balance'
                                             AND tax_pr_form_id = a_tax_pr_form_id)
           group by a_tax_pr_form_id, id.tax_pr_summary_id, r.total_balance
          ) b
    on (a.tax_pr_form_id = b.tax_pr_form_id
       and a.tax_pr_summary_id = b.tax_pr_summary_id)
    when matched then
      update
      set a.beg_balance = b.beg_balance,
      a.additions = b.additions,
      a.adjustments = b.adjustments,
      a.transfers = b.transfers,
      a.retirements = b.retirements,
      a.book_provision = b.book_provision,
      a.actual_cor = b.actual_cor,
      a.actual_salvage = b.actual_salvage,
      a.other = b.other,
      a.end_balance = b.end_balance
    when not matched then
      insert (tax_pr_form_id, tax_pr_summary_id, beg_balance, additions, adjustments, transfers, retirements, book_provision,
             actual_cor, actual_salvage, other, end_balance)
      values (b.tax_pr_form_id, b.tax_pr_summary_id, b.beg_balance, b.additions, b.adjustments, b.transfers, b.retirements, b.book_provision,
             b.actual_cor, b.actual_salvage, b.other, b.end_balance);

    write_log(g_job_no, 0, 0, 'Subtotal calculation complete');
    return 0;
    exception
      when others then
        write_log(g_job_no, 4, SQLCODE, 'Error in PKG_TAX_PLANT_RECON.F_CALC_SUBTOTALS. ' || SQLERRM || ' ' || Dbms_Utility.format_error_backtrace);
        return -1;

  end f_calc_subtotals;


	-- =============================================================================
	--  Procedure SET_SESSION_PARAMETER
	-- =============================================================================
	-- procedure set_session_parameter
	-- is
		-- cursor session_param_cur is
			-- select parameter, value, type
			-- from pp_session_parameters
			-- where lower(users) = 'all'
			-- or lower(users) = 'depr';
		-- l_count pls_integer;
		-- i       pls_integer;
		-- type type_session_param_rec is table of session_param_cur%rowtype;
		-- l_session_param_rec type_session_param_rec;
		-- l_code              pls_integer;
		-- l_sql               varchar2(200);
	-- begin

	-- open session_param_cur;
	-- fetch session_param_cur bulk collect
	 -- into l_session_param_rec;
	-- close session_param_cur;

	-- l_count := l_session_param_rec.count;
	-- for i in 1 .. l_count
	-- loop
	 -- if l_session_param_rec(i).type = 'number' or l_session_param_rec(i).type = 'boolean' then
		-- l_sql := 'alter session set ' || l_session_param_rec(i).parameter || ' = ' || l_session_param_rec(i)
				-- .value;
	 -- elsif l_session_param_rec(i).type = 'string' then
		-- l_sql := 'alter session set ' || l_session_param_rec(i).parameter || ' = ''' || l_session_param_rec(i)
				-- .value || '''';
	 -- end if;
	 -- execute immediate l_sql;
	 -- write_log(g_job_no,
			   -- 0,
			   -- 0,
			   -- 'Set Session Parameter ' || l_session_param_rec(i).parameter || ' = ' || l_session_param_rec(i)
			   -- .value);
	-- end loop;
	-- return;
	-- exception
	-- when no_data_found then
	 -- return;
	-- when others then
	 -- l_code := sqlcode;
	 -- write_log(g_job_no,
			   -- 5,
			   -- l_code,
			   -- 'Set Session Parameter ' || sqlerrm(l_code) || ' ' ||
			   -- dbms_utility.format_error_backtrace);
	 -- return;
	-- end set_session_parameter;

	--=============================================================================
	-- Procedure WRITE_LOG
	--=============================================================================
	PROCEDURE write_log(a_job_no NUMBER,
			a_error_type NUMBER,
			a_code NUMBER,
			a_msg VARCHAR2)
	IS
		PRAGMA autonomous_transaction;
		l_code PLS_INTEGER;
		l_msg VARCHAR2(2000);
		l_cur_ts TIMESTAMP;
		l_date_str VARCHAR2(100);

	BEGIN
		l_cur_ts := current_timestamp;
		l_date_str := To_Char(SYSDATE, 'HH24:MI:SS');
		l_msg := a_msg;
		Dbms_Output.put_line(l_msg);
		INSERT INTO tax_job_log (job_no, line_no, log_date, error_type, error_code, msg)
		VALUES (a_job_no, g_line_no, SYSDATE, a_error_type, a_code, l_msg);

		COMMIT;
		g_line_no := g_line_no + 1;
		RETURN;
	EXCEPTION
		WHEN OTHERS THEN
			l_code := SQLCODE;
			Dbms_Output.put_line('Write Log ' || SQLERRM(l_code) || ' ' ||
			Dbms_Utility.format_error_backtrace);
			RETURN;

	END write_log;

END pkg_tax_plant_recon;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (16293, 0, 2018, 2, 0, 0, 0, 'c:\plasticwks\powerplant\sql\packages', 
    'PKG_TAX_PLANT_RECON.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
