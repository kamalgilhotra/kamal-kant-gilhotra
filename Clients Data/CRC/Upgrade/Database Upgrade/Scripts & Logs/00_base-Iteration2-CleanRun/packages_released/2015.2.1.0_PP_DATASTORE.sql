/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036012_system_PP_DATASTORE.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/27/2014 Chris Mardis
|| 2015.1.4	01/13/2015 Charlie Shilling	   Oracle 12c complications
||============================================================================
*/

create or replace package PP_DATASTORE is

   type T_ROW_MODIFY is table of char(1);
   type T_COL_NAMES is table of varchar2(30);
   -- type T_BIND_VARS is table of varchar2(2000);
   -- type T_TABLE_VARS is table of T_BIND_VARS;
   type T_COLS is table of varchar2(4000) index by pls_integer;
   type T_TAB is table of T_COLS index by pls_integer;
   type T_CMD_LINE is table of varchar2(2000);
   -- Column Types
   type T_NUMBER_TABLE is table of DBMS_SQL.NUMBER_TABLE;
   type T_VARCHAR2_TABLE is table of DBMS_SQL.VARCHAR2_TABLE;
   type T_DATE_TABLE is table of DBMS_SQL.DATE_TABLE;
   type T_BLOB_TABLE is table of DBMS_SQL.BLOB_TABLE;
   type T_CLOB_TABLE is table of DBMS_SQL.CLOB_TABLE;
   type T_TIMESTAMP_TABLE is table of DBMS_SQL.TIMESTAMP_TABLE;

   type T_COLUMN_MAP_REC is record(
      COL_NAME       varchar2(30),
      COL_TYPE       pls_integer,
      COL_UPDATEABLE boolean,
      COL_TYPE_POS   pls_integer);
   type T_COLUMN_MAP_TABLE is table of T_COLUMN_MAP_REC index by pls_integer;
   type T_ROW_MAP_TABLE is table of pls_integer;
   subtype HASH_T is varchar2(200);

   G_COLLECTION T_BIND_VARS;
   type T_DATASTORE_REC is record(
      --- type 2
      COL_NUM_TABLES   T_NUMBER_TABLE,
      NUM_TABLES_COUNT pls_integer,

      -- type 1 ,95 and 9
      COL_VAR2_TABLES   T_VARCHAR2_TABLE,
      VAR2_TABLES_COUNT pls_integer,

      --- type 12
      COL_DATE_TABLES   T_DATE_TABLE,
      DATE_TABLES_COUNT pls_integer,
      -- type 113
      COL_BLOB_TABLES   T_BLOB_TABLE,
      BLOB_TABLES_COUNT pls_integer,
      -- type 112
      COL_CLOB_TABLES   T_CLOB_TABLE,
      CLOB_TABLES_COUNT pls_integer,
      -- type 187
      COL_TS_TABLES   T_TIMESTAMP_TABLE,
      TS_TABLES_COUNT pls_integer,

      FIND_LAST_POS pls_integer,
      FIND_LIST     DBMS_SQL.NUMBER_TABLE,

      -- DataStore Info
      R_CURSOR            integer,
      SQLCMD              varchar2(32767),
      COLUMN_MAP          T_COLUMN_MAP_TABLE,
      ROW_MAP             T_ROW_MAP_TABLE,
      FILTER_ROW_MAP      T_ROW_MAP_TABLE,
      ROW_MODIFY          T_ROW_MODIFY,
      UPDATEABLE_COLUMNS  T_COL_NAMES,
      PRIMARY_KEY_COLUMNS T_COL_NAMES,
      UPDATEABLE_TABLE    varchar2(30));

   type T_DS_LIST is table of T_DATASTORE_REC index by varchar2(200);
   G_DS T_DS_LIST;

   function DISPLAYROWS(A_DS_NAME    varchar2,
                        A_COL_OPTION integer := 0) return T_BIND_VARS
      pipelined;

   function DISPLAYINFO(A_DS_NAME varchar2) return T_BIND_VARS
      pipelined;

   function DISPLAYTABLE(A_DS_NAME    varchar2,
                         A_COL_OPTION integer := 0) return T_TABLE_VARS
      pipelined;

   function DSCREATE(A_DS_NAME varchar2,
                     A_SQL     varchar2) return number;

   function DSDESTROY(A_DS_NAME varchar2) return integer;

   function UPDATEDATA(A_DS_NAME varchar2) return number;

   function INSERTROW(A_DS_NAME varchar2) return pls_integer;

   function DELETEROW(A_DS_NAME varchar2,
                      A_ROW     integer) return integer;

   function UPDATETABLE(A_DS_NAME varchar2,
                        A_TABLE   varchar2,
                        A_COLUMNS T_COL_NAMES) return integer;

   function rowcount(A_DS_NAME varchar2) return integer;

   function COLUMNCOUNT(A_DS_NAME varchar2) return integer;

   function SORT(A_DS_NAME  varchar2,
                 A_COL_NAME varchar := null,
                 A_DESC     integer := 0) return integer;

   function FILTER(A_DS_NAME  varchar2,
                   A_COL_NAME varchar2,
                   A_FILTER   varchar2 := null) return integer;

   function GETCOLUMN(A_DS_NAME varchar2,
                      A_POS     integer,
                      A_VAL     varchar2) return varchar2;

   function FIND(A_DS_NAME   varchar2,
                 A_COL_NAME  varchar2,
                 A_FILTER    varchar2,
                 A_START_ROW pls_integer := 0,
                 A_END_ROW   pls_integer := 0) return integer;

   function FINDNEXT(A_DS_NAME varchar2) return integer;

   function SETITEM(A_DS_NAME varchar2,
                    A_ROW     integer,
                    A_COL_POS integer,
                    A_VAL     varchar2) return number;

   function SETITEM(A_DS_NAME  varchar2,
                    A_ROW      integer,
                    A_COL_NAME varchar2,
                    A_VAL      varchar2) return number;

   function SETITEM(A_DS_NAME varchar2,
                    A_ROW     integer,
                    A_COL_POS integer,
                    A_VAL     number) return number;

   function SETITEM(A_DS_NAME  varchar2,
                    A_ROW      integer,
                    A_COL_NAME varchar2,
                    A_VAL      number) return number;

   function SETITEM(A_DS_NAME varchar2,
                    A_ROW     integer,
                    A_COL_POS integer,
                    A_VAL     date) return number;

   function SETITEM(A_DS_NAME  varchar2,
                    A_ROW      integer,
                    A_COL_NAME varchar2,
                    A_VAL      date) return number;

   function SETITEM(A_DS_NAME varchar2,
                    A_ROW     integer,
                    A_COL_POS integer,
                    A_VAL     clob) return number;

   function SETITEM(A_DS_NAME varchar2,
                    A_ROW     integer,
                    A_COL_POS integer,
                    A_VAL     blob) return number;

   function SETITEM(A_DS_NAME  varchar2,
                    A_ROW      integer,
                    A_COL_NAME varchar2,
                    A_VAL      blob) return number;

   function SETITEM(A_DS_NAME varchar2,
                    A_ROW     integer,
                    A_COL_POS integer,
                    A_VAL     timestamp) return number;

   function SETITEM(A_DS_NAME  varchar2,
                    A_ROW      integer,
                    A_COL_NAME varchar2,
                    A_VAL      timestamp) return number;

   function GETITEMDATE(A_DS_NAME varchar2,
                        A_ROW     integer,
                        A_COL_POS integer) return date;

   function GETITEMDATE(A_DS_NAME  varchar2,
                        A_ROW      integer,
                        A_COL_NAME varchar2) return date;

   function GETITEMNUMBER(A_DS_NAME varchar2,
                          A_ROW     integer,
                          A_COL_POS integer) return number;

   function GETITEMNUMBER(A_DS_NAME  varchar2,
                          A_ROW      integer,
                          A_COL_NAME varchar2) return number;

   function GETITEMCLOB(A_DS_NAME varchar2,
                        A_ROW     integer,
                        A_COL_POS integer) return clob;

   function GETITEMCLOB(A_DS_NAME  varchar2,
                        A_ROW      integer,
                        A_COL_NAME varchar2) return clob;

   function GETITEMBLOB(A_DS_NAME varchar2,
                        A_ROW     integer,
                        A_COL_POS integer) return blob;

   function GETITEMBLOB(A_DS_NAME  varchar2,
                        A_ROW      integer,
                        A_COL_NAME varchar2) return blob;

   function GETITEMTIMESTAMP(A_DS_NAME varchar2,
                             A_ROW     integer,
                             A_COL_POS integer) return timestamp;

   function GETITEMTIMESTAMP(A_DS_NAME  varchar2,
                             A_ROW      integer,
                             A_COL_NAME varchar2) return timestamp;

   function GETITEMSTRING(A_DS_NAME  varchar2,
                          A_ROW      integer,
                          A_COL_NAME varchar2) return varchar2;

   function GETITEMSTRING(A_DS_NAME varchar2,
                          A_ROW     integer,
                          A_COL_POS integer) return varchar2;

   function RETRIEVE(A_DS_NAME     varchar2,
                     A_BIND_VARS   in T_BIND_VARS := null,
                     A_BIND_TYPES  in T_BIND_VARS := null,
                     A_BIND_VALUES in T_BIND_VARS := null) return number;
   /* TYPECODE_DATE            PLS_INTEGER :=  12;
   TYPECODE_NUMBER          PLS_INTEGER :=   2;
   TYPECODE_RAW             PLS_INTEGER :=  95;
   TYPECODE_CHAR            PLS_INTEGER :=  96;
   TYPECODE_VARCHAR2        PLS_INTEGER :=   9;
   TYPECODE_VARCHAR         PLS_INTEGER :=   1;
   TYPECODE_MLSLABEL        PLS_INTEGER := 105;
   TYPECODE_BLOB            PLS_INTEGER := 113;
   TYPECODE_BFILE           PLS_INTEGER := 114;
   TYPECODE_CLOB            PLS_INTEGER := 112;
   TYPECODE_CFILE           PLS_INTEGER := 115;
   TYPECODE_TIMESTAMP       PLS_INTEGER := 187; */
end PP_DATASTORE;
/


create or replace package body PP_DATASTORE is

   /*************************************** Create DataStore ***************************************************/
   function DSCREATE(A_DS_NAME varchar2,
                     A_SQL     varchar2) return number is

      L_CODE          pls_integer;
      L_DATASTORE_REC T_DATASTORE_REC;
      L_COL_DESC      DBMS_SQL.DESC_TAB2;
      L_COL_COUNT     pls_integer;
      L_CURSOR        integer;

   begin
      if not G_DS.EXISTS(A_DS_NAME) then
         G_DS(A_DS_NAME) := L_DATASTORE_REC;
      end if;

      G_DS(A_DS_NAME).COLUMN_MAP.DELETE;
      if G_DS(A_DS_NAME).ROW_MAP is null then
         G_DS(A_DS_NAME).ROW_MAP := T_ROW_MAP_TABLE();
      end if;
      G_DS(A_DS_NAME).ROW_MAP.DELETE;
      L_CURSOR := DBMS_SQL.OPEN_CURSOR;
      G_DS(A_DS_NAME).R_CURSOR := L_CURSOR;
      G_DS(A_DS_NAME).SQLCMD := A_SQL;
      DBMS_SQL.PARSE(L_CURSOR, A_SQL, DBMS_SQL.V7);
      DBMS_SQL.DESCRIBE_COLUMNS2(L_CURSOR, L_COL_COUNT, L_COL_DESC);
      /* define the column types for each column */
      for J in 1 .. L_COL_COUNT
      loop
         case
            when L_COL_DESC(J).COL_TYPE = 1 then
               -- varchar2
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_NAME := LOWER(L_COL_DESC(J).COL_NAME);
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE := L_COL_DESC(J).COL_TYPE;
            when L_COL_DESC(J).COL_TYPE = 9 then
               -- varchar
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_NAME := LOWER(L_COL_DESC(J).COL_NAME);
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE := L_COL_DESC(J).COL_TYPE;
            when L_COL_DESC(J).COL_TYPE = 96 then
               -- char
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_NAME := LOWER(L_COL_DESC(J).COL_NAME);
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE := L_COL_DESC(J).COL_TYPE;
            when L_COL_DESC(J).COL_TYPE = 2 then
               -- number
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_NAME := LOWER(L_COL_DESC(J).COL_NAME);
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE := L_COL_DESC(J).COL_TYPE;
            when L_COL_DESC(J).COL_TYPE = 12 then
               -- date
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_NAME := LOWER(L_COL_DESC(J).COL_NAME);
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE := L_COL_DESC(J).COL_TYPE;
            when L_COL_DESC(J).COL_TYPE = 112 then
               -- clob
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_NAME := LOWER(L_COL_DESC(J).COL_NAME);
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE := L_COL_DESC(J).COL_TYPE;
            when L_COL_DESC(J).COL_TYPE = 113 then
               -- blob
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_NAME := LOWER(L_COL_DESC(J).COL_NAME);
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE := L_COL_DESC(J).COL_TYPE;
            when L_COL_DESC(J).COL_TYPE = 187 then
               -- timestamp
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_NAME := LOWER(L_COL_DESC(J).COL_NAME);
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE := L_COL_DESC(J).COL_TYPE;
            else
               null;
         end case;
      end loop;

      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE ds_create ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end DSCREATE;

   /************************************** GetColumnPos *************************************************/
   function GETCOLUMNPOS(A_DS_NAME  varchar2,
                         A_COL_NAME varchar2) return pls_integer is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      for I in 1 .. G_DS(A_DS_NAME).COLUMN_MAP.COUNT
      loop
         if G_DS(A_DS_NAME).COLUMN_MAP(I).COL_NAME = A_COL_NAME then
            return I;
         end if;
      end loop;
      return '';
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetColumnPos ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
   end GETCOLUMNPOS;

   /*************************************** UpdataData ***************************************************/
   function INSERTROW(A_DS_NAME varchar2) return pls_integer is
      I         pls_integer;
      L_CODE    pls_integer;
      L_ROW     pls_integer;
      L_ROW_MAP pls_integer;
      I_POS     pls_integer;
   begin

      G_DS(A_DS_NAME).ROW_MODIFY.EXTEND;
      G_DS(A_DS_NAME).ROW_MAP.EXTEND;
      L_ROW_MAP := G_DS(A_DS_NAME).ROW_MAP.COUNT;
      L_ROW := G_DS(A_DS_NAME).ROW_MODIFY.COUNT;
      G_DS(A_DS_NAME).ROW_MAP(L_ROW_MAP) := L_ROW;
      G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) := 'I';
      for I_POS in 1 .. G_DS(A_DS_NAME).COLUMN_MAP.COUNT
      loop
         case
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 1 then
               -- varchar2
               G_DS(A_DS_NAME).COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE_POS)(L_ROW) := '';
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 9 then
               -- varchar
               G_DS(A_DS_NAME).COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE_POS)(L_ROW) := '';
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 96 then
               -- char
               G_DS(A_DS_NAME).COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE_POS)(L_ROW) := '';
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 2 then
               -- number
               G_DS(A_DS_NAME).COL_NUM_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE_POS)(L_ROW) := 0;
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 12 then
               -- date
               G_DS(A_DS_NAME).COL_DATE_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE_POS)(L_ROW) := null;
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 112 then
               -- clob
               G_DS(A_DS_NAME).COL_CLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE_POS)(L_ROW) := '';
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 113 then
               -- blob
               G_DS(A_DS_NAME).COL_BLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE_POS)(L_ROW) := null;
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 187 then
               -- timestamp
               G_DS(A_DS_NAME).COL_TS_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE_POS)(L_ROW) := null;
            else
               null;
         end case;
      end loop;
      return L_ROW;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE InsertRow ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end INSERTROW;

   /*************************************** UpdataData ***************************************************/
   function UPDATEDATA(A_DS_NAME varchar2) return number is
      I              pls_integer;
      L_IDX          pls_integer;
      L_CODE         pls_integer;
      L_UPDATE_TABLE varchar2(2000);
      L_UPDATE_COLS  T_COL_NAMES := T_COL_NAMES();
      L_INSERT_COLS  T_COL_NAMES := T_COL_NAMES();
      L_SET_CLAUSE   varchar2(4000);
      L_WHERE_CLAUSE varchar2(4000);
      L_TOTAL_BIND   pls_integer;
      L_CURSOR       integer;
      I_POS          pls_integer;
      L_ROWS         pls_integer;
      type T_INTEGER_LIST is table of pls_integer;
      L_ROW_MODIFY_LIST T_INTEGER_LIST;
      L_ROW_INSERT_LIST T_INTEGER_LIST;
      L_ROW_DELETE_LIST T_INTEGER_LIST;
      L_INSERT_COLUMNS  varchar2(4000);
      L_VALUE_LIST      varchar2(4000);
      L_FOUND           boolean;

      --- type 2
      L_COL_NUM_UPDATE_TABLES   T_NUMBER_TABLE := T_NUMBER_TABLE();
      L_COL_NUM_INSERT_TABLES   T_NUMBER_TABLE := T_NUMBER_TABLE();
      L_COL_NUM_DELETE_TABLES   T_NUMBER_TABLE := T_NUMBER_TABLE();
      L_NUM_UPDATE_TABLES_COUNT pls_integer;
      L_NUM_INSERT_TABLES_COUNT pls_integer;
      L_NUM_DELETE_TABLES_COUNT pls_integer;
      -- type 1 ,95 and 9
      L_COL_VAR2_UPDATE_TABLES   T_VARCHAR2_TABLE := T_VARCHAR2_TABLE();
      L_COL_VAR2_INSERT_TABLES   T_VARCHAR2_TABLE := T_VARCHAR2_TABLE();
      L_COL_VAR2_DELETE_TABLES   T_VARCHAR2_TABLE := T_VARCHAR2_TABLE();
      L_VAR2_UPDATE_TABLES_COUNT pls_integer;
      L_VAR2_INSERT_TABLES_COUNT pls_integer;
      L_VAR2_DELETE_TABLES_COUNT pls_integer;
      --- type 12
      L_COL_DATE_UPDATE_TABLES   T_DATE_TABLE := T_DATE_TABLE();
      L_COL_DATE_INSERT_TABLES   T_DATE_TABLE := T_DATE_TABLE();
      L_COL_DATE_DELETE_TABLES   T_DATE_TABLE := T_DATE_TABLE();
      L_DATE_UPDATE_TABLES_COUNT pls_integer;
      L_DATE_INSERT_TABLES_COUNT pls_integer;
      L_DATE_DELETE_TABLES_COUNT pls_integer;
      -- type 113
      L_COL_BLOB_UPDATE_TABLES   T_BLOB_TABLE := T_BLOB_TABLE();
      L_COL_BLOB_INSERT_TABLES   T_BLOB_TABLE := T_BLOB_TABLE();
      L_COL_BLOB_DELETE_TABLES   T_BLOB_TABLE := T_BLOB_TABLE();
      L_BLOB_UPDATE_TABLES_COUNT pls_integer;
      L_BLOB_INSERT_TABLES_COUNT pls_integer;
      L_BLOB_DELETE_TABLES_COUNT pls_integer;
      -- type 112
      L_COL_CLOB_UPDATE_TABLES   T_CLOB_TABLE := T_CLOB_TABLE();
      L_COL_CLOB_INSERT_TABLES   T_CLOB_TABLE := T_CLOB_TABLE();
      L_COL_CLOB_DELETE_TABLES   T_CLOB_TABLE := T_CLOB_TABLE();
      L_CLOB_UPDATE_TABLES_COUNT pls_integer;
      L_CLOB_INSERT_TABLES_COUNT pls_integer;
      L_CLOB_DELETE_TABLES_COUNT pls_integer;
      -- type 187
      L_COL_TS_UPDATE_TABLES   T_TIMESTAMP_TABLE := T_TIMESTAMP_TABLE();
      L_COL_TS_INSERT_TABLES   T_TIMESTAMP_TABLE := T_TIMESTAMP_TABLE();
      L_COL_TS_DELETE_TABLES   T_TIMESTAMP_TABLE := T_TIMESTAMP_TABLE();
      L_TS_UPDATE_TABLES_COUNT pls_integer;
      L_TS_INSERT_TABLES_COUNT pls_integer;
      L_TS_DELETE_TABLES_COUNT pls_integer;
   begin

      --getcolumnpos(a_col_name varchar2);
      for I in 1 .. G_DS(A_DS_NAME).COLUMN_MAP.COUNT
      loop
         if G_DS(A_DS_NAME).COLUMN_MAP(I).COL_UPDATEABLE = true then
            L_UPDATE_COLS.EXTEND;
            L_UPDATE_COLS(L_UPDATE_COLS.COUNT) := G_DS(A_DS_NAME).COLUMN_MAP(I).COL_NAME;
         end if;
      end loop;
      if L_UPDATE_COLS.COUNT = 0 then
         RAISE_APPLICATION_ERROR(-20021, 'PP_DATASTORE UpdateData - No Updateable Columns. ');
         return -1;
      end if;
      L_SET_CLAUSE := '';
      for I in 1 .. L_UPDATE_COLS.COUNT
      loop
         if I = L_UPDATE_COLS.COUNT then
            L_SET_CLAUSE := L_SET_CLAUSE || ' ' || L_UPDATE_COLS(I) || ' = :' || CHR(I + 96) || ' ';
         else
            L_SET_CLAUSE := L_SET_CLAUSE || ' ' || L_UPDATE_COLS(I) || ' = :' || CHR(I + 96) || ', ';
         end if;
      end loop;
      if G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.COUNT = 0 then
         RAISE_APPLICATION_ERROR(-20021, 'PP_DATASTORE UpdateData - No Primary Key. ');
         return -1;
      end if;
      L_IDX          := L_UPDATE_COLS.COUNT;
      L_WHERE_CLAUSE := '';
      for I in 1 .. G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.COUNT
      loop
         if I = G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.COUNT then
            L_WHERE_CLAUSE := L_WHERE_CLAUSE || ' ' || G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS(I) ||
                              ' = :' || CHR(I + L_IDX + 96) || ' ';
         else
            L_WHERE_CLAUSE := L_WHERE_CLAUSE || ' ' || G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS(I) ||
                              ' = :' || CHR(I + L_IDX + 96) || ' and ';
         end if;
      end loop;

      L_TOTAL_BIND := L_IDX + G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.COUNT;
      -- Create a list of modified rows
      L_ROW_MODIFY_LIST := T_INTEGER_LIST();
      L_ROW_INSERT_LIST := T_INTEGER_LIST();
      L_ROW_DELETE_LIST := T_INTEGER_LIST();
      for I in 1 .. G_DS(A_DS_NAME).ROW_MODIFY.COUNT
      loop
         if G_DS(A_DS_NAME).ROW_MODIFY(I) = 'Y' then
            L_ROW_MODIFY_LIST.EXTEND;
            L_ROW_MODIFY_LIST(L_ROW_MODIFY_LIST.COUNT) := I;
         elsif G_DS(A_DS_NAME).ROW_MODIFY(I) = 'I' then
            L_ROW_INSERT_LIST.EXTEND;
            L_ROW_INSERT_LIST(L_ROW_INSERT_LIST.COUNT) := I;
         elsif G_DS(A_DS_NAME).ROW_MODIFY(I) = 'D' then
            L_ROW_DELETE_LIST.EXTEND;
            L_ROW_DELETE_LIST(L_ROW_DELETE_LIST.COUNT) := I;
         end if;
      end loop;

      if L_ROW_DELETE_LIST.COUNT > 0 then
         L_NUM_DELETE_TABLES_COUNT  := 0;
         L_VAR2_DELETE_TABLES_COUNT := 0;
         L_DATE_DELETE_TABLES_COUNT := 0;
         L_BLOB_DELETE_TABLES_COUNT := 0;
         L_CLOB_DELETE_TABLES_COUNT := 0;
         L_TS_DELETE_TABLES_COUNT   := 0;
         L_CURSOR                   := DBMS_SQL.OPEN_CURSOR;
         DBMS_OUTPUT.PUT_LINE('delete  ' || G_DS(A_DS_NAME).UPDATEABLE_TABLE || ' where ' ||
                              L_WHERE_CLAUSE);
         DBMS_SQL.PARSE(L_CURSOR,
                        'delete  ' || G_DS(A_DS_NAME).UPDATEABLE_TABLE || ' where ' ||
                        L_WHERE_CLAUSE,
                        DBMS_SQL.NATIVE);
         for I in 1 .. G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.COUNT
         loop
            I_POS := GETCOLUMNPOS(A_DS_NAME, G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS(I));
            case
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 1 then
                  -- varchar2
                  L_VAR2_DELETE_TABLES_COUNT := L_VAR2_DELETE_TABLES_COUNT + 1;
                  L_COL_VAR2_DELETE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_DELETE_LIST.COUNT
                  loop
                     L_COL_VAR2_DELETE_TABLES(L_VAR2_DELETE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_DELETE_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_VAR2_DELETE_TABLES(L_VAR2_DELETE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 9 then
                  -- varchar
                  L_VAR2_DELETE_TABLES_COUNT := L_VAR2_DELETE_TABLES_COUNT + 1;
                  L_COL_VAR2_DELETE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_DELETE_LIST.COUNT
                  loop
                     L_COL_VAR2_DELETE_TABLES(L_VAR2_DELETE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_DELETE_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_VAR2_DELETE_TABLES(L_VAR2_DELETE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 96 then
                  -- char
                  L_VAR2_DELETE_TABLES_COUNT := L_VAR2_DELETE_TABLES_COUNT + 1;
                  L_COL_VAR2_DELETE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_DELETE_LIST.COUNT
                  loop
                     L_COL_VAR2_DELETE_TABLES(L_VAR2_DELETE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_DELETE_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_VAR2_DELETE_TABLES(L_VAR2_DELETE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 2 then
                  -- number
                  L_NUM_DELETE_TABLES_COUNT := L_NUM_DELETE_TABLES_COUNT + 1;
                  L_COL_NUM_DELETE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_DELETE_LIST.COUNT
                  loop
                     L_COL_NUM_DELETE_TABLES(L_NUM_DELETE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                              .COL_NUM_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                              .COL_TYPE_POS)
                                                                               (L_ROW_DELETE_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_NUM_DELETE_TABLES(L_NUM_DELETE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 12 then
                  -- date
                  L_DATE_DELETE_TABLES_COUNT := L_DATE_DELETE_TABLES_COUNT + 1;
                  L_COL_DATE_DELETE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_DELETE_LIST.COUNT
                  loop
                     L_COL_DATE_DELETE_TABLES(L_DATE_DELETE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_DATE_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_DELETE_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_DATE_DELETE_TABLES(L_DATE_DELETE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 112 then
                  -- clob
                  L_CLOB_DELETE_TABLES_COUNT := L_CLOB_DELETE_TABLES_COUNT + 1;
                  L_COL_CLOB_DELETE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_DELETE_LIST.COUNT
                  loop
                     L_COL_CLOB_DELETE_TABLES(L_CLOB_DELETE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_CLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_DELETE_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_CLOB_DELETE_TABLES(L_CLOB_DELETE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 113 then
                  -- blob
                  L_BLOB_DELETE_TABLES_COUNT := L_BLOB_DELETE_TABLES_COUNT + 1;
                  L_COL_BLOB_DELETE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_DELETE_LIST.COUNT
                  loop
                     L_COL_BLOB_DELETE_TABLES(L_BLOB_DELETE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_BLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_DELETE_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_BLOB_DELETE_TABLES(L_BLOB_DELETE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 187 then
                  -- timestamp
                  L_TS_DELETE_TABLES_COUNT := L_TS_DELETE_TABLES_COUNT + 1;
                  L_COL_TS_DELETE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_DELETE_LIST.COUNT
                  loop
                     L_COL_TS_DELETE_TABLES(L_TS_DELETE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                            .COL_TS_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                           .COL_TYPE_POS)
                                                                             (L_ROW_DELETE_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_TS_DELETE_TABLES(L_TS_DELETE_TABLES_COUNT));
               else
                  null;
            end case;
         end loop;

         L_ROWS := DBMS_SQL.EXECUTE(L_CURSOR);
         DBMS_SQL.CLOSE_CURSOR(L_CURSOR);
         -- clean up
         for I in 1 .. L_NUM_DELETE_TABLES_COUNT
         loop
            L_COL_NUM_DELETE_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_VAR2_DELETE_TABLES_COUNT
         loop
            L_COL_VAR2_DELETE_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_DATE_DELETE_TABLES_COUNT
         loop
            L_COL_DATE_DELETE_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_BLOB_DELETE_TABLES_COUNT
         loop
            L_COL_BLOB_DELETE_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_CLOB_DELETE_TABLES_COUNT
         loop
            L_COL_CLOB_DELETE_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_TS_DELETE_TABLES_COUNT
         loop
            L_COL_TS_DELETE_TABLES(I).DELETE;
         end loop;
      end if; /* End of Delete */

      /* update modified rows */
      if L_ROW_MODIFY_LIST.COUNT > 0 then
         L_NUM_UPDATE_TABLES_COUNT  := 0;
         L_VAR2_UPDATE_TABLES_COUNT := 0;
         L_DATE_UPDATE_TABLES_COUNT := 0;
         L_BLOB_UPDATE_TABLES_COUNT := 0;
         L_CLOB_UPDATE_TABLES_COUNT := 0;
         L_TS_UPDATE_TABLES_COUNT   := 0;

         L_CURSOR := DBMS_SQL.OPEN_CURSOR;
         DBMS_OUTPUT.PUT_LINE('update  ' || G_DS(A_DS_NAME).UPDATEABLE_TABLE || ' set ' ||
                              L_SET_CLAUSE || ' where ' || L_WHERE_CLAUSE);
         DBMS_SQL.PARSE(L_CURSOR,
                        'update  ' || G_DS(A_DS_NAME).UPDATEABLE_TABLE || ' set ' || L_SET_CLAUSE ||
                        ' where ' || L_WHERE_CLAUSE,
                        DBMS_SQL.NATIVE);
         -- Copy modified data to the update collections and bind data
         for I in 1 .. L_UPDATE_COLS.COUNT
         loop
            I_POS := GETCOLUMNPOS(A_DS_NAME, L_UPDATE_COLS(I));
            case
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 1 then
                  -- varchar2
                  L_VAR2_UPDATE_TABLES_COUNT := L_VAR2_UPDATE_TABLES_COUNT + 1;
                  L_COL_VAR2_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_VAR2_UPDATE_TABLES(L_VAR2_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_VAR2_UPDATE_TABLES(L_VAR2_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 9 then
                  -- varchar
                  L_VAR2_UPDATE_TABLES_COUNT := L_VAR2_UPDATE_TABLES_COUNT + 1;
                  L_COL_VAR2_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_VAR2_UPDATE_TABLES(L_VAR2_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_VAR2_UPDATE_TABLES(L_VAR2_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 96 then
                  -- char
                  L_VAR2_UPDATE_TABLES_COUNT := L_VAR2_UPDATE_TABLES_COUNT + 1;
                  L_COL_VAR2_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_VAR2_UPDATE_TABLES(L_VAR2_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_VAR2_UPDATE_TABLES(L_VAR2_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 2 then
                  -- number
                  L_NUM_UPDATE_TABLES_COUNT := L_NUM_UPDATE_TABLES_COUNT + 1;
                  L_COL_NUM_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_NUM_UPDATE_TABLES(L_NUM_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                              .COL_NUM_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                              .COL_TYPE_POS)
                                                                               (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_NUM_UPDATE_TABLES(L_NUM_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 12 then
                  -- date
                  L_DATE_UPDATE_TABLES_COUNT := L_DATE_UPDATE_TABLES_COUNT + 1;
                  L_COL_DATE_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_DATE_UPDATE_TABLES(L_DATE_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_DATE_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_DATE_UPDATE_TABLES(L_DATE_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 112 then
                  -- clob
                  L_CLOB_UPDATE_TABLES_COUNT := L_CLOB_UPDATE_TABLES_COUNT + 1;
                  L_COL_CLOB_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_CLOB_UPDATE_TABLES(L_CLOB_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_CLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_CLOB_UPDATE_TABLES(L_CLOB_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 113 then
                  -- blob
                  L_BLOB_UPDATE_TABLES_COUNT := L_BLOB_UPDATE_TABLES_COUNT + 1;
                  L_COL_BLOB_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_BLOB_UPDATE_TABLES(L_BLOB_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_BLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_BLOB_UPDATE_TABLES(L_BLOB_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 187 then
                  -- timestamp
                  L_TS_UPDATE_TABLES_COUNT := L_TS_UPDATE_TABLES_COUNT + 1;
                  L_COL_TS_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_TS_UPDATE_TABLES(L_TS_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                            .COL_TS_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                           .COL_TYPE_POS)
                                                                             (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_TS_UPDATE_TABLES(L_TS_UPDATE_TABLES_COUNT));
               else
                  null;
            end case;
         end loop;
         for I in 1 .. G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.COUNT
         loop
            I_POS := GETCOLUMNPOS(A_DS_NAME, G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS(I));
            case
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 1 then
                  -- varchar2
                  L_VAR2_UPDATE_TABLES_COUNT := L_VAR2_UPDATE_TABLES_COUNT + 1;
                  L_COL_VAR2_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_VAR2_UPDATE_TABLES(L_VAR2_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_VAR2_UPDATE_TABLES(L_VAR2_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 9 then
                  -- varchar
                  L_VAR2_UPDATE_TABLES_COUNT := L_VAR2_UPDATE_TABLES_COUNT + 1;
                  L_COL_VAR2_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_VAR2_UPDATE_TABLES(L_VAR2_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_VAR2_UPDATE_TABLES(L_VAR2_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 96 then
                  -- char
                  L_VAR2_UPDATE_TABLES_COUNT := L_VAR2_UPDATE_TABLES_COUNT + 1;
                  L_COL_VAR2_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_VAR2_UPDATE_TABLES(L_VAR2_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_VAR2_UPDATE_TABLES(L_VAR2_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 2 then
                  -- number
                  L_NUM_UPDATE_TABLES_COUNT := L_NUM_UPDATE_TABLES_COUNT + 1;
                  L_COL_NUM_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_NUM_UPDATE_TABLES(L_NUM_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                              .COL_NUM_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                              .COL_TYPE_POS)
                                                                               (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_NUM_UPDATE_TABLES(L_NUM_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 12 then
                  -- date
                  L_DATE_UPDATE_TABLES_COUNT := L_DATE_UPDATE_TABLES_COUNT + 1;
                  L_COL_DATE_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_DATE_UPDATE_TABLES(L_DATE_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_DATE_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_DATE_UPDATE_TABLES(L_DATE_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 112 then
                  -- clob
                  L_CLOB_UPDATE_TABLES_COUNT := L_CLOB_UPDATE_TABLES_COUNT + 1;
                  L_COL_CLOB_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_CLOB_UPDATE_TABLES(L_CLOB_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_CLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_CLOB_UPDATE_TABLES(L_CLOB_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 113 then
                  -- blob
                  L_BLOB_UPDATE_TABLES_COUNT := L_BLOB_UPDATE_TABLES_COUNT + 1;
                  L_COL_BLOB_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_BLOB_UPDATE_TABLES(L_BLOB_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_BLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96 + L_IDX),
                                      L_COL_BLOB_UPDATE_TABLES(L_BLOB_UPDATE_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 187 then
                  -- timestamp
                  L_TS_UPDATE_TABLES_COUNT := L_TS_UPDATE_TABLES_COUNT + 1;
                  L_COL_TS_UPDATE_TABLES.EXTEND;
                  for J in 1 .. L_ROW_MODIFY_LIST.COUNT
                  loop
                     L_COL_TS_UPDATE_TABLES(L_TS_UPDATE_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                            .COL_TS_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                           .COL_TYPE_POS)
                                                                             (L_ROW_MODIFY_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_TS_UPDATE_TABLES(L_TS_UPDATE_TABLES_COUNT));
               else
                  null;
            end case;
         end loop;

         L_ROWS := DBMS_SQL.EXECUTE(L_CURSOR);
         DBMS_SQL.CLOSE_CURSOR(L_CURSOR);
         -- clean up
         for I in 1 .. L_NUM_UPDATE_TABLES_COUNT
         loop
            L_COL_NUM_UPDATE_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_VAR2_UPDATE_TABLES_COUNT
         loop
            L_COL_VAR2_UPDATE_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_DATE_UPDATE_TABLES_COUNT
         loop
            L_COL_DATE_UPDATE_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_BLOB_UPDATE_TABLES_COUNT
         loop
            L_COL_BLOB_UPDATE_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_CLOB_UPDATE_TABLES_COUNT
         loop
            L_COL_CLOB_UPDATE_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_TS_UPDATE_TABLES_COUNT
         loop
            L_COL_TS_UPDATE_TABLES(I).DELETE;
         end loop;
      end if; -- End of Modify

      if L_ROW_INSERT_LIST.COUNT > 0 then
         L_INSERT_COLS.DELETE;
         L_NUM_INSERT_TABLES_COUNT  := 0;
         L_VAR2_INSERT_TABLES_COUNT := 0;
         L_DATE_INSERT_TABLES_COUNT := 0;
         L_BLOB_INSERT_TABLES_COUNT := 0;
         L_CLOB_INSERT_TABLES_COUNT := 0;
         L_TS_INSERT_TABLES_COUNT   := 0;
         for I in 1 .. L_UPDATE_COLS.COUNT
         loop
            I_POS   := GETCOLUMNPOS(A_DS_NAME, L_UPDATE_COLS(I));
            L_FOUND := false;
            for J in 1 .. G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.COUNT
            loop
               if L_UPDATE_COLS(I) = G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS(J) then
                  L_FOUND := true;
               end if;
            end loop;
            if L_FOUND = false then
               L_INSERT_COLS.EXTEND;
               L_INSERT_COLS(L_INSERT_COLS.COUNT) := L_UPDATE_COLS(I);
            end if;
         end loop;
         for J in 1 .. G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.COUNT
         loop
            L_INSERT_COLS.EXTEND;
            L_INSERT_COLS(L_INSERT_COLS.COUNT) := G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS(J);
         end loop;
         L_INSERT_COLUMNS := '';
         for I in 1 .. L_INSERT_COLS.COUNT
         loop
            L_INSERT_COLUMNS := L_INSERT_COLUMNS || ' ' || L_INSERT_COLS(I);
            L_VALUE_LIST     := L_VALUE_LIST || ' :' || CHR(I + 96);
            if I <> L_INSERT_COLS.COUNT then
               L_INSERT_COLUMNS := L_INSERT_COLUMNS || ',';
               L_VALUE_LIST     := L_VALUE_LIST || ',';
            end if;
         end loop;
         L_CURSOR := DBMS_SQL.OPEN_CURSOR;
         DBMS_OUTPUT.PUT_LINE('insert into ' || G_DS(A_DS_NAME).UPDATEABLE_TABLE || '(' ||
                              L_INSERT_COLUMNS || ') values(' || L_VALUE_LIST || ')');
         DBMS_SQL.PARSE(L_CURSOR,
                        'insert into ' || G_DS(A_DS_NAME).UPDATEABLE_TABLE || '(' ||
                        L_INSERT_COLUMNS || ') values(' || L_VALUE_LIST || ')',
                        DBMS_SQL.NATIVE);

         for I in 1 .. L_INSERT_COLS.COUNT
         loop
            I_POS := GETCOLUMNPOS(A_DS_NAME, L_INSERT_COLS(I));
            case
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 1 then
                  -- varchar2
                  L_VAR2_INSERT_TABLES_COUNT := L_VAR2_INSERT_TABLES_COUNT + 1;
                  L_COL_VAR2_INSERT_TABLES.EXTEND;
                  for J in 1 .. L_ROW_INSERT_LIST.COUNT
                  loop
                     L_COL_VAR2_INSERT_TABLES(L_VAR2_INSERT_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_INSERT_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_VAR2_INSERT_TABLES(L_VAR2_INSERT_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 9 then
                  -- varchar
                  L_VAR2_INSERT_TABLES_COUNT := L_VAR2_INSERT_TABLES_COUNT + 1;
                  L_COL_VAR2_INSERT_TABLES.EXTEND;
                  for J in 1 .. L_ROW_INSERT_LIST.COUNT
                  loop
                     L_COL_VAR2_INSERT_TABLES(L_VAR2_INSERT_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_INSERT_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_VAR2_INSERT_TABLES(L_VAR2_INSERT_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 96 then
                  -- char
                  L_VAR2_INSERT_TABLES_COUNT := L_VAR2_INSERT_TABLES_COUNT + 1;
                  L_COL_VAR2_INSERT_TABLES.EXTEND;
                  for J in 1 .. L_ROW_INSERT_LIST.COUNT
                  loop
                     L_COL_VAR2_INSERT_TABLES(L_VAR2_INSERT_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_INSERT_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_VAR2_INSERT_TABLES(L_VAR2_INSERT_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 2 then
                  -- number
                  L_NUM_INSERT_TABLES_COUNT := L_NUM_INSERT_TABLES_COUNT + 1;
                  L_COL_NUM_INSERT_TABLES.EXTEND;
                  for J in 1 .. L_ROW_INSERT_LIST.COUNT
                  loop
                     L_COL_NUM_INSERT_TABLES(L_NUM_INSERT_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                              .COL_NUM_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                              .COL_TYPE_POS)
                                                                               (L_ROW_INSERT_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_NUM_INSERT_TABLES(L_NUM_INSERT_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 12 then
                  -- date
                  L_DATE_INSERT_TABLES_COUNT := L_DATE_INSERT_TABLES_COUNT + 1;
                  L_COL_DATE_INSERT_TABLES.EXTEND;
                  for J in 1 .. L_ROW_INSERT_LIST.COUNT
                  loop
                     L_COL_DATE_INSERT_TABLES(L_DATE_INSERT_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_DATE_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_INSERT_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_DATE_INSERT_TABLES(L_DATE_INSERT_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 112 then
                  -- clob
                  L_CLOB_INSERT_TABLES_COUNT := L_CLOB_INSERT_TABLES_COUNT + 1;
                  L_COL_CLOB_INSERT_TABLES.EXTEND;
                  for J in 1 .. L_ROW_INSERT_LIST.COUNT
                  loop
                     L_COL_CLOB_INSERT_TABLES(L_CLOB_INSERT_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_CLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_INSERT_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_CLOB_INSERT_TABLES(L_CLOB_INSERT_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 113 then
                  -- blob
                  L_BLOB_INSERT_TABLES_COUNT := L_BLOB_INSERT_TABLES_COUNT + 1;
                  L_COL_BLOB_INSERT_TABLES.EXTEND;
                  for J in 1 .. L_ROW_INSERT_LIST.COUNT
                  loop
                     L_COL_BLOB_INSERT_TABLES(L_BLOB_INSERT_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                                .COL_BLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                                 .COL_TYPE_POS)
                                                                                 (L_ROW_INSERT_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_BLOB_INSERT_TABLES(L_BLOB_INSERT_TABLES_COUNT));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 187 then
                  -- timestamp
                  L_TS_INSERT_TABLES_COUNT := L_TS_INSERT_TABLES_COUNT + 1;
                  L_COL_TS_INSERT_TABLES.EXTEND;
                  for J in 1 .. L_ROW_INSERT_LIST.COUNT
                  loop
                     L_COL_TS_INSERT_TABLES(L_TS_INSERT_TABLES_COUNT)(J) := G_DS(A_DS_NAME)
                                                                            .COL_TS_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                                           .COL_TYPE_POS)
                                                                             (L_ROW_INSERT_LIST(J));
                  end loop;
                  DBMS_SQL.BIND_ARRAY(L_CURSOR,
                                      CHR(I + 96),
                                      L_COL_TS_INSERT_TABLES(L_TS_INSERT_TABLES_COUNT));
               else
                  null;
            end case;
         end loop;
         L_ROWS := DBMS_SQL.EXECUTE(L_CURSOR);
         DBMS_SQL.CLOSE_CURSOR(L_CURSOR);
         -- clean up
         for I in 1 .. L_NUM_INSERT_TABLES_COUNT
         loop
            L_COL_NUM_INSERT_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_VAR2_INSERT_TABLES_COUNT
         loop
            L_COL_VAR2_INSERT_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_DATE_INSERT_TABLES_COUNT
         loop
            L_COL_DATE_INSERT_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_BLOB_INSERT_TABLES_COUNT
         loop
            L_COL_BLOB_INSERT_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_CLOB_INSERT_TABLES_COUNT
         loop
            L_COL_CLOB_INSERT_TABLES(I).DELETE;
         end loop;
         for I in 1 .. L_TS_INSERT_TABLES_COUNT
         loop
            L_COL_TS_INSERT_TABLES(I).DELETE;
         end loop;

      end if; -- End of Insert

      for I in 1 .. G_DS(A_DS_NAME).ROW_MODIFY.COUNT
      loop
         if G_DS(A_DS_NAME).ROW_MODIFY(I) = 'I' or G_DS(A_DS_NAME).ROW_MODIFY(I) = 'Y' then
            G_DS(A_DS_NAME).ROW_MODIFY(I) := 'N';
         end if;
      end loop;

      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE UpdateData ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end UPDATEDATA;
   /*************************************** Setitem ***************************************************/
   function SETITEM(A_DS_NAME varchar2,
                    A_ROW     integer,
                    A_COL_POS integer,
                    A_VAL     varchar2) return number is
      I      pls_integer;
      L_CODE pls_integer;
      L_ROW  pls_integer;
   begin
      L_ROW := G_DS(A_DS_NAME).ROW_MAP(A_ROW);
      G_DS(A_DS_NAME).COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(L_ROW) := A_VAL;
      if G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) <> 'I' then
         G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) := 'Y';
      end if;

      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE SetItem ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SETITEM;
   /*************************************** Setitem ***************************************************/
   function SETITEM(A_DS_NAME  varchar2,
                    A_ROW      integer,
                    A_COL_NAME varchar2,
                    A_VAL      varchar2) return number is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      return SETITEM(A_DS_NAME, A_ROW, GETCOLUMNPOS(A_DS_NAME, A_COL_NAME), A_VAL);
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE Setitem ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SETITEM;

   /*************************************** Setitem ***************************************************/
   function SETITEM(A_DS_NAME varchar2,
                    A_ROW     integer,
                    A_COL_POS integer,
                    A_VAL     number) return number is
      I      pls_integer;
      L_CODE pls_integer;
      L_ROW  pls_integer;
   begin
      L_ROW := G_DS(A_DS_NAME).ROW_MAP(A_ROW);
      G_DS(A_DS_NAME).COL_NUM_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(L_ROW) := A_VAL;
      if G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) <> 'I' then
         G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) := 'Y';
      end if;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE SetItem ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SETITEM;
   /*************************************** Setitem ***************************************************/
   function SETITEM(A_DS_NAME  varchar2,
                    A_ROW      integer,
                    A_COL_NAME varchar2,
                    A_VAL      number) return number is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      return SETITEM(A_DS_NAME, A_ROW, GETCOLUMNPOS(A_DS_NAME, A_COL_NAME), A_VAL);
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE Setitem ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SETITEM;
   /*************************************** Setitem ***************************************************/
   function SETITEM(A_DS_NAME varchar2,
                    A_ROW     integer,
                    A_COL_POS integer,
                    A_VAL     date) return number is
      I      pls_integer;
      L_CODE pls_integer;
      L_ROW  pls_integer;
   begin
      L_ROW := G_DS(A_DS_NAME).ROW_MAP(A_ROW);
      G_DS(A_DS_NAME).COL_DATE_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(L_ROW) := A_VAL;
      if G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) <> 'I' then
         G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) := 'Y';
      end if;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE SetItem ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SETITEM;
   /*************************************** Setitem ***************************************************/
   function SETITEM(A_DS_NAME  varchar2,
                    A_ROW      integer,
                    A_COL_NAME varchar2,
                    A_VAL      date) return number is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      return SETITEM(A_DS_NAME, A_ROW, GETCOLUMNPOS(A_DS_NAME, A_COL_NAME), A_VAL);
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE Setitem ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SETITEM;
   /*************************************** Setitem ***************************************************/
   function SETITEM(A_DS_NAME varchar2,
                    A_ROW     integer,
                    A_COL_POS integer,
                    A_VAL     clob) return number is
      I      pls_integer;
      L_CODE pls_integer;
      L_ROW  pls_integer;
   begin
      L_ROW := G_DS(A_DS_NAME).ROW_MAP(A_ROW);
      G_DS(A_DS_NAME).COL_CLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(L_ROW) := A_VAL;
      if G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) <> 'I' then
         G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) := 'Y';
      end if;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE SetItem ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SETITEM;
   /*************************************** Setitem ***************************************************/
   function SETITEM(A_DS_NAME  varchar2,
                    A_ROW      integer,
                    A_COL_NAME varchar2,
                    A_VAL      clob) return number is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      return SETITEM(A_DS_NAME, A_ROW, GETCOLUMNPOS(A_DS_NAME, A_COL_NAME), A_VAL);
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE Setitem ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SETITEM;

   /*************************************** Setitem ***************************************************/
   function SETITEM(A_DS_NAME varchar2,
                    A_ROW     integer,
                    A_COL_POS integer,
                    A_VAL     blob) return number is
      I      pls_integer;
      L_CODE pls_integer;
      L_ROW  pls_integer;
   begin
      L_ROW := G_DS(A_DS_NAME).ROW_MAP(A_ROW);
      G_DS(A_DS_NAME).COL_BLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(L_ROW) := A_VAL;
      if G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) <> 'I' then
         G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) := 'Y';
      end if;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE SetItem ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SETITEM;
   /*************************************** Setitem ***************************************************/
   function SETITEM(A_DS_NAME  varchar2,
                    A_ROW      integer,
                    A_COL_NAME varchar2,
                    A_VAL      blob) return number is
      I      pls_integer;
      L_CODE pls_integer;
      L_ROW  pls_integer;
   begin
      return SETITEM(A_DS_NAME, A_ROW, GETCOLUMNPOS(A_DS_NAME, A_COL_NAME), A_VAL);
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE Setitem ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SETITEM;
   /*************************************** Setitem ***************************************************/
   function SETITEM(A_DS_NAME varchar2,
                    A_ROW     integer,
                    A_COL_POS integer,
                    A_VAL     timestamp) return number is
      I      pls_integer;
      L_CODE pls_integer;
      L_ROW  pls_integer;
   begin
      L_ROW := G_DS(A_DS_NAME).ROW_MAP(A_ROW);
      G_DS(A_DS_NAME).COL_TS_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(L_ROW) := A_VAL;
      if G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) <> 'I' then
         G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) := 'Y';
      end if;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE SetItem ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SETITEM;
   /*************************************** Setitem ***************************************************/
   function SETITEM(A_DS_NAME  varchar2,
                    A_ROW      integer,
                    A_COL_NAME varchar2,
                    A_VAL      timestamp) return number is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      return SETITEM(A_DS_NAME, A_ROW, GETCOLUMNPOS(A_DS_NAME, A_COL_NAME), A_VAL);
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE Setitem ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SETITEM;

   /************************************** UpdateTable *************************************************/
   function CREATE_WHERE_STRING(A_DS_NAME varchar2,
                                A_ROW     integer) return varchar2 is
      L_SQL         varchar2(4000);
      L_WHERE_COUNT pls_integer;
      L_PRIMARY_KEY boolean;
      L_WHERE_LIST  T_BIND_VARS;
      L_CODE        pls_integer;
   begin
      L_WHERE_COUNT := 0;
      L_WHERE_LIST  := T_BIND_VARS();

      for I in 1 .. G_DS(A_DS_NAME).COLUMN_MAP.COUNT
      loop
         L_PRIMARY_KEY := false;
         for J in 1 .. G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.COUNT
         loop
            if G_DS(A_DS_NAME).COLUMN_MAP(I).COL_NAME = G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS(J) then
               L_PRIMARY_KEY := true;
               exit;
            end if;
         end loop;
         case
            when G_DS(A_DS_NAME).COLUMN_MAP(I).COL_TYPE = 1 then
               -- varchar2
               if L_PRIMARY_KEY then
                  L_WHERE_COUNT := L_WHERE_COUNT + 1;
                  L_WHERE_LIST.EXTEND;
                  L_WHERE_LIST(L_WHERE_COUNT) := G_DS(A_DS_NAME).COLUMN_MAP(I)
                                                 .COL_NAME || ' = ''' || G_DS(A_DS_NAME)
                                                 .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I)
                                                                  .COL_TYPE_POS) (A_ROW) || ''' ';
               end if;
            when G_DS(A_DS_NAME).COLUMN_MAP(I).COL_TYPE = 9 then
               -- varchar
               if L_PRIMARY_KEY then
                  L_WHERE_COUNT := L_WHERE_COUNT + 1;
                  L_WHERE_LIST.EXTEND;
                  L_WHERE_LIST(L_WHERE_COUNT) := G_DS(A_DS_NAME).COLUMN_MAP(I)
                                                 .COL_NAME || ' = ''' || G_DS(A_DS_NAME)
                                                 .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I)
                                                                  .COL_TYPE_POS) (A_ROW) || '''';
               end if;
            when G_DS(A_DS_NAME).COLUMN_MAP(I).COL_TYPE = 96 then
               -- char
               if L_PRIMARY_KEY then
                  L_WHERE_COUNT := L_WHERE_COUNT + 1;
                  L_WHERE_LIST(L_WHERE_COUNT) := G_DS(A_DS_NAME).COLUMN_MAP(I)
                                                 .COL_NAME || ' = ''' || G_DS(A_DS_NAME)
                                                 .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I)
                                                                  .COL_TYPE_POS) (A_ROW) || '''';
               end if;
            when G_DS(A_DS_NAME).COLUMN_MAP(I).COL_TYPE = 2 then
               -- number
               if L_PRIMARY_KEY then
                  L_WHERE_COUNT := L_WHERE_COUNT + 1;
                  L_WHERE_LIST.EXTEND;
                  L_WHERE_LIST(L_WHERE_COUNT) := G_DS(A_DS_NAME).COLUMN_MAP(I)
                                                 .COL_NAME || ' = ' ||
                                                  TO_CHAR(G_DS(A_DS_NAME)
                                                          .COL_NUM_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I)
                                                                          .COL_TYPE_POS) (A_ROW));
               end if;
            when G_DS(A_DS_NAME).COLUMN_MAP(I).COL_TYPE = 12 then
               -- date
               if L_PRIMARY_KEY then
                  L_WHERE_COUNT := L_WHERE_COUNT + 1;
                  L_WHERE_LIST.EXTEND;
                  L_WHERE_LIST(L_WHERE_COUNT) := G_DS(A_DS_NAME).COLUMN_MAP(I)
                                                 .COL_NAME || ' = ''' ||
                                                  TO_CHAR(G_DS(A_DS_NAME)
                                                          .COL_DATE_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I)
                                                                           .COL_TYPE_POS) (A_ROW),
                                                          'MM-DD-YYYY H24:MI:SS') || '''';
               end if;
            when G_DS(A_DS_NAME).COLUMN_MAP(I).COL_TYPE = 112 then
               -- clob
               null;
            when G_DS(A_DS_NAME).COLUMN_MAP(I).COL_TYPE = 113 then
               -- blob
               null;
            when G_DS(A_DS_NAME).COLUMN_MAP(I).COL_TYPE = 187 then
               -- timestamp
               if L_PRIMARY_KEY then
                  L_WHERE_COUNT := L_WHERE_COUNT + 1;
                  L_WHERE_LIST.EXTEND;
                  L_WHERE_LIST(L_WHERE_COUNT) := G_DS(A_DS_NAME).COLUMN_MAP(I)
                                                 .COL_NAME || ' = ''' ||
                                                  TO_CHAR(G_DS(A_DS_NAME)
                                                          .COL_TS_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I)
                                                                         .COL_TYPE_POS) (A_ROW)) || '''';
               end if;
            else
               null;
         end case;

      end loop;
      for I in 1 .. L_WHERE_COUNT
      loop
         if I = L_WHERE_COUNT then
            L_SQL := L_SQL || ' ' || L_WHERE_LIST(I);
         else
            L_SQL := L_SQL || ' ' || L_WHERE_LIST(I) || ' and ';
         end if;
      end loop;
      return L_SQL;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE CreateWhereString ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
   end CREATE_WHERE_STRING;
   /************************************** UpdateTable *************************************************/
   function DELETEROW(A_DS_NAME varchar2,
                      A_ROW     integer) return integer is
      L_DEL_TABLE varchar2(4000);
      L_CODE      pls_integer;
      L_ROW       pls_integer;
   begin
      L_ROW := G_DS(A_DS_NAME).ROW_MAP(A_ROW);
      /*  l_del_table := 'delete ' || g_ds(a_ds_name).updateable_table || ' where ' || create_where_string(a_ds_name,l_row );
      dbms_output.put_line(l_del_table);
      --execute immediate l_del_table;

      g_ds(a_ds_name).delete_list.extend;
      g_ds(a_ds_name).delete_list(g_ds(a_ds_name).delete_list.count) := l_del_table;
      -- now delete it from the collections

      for i in 1..g_ds(a_ds_name).column_map.count loop
         case
              when g_ds(a_ds_name).column_map(i).col_type = 1 then -- varchar2
                   g_ds(a_ds_name).col_var2_tables(g_ds(a_ds_name).column_map(i).col_type_pos).delete(l_row);
              when g_ds(a_ds_name).column_map(i).col_type  = 9 then -- varchar
                   g_ds(a_ds_name).col_var2_tables(g_ds(a_ds_name).column_map(i).col_type_pos).delete(l_row);
              when g_ds(a_ds_name).column_map(i).col_type  = 96 then -- char
                   g_ds(a_ds_name).col_var2_tables(g_ds(a_ds_name).column_map(i).col_type_pos).delete(l_row);
              when g_ds(a_ds_name).column_map(i).col_type  = 2 then -- number
                   g_ds(a_ds_name).col_num_tables(g_ds(a_ds_name).column_map(i).col_type_pos).delete(l_row);
              when g_ds(a_ds_name).column_map(i).col_type  = 12 then -- date
                   g_ds(a_ds_name).col_date_tables(g_ds(a_ds_name).column_map(i).col_type_pos).delete(l_row);
              when g_ds(a_ds_name).column_map(i).col_type  = 112 then -- clob
                   g_ds(a_ds_name).col_clob_tables(g_ds(a_ds_name).column_map(i).col_type_pos).delete(l_row);
              when g_ds(a_ds_name).column_map(i).col_type  = 113 then -- blob
                   g_ds(a_ds_name).col_blob_tables(g_ds(a_ds_name).column_map(i).col_type_pos).delete(l_row);
              when g_ds(a_ds_name).column_map(i).col_type  = 187 then -- timestamp
                   g_ds(a_ds_name).col_ts_tables(g_ds(a_ds_name).column_map(i).col_type_pos).delete(l_row);
              else
                   null;
            end case;

      end loop; */

      G_DS(A_DS_NAME).ROW_MODIFY(L_ROW) := 'D';
      for I in A_ROW .. G_DS(A_DS_NAME).ROW_MAP.COUNT - 1
      loop
         G_DS(A_DS_NAME).ROW_MAP(I) := G_DS(A_DS_NAME).ROW_MAP(I + 1);
      end loop;
      G_DS(A_DS_NAME).ROW_MAP.TRIM;

      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE DeleteRow ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end DELETEROW;
   /************************************** UpdateTable *************************************************/
   function UPDATETABLE(A_DS_NAME varchar2,
                        A_TABLE   varchar2,
                        A_COLUMNS T_COL_NAMES) return integer is
      L_CODE   pls_integer;
      I        pls_integer;
      J        pls_integer;
      COL_NAME varchar2(20);
      L_TABLE  varchar2(30);
      cursor PK_KEY_CUR is
         select LOWER(COLUMN_NAME)
           from ALL_CONS_COLUMNS
          where CONSTRAINT_NAME = (select CONSTRAINT_NAME
                                     from ALL_CONSTRAINTS
                                    where CONSTRAINT_TYPE = 'P'
                                      and OWNER = 'PWRPLANT'
                                      and TABLE_NAME = UPPER(L_TABLE))
            and TABLE_NAME = UPPER(L_TABLE)
            and OWNER = 'PWRPLANT'
          order by POSITION;
   begin
      for I in 1 .. G_DS(A_DS_NAME).COLUMN_MAP.COUNT
      loop
         G_DS(A_DS_NAME).COLUMN_MAP(I).COL_UPDATEABLE := false;
         for J in 1 .. A_COLUMNS.COUNT
         loop
            if G_DS(A_DS_NAME).COLUMN_MAP(I).COL_NAME = A_COLUMNS(J) then
               G_DS(A_DS_NAME).COLUMN_MAP(I).COL_UPDATEABLE := true;
               exit;
            end if;
         end loop;
      end loop;
      G_DS(A_DS_NAME).UPDATEABLE_TABLE := A_TABLE;
      L_TABLE := G_DS(A_DS_NAME).UPDATEABLE_TABLE;
      open PK_KEY_CUR;
      if G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.EXISTS(1) then
         G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.DELETE;
      else
         G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS := T_COL_NAMES();
      end if;
      loop
         fetch PK_KEY_CUR
            into COL_NAME;
         exit when PK_KEY_CUR%notfound;
         G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.EXTEND;
         G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS(G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.COUNT) := COL_NAME;
         DBMS_OUTPUT.PUT_LINE('PK=' || COL_NAME);
      end loop;
      close PK_KEY_CUR;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE UpdateTable ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end UPDATETABLE;
   /************************************** GetItemString *************************************************/
   function GETITEMSTRING(A_DS_NAME varchar2,
                          A_ROW     integer,
                          A_COL_POS integer) return varchar2 is
      L_CODE pls_integer;
   begin
      if G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS)
       .COL_TYPE = 1 or G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE = 95 or G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS)
         .COL_TYPE = 9 then
         return G_DS(A_DS_NAME).COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(A_ROW);
      elsif G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE = 112 then
         return G_DS(A_DS_NAME).COL_CLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(A_ROW);
      end if;
      return '';
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetItemString ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

   end GETITEMSTRING;

   /************************************** GetItemString *************************************************/
   function GETITEMSTRING(A_DS_NAME  varchar2,
                          A_ROW      integer,
                          A_COL_NAME varchar2) return varchar2 is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      return GETITEMSTRING(A_DS_NAME, A_ROW, GETCOLUMNPOS(A_DS_NAME, A_COL_NAME));
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetItemString ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

   end GETITEMSTRING;

   /************************************** GetItemDate *************************************************/
   function GETITEMDATE(A_DS_NAME varchar2,
                        A_ROW     integer,
                        A_COL_POS integer) return date is
      L_CODE pls_integer;
   begin
      if G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE = 12 then
         return G_DS(A_DS_NAME).COL_DATE_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(A_ROW);
      end if;
      return '';
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetItemDate ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

   end GETITEMDATE;

   /************************************** GetItemDate *************************************************/
   function GETITEMDATE(A_DS_NAME  varchar2,
                        A_ROW      integer,
                        A_COL_NAME varchar2) return date is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      return GETITEMDATE(A_DS_NAME, A_ROW, GETCOLUMNPOS(A_DS_NAME, A_COL_NAME));
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetItemDate ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

   end GETITEMDATE;

   /************************************** GetItemNumber *************************************************/
   function GETITEMNUMBER(A_DS_NAME varchar2,
                          A_ROW     integer,
                          A_COL_POS integer) return number is
      L_CODE pls_integer;
   begin
      if G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE = 2 then
         return G_DS(A_DS_NAME).COL_NUM_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(A_ROW);
      end if;
      return '';
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetItemNumber ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

   end GETITEMNUMBER;

   /************************************** GetItemNumber *************************************************/
   function GETITEMNUMBER(A_DS_NAME  varchar2,
                          A_ROW      integer,
                          A_COL_NAME varchar2) return number is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      return GETITEMNUMBER(A_DS_NAME, A_ROW, GETCOLUMNPOS(A_DS_NAME, A_COL_NAME));
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetItemNumber ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

   end GETITEMNUMBER;
   /************************************** GetItemClob *************************************************/
   function GETITEMCLOB(A_DS_NAME varchar2,
                        A_ROW     integer,
                        A_COL_POS integer) return clob is
      L_CODE pls_integer;
   begin
      if G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE = 112 then
         return G_DS(A_DS_NAME).COL_CLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(A_ROW);
      end if;
      return '';
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetItemClob ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

   end GETITEMCLOB;

   /************************************** GetItemClob *************************************************/
   function GETITEMCLOB(A_DS_NAME  varchar2,
                        A_ROW      integer,
                        A_COL_NAME varchar2) return clob is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      return GETITEMCLOB(A_DS_NAME, A_ROW, GETCOLUMNPOS(A_DS_NAME, A_COL_NAME));
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetItemClob ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

   end GETITEMCLOB;

   /************************************** GetItemBlob *************************************************/
   function GETITEMBLOB(A_DS_NAME varchar2,
                        A_ROW     integer,
                        A_COL_POS integer) return blob is
      L_CODE pls_integer;
   begin
      if G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE = 112 then
         return G_DS(A_DS_NAME).COL_BLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(A_ROW);
      end if;
      return null;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetItemBlob ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

   end GETITEMBLOB;

   /************************************** GetItemBlob *************************************************/
   function GETITEMBLOB(A_DS_NAME  varchar2,
                        A_ROW      integer,
                        A_COL_NAME varchar2) return blob is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      return GETITEMBLOB(A_DS_NAME, A_ROW, GETCOLUMNPOS(A_DS_NAME, A_COL_NAME));
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetItemBlob ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

   end GETITEMBLOB;
   /************************************** GetItemTimeStamp *************************************************/
   function GETITEMTIMESTAMP(A_DS_NAME varchar2,
                             A_ROW     integer,
                             A_COL_POS integer) return timestamp is
      L_CODE pls_integer;
   begin
      if G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE = 187 then
         return G_DS(A_DS_NAME).COL_TS_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(A_COL_POS).COL_TYPE_POS)(A_ROW);
      end if;
      return '';
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetItemTimeStamp ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

   end GETITEMTIMESTAMP;

   /************************************** GetItemTimeStamp *************************************************/
   function GETITEMTIMESTAMP(A_DS_NAME  varchar2,
                             A_ROW      integer,
                             A_COL_NAME varchar2) return timestamp is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      return GETITEMTIMESTAMP(A_DS_NAME, A_ROW, GETCOLUMNPOS(A_DS_NAME, A_COL_NAME));
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetItemTimeStamp ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
   end GETITEMTIMESTAMP;

   /************************************** GetColumn *************************************************/
   function GETCOLUMN(A_DS_NAME varchar2,
                      A_POS     integer,
                      A_VAL     varchar2) return varchar2 is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      if A_VAL = 'name' then
         return G_DS(A_DS_NAME).COLUMN_MAP(A_POS).COL_NAME;
      elsif A_VAL = 'type' then
         case
            when G_DS(A_DS_NAME).COLUMN_MAP(A_POS).COL_TYPE = 1 then
               -- varchar2
               return 'varchar2';
            when G_DS(A_DS_NAME).COLUMN_MAP(A_POS).COL_TYPE = 9 then
               -- varchar
               return 'varchar';
            when G_DS(A_DS_NAME).COLUMN_MAP(A_POS).COL_TYPE = 96 then
               -- char
               return 'char';
            when G_DS(A_DS_NAME).COLUMN_MAP(A_POS).COL_TYPE = 2 then
               -- number
               return 'number';
            when G_DS(A_DS_NAME).COLUMN_MAP(A_POS).COL_TYPE = 12 then
               -- date
               return 'date';
            when G_DS(A_DS_NAME).COLUMN_MAP(A_POS).COL_TYPE = 112 then
               -- clob
               return 'clob';
            when G_DS(A_DS_NAME).COLUMN_MAP(A_POS).COL_TYPE = 113 then
               -- blob
               return 'blob' || CHR(9);
            when G_DS(A_DS_NAME).COLUMN_MAP(A_POS).COL_TYPE = 187 then
               -- timestamp
               return 'timestamp';
            else
               return 'unknown';
         end case;
      end if;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE GetColumn ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
   end GETCOLUMN;

   /**************************************  Sort *************************************************/
   function SORT(A_DS_NAME  varchar2,
                 A_COL_NAME varchar := null,
                 A_DESC     integer := 0) return integer is
      L_CODE                 pls_integer;
      L_NUM_ARR              DBMS_SQL.NUMBER_TABLE;
      L_ROW                  pls_integer;
      L_POS                  pls_integer;
      L_TYPE_POS             pls_integer;
      L_VARCHAR2_SORT_TABLE  VARCHAR2_SORT_TABLE_T := VARCHAR2_SORT_TABLE_T();
      L_DATE_SORT_TABLE      DATE_SORT_TABLE_T := DATE_SORT_TABLE_T();
      L_NUMBER_SORT_TABLE    NUMBER_SORT_TABLE_T := NUMBER_SORT_TABLE_T();
      L_TIMESTAMP_SORT_TABLE TIMESTAMP_SORT_TABLE_T := TIMESTAMP_SORT_TABLE_T();
      type ROW_POS_T is table of pls_integer index by pls_integer;
      L_ROW_POS ROW_POS_T;
      function SORT_COLLECTION(A_COLLECTION in out nocopy VARCHAR2_SORT_TABLE_T,
                               A_DESC       integer := 0) return DBMS_SQL.NUMBER_TABLE is
         L_NUM_ARR DBMS_SQL.NUMBER_TABLE;
      begin
         if A_DESC = 0 then
            select ROWNUM bulk collect
              into L_NUM_ARR
              from table(A_COLLECTION)
             order by COLUMN_VALUE;
         else
            select ROWNUM bulk collect
              into L_NUM_ARR
              from table(A_COLLECTION)
             order by COLUMN_VALUE desc;
         end if;
         return L_NUM_ARR;
      end SORT_COLLECTION;
      function SORT_COLLECTION(A_COLLECTION in out nocopy NUMBER_SORT_TABLE_T,
                               A_DESC       integer := 0) return DBMS_SQL.NUMBER_TABLE is
         L_NUM_ARR DBMS_SQL.NUMBER_TABLE;
      begin
         if A_DESC = 0 then
            select ROWNUM bulk collect
              into L_NUM_ARR
              from table(A_COLLECTION)
             order by COLUMN_VALUE;
         else
            select ROWNUM bulk collect
              into L_NUM_ARR
              from table(A_COLLECTION)
             order by COLUMN_VALUE desc;
         end if;
         return L_NUM_ARR;
      end SORT_COLLECTION;
      function SORT_COLLECTION(A_COLLECTION in out nocopy DATE_SORT_TABLE_T,
                               A_DESC       integer := 0) return DBMS_SQL.NUMBER_TABLE is
         L_NUM_ARR DBMS_SQL.NUMBER_TABLE;
      begin
         if A_DESC = 0 then
            select ROWNUM bulk collect
              into L_NUM_ARR
              from table(A_COLLECTION)
             order by COLUMN_VALUE;
         else
            select ROWNUM bulk collect
              into L_NUM_ARR
              from table(A_COLLECTION)
             order by COLUMN_VALUE desc;
         end if;
         return L_NUM_ARR;
      end SORT_COLLECTION;
      function SORT_COLLECTION(A_COLLECTION in out nocopy TIMESTAMP_SORT_TABLE_T,
                               A_DESC       integer := 0) return DBMS_SQL.NUMBER_TABLE is
         L_NUM_ARR DBMS_SQL.NUMBER_TABLE;
      begin
         if A_DESC = 0 then
            select ROWNUM bulk collect
              into L_NUM_ARR
              from table(A_COLLECTION)
             order by COLUMN_VALUE;
         else
            select ROWNUM bulk collect
              into L_NUM_ARR
              from table(A_COLLECTION)
             order by COLUMN_VALUE desc;
         end if;
         return L_NUM_ARR;
      end SORT_COLLECTION;
   begin
      if A_COL_NAME is null then
         -- remove the sort
         L_POS := 0;
         for I in 1 .. G_DS(A_DS_NAME).ROW_MODIFY.COUNT
         loop
            if G_DS(A_DS_NAME).ROW_MODIFY(I) <> 'D' then
               L_POS := L_POS + 1;
               G_DS(A_DS_NAME).ROW_MAP(L_POS) := I;
            end if;
         end loop;
         return 0;
      end if;
      L_POS := GETCOLUMNPOS(A_DS_NAME, A_COL_NAME);

      case
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS)
          .COL_TYPE = 1 -- varchar2
              or G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 9 -- varchar
              or G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 96 then
            -- char
            L_TYPE_POS := G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE_POS;
            for I in 1 .. G_DS(A_DS_NAME).ROW_MAP.COUNT
            loop
               L_VARCHAR2_SORT_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_ROW_POS(I) := L_ROW;
               L_VARCHAR2_SORT_TABLE(I) := G_DS(A_DS_NAME).COL_VAR2_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_NUM_ARR := SORT_COLLECTION(L_VARCHAR2_SORT_TABLE, A_DESC);
            for I in 1 .. L_NUM_ARR.COUNT
            loop
               G_DS(A_DS_NAME).ROW_MAP(I) := L_ROW_POS(L_NUM_ARR(I));
            end loop;
            L_VARCHAR2_SORT_TABLE.DELETE;
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 2 then
            -- number
            L_TYPE_POS := G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE_POS;
            for I in 1 .. G_DS(A_DS_NAME).ROW_MAP.COUNT
            loop
               L_NUMBER_SORT_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_ROW_POS(I) := L_ROW;
               L_NUMBER_SORT_TABLE(I) := G_DS(A_DS_NAME).COL_VAR2_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_NUM_ARR := SORT_COLLECTION(L_NUMBER_SORT_TABLE, A_DESC);
            for I in 1 .. L_NUM_ARR.COUNT
            loop
               G_DS(A_DS_NAME).ROW_MAP(I) := L_ROW_POS(L_NUM_ARR(I));
            end loop;
            L_NUMBER_SORT_TABLE.DELETE;
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 12 then
            -- date
            L_TYPE_POS := G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE_POS;
            for I in 1 .. G_DS(A_DS_NAME).ROW_MAP.COUNT
            loop
               L_DATE_SORT_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_ROW_POS(I) := L_ROW;
               L_DATE_SORT_TABLE(I) := G_DS(A_DS_NAME).COL_DATE_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_NUM_ARR := SORT_COLLECTION(L_DATE_SORT_TABLE, A_DESC);
            for I in 1 .. L_NUM_ARR.COUNT
            loop
               G_DS(A_DS_NAME).ROW_MAP(I) := L_ROW_POS(L_NUM_ARR(I));
            end loop;
            L_DATE_SORT_TABLE.DELETE;
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 112 then
            -- clob
            L_TYPE_POS := G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE_POS;
            for I in 1 .. G_DS(A_DS_NAME).ROW_MAP.COUNT
            loop
               L_VARCHAR2_SORT_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_ROW_POS(I) := L_ROW;
               L_VARCHAR2_SORT_TABLE(I) := G_DS(A_DS_NAME).COL_CLOB_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_NUM_ARR := SORT_COLLECTION(L_VARCHAR2_SORT_TABLE, A_DESC);
            for I in 1 .. L_NUM_ARR.COUNT
            loop
               G_DS(A_DS_NAME).ROW_MAP(I) := L_ROW_POS(L_NUM_ARR(I));
            end loop;
            L_VARCHAR2_SORT_TABLE.DELETE;
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 113 then
            -- blob
            null;
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 187 then
            -- timestamp
            L_TYPE_POS := G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE_POS;
            for I in 1 .. G_DS(A_DS_NAME).ROW_MAP.COUNT
            loop
               L_TIMESTAMP_SORT_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_ROW_POS(I) := L_ROW;
               L_TIMESTAMP_SORT_TABLE(I) := G_DS(A_DS_NAME).COL_TS_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_NUM_ARR := SORT_COLLECTION(L_TIMESTAMP_SORT_TABLE, A_DESC);
            for I in 1 .. L_NUM_ARR.COUNT
            loop
               G_DS(A_DS_NAME).ROW_MAP(I) := L_ROW_POS(L_NUM_ARR(I));
            end loop;
            L_TIMESTAMP_SORT_TABLE.DELETE;
         else
            null;
      end case;

      /*    for i in 1..g_ds(a_ds_name).col_var2_tables(1).count loop
           l_varchar2_sort_table.extend;
           l_varchar2_sort_table(i) := g_ds(a_ds_name).col_var2_tables(1)(i);
         end loop;

      l_num_arr := sort_collection( l_varchar2_sort_table);
      for i in 1..l_num_arr.count loop
          dbms_output.put_line(g_ds(a_ds_name).col_var2_tables(1)(l_num_arr(i)));
      end loop;*/
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE sort ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SORT;

   /**************************************  Row Count *************************************************/
   function rowcount(A_DS_NAME varchar2) return integer is
      L_CODE pls_integer;
   begin
      return G_DS(A_DS_NAME).ROW_MAP.COUNT;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE RowCount ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
   end rowcount;
   /**************************************  Filter *************************************************/
   function FILTER(A_DS_NAME  varchar2,
                   A_COL_NAME varchar2,
                   A_FILTER   varchar2) return integer is
      L_CODE                  pls_integer;
      L_ROW                   pls_integer;
      L_POS                   pls_integer;
      L_TYPE_POS              pls_integer;
      L_VARCHAR2_FILTER_TABLE VARCHAR2_SORT_TABLE_T := VARCHAR2_SORT_TABLE_T();
      L_NUM_FILTER_TABLE      NUMBER_SORT_TABLE_T := NUMBER_SORT_TABLE_T();
      L_DATE_FILTER_TABLE     DATE_SORT_TABLE_T := DATE_SORT_TABLE_T();
      L_TS_FILTER_TABLE       TIMESTAMP_SORT_TABLE_T := TIMESTAMP_SORT_TABLE_T();
      L_NUM_ARR               DBMS_SQL.NUMBER_TABLE;
      L_COUNT                 pls_integer;
      type ROW_POS_T is table of pls_integer index by pls_integer;
      L_ROW_POS ROW_POS_T;
      function FILTER_COLLECTION(A_COLLECTION in out nocopy VARCHAR2_SORT_TABLE_T,
                                 A_FILTER     varchar2,
                                 A_DESC       integer := 0) return DBMS_SQL.NUMBER_TABLE is
         L_NUM_ARR DBMS_SQL.NUMBER_TABLE;
         L_CURSOR  integer;
         L_ROWS    integer;
      begin
         L_CURSOR := DBMS_SQL.OPEN_CURSOR;
         DBMS_SQL.PARSE(L_CURSOR,
                        'select n from (SELECT rownum n,column_value  FROM   TABLE(:collection) ) where  column_value ' ||
                        A_FILTER || '  ORDER  BY 1 ',
                        DBMS_SQL.V7);
         DBMS_SQL.BIND_VARIABLE(L_CURSOR, 'collection', A_COLLECTION);
         DBMS_SQL.DEFINE_ARRAY(L_CURSOR, 1, L_NUM_ARR, 50, 1);
         L_ROWS := DBMS_SQL.EXECUTE(L_CURSOR);
         loop
            L_ROWS := DBMS_SQL.FETCH_ROWS(L_CURSOR);
            DBMS_SQL.COLUMN_VALUE(L_CURSOR, 1, L_NUM_ARR);
            exit when L_ROWS <> 50;
         end loop;
         DBMS_SQL.CLOSE_CURSOR(L_CURSOR);
         return L_NUM_ARR;
      end FILTER_COLLECTION;
      function FILTER_COLLECTION(A_COLLECTION in out nocopy NUMBER_SORT_TABLE_T,
                                 A_FILTER     varchar2) return DBMS_SQL.NUMBER_TABLE is
         L_NUM_ARR DBMS_SQL.NUMBER_TABLE;
         L_CURSOR  integer;
         L_ROWS    integer;

      begin
         L_CURSOR := DBMS_SQL.OPEN_CURSOR;
         DBMS_SQL.PARSE(L_CURSOR,
                        'select n from (SELECT rownum n,column_value  FROM   TABLE(:collection) ) where  column_value ' ||
                        A_FILTER || '  ORDER  BY 1 ',
                        DBMS_SQL.V7);
         DBMS_SQL.BIND_VARIABLE(L_CURSOR, 'collection', A_COLLECTION);
         DBMS_SQL.DEFINE_ARRAY(L_CURSOR, 1, L_NUM_ARR, 50, 1);
         L_ROWS := DBMS_SQL.EXECUTE(L_CURSOR);
         loop
            L_ROWS := DBMS_SQL.FETCH_ROWS(L_CURSOR);
            DBMS_SQL.COLUMN_VALUE(L_CURSOR, 1, L_NUM_ARR);
            exit when L_ROWS <> 50;
         end loop;
         DBMS_SQL.CLOSE_CURSOR(L_CURSOR);
         return L_NUM_ARR;
      end FILTER_COLLECTION;
      function FILTER_COLLECTION(A_COLLECTION in out nocopy DATE_SORT_TABLE_T,
                                 A_FILTER     varchar2) return DBMS_SQL.NUMBER_TABLE is
         L_NUM_ARR DBMS_SQL.NUMBER_TABLE;
         L_CURSOR  integer;
         L_ROWS    integer;
      begin
         L_CURSOR := DBMS_SQL.OPEN_CURSOR;
         DBMS_SQL.PARSE(L_CURSOR,
                        'select n from (SELECT rownum n,column_value  FROM   TABLE(:collection) ) where  column_value ' ||
                        A_FILTER || '  ORDER  BY 1 ',
                        DBMS_SQL.V7);
         DBMS_SQL.BIND_VARIABLE(L_CURSOR, 'collection', A_COLLECTION);
         DBMS_SQL.DEFINE_ARRAY(L_CURSOR, 1, L_NUM_ARR, 50, 1);
         L_ROWS := DBMS_SQL.EXECUTE(L_CURSOR);
         loop
            L_ROWS := DBMS_SQL.FETCH_ROWS(L_CURSOR);
            DBMS_SQL.COLUMN_VALUE(L_CURSOR, 1, L_NUM_ARR);
            exit when L_ROWS <> 50;
         end loop;
         DBMS_SQL.CLOSE_CURSOR(L_CURSOR);
         return L_NUM_ARR;
      end FILTER_COLLECTION;

      function FILTER_COLLECTION(A_COLLECTION in out nocopy TIMESTAMP_SORT_TABLE_T,
                                 A_FILTER     varchar2 := null) return DBMS_SQL.NUMBER_TABLE is
         L_NUM_ARR DBMS_SQL.NUMBER_TABLE;
         L_CURSOR  integer;
         L_ROWS    integer;
      begin
         L_CURSOR := DBMS_SQL.OPEN_CURSOR;
         DBMS_SQL.PARSE(L_CURSOR,
                        'select n from (SELECT rownum n,column_value  FROM   TABLE(:collection) ) where  column_value ' ||
                        A_FILTER || '  ORDER  BY 1 ',
                        DBMS_SQL.V7);
         DBMS_SQL.BIND_VARIABLE(L_CURSOR, 'collection', A_COLLECTION);
         DBMS_SQL.DEFINE_ARRAY(L_CURSOR, 1, L_NUM_ARR, 50, 1);
         L_ROWS := DBMS_SQL.EXECUTE(L_CURSOR);
         loop
            L_ROWS := DBMS_SQL.FETCH_ROWS(L_CURSOR);
            DBMS_SQL.COLUMN_VALUE(L_CURSOR, 1, L_NUM_ARR);
            exit when L_ROWS <> 50;
         end loop;
         DBMS_SQL.CLOSE_CURSOR(L_CURSOR);
         return L_NUM_ARR;
      end FILTER_COLLECTION;
   begin
      if A_FILTER is null then
         L_COUNT := 0;
         G_DS(A_DS_NAME).ROW_MAP.DELETE;
         for I in 1 .. G_DS(A_DS_NAME).ROW_MODIFY.COUNT
         loop
            if G_DS(A_DS_NAME).ROW_MODIFY(I) <> 'D' then
               L_COUNT := L_COUNT + 1;
               G_DS(A_DS_NAME).ROW_MAP.EXTEND;
               G_DS(A_DS_NAME).ROW_MAP(L_COUNT) := I;
            end if;
         end loop;
         return 0;
      end if;
      L_POS      := GETCOLUMNPOS(A_DS_NAME, A_COL_NAME);
      L_TYPE_POS := G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE_POS;

      L_POS      := GETCOLUMNPOS(A_DS_NAME, A_COL_NAME);
      L_TYPE_POS := G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE_POS;
      case
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS)
          .COL_TYPE = 1 -- varchar2
              or G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 9 -- varchar
              or G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 96 then
            -- char
            for I in 1 .. G_DS(A_DS_NAME).ROW_MAP.COUNT
            loop
               L_VARCHAR2_FILTER_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_ROW_POS(I) := L_ROW;
               L_VARCHAR2_FILTER_TABLE(I) := G_DS(A_DS_NAME).COL_VAR2_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_NUM_ARR := FILTER_COLLECTION(L_VARCHAR2_FILTER_TABLE, A_FILTER);
            L_VARCHAR2_FILTER_TABLE.DELETE;
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 2 then
            -- number
            for I in 1 .. G_DS(A_DS_NAME).ROW_MAP.COUNT
            loop
               L_VARCHAR2_FILTER_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_ROW_POS(I) := L_ROW;
               L_NUM_FILTER_TABLE(I) := G_DS(A_DS_NAME).COL_NUM_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_NUM_ARR := FILTER_COLLECTION(L_NUM_FILTER_TABLE, A_FILTER);
            L_NUM_FILTER_TABLE.DELETE;
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 12 then
            -- date
            for I in 1 .. G_DS(A_DS_NAME).ROW_MAP.COUNT
            loop
               L_DATE_FILTER_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_ROW_POS(I) := L_ROW;
               L_DATE_FILTER_TABLE(I) := G_DS(A_DS_NAME).COL_DATE_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_NUM_ARR := FILTER_COLLECTION(L_DATE_FILTER_TABLE, A_FILTER);
            L_DATE_FILTER_TABLE.DELETE;
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 112 then
            -- clob
            for I in 1 .. G_DS(A_DS_NAME).ROW_MAP.COUNT
            loop
               L_DATE_FILTER_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_ROW_POS(I) := L_ROW;
               L_VARCHAR2_FILTER_TABLE(I) := G_DS(A_DS_NAME).COL_CLOB_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_NUM_ARR := FILTER_COLLECTION(L_VARCHAR2_FILTER_TABLE, A_FILTER);
            L_VARCHAR2_FILTER_TABLE.DELETE;
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 113 then
            -- blob
            null;
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 187 then
            -- timestamp
            for I in 1 .. G_DS(A_DS_NAME).ROW_MAP.COUNT
            loop
               L_TS_FILTER_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_ROW_POS(I) := L_ROW;
               L_TS_FILTER_TABLE(I) := G_DS(A_DS_NAME).COL_TS_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_NUM_ARR := FILTER_COLLECTION(L_TS_FILTER_TABLE, A_FILTER);
            L_TS_FILTER_TABLE.DELETE;
         else
            null;
      end case;
      G_DS(A_DS_NAME).ROW_MAP.DELETE;
      for I in 1 .. L_NUM_ARR.COUNT
      loop
         G_DS(A_DS_NAME).ROW_MAP.EXTEND;
         G_DS(A_DS_NAME).ROW_MAP(I) := L_ROW_POS(L_NUM_ARR(I));
      end loop;

      return G_DS(A_DS_NAME).ROW_MAP.COUNT;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE Filter ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
   end FILTER;
   /**************************************  Find *************************************************/
   function FIND(A_DS_NAME   varchar2,
                 A_COL_NAME  varchar2,
                 A_FILTER    varchar2,
                 A_START_ROW pls_integer := 0,
                 A_END_ROW   pls_integer := 0) return integer is
      L_CODE                pls_integer;
      L_ROW                 pls_integer;
      L_POS                 pls_integer;
      L_TYPE_POS            pls_integer;
      L_START_ROW           pls_integer;
      L_END_ROW             pls_integer;
      L_VARCHAR2_FIND_TABLE VARCHAR2_SORT_TABLE_T := VARCHAR2_SORT_TABLE_T();
      L_NUM_FIND_TABLE      NUMBER_SORT_TABLE_T := NUMBER_SORT_TABLE_T();
      L_DATE_FIND_TABLE     DATE_SORT_TABLE_T := DATE_SORT_TABLE_T();
      L_TS_FIND_TABLE       TIMESTAMP_SORT_TABLE_T := TIMESTAMP_SORT_TABLE_T();
      L_NUM_ARR             DBMS_SQL.NUMBER_TABLE;
      L_FIND_ROWS           DBMS_SQL.NUMBER_TABLE;
      function FIND_COLLECTION(A_COLLECTION in out nocopy VARCHAR2_SORT_TABLE_T,
                               A_FILTER     varchar2) return DBMS_SQL.NUMBER_TABLE is
         L_NUM_ARR DBMS_SQL.NUMBER_TABLE;
         L_CURSOR  integer;
         L_ROWS    integer;
      begin
         L_CURSOR := DBMS_SQL.OPEN_CURSOR;
         DBMS_SQL.PARSE(L_CURSOR,
                        'select n from (SELECT rownum n,column_value  FROM   TABLE(:collection) ) where  column_value ' ||
                        A_FILTER || '  ORDER  BY 1 ',
                        DBMS_SQL.V7);
         DBMS_SQL.BIND_VARIABLE(L_CURSOR, 'collection', A_COLLECTION);
         DBMS_SQL.DEFINE_ARRAY(L_CURSOR, 1, L_NUM_ARR, 50, 1);
         L_ROWS := DBMS_SQL.EXECUTE(L_CURSOR);
         loop
            L_ROWS := DBMS_SQL.FETCH_ROWS(L_CURSOR);
            DBMS_SQL.COLUMN_VALUE(L_CURSOR, 1, L_NUM_ARR);
            exit when L_ROWS <> 50;
         end loop;
         DBMS_SQL.CLOSE_CURSOR(L_CURSOR);
         return L_NUM_ARR;
      end FIND_COLLECTION;
      function FIND_COLLECTION(A_COLLECTION in out nocopy NUMBER_SORT_TABLE_T,
                               A_FILTER     varchar2) return DBMS_SQL.NUMBER_TABLE is
         L_NUM_ARR DBMS_SQL.NUMBER_TABLE;
         L_CURSOR  integer;
         L_ROWS    integer;
      begin
         L_CURSOR := DBMS_SQL.OPEN_CURSOR;
         DBMS_SQL.PARSE(L_CURSOR,
                        'select n from (SELECT rownum n,column_value  FROM   TABLE(:collection) ) where  column_value ' ||
                        A_FILTER || '  ORDER  BY 1 ',
                        DBMS_SQL.V7);
         DBMS_SQL.BIND_VARIABLE(L_CURSOR, 'collection', A_COLLECTION);
         DBMS_SQL.DEFINE_ARRAY(L_CURSOR, 1, L_NUM_ARR, 50, 1);
         L_ROWS := DBMS_SQL.EXECUTE(L_CURSOR);
         loop
            L_ROWS := DBMS_SQL.FETCH_ROWS(L_CURSOR);
            DBMS_SQL.COLUMN_VALUE(L_CURSOR, 1, L_NUM_ARR);
            exit when L_ROWS <> 50;
         end loop;
         DBMS_SQL.CLOSE_CURSOR(L_CURSOR);
         return L_NUM_ARR;
      end FIND_COLLECTION;
      function FIND_COLLECTION(A_COLLECTION in out nocopy DATE_SORT_TABLE_T,
                               A_FILTER     varchar2) return DBMS_SQL.NUMBER_TABLE is
         L_NUM_ARR DBMS_SQL.NUMBER_TABLE;
         L_CURSOR  integer;
         L_ROWS    integer;
      begin
         L_CURSOR := DBMS_SQL.OPEN_CURSOR;
         DBMS_SQL.PARSE(L_CURSOR,
                        'select n from (SELECT rownum n,column_value  FROM   TABLE(:collection) ) where  column_value ' ||
                        A_FILTER || '  ORDER  BY 1 ',
                        DBMS_SQL.V7);
         DBMS_SQL.BIND_VARIABLE(L_CURSOR, 'collection', A_COLLECTION);
         DBMS_SQL.DEFINE_ARRAY(L_CURSOR, 1, L_NUM_ARR, 50, 1);
         L_ROWS := DBMS_SQL.EXECUTE(L_CURSOR);
         loop
            L_ROWS := DBMS_SQL.FETCH_ROWS(L_CURSOR);
            DBMS_SQL.COLUMN_VALUE(L_CURSOR, 1, L_NUM_ARR);
            exit when L_ROWS <> 50;
         end loop;
         DBMS_SQL.CLOSE_CURSOR(L_CURSOR);
         return L_NUM_ARR;
      end FIND_COLLECTION;
      function FIND_COLLECTION(A_COLLECTION in out nocopy TIMESTAMP_SORT_TABLE_T,
                               A_FILTER     varchar2) return DBMS_SQL.NUMBER_TABLE is
         L_NUM_ARR DBMS_SQL.NUMBER_TABLE;
         L_CURSOR  integer;
         L_ROWS    integer;
      begin
         L_CURSOR := DBMS_SQL.OPEN_CURSOR;
         DBMS_SQL.PARSE(L_CURSOR,
                        'select n from (SELECT rownum n,column_value  FROM   TABLE(:collection) ) where  column_value ' ||
                        A_FILTER || '  ORDER  BY 1 ',
                        DBMS_SQL.V7);
         DBMS_SQL.BIND_VARIABLE(L_CURSOR, 'collection', A_COLLECTION);
         DBMS_SQL.DEFINE_ARRAY(L_CURSOR, 1, L_NUM_ARR, 50, 1);
         L_ROWS := DBMS_SQL.EXECUTE(L_CURSOR);
         loop
            L_ROWS := DBMS_SQL.FETCH_ROWS(L_CURSOR);
            DBMS_SQL.COLUMN_VALUE(L_CURSOR, 1, L_NUM_ARR);
            exit when L_ROWS <> 50;
         end loop;
         DBMS_SQL.CLOSE_CURSOR(L_CURSOR);
         return L_NUM_ARR;
      end FIND_COLLECTION;
   begin
      G_DS(A_DS_NAME).FIND_LIST.DELETE;
      G_DS(A_DS_NAME).FIND_LAST_POS := 0;

      L_POS      := GETCOLUMNPOS(A_DS_NAME, A_COL_NAME);
      L_TYPE_POS := G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE_POS;
      L_TYPE_POS := G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE_POS;
      if A_START_ROW = 0 then
         L_START_ROW := 1;
      else
         L_START_ROW := A_START_ROW;
      end if;
      if A_END_ROW = 0 then
         L_END_ROW := G_DS(A_DS_NAME).ROW_MAP.COUNT;
      else
         L_END_ROW := A_END_ROW;
      end if;
      case
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS)
          .COL_TYPE = 1 -- varchar2
              or G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 9 -- varchar
              or G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 96 then
            -- char
            for I in L_START_ROW .. L_END_ROW
            loop
               L_VARCHAR2_FIND_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_VARCHAR2_FIND_TABLE(I) := G_DS(A_DS_NAME).COL_VAR2_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_FIND_ROWS := FIND_COLLECTION(L_VARCHAR2_FIND_TABLE, A_FILTER);

         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 2 then
            -- number
            for I in L_START_ROW .. L_END_ROW
            loop
               L_NUM_FIND_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_NUM_FIND_TABLE(I) := G_DS(A_DS_NAME).COL_NUM_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_FIND_ROWS := FIND_COLLECTION(L_NUM_FIND_TABLE, A_FILTER);
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 12 then
            -- date
            for I in L_START_ROW .. L_END_ROW
            loop
               L_DATE_FIND_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_DATE_FIND_TABLE(I) := G_DS(A_DS_NAME).COL_DATE_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_FIND_ROWS := FIND_COLLECTION(L_NUM_FIND_TABLE, A_FILTER);
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 112 then
            -- clob
            for I in L_START_ROW .. L_END_ROW
            loop
               L_VARCHAR2_FIND_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_VARCHAR2_FIND_TABLE(I) := G_DS(A_DS_NAME).COL_VAR2_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_FIND_ROWS := FIND_COLLECTION(L_VARCHAR2_FIND_TABLE, A_FILTER);
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 113 then
            -- blob
            null;
         when G_DS(A_DS_NAME).COLUMN_MAP(L_POS).COL_TYPE = 187 then
            -- timestamp
            for I in L_START_ROW .. L_END_ROW
            loop
               L_TS_FIND_TABLE.EXTEND;
               L_ROW := G_DS(A_DS_NAME).ROW_MAP(I);
               L_TS_FIND_TABLE(I) := G_DS(A_DS_NAME).COL_TS_TABLES(L_TYPE_POS) (L_ROW);
            end loop;
            L_FIND_ROWS := FIND_COLLECTION(L_TS_FIND_TABLE, A_FILTER);
         else
            null;
      end case;
      if L_FIND_ROWS.COUNT = 0 then
         return -1;
      end if;
      G_DS(A_DS_NAME).FIND_LAST_POS := L_FIND_ROWS(1);
      G_DS(A_DS_NAME).FIND_LIST := L_FIND_ROWS;

      return L_FIND_ROWS(1);
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE Find ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end FIND;
   /**************************************  Find next *************************************************/
   function FINDNEXT(A_DS_NAME varchar2) return integer is
      L_CODE  pls_integer;
      L_COUNT pls_integer;
   begin
      L_COUNT := G_DS(A_DS_NAME).FIND_LIST.COUNT;
      if L_COUNT > G_DS(A_DS_NAME).FIND_LAST_POS then
         G_DS(A_DS_NAME).FIND_LAST_POS := G_DS(A_DS_NAME).FIND_LAST_POS + 1;
         return G_DS(A_DS_NAME).FIND_LIST(G_DS(A_DS_NAME).FIND_LAST_POS);
      end if;
      return -1;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE FindNext ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end FINDNEXT;
   /**************************************  Column Count *************************************************/
   function COLUMNCOUNT(A_DS_NAME varchar2) return integer is
      L_CODE pls_integer;
   begin
      return G_DS(A_DS_NAME).COLUMN_MAP.COUNT;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE ColumnCount ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
   end COLUMNCOUNT;

   /************************************** Display Rows *************************************************/
   function DISPLAYROWS(A_DS_NAME    varchar2,
                        A_COL_OPTION integer := 0) return T_BIND_VARS
      pipelined is
      L_OUTPUT_LINE varchar2(2000);
      I_ROW         pls_integer;
      I_TOTAL_ROWS  pls_integer;
      I_POS         pls_integer;
      I             integer;
   begin
      L_OUTPUT_LINE := '';
      if A_COL_OPTION = 1 then
         for I_POS in 1 .. G_DS(A_DS_NAME).COLUMN_MAP.COUNT
         loop
            L_OUTPUT_LINE := L_OUTPUT_LINE || G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_NAME;
            if I_POS <> G_DS(A_DS_NAME).COLUMN_MAP.COUNT then
               L_OUTPUT_LINE := L_OUTPUT_LINE || CHR(9);
            end if;
         end loop;
         pipe row(L_OUTPUT_LINE);
      end if;
      I_TOTAL_ROWS := G_DS(A_DS_NAME).ROW_MAP.COUNT;
      for I in 1 .. I_TOTAL_ROWS
      loop
         L_OUTPUT_LINE := '';
         I_ROW         := G_DS(A_DS_NAME).ROW_MAP(I);
         for I_POS in 1 .. G_DS(A_DS_NAME).COLUMN_MAP.COUNT
         loop
            case
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 1 then
                  -- varchar2
                  L_OUTPUT_LINE := L_OUTPUT_LINE || G_DS(A_DS_NAME)
                                  .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE_POS)
                                   (I_ROW);
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 9 then
                  -- varchar
                  L_OUTPUT_LINE := L_OUTPUT_LINE || G_DS(A_DS_NAME)
                                  .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE_POS)
                                   (I_ROW);
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 96 then
                  -- char
                  L_OUTPUT_LINE := L_OUTPUT_LINE || G_DS(A_DS_NAME)
                                  .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE_POS)
                                   (I_ROW);
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 2 then
                  -- number
                  L_OUTPUT_LINE := L_OUTPUT_LINE ||
                                   TO_CHAR(G_DS(A_DS_NAME).COL_NUM_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                           .COL_TYPE_POS) (I_ROW));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 12 then
                  -- date
                  L_OUTPUT_LINE := L_OUTPUT_LINE ||
                                   TO_CHAR(G_DS(A_DS_NAME).COL_DATE_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                            .COL_TYPE_POS) (I_ROW));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 112 then
                  -- clob
                  L_OUTPUT_LINE := L_OUTPUT_LINE ||
                                   TO_CHAR(G_DS(A_DS_NAME).COL_CLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                            .COL_TYPE_POS) (I_ROW));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 113 then
                  -- blob
                  L_OUTPUT_LINE := L_OUTPUT_LINE || ' ';
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 187 then
                  -- timestamp
                  L_OUTPUT_LINE := L_OUTPUT_LINE ||
                                   TO_CHAR(G_DS(A_DS_NAME).COL_TS_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                          .COL_TYPE_POS) (I_ROW));
               else
                  null;
            end case;
            if I_POS <> G_DS(A_DS_NAME).COLUMN_MAP.COUNT then
               L_OUTPUT_LINE := L_OUTPUT_LINE || CHR(9);
            end if;
         end loop;
         pipe row(L_OUTPUT_LINE);
      end loop;

   end DISPLAYROWS;
   /************************************** Display Info *************************************************/
   function DISPLAYINFO(A_DS_NAME varchar2) return T_BIND_VARS
      pipelined is
      L_OUTPUT_LINE varchar2(2000);
      I_ROW         pls_integer;
      I_TOTAL_ROWS  pls_integer;
      I_POS         pls_integer;
      I             integer;
   begin
      L_OUTPUT_LINE := '';

      for I_POS in 1 .. G_DS(A_DS_NAME).COLUMN_MAP.COUNT
      loop
         L_OUTPUT_LINE := TO_CHAR(I_POS) || CHR(9);
         L_OUTPUT_LINE := L_OUTPUT_LINE || G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_NAME || CHR(9);
         if I_POS <> G_DS(A_DS_NAME).COLUMN_MAP.COUNT then
            L_OUTPUT_LINE := L_OUTPUT_LINE || CHR(9);
         end if;
         case
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 1 then
               -- varchar2
               L_OUTPUT_LINE := L_OUTPUT_LINE || 'varchar2' || CHR(9);
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 9 then
               -- varchar
               L_OUTPUT_LINE := L_OUTPUT_LINE || 'varchar' || CHR(9);
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 96 then
               -- char
               L_OUTPUT_LINE := L_OUTPUT_LINE || 'char' || CHR(9);
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 2 then
               -- number
               L_OUTPUT_LINE := L_OUTPUT_LINE || 'number' || CHR(9);
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 12 then
               -- date
               L_OUTPUT_LINE := L_OUTPUT_LINE || 'date' || CHR(9);
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 112 then
               -- clob
               L_OUTPUT_LINE := L_OUTPUT_LINE || 'clob' || CHR(9);
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 113 then
               -- blob
               L_OUTPUT_LINE := L_OUTPUT_LINE || 'blob' || CHR(9);
            when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 187 then
               -- timestamp
               L_OUTPUT_LINE := L_OUTPUT_LINE || 'timestamp' || CHR(9);
            else
               null;
         end case;
         pipe row(L_OUTPUT_LINE);
      end loop;

   end DISPLAYINFO;

   /************************************** Display Info *************************************************/
   function DISPLAYTABLE(A_DS_NAME    varchar2,
                         A_COL_OPTION integer := 0) return T_TABLE_VARS
      pipelined is
      L_OUTPUT_LINE varchar2(2000);
      I_ROW         pls_integer;
      I_TOTAL_ROWS  pls_integer;
      I_POS         pls_integer;
      I             integer;
      L_TABLES      T_TABLE_VARS := T_TABLE_VARS();
   begin
      L_OUTPUT_LINE := '';
      I_TOTAL_ROWS  := G_DS(A_DS_NAME).ROW_MAP.COUNT;
      L_TABLES.EXTEND;
      L_TABLES(1) := T_BIND_VARS();
      L_TABLES(1).EXTEND(G_DS(A_DS_NAME).COLUMN_MAP.COUNT);
      for I in 1 .. I_TOTAL_ROWS
      loop
         L_OUTPUT_LINE := '';
         I_ROW         := G_DS(A_DS_NAME).ROW_MAP(I);
         for I_POS in 1 .. G_DS(A_DS_NAME).COLUMN_MAP.COUNT
         loop

            case
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 1 then
                  -- varchar2
                  L_TABLES(1)(I_POS) := G_DS(A_DS_NAME)
                                        .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                         .COL_TYPE_POS) (I_ROW);
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 9 then
                  -- varchar
                  L_TABLES(1)(I_POS) := G_DS(A_DS_NAME)
                                        .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                         .COL_TYPE_POS) (I_ROW);
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 96 then
                  -- char
                  L_TABLES(1)(I_POS) := G_DS(A_DS_NAME)
                                        .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                         .COL_TYPE_POS) (I_ROW);
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 2 then
                  -- number
                  L_TABLES(1)(I_POS) := TO_CHAR(G_DS(A_DS_NAME).COL_NUM_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                .COL_TYPE_POS) (I_ROW));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 12 then
                  -- date
                  L_TABLES(1)(I_POS) := TO_CHAR(G_DS(A_DS_NAME).COL_DATE_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                 .COL_TYPE_POS) (I_ROW));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 112 then
                  -- clob
                  L_TABLES(1)(I_POS) := TO_CHAR(G_DS(A_DS_NAME).COL_CLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                                 .COL_TYPE_POS) (I_ROW));
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 113 then
                  -- blob
                  L_TABLES(1)(I_POS) := ' ';
               when G_DS(A_DS_NAME).COLUMN_MAP(I_POS).COL_TYPE = 187 then
                  -- timestamp
                  L_TABLES(1)(I_POS) := TO_CHAR(G_DS(A_DS_NAME).COL_TS_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(I_POS)
                                                               .COL_TYPE_POS) (I_ROW));
               else
                  null;
            end case;
         end loop;
         pipe row(L_TABLES(1));
      end loop;

   end DISPLAYTABLE;
   /************************************** Destory DateStore *************************************************/

   function DSDESTROY(A_DS_NAME varchar2) return integer is
      I      pls_integer;
      L_CODE pls_integer;
   begin
      for I in 1 .. G_DS(A_DS_NAME).COL_VAR2_TABLES.COUNT
      loop
         G_DS(A_DS_NAME).COL_VAR2_TABLES(I).DELETE;
      end loop;
      G_DS(A_DS_NAME).COL_VAR2_TABLES.DELETE;

      for I in 1 .. G_DS(A_DS_NAME).COL_NUM_TABLES.COUNT
      loop
         G_DS(A_DS_NAME).COL_NUM_TABLES(I).DELETE;
      end loop;
      G_DS(A_DS_NAME).COL_NUM_TABLES.DELETE;

      for I in 1 .. G_DS(A_DS_NAME).COL_DATE_TABLES.COUNT
      loop
         G_DS(A_DS_NAME).COL_DATE_TABLES(I).DELETE;
      end loop;
      G_DS(A_DS_NAME).COL_DATE_TABLES.DELETE;

      for I in 1 .. G_DS(A_DS_NAME).COL_CLOB_TABLES.COUNT
      loop
         G_DS(A_DS_NAME).COL_CLOB_TABLES(I).DELETE;
      end loop;
      G_DS(A_DS_NAME).COL_CLOB_TABLES.DELETE;

      for I in 1 .. G_DS(A_DS_NAME).COL_BLOB_TABLES.COUNT
      loop
         G_DS(A_DS_NAME).COL_BLOB_TABLES(I).DELETE;
      end loop;
      G_DS(A_DS_NAME).COL_BLOB_TABLES.DELETE;

      for I in 1 .. G_DS(A_DS_NAME).COL_TS_TABLES.COUNT
      loop
         G_DS(A_DS_NAME).COL_TS_TABLES(I).DELETE;
      end loop;
      G_DS(A_DS_NAME).COL_TS_TABLES.DELETE;

      if not G_DS(A_DS_NAME).FIND_LIST is null then
         G_DS(A_DS_NAME).FIND_LIST.DELETE;
      end if;
      if not G_DS(A_DS_NAME).COLUMN_MAP is null then
         G_DS(A_DS_NAME).COLUMN_MAP.DELETE;
      end if;
      if not G_DS(A_DS_NAME).ROW_MAP is null then
         G_DS(A_DS_NAME).ROW_MAP.DELETE;
      end if;
      if not G_DS(A_DS_NAME).FILTER_ROW_MAP is null then
         G_DS(A_DS_NAME).FILTER_ROW_MAP.DELETE;
      end if;
      if not G_DS(A_DS_NAME).ROW_MODIFY is null then
         G_DS(A_DS_NAME).ROW_MODIFY.DELETE;
      end if;
      if not G_DS(A_DS_NAME).UPDATEABLE_COLUMNS is null then
         G_DS(A_DS_NAME).UPDATEABLE_COLUMNS.DELETE;
      end if;
      if not G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS is null then
         G_DS(A_DS_NAME).PRIMARY_KEY_COLUMNS.DELETE;
      end if;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE DSDestroy ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end DSDESTROY;

   /************************************** Retrieve *************************************************/
   function RETRIEVE(A_DS_NAME     varchar2,
                     A_BIND_VARS   in T_BIND_VARS := null,
                     A_BIND_TYPES  in T_BIND_VARS := null,
                     A_BIND_VALUES in T_BIND_VARS := null) return number is
      L_COL_DESC          DBMS_SQL.DESC_TAB2;
      L_COL_COUNT         pls_integer;
      L_CURSOR            integer;
      L_ROWS              pls_integer;
      I                   pls_integer;
      J                   pls_integer;
      K                   integer;
      L_RETRIEVE_SIZE     pls_integer;
      L_CODE              pls_integer;
      L_TOTAL_ROWS        pls_integer;
      L_COL_NUM_TABLES    T_NUMBER_TABLE; --- type 2
      L_NUM_TABLES_COUNT  pls_integer;
      L_COL_VAR2_TABLES   T_VARCHAR2_TABLE; -- type 1 ,95 and 9
      L_VAR2_TABLES_COUNT pls_integer;
      L_COL_DATE_TABLES   T_DATE_TABLE; --- type 12
      L_DATE_TABLES_COUNT pls_integer;
      L_COL_BLOB_TABLES   T_BLOB_TABLE; -- type 113
      L_BLOB_TABLES_COUNT pls_integer;
      L_COL_CLOB_TABLES   T_CLOB_TABLE; -- type 112
      L_CLOB_TABLES_COUNT pls_integer;
      L_COL_TS_TABLES     T_TIMESTAMP_TABLE; -- type 187
      L_TS_TABLES_COUNT   pls_integer;
   begin
      --xx := t_bind_vars('');
      L_RETRIEVE_SIZE := 10000;
      G_DS(A_DS_NAME).NUM_TABLES_COUNT := 0;
      G_DS(A_DS_NAME).VAR2_TABLES_COUNT := 0;
      G_DS(A_DS_NAME).DATE_TABLES_COUNT := 0;
      G_DS(A_DS_NAME).BLOB_TABLES_COUNT := 0;
      G_DS(A_DS_NAME).CLOB_TABLES_COUNT := 0;
      G_DS(A_DS_NAME).TS_TABLES_COUNT := 0;

      L_CURSOR := G_DS(A_DS_NAME).R_CURSOR;
      if DBMS_SQL.IS_OPEN(L_CURSOR) = false then
         RAISE_APPLICATION_ERROR(-20021, 'PP_DATASTORE Retrieve: Cursor is not open.');
         return -1;
      end if;

      if not A_BIND_VARS is null or not A_BIND_TYPES is null or not A_BIND_VALUES is null then
         for I in 1 .. A_BIND_VARS.COUNT
         loop
            case
               when A_BIND_TYPES(I) = 'varchar2' then
                  -- varchar2
                  DBMS_SQL.BIND_VARIABLE(L_CURSOR, A_BIND_VARS(I), A_BIND_VALUES(I));
               when A_BIND_TYPES(I) = 'varchar' then
                  -- varchar
                  DBMS_SQL.BIND_VARIABLE(L_CURSOR, A_BIND_VARS(I), A_BIND_VALUES(I));
               when A_BIND_TYPES(I) = 'char' then
                  -- char
                  DBMS_SQL.BIND_VARIABLE(L_CURSOR, A_BIND_VARS(I), A_BIND_VALUES(I));
               when A_BIND_TYPES(I) = 'number' then
                  -- number
                  DBMS_SQL.BIND_VARIABLE(L_CURSOR, A_BIND_VARS(I), TO_NUMBER(A_BIND_VALUES(I)));
               when A_BIND_TYPES(I) = 'date' then
                  -- date
                  DBMS_SQL.BIND_VARIABLE(L_CURSOR,
                                         A_BIND_VARS(I),
                                         TO_DATE(A_BIND_VALUES(I), 'MM-DD-YYYY H24:MI:SS'));
               when A_BIND_TYPES(I) = 'clob' then
                  -- clob
                  DBMS_SQL.BIND_VARIABLE(L_CURSOR, A_BIND_VARS(I), A_BIND_VALUES(I));
               when A_BIND_TYPES(I) = 'blob' then
                  -- blob
                  DBMS_SQL.BIND_VARIABLE(L_CURSOR, A_BIND_VARS(I), A_BIND_VALUES(I));
               when A_BIND_TYPES(I) = 'timestamp' then
                  -- timestamp
                  DBMS_SQL.BIND_VARIABLE(L_CURSOR, A_BIND_VARS(I), TO_TIMESTAMP(A_BIND_VALUES(I)));
               else
                  null;
            end case;
         end loop;
      end if;
      G_DS(A_DS_NAME).COL_VAR2_TABLES := T_VARCHAR2_TABLE();
      G_DS(A_DS_NAME).COL_NUM_TABLES := T_NUMBER_TABLE();
      G_DS(A_DS_NAME).COL_DATE_TABLES := T_DATE_TABLE();
      G_DS(A_DS_NAME).COL_CLOB_TABLES := T_CLOB_TABLE();
      G_DS(A_DS_NAME).COL_BLOB_TABLES := T_BLOB_TABLE();
      G_DS(A_DS_NAME).COL_TS_TABLES := T_TIMESTAMP_TABLE();
      L_COL_COUNT := G_DS(A_DS_NAME).COLUMN_MAP.COUNT;
      for J in 1 .. G_DS(A_DS_NAME).COLUMN_MAP.COUNT
      loop

         case
            when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 1 then
               -- varchar2
               G_DS(A_DS_NAME).VAR2_TABLES_COUNT := G_DS(A_DS_NAME).VAR2_TABLES_COUNT + 1;
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS := G_DS(A_DS_NAME).VAR2_TABLES_COUNT;
               G_DS(A_DS_NAME).COL_VAR2_TABLES.EXTEND;
               DBMS_SQL.DEFINE_ARRAY(L_CURSOR,
                                     J,
                                     G_DS(A_DS_NAME)
                                     .COL_VAR2_TABLES(G_DS(A_DS_NAME).VAR2_TABLES_COUNT),
                                     L_RETRIEVE_SIZE,
                                     1);
            when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 9 then
               -- varchar
               G_DS(A_DS_NAME).VAR2_TABLES_COUNT := G_DS(A_DS_NAME).VAR2_TABLES_COUNT + 1;
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS := G_DS(A_DS_NAME).VAR2_TABLES_COUNT;
               G_DS(A_DS_NAME).COL_VAR2_TABLES.EXTEND;
               DBMS_SQL.DEFINE_ARRAY(L_CURSOR,
                                     J,
                                     G_DS(A_DS_NAME)
                                     .COL_VAR2_TABLES(G_DS(A_DS_NAME).VAR2_TABLES_COUNT),
                                     L_RETRIEVE_SIZE,
                                     1);
            when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 96 then
               -- char
               G_DS(A_DS_NAME).VAR2_TABLES_COUNT := G_DS(A_DS_NAME).VAR2_TABLES_COUNT + 1;
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS := G_DS(A_DS_NAME).VAR2_TABLES_COUNT;
               G_DS(A_DS_NAME).COL_VAR2_TABLES.EXTEND;
               DBMS_SQL.DEFINE_ARRAY(L_CURSOR,
                                     J,
                                     G_DS(A_DS_NAME)
                                     .COL_VAR2_TABLES(G_DS(A_DS_NAME).VAR2_TABLES_COUNT),
                                     L_RETRIEVE_SIZE,
                                     1);
            when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 2 then
               -- number
               G_DS(A_DS_NAME).NUM_TABLES_COUNT := G_DS(A_DS_NAME).NUM_TABLES_COUNT + 1;
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS := G_DS(A_DS_NAME).NUM_TABLES_COUNT;
               G_DS(A_DS_NAME).COL_NUM_TABLES.EXTEND;
               DBMS_SQL.DEFINE_ARRAY(L_CURSOR,
                                     J,
                                     G_DS(A_DS_NAME)
                                     .COL_NUM_TABLES(G_DS(A_DS_NAME).NUM_TABLES_COUNT),
                                     L_RETRIEVE_SIZE,
                                     1);
            when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 12 then
               -- date
               G_DS(A_DS_NAME).DATE_TABLES_COUNT := G_DS(A_DS_NAME).DATE_TABLES_COUNT + 1;
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS := G_DS(A_DS_NAME).DATE_TABLES_COUNT;
               G_DS(A_DS_NAME).COL_DATE_TABLES.EXTEND;
               DBMS_SQL.DEFINE_ARRAY(L_CURSOR,
                                     J,
                                     G_DS(A_DS_NAME)
                                     .COL_DATE_TABLES(G_DS(A_DS_NAME).DATE_TABLES_COUNT),
                                     L_RETRIEVE_SIZE,
                                     1);
            when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 112 then
               -- clob
               G_DS(A_DS_NAME).CLOB_TABLES_COUNT := G_DS(A_DS_NAME).CLOB_TABLES_COUNT + 1;
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS := G_DS(A_DS_NAME).CLOB_TABLES_COUNT;
               G_DS(A_DS_NAME).COL_CLOB_TABLES.EXTEND;
               DBMS_SQL.DEFINE_ARRAY(L_CURSOR,
                                     J,
                                     G_DS(A_DS_NAME)
                                     .COL_CLOB_TABLES(G_DS(A_DS_NAME).CLOB_TABLES_COUNT),
                                     L_RETRIEVE_SIZE,
                                     1);
            when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 113 then
               -- blob
               G_DS(A_DS_NAME).BLOB_TABLES_COUNT := G_DS(A_DS_NAME).BLOB_TABLES_COUNT + 1;
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS := G_DS(A_DS_NAME).BLOB_TABLES_COUNT;
               G_DS(A_DS_NAME).COL_BLOB_TABLES.EXTEND;
               DBMS_SQL.DEFINE_ARRAY(L_CURSOR,
                                     J,
                                     G_DS(A_DS_NAME)
                                     .COL_BLOB_TABLES(G_DS(A_DS_NAME).BLOB_TABLES_COUNT),
                                     L_RETRIEVE_SIZE,
                                     1);
            when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 187 then
               -- timestamp
               G_DS(A_DS_NAME).TS_TABLES_COUNT := G_DS(A_DS_NAME).TS_TABLES_COUNT + 1;
               G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS := G_DS(A_DS_NAME).TS_TABLES_COUNT;
               G_DS(A_DS_NAME).COL_TS_TABLES.EXTEND;
               DBMS_SQL.DEFINE_ARRAY(L_CURSOR,
                                     J,
                                     G_DS(A_DS_NAME).COL_TS_TABLES(G_DS(A_DS_NAME).TS_TABLES_COUNT),
                                     L_RETRIEVE_SIZE,
                                     1);
            else
               null;
         end case;
      end loop;

      --dbms_sql.bind_variable(l_cursor,':z',report_table);
      --dbms_sql.define_column(user_cursor,1,counts);
      L_TOTAL_ROWS := 0;
      G_DS(A_DS_NAME).ROW_MODIFY := T_ROW_MODIFY();
      L_ROWS := DBMS_SQL.EXECUTE(L_CURSOR);
      loop
         L_ROWS := DBMS_SQL.FETCH_ROWS(L_CURSOR);
         for J in 1 .. L_COL_COUNT
         loop
            case
               when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 1 then
                  -- varchar2
                  DBMS_SQL.COLUMN_VALUE(L_CURSOR,
                                        J,
                                        G_DS(A_DS_NAME)
                                        .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS));
               when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 9 then
                  -- varchar
                  DBMS_SQL.COLUMN_VALUE(L_CURSOR,
                                        J,
                                        G_DS(A_DS_NAME)
                                        .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS));
               when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 96 then
                  -- char
                  DBMS_SQL.COLUMN_VALUE(L_CURSOR,
                                        J,
                                        G_DS(A_DS_NAME)
                                        .COL_VAR2_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS));
               when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 2 then
                  -- number
                  DBMS_SQL.COLUMN_VALUE(L_CURSOR,
                                        J,
                                        G_DS(A_DS_NAME)
                                        .COL_NUM_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS));
               when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 12 then
                  -- date
                  DBMS_SQL.COLUMN_VALUE(L_CURSOR,
                                        J,
                                        G_DS(A_DS_NAME)
                                        .COL_DATE_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS));
               when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 112 then
                  -- clob
                  DBMS_SQL.COLUMN_VALUE(L_CURSOR,
                                        J,
                                        G_DS(A_DS_NAME)
                                        .COL_CLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS));
               when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 113 then
                  -- blob
                  DBMS_SQL.COLUMN_VALUE(L_CURSOR,
                                        J,
                                        G_DS(A_DS_NAME)
                                        .COL_BLOB_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS));
               when G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE = 187 then
                  -- timestamp
                  DBMS_SQL.COLUMN_VALUE(L_CURSOR,
                                        J,
                                        G_DS(A_DS_NAME)
                                        .COL_TS_TABLES(G_DS(A_DS_NAME).COLUMN_MAP(J).COL_TYPE_POS));
               else
                  null;
            end case;
         end loop;
         for K in 1 .. L_ROWS
         loop
            G_DS(A_DS_NAME).ROW_MODIFY.EXTEND;
            G_DS(A_DS_NAME).ROW_MODIFY(K + L_TOTAL_ROWS) := 'N';
            G_DS(A_DS_NAME).ROW_MAP.EXTEND;
            G_DS(A_DS_NAME).ROW_MAP(K + L_TOTAL_ROWS) := K + L_TOTAL_ROWS;
         end loop;
         L_TOTAL_ROWS := L_ROWS + L_TOTAL_ROWS;
         if L_ROWS <> L_RETRIEVE_SIZE then
            exit;
         end if;
      end loop;

      /*     for i in 1..g_ds(a_ds_name).col_var2_tables(1).count loop
          dbms_output.put_line(to_char(i) || ' ' || g_ds(a_ds_name).col_var2_tables(1)(i) );
      end loop;
      for j in 1..l_col_count loop
              dbms_output.put_line('Column=' ||  l_col_desc(j).col_name || ' Column Type=' || to_char(l_col_desc(j).col_type) );
      end loop;*/
      DBMS_SQL.CLOSE_CURSOR(L_CURSOR);
      return L_TOTAL_ROWS;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'PP_DATASTORE Retrieve ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end RETRIEVE;

begin
   null;
end PP_DATASTORE;
/

--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (3023, 0, 2015, 2, 1, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2015.2.1.0_PP_DATASTORE.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
