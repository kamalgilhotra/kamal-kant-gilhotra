create or replace package pkg_lease_depr as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_DEPR
   || Description:
   ||============================================================================
   || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By      Reason for Change
   || -------- ---------- --------------  ----------------------------------------
   || 10.4.3.0 06/25/2014 B.Beck          Original Version
   || 10.4.3.0 07/07/2014 Charlie Shilling
   || 2015.2.0 09/29/2015 Andrew Scott    maint-45012.  Change how SL is calced for performance.
   || 2016.1.0 02/26/2016 Anand R         maint-45470.
   || 2017.4.0 06/08/2018 Levine          maint-51388 - Add F_LEASE_DEPR_UOP_ROLLFWD and F_LEASE_DEPR_UOP_UWA
   ||============================================================================
   */
	G_PKG_VERSION varchar(35) := '2018.1.0.0';

  FUNCTION F_CALC_COMPANY_EXPENSE(A_LS_ASSET_ID IN NUMBER,
                                  A_REVISION    IN NUMBER,
                                  A_ASSET_ID    IN NUMBER,
                                  A_SOB_ID      IN NUMBER,
                                  A_START_MONTH DATE) RETURN VARCHAR2;

  procedure P_DEPR_SLE(A_LS_ASSET_ID in number,
                       A_REVISION    in number,
                       A_ASSET_ID    in number,
                       A_SOB_ID      in number,
                       A_MONTH       date,
                       A_LIFE        number,
                       A_MSG         out varchar2);

  procedure P_DEPR_SL(A_LS_ASSET_ID in number,
                      A_REVISION    in number,
                      A_ASSET_ID    in number,
                      A_SOB_ID      in number,
                      A_MONTH       date,
                      A_LIFE        number,
                      A_MP_CONV     NUMBER,
                      A_MSG         out varchar2);

  procedure P_FCST_LEASE(A_ILR_ID   in number,
                         A_REVISION in number);

  procedure P_GET_LEASE_DEPR(A_ILR_ID   in number,
                             A_REVISION in number);

  PROCEDURE p_fcst_lease_use_temp_table(a_ilr_id   NUMBER,
                                        a_revision NUMBER);

  FUNCTION F_LEASE_DEPR_UOP_ROLLFWD(A_COMPANY_ID number,
                                    A_MONTH      in date) RETURN VARCHAR2;

  FUNCTION F_LEASE_DEPR_UOP_UWA(A_MONTH in date) RETURN VARCHAR2;

end PKG_LEASE_DEPR;
/
create or replace package body pkg_lease_depr as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_DEPR
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By      Reason for Change
   || -------- ---------- --------------  ----------------------------------------
   || 10.4.3.0 06/25/2014 B.Beck          Original Version
   || 10.4.3.0 07/07/2014 Charlie Shilling
   || 2015.2.0 09/29/2015 Andrew Scott    maint-45012.  Change how SL is calced for performance.
   || 2016.1.0 02/26/2016 Anand R         maint-45470.
   ||============================================================================
   */

	--**************************************************************************
	--                            Start Body
	--**************************************************************************
	--**************************************************************************
	--                            PROCEDURES
	--**************************************************************************
  
  --**************************************************************************
  --                            P_MC_SLE_ROUNDING
  --**************************************************************************
  PROCEDURE P_MC_SLE_ROUNDING(A_LS_ASSET_ID IN NUMBER,
                              A_REVISION    IN NUMBER,
                              A_SOB_ID      IN NUMBER,
                              A_ASSET_ID    IN NUMBER) IS
    L_BOOK_SUMMARY_ID          NUMBER;
    L_CPR_AMOUNT               NUMBER;
    L_ASSET_AMOUNT             NUMBER;
    L_DEPR_METHOD_SLE_AMOUNT   NUMBER;
    L_RESIDUAL_AMOUNT          NUMBER;
    L_IN_SERVICE_EXCHANGE_RATE LS_ILR_OPTIONS.IN_SERVICE_EXCHANGE_RATE%type;
    L_SQLS                     VARCHAR2(4000);
  BEGIN
    pkg_pp_error.set_module_name('PKG_LEASE_DEPR.P_MC_SLE_ROUNDING');

    SELECT fasb.book_summary_id
    INTO L_BOOK_SUMMARY_ID
    FROM ls_asset la
      JOIN ls_ilr_options lio ON lio.ilr_id = la.ilr_id
      JOIN ls_lease_cap_type lct ON lct.ls_lease_cap_type_id =
                                    lio.lease_cap_type_id
      JOIN ls_fasb_cap_type_sob_map fasb ON fasb.lease_cap_type_id =
                                            lio.lease_cap_type_id
    WHERE la.ls_asset_id = A_LS_ASSET_ID
    AND lio.revision = A_REVISION
    AND fasb.set_of_books_id = A_SOB_ID;

    L_SQLS := 'SELECT clb.basis_' || L_BOOK_SUMMARY_ID || ' ' ||
    'FROM cpr_ldg_basis clb ' ||
    ' JOIN ls_cpr_asset_map map ON clb.asset_id = map.asset_id ' ||
    'WHERE map.ls_asset_id = ' || A_LS_ASSET_ID;

    BEGIN
      EXECUTE IMMEDIATE L_SQLS
      INTO L_CPR_AMOUNT;
    EXCEPTION
      WHEN No_Data_Found THEN
        L_CPR_AMOUNT := 0;
    END;

    pkg_pp_log.p_write_message('Existing CPR Asset Amount: ' ||
                               To_Char(L_CPR_AMOUNT));

    SELECT Nvl(Round(psob.posting_amount * lio.in_service_exchange_rate, 2),
                0), lio.in_service_exchange_rate
    INTO L_ASSET_AMOUNT, L_IN_SERVICE_EXCHANGE_RATE
    FROM ls_pend_transaction pt
      JOIN ls_pend_set_of_books psob ON pt.ls_pend_trans_id =
                                        psob.ls_pend_trans_id
      JOIN ls_ilr_options lio ON lio.ilr_id = pt.ilr_id
                             AND lio.revision = pt.revision
    WHERE pt.ls_asset_id = A_LS_ASSET_ID
    AND pt.revision = A_REVISION
    AND psob.set_of_books_id = A_SOB_ID;

    pkg_pp_log.p_write_message('New Asset Addition: ' ||
                               To_Char(L_ASSET_AMOUNT));

    SELECT Nvl(Round(CASE
                       WHEN la.estimated_residual = 0 and lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Amort to Guaranteed Residual',
                                                                                                                       la.COMPANY_ID))) =
                            'yes' THEN
      LA.GUARANTEED_RESIDUAL_AMOUNT
    ELSE
                        Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual',
                                                                                            la.COMPANY_ID))),
                               'no',
                               0,
                               round(la.ESTIMATED_RESIDUAL * la.FMV, 2))
                     END * L_IN_SERVICE_EXCHANGE_RATE,
                     2),
               0)
    INTO L_RESIDUAL_AMOUNT
    FROM ls_asset la
    WHERE ls_asset_id = A_LS_ASSET_ID;

    pkg_pp_log.p_write_message('Asset Residual Amount: ' ||
                               To_Char(L_RESIDUAL_AMOUNT));

    SELECT Nvl(Sum(depr_exp_comp_curr), 0)
    INTO L_DEPR_METHOD_SLE_AMOUNT
    FROM ls_depr_forecast
    WHERE ls_asset_id = A_LS_ASSET_ID
    AND set_of_books_id = A_SOB_ID
    and revision = A_REVISION;

    pkg_pp_log.p_write_message('Total Depr Expense Amount: ' ||
                               To_Char(L_DEPR_METHOD_SLE_AMOUNT));

    IF L_CPR_AMOUNT + L_ASSET_AMOUNT - L_RESIDUAL_AMOUNT =
       L_DEPR_METHOD_SLE_AMOUNT THEN
      pkg_pp_log.p_write_message('Asset and total depr amounts match. No rounding needed.');
    ELSE
      pkg_pp_log.p_write_message('Updating last month with rounding plug: ' ||
                                 To_Char(L_CPR_AMOUNT + L_ASSET_AMOUNT -
                                         L_RESIDUAL_AMOUNT -
                                         L_DEPR_METHOD_SLE_AMOUNT));

      UPDATE ls_depr_forecast
         SET depr_exp_comp_curr = depr_exp_comp_curr +
                                  (L_CPR_AMOUNT + L_ASSET_AMOUNT -
                                   L_RESIDUAL_AMOUNT - L_DEPR_METHOD_SLE_AMOUNT)
      WHERE ls_asset_id = A_LS_ASSET_ID
        AND set_of_books_id = A_SOB_ID
        and revision = A_REVISION
        AND month =
             (SELECT Max(month)
                FROM ls_depr_forecast
               WHERE ls_asset_id = A_LS_ASSET_ID
                 AND set_of_books_id = A_SOB_ID
                 and revision = A_REVISION);
    END if;

    pkg_pp_error.remove_module_name;
  EXCEPTION
  WHEN OTHERS THEN
      pkg_pp_error.Raise_error('ERROR', 'Failed Operation');
  END P_MC_SLE_ROUNDING;

  --**************************************************************************
  --                            F_CALC_COMPANY_EXPENSE
  --  NOTE: This is called from PKG_LEASE_ASSET_POST when an ILR revision
  --        is in-serviced.
  --**************************************************************************
  FUNCTION F_CALC_COMPANY_EXPENSE(A_LS_ASSET_ID IN NUMBER,
                                  A_REVISION    IN NUMBER,
                                  A_ASSET_ID    IN NUMBER,
                                  A_SOB_ID      IN NUMBER,
                                  A_START_MONTH IN DATE) RETURN VARCHAR2 IS
    L_MSG varchar2(4000);
  BEGIN
    -- Populate the depr_exp_comp_curr and exchange_rate fields now that the 
    -- in service/weighted average rates have been finalized
    L_MSG := 'Update LS_DEPR_FORECAST with depreciation expense converted to company currency';
    pkg_pp_log.p_write_message(L_MSG);
    merge into ls_depr_forecast a
    using (
      with war as
       (select ilr_id, A_LS_ASSET_ID ls_asset_id, revision, set_of_books_id,
               net_weighted_avg_rate, effective_month
          from (select war.ilr_id, war.revision, war.set_of_books_id,
                        war.net_weighted_avg_rate,
                        trunc(nvl(lio.remeasurement_date, ilr.est_in_svc_date),
                               'month') effective_month,
                        row_number() over(partition by lia.ilr_id, trunc(nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') order by lia.approval_date DESC) rnum
                   from ls_ilr_weighted_avg_rates war
                   join ls_ilr ilr on war.ilr_id = ilr.ilr_id
                   join ls_ilr_options lio on lio.ilr_id = war.ilr_id
                                          and lio.revision = war.revision
                   join ls_ilr_approval lia on lia.ilr_id = lio.ilr_id
                                           and lia.revision = lio.revision
                   join ls_asset la on la.ilr_id = ilr.ilr_id
                  where war.set_of_books_id = A_SOB_ID
                    and lia.approval_status_id = 3
                    and la.ls_asset_id = A_LS_ASSET_ID
                    and lia.approval_date <=
                        (select approval_date
                           from ls_ilr_approval
                          where ilr_id = ilr.ilr_id
                            and revision = A_REVISION))
         where rnum = 1)
      select f.ls_asset_id, f.set_of_books_id, f.revision, f.month,
             f.depr_expense * war.net_weighted_avg_rate depr_exp_comp_curr,
             war.net_weighted_avg_rate exchange_rate,
             f.depr_exp_alloc_adjust * war.net_weighted_avg_rate depr_exp_aa_comp_curr
        from war
        join ls_depr_forecast f on f.ls_asset_id = war.ls_asset_id
                               and f.set_of_books_id = war.set_of_books_id
                               and f.revision = A_REVISION
       where trunc(f.month,'fmmonth') >= trunc(A_START_MONTH,'fmmonth')
         and f.month >= war.effective_month
         and war.effective_month =
             (select max(effective_month)
                from war eff_war
               where eff_war.ilr_id = war.ilr_id
                 and eff_war.effective_month <= f.month)) b 
    on (    a.ls_asset_id = b.ls_asset_id 
        and a.set_of_books_id = b.set_of_books_id 
        and a.revision = b.revision 
        and a.month = b.month) 
    when matched then
      update set a.depr_exp_comp_curr    = b.depr_exp_comp_curr,
                 a.exchange_rate         = b.exchange_rate,
                 a.depr_exp_aa_comp_curr = b.depr_exp_aa_comp_curr;

    -- Account for any rounding differences due to calculating the monthly depreciation in contract currency
    -- and converting that amount to company currency  
    L_MSG := 'Entering P_MC_SLE_ROUNDING';
    pkg_pp_log.p_write_message(L_MSG);
    P_MC_SLE_ROUNDING(A_LS_ASSET_ID, A_REVISION, A_SOB_ID, A_ASSET_ID);

    RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
      L_MSG := Substr(L_MSG || ': ' || SQLERRM, 1, 4000);
    
      RETURN L_MSG;
  END F_CALC_COMPANY_EXPENSE;

  --**************************************************************************
  --                            P_DEPR_SLE
  --**************************************************************************
  procedure P_DEPR_SLE(A_LS_ASSET_ID in number,
                       A_REVISION    in number,
                       A_ASSET_ID    in number,
                       A_SOB_ID      in number,
                       A_MONTH       date,
                       A_LIFE        number,
                       A_MSG         out varchar2) is
    L_MONTHLY_OBL        number(22, 2);
    L_TOTAL_OBL          number(22, 2);
    L_TOTAL_CHECK        number(22, 2);
    L_NUM_MONTHS         number(22, 0);
    L_ROUND              number(22, 2);
    L_MAX_MONTH          LS_ASSET_SCHEDULE."MONTH"%type;
    L_CUR_MONTH          LS_ASSET_SCHEDULE."MONTH"%type;
    L_START_MONTH        LS_ASSET_SCHEDULE."MONTH"%type;
    L_MONTHLOOP          number;
    l_is_om              number;
    L_IS_REMEASUREMENT   number;
    L_REMEASUREMENT_DATE date;
    L_OWNER_TRF          number;
  begin
    select case
             when purchase_option_type_id = 1 then
              0
             else
              1
           end
      into L_OWNER_TRF
      from ls_ilr_options
        JOIN ls_asset ON ls_asset.ilr_id = ls_ilr_options.ilr_id
      where ls_asset.ls_asset_id = a_ls_asset_id
      and ls_ilr_options.revision = a_revision;

      a_msg := 'GET number of months';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
  
	  begin
      select abc."MONTH", add_months(abc."MONTH", num_months - 1),
             num_months, total_expense, abc.is_om, is_remeasurement,
             remeasurement_date
        into L_START_MONTH, L_MAX_MONTH, L_NUM_MONTHS, L_TOTAL_OBL, l_is_om,
             L_IS_REMEASUREMENT, L_REMEASUREMENT_DATE
        from (select las.month, las.is_om, remeasurement.is_remeasurement,
                      Trunc(remeasurement.remeasurement_date, 'month') remeasurement_date,
          -- If this is a partial month asset then need to subtract a month
                      count(1) over() -case
                        when first_value(nvl(las.PARTIAL_MONTH_PERCENT,1))
                         over(partition by
                                  las.ls_asset_id,
                                  las.revision,
                                  las.set_of_books_id order by
                                  las.month) < 1 then
                              1
                            else
                              0
                            end as num_months,
                      Decode(Nvl(is_remeasurement, 0),
                0,
                             sum(las.INTEREST_PAID)
                             over() + las.end_capital_cost,
                             sum(principal_paid)
                             OVER() + sum(las.INTEREST_PAID)
                             over() +
                             Nvl(remeasurement.prior_month_end_nbv, 0) -
                             Nvl(remeasurement.prior_month_end_liability, 0) +
                             Decode(remeasurement.om_to_cap_indicator,
                                    1,
                                    remeasurement.prior_month_end_prepaid_rent -
                                    remeasurement.prior_month_end_deferred_rent -
                                    remeasurement.prior_month_end_arrears_accr,
                                    0) - Decode(nvl(o.partial_termination, 0),
                                                1,
                                                las.partial_term_gain_loss,
                                                0)) - case
                        when la.estimated_residual = 0 and lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual',
                                                                                                                        la.COMPANY_ID))) =
                             'yes' then
                         nvl(la.guaranteed_residual_amount, 0)
                        else
                         la.fmv * la.estimated_residual
                      end AS total_expense,
          row_number() over(order by month) as the_row
				from ls_asset_schedule las
          JOIN ls_asset la ON la.ls_asset_id = las.ls_asset_id
                 JOIN ls_ilr_options o ON o.ilr_id = la.ilr_id
                                      AND o.revision = las.revision
                 left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON las.ls_asset_id =
                                                                                remeasurement.ls_asset_id
                                                                            AND las.revision =
                                                                                remeasurement.revision
                                                                            AND las.set_of_books_id =
                                                                                remeasurement.set_of_books_id
                                                                            AND las.month =
                                                                                Trunc(remeasurement.remeasurement_date,
                                                                                      'month')
				where las.ls_asset_id = A_LS_ASSET_ID
				and las.set_of_books_id = A_SOB_ID
				and las.revision = A_REVISION
				and la.ls_asset_id = A_LS_ASSET_ID
      		and las.is_om = 0
                  and las.MONTH >= Trunc(Nvl(o.remeasurement_date,
                                             To_Date('180001', 'yyyymm')),
                                         'month')) abc
			where the_row = 1;
	  EXCEPTION
	  	WHEN no_data_found THEN
        a_msg := 'OK';
			RETURN;
	  END;

      if A_LIFE > 0 THEN
      L_MONTHLY_OBL := round(L_TOTAL_OBL / L_NUM_MONTHS, 2);
         L_TOTAL_CHECK := L_NUM_MONTHS * L_MONTHLY_OBL;
      L_ROUND       := L_TOTAL_OBL - L_TOTAL_CHECK;

          A_MSG := 'INSERTING depreciation expense for SLE Method';
          PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    
       -- the partial_month_percent is always populated.  It is either the percent for the first and last month or it is 1 so always multiplying will be ok.
      insert into ls_depr_forecast
        (ls_asset_id, set_of_books_id, revision, month, begin_reserve,
         depr_expense, depr_exp_alloc_adjust, end_reserve, depr_group_id,
         mid_period_method, mid_period_conv, partial_month_percent)
        select las.ls_asset_id, las.set_of_books_id, las.revision,
               las."MONTH",
               sum(case
                      when LAS."MONTH" <> L_MAX_MONTH then
                       (L_MONTHLY_OBL * nvl(las.PARTIAL_MONTH_PERCENT,1) -
                       LAS.INTEREST_ACCRUAL)
                      else
                       (L_MONTHLY_OBL * nvl(las.PARTIAL_MONTH_PERCENT,1) -
                       LAS.INTEREST_ACCRUAL + L_ROUND)
                    end) over(partition by las.ls_asset_id, las.set_of_books_id, las.revision order by las.month) -case
                 when LAS."MONTH" <>
                      L_MAX_MONTH then
                  (L_MONTHLY_OBL *
                  nvl(las.PARTIAL_MONTH_PERCENT,1) -
                  LAS.INTEREST_ACCRUAL)
                 else
                  (L_MONTHLY_OBL *
                  nvl(las.PARTIAL_MONTH_PERCENT,1) -
                  LAS.INTEREST_ACCRUAL +
                  L_ROUND)
               end + Nvl(remeasurement.prior_month_end_depr_reserve, 0) as begin_reserve,
               case
                 when LAS."MONTH" <> L_MAX_MONTH then
                  (L_MONTHLY_OBL * nvl(las.PARTIAL_MONTH_PERCENT,1) -
                  LAS.INTEREST_ACCRUAL)
                 else
                  (L_MONTHLY_OBL * nvl(las.PARTIAL_MONTH_PERCENT,1) -
                  LAS.INTEREST_ACCRUAL + L_ROUND)
				   end as depr_expense, 0 as depr_exp_alloc_adjust,
               sum(case
                     when LAS."MONTH" <> L_MAX_MONTH then
                      (L_MONTHLY_OBL * nvl(las.PARTIAL_MONTH_PERCENT,1) -
                      LAS.INTEREST_ACCRUAL)
                     else
                      (L_MONTHLY_OBL * nvl(las.PARTIAL_MONTH_PERCENT,1) -
                      LAS.INTEREST_ACCRUAL + L_ROUND)
                   end) over(partition by las.ls_asset_id, las.set_of_books_id, las.revision order by las.month) + Nvl(remeasurement.prior_month_end_depr_reserve, 0) as end_reserve,
               dg.depr_group_id, 'SLE', dg.mid_period_conv,
               nvl(las.PARTIAL_MONTH_PERCENT,1)
			from ls_asset_schedule las
          join ls_asset a on a.ls_asset_id = las.ls_asset_id
          join depr_group dg on dg.depr_group_id = a.depr_group_id
          left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON las.revision =
                                                                         remeasurement.revision
                                                                     AND las.ls_asset_id =
                                                                         remeasurement.ls_asset_id
                                                                     AND las.set_of_books_id =
                                                                         remeasurement.set_of_books_id
                                                                     AND las.month >=
                                                                         Trunc(remeasurement.remeasurement_date,
                                                                               'month')
			where LAS.LS_ASSET_ID = A_LS_ASSET_ID
			 and LAS.REVISION = A_REVISION
			 and LAS.SET_OF_BOOKS_ID = A_SOB_ID
			 and las.is_om = 0
           and las.month >=
               Trunc(Nvl(L_REMEASUREMENT_DATE, To_Date('180001', 'yyyymm')),
                     'month');

      PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount ||
                                 ' rows inserted into LS_DEPR_FORECAST.');
    
       -- add extra months if A_LIFE > L_NUM_MONTHS.  This is the portion after the lease term
       -- and when the asset is "an owned asset"
       --Only need to handle ls_depr_forecast. For in-servicing/approval, extra months/rows copy directly from LS_DEPR_FORECAST to DEPR_METHOD_SLE
       if A_LIFE > L_NUM_MONTHS and A_REVISION > 0 AND L_OWNER_TRF = 1 then
        for L_MONTHLOOP in L_NUM_MONTHS .. (A_LIFE - 1 - (CASE
                                             WHEN L_IS_REMEASUREMENT = 1 THEN
                                              (A_LIFE - L_NUM_MONTHS)
                                             ELSE
                                              0
                                           END)) loop
          L_CUR_MONTH := add_months(L_START_MONTH, L_MONTHLOOP);

				if l_is_om = 0 then
					insert into LS_DEPR_FORECAST
              (LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, MONTH, begin_reserve,
               depr_expense, depr_exp_alloc_adjust, end_reserve)
              select LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, L_CUR_MONTH,
                     dg.end_reserve,
                     CASE
                       WHEN L_MONTHLOOP <> (A_LIFE - 1) then
                        L_MONTHLY_OBL
                       else
                        L_MONTHLY_OBL + L_ROUND
                     end, 0,
                     dg.end_reserve + CASE
                       WHEN L_MONTHLOOP <> (A_LIFE - 1) then
                        L_MONTHLY_OBL
                       else
                        L_MONTHLY_OBL + L_ROUND
                     end
					from LS_DEPR_FORECAST dg
					where dg.LS_ASSET_ID = A_LS_ASSET_ID
					and dg.REVISION = A_REVISION
					and dg.SET_OF_BOOKS_ID = A_SOB_ID
                 and dg."MONTH" = add_months(L_CUR_MONTH, -1);
				end if;
        end loop;
       end if;
      end if;

      A_MSG := 'OK';
   end P_DEPR_SLE;

  --**************************************************************************
  --                            P_DEPR_SL
  --**************************************************************************
  procedure P_DEPR_SL(A_LS_ASSET_ID in number,
                      A_REVISION    in number,
                      A_ASSET_ID    in number,
                      A_SOB_ID      in number,
                      A_MONTH       date,
                      A_LIFE        number,
                      A_MP_CONV     NUMBER,
                      A_MSG         out varchar2) is
    L_MONTHLY_EXP         number(22, 2);
    L_TOTAL_EXP           number(22, 2);
    L_TOTAL_CHECK         number(22, 2);
    L_NUM_MONTHS          number(22, 1);
    L_ROUND               number(22, 2);
    L_MAX_MONTH           LS_ASSET_SCHEDULE."MONTH"%type;
    L_START_MONTH         LS_ASSET_SCHEDULE."MONTH"%type;
    L_REMEASUREMENT_DATE  DATE;
    L_BEG_CAP_COST        number(22, 2);
    L_END_CAP_COST        number(22, 2);
    L_BEG_RESERVE         number(22, 2);
    L_REMEASURE_EXP       number(22, 2);
    L_LEASE_START_MONTH   DATE;
    L_MONTH_NUMBER        number(22, 1);
    L_OFF_TO_ON           number(1, 0) := 0;
    L_PRIOR_REVISION      number(22, 0);
    L_PRIOR_INIT_LIFE     number(22, 0);
    L_ILR_ID              number(22, 0);
    L_ECONOMIC_LIFE       number(22, 0);
    L_PARTIAL_MONTH_CHECK number;
    L_MP_CONV             number;
	L_IMPAIRMENT_DATE	  DATE;
	L_RECALC_DATE	  	  DATE;
	L_IS_IMPAIRMENT	  	  NUMBER(1);
	L_IMPAIRMENT_ACTIVITY NUMBER(22,2);
	L_IMPAIRMENT_EXP	  NUMBER(22,2);
	L_BEG_ACCUM_IMPAIR	  NUMBER(22,2);
	L_TOTAL_IMPAIR 		  NUMBER(22,2);
   begin
     -- Check to see if this asset uses partial month payment terms and override the MPC to 1 if it does
     select count(1)
       into L_PARTIAL_MONTH_CHECK
       from ls_asset a
      join (select ilr_id, revision,
                   min(payment_term_type_id) payment_term_type_id
                 from ls_ilr_payment_term
             group by ilr_id, revision) p on p.ilr_id = a.ilr_id
               and p.revision = A_REVISION
      where p.payment_term_type_id = 4
        and a.ls_asset_id = A_LS_ASSET_ID;

     if L_PARTIAL_MONTH_CHECK > 0 then
       L_MP_CONV := 1;
     else
       L_MP_CONV := A_MP_CONV;
     end if;

      -- CJS 5/17/17 Use A_LIFE - 1, otherwise end up with an extra month
	  a_msg := 'GET number of months';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
	  begin
      SELECT Decode(recalc_start_date,
                     NULL,
                     abc.MONTH,
                     Add_Months(abc.MONTH,
                                Months_Between(recalc_start_date,
                                               abc.MONTH))) l_start_month,
             Decode(recalc_start_date,
                     NULL,
                     add_months(abc."MONTH",
                                Decode(recalc_start_date,
                                       null,
                                       Least(A_LIFE, num_months),
                                       num_months) - 1),
                     Add_Months(Add_Months(abc.MONTH,
                                           Months_Between(recalc_start_date,
                                                          abc.MONTH)),
                                Least(A_LIFE, num_months) -
                                Months_Between(recalc_start_date,
                                               abc.MONTH) - 1)) l_max_month,
             Decode(recalc_start_date,
                     NULL,
                     Least(A_LIFE, num_months),
                     Least(A_LIFE, num_months) -
                     Months_Between(recalc_start_date,
                                    abc.MONTH)) l_num_months, total_expense,
             remeasurement_date,
             current_revision, ilr_id, economic_life, impairment_date, nvl(is_impairment,0)
        into L_START_MONTH, L_MAX_MONTH, L_NUM_MONTHS, L_TOTAL_EXP,
             L_REMEASUREMENT_DATE, L_PRIOR_REVISION, L_ILR_ID,
             L_ECONOMIC_LIFE, L_IMPAIRMENT_DATE, L_IS_IMPAIRMENT
        from (select las.month,
				count(1) over() - Decode(Nvl(L_MP_CONV, 1), 1, 0, 1) as num_months,
				row_number() over(order by month) as the_row,
        --Last_Value(end_capital_cost) OVER() is equal to the new end capital cost after remeasurement
                      (Last_Value(end_capital_cost)
                       OVER() -
                       Decode(decode(nvl(o.is_impairment,0), 1, o.impairment_date, o.remeasurement_date), NULL, 0, 1) *
                       decode(nvl(o.is_impairment,0), 1, nvl(impairment.prior_month_end_depr_reserve,0), Nvl(remeasurement.prior_month_end_depr_reserve,0)) - case
                         when (la.ESTIMATED_RESIDUAL = 0 and lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual',
                                                                                                                          la.COMPANY_ID))) =
                              'yes') /* WMD */
                          then
                          la.GUARANTEED_RESIDUAL_AMOUNT
                         else
                          round(la.ESTIMATED_RESIDUAL * la.FMV, 2)
                       end) as total_expense,
		Trunc(o.remeasurement_date, 'month') remeasurement_date, 
		trunc(o.impairment_date, 'month') impairment_date,
        decode(nvl(o.is_impairment,0), 1, trunc(o.impairment_date, 'month'),Trunc(o.remeasurement_date, 'month')) recalc_start_date, --Impairment revisions are flagged
		nvl(o.is_impairment,0) is_impairment,
        nvl(remeasurement.current_revision, ilr.current_revision) current_revision,
        nvl(remeasurement.ilr_id, ilr.ilr_id) ilr_id,
        la.economic_life economic_life
				from ls_asset_schedule las
				 JOIN ls_asset la ON las.ls_asset_id = la.ls_asset_id
				 JOIN ls_ilr_payment_term ilrpt ON la.ilr_id = ilrpt.ilr_id
				 JOIN ls_ilr ilr ON la.ilr_id = ilr.ilr_id
                 JOIN ls_ilr_options o ON o.ilr_id = la.ilr_id
                                      AND o.revision = las.revision
                 left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON las.ls_asset_id =
                                                                                remeasurement.ls_asset_id
                                                                            AND las.revision =
                                                                                remeasurement.revision
                                                                            AND las.set_of_books_id =
                                                                                remeasurement.set_of_books_id
				left outer join v_ls_asset_impairment_amts impairment on (las.ls_asset_id =
																			   impairment.ls_asset_id and
																			   las.revision =
																			   impairment.revision and
																			   las.set_of_books_id =
																			   impairment.set_of_books_id)
            --Does not join on date so the prior month reserve will be included in "the_row = 1"
				where las.ls_asset_id = A_LS_ASSET_ID
				and las.set_of_books_id = A_SOB_ID
				and las.revision = A_REVISION
				and la.ls_asset_id = A_LS_ASSET_ID
			 and ilrpt.revision = A_REVISION
			 and ilrpt.payment_term_id = 1
                  and las.is_om = 0
                  and las.month >= trunc(ilrpt.payment_term_date, 'month')) abc
		  where the_row = 1;
	  exception
			when no_data_found then
				a_msg := 'OK';
				return;
    end;
	
    -- If the ILR is in transition (via remeasurement)then there will be IS_OM = 1 months that are excluded from determining L_MAX_MONTH above.
    -- Need to decrement the date by the number of IS_OM = 1 to get the actual max month.
    -- This should not change the date for calculation with the transition tool (when remeasurement_date is null).
	  a_msg := 'GET Transition Max Month';
    SELECT Add_Months(L_MAX_MONTH, -1 * Nvl(Count(1), 0))
    INTO L_MAX_MONTH
    from ls_asset_schedule las
      JOIN ls_asset la ON la.ls_asset_id = las.ls_asset_id
      JOIN ls_ilr_options o ON o.ilr_id = la.ilr_id
                           AND o.revision = las.revision
      left OUTER JOIN v_ls_asset_remeasurement_amts remeasurement ON las.ls_asset_id =
                                                                     remeasurement.ls_asset_id
                                                                 AND las.revision =
                                                                     remeasurement.revision
                                                                 AND las.set_of_books_id =
                                                                     remeasurement.set_of_books_id
                                                                 AND las.month =
                                                                     Trunc(remeasurement.remeasurement_date,
                                                                           'month')
		where las.ls_asset_id = A_LS_ASSET_ID
		and las.set_of_books_id = A_SOB_ID
		and las.revision = A_REVISION
		and la.ls_asset_id = A_LS_ASSET_ID
    and las.is_om = 1
    and las.MONTH >= Trunc(o.remeasurement_date, 'month');

    -- If the calculated remaining number of months is less than 0, then set remaining months to 1 to expense all remaining depreciaton in the remeasurement month
    -- i.e. economic life is less than the number of lease terms and the asset was fully depreciated prior to remeasurement
    IF L_REMEASUREMENT_DATE IS NOT NULL AND L_NUM_MONTHS <= 0 THEN
      L_NUM_MONTHS := 1;
      L_MAX_MONTH  := L_REMEASUREMENT_DATE;
    END IF;

    -- Calculate the depr expense for the remeasurement month, since we don't want to follow the
    -- same logic with the mid period convention as for the beginning of a lease
    IF L_REMEASUREMENT_DATE IS NOT NULL or L_IMPAIRMENT_DATE IS NOT NULL THEN
	  IF L_IS_IMPAIRMENT = 1 THEN
		L_RECALC_DATE := L_IMPAIRMENT_DATE;
	  ELSE
		L_RECALC_DATE := L_REMEASUREMENT_DATE;
	  END IF;
	  
	  a_msg := 'Retrieving Capital Costs for SL Depr Calc';
	  if l_is_impairment <> 1 then
		  -- Get the capital costs for the asset for the remeasurement month
		  select beg_capital_cost, end_capital_cost, nvl(begin_accum_impair,0), nvl(impairment_activity,0)
			into L_BEG_CAP_COST, L_END_CAP_COST, L_BEG_ACCUM_IMPAIR, L_IMPAIRMENT_ACTIVITY
			from ls_asset_schedule
		   where ls_asset_id = A_LS_ASSET_ID
			 and revision = A_REVISION
			 and set_of_books_id = A_SOB_ID
			 and month = trunc(L_REMEASUREMENT_DATE, 'fmmonth');
	  elsif L_IS_IMPAIRMENT = 1 THEN
		--Get Impairment Activity
		select impairment_activity, begin_accum_impair, beg_capital_cost, end_capital_cost
		into L_IMPAIRMENT_ACTIVITY, L_BEG_ACCUM_IMPAIR, L_BEG_CAP_COST, L_END_CAP_COST
		from ls_asset_schedule
		where ls_asset_id = A_LS_ASSET_ID
		 and revision = A_REVISION
		 and set_of_books_id = A_SOB_ID
		 and month = trunc(L_IMPAIRMENT_DATE, 'fmmonth');
		 
		 
		 --Subtract Impairment Activity out of Total Expense
		 l_total_impair := l_beg_accum_impair + l_impairment_activity;
	  end if;

      -- Get last month's end reserve to use as the beginning reserve for the remeasurement month
      begin
        select end_reserve
          into L_BEG_RESERVE
          from ls_depr_forecast
         where ls_asset_id = A_LS_ASSET_ID
           and revision = A_REVISION
           and set_of_books_id = A_SOB_ID
           and month =
               add_months(trunc(L_RECALC_DATE, 'fmmonth'), -1);
      exception
			when no_data_found then
			-- If this is a remeasurement from off to on, there won't be anything in depr forecast
			L_BEG_RESERVE := 0;
			L_OFF_TO_ON   := 1;
      end;

      -- Calculate the divisor to use for calculating depr expense based on the mid period convention
      -- and the remeasurement month
	  a_msg := 'Retrieving Lease Start Month for SL Depr Calc';
      select min(month)
        into L_LEASE_START_MONTH
        from ls_asset_schedule
       where ls_asset_id = A_LS_ASSET_ID
         and revision = A_REVISION
         and set_of_books_id = A_SOB_ID;

      if NVL(L_MP_CONV,1) = 0 then
        -- For MPC 0 and 1: Need to get the prior revision's life to use as the month number
        -- for figuring out the remeasurement expense because the remeasurement expense
        -- will be calculated incorrectly if the life has changed with the remeasurement
        L_PRIOR_INIT_LIFE := pkg_lease_common.f_get_init_life(A_LS_ASSET_ID, L_PRIOR_REVISION, L_ILR_ID, L_ECONOMIC_LIFE)
                             .init_life;
      else
        -- MPC .5 and 1 should use the new life as of remeasurement
        L_PRIOR_INIT_LIFE := A_LIFE;
      end if;
      L_MONTH_NUMBER := L_PRIOR_INIT_LIFE +
                        months_between(L_LEASE_START_MONTH,
                                       L_RECALC_DATE) +
                        (1 - NVL(L_MP_CONV, 1));

      PKG_PP_LOG.P_WRITE_MESSAGE('L_LEASE_START_MONTH : '|| L_LEASE_START_MONTH);
      PKG_PP_LOG.P_WRITE_MESSAGE('L_RECALC_DATE : '|| L_RECALC_DATE);
      PKG_PP_LOG.P_WRITE_MESSAGE('L_MONTH_NUMBER : ' || L_MONTH_NUMBER);
      PKG_PP_LOG.P_WRITE_MESSAGE('L_BEG_RESERVE : ' || L_BEG_RESERVE);
      PKG_PP_LOG.P_WRITE_MESSAGE('L_BEG_CAP_COST : ' || L_BEG_CAP_COST);
	  if nvl(L_IMPAIRMENT_ACTIVITY, 0) <> 0 then
		PKG_PP_LOG.P_WRITE_MESSAGE('L_IMPAIRMENT_ACTIVITY : ' || L_IMPAIRMENT_ACTIVITY);
		PKG_PP_LOG.P_WRITE_MESSAGE('L_BEG_ACCUM_IMPAIR : ' || L_BEG_ACCUM_IMPAIR);
	  end if;

      -- Calculate the depr expense for the remeasurement month
      L_REMEASURE_EXP := round(case
                                 when NVL(L_MP_CONV, 1) = 1 then
                                      (L_END_CAP_COST - L_BEG_RESERVE - L_BEG_ACCUM_IMPAIR - L_IMPAIRMENT_ACTIVITY) / L_MONTH_NUMBER
                                 when NVL(L_MP_CONV, 1) = 0 then
                                      (L_BEG_CAP_COST - L_BEG_RESERVE - L_BEG_ACCUM_IMPAIR) / L_MONTH_NUMBER
                                 when NVL(L_MP_CONV, 1) = .5 then
                                  (((L_BEG_CAP_COST + L_END_CAP_COST - L_IMPAIRMENT_ACTIVITY) / 2) -
                                  L_BEG_RESERVE - L_BEG_ACCUM_IMPAIR) / L_MONTH_NUMBER
                               end,
                               2);

    END IF;

    -- If the mid period convention is 0 or .5, need to subtract the remeasurement expense from the total
    -- to ensure the last month's end reserve is equal to the new capital cost after remeasurement
    IF L_REMEASUREMENT_DATE IS NOT NULL and NVL(L_MP_CONV, 1) <> 1 and l_is_impairment <> 1 and
       L_OFF_TO_ON = 0 THEN
       L_TOTAL_EXP := L_TOTAL_EXP - L_REMEASURE_EXP;
    END IF;

    -- If the mid period convention is .5, need to subtract .5 from the number of months
    -- to get the monthly expense correct when there is a remeasurement or impairment.  Only do this
    -- if the remeasurement is not from off B/S to on B/S; this case requires a whole
    -- number for the first month.
    IF L_REMEASUREMENT_DATE IS NOT NULL and NVL(L_MP_CONV, 1) = .5 and L_OFF_TO_ON = 0 THEN
	   L_NUM_MONTHS := L_NUM_MONTHS - .5;
    END IF;
	
	IF l_is_impairment = 1 and nvl(l_mp_conv,1) = .5 and nvl(l_impairment_activity,0) <> 0 then
		L_NUM_MONTHS := L_NUM_MONTHS - .5;
	END IF;
	
	--If this is an impairment revision with MPC .5 then we need to subtract remeasurement(impairment) expense out of total
	IF L_IS_IMPAIRMENT = 1 and nvl(l_impairment_activity, 0) <> 0 THEN
		IF nvl(l_mp_conv, 1) = 1 THEN
		   L_TOTAL_EXP := L_TOTAL_EXP - L_TOTAL_IMPAIR;
		elsif nvl(l_impairment_activity,0) <> 0 and nvl(l_mp_conv, 1) <> 1 then
		   L_TOTAL_EXP := L_TOTAL_EXP - L_TOTAL_IMPAIR - L_REMEASURE_EXP;
		end if;
	END IF;

    /* CJS 4/14/17 If we have a MPC of 0.5, will be off by a penny on odd numbers if we don't add check here */
    L_MONTHLY_EXP := round(L_TOTAL_EXP / L_NUM_MONTHS, 2);
    if Nvl(L_MP_CONV, 1) = 0.5 THEN
      L_TOTAL_CHECK := (L_NUM_MONTHS - 1) * L_MONTHLY_EXP +
                       (2 * Round(L_MONTHLY_EXP * Nvl(L_MP_CONV, 1), 2));
    else
      L_TOTAL_CHECK := L_NUM_MONTHS * L_MONTHLY_EXP;
    end if;
    L_ROUND := L_TOTAL_EXP - L_TOTAL_CHECK;

    PKG_PP_LOG.P_WRITE_MESSAGE('A_LIFE : ' || A_LIFE);
    PKG_PP_LOG.P_WRITE_MESSAGE('A_MP_CONV : ' || L_MP_CONV);
    PKG_PP_LOG.P_WRITE_MESSAGE('L_MAX_MONTH : ' || L_MAX_MONTH);
    PKG_PP_LOG.P_WRITE_MESSAGE('L_NUM_MONTHS : ' || L_NUM_MONTHS);
    PKG_PP_LOG.P_WRITE_MESSAGE('L_TOTAL_EXP : ' || L_TOTAL_EXP);
    PKG_PP_LOG.P_WRITE_MESSAGE('L_MONTHLY_EXP : ' || L_MONTHLY_EXP);
	PKG_PP_LOG.P_WRITE_MESSAGE('L_REMEASURE_EXP : ' || L_REMEASURE_EXP);
    PKG_PP_LOG.P_WRITE_MESSAGE('L_TOTAL_CHECK : ' || L_TOTAL_CHECK);
    PKG_PP_LOG.P_WRITE_MESSAGE('L_ROUND : ' || L_ROUND);

	A_MSG := 'INSERTING depreciation expense for SL Method';
	PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    insert into ls_depr_forecast
      (ls_asset_id, set_of_books_id, revision, month, begin_reserve,
       depr_expense, depr_exp_alloc_adjust, end_reserve, depr_group_id,
       mid_period_method, mid_period_conv, partial_month_percent)
		select las.ls_asset_id, las.set_of_books_id, las.revision, las."MONTH",
			     /* CJS 3/7/17 Should only put amounts up to max month, round in max month, then 0 after */
			     /* CJS 4/11/17 Factor in mid-period_method for start month and additional month; in BPO month extensions, include MP Convention in first/last month and extend if needed
			     --Add in a new month for anything besides a MPC of 1; we're assuming 0, 0.5, and 1; if others happen to ever be needed, this will need to be updated to use (1 - MPC) in last month and other changes */
           sum(case 
                WHEN LAS."MONTH" < L_MAX_MONTH AND LAS."MONTH" = L_START_MONTH THEN
                 case when L_REMEASUREMENT_DATE IS NOT NULL and L_OFF_TO_ON = 0 and L_IS_IMPAIRMENT <> 1 then
                   Round(L_REMEASURE_EXP * las.PARTIAL_MONTH_PERCENT, 2)
				 when L_IS_IMPAIRMENT = 1 AND L_IMPAIRMENT_ACTIVITY <> 0 then
					Round(L_REMEASURE_EXP * las.PARTIAL_MONTH_PERCENT, 2)
                 else
                   Round(L_MONTHLY_EXP * Nvl(L_MP_CONV, 1) * las.PARTIAL_MONTH_PERCENT, 2)
                 end
                when LAS."MONTH" < Add_Months(L_MAX_MONTH, Decode(Nvl(L_MP_CONV, 1), 1, 0, 1)) 
                     AND LAS."MONTH" <> L_START_MONTH then
					L_MONTHLY_EXP
                when LAS."MONTH" = Add_Months(L_MAX_MONTH, Decode(Nvl(L_MP_CONV, 1), 1, 0, 1)) then
					Round((L_MONTHLY_EXP * Decode(Nvl(L_MP_CONV, 1), 0, 1, Nvl(L_MP_CONV, 1)) + L_ROUND), 2)
                when LAS."MONTH" = Add_Months(L_MAX_MONTH, Decode(Nvl(L_MP_CONV, 1), 1, 0, 1) + 1) 
                     AND las.last_month_pct <> 1 then
					Round((L_MONTHLY_EXP * Decode(Nvl(L_MP_CONV, 1), 0, 1, Nvl(L_MP_CONV, 1))) * las.last_month_pct, 2)
                else
                     0
               end
              ) over(partition by las.ls_asset_id, las.set_of_books_id, las.revision order by las.month) 
           - case
              WHEN LAS."MONTH" < L_MAX_MONTH AND LAS."MONTH" = L_START_MONTH THEN
               case when L_REMEASUREMENT_DATE IS NOT NULL and L_OFF_TO_ON = 0 and l_is_impairment <> 1 then
                 Round(L_REMEASURE_EXP * las.PARTIAL_MONTH_PERCENT, 2)
			   when L_IS_IMPAIRMENT = 1 AND L_IMPAIRMENT_ACTIVITY <> 0 then
				 Round(L_REMEASURE_EXP * las.PARTIAL_MONTH_PERCENT, 2)
               else
                 Round(L_MONTHLY_EXP * Nvl(L_MP_CONV, 1) * las.PARTIAL_MONTH_PERCENT, 2)
               end
              when LAS."MONTH" < Add_Months(L_MAX_MONTH, Decode(Nvl(L_MP_CONV, 1), 1, 0, 1)) 
                   AND LAS."MONTH" <> L_START_MONTH then
               L_MONTHLY_EXP
             when LAS."MONTH" = Add_Months(L_MAX_MONTH, Decode(Nvl(L_MP_CONV, 1), 1, 0, 1)) then
              Round((L_MONTHLY_EXP * Decode(Nvl(L_MP_CONV, 1), 0, 1, Nvl(L_MP_CONV, 1)) + L_ROUND), 2)
             when LAS."MONTH" = Add_Months(L_MAX_MONTH, Decode(Nvl(L_MP_CONV, 1), 1, 0, 1) + 1) 
                  AND las.last_month_pct <> 1 then
              Round((L_MONTHLY_EXP * Decode(Nvl(L_MP_CONV, 1), 0, 1, Nvl(L_MP_CONV, 1))) * las.last_month_pct, 2)
             else
              0
            end + decode(nvl(L_IS_IMPAIRMENT,0), 1, nvl( impairment.prior_month_end_depr_reserve, 0),  Nvl(remeasurement.prior_month_end_depr_reserve, 0)) as begin_reserve,
             case
              WHEN LAS."MONTH" < L_MAX_MONTH AND LAS."MONTH" = L_START_MONTH THEN
               case when L_REMEASUREMENT_DATE IS NOT NULL and L_OFF_TO_ON = 0 and L_IS_IMPAIRMENT <> 1 then
                 Round(L_REMEASURE_EXP * las.PARTIAL_MONTH_PERCENT, 2)
			   when L_IS_IMPAIRMENT = 1 AND L_IMPAIRMENT_ACTIVITY <> 0 then
			     Round(L_REMEASURE_EXP * las.PARTIAL_MONTH_PERCENT, 2)
               else
                 Round(L_MONTHLY_EXP * Nvl(L_MP_CONV, 1) * las.PARTIAL_MONTH_PERCENT, 2)
               end
              when LAS."MONTH" < Add_Months(L_MAX_MONTH, Decode(Nvl(L_MP_CONV, 1), 1, 0, 1)) 
                   AND LAS."MONTH" <> L_START_MONTH then
               L_MONTHLY_EXP
             when LAS."MONTH" = Add_Months(L_MAX_MONTH, Decode(Nvl(L_MP_CONV, 1), 1, 0, 1)) then
              Round((L_MONTHLY_EXP * Decode(Nvl(L_MP_CONV, 1), 0, 1, Nvl(L_MP_CONV, 1)) + L_ROUND), 2)
             when LAS."MONTH" = Add_Months(L_MAX_MONTH, Decode(Nvl(L_MP_CONV, 1), 1, 0, 1) + 1) 
                  AND las.last_month_pct <> 1 then
              Round((L_MONTHLY_EXP * Decode(Nvl(L_MP_CONV, 1), 0, 1, Nvl(L_MP_CONV, 1))) * las.last_month_pct, 2)
             else
              0
            end as depr_expense, 
            0 as depr_exp_alloc_adjust,
            sum(case
                 WHEN LAS."MONTH" < L_MAX_MONTH AND LAS."MONTH" = L_START_MONTH THEN
                  case when L_REMEASUREMENT_DATE IS NOT NULL and L_OFF_TO_ON = 0 and L_IS_IMPAIRMENT <> 1 then
                    Round(L_REMEASURE_EXP * las.PARTIAL_MONTH_PERCENT, 2)
				  when L_IS_IMPAIRMENT = 1 AND L_IMPAIRMENT_ACTIVITY <> 0 THEN
				    Round(L_REMEASURE_EXP * las.PARTIAL_MONTH_PERCENT, 2)
                  else
                     Round(L_MONTHLY_EXP * Nvl(L_MP_CONV, 1) * las.PARTIAL_MONTH_PERCENT, 2)
                  end
                 when LAS."MONTH" < Add_Months(L_MAX_MONTH, Decode(Nvl(L_MP_CONV, 1), 1, 0, 1)) 
                      AND LAS."MONTH" <> L_START_MONTH then
                  L_MONTHLY_EXP
                 when LAS."MONTH" = Add_Months(L_MAX_MONTH, Decode(Nvl(L_MP_CONV, 1), 1, 0, 1)) then
                  Round((L_MONTHLY_EXP * Decode(Nvl(L_MP_CONV, 1), 0, 1, Nvl(L_MP_CONV, 1)) + L_ROUND), 2)
                 when LAS."MONTH" = Add_Months(L_MAX_MONTH, Decode(Nvl(L_MP_CONV, 1), 1, 0, 1) + 1) 
                      AND las.last_month_pct <> 1 then
                  Round((L_MONTHLY_EXP * Decode(Nvl(L_MP_CONV, 1), 0, 1, Nvl(L_MP_CONV, 1))) * las.last_month_pct, 2)
                 else
                  0
                end) over(partition by las.ls_asset_id, las.set_of_books_id, las.revision order by las.month) 
           + decode(nvl(L_IS_IMPAIRMENT,0), 1, nvl( impairment.prior_month_end_depr_reserve, 0),  Nvl(remeasurement.prior_month_end_depr_reserve, 0)) as end_reserve,
           dg.depr_group_id, dg.mid_period_method, L_MP_CONV,
           las.PARTIAL_MONTH_PERCENT
        from (select las.ls_asset_id, las.set_of_books_id, las.revision,
                      las."MONTH", las.is_om, nvl(las.PARTIAL_MONTH_PERCENT,1) partial_month_percent,
                      Nvl(Last_Value(nvl(las.PARTIAL_MONTH_PERCENT,1))
                           OVER(PARTITION BY las.ls_asset_id,
                                las.set_of_books_id,
                                las.revision ORDER BY las.MONTH
                                ROWS BETWEEN unbounded preceding AND unbounded
                                following),
                           1) last_month_pct
                 from ls_asset_schedule las) las
        join ls_asset la on las.ls_asset_id = la.ls_asset_id
        join depr_group dg on dg.depr_group_id = la.depr_group_id
        join ls_ilr_payment_term ilrpt on la.ilr_id = ilrpt.ilr_id
        left outer join v_ls_asset_remeasurement_amts remeasurement on (las.ls_asset_id =
                                                                       remeasurement.ls_asset_id and
                                                                       las.revision =
                                                                       remeasurement.revision and
                                                                       las.set_of_books_id =
                                                                       remeasurement.set_of_books_id and
                                                                       las.month >=
                                                                       Trunc(remeasurement.remeasurement_date,
                                                                              'month'))
        left outer join v_ls_asset_impairment_amts impairment on (las.ls_asset_id =
                                                                       impairment.ls_asset_id and
                                                                       las.revision =
                                                                       impairment.revision and
                                                                       las.set_of_books_id =
                                                                       impairment.set_of_books_id and
                                                                       las.month >=
                                                                       Trunc(impairment.impairment_date,
                                                                              'month'))
		where LAS.LS_ASSET_ID = A_LS_ASSET_ID
			and LAS.REVISION = A_REVISION
			and LAS.SET_OF_BOOKS_ID = A_SOB_ID
			and ilrpt.revision = A_REVISION
			and ilrpt.payment_term_id = 1
         and las.is_om = 0
         and las.month >= Greatest(trunc(ilrpt.payment_term_date, 'month'),
                                   Trunc(Nvl(L_RECALC_DATE,
                                             To_Date('180001', 'yyyymm')),
                                         'month'));

    PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount ||
                               ' rows inserted into LS_DEPR_FORECAST.');
  
    A_MSG := 'OK';
   end P_DEPR_SL;

  --**************************************************************************
  --                            P_DEPR_FERC_FCST
  --**************************************************************************
  procedure P_DEPR_FERC_FCST(A_ILR_ID   in number,
                             A_REVISION in number) is
   begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_DEPR.P_DEPR_FERC_FCST');

    insert into ls_depr_forecast
      (ls_asset_id, set_of_books_id, revision, month, begin_reserve,
       depr_expense, depr_exp_alloc_adjust, end_reserve, depr_group_id,
       mid_period_method, mid_period_conv, partial_month_percent)
      select las.ls_asset_id, las.set_of_books_id, las.revision, las.month,
             sum(las.principal_accrual) over(partition by las.ls_asset_id, las.set_of_books_id, las.revision order by las.month) - las.principal_accrual + Nvl(remeasurement.prior_month_end_depr_reserve, 0) as begin_reserve,
             las.principal_accrual as depr_expense,
             0 as depr_exp_alloc_adjust,
             sum(las.principal_accrual) over(partition by las.ls_asset_id, las.set_of_books_id, las.revision order by las.month) + Nvl(remeasurement.prior_month_end_depr_reserve, 0) as end_reserve,
             dg.depr_group_id, dg.mid_period_method, dg.mid_period_conv,
             nvl(las.PARTIAL_MONTH_PERCENT,1)
		from ls_asset_schedule las
        join ls_asset la on las.ls_asset_id = la.ls_asset_id
        join depr_group dg on la.depr_group_id = dg.depr_group_id
        join ls_ilr_options lio on lio.ilr_id = la.ilr_id
                               and lio.revision = las.revision
        join ls_fasb_cap_type_sob_map sob_map on sob_map.lease_cap_type_id =
                                                 lio.lease_cap_type_id
                                             and sob_map.set_of_books_id =
                                                 las.set_of_books_id
        left outer join v_ls_asset_remeasurement_amts remeasurement on las.ls_asset_id =
                                                                       remeasurement.ls_asset_id
                                                                   AND las.revision =
                                                                       remeasurement.revision
                                                                   AND las.set_of_books_id =
                                                                       remeasurement.set_of_books_id
                                                                   AND las.month >=
                                                                       Trunc(remeasurement.remeasurement_date,
                                                                             'month')
		where lower(dg.mid_period_method) = 'ferc'
         and lio.ilr_id = A_ILR_ID
         and lio.revision = A_REVISION
		and las.revision = A_REVISION
		and la.ilr_id = A_ILR_ID
		and las.is_om = 0
         and sob_map.fasb_cap_type_id <> 2 --fasb_cap_type_id 2 = Type B, which always uses SLE method, regardless of depr group setting.
         and las.month >= nvl(trunc(lio.remeasurement_date, 'month'),
                              to_date('180001', 'yyyymm'));

    PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount ||
                               ' rows inserted into LS_DEPR_FORECAST.');

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	end P_DEPR_FERC_FCST;

  --**************************************************************************
  --                            P_GET_LEASE_DEPR
  --**************************************************************************
  procedure P_GET_LEASE_DEPR(A_ILR_ID   in number,
                             A_REVISION in number) is
    L_MSG       varchar2(2000);
    L_CC_ID     NUMBER;
    L_DG_CC_IND varchar2(100);

		type ASSET_REC is record(
      LS_ASSET_ID        LS_ASSET.LS_ASSET_ID%type,
      COMPANY_ID         COMPANY_SETUP.COMPANY_ID%type,
      GL_ACCOUNT_ID      GL_ACCOUNT.GL_ACCOUNT_ID%type,
      MAJOR_LOCATION_ID  MAJOR_LOCATION.MAJOR_LOCATION_ID%type,
			UTILITY_ACCOUNT_ID UTILITY_ACCOUNT.UTILITY_ACCOUNT_ID%type,
      BUS_SEGMENT_ID     BUSINESS_SEGMENT.BUS_SEGMENT_ID%type,
      SUB_ACCOUNT_ID     SUB_ACCOUNT.SUB_ACCOUNT_ID%type,
      VINTAGE            number,
      ASSET_LOCATION_ID  ASSET_LOCATION.ASSET_LOCATION_ID%type,
      LOCATION_TYPE_ID   LOCATION_TYPE.LOCATION_TYPE_ID%type,
			RETIREMENT_UNIT_ID RETIREMENT_UNIT.RETIREMENT_UNIT_ID%type,
      PROPERTY_UNIT_ID   PROPERTY_UNIT.PROPERTY_UNIT_ID%type,
      CC_VALUE           LS_ASSET_CLASS_CODE.VALUE%type);

		type ASSET_TABLE is table of ASSET_REC index by pls_integer;
		L_ASSET_TABLE ASSET_TABLE;
	begin
      --start log back up again
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());

    L_MSG := 'Finding depr group';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

		L_DG_CC_IND := pkg_pp_system_control.f_pp_system_control('Lease Depr Group Class Code');

		if nvl(L_DG_CC_IND, 'none') = 'none' then
		    L_CC_ID := -1;
		else
		    begin
		    select CLASS_CODE_ID
		    into L_CC_ID
		    from CLASS_CODE
         where lower(trim(description)) =
               pkg_pp_system_control.f_pp_system_control('DEPR GROUP CLASS CODE');

      exception
        when no_data_found then
		      L_CC_ID := -1;
		    end;
		end if;

		-- get attributes required to find depreciation group and calc depreciation
    select LA.LS_ASSET_ID, LA.COMPANY_ID,
           IA.CAP_ASSET_ACCOUNT_ID AS GL_ACCOUNT_ID, AL.MAJOR_LOCATION_ID,
           LA.UTILITY_ACCOUNT_ID, LA.BUS_SEGMENT_ID, LA.SUB_ACCOUNT_ID,
           TO_NUMBER(TO_CHAR(LI.EST_IN_SVC_DATE, 'YYYY')),
			LA.ASSET_LOCATION_ID, ML.LOCATION_TYPE_ID, LA.RETIREMENT_UNIT_ID,
           RU.PROPERTY_UNIT_ID,
           nvl(decode(L_DG_CC_IND,
                       'mla',
                       LCC.VALUE,
                       'ilr',
                       ICC.VALUE,
                       'asset',
                       ACC.VALUE,
                       'NO CLASS CODE'),
                'NO CLASS CODE')
		bulk collect
		into L_ASSET_TABLE
      from LS_ILR_ACCOUNT IA, LS_ASSET LA, ASSET_LOCATION AL,
           MAJOR_LOCATION ML, LS_ILR LI, RETIREMENT_UNIT RU, LS_LEASE LL,
			(select * from LS_MLA_CLASS_CODE where CLASS_CODE_ID = L_CC_ID) LCC,
			(select * from LS_ILR_CLASS_CODE where CLASS_CODE_ID = L_CC_ID) ICC,
			(select * from LS_ASSET_CLASS_CODE where CLASS_CODE_ID = L_CC_ID) ACC
		where IA.ILR_ID = A_ILR_ID
		and LI.ILR_ID = A_ILR_ID
         and LA.ILR_ID = A_ILR_ID
		and LA.ASSET_LOCATION_ID = AL.ASSET_LOCATION_ID
		and AL.MAJOR_LOCATION_ID = ML.MAJOR_LOCATION_ID
		and LA.RETIREMENT_UNIT_ID = RU.RETIREMENT_UNIT_ID
		and LL.LEASE_ID = LI.LEASE_ID
       and LL.LEASE_ID = LCC.LEASE_ID(+)
       and LI.ILR_ID = ICC.ILR_ID(+)
       and LA.LS_ASSET_ID = ACC.LS_ASSET_ID(+);

    L_MSG := 'Updating Depr Group on LS Asset';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      forall ndx in indices of L_ASSET_TABLE
			update ls_asset la
         set depr_group_id = PP_DEPR_PKG.F_FIND_DEPR_GROUP(L_ASSET_TABLE(ndx)
                                                           .COMPANY_ID,
                                                           L_ASSET_TABLE(ndx)
                                                           .GL_ACCOUNT_ID,
                                                           L_ASSET_TABLE(ndx)
                                                           .MAJOR_LOCATION_ID,
                                                           L_ASSET_TABLE(ndx)
                                                           .UTILITY_ACCOUNT_ID,
                                                           L_ASSET_TABLE(ndx)
                                                           .BUS_SEGMENT_ID,
                                                           L_ASSET_TABLE(ndx)
                                                           .SUB_ACCOUNT_ID,
										-100, --SUBLEDGER_TYPE_ID
                                                           L_ASSET_TABLE(ndx)
                                                           .VINTAGE,
                                                           L_ASSET_TABLE(ndx)
                                                           .ASSET_LOCATION_ID,
                                                           L_ASSET_TABLE(ndx)
                                                           .LOCATION_TYPE_ID,
                                                           L_ASSET_TABLE(ndx)
                                                           .RETIREMENT_UNIT_ID,
                                                           L_ASSET_TABLE(ndx)
                                                           .PROPERTY_UNIT_ID,
										L_CC_ID, --A_CLASS_CODE_ID
                                                           L_ASSET_TABLE(ndx)
                                                           .CC_VALUE)
			where la.ls_asset_id = L_ASSET_TABLE(ndx).LS_ASSET_ID
         AND PP_DEPR_PKG.F_FIND_DEPR_GROUP(L_ASSET_TABLE(ndx).COMPANY_ID,
										L_ASSET_TABLE(ndx).GL_ACCOUNT_ID,
                                           L_ASSET_TABLE(ndx)
                                           .MAJOR_LOCATION_ID,
                                           L_ASSET_TABLE(ndx)
                                           .UTILITY_ACCOUNT_ID,
										L_ASSET_TABLE(ndx).BUS_SEGMENT_ID,
										L_ASSET_TABLE(ndx).SUB_ACCOUNT_ID,
										-100, --SUBLEDGER_TYPE_ID
										L_ASSET_TABLE(ndx).VINTAGE,
                                           L_ASSET_TABLE(ndx)
                                           .ASSET_LOCATION_ID,
                                           L_ASSET_TABLE(ndx)
                                           .LOCATION_TYPE_ID,
                                           L_ASSET_TABLE(ndx)
                                           .RETIREMENT_UNIT_ID,
                                           L_ASSET_TABLE(ndx)
                                           .PROPERTY_UNIT_ID,
										L_CC_ID, --A_CLASS_CODE_ID
                                           L_ASSET_TABLE(ndx).CC_VALUE) > 0;

    PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount ||
                               ' rows on ls_asset updated with a depr group.');

	end P_GET_LEASE_DEPR;

  --**************************************************************************
  --                            P_fcst_lease
  --**************************************************************************
  PROCEDURE P_fcst_lease(a_ilr_id   IN NUMBER,
                         a_revision IN NUMBER) IS
  BEGIN
      DELETE FROM LS_DEPR_INFO_TMP;

      INSERT INTO LS_DEPR_INFO_TMP
      (ilr_id, ls_asset_id, set_of_books_id, revision, month,
       end_capital_cost, depr_group_id, economic_life, set_of_books,
       mid_period_conv, mid_period_method, fasb_cap_type_id)
      SELECT a_ilr_id, ls_asset_id, set_of_books_id, revision, month,
             end_capital_cost, depr_group_id, economic_life, set_of_books,
             mid_period_conv, mid_period_method, fasb_cap_type_id
        FROM (SELECT las.ls_asset_id, las.set_of_books_id, las.revision,
                      las.month, las.end_capital_cost, la.depr_group_id,
                      la.economic_life, /* WMD */
                      Row_number() over(PARTITION BY las.ls_asset_id, las.set_of_books_id, las.revision ORDER BY las.month) AS the_row,
                      dg.mid_period_conv /* CJS 4/11/17 */,
                      sob.description set_of_books,
                      Lower(dg.mid_period_method) mid_period_method,
                      sob_map.fasb_cap_type_id
                 FROM LS_ASSET_SCHEDULE las, LS_ASSET la, DEPR_GROUP dg,
                      LS_ILR_OPTIONS lio, LS_FASB_CAP_TYPE_SOB_MAP sob_map,
                      SET_OF_BOOKS sob
                WHERE la.depr_group_id = dg.depr_group_id
                  AND las.ls_asset_id = la.ls_asset_id
                  AND la.ilr_id = a_ilr_id
                  AND las.revision = a_revision
                  AND lio.ilr_id = a_ilr_id
                  AND lio.revision = a_revision
                  AND sob_map.lease_cap_type_id = lio.lease_cap_type_id
                  AND sob_map.set_of_books_id = las.set_of_books_id
                  AND las.set_of_books_id = sob.set_of_books_id
                  AND las.revision = a_revision
                  AND la.ilr_id = a_ilr_id)
       WHERE the_row = 1;

    P_fcst_lease_use_temp_table(a_ilr_id, a_revision);

      DELETE FROM LS_DEPR_INFO_TMP;

  END p_fcst_lease;

  --**************************************************************************
  --                            P_fcst_lease_use_temp_table
  --**************************************************************************
  PROCEDURE P_fcst_lease_use_temp_table(a_ilr_id   NUMBER,
                                        a_revision NUMBER) IS
    l_msg       VARCHAR2(2000);
    l_init_life NUMBER;
    l_num       NUMBER;
  BEGIN
    pkg_pp_error.Set_module_name('PKG_LEASE_DEPR.P_FCST_LEASE');
      --start log back up again
    pkg_pp_log.P_start_log(p_process_id => pkg_lease_common.F_getprocess());

    pkg_pp_log.P_write_message('A_ILR_ID : ' || a_ilr_id);

    pkg_pp_log.P_write_message('A_REVISION : ' || a_revision);

	DELETE FROM LS_DEPR_FORECAST
	 WHERE revision = a_revision
	   AND ls_asset_id IN
		   (SELECT la.ls_asset_id FROM LS_ASSET la WHERE la.ilr_id = a_ilr_id)
	   AND MONTH >= Nvl((SELECT decode(is_impairment, 1, trunc(impairment_date, 'month'), trunc(remeasurement_date, 'month'))
						  FROM ls_ilr_options
						 WHERE ilr_id = A_ILR_ID
						   AND revision = A_REVISION),
						To_Date('180001', 'yyyymm'))
	   AND (LS_ASSET_ID, SET_OF_BOOKS_ID) IN
		   (SELECT LS_ASSET_ID, SET_OF_BOOKS_ID
			  FROM LS_DEPR_INFO_TMP
			 WHERE ILR_ID = A_ILR_ID);


	  pkg_pp_log.P_write_message( SQL%rowcount||' rows deleted from LS_DEPR_FORECAST.' );

      -- calc ferc
    pkg_pp_log.P_write_message('FERC Calc');

    pkg_pp_log.P_write_message('Entering P_DEPR_FERC_FCST');

    P_depr_ferc_fcst(a_ilr_id, a_revision);

      -- calc sle
    pkg_pp_log.P_write_message('SLE Calc');

    FOR l_calc_depr IN (SELECT a_ilr_id, ls_asset_id, set_of_books_id,
                               revision, month, end_capital_cost,
                               depr_group_id, economic_life, set_of_books,
                               mid_period_conv, mid_period_method
                          FROM LS_DEPR_INFO_TMP
                         WHERE ilr_id = a_ilr_id
                           AND revision = a_revision
                           AND fasb_cap_type_id = 2 --fasb_cap_type_id 2 = Type B, which always uses SLE method, regardless of depr group setting. 
						   and (ilr_id, set_of_books_id) not in (select ilr_id, set_of_books_id  --Impaired ILRs are excluded
																from ls_ilr_impair)
                          ) LOOP
      pkg_pp_log.P_write_message('Getting init life');

      l_init_life := pkg_lease_common.F_get_init_life(l_calc_depr.ls_asset_id, l_calc_depr.revision, a_ilr_id, l_calc_depr.economic_life)
                     .init_life;

      pkg_pp_log.P_write_message('init life : ' || l_init_life);

      pkg_pp_log.P_write_message('Entering P_DEPR_SLE for set of books: ' ||
                                 l_calc_depr.set_of_books);

      P_depr_sle(l_calc_depr.ls_asset_id,
                 l_calc_depr.revision,
                 0,
                 l_calc_depr.set_of_books_id,
                 l_calc_depr.month,
                 l_init_life,
                 l_msg);
      END LOOP;

      -- calc sl
    pkg_pp_log.P_write_message('SL Calc');

    FOR l_calc_depr IN (SELECT a_ilr_id, ls_asset_id, set_of_books_id,
                               revision, month, end_capital_cost,
                               depr_group_id, economic_life, set_of_books,
                               mid_period_conv, mid_period_method
                          FROM LS_DEPR_INFO_TMP
                         WHERE ilr_id = a_ilr_id
                           AND revision = a_revision
                           AND ((fasb_cap_type_id <> 2  and mid_period_method in ('sl', 'uopr'))--fasb_cap_type_id 2 = Type B, which always uses SLE method, regardless of depr group setting.
								OR (ilr_id, set_of_books_id) in (select ilr_id, set_of_books_id  -- Impaired ILRs are included in this group
																from ls_ilr_impair))
						   ) LOOP
      pkg_pp_log.P_write_message('Getting init life');

      l_init_life := pkg_lease_common.F_get_init_life(l_calc_depr.ls_asset_id, l_calc_depr.revision, a_ilr_id, l_calc_depr.economic_life)
                     .init_life;

      pkg_pp_log.P_write_message('init life : ' || l_init_life);

      pkg_pp_log.P_write_message('Entering P_DEPR_SL for set of books: ' ||
                                 l_calc_depr.set_of_books);

      P_depr_sl(l_calc_depr.ls_asset_id,
                l_calc_depr.revision,
                0,
                l_calc_depr.set_of_books_id,
                l_calc_depr.month,
                l_init_life,
                l_calc_depr.mid_period_conv,
                l_msg);
      END LOOP;

      pkg_pp_error.remove_module_name;
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.Raise_error('ERROR', 'Failed Operation');
  END p_fcst_lease_use_temp_table;

  --**************************************************************************
  --                            F_LEASE_DEPR_UOP_ROLLFWD
  --**************************************************************************
  FUNCTION F_LEASE_DEPR_UOP_ROLLFWD(A_COMPANY_ID number,
                                    A_MONTH      in date) RETURN VARCHAR2 IS
    l_msg VARCHAR2(2000);
  BEGIN

      --Lock the UOP data for the current month.
      l_msg := 'Lock the UOP data for the current month';
      update LS_ASSET_UOP a
      set locked = 1
      where gl_posting_mo_yr = A_MONTH
       and exists (select 1
            from ls_asset b
            where company_id = a_company_id
            and b.ls_asset_id = a.ls_asset_id);

      --Roll-Forward the UOP data for next month
      -- Update for existing
      l_msg := 'Roll-Forward Asset UOP records for next month';
      UPDATE ls_asset_uop uop
       SET (estimated_production) =
           (SELECT distinct nvl(estimated_production, 0) -
                             nvl(production, 0)
                FROM ls_asset_uop uop2
                WHERE uop2.ls_asset_id = uop.ls_asset_id
               AND add_months(uop2.gl_posting_mo_yr, 1) =
                   uop.gl_posting_mo_yr)
     WHERE EXISTS (SELECT 1
         FROM ls_asset_uop uop2
        WHERE uop2.ls_asset_id = uop.ls_asset_id
               AND add_months(uop2.gl_posting_mo_yr, 1) =
                   uop.gl_posting_mo_yr);

      -- Insert for missing records that client hasn't already added
      l_msg := 'Add missing UOP records for next month';
    INSERT INTO LS_ASSET_UOP
      (ls_asset_id, gl_posting_mo_yr, production, estimated_production,
       locked)
      SELECT DISTINCT uop.ls_asset_id, add_months(uop.gl_posting_mo_yr, 1),
                      uop.production,
             uop.estimated_production - uop.production, 0
        FROM ls_asset_uop uop
       WHERE uop.gl_posting_mo_yr = A_MONTH
         AND NOT EXISTS (SELECT 1
               FROM ls_asset_uop uop2
              WHERE uop.ls_asset_id = uop2.ls_asset_id
                 AND add_months(uop.gl_posting_mo_yr, 1) =
                     uop2.gl_posting_mo_yr);

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
      l_msg := Substr(l_msg || ': ' || SQLERRM, 1, 2000);

               RETURN l_msg;
  END F_LEASE_DEPR_UOP_ROLLFWD;

  --**************************************************************************
  --                            F_LEASE_DEPR_UOP_UWA
  --**************************************************************************
  FUNCTION F_LEASE_DEPR_UOP_UWA(A_MONTH in date) RETURN VARCHAR2 IS
    l_msg VARCHAR2(2000);
  BEGIN
    
    --update with actuals: uses STG table rather than CPR_DEPR because this function is called just after
    --      the individual results are calculated and haven't yet been put into CPR_DEPR
    --      This allows company and month to be ignored as arguments since the STG table already has
    --      the assets we will focus on.
    update ls_depr_forecast a
       set (depr_expense, depr_exp_comp_curr)  =
           (select b.curr_depr_expense / a.exchange_rate, b.curr_depr_expense
             from cpr_depr_calc_stg b, ls_cpr_asset_map m
            where b.depr_calc_status = 9
              and b.mid_period_method = 'uopr'
              and b.subledger_type_id = -100
              and a.ls_asset_id = m.ls_asset_id
              and b.asset_id = m.asset_id
              and a.month = b.gl_posting_mo_yr
              and a.set_of_books_id = b.set_of_books_id)
     where exists (select 1
        from cpr_depr_calc_stg c, ls_cpr_asset_map m
       where c.depr_calc_status = 9
         and c.mid_period_method = 'uopr'
         and c.subledger_type_id = -100
         and a.ls_asset_id = m.ls_asset_id
         and m.asset_id = c.asset_id
         and a.month = c.gl_posting_mo_yr
         and a.set_of_books_id = c.set_of_books_id);

     -- Re-calculuate LS_DEPR_FORECAST.END_RESERVE for the month with the newly updated DEPR_EXPENSE
     update ls_depr_forecast a
       set end_reserve = begin_reserve + depr_expense +
                         depr_exp_alloc_adjust
     where exists (select 1
         from cpr_depr_calc_stg c, ls_cpr_asset_map m
        where a.ls_asset_id = m.ls_asset_id
          and m.asset_id = c.asset_id
          and a.month = c.gl_posting_mo_yr
          and a.set_of_books_id = c.set_of_books_id
          and c.depr_calc_status = 9
          and c.mid_period_method = 'uopr'
          and c.subledger_type_id = -100);

     -- Re-calc LS_DEPR_FORECAST for future months by reallocating the remaining months on a straight line basis
     merge into ls_depr_forecast ldf
     using (
      with cpr_stg as (
             select distinct m.ls_asset_id, c.set_of_books_id
               from cpr_depr_calc_stg c, ls_cpr_asset_map m
              where m.asset_id = c.asset_id
                and c.depr_calc_status = 9
                and c.mid_period_method = 'uopr'
                and c.subledger_type_id = -100),
           asset_revision as (
             select d.ls_asset_id, d.set_of_books_id, max(revision) revision
               from ls_depr_forecast d, cpr_stg c
              where d.ls_asset_id = c.ls_asset_id
                and d.set_of_books_id = c.set_of_books_id
              group by d.ls_asset_id, d.set_of_books_id),
          prior_month as (
            select a.ls_asset_id, a.revision, a.set_of_books_id, nvl(a.end_reserve,0) end_reserve
               from ls_depr_forecast a, asset_revision r
              where a.ls_asset_id = r.ls_asset_id
                and a.revision = r.revision
                and a.set_of_books_id = r.set_of_books_id
                and a.month = A_MONTH),
         total as ( 
           select las.ls_asset_id, las.revision, las.set_of_books_id, las.month,
                  LAST_VALUE(las.end_capital_cost) over(order by las.ls_asset_id, las.revision, las.set_of_books_id) 
                  - pm.end_reserve 
                  - case when (la.ESTIMATED_RESIDUAL = 0 and
                         lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual',
                                                                                    la.COMPANY_ID))) 
                         = 'yes') then
                      la.GUARANTEED_RESIDUAL_AMOUNT
                    else
                      round(la.ESTIMATED_RESIDUAL * la.FMV, 2)
                    end total_expense
               from ls_asset_schedule las, ls_asset la, prior_month pm
              where las.ls_asset_id = la.ls_asset_id
                and las.ls_asset_id = pm.ls_asset_id
                and las.revision = pm.revision
                and las.set_of_books_id = pm.set_of_books_id),
         asset_schedule as (
           select las.ls_asset_id, las.revision, las.set_of_books_id, las.month,
                  LAST_VALUE(las.month) over(order by las.ls_asset_id, las.revision, las.set_of_books_id) as max_month,
                  nvl(dg.mid_period_conv, 1) mid_period_conv
             from ls_asset_schedule las, ls_asset la, asset_revision ar,  depr_group dg
            where las.ls_asset_id = la.ls_asset_id
              and las.ls_asset_id = ar.ls_asset_id
              and las.ls_asset_id = ar.ls_asset_id
              and las.revision = ar.revision
              and las.set_of_books_id = ar.set_of_books_id
              and las.month > A_MONTH
              and la.depr_group_id = dg.depr_group_id)
      select s.ls_asset_id, s.revision, s.set_of_books_id, s.month,
             sum(case 
                  when s.month < ADD_MONTHS(s.max_month, decode(s.mid_period_conv, 1, 0, 1)) 
                       and s.month > A_MONTH then
                   round(t.total_expense / MONTHS_BETWEEN(s.max_month, A_MONTH), 2)
                  when s.month = ADD_MONTHS(s.max_month, decode(s.mid_period_conv, 1, 0, 1)) then
                   round(t.total_expense / MONTHS_BETWEEN(s.max_month, A_MONTH), 2) 
                   * decode(s.mid_period_conv, 0, 1, s.mid_period_conv)
                   + (t.total_expense - (round(t.total_expense / MONTHS_BETWEEN(s.max_month, A_MONTH), 2) 
                                         * MONTHS_BETWEEN(s.max_month, A_MONTH)))
                  else
                   0
                 end) over(partition by s.ls_asset_id, s.revision, s.set_of_books_id order by s.month) 
             + pm.end_reserve 
             - case 
                when s.month < ADD_MONTHS(s.max_month, decode(s.mid_period_conv, 1, 0, 1)) 
                     and s.month > A_MONTH then
                 round(t.total_expense / MONTHS_BETWEEN(s.max_month, A_MONTH), 2)
                when s.month = ADD_MONTHS(s.max_month, decode(s.mid_period_conv, 1, 0, 1))  then
                 round(t.total_expense / MONTHS_BETWEEN(s.max_month, A_MONTH), 2) 
                 * decode(s.mid_period_conv, 0, 1, s.mid_period_conv)
                 + (t.total_expense - (round(t.total_expense / MONTHS_BETWEEN(s.max_month, A_MONTH), 2) 
                                     * MONTHS_BETWEEN(s.max_month, A_MONTH)))
               else
                0
               end as begin_reserve,
             case 
              when s.month < ADD_MONTHS(s.max_month, decode(s.mid_period_conv, 1, 0, 1)) 
                   and s.month > A_MONTH then
               round(t.total_expense / MONTHS_BETWEEN(s.max_month, A_MONTH), 2)
              when s.month = ADD_MONTHS(s.max_month, decode(s.mid_period_conv, 1, 0, 1))  then
               round(t.total_expense / MONTHS_BETWEEN(s.max_month, A_MONTH), 2) 
               * decode(s.mid_period_conv, 0, 1, s.mid_period_conv)
               + (t.total_expense - (round(t.total_expense / MONTHS_BETWEEN(s.max_month, A_MONTH), 2) 
                                   * MONTHS_BETWEEN(s.max_month, A_MONTH)))
             else
              0
             end as depr_expense, 
             0 as depr_exp_alloc_adjust,
             sum(case 
                  when s.month < ADD_MONTHS(s.max_month, decode(s.mid_period_conv, 1, 0, 1)) 
                       and s.month > A_MONTH then
                   round(t.total_expense / MONTHS_BETWEEN(s.max_month, A_MONTH), 2)
                  when s.month = ADD_MONTHS(s.max_month, decode(s.mid_period_conv, 1, 0, 1)) then
                   round(t.total_expense / MONTHS_BETWEEN(s.max_month, A_MONTH), 2) 
                   * decode(s.mid_period_conv, 0, 1, s.mid_period_conv)
                   + (t.total_expense - (round(t.total_expense / MONTHS_BETWEEN(s.max_month, A_MONTH), 2) 
                                         * MONTHS_BETWEEN(s.max_month, A_MONTH)))
                  else
                   0
                 end) over(partition by s.ls_asset_id, s.revision, s.set_of_books_id order by s.month) 
             + pm.end_reserve as end_reserve
        from asset_schedule s
        JOIN total t on s.ls_asset_id = t.ls_asset_id
                    and s.revision = t.revision
                    and s.set_of_books_id = t.set_of_books_id
                    and s.month = t.month
        JOIN prior_month pm on s.ls_asset_id = pm.ls_asset_id
                           and s.revision = pm.revision
                           and s.set_of_books_id = pm.set_of_books_id
     ) sl 
     on (    ldf.ls_asset_id = sl.ls_asset_id 
         and ldf.revision = sl.revision 
         and ldf.set_of_books_id = sl.set_of_books_id 
         and ldf.month = sl.month) 
     when matched then
        update
           set ldf.begin_reserve         = case when sl.month = A_MONTH then
                                             ldf.begin_reserve
                                           else
                                             sl.begin_reserve
                                           end,
               ldf.depr_expense          = sl.depr_expense,
               ldf.depr_exp_alloc_adjust = sl.depr_exp_alloc_adjust,
               ldf.end_reserve           = sl.end_reserve
     when not matched then
        insert
          (ls_asset_id, set_of_books_id, revision, month, begin_reserve,
           depr_expense, depr_exp_alloc_adjust, end_reserve)
        values
          (sl.ls_asset_id, sl.set_of_books_id, sl.revision, sl.month,
           sl.begin_reserve, sl.depr_expense, sl.depr_exp_alloc_adjust,
           sl.end_reserve);

    RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
      l_msg := Substr(l_msg || ': ' || SQLERRM, 1, 2000);

               RETURN l_msg;
  END F_LEASE_DEPR_UOP_UWA;

end PKG_LEASE_DEPR;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (12923, 0, 2018, 1, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2018.1.0.0_PKG_LEASE_DEPR.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
