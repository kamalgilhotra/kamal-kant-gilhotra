create or replace package PP_DEPR_PKG
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: PP_DEPR_PKG
|| Description: Depr functions and procedures for PowerPlant application.
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| 1.0     07/18/2011 David Liss     Create
|| 2.0     04/03/2013 Brandon Beck   Adding Depreciation Calculation Functions
|| 3.0     10/08/13   Brandon Beck   Add Over Depr Check
|| 4.0     11/21/14   A. Rajashekar  maint-41003. Transfer in Est Adds sys control
||                                   needs to be accounted for in calc.
|| 5.0     11/19/14   D. Motter      maint-41332. forecast depr joins to company sob.
|| 6.0     12/10/14   A. Rajashekar  maint-41660 Transfer in Est Adds sys control
||                                   needs to be accounted for forecast depr calc.
|| 7.0     03/27/15   Anand R        PP-43314 - Add NVL around rwip_allocation
|| 8.0     04/30/15   BBeck          maint-43785 - 0 as salvage exp alloc adjust
|| 9.0     08/24/15   Anand R        PP-44361 incremental depr calc for Reg layer
|| 10.0    10/05/2015 Anand R        PP-45013 Recurring Depr Adjusts for CPR_DEPR assets not working in PL/SQL Depr Calc
||============================================================================
*/
 as

    procedure P_STAGEMONTHENDDEPR_BL(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
							A_MONTHS PKG_PP_COMMON.DATE_TABTYPE);

   procedure P_STAGEMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
							A_MONTHS PKG_PP_COMMON.DATE_TABTYPE,
							A_LOAD_RECURRING integer);

   procedure P_STAGEMONTHENDDEPR(A_COMPANY_ID    NUMBER,
							A_MONTH date,
							A_LOAD_RECURRING integer);

   procedure P_FCSTDEPRSTAGE(A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
                      A_MONTHS PKG_PP_COMMON.DATE_TABTYPE);

	procedure P_FCST_AMORT_STAGE;
	 procedure P_HANDLERESULTS_BL;

	procedure P_BUILD_VINTAGE_SUMMARY(	A_DG_IDS PKG_PP_COMMON.NUM_TABTYPE,
										A_START_MONTH date);

	function F_GETRESERVERATIO(	A_DEPR_GROUP_ID		DEPR_GROUP.DEPR_GROUP_ID%TYPE,
								A_SOB_ID			DEPR_LEDGER.SET_OF_BOOKS_ID%TYPE,
								A_MONTH				DEPR_LEDGER.GL_POST_MO_YR%TYPE,
								A_RESERVE_TYPE		varchar2) return number;
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: f_find_depr_group
   || Description: Finds the depr group under the depr group control lookup.
   || None of the parameters can be null before entering the function.
   || Make sure you check sqlca.sqlcode when calling this function.
   || Return Values: -1 - No data found
   ||             dg_id - Valid depr group ID
   ||                -9 - Invalid depr group
   ||============================================================================
   || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     07/18/2011 David Liss     Create
   || 2.0     04/03/2013 Brandon Beck   Adding Depreciation Calculation Functions
   ||============================================================================
   */
   function F_FIND_DEPR_GROUP(A_COMPANY_ID         number,
                              A_GL_ACCOUNT_ID      number,
                              A_MAJOR_LOCATION_ID  number,
                              A_UTILITY_ACCOUNT_ID number,
                              A_BUS_SEGMENT_ID     number,
                              A_SUB_ACCOUNT_ID     number,
                              A_SUBLEDGER_TYPE_ID  number,
                              A_BOOK_VINTAGE       number,
                              A_ASSET_LOCATION_ID  number,
                              A_LOCATION_TYPE_ID   number,
                              A_RETIREMENT_UNIT_ID number,
                              A_PROPERTY_UNIT_ID   number,
                              A_CLASS_CODE_ID      number,
                              A_CC_VALUE           varchar2)

    return number;
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: f_get_dg_cc_id
   || Description: Finds the depr group class code.
   || Make sure you check sqlca.sqlcode when calling this function.
   || Return Values: 0 - No data found
   ||                dg_cc_id - DEPR GROUP CONTROL class code id
   ||============================================================================
   || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     08/12/2011 David Liss     Create
   ||============================================================================
   */
   function F_GET_DG_CC_ID return number;

   --Check to validate depr group status
   procedure P_DG_STATUS_CHECK(A_ENABLE boolean);

   --Check to validate depr group business segment
   procedure P_CHECK_DG_BS;

   procedure P_BACKFILLDATA_FCST(A_START_MONTH date, A_END_MONTH date);
   procedure P_STAGE_BLENDING_FCST;
   procedure P_SET_FISCALYEARSTART;

   G_VERSION_ID      FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%type;

   G_DG_STATUS_CHECK boolean := true;
   G_CHECK_DG_BS     varchar2(10);

end PP_DEPR_PKG;
/


create or replace package body PP_DEPR_PKG as
   --**************************************************************
   --       TYPE DECLARATIONS
   --**************************************************************
	TYPE t_company_status_check IS RECORD (
		company_id		company_setup.company_id%TYPE,
		check_inactive	pp_system_control_company.control_value%TYPE
	);

   --**************************************************************
   --       VARIABLES
   --**************************************************************
   type TAB_COMPANY_STATUS_CHECK is table of T_COMPANY_STATUS_CHECK;
   G_COMPANY_STATUS_CHECK TAB_COMPANY_STATUS_CHECK := TAB_COMPANY_STATUS_CHECK();

   --G_VERSION_ID      FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%type;
   G_MONTHS          PKG_PP_COMMON.DATE_TABTYPE;

   G_RTN           NUMBER;

   --**************************************************************
   --       PROCEDURES
   --**************************************************************


   procedure P_BACKFILLDATA is
      L_STATUS varchar2(254);
   begin
	  PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA');

	  --BAL columns from last month. If they don't exist they were initialized as 0 so we're good
	  update DEPR_CALC_STG DCS
		set (RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE, RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS,
				RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT, RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_BAL_SALVAGE_EXP) =
		(select DL.RESERVE_BAL_PROVISION, DL.RESERVE_BAL_COR, DL.SALVAGE_BALANCE, DL.RESERVE_BAL_ADJUST, DL.RESERVE_BAL_RETIREMENTS,
				DL.RESERVE_BAL_TRAN_IN, DL.RESERVE_BAL_TRAN_OUT, DL.RESERVE_BAL_OTHER_CREDITS, DL.RESERVE_BAL_GAIN_LOSS,
				DL.RESERVE_BAL_SALVAGE_EXP
			from DEPR_LEDGER DL
			where 	DL.DEPR_GROUP_ID = DCS.DEPR_GROUP_ID
			and		DL.SET_OF_BOOKS_ID = DCS.SET_OF_BOOKS_ID
			and		add_months(DL.GL_POST_MO_YR,1) = DCS.GL_POST_MO_YR)
		where DEPR_CALC_STATUS = 1
		and exists (select 1
					from DEPR_LEDGER DL2
					where DL2.DEPR_GROUP_ID = DCS.DEPR_GROUP_ID
					and DL2.SET_OF_BOOKS_ID = DCS.SET_OF_BOOKS_ID
					and add_months(DL2.GL_POST_MO_YR,1) = DCS.GL_POST_MO_YR);

      --fiscal qtrs based on pp_calendars instead of pp_table_months
      update DEPR_CALC_STG STG
         set (FISCALQTRSTART, FISCAL_MONTH) =
              (select ADD_MONTHS(STG.FISCALYEARSTART, (CAL.FISCAL_QUARTER - 1) * 3), CAL.FISCAL_MONTH
                 from PP_CALENDAR CAL
                where CAL.MONTH = STG.GL_POST_MO_YR)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('qtr', 'quarterly');

         PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA1');

      -- begin_balance_qtr and begin_reserve_qtr for Quarterly
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_QTR, BEGIN_RESERVE_QTR, BEGIN_COR_QTR) =
              (select NVL(DL.BEGIN_BALANCE, 0), NVL(DL.BEGIN_RESERVE, 0), NVL(DL.COR_BEG_RESERVE,0)
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR = STG.FISCALQTRSTART)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('qtr', 'quarterly')
		 and exists (select 1
					from DEPR_LEDGER DL2
					where DL2.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
					and DL2.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
					and DL2.GL_POST_MO_YR = STG.FISCALQTRSTART);

       --calculate begin_balance_qtr and begin_reserve_qtr for Quarterly when TRF_IN_EST_ADDS = 'no'
       update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_QTR, BEGIN_RESERVE_QTR, BEGIN_COR_QTR) =
              (select NVL(STG.BEGIN_BALANCE_QTR + SUM(DL.TRANSFERS_IN + DL.TRANSFERS_OUT ), 0), NVL(STG.BEGIN_RESERVE_QTR + SUM(DL.RESERVE_TRAN_IN + DL.RESERVE_TRAN_OUT),0 )
                      , NVL(STG.BEGIN_COR_QTR + SUM(DL.COR_RES_TRAN_IN + DL.COR_RES_TRAN_OUT), 0)
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR between stg.FISCALQTRSTART and stg.gl_post_mo_yr )
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('qtr', 'quarterly')
         and exists (select 1
               from DEPR_LEDGER DL2
               where DL2.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
               and DL2.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
               and DL2.GL_POST_MO_YR between stg.FISCALQTRSTART and stg.gl_post_mo_yr )
         and lower(trim(TRF_IN_EST_ADDS)) = 'no';

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA2');

     -- calculate plant activity, reserve_activity, and cor_activity for Quarterly
     update DEPR_CALC_STG STG
        set (STG.PLANT_ACTIVITY_2, STG.RESERVE_ACTIVITY_2, STG.COR_ACTIVITY_2) =
              (select round(sum((dl.ADDITIONS + dl.RETIREMENTS + decode(STG.trf_in_est_adds, 'no', 0, dl.TRANSFERS_IN + dl.TRANSFERS_OUT)
                     + dl.ADJUSTMENTS + dl.IMPAIRMENT_ASSET_AMOUNT)
                     / (decode(STG.smooth_curve, 'no', 1, (3-months_between(dl.gl_post_mo_yr, STG.FISCALQTRSTART))))
                  ),2)
                  * decode(STG.smooth_curve, 'no', 1, 3),
               round(sum((dl.RETIREMENTS + DL.GAIN_LOSS + DL.SALVAGE_CASH + DL.SALVAGE_RETURNS +
                     + DL.RESERVE_CREDITS + decode(STG.trf_in_est_adds, 'no', 0, DL.RESERVE_TRAN_IN + DL.RESERVE_TRAN_OUT)
                     + DL.RESERVE_ADJUSTMENTS + IMPAIRMENT_ASSET_AMOUNT + IMPAIRMENT_EXPENSE_AMOUNT)
                     / (decode(stg.smooth_curve, 'no', 1, (3-months_between(dl.gl_post_mo_yr, stg.FISCALQTRSTART))))
                  ),2)
                  * decode(stg.smooth_curve, 'no', 1, 3),
               round(sum((DL.COST_OF_REMOVAL + DL.COR_RES_ADJUST + DECODE(STG.TRF_IN_EST_ADDS, 'no', 0, DL.COR_RES_TRAN_IN + DL.COR_RES_TRAN_OUT))
                     / (decode(stg.smooth_curve, 'no', 1, (3-months_between(dl.gl_post_mo_yr, stg.FISCALQTRSTART))))
                  ),2)
                  * decode(stg.smooth_curve, 'no', 1, 3)
             from depr_ledger dl
            where dl.depr_group_id = stg.depr_group_id
              and dl.set_of_books_id = stg.set_of_books_id
              and dl.gl_post_mo_yr between stg.FISCALQTRSTART and stg.gl_post_mo_yr)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('qtr', 'quarterly');

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA3');

      -- begin_balance_year and begin_reserve_year for Yearly, SFY
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_YEAR, BEGIN_RESERVE_YEAR, BEGIN_COR_YEAR) =
              (select NVL(DL.BEGIN_BALANCE, 0), NVL(DL.BEGIN_RESERVE, 0), NVL(DL.COR_BEG_RESERVE,0)
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR = STG.FISCALYEARSTART)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('yearly', 'sfy', 'curve', 'curve_no_true_up')
		 and exists (select 1
					from DEPR_LEDGER DL2
					where DL2.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
					and DL2.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
					and DL2.GL_POST_MO_YR = STG.FISCALYEARSTART);

      -- begin_balance_year and begin_reserve_year for Yearly, SFY when TRF_IN_EST_ADDS = 'no'
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_YEAR, BEGIN_RESERVE_YEAR, BEGIN_COR_YEAR) =
              (select NVL(STG.BEGIN_BALANCE_YEAR + SUM(DL.TRANSFERS_IN + DL.TRANSFERS_OUT), 0), NVL(STG.BEGIN_RESERVE_YEAR + SUM(DL.RESERVE_TRAN_IN + DL.RESERVE_TRAN_OUT), 0)
                      , NVL(STG.BEGIN_COR_YEAR + SUM(DL.COR_RES_TRAN_IN + DL.COR_RES_TRAN_OUT),0)
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR between STG.FISCALYEARSTART and stg.gl_post_mo_yr)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('yearly', 'sfy', 'curve', 'curve_no_true_up')
         and exists (select 1
               from DEPR_LEDGER DL2
               where DL2.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
               and DL2.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
               and DL2.GL_POST_MO_YR between STG.FISCALYEARSTART and stg.gl_post_mo_yr)
         and lower(trim(TRF_IN_EST_ADDS)) = 'no';

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA4');

     -- calculate plant activity, reserve_activity, and cor_activity for Yearly, SFY
     update DEPR_CALC_STG STG
        set (STG.PLANT_ACTIVITY_2, STG.RESERVE_ACTIVITY_2, STG.COR_ACTIVITY_2) =
              (select round(sum((dl.ADDITIONS + dl.RETIREMENTS + decode(STG.trf_in_est_adds, 'no', 0, dl.TRANSFERS_IN + dl.TRANSFERS_OUT)
                     + dl.ADJUSTMENTS + dl.IMPAIRMENT_ASSET_AMOUNT)
                     / (decode(STG.smooth_curve, 'no', 1, (12-months_between(dl.gl_post_mo_yr, STG.fiscalyearstart))))
                  ),2)
                  * decode(STG.smooth_curve, 'no', 1, 12),
               round(sum((dl.RETIREMENTS + DL.GAIN_LOSS + DL.SALVAGE_CASH + DL.SALVAGE_RETURNS +
                     + DL.RESERVE_CREDITS + decode(STG.trf_in_est_adds, 'no', 0, DL.RESERVE_TRAN_IN + DL.RESERVE_TRAN_OUT)
                     + DL.RESERVE_ADJUSTMENTS + IMPAIRMENT_ASSET_AMOUNT + IMPAIRMENT_EXPENSE_AMOUNT)
                     / (decode(stg.smooth_curve, 'no', 1, (12-months_between(dl.gl_post_mo_yr, stg.fiscalyearstart))))
                  ),2)
                  * decode(stg.smooth_curve, 'no', 1, 12),
               round(sum((DL.COST_OF_REMOVAL + DL.COR_RES_ADJUST + DECODE(STG.TRF_IN_EST_ADDS, 'no', 0, DL.COR_RES_TRAN_IN + DL.COR_RES_TRAN_OUT))
                     / (decode(stg.smooth_curve, 'no', 1, (12-months_between(dl.gl_post_mo_yr, stg.fiscalyearstart))))
                  ),2)
                  * decode(stg.smooth_curve, 'no', 1, 12)
             from depr_ledger dl
            where dl.depr_group_id = stg.depr_group_id
              and dl.set_of_books_id = stg.set_of_books_id
              and dl.gl_post_mo_yr between stg.fiscalyearstart and stg.gl_post_mo_yr)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('yearly', 'sfy');

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA5');

    -- overwrite est_ann_net_adds for curve if in fiscal month = 12
      update DEPR_CALC_STG STG
         set EST_ANN_NET_ADDS =
              (select case
                         when STG.TRF_IN_EST_ADDS = 'no' then
                          sum(ADDITIONS + RETIREMENTS + ADJUSTMENTS + IMPAIRMENT_ASSET_AMOUNT)
                         else
                          sum(ADDITIONS + RETIREMENTS + TRANSFERS_IN + TRANSFERS_OUT + ADJUSTMENTS +
                              IMPAIRMENT_ASSET_AMOUNT)
                      end
                 from DEPR_LEDGER DL
                where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR between STG.FISCALYEARSTART and STG.GL_POST_MO_YR)
       where DEPR_CALC_STATUS = 1
         and FISCAL_MONTH = 12
         and MID_PERIOD_METHOD in ('curve');

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA6');

    --calculate curve true up adjustments
    update DEPR_CALC_STG STG
       set (CURVE_TRUEUP_ADJ, CURVE_TRUEUP_ADJ_SALV, CURVE_TRUEUP_ADJ_COR) =
        (
		select ROUND((sum(HIST.EXPECTED_DEPR) - sum(HIST.ACTUAL_DEPR)) *
                DECODE(STG.SMOOTH_CURVE, 'no', 1, SFM3.FACTOR / EXCESS_FACTOR.FACTOR),
                2),
            ROUND((sum(HIST.EXPECTED_SALV) - sum(HIST.ACTUAL_SALV)) *
                DECODE(STG.SMOOTH_CURVE, 'no', 1, SFM3.FACTOR / EXCESS_FACTOR.FACTOR),
                2),
            ROUND((sum(HIST.EXPECTED_COR) - sum(HIST.ACTUAL_COR)) *
                DECODE(STG.SMOOTH_CURVE, 'no', 1, SFM3.FACTOR / EXCESS_FACTOR.FACTOR),
                2)
           from (
				--  This subselect figures out the expected depreciation by month
				--  for historical months going back to beginning of fiscal year
				select DL.DEPR_GROUP_ID,
                DL.SET_OF_BOOKS_ID,
                SFM.FACTOR_ID,
                ROUND((STG3.BEGIN_BALANCE_YEAR + (STG3.EST_ANN_NET_ADDS * STG3.MID_PERIOD_CONV)) * DMR.RATE * SFM.FACTOR, 2) as EXPECTED_DEPR,
                DL.DEPRECIATION_EXPENSE + DL.DEPR_EXP_ALLOC_ADJUST as ACTUAL_DEPR,
                ROUND((STG3.BEGIN_BALANCE_YEAR + STG3.EST_ANN_NET_ADDS * STG3.MID_PERIOD_CONV) * DMR.SALVAGE_RATE * SFM.FACTOR, 2) as EXPECTED_SALV,
                DL.SALVAGE_EXPENSE + DL.SALVAGE_EXP_ALLOC_ADJUST as ACTUAL_SALV,
                ROUND((STG3.BEGIN_BALANCE_YEAR + STG3.EST_ANN_NET_ADDS * STG3.MID_PERIOD_CONV) * DMR.COST_OF_REMOVAL_RATE * SFM.FACTOR, 2) as EXPECTED_COR,
                DL.COR_EXPENSE + DL.COR_EXP_ALLOC_ADJUST as ACTUAL_COR
               from DEPR_LEDGER           DL,
                DEPR_GROUP            DG,
                DEPR_METHOD_RATES     DMR,
                SPREAD_FACTOR_MONTHLY SFM,
                DEPR_CALC_STG         STG3
              where DL.DEPR_GROUP_ID = DG.DEPR_GROUP_ID
              and DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
              and DMR.SET_OF_BOOKS_ID = DL.SET_OF_BOOKS_ID
              and DL.DEPR_GROUP_ID = STG3.DEPR_GROUP_ID
              and DL.SET_OF_BOOKS_ID = STG3.SET_OF_BOOKS_ID
			  and STG3.GL_POST_MO_YR = STG3.CALC_MONTH
              and DMR.EFFECTIVE_DATE =
                (select max(EFFECTIVE_DATE)
                   from DEPR_METHOD_RATES DMR2
                  where DMR2.DEPR_METHOD_ID = DMR.DEPR_METHOD_ID
                  and DMR2.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
                  and DMR2.EFFECTIVE_DATE <= DL.GL_POST_MO_YR
                  group by DMR2.DEPR_METHOD_ID, DMR2.SET_OF_BOOKS_ID)
              and SFM.FACTOR_ID = DG.FACTOR_ID
              and SFM.FISCAL_MONTH = 1 + MONTHS_BETWEEN(DL.GL_POST_MO_YR, STG3.FISCALYEARSTART)
              and DL.GL_POST_MO_YR >= STG3.FISCALYEARSTART
              and DL.GL_POST_MO_YR < STG3.GL_POST_MO_YR) HIST,
            (
				-- Subselect determines the remaining year factors.
				select STG2.DEPR_GROUP_ID, STG2.SET_OF_BOOKS_ID, FACTOR_ID, sum(FACTOR) as FACTOR
               from SPREAD_FACTOR_MONTHLY SFM2, DEPR_CALC_STG STG2
              where SFM2.FACTOR_ID = STG2.SPREAD_FACTOR_ID
              and SFM2.FISCAL_MONTH >= STG2.FISCAL_MONTH
			  and STG2.GL_POST_MO_YR = STG2.CALC_MONTH
              and SFM2.FISCAL_MONTH <= 12
              group by STG2.DEPR_GROUP_ID, STG2.SET_OF_BOOKS_ID, FACTOR_ID) EXCESS_FACTOR,
            SPREAD_FACTOR_MONTHLY SFM3
          where HIST.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
          and HIST.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
          and EXCESS_FACTOR.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
          and EXCESS_FACTOR.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
          and STG.SPREAD_FACTOR_ID = HIST.FACTOR_ID
          and STG.SPREAD_FACTOR_ID = EXCESS_FACTOR.FACTOR_ID
          and STG.SPREAD_FACTOR_ID = SFM3.FACTOR_ID
          and STG.FISCAL_MONTH = SFM3.FISCAL_MONTH
          group by STG.DEPR_GROUP_ID,
               STG.SET_OF_BOOKS_ID,
               DECODE(STG.SMOOTH_CURVE, 'no', 1, SFM3.FACTOR / EXCESS_FACTOR.FACTOR)
		)
     where DEPR_CALC_STATUS = 1
           and STG.MID_PERIOD_METHOD in ('curve', 'curve_no_true_up')
		   and STG.GL_POST_MO_YR = STG.CALC_MONTH
		   and STG.FISCAL_MONTH <> 1;

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA9');

      --production, estimated_production, production_2, estimated_production_2, min_calc, and type_2_exist for UOP
      update DEPR_CALC_STG STG
         set (PRODUCTION, ESTIMATED_PRODUCTION, PRODUCTION_2, ESTIMATED_PRODUCTION_2, MIN_CALC, TYPE_2_EXIST) =
              (select least(nvl(PRODUCTION,0), nvl(ESTIMATED_PRODUCTION,0)),
                      nvl(ESTIMATED_PRODUCTION,0),
                      least(nvl(PRODUCTION_2,0), nvl(ESTIMATED_PRODUCTION_2,0)),
                      nvl(ESTIMATED_PRODUCTION_2,0),
                      nvl(MIN_CALC_OPTION,1),
            case when nvl(UOP_TYPE_ID2,0) > 0 then 1 else 0 end
                 from DEPR_METHOD_UOP M
                where M.DEPR_METHOD_ID = STG.DEPR_METHOD_ID
                  and M.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and M.GL_POST_MO_YR = STG.GL_POST_MO_YR)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD = 'uop';

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA10');

    update DEPR_CALC_STG STG
       set (MIN_ANNUAL_EXPENSE, MAX_ANNUAL_EXPENSE) = (
        select nvl(MIN_ANNUAL_EXPENSE,0), nvl(MAX_ANNUAL_EXPENSE,0)
          from DEPR_GROUP_UOP DGU
         where DGU.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
           and DGU.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
           and DGU.GL_POST_MO_YR = STG.GL_POST_MO_YR)
     where DEPR_CALC_STATUS = 1
       and MID_PERIOD_METHOD = 'uop'
       and MIN_CALC = 1;

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA11');

      --cum_uop_depr, cum_uop_depr_2 for UOP
      update DEPR_CALC_STG STG
         set (CUM_UOP_DEPR, CUM_UOP_DEPR_2, CUM_RATE_DEPR, CUM_LEDGER_DEPR_EXPENSE) =
              (select NVL(sum(UOP_DEPR_EXPENSE), 0), NVL(sum(UOP_DEPR_EXPENSE_2), 0), NVL(sum(RATE_DEPR_EXPENSE), 0), NVL(sum(LEDGER_DEPR_EXPENSE + UOP_EXP_ADJ),0)
                 from DEPR_GROUP_UOP DGO
                where DGO.GL_POST_MO_YR < STG.GL_POST_MO_YR
                  and DGO.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DGO.DEPR_GROUP_ID = STG.DEPR_GROUP_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD = 'uop'
     and (NET_GROSS = 1
      or MIN_CALC = 3);


PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA12');

       --reserve_diff for UOP  (I think this is needed for net method...)
      update DEPR_CALC_STG STG
         set STG.RESERVE_DIFF = STG.BEGIN_RESERVE -
        nvl((select sum(DGO.LEDGER_DEPR_EXPENSE)
         from DEPR_GROUP_UOP DGO
        where DGO.GL_POST_MO_YR < STG.GL_POST_MO_YR
          and DGO.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
          and DGO.DEPR_GROUP_ID = STG.DEPR_GROUP_ID), 0)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD = 'uop';

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA13');

      --ytd_uop_depr, ytd_uop_depr_2 for UOP
      update DEPR_CALC_STG STG
         set (YTD_UOP_DEPR, YTD_UOP_DEPR_2, YTD_RATE_DEPR, YTD_LEDGER_DEPR_EXPENSE) =
              (select NVL(sum(UOP_DEPR_EXPENSE), 0), NVL(sum(UOP_DEPR_EXPENSE_2), 0), NVL(sum(RATE_DEPR_EXPENSE), 0), NVL(sum(LEDGER_DEPR_EXPENSE + UOP_EXP_ADJ), 0)
                 from DEPR_GROUP_UOP DG
                where DG.GL_POST_MO_YR < STG.GL_POST_MO_YR
                  and DG.GL_POST_MO_YR >= STG.FISCALYEARSTART
                  and DG.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DG.DEPR_GROUP_ID = STG.DEPR_GROUP_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD = 'uop';

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA14');

      --COR_YTD AND SALV_YTD
      update DEPR_CALC_STG STG
         set (COR_YTD, SALV_YTD) = (
				select YTD.COR_YTD, YTD.SALV_YTD
				from (select distinct DL.DEPR_GROUP_ID, DL.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR,
						nvl(sum(DL.COST_OF_REMOVAL) OVER (PARTITION BY DL.DEPR_GROUP_ID, DL.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR ORDER BY STG.GL_POST_MO_YR DESC),0) COR_YTD,
						nvl(sum(DL.SALVAGE_CASH - DL.SALVAGE_RETURNS) OVER (PARTITION BY DL.DEPR_GROUP_ID, DL.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR ORDER BY STG.GL_POST_MO_YR DESC),0) SALV_YTD
					from DEPR_LEDGER DL, DEPR_CALC_STG STG
					where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
					and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
					and DL.GL_POST_MO_YR >= STG.FISCALYEARSTART
					and DL.GL_POST_MO_YR < STG.GL_POST_MO_YR) YTD
				where YTD.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
				and YTD.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
				and YTD.GL_POST_MO_YR = STG.GL_POST_MO_YR)
       where STG.DEPR_CALC_STATUS = 1
         and STG.COR_TREATMENT + STG.SALVAGE_TREATMENT > 0
		 and exists (
				select 1
				from (select distinct DL.DEPR_GROUP_ID, DL.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR,
						nvl(sum(DL.COST_OF_REMOVAL) OVER (PARTITION BY DL.DEPR_GROUP_ID, DL.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR ORDER BY STG.GL_POST_MO_YR DESC),0) COR_YTD,
						nvl(sum(DL.SALVAGE_CASH - DL.SALVAGE_RETURNS) OVER (PARTITION BY DL.DEPR_GROUP_ID, DL.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR ORDER BY STG.GL_POST_MO_YR DESC),0) SALV_YTD
					from DEPR_LEDGER DL, DEPR_CALC_STG STG
					where DL.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
					and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
					and DL.GL_POST_MO_YR >= STG.FISCALYEARSTART
					and DL.GL_POST_MO_YR < STG.GL_POST_MO_YR) YTD
				where YTD.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
				and YTD.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
				and YTD.GL_POST_MO_YR = STG.GL_POST_MO_YR);

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
	PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA15');

	--inorporate the spreadfactors rate into the depr rate
      update DEPR_CALC_STG STG
		set (stg.RATE, stg.DMR_COST_OF_REMOVAL_RATE, stg.DMR_SALVAGE_RATE) =
		(
			select 12 * stg.rate * NVL(S.factor , 0),
				12 * stg.DMR_COST_OF_REMOVAL_RATE * NVL(S.factor , 0),
				12 * stg.DMR_SALVAGE_RATE * NVL(S.factor , 0)
			from SPREAD_FACTOR_MONTHLY s
			where s.factor_id = stg.spread_factor_id
			and s.fiscal_month = stg.fiscal_month
		)
		where exists
		(
			select 1
			from SPREAD_FACTOR_MONTHLY s
			where s.factor_id = stg.spread_factor_id
			and s.fiscal_month = stg.fiscal_month
		)
		and STG.MID_PERIOD_METHOD in ('curve','curve_no_true_up')
		;

    PKG_PP_ERROR.REMOVE_MODULE_NAME;

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
   exception
      when others then
         L_STATUS := sqlerrm;
         update DEPR_CALC_STG set DEPR_CALC_STATUS = -25, DEPR_CALC_MESSAGE = L_STATUS;
     PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_BACKFILLDATA;

   --**************************************************************************
   --                            P_BACKFILLDATA_FCST
   --**************************************************************************

   procedure P_BACKFILLDATA_FCST(A_START_MONTH date, A_END_MONTH date) is
      L_STATUS varchar2(254);
   begin
    PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST');

	  --BAL columns from last month. If they don't exist they were initialized as 0 so we're good
	  update DEPR_CALC_STG DCS
		set (RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE, RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS,
				RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT, RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_BAL_SALVAGE_EXP) =
		(select DL.RESERVE_BAL_PROVISION, DL.RESERVE_BAL_COR, DL.SALVAGE_BALANCE, DL.RESERVE_BAL_ADJUST, DL.RESERVE_BAL_RETIREMENTS,
				DL.RESERVE_BAL_TRAN_IN, DL.RESERVE_BAL_TRAN_OUT, DL.RESERVE_BAL_OTHER_CREDITS, DL.RESERVE_BAL_GAIN_LOSS,
				DL.RESERVE_BAL_SALVAGE_EXP
			from FCST_DEPR_LEDGER DL
			where 	DL.FCST_DEPR_GROUP_ID = DCS.DEPR_GROUP_ID
			and		DL.SET_OF_BOOKS_ID = DCS.SET_OF_BOOKS_ID
			and	DL.FCST_DEPR_VERSION_ID = G_VERSION_ID
			and		add_months(DL.GL_POST_MO_YR,1) = DCS.GL_POST_MO_YR)
		where DEPR_CALC_STATUS = 1
		and exists (select 1
					from FCST_DEPR_LEDGER DL2
					where DL2.FCST_DEPR_VERSION_ID = G_VERSION_ID
					and DL2.FCST_DEPR_GROUP_ID = DCS.DEPR_GROUP_ID
					and DL2.SET_OF_BOOKS_ID = DCS.SET_OF_BOOKS_ID
					and add_months(DL2.GL_POST_MO_YR,1) = DCS.GL_POST_MO_YR);

PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST1');

      --fiscal qtrs based on pp_calendars instead of pp_table_months
      update DEPR_CALC_STG STG
         set (FISCALQTRSTART, FISCAL_MONTH) =
              (select ADD_MONTHS(STG.FISCALYEARSTART, (CAL.FISCAL_QUARTER - 1) * 3), CAL.FISCAL_MONTH
                 from PP_CALENDAR CAL
                where CAL.MONTH = STG.GL_POST_MO_YR)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('qtr', 'quarterly');

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST2');

      -- begin_balance_qtr and begin_reserve_qtr for Quarterly
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_QTR, BEGIN_RESERVE_QTR, BEGIN_COR_QTR) =
              (select NVL(DL.BEGIN_BALANCE, 0), NVL(DL.BEGIN_RESERVE, 0), NVL(DL.COR_BEG_RESERVE,0)
                 from FCST_DEPR_LEDGER DL
                where DL.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR = STG.FISCALQTRSTART
          and DL.FCST_DEPR_VERSION_ID = G_VERSION_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('qtr', 'quarterly')
		 and exists (select 1
					from FCST_DEPR_LEDGER DL2
					where DL2.FCST_DEPR_VERSION_ID = G_VERSION_ID
					and DL2.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
					and DL2.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
					and DL2.GL_POST_MO_YR = STG.FISCALQTRSTART);

      -- begin_balance_qtr and begin_reserve_qtr for Quarterly when TRF_IN_EST_ADDS = 'no'
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_QTR, BEGIN_RESERVE_QTR, BEGIN_COR_QTR) =
              (select NVL(STG.BEGIN_BALANCE_QTR + SUM(DL.TRANSFERS_IN + DL.TRANSFERS_OUT ), 0), NVL(STG.BEGIN_RESERVE_QTR + SUM(DL.RESERVE_TRAN_IN + DL.RESERVE_TRAN_OUT),0 )
                      , NVL(STG.BEGIN_COR_QTR + SUM(DL.COR_RES_TRAN_IN + DL.COR_RES_TRAN_OUT), 0)
                 from FCST_DEPR_LEDGER DL
                where DL.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR between STG.FISCALQTRSTART and STG.Gl_Post_Mo_Yr
          and DL.FCST_DEPR_VERSION_ID = G_VERSION_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('qtr', 'quarterly')
		 and exists (select 1
					from FCST_DEPR_LEDGER DL2
					where DL2.FCST_DEPR_VERSION_ID = G_VERSION_ID
					and DL2.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
					and DL2.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
					and DL2.GL_POST_MO_YR between STG.FISCALQTRSTART and STG.Gl_Post_Mo_Yr)
     and lower(trim(TRF_IN_EST_ADDS)) = 'no' ;

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST3');

     -- calculate plant activity, reserve_activity, and cor_activity for Yearly, SFY
     update DEPR_CALC_STG STG
        set (STG.PLANT_ACTIVITY_2, STG.RESERVE_ACTIVITY_2, STG.COR_ACTIVITY_2) =
              (select round(sum((DL.ADDITIONS + DL.RETIREMENTS + decode(STG.TRF_IN_EST_ADDS, 'no', 0, DL.TRANSFERS_IN + DL.TRANSFERS_OUT)
                     + DL.ADJUSTMENTS + DL.IMPAIRMENT_ASSET_AMOUNT)
                     / (decode(STG.SMOOTH_CURVE, 'no', 1, (3-months_between(DL.GL_POST_MO_YR, STG.FISCALQTRSTART))))
                  ),2)
                  * decode(STG.SMOOTH_CURVE, 'no', 1, 3),
               round(sum((DL.RETIREMENTS + DL.GAIN_LOSS + DL.SALVAGE_CASH + DL.SALVAGE_RETURNS +
                     + DL.RESERVE_CREDITS + decode(STG.TRF_IN_EST_ADDS, 'no', 0, DL.RESERVE_TRAN_IN + DL.RESERVE_TRAN_OUT)
                     + DL.RESERVE_ADJUSTMENTS + IMPAIRMENT_ASSET_AMOUNT + IMPAIRMENT_EXPENSE_AMOUNT)
                     / (decode(STG.SMOOTH_CURVE, 'no', 1, (3-months_between(DL.GL_POST_MO_YR, STG.FISCALQTRSTART))))
                  ),2)
                  * decode(STG.SMOOTH_CURVE, 'no', 1, 3),
               round(sum((DL.COST_OF_REMOVAL + DL.COR_RES_ADJUST + DECODE(STG.TRF_IN_EST_ADDS, 'no', 0, DL.COR_RES_TRAN_IN + DL.COR_RES_TRAN_OUT))
                     / (decode(STG.SMOOTH_CURVE, 'no', 1, (3-months_between(DL.GL_POST_MO_YR, STG.FISCALQTRSTART))))
                  ),2)
                  * decode(STG.SMOOTH_CURVE, 'no', 1, 3)
             from FCST_DEPR_LEDGER DL
            where DL.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
              and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
              and DL.GL_POST_MO_YR between STG.FISCALQTRSTART and STG.GL_POST_MO_YR
        and DL.FCST_DEPR_VERSION_ID = G_VERSION_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('qtr', 'quarterly');

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST4');

      -- begin_balance_year and begin_reserve_year for Yearly, SFY
    /*Need to outer join to DL in case record does not exist*/
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_YEAR, BEGIN_RESERVE_YEAR, BEGIN_COR_YEAR) =
              (select NVL(DL.BEGIN_BALANCE, 0), NVL(DL.BEGIN_RESERVE, 0), NVL(DL.COR_BEG_RESERVE,0)
                 from FCST_DEPR_LEDGER DL
                where DL.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR = STG.FISCALYEARSTART
          and DL.FCST_DEPR_VERSION_ID = G_VERSION_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('yearly', 'sfy', 'curve', 'curve_no_true_up')
		 and exists (select 1
					from FCST_DEPR_LEDGER DL2
					where DL2.FCST_DEPR_VERSION_ID = G_VERSION_ID
					and DL2.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
					and DL2.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
					and DL2.GL_POST_MO_YR = STG.FISCALYEARSTART);

      -- begin_balance_year and begin_reserve_year for Yearly, SFY when TRF_IN_EST_ADDS = 'no'
      update DEPR_CALC_STG STG
         set (BEGIN_BALANCE_YEAR, BEGIN_RESERVE_YEAR, BEGIN_COR_YEAR) =
              (select NVL(STG.BEGIN_BALANCE_YEAR + SUM(DL.TRANSFERS_IN + DL.TRANSFERS_OUT), 0), NVL(STG.BEGIN_RESERVE_YEAR + SUM(DL.RESERVE_TRAN_IN + DL.RESERVE_TRAN_OUT), 0)
                      , NVL(STG.BEGIN_COR_YEAR + SUM(DL.COR_RES_TRAN_IN + DL.COR_RES_TRAN_OUT),0)
                 from FCST_DEPR_LEDGER DL
                where DL.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR between STG.FISCALYEARSTART and stg.gl_post_mo_yr
          and DL.FCST_DEPR_VERSION_ID = G_VERSION_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('yearly', 'sfy', 'curve', 'curve_no_true_up')
		 and exists (select 1
					from FCST_DEPR_LEDGER DL2
					where DL2.FCST_DEPR_VERSION_ID = G_VERSION_ID
					and DL2.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
					and DL2.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
					and DL2.GL_POST_MO_YR between STG.FISCALYEARSTART and stg.gl_post_mo_yr)
     and lower(trim(TRF_IN_EST_ADDS)) = 'no';

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST5');

     -- calculate plant activity, reserve_activity, and cor_activity for Yearly, SFY
     update DEPR_CALC_STG STG
        set (STG.PLANT_ACTIVITY_2, STG.RESERVE_ACTIVITY_2, STG.COR_ACTIVITY_2) =
              (select round(sum((dl.ADDITIONS + dl.RETIREMENTS + decode(STG.trf_in_est_adds, 'no', 0, dl.TRANSFERS_IN + dl.TRANSFERS_OUT)
                     + dl.ADJUSTMENTS + dl.IMPAIRMENT_ASSET_AMOUNT)
                     / (decode(STG.smooth_curve, 'no', 1, (12-months_between(dl.gl_post_mo_yr, STG.fiscalyearstart))))
                  ),2)
                  * decode(STG.smooth_curve, 'no', 1, 12),
               round(sum((dl.RETIREMENTS + DL.GAIN_LOSS + DL.SALVAGE_CASH + DL.SALVAGE_RETURNS +
                     + DL.RESERVE_CREDITS + decode(STG.trf_in_est_adds, 'no', 0, DL.RESERVE_TRAN_IN + DL.RESERVE_TRAN_OUT)
                     + DL.RESERVE_ADJUSTMENTS + IMPAIRMENT_ASSET_AMOUNT + IMPAIRMENT_EXPENSE_AMOUNT)
                     / (decode(stg.smooth_curve, 'no', 1, (12-months_between(dl.gl_post_mo_yr, stg.fiscalyearstart))))
                  ),2)
                  * decode(stg.smooth_curve, 'no', 1, 12),
               round(sum((DL.COST_OF_REMOVAL + DL.COR_RES_ADJUST + DECODE(STG.TRF_IN_EST_ADDS, 'no', 0, DL.COR_RES_TRAN_IN + DL.COR_RES_TRAN_OUT))
                     / (decode(stg.smooth_curve, 'no', 1, (12-months_between(dl.gl_post_mo_yr, stg.fiscalyearstart))))
                  ),2)
                  * decode(stg.smooth_curve, 'no', 1, 12)
             from FCST_DEPR_LEDGER DL
            where DL.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
              and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
              and DL.GL_POST_MO_YR between STG.FISCALYEARSTART and STG.GL_POST_MO_YR
        and DL.FCST_DEPR_VERSION_ID = G_VERSION_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD in ('yearly', 'sfy');

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST6');

    -- overwrite est_ann_net_adds for curve if in fiscal month = 12
      update DEPR_CALC_STG STG
         set EST_ANN_NET_ADDS =
              (select case
                         when STG.TRF_IN_EST_ADDS = 'no' then
                          sum(ADDITIONS + RETIREMENTS + ADJUSTMENTS + IMPAIRMENT_ASSET_AMOUNT)
                         else
                          sum(ADDITIONS + RETIREMENTS + TRANSFERS_IN + TRANSFERS_OUT + ADJUSTMENTS +
                              IMPAIRMENT_ASSET_AMOUNT)
                      end
                 from FCST_DEPR_LEDGER DL
                where DL.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
                  and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DL.GL_POST_MO_YR between STG.FISCALYEARSTART and STG.GL_POST_MO_YR
          and DL.FCST_DEPR_VERSION_ID = G_VERSION_ID)
       where DEPR_CALC_STATUS = 1
         and FISCAL_MONTH = 12
         and MID_PERIOD_METHOD in ('curve');

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST7');

update depr_calc_stg
set CURVE_TRUEUP_ADJ = 0,
	 CURVE_TRUEUP_ADJ_SALV = 0,
	CURVE_TRUEUP_ADJ_COR = 0
where DEPR_CALC_STATUS = 1
and MID_PERIOD_METHOD in ('curve', 'curve_no_true_up');

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST10');

	  --make sure all depr_ledger rows exist.
	  PKG_DEPR_FCST.P_FILL_UOP_TABLES(G_VERSION_ID, A_START_MONTH, A_END_MONTH);

      --production, estimated_production, production_2, estimated_production_2, min_calc, and type_2_exist for UOP
      update DEPR_CALC_STG STG
         set (PRODUCTION, ESTIMATED_PRODUCTION, PRODUCTION_2, ESTIMATED_PRODUCTION_2, MIN_CALC, TYPE_2_EXIST) =
              (select least(nvl(PRODUCTION,0), nvl(ESTIMATED_PRODUCTION,0)),
                      nvl(ESTIMATED_PRODUCTION,0),
                      least(nvl(PRODUCTION_2,0), nvl(ESTIMATED_PRODUCTION_2,0)),
                      nvl(ESTIMATED_PRODUCTION_2,0),
                      nvl(MIN_CALC_OPTION,1),
					  case when nvl(UOP_TYPE_ID2,0) > 0 then 1 else 0 end
                 from FCST_DEPR_METHOD_UOP M
                where M.FCST_DEPR_METHOD_ID = STG.DEPR_METHOD_ID
                  and M.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and M.GL_POST_MO_YR = STG.GL_POST_MO_YR
				  and M.FCST_DEPR_VERSION_ID = G_VERSION_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD = 'uop'
		 and exists (
			select 1
			from FCST_DEPR_METHOD_UOP M
			where M.FCST_DEPR_METHOD_ID = STG.DEPR_METHOD_ID
			  and M.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
			  and M.GL_POST_MO_YR = STG.GL_POST_MO_YR
			  and M.FCST_DEPR_VERSION_ID = G_VERSION_ID) ;

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST11');

    update DEPR_CALC_STG STG
       set (STG.MIN_ANNUAL_EXPENSE, STG.MAX_ANNUAL_EXPENSE) = (
        select nvl(DGU.MIN_ANNUAL_EXPENSE,0), nvl(DGU.MAX_ANNUAL_EXPENSE,0)
          from FCST_DEPR_GROUP_UOP DGU
         where DGU.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
           and DGU.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
           and DGU.GL_POST_MO_YR = STG.GL_POST_MO_YR
           and DGU.FCST_DEPR_VERSION_ID = G_VERSION_ID)
     where STG.DEPR_CALC_STATUS = 1
       and STG.MID_PERIOD_METHOD = 'uop'
       and STG.MIN_CALC = 1
		 and exists (
			select 1
			from FCST_DEPR_GROUP_UOP M
			where M.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
			  and M.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
			  and M.GL_POST_MO_YR = STG.GL_POST_MO_YR
			  and M.FCST_DEPR_VERSION_ID = G_VERSION_ID) ;

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST12');

      --cum_uop_depr, cum_uop_depr_2 for UOP
      update DEPR_CALC_STG STG
         set (STG.CUM_UOP_DEPR, STG.CUM_UOP_DEPR_2, STG.CUM_RATE_DEPR, STG.CUM_LEDGER_DEPR_EXPENSE) =
              (select NVL(sum(DGO.UOP_DEPR_EXPENSE), 0), NVL(sum(DGO.UOP_DEPR_EXPENSE_2), 0), NVL(sum(DGO.RATE_DEPR_EXPENSE), 0), NVL(sum(DGO.LEDGER_DEPR_EXPENSE + DGO.UOP_EXP_ADJ),0)
                 from FCST_DEPR_GROUP_UOP DGO
                where DGO.GL_POST_MO_YR < STG.GL_POST_MO_YR
                  and DGO.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DGO.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
				  and DGO.FCST_DEPR_VERSION_ID = G_VERSION_ID)
       where STG.DEPR_CALC_STATUS = 1
         and STG.MID_PERIOD_METHOD = 'uop'
		 and (STG.NET_GROSS = 1
		  or STG.MIN_CALC = 3);

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST13');

       --reserve_diff for UOP  (I think this is needed for net method...)
      update DEPR_CALC_STG STG
         set STG.RESERVE_DIFF = STG.BEGIN_RESERVE -
			nvl((select sum(DGO.LEDGER_DEPR_EXPENSE)
			 from FCST_DEPR_GROUP_UOP DGO
			where DGO.GL_POST_MO_YR < STG.GL_POST_MO_YR
			  and DGO.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
			  and DGO.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
			  and DGO.FCST_DEPR_VERSION_ID = G_VERSION_ID), 0 )
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD = 'uop';

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST14');

      --ytd_uop_depr, ytd_uop_depr_2 for UOP
      update DEPR_CALC_STG STG
         set (YTD_UOP_DEPR, YTD_UOP_DEPR_2, YTD_RATE_DEPR, YTD_LEDGER_DEPR_EXPENSE) =
              (select NVL(sum(UOP_DEPR_EXPENSE), 0), NVL(sum(UOP_DEPR_EXPENSE_2), 0), NVL(sum(RATE_DEPR_EXPENSE), 0), NVL(sum(LEDGER_DEPR_EXPENSE + UOP_EXP_ADJ), 0)
                 from FCST_DEPR_GROUP_UOP DG
                where DG.GL_POST_MO_YR < STG.GL_POST_MO_YR
                  and DG.GL_POST_MO_YR >= STG.FISCALYEARSTART
                  and DG.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
                  and DG.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
          and DG.FCST_DEPR_VERSION_ID = G_VERSION_ID)
       where DEPR_CALC_STATUS = 1
         and MID_PERIOD_METHOD = 'uop';

PKG_PP_ERROR.REMOVE_MODULE_NAME;
PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA_FCST15');

      --COR_YTD AND SALV_YTD
      update DEPR_CALC_STG STG
         set (COR_YTD, SALV_YTD) = (
				select YTD.COR_YTD, YTD.SALV_YTD
				from (select distinct DL.FCST_DEPR_GROUP_ID, DL.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR,
						nvl(sum(DL.COST_OF_REMOVAL) OVER (PARTITION BY DL.FCST_DEPR_GROUP_ID, DL.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR ORDER BY STG.GL_POST_MO_YR DESC),0) COR_YTD,
						nvl(sum(DL.SALVAGE_CASH - DL.SALVAGE_RETURNS) OVER (PARTITION BY DL.FCST_DEPR_GROUP_ID, DL.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR ORDER BY STG.GL_POST_MO_YR DESC),0) SALV_YTD
					from FCST_DEPR_LEDGER DL, DEPR_CALC_STG STG
					where DL.FCST_DEPR_VERSION_ID = G_VERSION_ID
					and DL.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
					and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
					and DL.GL_POST_MO_YR >= STG.FISCALYEARSTART
					and DL.GL_POST_MO_YR < STG.GL_POST_MO_YR) YTD
				where YTD.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
				and YTD.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
				and YTD.GL_POST_MO_YR = STG.GL_POST_MO_YR)
       where STG.DEPR_CALC_STATUS = 1
         and STG.COR_TREATMENT + STG.SALVAGE_TREATMENT > 0
		 and exists (
				select 1
				from (select distinct DL.FCST_DEPR_GROUP_ID, DL.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR,
						nvl(sum(DL.COST_OF_REMOVAL) OVER (PARTITION BY DL.FCST_DEPR_GROUP_ID, DL.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR ORDER BY STG.GL_POST_MO_YR DESC),0) COR_YTD,
						nvl(sum(DL.SALVAGE_CASH - DL.SALVAGE_RETURNS) OVER (PARTITION BY DL.FCST_DEPR_GROUP_ID, DL.SET_OF_BOOKS_ID, STG.GL_POST_MO_YR ORDER BY STG.GL_POST_MO_YR DESC),0) SALV_YTD
					from FCST_DEPR_LEDGER DL, DEPR_CALC_STG STG
					where DL.FCST_DEPR_VERSION_ID = G_VERSION_ID
					and DL.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
					and DL.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
					and DL.GL_POST_MO_YR >= STG.FISCALYEARSTART
					and DL.GL_POST_MO_YR < STG.GL_POST_MO_YR) YTD
				where YTD.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
				and YTD.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
				and YTD.GL_POST_MO_YR = STG.GL_POST_MO_YR);

	    PKG_PP_ERROR.REMOVE_MODULE_NAME;
	PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.F_BACKFILLDATA16');

      --inorporate the spreadfactors rate into the depr rate
      update DEPR_CALC_STG STG
		set (stg.RATE, stg.DMR_COST_OF_REMOVAL_RATE, stg.DMR_SALVAGE_RATE) =
		(
			select 12 * stg.rate * NVL(S.factor , 0),
				12 * stg.DMR_COST_OF_REMOVAL_RATE * NVL(S.factor , 0),
				12 * stg.DMR_SALVAGE_RATE * NVL(S.factor , 0)
			from SPREAD_FACTOR_MONTHLY s
			where s.factor_id = stg.spread_factor_id
			and s.fiscal_month = stg.fiscal_month
		)
		where exists
		(
			select 1
			from SPREAD_FACTOR_MONTHLY s
			where s.factor_id = stg.spread_factor_id
			and s.fiscal_month = stg.fiscal_month
		)
		and STG.MID_PERIOD_METHOD in ('curve','curve_no_true_up')
		;

    PKG_PP_ERROR.REMOVE_MODULE_NAME;

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
   exception
      when others then
         L_STATUS := sqlerrm;
         update DEPR_CALC_STG set DEPR_CALC_STATUS = -25, DEPR_CALC_MESSAGE = L_STATUS;
     PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_BACKFILLDATA_FCST;


   --**************************************************************************
   --                            P_BUILD_VINTAGE_SUMMARY
   --**************************************************************************
   -- Builds DEPR_VINTAGE_SUMMARY for the passed in DGs for passed in month
  procedure P_BUILD_VINTAGE_SUMMARY(  A_DG_IDS PKG_PP_COMMON.NUM_TABTYPE,
                    A_START_MONTH date) is
  begin
    PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_BUILD_VINTAGE_SUMMARY');

    forall i in indices of A_DG_IDS
    merge into DEPR_VINTAGE_SUMMARY D
    using (select SET_OF_BOOKS_ID,
            DEPR_GROUP_ID,
            VINTAGE,
            sum(ACCUM_COST) as ACCUM_COST,
            ACCOUNTING_MONTH,
            COMBINED_DEPR_GROUP_ID
         from (with MAX_CADG as (select CADG.ASSET_ID,
                        GL_POSTING_MO_YR as MAX_MONTH,
                        ROW_NUMBER() OVER(partition by ASSET_ID order by GL_POSTING_MO_YR desc) as THE_ROW
                       from CPR_ACT_DEPR_GROUP CADG
                      where CADG.GL_POSTING_MO_YR <= A_START_MONTH)
              select DV.SET_OF_BOOKS_ID,
                 DV.DEPR_GROUP_ID,
                 DV.VINTAGE,
                 DV.ACCUM_COST,
                 A_START_MONTH as ACCOUNTING_MONTH,
                 DG.COMBINED_DEPR_GROUP_ID
              from DEPR_VINTAGE_SUMMARY DV, DEPR_GROUP DG
               where DV.DEPR_GROUP_ID = DG.DEPR_GROUP_ID
               and DG.DEPR_GROUP_ID = A_DG_IDS(i)
               and DV.ACCOUNTING_MONTH = ADD_MONTHS(A_START_MONTH, -1)
              union all
              select SET_OF_BOOKS.SET_OF_BOOKS_ID,
                 CPR_ACT_DEPR_GROUP.DEPR_GROUP_ID,
                 TO_NUMBER(TO_CHAR(CPR_LEDGER.ENG_IN_SERVICE_YEAR, 'YYYY')),

                 sum(NVL(CPR_ACT_BASIS.BASIS_1 * SET_OF_BOOKS.BASIS_1_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_2 * SET_OF_BOOKS.BASIS_2_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_3 * SET_OF_BOOKS.BASIS_3_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_4 * SET_OF_BOOKS.BASIS_4_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_5 * SET_OF_BOOKS.BASIS_5_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_6 * SET_OF_BOOKS.BASIS_6_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_7 * SET_OF_BOOKS.BASIS_7_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_8 * SET_OF_BOOKS.BASIS_8_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_9 * SET_OF_BOOKS.BASIS_9_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_10 * SET_OF_BOOKS.BASIS_10_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_11 * SET_OF_BOOKS.BASIS_11_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_12 * SET_OF_BOOKS.BASIS_12_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_13 * SET_OF_BOOKS.BASIS_13_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_14 * SET_OF_BOOKS.BASIS_14_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_15 * SET_OF_BOOKS.BASIS_15_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_16 * SET_OF_BOOKS.BASIS_16_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_17 * SET_OF_BOOKS.BASIS_17_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_18 * SET_OF_BOOKS.BASIS_18_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_19 * SET_OF_BOOKS.BASIS_19_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_20 * SET_OF_BOOKS.BASIS_20_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_21 * SET_OF_BOOKS.BASIS_21_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_22 * SET_OF_BOOKS.BASIS_22_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_23 * SET_OF_BOOKS.BASIS_23_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_24 * SET_OF_BOOKS.BASIS_24_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_25 * SET_OF_BOOKS.BASIS_25_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_26 * SET_OF_BOOKS.BASIS_26_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_27 * SET_OF_BOOKS.BASIS_27_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_28 * SET_OF_BOOKS.BASIS_28_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_29 * SET_OF_BOOKS.BASIS_29_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_30 * SET_OF_BOOKS.BASIS_30_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_31 * SET_OF_BOOKS.BASIS_31_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_32 * SET_OF_BOOKS.BASIS_32_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_33 * SET_OF_BOOKS.BASIS_33_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_34 * SET_OF_BOOKS.BASIS_34_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_35 * SET_OF_BOOKS.BASIS_35_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_36 * SET_OF_BOOKS.BASIS_36_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_37 * SET_OF_BOOKS.BASIS_37_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_38 * SET_OF_BOOKS.BASIS_38_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_39 * SET_OF_BOOKS.BASIS_39_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_40 * SET_OF_BOOKS.BASIS_40_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_41 * SET_OF_BOOKS.BASIS_41_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_42 * SET_OF_BOOKS.BASIS_42_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_43 * SET_OF_BOOKS.BASIS_43_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_44 * SET_OF_BOOKS.BASIS_44_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_45 * SET_OF_BOOKS.BASIS_45_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_46 * SET_OF_BOOKS.BASIS_46_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_47 * SET_OF_BOOKS.BASIS_47_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_48 * SET_OF_BOOKS.BASIS_48_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_49 * SET_OF_BOOKS.BASIS_49_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_50 * SET_OF_BOOKS.BASIS_50_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_51 * SET_OF_BOOKS.BASIS_51_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_52 * SET_OF_BOOKS.BASIS_52_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_53 * SET_OF_BOOKS.BASIS_53_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_54 * SET_OF_BOOKS.BASIS_54_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_55 * SET_OF_BOOKS.BASIS_55_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_56 * SET_OF_BOOKS.BASIS_56_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_57 * SET_OF_BOOKS.BASIS_57_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_58 * SET_OF_BOOKS.BASIS_58_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_59 * SET_OF_BOOKS.BASIS_59_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_60 * SET_OF_BOOKS.BASIS_60_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_61 * SET_OF_BOOKS.BASIS_61_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_62 * SET_OF_BOOKS.BASIS_62_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_63 * SET_OF_BOOKS.BASIS_63_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_64 * SET_OF_BOOKS.BASIS_64_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_65 * SET_OF_BOOKS.BASIS_65_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_66 * SET_OF_BOOKS.BASIS_66_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_67 * SET_OF_BOOKS.BASIS_67_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_68 * SET_OF_BOOKS.BASIS_68_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_69 * SET_OF_BOOKS.BASIS_69_INDICATOR, 0) +
                   NVL(CPR_ACT_BASIS.BASIS_70 * SET_OF_BOOKS.BASIS_70_INDICATOR, 0)),

                 CPR_ACTIVITY.GL_POSTING_MO_YR,
                 DEPR_GROUP.COMBINED_DEPR_GROUP_ID
              from SET_OF_BOOKS,
                 CPR_ACTIVITY,
                 CPR_ACT_BASIS,
                 CPR_LEDGER,
                 DEPR_GROUP,
                 COMPANY_SET_OF_BOOKS,
                 CPR_ACT_DEPR_GROUP,
                 MAX_CADG
               where CPR_LEDGER.ASSET_ID = CPR_ACTIVITY.ASSET_ID
               and CPR_LEDGER.LEDGER_STATUS < 100
               and CPR_ACTIVITY.ASSET_ID = CPR_ACT_BASIS.ASSET_ID
               and CPR_ACTIVITY.ASSET_ACTIVITY_ID = CPR_ACT_BASIS.ASSET_ACTIVITY_ID
               and CPR_LEDGER.ASSET_ID = CPR_ACT_DEPR_GROUP.ASSET_ID
               and MAX_CADG.ASSET_ID = CPR_LEDGER.ASSET_ID
               and MAX_CADG.THE_ROW = 1
               and CPR_ACT_DEPR_GROUP.GL_POSTING_MO_YR = MAX_CADG.MAX_MONTH
               and CPR_ACT_DEPR_GROUP.DEPR_GROUP_ID = DEPR_GROUP.DEPR_GROUP_ID
               and COMPANY_SET_OF_BOOKS.COMPANY_ID = DEPR_GROUP.COMPANY_ID
               and COMPANY_SET_OF_BOOKS.SET_OF_BOOKS_ID = SET_OF_BOOKS.SET_OF_BOOKS_ID
               and CPR_ACTIVITY.GL_POSTING_MO_YR = A_START_MONTH
               and DEPR_GROUP.DEPR_GROUP_ID = A_DG_IDS(I)
               group by SET_OF_BOOKS.SET_OF_BOOKS_ID,
                  CPR_ACT_DEPR_GROUP.DEPR_GROUP_ID,
                  TO_NUMBER(TO_CHAR(CPR_LEDGER.ENG_IN_SERVICE_YEAR, 'YYYY')),
                  DEPR_GROUP.COMBINED_DEPR_GROUP_ID,
                  CPR_ACTIVITY.GL_POSTING_MO_YR)
               group by SET_OF_BOOKS_ID,
                  DEPR_GROUP_ID,
                  VINTAGE,
                  ACCOUNTING_MONTH,
                  COMBINED_DEPR_GROUP_ID
         ) B
    on (D.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and D.DEPR_GROUP_ID = B.DEPR_GROUP_ID and D.VINTAGE = B.VINTAGE and D.ACCOUNTING_MONTH = B.ACCOUNTING_MONTH)
    when matched then
      update set D.ACCUM_COST = B.ACCUM_COST
    when not matched then
      insert
        (SET_OF_BOOKS_ID, DEPR_GROUP_ID, VINTAGE, ACCOUNTING_MONTH, COMBINED_DEPR_GROUP_ID,
         ACCUM_COST)
      values
        (B.SET_OF_BOOKS_ID, B.DEPR_GROUP_ID, B.VINTAGE, B.ACCOUNTING_MONTH,
         B.COMBINED_DEPR_GROUP_ID, B.ACCUM_COST);


    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_BUILD_VINTAGE_SUMMARY;

   --**************************************************************************
   --                            P_SET_FISCALYEARSTART
   --**************************************************************************
   -- Sets the beginning of the fiscal year
   procedure P_SET_FISCALYEARSTART is

   begin

    PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_SET_FISCALYEARSTART');

      update DEPR_CALC_STG STG
         set (FISCALYEARSTART, FISCAL_MONTH) =
              (select FY.MONTH, C.FISCAL_MONTH
                 from PP_CALENDAR C, PP_CALENDAR FY
                where C.MONTH = STG.GL_POST_MO_YR
                  and C.FISCAL_YEAR = FY.FISCAL_YEAR
                  and FY.FISCAL_MONTH = 1)
       where STG.DEPR_CALC_STATUS = 1;

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: '||sqlerrm);
   end P_SET_FISCALYEARSTART;

   --**************************************************************************
   --                            P_CHECKINACTIVEDEPR
   --**************************************************************************
   --
   -- Set the companies that are ignoring inactive status
   --
   procedure P_CHECKINACTIVEDEPR(A_COMPANY_ID number,
                                 A_INDEX      number) is

   begin
      PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_CHECKINACTIVEDEPR');

    G_COMPANY_STATUS_CHECK.EXTEND;
    G_COMPANY_STATUS_CHECK(A_INDEX).COMPANY_ID := A_COMPANY_ID;
    G_COMPANY_STATUS_CHECK(A_INDEX).CHECK_INACTIVE := Lower(Nvl(Trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Ignore Inactive Depr Groups',
                                                                          A_COMPANY_ID)),'no'));

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
   end P_CHECKINACTIVEDEPR;

   procedure P_AMORT_STAGE (A_MONTH date) is
   begin
    PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_AMORT_STAGE');
      PKG_PP_LOG.P_WRITE_MESSAGE('LOADING the amortization staging table for month: ' ||
                           TO_CHAR(A_MONTH, 'YYYYMM'));

        insert into DEPR_CALC_AMORT_STG (
            DEPR_GROUP_ID,
            SET_OF_BOOKS_ID,
            GL_POST_MO_YR,
            VINTAGE,
            COR_TREATMENT,
            COR_LIFE,
            PRIOR_COR_BALANCE,
            PRIOR_COR_RESERVE,
            COR_BALANCE,
            COR_AMORT,
            COR_RESERVE,
            SALV_TREATMENT,
            SALV_LIFE,
            PRIOR_SALV_BALANCE,
            PRIOR_SALV_RESERVE,
            SALV_BALANCE,
            SALV_AMORT,
            SALV_RESERVE)
		select STG.DEPR_GROUP_ID,
			STG.SET_OF_BOOKS_ID,
			STG.GL_POST_MO_YR,
			DNSA.VINTAGE,
			decode(STG.COR_TREATMENT, 2, 'month', 1, 'amort', 'no'),
			STG.NET_SALVAGE_AMORT_LIFE,
			nvl(DNSB.COST_OF_REMOVAL_BAL,0) AS PRIOR_COR_BALANCE,
			nvl(DNSB.COST_OF_REMOVAL_RESERVE,0) AS PRIOR_COR_RESERVE,
			decode(nvl(STG.COR_TREATMENT,0), 0, 0, nvl(DNSA.COST_OF_REMOVAL_BAL, 0)) AS COR_BALANCE,
			0 AS COR_AMORT,
			0 AS COR_RESERVE,
			decode(STG.SALVAGE_TREATMENT, 2, 'month', 1, 'amort', 'no'),
			STG.NET_SALVAGE_AMORT_LIFE,
			nvl(DNSB.SALVAGE_BAL,0) AS PRIOR_SALV_BALANCE,
			nvl(DNSB.SALVAGE_RESERVE,0) AS PRIOR_SALV_RESERVE,
			decode(nvl(STG.SALVAGE_TREATMENT,0), 0, 0, nvl(DNSA.SALVAGE_BAL,0)) AS SALV_BALANCE,
			0 AS SALV_AMORT,
			0 AS SALV_RESERVE
		from DEPR_CALC_STG STG, DEPR_NET_SALVAGE_AMORT DNSA, DEPR_NET_SALVAGE_AMORT DNSB
		where STG.DEPR_GROUP_ID = DNSA.DEPR_GROUP_ID
		and STG.SET_OF_BOOKS_ID = DNSA.SET_OF_BOOKS_ID
		and STG.GL_POST_MO_YR = DNSA.GL_POST_MO_YR
		and DNSA.VINTAGE < TO_NUMBER(TO_CHAR(STG.GL_POST_MO_YR,'YYYY'))
		and DNSA.DEPR_GROUP_ID = DNSB.DEPR_GROUP_ID (+)
		and DNSA.SET_OF_BOOKS_ID = DNSB.SET_OF_BOOKS_ID (+)
		and ADD_MONTHS(DNSA.GL_POST_MO_YR,-1) = DNSB.GL_POST_MO_YR (+)
		and DNSA.VINTAGE = DNSB.VINTAGE (+)
		and (STG.COR_TREATMENT <> 0  /*0 is no*/
		 or STG.SALVAGE_TREATMENT <> 0) /*0 is no*/
		union
		select STG.DEPR_GROUP_ID,
			STG.SET_OF_BOOKS_ID,
			STG.GL_POST_MO_YR,
			TO_NUMBER(TO_CHAR(STG.GL_POST_MO_YR,'YYYY')) AS VINTAGE,
			decode(STG.COR_TREATMENT, 2, 'month', 1, 'amort', 'no') AS COR_TREATMENT,
			STG.NET_SALVAGE_AMORT_LIFE,
			nvl(DNSB.COST_OF_REMOVAL_BAL,0) AS PRIOR_COR_BALANCE,
			nvl(DNSB.COST_OF_REMOVAL_RESERVE,0) AS PRIOR_COR_RESERVE,
			decode(nvl(STG.COR_TREATMENT,0), 2, nvl(DNSB.COST_OF_REMOVAL_BAL, 0) + DL.COST_OF_REMOVAL, 0) AS COR_BALANCE,
			0 AS COR_AMORT,
			0 AS COR_RESERVE,
			decode(STG.SALVAGE_TREATMENT, 2, 'month', 1, 'amort', 'no') AS SALVAGE_TREATMENT,
			STG.NET_SALVAGE_AMORT_LIFE,
			nvl(DNSB.SALVAGE_BAL,0) AS PRIOR_SALV_BALANCE,
			nvl(DNSB.SALVAGE_RESERVE,0) AS PRIOR_SALV_RESERVE,
			decode(nvl(STG.SALVAGE_TREATMENT,0), 2, nvl(DNSB.SALVAGE_BAL,0) + DL.SALVAGE_CASH + DL.SALVAGE_RETURNS, 0) AS SALV_BALANCE,
			0 AS SALV_AMORT,
			0 AS SALV_RESERVE
		from DEPR_CALC_STG STG, DEPR_NET_SALVAGE_AMORT DNSB, DEPR_LEDGER DL
		where STG.DEPR_GROUP_ID = DL.DEPR_GROUP_ID
		and STG.SET_OF_BOOKS_ID = DL.SET_OF_BOOKS_ID
		and STG.GL_POST_MO_YR = DL.GL_POST_MO_YR
		and STG.DEPR_GROUP_ID = DNSB.DEPR_GROUP_ID (+)
		and STG.SET_OF_BOOKS_ID = DNSB.SET_OF_BOOKS_ID (+)
		and ADD_MONTHS(STG.GL_POST_MO_YR,-1) = DNSB.GL_POST_MO_YR (+)
		and TO_NUMBER(TO_CHAR(STG.GL_POST_MO_YR,'YYYY')) = DNSB.VINTAGE (+)
		and (STG.COR_TREATMENT = 2  /*2 is monthly */
		 or STG.SALVAGE_TREATMENT = 2);

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
   end P_AMORT_STAGE;

   procedure P_FCST_AMORT_STAGE IS
       V_COUNT INTEGER;
   begin
    PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_FCST_AMORT_STAGE');
      PKG_PP_LOG.P_WRITE_MESSAGE('LOADING the amortization staging table.');

    /*----------------------------------------------------------------------------
    //insert records that we need to calc for previous years.
    //
    //The top half is for vintages before the "current" year (monthly and annual),
    //the bottom half is for the monthly accumulation (monthly only).
    //
    //I'm making the assumption that any records staged in depr_calc_stg should also be
    //staged here (assuming cor_treatment and/or salvage_treatment is not NO), so I
    //am not putting any restriction on months.
    -------------------------------------------------------------------------------*/
    insert into DEPR_CALC_AMORT_STG (DEPR_GROUP_ID,
      SET_OF_BOOKS_ID,
      GL_POST_MO_YR,
      VINTAGE,
      COR_TREATMENT,
      COR_LIFE,
      PRIOR_COR_BALANCE,
      PRIOR_COR_RESERVE,
      COR_BALANCE,
      COR_AMORT,
      COR_RESERVE,
      SALV_TREATMENT,
      SALV_LIFE,
      PRIOR_SALV_BALANCE,
      PRIOR_SALV_RESERVE,
      SALV_BALANCE,
      SALV_AMORT,
      SALV_RESERVE)
    select STG.DEPR_GROUP_ID,
      STG.SET_OF_BOOKS_ID,
      STG.GL_POST_MO_YR,
      TOTALS.VINTAGE,
      decode(STG.COR_TREATMENT, 2, 'month', 1, 'amort', 'no'),
      STG.NET_SALVAGE_AMORT_LIFE,
      0 AS PRIOR_COR_BALANCE,
      0 AS PRIOR_COR_RESERVE,
      decode(STG.COR_TREATMENT, 0, 0, TOTALS.COR_BALANCE),
      0 AS COR_AMORT,
      0 AS COR_RESERVE,
      decode(STG.SALVAGE_TREATMENT, 2, 'month', 1, 'amort', 'no'),
      STG.NET_SALVAGE_AMORT_LIFE,
      0 AS PRIOR_SALV_BALANCE,
      0 AS PRIOR_SALV_RESERVE,
      decode(STG.SALVAGE_TREATMENT, 0, 0, TOTALS.SALVAGE_BALANCE),
      0 AS SALV_AMORT,
      0 AS SALV_RESERVE
    from DEPR_CALC_STG STG,
      (select FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, TO_NUMBER(TO_CHAR(GL_POST_MO_YR,'YYYY')) AS VINTAGE,
        SUM(COST_OF_REMOVAL) AS COR_BALANCE,
        SUM(SALVAGE_CASH + SALVAGE_RETURNS) SALVAGE_BALANCE
      from FCST_DEPR_LEDGER
      where FCST_DEPR_VERSION_ID = G_VERSION_ID
      group by FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, TO_CHAR(GL_POST_MO_YR,'YYYY')) TOTALS
    where STG.DEPR_GROUP_ID = TOTALS.FCST_DEPR_GROUP_ID
    and STG.SET_OF_BOOKS_ID = TOTALS.SET_OF_BOOKS_ID
    and TOTALS.VINTAGE < TO_NUMBER(TO_CHAR(GL_POST_MO_YR,'YYYY'))
    and (STG.COR_TREATMENT <> 0  /*0 is no*/
     or STG.SALVAGE_TREATMENT <> 0) /*0 is no*/
    and STG.FCST_DEPR_VERSION_ID = G_VERSION_ID
    union
    select STG.DEPR_GROUP_ID,
      STG.SET_OF_BOOKS_ID,
      STG.GL_POST_MO_YR,
      TO_NUMBER(TO_CHAR(STG.GL_POST_MO_YR,'YYYY')) AS VINTAGE,
      decode(STG.COR_TREATMENT, 2, 'month', 1, 'amort', 'no'),
      STG.NET_SALVAGE_AMORT_LIFE,
      0 AS PRIOR_COR_BALANCE,
      0 AS PRIOR_COR_RESERVE,
      decode(STG.COR_TREATMENT, 0, 0, TOTALS.COR_BALANCE),
      0 AS COR_AMORT,
      0 AS COR_RESERVE,
      decode(STG.SALVAGE_TREATMENT, 2, 'month', 1, 'amort', 'no'),
      STG.NET_SALVAGE_AMORT_LIFE,
      0 AS PRIOR_SALV_BALANCE,
      0 AS PRIOR_SALV_RESERVE,
      decode(STG.SALVAGE_TREATMENT, 0, 0, TOTALS.SALVAGE_BALANCE),
      0 AS SALV_AMORT,
      0 AS SALV_RESERVE
    from DEPR_CALC_STG STG,
      (select FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR,
        SUM(COST_OF_REMOVAL) OVER (PARTITION BY FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, TO_CHAR(GL_POST_MO_YR,'YYYY') ORDER BY GL_POST_MO_YR) COR_BALANCE,
        SUM(SALVAGE_CASH + SALVAGE_RETURNS) OVER (PARTITION BY FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, TO_CHAR(GL_POST_MO_YR,'YYYY') ORDER BY GL_POST_MO_YR) SALVAGE_BALANCE
      from FCST_DEPR_LEDGER DL
      where FCST_DEPR_VERSION_ID = G_VERSION_ID) TOTALS
    where STG.DEPR_GROUP_ID = TOTALS.FCST_DEPR_GROUP_ID
    and STG.SET_OF_BOOKS_ID = TOTALS.SET_OF_BOOKS_ID
    and STG.GL_POST_MO_YR = TOTALS.GL_POST_MO_YR
    and (STG.COR_TREATMENT = 2  /*2 is monthly*/
     or STG.SALVAGE_TREATMENT = 2) /*2 is monthly*/
    and STG.FCST_DEPR_VERSION_ID = G_VERSION_ID;

    /*For first month, prime "prior" values with prior month's values from fcst_net_salvage_amort or depr_net_salvage_amort
    If the last month's values do not exist, they will not be updated. */
    update DEPR_CALC_AMORT_STG STG
    set (PRIOR_COR_BALANCE, PRIOR_COR_RESERVE, PRIOR_SALV_BALANCE, PRIOR_SALV_RESERVE) =
      (select COST_OF_REMOVAL_BAL, COST_OF_REMOVAL_RESERVE, SALVAGE_BAL, SALVAGE_RESERVE
      from (select DEPR_GROUP_ID, SET_OF_BOOKS_ID, MIN(GL_POST_MO_YR) MIN_MONTH
        from DEPR_CALC_STG
        group by DEPR_GROUP_ID, SET_OF_BOOKS_ID) MIN_MONTHS,
        FCST_NET_SALVAGE_AMORT FNSA
      where STG.DEPR_GROUP_ID = MIN_MONTHS.DEPR_GROUP_ID
      and STG.SET_OF_BOOKS_ID = MIN_MONTHS.SET_OF_BOOKS_ID
      and STG.GL_POST_MO_YR = MIN_MONTHS.MIN_MONTH
      and STG.DEPR_GROUP_ID = FNSA.FCST_DEPR_GROUP_ID
      and STG.SET_OF_BOOKS_ID = FNSA.SET_OF_BOOKS_ID
      and ADD_MONTHS(STG.GL_POST_MO_YR, -1) = FNSA.GL_POST_MO_YR
      and STG.VINTAGE = FNSA.VINTAGE)
    where exists
      (select 1
      from (select DEPR_GROUP_ID, SET_OF_BOOKS_ID, MIN(GL_POST_MO_YR) MIN_MONTH
        from DEPR_CALC_STG
        group by DEPR_GROUP_ID, SET_OF_BOOKS_ID) MIN_MONTHS,
        FCST_NET_SALVAGE_AMORT FNSA
      where STG.DEPR_GROUP_ID = MIN_MONTHS.DEPR_GROUP_ID
      and STG.SET_OF_BOOKS_ID = MIN_MONTHS.SET_OF_BOOKS_ID
      and STG.GL_POST_MO_YR = MIN_MONTHS.MIN_MONTH
      and STG.DEPR_GROUP_ID = FNSA.FCST_DEPR_GROUP_ID
      and STG.SET_OF_BOOKS_ID = FNSA.SET_OF_BOOKS_ID
      and ADD_MONTHS(STG.GL_POST_MO_YR, -1) = FNSA.GL_POST_MO_YR
      and STG.VINTAGE = FNSA.VINTAGE);

    select Count(1)
    into V_COUNT
    from DEPR_CALC_AMORT_STG;

    PKG_PP_LOG.P_WRITE_MESSAGE('  ' || V_COUNT || ' records staged in DEPR_CALC_AMORT_STG');

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
   end P_FCST_AMORT_STAGE;

   procedure P_STAGE_BLENDING_METHOD
   is
		l_msg varchar2(2000);
   begin
		PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_STAGE_BLENDING_METHOD');

		delete from depr_ledger_blending_temp;

		insert into depr_ledger_blending_temp
		(
			depr_method_id, target_set_of_books_id,
			effective_date, source_set_of_books_id,
			source_percent, priority
		)
		select depr_method_id, target_set_of_books_id,
			effective_date, source_set_of_books_id,
			source_percent, priority
		from depr_method_blending
		;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
   exception
	when others then
		PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_STAGE_BLENDING_METHOD;


      --**************************************************************************
   --                            P_STAGEMONTHENDDEPR_BL
   --**************************************************************************
   --
   --  Loads the global temp table for calculating depreciation for blended groups.
   --
   procedure P_STAGEMONTHENDDEPR_BL(A_MONTH date) is

   begin
    PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_STAGEMONTHENDDEPR_BL '||to_char(A_MONTH));
      PKG_PP_LOG.P_WRITE_MESSAGE('LOADING the calculation staging table for month:' ||
                                 TO_CHAR(A_MONTH, 'yyyymm'));

      forall I in indices of G_COMPANY_STATUS_CHECK
         insert into DEPR_CALC_STG
			(DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
			DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, RWIP_IN_OVER_DEPR, SMOOTH_CURVE,
			DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD, COMPANY_ID, SUBLEDGER_TYPE_ID,
			DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV, MID_PERIOD_METHOD,
			RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE,
			END_OF_LIFE, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE,
			COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT,
			SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE, DEPR_LEDGER_STATUS,
			BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE,
			RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
			RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR, BEGIN_BALANCE,
			ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE,
			END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
			DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS,
			SALVAGE_CASH, RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT,
			GAIN_LOSS, VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE,
			CURRENT_NET_SALVAGE_AMORT, CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG,
			IMPAIRMENT_RESERVE_ACT, IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION,
			COR_BEG_RESERVE, COR_EXPENSE, COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN,
			COR_RES_TRAN_OUT, COR_RES_ADJUST, COR_END_RESERVE, COST_OF_REMOVAL_RATE,
			COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL, RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS,
			RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST,
			SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT,
			RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER,
			IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT,
			ORIG_DEPR_EXPENSE, ORIG_DEPR_EXP_ALLOC_ADJUST, ORIG_SALV_EXPENSE,
			ORIG_SALV_EXP_ALLOC_ADJUST, ORIG_COR_EXP_ALLOC_ADJUST, ORIG_COR_EXPENSE,
			ORIG_BEGIN_RESERVE, ORIG_COR_BEG_RESERVE, IMPAIRMENT_ASSET_ACTIVITY_SALV, IMPAIRMENT_ASSET_BEGIN_BALANCE,
			CURVE_TRUEUP_ADJ, CURVE_TRUEUP_ADJ_SALV, CURVE_TRUEUP_ADJ_COR, SPREAD_FACTOR_ID, UOP_EXP_ADJ,
			COMBINED_DEPR_GROUP_ID, MORTALITY_CURVE_ID)
            select DL.DEPR_GROUP_ID, --  depr_group_id                   NULL,
                   DL.SET_OF_BOOKS_ID, --  set_of_books_id              NUMBER(22,0)   NULL,
                   DL.GL_POST_MO_YR, --  gl_post_mo_yr                DATE           NULL,
                   A_MONTH,   --  calc_month                   DATE           NULL,
                   1, --  depr_calc_status             NUMBER(2,0)    NULL,
                   '', --  depr_calc_message            VARCHAR2(2000) NULL,
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                                               DG.COMPANY_ID),
                             'no')), --  trf_in_est_adds              VARCHAR2(35)   NULL,
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
                                                                               DG.COMPANY_ID),
                             'no')), --  include_rwip_in_net          VARCHAR2(35)   NULL,
                   decode(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Over Depr Check',
                                                                               DG.COMPANY_ID),
                             'no')), 'yes', 1, 0), -- RWIP_IN_OVER_DEPR
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('DEPR - Smooth Curve True-up',
                                                                               DG.COMPANY_ID),
                             '0')), -- smooth_curve
                   0, --  dnsa_cor_bal                 NUMBER(22,2)   NULL,
                   0, --  dnsa_salv_bal                NUMBER(22,2)   NULL,
                   0, --  cor_ytd                      NUMBER(22,2)   NULL,
                   0, --  salv_ytd                     NUMBER(22,2)   NULL,
                   DG.COMPANY_ID, --  company_id                   NUMBER(22,0)   NULL,
                   DG.SUBLEDGER_TYPE_ID, --  subledger_type_id            NUMBER(22,0)   NULL,
                   DG.DESCRIPTION, --  description                  VARCHAR2(254)  NULL,
                   DG.DEPR_METHOD_ID, --  depr_method_id               NUMBER(22,0)   NULL,
                   DG.MID_PERIOD_CONV, --  mid_period_conv              NUMBER(22,2)   NULL,
                   lower(trim(DG.MID_PERIOD_METHOD)), --  mid_period_method            VARCHAR2(35)   NULL,
                   DMR.RATE, --  rate                         NUMBER(22,8)   NULL,
                   CASE
					  WHEN Lower(Trim(DG.MID_PERIOD_METHOD)) = 'end of life' THEN 1
					  WHEN Lower(Trim(DG.MID_PERIOD_METHOD)) = 'curve' or Lower(Trim(DG.MID_PERIOD_METHOD)) = 'curve_no_true_up' THEN 2
					  ELSE DMR.NET_GROSS
				   END, --  net_gross                    NUMBER(22,0)   NULL,
                   NVL(DMR.OVER_DEPR_CHECK, 0), --  over_depr_check              NUMBER(22,0)   NULL,
                   nvl(DMR.NET_SALVAGE_PCT, 0), --  net_salvage_pct              NUMBER(22,8)   NULL,
                   NVL(DMR.RATE_USED_CODE, 0), --  rate_used_code               NUMBER(22,0)   NULL,
                   DMR.END_OF_LIFE, --  end_of_life                  NUMBER(22,0)   NULL,
                   NVL(DMR.EXPECTED_AVERAGE_LIFE, 0), --  expected_average_life        NUMBER(22,0)   NULL,
                   NVL(DMR.RESERVE_RATIO_ID, 0), --  reserve_ratio_id             NUMBER(22,0)   NULL,
                   nvl(DMR.COST_OF_REMOVAL_RATE, 0), --  dmr_cost_of_removal_rate     NUMBER(22,8)   NULL,
                   nvl(DMR.COST_OF_REMOVAL_PCT, 0), --  cost_of_removal_pct          NUMBER(22,8)   NULL,
                   nvl(DMR.SALVAGE_RATE, 0), --  dmr_salvage_rate             NUMBER(22,8)   NULL,
                   DMR.INTEREST_RATE, --  interest_rate                NUMBER(22,8)   NULL,
                   NVL(DMR.AMORTIZABLE_LIFE, 0), --  amortizable_life             NUMBER(22,0)   NULL,
                   DECODE(LOWER(NVL(TRIM(DMR.COR_TREATMENT), 'no')), 'no', 0, 'month', 2, 1), --  cor_treatment                NUMBER(1,0)    NULL,
                   DECODE(LOWER(NVL(TRIM(DMR.SALVAGE_TREATMENT), 'no')), 'no', 0, 'month', 2, 1), --  salvage_treatment            NUMBER(1,0)    NULL,
                   NVL(DMR.NET_SALVAGE_AMORT_LIFE, 1), --  net_salvage_amort_life       NUMBER(22,0)   NULL,
                   nvl(DMR.ALLOCATION_PROCEDURE, 'BROAD'), --  allocation_procedure         VARCHAR2(5)    NULL,
                   DL.DEPR_LEDGER_STATUS, --  depr_ledger_status           NUMBER(22,0)   NULL,
                   DL.BEGIN_RESERVE, --  begin_reserve                NUMBER(22,2)   NULL,
                   0 AS END_RESERVE, --  end_reserve                  NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_PROVISION, --  reserve_bal_provision        NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_COR, --  reserve_bal_cor              NUMBER(22,2)   NULL,
                   0 as SALVAGE_BALANCE, --  salvage_balance              NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_ADJUST, --  reserve_bal_adjust           NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_RETIREMENTS, --  reserve_bal_retirements      NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_TRAN_IN, --  reserve_bal_tran_in          NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_TRAN_OUT, --  reserve_bal_tran_out         NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_OTHER_CREDITS, --  reserve_bal_other_credits    NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_GAIN_LOSS, --  reserve_bal_gain_loss        NUMBER(22,2)   NULL,
                   DL.RESERVE_ALLOC_FACTOR, --  reserve_alloc_factor         NUMBER(22,2)   NULL,
                   DL.BEGIN_BALANCE, --  begin_balance                NUMBER(22,2)   NULL,
                   DL.ADDITIONS, --  additions
                   DL.RETIREMENTS, --  retirements                  NUMBER(22,2)   NULL,
                   DL.TRANSFERS_IN, --  transfers_in                 NUMBER(22,2)   NULL,
                   DL.TRANSFERS_OUT, --  transfers_out                NUMBER(22,2)   NULL,
                   DL.ADJUSTMENTS, --  adjustments                  NUMBER(22,2)   NULL,
                   0 AS DEPRECIATION_BASE, --  depreciation_base            NUMBER(22,2)
                   0 AS END_BALANCE, --  end_balance                  NUMBER(22,2)   NULL
                   0 AS DEPRECIATION_RATE, --  depreciation_rate            NUMBER(22,8)   NULL,
                   0 AS DEPRECIATION_EXPENSE, --  depreciation_expense         NUMBER(22,2)   NULL,
                   DL.DEPR_EXP_ADJUST, --  depr_exp_adjust              NUMBER(22,2)   NULL,
                   0 AS DEPR_EXP_ALLOC_ADJUST, --  depr_exp_alloc_adjust        NUMBER(22,2)   NULL,
                   DL.COST_OF_REMOVAL, --  cost_of_removal              NUMBER(22,2)   NULL,
                   DL.RESERVE_RETIREMENTS, --  reserve_retirements          NUMBER(22,2)   NULL,
                   DL.SALVAGE_RETURNS, --  salvage_returns              NUMBER(22,2)   NULL,
                   DL.SALVAGE_CASH, --  salvage_cash                 NUMBER(22,2)   NULL,
                   DL.RESERVE_CREDITS, --  reserve_credits              NUMBER(22,2)   NULL,
                   DL.RESERVE_ADJUSTMENTS, --  reserve_adjustments          NUMBER(22,2)   NULL,
                   DL.RESERVE_TRAN_IN, --  reserve_tran_in              NUMBER(22,2)   NULL,
                   DL.RESERVE_TRAN_OUT, --  reserve_tran_out             NUMBER(22,2)   NULL,
                   DL.GAIN_LOSS, --  gain_loss                    NUMBER(22,2)   NULL,
                   DL.VINTAGE_NET_SALVAGE_AMORT, --  vintage_net_salvage_amort    NUMBER(22,2)   NULL,
                   DL.VINTAGE_NET_SALVAGE_RESERVE, --  vintage_net_salvage_reserve  NUMBER(22,2)   NULL,
                   DL.CURRENT_NET_SALVAGE_AMORT, --  current_net_salvage_amort    NUMBER(22,2)   NULL,
                   DL.CURRENT_NET_SALVAGE_RESERVE, --  current_net_salvage_reserve  NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_RESERVE_BEG, --  impairment_reserve_beg       NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_RESERVE_ACT, --  impairment_reserve_act       NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_RESERVE_END, --  impairment_reserve_end       NUMBER(22,2)   NULL,
                   nvl(DG.EST_ANN_NET_ADDS, 0), --  est_ann_net_adds             NUMBER(22,2)   NULL,
                   NVL(DL.RWIP_ALLOCATION, 0), --  rwip_allocation              NUMBER(22,2)   NULL,
                   DL.COR_BEG_RESERVE, --  cor_beg_reserve              NUMBER(22,2)   NULL,
                   0 AS COR_EXPENSE, --  cor_expense                  NUMBER(22,2)   NULL,
                   DL.COR_EXP_ADJUST, --  cor_exp_adjust               NUMBER(22,2)   NULL,
                   0 AS COR_EXP_ALLOC_ADJUST, --  cor_exp_alloc_adjust         NUMBER(22,2)   NULL,
                   DL.COR_RES_TRAN_IN, --  cor_res_tran_in              NUMBER(22,2)   NULL,
                   DL.COR_RES_TRAN_OUT, --  cor_res_tran_out             NUMBER(22,2)   NULL,
                   DL.COR_RES_ADJUST, --  cor_res_adjust               NUMBER(22,2)   NULL,
                   0 AS COR_END_RESERVE, --  cor_end_reserve              NUMBER(22,2)   NULL,
                   DL.COST_OF_REMOVAL_RATE, --  cost_of_removal_rate         NUMBER(22,8)   NULL,
                   DL.COST_OF_REMOVAL_BASE, --  cost_of_removal_base         NUMBER(22,2)   NULL,
                   DL.RWIP_COST_OF_REMOVAL, --  rwip_cost_of_removal         NUMBER(22,2)   NULL,
                   DL.RWIP_SALVAGE_CASH, --  rwip_salvage_cash            NUMBER(22,2)   NULL,
                   DL.RWIP_SALVAGE_RETURNS, --  rwip_salvage_returns         NUMBER(22,2)   NULL,
                   DL.RWIP_RESERVE_CREDITS, --  rwip_reserve_credits         NUMBER(22,2)   NULL,
                   DL.SALVAGE_RATE, --  salvage_rate                 NUMBER(22,8)   NULL,
                   0 AS SALVAGE_BASE, --  salvage_base                 NUMBER(22,2)   NULL,
                   0 AS SALVAGE_EXPENSE, --  salvage_expense              NUMBER(22,2)   NULL,
                   DL.SALVAGE_EXP_ADJUST, --  salvage_exp_adjust           NUMBER(22,2)   NULL,
                   0 AS SALVAGE_EXP_ALLOC_ADJUST, --  salvage_exp_alloc_adjust     NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_SALVAGE_EXP, --  reserve_bal_salvage_exp      NUMBER(22,2)   NULL,
                   0 as RESERVE_BLENDING_ADJUSTMENT, --  reserve_blending_adjustment  NUMBER(22,2)   NULL,
                   0 as RESERVE_BLENDING_TRANSFER, --  reserve_blending_transfer    NUMBER(22,2)   NULL,
                   0 as COR_BLENDING_ADJUSTMENT, --  cor_blending_adjustment      NUMBER(22,2)   NULL,
                   0 as COR_BLENDING_TRANSFER, --  cor_blending_transfer        NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_ASSET_AMOUNT, --  impairment_asset_amount      NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_EXPENSE_AMOUNT, --  impairment_expense_amount    NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_IMPAIRMENT, --  reserve_bal_impairment       NUMBER(22,2)   NUll,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.DEPRECIATION_EXPENSE,0)), --ORIG_DEPR_EXPENSE
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.DEPR_EXP_ALLOC_ADJUST,0)), --  orig_depr_exp_alloc_adjust   NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.SALVAGE_EXPENSE,0)), --  orig_salv_expense            NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.SALVAGE_EXP_ALLOC_ADJUST,0)), --  orig_salv_exp_alloc_adjust   NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.COR_EXP_ALLOC_ADJUST,0)), --  orig_cor_exp_alloc_adjust    NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.COR_EXPENSE,0)), --  orig_cor_expense             NUMBER(22,2)   NULL
                   DL.BEGIN_RESERVE, --orig_begin_reserve          NUMBER(22,2) DEFAULT 0  NULL,
                   DL.COR_BEG_RESERVE, --  orig_cor_begin_reserve     NUMBER(22,2) DEFAULT 0 NULL
				  DL.IMPAIRMENT_ASSET_ACTIVITY_SALV,
				  DL.IMPAIRMENT_ASSET_BEGIN_BALANCE,
				  0 as CURVE_TRUEUP_ADJ,
				  0 as CURVE_TRUEUP_ADJ_SALV,
				  0 as CURVE_TRUEUP_ADJ_COR,
				  DG.FACTOR_ID,
				  0 as UOP_EXP_ADJ,
				  DG.COMBINED_DEPR_GROUP_ID,
				  DMR.MORTALITY_CURVE_ID
              from DEPR_GROUP DG, DEPR_LEDGER DL,
        (
        select dmr1.depr_method_id, dmr1.set_of_books_id, dmr1.EFFECTIVE_DATE, RATE_USED_CODE, RATE,
          allocation_procedure, end_of_life, NET_GROSS, net_salvage_amort_life, salvage_treatment, cor_treatment,
          amortizable_life, interest_rate, salvage_rate, cost_of_removal_pct, cost_of_removal_rate, reserve_ratio_id,
          expected_average_life, net_salvage_pct, over_depr_check, mortality_curve_id
        from DEPR_METHOD_RATES DMR1,
		(
			select distinct r.depr_method_id, r.effective_date
			from depr_method_rates r
			where r.rate_used_code = 3
			and r.effective_date = (select max(rr.effective_date)
									from depr_method_rates rr
									where rr.depr_method_id = r.depr_method_id
									and rr.EFFECTIVE_DATE <= A_MONTH
                  group by rr.depr_method_id)
		) dmr2
        where DMR1.EFFECTIVE_DATE = dmr2.effective_date
		and dmr1.depr_method_id = dmr2.depr_method_id
      ) DMR
	 where DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
	   and DMR.SET_OF_BOOKS_ID = DL.SET_OF_BOOKS_ID
	   and DL.DEPR_GROUP_ID = DG.DEPR_GROUP_ID
	   and DL.GL_POST_MO_YR <= A_MONTH
	   and DL.GL_POST_MO_YR >= case
			when DMR.RATE_USED_CODE = 2 and lower(trim(DG.MID_PERIOD_METHOD)) not in ('curve','curve_no_trueup','uop') then
				DMR.EFFECTIVE_DATE
			else
				A_MONTH
			end
	   and DG.SUBLEDGER_TYPE_ID = 0
	   and DG.COMPANY_ID = G_COMPANY_STATUS_CHECK(I).COMPANY_ID
	   and (G_COMPANY_STATUS_CHECK(I).CHECK_INACTIVE = 'no' /*either the system control is no or we look for one of the following values.*/
		or NVL(DG.STATUS_ID, 1) = 1 or DL.END_BALANCE <> 0 or DL.END_RESERVE <> 0 or
			   DL.COR_END_RESERVE <> 0 or DL.BEGIN_BALANCE <> 0 or DL.BEGIN_RESERVE <> 0 or
			   DL.COR_BEG_RESERVE <> 0);

	G_RTN := analyze_table('DEPR_CALC_STG',100);

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_STAGEMONTHENDDEPR_BL;


   --**************************************************************************
   --                            P_STAGEMONTHENDDEPR_BL
   --**************************************************************************
   --
   --  WRAPPER for the prep function to pass in an array of months.
   --
   procedure P_STAGEMONTHENDDEPR_BL(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                A_MONTHS PKG_PP_COMMON.DATE_TABTYPE) is

		MY_STR varchar2(2000);

   begin
    PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_STAGEMONTHENDDEPR_BL');

    for I in 1 .. A_COMPANY_IDS.COUNT loop
		-- load the arrays for companies ignoring inactive depr groups
		P_CHECKINACTIVEDEPR(A_COMPANY_IDS(I), I);
    end loop;

	delete from depr_calc_stg;
	delete from DEPR_CALC_AMORT_STG;

	for I in A_MONTHS.FIRST .. A_MONTHS.LAST loop
		P_STAGEMONTHENDDEPR_BL(A_MONTHS(I));
		P_AMORT_STAGE(A_MONTHS(I));
	end loop;

	P_STAGE_BLENDING_METHOD();
    P_SET_FISCALYEARSTART();

    -- BACKFILL VALUES
    P_BACKFILLDATA();

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_STAGEMONTHENDDEPR_BL;

   --**************************************************************************
   --                            P_HANDLERESULTS_BL
   --**************************************************************************
   --
   --  Handles the results of depr blending
   --
   procedure P_HANDLERESULTS_BL is
		MY_STR varchar2(2000);
   begin
    PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_HANDLERESULTS_BL');

	merge into DEPR_LEDGER_BLENDING DLB
	using
	(
		select dlbs.*
		from DEPR_LEDGER_BLENDING_STG DLBS, DEPR_CALC_STG DCS
		where DCS.DEPR_GROUP_ID = DLBS.DEPR_GROUP_ID
		and DCS.SET_OF_BOOKS_ID = DLBS.source_sob_id
		and DCS.GL_POST_MO_YR = DLBS.GL_POST_MO_YR
		and DCS.DEPR_CALC_STATUS = 9
	) b
	on (dlb.depr_group_id = b.depr_group_id
		and dlb.set_of_books_id = b.source_sob_id
		and dlb.gl_post_mo_yr = b.gl_post_mo_yr)
	when matched then
	update set
		dlb.additions_bl = b.additions_bl,
		dlb.retirements_bl = b.retirements_bl,
		dlb.transfers_in_bl = b.transfers_in_bl,
		dlb.transfers_out_bl = b.transfers_out_bl,
		dlb.adjustments_bl = b.adjustments_bl,
		dlb.impairment_asset_amount_bl = b.impairment_asset_amount_bl,
		dlb.reserve_bal_impairment_bl = b.reserve_bal_impairment_bl,
		dlb.end_balance_bl = b.end_balance_bl,
		dlb.end_reserve_bl = b.end_reserve_bl,
		dlb.cor_end_reserve_bl = b.cor_end_reserve_bl,
		dlb.reserve_bal_cor_bl = b.reserve_bal_cor_bl,
		dlb.salvage_balance_bl = b.salvage_balance_bl,
		dlb.reserve_bal_adjust_bl = b.reserve_bal_adjust_bl,
		dlb.reserve_bal_retirements_bl = b.reserve_bal_retirements_bl,
		dlb.reserve_bal_tran_in_bl = b.reserve_bal_tran_in_bl,
		dlb.reserve_bal_tran_out_bl = b.reserve_bal_tran_out_bl,
		dlb.reserve_bal_other_credits_bl = b.reserve_bal_other_credits_bl,
		dlb.reserve_bal_gain_loss_bl = b.reserve_bal_gain_loss_bl,
		dlb.cost_of_removal_bl = b.cost_of_removal_bl,
		dlb.salvage_returns_bl = b.salvage_returns_bl,
		dlb.salvage_cash_bl = b.salvage_cash_bl,
		dlb.reserve_credits_bl = b.reserve_credits_bl,
		dlb.reserve_adjustments_bl = b.reserve_adjustments_bl,
		dlb.reserve_tran_in_bl = b.reserve_tran_in_bl,
		dlb.reserve_tran_out_bl = b.reserve_tran_out_bl,
		dlb.gain_loss_bl = b.gain_loss_bl,
		dlb.rwip_allocation_bl = b.rwip_allocation_bl,
		dlb.cor_res_tran_in_bl = b.cor_res_tran_in_bl,
		dlb.cor_res_tran_out_bl = b.cor_res_tran_out_bl,
		dlb.cor_res_adjust_bl = b.cor_res_adjust_bl,
		dlb.rwip_cost_of_removal_bl = b.rwip_cost_of_removal_bl,
		dlb.rwip_salvage_cash_bl = b.rwip_salvage_cash_bl,
		dlb.rwip_salvage_returns_bl = b.rwip_salvage_returns_bl,
		dlb.rwip_reserve_credits_bl = b.rwip_reserve_credits_bl
	when not matched then
	insert (gl_post_mo_yr,
		depr_group_id, set_of_books_id,
		reserve_bal_impairment_bl, impairment_expense_amount_bl,
		impairment_asset_amount_bl, cor_blending_transfer,
		reserve_blending_transfer, cor_blending_adjustment,
		reserve_blending_adjustment, blending_adjustment,
		reserve_bal_salvage_exp_bl, salvage_exp_alloc_adjust_bl,
		salvage_exp_adjust_bl, salvage_expense_bl,
		salvage_base_bl, salvage_rate_bl,
		rwip_reserve_credits_bl, rwip_salvage_returns_bl,
		rwip_salvage_cash_bl, rwip_cost_of_removal_bl,
		cost_of_removal_base_bl, cost_of_removal_rate_bl,
		cor_end_reserve_bl, cor_res_adjust_bl,
		cor_res_tran_out_bl, cor_res_tran_in_bl,
		cor_exp_alloc_adjust_bl, cor_exp_adjust_bl,
		cor_expense_bl, cor_beg_reserve_bl,
		rwip_allocation_bl, est_ann_net_adds_bl,
		impairment_reserve_end_bl, impairment_reserve_act_bl,
		impairment_reserve_beg_bl, current_net_salvage_reserve_bl,
		current_net_salvage_amort_bl, vintage_net_salvage_reserve_bl,
		vintage_net_salvage_amort_bl, gain_loss_bl,
		reserve_tran_out_bl, reserve_tran_in_bl,
		reserve_adjustments_bl, reserve_credits_bl,
		salvage_cash_bl, salvage_returns_bl,
		reserve_retirements_bl, cost_of_removal_bl,
		depr_exp_alloc_adjust_bl, depr_exp_adjust_bl,
		depreciation_expense_bl, depreciation_rate_bl,
		end_balance_bl, depreciation_base_bl,
		adjustments_bl, transfers_out_bl,
		transfers_in_bl, retirements_bl,
		additions_bl, begin_balance_bl,
		reserve_alloc_factor_bl, reserve_bal_gain_loss_bl,
		reserve_bal_other_credits_bl, reserve_bal_tran_out_bl,
		reserve_bal_tran_in_bl, reserve_bal_retirements_bl,
		reserve_bal_adjust_bl, salvage_balance_bl,
		reserve_bal_cor_bl, reserve_bal_provision_bl,
		end_reserve_bl, begin_reserve_bl,
		source_percent, depr_ledger_status
	)
	values (
		b.gl_post_mo_yr,
		b.depr_group_id, b.source_sob_id,
		b.reserve_bal_impairment_bl, b.impairment_expense_amount_bl,
		b.impairment_asset_amount_bl, b.cor_blending_transfer,
		b.reserve_blending_transfer, b.cor_blending_adjustment,
		b.reserve_blending_adjustment, b.blending_adjustment,
		b.reserve_bal_salvage_exp_bl, b.salvage_exp_alloc_adjust_bl,
		b.salvage_exp_adjust_bl, b.salvage_expense_bl,
		b.salvage_base_bl, b.salvage_rate_bl,
		b.rwip_reserve_credits_bl, b.rwip_salvage_returns_bl,
		b.rwip_salvage_cash_bl, b.rwip_cost_of_removal_bl,
		b.cost_of_removal_base_bl, b.cost_of_removal_rate_bl,
		b.cor_end_reserve_bl, b.cor_res_adjust_bl,
		b.cor_res_tran_out_bl, b.cor_res_tran_in_bl,
		b.cor_exp_alloc_adjust_bl, b.cor_exp_adjust_bl,
		b.cor_expense_bl, b.cor_beg_reserve_bl,
		b.rwip_allocation_bl, b.est_ann_net_adds_bl,
		b.impairment_reserve_end_bl, b.impairment_reserve_act_bl,
		b.impairment_reserve_beg_bl, b.current_net_salvage_reserve_bl,
		b.current_net_salvage_amort_bl, b.vintage_net_salvage_reserve_bl,
		b.vintage_net_salvage_amort_bl, b.gain_loss_bl,
		b.reserve_tran_out_bl, b.reserve_tran_in_bl,
		b.reserve_adjustments_bl, b.reserve_credits_bl,
		b.salvage_cash_bl, b.salvage_returns_bl,
		b.reserve_retirements_bl, b.cost_of_removal_bl,
		b.depr_exp_alloc_adjust_bl, b.depr_exp_adjust_bl,
		b.depreciation_expense_bl, b.depreciation_rate_bl,
		b.end_balance_bl, b.depreciation_base_bl,
		b.adjustments_bl, b.transfers_out_bl,
		b.transfers_in_bl, b.retirements_bl,
		b.additions_bl, b.begin_balance_bl,
		b.reserve_alloc_factor_bl, b.reserve_bal_gain_loss_bl,
		b.reserve_bal_other_credits_bl, b.reserve_bal_tran_out_bl,
		b.reserve_bal_tran_in_bl, b.reserve_bal_retirements_bl,
		b.reserve_bal_adjust_bl, b.salvage_balance_bl,
		b.reserve_bal_cor_bl, b.reserve_bal_provision_bl,
		b.end_reserve_bl, b.begin_reserve_bl,
		b.source_percent, b.depr_ledger_status
	);

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_HANDLERESULTS_BL;



   --**************************************************************************
   --                            P_STAGEMONTHENDDEPR
   --**************************************************************************
   --
   --  Loads the global temp table for calculating depreciation.
   -- THE Load is based on depr ledger for a passed in array of company ids.
   -- AND a single month
   --
   procedure P_STAGEMONTHENDDEPR(A_MONTH date) is

   begin
    PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_STAGEMONTHENDDEPR '||to_char(A_MONTH));
      PKG_PP_LOG.P_WRITE_MESSAGE('LOADING the calculation staging table for month:' ||
                                 TO_CHAR(A_MONTH, 'yyyymm'));

      forall I in indices of G_COMPANY_STATUS_CHECK
         insert into DEPR_CALC_STG
			(DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
			DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, RWIP_IN_OVER_DEPR, SMOOTH_CURVE,
			EST_IN_ZERO_CHECK,
			DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD, COMPANY_ID, SUBLEDGER_TYPE_ID,
			DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV, MID_PERIOD_METHOD,
			RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE,
			END_OF_LIFE, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE,
			COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT,
			SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE, DEPR_LEDGER_STATUS,
			BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE,
			RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
			RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR, BEGIN_BALANCE,
			ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE,
			END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
			DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS,
			SALVAGE_CASH, RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT,
			GAIN_LOSS, VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE,
			CURRENT_NET_SALVAGE_AMORT, CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG,
			IMPAIRMENT_RESERVE_ACT, IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION,
			COR_BEG_RESERVE, COR_EXPENSE, COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN,
			COR_RES_TRAN_OUT, COR_RES_ADJUST, COR_END_RESERVE, COST_OF_REMOVAL_RATE,
			COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL, RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS,
			RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST,
			SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT,
			RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER,
			IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT,
			ORIG_DEPR_EXPENSE, ORIG_DEPR_EXP_ALLOC_ADJUST, ORIG_SALV_EXPENSE,
			ORIG_SALV_EXP_ALLOC_ADJUST, ORIG_COR_EXP_ALLOC_ADJUST, ORIG_COR_EXPENSE,
			ORIG_BEGIN_RESERVE, ORIG_COR_BEG_RESERVE, IMPAIRMENT_ASSET_ACTIVITY_SALV, IMPAIRMENT_ASSET_BEGIN_BALANCE,
			CURVE_TRUEUP_ADJ, CURVE_TRUEUP_ADJ_SALV, CURVE_TRUEUP_ADJ_COR, SPREAD_FACTOR_ID, UOP_EXP_ADJ,
			COMBINED_DEPR_GROUP_ID, MORTALITY_CURVE_ID)
            select DL.DEPR_GROUP_ID, --  depr_group_id                   NULL,
                   DL.SET_OF_BOOKS_ID, --  set_of_books_id              NUMBER(22,0)   NULL,
                   DL.GL_POST_MO_YR, --  gl_post_mo_yr                DATE           NULL,
                   A_MONTH,   --  calc_month                   DATE           NULL,
                   1, --  depr_calc_status             NUMBER(2,0)    NULL,
                   '', --  depr_calc_message            VARCHAR2(2000) NULL,
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                                               DG.COMPANY_ID),
                             'no')), --  trf_in_est_adds              VARCHAR2(35)   NULL,
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
                                                                               DG.COMPANY_ID),
                             'no')), --  include_rwip_in_net          VARCHAR2(35)   NULL,
                   decode(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Over Depr Check',
                                                                               DG.COMPANY_ID),
                             'no')), 'yes', 1, 0), -- RWIP_IN_OVER_DEPR
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('DEPR - Smooth Curve True-up',
                                                                               DG.COMPANY_ID),
                             '0')), -- smooth_curve
                   LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Estimate in Zero Check',
                                                                               DG.COMPANY_ID),
                             'no')), -- EST_IN_ZERO_CHECK
                   0, --  dnsa_cor_bal                 NUMBER(22,2)   NULL,
                   0, --  dnsa_salv_bal                NUMBER(22,2)   NULL,
                   0, --  cor_ytd                      NUMBER(22,2)   NULL,
                   0, --  salv_ytd                     NUMBER(22,2)   NULL,
                   DG.COMPANY_ID, --  company_id                   NUMBER(22,0)   NULL,
                   DG.SUBLEDGER_TYPE_ID, --  subledger_type_id            NUMBER(22,0)   NULL,
                   DG.DESCRIPTION, --  description                  VARCHAR2(254)  NULL,
                   DG.DEPR_METHOD_ID, --  depr_method_id               NUMBER(22,0)   NULL,
                   DG.MID_PERIOD_CONV, --  mid_period_conv              NUMBER(22,2)   NULL,
                   lower(trim(DG.MID_PERIOD_METHOD)), --  mid_period_method            VARCHAR2(35)   NULL,
                   DMR.RATE, --  rate                         NUMBER(22,8)   NULL,
                   CASE
					  WHEN Lower(Trim(DG.MID_PERIOD_METHOD)) = 'end of life' THEN 1
					  WHEN Lower(Trim(DG.MID_PERIOD_METHOD)) = 'curve' or Lower(Trim(DG.MID_PERIOD_METHOD)) = 'curve_no_true_up' THEN 2
					  ELSE DMR.NET_GROSS
				   END, --  net_gross                    NUMBER(22,0)   NULL,
                   NVL(DMR.OVER_DEPR_CHECK, 0), --  over_depr_check              NUMBER(22,0)   NULL,
                   nvl(DMR.NET_SALVAGE_PCT, 0), --  net_salvage_pct              NUMBER(22,8)   NULL,
                   NVL(DMR.RATE_USED_CODE, 0), --  rate_used_code               NUMBER(22,0)   NULL,
                   DMR.END_OF_LIFE, --  end_of_life                  NUMBER(22,0)   NULL,
                   NVL(DMR.EXPECTED_AVERAGE_LIFE, 0), --  expected_average_life        NUMBER(22,0)   NULL,
                   NVL(DMR.RESERVE_RATIO_ID, 0), --  reserve_ratio_id             NUMBER(22,0)   NULL,
                   nvl(DMR.COST_OF_REMOVAL_RATE, 0), --  dmr_cost_of_removal_rate     NUMBER(22,8)   NULL,
                   nvl(DMR.COST_OF_REMOVAL_PCT, 0), --  cost_of_removal_pct          NUMBER(22,8)   NULL,
                   nvl(DMR.SALVAGE_RATE, 0), --  dmr_salvage_rate             NUMBER(22,8)   NULL,
                   DMR.INTEREST_RATE, --  interest_rate                NUMBER(22,8)   NULL,
                   NVL(DMR.AMORTIZABLE_LIFE, 0), --  amortizable_life             NUMBER(22,0)   NULL,
                   DECODE(LOWER(NVL(TRIM(DMR.COR_TREATMENT), 'no')), 'no', 0, 'month', 2, 1), --  cor_treatment                NUMBER(1,0)    NULL,
                   DECODE(LOWER(NVL(TRIM(DMR.SALVAGE_TREATMENT), 'no')), 'no', 0, 'month', 2, 1), --  salvage_treatment            NUMBER(1,0)    NULL,
                   NVL(DMR.NET_SALVAGE_AMORT_LIFE, 1), --  net_salvage_amort_life       NUMBER(22,0)   NULL,
                   nvl(DMR.ALLOCATION_PROCEDURE, 'BROAD'), --  allocation_procedure         VARCHAR2(5)    NULL,
                   DL.DEPR_LEDGER_STATUS, --  depr_ledger_status           NUMBER(22,0)   NULL,
                   DL.BEGIN_RESERVE, --  begin_reserve                NUMBER(22,2)   NULL,
                   0 AS END_RESERVE, --  end_reserve                  NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_PROVISION, --  reserve_bal_provision        NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_COR, --  reserve_bal_cor              NUMBER(22,2)   NULL,
                   0 as SALVAGE_BALANCE, --  salvage_balance              NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_ADJUST, --  reserve_bal_adjust           NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_RETIREMENTS, --  reserve_bal_retirements      NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_TRAN_IN, --  reserve_bal_tran_in          NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_TRAN_OUT, --  reserve_bal_tran_out         NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_OTHER_CREDITS, --  reserve_bal_other_credits    NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_GAIN_LOSS, --  reserve_bal_gain_loss        NUMBER(22,2)   NULL,
                   DL.RESERVE_ALLOC_FACTOR, --  reserve_alloc_factor         NUMBER(22,2)   NULL,
                   DL.BEGIN_BALANCE, --  begin_balance                NUMBER(22,2)   NULL,
                   DL.ADDITIONS, --  additions
                   DL.RETIREMENTS, --  retirements                  NUMBER(22,2)   NULL,
                   DL.TRANSFERS_IN, --  transfers_in                 NUMBER(22,2)   NULL,
                   DL.TRANSFERS_OUT, --  transfers_out                NUMBER(22,2)   NULL,
                   DL.ADJUSTMENTS, --  adjustments                  NUMBER(22,2)   NULL,
                   0 AS DEPRECIATION_BASE, --  depreciation_base            NUMBER(22,2)
                   0 AS END_BALANCE, --  end_balance                  NUMBER(22,2)   NULL
                   0 AS DEPRECIATION_RATE, --  depreciation_rate            NUMBER(22,8)   NULL,
                   0 AS DEPRECIATION_EXPENSE, --  depreciation_expense         NUMBER(22,2)   NULL,
                   DL.DEPR_EXP_ADJUST, --  depr_exp_adjust              NUMBER(22,2)   NULL,
                   0 AS DEPR_EXP_ALLOC_ADJUST, --  depr_exp_alloc_adjust        NUMBER(22,2)   NULL,
                   DL.COST_OF_REMOVAL, --  cost_of_removal              NUMBER(22,2)   NULL,
                   DL.RESERVE_RETIREMENTS, --  reserve_retirements          NUMBER(22,2)   NULL,
                   DL.SALVAGE_RETURNS, --  salvage_returns              NUMBER(22,2)   NULL,
                   DL.SALVAGE_CASH, --  salvage_cash                 NUMBER(22,2)   NULL,
                   DL.RESERVE_CREDITS, --  reserve_credits              NUMBER(22,2)   NULL,
                   DL.RESERVE_ADJUSTMENTS, --  reserve_adjustments          NUMBER(22,2)   NULL,
                   DL.RESERVE_TRAN_IN, --  reserve_tran_in              NUMBER(22,2)   NULL,
                   DL.RESERVE_TRAN_OUT, --  reserve_tran_out             NUMBER(22,2)   NULL,
                   DL.GAIN_LOSS, --  gain_loss                    NUMBER(22,2)   NULL,
                   DL.VINTAGE_NET_SALVAGE_AMORT, --  vintage_net_salvage_amort    NUMBER(22,2)   NULL,
                   DL.VINTAGE_NET_SALVAGE_RESERVE, --  vintage_net_salvage_reserve  NUMBER(22,2)   NULL,
                   DL.CURRENT_NET_SALVAGE_AMORT, --  current_net_salvage_amort    NUMBER(22,2)   NULL,
                   DL.CURRENT_NET_SALVAGE_RESERVE, --  current_net_salvage_reserve  NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_RESERVE_BEG, --  impairment_reserve_beg       NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_RESERVE_ACT, --  impairment_reserve_act       NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_RESERVE_END, --  impairment_reserve_end       NUMBER(22,2)   NULL,
                   nvl(DG.EST_ANN_NET_ADDS, 0), --  est_ann_net_adds             NUMBER(22,2)   NULL,
                   NVL(DL.RWIP_ALLOCATION, 0), --  rwip_allocation              NUMBER(22,2)   NULL,
                   DL.COR_BEG_RESERVE, --  cor_beg_reserve              NUMBER(22,2)   NULL,
                   0 AS COR_EXPENSE, --  cor_expense                  NUMBER(22,2)   NULL,
                   DL.COR_EXP_ADJUST, --  cor_exp_adjust               NUMBER(22,2)   NULL,
                   0 AS COR_EXP_ALLOC_ADJUST, --  cor_exp_alloc_adjust         NUMBER(22,2)   NULL,
                   DL.COR_RES_TRAN_IN, --  cor_res_tran_in              NUMBER(22,2)   NULL,
                   DL.COR_RES_TRAN_OUT, --  cor_res_tran_out             NUMBER(22,2)   NULL,
                   DL.COR_RES_ADJUST, --  cor_res_adjust               NUMBER(22,2)   NULL,
                   0 AS COR_END_RESERVE, --  cor_end_reserve              NUMBER(22,2)   NULL,
                   DL.COST_OF_REMOVAL_RATE, --  cost_of_removal_rate         NUMBER(22,8)   NULL,
                   DL.COST_OF_REMOVAL_BASE, --  cost_of_removal_base         NUMBER(22,2)   NULL,
                   DL.RWIP_COST_OF_REMOVAL, --  rwip_cost_of_removal         NUMBER(22,2)   NULL,
                   DL.RWIP_SALVAGE_CASH, --  rwip_salvage_cash            NUMBER(22,2)   NULL,
                   DL.RWIP_SALVAGE_RETURNS, --  rwip_salvage_returns         NUMBER(22,2)   NULL,
                   DL.RWIP_RESERVE_CREDITS, --  rwip_reserve_credits         NUMBER(22,2)   NULL,
                   DL.SALVAGE_RATE, --  salvage_rate                 NUMBER(22,8)   NULL,
                   0 AS SALVAGE_BASE, --  salvage_base                 NUMBER(22,2)   NULL,
                   0 AS SALVAGE_EXPENSE, --  salvage_expense              NUMBER(22,2)   NULL,
                   DL.SALVAGE_EXP_ADJUST, --  salvage_exp_adjust           NUMBER(22,2)   NULL,
                   0 AS SALVAGE_EXP_ALLOC_ADJUST, --  salvage_exp_alloc_adjust     NUMBER(22,2)   NULL,
                   0 as RESERVE_BAL_SALVAGE_EXP, --  reserve_bal_salvage_exp      NUMBER(22,2)   NULL,
                   0 as RESERVE_BLENDING_ADJUSTMENT, --  reserve_blending_adjustment  NUMBER(22,2)   NULL,
                   0 as RESERVE_BLENDING_TRANSFER, --  reserve_blending_transfer    NUMBER(22,2)   NULL,
                   0 as COR_BLENDING_ADJUSTMENT, --  cor_blending_adjustment      NUMBER(22,2)   NULL,
                   0 as COR_BLENDING_TRANSFER, --  cor_blending_transfer        NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_ASSET_AMOUNT, --  impairment_asset_amount      NUMBER(22,2)   NULL,
                   DL.IMPAIRMENT_EXPENSE_AMOUNT, --  impairment_expense_amount    NUMBER(22,2)   NULL,
                   DL.RESERVE_BAL_IMPAIRMENT, --  reserve_bal_impairment       NUMBER(22,2)   NUll,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.DEPRECIATION_EXPENSE,0)), --ORIG_DEPR_EXPENSE
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.DEPR_EXP_ALLOC_ADJUST,0)), --  orig_depr_exp_alloc_adjust   NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.SALVAGE_EXPENSE,0)), --  orig_salv_expense            NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.SALVAGE_EXP_ALLOC_ADJUST,0)), --  orig_salv_exp_alloc_adjust   NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.COR_EXP_ALLOC_ADJUST,0)), --  orig_cor_exp_alloc_adjust    NUMBER(22,2)   NULL,
                   DECODE(DL.GL_POST_MO_YR, A_MONTH, 0, NVL(DL.COR_EXPENSE,0)), --  orig_cor_expense             NUMBER(22,2)   NULL
                   DL.BEGIN_RESERVE, --orig_begin_reserve          NUMBER(22,2) DEFAULT 0  NULL,
                   DL.COR_BEG_RESERVE, --  orig_cor_begin_reserve     NUMBER(22,2) DEFAULT 0 NULL
				  DL.IMPAIRMENT_ASSET_ACTIVITY_SALV,
				  DL.IMPAIRMENT_ASSET_BEGIN_BALANCE,
				  0 as CURVE_TRUEUP_ADJ,
				  0 as CURVE_TRUEUP_ADJ_SALV,
				  0 as CURVE_TRUEUP_ADJ_COR,
				  DG.FACTOR_ID,
				  0 as UOP_EXP_ADJ,
				  DG.COMBINED_DEPR_GROUP_ID,
				  DMR.MORTALITY_CURVE_ID
              from DEPR_GROUP DG, DEPR_LEDGER DL,
        (
        select depr_method_id, set_of_books_id, EFFECTIVE_DATE, RATE_USED_CODE, RATE,
          allocation_procedure, end_of_life, NET_GROSS, net_salvage_amort_life, salvage_treatment, cor_treatment,
          amortizable_life, interest_rate, salvage_rate, cost_of_removal_pct, cost_of_removal_rate, reserve_ratio_id,
          expected_average_life, net_salvage_pct, over_depr_check, mortality_curve_id,
          row_number() over(partition by depr_method_id, set_of_books_id order by EFFECTIVE_DATE desc) as the_row
                      from DEPR_METHOD_RATES DMR1
        where DMR1.EFFECTIVE_DATE <= A_MONTH
      ) DMR
             where DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
               and DMR.SET_OF_BOOKS_ID = DL.SET_OF_BOOKS_ID
			   AND DMR.THE_ROW = 1
               and DL.DEPR_GROUP_ID = DG.DEPR_GROUP_ID
               and DL.GL_POST_MO_YR <= A_MONTH
               and DL.GL_POST_MO_YR >= case
					when DMR.RATE_USED_CODE = 2 and lower(trim(DG.MID_PERIOD_METHOD)) not in ('curve','curve_no_trueup','uop') then
						DMR.EFFECTIVE_DATE
					else
						A_MONTH
					end
			   and DG.SUBLEDGER_TYPE_ID = 0
               and DG.COMPANY_ID = G_COMPANY_STATUS_CHECK(I).COMPANY_ID
               and (G_COMPANY_STATUS_CHECK(I).CHECK_INACTIVE = 'no' /*either the system control is no or we look for one of the following values.*/
			    or NVL(DG.STATUS_ID, 1) = 1 or DL.END_BALANCE <> 0 or DL.END_RESERVE <> 0 or
					   DL.COR_END_RESERVE <> 0 or DL.BEGIN_BALANCE <> 0 or DL.BEGIN_RESERVE <> 0 or
					   DL.COR_BEG_RESERVE <> 0);

	G_RTN := analyze_table('DEPR_CALC_STG',100);

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_STAGEMONTHENDDEPR;

   --**************************************************************************
   --                            P_STAGEMONTHENDDEPR
   --**************************************************************************
   --
   --  WRAPPER for the prep function to pass in an array of months.
   --
   procedure P_STAGEMONTHENDDEPR(A_COMPANY_IDS    PKG_PP_COMMON.NUM_TABTYPE,
                A_MONTHS PKG_PP_COMMON.DATE_TABTYPE,
				A_LOAD_RECURRING integer) is

		MY_STR varchar2(2000);

   begin
    PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_STAGEMONTHENDDEPR');

	if A_LOAD_RECURRING = 1 then
		if PKG_PP_DEPR_ACTIVITY.F_RECURRINGACTIVITY(A_COMPANY_IDS, A_MONTHS, 0, MY_STR) = -1 then
			PKG_PP_LOG.P_WRITE_MESSAGE(MY_STR);
			return;
		end if;
	end if;

	PKG_PP_DEPR_ACTIVITY.P_DEPR_BLENDING_TRF(A_COMPANY_IDS, A_MONTHS(A_MONTHS.LAST));

    for I in 1 .. A_COMPANY_IDS.COUNT loop
		-- load the arrays for companies ignoring inactive depr groups
		P_CHECKINACTIVEDEPR(A_COMPANY_IDS(I), I);
    end loop;


	delete from depr_calc_stg;
	delete from DEPR_CALC_AMORT_STG;

	for I in A_MONTHS.FIRST .. A_MONTHS.LAST loop
		P_STAGEMONTHENDDEPR(A_MONTHS(I));
		P_AMORT_STAGE(A_MONTHS(I));
	end loop;

	P_STAGE_BLENDING_METHOD();

    P_SET_FISCALYEARSTART();

    -- BACKFILL VALUES
    P_BACKFILLDATA();

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_STAGEMONTHENDDEPR;

   --Overloaded function that takes in scalars
   procedure P_STAGEMONTHENDDEPR(A_COMPANY_ID    number,
                A_MONTH date,
				A_LOAD_RECURRING integer) is
  v_company   pkg_pp_common.num_tabtype;
  v_month    pkg_pp_common.date_tabtype;
   begin
    PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_STAGEMONTHENDDEPR');

    v_company(1) := a_company_id;
    v_month(1) := a_month;

    p_stagemonthenddepr(v_company, v_month, a_load_recurring);

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_STAGEMONTHENDDEPR;


   procedure P_STAGE_BLENDING_FCST
   is
		l_msg varchar2(2000);
   begin
		PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_STAGE_BLENDING_FCST');

		delete from depr_ledger_blending_temp;

		insert into depr_ledger_blending_temp
		(
			depr_method_id, target_set_of_books_id,
			effective_date, source_set_of_books_id,
			source_percent, priority
		)
		select fcst_depr_method_id, target_set_of_books_id,
			effective_date, source_set_of_books_id,
			source_percent, priority
		from fcst_depr_method_blending
		where fcst_depr_version_id = g_version_id
		;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
   exception
	when others then
		PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_STAGE_BLENDING_FCST;


   --**************************************************************************
   --                            P_FCSTDEPRSTAGE
   --**************************************************************************
   --
  --  WRAPPER for the prep function to pass in an array of months.
   --
  procedure P_FCSTDEPRSTAGE(A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
                A_MONTHS PKG_PP_COMMON.DATE_TABTYPE)
  is
    type t_company_ids is table of company_setup.company_id%type;
    l_company_ids t_company_ids;

	l_month date;

    V_COUNT INTEGER;
  begin
    PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_FCSTDEPRSTAGE');
    G_VERSION_ID := A_VERSION_ID;
    PKG_PP_LOG.P_WRITE_MESSAGE('STAGING version: ' || to_char(G_VERSION_ID));

    begin
      select distinct COMPANY_ID
      bulk collect into L_COMPANY_IDS
      from FCST_DEPR_GROUP_VERSION
      where FCST_DEPR_VERSION_ID = A_VERSION_ID;
    exception
      when NO_DATA_FOUND then
        PKG_PP_LOG.P_WRITE_MESSAGE('No FCST_DEPR_GROUP_VERSION records found for forecast version ID: ' || A_VERSION_ID);
        return;
    end;

      for I in 1 .. L_COMPANY_IDS.COUNT
        loop
           -- load the arrays for companies ignoring inactive depr groups
           P_CHECKINACTIVEDEPR(L_COMPANY_IDS(I), I);
        end loop;

	l_month := A_MONTHS(A_MONTHS.FIRST);

    for K in G_COMPANY_STATUS_CHECK.FIRST .. G_COMPANY_STATUS_CHECK.LAST loop
      /*I'm putting the forall on a_months because there will usually be more
      months than companies, and the forall should be more efficient. */
      forall i in indices of A_MONTHS
        insert into DEPR_CALC_STG
        (
          DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, CALC_MONTH, DEPR_CALC_STATUS,
          DEPR_CALC_MESSAGE, TRF_IN_EST_ADDS, INCLUDE_RWIP_IN_NET, RWIP_IN_OVER_DEPR, SMOOTH_CURVE,
          DNSA_COR_BAL, DNSA_SALV_BAL, COR_YTD, SALV_YTD, COMPANY_ID, SUBLEDGER_TYPE_ID,
          DESCRIPTION, DEPR_METHOD_ID, MID_PERIOD_CONV, MID_PERIOD_METHOD,
          RATE, NET_GROSS, OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE,
          END_OF_LIFE, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, DMR_COST_OF_REMOVAL_RATE,
          COST_OF_REMOVAL_PCT, DMR_SALVAGE_RATE, INTEREST_RATE, AMORTIZABLE_LIFE, COR_TREATMENT,
          SALVAGE_TREATMENT, NET_SALVAGE_AMORT_LIFE, ALLOCATION_PROCEDURE, DEPR_LEDGER_STATUS,
          BEGIN_RESERVE, END_RESERVE, RESERVE_BAL_PROVISION, RESERVE_BAL_COR, SALVAGE_BALANCE,
          RESERVE_BAL_ADJUST, RESERVE_BAL_RETIREMENTS, RESERVE_BAL_TRAN_IN, RESERVE_BAL_TRAN_OUT,
          RESERVE_BAL_OTHER_CREDITS, RESERVE_BAL_GAIN_LOSS, RESERVE_ALLOC_FACTOR, BEGIN_BALANCE,
          ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, DEPRECIATION_BASE,
          END_BALANCE, DEPRECIATION_RATE, DEPRECIATION_EXPENSE, DEPR_EXP_ADJUST,
          DEPR_EXP_ALLOC_ADJUST, COST_OF_REMOVAL, RESERVE_RETIREMENTS, SALVAGE_RETURNS,
          SALVAGE_CASH, RESERVE_CREDITS, RESERVE_ADJUSTMENTS, RESERVE_TRAN_IN, RESERVE_TRAN_OUT,
          GAIN_LOSS, VINTAGE_NET_SALVAGE_AMORT, VINTAGE_NET_SALVAGE_RESERVE,
          CURRENT_NET_SALVAGE_AMORT, CURRENT_NET_SALVAGE_RESERVE, IMPAIRMENT_RESERVE_BEG,
          IMPAIRMENT_RESERVE_ACT, IMPAIRMENT_RESERVE_END, EST_ANN_NET_ADDS, RWIP_ALLOCATION,
          COR_BEG_RESERVE, COR_EXPENSE, COR_EXP_ADJUST, COR_EXP_ALLOC_ADJUST, COR_RES_TRAN_IN,
          COR_RES_TRAN_OUT, COR_RES_ADJUST, COR_END_RESERVE, COST_OF_REMOVAL_RATE,
          COST_OF_REMOVAL_BASE, RWIP_COST_OF_REMOVAL, RWIP_SALVAGE_CASH, RWIP_SALVAGE_RETURNS,
          RWIP_RESERVE_CREDITS, SALVAGE_RATE, SALVAGE_BASE, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST,
          SALVAGE_EXP_ALLOC_ADJUST, RESERVE_BAL_SALVAGE_EXP, RESERVE_BLENDING_ADJUSTMENT,
          RESERVE_BLENDING_TRANSFER, COR_BLENDING_ADJUSTMENT, COR_BLENDING_TRANSFER,
          IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, RESERVE_BAL_IMPAIRMENT,
          ORIG_DEPR_EXPENSE, ORIG_DEPR_EXP_ALLOC_ADJUST, ORIG_SALV_EXPENSE,
          ORIG_SALV_EXP_ALLOC_ADJUST, ORIG_COR_EXP_ALLOC_ADJUST, ORIG_COR_EXPENSE,
          ORIG_BEGIN_RESERVE, ORIG_COR_BEG_RESERVE, impairment_asset_activity_salv, impairment_asset_begin_balance,
          FCST_DEPR_VERSION_ID, curve_trueup_adj, CURVE_TRUEUP_ADJ_SALV, CURVE_TRUEUP_ADJ_COR,
          SPREAD_FACTOR_ID, UOP_EXP_ADJ, COMBINED_DEPR_GROUP_ID, MORTALITY_CURVE_ID
        )
        select DL.FCST_DEPR_GROUP_ID, --  depr_group_id                   NULL,
          DL.SET_OF_BOOKS_ID, --  set_of_books_id              NUMBER(22,0)   NULL,
          DL.GL_POST_MO_YR, --  gl_post_mo_yr                DATE           NULL,
          null,   --  calc_month                   DATE           NULL,
          1, --  depr_calc_status             NUMBER(2,0)    NULL,
          '', --  depr_calc_message            VARCHAR2(2000) NULL,
          LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
             DGV.COMPANY_ID),
          'no')), --  trf_in_est_adds              VARCHAR2(35)   NULL,
          LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Net Calculation',
             DGV.COMPANY_ID),
          'no')), --  include_rwip_in_net          VARCHAR2(35)   NULL,
		   decode(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Include RWIP in Over Depr Check',
																	   DGV.COMPANY_ID),
					 'no')), 'yes', 1, 0), -- RWIP_IN_OVER_DEPR
          LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('DEPR - Smooth Curve True-up',
             DGV.COMPANY_ID),
          '0')), -- smooth_curve
          0, --  dnsa_cor_bal                 NUMBER(22,2)   NULL,
          0, --  dnsa_salv_bal                NUMBER(22,2)   NULL,
          0, --  cor_ytd                      NUMBER(22,2)   NULL,
          0, --  salv_ytd                     NUMBER(22,2)   NULL,
          DGV.COMPANY_ID, --  company_id                   NUMBER(22,0)   NULL,
          DGV.SUBLEDGER_TYPE_ID, --  subledger_type_id            NUMBER(22,0)   NULL,
          DGV.DESCRIPTION, --  description                  VARCHAR2(254)  NULL,
          DGV.FCST_DEPR_METHOD_ID, --  depr_method_id               NUMBER(22,0)   NULL,
          DGV.MID_PERIOD_CONV, --  mid_period_conv              NUMBER(22,2)   NULL,
          lower(trim(DGV.MID_PERIOD_METHOD)), --  mid_period_method            VARCHAR2(35)   NULL,
          0, --  rate                         NUMBER(22,8)   NULL,
          0, --  net_gross                    NUMBER(22,0)   NULL,
          0, --  over_depr_check              NUMBER(22,0)   NULL,
          0, --  net_salvage_pct              NUMBER(22,8)   NULL,
          0, --  rate_used_code               NUMBER(22,0)   NULL,
          null, --  end_of_life                  NUMBER(22,0)   NULL,
          0, --  expected_average_life        NUMBER(22,0)   NULL,
          0, --  reserve_ratio_id             NUMBER(22,0)   NULL,
          0, --  dmr_cost_of_removal_rate     NUMBER(22,8)   NULL,
          0, --  cost_of_removal_pct          NUMBER(22,8)   NULL,
          0, --  dmr_salvage_rate             NUMBER(22,8)   NULL,
          null, --  interest_rate                NUMBER(22,8)   NULL,
          0, --  amortizable_life             NUMBER(22,0)   NULL,
          0, --  cor_treatment                NUMBER(1,0)    NULL,
          0, --  salvage_treatment            NUMBER(1,0)    NULL,
          0, --  net_salvage_amort_life       NUMBER(22,0)   NULL,
          'BROAD', --  allocation_procedure         VARCHAR2(5)    NULL,
          DL.DEPR_LEDGER_STATUS, --  depr_ledger_status           NUMBER(22,0)   NULL,
          nvl(DL.BEGIN_RESERVE,0), --  begin_reserve                NUMBER(22,2)   NULL,
          0 AS END_RESERVE, --  end_reserve                  NUMBER(22,2)   NULL,
          DL.RESERVE_BAL_PROVISION, --  reserve_bal_provision        NUMBER(22,2)   NULL,
          DL.RESERVE_BAL_COR, --  reserve_bal_cor              NUMBER(22,2)   NULL,
          DL.SALVAGE_BALANCE, --  salvage_balance              NUMBER(22,2)   NULL,
          DL.RESERVE_BAL_ADJUST, --  reserve_bal_adjust           NUMBER(22,2)   NULL,
          DL.RESERVE_BAL_RETIREMENTS, --  reserve_bal_retirements      NUMBER(22,2)   NULL,
          DL.RESERVE_BAL_TRAN_IN, --  reserve_bal_tran_in          NUMBER(22,2)   NULL,
          DL.RESERVE_BAL_TRAN_OUT, --  reserve_bal_tran_out         NUMBER(22,2)   NULL,
          DL.RESERVE_BAL_OTHER_CREDITS, --  reserve_bal_other_credits    NUMBER(22,2)   NULL,
          DL.RESERVE_BAL_GAIN_LOSS, --  reserve_bal_gain_loss        NUMBER(22,2)   NULL,
          DL.RESERVE_ALLOC_FACTOR, --  reserve_alloc_factor         NUMBER(22,2)   NULL,
          DL.BEGIN_BALANCE, --  begin_balance                NUMBER(22,2)   NULL,
          DL.ADDITIONS, --  additions
          DL.RETIREMENTS, --  retirements                  NUMBER(22,2)   NULL,
          DL.TRANSFERS_IN, --  transfers_in                 NUMBER(22,2)   NULL,
          DL.TRANSFERS_OUT, --  transfers_out                NUMBER(22,2)   NULL,
          DL.ADJUSTMENTS, --  adjustments                  NUMBER(22,2)   NULL,
          0 AS DEPRECIATION_BASE, --  depreciation_base            NUMBER(22,2)
          0 AS END_BALANCE, --  end_balance                  NUMBER(22,2)   NULL
          0, --  depreciation_rate            NUMBER(22,8)   NULL,
          0 AS DEPRECIATION_EXPENSE, --  depreciation_expense         NUMBER(22,2)   NULL,
          DL.DEPR_EXP_ADJUST, --  depr_exp_adjust              NUMBER(22,2)   NULL,
          0 AS DEPR_EXP_ALLOC_ADJUST, --  depr_exp_alloc_adjust        NUMBER(22,2)   NULL,
          DL.COST_OF_REMOVAL, --  cost_of_removal              NUMBER(22,2)   NULL,
          DL.RESERVE_RETIREMENTS, --  reserve_retirements          NUMBER(22,2)   NULL,
          DL.SALVAGE_RETURNS, --  salvage_returns              NUMBER(22,2)   NULL,
          DL.SALVAGE_CASH, --  salvage_cash                 NUMBER(22,2)   NULL,
          DL.RESERVE_CREDITS, --  reserve_credits              NUMBER(22,2)   NULL,
          DL.RESERVE_ADJUSTMENTS, --  reserve_adjustments          NUMBER(22,2)   NULL,
          DL.RESERVE_TRAN_IN, --  reserve_tran_in              NUMBER(22,2)   NULL,
          DL.RESERVE_TRAN_OUT, --  reserve_tran_out             NUMBER(22,2)   NULL,
          DL.GAIN_LOSS, --  gain_loss                    NUMBER(22,2)   NULL,
          DL.VINTAGE_NET_SALVAGE_AMORT, --  vintage_net_salvage_amort    NUMBER(22,2)   NULL,
          DL.VINTAGE_NET_SALVAGE_RESERVE, --  vintage_net_salvage_reserve  NUMBER(22,2)   NULL,
          DL.CURRENT_NET_SALVAGE_AMORT, --  current_net_salvage_amort    NUMBER(22,2)   NULL,
          DL.CURRENT_NET_SALVAGE_RESERVE, --  current_net_salvage_reserve  NUMBER(22,2)   NULL,
          DL.IMPAIRMENT_RESERVE_BEG, --  impairment_reserve_beg       NUMBER(22,2)   NULL,
          DL.IMPAIRMENT_RESERVE_ACT, --  impairment_reserve_act       NUMBER(22,2)   NULL,
          DL.IMPAIRMENT_RESERVE_END, --  impairment_reserve_end       NUMBER(22,2)   NULL,
          nvl(DGV.EST_ANN_NET_ADDS, 0), --  est_ann_net_adds             NUMBER(22,2)   NULL,
          NVL(DL.RWIP_ALLOCATION, 0), --  rwip_allocation              NUMBER(22,2)   NULL,
          DL.COR_BEG_RESERVE, --  cor_beg_reserve              NUMBER(22,2)   NULL,
          0 AS COR_EXPENSE, --  cor_expense                  NUMBER(22,2)   NULL,
          NVL(DL.COR_EXP_ADJUST,0), --  cor_exp_adjust               NUMBER(22,2)   NULL,
          0 AS COR_EXP_ALLOC_ADJUST, --  cor_exp_alloc_adjust         NUMBER(22,2)   NULL,
          DL.COR_RES_TRAN_IN, --  cor_res_tran_in              NUMBER(22,2)   NULL,
          DL.COR_RES_TRAN_OUT, --  cor_res_tran_out             NUMBER(22,2)   NULL,
          DL.COR_RES_ADJUST, --  cor_res_adjust               NUMBER(22,2)   NULL,
          0 AS COR_END_RESERVE, --  cor_end_reserve              NUMBER(22,2)   NULL,
          nvl(DL.COST_OF_REMOVAL_RATE, 0), --  cost_of_removal_rate         NUMBER(22,8)   NULL,
          DL.COST_OF_REMOVAL_BASE, --  cost_of_removal_base         NUMBER(22,2)   NULL,
          nvl(DL.RWIP_COST_OF_REMOVAL,0), --  rwip_cost_of_removal         NUMBER(22,2)   NULL,
          nvl(DL.RWIP_SALVAGE_CASH,0), --  rwip_salvage_cash            NUMBER(22,2)   NULL,
          nvl(DL.RWIP_SALVAGE_RETURNS,0), --  rwip_salvage_returns         NUMBER(22,2)   NULL,
          nvl(DL.RWIP_RESERVE_CREDITS,0), --  rwip_reserve_credits         NUMBER(22,2)   NULL,
          nvl(DL.SALVAGE_RATE,0), --  salvage_rate                 NUMBER(22,8)   NULL,
          nvl(SALVAGE_BASE,0), --  salvage_base                 NUMBER(22,2)   NULL,
          nvl(SALVAGE_EXPENSE,0), --  salvage_expense              NUMBER(22,2)   NULL,
          nvl(DL.SALVAGE_EXP_ADJUST,0), --  salvage_exp_adjust           NUMBER(22,2)   NULL,
         0 as salvage_exp_alloc_adjust, --  salvage_exp_alloc_adjust     NUMBER(22,2)   NULL,
          nvl(DL.RESERVE_BAL_SALVAGE_EXP,0), --  reserve_bal_salvage_exp      NUMBER(22,2)   NULL,
          nvl(DL.RESERVE_BLENDING_ADJUSTMENT,0), --  reserve_blending_adjustment  NUMBER(22,2)   NULL,
          nvl(DL.RESERVE_BLENDING_TRANSFER,0), --  reserve_blending_transfer    NUMBER(22,2)   NULL,
          nvl(DL.COR_BLENDING_ADJUSTMENT,0), --  cor_blending_adjustment      NUMBER(22,2)   NULL,
          nvl(DL.COR_BLENDING_TRANSFER,0), --  cor_blending_transfer        NUMBER(22,2)   NULL,
          nvl(DL.IMPAIRMENT_ASSET_AMOUNT,0), --  impairment_asset_amount      NUMBER(22,2)   NULL,
          nvl(DL.IMPAIRMENT_EXPENSE_AMOUNT,0), --  impairment_expense_amount    NUMBER(22,2)   NULL,
          nvl(DL.RESERVE_BAL_IMPAIRMENT,0), --  reserve_bal_impairment       NUMBER(22,2)   NUll,
          0, --ORIG_DEPR_EXPENSE
          0, --  orig_depr_exp_alloc_adjust   NUMBER(22,2)   NULL,
          0, --  orig_salv_expense            NUMBER(22,2)   NULL,
          0, --  orig_salv_exp_alloc_adjust   NUMBER(22,2)   NULL,
          0, --  orig_cor_exp_alloc_adjust    NUMBER(22,2)   NULL,
          0, --  orig_cor_expense             NUMBER(22,2)   NULL
          nvl(DL.BEGIN_RESERVE,0), --orig_begin_reserve         NUMBER(22,2) DEFAULT 0  NULL,
          nvl(DL.COR_BEG_RESERVE, 0), --  orig_cor_begin_reserve     NUMBER(22,2) DEFAULT 0 NULL
          dl.impairment_asset_activity_salv,
          dl.impairment_asset_begin_balance,
          A_VERSION_ID,
          0 as curve_trueup_adj,
          0 as CURVE_TRUEUP_ADJ_SALV,
          0 as CURVE_TRUEUP_ADJ_COR,
          DGV.FACTOR_ID,
          0 as UOP_EXP_ADJ,
          DGV.FCST_COMBINED_DEPR_GROUP_ID,
          null
        from FCST_DEPR_LEDGER DL, FCST_DEPR_GROUP_VERSION DGV, depr_fcst_group_stg tt, COMPANY_SET_OF_BOOKS CSOB
        where DL.FCST_DEPR_GROUP_ID = DGV.FCST_DEPR_GROUP_ID
        and DL.FCST_DEPR_VERSION_ID = DGV.FCST_DEPR_VERSION_ID
        and DL.GL_POST_MO_YR = A_MONTHS(i)
        and DGV.FCST_DEPR_VERSION_ID = G_VERSION_ID
        and DGV.COMPANY_ID = G_COMPANY_STATUS_CHECK(K).COMPANY_ID
          and DGV.SUBLEDGER_TYPE_ID = 0
		and tt.fcst_depr_group_id = dgv.fcst_depr_group_id
        and (G_COMPANY_STATUS_CHECK(K).CHECK_INACTIVE = 'no' /*either the system control is no or we look for one of the following values.*/
          or NVL(DGV.STATUS_ID, 1) = 1 or DL.END_BALANCE <> 0 or DL.END_RESERVE <> 0
          or DL.COR_END_RESERVE <> 0 or DL.BEGIN_BALANCE <> 0 or DL.BEGIN_RESERVE <> 0
          or DL.COR_BEG_RESERVE <> 0
        )
        --Don't do calculations on rows pulled from actuals
        and DL.DEPR_LEDGER_STATUS <> 1
        and DGV.COMPANY_ID = CSOB.COMPANY_ID
        and DL.SET_OF_BOOKS_ID = CSOB.SET_OF_BOOKS_ID;
    end loop; /*loop over elements of G_COMPANY_STATUS_CHECK*/

	G_RTN := analyze_table('DEPR_CALC_STG',100);

	PKG_PP_ERROR.SET_MODULE_NAME('PP_DEPR_PKG.P_FCSTDEPRSTAGE2');
	update DEPR_CALC_STG
	set calc_month = l_month;

	PKG_PP_ERROR.REMOVE_MODULE_NAME;

	-- Call a function to stage the depr method blending percents for the version
	P_STAGE_BLENDING_FCST;

    select Count(1)
    into V_COUNT
    from DEPR_CALC_STG;

    PKG_PP_LOG.P_WRITE_MESSAGE('  ' || V_COUNT || ' records staged in DEPR_CALC_STG');

    P_SET_FISCALYEARSTART;

    -- BACKFILL VALUES
    P_BACKFILLDATA_FCST(A_MONTHS(A_MONTHS.first), A_MONTHS(A_MONTHS.last));

    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
  end P_FCSTDEPRSTAGE;


    function F_GETRESERVERATIO(  A_DEPR_GROUP_ID    DEPR_GROUP.DEPR_GROUP_ID%TYPE,
                A_SOB_ID      DEPR_LEDGER.SET_OF_BOOKS_ID%TYPE,
                A_MONTH        DEPR_LEDGER.GL_POST_MO_YR%TYPE,
                A_RESERVE_TYPE varchar2) return number is
    L_COUNT number;
    L_BALANCE number;
    L_RESERVE number;
  begin
    --This function returns the ENDING reserve ratio. It first checks DEPR_CALC_STG then looks in DEPR_LEDGER

    --COR: ENDING balance = cost_removal_pct * ENDING_BALANCE
    if trim(upper(A_RESERVE_TYPE)) = 'LIFE' then
    --See if the month is in DEPR_CALC_STG
      select count(*)
      into L_COUNT
      from DEPR_CALC_STG
      where DEPR_GROUP_ID = A_DEPR_GROUP_ID
      and SET_OF_BOOKS_ID = A_SOB_ID
      and GL_POST_MO_YR = A_MONTH;

      if L_COUNT = 1 then
        select END_BALANCE, END_RESERVE
        into L_BALANCE, L_RESERVE
        from DEPR_CALC_STG
        where DEPR_GROUP_ID = A_DEPR_GROUP_ID
        and SET_OF_BOOKS_ID = A_SOB_ID
        and GL_POST_MO_YR = A_MONTH;

        if L_BALANCE = 0 then return 1; end if;--Protect against divide by zero

        return L_RESERVE/L_BALANCE;
      end if;

      --See if the month is in DEPR_LEDGER
      select count(*)
      into L_COUNT
      from DEPR_LEDGER
      where DEPR_GROUP_ID = A_DEPR_GROUP_ID
      and SET_OF_BOOKS_ID = A_SOB_ID
      and GL_POST_MO_YR = A_MONTH;

      if L_COUNT = 1 then
        select END_BALANCE, END_RESERVE
        into L_BALANCE, L_RESERVE
        from DEPR_LEDGER
        where DEPR_GROUP_ID = A_DEPR_GROUP_ID
        and SET_OF_BOOKS_ID = A_SOB_ID
        and GL_POST_MO_YR = A_MONTH;

        if L_BALANCE = 0 then return 1; end if;--Protect against divide by zero

        return L_RESERVE/L_BALANCE;
      end if;

    --If it's in neither, then return 1 (since 0/0 = 1)
      return 1;

    elsif trim(upper(A_RESERVE_TYPE)) = 'COR' then
      select count(*)
      into L_COUNT
      from DEPR_CALC_STG
      where DEPR_GROUP_ID = A_DEPR_GROUP_ID
      and SET_OF_BOOKS_ID = A_SOB_ID
      and GL_POST_MO_YR = A_MONTH;

      if L_COUNT = 1 then
        select COST_OF_REMOVAL_RATE * END_BALANCE, COR_END_RESERVE
        into L_BALANCE, L_RESERVE
        from DEPR_CALC_STG
        where DEPR_GROUP_ID = A_DEPR_GROUP_ID
        and SET_OF_BOOKS_ID = A_SOB_ID
        and GL_POST_MO_YR = A_MONTH;

        if L_BALANCE = 0 then return 1; end if;--Protect against divide by zero

        return L_RESERVE/L_BALANCE;
      end if;

      --See if the month is in DEPR_LEDGER
      select count(*)
      into L_COUNT
      from DEPR_LEDGER
      where DEPR_GROUP_ID = A_DEPR_GROUP_ID
      and SET_OF_BOOKS_ID = A_SOB_ID
      and GL_POST_MO_YR = A_MONTH;

      if L_COUNT = 1 then
        select COST_OF_REMOVAL_RATE * END_BALANCE, COR_END_RESERVE
        into L_BALANCE, L_RESERVE
        from DEPR_LEDGER
        where DEPR_GROUP_ID = A_DEPR_GROUP_ID
        and SET_OF_BOOKS_ID = A_SOB_ID
        and GL_POST_MO_YR = A_MONTH;

        if L_BALANCE = 0 then return 1; end if;--Protect against divide by zero

        return L_RESERVE/L_BALANCE;
      end if;

    --If it's in neither, then return 1 (since 0/0 = 1 or maybe it's infinity. Who has time for math?)
      return 1;
    else
      --Problematic if it gets here
      return 0;
    end if;

    end;

   --**************************************************************************
   --                            F_FIND_DEPR_GROUP
   --**************************************************************************
   function F_FIND_DEPR_GROUP(A_COMPANY_ID         number,
                              A_GL_ACCOUNT_ID      number,
                              A_MAJOR_LOCATION_ID  number,
                              A_UTILITY_ACCOUNT_ID number,
                              A_BUS_SEGMENT_ID     number,
                              A_SUB_ACCOUNT_ID     number,
                              A_SUBLEDGER_TYPE_ID  number,
                              A_BOOK_VINTAGE       number,
                              A_ASSET_LOCATION_ID  number,
                              A_LOCATION_TYPE_ID   number,
                              A_RETIREMENT_UNIT_ID number,
                              A_PROPERTY_UNIT_ID   number,
                              A_CLASS_CODE_ID      number,
                              A_CC_VALUE           varchar2) return number is
      DG_ID  number;
      STATUS number;
      CC_VAL varchar2(254);
	  CC_ID	 number;
      VNTG   number;

   begin

      if A_CC_VALUE is null then
         CC_VAL := 'NO CLASS CODE';
      else
         CC_VAL := A_CC_VALUE;
      end if;
	  if A_CLASS_CODE_ID is null then
         CC_ID := 0;
      else
         CC_ID := A_CLASS_CODE_ID;
      end if;
      if A_BOOK_VINTAGE is null then
         VNTG := 0;
      else
         VNTG := A_BOOK_VINTAGE;
      end if;

      select DEPR_GROUP_ID, STATUS_ID
        into DG_ID, STATUS
        from (select DEPR_GROUP_CONTROL.DEPR_GROUP_ID DEPR_GROUP_ID, NVL(STATUS_ID, 1) STATUS_ID
                from DEPR_GROUP_CONTROL, DEPR_GROUP
               where DEPR_GROUP.COMPANY_ID = A_COMPANY_ID
                 and DEPR_GROUP_CONTROL.BUS_SEGMENT_ID = A_BUS_SEGMENT_ID
                 and DEPR_GROUP_CONTROL.UTILITY_ACCOUNT_ID = A_UTILITY_ACCOUNT_ID
                 and NVL(DEPR_GROUP_CONTROL.GL_ACCOUNT_ID, A_GL_ACCOUNT_ID) = A_GL_ACCOUNT_ID
                 and NVL(DEPR_GROUP_CONTROL.MAJOR_LOCATION_ID, A_MAJOR_LOCATION_ID) =
                     A_MAJOR_LOCATION_ID
                 and NVL(DEPR_GROUP_CONTROL.SUB_ACCOUNT_ID, A_SUB_ACCOUNT_ID) = A_SUB_ACCOUNT_ID
                 and NVL(TO_NUMBER(TO_CHAR(DEPR_GROUP_CONTROL.BOOK_VINTAGE, 'YYYY')), VNTG) =
                     VNTG
                 and NVL(DEPR_GROUP_CONTROL.SUBLEDGER_TYPE_ID, A_SUBLEDGER_TYPE_ID) =
                     A_SUBLEDGER_TYPE_ID
                 and NVL(DEPR_GROUP_CONTROL.ASSET_LOCATION_ID, A_ASSET_LOCATION_ID) =
                     A_ASSET_LOCATION_ID
                 and NVL(DEPR_GROUP_CONTROL.LOCATION_TYPE_ID, A_LOCATION_TYPE_ID) =
                     A_LOCATION_TYPE_ID
                 and NVL(DEPR_GROUP_CONTROL.PROPERTY_UNIT_ID, A_PROPERTY_UNIT_ID) =
                     A_PROPERTY_UNIT_ID
                 and NVL(DEPR_GROUP_CONTROL.RETIREMENT_UNIT_ID, A_RETIREMENT_UNIT_ID) =
                     A_RETIREMENT_UNIT_ID
                 and NVL(DEPR_GROUP_CONTROL.CLASS_CODE_ID, CC_ID) = CC_ID
                 and NVL(DEPR_GROUP_CONTROL.CC_VALUE, CC_VAL) = CC_VAL
                 and DEPR_GROUP_CONTROL.DEPR_GROUP_ID = DEPR_GROUP.DEPR_GROUP_ID
                 and DECODE(G_CHECK_DG_BS,
                            'YES',
                            TO_CHAR(DEPR_GROUP_CONTROL.BUS_SEGMENT_ID),
                            'NO_BUS_SEG_CHECK') =
                     DECODE(G_CHECK_DG_BS,
                            'YES',
                            TO_CHAR(DEPR_GROUP.BUS_SEGMENT_ID),
                            'NO_BUS_SEG_CHECK')
               order by DEPR_GROUP.COMPANY_ID,
                        DEPR_GROUP_CONTROL.BUS_SEGMENT_ID,
                        DEPR_GROUP_CONTROL.UTILITY_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.SUB_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.GL_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.MAJOR_LOCATION_ID,
                        DEPR_GROUP_CONTROL.ASSET_LOCATION_ID,
                        DEPR_GROUP_CONTROL.LOCATION_TYPE_ID,
                        DEPR_GROUP_CONTROL.PROPERTY_UNIT_ID,
                        DEPR_GROUP_CONTROL.RETIREMENT_UNIT_ID,
                        TO_CHAR(DEPR_GROUP_CONTROL.BOOK_VINTAGE, 'YYYY'),
                        DEPR_GROUP_CONTROL.SUBLEDGER_TYPE_ID,
                        DEPR_GROUP_CONTROL.CLASS_CODE_ID,
                        DEPR_GROUP_CONTROL.CC_VALUE)
       where ROWNUM = 1;

      if STATUS = 1 then
         return DG_ID;
      else
         /* invalid depr group */
         if G_DG_STATUS_CHECK then
            return - 9;
         else
            return DG_ID;
         end if;
      end if;
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         return - 1;
   end;

   --**************************************************************************
   --                            F_GET_DG_CC_ID
   --**************************************************************************
   function F_GET_DG_CC_ID return number is
      DG_CC_DESC varchar2(35);
      DG_CC_ID   number;

   begin

      select CONTROL_VALUE
        into DG_CC_DESC
        from PP_SYSTEM_CONTROL
       where UPPER(trim(CONTROL_NAME)) = 'DEPR GROUP CLASS CODE';

      select CLASS_CODE_ID
        into DG_CC_ID
        from CLASS_CODE
       where UPPER(trim(DESCRIPTION)) = UPPER(trim(DG_CC_DESC));

      return DG_CC_ID;

   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         return 0;
   end;

   --**************************************************************************
   --                            P_DG_STATUS_CHECK
   --**************************************************************************
   procedure P_DG_STATUS_CHECK(A_ENABLE boolean) is

   begin
      G_DG_STATUS_CHECK := A_ENABLE;
   end P_DG_STATUS_CHECK;

   --**************************************************************************
   --                            P_CHECK_DG_BS
   --**************************************************************************
   procedure P_CHECK_DG_BS is
      PP_CONTROL_VALUE varchar2(35);

   begin
      select UPPER(trim(CONTROL_VALUE))
        into PP_CONTROL_VALUE
        from PP_SYSTEM_CONTROL
       where UPPER(trim(CONTROL_NAME)) = 'POST VALIDATE DEPR BUSINESS SEGMENT';

      if PP_CONTROL_VALUE = 'YES' then
         G_CHECK_DG_BS := 'YES';
      else
         G_CHECK_DG_BS := 'NO';
      end if;
   end P_CHECK_DG_BS;



--**************************************************************************************
-- Initialization procedure (Called the first time this package is opened in a session)
-- Initialize the VALIDATE DEPR BUSINESS SEGMENT variable
--**************************************************************************************
begin
   P_CHECK_DG_BS;

end PP_DEPR_PKG;
/

--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (3242, 0, 2015, 2, 3, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2015.2.3.0_PP_DEPR_PKG.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
