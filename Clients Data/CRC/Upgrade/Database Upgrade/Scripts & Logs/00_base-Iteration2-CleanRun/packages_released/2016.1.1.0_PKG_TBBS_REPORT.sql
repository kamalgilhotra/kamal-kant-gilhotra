/*
||============================================================================
|| Application: PowerPlant
|| File Name:   PKG_TBBS_REPORT.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0 02/08/2016 Jarrett Skov   Original Version
|| 2016.1.0.0 09/02/2016 Jared Watkins  Harvest package into base
|| 2016.1.1.0 01/24/2017 Jarrett Skov   During implementation, discovered an issue when Merging the Current Year Included indicator
||                                        into tbbs_tax_balance_tmp for period 13 months
||============================================================================
*/
CREATE OR REPLACE PACKAGE PKG_TBBS_REPORT AS
	PROCEDURE P_REPORT_RUN (RTN_NUM OUT NUMBER);
	PROCEDURE P_GET_VARS;
	PROCEDURE P_PROCESS_TAX_CY;
	PROCEDURE P_PROCESS_TAX_PY;
	PROCEDURE P_PROCESS_REPORT_STG;
	PROCEDURE P_PROCESS_REPORT_STG_RC;
	PROCEDURE P_PROCESS_REPORT_STG_ADJ;
	PROCEDURE P_PROCESS_REPORT_STG_CLOSE;
	PROCEDURE P_PROCESS_REPORT_STG_CLEAN;
	PROCEDURE P_PROCESS_REPORT_DATA;
	PROCEDURE P_PROCESS_REPORT_CALC;
	PROCEDURE P_PROCESS_REPORT_SUMMARIZE;
END PKG_TBBS_REPORT;
/

CREATE OR REPLACE PACKAGE BODY PKG_TBBS_REPORT AS
--**************************************************************************
--                            VARIABLES
--**************************************************************************

	G_TA_VERSION_ID_CY	tbbs_rep_criteria_tmp.id%type;
	G_GL_MONTH 					tbbs_rep_criteria_tmp.id%type;
	G_ENTITY_ID					tbbs_rep_criteria_tmp.id%type;
	G_TBBS_MONTH				tbbs_rep_criteria_tmp.id%type;
	G_PP_TREE_TYPE_ID		tbbs_rep_criteria_tmp.id%type;
	G_TA_VERSION_ID_PY	tbbs_rep_criteria_tmp.id%type;
	G_INC_UNBOOKED_RTA	number(1,0);
	G_DELETE_EMPTY_AMOUNTS	number(1,0);
	G_COLUMNS						VARCHAR2(254);
	G_PROCESS_ID				pp_processes.process_id%type;
	G_MSG				VARCHAR2(254);
	G_COUNT							NUMBER(22,0);
	G_COL_CLOSE_COUNT		NUMBER(22,0);


--**************************************************************************
--                            PROCEDURES
--**************************************************************************

	PROCEDURE P_REPORT_RUN (RTN_NUM OUT NUMBER) is

		L_PROCESS_ID_FOUND	number(1,0);

	BEGIN
		L_PROCESS_ID_FOUND := 0;
		RTN_NUM := 0;

		-- Get the Process ID for logging
		G_MSG := 'Unable to find the Process ID for the "TBBS - RUN REPORT" in pp_processes';

		select PROCESS_ID into G_PROCESS_ID
		from PP_PROCESSES where UPPER(DESCRIPTION) = 'TBBS - RUN REPORT';

		IF G_PROCESS_ID IS NOT NULL THEN
		L_PROCESS_ID_FOUND := 1;
		end if;

		-- Start Log
		PKG_PP_LOG.P_START_LOG(G_PROCESS_ID);

		-- Get Vars
		PKG_TBBS_REPORT.P_GET_VARS;
		-- Current Year
		PKG_TBBS_REPORT.P_PROCESS_TAX_CY;
		-- Prior Year
		IF G_TA_VERSION_ID_PY IS NOT NULL THEN
		PKG_TBBS_REPORT.P_PROCESS_TAX_PY;
		END IF;
		-- Stage the data
		PKG_TBBS_REPORT.P_PROCESS_REPORT_STG;
		-- Bring in Reclasses
		PKG_TBBS_REPORT.P_PROCESS_REPORT_STG_RC;
		-- Bring in Adjustments
		PKG_TBBS_REPORT.P_PROCESS_REPORT_STG_ADJ;
		--Close selectecd columns. Only run if there are columns to close.
		IF G_COL_CLOSE_COUNT > 0 THEN
		PKG_TBBS_REPORT.P_PROCESS_REPORT_STG_CLOSE;
		END IF;
		-- Clean Staging data
		PKG_TBBS_REPORT.P_PROCESS_REPORT_STG_CLEAN;
		-- Build report structure
		PKG_TBBS_REPORT.P_PROCESS_REPORT_DATA;
		-- Update the data based on line item treatments, calculate adjusted Book basis, timing difference, deferred taxes and Tax basis
		pkg_tbbs_report.P_PROCESS_REPORT_CALC;
		-- Summarize report header and tree nodes
		PKG_TBBS_REPORT.P_PROCESS_REPORT_SUMMARIZE;

		PKG_PP_LOG.P_WRITE_MESSAGE('TBBS Report processed successfully');

		COMMIT;

		PKG_PP_LOG.P_END_LOG();

		RTN_NUM := 1;

	exception
		when others then
			IF L_PROCESS_ID_FOUND = 1 THEN
			PKG_PP_LOG.P_END_LOG();
			END IF;
			RAISE_APPLICATION_ERROR(-20000, G_MSG || ': ' || sqlerrm);
	END P_REPORT_RUN;

--*******************************************************************************************************************************************************
--*******************************************************************************************************************************************************

	PROCEDURE P_GET_VARS IS

	BEGIN
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_TBBS_REPORT.P_GET_VARS');

		-- Grab required report criteria into variables - Need a Current year case, GL Month and Entity. Oper ind will be left in the SQL as it can be multiple values.
		G_MSG := 'Getting current year Provision case from the report criteria table. Please ensure a Current Year case is selected.' ;
		SELECT id INTO G_TA_VERSION_ID_CY FROM tbbs_rep_criteria_tmp WHERE Upper(criteria_type) = 'TA_VERSION_ID_CY';
		IF G_TA_VERSION_ID_CY IS NULL THEN
		PKG_PP_ERROR.RAISE_ERROR('ERROR', G_MSG);
		end if;

		G_MSG := 'Getting Provision GL Month from the report criteria table. Please ensure a GL Month is selected.';
		SELECT id INTO G_GL_MONTH FROM tbbs_rep_criteria_tmp WHERE Upper(criteria_type) = 'GL_MONTH';
		IF G_GL_MONTH IS NULL THEN
		PKG_PP_ERROR.RAISE_ERROR('ERROR', G_MSG);
		end if;

		G_MSG := 'Getting filtered entity (jurisdiction) from the report criteria table. Please ensure an entity is selected.';
		SELECT id INTO G_ENTITY_ID FROM tbbs_rep_criteria_tmp WHERE Upper(criteria_type) = 'ENTITY_ID';
		IF G_ENTITY_ID IS NULL THEN
		PKG_PP_ERROR.RAISE_ERROR('ERROR', G_MSG);
		end if;

		G_MSG := 'Determining TBBS month number for GL Month ' || G_GL_MONTH || ' in case # ' || G_TA_VERSION_ID_CY ||
		'. Please ensure the TBBS Month value is populated in the Months Manager tab.';
		SELECT tbbs_month INTO G_TBBS_MONTH FROM tax_accrual_gl_month_def
		WHERE ta_version_id = G_TA_VERSION_ID_CY AND gl_month = G_GL_MONTH;
		IF G_TBBS_MONTH IS NULL THEN
		PKG_PP_ERROR.RAISE_ERROR('ERROR', G_MSG);
		end if;

		G_MSG := 'Getting TBBS tree structure from the report criteria table. Please ensure a tree structure is selected.';
		SELECT id INTO G_PP_TREE_TYPE_ID FROM tbbs_rep_criteria_tmp WHERE Upper(criteria_type) = 'PP_TREE_TYPE_ID';
		IF G_PP_TREE_TYPE_ID IS NULL THEN
		PKG_PP_ERROR.RAISE_ERROR('ERROR', G_MSG);
		end if;

		-- Check on Oper Ind filters. If no Oper Ind filters are selected, we want to make sure they all exist on the criteria table so they are used during the tax process.
		G_MSG := 'Checking Oper Ind filter criteria';
		SELECT Count(*) INTO G_COUNT FROM tbbs_rep_criteria_tmp WHERE Upper(criteria_type) = 'OPER_IND';
		IF G_COUNT = 0 THEN
		INSERT INTO tbbs_rep_criteria_tmp (criteria_type, id, description)
		SELECT DISTINCT 'OPER_IND', oper_ind, description FROM tax_accrual_oper_ind;
		COMMIT;
		END IF;

		-- If a prior year case is not selected, the Prior Year process should not run.
		G_MSG := 'Getting prior year Provision case from the report criteria table.' ;
		SELECT Count(*) INTO G_COUNT FROM tbbs_rep_criteria_tmp WHERE Upper(criteria_type) = 'TA_VERSION_ID_PY' AND id IS NOT NULL;
		IF G_COUNT <> 0 THEN
		SELECT id INTO G_TA_VERSION_ID_PY FROM tbbs_rep_criteria_tmp WHERE Upper(criteria_type) = 'TA_VERSION_ID_PY';
		ELSE
		G_TA_VERSION_ID_PY := NULL;
		END IF;

		-- Do we include unbooked RTA amounts as part of the timing difference balance? If this row does not exist in the criteria table, make it 0.
		-- If we did not select a prior year case, then dont include unbooked RTA
		G_MSG := 'Getting Unbooked RTA indicator from the report criteria table.';
		SELECT Count(*) INTO G_COUNT FROM tbbs_rep_criteria_tmp WHERE Upper(criteria_type) = 'INCLUDE_UNBOOKED_RTA';
		IF G_COUNT <> 0 AND G_TA_VERSION_ID_PY <> NULL THEN
		SELECT id INTO G_INC_UNBOOKED_RTA FROM tbbs_rep_criteria_tmp WHERE Upper(criteria_type) = 'INCLUDE_UNBOOKED_RTA';
		ELSE
		G_INC_UNBOOKED_RTA := 0;
		END IF;

		-- Toggle on deleting $0 Amount rows
		G_MSG := 'Getting Delete $0 Amount Rows indicator from the report criteria table.';
		SELECT Count(*) INTO G_COUNT FROM tbbs_rep_criteria_tmp WHERE Upper(criteria_type) = 'DELETE_EMPTY_AMOUNTS';
		IF G_COUNT <> 0 THEN
		SELECT id INTO G_DELETE_EMPTY_AMOUNTS FROM tbbs_rep_criteria_tmp WHERE Upper(criteria_type) = 'DELETE_EMPTY_AMOUNTS';
		ELSE
		G_DELETE_EMPTY_AMOUNTS := 0;
		END IF;

		-- Get count of closing columns
		G_MSG := 'Finding count of columns that are being closed.';
		SELECT Count(*) INTO G_COL_CLOSE_COUNT FROM
		(SELECT DISTINCT line_item_id FROM tbbs_column_close WHERE line_item_id IS NOT NULL)
		;

		-- Error handling using PKG_PP_ERROR
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			pkg_pp_error.raise_error('ERROR', g_msg);
			RAISE_APPLICATION_ERROR(-20000, G_MSG || ': ' || sqlerrm);
	END P_GET_VARS;


--*******************************************************************************************************************************************************
--*******************************************************************************************************************************************************

	PROCEDURE P_PROCESS_TAX_CY IS

	BEGIN

		PKG_PP_ERROR.SET_MODULE_NAME('PKG_TBBS_REPORT.P_PROCESS_TAX_CY');


		G_MSG := 'Clearing the tbbs_tax_balance_tmp table.';
		EXECUTE IMMEDIATE 'delete from tbbs_tax_balance_tmp';

		-- *********************************************************************************************************************
		-- Insert base rows
		-- Selection of the distinct Company, M Item and Oper from current year case.
		-- *********************************************************************************************************************
		G_MSG := 'Inserting base rows into tbbs_tax_balance_tmp';
		INSERT INTO tbbs_tax_balance_tmp
		(
		company_id,
		gl_month,
		m_item_id,
		oper_ind,
		entity_id,
		gl_company,
		tbbs_month,
		ta_version_id_cy,
		m_id_cy,
		m_type_id,
		reset_balance_ind,
		included_ind_cy,
		ta_version_id_py
		)
		SELECT
		control.company_id,
		months.gl_month,
		control.m_item_id,
		control.oper_ind,
		app.entity_id,
		comp_assign.gl_company,
		months.tbbs_month,
		G_TA_VERSION_ID_CY ta_version_id_cy,
		control.m_id,
		m_type.m_type_id,
		m_type.reset_balance_ind,
		0 included_ind_cy,
		G_TA_VERSION_ID_PY ta_version_id_py
		FROM
		tmp_companies comp
		JOIN
		tbbs_company_assign comp_assign
		ON comp_assign.company_id = comp.company_id
		JOIN
		tbbs_rep_criteria_tmp crit_oper
		ON Upper(crit_oper.criteria_type) = 'OPER_IND'
		JOIN
		tbbs_schema_assign schema_assign
		ON schema_assign.gl_company = comp_assign.gl_company
		JOIN
		tbbs_tax_assign tax_assign
		ON tax_assign.schema_id = schema_assign.schema_id
		JOIN
		tax_accrual_control control
		ON control.ta_version_id = G_TA_VERSION_ID_CY
		AND control.company_id = comp.company_id
		AND control.m_item_id = tax_assign.m_item_id
		AND control.oper_ind = crit_oper.id
		JOIN
		(
		SELECT DISTINCT
		company_id,
		entity_id
		FROM
		tax_accrual_apportionment
		) app
		ON app.company_id = control.company_id
		AND app.entity_id = G_ENTITY_ID
		JOIN
		tax_accrual_gl_month_def months
		ON months.ta_version_id = G_TA_VERSION_ID_CY
		AND months.gl_month = G_GL_MONTH
		JOIN
		tax_accrual_m_master m_master
		ON m_master.m_item_id = control.m_item_id
		JOIN
		tax_accrual_m_type m_type
		ON m_type.m_type_id = m_master.m_type_id
		;


		-- -- *********************************************************************************************************************
		-- -- Merge in the current year included indicator using the entity include ID
		-- --	This is used to determine if the M item is included for the selected jurisdiction at the given time.
		-- -- *********************************************************************************************************************
		G_MSG := 'Merging the Current Year Included indicator into tbbs_tax_balance_tmp';
		MERGE INTO tbbs_tax_balance_tmp d
		USING
		(
		SELECT
		  tax.company_id,
			tax.gl_month,
			tax.m_item_id,
			tax.oper_ind,
			tax.entity_id,
			tax.m_id_cy,
			con.entity_include_id
		FROM tbbs_tax_balance_tmp tax
		JOIN tax_accrual_control con
			ON con.m_id = tax.m_id_cy
		JOIN tax_accrual_version_gl_month months
			ON months.ta_version_id = tax.ta_version_id_cy
			and months.gl_month = tax.gl_month
		JOIN tax_accrual_entity_include_act inc_act
		ON inc_act.entity_include_id = con.entity_include_id
		AND inc_act.entity_id = tax.entity_id
		AND inc_act.effective_date =
		  ( select max(effective_date)
			from tax_accrual_entity_include_act inside
			WHERE inside.entity_include_id = inc_act.entity_include_id
			  AND inside.entity_id = inc_act.entity_id
			  AND inside.effective_date <= months.effective_date
		  )
		) s
		ON (s.company_id = d.company_id
		  AND s.gl_month = d.gl_month
			AND s.m_item_id = d.m_item_id
			AND s.oper_ind = d.oper_ind
			AND s.entity_id = d.entity_id
		)
		WHEN MATCHED THEN UPDATE SET d.included_ind_cy = 1
		;

		-- *********************************************************************************************************************
		-- Merge in the current Year Beginning Balance of the M Items
		-- *********************************************************************************************************************
		G_MSG := 'Merging the Current Year Beginning Balance into tbbs_tax_balance_tmp';
		merge INTO tbbs_tax_balance_tmp d
		USING
		(
		SELECT
		control.ta_version_id,
		control.company_id,
		control.m_item_id,
		control.oper_ind,
		control.m_id,
		m_item.beg_balance
		FROM
		tmp_companies comp
		CROSS JOIN
		(
		SELECT ta_version_id, Min(gl_month) gl_month
		FROM tax_accrual_gl_month_def
		WHERE ta_version_id = G_TA_VERSION_ID_CY
		GROUP BY ta_version_id
		) months
		JOIN
		tax_accrual_control control
		ON control.ta_version_id = months.ta_version_id
		AND control.company_id = comp.company_id
		JOIN
		tax_accrual_m_item m_item
		ON m_item.m_id = control.m_id
		AND m_item.gl_month = months.gl_month
		) s ON
		(	s.company_id = d.company_id
		AND s.m_item_id = d.m_item_id
		AND s.oper_ind = d.oper_ind
		)
		WHEN matched THEN UPDATE SET
		d.m_beg_year_balance_cy = s.beg_balance * d.included_ind_cy
		;


		-- *********************************************************************************************************************
		-- Merge in the Current Year activity and End Balance of the M Items
		-- *********************************************************************************************************************
		-- Logic should align with new 51051 handling of non-current adjustment activity.  The only difference is that M13 will have its own column in TBBS, while it will be included in Current Year on 51051
		G_MSG := 'Merging the Current Year Activity and Ending Balance into tbbs_tax_balance_tmp';
		merge INTO tbbs_tax_balance_tmp d
		USING
		(
			SELECT
			control.company_id,
			months.gl_month,
			control.m_item_id,
			control.oper_ind,
			control.m_id,
			Sum(
				Decode(
					months.rtp_month,
					1, 0,
					Decode(
						months.ytd_include,
						0, m_item.amount_act + m_item.amount_adj,
						0
					)
				)
			) OVER (PARTITION BY m_item.m_id ORDER BY months.gl_month) activity_ytd_cur,
			Sum(
				Decode(
					months.rtp_month,
					1, 0,
					Decode(
						months.process_option_id,
						3,0,
						Decode(
							months.ytd_include,
							1,m_item.amount_act + m_item.amount_adj,
							0
						) 
					)
				)
			) OVER (PARTITION BY m_item.m_id ORDER BY months.gl_month) activity_ytd_adj,
			Sum(
				Decode(
					months.rtp_month,
					1, 0,
					Decode(
						months.process_option_id,
						3, m_item.amount_act  + m_item.amount_adj,
						0
					)
				)
			) OVER (PARTITION BY m_item.m_id ORDER BY months.gl_month) activity_ytd_rta,
			Sum(
				Decode(
					months.rtp_month,
					1,	m_item.amount_act + m_item.amount_adj,
					0
				)
			) OVER (PARTITION BY m_item.m_id ORDER BY months.gl_month) activity_ytd_m13,
			end_balance
		FROM
		tmp_companies comp
		JOIN
		tax_accrual_version_gl_month months
		ON months.ta_version_id = G_TA_VERSION_ID_CY
		JOIN
		tax_accrual_control control
		ON control.ta_version_id = months.ta_version_id
		AND control.company_id = comp.company_id
		JOIN
		tax_accrual_m_item m_item
		ON m_item.m_id = control.m_id
		AND m_item.gl_month = months.gl_month
		) s ON
		(	s.company_id = d.company_id
		AND s.gl_month = d.gl_month
		AND s.m_item_id = d.m_item_id
		AND s.oper_ind = d.oper_ind
		)
		WHEN matched THEN UPDATE SET
		d.m_activity_ytd_cur_cy = s.activity_ytd_cur * d.included_ind_cy,
		d.m_activity_ytd_adj_cy = s.activity_ytd_adj * d.included_ind_cy,
		d.m_activity_ytd_rta_cy = s.activity_ytd_rta * d.included_ind_cy,
		d.m_activity_ytd_m13_cy = s.activity_ytd_m13 * d.included_ind_cy,
		d.m_end_ltd_balance_cy = s.end_balance * d.included_ind_cy
		;

		-- *********************************************************************************************************************
		-- Merge in the Current Year Def Tax balances
		-- *********************************************************************************************************************
		-- Dont multiply this by the include ind, as we may have deferred taxes in a jurisdiction that is not part of the entity include ID (fed offset on a state only M)
		G_MSG := 'Merging the Current Year Deferred Tax Balances into tbbs_tax_balance_tmp';
		merge INTO tbbs_tax_balance_tmp d
		USING
		(
		SELECT
			fas_all.company_id,
			fas_all.gl_month,
			fas_all.m_item_id,
			fas_all.oper_ind,
			fas_all.entity_id,
			sum(fas_all.m_def_tax_bal) m_def_tax_bal,
			sum(fas_all.m_def_tax_bal_reg) m_def_tax_bal_reg
		FROM
		(
		SELECT
			control.company_id,
			months.gl_month,
			control.m_item_id,
			control.oper_ind,
			Coalesce(ent.actual_entity_id, ent.entity_id) entity_id,
			control.m_id,
		Decode(
			coalesce(fas_con.reg_ind,0),
			1, coalesce(accum_reg_dit_end,0),
			coalesce(accum_reg_dit_end,0) + coalesce(fas109_increment_end,0)
			) m_def_tax_bal,
		Decode(
			coalesce(fas_con.reg_ind,0),
			1, coalesce(fas109_increment_end,0) + coalesce(entity_grossup_end,0),
			0
			) m_def_tax_bal_reg
		FROM
		tmp_companies comp
		JOIN
		tax_accrual_gl_month_def months
		ON months.ta_version_id = G_TA_VERSION_ID_CY
		AND months.gl_month = G_GL_MONTH
		JOIN
		tax_accrual_control control
		ON control.ta_version_id = months.ta_version_id
		AND control.company_id = comp.company_id
		JOIN
		tax_accrual_month_type month_type
		ON month_type.month_type_id = months.month_type_id
		JOIN
		tax_accrual_activity_type activity_type
		ON activity_type.activity_type_id = month_type.activity_type_id
		JOIN
		tax_accrual_fas109_control fas_con
		ON fas_con.m_id = control.m_id
		JOIN
		tax_accrual_fas109 fas
		ON fas.m_id = control.m_id
		and fas.entity_id = fas_con.entity_id
		AND fas.gl_month = months.gl_month
		JOIN
		tax_accrual_entity ent
		ON ent.entity_id = fas_con.entity_id
		) fas_all
		GROUP BY
		fas_all.company_id,
		fas_all.gl_month,
		fas_all.m_item_id,
		fas_all.oper_ind,
		fas_all.entity_id
		) s ON
		(	s.company_id = d.company_id
		AND s.gl_month = d.gl_month
		AND s.m_item_id = d.m_item_id
		AND s.oper_ind = d.oper_ind
		AND s.entity_id = d.entity_id
		)
		WHEN matched THEN UPDATE SET
		d.m_def_tax_bal = s.m_def_tax_bal,
		d.m_def_tax_bal_reg = s.m_def_tax_bal_reg
		;

		COMMIT;

		-- Error handling using PKG_PP_ERROR
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			pkg_pp_error.raise_error('ERROR', g_msg);
			RAISE_APPLICATION_ERROR(-20000, G_MSG || ': ' || sqlerrm);
	END P_PROCESS_TAX_CY;

--*******************************************************************************************************************************************************
--*******************************************************************************************************************************************************

	PROCEDURE P_PROCESS_TAX_PY IS
	BEGIN
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_TBBS_REPORT.P_PROCESS_TAX_PY');

		-- *********************************************************************************************************************
		-- Merge in base rows from the Prior year Provision case. Don't insert where not exists - If it is not in the current year ending balance, we don't need it.
		-- *********************************************************************************************************************
		G_MSG := 'Merging the Prior Year ta_version_id and m_ids into tbbs_tax_balance_tmp';
		MERGE INTO tbbs_tax_balance_tmp d
		USING
		(
		SELECT
		control.company_id,
		months.gl_month,
		control.m_item_id,
		control.oper_ind,
		app.entity_id,
		comp_assign.gl_company,
		months.tbbs_month,
		control.ta_version_id,
		control.m_id,
		m_type.m_type_id,
		m_type.reset_balance_ind
		FROM
		tmp_companies comp
		JOIN
		tbbs_company_assign comp_assign
		ON comp_assign.company_id = comp.company_id
		JOIN
		tbbs_rep_criteria_tmp crit_oper
		ON Upper(crit_oper.criteria_type) = 'OPER_IND'
		JOIN
		tbbs_schema_assign schema_assign
		ON schema_assign.gl_company = comp_assign.gl_company
		JOIN
		tbbs_tax_assign tax_assign
		ON tax_assign.schema_id = schema_assign.schema_id
		JOIN
		tax_accrual_control control
		ON control.ta_version_id = G_TA_VERSION_ID_PY
		AND control.company_id = comp.company_id
		AND control.m_item_id = tax_assign.m_item_id
		AND control.oper_ind = crit_oper.id
		JOIN
		(
		SELECT DISTINCT
		company_id,
		entity_id
		FROM
		tax_accrual_apportionment
		) app
		ON app.company_id	= control.company_id
		AND app.entity_id = G_ENTITY_ID
		JOIN
		tax_accrual_gl_month_def months
		ON months.ta_version_id = G_TA_VERSION_ID_CY
		AND months.gl_month = G_GL_MONTH
		JOIN
		tax_accrual_m_master m_master
		ON m_master.m_item_id = control.m_item_id
		JOIN
		tax_accrual_m_type m_type
		ON m_type.m_type_id = m_master.m_type_id
		) s ON
		(	s.company_id = d.company_id
		AND s.gl_month = d.gl_month
		AND s.m_item_id = d.m_item_id
		AND s.oper_ind = d.oper_ind
		AND s.entity_id = d.entity_id
		)
		WHEN MATCHED THEN UPDATE SET
		d.ta_version_id_py = s.ta_version_id,
		d.m_id_py = s.m_id
		;

		-- *********************************************************************************************************************
		-- Merge in the Prior Year M Item Beg Balance. Note that we dont use a Prior Year include indicator, only current.
		--		If the M Item is not included in the current year, don't show it in the Prior year either.
		-- *********************************************************************************************************************
		G_MSG := 'Merging the Prior Year Beg Balance into tbbs_tax_balance_tmp';
		merge INTO tbbs_tax_balance_tmp d
		USING
		(
		SELECT
		control.ta_version_id,
		control.company_id,
		control.m_item_id,
		control.oper_ind,
		control.m_id,
		m_item.beg_balance
		FROM
		tmp_companies comp
		JOIN
		(
		SELECT ta_version_id, Min(gl_month) gl_month
		FROM tax_accrual_gl_month_def
		GROUP BY ta_version_id
		) months
		ON months.ta_version_id = G_TA_VERSION_ID_PY
		JOIN
		tax_accrual_control control
		ON control.ta_version_id = months.ta_version_id
		AND control.company_id = comp.company_id
		JOIN
		tax_accrual_m_item m_item
		ON m_item.m_id = control.m_id
		AND m_item.gl_month = months.gl_month
		) s ON
		(	s.company_id = d.company_id
		AND s.m_item_id = d.m_item_id
		AND s.oper_ind = d.oper_ind
		)
		WHEN matched THEN UPDATE SET
		d.m_beg_year_balance_py = s.beg_balance * d.included_ind_cy
		;

		-- *********************************************************************************************************************
		-- Merge in Prior Year M activity and End Balance
		-- *********************************************************************************************************************
		-- Logic should align with new 51051 handling of non-current adjustment activity.  The only difference is that M13 will have its own column in TBBS, while it will be included in Current Year on 51051
		G_MSG := 'Merging the Prior Year M Activity and End Balance into tbbs_tax_balance_tmp';
		merge INTO tbbs_tax_balance_tmp d
		USING
		(
		SELECT
		control.company_id,
		control.m_item_id,
		control.oper_ind,
			Sum(
				Decode(
					months.rtp_month,
					1, 0,
					Decode(
						months.ytd_include,
						0, m_item.amount_act + m_item.amount_adj,
						0
					)
				)
			) activity_ytd_cur,
			Sum(
				Decode(
					months.rtp_month,
					1, 0,
					Decode(
						months.process_option_id,
						3,0,
						Decode(
							months.ytd_include,
							1,m_item.amount_act + m_item.amount_adj,
							0
						)
					)
				)
			) activity_ytd_adj,
			Sum(
				Decode(
					months.rtp_month,
					1, 0,
					Decode(
						months.process_option_id,
						3, m_item.amount_act  + m_item.amount_adj,
						0
					)
				)
			) activity_ytd_rta,
			Sum(
				Decode(
					months.rtp_month,
					1,	m_item.amount_act + m_item.amount_adj,
					0
				)
			) activity_ytd_m13
		FROM
		tmp_companies comp
		JOIN
		tax_accrual_version_gl_month months
		ON months.ta_version_id = G_TA_VERSION_ID_PY
		JOIN
		tax_accrual_control control
		ON control.ta_version_id = months.ta_version_id
		AND control.company_id = comp.company_id
		JOIN
		tax_accrual_m_item m_item
		ON m_item.m_id = control.m_id
		AND m_item.gl_month = months.gl_month
		GROUP BY
		control.company_id,
		control.m_item_id,
		control.oper_ind
		) s ON
		(	s.company_id = d.company_id
		AND s.m_item_id = d.m_item_id
		AND s.oper_ind = d.oper_ind
		)
		WHEN matched THEN UPDATE SET
		d.m_activity_ytd_cur_py = s.activity_ytd_cur * d.included_ind_cy,
		d.m_activity_ytd_adj_py = s.activity_ytd_adj * d.included_ind_cy,
		d.m_activity_ytd_rta_py = s.activity_ytd_rta * d.included_ind_cy,
		d.m_activity_ytd_m13_py = s.activity_ytd_m13 * d.included_ind_cy
		;

		COMMIT;

		-- Error handling using PKG_PP_ERROR
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			pkg_pp_error.raise_error('ERROR', g_msg);
			RAISE_APPLICATION_ERROR(-20000, G_MSG || ': ' || sqlerrm);
	END P_PROCESS_TAX_PY;

--*******************************************************************************************************************************************************
--*******************************************************************************************************************************************************

	PROCEDURE P_PROCESS_REPORT_STG IS
	BEGIN
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_TBBS_REPORT.P_PROCESS_REPORT_STG');

		-- *********************************************************************************************************************
		-- Insert in report staging data from TBBS book and tax tables.
		-- This statement cartesian joins the BOOK_DETAIL, TAX_DETAIL, and TAX_ADJ "Line Types". This is to ensure that each type of record is separated for drilling down.
		-- *********************************************************************************************************************
		G_MSG := 'Clearing the tbbs_report_stg_tmp staging table';
		DELETE FROM tbbs_report_stg_tmp;

		G_MSG := 'Inserting the base rows into the tbbs_report_stg_tmp staging table';
		INSERT INTO tbbs_report_stg_tmp
		(
		id,
		ta_version_id_cy,
		ta_version_id_py,
		gl_company,
		gl_month,
		tbbs_month,
		line_item_id,
		line_item_description,
		treatment_type_id,
		line_type_id,
		line_type_order,
		line_description,
		book_account_id,
		book_account_description,
		primary_account_description,
		book_amount,
		book_rc,
		book_adj,
		m_item_id,
		m_reset_bal_ind,
		m_py_exists_ind,
		entity_id,
		m_description,
		m_beg_bal_py,
		m_activity_py,
		m_adjust_py,
		m_variance_py,
		m_m13_py,
		m_beg_bal_cy,
		m_activity_cy,
		m_adjust_cy,
		m_rta_cy,
		m_variance_cy,
		m_ltd_bal,
		m_unbooked_rta,
		m_def_tax_bal,
		m_def_tax_bal_reg,
		tax_m_rc,
		tax_def_rc,
		tax_m_adj
		)
		SELECT
		id,
		ta_version_id_cy,
		ta_version_id_py,
		gl_company,
		gl_month,
		tbbs_month,
		line_item_id,
		line_item_description,
		treatment_type_id,
		line_type_id,
		line_type_order,
		NULL,
		book_account_id,
		book_account_description,
		primary_account_description,
		book_amount,
		book_rc,
		book_adj,
		m_item_id,
		m_reset_bal_ind,
		m_py_exists_ind,
		entity_id,
		m_description,
		m_beg_bal_py,
		m_activity_py,
		m_adjust_py,
		m_variance_py,
		m_m13_py,
		m_beg_bal_cy,
		m_activity_cy,
		m_adjust_cy,
		m_rta_cy,
		m_variance_cy,
		m_ltd_bal,
		m_unbooked_rta,
		m_def_tax_bal,
		m_def_tax_bal_reg,
		tax_m_rc,
		tax_def_rc,
		tax_m_adj
		FROM
		(
		SELECT
		Row_Number() OVER
		(
		ORDER BY
		line_item.line_item_id,
		line_type.line_type_order,
		co_assign.gl_company,
		book.description,
		tax.m_description
		) id,
		G_TA_VERSION_ID_CY ta_version_id_cy,
		G_TA_VERSION_ID_PY ta_version_id_py,
		co_assign.gl_company,
		G_GL_MONTH gl_month,
		G_TBBS_MONTH tbbs_month,
		line_item.line_item_id,
		line_item.description line_item_description,
		line_item.treatment_type_id,
		line_type.line_type_id,
		line_type.line_type_order,

		book.book_account_id,
		coalesce(book.description, 'N/A') book_account_description,
		Decode(book.primary_description, '', ' ', ' - ' || book.primary_description) primary_account_description,
		book.book_amount book_amount,
		0 book_rc,
		0 book_adj,

		tax.m_item_id,
		tax.m_reset_bal_ind,
		nvl2(tax.m_item_id, 0, null) m_py_exists_ind,
		tax.entity_id,
		coalesce(tax.m_description, 'N/A') m_description,

		tax.m_beg_bal_py m_beg_bal_py,
		tax.m_activity_py m_activity_py,
		tax.m_adjust_py m_adjust_py,
		Nvl2(G_TA_VERSION_ID_PY, tax.m_beg_bal_cy - (tax.m_beg_bal_py + tax.m_activity_py + tax.m_adjust_py), 0) m_variance_py,
		tax.m_m13_py m_m13_py,
		tax.m_beg_bal_cy m_beg_bal_cy,
		tax.m_activity_cy m_activity_cy,
		tax.m_adjust_cy m_adjust_cy,
		tax.m_rta_cy m_rta_cy,
		tax.m_ltd_bal - (tax.m_beg_bal_cy + tax.m_activity_cy + tax.m_adjust_cy + tax.m_rta_cy) m_variance_cy,
		tax.m_ltd_bal m_ltd_bal,
		Nvl2(G_TA_VERSION_ID_PY, tax.m_unbooked_rta, 0) m_unbooked_rta,
		tax.m_def_tax_bal m_def_tax_bal,
		tax.m_def_tax_bal_reg m_def_tax_bal_reg,
		0 tax_m_rc,
		0 tax_def_rc,
		0 tax_m_adj
		FROM tmp_companies co_tmp
		JOIN tbbs_company_assign co_assign
		ON co_assign.company_id = co_tmp.company_id
		CROSS JOIN tbbs_line_item line_item
		JOIN tbbs_line_type line_type
		ON line_type.line_type_id IN (4,5)
		-- Tax join
		LEFT JOIN
		(
		SELECT
		tax_assign.line_item_id,
		tax_bal.gl_company,
		tax_bal.gl_month,
		tax_bal.tbbs_month,
		tax_bal.m_item_id,
		m_type.reset_balance_ind m_reset_bal_ind,
		m_master.description m_description,
		tax_bal.entity_id,

		Sum(coalesce(tax_bal.m_beg_year_balance_py,0)) m_beg_bal_py,
		Sum(coalesce(tax_bal.m_activity_ytd_cur_py,0)) m_activity_py,
		Sum(coalesce(tax_bal.m_activity_ytd_adj_py,0)) + Sum(coalesce(tax_bal.m_activity_ytd_rta_py,0)) m_adjust_py,
		Sum(coalesce(tax_bal.m_activity_ytd_m13_py,0)) m_m13_py,
		Sum(coalesce(tax_bal.m_beg_year_balance_cy,0)) m_beg_bal_cy,
		Sum(coalesce(tax_bal.m_activity_ytd_cur_cy,0)) + Sum(coalesce(tax_bal.m_activity_ytd_m13_cy,0)) m_activity_cy,
		Sum(coalesce(tax_bal.m_activity_ytd_adj_cy,0)) m_adjust_cy,
		Sum(coalesce(tax_bal.m_activity_ytd_rta_cy,0)) m_rta_cy,
		Sum(coalesce(tax_bal.m_end_ltd_balance_cy,0)) m_ltd_bal,
		Sum(coalesce(tax_bal.m_activity_ytd_m13_py,0)) - Sum(coalesce(tax_bal.m_activity_ytd_rta_cy,0)) m_unbooked_rta,

		Sum(coalesce(tax_bal.m_def_tax_bal,0)) m_def_tax_bal,
		Sum(coalesce(tax_bal.m_def_tax_bal_reg,0)) m_def_tax_bal_reg
		FROM tbbs_tax_balance_tmp tax_bal
		JOIN tbbs_schema_assign sch_assign
		ON sch_assign.gl_company = tax_bal.gl_company
		JOIN tbbs_tax_assign tax_assign
		ON tax_assign.schema_id = sch_assign.schema_id
		AND tax_assign.m_item_id = tax_bal.m_item_id
		JOIN tax_accrual_m_master m_master
		ON m_master.m_item_id = tax_bal.m_item_id
		JOIN tax_accrual_m_type m_type
		ON m_type.m_type_id = m_master.m_type_id
		GROUP BY
		tax_assign.line_item_id,
		tax_bal.gl_company,
		tax_bal.gl_month,
		tax_bal.tbbs_month,
		tax_bal.m_item_id,
		m_type.reset_balance_ind,
		m_master.description,
		tax_bal.entity_id
		) tax
		ON tax.line_item_id = line_item.line_item_id
		AND tax.gl_company = co_assign.gl_company
		AND line_type.line_type_id = 5
		-- Book Join
		LEFT JOIN
		(
		SELECT
		book_assign.schema_id,
		book_assign.line_item_id,
		book_bal.gl_company,
		book_bal.month_number,
		book_acct.book_account_id,
		book_acct.description,
		book_bal.amount book_amount,
		book_acct.primary_description
		FROM tmp_companies tmp_co
		JOIN tbbs_company_assign co_assign
		ON co_assign.company_id = tmp_co.company_id
		JOIN tbbs_schema_assign sch_assign
		ON sch_assign.gl_company = co_assign.gl_company
		JOIN tbbs_book_assign book_assign
		ON book_assign.schema_id = sch_assign.schema_id
		JOIN tbbs_book_account book_acct
		ON book_acct.book_account_id = book_assign.book_account_id
		JOIN tbbs_book_balance book_bal
		ON book_bal.book_account_id = book_assign.book_account_id
		AND book_bal.gl_company = co_assign.gl_company
		WHERE book_bal.month_number = G_TBBS_MONTH
		) book
		ON book.line_item_id = line_item.line_item_id
		AND book.gl_company = co_assign.gl_company
		AND line_type.line_type_id = 4
		)
		ORDER BY id
		;

		COMMIT;

		-- Error handling using PKG_PP_ERROR
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			pkg_pp_error.raise_error('ERROR', g_msg);
			RAISE_APPLICATION_ERROR(-20000, G_MSG || ': ' || sqlerrm);
	END P_PROCESS_REPORT_STG;

--*******************************************************************************************************************************************************
--*******************************************************************************************************************************************************

	PROCEDURE P_PROCESS_REPORT_STG_RC IS
	BEGIN
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_TBBS_REPORT.P_PROCESS_REPORT_STG_RC');

		-- *********************************************************************************************************************
		-- Merge in the Book Reclass adjustments. Need to note the effective dates with the month we are running the report for.
		-- Dont use "when not matched then insert". If a book account is not mapped to a line item in the staging table, then we dont need it anyway.
		-- *********************************************************************************************************************
		G_MSG := 'Merging Book Reclass adjustments into the tbbs_report_stg_tmp staging table';
		MERGE INTO tbbs_report_stg_tmp d
		USING
		(
		WITH rc_with AS
		(
		select
		  rc.gl_company,
		  rc.src_book_account_id,
		  rc.dst_book_account_id,
		  rc.reclass_active_ind,
		  coalesce(stg.book_amount,0) book_amount
		FROM (
		  SELECT
			coalesce(sep_comp.gl_company, all_comp.gl_company) gl_company,
			coalesce(sep_comp.effective_month, all_comp.effective_month) effective_month,
			coalesce(sep_comp.src_book_account_id, all_comp.src_book_account_id) src_book_account_id,
			coalesce(sep_comp.dst_book_account_id, all_comp.dst_book_account_id) dst_book_account_id,
			coalesce(sep_comp.reclass_active_ind, all_comp.reclass_active_ind) reclass_active_ind,
			row_number() over
			  (partition by
				coalesce(sep_comp.gl_company, all_comp.gl_company),
				coalesce(sep_comp.src_book_account_id, all_comp.src_book_account_id)
			  order by coalesce(sep_comp.effective_month, all_comp.effective_month) desc
			) priority
		  FROM
		  (
			SELECT
			  comp.gl_company,
			  rc.effective_month,
			  rc.src_book_account_id,
			  rc.dst_book_account_id,
			  rc.reclass_active_ind
			FROM tbbs_book_reclass rc
			CROSS JOIN tbbs_gl_company comp
			WHERE comp.gl_company <> 'ALL_COMPANIES'
			  AND rc.gl_company = 'ALL_COMPANIES'
			  AND rc.effective_month <= G_TBBS_MONTH
		  ) all_comp
		  FULL OUTER JOIN
		  (
			SELECT
			  rc.gl_company,
			  rc.effective_month,
			  rc.src_book_account_id,
			  rc.dst_book_account_id,
			  rc.reclass_active_ind
			FROM tbbs_book_reclass rc
			WHERE rc.gl_company <> 'ALL_COMPANIES'
			  AND rc.effective_month <= G_TBBS_MONTH
		  ) sep_comp
			ON sep_comp.gl_company = all_comp.gl_company
			  AND sep_comp.effective_month = all_comp.effective_month
			AND sep_comp.src_book_account_id = all_comp.src_book_account_id
		) rc
		JOIN tbbs_report_stg_tmp stg
		  ON stg.gl_company = rc.gl_company
		  AND stg.book_account_id = rc.src_book_account_id
		WHERE rc.priority = 1
		  AND rc.reclass_active_ind = 1
		)
		SELECT
		gl_company,
		book_account_id,
		Sum(amount) rc_amount
		FROM
		(
		SELECT
		rc_with.gl_company,
		rc_with.dst_book_account_id book_account_id,
		rc_with.book_amount amount
		FROM
		rc_with
		UNION
		SELECT
		rc_with.gl_company,
		rc_with.src_book_account_id book_account_id,
		rc_with.book_amount * -1 amount
		FROM
		rc_with
		)
		GROUP BY
		gl_company,
		book_account_id
		) s ON
		(s.gl_company = d.gl_company
		AND s.book_account_id = d.book_account_id
		)
		WHEN MATCHED THEN UPDATE
		SET d.book_rc = s.rc_amount
		;


		-- *********************************************************************************************************************
		-- Merge in the Tax Reclass adjustments. Need to note the effective dates with the month we are running the report for.
		-- Dont use "when not matched then insert". If a book account is not mapped to a line item in the staging table, then we dont need it anyway.
		-- Also need to reclass total deferred tax balances
		-- *********************************************************************************************************************
		G_MSG := 'Merging Tax Reclass adjustments into the tbbs_report_stg_tmp staging table';
		MERGE INTO tbbs_report_stg_tmp d
		USING
		(
		WITH rc_with AS
		(
		select
		  rc.gl_company,
		  rc.src_m_item_id,
		  rc.dst_m_item_id,
		  rc.reclass_active_ind,
		  coalesce(stg.m_ltd_bal,0) m_amount,
		  coalesce(m_def_tax_bal,0) + coalesce(m_def_tax_bal_reg,0) def_amount
		FROM (
		  SELECT
			coalesce(sep_comp.gl_company, all_comp.gl_company) gl_company,
			coalesce(sep_comp.effective_month, all_comp.effective_month) effective_month,
			coalesce(sep_comp.src_m_item_id, all_comp.src_m_item_id) src_m_item_id,
			coalesce(sep_comp.dst_m_item_id, all_comp.dst_m_item_id) dst_m_item_id,
			coalesce(sep_comp.reclass_active_ind, all_comp.reclass_active_ind) reclass_active_ind,
			row_number() over
			  (partition by
				coalesce(sep_comp.gl_company, all_comp.gl_company),
				coalesce(sep_comp.src_m_item_id, all_comp.src_m_item_id)
			  order by coalesce(sep_comp.effective_month, all_comp.effective_month) desc
			) priority
		  FROM
		  (
			SELECT
			  comp.gl_company,
			  rc.effective_month,
			  rc.src_m_item_id,
			  rc.dst_m_item_id,
			  rc.reclass_active_ind
			FROM tbbs_tax_reclass rc
			CROSS JOIN tbbs_gl_company comp
			WHERE comp.gl_company <> 'ALL_COMPANIES'
			  AND rc.gl_company = 'ALL_COMPANIES'
			  AND rc.effective_month <= G_TBBS_MONTH
		  ) all_comp
		  FULL OUTER JOIN
		  (
			SELECT
			  rc.gl_company,
			  rc.effective_month,
			  rc.src_m_item_id,
			  rc.dst_m_item_id,
			  rc.reclass_active_ind
			FROM tbbs_tax_reclass rc
			WHERE rc.gl_company <> 'ALL_COMPANIES'
			  AND rc.effective_month <= G_TBBS_MONTH
		  ) sep_comp
			ON sep_comp.gl_company = all_comp.gl_company
			  AND sep_comp.effective_month = all_comp.effective_month
			AND sep_comp.src_m_item_id = all_comp.src_m_item_id
		) rc
		JOIN tbbs_report_stg_tmp stg
		  ON stg.gl_company = rc.gl_company
		  AND stg.m_item_id = rc.src_m_item_id
		WHERE rc.priority = 1
		  AND rc.reclass_active_ind = 1
		)
		SELECT
		gl_company,
		m_item_id,
		Sum(m_amount) m_rc_amount,
		Sum(def_amount) def_rc_amount
		FROM
		(
		SELECT
		rc_with.gl_company,
		rc_with.dst_m_item_id m_item_id,
		rc_with.m_amount,
		rc_with.def_amount
		FROM
		rc_with
		UNION
		SELECT
		rc_with.gl_company,
		rc_with.src_m_item_id m_item_id,
		rc_with.m_amount * -1 m_amount,
		rc_with.def_amount * -1 def_amount
		FROM
		rc_with
		)
		GROUP BY
		gl_company,
		m_item_id
		) s ON
		(s.gl_company = d.gl_company
		AND s.m_item_id = d.m_item_id
		)
		WHEN MATCHED THEN UPDATE
		SET d.tax_m_rc = s.m_rc_amount,
			d.tax_def_rc = s.def_rc_amount
		;

		COMMIT;

		-- Error handling using PKG_PP_ERROR
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			pkg_pp_error.raise_error('ERROR', g_msg);
			RAISE_APPLICATION_ERROR(-20000, G_MSG || ': ' || sqlerrm);
	END P_PROCESS_REPORT_STG_RC;

--*******************************************************************************************************************************************************
--*******************************************************************************************************************************************************

	PROCEDURE P_PROCESS_REPORT_STG_ADJ IS
	BEGIN
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_TBBS_REPORT.P_PROCESS_REPORT_STG_ADJ');


		-- *********************************************************************************************************************
		-- Merge in the manual Book adjustments. Need to note the effective dates with the month we are running the report for.
		-- Dont use "when not matched then insert". If a book account is not mapped to a line item in the staging table, then we dont need it anyway.
		-- *********************************************************************************************************************
		G_MSG := 'Merging Manual Book adjustments into the tbbs_report_stg_tmp staging table';
		MERGE INTO tbbs_report_stg_tmp d
		USING
		(
		WITH adj AS
		(
		SELECT
		gl_company,
		G_TBBS_MONTH month_number,
		dr_book_account_id,
		cr_book_account_id,
		amount
		FROM tbbs_book_adjust a
		WHERE
		(
		gl_company,
		dr_book_account_id,
		cr_book_account_id,
		effective_month
		) IN
		(
		SELECT
		gl_company,
		dr_book_account_id,
		cr_book_account_id,
		Max(effective_month) effective_month
		FROM tbbs_book_adjust
		WHERE effective_month <= G_TBBS_MONTH
		GROUP BY
		gl_company,
		dr_book_account_id,
		cr_book_account_id
		)
		)
		SELECT
		gl_company,
		month_number,
		book_account_id,
		Sum(amount) adj_amount
		FROM
		(
		SELECT
		adj.gl_company,
		adj.month_number,
		adj.dr_book_account_id book_account_id,
		Sum(Abs(adj.amount)) amount
		FROM
		adj
		GROUP BY
		adj.gl_company,
		adj.month_number,
		adj.dr_book_account_id

		UNION

		SELECT
		adj.gl_company,
		adj.month_number,
		adj.cr_book_account_id book_account_id,
		Sum(Abs(adj.amount))*-1 amount
		FROM
		adj
		GROUP BY
		adj.gl_company,
		adj.month_number,
		adj.cr_book_account_id
		) inside
		GROUP BY
		gl_company,
		month_number,
		book_account_id
		) s ON
		(s.gl_company = d.gl_company
		AND s.month_number = d.tbbs_month
		AND s.book_account_id = d.book_account_id
		)
		WHEN MATCHED THEN UPDATE
		SET d.book_adj = s.adj_amount
		;




		-- *********************************************************************************************************************
		-- Merge in the manual Tax adjustments. Need to note the effective dates with the month we are running the report for.
		-- Note that these adjustments exist by line item, not M item.
		-- *********************************************************************************************************************
		G_MSG := 'Merging Manual Tax adjustments into the tbbs_report_stg_tmp staging table';
		MERGE INTO tbbs_report_stg_tmp d
		USING
		(
		WITH adj AS
		(
		SELECT
		gl_company,
		G_TBBS_MONTH month_number,
		dr_m_item_id,
		cr_m_item_id,
		amount
		FROM tbbs_tax_adjust a
		WHERE
		(
		gl_company,
		dr_m_item_id,
		cr_m_item_id,
		effective_month
		) IN
		(
		SELECT
		gl_company,
		dr_m_item_id,
		cr_m_item_id,
		Max(effective_month) effective_month
		FROM tbbs_tax_adjust
		WHERE effective_month <= G_TBBS_MONTH
		GROUP BY
		gl_company,
		dr_m_item_id,
		cr_m_item_id
		)
		)
		SELECT
		gl_company,
		month_number,
		m_item_id,
		Sum(amount) adj_amount
		FROM
		(
		SELECT
		adj.gl_company,
		adj.month_number,
		adj.dr_m_item_id m_item_id,
		Sum(Abs(adj.amount)) amount
		FROM
		adj
		GROUP BY
		adj.gl_company,
		adj.month_number,
		adj.dr_m_item_id

		UNION

		SELECT
		adj.gl_company,
		adj.month_number,
		adj.cr_m_item_id m_item_id,
		Sum(Abs(adj.amount))*-1 amount
		FROM
		adj
		GROUP BY
		adj.gl_company,
		adj.month_number,
		adj.cr_m_item_id
		) inside
		GROUP BY
		gl_company,
		month_number,
		m_item_id
		) s ON
		(s.gl_company = d.gl_company
		AND s.month_number = d.tbbs_month
		AND s.m_item_id = d.m_item_id
		)
		WHEN MATCHED THEN UPDATE
		SET d.tax_m_adj = s.adj_amount
		;


		COMMIT;

		-- Error handling using PKG_PP_ERROR
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			pkg_pp_error.raise_error('ERROR', g_msg);
			RAISE_APPLICATION_ERROR(-20000, G_MSG || ': ' || sqlerrm);
	END P_PROCESS_REPORT_STG_ADJ;



--*******************************************************************************************************************************************************
--*******************************************************************************************************************************************************


	--This procuedure closes preliminary column totals to a line item specified in tbbs_column_close, bringing the final total to 0
	PROCEDURE P_PROCESS_REPORT_STG_CLOSE IS
		v_sql          VARCHAR2(4000);
		CURSOR c1 IS
			SELECT column_id, column_name, description, line_item_id
			FROM tbbs_column_close
			WHERE line_item_id IS NOT NULL;
	BEGIN
		pkg_pp_error.set_module_name('PKG_TBBS_REPORT.P_PROCESS_REPORT_STG_CLOSE');
		G_MSG := 'Checking for column close configruation';

		-- Insert the base rows to hold the adjustments
		INSERT INTO tbbs_report_stg_tmp
		(
		id,
		ta_version_id_cy,
		ta_version_id_py,
		gl_company,
		gl_month,
		tbbs_month,
		line_item_id,
		line_item_description,
		treatment_type_id,
		line_type_id,
		line_type_order,
		line_description,
		book_amount,
		book_rc,
		book_adj,
		m_beg_bal_py,
		m_activity_py,
		m_adjust_py,
		m_variance_py,
		m_m13_py,
		m_beg_bal_cy,
		m_activity_cy,
		m_adjust_cy,
		m_rta_cy,
		m_variance_cy,
		m_ltd_bal,
		m_unbooked_rta,
		m_def_tax_bal,
		m_def_tax_bal_reg,
		tax_m_rc,
		tax_def_rc,
		tax_m_adj
		)
		SELECT
		  a.max_id + rownum id,
		  G_TA_VERSION_ID_CY ta_version_id_cy,
		  G_TA_VERSION_ID_PY ta_version_id_py,
		  'N/A' gl_company,
		  G_GL_MONTH gl_month,
		  G_TBBS_MONTH tbbs_month,
		  col_close.line_item_id,
		  line_item.description line_item_description,
		  line_item.treatment_type_id,
		  line_type.line_type_id,
		  line_type.line_type_order,
		  'Column Closing Adjustment' line_description,
		  0 book_amount,
		  0 book_rc,
		  0 book_adj,
		  0 m_beg_bal_py,
		  0 m_activity_py,
		  0 m_adjust_py,
		0 m_variance_py,
		0 m_m13_py,
		  0 m_beg_bal_cy,
		  0 m_activity_cy,
		  0 m_adjust_cy,
		  0 m_rta_cy,
		0 m_variance_cy,
		  0 m_ltd_bal,
		  0 m_unbooked_rta,
		  0 m_def_tax_bal,
		  0 m_def_tax_bal_reg,
		0 tax_m_rc,
		0 tax_def_rc,
		0 tax_m_adj
		FROM
		  (select max(id) max_id from tbbs_report_stg_tmp) a
		CROSS JOIN
		(SELECT DISTINCT line_item_id FROM tbbs_column_close WHERE line_item_id IS NOT NULL) col_close
		JOIN tbbs_line_type line_type
		ON line_type.line_type_id = 3
		JOIN tbbs_line_item line_item
		ON col_close.line_item_id = line_item.line_item_id
		;

		-- Loop through the columns and merge in the totals
		FOR close_rec IN c1
		LOOP
		G_MSG := 'Closing Column: ' || to_char(close_rec.description);

		IF Lower(close_rec.column_name) IN ('m_beg_bal_py', 'm_activity_py', 'm_adjust_py') AND G_TA_VERSION_ID_PY IS NULL THEN CONTINUE;
		ELSIF close_rec.column_name = 'm_beg_bal_cy' AND G_TA_VERSION_ID_PY IS NOT NULL THEN CONTINUE;
		ELSE
		  v_sql := 'UPDATE tbbs_report_stg_tmp d set d.' || close_rec.column_name || ' = (select sum(' || close_rec.column_name || ')*-1 from tbbs_report_stg_tmp) where d.line_item_id = ' ||
			To_Char( close_rec.line_item_id) || ' and line_type_id = 3';

		  EXECUTE IMMEDIATE v_sql;
		END IF;
		END LOOP;

		-- After looping, update the m_beg_bal_cy and m_ltd_bal columns to sum the corresponding totals

		-- Only calc the CY Beg Balance if PY is not null
		IF G_TA_VERSION_ID_PY IS NOT NULL THEN
		G_MSG := 'Recalculating Current Year Beginning Balance for Closing Adjustment';
		v_sql := 'UPDATE tbbs_report_stg_tmp d set d.m_beg_bal_cy = d.m_beg_bal_py + d.m_activity_py + d.m_adjust_py + m_variance_py where line_type_id = 3';
		EXECUTE IMMEDIATE v_sql;
		END IF;

		-- Now Calc LTD End Balance
		G_MSG := 'Recalculating M-Item Life-to-Date Balance for Closing Adjustment';
		v_sql := 'UPDATE tbbs_report_stg_tmp d set d.m_ltd_bal = d.m_beg_bal_cy + d.m_activity_cy + d.m_adjust_cy + m_rta_cy + m_variance_cy where line_type_id = 3';
		EXECUTE IMMEDIATE v_sql;

		COMMIT;

		-- Error handling using PKG_PP_ERROR
		pkg_pp_error.remove_module_name;
	exception
		WHEN others THEN
			pkg_pp_error.raise_error('ERROR', g_msg);
			RAISE_APPLICATION_ERROR(-20000, G_MSG || ': ' || sqlerrm);
	END P_PROCESS_REPORT_STG_CLOSE;


--*******************************************************************************************************************************************************
--*******************************************************************************************************************************************************



	PROCEDURE P_PROCESS_REPORT_STG_CLEAN IS
	BEGIN
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_TBBS_REPORT.P_PROCESS_REPORT_STG_CLEAN');

		-- *********************************************************************************************************************
		-- Clear out Book / Tax rows with null records - this would apply to any line item that has only book accounts or only M items mapped. Also clear tax adjustment lines that do not have amounts.
		-- *********************************************************************************************************************
		G_MSG := 'Clearing Null records from the tbbs_report_stg_tmp staging table';
		-- Book
		DELETE FROM tbbs_report_stg_tmp
		WHERE line_type_id = 4
		AND book_account_id IS NULL;

		-- Tax
		DELETE FROM tbbs_report_stg_tmp
		WHERE line_type_id = 5
		AND m_item_id IS NULL;



		-- If selected, Delete $0 Amount rows
		G_MSG := 'Removing rows with all $0 Amounts';
		IF G_DELETE_EMPTY_AMOUNTS = 1 THEN
		DELETE FROM tbbs_report_stg_tmp
		WHERE coalesce(book_amount,0) = 0
		AND coalesce(book_rc,0) = 0
		AND coalesce(book_adj,0) = 0
		AND coalesce(m_beg_bal_py,0) = 0
		AND coalesce(m_activity_py,0) = 0
		AND coalesce(m_adjust_py,0) = 0
		AND coalesce(m_variance_py,0) = 0
		AND coalesce(m_m13_py,0) = 0
		AND coalesce(m_beg_bal_cy,0) = 0
		AND coalesce(m_activity_cy,0) = 0
		AND coalesce(m_adjust_cy,0) = 0
		AND coalesce(m_rta_cy,0) = 0
		AND coalesce(m_variance_cy,0) = 0
		AND coalesce(m_ltd_bal,0) = 0
		AND coalesce(m_unbooked_rta,0) = 0
		AND coalesce(m_def_tax_bal,0) = 0
		AND coalesce(m_def_tax_bal_reg,0) = 0
		AND coalesce(tax_m_rc,0) = 0
		AND coalesce(tax_def_rc,0) = 0
		AND coalesce(tax_m_adj,0) = 0
		;
		END IF;


		-- *********************************************************************************************************************
		-- Collapse detail for specified line items
		-- *********************************************************************************************************************
		G_MSG := 'Inserting the collapsed detail rows into tbbs_report_stg_tmp';
		INSERT INTO tbbs_report_stg_tmp
		(
		id,
		ta_version_id_cy,
		ta_version_id_py,
		gl_company,
		gl_month,
		tbbs_month,
		line_item_id,
		line_item_description,
		treatment_type_id,
		line_type_id,
		line_type_order,
		line_description,
		book_amount,
		book_rc,
		book_adj,
		m_beg_bal_py,
		m_activity_py,
		m_adjust_py,
		m_variance_py,
		m_m13_py,
		m_beg_bal_cy,
		m_activity_cy,
		m_adjust_cy,
		m_rta_cy,
		m_variance_cy,
		m_ltd_bal,
		m_unbooked_rta,
		m_def_tax_bal,
		m_def_tax_bal_reg,
		tax_m_rc,
		tax_def_rc,
		tax_m_adj
		)
		SELECT
			a.max_id + rownum id,
			G_TA_VERSION_ID_CY ta_version_id_cy,
			G_TA_VERSION_ID_PY ta_version_id_py,
			stg.gl_company,
			stg.gl_month,
			stg.tbbs_month,
			stg.line_item_id,
			stg.line_item_description,
			stg.treatment_type_id,
			line_type.line_type_id,
			line_type.line_type_order,
			'Collapsed Detail: ' || stg.line_item_description line_description,
			stg.book_amount,
			stg.book_rc,
			stg.book_adj,
			stg.m_beg_bal_py,
			stg.m_activity_py,
			stg.m_adjust_py,
		  stg.m_variance_py,
		  stg.m_m13_py,
			stg.m_beg_bal_cy,
			stg.m_activity_cy,
			stg.m_adjust_cy,
			stg.m_rta_cy,
		  stg.m_variance_cy,
			stg.m_ltd_bal,
			stg.m_unbooked_rta,
			stg.m_def_tax_bal,
			stg.m_def_tax_bal_reg,
		  stg.tax_m_rc,
		  stg.tax_def_rc,
		  stg.tax_m_adj
		FROM
			(select max(id) max_id from tbbs_report_stg_tmp) a
		JOIN tbbs_line_type line_type
		  ON line_type.line_type_id = 6
		CROSS JOIN
		(
			SELECT
				gl_company,
				gl_month,
				tbbs_month,
				line_item_id,
				line_item_description,
				treatment_type_id,
				sum(coalesce(book_amount,0)) book_amount,
				sum(coalesce(book_rc,0)) book_rc,
				sum(coalesce(book_adj,0)) book_adj,
				sum(coalesce(m_beg_bal_py,0)) m_beg_bal_py,
				sum(coalesce(m_activity_py,0)) m_activity_py,
				sum(coalesce(m_adjust_py,0)) m_adjust_py,
				sum(coalesce(m_variance_py,0)) m_variance_py,
				sum(coalesce(m_m13_py,0)) m_m13_py,
				sum(coalesce(m_beg_bal_cy,0)) m_beg_bal_cy,
				sum(coalesce(m_activity_cy,0)) m_activity_cy,
				sum(coalesce(m_adjust_cy,0)) m_adjust_cy,
				sum(coalesce(m_rta_cy,0)) m_rta_cy,
				sum(coalesce(m_variance_cy,0)) m_variance_cy,
				sum(coalesce(m_ltd_bal,0)) m_ltd_bal,
				sum(coalesce(m_unbooked_rta,0)) m_unbooked_rta,
				sum(coalesce(m_def_tax_bal,0)) m_def_tax_bal,
				sum(coalesce(m_def_tax_bal_reg,0)) m_def_tax_bal_reg,
				sum(coalesce(tax_m_rc,0)) tax_m_rc,
				sum(coalesce(tax_def_rc,0)) tax_def_rc,
				sum(coalesce(tax_m_adj,0)) tax_m_adj
			FROM tbbs_report_stg_tmp stg
			WHERE line_item_id in (select line_item_id from tbbs_line_item where collapse_detail_ind = 1)
		  AND line_type_id IN (4,5)
			GROUP BY
				gl_company,
				gl_month,
				tbbs_month,
				line_item_id,
				line_item_description,
				treatment_type_id
		) stg;

		G_MSG := 'Deleting the original detail rows from tbbs_report_stg_tmp';
		DELETE FROM tbbs_report_stg_tmp
		where line_item_id in (select line_item_id from tbbs_line_item where collapse_detail_ind = 1)
		  AND line_type_id IN (4,5);


		-- *********************************************************************************************************************
		-- Update the line description based on the line type. This is the description that will appear on the final report for each detail item.
		-- *********************************************************************************************************************
		G_MSG := 'Updating the line descriptions on the tbbs_report_stg_tmp staging table';
		UPDATE tbbs_report_stg_tmp
		SET line_description =
		Decode(line_type_id,
		4, SubStr(Trim(book_account_description || primary_account_description), 1, 100),
		5, SubStr(Trim(m_description), 1, 100),
		3, 'Column Closing Adjustment',
		6, 'Collapsed Detail: ' || line_item_description
		)
		WHERE line_type_id IN (3,4,5,6);

		COMMIT;

		-- Error handling using PKG_PP_ERROR
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			pkg_pp_error.raise_error('ERROR', g_msg);
			RAISE_APPLICATION_ERROR(-20000, G_MSG || ': ' || sqlerrm);
	END P_PROCESS_REPORT_STG_CLEAN;



--*******************************************************************************************************************************************************
--*******************************************************************************************************************************************************

	PROCEDURE P_PROCESS_REPORT_DATA IS
	BEGIN
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_TBBS_REPORT.P_PROCESS_REPORT_DATA');

		-- *********************************************************************************************************************
		-- Clear and insert report data from the staging table.
		-- Align it with the structure of the selected TBBS tree and order each line using the line_id.
		-- Also, sum the amounts to consolidate any separate GL_COMPANY records as the report will only run for one company / consolidation.
		-- Once again, cartesian joins based on the line_type_ind. This allows us to create each detail and summary row.
		-- Each Empty Node from the pp_tree will have it's own row (line_type_ind = 'TREE_NODE')
		-- The subquery from tbbs_report_data_stg unions with tbbs_line_item to ensure that there is a line item header (line_type_ind = 'LINE_ITEM') row to join to the tree
		-- 		even if the line_item had no data mapped to it and was deleted from the staging table.
		-- *********************************************************************************************************************
		G_MSG := 'Clearing the tbbs_report_data_tmp report table';
		DELETE FROM tbbs_report_data_tmp;

		G_MSG := 'Inserting base rows into the tbbs_report_data_tmp report table';
		INSERT INTO tbbs_report_data_tmp
		(
		line_id,
		parent_line_id,
		tree_order,
		line_type_order,
		gl_month,
		tbbs_month,
		line_type_id,
		line_type_ind,
		treatment_type_id,
		line_description,

		book_account_id,
		book_amount,
		book_rc,
		book_adj,
		book_elim,

		m_item_id,
		entity_id,

		m_beg_bal_py,
		m_activity_py,
		m_adjust_py,
		m_variance_py,
		m_beg_bal_cy,
		m_activity_cy,
		m_adjust_cy,
		m_rta_cy,
		m_variance_cy,
		m_ltd_bal,
		m_m13_py,
		m_unbooked_rta,
		tax_m_rc,
		tax_m_adj,
		tax_m_elim,

		m_def_tax_bal,
		m_def_tax_bal_reg,
		tax_def_rc,

		line_item_description,
		tree_level,
		empty_node,
		detail_ind,
		tree_parent_id,
		tree_child_id,
		tree_path
		)
		-- The connect by clause here based on the pp_tree parent/child relationships allow us to build the line_id / parent_line_id structure which is used by the TBBS report
		SELECT
		line_id,
		PRIOR line_id parent_line_id,
		tree_order,
		line_type_order,
		gl_month,
		tbbs_month,
		line_type_id,
		line_type_ind,
		treatment_type_id,
		SubStr(RPAD(' ', (LEVEL-1)*5, ' ') || line_description,1,100) line_description,

		book_account_id,
		book_amount,
		book_rc,
		book_adj,
		0 book_elim,

		m_item_id,
		entity_id,

		m_beg_bal_py,
		m_activity_py,
		m_adjust_py,
		m_variance_py,
		m_beg_bal_cy,
		m_activity_cy,
		m_adjust_cy,
		m_rta_cy,
		m_variance_cy,
		m_ltd_bal,
		m_m13_py,
		m_unbooked_rta,
		tax_m_rc,
		tax_m_adj,
		0 tax_m_elim,

		m_def_tax_bal,
		m_def_tax_bal_reg,
		tax_def_rc,

		line_item_description,
		LEVEL tree_level,
		empty_node,
		decode(line_type_id, 1, 0, 2, 0, 1) detail_ind,
		tree_parent_id,
		tree_child_id,
		SubStr(sys_connect_by_path (tree_parent_id, '|||'),1,200) tree_path

		FROM
		(
		SELECT
		Row_Number() OVER
		(
		ORDER BY
		tree.tree_order,
		coalesce(rep.line_type_order,0),
		rep.line_description
		) line_id,
		tree.empty_node,
		Decode(tree.empty_node,
		1, Decode(tree.child_id, tree.parent_id, NULL, tree.parent_id),
		Decode(line_type.line_type_id, 2, tree.parent_id, coalesce(rep.parent_id, tree.parent_id))
		) tree_parent_id,
		coalesce(rep.child_id, tree.child_id) tree_child_id,
		tree.tree_order,
		coalesce(line_type.line_type_order,0) line_type_order,

		G_GL_MONTH gl_month,
		G_TBBS_MONTH tbbs_month,

		line_type.line_type_id,
		coalesce(line_type.line_type_ind, 'NONE') line_type_ind,
		rep.treatment_type_id,
		Decode( tree.empty_node, 1, tree.node_desc, coalesce(rep.line_description, 'No Mapped Balances Found')) line_description,

		rep.book_account_id,
		rep.book_amount book_amount,
		rep.book_rc book_rc,
		rep.book_adj book_adj,

		rep.m_item_id,
		rep.entity_id,

		rep.m_beg_bal_py m_beg_bal_py,
		rep.m_activity_py m_activity_py,
		rep.m_adjust_py m_adjust_py,
		rep.m_variance_py m_variance_py,
		rep.m_m13_py m_m13_py,
		rep.m_beg_bal_cy m_beg_bal_cy,
		rep.m_activity_cy	m_activity_cy,
		rep.m_adjust_cy m_adjust_cy,
		rep.m_rta_cy m_rta_cy,
		rep.m_variance_cy m_variance_cy,
		rep.m_ltd_bal m_ltd_bal,
		rep.m_unbooked_rta m_unbooked_rta,
		rep.tax_m_rc tax_m_rc,
		rep.tax_m_adj tax_m_adj,

		rep.m_def_tax_bal m_def_tax_bal,
		rep.m_def_tax_bal_reg m_def_tax_bal_reg,
		rep.tax_def_rc tax_def_rc,

		rep.line_item_description

		FROM
		(
		SELECT
		a.pp_tree_type_id,
		a.empty_node,
		a.parent_id,
		a.child_id,
		Decode(a.empty_node,
		0, NULL,
		Decode(a.parent_id,
		a.child_id, a.override_parent_description,
		b.override_parent_description
		)
		) node_desc,
		a.sort_order tree_order,
		line_type.line_type_id
		FROM
		pp_tree_tbbs_tmp a
		JOIN tbbs_line_type line_type
		  ON (line_type.line_type_id = 1 AND a.empty_node = 1) OR (line_type.line_type_id = 2 and a.empty_node = 0)
		LEFT OUTER JOIN
		(
		SELECT distinct
		pp_tree_type_id,
		parent_id,
		override_parent_description
		FROM
		pp_tree_tbbs_tmp
		WHERE parent_id <> child_id
		) b
		ON b.pp_tree_type_id = a.pp_tree_type_id
		AND b.parent_id = a.child_id
		AND b.parent_id <> a.parent_id
		WHERE a.pp_tree_type_id = G_PP_TREE_TYPE_ID
		ORDER BY sort_order
		) tree
		LEFT JOIN
		(
		SELECT
		line_item_id parent_id,
		Decode(detail_id, 0, line_item_id, detail_id) child_id,
		line_type_id,
		line_type_order,
		line_description,
		treatment_type_id,
		book_account_id,
		book_amount,
		book_rc,
		book_adj,
		m_item_id,
		entity_id,
		m_beg_bal_py,
		m_activity_py,
		m_adjust_py,
		m_variance_py,
		m_m13_py,
		m_beg_bal_cy,
		m_activity_cy,
		m_adjust_cy,
		m_rta_cy,
		m_variance_cy,
		m_ltd_bal,
		m_unbooked_rta,
		tax_m_rc,
		tax_m_adj,
		m_def_tax_bal,
		m_def_tax_bal_reg,
		tax_def_rc,

		line_item_description

		FROM
		(
		-- Selects the detail data from the staging table.
		--The row_number is multiplied by -1 to ensure that we do not duplicate a parent/child relationship which would cause loops in the tree structure.
		SELECT
		-1 * Row_Number() OVER (ORDER BY stg.line_item_id, stg.line_type_order) detail_id,
		stg.line_item_id,
		stg.line_item_description,
		stg.treatment_type_id,
		stg.line_type_id,
		stg.line_type_order,
		stg.line_description,

		stg.book_account_id,
		sum(coalesce(stg.book_amount,0)) book_amount,
		sum(coalesce(stg.book_rc,0)) book_rc,
		sum(coalesce(stg.book_adj,0)) book_adj,
		stg.m_item_id,
		stg.entity_id,
		sum(coalesce(stg.m_beg_bal_py,0)) m_beg_bal_py,
		sum(coalesce(stg.m_activity_py,0)) m_activity_py,
		sum(coalesce(stg.m_adjust_py,0)) m_adjust_py,
		sum(coalesce(stg.m_variance_py,0)) m_variance_py,
		sum(coalesce(stg.m_m13_py,0)) m_m13_py,
		sum(coalesce(stg.m_beg_bal_cy,0)) m_beg_bal_cy,
		sum(coalesce(stg.m_activity_cy,0)) m_activity_cy,
		sum(coalesce(stg.m_adjust_cy,0)) m_adjust_cy,
		sum(coalesce(stg.m_rta_cy,0)) m_rta_cy,
		sum(coalesce(stg.m_variance_cy,0)) m_variance_cy,
		sum(coalesce(stg.m_ltd_bal,0)) m_ltd_bal,
		sum(coalesce(stg.m_unbooked_rta,0)) m_unbooked_rta,
		sum(coalesce(stg.tax_m_rc,0)) tax_m_rc,
		sum(coalesce(stg.tax_m_adj,0)) tax_m_adj,
		sum(coalesce(stg.m_def_tax_bal,0)) m_def_tax_bal,
		sum(coalesce(stg.m_def_tax_bal_reg,0)) m_def_tax_bal_reg,
		sum(coalesce(stg.tax_def_rc,0)) tax_def_rc
		FROM
		tbbs_report_stg_tmp stg
		GROUP BY
		stg.line_item_id,
		stg.line_item_description,
		stg.treatment_type_id,
		stg.line_type_id,
		stg.line_type_order,
		stg.line_description,
		stg.book_account_id,
		stg.m_item_id,
		stg.entity_id

		UNION ALL

		-- Unions in the distinct line_item_ids to ensure we have a line item header record even if it has no detail data.
		SELECT DISTINCT
		0 detail_id,
		line_item.line_item_id,
		line_item.description line_item_description,
		line_item.treatment_type_id,
		line_type.line_type_id,
		line_type.line_type_order,
		line_item.description line_description,
		NULL book_account_id,
		0 book_amount,
		0 book_rc,
		0 book_adj,
		NULL m_item_id,
		NULL entity_id,
		0 m_beg_bal_py,
		0 m_activity_py,
		0 m_adjust_py,
		0 m_variance_py,
		0 m_m13_py,
		0 m_beg_bal_cy,
		0 m_activity_cy,
		0 m_adjust_cy,
		0 m_rta_cy,
		0 m_variance_cy,
		0 m_ltd_bal,
		0 m_unbooked_rta,
		0 tax_m_rc,
		0 tax_m_adj,
		0 m_def_tax_bal,
		0 m_def_tax_bal_reg,
		0 tax_def_rc
		FROM tbbs_line_item line_item
		JOIN tbbs_line_type line_type
		  ON line_type.line_type_id = 2
		)
		ORDER BY
		line_item_id,
		line_type_order
		) rep
		ON rep.parent_id = tree.child_id
		JOIN tbbs_line_type line_type
		  ON line_type.line_type_id = coalesce(rep.line_type_id, tree.line_type_id)
		)
		START WITH tree_parent_id IS null
		CONNECT BY PRIOR tree_child_id = tree_parent_id
		ORDER BY line_id
		;

		-- Delete any rows that have a nonexistent line item mapping
		delete from tbbs_report_data_tmp where line_type_ind = 'NONE';



		COMMIT;

		-- Error handling using PKG_PP_ERROR
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			pkg_pp_error.raise_error('ERROR', g_msg);
			RAISE_APPLICATION_ERROR(-20000, G_MSG || ': ' || sqlerrm);
	END P_PROCESS_REPORT_DATA;





--*******************************************************************************************************************************************************
--*******************************************************************************************************************************************************


	PROCEDURE P_PROCESS_REPORT_CALC IS
		v_treatment_sql VARCHAR2(4000);
		n_loc           NUMBER;
		CURSOR c1 IS
			SELECT treatment_type_id, treatment_sql
			FROM tbbs_treatment_type
			WHERE treatment_sql IS NOT NULL;
	BEGIN

		PKG_PP_ERROR.SET_MODULE_NAME('PKG_TBBS_REPORT.P_PROCESS_REPORT_CALC');

		-- *********************************************************************************************************************
		-- Process the treatment types
		-- *********************************************************************************************************************

		-- Treatment type update logic is stored in the tbbs_treatment_type table. Loop through the logic in a cursor.
		-- If the SQL is blank, continue.
		-- Statements in table must begin with SET. We will preset the statement with UPDATE tbbs_report_data_tmp.

		FOR treatment_rec IN c1
		LOOP
			n_loc := 0;
			n_loc := InStr(treatment_rec.treatment_sql, ';');
			IF Nvl(n_loc,0) = 0 THEN
			v_treatment_sql := 'UPDATE tbbs_report_data_tmp ' || treatment_rec.treatment_sql || ' WHERE treatment_type_id = ' || to_char(treatment_rec.treatment_type_id);
			ELSE
			v_treatment_sql := 'UPDATE tbbs_report_data_tmp ' || SubStr(treatment_rec.treatment_sql,1,n_loc-1) || ' WHERE treatment_type_id = ' || to_char(treatment_rec.treatment_type_id);
			END IF;

			G_MSG := 'Processing Treatment Type ID: ' || to_char(treatment_rec.treatment_type_id);
			EXECUTE IMMEDIATE v_treatment_sql;
		END LOOP;


		-- *********************************************************************************************************************
		-- Calculate the adjusted book basis and adjusted timing diff and Tax Basis. Do another double check for null values.
		-- *********************************************************************************************************************

		-- **********
		-- Adjusted Book Basis
		-- **********
		G_MSG := 'Calculating Adjusted Book Basis';
		EXECUTE IMMEDIATE 'UPDATE tbbs_report_data_tmp SET adjusted_book_basis = coalesce(book_amount,0) + coalesce(book_rc,0) + coalesce(book_adj,0) + coalesce(book_elim,0)';


		-- **********
		--Adjusted Timing Difference
		-- **********
		G_MSG := 'Determining Timing Difference adjustment columns and calculating Adjusted Timing Difference';
		G_COLUMNS := 'coalesce(m_ltd_bal,0) + coalesce(tax_m_adj,0) + coalesce(tax_m_rc,0) + coalesce(tax_m_elim,0)';
		-- Add in the Unbooked RTA option if that is chosen
		IF G_INC_UNBOOKED_RTA = 1 THEN
		G_COLUMNS := G_COLUMNS || ' + coalesce(m_unbooked_rta,0)';
		END IF;
		-- Update
		EXECUTE IMMEDIATE 'UPDATE tbbs_report_data_tmp SET adjusted_m_ltd_bal = ' || G_COLUMNS;


		-- **********
		--Adjusted Deferred Tax Balance
		-- **********
		EXECUTE IMMEDIATE 'UPDATE tbbs_report_data_tmp SET adjusted_def_tax_tot = coalesce(m_def_tax_bal,0) + coalesce(m_def_tax_bal_reg,0) + coalesce(tax_def_rc,0)';


		-- *********************************************************************************************************************
		--	-- Calculate Tax Basis
		-- *********************************************************************************************************************
		G_MSG := 'Calculating Tax Basis';
		EXECUTE IMMEDIATE 'UPDATE tbbs_report_data_tmp SET calc_tax_basis = adjusted_book_basis + adjusted_m_ltd_bal';


		COMMIT;

		-- Error handling using PKG_PP_ERROR
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			pkg_pp_error.raise_error('ERROR', g_msg);
			RAISE_APPLICATION_ERROR(-20000, G_MSG || ': ' || sqlerrm);
	END P_PROCESS_REPORT_CALC;

--*******************************************************************************************************************************************************
--*******************************************************************************************************************************************************

	PROCEDURE P_PROCESS_REPORT_SUMMARIZE IS
	BEGIN
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_TBBS_REPORT.P_PROCESS_REPORT_SUMMARIZE');


		-- *********************************************************************************************************************
		-- Insert a grand total line. Sum the data from the tbbs_report_data_tmp table only using the line item detail records (detail_ind = 1)
		-- *********************************************************************************************************************
		G_MSG := 'Inserting Grand Total line into the tbbs_report_data_tmp report table';
		INSERT INTO tbbs_report_data_tmp
		(
		line_id,
		parent_line_id,
		tree_order,
		line_type_id,
		line_type_order,
		line_type_ind,
		line_description,

		tree_level,
		empty_node,

		book_amount,
		book_rc,
		book_adj,
		book_elim,
		adjusted_book_basis,
		m_beg_bal_py,
		m_activity_py,
		m_adjust_py,
		m_variance_py, -- ***
		m_beg_bal_cy,
		m_activity_cy,
		m_adjust_cy,
		m_rta_cy,
		m_variance_cy, -- ***
		m_ltd_bal,
		m_m13_py,  -- ***
		m_unbooked_rta,
		tax_m_rc,
		tax_m_adj,
		tax_m_elim, -- ***
		adjusted_m_ltd_bal,
		calc_tax_basis,
		m_def_tax_bal,
		m_def_tax_bal_reg,
		tax_def_rc,
		adjusted_def_tax_tot
		)

		SELECT
		a.max_id + 1 line_id,
		NULL parent_line_id,
		b.max_id + 1 tree_order,
		line_type.line_type_id,
		line_type.line_type_order,
		line_type.line_type_ind,
		'Grand Total:' line_description,

		1 tree_level,
		1 empty_node,

		c.book_amount,
		c.book_rc,
		c.book_adj,
		c.book_elim,
		c.adjusted_book_basis,
		c.m_beg_bal_py,
		c.m_activity_py,
		c.m_adjust_py,
		c.m_variance_py,
		c.m_beg_bal_cy,
		c.m_activity_cy,
		c.m_adjust_cy,
		c.m_rta_cy,
		c.m_variance_cy,
		c.m_ltd_bal,
		c.m_m13_py,
		c.m_unbooked_rta,
		c.tax_m_rc,
		c.tax_m_adj,
		c.tax_m_elim,
		c.adjusted_m_ltd_bal,
		c.calc_tax_basis,
		c.m_def_tax_bal,
		c.m_def_tax_bal_reg,
		c.tax_def_rc,
		c.adjusted_def_tax_tot
		FROM
		(SELECT Max(line_id) max_id FROM tbbs_report_data_tmp) a
		CROSS JOIN
		(SELECT Max(tree_order) max_id FROM tbbs_report_data_tmp) b
		JOIN tbbs_line_type line_type
		  ON line_type.line_type_id = 7
		CROSS JOIN
		(
		SELECT
		sum(coalesce(book_amount,0)) book_amount,
		sum(coalesce(book_rc,0)) book_rc,
		sum(coalesce(book_adj,0)) book_adj,
		sum(coalesce(book_elim,0)) book_elim,
		sum(coalesce(adjusted_book_basis,0)) adjusted_book_basis,
		sum(coalesce(m_beg_bal_py,0)) m_beg_bal_py,
		sum(coalesce(m_activity_py,0)) m_activity_py,
		sum(coalesce(m_adjust_py,0)) m_adjust_py,
		sum(coalesce(m_variance_py,0)) m_variance_py,
		sum(coalesce(m_beg_bal_cy,0)) m_beg_bal_cy,
		sum(coalesce(m_activity_cy,0)) m_activity_cy,
		sum(coalesce(m_adjust_cy,0)) m_adjust_cy,
		sum(coalesce(m_rta_cy,0)) m_rta_cy,
		sum(coalesce(m_variance_cy,0)) m_variance_cy,
		sum(coalesce(m_ltd_bal,0)) m_ltd_bal,
		sum(coalesce(m_m13_py,0)) m_m13_py,
		sum(coalesce(m_unbooked_rta,0)) m_unbooked_rta,
		sum(coalesce(tax_m_rc,0)) tax_m_rc,
		sum(coalesce(tax_m_adj,0)) tax_m_adj,
		sum(coalesce(tax_m_elim,0)) tax_m_elim,
		sum(coalesce(adjusted_m_ltd_bal,0)) adjusted_m_ltd_bal,
		sum(coalesce(calc_tax_basis,0)) calc_tax_basis,
		sum(coalesce(m_def_tax_bal,0)) m_def_tax_bal,
		sum(coalesce(m_def_tax_bal_reg,0)) m_def_tax_bal_reg,
		sum(coalesce(tax_def_rc,0)) tax_def_rc,
		sum(coalesce(adjusted_def_tax_tot,0)) adjusted_def_tax_tot
		FROM
		tbbs_report_data_tmp
		WHERE empty_node = 0
		) c
		;


		-- *********************************************************************************************************************
		-- Summarize the Line Item Headers and Tree Nodes based on the parent/child structure of line_id and parent_line_id
		-- This uses several scalar subqueries, but as the data volume in this table should never be astronomical it shouldnt cause a big performance impact.
		-- *********************************************************************************************************************

		G_MSG := 'Summarizing the Line Item Header and Tree Node lines on the tbbs_report_data_tmp report table';
		MERGE INTO tbbs_report_data_tmp d
		USING
		(
		SELECT
		line_id,
		( Select sum(book_amount) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) book_amount,
		( Select sum(book_rc) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) book_rc,
		( Select sum(book_adj) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) book_adj,
		( Select sum(book_elim) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) book_elim,
		( Select sum(adjusted_book_basis) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) adjusted_book_basis,
		( Select sum(m_beg_bal_py) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_beg_bal_py,
		( Select sum(m_activity_py) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_activity_py,
		( Select sum(m_adjust_py) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_adjust_py,
		( Select sum(m_variance_py) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_variance_py,
		( Select sum(m_beg_bal_cy) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_beg_bal_cy,
		( Select sum(m_activity_cy) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_activity_cy,
		( Select sum(m_adjust_cy) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_adjust_cy,
		( Select sum(m_rta_cy) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_rta_cy,
		( Select sum(m_variance_cy) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_variance_cy,
		( Select sum(m_ltd_bal) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_ltd_bal,
		( Select sum(m_m13_py) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_m13_py,
		( Select sum(m_unbooked_rta) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_unbooked_rta,
		( Select sum(tax_m_rc) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) tax_m_rc,
		( Select sum(tax_m_adj) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) tax_m_adj,
		( Select sum(tax_m_elim) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) tax_m_elim,
		( Select sum(adjusted_m_ltd_bal) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) adjusted_m_ltd_bal,
		( Select sum(calc_tax_basis) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) calc_tax_basis,
		( Select sum(m_def_tax_bal) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_def_tax_bal,
		( Select sum(m_def_tax_bal_reg) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) m_def_tax_bal_reg,
		( Select sum(tax_def_rc) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) tax_def_rc,
		( Select sum(adjusted_def_tax_tot) FROM tbbs_report_data_tmp t2 START WITH t2.line_id = t1.line_id CONNECT BY PRIOR t2.line_id = t2.parent_line_id ) adjusted_def_tax_tot
		FROM tbbs_report_data_tmp t1
		WHERE t1.tree_child_id > 0
		START WITH parent_line_id IS null
		CONNECT BY PRIOR line_id = parent_line_id
		) s ON (s.line_id = d.line_id)
		WHEN MATCHED THEN UPDATE SET
		d.book_amount = s.book_amount,
		d.book_rc = s.book_rc,
		d.book_adj = s.book_adj,
		d.book_elim = s.book_elim,
		d.adjusted_book_basis = s.adjusted_book_basis,
		d.m_beg_bal_py = s.m_beg_bal_py,
		d.m_activity_py = s.m_activity_py,
		d.m_adjust_py = s.m_adjust_py,
		d.m_variance_py = s.m_variance_py,
		d.m_beg_bal_cy = s.m_beg_bal_cy,
		d.m_activity_cy = s.m_activity_cy,
		d.m_adjust_cy = s.m_adjust_cy,
		d.m_rta_cy = s.m_rta_cy,
		d.m_variance_cy = s.m_variance_cy,
		d.m_ltd_bal = s.m_ltd_bal,
		d.m_m13_py = s.m_m13_py,
		d.m_unbooked_rta = s.m_unbooked_rta,
		d.tax_m_rc = s.tax_m_rc,
		d.tax_m_adj = s.tax_m_adj,
		d.tax_m_elim = s.tax_m_elim,
		d.adjusted_m_ltd_bal = s.adjusted_m_ltd_bal,
		d.calc_tax_basis = s.calc_tax_basis,
		d.m_def_tax_bal = s.m_def_tax_bal,
		d.m_def_tax_bal_reg = s.m_def_tax_bal_reg,
		d.tax_def_rc = s.tax_def_rc,
		d.adjusted_def_tax_tot = s.adjusted_def_tax_tot
		;


		-- Calculate the deferred tax rate
		UPDATE tbbs_report_data_tmp SET calc_def_rate = Decode(adjusted_m_ltd_bal, 0, 0, adjusted_def_tax_tot / adjusted_m_ltd_bal) ;

		COMMIT;

		-- Error handling using PKG_PP_ERROR
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			pkg_pp_error.raise_error('ERROR', g_msg);
			RAISE_APPLICATION_ERROR(-20000, G_MSG || ': ' || sqlerrm);
	END P_PROCESS_REPORT_SUMMARIZE;

--*******************************************************************************************************************************************************
--*******************************************************************************************************************************************************

END PKG_TBBS_REPORT;
/

--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (3401, 0, 2016, 1, 1, 0, 0, 'C:\PlasticWKS\POWERPLANT_MASTER_REP\qa-2016.1.1\sql\packages', '2016.1.1.0_PKG_TBBS_REPORT.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
