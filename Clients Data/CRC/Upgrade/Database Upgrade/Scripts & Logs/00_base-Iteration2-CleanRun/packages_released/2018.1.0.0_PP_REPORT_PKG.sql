create or replace package PP_REPORT_PKG as
   G_PKG_VERSION varchar(35) := '2018.1.0.0';
   function PP_REPORT_START_MONTH_NUMBER return number;
   function PP_REPORT_END_MONTH_NUMBER return number;
   function PP_REPORT_START_MONTH return date;
   function PP_REPORT_END_MONTH return date;
end PP_REPORT_PKG;
/

create or replace package body PP_REPORT_PKG as
   --**************************************************************************
   --                      PP_REPORT_START_MONTH_NUMBER
   --**************************************************************************
   function PP_REPORT_START_MONTH_NUMBER return number is
      START_MONTH number;

   begin
      select TO_NUMBER(TO_CHAR(START_MONTH, 'yyyymm'))
        into START_MONTH
        from REPORT_TIME
       where SESSION_ID = USERENV('sessionid')
         and LOWER(USER_ID) = LOWER(user);

      return(START_MONTH);
   end;

   --**************************************************************************
   --                     PP_REPORT_END_MONTH_NUMBER
   --**************************************************************************
   function PP_REPORT_END_MONTH_NUMBER return number is
      END_MONTH number;

   begin
      select TO_NUMBER(TO_CHAR(END_MONTH, 'yyyymm'))
        into END_MONTH
        from REPORT_TIME
       where SESSION_ID = USERENV('sessionid')
         and LOWER(USER_ID) = LOWER(user);

      return(END_MONTH);
   end;

   --**************************************************************************
   --                      PP_REPORT_START_MONTH
   --**************************************************************************
   function PP_REPORT_START_MONTH return date is
      START_MONTH date;

   begin
      select START_MONTH
        into START_MONTH
        from REPORT_TIME
       where SESSION_ID = USERENV('sessionid')
         and LOWER(USER_ID) = LOWER(user);

      return(START_MONTH);
   end;

   --**************************************************************************
   --                     PP_REPORT_END_MONTH
   --**************************************************************************
   function PP_REPORT_END_MONTH return date is
      END_MONTH date;

   begin
      select END_MONTH
        into END_MONTH
        from REPORT_TIME
       where SESSION_ID = USERENV('sessionid')
         and LOWER(USER_ID) = LOWER(user);

      return(END_MONTH);
   end;

end PP_REPORT_PKG;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (11695, 0, 2018, 1, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2018.1.0.0_PP_REPORT_PKG.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
