CREATE OR REPLACE package pkg_lease_asset_post as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_ASSET_POST
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 2015.1   11/07/2014 Anand Rajashekar  PP-41098
   || 2015.2   10/12/2015 Andrew Scott   Maint-45003. Prevent error when xfering unmatched sobs.
   || 2016.1   03/11/2016 S. Byers       Maint-45567: Add company_set_of_books when checking for second book
   || 2017.1   07/14/2017 Sisouphanh     Maint-48238: MultCurr - Enhance Asset In Service to Translate Amounts
   ||                                                 into Company Currency
   || 2017.1   07/26/2017 Charlie Shilling	maint-48366: update retire function to work with multicurrency
   || 2017.1   07/28/2017 Anand R  	     maint-48364: update tranfer function to work with multicurrency
   ||============================================================================
   */
   G_SEND_JES boolean NOT NULL := true;

   procedure P_SET_ILR_ID(A_ILR_ID number);

   procedure P_GETTAXES(A_ASSET_IDS in PKG_LEASE_CALC.NUM_ARRAY);

   -- Function to perform specific Leased Asset addition
   function F_ADD_ASSET(A_LS_ASSET_ID number) return varchar2;

   -- Function to perform specific Leased Asset retirements
   function F_RETIRE_ASSET(A_CPR_ASSET_ID        number,
                           A_ACCT_MONTH          date,
                           A_MONTH               date,
                           A_RETIREMENT_AMOUNT   number,
                           A_RETIREMENT_QUANTITY number,
                           A_GAIN_LOSS_AMOUNT    number,
                           A_DG_ID               number,
                           A_PT_ID               number,
                           A_MSG                 out varchar2) return number;

   -- Function to perform specific Leased Asset transfer from
   function F_TRANSFER_ASSET_FROM(A_CPR_ASSET_ID number,
                                  A_AMOUNT       number,
                                  A_QUANTITY     number,
                                  A_PT_ID        number,
                                  A_MSG          out varchar2) return number;

   -- Function to perform specific Leased Asset transfer to
   -- takes in the asset attributes to handle a transfer.
   -- out parameter for message.  Returns a number
   -- 1 for success, 0 for failure
   function F_TRANSFER_ASSET_TO(A_CPR_ASSET_ID       number,
                                A_AMOUNT             number,
                                A_QUANTITY           number,
                                A_COMPANY_ID         number,
                                A_BUS_SEGMENT_ID     number,
                                A_UTILITY_ACCOUNT_ID number,
                                A_SUB_ACCOUNT_ID     number,
                                A_FUNC_CLASS_ID      number,
                                A_PROPERTY_GROUP_ID  number,
                                A_RETIREMENT_UNIT_ID number,
                                A_ASSET_LOCATION_ID  number,
                                A_DG_ID              number,
                                A_PT_ID              number,
                                A_MSG                out varchar2) return number;

  -- Function to delete assets and all dependant tables
  function F_DELETE_ASSETS(A_ASSET_IDS in PKG_LEASE_CALC.NUM_ARRAY) return varchar2;

   function F_GET_ILR_ID return number;
end PKG_LEASE_ASSET_POST;
/

CREATE OR REPLACE package body pkg_lease_asset_post as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_ASSET_POST
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 2015.1   11/07/2014 Anand Rajashekar  PP-41098
   || 2015.2   10/12/2015 Andrew Scott   Maint-45003. Prevent error when xfering unmatched sobs.
   || 2016.1   03/11/2016 S. Byers       Maint-45567: Add company_set_of_books when checking for second book
   ||============================================================================
   */

   L_ILR_ID number;

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   --**************************************************************************
   --                            SET_ILR_ID
   --**************************************************************************

   procedure P_SET_ILR_ID(A_ILR_ID number) is
   begin
      L_ILR_ID := A_ILR_ID;
   end P_SET_ILR_ID;

   --**************************************************************************
   --                            GETTAXES
   --**************************************************************************

procedure P_GETTAXES(A_ASSET_IDS in PKG_LEASE_CALC.NUM_ARRAY) is
      ENABLED_LOCAL_TAXES PKG_LEASE_CALC.NUM_ARRAY;
      counter number;
   begin

      /* WMD We need to change the way this is working. Initiated assets are getting all tax types assigned to them */
      for I in 1 .. A_ASSET_IDS.COUNT
      loop

        /* This is ridiculous but we have to do this */
        insert into ls_asset_tax_map
        (ls_asset_id, tax_local_id, status_code_id)
        select
        A_ASSET_IDS(I),
        tl.tax_local_id,
        0
        from ls_tax_local tl
        where not exists
        (select 1 from ls_asset_tax_map z where z.ls_asset_id = A_ASSET_IDS(I) and z.tax_local_id = tl.tax_local_id);

        select count(1)
        into counter
        from ls_asset_tax_map
        where ls_asset_id = A_ASSET_IDS(I)
          and status_code_id = 1;

        if counter = 0 then
          goto endloop;
        end if;


         --create an array of local taxes that are disabled under this asset
         select TAX_LOCAL_ID
         bulk collect into ENABLED_LOCAL_TAXES
         from LS_ASSET_TAX_MAP
         where STATUS_CODE_ID = 1
           and LS_ASSET_ID = A_ASSET_IDS(I);

         --delete old records from ls_asset_tax_map
         delete from LS_ASSET_TAX_MAP where LS_ASSET_ID = A_ASSET_IDS(I);

         --insert new records into ls_asset_tax_map
         insert into LS_ASSET_TAX_MAP
            (LS_ASSET_ID, TAX_LOCAL_ID, STATUS_CODE_ID)
            (select A_ASSET_IDS(I) LS_ASSET_ID, TL.TAX_LOCAL_ID TAX_LOCAL_ID, 0 STATUS_CODE_ID
               from LS_ASSET A, LS_TAX_LOCAL TL
              where A_ASSET_IDS(I) = A.LS_ASSET_ID
                and A.TAX_SUMMARY_ID = TL.TAX_SUMMARY_ID);

         --the insert statement defaults all local taxes to enabled, so disable the ones that need to be
         for J in 1 .. ENABLED_LOCAL_TAXES.COUNT
         loop
            update LS_ASSET_TAX_MAP
               set STATUS_CODE_ID = 1
             where LS_ASSET_ID = A_ASSET_IDS(I)
               and TAX_LOCAL_ID = ENABLED_LOCAL_TAXES(J);
         end loop;

      <<endloop>>
      null;
      end loop;
   end P_GETTAXES;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
  function F_FOOT_DEPR(A_CPR_ASSET_ID IN NUMBER, A_MONTH IN DATE) return varchar2 is
  L_STATUS varchar2(5000);
  begin

  L_STATUS:='Footing CPR depr end balances';
  update cpr_depr
      set depr_reserve = nvl(beg_reserve_month,0) + nvl(retirements,0) + nvl(depr_exp_alloc_adjust,0) + nvl(depr_exp_adjust,0) + nvl(salvage_dollars,0) +
      nvl(cost_of_removal,0) + nvl(other_credits_and_adjust,0) + nvl(gain_loss,0) + nvl(reserve_trans_in,0) +
      nvl(reserve_trans_out,0) + nvl(curr_depr_expense,0) + nvl(reserve_adjustment,0) +nvl(impairment_asset_amount,0) +
      nvl(impairment_expense_amount,0) ,
      asset_dollars = nvl(beg_asset_dollars,0) + nvl(net_adds_and_adjust,0) + nvl(retirements,0) + nvl(transfers_in,0) + nvl(transfers_out,0)
                + nvl(impairment_asset_amount,0)
      where gl_posting_mo_yr = A_MONTH
      and asset_id = A_CPR_ASSET_ID;

   L_STATUS:='Footing Depr Ledger end Balances';
      update depr_ledger
    set end_reserve = begin_reserve +  reserve_retirements +  depreciation_expense  + depr_exp_adjust
               +  depr_exp_alloc_adjust +  cost_of_removal +   salvage_returns +  salvage_cash
               +  reserve_credits +  reserve_adjustments +  reserve_tran_in +  reserve_tran_out
               +  gain_loss -  current_net_salvage_amort + impairment_asset_amount + impairment_expense_amount
    , end_balance =  begin_balance +  additions +  retirements +  transfers_in +  transfers_out +  adjustments + impairment_asset_amount
    where gl_post_mo_yr = A_MONTH
    and (end_reserve <> begin_reserve +  reserve_retirements +  depreciation_expense  + depr_exp_adjust
               +  depr_exp_alloc_adjust +  cost_of_removal +   salvage_returns +  salvage_cash
               +  reserve_credits +  reserve_adjustments +  reserve_tran_in +  reserve_tran_out
               +  gain_loss -  current_net_salvage_amort  +  impairment_asset_amount + impairment_expense_amount
       or end_balance <>  begin_balance +  additions +  retirements +  transfers_in +  transfers_out +  adjustments + impairment_asset_amount
       )
    AND depr_group_id = (select depr_group_id from cpr_ledger where asset_id = A_CPR_ASSET_ID);

    RETURN 'OK';

   EXCEPTION WHEN OTHERS THEN
    RETURN SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
  END F_FOOT_DEPR;
   -- Function to perform specific Leased Asset Adjustment
   function F_ADJUST_ASSET(A_LS_ASSET   in LS_ASSET%rowtype,
                           A_REVISION   in number,
                           A_SOB        in number,
                           A_OBL_ADJ    out number,
                           A_LT_OBL_ADJ out number,
                           A_DEF_ADJ	out number,
                           A_LT_DEF_ADJ	out number,
                           A_ASSET_ID   out number,
               A_MONTH      in date:=null) return varchar2 is
      L_MSG                     varchar2(2000);
      L_ASSET_SCHEDULE_MONTH    LS_ASSET_SCHEDULE%rowtype;
      L_ASSET_SCHEDULE_APPROVED LS_ASSET_SCHEDULE%rowtype;
      L_MONTH                   date;
      L_TRANSFER_CHECK          number;
      L_COUNT number;
   begin

    pkg_pp_log.p_write_message('ADJUSTING ASSET');


    L_MSG := 'Getting the current open month for lessee';
      select greatest(min(LPC.GL_POSTING_MO_YR),min(LAS.MONTH))
        into L_MONTH
        from LS_PROCESS_CONTROL LPC, LS_ASSET_SCHEDULE LAS
       where LPC.LAM_CLOSED is null
         and LPC.COMPANY_ID = A_LS_ASSET.COMPANY_ID
     and LAS.LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID
     and LAS.REVISION = A_REVISION;

     /* WMD */
     IF A_MONTH IS NOT NULL THEN
      L_MONTH:=A_MONTH;
     end if;

      L_MSG := 'Starting to retrieve leased asset schedule for the month';
      --get the asset_schedule for the first month
      select AA.*
        into L_ASSET_SCHEDULE_MONTH
        from LS_ASSET_SCHEDULE AA
       where AA.LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID
         and AA.REVISION = A_REVISION
         and AA.SET_OF_BOOKS_ID = A_SOB
         and aa."MONTH" = L_MONTH;

      select count(1)
      into L_TRANSFER_CHECK
      from ls_asset_transfer_history
      where to_ls_asset_id = A_LS_ASSET.LS_ASSET_ID
        and month = L_MONTH;

      if L_TRANSFER_CHECK = 1 THEN
      L_MSG := 'Starting to retrieve leased asset schedule for the transferred revision';
      --get the asset_schedule for the first month
      select AA.*
        into L_ASSET_SCHEDULE_APPROVED
        from LS_ASSET_SCHEDULE AA
       where AA.LS_ASSET_ID = (select from_ls_asset_id from ls_asset_transfer_history where month = L_MONTH and to_ls_asset_id = A_LS_ASSET.LS_ASSET_ID)
         and AA.REVISION = (select approved_revision from ls_asset where ls_asset_id =
                            (select from_ls_asset_id from ls_asset_transfer_history where month = L_MONTH and to_ls_asset_id = A_LS_ASSET.LS_ASSET_ID))
         and AA.SET_OF_BOOKS_ID = A_SOB
         and aa."MONTH" = L_MONTH;
      ELSE

      L_MSG := 'Starting to retrieve leased asset schedule for the approved revision';
      --get the asset_schedule for the first month
      select count(1)
      into l_count
      from LS_ASSET_SCHEDULE AA
       where AA.LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID
         and AA.REVISION = A_LS_ASSET.APPROVED_REVISION
         and AA.SET_OF_BOOKS_ID = A_SOB
         and aa."MONTH" = L_MONTH;

      if L_COUNT = 0 then
          PKG_PP_LOG.P_WRITE_MESSAGE('No row found for ' || to_char(l_month, 'yyyymm') || ' for this asset. No adjustment will be made');
          return 'OK';
      end if;
      select AA.*
        into L_ASSET_SCHEDULE_APPROVED
        from LS_ASSET_SCHEDULE AA
       where AA.LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID
         and AA.REVISION = A_LS_ASSET.APPROVED_REVISION
         and AA.SET_OF_BOOKS_ID = A_SOB
         and aa."MONTH" = L_MONTH;

       end if ;

     /* CJS 2/16/15 Changing ST obligation amount */
      A_OBL_ADJ    := case when L_ASSET_SCHEDULE_MONTH.IS_OM = 1 then 0 else (L_ASSET_SCHEDULE_MONTH.BEG_OBLIGATION - L_ASSET_SCHEDULE_MONTH.BEG_LT_OBLIGATION) end -
                      (case when L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 then 0 else  L_ASSET_SCHEDULE_APPROVED.BEG_OBLIGATION - L_ASSET_SCHEDULE_APPROVED.BEG_LT_OBLIGATION end);

      pkg_pp_log.p_write_message('Short term obligation amount: ' || to_char(a_obl_adj));

      A_LT_OBL_ADJ := case when L_ASSET_SCHEDULE_MONTH.IS_OM = 1 then 0 else L_ASSET_SCHEDULE_MONTH.BEG_LT_OBLIGATION end -
                      case when L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 then 0 else L_ASSET_SCHEDULE_APPROVED.BEG_LT_OBLIGATION END;

      pkg_pp_log.p_write_message('Long term obligation amount: ' || to_char(a_lt_obl_adj));

      -- go ahead and break out the use cases for both sides
      -- for Off Balance Sheet to Off Balance Sheet, just want the true difference between deferred rent
      -- for Off-BS to On-BS, we want to back out the deferred rent from the approved revision
      -- for On-BS to Off-BS, we want to true-up just the new revision deferred rent
      -- for On-BS to On-BS, we just want to pull whatever is there
      -- also to note, for On-BS, this is used for 3057/3058 trans types, for Off-BS, its used in 3053-3056 trans types
      A_DEF_ADJ    := case when L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 and L_ASSET_SCHEDULE_MONTH.IS_OM = 1
                           then L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT
                           when L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 and L_ASSET_SCHEDULE_MONTH.IS_OM = 0
                           then L_ASSET_SCHEDULE_APPROVED.BEG_ST_DEFERRED_RENT
                           when L_ASSET_SCHEDULE_APPROVED.IS_OM = 0 and L_ASSET_SCHEDULE_MONTH.IS_OM = 1
                           then L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT
                           else L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT end -
                      case when L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 and L_ASSET_SCHEDULE_MONTH.IS_OM = 1
                           then L_ASSET_SCHEDULE_APPROVED.BEG_ST_DEFERRED_RENT
                           when L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 and L_ASSET_SCHEDULE_MONTH.IS_OM = 0
                           then 0
                           when L_ASSET_SCHEDULE_APPROVED.IS_OM = 0 and L_ASSET_SCHEDULE_MONTH.IS_OM = 1
                           then 0
                           else L_ASSET_SCHEDULE_APPROVED.BEG_ST_DEFERRED_RENT end;

		  pkg_pp_log.p_write_message('Short term deferred rent amount: ' || to_char(a_def_adj));

      A_LT_DEF_ADJ := case when L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 AND L_ASSET_SCHEDULE_MONTH.IS_OM = 1
                           then (L_ASSET_SCHEDULE_MONTH.BEG_DEFERRED_RENT - L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT)
                           when L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 AND L_ASSET_SCHEDULE_MONTH.IS_OM = 0
                           then (L_ASSET_SCHEDULE_APPROVED.BEG_DEFERRED_RENT - L_ASSET_SCHEDULE_APPROVED.BEG_ST_DEFERRED_RENT)
                           when L_ASSET_SCHEDULE_APPROVED.IS_OM = 0 AND L_ASSET_SCHEDULE_MONTH.IS_OM = 1
                           then (L_ASSET_SCHEDULE_MONTH.BEG_DEFERRED_RENT - L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT)
                           else (L_ASSET_SCHEDULE_MONTH.BEG_DEFERRED_RENT - L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT) end -
                      case when L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 AND L_ASSET_SCHEDULE_MONTH.IS_OM = 1
                           then (L_ASSET_SCHEDULE_APPROVED.BEG_DEFERRED_RENT - L_ASSET_SCHEDULE_APPROVED.BEG_ST_DEFERRED_RENT)
                           when L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 AND L_ASSET_SCHEDULE_MONTH.IS_OM = 0
                           then 0
                           when L_ASSET_SCHEDULE_APPROVED.IS_OM = 0 and L_ASSET_SCHEDULE_MONTH.IS_OM = 1
                           then 0
                           else (L_ASSET_SCHEDULE_APPROVED.BEG_DEFERRED_RENT - L_ASSET_SCHEDULE_APPROVED.BEG_ST_DEFERRED_RENT) end;

		    pkg_pp_log.p_write_message('Long term deferred rent amount: ' || to_char(a_lt_def_adj));

      L_MSG := 'Get the asset id';
      select A.ASSET_ID
        into A_ASSET_ID
        from (select ASSET_ID,
                     ROW_NUMBER() OVER(partition by LS_ASSET_ID order by EFFECTIVE_DATE desc) as THE_ROW
                from LS_CPR_ASSET_MAP
               where LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID) A
       where A.THE_ROW = 1;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_ADJUST_ASSET;

   -- Function to perform specific Leased Asset addition
   function F_ADD_ASSET(A_LS_ASSET_ID number) return varchar2 is

      L_MSG            varchar2(2000);
      L_DG_ID          DEPR_GROUP.DEPR_GROUP_ID%type;
      L_LS_ASSET       LS_ASSET%rowtype;
      L_LS_PEND_TRANS  LS_PEND_TRANSACTION%rowtype;
      L_ASSET_SCHEDULE LS_ASSET_SCHEDULE%rowtype;
      L_MAJOR_LOC      MAJOR_LOCATION.MAJOR_LOCATION_ID%type;
      L_PROP_UNIT      PROPERTY_UNIT.PROPERTY_UNIT_ID%type;
      L_LOC_TYPE       LOCATION_TYPE.LOCATION_TYPE_ID%type;
      L_ASSET_ID       CPR_LEDGER.ASSET_ID%type;
      L_SECOND_COST    CPR_LEDGER.SECOND_FINANCIAL_COST%type;
      L_GL_ACCT_ID     GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_ST_ACCT_ID    GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_LT_ACCT_ID    GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_AP_ACCT_ID    GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_RES_ACCT_ID   GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_ST_DEF_ACCT_ID GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_LT_DEF_ACCT_ID GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_INTACC_ACCT_ID GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_ASSET_ACT_ID   number;
      L_ACT_CODE       CPR_ACTIVITY.ACTIVITY_CODE%type;
      L_FERC_ACT_CODE  CPR_ACTIVITY.FERC_ACTIVITY_CODE%type;
      L_CHECK          number;
      L_ADJ            number;
      L_RTN            number;
      L_OBL_ADJ        number;
      L_LT_OBL_ADJ     number;
      L_DEF_ADJ        number;
      L_LT_DEF_ADJ     number;
      L_RESERVE_ADJ    NUMBER;
      L_INIT_LIFE      number;
      L_MONTH          date;
      L_ACCT_MONTH     date;
      L_MID_PERIOD_METHOD varchar2(50);
	  L_MAX_MONTH      date;
	  L_IS_OM          number(1,0);
	  L_CAP_TYPE       NUMBER(22,0);
	  L_FASB_CHECK     NUMBER(22,0);
	  --$$KES: Added to find class code
	  L_CC_ID          NUMBER;
	  L_CC_VALUE       varchar2(100);
	  L_DG_CC_IND      varchar2(100);
	  L_SQLS           varchar2(2000);
      L_APPROVED_RATE  number;
      L_CURRENCY_FROM  number;
      L_CURRENCY_TO    number;
   begin
      L_MSG := 'Starting to add leased asset';

      -- ls_asset
      select * into L_LS_ASSET from LS_ASSET where LS_ASSET_ID = A_LS_ASSET_ID;

      -- ls_pend_transaction
      select * into L_LS_PEND_TRANS from LS_PEND_TRANSACTION where LS_ASSET_ID = A_LS_ASSET_ID;

    L_MSG := 'Getting the current open month for lessee';
      select greatest(min(LPC.GL_POSTING_MO_YR),min(LAS.MONTH)), min(LPC.GL_POSTING_MO_YR)
        into L_MONTH, L_ACCT_MONTH
        from LS_PROCESS_CONTROL LPC, LS_ASSET_SCHEDULE LAS
       where LPC.LAM_CLOSED is null
         and LPC.COMPANY_ID = L_LS_ASSET.COMPANY_ID
     and LAS.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
     and LAS.REVISION = L_LS_PEND_TRANS.REVISION;

    /* WMD */
    if L_LS_PEND_TRANS.GL_POSTING_MO_YR is not null then
      L_ACCT_MONTH:=L_LS_PEND_TRANS.GL_POSTING_MO_YR;
    end if;

    /* CJS 4/2/15 Checking to make sure asset schedule not already run out; Would never get picked up for retirement */
    L_MSG := 'Checking for greatest month on asset schedule';
    select max(LAS.MONTH)
      into L_MAX_MONTH
      from LS_ASSET_SCHEDULE LAS
    where LAS.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
      and LAS.REVISION = L_LS_PEND_TRANS.REVISION;

    if L_MAX_MONTH < L_ACCT_MONTH then
      return 'Error: Asset schedule expired before current open month. Cannot add an already retired asset.';
    end if;
    l_init_life := PKG_LEASE_COMMON.F_GET_INIT_LIFE(A_LS_ASSET_ID, L_LS_PEND_TRANS.revision,
                    L_LS_ASSET.ilr_id, L_LS_ASSET.economic_life);

      -- adding an asset if the count returns 0 (meaning it exists in the CPR) and there is an approved revision
      if L_LS_PEND_TRANS.ACTIVITY_CODE = 11 then
         L_ACT_CODE      := 'UADJ';
         L_ADJ           := 1;
         L_FERC_ACT_CODE := 3;

         -- this is an ADJUSTMENT
         -- figure out the adjustment amounts.
         L_MSG := F_ADJUST_ASSET(L_LS_ASSET,
                                 L_LS_PEND_TRANS.REVISION,
                                 1,
                                 L_OBL_ADJ,
                                 L_LT_OBL_ADJ,
                                 L_DEF_ADJ,
                                 L_LT_DEF_ADJ,
                                 L_ASSET_ID,
                                 L_LS_PEND_TRANS.GL_POSTING_MO_YR);
         if L_MSG <> 'OK' then
            return L_MSG;
         end if;

         L_LS_PEND_TRANS.POSTING_QUANTITY := 0;
      else
         --Generate asset id
         --Hopefully use something marginally better than pwrplant1
         select PWRPLANT1.NEXTVAL into L_ASSET_ID from DUAL;

         L_ACT_CODE      := 'UADD';
         L_ADJ           := 0;
         L_FERC_ACT_CODE := 1;
         L_ASSET_ACT_ID  := 1;
      end if;

      -- gl_account
      /* WMD */
      L_MSG:='Getting GL Accounts';
      select IA.CAP_ASSET_ACCOUNT_ID, IA.ST_OBLIG_ACCOUNT_ID, IA.LT_OBLIG_ACCOUNT_ID, IA.AP_ACCOUNT_ID,
             IA.ST_DEFERRED_ACCOUNT_ID, IA.LT_DEFERRED_ACCOUNT_ID, IA.INT_ACCRUAL_ACCOUNT_ID
        into L_GL_ACCT_ID, L_ST_ACCT_ID, L_LT_ACCT_ID, L_AP_ACCT_ID, L_ST_DEF_ACCT_ID, L_LT_DEF_ACCT_ID, L_INTACC_ACCT_ID
        from LS_ILR_ACCOUNT IA, LS_ASSET LA
       where LA.ILR_ID = IA.ILR_ID
         and LA.LS_ASSET_ID = A_LS_ASSET_ID;

      -- major_location
      select MAJOR_LOCATION_ID
        into L_MAJOR_LOC
        from ASSET_LOCATION
       where ASSET_LOCATION_ID = L_LS_ASSET.ASSET_LOCATION_ID;

      -- location_type
      select LOCATION_TYPE_ID
        into L_LOC_TYPE
        from MAJOR_LOCATION
       where MAJOR_LOCATION_ID = L_MAJOR_LOC;

      -- property_unit
      select PROPERTY_UNIT_ID
        into L_PROP_UNIT
        from RETIREMENT_UNIT
       where RETIREMENT_UNIT_ID = L_LS_PEND_TRANS.RETIREMENT_UNIT_ID;

      L_MSG   := 'Finding depr group';
      --CJS 5/2/17 If this is an adjustment, we should not re-find depr group; if the control tree has changed, it messes things up. only transfers/additions should trigger this
      --CJS Also need to change back to the original depr_group_id on ls_asset. p_get_lease_depr changes it when saving schedule, but no good solution to prevent it
      if L_LS_PEND_TRANS.ACTIVITY_CODE = 11 THEN
        select DEPR_GROUP_ID
          into L_DG_ID
          from CPR_LEDGER CPR, LS_CPR_ASSET_MAP MAP
         where MAP.LS_ASSET_ID = A_LS_ASSET_ID
           and MAP.ASSET_ID = CPR.ASSET_ID;

        update LS_ASSET
           set DEPR_GROUP_ID = L_DG_ID
         where LS_ASSET_ID = A_LS_ASSET_ID;

      else
        
        L_DG_CC_IND := pkg_pp_system_control.f_pp_system_control('Lease Depr Group Class Code');

        if nvl(L_DG_CC_IND, 'none') = 'none' then
		    L_CC_ID := -1;
		    L_CC_VALUE := 'NO CLASS CODE';
        else
		    begin
		    select CLASS_CODE_ID
		    into L_CC_ID
		    from CLASS_CODE
		    where lower(trim(description)) = pkg_pp_system_control.f_pp_system_control('DEPR GROUP CLASS CODE');
		    
		    exception when no_data_found then
		      L_CC_ID := -1;
		    end;
		    
		    begin
			L_SQLS := 'select value from ls_'||L_DG_CC_IND||'_class_code where class_code_id = '||L_CC_ID;
		    case when L_DG_CC_IND = 'mla' then
		      L_SQLS := L_SQLS || ' and lease_id = (select lease_id from ls_ilr ilr, ls_asset la where ilr.ilr_id = la.ilr_id and la.ls_asset_id = '||A_LS_ASSET_ID||')';
		    when L_DG_CC_IND = 'ilr' then
		      L_SQLS := L_SQLS || ' and ilr_id = (select ilr_id from ls_asset where ls_asset_id = '||A_LS_ASSET_ID||')';
		    when L_DG_CC_IND = 'asset' then
		      L_SQLS := L_SQLS || ' and ls_asset_id = '||A_LS_ASSET_ID;
		    end case;
		      
		    EXECUTE IMMEDIATE L_SQLS
		    into L_CC_VALUE;
			
		    exception when no_data_found then
		      L_CC_VALUE:= 'NO CLASS CODE';
		    end;
        end if;

		L_DG_ID := PP_DEPR_PKG.F_FIND_DEPR_GROUP(L_LS_PEND_TRANS.COMPANY_ID,
                                                L_GL_ACCT_ID,
                                                L_MAJOR_LOC,
                                                L_LS_PEND_TRANS.UTILITY_ACCOUNT_ID,
                                                L_LS_PEND_TRANS.BUS_SEGMENT_ID,
                                                L_LS_PEND_TRANS.SUB_ACCOUNT_ID,
                                                -100, --SUBLEDGER_TYPE_ID
                                                TO_NUMBER(TO_CHAR(NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE,
                                                                      L_MONTH),
                                                                  'YYYY')),
                                                L_LS_PEND_TRANS.ASSET_LOCATION_ID,
                                                L_LOC_TYPE,
                                                L_LS_PEND_TRANS.RETIREMENT_UNIT_ID,
                                                L_PROP_UNIT,
                                                L_CC_ID, --A_CLASS_CODE_ID
                                                L_CC_VALUE); --A_CC_VALUE
        if L_DG_ID in (-1, -9) then
          -- we canot save... set error_message on ls_pend_transactions
          return 'ERROR: Finding Depreciation Group (' || TO_CHAR(NVL(L_LS_PEND_TRANS.COMPANY_ID,
                                                                      '')) || ' : ' || TO_CHAR(NVL(L_GL_ACCT_ID,
                                                                                                    '')) || ' : ' || TO_CHAR(NVL(L_MAJOR_LOC,
                                                                                                                                '')) || ' : ' || TO_CHAR(NVL(L_LS_PEND_TRANS.UTILITY_ACCOUNT_ID,
                                                                                                                                                              '')) || ' : ' || TO_CHAR(NVL(L_LS_PEND_TRANS.BUS_SEGMENT_ID,
                                                                                                                                                                                          '')) || ' : ' || TO_CHAR(NVL(L_LS_PEND_TRANS.SUB_ACCOUNT_ID,
                                                                                                                                                                                                                        '')) || ' : -100 : ' || TO_CHAR(NVL(TO_CHAR(NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE,
                                                                                                                                                                                                                                                                        L_MONTH),
                                                                                                                                                                                                                                                                    'YYYY'),
                                                                                                                                                                                                                                                            '')) || ' : ' || TO_CHAR(NVL(L_LS_PEND_TRANS.ASSET_LOCATION_ID,
                                                                                                                                                                                                                                                                                        '')) || ' : ' || TO_CHAR(NVL(L_LOC_TYPE,
                                                                                                                                                                                                                                                                                                                      '')) || ' : ' || TO_CHAR(NVL(L_LS_PEND_TRANS.RETIREMENT_UNIT_ID,
                                                                                                                                                                                                                                                                                                                                                  '')) || ' : ' || TO_CHAR(NVL(L_PROP_UNIT,
                                                                                                                                                                                                                                                                                                                                                                                ''));
        end if;

        -- check to make sure the depr group has subledger_type_id = -100
        L_CHECK := 0;
        select count(1)
          into L_CHECK
          from DEPR_GROUP
        where DEPR_GROUP_ID = L_DG_ID
          and SUBLEDGER_TYPE_ID = -100;

        if L_CHECK <> 1 then
          return 'ERROR: Depreciation Group is not a lease depreciation group.  Fix the depreciation group control tree: ' || TO_CHAR(L_DG_ID);
        end if;
      end if; -- CJS 5/2/17

      SELECT RESERVE_ACCT_ID
      INTO L_RES_ACCT_ID
      FROM DEPR_GROUP
      WHERE DEPR_GROUP_ID = L_DG_ID;
      L_MSG          := 'Get the next activity id';
      L_ASSET_ACT_ID := PP_CPR_PKG.F_GET_ASSET_ACTIVITY_ID(L_ASSET_ID, L_DG_ID);

      select UPPER(trim(MID_PERIOD_METHOD))
      into L_MID_PERIOD_METHOD
      from DEPR_GROUP
      where DEPR_GROUP_ID = L_DG_ID;
 	  
	  --call to new function to extend BPO schedules out 1 extra month if mid period conv on the DG found is <> 1
       L_MSG := PKG_LEASE_SCHEDULE.F_BPO_SCHEDULE_EXTEND(L_LS_ASSET.LS_ASSET_ID, L_LS_ASSET.ILR_ID, L_LS_PEND_TRANS.REVISION);
 
       IF L_MSG <> 'OK' THEN
         PKG_PP_LOG.P_WRITE_MESSAGE('Error extending schedules for LS_ASSET_ID: ' || L_LS_ASSET.LS_ASSET_ID || ' ILR_ID: ' || L_LS_ASSET.ILR_ID || ' Revision: ' || L_LS_PEND_TRANS.REVISION);
         ROLLBACK;
       END IF;

      L_MSG:='Checking if operating or capital, pulling cap type';
         select lct.is_om, lct.ls_lease_cap_type_id
         into L_IS_OM, L_CAP_TYPE
         from ls_lease_cap_type lct, ls_ilr_options ilro
         where ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
          and ilro.ilr_id = L_LS_PEND_TRANS.ILR_ID
          and ilro.revision = L_LS_PEND_TRANS.REVISION;

      for L_SOBS in (select PB.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID, PB.POSTING_AMOUNT as AMOUNT
                       from LS_PEND_SET_OF_BOOKS PB
                      where PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID)
      loop
         --get the asset_schedule for the first month
         begin
            select A.*
            into L_ASSET_SCHEDULE
            from LS_ASSET_SCHEDULE A
            where A.LS_ASSET_ID = A_LS_ASSET_ID
            and A.REVISION = L_LS_PEND_TRANS.REVISION
            and A.SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
            and A.MONTH = L_MONTH;

         exception
            when NO_DATA_FOUND then
               continue;
         end;

      --CJS 3/6/17 if it's SLE on a different SOB, need to account for it here
	  L_MSG:='Pulling FASB Cap Type';
	  begin
      SELECT FASB_CAP_TYPE_ID
      INTO L_FASB_CHECK
      FROM LS_FASB_CAP_TYPE_SOB_MAP
      WHERE LEASE_CAP_TYPE_ID = L_CAP_TYPE
      AND SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID;
	  
      exception
         when NO_DATA_FOUND then
            continue;
      end;

      if L_MID_PERIOD_METHOD = 'SLE' OR (Nvl(L_FASB_CHECK,1) = 2 AND L_IS_OM = 0) then
      -- load SLE table for depreciation
         PKG_LEASE_DEPR.P_DEPR_SLE( A_LS_ASSET_ID, L_LS_PEND_TRANS.REVISION, L_ASSET_ID, L_SOBS.SET_OF_BOOKS_ID,
          L_LS_PEND_TRANS.IN_SERVICE_DATE, l_init_life, L_MSG );
         if L_MSG <> 'OK' then
            return L_MSG;
      end if;
      elsif L_MID_PERIOD_METHOD = 'FERC' then
         PKG_LEASE_DEPR.P_DEPR_FERC( A_LS_ASSET_ID, L_LS_PEND_TRANS.REVISION, L_ASSET_ID, L_SOBS.SET_OF_BOOKS_ID,
          L_LS_PEND_TRANS.IN_SERVICE_DATE, L_MSG );
         if L_MSG <> 'OK' then
            return L_MSG;
         end if;
      end if;

         if L_ADJ = 1 then
            -- this is an ADJUSTMENT
            -- figure out the adjustment amounts.
            L_MSG := F_ADJUST_ASSET(L_LS_ASSET,
                                    L_LS_PEND_TRANS.REVISION,
                                    L_SOBS.SET_OF_BOOKS_ID,
                                    L_OBL_ADJ,
                                    L_LT_OBL_ADJ,
                                    L_DEF_ADJ,
                                    L_LT_DEF_ADJ,
                                    L_ASSET_ID,
                                    L_LS_PEND_TRANS.GL_POSTING_MO_YR);
            if L_MSG <> 'OK' then
               return L_MSG;
            end if;

            L_ASSET_SCHEDULE.BEG_OBLIGATION     := L_SOBS.AMOUNT;
            L_ASSET_SCHEDULE.BEG_LT_OBLIGATION  := 0;
         end if;

--         CJS 3/6/17 Move this up above to be used in depr check
--         L_MSG:='Checking if operating or capital';
--         select lct.is_om
--         into L_IS_OM
--         from ls_lease_cap_type lct, ls_ilr_options ilro
--         where ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
--          and ilro.ilr_id = L_LS_PEND_TRANS.ILR_ID
--          and ilro.revision = L_LS_PEND_TRANS.REVISION;

          -- CJS set the approved revision here; was setting it too late such that wrong lease cap type is used for JE exclusions
          update LS_ASSET
            set APPROVED_REVISION = L_LS_PEND_TRANS.REVISION,
                IN_SERVICE_DATE = NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE, L_MONTH), LS_ASSET_STATUS_ID = 3,
              EXPECTED_LIFE = L_INIT_LIFE
          where LS_ASSET_ID = A_LS_ASSET_ID;

         -- Process by set of books
         -- process JEs 3001, 3002, and 3003

          -- L_CURRENCY_FROM
          L_MSG:='Retrieving L_CURRENCY_FROM';
          SELECT contract_currency_id
          INTO L_CURRENCY_FROM
          FROM ls_lease l, ls_ilr i
          WHERE l.lease_id = i.lease_id
            AND i.ilr_id = L_LS_PEND_TRANS.ILR_ID;

            -- L_CURRENCY_TO
          L_MSG:='Retrieving L_CURRENCY_TO';
          SELECT currency_id
          INTO L_CURRENCY_TO
          FROM ls_ilr ilr, currency_schema cs
          WHERE ilr.company_id = cs.company_id
            AND currency_type_id = 1
            AND ilr_id = L_LS_PEND_TRANS.ILR_ID;

          -- L_APPROVED_RATE
            L_MSG:='Retrieving L_APPROVED_RATE';
          SELECT in_service_exchange_rate
          INTO L_APPROVED_RATE
          FROM ls_ilr_options ilro
          WHERE ilr_id = L_LS_PEND_TRANS.ILR_ID
            AND revision = L_LS_PEND_TRANS.REVISION;

         L_MSG := 'Creating Journal (Leased Asset Addition Debit)';
         if L_IS_OM <> 1 and G_SEND_JES then
            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                               3001,
                                               L_SOBS.AMOUNT,
                                               L_ASSET_ACT_ID,
                                               L_DG_ID,
                                               L_LS_PEND_TRANS.WORK_ORDER_ID,
                                               L_GL_ACCT_ID,
                                               0,
                                               L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                               L_LS_PEND_TRANS.COMPANY_ID,
                                               L_ACCT_MONTH,
                                               1,
                                               L_LS_PEND_TRANS.GL_JE_CODE,
                                               L_SOBS.SET_OF_BOOKS_ID,
                                               L_APPROVED_RATE,
                                               L_CURRENCY_FROM,
                                               L_CURRENCY_TO,
                                               L_MSG);
            if L_RTN = -1 then
               return L_MSG;
            end if;

      /* CJS 2/16/15 Changing to use obligation adjustments */
            L_MSG := 'Creating Journal (Leased Asset Addition ST Obligation Credit)';
            if case when L_ADJ = 1 then L_OBL_ADJ else L_ASSET_SCHEDULE.BEG_OBLIGATION - NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0) end <> 0 then /* WMD */
               L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                  3002,
                                                  case
                            when L_ADJ = 1 then
                            L_OBL_ADJ
                          else
                            L_ASSET_SCHEDULE.BEG_OBLIGATION - NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0)
                          end, /* WMD */
                                                  L_ASSET_ACT_ID,
                                                  L_DG_ID,
                                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                  L_ST_ACCT_ID,
                                                  0,
                                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                  L_LS_PEND_TRANS.COMPANY_ID,
                                                  L_ACCT_MONTH,
                                                  0,
                                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                                  L_SOBS.SET_OF_BOOKS_ID,
                                                  L_APPROVED_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  L_MSG);
               if L_RTN = -1 then
                  return L_MSG;
               end if;
            end if; -- short term JE

            L_MSG := 'Creating Journal (Leased Asset Addition LT Obligation Credit)';
            if case when L_ADJ = 1 then L_LT_OBL_ADJ else NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0) end <> 0 then /* WMD */
               L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                  3003,
                                                  case
                            when L_ADJ = 1 then
                            L_LT_OBL_ADJ
                          else
                            NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0)
                          end, /* WMD */
                                                  L_ASSET_ACT_ID,
                                                  L_DG_ID,
                                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                  L_LT_ACCT_ID,
                                                  0,
                                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                  L_LS_PEND_TRANS.COMPANY_ID,
                                                  L_ACCT_MONTH,
                                                  0,
                                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                                  L_SOBS.SET_OF_BOOKS_ID,
                                                  L_APPROVED_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  L_MSG);
               if L_RTN = -1 then
                  return L_MSG;
               end if;
            end if; -- long term JE

			/* Debit ST Deferred/Accrued Rent */
            if L_ADJ = 1 and L_DEF_ADJ <> 0 then
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                  3057,
                                                  L_DEF_ADJ,
                                                  L_ASSET_ACT_ID,
                                                  L_DG_ID,
                                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                  L_ST_DEF_ACCT_ID,
                                                  0,
                                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                  L_LS_PEND_TRANS.COMPANY_ID,
                                                  L_ACCT_MONTH,
                                                  1,
                                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                                  L_SOBS.SET_OF_BOOKS_ID,
                                                  L_APPROVED_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  L_MSG);
               if L_RTN = -1 then
                  return L_MSG;
               end if;
            end if;

            /* Debit LT Deferred/Accrued Rent */
            if L_ADJ = 1 and L_LT_DEF_ADJ <> 0 then
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                  3058,
                                                  L_LT_DEF_ADJ,
                                                  L_ASSET_ACT_ID,
                                                  L_DG_ID,
                                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                  L_LT_DEF_ACCT_ID,
                                                  0,
                                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                  L_LS_PEND_TRANS.COMPANY_ID,
                                                  L_ACCT_MONTH,
                                                  1,
                                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                                  L_SOBS.SET_OF_BOOKS_ID,
                                                  L_APPROVED_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  L_MSG);
               if L_RTN = -1 then
                  return L_MSG;
               end if;
            end if;

            /* Hit depr if our adjustment doesn't balance */
            if L_ADJ = 1 and (L_SOBS.AMOUNT + L_DEF_ADJ + L_LT_DEF_ADJ - L_LT_OBL_ADJ - L_OBL_ADJ) <> 0 then
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                  3033,
                                                  L_SOBS.AMOUNT + L_DEF_ADJ + L_LT_DEF_ADJ - L_LT_OBL_ADJ - L_OBL_ADJ, /* WMD */
                                                  L_ASSET_ACT_ID,
                                                  L_DG_ID,
                                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                  L_RES_ACCT_ID,
                                                  0,
                                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                  L_LS_PEND_TRANS.COMPANY_ID,
                                                  L_ACCT_MONTH,
                                                  0,
                                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                                  L_SOBS.SET_OF_BOOKS_ID,
                                                  L_APPROVED_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  L_MSG);
               if L_RTN = -1 then
                  return L_MSG;
               end if;
              L_MSG:='Updating cpr depr table for reserve adjustment';
               update cpr_depr
               set reserve_adjustment = reserve_adjustment + L_SOBS.AMOUNT + L_DEF_ADJ + L_LT_DEF_ADJ - L_LT_OBL_ADJ - L_OBL_ADJ
               where gl_posting_mo_yr = L_ACCT_MONTH
                and asset_id = (select asset_id from ls_cpr_asset_map where ls_asset_id = A_LS_ASSET_ID)
                and set_of_books_id = L_SOBS.SET_OF_BOOKS_ID;

              L_MSG:='Updating depr ledger table for reserve adjustment';
               update depr_ledger
               set reserve_adjustments = reserve_adjustments + L_SOBS.AMOUNT + L_DEF_ADJ + L_LT_DEF_ADJ - L_LT_OBL_ADJ - L_OBL_ADJ
               where depr_group_id = L_DG_id
                and gl_post_mo_yr = L_ACCT_MONTH
                and set_of_books_id = L_SOBS.SET_OF_BOOKS_ID;
            end if; -- depr JE

			if L_ADJ = 0 AND L_SOBS.AMOUNT + L_DEF_ADJ + L_LT_DEF_ADJ <> nvl(L_ASSET_SCHEDULE.BEG_OBLIGATION, 0) then
          L_MSG := 'Creating Journal (Leased Asset Addition Backdated Obligation Trueup)';
          L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                            3042,
													  L_SOBS.AMOUNT + L_DEF_ADJ + L_LT_DEF_ADJ - nvl(L_ASSET_SCHEDULE.beg_obligation, 0),
                            L_ASSET_ACT_ID,
                            L_DG_ID,
                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                            L_ST_ACCT_ID,
                            0,
                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                            L_LS_PEND_TRANS.COMPANY_ID,
                            L_ACCT_MONTH,
                            0,
                            L_LS_PEND_TRANS.GL_JE_CODE,
                            L_SOBS.SET_OF_BOOKS_ID,
                            L_APPROVED_RATE,
                            L_CURRENCY_FROM,
                            L_CURRENCY_TO,
                            L_MSG);
         if L_RTN = -1 then
            return L_MSG;
         end if;

         L_RESERVE_ADJ:= L_SOBS.AMOUNT + L_DEF_ADJ + L_LT_DEF_ADJ - nvl(L_ASSET_SCHEDULE.beg_obligation, 0);
         /* WMD adding principal catchup */
         if lower(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY( 'Lease Catch Up Principal',L_LS_PEND_TRANS.COMPANY_ID)))='yes' then
          L_MSG := 'Creating Journal (Leased Asset Addition Backdated Principal Trueup Debit to Principal)';
          L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                            3018,
													  L_SOBS.AMOUNT + L_DEF_ADJ + L_LT_DEF_ADJ - nvl(L_ASSET_SCHEDULE.beg_obligation, 0),
                            L_ASSET_ACT_ID,
                            L_DG_ID,
                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                            L_ST_ACCT_ID,
                            0,
                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                            L_LS_PEND_TRANS.COMPANY_ID,
                            L_ACCT_MONTH,
                            1,
                            L_LS_PEND_TRANS.GL_JE_CODE,
                            L_SOBS.SET_OF_BOOKS_ID,
                            L_APPROVED_RATE,
                            L_CURRENCY_FROM,
                            L_CURRENCY_TO,
                            L_MSG);
         if L_RTN = -1 then
            return L_MSG;
         end if;

          L_MSG := 'Creating Journal (Leased Asset Addition Backdated Principal Trueup Debit to Principal)';
          L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                            3022,
													  L_SOBS.AMOUNT + L_DEF_ADJ + L_LT_DEF_ADJ - nvl(L_ASSET_SCHEDULE.beg_obligation, 0),
                            L_ASSET_ACT_ID,
                            L_DG_ID,
                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                            L_AP_ACCT_ID,
                            0,
                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                            L_LS_PEND_TRANS.COMPANY_ID,
                            L_ACCT_MONTH,
                            0,
                            L_LS_PEND_TRANS.GL_JE_CODE,
                            L_SOBS.SET_OF_BOOKS_ID,
                            L_APPROVED_RATE,
                            L_CURRENCY_FROM,
                            L_CURRENCY_TO,
                            L_MSG);
         if L_RTN = -1 then
            return L_MSG;
         end if;

         end if; -- Principal Catch Up
      end if; -- Trueup
		 --For additions and adjustments, lets book deferred rent amounts (should never start with a balance on deferred rent though)
         elsif L_IS_OM = 1 and G_SEND_JES then
		      if lower(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Generate Deferred Rent JEs', L_LS_PEND_TRANS.COMPANY_ID)) = 'yes' then
				  L_MSG := 'Creating Journal (Leased Asset Addition ST Deferred Rent Credit)';
				  if L_ADJ = 1 and L_DEF_ADJ <> 0 then
				    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													    3053,
													    L_DEF_ADJ,
													    L_ASSET_ACT_ID,
													    L_DG_ID,
													    L_LS_PEND_TRANS.WORK_ORDER_ID,
													    L_ST_DEF_ACCT_ID,
													    0,
													    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													    L_LS_PEND_TRANS.COMPANY_ID,
													    L_ACCT_MONTH,
													    0,
													    L_LS_PEND_TRANS.GL_JE_CODE,
													    L_SOBS.SET_OF_BOOKS_ID,
													    L_APPROVED_RATE,
													    L_CURRENCY_FROM,
													    L_CURRENCY_TO,
													    L_MSG);
				    if L_RTN = -1 then
					    return L_MSG;
				    end if;
				  end if; -- short term Deferred JE

				  L_MSG := 'Creating Journal (Leased Asset Addition LT Deferred Rent Credit)';
				  if L_ADJ = 1 and L_LT_DEF_ADJ <> 0 then
				    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													    3054,
													    L_LT_DEF_ADJ,
													    L_ASSET_ACT_ID,
													    L_DG_ID,
													    L_LS_PEND_TRANS.WORK_ORDER_ID,
													    L_LT_DEF_ACCT_ID,
													    0,
													    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													    L_LS_PEND_TRANS.COMPANY_ID,
													    L_ACCT_MONTH,
													    0,
													    L_LS_PEND_TRANS.GL_JE_CODE,
													    L_SOBS.SET_OF_BOOKS_ID,
													    L_APPROVED_RATE,
													    L_CURRENCY_FROM,
													    L_CURRENCY_TO,
													    L_MSG);
				    if L_RTN = -1 then
					    return L_MSG;
				    end if;
				  end if; -- long term Deferred JE

				  L_MSG := 'Creating Journal (Leased Asset Addition Deferred Rent Debit)';
				  if L_ADJ = 1 and L_DEF_ADJ + L_LT_DEF_ADJ <> 0 then
				    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													    3055,
													    L_DEF_ADJ + L_LT_DEF_ADJ,
													    L_ASSET_ACT_ID,
													    L_DG_ID,
													    L_LS_PEND_TRANS.WORK_ORDER_ID,
													    L_INTACC_ACCT_ID,
													    0,
													    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													    L_LS_PEND_TRANS.COMPANY_ID,
													    L_ACCT_MONTH,
													    1,
													    L_LS_PEND_TRANS.GL_JE_CODE,
													    L_SOBS.SET_OF_BOOKS_ID,
													    L_APPROVED_RATE,
													    L_CURRENCY_FROM,
													    L_CURRENCY_TO,
													    L_MSG);
				    if L_RTN = -1 then
					    return L_MSG;
				    end if;
				  end if; -- Deferred Debit JE

				  if lower(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Generate Catch-up Deferred Rent JEs', L_LS_PEND_TRANS.COMPANY_ID)) = 'yes' then
					  L_MSG := 'Creating Journal (Leased Asset Addition Backdated ST Deferred Rent)';
					    if L_ADJ = 0 and L_ASSET_SCHEDULE.beg_st_deferred_rent <> 0 then
						    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													      3053,
													      L_ASSET_SCHEDULE.beg_st_deferred_rent,
													      L_ASSET_ACT_ID,
													      L_DG_ID,
													      L_LS_PEND_TRANS.WORK_ORDER_ID,
													      L_ST_DEF_ACCT_ID,
													      0,
													      L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													      L_LS_PEND_TRANS.COMPANY_ID,
													      L_ACCT_MONTH,
													      0,
													      L_LS_PEND_TRANS.GL_JE_CODE,
													      L_SOBS.SET_OF_BOOKS_ID,
													      L_APPROVED_RATE,
													      L_CURRENCY_FROM,
													      L_CURRENCY_TO,
													      L_MSG);
						    if L_RTN = -1 then
						      return L_MSG;
						    end if;
					    end if; -- short term Deferred JE

					    L_MSG := 'Creating Journal (Leased Asset Addition Backdated LT Deferred Rent)';
					    if L_ADJ = 0 and L_ASSET_SCHEDULE.beg_deferred_rent - L_ASSET_SCHEDULE.beg_st_deferred_rent <> 0 then
						    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													      3054,
													      L_ASSET_SCHEDULE.beg_deferred_rent - L_ASSET_SCHEDULE.beg_st_deferred_rent,
													      L_ASSET_ACT_ID,
													      L_DG_ID,
													      L_LS_PEND_TRANS.WORK_ORDER_ID,
													      L_LT_DEF_ACCT_ID,
													      0,
													      L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													      L_LS_PEND_TRANS.COMPANY_ID,
													      L_ACCT_MONTH,
													      0,
													      L_LS_PEND_TRANS.GL_JE_CODE,
													      L_SOBS.SET_OF_BOOKS_ID,
													      L_APPROVED_RATE,
													      L_CURRENCY_FROM,
													      L_CURRENCY_TO,
													      L_MSG);
						    if L_RTN = -1 then
						      return L_MSG;
						    end if;
					    end if; -- long term Deferred JE

					    L_MSG := 'Creating Journal (Leased Asset Addition Backdated Deferred Rent Debit)';
					    if L_ADJ = 0 and L_ASSET_SCHEDULE.beg_deferred_rent <> 0 then
						    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													      3056,
													      L_ASSET_SCHEDULE.beg_deferred_rent,
													      L_ASSET_ACT_ID,
													      L_DG_ID,
													      L_LS_PEND_TRANS.WORK_ORDER_ID,
													      L_INTACC_ACCT_ID,
													      0,
													      L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													      L_LS_PEND_TRANS.COMPANY_ID,
													      L_ACCT_MONTH,
													      1,
													      L_LS_PEND_TRANS.GL_JE_CODE,
													      L_SOBS.SET_OF_BOOKS_ID,
													      L_APPROVED_RATE,
													      L_CURRENCY_FROM,
													      L_CURRENCY_TO,
													      L_MSG);
						    if L_RTN = -1 then
						      return L_MSG;
						    end if;
					    end if; -- Deferred Rent Debit
				    end if; -- Deferred Rent Catch-up
			    end if; -- Generate Deferred Rent JEs
         end if; -- process JEs for this SOB
      end loop; -- END Process by set of books

      L_SECOND_COST := 0;

      select count(*) into L_SECOND_COST
        from SET_OF_BOOKS S
       where S.SECOND_SET_OF_BOOKS_IND = 1
         and S.SET_OF_BOOKS_ID in (
          select set_of_books_id from company_set_of_books
           where nvl(include_indicator,0) = 1
            and company_id = L_LS_PEND_TRANS.COMPANY_ID);

      if L_SECOND_COST > 0 then
         L_MSG := 'Retrieving second cost';
         BEGIN
           select POSTING_AMOUNT
             into L_SECOND_COST
             from SET_OF_BOOKS S, LS_PEND_SET_OF_BOOKS PB
            where S.SECOND_SET_OF_BOOKS_IND = 1
              and PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID
              and S.SET_OF_BOOKS_ID = PB.SET_OF_BOOKS_ID;
         EXCEPTION
           WHEN no_data_found then
             L_MSG := 'ERROR: Second Cost Book not found in LS_PEND_SET_OF_BOOKS.';
             return L_MSG;
         END;
      end if;

      -- if it's on pend transaction, take it from there
      L_MSG := 'Adding asset to CPR Ledger';
      merge into CPR_LEDGER A
      using (select L_ASSET_ID as ASSET_ID,
                    L_LS_PEND_TRANS.PROPERTY_GROUP_ID as PROPERTY_GROUP_ID,
                    L_DG_ID as DEPR_GROUP_ID,
                    L_LS_PEND_TRANS.RETIREMENT_UNIT_ID as RETIREMENT_UNIT_ID,
                    L_LS_PEND_TRANS.BUS_SEGMENT_ID as BUS_SEGMENT_ID,
                    L_LS_PEND_TRANS.COMPANY_ID as COMPANY_ID,
                    L_LS_PEND_TRANS.FUNC_CLASS_ID as FUNC_CLASS_ID,
                    L_LS_PEND_TRANS.UTILITY_ACCOUNT_ID as UTILITY_ACCOUNT_ID,
                    L_GL_ACCT_ID as GL_ACCOUNT_ID,
                    L_LS_PEND_TRANS.ASSET_LOCATION_ID as ASSET_LOCATION_ID,
                    L_LS_PEND_TRANS.SUB_ACCOUNT_ID as SUB_ACCOUNT_ID,
                    WOC.WORK_ORDER_NUMBER as WORK_ORDER_NUMBER,
                    NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE, L_MONTH) as IN_SERVICE_DATE,
                    L_LS_PEND_TRANS.POSTING_QUANTITY as POSTING_QUANTITY,
                    L_LS_PEND_TRANS.POSTING_AMOUNT * L_APPROVED_RATE as POSTING_AMOUNT,
                    L_LS_ASSET.DESCRIPTION as DESCRIPTION,
                    L_LS_ASSET.LONG_DESCRIPTION as LONG_DESCRIPTION,
                    L_LS_PEND_TRANS.SERIAL_NUMBER as SERIAL_NUMBER,
                    case
                       when L_SECOND_COST > 0 then
                        L_LS_PEND_TRANS.POSTING_AMOUNT * L_APPROVED_RATE
                       else
                        0
                    end as SECOND_COST
               from WORK_ORDER_CONTROL WOC
              where WOC.WORK_ORDER_ID = L_LS_PEND_TRANS.WORK_ORDER_ID) B
      on (A.ASSET_ID = B.ASSET_ID)
      when matched then
         update
            set A.ACCUM_QUANTITY = A.ACCUM_QUANTITY + B.POSTING_QUANTITY,
                A.ACCUM_COST = A.ACCUM_COST + B.POSTING_AMOUNT,
                A.SECOND_FINANCIAL_COST = A.SECOND_FINANCIAL_COST + B.SECOND_COST,
                /* WMD Update in service dates if the start date */
                A.ENG_IN_SERVICE_YEAR =B.IN_SERVICE_DATE,
                A.IN_SERVICE_YEAR = B.IN_SERVICE_DATE
      when not matched then
         insert
            (ASSET_ID, PROPERTY_GROUP_ID, DEPR_GROUP_ID, BOOKS_SCHEMA_ID, RETIREMENT_UNIT_ID,
             BUS_SEGMENT_ID, COMPANY_ID, FUNC_CLASS_ID, UTILITY_ACCOUNT_ID, GL_ACCOUNT_ID,
             ASSET_LOCATION_ID, SUB_ACCOUNT_ID, WORK_ORDER_NUMBER, LEDGER_STATUS, IN_SERVICE_YEAR,
             ACCUM_QUANTITY, ACCUM_COST, SUBLEDGER_INDICATOR, DESCRIPTION, LONG_DESCRIPTION,
             ENG_IN_SERVICE_YEAR, SERIAL_NUMBER, ACCUM_COST_2, SECOND_FINANCIAL_COST)
         values
            (B.ASSET_ID, B.PROPERTY_GROUP_ID, B.DEPR_GROUP_ID, 1, B.RETIREMENT_UNIT_ID,
             B.BUS_SEGMENT_ID, B.COMPANY_ID, B.FUNC_CLASS_ID, B.UTILITY_ACCOUNT_ID, B.GL_ACCOUNT_ID,
             B.ASSET_LOCATION_ID, B.SUB_ACCOUNT_ID, B.WORK_ORDER_NUMBER, 1, B.IN_SERVICE_DATE,
             B.POSTING_QUANTITY, B.POSTING_AMOUNT, -100, B.DESCRIPTION, B.LONG_DESCRIPTION,
             B.IN_SERVICE_DATE, B.SERIAL_NUMBER, 0, B.SECOND_COST);

      L_MSG := 'Adding CPR Activity';
      insert into CPR_ACTIVITY
         (ASSET_ID, ASSET_ACTIVITY_ID, GL_POSTING_MO_YR, CPR_POSTING_MO_YR, WORK_ORDER_NUMBER,
          GL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION, ACTIVITY_CODE, ACTIVITY_STATUS,
          ACTIVITY_QUANTITY, ACTIVITY_COST, FERC_ACTIVITY_CODE, MONTH_NUMBER, ACTIVITY_COST_2,
          ACT_SECOND_FINANCIAL_COST)
         select L_ASSET_ID,
                L_ASSET_ACT_ID,
                L_ACCT_MONTH, --gl_posting_mo_yr
                L_ACCT_MONTH, --cpr_posting_mo_yr
                WOC.WORK_ORDER_NUMBER,
                L_LS_PEND_TRANS.GL_JE_CODE,
                L_LS_ASSET.DESCRIPTION,
                L_LS_ASSET.LONG_DESCRIPTION,
                L_ACT_CODE, --Activity Code
                L_LS_PEND_TRANS.LS_PEND_TRANS_ID, --activity_status (this is the pend_trans_id)
                L_LS_PEND_TRANS.POSTING_QUANTITY,
                L_LS_PEND_TRANS.POSTING_AMOUNT * L_APPROVED_RATE,
                L_FERC_ACT_CODE, --ferc_activity_code (addition)
                TO_NUMBER(TO_CHAR(L_ACCT_MONTH, 'yyyymm')), --month_number
                0,
                case
                   when L_SECOND_COST > 0 then
                    L_LS_PEND_TRANS.POSTING_AMOUNT * L_APPROVED_RATE
                   else
                    0
                end
           from WORK_ORDER_CONTROL WOC
          where WOC.WORK_ORDER_ID = L_LS_PEND_TRANS.WORK_ORDER_ID;

      L_MSG := 'Adding to activity basis table';
      insert into CPR_ACT_BASIS
         (ASSET_ID, ASSET_ACTIVITY_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6,
          BASIS_7, BASIS_8, BASIS_9, BASIS_10, BASIS_11, BASIS_12, BASIS_13, BASIS_14, BASIS_15,
          BASIS_16, BASIS_17, BASIS_18, BASIS_19, BASIS_20, BASIS_21, BASIS_22, BASIS_23, BASIS_24,
          BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32, BASIS_33,
          BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41, BASIS_42,
          BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50, BASIS_51,
          BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60,
          BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68, BASIS_69,
          BASIS_70)
         select L_ASSET_ID,
                L_ASSET_ACT_ID,
                BASIS_1 * L_APPROVED_RATE AS BASIS_1,
                BASIS_2 * L_APPROVED_RATE AS BASIS_2,
                BASIS_3 * L_APPROVED_RATE AS BASIS_3,
                BASIS_4 * L_APPROVED_RATE AS BASIS_4,
                BASIS_5 * L_APPROVED_RATE AS BASIS_5,
                BASIS_6 * L_APPROVED_RATE AS BASIS_6,
                BASIS_7 * L_APPROVED_RATE AS BASIS_7,
                BASIS_8 * L_APPROVED_RATE AS BASIS_8,
                BASIS_9 * L_APPROVED_RATE AS BASIS_9,
                BASIS_10 * L_APPROVED_RATE AS BASIS_10,
                BASIS_11 * L_APPROVED_RATE AS BASIS_11,
                BASIS_12 * L_APPROVED_RATE AS BASIS_12,
                BASIS_13 * L_APPROVED_RATE AS BASIS_13,
                BASIS_14 * L_APPROVED_RATE AS BASIS_14,
                BASIS_15 * L_APPROVED_RATE AS BASIS_15,
                BASIS_16 * L_APPROVED_RATE AS BASIS_16,
                BASIS_17 * L_APPROVED_RATE AS BASIS_17,
                BASIS_18 * L_APPROVED_RATE AS BASIS_18,
                BASIS_19 * L_APPROVED_RATE AS BASIS_19,
                BASIS_20 * L_APPROVED_RATE AS BASIS_20,
                BASIS_21 * L_APPROVED_RATE AS BASIS_21,
                BASIS_22 * L_APPROVED_RATE AS BASIS_22,
                BASIS_23 * L_APPROVED_RATE AS BASIS_23,
                BASIS_24 * L_APPROVED_RATE AS BASIS_24,
                BASIS_25 * L_APPROVED_RATE AS BASIS_25,
                BASIS_26 * L_APPROVED_RATE AS BASIS_26,
                BASIS_27 * L_APPROVED_RATE AS BASIS_27,
                BASIS_28 * L_APPROVED_RATE AS BASIS_28,
                BASIS_29 * L_APPROVED_RATE AS BASIS_29,
                BASIS_30 * L_APPROVED_RATE AS BASIS_30,
                BASIS_31 * L_APPROVED_RATE AS BASIS_31,
                BASIS_32 * L_APPROVED_RATE AS BASIS_32,
                BASIS_33 * L_APPROVED_RATE AS BASIS_33,
                BASIS_34 * L_APPROVED_RATE AS BASIS_34,
                BASIS_35 * L_APPROVED_RATE AS BASIS_35,
                BASIS_36 * L_APPROVED_RATE AS BASIS_36,
                BASIS_37 * L_APPROVED_RATE AS BASIS_37,
                BASIS_38 * L_APPROVED_RATE AS BASIS_38,
                BASIS_39 * L_APPROVED_RATE AS BASIS_39,
                BASIS_40 * L_APPROVED_RATE AS BASIS_40,
                BASIS_41 * L_APPROVED_RATE AS BASIS_41,
                BASIS_42 * L_APPROVED_RATE AS BASIS_42,
                BASIS_43 * L_APPROVED_RATE AS BASIS_43,
                BASIS_44 * L_APPROVED_RATE AS BASIS_44,
                BASIS_45 * L_APPROVED_RATE AS BASIS_45,
                BASIS_46 * L_APPROVED_RATE AS BASIS_46,
                BASIS_47 * L_APPROVED_RATE AS BASIS_47,
                BASIS_48 * L_APPROVED_RATE AS BASIS_48,
                BASIS_49 * L_APPROVED_RATE AS BASIS_49,
                BASIS_50 * L_APPROVED_RATE AS BASIS_50,
                BASIS_51 * L_APPROVED_RATE AS BASIS_51,
                BASIS_52 * L_APPROVED_RATE AS BASIS_52,
                BASIS_53 * L_APPROVED_RATE AS BASIS_53,
                BASIS_54 * L_APPROVED_RATE AS BASIS_54,
                BASIS_55 * L_APPROVED_RATE AS BASIS_55,
                BASIS_56 * L_APPROVED_RATE AS BASIS_56,
                BASIS_57 * L_APPROVED_RATE AS BASIS_57,
                BASIS_58 * L_APPROVED_RATE AS BASIS_58,
                BASIS_59 * L_APPROVED_RATE AS BASIS_59,
                BASIS_60 * L_APPROVED_RATE AS BASIS_60,
                BASIS_61 * L_APPROVED_RATE AS BASIS_61,
                BASIS_62 * L_APPROVED_RATE AS BASIS_62,
                BASIS_63 * L_APPROVED_RATE AS BASIS_63,
                BASIS_64 * L_APPROVED_RATE AS BASIS_64,
                BASIS_65 * L_APPROVED_RATE AS BASIS_65,
                BASIS_66 * L_APPROVED_RATE AS BASIS_66,
                BASIS_67 * L_APPROVED_RATE AS BASIS_67,
                BASIS_68 * L_APPROVED_RATE AS BASIS_68,
                BASIS_69 * L_APPROVED_RATE AS BASIS_69,
                BASIS_70 * L_APPROVED_RATE AS BASIS_70
           from LS_PEND_BASIS
          where LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID;

      L_MSG := 'Adding to ledger basis table';
      merge into CPR_LDG_BASIS A
      using (select L_ASSET_ID as ASSET_ID,
                BASIS_1 * L_APPROVED_RATE AS BASIS_1,
                BASIS_2 * L_APPROVED_RATE AS BASIS_2,
                BASIS_3 * L_APPROVED_RATE AS BASIS_3,
                BASIS_4 * L_APPROVED_RATE AS BASIS_4,
                BASIS_5 * L_APPROVED_RATE AS BASIS_5,
                BASIS_6 * L_APPROVED_RATE AS BASIS_6,
                BASIS_7 * L_APPROVED_RATE AS BASIS_7,
                BASIS_8 * L_APPROVED_RATE AS BASIS_8,
                BASIS_9 * L_APPROVED_RATE AS BASIS_9,
                BASIS_10 * L_APPROVED_RATE AS BASIS_10,
                BASIS_11 * L_APPROVED_RATE AS BASIS_11,
                BASIS_12 * L_APPROVED_RATE AS BASIS_12,
                BASIS_13 * L_APPROVED_RATE AS BASIS_13,
                BASIS_14 * L_APPROVED_RATE AS BASIS_14,
                BASIS_15 * L_APPROVED_RATE AS BASIS_15,
                BASIS_16 * L_APPROVED_RATE AS BASIS_16,
                BASIS_17 * L_APPROVED_RATE AS BASIS_17,
                BASIS_18 * L_APPROVED_RATE AS BASIS_18,
                BASIS_19 * L_APPROVED_RATE AS BASIS_19,
                BASIS_20 * L_APPROVED_RATE AS BASIS_20,
                BASIS_21 * L_APPROVED_RATE AS BASIS_21,
                BASIS_22 * L_APPROVED_RATE AS BASIS_22,
                BASIS_23 * L_APPROVED_RATE AS BASIS_23,
                BASIS_24 * L_APPROVED_RATE AS BASIS_24,
                BASIS_25 * L_APPROVED_RATE AS BASIS_25,
                BASIS_26 * L_APPROVED_RATE AS BASIS_26,
                BASIS_27 * L_APPROVED_RATE AS BASIS_27,
                BASIS_28 * L_APPROVED_RATE AS BASIS_28,
                BASIS_29 * L_APPROVED_RATE AS BASIS_29,
                BASIS_30 * L_APPROVED_RATE AS BASIS_30,
                BASIS_31 * L_APPROVED_RATE AS BASIS_31,
                BASIS_32 * L_APPROVED_RATE AS BASIS_32,
                BASIS_33 * L_APPROVED_RATE AS BASIS_33,
                BASIS_34 * L_APPROVED_RATE AS BASIS_34,
                BASIS_35 * L_APPROVED_RATE AS BASIS_35,
                BASIS_36 * L_APPROVED_RATE AS BASIS_36,
                BASIS_37 * L_APPROVED_RATE AS BASIS_37,
                BASIS_38 * L_APPROVED_RATE AS BASIS_38,
                BASIS_39 * L_APPROVED_RATE AS BASIS_39,
                BASIS_40 * L_APPROVED_RATE AS BASIS_40,
                BASIS_41 * L_APPROVED_RATE AS BASIS_41,
                BASIS_42 * L_APPROVED_RATE AS BASIS_42,
                BASIS_43 * L_APPROVED_RATE AS BASIS_43,
                BASIS_44 * L_APPROVED_RATE AS BASIS_44,
                BASIS_45 * L_APPROVED_RATE AS BASIS_45,
                BASIS_46 * L_APPROVED_RATE AS BASIS_46,
                BASIS_47 * L_APPROVED_RATE AS BASIS_47,
                BASIS_48 * L_APPROVED_RATE AS BASIS_48,
                BASIS_49 * L_APPROVED_RATE AS BASIS_49,
                BASIS_50 * L_APPROVED_RATE AS BASIS_50,
                BASIS_51 * L_APPROVED_RATE AS BASIS_51,
                BASIS_52 * L_APPROVED_RATE AS BASIS_52,
                BASIS_53 * L_APPROVED_RATE AS BASIS_53,
                BASIS_54 * L_APPROVED_RATE AS BASIS_54,
                BASIS_55 * L_APPROVED_RATE AS BASIS_55,
                BASIS_56 * L_APPROVED_RATE AS BASIS_56,
                BASIS_57 * L_APPROVED_RATE AS BASIS_57,
                BASIS_58 * L_APPROVED_RATE AS BASIS_58,
                BASIS_59 * L_APPROVED_RATE AS BASIS_59,
                BASIS_60 * L_APPROVED_RATE AS BASIS_60,
                BASIS_61 * L_APPROVED_RATE AS BASIS_61,
                BASIS_62 * L_APPROVED_RATE AS BASIS_62,
                BASIS_63 * L_APPROVED_RATE AS BASIS_63,
                BASIS_64 * L_APPROVED_RATE AS BASIS_64,
                BASIS_65 * L_APPROVED_RATE AS BASIS_65,
                BASIS_66 * L_APPROVED_RATE AS BASIS_66,
                BASIS_67 * L_APPROVED_RATE AS BASIS_67,
                BASIS_68 * L_APPROVED_RATE AS BASIS_68,
                BASIS_69 * L_APPROVED_RATE AS BASIS_69,
                BASIS_70 * L_APPROVED_RATE AS BASIS_70
               from LS_PEND_BASIS
              where LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID) B
      on (A.ASSET_ID = B.ASSET_ID)
      when matched then
         update
            set A.BASIS_1 = A.BASIS_1 + B.BASIS_1, A.BASIS_2 = A.BASIS_2 + B.BASIS_2,
                A.BASIS_3 = A.BASIS_3 + B.BASIS_3, A.BASIS_4 = A.BASIS_4 + B.BASIS_4,
                A.BASIS_5 = A.BASIS_5 + B.BASIS_5, A.BASIS_6 = A.BASIS_6 + B.BASIS_6,
                A.BASIS_7 = A.BASIS_7 + B.BASIS_7, A.BASIS_8 = A.BASIS_8 + B.BASIS_8,
                A.BASIS_9 = A.BASIS_9 + B.BASIS_9, A.BASIS_10 = A.BASIS_10 + B.BASIS_10,
                A.BASIS_11 = A.BASIS_11 + B.BASIS_11, A.BASIS_12 = A.BASIS_12 + B.BASIS_12,
                A.BASIS_13 = A.BASIS_13 + B.BASIS_13, A.BASIS_14 = A.BASIS_14 + B.BASIS_14,
                A.BASIS_15 = A.BASIS_15 + B.BASIS_15, A.BASIS_16 = A.BASIS_16 + B.BASIS_16,
                A.BASIS_17 = A.BASIS_17 + B.BASIS_17, A.BASIS_18 = A.BASIS_18 + B.BASIS_18,
                A.BASIS_19 = A.BASIS_19 + B.BASIS_19, A.BASIS_20 = A.BASIS_20 + B.BASIS_20,
                A.BASIS_21 = A.BASIS_21 + B.BASIS_21, A.BASIS_22 = A.BASIS_22 + B.BASIS_22,
                A.BASIS_23 = A.BASIS_23 + B.BASIS_23, A.BASIS_24 = A.BASIS_24 + B.BASIS_24,
                A.BASIS_25 = A.BASIS_25 + B.BASIS_25, A.BASIS_26 = A.BASIS_26 + B.BASIS_26,
                A.BASIS_27 = A.BASIS_27 + B.BASIS_27, A.BASIS_28 = A.BASIS_28 + B.BASIS_28,
                A.BASIS_29 = A.BASIS_29 + B.BASIS_29, A.BASIS_30 = A.BASIS_30 + B.BASIS_30,
                A.BASIS_31 = A.BASIS_31 + B.BASIS_31, A.BASIS_32 = A.BASIS_32 + B.BASIS_32,
                A.BASIS_33 = A.BASIS_33 + B.BASIS_33, A.BASIS_34 = A.BASIS_34 + B.BASIS_34,
                A.BASIS_35 = A.BASIS_35 + B.BASIS_35, A.BASIS_36 = A.BASIS_36 + B.BASIS_36,
                A.BASIS_37 = A.BASIS_37 + B.BASIS_37, A.BASIS_38 = A.BASIS_38 + B.BASIS_38,
                A.BASIS_39 = A.BASIS_39 + B.BASIS_39, A.BASIS_40 = A.BASIS_40 + B.BASIS_40,
                A.BASIS_41 = A.BASIS_41 + B.BASIS_41, A.BASIS_42 = A.BASIS_42 + B.BASIS_42,
                A.BASIS_43 = A.BASIS_43 + B.BASIS_43, A.BASIS_44 = A.BASIS_44 + B.BASIS_44,
                A.BASIS_45 = A.BASIS_45 + B.BASIS_45, A.BASIS_46 = A.BASIS_46 + B.BASIS_46,
                A.BASIS_47 = A.BASIS_47 + B.BASIS_47, A.BASIS_48 = A.BASIS_48 + B.BASIS_48,
                A.BASIS_49 = A.BASIS_49 + B.BASIS_49, A.BASIS_50 = A.BASIS_50 + B.BASIS_50,
                A.BASIS_51 = A.BASIS_51 + B.BASIS_51, A.BASIS_52 = A.BASIS_52 + B.BASIS_52,
                A.BASIS_53 = A.BASIS_53 + B.BASIS_53, A.BASIS_54 = A.BASIS_54 + B.BASIS_54,
                A.BASIS_55 = A.BASIS_55 + B.BASIS_55, A.BASIS_56 = A.BASIS_56 + B.BASIS_56,
                A.BASIS_57 = A.BASIS_57 + B.BASIS_57, A.BASIS_58 = A.BASIS_58 + B.BASIS_58,
                A.BASIS_59 = A.BASIS_59 + B.BASIS_59, A.BASIS_60 = A.BASIS_60 + B.BASIS_60,
                A.BASIS_61 = A.BASIS_61 + B.BASIS_61, A.BASIS_62 = A.BASIS_62 + B.BASIS_62,
                A.BASIS_63 = A.BASIS_63 + B.BASIS_63, A.BASIS_64 = A.BASIS_64 + B.BASIS_64,
                A.BASIS_65 = A.BASIS_65 + B.BASIS_65, A.BASIS_66 = A.BASIS_66 + B.BASIS_66,
                A.BASIS_67 = A.BASIS_67 + B.BASIS_67, A.BASIS_68 = A.BASIS_68 + B.BASIS_68,
                A.BASIS_69 = A.BASIS_69 + B.BASIS_69, A.BASIS_70 = A.BASIS_70 + B.BASIS_70
      when not matched then
         insert
            (ASSET_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6, BASIS_7, BASIS_8,
             BASIS_9, BASIS_10, BASIS_11, BASIS_12, BASIS_13, BASIS_14, BASIS_15, BASIS_16, BASIS_17,
             BASIS_18, BASIS_19, BASIS_20, BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25,
             BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32, BASIS_33,
             BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41,
             BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49,
             BASIS_50, BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57,
             BASIS_58, BASIS_59, BASIS_60, BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65,
             BASIS_66, BASIS_67, BASIS_68, BASIS_69, BASIS_70)
         values
            (B.ASSET_ID, B.BASIS_1, B.BASIS_2, B.BASIS_3, B.BASIS_4, B.BASIS_5, B.BASIS_6,
             B.BASIS_7, B.BASIS_8, B.BASIS_9, B.BASIS_10, B.BASIS_11, B.BASIS_12, B.BASIS_13,
             B.BASIS_14, B.BASIS_15, B.BASIS_16, B.BASIS_17, B.BASIS_18, B.BASIS_19, B.BASIS_20,
             B.BASIS_21, B.BASIS_22, B.BASIS_23, B.BASIS_24, B.BASIS_25, B.BASIS_26, B.BASIS_27,
             B.BASIS_28, B.BASIS_29, B.BASIS_30, B.BASIS_31, B.BASIS_32, B.BASIS_33, B.BASIS_34,
             B.BASIS_35, B.BASIS_36, B.BASIS_37, B.BASIS_38, B.BASIS_39, B.BASIS_40, B.BASIS_41,
             B.BASIS_42, B.BASIS_43, B.BASIS_44, B.BASIS_45, B.BASIS_46, B.BASIS_47, B.BASIS_48,
             B.BASIS_49, B.BASIS_50, B.BASIS_51, B.BASIS_52, B.BASIS_53, B.BASIS_54, B.BASIS_55,
             B.BASIS_56, B.BASIS_57, B.BASIS_58, B.BASIS_59, B.BASIS_60, B.BASIS_61, B.BASIS_62,
             B.BASIS_63, B.BASIS_64, B.BASIS_65, B.BASIS_66, B.BASIS_67, B.BASIS_68, B.BASIS_69,
             B.BASIS_70);

      L_MSG := 'Loading cpr_depr';
      merge into CPR_DEPR A
      using (select L_ASSET_ID as ASSET_ID,
                    PB.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                    l_init_life as EXPECTED_LIFE,
                    PB.POSTING_AMOUNT * L_APPROVED_RATE as AMOUNT,
                    DMR.RATE / 12 as MONTH_RATE,
                    L_LS_PEND_TRANS.COMPANY_ID as COMPANY_ID,
                    DG.MID_PERIOD_METHOD as MID_PERIOD_METHOD,
                    DG.MID_PERIOD_CONV as MID_PERIOD_CONV,
                    L_DG_ID as DEPR_GROUP_ID,
                    DMR.DEPR_METHOD_ID as DEPR_METHOD_ID,
                    DG.TRUE_UP_CPR_DEPR as TRUE_UP_CPR_DEPR,
                    L_MONTH as GL_POSTING_MO_YR,
          case when dg.mid_period_method = 'FERC' then Nvl(L_RESERVE_ADJ,0) * L_APPROVED_RATE else 0 end AS reserve_adjustment,
          case when (L_LS_ASSET.ESTIMATED_RESIDUAL = 0
                      and lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual', L_LS_PEND_TRANS.COMPANY_ID))) = 'yes') /* WMD */
            then L_LS_ASSET.GUARANTEED_RESIDUAL_AMOUNT
            else round(L_LS_ASSET.ESTIMATED_RESIDUAL * L_LS_ASSET.FMV, 2) end * L_APPROVED_RATE as estimated_salvage
               from DEPR_GROUP DG,
                    LS_PEND_SET_OF_BOOKS PB,
                    (select DDD.DEPR_METHOD_ID,
                            DDD.RATE,
                            DDD.SET_OF_BOOKS_ID,
                            DDD.EXPECTED_AVERAGE_LIFE,
                            ROW_NUMBER() OVER(partition by DDD.DEPR_METHOD_ID, DDD.SET_OF_BOOKS_ID order by DDD.EFFECTIVE_DATE desc) as THE_ROW
                       from DEPR_METHOD_RATES DDD) DMR
              where DG.DEPR_GROUP_ID = L_DG_ID
                and DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
                and PB.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
                and PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID
                and DMR.THE_ROW = 1) B
      on (A.ASSET_ID = B.ASSET_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.GL_POSTING_MO_YR = B.GL_POSTING_MO_YR and A.DEPR_GROUP_ID = B.DEPR_GROUP_ID)
      when matched then
         update
            set NET_ADDS_AND_ADJUST = NET_ADDS_AND_ADJUST + B.AMOUNT,
                ASSET_DOLLARS = ASSET_DOLLARS + B.AMOUNT,
        ESTIMATED_SALVAGE = case when (asset_dollars + b.amount) = 0 then 0
                    else B.ESTIMATED_SALVAGE / (asset_dollars + b.amount) END,
        RESERVE_ADJUSTMENT = B.RESERVE_ADJUSTMENT
      when not matched then
         insert
            (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, INIT_LIFE, REMAINING_LIFE,
             ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS, TRANSFERS_IN,
             TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH, SALVAGE_DOLLARS, RESERVE_ADJUSTMENT,
             COST_OF_REMOVAL, RESERVE_TRANS_IN, RESERVE_TRANS_OUT, DEPR_EXP_ADJUST,
             OTHER_CREDITS_AND_ADJUST, GAIN_LOSS, DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE,
             BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE, YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE,
             PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB, MONTH_RATE, COMPANY_ID, MID_PERIOD_METHOD,
             MID_PERIOD_CONV, DEPR_GROUP_ID, DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR,
             SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT,
             IMPAIRMENT_EXPENSE_AMOUNT, IMPAIRMENT_ASSET_ACTIVITY_SALV,
             IMPAIRMENT_ASSET_BEGIN_BALANCE)
         values
            (B.ASSET_ID, B.SET_OF_BOOKS_ID, B.GL_POSTING_MO_YR, B.EXPECTED_LIFE, B.EXPECTED_LIFE,
       case when b.amount = 0 then 0 else B.ESTIMATED_SALVAGE / b.amount end,
       0, B.AMOUNT, 0, 0, 0, B.AMOUNT, 0, 0, b.reserve_adjustment, 0, 0, 0, 0, 0, 0, 0,
       0, 0, 0, 0, 0, 0, 0, 0,
             B.MONTH_RATE, B.COMPANY_ID, B.MID_PERIOD_METHOD, B.MID_PERIOD_CONV, B.DEPR_GROUP_ID, 0,
             B.DEPR_METHOD_ID, B.TRUE_UP_CPR_DEPR, 0, 0, 0, 0, 0, 0, 0);

      L_MSG:='Updating CPR Depr Init Life if before in service date';
      update cpr_depr
      set remaining_life = l_init_life,
          init_life = l_init_life
      where asset_id = L_ASSET_ID
        and gl_posting_mo_yr <= (select min(payment_term_date) from ls_ilr_payment_term where L_LS_PEND_TRANS.ILR_ID = ILR_ID AND REVISION = L_LS_PEND_TRANS.REVISION);


      L_MSG := 'Updating Account Summary';
      merge into ACCOUNT_SUMMARY A
      using (select L_LS_PEND_TRANS.COMPANY_ID as COMPANY,
                    PB.SET_OF_BOOKS_ID as SET_OF_BOOKS,
                    L_GL_ACCT_ID as ACCOUNT,
                    L_LS_PEND_TRANS.BUS_SEGMENT_ID as BUSINESS_SEGMENT,
                    L_LS_PEND_TRANS.SUB_ACCOUNT_ID as SUB_ACCOUNT,
                    L_LS_PEND_TRANS.UTILITY_ACCOUNT_ID as UTILITY_ACCOUNT,
                    L_MAJOR_LOC as MAJOR_LOCATION,
                    L_MONTH as GL_POSTING_MO_YR, --GL_POSTING_MO_YR
                    (1 - L_ADJ) * PB.POSTING_AMOUNT * L_APPROVED_RATE as ADDITION,
                    L_ADJ * PB.POSTING_AMOUNT * L_APPROVED_RATE as ADJUSTMENTS
               from LS_PEND_SET_OF_BOOKS PB
              where PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID) B
      on (A.COMPANY_ID = B.COMPANY and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS and A.GL_ACCOUNT_ID = B.ACCOUNT and A.BUS_SEGMENT_ID = B.BUSINESS_SEGMENT and A.SUB_ACCOUNT_ID = B.SUB_ACCOUNT and A.UTILITY_ACCOUNT_ID = B.UTILITY_ACCOUNT and A.MAJOR_LOCATION_ID = B.MAJOR_LOCATION and A.GL_POSTING_MO_YR = B.GL_POSTING_MO_YR)
      when matched then
         update
            set A.ENDING_BALANCE = A.ENDING_BALANCE + B.ADDITION + B.ADJUSTMENTS,
                A.ADDITIONS = A.ADDITIONS + B.ADDITION,
                A.ADJUSTMENTS = A.ADJUSTMENTS + B.ADJUSTMENTS
      when not matched then
         insert
            (SET_OF_BOOKS_ID, COMPANY_ID, GL_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
             UTILITY_ACCOUNT_ID, MAJOR_LOCATION_ID, GL_POSTING_MO_YR, BEGINNING_BALANCE,
             ACCT_SUMM_STATUS, RETIREMENTS, ADDITIONS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS,
             ENDING_BALANCE)
         values
            (B.SET_OF_BOOKS, B.COMPANY, B.ACCOUNT, B.BUSINESS_SEGMENT, B.SUB_ACCOUNT,
             B.UTILITY_ACCOUNT, B.MAJOR_LOCATION, B.GL_POSTING_MO_YR, 0, 1, 0, B.ADDITION,
             0, 0, B.ADJUSTMENTS, B.ADDITION + B.ADJUSTMENTS);

      L_MSG := 'Updating Depr Ledger';
      merge into DEPR_LEDGER A
      using (select DG2.DEPR_GROUP_ID as DEPR_GROUP,
                    PB.SET_OF_BOOKS_ID as SET_OF_BOOKS,
                    L_MONTH as GL_POSTING_MO_YR, --GL_POSTING_MO_YR
                    (1 - L_ADJ) * PB.POSTING_AMOUNT * L_APPROVED_RATE as ADDITION,
                    L_ADJ * PB.POSTING_AMOUNT * L_APPROVED_RATE as ADJUSTMENTS
               from LS_PEND_SET_OF_BOOKS PB,
                    (select DG.DEPR_GROUP_ID,
                            DG.COMPANY_ID,
                            DMR.SET_OF_BOOKS_ID,
                            ROW_NUMBER() OVER(partition by DMR.SET_OF_BOOKS_ID, DMR.DEPR_METHOD_ID order by DMR.EFFECTIVE_DATE desc) as THE_ROW
                       from DEPR_METHOD_RATES DMR, DEPR_GROUP DG
                      where DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
                        and DG.DEPR_GROUP_ID = L_DG_ID) DG2
              where DG2.THE_ROW = 1
                and PB.SET_OF_BOOKS_ID = DG2.SET_OF_BOOKS_ID
                and PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID) B
      on (A.DEPR_GROUP_ID = B.DEPR_GROUP and A.GL_POST_MO_YR = B.GL_POSTING_MO_YR and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS)
      when matched then
         update
            set A.END_BALANCE = A.END_BALANCE + B.ADDITION + B.ADJUSTMENTS,
                A.ADDITIONS = A.ADDITIONS + B.ADDITION,
                A.ADJUSTMENTS = A.ADJUSTMENTS + B.ADJUSTMENTS;

--      -- set the approved revision
--      update LS_ASSET
--         set APPROVED_REVISION = L_LS_PEND_TRANS.REVISION,
--             IN_SERVICE_DATE = NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE, L_MONTH), LS_ASSET_STATUS_ID = 3,
--          EXPECTED_LIFE = L_INIT_LIFE
--       where LS_ASSET_ID = A_LS_ASSET_ID;

    /* WMD Update Property Tax Date and Cost if they are null */
    L_MSG :='Update Property Tax Date if null';
    update LS_ASSET
    set property_tax_date = in_service_date
    where property_tax_date is null
      and ls_asset_id = A_LS_ASSET_ID;

    L_MSG :='Update Property Tax Amount if null';
    update ls_asset a
    set property_tax_amount =
    (select sum(cc.amount)
     from ls_component_charge cc, ls_component lc
     where cc.component_id = lc.component_id
      and cc.interim_interest_start_date is not null
      and nvl(lc.prop_tax_exempt,0) <> 1
      and a.ls_asset_id = lc.ls_asset_id)
    WHERE property_tax_amount is null
      and ls_asset_id = A_LS_ASSET_ID; /* WMD we need to make sure that null is all they want */
      -- only do the below on initial adds
      if L_ADJ = 0 then
         L_MSG := 'Adding class codes';
         insert into CLASS_CODE_CPR_LEDGER
            (CLASS_CODE_ID, ASSET_ID, value)
            select CLASS_CODE_ID, L_ASSET_ID, value
              from LS_PEND_CLASS_CODE
             where LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID;

         --* Link the LS_ASSET to the CPR
         L_MSG := 'Connecting leased asset to CPR';
         insert into LS_CPR_ASSET_MAP
            (LS_ASSET_ID, ASSET_ID, EFFECTIVE_DATE)
         values
            (L_LS_PEND_TRANS.LS_ASSET_ID, L_ASSET_ID, L_MONTH);

         L_MSG := 'Connecting activity and depr group';
         insert into CPR_ACT_DEPR_GROUP
            (ASSET_ID, GL_POSTING_MO_YR, DEPR_GROUP_ID)
         values
            (L_ASSET_ID, L_MONTH, L_DG_ID);
      end if; -- adjustment


      /* WMD Mark Components Posted */
      L_MSG:='Marking components posted to GL';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      update ls_component_charge cc
      set posted_to_gl =1
      where interim_interest_start_date is not null
        and posted_to_gl <> 1
        and component_id in (
          select lc.component_id
          from ls_asset la, ls_component lc
          where la.ls_asset_id = L_LS_PEND_TRANS.LS_ASSET_ID
            and lc.ls_asset_id = la.ls_asset_id
            and cc.component_id = lc.component_id);

      /* CJS 2/16/15 Need to readdress; ends log prematurely during transfers
      PKG_PP_LOG.P_END_LOG(); */
      L_MSG := 'OK';
      return L_MSG;
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_ADD_ASSET;

   -- Function to perform specific Leased Asset retirement
   -- return 1 on success, 0 on failure.  a_msg is an out param for messaging
   function F_RETIRE_ASSET(A_CPR_ASSET_ID        number,
                           A_ACCT_MONTH          date,
                           A_MONTH               date,
                           A_RETIREMENT_AMOUNT   number,
                           A_RETIREMENT_QUANTITY number,
                           A_GAIN_LOSS_AMOUNT    number,
                           A_DG_ID               number,
                           A_PT_ID               number,
                           A_MSG                 out varchar2) return number is
      L_LS_ASSET     LS_ASSET%rowtype;
      L_LS_ASSET_SCH LS_ASSET_SCHEDULE%rowtype;
      L_GL_ACCT_ID     GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_ST_ACCT_ID    GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_LT_ACCT_ID    GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_AMT_OBL      number(22, 2);
      L_PCT          number(22, 12);
      L_RTN          number;
      L_GL_JE_CODE   varchar2(35);
      L_ILR_ID       number;
      L_STATUS       varchar2(4000);
      L_SQLS         varchar2(2000);
    rtn            varchar2(5000);
    L_SCH_MONTH date;
    L_ASSET_IDS   PKG_LEASE_CALC.NUM_ARRAY;
    L_PEND_TRANS_COUNTER NUMBER;
	L_CURRENCY_FROM pend_transaction.currency_from%type;
	L_CURRENCY_TO pend_transaction.currency_to%type;
	L_EXCHANGE_RATE pend_transaction.exchange_rate%type;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_MSG := 'Starting to retire the leased asset for month: ' || TO_CHAR(A_MONTH, 'yyyymm');
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Accounting Month: ' || TO_CHAR(A_ACCT_MONTH, 'yyyymm');
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Quantity: ' || TO_CHAR(A_RETIREMENT_QUANTITY);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Amount: ' || TO_CHAR(A_RETIREMENT_AMOUNT);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Gain / Loss: ' || TO_CHAR(A_GAIN_LOSS_AMOUNT);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Associated Pending Transaction: ' || TO_CHAR(A_PT_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      --Get our LS_ASSET
      A_MSG := 'Get the leased asset record';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      select A.*
        into L_LS_ASSET
        from LS_ASSET A, LS_CPR_ASSET_MAP C
       where C.ASSET_ID = A_CPR_ASSET_ID
         and C.LS_ASSET_ID = A.LS_ASSET_ID;

      A_MSG:='Checking to see if depreciation has been calculated for the month';
      select count(1)
      into l_rtn
      from ls_process_control
      where company_id = L_LS_ASSET.COMPANY_ID
        and gl_posting_mo_yr = A_MONTH
        and depr_approved is not null;

      if l_rtn <> 1 then
        A_MSG:='You cannot retire this asset yet because depreciation has not been calculated and approved for the out of service month.';
        return -1;
      end if;

      /* WMD */
      A_MSG:='Getting GL Accounts';
      select IA.CAP_ASSET_ACCOUNT_ID, IA.ST_OBLIG_ACCOUNT_ID, IA.LT_OBLIG_ACCOUNT_ID
        into L_GL_ACCT_ID, L_ST_ACCT_ID, L_LT_ACCT_ID
        from LS_ILR_ACCOUNT IA, LS_ASSET LA
       where LA.ILR_ID = IA.ILR_ID
         and LA.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID;

      -- is this asset fully retired?
      if L_LS_ASSET.QUANTITY + A_RETIREMENT_QUANTITY = 0 then
         -- fully retired
         -- set the status to say retired
         L_LS_ASSET.LS_ASSET_STATUS_ID := 4;
      end if;

      L_PCT := -1 * A_RETIREMENT_QUANTITY / L_LS_ASSET.QUANTITY;

      A_MSG := 'Updating LS ASSET';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      -- update the capital cost and the quantity based on the retirement amount
      update LS_ASSET
         set QUANTITY = QUANTITY + A_RETIREMENT_QUANTITY,
             LS_ASSET_STATUS_ID = L_LS_ASSET.LS_ASSET_STATUS_ID, RETIREMENT_DATE = A_MONTH
       where LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID;

		/* WMD */
		/* CJS 4/21/17 Post isn't using external values for category, update here */
       A_MSG := 'Updating Asset ID on GL Transaction to LS Asset ID';
       PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
       update gl_transaction
       set asset_id = L_LS_ASSET.LS_ASSET_ID,
           source='LESSEE',
           description = 'TRANS TYPE: ' || trans_type,
		   gl_status_id = to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', L_LS_ASSET.COMPANY_ID)), 1)),
       gl_je_code = Nvl((SELECT external_je_code FROM standard_journal_entries s WHERE Trim(s.gl_je_code) = Trim(gl_transaction.gl_je_code)),gl_je_code)
       where month=A_ACCT_MONTH
        and asset_id = A_CPR_ASSET_ID
        and originator = 'POST'
        and trans_type in (3025, 3026, 3030, 3031);

        PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount || ' rows updated');
      -- normal asset retirement transactions are handled by POST
      -- ie 3025 and 3026.
      -- in this function only handle the lease specific entries (termination_penalty_amount: 3036 and 3037)

      select NVL(E.EXTERNAL_JE_CODE, E.GL_JE_CODE)
        into L_GL_JE_CODE
        from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
       where E.JE_ID = G.JE_ID
         and G.PROCESS_ID = 'LAM RETIREMENTS';

		-- get exchange rate information'
		SELECT currency_from, currency_to, exchange_rate
		INTO L_CURRENCY_FROM, L_CURRENCY_TO, L_EXCHANGE_RATE
		FROM pend_transaction
		WHERE pend_trans_id = A_PT_ID;

      -- loop over the set of books eligible for this asset
      A_MSG := 'Looping over set of books';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      for L_SOB in (select CS.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID, LCT.BOOK_SUMMARY_ID as BS_ID
                      from COMPANY_SET_OF_BOOKS CS,
                           SET_OF_BOOKS         S,
                           LS_ILR_OPTIONS       ILR,
                           LS_LEASE_CAP_TYPE    LCT
                     where CS.COMPANY_ID = L_LS_ASSET.COMPANY_ID
                       and CS.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                       and ILR.ILR_ID = L_LS_ASSET.ILR_ID
                       and ILR.REVISION = L_LS_ASSET.APPROVED_REVISION
                       and LCT.LS_LEASE_CAP_TYPE_ID = ILR.LEASE_CAP_TYPE_ID)
      loop
         A_MSG := '   Set of Books: ' || TO_CHAR(L_SOB.SET_OF_BOOKS_ID);
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         -- get the current month's row for the schedule for the current revision
         A_MSG := 'Get the current month schedule for leased asset id: ' ||
                  TO_CHAR(L_LS_ASSET.LS_ASSET_ID);
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

     select max(s.month)
           into L_SCH_MONTH
           from LS_ASSET_SCHEDULE S
          where S.MONTH <= A_MONTH
            and S.REVISION = L_LS_ASSET.APPROVED_REVISION
            and S.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
            and S.SET_OF_BOOKS_ID = L_SOB.SET_OF_BOOKS_ID;

         select S.*
           into L_LS_ASSET_SCH
           from LS_ASSET_SCHEDULE S
          where S.MONTH = L_SCH_MONTH
            and S.REVISION = L_LS_ASSET.APPROVED_REVISION
            and S.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
            and S.SET_OF_BOOKS_ID = L_SOB.SET_OF_BOOKS_ID;

        -- is there a termination penalty
     -- CJS 4/20/15 Only book actual termination amount

         A_MSG := A_MSG || '   Original Termination Amount: ' ||
                  TO_CHAR(NVL(L_LS_ASSET.TERMINATION_PENALTY_AMOUNT, 0));
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         A_MSG := A_MSG || '   Actual Termination Amount: ' ||
                  TO_CHAR(NVL(L_LS_ASSET.ACTUAL_TERMINATION_AMOUNT, 0));
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         if NVL(L_LS_ASSET.ACTUAL_TERMINATION_AMOUNT, 0) /*- L_LS_ASSET.TERMINATION_PENALTY_AMOUNT*/ <> 0 then
            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                               3036,
                                               L_LS_ASSET.ACTUAL_TERMINATION_AMOUNT /*- L_LS_ASSET.TERMINATION_PENALTY_AMOUNT*/,
                                               0,
                                               A_DG_ID,
                                               L_LS_ASSET.WORK_ORDER_ID,
                                               0,
                                               1,
                                               A_PT_ID,
                                               L_LS_ASSET.COMPANY_ID,
                                               A_ACCT_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               L_SOB.SET_OF_BOOKS_ID,
											   L_EXCHANGE_RATE,
											   L_CURRENCY_FROM,
											   L_CURRENCY_TO,
                                               A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;

            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                               3037,
                                               L_LS_ASSET.ACTUAL_TERMINATION_AMOUNT /*- L_LS_ASSET.TERMINATION_PENALTY_AMOUNT*/,
                                               0,
                                               A_DG_ID,
                                               L_LS_ASSET.WORK_ORDER_ID,
                                               0,
                                               1,
                                               A_PT_ID,
                                               L_LS_ASSET.COMPANY_ID,
                                               A_ACCT_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_SOB.SET_OF_BOOKS_ID,
											   L_EXCHANGE_RATE,
											   L_CURRENCY_FROM,
											   L_CURRENCY_TO,
                                               A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;
         end if; -- termination penalty

         -- check to see if the ilr is capital for this set of books
         L_SQLS := 'select to_char(basis_' || TO_CHAR(L_SOB.BS_ID) ||
                   '_indicator) from set_of_books s where set_of_books_id = ' ||
                   TO_CHAR(L_SOB.SET_OF_BOOKS_ID);

         A_MSG := '   Set of Books (Cap or Operating): ' || L_SQLS;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

         L_RTN := TO_NUMBER(PP_MISC_PKG.DYNAMIC_SELECT(L_SQLS));
         PKG_PP_LOG.P_WRITE_MESSAGE('      RETURN: ' || TO_CHAR(L_RTN));

         -- plus additional g/l for the st / lt obligation clearing (3027 G/L CR, 3028 ST DR, 2029 LT DR)
         if L_RTN = 1 and L_LS_ASSET.IS_EARLY_RET = 1 then
            A_MSG := '     CAPITAL and Early Retirement: ' || L_SQLS;
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            -- this is an early retirement
            -- so calc additional G/L by clearing out the obligations
            if(L_LS_ASSET_SCH.BEG_LT_OBLIGATION) = 0 then
                L_AMT_OBL := L_PCT *
                (L_LS_ASSET_SCH.END_OBLIGATION -
                CASE WHEN L_LS_ASSET.ESTIMATED_RESIDUAL = 0 AND lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual', L_LS_ASSET.COMPANY_ID))) = 'yes' THEN
                  NVL(L_LS_ASSET.GUARANTEED_RESIDUAL_AMOUNT,0)
                ELSE
                  /* CJS 3/7/17 Add system control to account for capitalization of estimated residual */
                  CASE WHEN lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', L_LS_ASSET.COMPANY_ID))) = 'no' THEN
                     0
                  else round(L_LS_ASSET.ESTIMATED_RESIDUAL * L_LS_ASSET.FMV, 2)
                  END
                end
                - NVL(L_LS_ASSET_SCH.END_LT_OBLIGATION, 0));

            else
                L_AMT_OBL := L_PCT *
                (L_LS_ASSET_SCH.END_OBLIGATION - NVL(L_LS_ASSET_SCH.END_LT_OBLIGATION, 0));
            end if;

            if L_AMT_OBL <> 0 then
               -- book the G/L/ credit
               L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3027,
                                                  L_AMT_OBL,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  0,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
											   L_EXCHANGE_RATE,
											   L_CURRENCY_FROM,
											   L_CURRENCY_TO,
                                                  A_MSG);
               if L_RTN = -1 then

                  return 0;
               end if;

               -- book the ST CLEAR DR
               L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3028,
                                                  L_AMT_OBL,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  L_ST_ACCT_ID,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
											   L_EXCHANGE_RATE,
											   L_CURRENCY_FROM,
											   L_CURRENCY_TO,
                                                  A_MSG);
               if L_RTN = -1 then

                  return 0;
               end if;
            end if; -- end clearing out short term obligation

            if(L_LS_ASSET_SCH.BEG_LT_OBLIGATION) <> 0 then
                L_AMT_OBL := L_PCT * ( L_LS_ASSET_SCH.END_LT_OBLIGATION -
                                       CASE WHEN L_LS_ASSET.ESTIMATED_RESIDUAL = 0 AND lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual', L_LS_ASSET.COMPANY_ID))) = 'yes' THEN
                                        NVL(L_LS_ASSET.GUARANTEED_RESIDUAL_AMOUNT,0)
                                       ELSE
                                        /* CJS 3/7/17 Add system control to account for capitalization of estimated residual */
                                        CASE WHEN lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', L_LS_ASSET.COMPANY_ID))) = 'no' THEN
                                          0
                                        else round(L_LS_ASSET.ESTIMATED_RESIDUAL * L_LS_ASSET.FMV, 2)
                                        end
                                       end);
            else
                L_AMT_OBL := L_PCT * L_LS_ASSET_SCH.END_LT_OBLIGATION ;
            end if;

            if NVL(L_AMT_OBL, 0) <> 0 then
               -- book the G/L/ credit
               L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3027,
                                                  L_AMT_OBL,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  0,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
											   L_EXCHANGE_RATE,
											   L_CURRENCY_FROM,
											   L_CURRENCY_TO,
                                                  A_MSG);
               if L_RTN = -1 then

                  return 0;
               end if;

               -- book the LT CLEAR DR
               L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3029,
                                                  L_AMT_OBL,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  L_LT_ACCT_ID,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
											   L_EXCHANGE_RATE,
											   L_CURRENCY_FROM,
											   L_CURRENCY_TO,
                                                  A_MSG);
               if L_RTN = -1 then

                  return 0;
               end if;
            end if; -- end clearing out long term obligation
         end if; -- end if capital and early retirement (to calc additional gain loss)
      end loop; -- loop over set of books

      if L_LS_ASSET.LS_ASSET_STATUS_ID = 4 then
         A_MSG := 'Fully retired, REMOVING asset from association to ILR';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         delete from LS_ILR_ASSET_MAP
          where LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
            and REVISION =
                (select ILR.CURRENT_REVISION from LS_ILR ILR where ILR_ID = L_LS_ASSET.ILR_ID);

    if L_LS_ASSET.ACTUAL_RESIDUAL_AMOUNT <> 0 then
      A_MSG := 'Processing actual residual amount';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      L_ASSET_IDS(1) := L_LS_ASSET.LS_ASSET_ID;

      A_MSG := PKG_LEASE_CALC.F_PROCESS_RESIDUAL(A_LS_ASSET_IDS => L_ASSET_IDS, A_END_LOG => 0);
      if A_MSG <> 'OK' then
        PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        return 0;
      end if;
    end if;

      end if;

     /* WMD 20160809 DO NOT DO THE PROCESSING BELOW IF WE STILL HAVE RETIREMENT TRANSACTIONS PENDING */
     A_MSG := 'Checking for other pending transactions associated with this ILR';
     SELECT COUNT(1)
     INTO L_PEND_TRANS_COUNTER
     FROM PEND_TRANSACTION
     WHERE LDG_ASSET_ID in (SELECT ASSET_ID
                           FROM LS_CPR_ASSET_MAP
                           WHERE LS_ASSET_ID IN (SELECT LS_ASSET_ID FROM LS_ASSET WHERE ILR_ID = L_LS_ASSET.ILR_ID AND LS_ASSET_ID <> L_LS_ASSET.LS_ASSET_ID))
      and trim(activity_code) = 'URGL';



    IF L_PEND_TRANS_COUNTER <> 0 THEN
      A_MSG:='Other pending transactions exist for this ILR... Waiting until all are posted to send for approval';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    ELSE
      -- Handle ILR Status changes if all assets under the ILR are retired
      L_RTN := 0;
      select count(1)
        into L_RTN
        from LS_ASSET
       where ILR_ID = L_LS_ASSET.ILR_ID
         and LS_ASSET_STATUS_ID <> 4;

      if L_RTN = 0 then
         A_MSG := 'Retiring the ILR';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

         update LS_ILR
         set ILR_STATUS_ID = 3,
             funding_status_id = 5
         where ILR_ID = L_LS_ASSET.ILR_ID;
      else
         L_RTN := 0;
         -- this is the current pending revision for the ilr
         A_MSG := 'Finding the pending revision for the ILR';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

     select count(1)
     into l_rtn
     from LS_ILR_APPROVAL L
          where L.ILR_ID = L_LS_ASSET.ILR_ID
            and L.APPROVAL_STATUS_ID = 2;


    if l_rtn = 1 then
       select L.REVISION
         into L_RTN
         from LS_ILR_APPROVAL L
        where L.ILR_ID = L_LS_ASSET.ILR_ID
        and L.APPROVAL_STATUS_ID = 2;

       A_MSG := 'Approving ILR';
       PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
       L_RTN := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(L_LS_ASSET.ILR_ID, L_RTN, L_STATUS);
       if L_RTN <> 1 then

        return 0;
       end if;
    end if;
      end if;


  END IF; /* END WMD CHANGES 20160809 */


      --POST.exe needs to be checking for a return value of 1.

  /* CJS Adding call to retirement revision function; 0 is retirement, 1 is transfer */
  if L_PCT = 1 then --only create retirement revision if asset is fully retiring
    A_MSG:='Creating retirement revision';
    PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    L_RTN := PKG_LEASE_ILR.F_RETIREREVISION(L_LS_ASSET.ILR_ID, L_LS_ASSET.LS_ASSET_ID, A_MONTH, 0, A_ACCT_MONTH);
    if L_RTN <> 1 then
       PKG_PP_LOG.P_WRITE_MESSAGE(L_RTN);
       PKG_PP_LOG.P_END_LOG();
       return 0;
    end if;
    A_MSG:='Clear out Tax Rows for Future Months';
    delete from ls_monthly_tax
    where ls_asset_id = L_LS_ASSET.LS_ASSET_ID
      and gl_posting_mo_yr >A_MONTH;


   if L_LS_ASSET_SCH.END_LT_OBLIGATION > 0 then
   update ls_asset
   set early_term_end_lt_obligation = nvl(L_LS_ASSET_SCH.END_LT_OBLIGATION,0)
   where ls_asset_id = L_LS_ASSET.LS_ASSET_ID;
   end if;
  end if;

  A_MSG:=F_FOOT_DEPR(A_CPR_ASSET_ID, A_ACCT_MONTH);

  IF A_MSG <> 'OK' THEN
    RETURN -1;
  END IF;

  PKG_PP_LOG.P_END_LOG();

      return 1;
   exception
      when others then
         A_MSG := SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || A_MSG);
     PKG_PP_LOG.P_END_LOG();
         return 0;
   end F_RETIRE_ASSET;

   -- Function to perform specific Leased Asset transfer from
   function F_TRANSFER_ASSET_FROM(A_CPR_ASSET_ID number,
                                  A_AMOUNT       number,
                                  A_QUANTITY     number,
                                  A_PT_ID        number,
                                  A_MSG          out varchar2) return number is
      L_LS_ASSET_ID number;
      OLD_QUANTITY  number;
      L_REVISION    number;
      L_ILR_ID      number;
      L_STATUS      varchar2(4000);
      L_RTN         number;

   begin
      /*      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());

            A_MSG := 'Starting to transfer asset from';
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            A_MSG := '   Quantity: ' || TO_CHAR(A_QUANTITY);
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            A_MSG := '   Amount: ' || TO_CHAR(A_AMOUNT);
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            A_MSG := '   CPR Asset ID: ' || TO_CHAR(A_CPR_ASSET_ID);
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            A_MSG := '   Associated Pending Transaction: ' || TO_CHAR(A_PT_ID);
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

          -- NO MAJOR PROCESSING HERE...
          -- all processing happens on the TO side

            A_MSG := 'DONE';
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);


            --Handle rounding... somehow...
      */
      A_MSG := 'DONE';
      return 1;
   exception
      when others then
         A_MSG := SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || A_MSG);

         return -1;
   end F_TRANSFER_ASSET_FROM;

   -- Function to perform specific Leased Asset transfer to
   function F_TRANSFER_ASSET_TO(A_CPR_ASSET_ID       number,
                                A_AMOUNT             number,
                                A_QUANTITY           number,
                                A_COMPANY_ID         number,
                                A_BUS_SEGMENT_ID     number,
                                A_UTILITY_ACCOUNT_ID number,
                                A_SUB_ACCOUNT_ID     number,
                                A_FUNC_CLASS_ID      number,
                                A_PROPERTY_GROUP_ID  number,
                                A_RETIREMENT_UNIT_ID number,
                                A_ASSET_LOCATION_ID  number,
                                A_DG_ID              number,
                                A_PT_ID              number,
                                A_MSG                out varchar2) return number is
      L_LS_ASSET_ID_FROM      number;
      L_LS_ASSET_ID_TO        number;
      L_REVISION_FROM         number;
      L_REVISION_TO           number;
      L_CURRENT_REVISION_FROM number;
      L_TO_ILR_ID             number;
      L_FROM_ILR_ID           number;
      L_WO_ID                 number;
      L_STATUS                varchar2(4000);
      L_RTN                   number;
      L_PCT                   number(22, 8);
      L_PEND_TRANS_TO         PEND_TRANSACTION%rowtype;
      L_PEND_TRANS_FROM       PEND_TRANSACTION%rowtype;
      L_MONTH                 date;
      COMPANY_FIELD varchar2(35);
      V_COUNTER number;
      V_COLUMN_COUNT number;
      SQLS varchar2(20000);
      L_IN_SVC_DATE date;
      L_GL_JE_CODE VARCHAR2(36);
      L_IS_OM NUMBER;
   begin
      --G_SEND_JES:=FALSE;
      --Update the lessee tables to the new CPR attributes
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_MSG := 'Starting to transfer asset to';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Quantity: ' || TO_CHAR(A_QUANTITY);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Amount: ' || TO_CHAR(A_AMOUNT);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   CPR Asset ID: ' || TO_CHAR(A_CPR_ASSET_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   TO Company ID: ' || TO_CHAR(A_COMPANY_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Associated Pending Transaction: ' || TO_CHAR(A_PT_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      -- the ldg_asset_id is the to side transaction
      A_MSG := 'GET to pending transaction';
	  select * into L_PEND_TRANS_TO from PEND_TRANSACTION where LDG_ASSET_ID = A_PT_ID;

      -- the pend_trans_id is the from side transaction
      A_MSG := 'GET from pending transaction';
	  select * into L_PEND_TRANS_FROM from PEND_TRANSACTION where PEND_TRANS_ID = A_PT_ID;

   L_MONTH:=L_PEND_TRANS_FROM.GL_POSTING_MO_YR;
      -- THE FROM SIDE
      A_MSG := 'GET from leased asset';
      select LS_ASSET_ID
        into L_LS_ASSET_ID_FROM
        from LS_CPR_ASSET_MAP
       where ASSET_ID = L_PEND_TRANS_FROM.LDG_ASSET_ID;

      A_MSG := 'GET percent transferred';

      select A_QUANTITY / F.QUANTITY, F.ILR_ID, F.APPROVED_REVISION
        into L_PCT, L_FROM_ILR_ID, L_CURRENT_REVISION_FROM
        from LS_ASSET F
       where F.LS_ASSET_ID = L_LS_ASSET_ID_FROM;

      A_MSG := 'FROM Leased Asset: ' || TO_CHAR(L_LS_ASSET_ID_FROM);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := 'PERCENT Transferred: ' || TO_CHAR(L_PCT);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      /* WMD we need in svc date so we don't lose component rows */
      A_MSG:='Getting in service date from ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      select trunc(EST_IN_SVC_DATE, 'month')
      into L_IN_SVC_DATE
      from ls_ilr ilr, ls_asset la
      where la.ls_asset_id = L_LS_ASSET_ID_FROM
        and la.ilr_id = ilr.ilr_id;

      --CJS 4/17/15 Adding check for partial transfer on components
    if L_PCT <> 1 then
        A_MSG := 'Checking for partial transfer on asset with components';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      L_RTN := 0;

      select count(1)
      into L_RTN
      from LS_COMPONENT
      where LS_ASSET_ID = L_LS_ASSET_ID_FROM;

      if L_RTN > 0 then
       A_MSG:='Cannot do a partial transfer with components. Delete transactions and modify transfer.';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
       return -1;
      end if;
    end if;

      A_MSG            := 'COPY from asset';
      L_LS_ASSET_ID_TO := PKG_LEASE_ILR.F_COPYASSET(L_LS_ASSET_ID_FROM, L_PCT, A_QUANTITY);
      if L_LS_ASSET_ID_TO = -1 then
         return -1;
      end if;

      A_MSG := 'TO Leased Asset: ' || TO_CHAR(L_LS_ASSET_ID_TO);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      A_MSG := 'LOAD LS_CPR_ASSET_MAP';
      insert into LS_CPR_ASSET_MAP
         (LS_ASSET_ID, ASSET_ID, EFFECTIVE_DATE)
      values
         (L_LS_ASSET_ID_TO, A_CPR_ASSET_ID, L_PEND_TRANS_TO.GL_POSTING_MO_YR);

    /* JKim 03282017 - insert the class codes from the pend transaction where filled out */
      A_MSG:='Inserting in class codes for To asset';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      delete from LS_ASSET_CLASS_CODE
		  where LS_ASSET_ID = L_LS_ASSET_ID_TO;

      INSERT INTO ls_asset_class_code
      (class_code_id, ls_asset_id, Value)
      SELECT class_code_id, L_LS_ASSET_ID_TO, Value
        FROM class_code_pending_Trans
        WHERE pend_trans_id = L_PEND_TRANS_TO.pend_trans_id
      UNION
      SELECT class_code_id, L_LS_ASSET_ID_TO, Value
        FROM ls_asset_class_code
        WHERE ls_Asset_id = L_LS_ASSET_ID_FROM
        AND class_code_id NOT IN
          (SELECT class_code_id FROM  class_code_pending_Trans
            WHERE pend_trans_id = L_PEND_TRANS_TO.pend_trans_id);

    /* end JKim 03282017 */

		 /* CJS 3/10/15 CALC STG check; converted assets don't have anything and need a new revision first */
	   A_MSG := 'Checking for previous CALC staging records';
	   L_RTN := 0;

     select count(1)
     into L_RTN
     from LS_ILR_ASSET_SCHEDULE_CALC_STG
     where LS_ASSET_ID = L_LS_ASSET_ID_FROM;

     if L_RTN is null or L_RTN = 0 then
        /* SET NEW ASSET TO STATUS = 4 so we don't split amounts */
        update ls_asset
        set ls_asset_status_id = 4
        where ls_asset_id = L_LS_ASSET_ID_TO;


        A_MSG:='Creating new revision for asset because no records exist in ls_ilr_asset_schedule_calc_stg';
        PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        L_REVISION_FROM := PKG_LEASE_ILR.F_NEWREVISION(L_FROM_ILR_ID);
        PKG_PP_LOG.P_WRITE_MESSAGE('New Revision = ' || L_REVISION_FROM);
      if L_REVISION_FROM = -1 then
         A_MSG:='Error creating new revision for asset with no records in ls_ilr_asset_schedule_calc_stg';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         return -1;
      end if;
      L_STATUS := PKG_LEASE_SCHEDULE.F_PROCESS_ILR(L_FROM_ILR_ID, L_REVISION_FROM);
      if L_STATUS <> 'OK' then
         A_MSG:='Error in F_PROCESS_ILR: ' || L_STATUS;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         return -1;
      end if;


      L_STATUS := PKG_LEASE_SCHEDULE.F_PROCESS_ASSETS(L_IN_SVC_DATE);
      if L_STATUS <> 'OK' then
         A_MSG:='Error in F_PROCESS_ASSETS: ' || L_STATUS;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         return -1;
      end if;
      L_STATUS := PKG_LEASE_SCHEDULE.F_SAVE_SCHEDULES(L_IN_SVC_DATE);
      if L_STATUS <> 'OK' then
         A_MSG:='Error in F_SAVE_SCHEDULES: ' || L_STATUS;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         return -1;
      end if;

      L_STATUS := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(L_FROM_ILR_ID, L_REVISION_FROM, A_MSG);
      if L_STATUS <> 1 then
         A_MSG:='Error in F_APPROVE_ILR_NO_COMMIT: ' || A_MSG;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         return -1;
      end if;

      update LS_ASSET
      set APPROVED_REVISION = L_REVISION_FROM
      where LS_ASSET_ID = L_LS_ASSET_ID_FROM;

--      select  F.APPROVED_REVISION
--        into  L_CURRENT_REVISION_FROM
--        from LS_ASSET F
--       where F.LS_ASSET_ID = L_LS_ASSET_ID_FROM;

      L_CURRENT_REVISION_FROM:=L_REVISION_FROM;

       PKG_PP_LOG.P_WRITE_MESSAGE('New from revision for converted asset = ' || L_CURRENT_REVISION_FROM);
       /* SET NEW ASSET TO STATUS = 1 again */
        update ls_asset
        set ls_asset_status_id = 1
        where ls_asset_id = L_LS_ASSET_ID_TO;
     end if;

      -- create the new from revision
      L_REVISION_FROM := PKG_LEASE_ILR.F_NEWREVISION(L_FROM_ILR_ID);
      if L_REVISION_FROM = -1 then
         return -1;
      end if;

      PKG_PP_LOG.P_WRITE_MESSAGE('From revision = ' || L_REVISION_FROM);

      --  if from company <> to company
      --  CREATE NEW ILR
      --  Set the new assets ilr
      --  Associate new asset to ILR / revision
      if L_PEND_TRANS_TO.COMPANY_ID <> L_PEND_TRANS_FROM.COMPANY_ID then
         L_TO_ILR_ID := PKG_LEASE_ILR.F_COPYILR(L_FROM_ILR_ID, L_PCT);
         if L_TO_ILR_ID = -1 then
            return -1;
         end if;
         L_REVISION_TO := 1;

         A_MSG:='Setting GL Account on LS_ILR_ACCOUNT';
         update ls_ilr_account
         set cap_asset_account_id = L_PEND_TRANS_TO.GL_ACCOUNT_ID
         where ilr_id = L_TO_ILR_ID;
         --
         A_MSG := 'SET company on new ILR';
         update LS_ILR
         set COMPANY_ID = A_COMPANY_ID
         where ILR_ID = L_TO_ILR_ID;

         -- update from ILR Payment TERMS if company not the same
         A_MSG := 'UPDATE from ILRs payment terms';
         update LS_ILR_PAYMENT_TERM LA
            set (EST_EXECUTORY_COST,
                  PAID_AMOUNT,
                  CONTINGENT_AMOUNT,
                  C_BUCKET_1,
                  C_BUCKET_2,
                  C_BUCKET_3,
                  C_BUCKET_4,
                  C_BUCKET_5,
                  C_BUCKET_6,
                  C_BUCKET_7,
                  C_BUCKET_8,
                  C_BUCKET_9,
                  C_BUCKET_10,
                  E_BUCKET_1,
                  E_BUCKET_2,
                  E_BUCKET_3,
                  E_BUCKET_4,
                  E_BUCKET_5,
                  E_BUCKET_6,
                  E_BUCKET_7,
                  E_BUCKET_8,
                  E_BUCKET_9,
                  E_BUCKET_10) =
                 (select LA.EST_EXECUTORY_COST - LT.EST_EXECUTORY_COST,
                         LA.PAID_AMOUNT - LT.PAID_AMOUNT,
                         LA.CONTINGENT_AMOUNT - LT.CONTINGENT_AMOUNT,
                         LA.C_BUCKET_1 - LT.C_BUCKET_1,
                         LA.C_BUCKET_2 - LT.C_BUCKET_2,
                         LA.C_BUCKET_3 - LT.C_BUCKET_3,
                         LA.C_BUCKET_4 - LT.C_BUCKET_4,
                         LA.C_BUCKET_5 - LT.C_BUCKET_5,
                         LA.C_BUCKET_6 - LT.C_BUCKET_6,
                         LA.C_BUCKET_7 - LT.C_BUCKET_7,
                         LA.C_BUCKET_8 - LT.C_BUCKET_8,
                         LA.C_BUCKET_9 - LT.C_BUCKET_9,
                         LA.C_BUCKET_10 - LT.C_BUCKET_10,
                         LA.E_BUCKET_1 - LT.E_BUCKET_1,
                         LA.E_BUCKET_2 - LT.E_BUCKET_2,
                         LA.E_BUCKET_3 - LT.E_BUCKET_3,
                         LA.E_BUCKET_4 - LT.E_BUCKET_4,
                         LA.E_BUCKET_5 - LT.E_BUCKET_5,
                         LA.E_BUCKET_6 - LT.E_BUCKET_6,
                         LA.E_BUCKET_7 - LT.E_BUCKET_7,
                         LA.E_BUCKET_8 - LT.E_BUCKET_8,
                         LA.E_BUCKET_9 - LT.E_BUCKET_9,
                         LA.E_BUCKET_10 - LT.E_BUCKET_10
                    from LS_ILR_PAYMENT_TERM LT
                   where LT.ILR_ID = L_TO_ILR_ID
                     and LT.REVISION = 1
					and LT.PAYMENT_TERM_ID = LA.PAYMENT_TERM_ID)
          where LA.ILR_ID = L_FROM_ILR_ID
            and LA.REVISION = L_REVISION_FROM;

         A_MSG := 'UPDATE from ILRs amounts by set of book';
         update LS_ILR_AMOUNTS_SET_OF_BOOKS LA
            set (NET_PRESENT_VALUE, CAPITAL_COST, CURRENT_LEASE_COST) =
                 (select LA.NET_PRESENT_VALUE - LT.NET_PRESENT_VALUE,
                         LA.CAPITAL_COST - LT.CAPITAL_COST,
                         LA.CURRENT_LEASE_COST - LT.CURRENT_LEASE_COST
                    from LS_ILR_AMOUNTS_SET_OF_BOOKS LT
                   where LT.ILR_ID = L_TO_ILR_ID
                     and LT.REVISION = 1
                     and LA.SET_OF_BOOKS_ID = LT.SET_OF_BOOKS_ID)
          where LA.ILR_ID = L_FROM_ILR_ID
            and LA.REVISION = L_REVISION_FROM;

         A_MSG := 'UPDATE from ILRs options';
         update LS_ILR_OPTIONS LA
            set (PURCHASE_OPTION_AMT, TERMINATION_AMT) =
                 (select LA.PURCHASE_OPTION_AMT - LT.PURCHASE_OPTION_AMT,
                         LA.TERMINATION_AMT - LT.TERMINATION_AMT
                    from LS_ILR_OPTIONS LT
                   where LT.ILR_ID = L_TO_ILR_ID
                     and LT.REVISION = 1)
          where LA.ILR_ID = L_FROM_ILR_ID
            and LA.REVISION = L_REVISION_FROM;
      else
         L_TO_ILR_ID   := L_FROM_ILR_ID;
         L_REVISION_TO := L_REVISION_FROM;
      end if;

      A_MSG := 'ASSOCIATE new asset to the ILR / Revision';
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, LS_ASSET_ID, REVISION)
      values
         (L_TO_ILR_ID, L_LS_ASSET_ID_TO, L_REVISION_TO);

      A_MSG := 'UPDATE asset ILR';
      update LS_ASSET
         set ILR_ID = L_TO_ILR_ID, APPROVED_REVISION = L_REVISION_TO
       where LS_ASSET_ID = L_LS_ASSET_ID_TO;

      -- UPDATE FROM ASSET's values
    /* CJS 3/3/15 Add NVL to values */
      A_MSG := 'UPDATE from assets values';
      update LS_ASSET LA
         set (
               TERMINATION_PENALTY_AMOUNT,
               GUARANTEED_RESIDUAL_AMOUNT,
               ACTUAL_RESIDUAL_AMOUNT,
               QUANTITY) =
              (select
                      nvl(LA.TERMINATION_PENALTY_AMOUNT,0) - nvl(LT.TERMINATION_PENALTY_AMOUNT,0),
                      nvl(LA.GUARANTEED_RESIDUAL_AMOUNT,0) - nvl(LT.GUARANTEED_RESIDUAL_AMOUNT,0),
                      nvl(LA.ACTUAL_RESIDUAL_AMOUNT,0) - nvl(LT.ACTUAL_RESIDUAL_AMOUNT,0),
                      nvl(LA.QUANTITY,0) - nvl(LT.QUANTITY,0)
                 from LS_ASSET LT
                where LT.LS_ASSET_ID = L_LS_ASSET_ID_TO)
       where LA.LS_ASSET_ID = L_LS_ASSET_ID_FROM;

     /* CJS 4/21/15 Set retirement date */
     L_MONTH:=L_PEND_TRANS_FROM.gl_posting_mo_yr;

      A_MSG := 'Retire fully transferred asset';
      update LS_ASSET LA
         set LS_ASSET_STATUS_ID = 4, RETIREMENT_DATE = L_MONTH
       where LA.LS_ASSET_ID = L_LS_ASSET_ID_FROM
         and nvl(LA.QUANTITY,0) = 0;

      A_MSG:= 'Retire ILR if no assets associated any more';
      update LS_ILR ILR
      set ilr_status_id = 3
      where ilr_id = L_FROM_ILR_ID
        and not exists (select 1 from ls_asset where ilr_id = ilr.ilr_id and ls_asset_status_id <=3);
      A_MSG := 'GET work order id';
      L_RTN := 0;
      select count(1)
        into L_RTN
        from WORK_ORDER_CONTROL
       where COMPANY_ID = A_COMPANY_ID
         and FUNDING_WO_INDICATOR = 0
         and WORK_ORDER_NUMBER = replace(L_PEND_TRANS_FROM.MISC_DESCRIPTION, 'wo:', '');

      if L_RTN = 1 then
         A_MSG := '   TO work order number: ' ||
                  replace(L_PEND_TRANS_FROM.MISC_DESCRIPTION, 'wo:', '');
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

         select WORK_ORDER_ID
           into L_WO_ID
           from WORK_ORDER_CONTROL
          where COMPANY_ID = A_COMPANY_ID
            and FUNDING_WO_INDICATOR = 0
            and WORK_ORDER_NUMBER = replace(L_PEND_TRANS_FROM.MISC_DESCRIPTION, 'wo:', '');
      else
         A_MSG := '   TO work order number: ' || L_PEND_TRANS_TO.WORK_ORDER_NUMBER;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

         select WORK_ORDER_ID
           into L_WO_ID
           from WORK_ORDER_CONTROL
          where COMPANY_ID = A_COMPANY_ID
            and FUNDING_WO_INDICATOR = 0
            and WORK_ORDER_NUMBER = L_PEND_TRANS_TO.WORK_ORDER_NUMBER;
      end if;

      --
      A_MSG := 'SET the cpr attributes on the NEW Asset';
      update LS_ASSET
         set UTILITY_ACCOUNT_ID = A_UTILITY_ACCOUNT_ID, COMPANY_ID = A_COMPANY_ID,
             BUS_SEGMENT_ID = A_BUS_SEGMENT_ID, RETIREMENT_UNIT_ID = A_RETIREMENT_UNIT_ID,
             SUB_ACCOUNT_ID = A_SUB_ACCOUNT_ID, PROPERTY_GROUP_ID = A_PROPERTY_GROUP_ID,
             ASSET_LOCATION_ID = A_ASSET_LOCATION_ID, FUNC_CLASS_ID = A_FUNC_CLASS_ID,
             WORK_ORDER_ID = L_WO_ID, ILR_ID = L_TO_ILR_ID, TAX_ASSET_LOCATION_ID = A_ASSET_LOCATION_ID
       where LS_ASSET_ID = L_LS_ASSET_ID_TO;

      -- call function to build the schedules for the new asset and the old asset
      PKG_PP_LOG.P_WRITE_MESSAGE('Processing asset transer');
      A_MSG := PKG_LEASE_SCHEDULE.F_PROCESS_ASSET_TRF(L_LS_ASSET_ID_FROM,
                                                      L_LS_ASSET_ID_TO,
                                                      L_CURRENT_REVISION_FROM,
                                                      L_REVISION_FROM,
                                                      L_PCT,
                                                      L_FROM_ILR_ID,
                                                      L_TO_ILR_ID,
                                                      L_IN_SVC_DATE);
      if A_MSG <> 'OK' then
         return -1;
      end if;

      A_MSG := 'Sending the new schedule and asset adjustments for approval';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      -- the "to" asset
      L_RTN := PKG_LEASE_CALC.F_SEND_ILR_NO_COMMIT(L_TO_ILR_ID, L_REVISION_TO, L_STATUS);
      if L_RTN <> 1 then
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
      end if;

      --Rebuild the ILR and asset schedules. I guess this works? It's done in the retirements above
      --Not sure how you can just approve it before sending it
      A_MSG := 'Approving the new schedule and asset adjustments';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      L_RTN := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(L_TO_ILR_ID, L_REVISION_TO, L_STATUS, TRUE, true);
      if L_RTN <> 1 then
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
      end if;

      -- only send the from if the comapnys are different and we created a new to side
      if L_PEND_TRANS_TO.COMPANY_ID <> L_PEND_TRANS_FROM.COMPANY_ID then
         A_MSG := '(FROM) Sending the new schedule and asset adjustments for approval';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         L_RTN := PKG_LEASE_CALC.F_SEND_ILR_NO_COMMIT(L_FROM_ILR_ID, L_REVISION_FROM, L_STATUS);
         if L_RTN <> 1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

            return -1;
         end if;

         --Rebuild the ILR and asset schedules. I guess this works? It's done in the retirements above
         --Not sure how you can just approve it before sending it
         A_MSG := '(FROM) Approving the new schedule and asset adjustments';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         L_RTN := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(L_FROM_ILR_ID, L_REVISION_FROM, L_STATUS, true, true);
         if L_RTN <> 1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            return -1;
         end if;
      end if;

      --Get taxes on the transferred Assets
      L_STATUS := PKG_LEASE_ILR.F_GETTAXES(L_TO_ILR_ID);
      if L_STATUS <> 'OK' then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            return -1;
         end if;

  /* WMD */
      A_MSG:= 'Adding to and from assets to transfer history table';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      insert into ls_asset_transfer_history
      (from_ls_asset_id, to_ls_asset_id, month, from_pend_trans_id, to_pend_trans_id)
      values
      (L_LS_ASSET_ID_FROM,L_LS_ASSET_ID_TO, L_MONTH, L_PEND_TRANS_FROM.pend_trans_id, L_PEND_TRANS_TO.pend_trans_id);

	 -- CJS 4/21/17 Update gl je code to get external value; Post is not
	 A_MSG:= 'Updating GL transaction to have LS Asset ID on To Record';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      update gl_transaction
      set asset_id = L_LS_ASSET_ID_TO,
          source='LESSEE',
          description = 'TRANS TYPE: ' || trans_type,
          gl_status_id = to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', L_PEND_TRANS_TO.COMPANY_ID)), 1)),
          gl_je_code = Nvl((SELECT external_je_code FROM standard_journal_entries s WHERE Trim(s.gl_je_code) = Trim(gl_transaction.gl_je_code)),gl_je_code)
      where month=L_PEND_TRANS_TO.GL_POSTING_MO_YR
        and pend_trans_id in (L_PEND_TRANS_FROM.PEND_TRANS_ID, L_PEND_TRANS_TO.PEND_TRANS_ID)
        and trans_type in (3034,3044,3048)
        and ORIGINATOR='POST';

      A_MSG:= 'Updating GL transaction to have LS Asset ID on From Record';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      update gl_transaction
      set asset_id = L_LS_ASSET_ID_FROM,
          source='LESSEE',
          description = 'TRANS TYPE: ' || trans_type,
          gl_status_id = to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', L_PEND_TRANS_FROM.COMPANY_ID)), 1)),
          gl_je_code = Nvl((SELECT external_je_code FROM standard_journal_entries s WHERE Trim(s.gl_je_code) = Trim(gl_transaction.gl_je_code)),gl_je_code)
      where month=L_PEND_TRANS_FROM.GL_POSTING_MO_YR
        and pend_trans_id in (L_PEND_TRANS_FROM.PEND_TRANS_ID, L_PEND_TRANS_TO.PEND_TRANS_ID)
        and trans_type in (3035, 3043, 3047)
        and ORIGINATOR='POST';

      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');

      A_MSG:='Clearing Depr Method SLE table for transferred asset';
      delete from depr_method_sle
      where asset_id = (select asset_id from ls_cpr_asset_map where ls_asset_id = L_LS_ASSET_ID_FROM)
        and GL_POSTING_MO_YR >= L_MONTH;

   PKG_LEASE_DEPR.P_FCST_LEASE(L_TO_ILR_ID, L_REVISION_TO);



      DELETE FROM DEPR_METHOD_SLE
      WHERE ASSET_ID = (select asset_id from ls_cpr_asset_map where ls_asset_id = L_LS_ASSET_ID_TO)
      and GL_POSTING_MO_YR < L_MONTH;

      A_MSG:='Inserting depreciation to date on asset in prior month so we dont end up with any crazy adjustments';
      insert into depr_method_sle
      (asset_id, set_of_books_id, gl_posting_mo_yr, depr_expense)
      select
      (select asset_id from ls_cpr_asset_map where ls_asset_id = L_LS_ASSET_ID_TO),
      cprd.set_of_books_id,
      add_months(L_MONTH, -1),
      cprd.beg_reserve_month
      from cpr_depr cprd
      where asset_id = (select asset_id from ls_cpr_asset_map where ls_asset_id = L_LS_ASSET_ID_FROM)
        and gl_posting_mo_yr = L_MONTH;
      A_MSG:='Updating end reserve on depreciation projection table';
      update ls_depr_forecast
      set end_reserve = end_reserve + nvl(L_PEND_TRANS_FROM.adjusted_reserve,0)
      where ls_asset_id = L_LS_ASSET_ID_FROM
        and revision = L_REVISION_FROM
        and month = L_PEND_TRANS_FROM.GL_POSTING_MO_YR;

    /* CJS 2/27/2015 Calling RETIREREVISION for from asset if completely transferred; 0 is retirement, 1 is transfer */

      if L_PCT = 1 then
          A_MSG:= 'Calling Retire Revision for fully transferred asset';
          L_RTN:= PKG_LEASE_ILR.F_RETIREREVISION(L_FROM_ILR_ID,L_LS_ASSET_ID_FROM, L_MONTH, 1, L_MONTH);
      if L_RTN <> 1 then
            PKG_PP_LOG.P_WRITE_MESSAGE('Error in RETIREREVISION for Transfer');
            return -1;
         end if;
    end if;

    SELECT lct.IS_OM
    INTO L_IS_OM
    FROM ls_asset la, ls_ilr ilr, ls_ilr_options ilro, ls_lease_cap_type lct
    where la.ls_asset_id = L_LS_ASSET_ID_FROM
      and la.ilr_id = ilr.ilr_id
      and ilr.current_revision = ilro.revision
      and ilr.ilr_id = ilro.ilr_id
      and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id;

    /* If we have an operating lease don't throw journals */
    if L_IS_OM <> 1 THEN

    /* WMD MAINT-44042 Clear out obligations */
    /* CJS 4/21/17 Add trim, Change to LAM TRANSFERS; It was 'LAM RETIREMENTS'... C'mon Will. */
    A_MSG:='Getting transfer GL JE Code';
      select NVL(E.EXTERNAL_JE_CODE, E.GL_JE_CODE)
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
        and G.PROCESS_ID = 'LAM TRANSFERS';

    A_MSG:='Getting asset schedule details on Transfer From asset';
    for OBLIG_FROM in (select las.*
                        from ls_asset_schedule las
                        where las.ls_asset_id = L_LS_ASSET_ID_FROM
                          and revision = L_CURRENT_REVISION_FROM
                          and month = L_MONTH)
    loop
    if nvl(OBLIG_FROM.BEG_OBLIGATION,0) - nvl(OBLIG_FROM.BEG_LT_OBLIGATION,0) > 0 then
      L_STATUS:='Processing short term obligation TRANSFER FROM debit on ls_asset_id = ' || L_LS_ASSET_ID_FROM;
      /* Clear ST Obligation on from asset */
      L_RTN:= PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_FROM,
                                                3002,
                                                L_PCT * (nvl(OBLIG_FROM.BEG_OBLIGATION,0) - nvl(OBLIG_FROM.BEG_LT_OBLIGATION,0)),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                -1,
                                                L_PEND_TRANS_FROM.COMPANY_ID,
                                                L_MONTH,
                                                1, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_FROM.SET_OF_BOOKS_ID,
                                                nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;


    IF L_PEND_TRANS_FROM.COMPANY_ID <> L_PEND_TRANS_TO.COMPANY_ID or
       (pkg_pp_system_control.F_PP_SYSTEM_CONTROL('POST INTERCOMPANY BUS SEG TRF GL') = 'yes' and L_PEND_TRANS_FROM.BUS_SEGMENT_ID <> L_PEND_TRANS_TO.BUS_SEGMENT_ID)  THEN
    L_STATUS:='Processing Intercompany TRANSFER FROM credit on ls_asset_id = ' || L_LS_ASSET_ID_FROM;
    L_RTN:= PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_FROM,
                                                3050,
                                                L_PCT * (nvl(OBLIG_FROM.BEG_OBLIGATION,0) - nvl(OBLIG_FROM.BEG_LT_OBLIGATION,0)),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                L_PEND_TRANS_FROM.PEND_TRANS_ID,
                                                L_PEND_TRANS_FROM.COMPANY_ID,
                                                L_MONTH,
                                                0, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_FROM.SET_OF_BOOKS_ID,
                                                nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    end if; /* 3050 entry */

    end if; /* ST OBLIGATION */

    if nvl(OBLIG_FROM.BEG_LT_OBLIGATION,0) > 0 then
      L_STATUS:='Processing Long Term Obligation Transfer From debit on ls_asset_id = ' || L_LS_ASSET_ID_FROM;
      /* Clear LT Obligation on from asset */
      L_RTN:= PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_FROM,
                                                3003,
                                                L_PCT * nvl(OBLIG_FROM.BEG_LT_OBLIGATION,0),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                -1,
                                                L_PEND_TRANS_FROM.COMPANY_ID,
                                                L_MONTH,
                                                1, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_FROM.SET_OF_BOOKS_ID,
                                                nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    L_STATUS:='Processing Intercompany TRANSFER To credit on ls_asset_id = ' || L_LS_ASSET_ID_FROM;
    IF L_PEND_TRANS_FROM.COMPANY_ID <> L_PEND_TRANS_TO.COMPANY_ID or
       (pkg_pp_system_control.F_PP_SYSTEM_CONTROL('POST INTERCOMPANY BUS SEG TRF GL') = 'yes' and L_PEND_TRANS_FROM.BUS_SEGMENT_ID <> L_PEND_TRANS_TO.BUS_SEGMENT_ID)  THEN
           L_RTN:= PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_FROM,
                                                3050,
                                                L_PCT * nvl(OBLIG_FROM.BEG_LT_OBLIGATION,0),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                L_PEND_TRANS_FROM.PEND_TRANS_ID,
                                                L_PEND_TRANS_FROM.COMPANY_ID,
                                                L_MONTH,
                                                0, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_FROM.SET_OF_BOOKS_ID,
                                                nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    end if; /* 3050 entry */

    end if; /* LT OBLIGATION */

    end loop;

    for OBLIG_TO in (select las.*
                        from ls_asset_schedule las, ls_asset la
                        where las.ls_asset_id = L_LS_ASSET_ID_TO
              and la.ls_asset_id = las.ls_asset_id
              and la.approved_revision = las.revision
              and month = L_MONTH)
    loop
    if nvl(OBLIG_TO.BEG_OBLIGATION,0) - nvl(OBLIG_TO.BEG_LT_OBLIGATION,0) > 0 then
      /* Clear ST Obligation on TO asset */
    IF L_PEND_TRANS_FROM.COMPANY_ID <> L_PEND_TRANS_TO.COMPANY_ID or
       (pkg_pp_system_control.F_PP_SYSTEM_CONTROL('POST INTERCOMPANY BUS SEG TRF GL') = 'yes' and L_PEND_TRANS_FROM.BUS_SEGMENT_ID <> L_PEND_TRANS_TO.BUS_SEGMENT_ID)  THEN
      L_STATUS:='Processing Intercompany TRANSFER TO ST debit on ls_asset_id = ' || L_LS_ASSET_ID_TO;
      L_RTN:= PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_TO,
                                                3049,
                                                (nvl(OBLIG_TO.BEG_OBLIGATION,0) - nvl(OBLIG_TO.BEG_LT_OBLIGATION,0)),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                L_PEND_TRANS_FROM.PEND_TRANS_ID, /* CJS 4/24/17 Change to FROM, follow all others */
                                                L_PEND_TRANS_TO.COMPANY_ID,
                                                L_MONTH,
                                                1, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_TO.SET_OF_BOOKS_ID,
                                                nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    end if; /* 3049 entry */

    L_STATUS:='Processing ST TRANSFER TO ST CREDIT on ls_asset_id = ' || L_LS_ASSET_ID_TO;
    L_RTN:= PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_TO,
                                                3002,
                                                (nvl(OBLIG_TO.BEG_OBLIGATION,0) - nvl(OBLIG_TO.BEG_LT_OBLIGATION,0)),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                -1,
                                                L_PEND_TRANS_TO.COMPANY_ID,
                                                L_MONTH,
                                                0, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_TO.SET_OF_BOOKS_ID,
                                                nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    end if; /* ST OBLIGATION */

    if nvl(OBLIG_TO.BEG_LT_OBLIGATION,0) > 0 then
      L_STATUS:='Processing Intercompany TRANSFER TO LT debit on ls_asset_id = ' || L_LS_ASSET_ID_TO;
      /* Clear LT Obligation on TO asset */
    IF L_PEND_TRANS_FROM.COMPANY_ID <> L_PEND_TRANS_TO.COMPANY_ID or
       (pkg_pp_system_control.F_PP_SYSTEM_CONTROL('POST INTERCOMPANY BUS SEG TRF GL') = 'yes' and L_PEND_TRANS_FROM.BUS_SEGMENT_ID <> L_PEND_TRANS_TO.BUS_SEGMENT_ID)  THEN
             L_RTN:= PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_TO,
                                                3049,
                                                nvl(OBLIG_TO.BEG_LT_OBLIGATION,0),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                L_PEND_TRANS_FROM.PEND_TRANS_ID, /* CJS 4/24/17 Change to FROM, follow all others */
                                                L_PEND_TRANS_TO.COMPANY_ID,
                                                L_MONTH,
                                                1, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_TO.SET_OF_BOOKS_ID,
                                                nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    end if; /* 3049 entry */

    L_STATUS:='Processing ST TRANSFER TO LT CREDIT on ls_asset_id = ' || L_LS_ASSET_ID_TO;
    L_RTN:= PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_TO,
                                                3003,
                                                nvl(OBLIG_TO.BEG_LT_OBLIGATION,0),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                -1,
                                                L_PEND_TRANS_TO.COMPANY_ID,
                                                L_MONTH,
                                                0, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_TO.SET_OF_BOOKS_ID,
                                                nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    end if; /* LT OBLIGATION */

    end loop;

    end if; /* IS_OM LOOP */
    A_MSG:='Deleting tax rows for transfer from asset';
    delete from ls_monthly_tax
    where ls_asset_id = L_LS_ASSET_ID_FROM
      and gl_posting_mo_yr >= L_MONTH;
      G_SEND_JES:=TRUE;

    /* WMD Set company column on deriver control to new company */
    A_MSG:= 'Checking for company column from CR system control table';
    select count(1)
    into v_counter
    from cr_system_control
    where upper(trim(control_name))= 'COMPANY FIELD';

    if v_counter = 1 then
      A_MSG:= 'Getting company column from CR system control table';
      select control_value
      into company_field
      from cr_system_control
      where upper(trim(control_name))= 'COMPANY FIELD';

      A_MSG:= 'Checking to see if company column is on CR Deriver Control';
      select count(1)
      into v_column_count
      from all_tab_columns
      where table_name = 'CR_DERIVER_CONTROL'
        and column_name = upper(trim(company_field));

      if v_column_count = 1 then
        A_MSG:= 'Dynamic update of company on CR deriver control';
        sqls:='update cr_deriver_control crd
              set ' || company_field || ' =
              (select gl_company_no
                from company_setup
                where company_id = ' || to_char(L_PEND_TRANS_TO.COMPANY_ID) || ')
              where type=''Lessee''
                and substr(crd.string, 0, instr(crd.string, '':'') -1) = ' || to_char(L_LS_ASSET_ID_TO);
        A_MSG:=sqls;
        execute immediate sqls;
      end if;
    end if;

    IF L_PCT=1 THEN

      A_MSG:='Deleting prior months from transfer to asset';
      delete from ls_asset_schedule
      where LS_ASSET_ID = L_LS_ASSET_ID_TO
        and month < L_MONTH;

      A_MSG:='Setting beginning capital cost to zero on transfer to asset';
      update ls_asset_schedule
      set beg_capital_cost = 0
      where LS_ASSET_ID = L_LS_ASSET_ID_TO
        and month = L_MONTH
        and revision = L_REVISION_TO;

      A_MSG:=PKG_LEASE_SCHEDULE.F_SYNC_ILR_SCHEDULE(L_TO_ILR_ID, L_REVISION_TO);

      IF A_MSG <> 'OK' then
        PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        PKG_PP_LOG.P_END_LOG();
        return -1;
      end if;
    update ls_ilr a
      set ilr_status_id = 3
      where ilr_id = L_FROM_ILR_ID
        and not exists (select 1 from ls_asset where ilr_id = a.ilr_id and ls_asset_status_id <4);

    END IF;

    A_MSG:='Delete out the empty prior month cpr depr rows so true up will work';
    delete from cpr_depr
    where asset_id = (select asset_id from ls_cpr_asset_map where ls_asset_id = L_LS_ASSET_ID_TO)
      and gl_posting_mo_yr < L_MONTH;


      PKG_PP_LOG.P_END_LOG();

      return 1;
   exception
      when others then
         A_MSG := SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || A_MSG);

         PKG_PP_LOG.P_END_LOG();
         return -1;
   end F_TRANSFER_ASSET_TO;

   --**************************************************************************
   --                            F_DELETE_ASSETS
   --**************************************************************************
  function F_DELETE_ASSETS(A_ASSET_IDS in PKG_LEASE_CALC.NUM_ARRAY) return varchar2 is
    L_STATUS varchar2(4000);
    ASSET_IDS T_NUM_ARRAY;
  begin
    PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_ASSET_POST.F_DELETE_ASSETS');
    PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
    PKG_PP_LOG.P_WRITE_MESSAGE('Preparing to delete ' || A_ASSET_IDS.COUNT || ' assets.');

    ASSET_IDS := T_NUM_ARRAY();
    for I in 1..A_ASSET_IDS.count
    loop
      PKG_PP_LOG.P_WRITE_MESSAGE('-- Asset ID: ' || to_char(A_ASSET_IDS(I)));
      ASSET_IDS.extend;
      ASSET_IDS(I) := A_ASSET_IDS(I);
    end loop;

    L_STATUS := 'Deleting class codes';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from LS_ASSET_CLASS_CODE
    where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));

    /* WMD Needed to Change Table to LS_COMPONENT FROM LS_ASSET_COMPONENT */
    L_STATUS := 'Deleting asset components';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from LS_COMPONENT
    where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));

    L_STATUS := 'Deleting documents';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from LS_ASSET_DOCUMENT
    where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));

    L_STATUS := 'Deleting tax maps';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from LS_ASSET_TAX_MAP
    where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));

    L_STATUS := 'Deleting local taxes';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from LS_ASSET_LOCAL_TAX
    where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));

    L_STATUS := 'Deleting leased assets';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from LS_ASSET
    where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));

    L_STATUS := 'Deleting JE Allocation Rows';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from cr_deriver_control
    where string like (select column_value || ':%' from table(ASSET_IDS));
    PKG_PP_LOG.P_WRITE_MESSAGE('Success');
    PKG_PP_ERROR.REMOVE_MODULE_NAME;
    return 'OK';

  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
  end F_DELETE_ASSETS;

   --**************************************************************************
   --                            GET_ILR_ID
   --**************************************************************************

   function F_GET_ILR_ID return number is

   begin
      return L_ILR_ID;
   end F_GET_ILR_ID;

--**************************************************************************
--                            Initialize Package
--**************************************************************************

begin
   L_ILR_ID := 0;

end PKG_LEASE_ASSET_POST;
/

GRANT EXECUTE ON pkg_lease_asset_post TO pwrplant_role_dev;

--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (4048, 0, 2017, 1, 0, 0, 0, 'C:\plasticwks\powerplant\sql\packages', '2017.1.0.0_PKG_LEASE_ASSET_POST.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit;