CREATE OR REPLACE package pkg_lease_schedule as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: LEASE_SCHEDULE_PKG
   || Description:
   ||============================================================================
   || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 05/07/2013 B.Beck         Original Version
   || 10.4.1.0 06/21/2013 Brandon Beck   Added BPO and Term Penalty.
   ||                                    Also correct the end obligation for guaranteed residual
   || 10.4.1.0 07/05/2013 Brandon Beck   Load schedule tables from the calc tables
   || 10.4.1.0 07/15/2013 B.Beck         Update IRR and NPV onto ls_ilr
   || 10.4.1.1 10/02/2013 Kyle Peterson  Interim Interest
   || 10.4.2.0 01/07/2014 Kyle Peterson
   || 10.4.2.0 03/17/2014 Brandon Beck
   || 10.4.2.2 06/16/2014 Brandon Beck
   || 10.4.2.2 06/16/1014 Brandon Beck
   || 10.4.3.0 06/30/2014 Kyle Peterson	Refactor NPV calc
   || 2015.1   04/06/2015 Anand R         PP-42670 Operating Prepaid Lease Schedule
   || 2015.2.0 08/20/2015 Anand R         PP-44441 error when updating current lease cost on LS_ILR_SCHEDULE
   || 2015.2.0 09/30/2015 Andrew Scott    added some more logging to aid in maint-45012
   || 2016.1.0 02/10/2016 S. Byers      PP-45363 Added NPV_START_DATE to LS_ILR_STG
   || 2016.1.0 02/11/2016 S. Byers      PP-45365 Added F_BUILD_FORECAST
   || 2016.1.0 02/22/2016 Anand R       PP-45407 Added F_BUILD_SUMMARY_FORECAST
   || 2016.1.0 02/23/2016 Anand R       PP-45366 Added F_CONVERT_FORECAST
   || 2016.1.0 02/29/2016 S. Byers		  PP-45508 Fixed call to F_COPY_REVISION in F_BUILD_FORECAST
   || 2016.1.0 03/08/2016 S. Byers      PP-45526 Trick the lease schedule logic into not checking for the IRR on summary forecasts
   || 2016.1.0 03/17/2016 Anand R       PP-45557 Update revision_built on ls_forecsast_version when revision is built
   || 2016.1.1 03/07/2017 J. Watkins       PP-47170 Incorrect revision grouping when creating forecasts
   || 2017.1.0 03/16/2017 David H       PP-47023 Add to initial measure included in schedule build
   || 2017.1.0 03/22/2017 David H.      PP-47023 and 47282 Added call to calculate variable payments during schedule build
   || 2017.1.0 06/26/2017 Jared S.		PP-48330 Added calculations for deferred rent
   || 2017.1.0 08/17/2017 Andrew H.     PP-48623 Add call to update currency_rate_default_dense after building schedule (in case max/min month changes)
   || 2017.1.0 08/30/2017 Anand R       PP-48861 Replace code block with view that uses materialzed asset schedule view
   || 2017.1.0 09/11/2017 Sisouphanh    PP-47249 Fixed F_POPULATE_SUMMARY_FORECAST to add the commit needed before selecting from materialzed view   
   || 2017.1.0 11/28/2017 Anand R       PP-49763 Add nvl around lookback_revision to calculate IRR. 
   || 2017.1.0 11/30/2017 Shane "C" Ward PP-49859 Update BPO Depreciation Calc
   || 2017.2.0 01/25/2018 Shane "C" Ward PP-50220 Performance Enhancements and formatting
   || 2017.3.0 03/02/2018 Anand R        PP-50205 Add new Lessee tables to forecast build  
   ||==================================================================================================================================================
   */

   -- Function to process the asset (allocate, find NBV)
   function F_PROCESS_ASSETS(A_MONTH in date:=null) return varchar2;

   -- Loads the ilr_scheduling table for an entire LEASE
   function F_PROCESS_ILRS(A_LEASE_ID number) return varchar2;

   --Loads LS_ILR_STG all at once to avoid PK violations
   function F_MASS_PROCESS_ILRS(A_ILR_IDS   PKG_LEASE_IMPORT.NUM_ARRAY,
                                A_REVISIONS PKG_LEASE_IMPORT.NUM_ARRAY) return varchar2;

   -- Loads the ilr_scheduling table for a single ILR
   function F_PROCESS_ILR(A_ILR_ID   number,
                          A_REVISION number) return varchar2;

   -- Builds forecast revisions for all ILR's
   function F_PROCESS_ILRS_FCST(A_CAP_TYPE number) return varchar2;

   /*
   *  Loads from the calc staging table to the "real" tables
   */
   function F_SAVE_SCHEDULES(A_MONTH in date:=null) return varchar2;

   /*
   *  This function is used for Asset transfers to create new asset schedules based on the transfer
   */
   function F_PROCESS_ASSET_TRF(A_FROM_ASSET_ID     number,
                                A_TO_ASSET_ID       number,
                                A_FROM_ILR_REVISION number,
                                A_TO_ILR_REVISION   number,
                                A_PERCENT           number,
                                A_FROM_ILR_ID       number,
                                A_TO_ILR_ID         number,
                                A_MONTH             date:=null) return varchar2;

   function F_GET_II_BEGIN_DATE(A_ILR_ID   number,
                                A_REVISION number) return date;

   function F_MAKE_II(A_ILR_ID   number,
                      A_REVISION number) return number;

	function F_LOAD_ILR_SCHEDULE_STG(a_month in date:=null) return varchar2;

	function F_NET_PRESENT_VALUE return varchar2;

	function F_LOAD_ILR_STG(a_ilr_id number,
  							a_revision number) return varchar2;
  function F_SYNC_ILR_SCHEDULE(A_ILR_ID IN NUMBER,
                               A_REVISION IN NUMBER) return varchar2;

   function F_BUILD_FORECAST(A_REVISION IN NUMBER) return varchar2;

   function F_BUILD_SUMMARY_FORECAST(A_REVISION IN NUMBER, A_RATE IN DECIMAL) return varchar2;

   function F_CONVERT_FORECAST(A_REVISION IN NUMBER) return varchar2;

   function F_POPULATE_SUMMARY_FORECAST(A_REVISION IN NUMBER, A_CUR_TYPE IN NUMBER, A_ACTUALS IN NUMBER:=null) return varchar2;

   -- Function to clear the staging tables used in the schedule calculation
   function F_CLEAR_STAGING_TABLES return varchar2;

   function F_LOAD_DEFERRED_RENT(A_SUMM_FCST IN BOOLEAN) return varchar2;
   
   FUNCTION F_BPO_SCHEDULE_EXTEND (A_LS_ASSET_ID IN NUMBER, A_ILR_ID IN NUMBER, A_REVISION IN NUMBER) RETURN VARCHAR2;
end PKG_LEASE_SCHEDULE;
/

CREATE OR REPLACE package body pkg_lease_schedule as
 G_COPY_LS_ASSET_ID          LS_ASSET.LS_ASSET_ID%type;
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: LEASE_SCHEDULE_PKG
   || Description:
   ||============================================================================
   || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 05/07/2013 B.Beck         Original Version
   || 10.4.1.0 06/21/2013 Brandon Beck   Added BPO and Term Penalty.
   ||                                    Also correct the end obligation for guaranteed residual
   || 10.4.1.0 07/05/2013 Brandon Beck   Load schedule tables from the calc tables
   || 10.4.1.0 07/15/2013 B.Beck         Update IRR and NPV onto ls_ilr
   || 10.4.1.1 10/02/2013 Kyle Peterson  Interim Interest
   || 10.4.2.0 01/07/2014 Kyle Peterson
   || 10.4.2.0 03/17/2014 Brandon Beck
   || 10.4.2.2 06/16/2014 Brandon Beck
   || 10.4.2.2 06/16/1014 Brandon Beck
   || 10.4.3.0 06/30/2014 Kyle Peterson	Refactor NPV calc
   || 2015.1   04/06/2015 Anand R         PP-42670 Operating Prepaid Lease Schedule
   || 2015.2.0 08/20/2015 Anand R         PP-44441 error when updating current lease cost on LS_ILR_SCHEDULE
   || 2015.2.0 09/30/2015 Andrew Scott    added some more logging to aid in maint-45012
   || 2016.1.0 02/10/2016 S. Byers      PP-45363 Added NPV_START_DATE to LS_ILR_STG
   || 2016.1.0 02/11/2016 S. Byers      PP-45365 Added F_BUILD_FORECAST
   || 2016.1.0 02/22/2016 Anand R       PP-45407 Added F_BUILD_SUMMARY_FORECAST
   || 2016.1.0 02/23/2016 Anand R       PP-45366 Added F_CONVERT_FORECAST
   || 2016.1.0 02/29/2016 S. Byers		  PP-45508 Fixed call to F_COPY_REVISION in F_BUILD_FORECAST
   || 2016.1.0 03/08/2016 S. Byers      PP-45526 Trick the lease schedule logic into not checking for the IRR on summary forecasts
   || 2016.1.0 03/17/2016 Anand R       PP-45557 Update revision_built on ls_forecsast_version when revision is built
   || 2017.1.0 03/16/2017 David H       PP-47023 Add to initial measure included in schedule build
   || 2017.1.0 03/22/2017 David H.      PP-47023 and 47282 Added call to calculate variable payments during schedule build
   || 2017.1.0 09/11/2017 Sisouphanh    PP-47249 Fixed F_POPULATE_SUMMARY_FORECAST to add the commit needed before selecting from materialzed view      
   || 2017.1.0 11/28/2017 Anand R       PP-49763 Add nvl around lookback_revision to calculate IRR.
   ||============================================================================
   */

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
function f_copy_asset_schedule_rows(A_MONTH in date)
  return varchar2
  is
  begin

  PKG_PP_LOG.P_WRITE_MESSAGE('Deleting from ls_asset_schedule');

  delete from ls_asset_schedule
  where (ls_asset_id, revision, set_of_books_id) in
  (select la.ls_asset_id, revision, stg.set_of_books_id
   from ls_asset la, ls_ilr ilr, ls_ilr_stg stg
   where la.ilr_id = ilr.ilr_id
    and ilr.ilr_id = stg.ilr_id
    and la.ls_asset_id = nvl(g_copy_ls_asset_id, la.ls_asset_id));

   PKG_PP_LOG.P_WRITE_MESSAGE('Copying prior rows in from ls_asset_schedule');
   insert into ls_asset_schedule
    (LS_ASSET_ID,
    REVISION,
    SET_OF_BOOKS_ID,
    MONTH,
    RESIDUAL_AMOUNT,
    TERM_PENALTY,
    BPO_PRICE,
    BEG_CAPITAL_COST,
    END_CAPITAL_COST,
    BEG_OBLIGATION,
    END_OBLIGATION,
    BEG_LT_OBLIGATION,
    END_LT_OBLIGATION,
    INTEREST_ACCRUAL,
    PRINCIPAL_ACCRUAL,
    INTEREST_PAID,
    PRINCIPAL_PAID,
    EXECUTORY_ACCRUAL1,
    EXECUTORY_ACCRUAL2,
    EXECUTORY_ACCRUAL3,
    EXECUTORY_ACCRUAL4,
    EXECUTORY_ACCRUAL5,
    EXECUTORY_ACCRUAL6,
    EXECUTORY_ACCRUAL7,
    EXECUTORY_ACCRUAL8,
    EXECUTORY_ACCRUAL9,
    EXECUTORY_ACCRUAL10,
    EXECUTORY_PAID1,
    EXECUTORY_PAID2,
    EXECUTORY_PAID3,
    EXECUTORY_PAID4,
    EXECUTORY_PAID5,
    EXECUTORY_PAID6,
    EXECUTORY_PAID7,
    EXECUTORY_PAID8,
    EXECUTORY_PAID9,
    EXECUTORY_PAID10,
    CONTINGENT_ACCRUAL1,
    CONTINGENT_ACCRUAL2,
    CONTINGENT_ACCRUAL3,
    CONTINGENT_ACCRUAL4,
    CONTINGENT_ACCRUAL5,
    CONTINGENT_ACCRUAL6,
    CONTINGENT_ACCRUAL7,
    CONTINGENT_ACCRUAL8,
    CONTINGENT_ACCRUAL9,
    CONTINGENT_ACCRUAL10,
    CONTINGENT_PAID1,
    CONTINGENT_PAID2,
    CONTINGENT_PAID3,
    CONTINGENT_PAID4,
    CONTINGENT_PAID5,
    CONTINGENT_PAID6,
    CONTINGENT_PAID7,
    CONTINGENT_PAID8,
    CONTINGENT_PAID9,
    CONTINGENT_PAID10,
    IS_OM,
    BEG_DEFERRED_RENT,
    DEFERRED_RENT,
    END_DEFERRED_RENT,
    BEG_ST_DEFERRED_RENT,
    END_ST_DEFERRED_RENT)
    select
    la.ls_asset_id,
    stg.revision,
    a.SET_OF_BOOKS_ID,
    a.MONTH,
    a.RESIDUAL_AMOUNT,
    a.TERM_PENALTY,
    a.BPO_PRICE,
    a.BEG_CAPITAL_COST,
    a.END_CAPITAL_COST,
    a.BEG_OBLIGATION,
    a.END_OBLIGATION,
    decode(a.is_om, 1, 0, a.BEG_LT_OBLIGATION),
    decode(a.is_om,1,0,a.END_LT_OBLIGATION),
    a.INTEREST_ACCRUAL,
    a.PRINCIPAL_ACCRUAL,
    a.INTEREST_PAID,
    a.PRINCIPAL_PAID,
    a.EXECUTORY_ACCRUAL1,
    a.EXECUTORY_ACCRUAL2,
    a.EXECUTORY_ACCRUAL3,
    a.EXECUTORY_ACCRUAL4,
    a.EXECUTORY_ACCRUAL5,
    a.EXECUTORY_ACCRUAL6,
    a.EXECUTORY_ACCRUAL7,
    a.EXECUTORY_ACCRUAL8,
    a.EXECUTORY_ACCRUAL9,
    a.EXECUTORY_ACCRUAL10,
    a.EXECUTORY_PAID1,
    a.EXECUTORY_PAID2,
    a.EXECUTORY_PAID3,
    a.EXECUTORY_PAID4,
    a.EXECUTORY_PAID5,
    a.EXECUTORY_PAID6,
    a.EXECUTORY_PAID7,
    a.EXECUTORY_PAID8,
    a.EXECUTORY_PAID9,
    a.EXECUTORY_PAID10,
    a.CONTINGENT_ACCRUAL1,
    a.CONTINGENT_ACCRUAL2,
    a.CONTINGENT_ACCRUAL3,
    a.CONTINGENT_ACCRUAL4,
    a.CONTINGENT_ACCRUAL5,
    a.CONTINGENT_ACCRUAL6,
    a.CONTINGENT_ACCRUAL7,
    a.CONTINGENT_ACCRUAL8,
    a.CONTINGENT_ACCRUAL9,
    a.CONTINGENT_ACCRUAL10,
    a.CONTINGENT_PAID1,
    a.CONTINGENT_PAID2,
    a.CONTINGENT_PAID3,
    a.CONTINGENT_PAID4,
    a.CONTINGENT_PAID5,
    a.CONTINGENT_PAID6,
    a.CONTINGENT_PAID7,
    a.CONTINGENT_PAID8,
    a.CONTINGENT_PAID9,
    a.CONTINGENT_PAID10,
    a.IS_OM,
    a.BEG_DEFERRED_RENT,
    a.DEFERRED_RENT,
    a.END_DEFERRED_RENT,
    a.BEG_ST_DEFERRED_RENT,
    a.END_ST_DEFERRED_RENT
    from ls_asset_schedule a, ls_asset la, ls_ilr_stg stg
    where a.ls_asset_id = la.ls_asset_id
      and a.ls_asset_id = nvl(g_copy_ls_asset_id, a.ls_asset_id)
      and a.set_of_books_id=stg.set_of_books_id
      and la.approved_revision = a.revision
      and la.ilr_id = stg.ilr_id
    and a.month < A_MONTH;

  return 'OK';
  exception when others then
    PKG_PP_LOG.P_WRITE_MESSAGE('ERROR');
    PKG_PP_LOG.P_WRITE_MESSAGE('Error Copying Asset Schedule: ' || sqlerrm);
    return 'Error Copying Asset Schedule: ' || sqlerrm;
end f_copy_asset_schedule_rows;


function f_copy_ilr_schedule_rows(A_MONTH in date)
  return varchar2
  is
  counter number;
  v_ilr_id number:=null;
  begin

  select count(1)
  into counter
  from ls_asset
  where ls_asset_id = g_copy_ls_asset_id;

  if counter=1 then
    select ilr_id
    into v_ilr_id
    from ls_asset where ls_asset_id =  G_COPY_LS_ASSET_ID;
  end if;

  delete from ls_ilr_schedule
  where (ilr_id, revision, set_of_books_id) in
  (select ilr_id, revision, set_of_books_id
   from ls_ilr_stg);

   insert into LS_ILR_SCHEDULE
    (ilr_id,
    REVISION,
    SET_OF_BOOKS_ID,
    MONTH,
    RESIDUAL_AMOUNT,
    TERM_PENALTY,
    BPO_PRICE,
    BEG_CAPITAL_COST,
    END_CAPITAL_COST,
    BEG_OBLIGATION,
    END_OBLIGATION,
    BEG_LT_OBLIGATION,
    END_LT_OBLIGATION,
    INTEREST_ACCRUAL,
    PRINCIPAL_ACCRUAL,
    INTEREST_PAID,
    PRINCIPAL_PAID,
    EXECUTORY_ACCRUAL1,
    EXECUTORY_ACCRUAL2,
    EXECUTORY_ACCRUAL3,
    EXECUTORY_ACCRUAL4,
    EXECUTORY_ACCRUAL5,
    EXECUTORY_ACCRUAL6,
    EXECUTORY_ACCRUAL7,
    EXECUTORY_ACCRUAL8,
    EXECUTORY_ACCRUAL9,
    EXECUTORY_ACCRUAL10,
    EXECUTORY_PAID1,
    EXECUTORY_PAID2,
    EXECUTORY_PAID3,
    EXECUTORY_PAID4,
    EXECUTORY_PAID5,
    EXECUTORY_PAID6,
    EXECUTORY_PAID7,
    EXECUTORY_PAID8,
    EXECUTORY_PAID9,
    EXECUTORY_PAID10,
    CONTINGENT_ACCRUAL1,
    CONTINGENT_ACCRUAL2,
    CONTINGENT_ACCRUAL3,
    CONTINGENT_ACCRUAL4,
    CONTINGENT_ACCRUAL5,
    CONTINGENT_ACCRUAL6,
    CONTINGENT_ACCRUAL7,
    CONTINGENT_ACCRUAL8,
    CONTINGENT_ACCRUAL9,
    CONTINGENT_ACCRUAL10,
    CONTINGENT_PAID1,
    CONTINGENT_PAID2,
    CONTINGENT_PAID3,
    CONTINGENT_PAID4,
    CONTINGENT_PAID5,
    CONTINGENT_PAID6,
    CONTINGENT_PAID7,
    CONTINGENT_PAID8,
    CONTINGENT_PAID9,
    CONTINGENT_PAID10,
    IS_OM,
    BEG_DEFERRED_RENT,
    DEFERRED_RENT,
    END_DEFERRED_RENT,
    BEG_ST_DEFERRED_RENT,
    END_ST_DEFERRED_RENT)
    select
    stg.ilr_id,
    stg.revision,
    a.SET_OF_BOOKS_ID,
    a.MONTH,
    a.RESIDUAL_AMOUNT,
    a.TERM_PENALTY,
    a.BPO_PRICE,
    a.BEG_CAPITAL_COST,
    a.END_CAPITAL_COST,
    a.BEG_OBLIGATION,
    a.END_OBLIGATION,
    decode(a.is_om, 1, 0, a.BEG_LT_OBLIGATION),
    decode(a.is_om,1,0,a.END_LT_OBLIGATION),
    a.INTEREST_ACCRUAL,
    a.PRINCIPAL_ACCRUAL,
    a.INTEREST_PAID,
    a.PRINCIPAL_PAID,
    a.EXECUTORY_ACCRUAL1,
    a.EXECUTORY_ACCRUAL2,
    a.EXECUTORY_ACCRUAL3,
    a.EXECUTORY_ACCRUAL4,
    a.EXECUTORY_ACCRUAL5,
    a.EXECUTORY_ACCRUAL6,
    a.EXECUTORY_ACCRUAL7,
    a.EXECUTORY_ACCRUAL8,
    a.EXECUTORY_ACCRUAL9,
    a.EXECUTORY_ACCRUAL10,
    a.EXECUTORY_PAID1,
    a.EXECUTORY_PAID2,
    a.EXECUTORY_PAID3,
    a.EXECUTORY_PAID4,
    a.EXECUTORY_PAID5,
    a.EXECUTORY_PAID6,
    a.EXECUTORY_PAID7,
    a.EXECUTORY_PAID8,
    a.EXECUTORY_PAID9,
    a.EXECUTORY_PAID10,
    a.CONTINGENT_ACCRUAL1,
    a.CONTINGENT_ACCRUAL2,
    a.CONTINGENT_ACCRUAL3,
    a.CONTINGENT_ACCRUAL4,
    a.CONTINGENT_ACCRUAL5,
    a.CONTINGENT_ACCRUAL6,
    a.CONTINGENT_ACCRUAL7,
    a.CONTINGENT_ACCRUAL8,
    a.CONTINGENT_ACCRUAL9,
    a.CONTINGENT_ACCRUAL10,
    a.CONTINGENT_PAID1,
    a.CONTINGENT_PAID2,
    a.CONTINGENT_PAID3,
    a.CONTINGENT_PAID4,
    a.CONTINGENT_PAID5,
    a.CONTINGENT_PAID6,
    a.CONTINGENT_PAID7,
    a.CONTINGENT_PAID8,
    a.CONTINGENT_PAID9,
    a.CONTINGENT_PAID10,
    a.IS_OM,
    a.BEG_DEFERRED_RENT,
    a.DEFERRED_RENT,
    a.END_DEFERRED_RENT,
    a.BEG_ST_DEFERRED_RENT,
    a.END_ST_DEFERRED_RENT
    from ls_ilr_schedule a, ls_ilr ilr, ls_ilr_stg stg
    where a.ilr_id = ilr.ilr_id
      and a.ilr_id = nvl(V_ILR_ID, a.ilr_id)
      and a.set_of_books_id=stg.set_of_books_id
      and ilr.current_revision = a.revision
      and ilr.ilr_id = stg.ilr_id
    and a.month < A_MONTH;

  return 'OK';
  exception when others then
    return 'Error Copying ILR Schedule: ' || sqlerrm;
end f_copy_ilr_schedule_rows;

   function F_LONG_TERM_OBLIGATION return varchar2 is
	L_STATUS varchar2(2000);
	type STG_MONTHS is table of ls_ilr_asset_schedule_stg%rowtype;
	L_STG_MONTHS STG_MONTHS;
   begin
	L_STATUS := 'setting long term obligation';

--L_STG_MONTHS will contain the shifted values
  select s2.* bulk collect into L_STG_MONTHS from ls_ilr_asset_schedule_stg s1, ls_ilr_asset_schedule_stg s2
  where s1.id < 0
  and s2.is_om = 0
  and exists
  (
    select 1
    from ls_ilr_asset_schedule_stg s2
    where s2.month = add_months(s1.month, 12 + s2.prepay_switch)
    and s1.ls_asset_id = s2.ls_asset_id
    and s1.set_of_books_id = s2.set_of_books_id
    and s1.revision = s2.revision
  );

	--Update the staging table with the shifted values
	forall ndx in indices of L_STG_MONTHS
    update ls_ilr_asset_schedule_stg s1
    set beg_lt_obligation = L_STG_MONTHS(ndx).beg_obligation,
        end_lt_obligation = L_STG_MONTHS(ndx).end_obligation
    where add_months(s1.month, 12 + L_STG_MONTHS(ndx).prepay_switch) = L_STG_MONTHS(ndx).month
    and s1.ls_asset_id = L_STG_MONTHS(ndx).ls_asset_id
    and s1.set_of_books_id = L_STG_MONTHS(ndx).set_of_books_id
    and s1.revision = L_STG_MONTHS(ndx).revision;

    update ls_ilr_asset_schedule_stg
    set beg_lt_obligation = 0,
        end_lt_obligation = 0
    where is_om = 1;
	return 'OK';
  exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
		return l_status;
   end F_LONG_TERM_OBLIGATION;



   function F_LOAD_COMPONENTS(A_MONTH in date:=null) return varchar2 is
	L_STATUS varchar2(2000);
  begin
	L_STATUS := 'inserting component months into schedule';

	insert into ls_ilr_asset_schedule_stg
	(ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, MONTH, AMOUNT,
	RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, PREPAY_SWITCH, PAYMENT_MONTH,
	MONTHS_TO_ACCRUE, RATE, NPV, CURRENT_LEASE_COST, BEG_CAPITAL_COST,
	END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION,
	UNPAID_BALANCE, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
	EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
	EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
	EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
	EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9,
	EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
	CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
	CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7,
	CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
	IS_OM, PAYMENT_TERM_TYPE_ID,
	BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT)
	select -1 * row_number() over(partition by c.ls_asset_id, c.set_of_books_id order by cal.month desc) as id,
	  c.ilr_id,
	  c.revision,
	  c.ls_asset_id,
	  c.set_of_books_id,
	  cal.month,
	  0 as amount,
	  0 as residual_amount,
	  0 as term_penalty,
	  0 as bpo_price,
	  c.prepay_switch as prepay_switch,
	  0 as payment_month,
	  1 as months_to_accrue,
	  0 as rate,
	  null as npv,
	  null as current_lease_cost,
	  case when month = comp_cap_month and comp_cap_month = A_MONTH then end_capital_cost else in_svc_capital_cost end  as beg_capital_cost,
	  in_svc_capital_cost as end_capital_cost,
	  in_svc_capital_cost as beg_obligation,
	  in_svc_capital_cost as end_obligation,
	  case when month = comp_cap_month and comp_cap_month = A_MONTH then beg_lt_obligation else in_svc_beg_lt_obligation end as beg_lt_obligation, /* Fix later */
	  in_svc_end_lt_obligation as end_lt_obligation,
	  null as unpaid_balance,
	  0 as INTEREST_ACCRUAL,
	  0 as PRINCIPAL_ACCRUAL,
	  0 as INTEREST_PAID,
	  0 as PRINCIPAL_PAID,
	  0 as EXECUTORY_ACCRUAL1,
	  0 as EXECUTORY_ACCRUAL2,
	  0 as EXECUTORY_ACCRUAL3,
	  0 as EXECUTORY_ACCRUAL4,
	  0 as EXECUTORY_ACCRUAL5,
	  0 as EXECUTORY_ACCRUAL6,
	  0 as EXECUTORY_ACCRUAL7,
	  0 as EXECUTORY_ACCRUAL8,
	  0 as EXECUTORY_ACCRUAL9,
	  0 as EXECUTORY_ACCRUAL10,
	  0 as EXECUTORY_PAID1,
	  0 as EXECUTORY_PAID2,
	  0 as EXECUTORY_PAID3,
	  0 as EXECUTORY_PAID4,
	  0 as EXECUTORY_PAID5,
	  0 as EXECUTORY_PAID6,
	  0 as EXECUTORY_PAID7,
	  0 as EXECUTORY_PAID8,
	  0 as EXECUTORY_PAID9,
	  0 as EXECUTORY_PAID10,
	  0 as CONTINGENT_ACCRUAL1,
	  0 as CONTINGENT_ACCRUAL2,
	  0 as CONTINGENT_ACCRUAL3,
	  0 as CONTINGENT_ACCRUAL4,
	  0 as CONTINGENT_ACCRUAL5,
	  0 as CONTINGENT_ACCRUAL6,
	  0 as CONTINGENT_ACCRUAL7,
	  0 as CONTINGENT_ACCRUAL8,
	  0 as CONTINGENT_ACCRUAL9,
	  0 as CONTINGENT_ACCRUAL10,
	  0 as CONTINGENT_PAID1,
	  0 as CONTINGENT_PAID2,
	  0 as CONTINGENT_PAID3,
	  0 as CONTINGENT_PAID4,
	  0 as CONTINGENT_PAID5,
	  0 as CONTINGENT_PAID6,
	  0 as CONTINGENT_PAID7,
	  0 as CONTINGENT_PAID8,
	  0 as CONTINGENT_PAID9,
	  0 as CONTINGENT_PAID10,
	  is_om as IS_OM,
	  2 as PAYMENT_TERM_TYPE_ID,
	  0 as BEG_DEFERRED_RENT,
	  0 as DEFERRED_RENT,
	  0 as END_DEFERRED_RENT,
	  0 as BEG_ST_DEFERRED_RENT,
	  0 as END_ST_DEFERRED_RENT
	  From
	  (select distinct la.ls_asset_id, trunc(interim_interest_start_date,'month') comp_cap_month, in_svc.prepay_switch,
			  in_svc.month as in_svc_date, ilr.ilr_id, in_svc.end_capital_cost as in_svc_capital_cost, in_svc.revision, in_svc.end_obligation as in_svc_end_obligation,
			  in_svc.beg_lt_obligation as in_svc_beg_lt_obligation, in_svc.end_lt_obligation as in_svc_end_lt_obligation,
			  nvl(current_sched.end_obligation,0) as end_obligation, nvl(current_sched.end_capital_cost,0) as end_capital_cost, nvl(current_sched.set_of_books_id,1) as set_of_books_id,
			  nvl(current_sched.beg_lt_obligation,0) as beg_lt_obligation, nvl(current_sched.end_lt_obligation,0) as end_lt_obligation, lct.is_om
	  from ls_component_charge cc, ls_component lc, ls_asset la, ls_ilr ilr, ls_lease ll, ls_ilr_options ilro, ls_lease_cap_type lct,
		(select a.* from ls_ilr_asset_schedule_stg a where id = 1 ) in_svc,
		(select las.ls_asset_id, end_obligation, end_capital_cost, set_of_books_id, end_lt_obligation, beg_lt_obligation
		from ls_asset_schedule las, ls_asset la
		where las.ls_asset_id in (select ls_asset_id from ls_ilr_asset_schedule_stg)
		  and la.approved_revision = las.revision
		  and las.month = A_MONTH
		  and las.ls_asset_id = la.ls_asset_id) current_sched
	  where la.ls_asset_id=lc.ls_asset_id
		and lc.component_id = cc.component_id
		and la.ilr_id=ilr.ilr_id
		and ilr.lease_id = ll.lease_id
		and la.ls_asset_id in (select ls_asset_id from ls_ilr_asset_schedule_stg)
		and in_svc.ls_asset_id = la.ls_asset_id
		and la.ls_asset_id = current_sched.ls_asset_id (+)
		and ilro.ilr_id = ilr.ilr_id
		and ilro.revision = in_svc.revision
		and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
		and trunc(interim_interest_start_date,'MONTH') = A_MONTH) c, pp_calendar cal
	  where c.comp_cap_month>=A_MONTH
		and cal.month >= c.comp_cap_month
		and cal.month < c.in_svc_date;


	  L_STATUS:=f_long_term_obligation;

	  if L_STATUS <> 'OK' then
		  return 'Error setting long term obligation on component rows : ' || L_STATUS;
	  end if;

    return 'OK';
  exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
		     return l_status;
   end F_LOAD_COMPONENTS;

   --**************************************************************************
   --                            P_CHECK_RECALC_NPV
   -- Recalc NPV after determing IRR for any ILRs that have an IRR
   -- set the process_npv to be 0 where irr is null
   --**************************************************************************
   procedure P_CHECK_RECALC_NPV is

   begin
      -- do not re-calculate NPV if OM
      update LS_ILR_STG set PROCESS_NPV = 0 where IS_OM = 1;

      -- do not recalc NPV if IRR was not found
      update LS_ILR_STG
         set PROCESS_NPV = 0
       where IRR is null
         and PROCESS_NPV = 1;

      -- do not recalc NPV if ANNUAL IRR not between 0 and 100
      update LS_ILR_STG A
         set A.PROCESS_NPV = 0, A.VALIDATION_MESSAGE = 'IRR Calculation not between 0 and 100%'
       where A.PROCESS_NPV = 1
         and (POWER((1 + A.IRR), 12) - 1) not between 0 and 1
         and lower(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY( 'Lease IRR Validation Control',-1)))<>'no';

      update LS_ILR_SCHEDULE_STG S
         set PROCESS_NPV = 0
       where exists (select 1
                from LS_ILR_STG L
               where L.ILR_ID = S.ILR_ID
                 and L.REVISION = S.REVISION
                 and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                 and L.PROCESS_NPV = 0);

      -- set the rate to be the irr for the schedule
      update LS_ILR_SCHEDULE_STG S
         set RATE =
              (select L.IRR
                 from LS_ILR_STG L
                where L.ILR_ID = S.ILR_ID
                  and L.REVISION = S.REVISION
                  and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                  and L.PROCESS_NPV = 1)
       where exists (select 1
                from LS_ILR_STG L
               where L.ILR_ID = S.ILR_ID
                 and L.REVISION = S.REVISION
                 and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                 and L.PROCESS_NPV = 1);
   end P_CHECK_RECALC_NPV;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   --**************************************************************************
   --                            F_ASSET_ALLOC_NPV
   -- This function will allocate the NPV of the schedule down to the assets
   --**************************************************************************
   function F_ASSET_ALLOC_NPV return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      insert into LS_ILR_ASSET_STG
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, CURRENT_LEASE_COST, ALLOC_NPV,
          RESIDUAL_AMOUNT, RESIDUAL_NPV, NPV_MINUS_RESIDUAL_NPV, TERM_PENALTY, BPO_PRICE, ALLOC_DEFERRED_RENT, IS_OM)
         with LS_ASSET_VIEW as
          (select LA.ILR_ID,
                  LA.LS_ASSET_ID,
                  LA.FMV as CURRENT_LEASE_COST,
                  case when la.estimated_residual = 0 and lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Amort to Guaranteed Residual', la.COMPANY_ID))) = 'yes' then
                  NVL(LA.GUARANTEED_RESIDUAL_AMOUNT, 0)
                  ELSE
                    /* CJS 3/7/17 Add system control to account for capitalization of estimated residual */
                    Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', la.COMPANY_ID))), 'no', 0, round(la.ESTIMATED_RESIDUAL * la.FMV, 2))
                  end as RESIDUAL_AMOUNT,
                  LS.SET_OF_BOOKS_ID,
                  LS.REVISION,
                  NVL(RATIO_TO_REPORT(LA.FMV)
                      OVER(partition by LS.ILR_ID, LS.SET_OF_BOOKS_ID, LS.REVISION),
                      1) as PCT_SPREAD
             from LS_ASSET LA, LS_ILR_STG LS, LS_ILR_ASSET_MAP M
            where LA.ILR_ID = LS.ILR_ID
              and M.ILR_ID = LS.ILR_ID
              and M.REVISION = LS.REVISION
              and M.LS_ASSET_ID = LA.LS_ASSET_ID
			  and la.ls_asset_status_id <> 4),
         LS_MONTHS_VIEW as
          (select Z.ILR_ID, Z.THE_LENGTH + 1 as THE_LENGTH, Z.RATE, Z.SET_OF_BOOKS_ID, Z.REVISION
             from (select MONTHS_BETWEEN(max(month)
                                         OVER(partition by ILR_ID, SET_OF_BOOKS_ID, REVISION),
                                         month) as THE_LENGTH,
                          ID,
                          month,
                          ILR_ID,
                          RATE,
                          SET_OF_BOOKS_ID,
                          REVISION
                     from LS_ILR_SCHEDULE_STG) Z
            where Z.ID = 1)
         select ROW_NUMBER() OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc, L.LS_ASSET_ID) as ID,
                S.ILR_ID,
                S.REVISION,
                L.LS_ASSET_ID,
                S.SET_OF_BOOKS_ID,
                L.CURRENT_LEASE_COST,
                ROUND(S.NPV * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       S.NPV - sum(ROUND(S.NPV * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0) as ALLOC_PAYMENT_WITH_PLUG,
                L.RESIDUAL_AMOUNT,
                ROUND(L.RESIDUAL_AMOUNT / POWER(1 + M.RATE, M.THE_LENGTH - S.PREPAY_SWITCH), 2),
                ROUND(S.NPV * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       S.NPV - sum(ROUND(S.NPV * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0) -
                ROUND(L.RESIDUAL_AMOUNT / POWER(1 + M.RATE, M.THE_LENGTH - S.PREPAY_SWITCH), 2),
                ROUND(S.TERM_PENALTY * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       S.TERM_PENALTY - sum(ROUND(S.TERM_PENALTY * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0),
                ROUND(S.BPO_PRICE * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       S.BPO_PRICE - sum(ROUND(S.BPO_PRICE * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0),
                ROUND(S.DEFERRED_RENT_BALANCE * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID order by L.CURRENT_LEASE_COST desc,
                            L.LS_ASSET_ID),
                       1,
                       S.DEFERRED_RENT_BALANCE - sum(ROUND(S.DEFERRED_RENT_BALANCE * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID),
                       0),
                S.IS_OM
           from LS_ILR_STG S, LS_ASSET_VIEW L, LS_MONTHS_VIEW M
          where S.ILR_ID = L.ILR_ID
            and S.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
            and S.REVISION = L.REVISION
            and S.ILR_ID = M.ILR_ID
            and S.SET_OF_BOOKS_ID = M.SET_OF_BOOKS_ID
            and S.REVISION = M.REVISION;

	  L_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ASSET_ALLOC_NPV;

   --**************************************************************************
   --                            F_ALLOCATE_ASSET_PAYMENTS
   -- This function will allocate the payments to the assets
   -- Spreads the payments based on pct current lease obligation
   --**************************************************************************
   function F_ALLOCATE_ASSET_PAYMENTS return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Starting Allocation of payment to Assets';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_SCHEDULE_STG
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, month, PREPAY_SWITCH, RATE,
          PAYMENT_MONTH, MONTHS_TO_ACCRUE, CURRENT_LEASE_COST, AMOUNT, RESIDUAL_AMOUNT,
          BEG_CAPITAL_COST, BEG_OBLIGATION, TERM_PENALTY, BPO_PRICE, CONTINGENT_PAID1,
          CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6,
          CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, EXECUTORY_PAID1,
          EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6,
          EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, IS_OM,
          PAYMENT_TERM_TYPE_ID)
         with LS_ASSET_VIEW as
          (select LA.ILR_ID,
                  LA.LS_ASSET_ID,
                  LA.CURRENT_LEASE_COST,
                  LA.RESIDUAL_AMOUNT as RESIDUAL_AMOUNT,
                  LA.ALLOC_NPV,
                  nvl(RATIO_TO_REPORT(LA.NPV_MINUS_RESIDUAL_NPV) OVER(partition by LA.ILR_ID, LA.REVISION, LA.SET_OF_BOOKS_ID), 1/(count(*) over(partition by la.ilr_id, la.revision, la.set_of_books_id))) as pct_spread,
                  LA.TERM_PENALTY,
                  LA.BPO_PRICE,
                  LA.ALLOC_DEFERRED_RENT,
                  LA.REVISION,
                  LA.SET_OF_BOOKS_ID
             from LS_ILR_ASSET_STG LA)
         select ROW_NUMBER() OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, L.LS_ASSET_ID order by S.MONTH) as ID,
                S.ILR_ID,
                S.REVISION,
                L.LS_ASSET_ID,
                S.SET_OF_BOOKS_ID,
                S.MONTH,
                S.PREPAY_SWITCH,
                S.RATE,
                S.PAYMENT_MONTH,
                S.MONTHS_TO_ACCRUE,
                L.CURRENT_LEASE_COST,
                ROUND(S.AMOUNT * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.AMOUNT - sum(ROUND(S.AMOUNT * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0) as ALLOC_PAYMENT_WITH_PLUG,
                DECODE(S.RESIDUAL_AMOUNT, 0, 0, L.RESIDUAL_AMOUNT) as RESIDUAL,
                L.ALLOC_NPV - L.ALLOC_DEFERRED_RENT,
                L.ALLOC_NPV,
                DECODE(S.TERM_PENALTY, 0, 0, L.TERM_PENALTY),
                DECODE(S.BPO_PRICE, 0, 0, L.BPO_PRICE),
                ROUND(S.CONTINGENT_PAID1 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID1 - sum(ROUND(S.CONTINGENT_PAID1 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID2 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID2 - sum(ROUND(S.CONTINGENT_PAID2 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID3 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID3 - sum(ROUND(S.CONTINGENT_PAID3 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID4 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID4 - sum(ROUND(S.CONTINGENT_PAID4 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID5 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID5 - sum(ROUND(S.CONTINGENT_PAID5 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID6 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID6 - sum(ROUND(S.CONTINGENT_PAID6 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID7 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID7 - sum(ROUND(S.CONTINGENT_PAID7 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID8 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID8 - sum(ROUND(S.CONTINGENT_PAID8 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID9 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID9 - sum(ROUND(S.CONTINGENT_PAID9 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.CONTINGENT_PAID10 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.CONTINGENT_PAID10 - sum(ROUND(S.CONTINGENT_PAID10 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID1 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID1 - sum(ROUND(S.EXECUTORY_PAID1 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID2 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID2 - sum(ROUND(S.EXECUTORY_PAID2 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID3 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID3 - sum(ROUND(S.EXECUTORY_PAID3 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID4 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID4 - sum(ROUND(S.EXECUTORY_PAID4 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID5 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID5 - sum(ROUND(S.EXECUTORY_PAID5 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID6 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID6 - sum(ROUND(S.EXECUTORY_PAID6 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID7 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID7 - sum(ROUND(S.EXECUTORY_PAID7 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID8 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID8 - sum(ROUND(S.EXECUTORY_PAID8 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID9 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID9 - sum(ROUND(S.EXECUTORY_PAID9 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                ROUND(S.EXECUTORY_PAID10 * L.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by S.ILR_ID,
                            S.REVISION,
                            S.SET_OF_BOOKS_ID,
                            S.MONTH order by S.RATE desc,
                            L.LS_ASSET_ID),
                       1,
                       S.EXECUTORY_PAID10 - sum(ROUND(S.EXECUTORY_PAID10 * L.PCT_SPREAD, 2))
                       OVER(partition by S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.MONTH),
                       0),
                S.IS_OM,
                S.PAYMENT_TERM_TYPE_ID
           from LS_ILR_SCHEDULE_STG S, LS_ASSET_VIEW L
          where S.ILR_ID = L.ILR_ID
            and S.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
            and S.REVISION = L.REVISION;

      -- make sure month with residual is set to 1
      L_STATUS := 'Setting the payment month for residual amounts';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      update LS_ILR_ASSET_SCHEDULE_STG S set S.PAYMENT_MONTH = 1 where RESIDUAL_AMOUNT <> 0;

      -- update the last payment month to be a 2
      update LS_ILR_ASSET_SCHEDULE_STG S
         set S.PAYMENT_MONTH = 2
       where (S.LS_ASSET_ID, S.MONTH) in (select A.LS_ASSET_ID, A.MONTH
                                            from (select B.LS_ASSET_ID,
                                                         B.MONTH,
                                                         ROW_NUMBER() OVER(partition by B.LS_ASSET_ID order by B.MONTH desc) as THE_ROW
                                                    from LS_ILR_ASSET_SCHEDULE_STG B) A
                                           where THE_ROW = 1);

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ALLOCATE_ASSET_PAYMENTS;

   --**************************************************************************
   --                            F_ALLOCATE_CONT_EXEC
   -- This function allocates the executory paid and contingent paid amounts
   -- To be accrued based on payment frequency
   --**************************************************************************
   function F_ALLOCATE_CONT_EXEC return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
      L_CHECK  number;
      L_SQLS   varchar2(32000);
   begin
      L_STATUS := 'Starting to determine contingent and executory accrual amounts';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

	  L_CHECK := 0;
      select count(1)
        into L_CHECK
        from LS_ILR_ASSET_SCHEDULE_STG S2
       where S2.PAYMENT_MONTH in (1, 2)
         and (CONTINGENT_PAID1 <> 0 or CONTINGENT_PAID2 <> 0 or CONTINGENT_PAID3 <> 0 or
             CONTINGENT_PAID4 <> 0 or CONTINGENT_PAID5 <> 0 or CONTINGENT_PAID6 <> 0 or
             CONTINGENT_PAID7 <> 0 or CONTINGENT_PAID8 <> 0 or CONTINGENT_PAID9 <> 0 or
             CONTINGENT_PAID10 <> 0 or EXECUTORY_PAID1 <> 0 or EXECUTORY_PAID2 <> 0 or
             EXECUTORY_PAID3 <> 0 or EXECUTORY_PAID4 <> 0 or EXECUTORY_PAID5 <> 0 or
             EXECUTORY_PAID6 <> 0 or EXECUTORY_PAID7 <> 0 or EXECUTORY_PAID8 <> 0 or
             EXECUTORY_PAID9 <> 0 or EXECUTORY_PAID10 <> 0);

      if L_CHECK > 0 then
FOR I IN (
SELECT COL, replace(col, 'PAID', 'ACCRUAL') ACCRUAL_COL fROM
(SELECT SUM(NVL(CONTINGENT_PAID10, 0)) AMOUNT, 'CONTINGENT_PAID10' COL FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID9, 0)) AMOUNT, 'CONTINGENT_PAID9' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID8, 0)) AMOUNT, 'CONTINGENT_PAID8' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID7, 0)) AMOUNT, 'CONTINGENT_PAID7' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID6, 0)) AMOUNT, 'CONTINGENT_PAID6' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID5, 0)) AMOUNT, 'CONTINGENT_PAID5' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID4, 0)) AMOUNT, 'CONTINGENT_PAID4' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID3, 0)) AMOUNT, 'CONTINGENT_PAID3' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID2, 0)) AMOUNT, 'CONTINGENT_PAID2' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(CONTINGENT_PAID1, 0)) AMOUNT, 'CONTINGENT_PAID1' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID10, 0)) AMOUNT, 'EXECUTORY_PAID10' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID9, 0)) AMOUNT, 'EXECUTORY_PAID9' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID8, 0)) AMOUNT, 'EXECUTORY_PAID8' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID7, 0)) AMOUNT, 'EXECUTORY_PAID7' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID6, 0)) AMOUNT, 'EXECUTORY_PAID6' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID5, 0)) AMOUNT, 'EXECUTORY_PAID5' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID4, 0)) AMOUNT, 'EXECUTORY_PAID4' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID3, 0)) AMOUNT, 'EXECUTORY_PAID3' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID2, 0)) AMOUNT, 'EXECUTORY_PAID2' FROM LS_ILR_ASSET_SCHEDULE_STG UNION
SELECT SUM(NVL(EXECUTORY_PAID1, 0)) AMOUNT, 'EXECUTORY_PAID1' FROM LS_ILR_ASSET_SCHEDULE_STG )
WHERE AMOUNT <> 0)
LOOP
L_SQLS:='
        MERGE INTO LS_ILR_ASSET_SCHEDULE_STG A
        USING (
         with ACCRUAL_MONTHS as
         (
           select ILR_ID, LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID,
            MONTH curr_month, add_months(MONTH, decode(PREPAY_SWITCH, 0, -1, 1) * (MONTHS_TO_ACCRUE - 1)) month_range,
            MONTHS_TO_ACCRUE, PREPAY_SWITCH,
            round(' || I.COL || ' / MONTHS_TO_ACCRUE, 2) as INTEREST_AMT,
            ' || I.COL || ' - ( MONTHS_TO_ACCRUE * round(' || I.COL || ' / MONTHS_TO_ACCRUE, 2)) as ROUNDER
           from LS_ILR_ASSET_SCHEDULE_STG
           where ' || I.COL || ' <> 0
           and MONTHS_TO_ACCRUE <> 1
           and MONTH is not null
           and (ILR_ID, REVISION, SET_OF_BOOKS_ID) in (select bb.ILR_ID, bb.REVISION, BB.SET_OF_BOOKS_ID from LS_ILR_STG bb)
         )
         select a.ID, a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID,
            a.MONTH, a.MONTHS_TO_ACCRUE, a.PREPAY_SWITCH,
            am.INTEREST_AMT + case when a.MONTH = am.CURR_MONTH then am.ROUNDER else 0 end ' || I.ACCRUAL_COL || ',
            am.ROUNDER
         from LS_ILR_ASSET_SCHEDULE_STG a, ACCRUAL_MONTHS am
         where a.month between LEAST(am.CURR_MONTH, am.MONTH_RANGE) and GREATEST(am.CURR_MONTH, am.MONTH_RANGE)
         and a.MONTHS_TO_ACCRUE <> 1
         and a.ILR_ID = am.ILR_ID
         and a.LS_ASSET_ID = am.LS_ASSET_ID
         and a.REVISION = am.REVISION
         and a.SET_OF_BOOKS_ID = am.SET_OF_BOOKS_ID
         and a.MONTH is not null
         and (a.ILR_ID, a.REVISION, A.SET_OF_BOOKS_ID) in (select bb.ILR_ID, bb.REVISION, BB.SET_OF_BOOKS_ID from LS_ILR_STG bb)) B
         ON (A.ID = B.ID AND A.ILR_ID = B.ILR_ID AND A.LS_ASSET_ID = B.LS_ASSET_ID AND A.REVISION = B.REVISION AND A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID)
         WHEN MATCHED THEN UPDATE SET A.' || I.ACCRUAL_COL || ' = B.' || I.ACCRUAL_COL;

L_STATUS:=L_SQLS;
EXECUTE IMMEDIATE L_SQLS;

L_SQLS:='
      UPDATE LS_ILR_ASSET_SCHEDULE_STG
      SET ' || I.ACCRUAL_COL || ' = ' || I.COL || '
      WHERE MONTHS_TO_ACCRUE = 1
        AND MONTH IS NOT NULL
        AND (ILR_ID, REVISION, SET_OF_BOOKS_ID) IN (SELECT ILR_ID, REVISION, SET_OF_BOOKS_ID FROM LS_ILR_STG)';

L_STATUS:=L_SQLS;
EXECUTE IMMEDIATE L_SQLS;


END LOOP;

	  end if;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ALLOCATE_CONT_EXEC;

   --**************************************************************************
   --                            F_ALLOCATE_TO_ASSETS
   -- This function will allocate the payments to the assets
   -- Spreads the payments based on pct current lease cost
   --**************************************************************************
   function F_ALLOCATE_TO_ASSETS return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Starting Allocation to Assets';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      -- allocate the NPV of the ILR to the ASSETS underneath
      L_MSG := F_ASSET_ALLOC_NPV;
      if L_MSG = 'OK' then
         -- Allocate the payments to the assets
         PKG_PP_LOG.P_WRITE_MESSAGE('ALLOCATE Payments');
         L_MSG := F_ALLOCATE_ASSET_PAYMENTS;
         if L_MSG = 'OK' then
            -- allocate the executory and contingent paid amounts
            -- to be accrued based on payment frequency
            PKG_PP_LOG.P_WRITE_MESSAGE('ALLOCATE contingents and executory buckets');
            return F_ALLOCATE_CONT_EXEC;
         end if;
      end if;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_ALLOCATE_TO_ASSETS;


   procedure P_SPREAD_PRIN_ACCRUAL
   is
   type MONTHS_ACCRUAL_REC is record
   (
      ID                LS_ILR_ASSET_SCHEDULE_CALC_STG.ID%type,
      ILR_ID            LS_ILR_ASSET_SCHEDULE_CALC_STG.ILR_ID%type,
      LS_ASSET_ID       LS_ILR_ASSET_SCHEDULE_CALC_STG.LS_ASSET_ID%type,
      REVISION          LS_ILR_ASSET_SCHEDULE_CALC_STG.REVISION%type,
      SET_OF_BOOKS_ID   LS_ILR_ASSET_SCHEDULE_CALC_STG.SET_OF_BOOKS_ID%type,
      MONTH             LS_ILR_ASSET_SCHEDULE_CALC_STG.MONTH%type,
      PRINCIPAL_ACCRUAL LS_ILR_ASSET_SCHEDULE_CALC_STG.PRINCIPAL_ACCRUAL%type,
      MONTHS_TO_ACCRUE  LS_ILR_ASSET_SCHEDULE_CALC_STG.MONTHS_TO_ACCRUE%type,
      PREPAY_SWITCH     LS_ILR_ASSET_SCHEDULE_CALC_STG.PREPAY_SWITCH%type,
      PRINCIPAL_AMT     LS_ILR_ASSET_SCHEDULE_CALC_STG.PRINCIPAL_ACCRUAL%type,
      ROUNDER           LS_ILR_ASSET_SCHEDULE_CALC_STG.PRINCIPAL_ACCRUAL%type
   );

   type MONTHS_ACCRUAL_TAB is table of MONTHS_ACCRUAL_REC index by PLS_INTEGER;

   L_ACCRUAL_MONTHS MONTHS_ACCRUAL_TAB;
   v_count number;

   begin
      PKG_PP_LOG.P_WRITE_MESSAGE('Handle Non Monthly Principal Accrual');

  select count(1)
  into v_count
  from ls_ilr_asset_schedule_calc_stg a, ls_ilr_stg b
  where a.ilr_id = b.ilr_id
    and a.revision = b.revision
    and a.set_of_books_id = b.set_of_books_id
    and nvl(months_to_accrue,0) <> 1;

  if v_count = 0 then
    return;
  end if;
		select *
		bulk collect
		into L_ACCRUAL_MONTHS
		from
		(
			-- figure out the payment months and how much accrual goes in each month
			with ACCRUAL_MONTHS as
			(
				select a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID,
					a.MONTH, a.PRINCIPAL_ACCRUAL, nvl(b.MONTHS_TO_ACCRUE, a.months_to_accrue) as months_to_accrue, a.PREPAY_SWITCH,
					round((case when a.payment_month = 2 then (a.prepay_switch * a.principal_accrual) else 0 end + a.principal_paid) / nvl(b.MONTHS_TO_ACCRUE, a.months_to_accrue), 2) as PRINCIPAL_AMT,
					round((case when a.payment_month = 2 then (a.prepay_switch * a.principal_accrual) else 0 end + a.principal_paid), 2) -
					( nvl(b.MONTHS_TO_ACCRUE, a.months_to_accrue) * round((case when a.payment_month = 2 then (a.prepay_switch * a.principal_accrual) else 0 end
												+ a.principal_paid) / nvl(b.MONTHS_TO_ACCRUE, a.months_to_accrue), 2)) as ROUNDER,
					a.PREPAY_SWITCH * greatest(a.payment_month - 1, 0) as is_last_month
				from LS_ILR_ASSET_SCHEDULE_CALC_STG a, LS_ILR_ASSET_SCHEDULE_CALC_STG b
				where a.PRINCIPAL_PAID <> 0
				and b.MONTHS_TO_ACCRUE <> 1
				and a.MONTH is not null
				and (a.ILR_ID, a.REVISION, a.set_of_books_id) in (select bb.ILR_ID, bb.REVISION, bb.set_of_books_id from LS_ILR_STG bb)
				and a.ilr_id = b.ilr_id (+)
				and a.ls_asset_id = b.ls_asset_id (+)
				and a.revision = b.revision (+)
				and a.set_of_books_id = b.set_of_books_id (+)
				and b.month (+) = add_months(a.month, -1)
			)
			-- FOR each month set the accrual amount and figure out the last month for the rounder.
			select
				a.ID, a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID,
				a.MONTH, a.PRINCIPAL_ACCRUAL, a.MONTHS_TO_ACCRUE, a.PREPAY_SWITCH,
				am.PRINCIPAL_AMT +
					case when a.MONTH = add_months( add_months(am.month, am.is_last_month) , -1 * am.PREPAY_SWITCH )
					then am.ROUNDER else 0 end as principal_amt,
				am.ROUNDER
			from LS_ILR_ASSET_SCHEDULE_CALC_STG a, ACCRUAL_MONTHS am
			where a.month between add_months(add_months(am.month, am.is_last_month), -1 * (am.MONTHS_TO_ACCRUE - (1 - am.PREPAY_SWITCH) ))
								and add_months(add_months(am.month, am.is_last_month), -1 * am.PREPAY_SWITCH)
			and a.MONTHS_TO_ACCRUE <> 1
			and a.ILR_ID = am.ILR_ID
			and a.LS_ASSET_ID = am.LS_ASSET_ID
			and a.REVISION = am.REVISION
			and a.SET_OF_BOOKS_ID = am.SET_OF_BOOKS_ID
			and a.MONTH is not null
			and (a.ILR_ID, a.REVISION, a.set_of_books_id) in (select bb.ILR_ID, bb.REVISION, bb.set_of_books_id from LS_ILR_STG bb)
		)
      ;

      PKG_PP_LOG.P_WRITE_MESSAGE('Principal Accrual to Spread: '|| to_char(L_ACCRUAL_MONTHS.COUNT));

      forall ndx in indices of L_ACCRUAL_MONTHS
         update LS_ILR_ASSET_SCHEDULE_CALC_STG s
         set s.PRINCIPAL_ACCRUAL = L_ACCRUAL_MONTHS(ndx).PRINCIPAL_AMT
         where s.ID = L_ACCRUAL_MONTHS(ndx).ID
         and s.ILR_ID = L_ACCRUAL_MONTHS(ndx).ILR_ID
         and s.LS_ASSET_ID = L_ACCRUAL_MONTHS(ndx).LS_ASSET_ID
         and s.REVISION = L_ACCRUAL_MONTHS(ndx).REVISION
         and s.SET_OF_BOOKS_ID = L_ACCRUAL_MONTHS(ndx).SET_OF_BOOKS_ID
         ;
   end P_SPREAD_PRIN_ACCRUAL;

   procedure P_HANDLE_OM
   is
   type MONTHS_ACCRUAL_REC is record
   (
      ID                LS_ILR_ASSET_SCHEDULE_CALC_STG.ID%type,
      ILR_ID            LS_ILR_ASSET_SCHEDULE_CALC_STG.ILR_ID%type,
      LS_ASSET_ID       LS_ILR_ASSET_SCHEDULE_CALC_STG.LS_ASSET_ID%type,
      REVISION          LS_ILR_ASSET_SCHEDULE_CALC_STG.REVISION%type,
      SET_OF_BOOKS_ID   LS_ILR_ASSET_SCHEDULE_CALC_STG.SET_OF_BOOKS_ID%type,
      MONTH             LS_ILR_ASSET_SCHEDULE_CALC_STG.MONTH%type,
      INTEREST_ACCRUAL  LS_ILR_ASSET_SCHEDULE_CALC_STG.INTEREST_ACCRUAL%type,
      MONTHS_TO_ACCRUE  LS_ILR_ASSET_SCHEDULE_CALC_STG.MONTHS_TO_ACCRUE%type,
      PREPAY_SWITCH     LS_ILR_ASSET_SCHEDULE_CALC_STG.PREPAY_SWITCH%type,
      INTEREST_AMT      LS_ILR_ASSET_SCHEDULE_CALC_STG.INTEREST_ACCRUAL%type,
      ROUNDER           LS_ILR_ASSET_SCHEDULE_CALC_STG.INTEREST_ACCRUAL%type
   );

   type MONTHS_ACCRUAL_TAB is table of MONTHS_ACCRUAL_REC index by PLS_INTEGER;

   L_ACCRUAL_MONTHS MONTHS_ACCRUAL_TAB;

   l_gas_13 varchar2(35);
   v_count number;
   begin
   select count(1)
   into v_count
   from ls_ilr_stg
   where is_om = 1;

   if v_count = 0 then
    return;
   end if;
      PKG_PP_LOG.P_WRITE_MESSAGE('Handle Operating Leases');

	/* CJS 2/17/15 Adding portion to allow to do system control by company*/
	for i in (select distinct company_id from ls_ilr ilr, ls_ilr_asset_schedule_stg stg where ilr.ilr_id = stg.ilr_id)
	loop
	  l_gas_13 := LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Lease Operating Expense as Incurred',
                                          nvl(i.company_id,-1)), 'no'));
      -- for OM records
      -- everything is interest expense not principal (move it).  Also no long term and short term obligations
      update LS_ILR_ASSET_SCHEDULE_CALC_STG
         set BEG_CAPITAL_COST = 0, END_CAPITAL_COST = 0,
             INTEREST_ACCRUAL = INTEREST_ACCRUAL + PRINCIPAL_ACCRUAL,
             INTEREST_PAID = INTEREST_PAID + PRINCIPAL_PAID, PRINCIPAL_ACCRUAL = 0,
             PRINCIPAL_PAID = 0, BEG_LT_OBLIGATION = 0, END_LT_OBLIGATION = 0
       where IS_OM = 1
	   and (ilr_id, revision, set_of_books_id) in (select bb.ILR_ID, bb.REVISION, bb.SET_OF_BOOKS_ID from ls_ilr_stg bb, LS_ILR LI where LI.ILR_ID = bb.ILR_ID and LI.COMPANY_ID = i.company_id);

      -- spread the interest accrual to be correct
      PKG_PP_LOG.P_WRITE_MESSAGE('Spread Operating Lease Rental Accrual Amounts');

if l_gas_13 = 'no' then
		  select *
		  bulk collect
		  into L_ACCRUAL_MONTHS
		from
		(
			with ACCRUAL_MONTHS as
			(
				select ILR_ID, LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, PREPAY_SWITCH,
					max(MONTH) as month,
					sum(INTEREST_ACCRUAL) as interest_accrual, count(1) as MONTHS_TO_ACCRUE,
					round( sum(INTEREST_ACCRUAL) / count(1), 2 ) as INTEREST_AMT,
					sum(INTEREST_ACCRUAL) - ( count(1) *  round( sum(INTEREST_ACCRUAL) / count(1), 2 ) ) as ROUNDER
				from LS_ILR_ASSET_SCHEDULE_CALC_STG
				where IS_OM = 1
				and MONTH is not null
				and (ILR_ID, REVISION, SET_OF_BOOKS_ID) in (select bb.ILR_ID, bb.REVISION, bb.SET_OF_BOOKS_ID from ls_ilr_stg bb, LS_ILR LI where LI.ILR_ID = bb.ILR_ID and LI.COMPANY_ID = i.company_id)
				group by ILR_ID, LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, PREPAY_SWITCH
			)
			select a.ID, a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID,
				a.MONTH, a.INTEREST_ACCRUAL, a.MONTHS_TO_ACCRUE, a.PREPAY_SWITCH,
				am.INTEREST_AMT + case when a.MONTH = am.MONTH then am.ROUNDER else 0 end,
				am.ROUNDER
			from LS_ILR_ASSET_SCHEDULE_CALC_STG a, ACCRUAL_MONTHS am
			where a.ILR_ID = am.ILR_ID
			and a.LS_ASSET_ID = am.LS_ASSET_ID
			and a.REVISION = am.REVISION
			and a.SET_OF_BOOKS_ID = am.SET_OF_BOOKS_ID
			and a.IS_OM = 1
			and a.MONTH is not null
			and (a.ILR_ID, a.REVISION, a.SET_OF_BOOKS_ID) in (select bb.ILR_ID, bb.REVISION, bb.SET_OF_BOOKS_ID from ls_ilr_stg bb, LS_ILR LI where LI.ILR_ID = bb.ILR_ID and LI.COMPANY_ID = i.company_id)
		);
	else
		select *
      bulk collect
      into L_ACCRUAL_MONTHS
      from
      (
         with ACCRUAL_MONTHS as
         (
           select ILR_ID, LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID,
            MONTH curr_month, add_months(MONTH, decode(PREPAY_SWITCH, 0, -1, 1) * (MONTHS_TO_ACCRUE - 1) ) month_range,
            INTEREST_ACCRUAL, MONTHS_TO_ACCRUE, PREPAY_SWITCH,
            round(INTEREST_PAID / MONTHS_TO_ACCRUE, 2) as INTEREST_AMT,
            INTEREST_PAID - ( MONTHS_TO_ACCRUE * round(INTEREST_PAID / MONTHS_TO_ACCRUE, 2)) as ROUNDER
           from LS_ILR_ASSET_SCHEDULE_CALC_STG
           where INTEREST_PAID <> 0
           and MONTHS_TO_ACCRUE <> 1
           and IS_OM = 1
           and MONTH is not null
           and (ILR_ID, REVISION, SET_OF_BOOKS_ID) in (select bb.ILR_ID, bb.REVISION, bb.SET_OF_BOOKS_ID from ls_ilr_stg bb, LS_ILR LI where LI.ILR_ID = bb.ILR_ID and LI.COMPANY_ID = i.company_id)
         )
         select a.ID, a.ILR_ID, a.LS_ASSET_ID, a.REVISION, a.SET_OF_BOOKS_ID,
            a.MONTH, a.INTEREST_ACCRUAL, a.MONTHS_TO_ACCRUE, a.PREPAY_SWITCH,
            am.INTEREST_AMT + case when a.MONTH = am.CURR_MONTH then am.ROUNDER else 0 end,
            am.ROUNDER
         from LS_ILR_ASSET_SCHEDULE_CALC_STG a, ACCRUAL_MONTHS am
         where a.month between LEAST(am.CURR_MONTH, am.MONTH_RANGE) and GREATEST(am.CURR_MONTH, am.MONTH_RANGE)
         and a.MONTHS_TO_ACCRUE <> 1
         and a.ILR_ID = am.ILR_ID
         and a.LS_ASSET_ID = am.LS_ASSET_ID
         and a.REVISION = am.REVISION
         and a.SET_OF_BOOKS_ID = am.SET_OF_BOOKS_ID
         and a.IS_OM = 1
         and a.MONTH is not null
         and (a.ILR_ID, a.REVISION, a.SET_OF_BOOKS_ID) in (select bb.ILR_ID, bb.REVISION, bb.SET_OF_BOOKS_ID from ls_ilr_stg bb, LS_ILR LI where LI.ILR_ID = bb.ILR_ID and LI.COMPANY_ID = i.company_id)
      )
      ;
	end if;

      PKG_PP_LOG.P_WRITE_MESSAGE('Interest Accrual to Respread: '|| to_char(L_ACCRUAL_MONTHS.COUNT));

      forall ndx in indices of L_ACCRUAL_MONTHS
         update LS_ILR_ASSET_SCHEDULE_CALC_STG s
         set s.INTEREST_ACCRUAL = L_ACCRUAL_MONTHS(ndx).INTEREST_AMT
         where s.ID = L_ACCRUAL_MONTHS(ndx).ID
         and s.ILR_ID = L_ACCRUAL_MONTHS(ndx).ILR_ID
         and s.LS_ASSET_ID = L_ACCRUAL_MONTHS(ndx).LS_ASSET_ID
         and s.REVISION = L_ACCRUAL_MONTHS(ndx).REVISION
         and s.SET_OF_BOOKS_ID = L_ACCRUAL_MONTHS(ndx).SET_OF_BOOKS_ID
         ;


	/* CJS End FAS 13 loop by company */
	end loop;

   end P_HANDLE_OM;

   procedure P_HANDLE_DEFERRED_RENT is 
      L_MSG varchar2(2000);
   begin
   --Moving this to separate procedure, since not just isolated to operating leases, won't always run in P_HANDLE_OM
   L_MSG:='Calculating deferred rent/rolling deferred rent forward';
   PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
   
   merge into LS_ILR_ASSET_SCHEDULE_CALC_STG A
	using (
            select ID,
            ILR_ID,
            REVISION,
            LS_ASSET_ID,
            SET_OF_BOOKS_ID,
            month,
            INT_ACCRUAL,
            INT_PAID,
            IS_OM,
            BEG_DEFERRED_RENT,
            DEFERRED_RENT,
            END_DEFERRED_RENT,
            BEG_ST_DEFERRED_RENT,
            END_ST_DEFERRED_RENT
        from LS_ILR_ASSET_SCHEDULE_CALC_STG A, LS_ILR_ASSET_STG B
        where A.ILR_ID = B.ILR_ID
        and A.REVISION = B.REVISION
        and A.LS_ASSET_ID = B.LS_ASSET_ID
        and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID
        MODEL partition by(A.ILR_ID, A.LS_ASSET_ID, A.REVISION, A.SET_OF_BOOKS_ID) DIMENSION by(ROW_NUMBER() OVER(partition by A.ILR_ID, A.LS_ASSET_ID, A.REVISION, A.SET_OF_BOOKS_ID order by month) as ID) MEASURES(INTEREST_ACCRUAL as INT_ACCRUAL, INTEREST_PAID as INT_PAID, month, MONTHS_BETWEEN(max(month) OVER(partition by A.ILR_ID, A.LS_ASSET_ID, A.REVISION, A.SET_OF_BOOKS_ID), month) as MONTH_ID, A.IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, 0 as ST_DEFERRED_RENT, END_ST_DEFERRED_RENT, PAYMENT_MONTH, 0 as INT_STORED, MONTHS_TO_ACCRUE, 0 AS FUTURE_PAID, 0 as MONTHS_REM_ACCRUE, a.PREPAY_SWITCH, 0 as PAID_SPREAD, 0 as PAID_ACCRUED, 0 as PAID_ACCRUED_ROUND, 0 as ADD_A_PENNY_PAID_SPREAD, ALLOC_DEFERRED_RENT)
      IGNORE NAV RULES UPSERT ITERATE(100000) UNTIL ITERATION_NUMBER = MONTH_ID [ 1 ]
      (
              DEFERRED_RENT [ 0 ] = 0, BEG_DEFERRED_RENT [ 0 ] = 0, END_DEFERRED_RENT [ 0 ] = DECODE(IS_OM [ 1 ], 1, 0, ALLOC_DEFERRED_RENT [ 1 ]),

              BEG_DEFERRED_RENT [ 1 ] = END_DEFERRED_RENT [ 0 ],

              BEG_ST_DEFERRED_RENT [ 0 ] = 0, ST_DEFERRED_RENT [ 0 ] = 0, END_ST_DEFERRED_RENT [ 0 ] = 0,

              INT_PAID [ 0 ] = 0, PAID_ACCRUED [ 0 ] = 0, PAID_ACCRUED_ROUND [ 0 ] = 0,

              --Need months to accrue + 1 in the 0 spot for arrears leases
              MONTHS_REM_ACCRUE [ 0 ] = (1 - PREPAY_SWITCH [ 1 ]) * (MONTHS_TO_ACCRUE [ 1 ] + 1),

              --For arrears, if last month was a payment, start back over on the remaining months to accrue
              --For prepaid, if this month is a payment, start back over on the remaining months to accrue
              --For both, if it's the last month (2) and there is actually a payment, restart months to accrue, if no payment, continue decrementing
              --Normal month, just decrement months to accrue from last month
              MONTHS_REM_ACCRUE [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - (1 - PREPAY_SWITCH [ CV(ID) ]) ], 1,
                      MONTHS_TO_ACCRUE [ CV(ID) ], 2,
                          DECODE(INT_PAID [ CV(ID) ], 0,
                              MONTHS_REM_ACCRUE [ CV(ID) - 1 ] - 1, MONTHS_TO_ACCRUE [ CV(ID) ]),
                      MONTHS_REM_ACCRUE [ CV(ID) - 1 ] - 1),

              --For arrears, need to be able to reference the payment amount for the period in non-payment months; for monthlys, just use that months rent
              FUTURE_PAID [ ITERATION_NUMBER + 1 ] = INT_PAID [ CV(ID) + DECODE(MONTHS_TO_ACCRUE [ CV(ID) ], 1, 0, MONTHS_REM_ACCRUE [ CV(ID) ] - (1 - PREPAY_SWITCH [ CV(ID) ])) ],

              --if prepaid, find the amount paid for the period and use for reference; if arrears, use the future_paid column; account for final month (2)
              INT_STORED [ ITERATION_NUMBER + 1 ] = DECODE(PREPAY_SWITCH [ CV(ID) ], 1, DECODE(PAYMENT_MONTH [ CV(ID) ], 1, INT_PAID [ CV(ID) ], 2, DECODE(INT_PAID [ CV(ID) ], 0 , INT_STORED [ CV(ID) - 1 ], INT_PAID [ CV(ID) ]), INT_STORED [ CV(ID) - 1 ]), FUTURE_PAID [ CV(ID) ]),

              PAID_SPREAD [ ITERATION_NUMBER + 1 ] = INT_STORED [ CV(ID) ] / MONTHS_TO_ACCRUE [ CV(ID) ],

              PAID_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - (1 - PREPAY_SWITCH [ CV(ID) ]) ], 1,
                                                PAID_SPREAD [ CV(ID) ], 2, DECODE(INT_PAID [ CV(ID) ], 0, PAID_ACCRUED [ CV(ID) - 1 ] + PAID_SPREAD [ CV(ID) ],
                                                PAID_SPREAD [ CV(ID) ]), PAID_ACCRUED [ CV(ID) - 1 ] + PAID_SPREAD [ CV(ID) ]),

              PAID_ACCRUED_ROUND [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - (1 - PREPAY_SWITCH [ CV(ID) ]) ], 1,
                                                ROUND(PAID_SPREAD [ CV(ID) ], 2), 2, DECODE(INT_PAID [ CV(ID) ], 0, PAID_ACCRUED_ROUND [ CV(ID) - 1 ] + ROUND(PAID_SPREAD [ CV(ID) ], 2),
                                                ROUND(PAID_SPREAD [ CV(ID) ], 2)), PAID_ACCRUED_ROUND [ CV(ID) - 1 ] + ROUND(PAID_SPREAD [ CV(ID) ], 2)),

              ADD_A_PENNY_PAID_SPREAD [ ITERATION_NUMBER + 1 ] = DECODE(MONTHS_REM_ACCRUE [ CV(ID) ], 1, ROUND(INT_STORED [ CV(ID) ], 2) - DECODE(MONTHS_TO_ACCRUE [ CV(ID) ], 1, 0, PAID_ACCRUED_ROUND [ CV(ID) - 1 ]) - ROUND(PAID_SPREAD [ CV(ID) ], 2), 0),

              PAID_SPREAD [ ITERATION_NUMBER + 1 ] = PAID_SPREAD [ CV(ID) ]  + ADD_A_PENNY_PAID_SPREAD [ CV(ID) ],

              PAID_ACCRUED [ ITERATION_NUMBER + 1 ] = PAID_ACCRUED [ CV(ID) ] + ADD_A_PENNY_PAID_SPREAD [ CV(ID) ],

              DEFERRED_RENT [ ITERATION_NUMBER + 1 ] = DECODE(IS_OM [ CV(ID) ], 1, INT_ACCRUAL [ CV(ID) ] - ROUND(PAID_SPREAD [ CV(ID) ], 2), 0),

              END_DEFERRED_RENT [ ITERATION_NUMBER + 1 ] = BEG_DEFERRED_RENT [ CV(ID) ] + DEFERRED_RENT [ CV(ID) ],

              BEG_DEFERRED_RENT [ ITERATION_NUMBER + 1 ] = END_DEFERRED_RENT [ CV(ID) - 1 ],

              BEG_DEFERRED_RENT [ ITERATION_NUMBER + 2 ] = END_DEFERRED_RENT [ CV(ID) - 1 ],

              -- CJS temporary for testing purposes to avoid skewing ST numbers until they're correct
			  -- ST_DEFERRED_RENT [ ITERATION_NUMBER - 11 ] = INT_PAID [ ITERATION_NUMBER + 1 ] - INT_PAID [ ITERATION_NUMBER - 11 ],

              BEG_ST_DEFERRED_RENT [ ITERATION_NUMBER - 11 ] = END_ST_DEFERRED_RENT [ CV(ID) - 1 ],

              END_ST_DEFERRED_RENT [ ITERATION_NUMBER - 11 ] = ST_DEFERRED_RENT [ CV(ID) ] - ST_DEFERRED_RENT [ CV(ID) - 11] + BEG_ST_DEFERRED_RENT [ CV(ID) ])

      ) B
    ON (A.ID = B.ID and A.ILR_ID = B.ILR_ID and A.REVISION = B.REVISION and A.LS_ASSET_ID = B.LS_ASSET_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.MONTH = B.MONTH)
    when matched then update set
        A.BEG_DEFERRED_RENT = B.BEG_DEFERRED_RENT,
        A.DEFERRED_RENT = B.DEFERRED_RENT,
        A.END_DEFERRED_RENT = B.END_DEFERRED_RENT,
        A.BEG_ST_DEFERRED_RENT = B.BEG_ST_DEFERRED_RENT,
        A.END_ST_DEFERRED_RENT = B.END_ST_DEFERRED_RENT;
        
   end P_HANDLE_DEFERRED_RENT;

   --**************************************************************************
   --                            F_CALC_ASSET_SCHEDULE
   -- This function will Build the asset schedule out
   --**************************************************************************
   function F_CALC_ASSET_SCHEDULE return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Undo prior asset schedule calculations';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_ASSET_SCHEDULE_CALC_STG
       where (ILR_ID, REVISION, set_of_books_id) in (select A.ILR_ID, A.REVISION, a.set_of_books_id from ls_ilr_stg A);
      L_STATUS := 'Starting to build asset schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_SCHEDULE_CALC_STG S
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, month, AMOUNT, RESIDUAL_AMOUNT,
          PREPAY_SWITCH, PAYMENT_MONTH, MONTHS_TO_ACCRUE, RATE, NPV, BEG_CAPITAL_COST,
          END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL,
          INTEREST_PAID, PRINCIPAL_PAID, PENNY_ROUNDER, PENNY_PRIN_ROUNDER, PENNY_INT_ROUNDER,
          PENNY_END_ROUNDER, PRINCIPAL_ACCRUED, INTEREST_ACCRUED, PRIN_ROUND_ACCRUED,
          INT_ROUND_ACCRUED, TERM_PENALTY, BPO_PRICE, BEG_LT_OBLIGATION, END_LT_OBLIGATION,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, PAYMENT_TERM_TYPE_ID, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT,
          BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT)
		 WITH prelim AS
        (SELECT ilr_id,
                ls_asset_id,
                revision,
                set_of_books_id, 
                MONTH,
                row_number() OVER (PARTITION BY ilr_id, ls_asset_id, revision, set_of_books_id ORDER BY MONTH) AS ID,
                months_between(MAX(MONTH) OVER(PARTITION BY ilr_id, ls_asset_id, revision, set_of_books_id), MONTH) AS month_id
        FROM ls_ilr_asset_schedule_stg)
         select ID,
                ILR_ID,
                REVISION,
                LS_ASSET_ID,
                SET_OF_BOOKS_ID,
                month,
                AMT,
                RES_AMT,
                PREPAY_SWITCH,
                PAYMENT_MONTH,
                MONTHS_TO_ACCRUE,
                RATE,
                NPV,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                INT_ACCRUAL,
                PRIN_ACCRUAL,
                INT_PAID,
                PRIN_PAID,
                ADD_A_PENNY,
                ADD_A_PENNY_PRIN_ACCRUAL,
                ADD_A_PENNY_INT_ACCRUAL,
                ADD_A_PENNY_END,
                PRIN_ACCRUED,
                INT_ACCRUED,
                PRIN_ACCRUED_ROUND,
                INT_ACCRUED_ROUND,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                IS_OM,
                PAYMENT_TERM_TYPE_ID,
                BEG_DEFERRED_RENT,
                DEFERRED_RENT,
                END_DEFERRED_RENT,
                BEG_ST_DEFERRED_RENT,
                END_ST_DEFERRED_RENT
            FROM (SELECT ilr_id,
                        ls_asset_id,
                        revision,
                        set_of_books_id,
                        MONTH,
                        prelim.ID,
                        month_id,
                        beg_obligation, 
                        amount AS amt, 
                        residual_amount AS res_amt, 
                        prepay_switch, 
                        payment_month, 
                        months_to_accrue,
                        rate, 
                        beg_capital_cost, 
                        amount AS amount, 
                        interest_accrual AS int_accrual, 
                        interest_paid AS int_paid, 
                        principal_accrual AS prin_accrual,  
                        principal_paid AS prin_paid, 
                        end_capital_cost, 
                        end_obligation, 
                        npv, 
                        term_penalty, 
                        bpo_price, 
                        beg_lt_obligation, 
                        end_lt_obligation, 
                        contingent_accrual1, 
                        contingent_accrual2, 
                        contingent_accrual3, 
                        contingent_accrual4, 
                        contingent_accrual5, 
                        contingent_accrual6, 
                        contingent_accrual7, 
                        contingent_accrual8, 
                        contingent_accrual9, 
                        contingent_accrual10, 
                        executory_accrual1, 
                        executory_accrual2, 
                        executory_accrual3, 
                        executory_accrual4, 
                        executory_accrual5, 
                        executory_accrual6, 
                        executory_accrual7, 
                        executory_accrual8, 
                        executory_accrual9, 
                        executory_accrual10, 
                        contingent_paid1, 
                        contingent_paid2, 
                        contingent_paid3, 
                        contingent_paid4, 
                        contingent_paid5, 
                        contingent_paid6, 
                        contingent_paid7, 
                        contingent_paid8, 
                        contingent_paid9, 
                        contingent_paid10, 
                        executory_paid1, 
                        executory_paid2, 
                        executory_paid3, 
                        executory_paid4, 
                        executory_paid5, 
                        executory_paid6, 
                        executory_paid7, 
                        executory_paid8, 
                        executory_paid9, 
                        executory_paid10, 
                        is_om, 
                        payment_term_type_id, 
                        amount AS prin_fixed, 
                        beg_deferred_rent, 
                        deferred_rent, 
                        end_deferred_rent, 
                        beg_st_deferred_rent, 
                        end_st_deferred_rent
                 FROM ls_ilr_asset_schedule_stg
                 JOIN prelim using(ilr_id, ls_asset_id, revision, set_of_books_id, month))
           MODEL PARTITION BY(ilr_id, 
                              ls_asset_id, 
                              revision, 
                              set_of_books_id) 
           DIMENSION BY(ID) 
           MEASURES(beg_obligation, 
                    amt, 
                    res_amt, 
                    prepay_switch, 
                    payment_month, 
                    months_to_accrue, 
                    0 AS add_a_penny, 
                    0 AS add_a_penny_end, 
                    0 AS add_a_penny_prin_accrual, 
                    0 AS add_a_penny_int_accrual, 
                    rate, 
                    beg_capital_cost, 
                    amount, 
                    int_accrual, 
                    0 AS int_accrued, 
                    int_paid, 
                    prin_accrual, 
                    0 AS prin_accrued, 
                    prin_paid, 
                    end_capital_cost, 
                    end_obligation, 
                    MONTH, 
                    npv, 
                    0 AS prin_accrued_round, 
                    0 AS int_accrued_round, 
                    month_id, 
                    term_penalty, 
                    bpo_price, 
                    beg_lt_obligation, 
                    end_lt_obligation, 
                    contingent_accrual1, 
                    contingent_accrual2, 
                    contingent_accrual3, 
                    contingent_accrual4, 
                    contingent_accrual5, 
                    contingent_accrual6, 
                    contingent_accrual7, 
                    contingent_accrual8, 
                    contingent_accrual9, 
                    contingent_accrual10, 
                    executory_accrual1, 
                    executory_accrual2, 
                    executory_accrual3, 
                    executory_accrual4, 
                    executory_accrual5, 
                    executory_accrual6, 
                    executory_accrual7, 
                    executory_accrual8, 
                    executory_accrual9, 
                    executory_accrual10, 
                    contingent_paid1, 
                    contingent_paid2, 
                    contingent_paid3, 
                    contingent_paid4, 
                    contingent_paid5, 
                    contingent_paid6, 
                    contingent_paid7, 
                    contingent_paid8, 
                    contingent_paid9, 
                    contingent_paid10, 
                    executory_paid1, 
                    executory_paid2, 
                    executory_paid3, 
                    executory_paid4, 
                    executory_paid5, 
                    executory_paid6, 
                    executory_paid7, 
                    executory_paid8, 
                    executory_paid9, 
                    executory_paid10, 
                    is_om, 
                    payment_term_type_id, 
                    prin_fixed, 
                    beg_deferred_rent, 
                    deferred_rent, 
                    end_deferred_rent, 
                    beg_st_deferred_rent, 
                    end_st_deferred_rent
					)
         IGNORE NAV RULES UPSERT ITERATE(100000) UNTIL ITERATION_NUMBER = MONTH_ID [ 1 ]
         (
                --
                -- iteration_number + 1 = the current month processing.
                -- iteration_number = prior month
                -- iteration_number - 11 = 12 months prior
                --
                -- if prepaid, the first months amount goes into the period zero accrual
                 PAYMENT_MONTH [ 0 ] = 1,
             PRIN_ACCRUAL [ 0 ] = DECODE(PREPAY_SWITCH [ 1 ], 1, AMOUNT [ 1 ], 0),
             PRIN_ACCRUED [ 0 ] = DECODE(PREPAY_SWITCH [ 1 ], 1, AMOUNT [ 1 ], 0),
             INT_ACCRUAL [ 0 ] = 0,
             INT_ACCRUED [ 0 ] = 0,
             PRIN_ACCRUED_ROUND [ 0 ] = DECODE(PREPAY_SWITCH [ 1 ], 1, AMOUNT [ 1 ], 0),
             INT_ACCRUED_ROUND [ 0 ] = 0,

            -- IFF the current processing month is a paymeny month AND it is prepaid,
            -- then reset the accural amount for last month to be based on current months payment amount (minus last months interest)
            -- ELSE lease prin_accrual for last month alone
            PRIN_ACCRUAL[ ITERATION_NUMBER ] = DECODE(PAYMENT_MONTH [ CV(ID) + 1 ], 0, PRIN_ACCRUAL[ CV(ID) ],
                                          case when PAYMENT_TERM_TYPE_ID[ CV(ID) + 1] = 3 then
                                              PRIN_ACCRUAL[CV(ID)]
                                          else
                                              DECODE(PREPAY_SWITCH[CV(ID) + 1], 1, AMOUNT[ CV(ID) + 1 ] - INT_ACCRUAL[CV(ID)], PRIN_ACCRUAL[CV(ID)] )
                                          end
                                          ),

                -- set the rate to be based on my number of months accrued (to account for monthly interest accrued during periods
                 RATE [ ITERATION_NUMBER + 1 ] = (POWER(1 + RATE [ CV(ID) ], MONTHS_TO_ACCRUE [ CV(ID) ]) - 1) / MONTHS_TO_ACCRUE [ CV(ID) ],
                -- If this is the month after the payment, then accrued amount is equal to 0 if arrears or accrual amount of prior period if prepaid.
                --  All other months are equal to prior accrued amount + prior accrual
                INT_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - 1 ], 0, INT_ACCRUED [ CV(ID) - 1 ] + INT_ACCRUAL [ CV(ID) - 1 ],
                                             PREPAY_SWITCH [ CV(ID) ] * INT_ACCRUAL [ CV(ID) - 1 ]),
            PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - 1 ], 0, PRIN_ACCRUED [ CV(ID) - 1 ] + PRIN_ACCRUAL [ CV(ID) - 1 ],
                                             PREPAY_SWITCH [ CV(ID) ] * PRIN_ACCRUAL [ CV(ID) - 1 ]),
            INT_ACCRUED_ROUND [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - 1 ], 0,
                                                INT_ACCRUED_ROUND [ CV(ID) - 1 ] + ROUND(INT_ACCRUAL [ CV(ID) - 1 ], 2),
                                                PREPAY_SWITCH [ CV(ID) ] * ROUND(INT_ACCRUAL [ CV(ID) - 1 ], 2)),
            PRIN_ACCRUED_ROUND [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) - 1 ], 0,
                                                PRIN_ACCRUED_ROUND [ CV(ID) - 1 ] + ROUND(PRIN_ACCRUAL [ CV(ID) - 1 ], 2),
                                                PREPAY_SWITCH [ CV(ID) ] * ROUND(PRIN_ACCRUAL [ CV(ID) - 1 ], 2)),

                -- if prepaid, the end balance is used to determine the accrual
                 INT_ACCRUAL [ ITERATION_NUMBER + 1 ] =
               case when PAYMENT_TERM_TYPE_ID[CV(ID)] = 1 then
                  (last_day(PKG_LEASE_SCHEDULE.F_GET_II_BEGIN_DATE(CV(ILR_ID), CV(REVISION))) - PKG_LEASE_SCHEDULE.F_GET_II_BEGIN_DATE(CV(ILR_ID), CV(REVISION)))/
                   (last_day(PKG_LEASE_SCHEDULE.F_GET_II_BEGIN_DATE(CV(ILR_ID), CV(REVISION))) - last_day(add_months(PKG_LEASE_SCHEDULE.F_GET_II_BEGIN_DATE(CV(ILR_ID), CV(REVISION)), -1)))
                   * RATE [ CV(ID) ]
                   * (BEG_OBLIGATION [ CV(ID) ] - DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, PREPAY_SWITCH [ CV(ID) ] * PRIN_ACCRUED [ CV(ID) ]))
               else RATE [ CV(ID) ] *
                  (BEG_OBLIGATION [ CV(ID) ] - DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, PREPAY_SWITCH [ CV(ID) ] * PRIN_ACCRUED [ CV(ID) ]))
               end,
            AMOUNT[ ITERATION_NUMBER + 1] = case
               when PKG_LEASE_SCHEDULE.F_MAKE_II(CV(ILR_ID),CV(REVISION)) = 1 and PAYMENT_TERM_TYPE_ID[CV(ID)] = 1 then
                  INT_ACCRUAL[ CV(ID) ]
               else
                  AMOUNT[ CV(ID)]
               end,

                 PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = case
                  when PAYMENT_TERM_TYPE_ID[ CV(ID) ] = 3 then
                    PRIN_FIXED[ CV(ID) ]
                  else
               -- if this is a payment month... and it is prepaid... then use just negative interest.
               DECODE(PAYMENT_MONTH [CV(ID)], 0, AMOUNT [CV(ID)],
                     DECODE(PREPAY_SWITCH[CV(ID)], 1, 0, AMOUNT [CV(ID)]) )
               - INT_ACCRUAL [CV(ID)]
                  end,
                -- for prepaid, the accrual from the prior period is used for the payment amount
                -- for prin, the first period will include the full first payment amount
                 INT_PAID [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0,(1 - PREPAY_SWITCH [ CV(ID) ]) * INT_ACCRUAL [ CV(ID) ] + INT_ACCRUED [ CV(ID) ]), PRIN_PAID [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0,(1 - PREPAY_SWITCH [ CV(ID) ]) * PRIN_ACCRUAL [ CV(ID) ] + PRIN_ACCRUED [ CV(ID) ]),

                -- after setting the paid amount, set the accrued amounts to be zero
                 PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, PRIN_ACCRUED [ CV(ID) ], 0), INT_ACCRUED [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, INT_ACCRUED [ CV(ID) ], 0),

                -- TRUEUP rounding that may come into play around accrued balanced <> paid balances
                -- take the prior months accrued rounding.  And add the accrual (if arrears).  Else do not add anything
                 ADD_A_PENNY_PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, DECODE(CV(ID), 1, 0, ROUND(PRIN_PAID [ CV(ID) ], 2) - PRIN_ACCRUED_ROUND [ CV(ID) ] - (1 - PREPAY_SWITCH [ CV(ID) ]) * ROUND(PRIN_ACCRUAL [ CV(ID) ], 2))), ADD_A_PENNY_INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 0, 0, DECODE(CV(ID), 1, 0, ROUND(INT_PAID [ CV(ID) ], 2) - INT_ACCRUED_ROUND [ CV(ID) ] - (1 - PREPAY_SWITCH [ CV(ID) ]) * ROUND(INT_ACCRUAL [ CV(ID) ], 2))),

                -- trueup principal accrual for rounding between periods for arrears
                 PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUAL [ CV(ID) ] + ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY_PRIN_ACCRUAL [ CV(ID) ]), INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = INT_ACCRUAL [ CV(ID) ] + ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY_INT_ACCRUAL [ CV(ID) ]),

                -- prepaids update prior month accrual.  And current month accrued
                 PRIN_ACCRUAL [ ITERATION_NUMBER ] = PRIN_ACCRUAL [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_PRIN_ACCRUAL [ CV(ID) + 1 ]), PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUED [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_PRIN_ACCRUAL [ CV(ID) ]), INT_ACCRUAL [ ITERATION_NUMBER ] = INT_ACCRUAL [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_INT_ACCRUAL [ CV(ID) + 1 ]), INT_ACCRUED [ ITERATION_NUMBER + 1 ] = INT_ACCRUED [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_INT_ACCRUAL [ CV(ID) ]),

                -- end capital cost and obligation
                 END_CAPITAL_COST [ ITERATION_NUMBER + 1 ] = BEG_CAPITAL_COST [ CV(ID) ], END_OBLIGATION [ ITERATION_NUMBER + 1 ] = BEG_OBLIGATION [ CV(ID) ] - PRIN_PAID [ CV(ID) ],

                -- check if a penny needs to be added to principal and subtracted from interest
                -- if the last month.  Make sure end obligation is zero
                 ADD_A_PENNY [ ITERATION_NUMBER + 1 ] = ROUND(BEG_OBLIGATION [ CV(ID) ], 2) - ROUND(PRIN_PAID [ CV(ID) ], 2) - ROUND(END_OBLIGATION [ CV(ID) ], 2),

                -- apply from current month accrual if arrears
                 INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = INT_ACCRUAL [ CV(ID) ] - ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY [ CV(ID) ]), PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUAL [ CV(ID) ] + ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY [ CV(ID) ]),

                -- for prepaid, apply to prior period accrual and current month accrued
                 INT_ACCRUAL [ ITERATION_NUMBER ] = INT_ACCRUAL [ CV(ID) ] - (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY [ CV(ID) + 1 ]), PRIN_ACCRUAL [ ITERATION_NUMBER ] = PRIN_ACCRUAL [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY [ CV(ID) + 1 ]), INT_ACCRUED [ ITERATION_NUMBER + 1 ] = INT_ACCRUED [ CV(ID) ] - (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY [ CV(ID) ]), PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUED [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY [ CV(ID) ]),

                -- Apply to current period payment
                 INT_PAID [ ITERATION_NUMBER + 1 ] = INT_PAID [ CV(ID) ] - ADD_A_PENNY [ CV(ID) ], PRIN_PAID [ ITERATION_NUMBER + 1 ] = PRIN_PAID [ CV(ID) ] + ADD_A_PENNY [ CV(ID) ],

                -- in the month where end obligation should go to zero (payment_month = 2).
                -- MAKE sure this happens by applying rounding
                -- check beginning balance minus payment minus res - bpo - termination
                 ADD_A_PENNY_END [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, ROUND(BEG_OBLIGATION [ CV(ID) ], 2) - ROUND(PRIN_PAID [ CV(ID) ], 2) - RES_AMT [ CV(ID) ] - BPO_PRICE [ CV(ID) ] - TERM_PENALTY [ CV(ID) ], 0),

                -- if arrears, then odify accrual for current month
                 INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = INT_ACCRUAL [ CV(ID) ] - ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY_END [ CV(ID) ]), PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUAL [ CV(ID) ] + ((1 - PREPAY_SWITCH [ CV(ID) ]) * ADD_A_PENNY_END [ CV(ID) ]),

                -- for prepaid, apply to prior period accrual and current month accrued
                 INT_ACCRUAL [ ITERATION_NUMBER ] = INT_ACCRUAL [ CV(ID) ] - (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_END [ CV(ID) + 1 ]), PRIN_ACCRUAL [ ITERATION_NUMBER ] = PRIN_ACCRUAL [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_END [ CV(ID) + 1 ]), INT_ACCRUED [ ITERATION_NUMBER + 1 ] = INT_ACCRUED [ CV(ID) ] - (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_END [ CV(ID) ]), PRIN_ACCRUED [ ITERATION_NUMBER + 1 ] = PRIN_ACCRUED [ CV(ID) ] + (PREPAY_SWITCH [ CV(ID) ] * ADD_A_PENNY_END [ CV(ID) ]),

                -- Apply to current period payment
                 INT_PAID [ ITERATION_NUMBER + 1 ] = INT_PAID [ CV(ID) ] - ADD_A_PENNY_END [ CV(ID) ], PRIN_PAID [ ITERATION_NUMBER + 1 ] = PRIN_PAID [ CV(ID) ] + ADD_A_PENNY_END [ CV(ID) ],

                -- set end obligation to be residual + bpo + termination if last month.
                 END_OBLIGATION [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, RES_AMT [ CV(ID) ] + BPO_PRICE [ CV(ID) ] + TERM_PENALTY [ CV(ID) ], END_OBLIGATION [ CV(ID) ]),

                 BEG_OBLIGATION [ ITERATION_NUMBER + 2 ] = END_OBLIGATION [ CV(ID) - 1 ],

                -- if last payment month and it is a prepaid, then set the accrual to be the prepaid payment amount (stored in prin_accrued for period 0)
                 PRIN_ACCRUAL [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, DECODE(PREPAY_SWITCH [ CV(ID) ], 1, PRIN_ACCRUED [ 0 ], PRIN_ACCRUAL [ CV(ID) ]), PRIN_ACCRUAL [ CV(ID) ]), INT_ACCRUAL [ ITERATION_NUMBER + 1 ] = DECODE(PAYMENT_MONTH [ CV(ID) ], 2, DECODE(PREPAY_SWITCH [ CV(ID) ], 1, 0, INT_ACCRUAL [ CV(ID) ]), INT_ACCRUAL [ CV(ID) ]),

                -- end lt obligation is equal to the end obligation in 12 months
                 BEG_LT_OBLIGATION [ ITERATION_NUMBER + 1 ] = 0, END_LT_OBLIGATION [ ITERATION_NUMBER + 1 ] = 0, BEG_LT_OBLIGATION [ ITERATION_NUMBER - 11 ] = BEG_OBLIGATION [ ITERATION_NUMBER + 1 ], END_LT_OBLIGATION [ ITERATION_NUMBER - 11 ] = END_OBLIGATION [ ITERATION_NUMBER + 1 ],

                 DEFERRED_RENT [ 0 ] = 0, BEG_DEFERRED_RENT [ 0 ] = 0, END_DEFERRED_RENT [ 0 ] = 0, BEG_ST_DEFERRED_RENT [ 0 ] = 0, END_ST_DEFERRED_RENT [ 0 ] = 0,

                 BEG_ST_DEFERRED_RENT [ ITERATION_NUMBER + 1 ] = 0, END_ST_DEFERRED_RENT [ ITERATION_NUMBER + 1 ] = 0);

    L_STATUS:='HANDLE OM';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    P_HANDLE_OM;
    L_STATUS:='HANDLE DEFERRED RENT';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    P_HANDLE_DEFERRED_RENT;
    L_STATUS:='SPREAD PRINCIPAL ACCRUAL';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
	  P_SPREAD_PRIN_ACCRUAL;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_CALC_ASSET_SCHEDULE;

   --**************************************************************************
   --                            F_LOAD_ASSET_SCHEDULE
   -- This function will Build the asset schedule out
   --**************************************************************************
   function F_LOAD_ASSET_SCHEDULE(A_MONTH in date:=null)  return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Starting to load asset schedule';
	  L_MSG := 'OK';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      merge into LS_ILR_ASSET_SCHEDULE_STG S
      using (select * from LS_ILR_ASSET_SCHEDULE_CALC_STG) B
      on (B.ILR_ID = S.ILR_ID and B.LS_ASSET_ID = S.LS_ASSET_ID and B.REVISION = S.REVISION and B.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID and B.MONTH = S.MONTH)
      when matched then
         update
            set S.BEG_CAPITAL_COST = ROUND(B.BEG_CAPITAL_COST, 2),
                S.END_CAPITAL_COST = ROUND(B.END_CAPITAL_COST, 2),
                S.BEG_OBLIGATION = ROUND(B.BEG_OBLIGATION, 2),
                S.END_OBLIGATION = ROUND(B.END_OBLIGATION, 2),
                S.BEG_LT_OBLIGATION = ROUND(B.BEG_LT_OBLIGATION, 2),
                S.END_LT_OBLIGATION = ROUND(B.END_LT_OBLIGATION, 2),
                S.INTEREST_ACCRUAL = ROUND(B.INTEREST_ACCRUAL, 2),
                S.PRINCIPAL_ACCRUAL = ROUND(B.PRINCIPAL_ACCRUAL, 2),
                S.INTEREST_PAID = ROUND(B.INTEREST_PAID, 2),
                S.PRINCIPAL_PAID = ROUND(B.PRINCIPAL_PAID, 2),
                S.CONTINGENT_PAID1 = ROUND(B.CONTINGENT_PAID1, 2),
                S.CONTINGENT_PAID2 = ROUND(B.CONTINGENT_PAID2, 2),
                S.CONTINGENT_PAID3 = ROUND(B.CONTINGENT_PAID3, 2),
                S.CONTINGENT_PAID4 = ROUND(B.CONTINGENT_PAID4, 2),
                S.CONTINGENT_PAID5 = ROUND(B.CONTINGENT_PAID5, 2),
                S.CONTINGENT_PAID6 = ROUND(B.CONTINGENT_PAID6, 2),
                S.CONTINGENT_PAID7 = ROUND(B.CONTINGENT_PAID7, 2),
                S.CONTINGENT_PAID8 = ROUND(B.CONTINGENT_PAID8, 2),
                S.CONTINGENT_PAID9 = ROUND(B.CONTINGENT_PAID9, 2),
                S.CONTINGENT_PAID10 = ROUND(B.CONTINGENT_PAID10, 2),
                S.EXECUTORY_PAID1 = ROUND(B.EXECUTORY_PAID1, 2),
                S.EXECUTORY_PAID2 = ROUND(B.EXECUTORY_PAID2, 2),
                S.EXECUTORY_PAID3 = ROUND(B.EXECUTORY_PAID3, 2),
                S.EXECUTORY_PAID4 = ROUND(B.EXECUTORY_PAID4, 2),
                S.EXECUTORY_PAID5 = ROUND(B.EXECUTORY_PAID5, 2),
                S.EXECUTORY_PAID6 = ROUND(B.EXECUTORY_PAID6, 2),
                S.EXECUTORY_PAID7 = ROUND(B.EXECUTORY_PAID7, 2),
                S.EXECUTORY_PAID8 = ROUND(B.EXECUTORY_PAID8, 2),
                S.EXECUTORY_PAID9 = ROUND(B.EXECUTORY_PAID9, 2),
                S.EXECUTORY_PAID10 = ROUND(B.EXECUTORY_PAID10, 2),
                S.CONTINGENT_ACCRUAL1 = ROUND(B.CONTINGENT_ACCRUAL1, 2),
                S.CONTINGENT_ACCRUAL2 = ROUND(B.CONTINGENT_ACCRUAL2, 2),
                S.CONTINGENT_ACCRUAL3 = ROUND(B.CONTINGENT_ACCRUAL3, 2),
                S.CONTINGENT_ACCRUAL4 = ROUND(B.CONTINGENT_ACCRUAL4, 2),
                S.CONTINGENT_ACCRUAL5 = ROUND(B.CONTINGENT_ACCRUAL5, 2),
                S.CONTINGENT_ACCRUAL6 = ROUND(B.CONTINGENT_ACCRUAL6, 2),
                S.CONTINGENT_ACCRUAL7 = ROUND(B.CONTINGENT_ACCRUAL7, 2),
                S.CONTINGENT_ACCRUAL8 = ROUND(B.CONTINGENT_ACCRUAL8, 2),
                S.CONTINGENT_ACCRUAL9 = ROUND(B.CONTINGENT_ACCRUAL9, 2),
                S.CONTINGENT_ACCRUAL10 = ROUND(B.CONTINGENT_ACCRUAL10, 2),
                S.EXECUTORY_ACCRUAL1 = ROUND(B.EXECUTORY_ACCRUAL1, 2),
                S.EXECUTORY_ACCRUAL2 = ROUND(B.EXECUTORY_ACCRUAL2, 2),
                S.EXECUTORY_ACCRUAL3 = ROUND(B.EXECUTORY_ACCRUAL3, 2),
                S.EXECUTORY_ACCRUAL4 = ROUND(B.EXECUTORY_ACCRUAL4, 2),
                S.EXECUTORY_ACCRUAL5 = ROUND(B.EXECUTORY_ACCRUAL5, 2),
                S.EXECUTORY_ACCRUAL6 = ROUND(B.EXECUTORY_ACCRUAL6, 2),
                S.EXECUTORY_ACCRUAL7 = ROUND(B.EXECUTORY_ACCRUAL7, 2),
                S.EXECUTORY_ACCRUAL8 = ROUND(B.EXECUTORY_ACCRUAL8, 2),
                S.EXECUTORY_ACCRUAL9 = ROUND(B.EXECUTORY_ACCRUAL9, 2),
                S.EXECUTORY_ACCRUAL10 = ROUND(B.EXECUTORY_ACCRUAL10, 2),
                S.BEG_DEFERRED_RENT = ROUND(B.BEG_DEFERRED_RENT, 2),
                S.DEFERRED_RENT = ROUND(B.DEFERRED_RENT, 2),
                S.END_DEFERRED_RENT = ROUND(B.END_DEFERRED_RENT, 2),
                S.BEG_ST_DEFERRED_RENT = ROUND(B.BEG_ST_DEFERRED_RENT, 2),
                S.END_ST_DEFERRED_RENT = ROUND(B.END_ST_DEFERRED_RENT, 2);


	  -- Process Components prior to save
		L_MSG := F_LOAD_COMPONENTS(A_MONTH);
    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

		return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_LOAD_ASSET_SCHEDULE;

   --**************************************************************************
   --                            F_PROCESS_ASSETS
   -- This function will process the assets under the ILRS
   -- IT will allocate the payments to the assets
   -- then calculate NBV for the asset
   --**************************************************************************
   function F_PROCESS_ASSETS(A_MONTH in date:=null) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('PROCESSING Assets');

      L_STATUS := 'CALLING F_ALLOCATE_TO_ASSETS';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := F_ALLOCATE_TO_ASSETS;
      if L_MSG = 'OK' then
         L_STATUS := 'CALLING F_CALC_ASSET_SCHEDULE';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_ASSET_SCHEDULE;
         if L_MSG = 'OK' then

            L_STATUS := 'CALLING F_LOAD_ASSET_SCHEDULE';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            L_MSG := F_LOAD_ASSET_SCHEDULE(A_MONTH);

            if L_MSG <> 'OK' THEN
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              return L_MSG;
	          end if;

            -- Calculate VP here- all asset schedules built and in staging table
            -- so formulas requiring schedule fields will be able to calc
            /*L_STATUS := 'CALLING F_CALC_ILR_VAR_PAYMENTS';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            L_MSG := PKG_LEASE_VAR_PAYMENTS.F_CALC_ALL_VAR_PAYMENTS;
            if L_MSG <> 'OK' THEN
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              return L_MSG;
            end if;*/

            L_MSG := 'CALLING F_COPY_ASSET_SCHEDULE_ROWS';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
	          L_MSG:=F_COPY_ASSET_SCHEDULE_ROWS(A_MONTH);
	          if L_MSG<>'OK' THEN
		          PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              return L_MSG;
	          end if;

            L_MSG := 'CALLING F_COPY_ILR_SCHEDULE_ROWS';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
	          L_MSG:=F_COPY_ILR_SCHEDULE_ROWS(A_MONTH);
	          if L_MSG<>'OK' THEN
		          PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              return L_MSG;
	          end if;

            return L_MSG;
         end if;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ASSETS;

   --**************************************************************************
   --                            F_CALC_IRR
   -- This function will calculate the internal rate of return
   -- For ILRs where NPV > FMV (net present value is greater than fair market value)
   --**************************************************************************
   function F_CALC_IRR return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      L_STATUS := 'Starting IRR Calculation';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      merge into LS_ILR_STG S
      using (select ILR_ID, REVISION, SET_OF_BOOKS_ID, 1 / X - 1 as THE_IRR
               from (select *
                       from (select ILR_ID, REVISION, SET_OF_BOOKS_ID, month, sum(AMOUNT) as AMOUNT
                               from (select L.ILR_ID,
                                            S.REVISION,
                                            S.SET_OF_BOOKS_ID,
                                            S.MONTH,
                                            S.AMOUNT + S.RESIDUAL_AMOUNT + S.BPO_PRICE + S.TERM_PENALTY as AMOUNT
                                       from LS_ILR_SCHEDULE_STG S, LS_ILR_STG L
                                      where L.ILR_ID = S.ILR_ID
                                        and L.REVISION = S.REVISION
                                        and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                                        and L.NPV > L.FMV
                                        and L.IS_OM = 0
                                        and L.REVISION NOT IN (SELECT REVISION FROM LS_FORECAST_VERSION WHERE FORECAST_TYPE_ID = 2 UNION SELECT nvl(LOOKBACK_REVISION,0) FROM LS_FORECAST_VERSION WHERE FORECAST_TYPE_ID = 2)
                                     union all
                                     select L.ILR_ID,
                                            S.REVISION,
                                            S.SET_OF_BOOKS_ID,
                                            min(ADD_MONTHS(S.MONTH, L.PREPAY_SWITCH - 1)),
                                            -1 * L.FMV
                                       from LS_ILR_SCHEDULE_STG S, LS_ILR_STG L
                                      where L.ILR_ID = S.ILR_ID
                                        and L.REVISION = S.REVISION
                                        and L.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                                        and L.NPV > L.FMV
                                        and L.IS_OM = 0
                                        and L.REVISION NOT IN (SELECT REVISION FROM LS_FORECAST_VERSION WHERE FORECAST_TYPE_ID = 2 UNION SELECT nvl(LOOKBACK_REVISION,0) FROM LS_FORECAST_VERSION WHERE FORECAST_TYPE_ID = 2)
                                      group by L.ILR_ID, L.FMV, S.REVISION, S.SET_OF_BOOKS_ID)
                              group by ILR_ID, month, REVISION, SET_OF_BOOKS_ID
                              order by 1, 2) MODEL partition by(ILR_ID, REVISION, SET_OF_BOOKS_ID) DIMENSION by(ROW_NUMBER() OVER(partition by ILR_ID, REVISION, SET_OF_BOOKS_ID order by month) as THE_ROW) MEASURES(MONTHS_BETWEEN(month, FIRST_VALUE(month) OVER(partition by ILR_ID, REVISION, SET_OF_BOOKS_ID order by month)) as month, AMOUNT S, 0 SS, 0 F_A, 0 F_B, 0 F_X, 0 A, 1 B, 0 X, 0 ITER) RULES ITERATE(10000) UNTIL(ABS(F_X [ 1 ]) < POWER(10, -20))(SS [ any ] = S [ CV() ] * POWER(A [ 1 ], month [ CV() ]), F_A [ 1 ] = sum(SS) [ any ], SS [ any ] = S [ CV() ] * POWER(B [ 1 ], month [ CV() ]), F_B [ 1 ] = sum(SS) [ any ], X [ 1 ] = A [ 1 ] - F_A [ 1 ] * (B [ 1 ] - A [ 1 ]) / (F_B [ 1 ] - F_A [ 1 ]), SS [ any ] = S [ CV() ] * POWER(X [ 1 ], month [ CV() ]), F_X [ 1 ] = sum(SS) [ any ], A [ 1 ] = DECODE(SIGN(F_A [ 1 ] * F_X [ 1 ]), 1, X [ 1 ], A [ 1 ]), B [ 1 ] = DECODE(SIGN(F_A [ 1 ] * F_X [ 1 ]), 1, B [ 1 ], X [ 1 ]), ITER [ 1 ] = ITERATION_NUMBER + 1)) B
              where B.THE_ROW = 1
                and B.X <> 0) I
      on (I.ILR_ID = S.ILR_ID and I.REVISION = S.REVISION and I.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID)
      when matched then
         update set S.IRR = I.THE_IRR;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_CALC_IRR;

   --**************************************************************************
   --                            F_NET_PRESENT_VALUE
   -- Determine the NET PRESENT VALUE of the future cash flow
   -- Based on the rate (either the discount rate entered or the IRR)
   --**************************************************************************
   function F_NET_PRESENT_VALUE return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
	  type NPV_REC is record (
		ID number(22,0),
		ILR_ID LS_ILR_SCHEDULE_STG.ILR_ID%type,
		REVISION LS_ILR_SCHEDULE_STG.REVISION%type,
		SET_OF_BOOKS_ID LS_ILR_SCHEDULE_STG.SET_OF_BOOKS_ID%type,
		MONTH LS_ILR_SCHEDULE_STG.MONTH%type,
		NPV LS_ILR_SCHEDULE_STG.NPV%type,
		PAYMENT_TERM_TYPE_ID LS_ILR_SCHEDULE_STG.PAYMENT_TERM_TYPE_ID%type);
	  type NPV_TABLE is table of NPV_REC index by pls_integer;
	  L_NPV_REC_TABLE NPV_TABLE;


   begin
      L_STATUS := 'Starting NBV by payment';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);


		select ID, ILR_ID, REVISION, SET_OF_BOOKS_ID, month, NPV, PAYMENT_TERM_TYPE_ID
		bulk collect
		into L_NPV_REC_TABLE
		from LS_ILR_SCHEDULE_STG
		where PROCESS_NPV = 1 MODEL partition by(ILR_ID, REVISION, SET_OF_BOOKS_ID)
		DIMENSION by(ROW_NUMBER() OVER(partition by ILR_ID, REVISION, SET_OF_BOOKS_ID order by month) as ID)
		MEASURES(AMOUNT,
				   0 NPV,
				   RATE,
				   month,
				   PREPAY_SWITCH,
				   RESIDUAL_AMOUNT,
				   BPO_PRICE,
				   TERM_PENALTY,
				   PAYMENT_TERM_TYPE_ID)
		RULES(NPV [ any ] = case
					when PAYMENT_TERM_TYPE_ID[CV(ID)] = 3 then
					  (AMOUNT [ CV(ID) ] + RESIDUAL_AMOUNT [ CV(ID) ] + BPO_PRICE [
					  CV(ID) ] + TERM_PENALTY [ CV(ID) ])
					else
					  (AMOUNT [ CV(ID) ] + RESIDUAL_AMOUNT [ CV(ID) ] + BPO_PRICE [
					  CV(ID) ] + TERM_PENALTY [ CV(ID) ]) /
					  POWER(1 + RATE [ CV(ID) ], CV(ID) - PREPAY_SWITCH [ CV(ID) ])
					end
			 );

		forall i in indices of L_NPV_REC_TABLE
			update LS_ILR_SCHEDULE_STG LISS
			set LISS.NPV = L_NPV_REC_TABLE(i).NPV
			where LISS.ILR_ID = L_NPV_REC_TABLE(i).ILR_ID
			and LISS.MONTH = L_NPV_REC_TABLE(i).MONTH
			and LISS.REVISION = L_NPV_REC_TABLE(i).REVISION
			and LISS.SET_OF_BOOKS_ID = L_NPV_REC_TABLE(i).SET_OF_BOOKS_ID
			and LISS.PROCESS_NPV = 1;

      L_STATUS := 'Updating NBV for ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      update LS_ILR_STG S
         set NPV =
              (select ROUND(sum(B.NPV), 2)
                 from LS_ILR_SCHEDULE_STG B
                where B.ILR_ID = S.ILR_ID
                  and B.REVISION = S.REVISION
                  and B.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID)
       where PROCESS_NPV = 1;

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_NET_PRESENT_VALUE;

   --**************************************************************************
   --                            F_LOAD_ILR_STG
   -- Loads the ILR_STG table for a single ILR ID
   --**************************************************************************
   function F_LOAD_ILR_STG(A_ILR_ID   number,
                           A_REVISION number) return varchar2 is
      L_MSG    varchar2(2000);
      L_SQLS   varchar2(2000);
      L_IS_CAP number(1, 0);
		L_IS_CAP2 number(1, 0);
   begin
      for L_SOBS in (select L.ILR_ID as ILR_ID,
                            LO.REVISION as REVISION,
                            LL.PRE_PAYMENT_SW as PREPAY_SWITCH,
                            LO.INCEPTION_AIR / (100 * (nvl(LL.DAYS_IN_YEAR, 360)/30 )) as DISCOUNT_RATE, /* WMD */
                            sum(NVL(LA.FMV, 0)) as FMV,
                            sum(
                            case when la.estimated_residual = 0 and lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Amort to Guaranteed Residual', la.COMPANY_ID))) = 'yes' then
                              NVL(LA.GUARANTEED_RESIDUAL_AMOUNT, 0)
                            else
                              /* CJS 3/7/17 Add system control to account for capitalization of estimated residual */
                              Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', la.COMPANY_ID))), 'no', 0, round(la.ESTIMATED_RESIDUAL * la.FMV, 2))
                            end
                            ) as RESIDUAL_AMOUNT,
                            NVL(LO.PURCHASE_OPTION_AMT, 0) as BPO_PRICE,
                            NVL(LO.TERMINATION_AMT, 0) as TERM_PENALTY,
                            C.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                            LLCT.BOOK_SUMMARY_ID as BOOK_SUMMARY_ID, L.LEASE_ID
                       from LS_ILR               L,
                            LS_LEASE             LL,
                            LS_ASSET             LA,
                            LS_ILR_OPTIONS       LO,
                            COMPANY_SET_OF_BOOKS C,
                            LS_LEASE_CAP_TYPE    LLCT,
                            LS_ILR_ASSET_MAP M
                      where L.LEASE_ID = LL.LEASE_ID
                        and M.LS_ASSET_ID = LA.LS_ASSET_ID
                        and M.ILR_ID = A_ILR_ID
                        and M.REVISION = A_REVISION
                        and L.ILR_ID = LA.ILR_ID(+)
                        and LA.LS_ASSET_STATUS_ID <> 4
                        and L.ILR_ID = A_ILR_ID
                        and LO.ILR_ID = L.ILR_ID
                        and LO.REVISION = A_REVISION
                        and L.COMPANY_ID = C.COMPANY_ID
                        and LO.LEASE_CAP_TYPE_ID = LLCT.LS_LEASE_CAP_TYPE_ID
						and (A_REVISION > 0 or
							 c.set_of_books_id in (select decode(set_of_books_id, -1, c.set_of_books_id, set_of_books_id) from ls_forecast_version where revision = A_REVISION))
                      group by L.ILR_ID,
                               LL.PRE_PAYMENT_SW,
                               LO.INCEPTION_AIR / (100 * (nvl(LL.DAYS_IN_YEAR, 360)/30 )), /* CJS */
                               LO.PURCHASE_OPTION_AMT,
                               LO.TERMINATION_AMT,
                               LO.REVISION,
                               C.SET_OF_BOOKS_ID,
                               LLCT.BOOK_SUMMARY_ID, L.LEASE_ID)
      loop
         L_MSG := 'PROCESSING set_of_books: ' || L_SOBS.SET_OF_BOOKS_ID;

         L_SQLS   := 'select basis_' || TO_CHAR(L_SOBS.BOOK_SUMMARY_ID) ||
                     '_indicator from set_of_books s where s.set_of_books_id = ' ||
                     TO_CHAR(L_SOBS.SET_OF_BOOKS_ID);
         L_IS_CAP := TO_NUMBER(PP_MISC_PKG.DYNAMIC_SELECT(L_SQLS));

		 select decode(l.lease_type_id, 3, 1, L_IS_CAP)
		 into L_IS_CAP2
		 from ls_lease l
		 where l.lease_id = L_SOBS.LEASE_ID
		 ;

         insert into LS_ILR_STG
            (ILR_ID, REVISION, PREPAY_SWITCH, DISCOUNT_RATE, FMV, RESIDUAL_AMOUNT, PROCESS_NPV,
             BPO_PRICE, TERM_PENALTY, SET_OF_BOOKS_ID, IS_OM)
         values
            (L_SOBS.ILR_ID, L_SOBS.REVISION, L_SOBS.PREPAY_SWITCH, L_IS_CAP2 * nvl(L_SOBS.DISCOUNT_RATE,0),
             L_SOBS.FMV, L_SOBS.RESIDUAL_AMOUNT, 1, L_SOBS.BPO_PRICE, L_SOBS.TERM_PENALTY,
             L_SOBS.SET_OF_BOOKS_ID, 1 - L_IS_CAP);
      end loop;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR('ERROR loading ls_ilr_stg: ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_LOAD_ILR_STG;


   --**************************************************************************
   --                            F_LOAD_ILRS_STG
   -- Loads the ILR_STG table for a single LEASE ID
   --**************************************************************************
   function F_LOAD_ILRS_STG(A_LEASE_ID number) return varchar2 is
      L_MSG    varchar2(2000);
      L_SQLS   varchar2(2000);
      L_IS_CAP number(1, 0);

   begin
      for L_SOBS in (select L.ILR_ID as ILR_ID,
                            LO.REVISION as REVISION,
                            LL.PRE_PAYMENT_SW as PREPAY_SWITCH,
                            LO.INCEPTION_AIR / (100 * (nvl(LL.DAYS_IN_YEAR, 360)/30 )) as DISCOUNT_RATE, /* WMD */
                            sum(NVL(LA.FMV, 0)) as FMV,
                            sum(
                            case when la.estimated_residual = 0 and lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Amort to Guaranteed Residual', la.COMPANY_ID))) = 'yes' then
                              NVL(LA.GUARANTEED_RESIDUAL_AMOUNT, 0)
                            ELSE
                              /* CJS 3/7/17 Add system control to account for capitalization of estimated residual */
                              Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', la.COMPANY_ID))), 'no', 0, round(la.ESTIMATED_RESIDUAL * la.FMV, 2))
                            end
                            ) as RESIDUAL_AMOUNT,
                            NVL(LO.PURCHASE_OPTION_AMT, 0) as BPO_PRICE,
                            NVL(LO.TERMINATION_AMT, 0) as TERM_PENALTY,
                            C.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                            LLCT.BOOK_SUMMARY_ID as BOOK_SUMMARY_ID
                       from LS_ILR               L,
                            LS_LEASE             LL,
                            LS_ASSET             LA,
                            LS_ILR_OPTIONS       LO,
                            COMPANY_SET_OF_BOOKS C,
                            LS_LEASE_CAP_TYPE    LLCT
                      where L.LEASE_ID = LL.LEASE_ID
                        and L.ILR_ID = LA.ILR_ID(+)
                        and LA.LS_ASSET_STATUS_ID <> 4
                        and LL.LEASE_ID = A_LEASE_ID
                        and LO.ILR_ID = L.ILR_ID
                        and LO.REVISION = L.CURRENT_REVISION
                        and L.COMPANY_ID = C.COMPANY_ID
                        and LO.LEASE_CAP_TYPE_ID = LLCT.LS_LEASE_CAP_TYPE_ID
                      group by L.ILR_ID,
                               LL.PRE_PAYMENT_SW,
                               LO.INCEPTION_AIR / (100 * (nvl(LL.DAYS_IN_YEAR, 360)/30 )), /* CJS */
                               LO.PURCHASE_OPTION_AMT,
                               LO.TERMINATION_AMT,
                               LO.REVISION,
                               C.SET_OF_BOOKS_ID,
                               LLCT.BOOK_SUMMARY_ID)
      loop
         L_SQLS   := 'select basis_' || TO_CHAR(L_SOBS.BOOK_SUMMARY_ID) ||
                     '_indicator from set_of_books s where s.set_of_books_id = ' ||
                     TO_CHAR(L_SOBS.SET_OF_BOOKS_ID);
         L_IS_CAP := TO_NUMBER(PP_MISC_PKG.DYNAMIC_SELECT(L_SQLS));

         insert into LS_ILR_STG
            (ILR_ID, REVISION, PREPAY_SWITCH, DISCOUNT_RATE, FMV, RESIDUAL_AMOUNT, PROCESS_NPV,
             BPO_PRICE, TERM_PENALTY, SET_OF_BOOKS_ID, IS_OM)
         values
            (L_SOBS.ILR_ID, L_SOBS.REVISION, L_SOBS.PREPAY_SWITCH, L_IS_CAP * L_SOBS.DISCOUNT_RATE,
             L_SOBS.FMV, L_SOBS.RESIDUAL_AMOUNT, 1, L_SOBS.BPO_PRICE, L_SOBS.TERM_PENALTY,
             L_SOBS.SET_OF_BOOKS_ID, 1 - L_IS_CAP);
      end loop;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR('ERROR loading ls_ilr_stg: ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_LOAD_ILRS_STG;

   --**************************************************************************
   --                            F_LOAD_ILR_SCHEDULE_STG
   -- Builds out the cash flow based on payment terms
   -- Take into consideration whether or not the payment is
   -- Paid at the beginning of the period or at the end of the period
   --**************************************************************************
   function F_LOAD_ILR_SCHEDULE_STG(A_MONTH in date:=null) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
      L_RTN    varchar2(35);

   begin
      L_STATUS := 'Loading ls_ilr_schedule_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_SCHEDULE_STG
         (ID, ILR_ID, REVISION, SET_OF_BOOKS_ID, month, AMOUNT, RESIDUAL_AMOUNT, RATE,
          PREPAY_SWITCH, PROCESS_NPV, PAYMENT_MONTH, MONTHS_TO_ACCRUE, BPO_PRICE, TERM_PENALTY,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, PAYMENT_TERM_TYPE_ID)
         with N as
          (
           -- create a "table" with 10000 rows
           select ROWNUM as THE_ROW from DUAL connect by level <= 10000),
         LP as
          (
           -- find the payment_term_id with the latest payment_term_date
           select L2.ILR_ID,
                   L2.PAYMENT_TERM_ID,
                   L2.REVISION,
                   L1.SET_OF_BOOKS_ID,
                   ROW_NUMBER() OVER(partition by L2.ILR_ID, L1.SET_OF_BOOKS_ID, L2.REVISION order by L2.PAYMENT_TERM_DATE desc) as THE_MAX
             from LS_ILR_PAYMENT_TERM_STG L2, LS_ILR_STG L1
            where L1.ILR_ID = L2.ILR_ID
              and L1.REVISION = L2.REVISION)
         select ROW_NUMBER() OVER(partition by P.ILR_ID, L.REVISION, L.SET_OF_BOOKS_ID order by ADD_MONTHS(P.PAYMENT_TERM_DATE, N.THE_ROW - 1)),
                P.ILR_ID,
                L.REVISION,
                L.SET_OF_BOOKS_ID,
                ADD_MONTHS(trunc( P.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1),
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    P.PAID_AMOUNT
                   else
                    0
                end as PAID_AMOUNT,
                case
                   when N.THE_ROW =
                        P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) and
                        P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                    L.RESIDUAL_AMOUNT
                   else
                    0
                end as RESIDUAL_AMOUNT,
                L.DISCOUNT_RATE,
                L.PREPAY_SWITCH,
                1,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    1
                   else
                    0
                end as PAYMENT_MONTH,
                DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) as MONTHS_TO_ACCRUE,
                case
                   when N.THE_ROW =
                        P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) and
                        P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                    L.BPO_PRICE
                   else
                    0
                end as BPO_PRICE,
                case
                   when N.THE_ROW =
                        P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1) and
                        P.PAYMENT_TERM_ID = LP.PAYMENT_TERM_ID then
                    L.TERM_PENALTY
                   else
                    0
                end as TERM_PENALTY,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_1, 0)
                   else
                    0
                end as CONTINGENT_PAID1,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_2, 0)
                   else
                    0
                end as CONTINGENT_PAID2,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_3, 0)
                   else
                    0
                end as CONTINGENT_PAID3,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_4, 0)
                   else
                    0
                end as CONTINGENT_PAID4,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_5, 0)
                   else
                    0
                end as CONTINGENT_PAID5,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_6, 0)
                   else
                    0
                end as CONTINGENT_PAID6,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_7, 0)
                   else
                    0
                end as CONTINGENT_PAID7,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_8, 0)
                   else
                    0
                end as CONTINGENT_PAID8,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_9, 0)
                   else
                    0
                end as CONTINGENT_PAID9,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.C_BUCKET_10, 0)
                   else
                    0
                end as CONTINGENT_PAID10,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_1, 0)
                   else
                    0
                end as EXECUTORY_PAID1,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_2, 0)
                   else
                    0
                end as EXECUTORY_PAID2,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_3, 0)
                   else
                    0
                end as EXECUTORY_PAID3,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_4, 0)
                   else
                    0
                end as EXECUTORY_PAID4,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_5, 0)
                   else
                    0
                end as EXECUTORY_PAID5,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_6, 0)
                   else
                    0
                end as EXECUTORY_PAID6,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_7, 0)
                   else
                    0
                end as EXECUTORY_PAID7,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_8, 0)
                   else
                    0
                end as EXECUTORY_PAID8,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_9, 0)
                   else
                    0
                end as EXECUTORY_PAID9,
                case
                   when mod(N.THE_ROW, DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(P.PAYMENT_FREQ_ID, 4, 0, L.PREPAY_SWITCH) then
                    NVL(P.E_BUCKET_10, 0)
                   else
                    0
                end as EXECUTORY_PAID10,
                L.IS_OM,
                P.PAYMENT_TERM_TYPE_ID
           from LS_ILR_PAYMENT_TERM_STG P, LS_ILR_STG L, LP, N
          where P.ILR_ID = L.ILR_ID
            and P.REVISION = L.REVISION
            and LP.ILR_ID = L.ILR_ID
            and LP.REVISION = L.REVISION
            and LP.THE_MAX = 1
            and LP.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
            and N.THE_ROW <= P.NUMBER_OF_TERMS * DECODE(P.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)
            AND ADD_MONTHS(TRUNC(P.PAYMENT_TERM_DATE,'MM'),THE_ROW-1) >= nvl(L.NPV_START_DATE, to_date(0001,'yyyy'))
          order by 2;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_LOAD_ILR_SCHEDULE_STG;

   --**************************************************************************
   --                            F_CALC_SCHEDULES
   --**************************************************************************
   function F_CALC_SCHEDULES return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      --call the function to load the ILR payment term staging table
      L_STATUS := 'Calling F_LOAD_ILR_PAYMENT_TERM_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := PKG_LEASE_VAR_PAYMENTS.F_LOAD_ILR_PAYMENT_TERM_STG;
      IF L_MSG <> 'OK' then
        PKG_PP_LOG.P_WRITE_MESSAGE('LS_ILR_PAYMENT_TERM_STG not loaded correctly: ' || L_MSG);
        return L_MSG;
      END IF;

      --call the function to calculate any additions to the initial measure for
      L_STATUS := 'CALLING F_CALC_INITIAL_AMT';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := PKG_LEASE_VAR_PAYMENTS.F_CALC_INITIAL_AMT;
      IF L_MSG <> 'OK' then
        PKG_PP_LOG.P_WRITE_MESSAGE('Initial measure not calculated correctly: ' || L_MSG);

        --if the initial measure amount calc failed, clear the staging tables
        L_STATUS := F_CLEAR_STAGING_TABLES;
        if L_STATUS <> 'OK' then
          PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
        end if;

        return L_MSG;
      END IF;

      --proceed with the calc with updated initial measure amounts
      L_STATUS := 'CALLING F_LOAD_ILR_SCHEDULE_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := F_LOAD_ILR_SCHEDULE_STG;
      if L_MSG = 'OK' then
         -- Determine the Net Present Value based on the
         -- entered discount rate and the cash flow
         L_STATUS := 'CALLING F_NET_PRESENT_VALUE';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_NET_PRESENT_VALUE;
         if L_MSG = 'OK' then
            -- Call the function to calculate internal rate of return
            -- the function will only update ILRs where NPV > FMV
            L_STATUS := 'CALLING F_CALC_IRR';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            L_MSG := F_CALC_IRR;
            if L_MSG = 'OK' then
               L_STATUS := 'DO NOT REPROCESS NPV FOR SOME ILRs';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
               P_CHECK_RECALC_NPV;

               L_STATUS := 'CALLING F_NET_PRESENT_VALUE AFTER IRR';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
               L_MSG := F_NET_PRESENT_VALUE;
               if L_MSG = 'OK' then
			      L_STATUS := 'CALLING F_LOAD_DEFERRED_RENT';
				  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
				  L_MSG := F_LOAD_DEFERRED_RENT(FALSE);
                  return L_MSG;
               end if;
            end if;
         end if;
      end if;

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         return L_STATUS;
   end F_CALC_SCHEDULES;

   --**************************************************************************
   --                            F_PROCESS_ILR
   --**************************************************************************
   function F_PROCESS_ILR(A_ILR_ID   number,
                          A_REVISION number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING ilr_id: ' || TO_CHAR(A_ILR_ID));
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING revision: ' || TO_CHAR(A_REVISION));

      L_STATUS := 'CALLING F_LOAD_ILR_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := F_LOAD_ILR_STG(A_ILR_ID, A_REVISION);
      if L_MSG = 'OK' then
         L_STATUS := 'CALLING F_CALC_SCHEDULES';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_SCHEDULES;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ILR;

   --**************************************************************************
   --                            F_PROCESS_ILRS
   --**************************************************************************
   function F_PROCESS_ILRS(A_LEASE_ID number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING lease_id: ' || TO_CHAR(A_LEASE_ID));

      L_STATUS := 'CALLING F_LOAD_ILRS_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_MSG := F_LOAD_ILRS_STG(A_LEASE_ID);
      if L_MSG = 'OK' then
         L_STATUS := 'CALLING F_CALC_SCHEDULES';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_SCHEDULES;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ILRS;

   --**************************************************************************
   --                            F_MASS_PROCESS_ILRS
   --**************************************************************************
   function F_MASS_PROCESS_ILRS(A_ILR_IDS   PKG_LEASE_IMPORT.NUM_ARRAY,
                                A_REVISIONS PKG_LEASE_IMPORT.NUM_ARRAY) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
      L_IRR NUMBER(22,4);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());

      for I in 1 .. A_ILR_IDS.COUNT
      loop
			  L_MSG := pkg_lease_schedule.f_process_ilr(A_ILR_IDS(I), 1);

			  IF L_MSG <> 'OK' THEN
				PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
				PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				ROLLBACK;
				RETURN L_MSG;
			  END IF;

			  L_STATUS := 'Calling f_process_assets';
			  L_MSG:=pkg_lease_schedule.f_process_assets;

			  IF L_MSG <> 'OK' THEN
				PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
				PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				ROLLBACK;
				RETURN L_MSG;
			  END IF;
				
			  L_STATUS := 'Calling F_CALC_ALL_VAR_PAYMENTS';
			  L_MSG:=PKG_LEASE_VAR_PAYMENTS.F_CALC_ALL_VAR_PAYMENTS(null);

				select IRR into L_IRR
				  from LS_ILR_STG
				where ILR_ID = A_ILR_IDS(i)
				  and REVISION = A_REVISIONS(i)
				  and SET_OF_BOOKS_ID = 1;

				if nvl(100 * (POWER((1 + L_IRR), 12) - 1), 0) > 200 then
				  RAISE_APPLICATION_ERROR(-20000, 'Invalid internal rate of return. The given FMV and NPV would cause an internal rate of return over 200%.');
				  rollback;
				  RETURN L_MSG;
				end if;


			  L_STATUS := 'Calling f_save_schedules';
			  L_MSG := pkg_lease_schedule.f_save_schedules;

			  IF L_MSG <> 'OK' THEN
				PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
				PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				ROLLBACK;
				RETURN L_MSG;
			  END IF;

			  -- Clear staging tables

			  L_STATUS := 'Calling f_clear_staging_tables';
			  L_MSG := pkg_lease_schedule.F_CLEAR_STAGING_TABLES;

			  IF L_MSG <> 'OK' THEN
				PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
				PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				ROLLBACK;
				RETURN L_MSG;
			  END IF;
      end loop;

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_MASS_PROCESS_ILRS;

   --**************************************************************************
   --                            F_PROCESS_ILRS_FCST
   --**************************************************************************
   function F_PROCESS_ILRS_FCST(A_CAP_TYPE number) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('STARTING ILR FORECAST');
	  
	  pkg_pp_log.p_write_message('Clearing 9999');

      -- Delete anything related to revision 9999's
      delete from LS_ASSET_SCHEDULE where REVISION = 9999;
      delete from LS_ILR_SCHEDULE where REVISION = 9999;
      delete from LS_ILR_PAYMENT_TERM where REVISION = 9999;
      delete from LS_ILR_ASSET_MAP where REVISION = 9999;
      delete from LS_ILR_OPTIONS where REVISION = 9999;
      delete from LS_ILR_APPROVAL where REVISION = 9999;

      -- Create revision 10,000's for all In-Service ILR's
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select ILR_ID, 9999, 4, 1
           from LS_ILR_APPROVAL
          where ILR_ID in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
            and REVISION =
                (select CURRENT_REVISION from LS_ILR where ILR_ID = LS_ILR_APPROVAL.ILR_ID);

      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT, PAYMENT_SHIFT)
         select ILR_ID,
                9999,
                PURCHASE_OPTION_TYPE_ID,
                PURCHASE_OPTION_AMT,
                RENEWAL_OPTION_TYPE_ID,
                CANCELABLE_TYPE_ID,
                ITC_SW,
                PARTIAL_RETIRE_SW,
                SUBLET_SW,
                MUNI_BO_SW,
                INCEPTION_AIR,
                A_CAP_TYPE,
                TERMINATION_AMT,
                PAYMENT_SHIFT
           from LS_ILR_OPTIONS
          where ILR_ID in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
            and REVISION =
                (select CURRENT_REVISION from LS_ILR where ILR_ID = LS_ILR_OPTIONS.ILR_ID);

      insert into LS_ILR_ASSET_MAP
         (ILR_ID, LS_ASSET_ID, REVISION)
         select ILR_ID, LS_ASSET_ID, 9999
           from LS_ILR_ASSET_MAP
          where ILR_ID in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
            and REVISION =
                (select CURRENT_REVISION from LS_ILR where ILR_ID = LS_ILR_ASSET_MAP.ILR_ID);

      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE, PAYMENT_FREQ_ID,
          NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT, CURRENCY_TYPE_ID,
          C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6, C_BUCKET_7,
          C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3, E_BUCKET_4,
          E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10, REVISION)
         select ILR_ID,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10,
                9999
           from LS_ILR_PAYMENT_TERM
          where ILR_ID in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
            and REVISION =
                (select CURRENT_REVISION from LS_ILR where ILR_ID = LS_ILR_PAYMENT_TERM.ILR_ID);

      -- Build schedules
      L_STATUS := 'CALLING F_LOAD_ILR_STG';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      for ILR in (select ILR_ID from LS_ILR where ILR_STATUS_ID = 2)
      loop
         L_MSG := F_LOAD_ILR_STG(ILR.ILR_ID, 9999);
         exit when L_MSG <> 'OK';
      end loop;
      if L_MSG = 'OK' then
         L_STATUS := 'CALLING F_CALC_SCHEDULES';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_MSG := F_CALC_SCHEDULES;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

        return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return L_MSG;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ILRS_FCST;

   --**************************************************************************
   --                            F_SAVE_SCHEDULES
   -- This function saves the scehdules frm the calc table to the stg tables
   --**************************************************************************
   function F_SAVE_SCHEDULES(A_MONTH in date:=null) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
      L_LEASE_TYPE number;
      L_COMPANY_ID number;
      L_FLOAT_RATE number;
	  L_RTN varchar2(2000);
	  L_MONTH date;
	  l_count number(10, 0);
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('SAVING SCHEDULE');

      L_STATUS := 'DELETING ls_ilr_schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_SCHEDULE
       where (ILR_ID, REVISION) in (select A.ILR_ID, A.REVISION from LS_ILR_ASSET_SCHEDULE_STG A)
        and month >= nvl(A_MONTH, TO_DATE(180001,'YYYYMM'));
      PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows deleted from LS_ILR_SCHEDULE');

      L_STATUS := 'LOADING ls_ilr_schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_SCHEDULE
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT)
         select ILR_ID,
                REVISION,
                SET_OF_BOOKS_ID,
                month,
                sum(RESIDUAL_AMOUNT),
                sum(TERM_PENALTY),
                sum(BPO_PRICE),
                sum(BEG_CAPITAL_COST),
                sum(END_CAPITAL_COST),
                sum(BEG_OBLIGATION),
                sum(END_OBLIGATION),
                sum(BEG_LT_OBLIGATION),
                sum(END_LT_OBLIGATION),
                sum(INTEREST_ACCRUAL),
                sum(PRINCIPAL_ACCRUAL),
                sum(INTEREST_PAID),
                sum(PRINCIPAL_PAID),
                sum(CONTINGENT_ACCRUAL1),
                sum(CONTINGENT_ACCRUAL2),
                sum(CONTINGENT_ACCRUAL3),
                sum(CONTINGENT_ACCRUAL4),
                sum(CONTINGENT_ACCRUAL5),
                sum(CONTINGENT_ACCRUAL6),
                sum(CONTINGENT_ACCRUAL7),
                sum(CONTINGENT_ACCRUAL8),
                sum(CONTINGENT_ACCRUAL9),
                sum(CONTINGENT_ACCRUAL10),
                sum(EXECUTORY_ACCRUAL1),
                sum(EXECUTORY_ACCRUAL2),
                sum(EXECUTORY_ACCRUAL3),
                sum(EXECUTORY_ACCRUAL4),
                sum(EXECUTORY_ACCRUAL5),
                sum(EXECUTORY_ACCRUAL6),
                sum(EXECUTORY_ACCRUAL7),
                sum(EXECUTORY_ACCRUAL8),
                sum(EXECUTORY_ACCRUAL9),
                sum(EXECUTORY_ACCRUAL10),
                sum(CONTINGENT_PAID1),
                sum(CONTINGENT_PAID2),
                sum(CONTINGENT_PAID3),
                sum(CONTINGENT_PAID4),
                sum(CONTINGENT_PAID5),
                sum(CONTINGENT_PAID6),
                sum(CONTINGENT_PAID7),
                sum(CONTINGENT_PAID8),
                sum(CONTINGENT_PAID9),
                sum(CONTINGENT_PAID10),
                sum(EXECUTORY_PAID1),
                sum(EXECUTORY_PAID2),
                sum(EXECUTORY_PAID3),
                sum(EXECUTORY_PAID4),
                sum(EXECUTORY_PAID5),
                sum(EXECUTORY_PAID6),
                sum(EXECUTORY_PAID7),
                sum(EXECUTORY_PAID8),
                sum(EXECUTORY_PAID9),
                sum(EXECUTORY_PAID10),
                IS_OM,
                SUM(BEG_DEFERRED_RENT),
                SUM(DEFERRED_RENT),
                SUM(END_DEFERRED_RENT),
                SUM(BEG_ST_DEFERRED_RENT),
                SUM(END_ST_DEFERRED_RENT)
           from LS_ILR_ASSET_SCHEDULE_STG
		  where month > = nvl(A_MONTH, TO_DATE(180001,'YYYYMM'))
          group by ILR_ID, REVISION, month, SET_OF_BOOKS_ID, IS_OM;
        PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows inserted into LS_ILR_SCHEDULE');

		  L_STATUS:='Setting ILR beginning capital to 0';
		  update ls_ilr_schedule z
          set beg_capital_cost = 0
          where (ilr_id, revision, set_of_books_id) in (
		     select ilr_id, revision, set_of_books_id
			 from ls_ilr_asset_schedule_stg )
		  and exists
          (select 1 from
          (select ilr_id, revision, set_of_books_id, month, row_number() over(partition by ilr_id, revision, set_of_books_id order by month) the_row
          from ls_ilr_schedule ilrs
          where (ilr_id, revision, set_of_books_id) in
            (select ilr_id, revision, set_of_books_id
             from ls_ilr_asset_schedule_stg))
          where the_row = 1
		  and ilr_id = z.ilr_id
		  and revision = z.revision
		  and set_of_books_id = z.set_of_books_id
		  and month = z.month);
       PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows updated on LS_ILR_SCHEDULE');


      L_STATUS := 'DELETING ls_asset_schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ASSET_SCHEDULE
       where (LS_ASSET_ID, REVISION, set_of_books_id) in
             (select A.LS_ASSET_ID, A.REVISION, a.set_of_books_id from LS_ILR_ASSET_SCHEDULE_STG A)
             and month >= nvl(A_MONTH, TO_DATE(180001,'YYYYMM'));
      PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows deleted from LS_ASSET_SCHEDULE');

      L_STATUS := 'LOADING ls_asset_schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ASSET_SCHEDULE
         (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT)
         select LS_ASSET_ID,
                REVISION,
                SET_OF_BOOKS_ID,
                month,
                RESIDUAL_AMOUNT,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                INTEREST_ACCRUAL,
                PRINCIPAL_ACCRUAL,
                INTEREST_PAID,
                PRINCIPAL_PAID,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                IS_OM,
                BEG_DEFERRED_RENT,
                DEFERRED_RENT,
                END_DEFERRED_RENT,
                BEG_ST_DEFERRED_RENT,
                END_ST_DEFERRED_RENT
           from LS_ILR_ASSET_SCHEDULE_STG
           where month >= nvl(A_MONTH, TO_DATE(180001,'YYYYMM'));
      PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows inserted into from LS_ASSET_SCHEDULE');

      /* WMD */
      L_STATUS:='Setting Asset beginning capital to 0';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		  update ls_asset_schedule z
          set beg_capital_cost = 0
          where (ls_asset_id, revision, set_of_books_id) in (
		     select ls_asset_id, revision, set_of_books_id
			 from ls_ilr_asset_schedule_stg )
		  and exists
          (select 1 from
          (select ls_asset_id, revision, set_of_books_id, month, row_number() over(partition by ls_asset_id, revision, set_of_books_id order by month) the_row
          from ls_asset_schedule las
          where (ls_asset_id, revision, set_of_books_id) in (
		     select ls_asset_id, revision, set_of_books_id
			 from ls_ilr_asset_schedule_stg z))
          where the_row = 1
		  and ls_asset_id = z.ls_asset_id
		  and revision = z.revision
		  and set_of_books_id = z.set_of_books_id
		  and month = z.month);
       PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount||' rows updated on LS_ASSET_SCHEDULE');

      L_STATUS := 'UPDATING depr group and depr forecast. Beginning depr logging.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

	  l_status := 'Clearing ls_depr_info_tmp';
      pkg_pp_log.p_write_message(l_status);
      delete from ls_depr_info_tmp;
      
      l_status := 'Populating ls_depr_info_tmp';
      INSERT INTO ls_depr_info_tmp(ilr_id,
                                   ls_asset_id, 
                                   set_of_books_id, 
                                   revision, 
                                   MONTH, 
                                   end_capital_cost,
                                   depr_group_id,
                                   economic_life,
                                   set_of_books,
                                   mid_period_conv,
                                   mid_period_method,
                                   fasb_cap_type_id)
      SELECT  ilr_id,
              ls_asset_id,
              set_of_books_id,
              revision,
              month,
              end_capital_cost,
              depr_group_id,
              economic_life,
              set_of_books,
              mid_period_conv,
              mid_period_method,
              fasb_cap_type_id
        from
        (
          SELECT  la.ilr_id, 
                  las.ls_asset_id, 
                  las.set_of_books_id, 
                  las.revision, 
                  las.MONTH, 
                  las.end_capital_cost, 
                  la.depr_group_id, 
                  la.economic_life, /* WMD */
                  row_number() OVER(PARTITION BY la.ilr_id, las.ls_asset_id, las.set_of_books_id, las.revision, sob_map.fasb_cap_type_id ORDER BY las.MONTH) AS the_row,
                  dg.mid_period_conv /* CJS 4/11/17 */, 
                  sob.DESCRIPTION set_of_books, 
                  LOWER(dg.mid_period_method) mid_period_method,
                  sob_map.fasb_cap_type_id
          from ls_asset_schedule las, 
                ls_asset la, 
                depr_group dg, 
                ls_ilr_options lio, 
                ls_fasb_cap_type_sob_map sob_map, 
                set_of_books sob
          where la.depr_group_id = dg.depr_group_id
          and las.ls_asset_id = la.ls_asset_id
          AND lio.ilr_id = la.ilr_id
          AND lio.revision = las.revision
          AND sob_map.lease_cap_type_id = lio.lease_cap_type_id
          AND sob_map.set_of_books_id = las.set_of_books_id
          and las.set_of_books_id = sob.set_of_books_id
	  and (la.ilr_id, las.revision) in (SELECT ilr_id, revision
	      		  		    FROM ls_ilr_stg)
        )
        where the_row = 1;
      

      for L_ILR in (select * from ls_ilr_stg) loop

         PKG_PP_LOG.P_WRITE_MESSAGE('Entering P_GET_LEASE_DEPR');
         PKG_LEASE_DEPR.P_GET_LEASE_DEPR(L_ILR.ILR_ID, L_ILR.REVISION);
		 pkg_pp_log.p_write_message('Entering P_FCST_LEASE_USE_TEMP_TABLE');
         PKG_LEASE_DEPR.P_FCST_LEASE_USE_TEMP_TABLE(L_ILR.ILR_ID, L_ILR.REVISION);

         PKG_PP_LOG.P_WRITE_MESSAGE('Finished P_FCST_LEASE_USE_TEMP_TABLE');

         --start log back up again
         PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());

         L_STATUS:='Checking to see if ILR is sinking fund';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

        select ll.lease_type_id, ilr.company_id, ig.use_floating_rate
        into L_LEASE_TYPE, L_COMPANY_ID, L_FLOAT_RATE
        from ls_lease ll, ls_ilr ilr, ls_ilr_group ig
        where ilr.lease_id = ll.lease_id
          and ilr.ilr_id = L_ILR.ilr_id
		  and ilr.ilr_group_id = ig.ilr_group_id;

        if L_LEASE_TYPE = 3 then
        if A_MONTH is null then
          select min(PAYMENT_TERM_DATE)
          into L_MONTH
          from LS_ILR_PAYMENT_TERM
          where ilr_id = L_ILR.ILR_ID
            AND revision = L_ILR.REVISION;
        else
         L_MONTH:= A_MONTH;
        end if;
		  L_STATUS:='Calling Sinking fund schedules calc';
		  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
          L_STATUS:=PKG_LEASE_CALC.F_FLOATING_RATE_SF_ACCRUAL(-1, L_COMPANY_ID, L_MONTH, L_ILR.ILR_ID, L_ILR.REVISION);
          IF L_STATUS <> 'OK' THEN
            PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
            PKG_PP_LOG.P_WRITE_MESSAGE('Error building sinking fund schedules: ' || L_STATUS);
            RETURN L_STATUS;
          END IF;
        end if;
		/* CJS 4/23/15 Call floating rate by ILR for variable leases */
		if L_LEASE_TYPE <> 3 and L_FLOAT_RATE = 1 then
		  L_STATUS:='Calling floating rate schedules calc';
		  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		  L_STATUS:=PKG_LEASE_CALC.F_FLOATING_RATE_PAYMENT(-1, L_COMPANY_ID, L_MONTH, L_ILR.ILR_ID, L_ILR.REVISION);
		  IF L_STATUS <> 'OK' THEN
            PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
            PKG_PP_LOG.P_WRITE_MESSAGE('Error building floating rate schedules: ' || L_STATUS);
            RETURN L_STATUS;
          END IF;
        end if;


      end loop;
	  
	  l_status := 'Clearing ls_depr_info_tmp';
      pkg_pp_log.p_write_message(l_status);
      delete from ls_depr_info_tmp;

      -- update term penalty on ls_asset
      L_STATUS := 'UPDATING asset termination penalty amount';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      update LS_ASSET A
         set A.TERMINATION_PENALTY_AMOUNT =
              (select min(M.TERM_PENALTY) from LS_ILR_ASSET_STG M where M.LS_ASSET_ID = A.LS_ASSET_ID)
       where exists (select 1 from LS_ILR_ASSET_STG M where M.LS_ASSET_ID = A.LS_ASSET_ID);

      -- update the ls_ilr_amounts_set_of_books
      L_STATUS := 'Deleting from ls_ilr_amounts_set_of_books';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_AMOUNTS_SET_OF_BOOKS
       where (ILR_ID, REVISION, set_of_books_id) in (select A.ILR_ID, A.REVISION, a.set_of_books_id from LS_ILR_stg A);

	  L_STATUS:='Inserting into ls_ilr_amounts_set_of_books';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          IS_OM, CURRENT_LEASE_COST)
         select L.ILR_ID,
                L.REVISION,
                L.SET_OF_BOOKS_ID,
                NVL(L.NPV, 0),
                NVL(100 * (POWER((1 + L.IRR), 12) - 1), 0),
                sum(A.BEG_CAPITAL_COST),
                L.IS_OM,
                L.FMV
           from LS_ILR_ASSET_SCHEDULE_STG A, LS_ILR_STG L
          where A.ID = 1
            and L.ILR_ID = A.ILR_ID
            and L.REVISION = A.REVISION
            and L.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
          group by L.ILR_ID,
                   L.REVISION,
                   L.SET_OF_BOOKS_ID,
                   NVL(L.NPV, 0),
                   NVL(100 * (POWER((1 + L.IRR), 12) - 1), 0),
                   L.IS_OM,
                   L.FMV;

      L_STATUS:='Inserting missing amounts into ls_ilr_amounts_set_of_books';
      insert into ls_ilr_amounts_set_of_books
      (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          IS_OM, CURRENT_LEASE_COST)
      select a.ILR_ID, stg.REVISION, a.SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          a.IS_OM, CURRENT_LEASE_COST
      from ls_ilr_amounts_set_of_books a, ls_ilr ilr, ls_ilr_stg stg
      where a.ilr_id = stg.ilr_id
        and a.set_of_books_id = stg.set_of_books_id
        and a.ilr_id = ilr.ilr_id
        and a.revision = ilr.current_revision
        and not exists (select 1 from ls_ilr_amounts_set_of_books where ilr_id = a.ilr_id and revision = stg.revision and set_of_books_id = a.set_of_books_id);

      L_STATUS:='Updating Current Lease Cost on LS_ASSET_SCHEDULE';
      update
      (select a.ls_asset_id, a.revision, a.month, a.current_lease_cost, lg.require_components, set_of_books_id
      from ls_asset_schedule a, ls_asset la, ls_ilr ilr, ls_lease ll, ls_lease_group lg
      where a.ls_asset_id = la.ls_asset_id
        and la.ilr_id = ilr.ilr_id
        and ilr.lease_id = ll.lease_id
        and ll.lease_group_id = lg.lease_group_id) las
      set current_lease_cost =
      case when las.require_components = 1 then
      (select sum(cc.amount)
      from ls_component_charge cc, ls_component lc, ls_asset la, ls_ilr ilr, ls_lease ll
      where las.ls_asset_id = la.ls_asset_id
        and cc.component_id = lc.component_id
        and lc.ls_asset_id = la.ls_asset_id
        and la.ilr_id = ilr.ilr_id
        and ilr.lease_id = ll.lease_id
        and cc.interim_interest_start_date is not null
        and las.month >= add_months(trunc(cc.interim_interest_start_date, 'month'), case when extract(day from cc.interim_interest_start_date) >= ll.cut_off_day then 1 else 0 end))
      else
        (select fmv
        from ls_asset la
        where la.ls_asset_id = las.ls_asset_id)
      end
      where (ls_asset_id, revision, set_of_books_id) in (select ls_asset_id, revision, set_of_books_id from ls_ilr_asset_schedule_stg);

      L_STATUS:='Updating Current Lease Cost on LS_ILR_SCHEDULE';
      update
      (select i.ilr_id, i.revision, i.month, i.current_lease_cost, lg.require_components, set_of_books_id
      from ls_ilr_schedule i, ls_ilr ilr, ls_lease ll, ls_lease_group lg
      where i.ilr_id = ilr.ilr_id
        and ilr.lease_id = ll.lease_id
        and ll.lease_group_id = lg.lease_group_id) lis
      set current_lease_cost =
      case when lis.require_components = 1 then
      (select sum(cc.amount)
      from ls_component_charge cc, ls_component lc, ls_asset la, ls_ilr ilr, ls_lease ll
      where lis.ilr_id = ilr.ilr_id
        and cc.component_id = lc.component_id
        and lc.ls_asset_id = la.ls_asset_id
        and la.ilr_id = ilr.ilr_id
        and ilr.lease_id = ll.lease_id
        and cc.interim_interest_start_date is not null
        and lis.month >= add_months(trunc(cc.interim_interest_start_date, 'month'), case when extract(day from cc.interim_interest_start_date) >= ll.cut_off_day then 1 else 0 end))
      else
        (select fmv
        from LS_ILR_STG STG
        WHERE STG.ILR_ID = LIS.ILR_ID
		  and stg.set_of_books_id = lis.set_of_books_id
          AND STG.REVISION = LIS.REVISION)
      end
      where (ilr_id, revision, set_of_books_id) in (select ilr_id, revision, set_of_books_id from ls_ilr_stg);

      L_STATUS:='Setting residual amount to the guarantee';
      update ls_asset_schedule a
      set residual_amount =
      (select nvl(guaranteed_residual_amount,0)
       from ls_asset la
       where la.ls_asset_id = a.ls_asset_id)
      where month = (
        select max(month)
        from ls_asset_schedule
        where ls_asset_id = a.ls_asset_id
        and set_of_books_id = a.set_of_books_id
        and revision = a.revision)
       and (ls_asset_id, revision, set_of_books_id) in (select ls_asset_id, revision, set_of_books_id from ls_ilr_asset_schedule_stg);
       L_STATUS:='Setting ILR schedule residual amount to the guarantee';
       merge into (select * from ls_ilr_schedule where (ilr_id, revision, set_of_books_id) in (select ilr_id, revision, set_of_books_id from ls_ilr_asset_schedule_stg)) a
      using (select la.ilr_id, las.revision, month, las.set_of_books_id, sum(nvl(residual_amount,0)) total_resid
       from ls_asset_schedule las, ls_asset la
       where la.ls_asset_id = las.ls_asset_id
        and la.approved_revision = las.revision
        and nvl(las.residual_amount,0) <> 0
        and (la.ls_asset_id, las.revision, las.set_of_books_id) in (select ls_asset_id, revision, set_of_books_id from ls_ilr_asset_schedule_stg)
       group by la.ilr_id, las.revision, las.month, las.set_of_books_id) b
    on (a.ilr_id = b.ilr_id and a.revision = b.revision and a.month = b.month and a.set_of_books_id = b.set_of_books_id)
    when matched then update set a.residual_amount = b.total_resid;
	
	L_STATUS:='Starting the BPO function.';
    FOR I IN (
              SELECT DISTINCT LS_ASSET_ID, ILR_ID, REVISION
              FROM LS_ILR_ASSET_SCHEDULE_STG
             )
    LOOP

      L_RTN := F_BPO_SCHEDULE_EXTEND(I.LS_ASSET_ID, I.ILR_ID, I.REVISION);

      IF L_RTN <> 'OK' THEN
        PKG_PP_LOG.P_WRITE_MESSAGE('Error extending schedules for LS_ASSET_ID: ' || I.LS_ASSET_ID || ' ILR_ID: ' || I.ILR_ID || ' Revision: ' || I.REVISION);
        ROLLBACK;
      END IF;


    END LOOP;
    L_STATUS:='The BPO function ended.';

    --Refresh dense currency rates in case there is a new min/max month on the schedule tables 
    --(which determine the months for which dense currency rates will be generated)
	PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);


    --Refresh dense currency rates in case there is a new min/max month on the schedule tables
    --(which determine the months for which dense currency rates will be generated)
      SELECT Count(1)
      INTO L_COUNT
          FROM (
                SELECT 1
                FROM (
                      SELECT MAX(TRUNC(MONTH, 'MONTH')) as max_month
                      FROM ls_asset_schedule
                      UNION ALL
                      SELECT MAX(TRUNC(MONTH, 'MONTH')) AS max_month
                      FROM ls_ilr_schedule
                     )
                WHERE max_month > (SELECT MAX(TRUNC(exchange_date, 'MONTH'))
                                   FROM currency_rate_default_dense)
               UNION ALL
                SELECT 1
                FROM (SELECT MIN(TRUNC(MONTH, 'MONTH')) AS min_month
                      FROM ls_asset_schedule
                      UNION ALL
                      SELECT MIN(TRUNC(MONTH, 'MONTH')) AS min_month
                      FROM ls_ilr_schedule
                     )
                WHERE min_month < (SELECT MIN(TRUNC(exchange_date, 'MONTH'))
                                   FROM currency_rate_default_dense)
               );

          if L_COUNT > 0 THEN
		    L_STATUS:='Refreshing Currency Rates';
			PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            p_refresh_curr_rate_dflt_dense;
          END IF;

        L_STATUS:='Currency function ended.';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

      return 'OK';
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_SAVE_SCHEDULES;

   function F_PROCESS_ASSET_TRF(A_FROM_ASSET_ID     number,
                                A_TO_ASSET_ID       number,
                                A_FROM_ILR_REVISION number,
                                A_TO_ILR_REVISION   number,
                                A_PERCENT           number,
                                A_FROM_ILR_ID       number,
                                A_TO_ILR_ID         number,
                                A_MONTH             date:=null) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('Processing Asset Transfer');
      PKG_PP_LOG.P_WRITE_MESSAGE('   FROM ASSET: ' || TO_CHAR(A_FROM_ASSET_ID));
      PKG_PP_LOG.P_WRITE_MESSAGE('   TO ASSET: ' || TO_CHAR(A_TO_ASSET_ID));
      PKG_PP_LOG.P_WRITE_MESSAGE('   FROM ILR: ' || TO_CHAR(A_FROM_ILR_ID));
      PKG_PP_LOG.P_WRITE_MESSAGE('   TO ILR: ' || TO_CHAR(A_TO_ILR_ID));
      PKG_PP_LOG.P_WRITE_MESSAGE('   FROM ILR Revision: ' || TO_CHAR(A_FROM_ILR_REVISION));
      PKG_PP_LOG.P_WRITE_MESSAGE('   TO ILR Revision: ' || TO_CHAR(A_TO_ILR_REVISION));
      PKG_PP_LOG.P_WRITE_MESSAGE('   PERCENT: ' || TO_CHAR(A_PERCENT));
	  PKG_PP_LOG.P_WRITE_MESSAGE('   In Service Month: ' || to_char(A_MONTH,'YYYYMM'));

      L_STATUS := 'STAGING ILR';
      insert into LS_ILR_STG
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NPV, FMV, IRR, IS_OM)
         select LS.ILR_ID,
                A_TO_ILR_REVISION,
                LS.SET_OF_BOOKS_ID,
                LS.NET_PRESENT_VALUE,
                LS.CURRENT_LEASE_COST,
                case
                   when LS.INTERNAL_RATE_RETURN = 0 then
                    LO.INCEPTION_AIR
                   else
                    LS.INTERNAL_RATE_RETURN
                end /  (100 * (nvl(LL.DAYS_IN_YEAR, 360)/30 )), /* WMD */
                LS.IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS LS, LS_ILR_OPTIONS LO, LS_ILR ILR, LS_LEASE LL
          where LS.ILR_ID = A_FROM_ILR_ID
            and LS.REVISION = A_TO_ILR_REVISION
            and LS.ILR_ID = LO.ILR_ID
            and LS.REVISION = LO.REVISION
            and ILR.ILR_ID = A_FROM_ILR_ID
            AND ILR.LEASE_ID = LL.LEASE_ID;

	  L_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_STATUS := 'STAGING assets on the ILR pre transfer';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_STG
         (ID, ILR_ID, SET_OF_BOOKS_ID, REVISION, LS_ASSET_ID, TERM_PENALTY, IS_OM)
         select ROW_NUMBER() OVER(partition by LS.ILR_ID, LS.REVISION, LS.SET_OF_BOOKS_ID order by LS.FMV desc, LA.LS_ASSET_ID),
                LS.ILR_ID,
                LS.SET_OF_BOOKS_ID,
                LS.REVISION,
                LA.LS_ASSET_ID,
                LA.TERMINATION_PENALTY_AMOUNT,
                LS.IS_OM
           from LS_ILR_STG LS, LS_ASSET LA
          where LS.ILR_ID = LA.ILR_ID
            and exists (select 1
                   from LS_ASSET_SCHEDULE LSCH
                  where LSCH.REVISION = A_FROM_ILR_REVISION
                    and LSCH.LS_ASSET_ID = LA.LS_ASSET_ID);

	  L_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      -- allocate amounts to NEW and OLD ASSET
      L_STATUS := 'ALLOCATE amounts between NEW and OLD ASSET';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_SCHEDULE_STG
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, month, RATE, PREPAY_SWITCH,
          PAYMENT_MONTH, MONTHS_TO_ACCRUE, AMOUNT, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, CONTINGENT_ACCRUAL1,
          CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5,
          CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9,
          CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3,
          EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7,
          EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, CONTINGENT_PAID1,
          CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6,
          CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, EXECUTORY_PAID1,
          EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6,
          EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, IS_OM,
          PAYMENT_TERM_TYPE_ID)
         select A.ID,
                V.ILR_ID,
                V.REVISION,
                V.LS_ASSET_ID,
                A.SET_OF_BOOKS_ID,
                A.MONTH,
                A.RATE,
                A.PREPAY_SWITCH,
                A.PAYMENT_MONTH,
                A.MONTHS_TO_ACCRUE,
                ROUND(A.AMOUNT * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.AMOUNT - sum(ROUND(A.AMOUNT * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as AMOUNT,
                ROUND(A.RESIDUAL_AMOUNT * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.RESIDUAL_AMOUNT - sum(ROUND(A.RESIDUAL_AMOUNT * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as RESIDUAL_AMOUNT,
                ROUND(A.TERM_PENALTY * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.TERM_PENALTY - sum(ROUND(A.TERM_PENALTY * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as TERM_PENALTY,
                ROUND(A.BPO_PRICE * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.BPO_PRICE - sum(ROUND(A.BPO_PRICE * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as BPO_PRICE,
                ROUND(A.BEG_CAPITAL_COST * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.BEG_CAPITAL_COST - sum(ROUND(A.BEG_CAPITAL_COST * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as BEG_CAPITAL_COST,
                ROUND(A.END_CAPITAL_COST * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.END_CAPITAL_COST - sum(ROUND(A.END_CAPITAL_COST * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as END_CAPITAL_COST,
                ROUND(A.BEG_OBLIGATION * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.BEG_OBLIGATION - sum(ROUND(A.BEG_OBLIGATION * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as BEG_OBLIGATION,
                ROUND(A.END_OBLIGATION * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.END_OBLIGATION - sum(ROUND(A.END_OBLIGATION * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as END_OBLIGATION,
                ROUND(A.CONTINGENT_ACCRUAL1 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL1 - sum(ROUND(A.CONTINGENT_ACCRUAL1 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL1,
                ROUND(A.CONTINGENT_ACCRUAL2 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL2 - sum(ROUND(A.CONTINGENT_ACCRUAL2 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL2,
                ROUND(A.CONTINGENT_ACCRUAL3 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL3 - sum(ROUND(A.CONTINGENT_ACCRUAL3 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL3,
                ROUND(A.CONTINGENT_ACCRUAL4 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL4 - sum(ROUND(A.CONTINGENT_ACCRUAL4 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL4,
                ROUND(A.CONTINGENT_ACCRUAL5 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL5 - sum(ROUND(A.CONTINGENT_ACCRUAL5 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL5,
                ROUND(A.CONTINGENT_ACCRUAL6 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL6 - sum(ROUND(A.CONTINGENT_ACCRUAL6 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL6,
                ROUND(A.CONTINGENT_ACCRUAL7 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL7 - sum(ROUND(A.CONTINGENT_ACCRUAL7 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL7,
                ROUND(A.CONTINGENT_ACCRUAL8 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL8 - sum(ROUND(A.CONTINGENT_ACCRUAL8 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL8,
                ROUND(A.CONTINGENT_ACCRUAL9 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL9 - sum(ROUND(A.CONTINGENT_ACCRUAL9 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL9,
                ROUND(A.CONTINGENT_ACCRUAL10 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_ACCRUAL10 - sum(ROUND(A.CONTINGENT_ACCRUAL10 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_ACCRUAL10,
                ROUND(A.EXECUTORY_ACCRUAL1 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL1 - sum(ROUND(A.EXECUTORY_ACCRUAL1 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL1,
                ROUND(A.EXECUTORY_ACCRUAL2 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL2 - sum(ROUND(A.EXECUTORY_ACCRUAL2 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL2,
                ROUND(A.EXECUTORY_ACCRUAL3 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL3 - sum(ROUND(A.EXECUTORY_ACCRUAL3 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL3,
                ROUND(A.EXECUTORY_ACCRUAL4 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL4 - sum(ROUND(A.EXECUTORY_ACCRUAL4 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL4,
                ROUND(A.EXECUTORY_ACCRUAL5 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL5 - sum(ROUND(A.EXECUTORY_ACCRUAL5 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL5,
                ROUND(A.EXECUTORY_ACCRUAL6 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL6 - sum(ROUND(A.EXECUTORY_ACCRUAL6 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL6,
                ROUND(A.EXECUTORY_ACCRUAL7 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL7 - sum(ROUND(A.EXECUTORY_ACCRUAL7 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL7,
                ROUND(A.EXECUTORY_ACCRUAL8 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL8 - sum(ROUND(A.EXECUTORY_ACCRUAL8 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL8,
                ROUND(A.EXECUTORY_ACCRUAL9 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL9 - sum(ROUND(A.EXECUTORY_ACCRUAL9 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL9,
                ROUND(A.EXECUTORY_ACCRUAL10 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_ACCRUAL10 - sum(ROUND(A.EXECUTORY_ACCRUAL10 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_ACCRUAL10,
                ROUND(A.CONTINGENT_PAID1 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID1 - sum(ROUND(A.CONTINGENT_PAID1 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID1,
                ROUND(A.CONTINGENT_PAID2 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID2 - sum(ROUND(A.CONTINGENT_PAID2 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID2,
                ROUND(A.CONTINGENT_PAID3 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID3 - sum(ROUND(A.CONTINGENT_PAID3 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID3,
                ROUND(A.CONTINGENT_PAID4 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID4 - sum(ROUND(A.CONTINGENT_PAID4 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID4,
                ROUND(A.CONTINGENT_PAID5 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID5 - sum(ROUND(A.CONTINGENT_PAID5 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID5,
                ROUND(A.CONTINGENT_PAID6 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID6 - sum(ROUND(A.CONTINGENT_PAID6 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID6,
                ROUND(A.CONTINGENT_PAID7 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID7 - sum(ROUND(A.CONTINGENT_PAID7 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID7,
                ROUND(A.CONTINGENT_PAID8 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID8 - sum(ROUND(A.CONTINGENT_PAID8 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID8,
                ROUND(A.CONTINGENT_PAID9 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID9 - sum(ROUND(A.CONTINGENT_PAID9 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID9,
                ROUND(A.CONTINGENT_PAID10 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.CONTINGENT_PAID10 - sum(ROUND(A.CONTINGENT_PAID10 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as CONTINGENT_PAID10,
                ROUND(A.EXECUTORY_PAID1 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID1 - sum(ROUND(A.EXECUTORY_PAID1 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID1,
                ROUND(A.EXECUTORY_PAID2 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID2 - sum(ROUND(A.EXECUTORY_PAID2 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID2,
                ROUND(A.EXECUTORY_PAID3 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID3 - sum(ROUND(A.EXECUTORY_PAID3 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID3,
                ROUND(A.EXECUTORY_PAID4 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID4 - sum(ROUND(A.EXECUTORY_PAID4 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID4,
                ROUND(A.EXECUTORY_PAID5 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID5 - sum(ROUND(A.EXECUTORY_PAID5 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID5,
                ROUND(A.EXECUTORY_PAID6 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID6 - sum(ROUND(A.EXECUTORY_PAID6 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID6,
                ROUND(A.EXECUTORY_PAID7 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID7 - sum(ROUND(A.EXECUTORY_PAID7 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID7,
                ROUND(A.EXECUTORY_PAID8 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID8 - sum(ROUND(A.EXECUTORY_PAID8 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID8,
                ROUND(A.EXECUTORY_PAID9 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID9 - sum(ROUND(A.EXECUTORY_PAID9 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID9,
                ROUND(A.EXECUTORY_PAID10 * V.PCT_SPREAD, 2) +
                DECODE(ROW_NUMBER() OVER(partition by A.SET_OF_BOOKS_ID,
                            A.MONTH order by V.PCT_SPREAD desc,
                            V.LS_ASSET_ID),
                       1,
                       A.EXECUTORY_PAID10 - sum(ROUND(A.EXECUTORY_PAID10 * V.PCT_SPREAD, 2))
                       OVER(partition by A.SET_OF_BOOKS_ID, A.MONTH),
                       0) as EXECUTORY_PAID10,
                A.IS_OM,
                A.PAYMENT_TERM_TYPE_ID
           from LS_ILR_ASSET_SCHEDULE_CALC_STG A,
                LS_ILR_ASSET_STG S,
                (select A_TO_ILR_ID as ILR_ID,
                        A_TO_ASSET_ID as LS_ASSET_ID,
                        A_PERCENT as PCT_SPREAD,
                        case
                           when A_TO_ILR_ID = A_FROM_ILR_ID then
                            A_TO_ILR_REVISION
                           else
                            1
                        end as REVISION
                   from DUAL
                 union all
                 select A_FROM_ILR_ID as ILR_ID,
                        A_FROM_ASSET_ID as LS_ASSET_ID,
                        1 - A_PERCENT as PCT_SPREAD,
                        A_TO_ILR_REVISION
                   from DUAL) V
          where S.LS_ASSET_ID = A.LS_ASSET_ID
            and A.REVISION = A_FROM_ILR_REVISION
            and A.MONTH is not null
            and A.ID > 0
            and S.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
            and S.LS_ASSET_ID = A_FROM_ASSET_ID;

      L_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      L_STATUS := 'LOAD remainder of assets for the schedule';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_SCHEDULE_STG
         (ID, ILR_ID, REVISION, LS_ASSET_ID, SET_OF_BOOKS_ID, month, AMOUNT, RESIDUAL_AMOUNT,
          TERM_PENALTY, BPO_PRICE, PREPAY_SWITCH, PAYMENT_MONTH, MONTHS_TO_ACCRUE, RATE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, CONTINGENT_ACCRUAL1,
          CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5,
          CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9,
          CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3,
          EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7,
          EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, CONTINGENT_PAID1,
          CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6,
          CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, EXECUTORY_PAID1,
          EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6,
          EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, IS_OM,
          PAYMENT_TERM_TYPE_ID)
         select A.ID,
                A.ILR_ID,
                S.REVISION,
                A.LS_ASSET_ID,
                A.SET_OF_BOOKS_ID,
                A.MONTH,
                A.AMOUNT,
                A.RESIDUAL_AMOUNT,
                A.TERM_PENALTY,
                A.BPO_PRICE,
                A.PREPAY_SWITCH,
                A.PAYMENT_MONTH,
                A.MONTHS_TO_ACCRUE,
                A.RATE,
                A.BEG_CAPITAL_COST,
                A.END_CAPITAL_COST,
                A.BEG_OBLIGATION,
                A.END_OBLIGATION,
                A.CONTINGENT_ACCRUAL1,
                A.CONTINGENT_ACCRUAL2,
                A.CONTINGENT_ACCRUAL3,
                A.CONTINGENT_ACCRUAL4,
                A.CONTINGENT_ACCRUAL5,
                A.CONTINGENT_ACCRUAL6,
                A.CONTINGENT_ACCRUAL7,
                A.CONTINGENT_ACCRUAL8,
                A.CONTINGENT_ACCRUAL9,
                A.CONTINGENT_ACCRUAL10,
                A.EXECUTORY_ACCRUAL1,
                A.EXECUTORY_ACCRUAL2,
                A.EXECUTORY_ACCRUAL3,
                A.EXECUTORY_ACCRUAL4,
                A.EXECUTORY_ACCRUAL5,
                A.EXECUTORY_ACCRUAL6,
                A.EXECUTORY_ACCRUAL7,
                A.EXECUTORY_ACCRUAL8,
                A.EXECUTORY_ACCRUAL9,
                A.EXECUTORY_ACCRUAL10,
                A.CONTINGENT_PAID1,
                A.CONTINGENT_PAID2,
                A.CONTINGENT_PAID3,
                A.CONTINGENT_PAID4,
                A.CONTINGENT_PAID5,
                A.CONTINGENT_PAID6,
                A.CONTINGENT_PAID7,
                A.CONTINGENT_PAID8,
                A.CONTINGENT_PAID9,
                A.CONTINGENT_PAID10,
                A.EXECUTORY_PAID1,
                A.EXECUTORY_PAID2,
                A.EXECUTORY_PAID3,
                A.EXECUTORY_PAID4,
                A.EXECUTORY_PAID5,
                A.EXECUTORY_PAID6,
                A.EXECUTORY_PAID7,
                A.EXECUTORY_PAID8,
                A.EXECUTORY_PAID9,
                A.EXECUTORY_PAID10,
                A.IS_OM,
                A.PAYMENT_TERM_TYPE_ID
           from LS_ILR_ASSET_SCHEDULE_CALC_STG A, LS_ILR_ASSET_STG S
          where S.LS_ASSET_ID = A.LS_ASSET_ID
            and A.REVISION = A_FROM_ILR_REVISION
            and A.MONTH is not null
            and A.ID > 0
            and S.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
            and S.LS_ASSET_ID <> A_FROM_ASSET_ID;

	  L_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      --clear out the from asset if it's 100%
      if A_PERCENT = 1 then
         delete from LS_ILR_ASSET_SCHEDULE_STG
         where LS_ASSET_ID = A_FROM_ASSET_ID;

		L_STATUS := '   Rows deleted: ' || TO_CHAR(sql%rowcount);
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         delete from LS_ILR_ASSET_STG
         where LS_ASSET_ID = A_FROM_ASSET_ID;

		L_STATUS := '   Rows deleted: ' || TO_CHAR(sql%rowcount);
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      end if;

	  G_COPY_LS_ASSET_ID:= A_FROM_ASSET_ID;

         /* CJS 3/9/17 Need to move this insert ahead of f_process_assets so the transfer will get picked up properly */
         -- need to insert correct values into ls_ilr_stg
         -- for the from and to ilrs
         -- since those have changed. (only if ILRs are different)
         if A_FROM_ILR_ID <> A_TO_ILR_ID then
            L_STATUS := 'LOAD ls_ilr_stg for NEW ilr';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            insert into LS_ILR_STG
               (ILR_ID, REVISION, SET_OF_BOOKS_ID, NPV, FMV, IRR, IS_OM)
               select LS.ILR_ID,
                      1,
                      LS.SET_OF_BOOKS_ID,
                      LS.NET_PRESENT_VALUE,
                      LS.CURRENT_LEASE_COST,
                      case
                         when LS.INTERNAL_RATE_RETURN = 0 then
                          LO.INCEPTION_AIR
                         else
                          LS.INTERNAL_RATE_RETURN
                      end /  (100 * (nvl(LL.DAYS_IN_YEAR, 360)/30 )), /* WMD */
                      LS.IS_OM
                 from LS_ILR_AMOUNTS_SET_OF_BOOKS LS, LS_ILR_OPTIONS LO, LS_ILR ILR, LS_LEASE LL
                where LS.ILR_ID = A_TO_ILR_ID
                  and LS.REVISION = 1
                  and LS.ILR_ID = LO.ILR_ID
                  and LS.REVISION = LO.REVISION
                  and ILR.ILR_ID = A_TO_ILR_ID
				  and ILR.ILR_ID = A_TO_ILR_ID
                  and ilr.lease_id = ll.lease_id;
         end if;

      L_STATUS := F_PROCESS_ASSETS(A_MONTH);
      G_COPY_LS_ASSET_ID:=NULL;
      if L_STATUS = 'OK' then
         -- close connection since f_save_schedule creates its own connections
         -- before saving to the schedule
--         -- need to insert correct values into ls_ilr_stg
--         -- for the from and to ilrs
--         -- since those have changed. (only if ILRs are different)
--         if A_FROM_ILR_ID <> A_TO_ILR_ID then
--            L_STATUS := 'LOAD ls_ilr_stg for NEW ilr';
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
--            insert into LS_ILR_STG
--               (ILR_ID, REVISION, SET_OF_BOOKS_ID, NPV, FMV, IRR, IS_OM)
--               select LS.ILR_ID,
--                      1,
--                      LS.SET_OF_BOOKS_ID,
--                      LS.NET_PRESENT_VALUE,
--                      LS.CURRENT_LEASE_COST,
--                      case
--                         when LS.INTERNAL_RATE_RETURN = 0 then
--                          LO.INCEPTION_AIR
--                         else
--                          LS.INTERNAL_RATE_RETURN
--                      end /  (100 * (nvl(LL.DAYS_IN_YEAR, 360)/30 )), /* WMD */
--                      LS.IS_OM
--                 from LS_ILR_AMOUNTS_SET_OF_BOOKS LS, LS_ILR_OPTIONS LO, LS_ILR ILR, LS_LEASE LL
--                where LS.ILR_ID = A_TO_ILR_ID
--                  and LS.REVISION = 1
--                  and LS.ILR_ID = LO.ILR_ID
--                  and LS.REVISION = LO.REVISION
--                  and ILR.ILR_ID = A_TO_ILR_ID
--				  and ILR.ILR_ID = A_TO_ILR_ID
--                  and ilr.lease_id = ll.lease_id;
--         end if;

         L_STATUS := F_SAVE_SCHEDULES(A_MONTH);

         -- clear the staging tables now that they've been rolled up to the live tables
         L_MSG := F_CLEAR_STAGING_TABLES;
         if L_MSG <> 'OK' then
           PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         end if;

         return L_STATUS;
      else
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
      end if;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return L_STATUS;
   end F_PROCESS_ASSET_TRF;

   function F_GET_II_BEGIN_DATE(A_ILR_ID   number,
                                A_REVISION number) return date is
      RTN_DATE date;
   begin
      select INTERIM_INTEREST_BEGIN_DATE
        into RTN_DATE
        from LS_ILR_PAYMENT_TERM
       where ILR_ID = A_ILR_ID
         and REVISION = A_REVISION
         and PAYMENT_TERM_TYPE_ID = 1;

      return RTN_DATE;

   exception
      when NO_DATA_FOUND then
         return 'JUL-4-1776';
      when TOO_MANY_ROWS then
         return 'JUL-4-1776';

   end F_GET_II_BEGIN_DATE;

   function F_MAKE_II(A_ILR_ID   number,
                      A_REVISION number) return number is
      RTN number;
   begin
      select MAKE_II_PAYMENT
        into RTN
        from LS_ILR_PAYMENT_TERM
       where ILR_ID = A_ILR_ID
         and REVISION = A_REVISION
         and PAYMENT_TERM_TYPE_ID = 1;

      return RTN;

   end F_MAKE_II;

function F_SYNC_ILR_SCHEDULE(A_ILR_ID IN NUMBER, A_REVISION IN NUMBER) RETURN VARCHAR2
  is
  location varchar2(30000);
  begin

  PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
  LOCATION:='Deleting existing ILR Schedule';
  PKG_PP_LOG.P_WRITE_MESSAGE(LOCATION);

  delete from ls_ilr_schedule
  where ilr_id = A_ILR_ID
    and revision = A_REVISION;

  LOCATION:='Merging all asset schedules into ILR schedule';

  merge into ls_ilr_schedule a
  using (
  select    la.ilr_id,
            A_REVISION as revision,
            las.set_of_books_id,
            las.month,
            las.is_om,
            sum(nvl(las.RESIDUAL_AMOUNT,0)) as RESIDUAL_AMOUNT,
            sum(nvl(las.TERM_PENALTY,0)) as TERM_PENALTY,
            sum(nvl(las.BPO_PRICE,0)) as BPO_PRICE,
            sum(nvl(las.BEG_CAPITAL_COST,0)) as BEG_CAPITAL_COST,
            sum(nvl(las.END_CAPITAL_COST,0)) as END_CAPITAL_COST,
            sum(nvl(las.BEG_OBLIGATION,0)) as BEG_OBLIGATION,
            sum(nvl(las.END_OBLIGATION,0)) as END_OBLIGATION,
            sum(nvl(las.BEG_LT_OBLIGATION,0)) as BEG_LT_OBLIGATION,
            sum(nvl(las.END_LT_OBLIGATION,0)) as END_LT_OBLIGATION,
            sum(nvl(las.INTEREST_ACCRUAL,0)) as INTEREST_ACCRUAL,
            sum(nvl(las.PRINCIPAL_ACCRUAL,0)) as PRINCIPAL_ACCRUAL,
            sum(nvl(las.INTEREST_PAID,0)) as INTEREST_PAID,
            sum(nvl(las.PRINCIPAL_PAID,0)) as PRINCIPAL_PAID,
            sum(nvl(las.EXECUTORY_ACCRUAL1,0)) as EXECUTORY_ACCRUAL1,
            sum(nvl(las.EXECUTORY_ACCRUAL2,0)) as EXECUTORY_ACCRUAL2,
            sum(nvl(las.EXECUTORY_ACCRUAL3,0)) as EXECUTORY_ACCRUAL3,
            sum(nvl(las.EXECUTORY_ACCRUAL4,0)) as EXECUTORY_ACCRUAL4,
            sum(nvl(las.EXECUTORY_ACCRUAL5,0)) as EXECUTORY_ACCRUAL5,
            sum(nvl(las.EXECUTORY_ACCRUAL6,0)) as EXECUTORY_ACCRUAL6,
            sum(nvl(las.EXECUTORY_ACCRUAL7,0)) as EXECUTORY_ACCRUAL7,
            sum(nvl(las.EXECUTORY_ACCRUAL8,0)) as EXECUTORY_ACCRUAL8,
            sum(nvl(las.EXECUTORY_ACCRUAL9,0)) as EXECUTORY_ACCRUAL9,
            sum(nvl(las.EXECUTORY_ACCRUAL10,0)) as EXECUTORY_ACCRUAL10,
            sum(nvl(las.EXECUTORY_PAID1,0)) as EXECUTORY_PAID1,
            sum(nvl(las.EXECUTORY_PAID2,0)) as EXECUTORY_PAID2,
            sum(nvl(las.EXECUTORY_PAID3,0)) as EXECUTORY_PAID3,
            sum(nvl(las.EXECUTORY_PAID4,0)) as EXECUTORY_PAID4,
            sum(nvl(las.EXECUTORY_PAID5,0)) as EXECUTORY_PAID5,
            sum(nvl(las.EXECUTORY_PAID6,0)) as EXECUTORY_PAID6,
            sum(nvl(las.EXECUTORY_PAID7,0)) as EXECUTORY_PAID7,
            sum(nvl(las.EXECUTORY_PAID8,0)) as EXECUTORY_PAID8,
            sum(nvl(las.EXECUTORY_PAID9,0)) as EXECUTORY_PAID9,
            sum(nvl(las.EXECUTORY_PAID10,0)) as EXECUTORY_PAID10,
            sum(nvl(las.CONTINGENT_ACCRUAL1,0)) as CONTINGENT_ACCRUAL1,
            sum(nvl(las.CONTINGENT_ACCRUAL2,0)) as CONTINGENT_ACCRUAL2,
            sum(nvl(las.CONTINGENT_ACCRUAL3,0)) as CONTINGENT_ACCRUAL3,
            sum(nvl(las.CONTINGENT_ACCRUAL4,0)) as CONTINGENT_ACCRUAL4,
            sum(nvl(las.CONTINGENT_ACCRUAL5,0)) as CONTINGENT_ACCRUAL5,
            sum(nvl(las.CONTINGENT_ACCRUAL6,0)) as CONTINGENT_ACCRUAL6,
            sum(nvl(las.CONTINGENT_ACCRUAL7,0)) as CONTINGENT_ACCRUAL7,
            sum(nvl(las.CONTINGENT_ACCRUAL8,0)) as CONTINGENT_ACCRUAL8,
            sum(nvl(las.CONTINGENT_ACCRUAL9,0)) as CONTINGENT_ACCRUAL9,
            sum(nvl(las.CONTINGENT_ACCRUAL10,0)) as CONTINGENT_ACCRUAL10,
            sum(nvl(las.CONTINGENT_PAID1,0)) as CONTINGENT_PAID1,
            sum(nvl(las.CONTINGENT_PAID2,0)) as CONTINGENT_PAID2,
            sum(nvl(las.CONTINGENT_PAID3,0)) as CONTINGENT_PAID3,
            sum(nvl(las.CONTINGENT_PAID4,0)) as CONTINGENT_PAID4,
            sum(nvl(las.CONTINGENT_PAID5,0)) as CONTINGENT_PAID5,
            sum(nvl(las.CONTINGENT_PAID6,0)) as CONTINGENT_PAID6,
            sum(nvl(las.CONTINGENT_PAID7,0)) as CONTINGENT_PAID7,
            sum(nvl(las.CONTINGENT_PAID8,0)) as CONTINGENT_PAID8,
            sum(nvl(las.CONTINGENT_PAID9,0)) as CONTINGENT_PAID9,
            sum(nvl(las.CONTINGENT_PAID10,0)) as CONTINGENT_PAID10,
            SUM(NVL(LAS.CURRENT_LEASE_COST,0)) AS CURRENT_LEASE_COST,
            SUM(NVL(las.BEG_DEFERRED_RENT,0)) as BEG_DEFERRED_RENT,
            SUM(NVL(las.DEFERRED_RENT,0)) as DEFERRED_RENT,
            SUM(NVL(las.END_DEFERRED_RENT,0)) as END_DEFERRED_RENT,
            SUM(NVL(las.BEG_ST_DEFERRED_RENT,0)) as BEG_ST_DEFERRED_RENT,
            SUM(NVL(las.END_ST_DEFERRED_RENT,0)) as END_ST_DEFERRED_RENT
  from ls_asset la, ls_asset_schedule las
  where la.ls_asset_id = las.ls_asset_id
    and la.ilr_id = A_ILR_ID
    and las.revision = A_REVISION
  group by la.ilr_id,
            A_REVISION,
            las.set_of_books_id,
            las.month,
            las.is_om) b
  on (a.ilr_id = b.ilr_id and a.revision = b.revision and a.set_of_books_id = b.set_of_books_id and a.month = b.month)
  when matched then update set
    a.RESIDUAL_AMOUNT = b.RESIDUAL_AMOUNT,
    a.TERM_PENALTY = b.TERM_PENALTY,
    a.BPO_PRICE = b.BPO_PRICE,
    a.BEG_CAPITAL_COST = b.BEG_CAPITAL_COST,
    a.END_CAPITAL_COST = b.END_CAPITAL_COST,
    a.BEG_OBLIGATION = b.BEG_OBLIGATION,
    a.END_OBLIGATION = b.END_OBLIGATION,
    a.BEG_LT_OBLIGATION = b.BEG_LT_OBLIGATION,
    a.END_LT_OBLIGATION = b.END_LT_OBLIGATION,
    a.INTEREST_ACCRUAL = b.INTEREST_ACCRUAL,
    a.PRINCIPAL_ACCRUAL = b.PRINCIPAL_ACCRUAL,
    a.INTEREST_PAID = b.INTEREST_PAID,
    a.PRINCIPAL_PAID = b.PRINCIPAL_PAID,
    a.EXECUTORY_ACCRUAL1 = b.EXECUTORY_ACCRUAL1,
    a.EXECUTORY_ACCRUAL2 = b.EXECUTORY_ACCRUAL2,
    a.EXECUTORY_ACCRUAL3 = b.EXECUTORY_ACCRUAL3,
    a.EXECUTORY_ACCRUAL4 = b.EXECUTORY_ACCRUAL4,
    a.EXECUTORY_ACCRUAL5 = b.EXECUTORY_ACCRUAL5,
    a.EXECUTORY_ACCRUAL6 = b.EXECUTORY_ACCRUAL6,
    a.EXECUTORY_ACCRUAL7 = b.EXECUTORY_ACCRUAL7,
    a.EXECUTORY_ACCRUAL8 = b.EXECUTORY_ACCRUAL8,
    a.EXECUTORY_ACCRUAL9 = b.EXECUTORY_ACCRUAL9,
    a.EXECUTORY_ACCRUAL10 = b.EXECUTORY_ACCRUAL10,
    a.EXECUTORY_PAID1 = b.EXECUTORY_PAID1,
    a.EXECUTORY_PAID2 = b.EXECUTORY_PAID2,
    a.EXECUTORY_PAID3 = b.EXECUTORY_PAID3,
    a.EXECUTORY_PAID4 = b.EXECUTORY_PAID4,
    a.EXECUTORY_PAID5 = b.EXECUTORY_PAID5,
    a.EXECUTORY_PAID6 = b.EXECUTORY_PAID6,
    a.EXECUTORY_PAID7 = b.EXECUTORY_PAID7,
    a.EXECUTORY_PAID8 = b.EXECUTORY_PAID8,
    a.EXECUTORY_PAID9 = b.EXECUTORY_PAID9,
    a.EXECUTORY_PAID10 = b.EXECUTORY_PAID10,
    a.CONTINGENT_ACCRUAL1 = b.CONTINGENT_ACCRUAL1,
    a.CONTINGENT_ACCRUAL2 = b.CONTINGENT_ACCRUAL2,
    a.CONTINGENT_ACCRUAL3 = b.CONTINGENT_ACCRUAL3,
    a.CONTINGENT_ACCRUAL4 = b.CONTINGENT_ACCRUAL4,
    a.CONTINGENT_ACCRUAL5 = b.CONTINGENT_ACCRUAL5,
    a.CONTINGENT_ACCRUAL6 = b.CONTINGENT_ACCRUAL6,
    a.CONTINGENT_ACCRUAL7 = b.CONTINGENT_ACCRUAL7,
    a.CONTINGENT_ACCRUAL8 = b.CONTINGENT_ACCRUAL8,
    a.CONTINGENT_ACCRUAL9 = b.CONTINGENT_ACCRUAL9,
    a.CONTINGENT_ACCRUAL10 = b.CONTINGENT_ACCRUAL10,
    a.CONTINGENT_PAID1 = b.CONTINGENT_PAID1,
    a.CONTINGENT_PAID2 = b.CONTINGENT_PAID2,
    a.CONTINGENT_PAID3 = b.CONTINGENT_PAID3,
    a.CONTINGENT_PAID4 = b.CONTINGENT_PAID4,
    a.CONTINGENT_PAID5 = b.CONTINGENT_PAID5,
    a.CONTINGENT_PAID6 = b.CONTINGENT_PAID6,
    a.CONTINGENT_PAID7 = b.CONTINGENT_PAID7,
    a.CONTINGENT_PAID8 = b.CONTINGENT_PAID8,
    a.CONTINGENT_PAID9 = b.CONTINGENT_PAID9,
    a.CONTINGENT_PAID10 = b.CONTINGENT_PAID10,
    a.CURRENT_LEASE_COST = b.CURRENT_LEASE_COST,
    a.BEG_DEFERRED_RENT = b.BEG_DEFERRED_RENT,
    a.DEFERRED_RENT = b.DEFERRED_RENT,
    a.END_DEFERRED_RENT = b.END_DEFERRED_RENT,
    a.BEG_ST_DEFERRED_RENT = b.BEG_ST_DEFERRED_RENT,
    a.END_ST_DEFERRED_RENT = b.END_ST_DEFERRED_RENT
  when not matched then insert
  (ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST,
  END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, INTEREST_ACCRUAL,
  PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3,
  EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9,
  EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6,
  EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3,
  CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9,
  CONTINGENT_ACCRUAL10, CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
  CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, IS_OM, CURRENT_LEASE_COST,
  BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT)
  values (
    b.ILR_ID,
    b.REVISION,
    b.SET_OF_BOOKS_ID,
    b.MONTH,
    b.RESIDUAL_AMOUNT,
    b.TERM_PENALTY,
    b.BPO_PRICE,
    b.BEG_CAPITAL_COST,
    b.END_CAPITAL_COST,
    b.BEG_OBLIGATION,
    b.END_OBLIGATION,
    b.BEG_LT_OBLIGATION,
    b.END_LT_OBLIGATION,
    b.INTEREST_ACCRUAL,
    b.PRINCIPAL_ACCRUAL,
    b.INTEREST_PAID,
    b.PRINCIPAL_PAID,
    b.EXECUTORY_ACCRUAL1,
    b.EXECUTORY_ACCRUAL2,
    b.EXECUTORY_ACCRUAL3,
    b.EXECUTORY_ACCRUAL4,
    b.EXECUTORY_ACCRUAL5,
    b.EXECUTORY_ACCRUAL6,
    b.EXECUTORY_ACCRUAL7,
    b.EXECUTORY_ACCRUAL8,
    b.EXECUTORY_ACCRUAL9,
    b.EXECUTORY_ACCRUAL10,
    b.EXECUTORY_PAID1,
    b.EXECUTORY_PAID2,
    b.EXECUTORY_PAID3,
    b.EXECUTORY_PAID4,
    b.EXECUTORY_PAID5,
    b.EXECUTORY_PAID6,
    b.EXECUTORY_PAID7,
    b.EXECUTORY_PAID8,
    b.EXECUTORY_PAID9,
    b.EXECUTORY_PAID10,
    b.CONTINGENT_ACCRUAL1,
    b.CONTINGENT_ACCRUAL2,
    b.CONTINGENT_ACCRUAL3,
    b.CONTINGENT_ACCRUAL4,
    b.CONTINGENT_ACCRUAL5,
    b.CONTINGENT_ACCRUAL6,
    b.CONTINGENT_ACCRUAL7,
    b.CONTINGENT_ACCRUAL8,
    b.CONTINGENT_ACCRUAL9,
    b.CONTINGENT_ACCRUAL10,
    b.CONTINGENT_PAID1,
    b.CONTINGENT_PAID2,
    b.CONTINGENT_PAID3,
    b.CONTINGENT_PAID4,
    b.CONTINGENT_PAID5,
    b.CONTINGENT_PAID6,
    b.CONTINGENT_PAID7,
    b.CONTINGENT_PAID8,
    b.CONTINGENT_PAID9,
    b.CONTINGENT_PAID10,
    b.IS_OM,
    b.CURRENT_LEASE_COST,
    b.BEG_DEFERRED_RENT,
    b.DEFERRED_RENT,
    b.END_DEFERRED_RENT,
    b.BEG_ST_DEFERRED_RENT,
    b.END_ST_DEFERRED_RENT);

    return 'OK';

  EXCEPTION
    WHEN Others THEN
    return 'Error syncing ILR schedule to assets: ' || sqlerrm;
  end F_SYNC_ILR_SCHEDULE;

  function F_BUILD_FORECAST(A_REVISION number) return varchar2 is
      LOOKBACK_REVISION number;
      B_HAS_LOOKBACK boolean;
      FORECAST_TYPE_ID number;
      EFFECTIVE_DATE date;
      LOOKBACK_DATE date;
      NULL_DATE date := null;
      MSG varchar2(2000);
	  L_CUR_TYPE number;
      L_STATUS varchar2(2000);
	    L_RTN number;
      location varchar2(30000);
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      LOCATION:='Entering F_BUILD_FORECAST';
      PKG_PP_LOG.P_WRITE_MESSAGE(LOCATION);

      -- Get the lookback revision
      select LOOKBACK_REVISION, FORECAST_TYPE_ID, CONVERSION_DATE, LOOKBACK_DATE, LS_CURRENCY_TYPE_ID
        into LOOKBACK_REVISION, FORECAST_TYPE_ID, EFFECTIVE_DATE, LOOKBACK_DATE, L_CUR_TYPE
        from LS_FORECAST_VERSION
       where REVISION = A_REVISION;

      if LOOKBACK_REVISION is null then
         B_HAS_LOOKBACK := FALSE;
      else
         B_HAS_LOOKBACK := TRUE;
      end if;

      -- Delete from several tables
      MSG := 'Deleting from LS_ILR_ASSET_SCHEDULE_STG: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ILR_ASSET_SCHEDULE_STG
       where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_STG: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ILR_STG
        where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_ASSET_STG: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ILR_ASSET_STG
        where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_SCHEDULE_STG: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ILR_SCHEDULE_STG
        where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_ASSET_SCHEDULE_CALC_STG: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ILR_ASSET_SCHEDULE_CALC_STG
        where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ASSET_SCHEDULE: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ASSET_SCHEDULE
        where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_ILR_SCHEDULE: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_ILR_SCHEDULE
        where revision in (A_REVISION, LOOKBACK_REVISION);

      MSG := 'Deleting from LS_FORECAST_SUMMARY: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      delete from LS_SUMMARY_FORECAST
        where revision in (A_REVISION, LOOKBACK_REVISION);

      -- Loop over LS_FORECAST_ILR_MASTER to load each ILR
      MSG := 'Looping over LS_FORECAST_ILR_MASTER to load each ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      for i in (select distinct ilr_id from ls_forecast_ilr_master where revision = A_REVISION)
	    loop
        L_STATUS := F_LOAD_ILR_STG(i.ilr_id, A_REVISION);

	      if L_STATUS <> 'OK' then
		       return L_STATUS;
	      end if;
	    end loop;

      -- Run an update statement on ls_ilr_stg and set the npv_start_date column equal to the effective month of the lookback revision.
      MSG := 'Updating ls_ilr_stg.npv_start_date: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      update ls_ilr_stg
         set npv_start_date = EFFECTIVE_DATE
       where revision = A_REVISION;

      -- Call F_CALC_SCHEDULES
      MSG := 'Calling F_CALC_SCHEDULES';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      L_STATUS := F_CALC_SCHEDULES();

      if L_STATUS <> 'OK' then
        return L_STATUS;
      end if;

      if FORECAST_TYPE_ID = 2 then
        MSG := 'Loading Deferred Rent for Summary Forecast';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        L_STATUS := F_LOAD_DEFERRED_RENT(TRUE);

        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;
      end if;

      -- Call F_PROCESS_ASSETS and F_SAVE_SCHEDULES
      --   Only pass in the effective date if we are not doing a Full Retrospective
      MSG := 'Calling F_PROCESS_ASSETS and F_SAVE_SCHEDULES';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
      if FORECAST_TYPE_ID = 3 then
        L_STATUS := F_PROCESS_ASSETS(NULL_DATE);

        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;
		
		L_STATUS := PKG_LEASE_VAR_PAYMENTS.F_CALC_ALL_VAR_PAYMENTS(NULL_DATE);
        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;

        L_STATUS := F_SAVE_SCHEDULES(NULL_DATE);

        -- clear staging tables
         MSG := F_CLEAR_STAGING_TABLES;
         if MSG <> 'OK' then
           PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
         end if;

        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;


      else
        L_STATUS := F_PROCESS_ASSETS(EFFECTIVE_DATE);

        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;
		
		L_STATUS := PKG_LEASE_VAR_PAYMENTS.F_CALC_ALL_VAR_PAYMENTS(EFFECTIVE_DATE);
        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;

        L_STATUS := F_SAVE_SCHEDULES(EFFECTIVE_DATE);

        -- clear staging tables
        MSG := F_CLEAR_STAGING_TABLES;
         if MSG <> 'OK' then
           PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
         end if;

        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;


      end if;

      -- If there is a lookback period on the forecast version, we need to copy all
      -- of our ILR's and build the schedules for the lookback period as well
      if B_HAS_LOOKBACK then
        MSG := 'Deleting from LS_DEPR_FORECAST for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_DEPR_FORECAST
          where revision = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_ASSET_MAP for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_ASSET_MAP
          where revision = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_OPTIONS for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_OPTIONS
          where revision = LOOKBACK_REVISION;
		  
		MSG := 'Deleting from LS_ILR_RENEWALS_OPTIONS for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_RENEWAL_OPTIONS 
          where ILR_RENEWAL_ID IN (SELECT ILR_RENEWAL_ID FROM LS_ILR_RENEWAL
                                   WHERE REVISION = LOOKBACK_REVISION);
        
        MSG := 'Deleting from LS_ILR_RENEWALS for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);                           
        delete from LS_ILR_RENEWAL 
          WHERE REVISION = LOOKBACK_REVISION;
          
        MSG := 'Deleting from LS_ILR_PAYMENT_TERM_VAR_PAYMNT for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_PAYMENT_TERM_VAR_PAYMNT
          where revision = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_PAYMENT_TERM for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_PAYMENT_TERM
          where revision = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_APPROVAL for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_APPROVAL
          where revision = LOOKBACK_REVISION;

        MSG := 'Deleting from LS_ILR_AMOUNTS_SET_OF_BOOKS for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_ILR_AMOUNTS_SET_OF_BOOKS
          where revision = LOOKBACK_REVISION;

        -- Call PKG_LEASE_ILR.F_COPYREVISION(A_ILR_ID, Our master revision passed into the SSP, The lookback period revision)
        MSG := 'Copying ILRs to Lookback Revision';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        for i in (select distinct ilr_id from ls_forecast_ilr_master where revision = A_REVISION)
        loop
          L_RTN := PKG_LEASE_ILR.F_COPYREVISION(i.ilr_id, A_REVISION, LOOKBACK_REVISION);

          if L_RTN = -1 then
             return 'ERROR in PKG_LEASE_ILF.F_COPY_REVISION';
	        end if;
        end loop;

        MSG := 'Deleting from LS_FORECAST_ILR_MASTER for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        delete from LS_FORECAST_ILR_MASTER
          where revision = LOOKBACK_REVISION;

        -- Copy the ILRS into ls_forecast_ilr_master for the lookback_revision
        MSG := 'Copying the ILRs to LS_FORECAST_ILR_MASTER for the Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        insert into ls_forecast_ilr_master (revision, ilr_id, from_cap_type_id, to_cap_type_id)
        select LOOKBACK_REVISION, ilr_id, from_cap_type_id, to_cap_type_id
          from ls_forecast_ilr_master
         where revision = A_REVISION;

        -- Loop over our ls_forecast_ilr_master table again and call PKG_LEASE_SCHEDULE.F_LOAD_ILR_STG for the ILR and Lookback revision.
        MSG := 'Looping over LS_FORECAST_ILR_MASTER to load each ILR for Lookback Revision';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        for i in (select distinct ilr_id from ls_forecast_ilr_master where revision = LOOKBACK_REVISION)
        loop
          L_STATUS := F_LOAD_ILR_STG(i.ilr_id, LOOKBACK_REVISION);

          if L_STATUS <> 'OK' then
             return L_STATUS;
	        end if;
        end loop;

        -- Run an update statement on ls_ilr_stg and set the npv_start_date column equal to the effective month of the lookback revision.
        MSG := 'Updating ls_ilr_stg.npv_start_date for Lookback Revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        update ls_ilr_stg
           set npv_start_date = LOOKBACK_DATE
         where revision = LOOKBACK_REVISION;

        -- Call PKG_LEASE_SCHEDULE.F_CALC_SCHEDULE() -- This function does not take in any arguments and will process all ILR's that we staged
        MSG := 'Calling F_CALC_SCHEDULES for Lookback Revision';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        L_STATUS := F_CALC_SCHEDULES();

        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;

        if FORECAST_TYPE_ID = 2 then
          MSG := 'Loading Deferred Rent for Summary Forecast';
          PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
          L_STATUS := F_LOAD_DEFERRED_RENT(TRUE);

          if L_STATUS <> 'OK' then
            return L_STATUS;
          end if;
        end if;

        -- Call PKG_LEASE_SCHEDULE.F_PROCESS_ASSETS(Effective month of the lookback revision).
        MSG := 'Calling F_PROCESS_ASSETS for Lookback Revision';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        L_STATUS := F_PROCESS_ASSETS(LOOKBACK_DATE);

        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;

        -- Call PKG_LEASE_SCHEDULE.F_SAVE_SCHEDULES(Effective month of the lookback revision).
        MSG := 'Calling F_SAVE_SCHEDULES for Lookback Revision';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        L_STATUS := F_SAVE_SCHEDULES(LOOKBACK_DATE);

        -- clear the staging tables
        MSG := F_CLEAR_STAGING_TABLES;
         if MSG <> 'OK' then
           PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
         end if;

        if L_STATUS <> 'OK' then
          return L_STATUS;
        end if;


      end if;

      -- Insert into ls_summary_forecast table for both the forecast and lookback revision
      --  Forecast Revision
      MSG := 'Inserting into ls_summary_forecast for forecast revision: ';
      PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        msg:=f_populate_summary_forecast(A_REVISION, L_CUR_TYPE);
        if msg <> 'OK' then
          PKG_PP_LOG.P_WRITE_MESSAGE(msg);
          return msg;
        end if;

      --  Lookback Revision
      if not LOOKBACK_REVISION is null then
        MSG := 'Inserting into ls_summary_forecast for lookback revision: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        msg:=f_populate_summary_forecast(lookback_revision, L_CUR_TYPE);
        if msg <> 'OK' then
          PKG_PP_LOG.P_WRITE_MESSAGE(msg);
          return msg;
        end if;
      end if;

      -- Populate Actuals into the Forecast Summary Table
        MSG := 'Inserting into ls_summary_forecast for actuals: ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        msg:=f_populate_summary_forecast(A_REVISION, L_CUR_TYPE, 1);
        if msg <> 'OK' then
          PKG_PP_LOG.P_WRITE_MESSAGE(msg);
          return msg;
        end if;
        
        -- Update revision_built to 1 
        MSG := 'Update ls_forecast_revision.revision_built to 1 ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        
        update LS_FORECAST_VERSION
        set REVISION_BUILT = 1
        where REVISION = A_REVISION;

      return 'OK';

   EXCEPTION
      WHEN OTHERS THEN
        L_STATUS := 'ERROR: Building Forecast: '||MSG||sqlerrm;
        return L_STATUS;
   end F_BUILD_FORECAST;

   function F_BUILD_SUMMARY_FORECAST(A_REVISION number, A_RATE decimal) return varchar2 is
     MSG varchar2(2000);
     L_STATUS varchar2(2000);

     begin

     MSG := 'Starting : F_BUILD_SUMMARY_FORECAST ';
     PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
     for i in ( select to_cap_type_id, pre_payment_sw, ilr_id, company_id, revision
                   from (
                         select a.revision, ilr.ilr_id, a.to_cap_type_id, ll.pre_payment_sw, ilr.company_id, row_number() over(partition by a.to_cap_type_id, ll.pre_payment_sw, ilr.company_id order by ilr.ilr_id) the_row
                         from ls_forecast_ilr_master a, ls_ilr ilr, ls_lease ll
                         where a.revision = A_REVISION
                         and a.ilr_id = ilr.ilr_id
                         and ilr.lease_id = ll.lease_id)
                   where the_row =1  )
     loop
       --Merge into ls_ilr_payment_term the total payment amounts by month for each grouping
       MSG := 'Merge into ls_ilr_payment_term : ';

       merge into ls_ilr_payment_term a using (
            with N as (select ROWNUM as THE_ROW from DUAL connect by level <= 10000)
            select i.ilr_id as ilr_id, i.revision as revision, ADD_MONTHS(trunc( ilrpt.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1) as month,
            sum(case when mod(N.THE_ROW, DECODE(ilrpt.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)) =
                        DECODE(ilrpt.PAYMENT_FREQ_ID, 4, 0, ll.pre_payment_sw) then
                    ilrpt.PAID_AMOUNT
                   else
                    0
                end) as PAID_AMOUNT,
            (select max(payment_term_id) from ls_ilr_payment_term where ilr_id = i.ilr_id and revision = i.revision) as max_payment_term_id,
            row_number() over(order by ADD_MONTHS(trunc( ilrpt.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1)) adder
                from ls_forecast_ilr_master a, ls_ilr_payment_term ilrpt, ls_lease ll, ls_ilr ilr, n
                where a.ilr_id = ilrpt.ilr_id
			    and a.revision = i.revision
                and a.revision = ilrpt.revision
                and a.to_cap_type_id = i.to_cap_type_id
                and a.ilr_id = ilr.ilr_id
                and ilr.company_id = i.company_id
                and ilr.lease_id = ll.lease_id
                and ll.pre_payment_sw = i.pre_payment_sw
                and N.THE_ROW <= ilrpt.NUMBER_OF_TERMS * DECODE(ilrpt.PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 1)
                and ilrpt.payment_term_type_id = 2
                group by i.ilr_id, i.revision, ADD_MONTHS(trunc( ilrpt.PAYMENT_TERM_DATE, 'MM' ), THE_ROW - 1)
             ) b
        on (a.ilr_id = b.ilr_id and a.revision = b.revision and a.payment_term_date = b.month)
        when matched then update set payment_freq_id = 4 /* monthly */,
                             number_of_terms = 1,
                             paid_amount = b.paid_amount,
                             contingent_amount = 0
        when not matched then insert (ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, paid_amount)
                              values (b.ilr_id, b.revision, b.max_payment_term_id + adder, 2, b.month, 4 /* monthly */, 1, b.paid_amount);

       --Insert a $0 row for any months in the middle of the payment term that may not have had a payment
       MSG := 'Insert into ls_ilr_payment_term : ';
       insert into ls_ilr_payment_term
       (ilr_id, revision, payment_term_id, payment_term_type_id, payment_term_date, payment_freq_id, number_of_terms, paid_amount)
       select i.ilr_id,  i.revision,  max_payment_term + rownum, 2, month, 4, 1, 0
       from
          (with N as (select ROWNUM as THE_ROW from DUAL connect by level <= 10000)
             select add_months(min_date, the_row) month, max_payment_term
             from
             (select min(payment_term_date) min_date, max(payment_term_date) max_date, max(payment_term_id) max_payment_term
              from ls_ilr_payment_term ilrpt
               where ilr_id = i.ilr_id
               and revision = i.revision ), n
               where n.the_row < months_between(max_date, min_date)) a
       where not exists (select 1 from ls_ilr_payment_term where ilr_id = i.ilr_id and revision = i.revision and payment_term_date = a.month);

       --Re-Sort the ilr_payment_term table:
       MSG := 'Re sort - Merge into ls_ilr_payment_term : ';

       merge into ls_ilr_payment_term a using(
         select row_number() over(partition by ilr_id, revision order by payment_term_date) the_row, a.*
         from ls_ilr_payment_term a
         where ilr_id =  i.ilr_id
         and revision = i.revision
         ) b
       on (a.ilr_id = b.ilr_id and a.payment_term_date = b.payment_term_date and a.revision = b.revision)
       when matched then update set payment_term_id = b.the_row ;

       --Delete out the payment term records for the other ILR's that have now been consolidated into another ILR:
       MSG := 'Delete from ls_ilr_payment_term : ';

       delete from ls_ilr_payment_term
       where ilr_id <> i.ilr_id
       and (ilr_id, revision) in
         (select a.ilr_id, a.revision
          from ls_forecast_ilr_master a, ls_ilr ilr, ls_lease ll
          where a.ilr_id = ilr.ilr_id
            and ilr.lease_id = ll.lease_id
            and ll.pre_payment_sw =  i.pre_payment_sw
            and a.to_cap_type_id = i.to_cap_type_id
            and revision = i.revision
            and company_id = i.company_id);

     end loop;

     --Update ls_ilr_options.inception_air with passed in rate:
       MSG := 'Update ls_ilr_options.inception_air : ';
       update ls_ilr_options
       set inception_air = A_RATE
       where revision = A_REVISION;

     MSG := 'F_BUILD_SUMMARY_FORECAST complete successfully';
     PKG_PP_LOG.P_WRITE_MESSAGE(MSG);

     return 'OK';

     EXCEPTION
      WHEN OTHERS THEN
        L_STATUS := 'ERROR: Building Summary Forecast: '||MSG||sqlerrm;
        return L_STATUS;

    end F_BUILD_SUMMARY_FORECAST;

    function F_CONVERT_FORECAST(A_REVISION IN NUMBER) return varchar2 IS
      msg varchar(2000);
      new_revision number;
      l_return number;
      begin
        msg := 'Starting : F_CONVERT_FORECAST ';
        PKG_PP_LOG.P_WRITE_MESSAGE(MSG);
        for i in (select * from ls_forecast_ilr_master where revision = A_REVISION)
          loop
            new_revision := PKG_LEASE_ILR.F_COPYREVISION(i.ilr_id, A_REVISION);

            l_return := PKG_LEASE_CALC.F_SEND_ILR_NO_COMMIT(i.ilr_id, new_revision, MSG);
            if l_return = -1 then
              msg := 'Error: PKG_LEASE_CALC.F_SEND_ILR_NO_COMMIT: ' || msg ;
              return msg;
            end if;

            l_return := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(i.ilr_id, new_revision, MSG, TRUE);
            if l_return = -1 then
              msg := 'Error: PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT: ' || msg ;
              return msg;
            end if;
          end loop;

         MSG := 'F_CONVERT_FORECAST completed successfully';
         PKG_PP_LOG.P_WRITE_MESSAGE(MSG);

      return 'OK';

      EXCEPTION
      WHEN OTHERS THEN
        msg := 'ERROR: Convert Forecast to Service: '||MSG||sqlerrm;
        return msg;

      end F_CONVERT_FORECAST;

  function F_POPULATE_SUMMARY_FORECAST(A_REVISION IN NUMBER, A_CUR_TYPE IN number, A_ACTUALS IN number:=null) return varchar2
  is
  msg varchar2(5000);
  begin
  
  msg:='Inserting into ls_summary_forecast for revision ' || a_revision;
    pkg_pp_log.p_write_message(msg);
  insert into ls_summary_forecast (
             COMPANY_ID, REVISION, SET_OF_BOOKS_ID, "MONTH", TYPE_A_EXPENSE, TYPE_B_EXPENSE, RESIDUAL_AMOUNT, TERM_PENALTY,
             BPO_PRICE, BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION,
             INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
             EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
             EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
             EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
             CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
             CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4,
             CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, current_lease_cost,
             begin_reserve, end_reserve, is_actuals, rental_expense, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, 
             BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT)
	SELECT
		company_id,
		revision,
		set_of_books_id,
		MONTH,
		Sum(type_a_expense) type_a_expense,
		Sum(type_b_expense) type_b_expense,
		Sum(residual_amount) residual_amount,
		Sum(term_penalty) term_penalty,
		Sum(bpo_price) bpo_price,
		Sum(beg_capital_cost) beg_capital_cost,
		Sum(end_capital_cost) end_capital_cost,
		Sum(beg_obligation) beg_obligation,
		Sum(end_obligation) end_obligation,
		Sum(beg_lt_obligation) beg_lt_obligation,
		Sum(end_lt_obligation) end_lt_obligation,
		Sum(interest_accrual) interest_accrual,
		Sum(principal_accrual) principal_accrual,
		Sum(interest_paid) interest_paid,
		Sum(principal_paid) principal_paid,
		Sum(executory_accrual1) executory_accrual1,
		Sum(executory_accrual2) executory_accrual2,
		Sum(executory_accrual3) executory_accrual3,
		Sum(executory_accrual4) executory_accrual4,
		Sum(executory_accrual5) executory_accrual5,
		Sum(executory_accrual6) executory_accrual6,
		Sum(executory_accrual7) executory_accrual7,
		Sum(executory_accrual8) executory_accrual8,
		Sum(executory_accrual9) executory_accrual9,
		Sum(executory_accrual10) executory_accrual10,
		Sum(executory_paid1) executory_paid1,
		Sum(executory_paid2) executory_paid2,
		Sum(executory_paid3) executory_paid3,
		Sum(executory_paid4) executory_paid4,
		Sum(executory_paid5) executory_paid5,
		Sum(executory_paid6) executory_paid6,
		Sum(executory_paid7) executory_paid7,
		Sum(executory_paid8) executory_paid8,
		Sum(executory_paid9) executory_paid9,
		Sum(executory_paid10) executory_paid10,
		Sum(contingent_accrual1) contingent_accrual1,
		Sum(contingent_accrual2) contingent_accrual2,
		Sum(contingent_accrual3) contingent_accrual3,
		Sum(contingent_accrual4) contingent_accrual4,
		Sum(contingent_accrual5) contingent_accrual5,
		Sum(contingent_accrual6) contingent_accrual6,
		Sum(contingent_accrual7) contingent_accrual7,
		Sum(contingent_accrual8) contingent_accrual8,
		Sum(contingent_accrual9) contingent_accrual9,
		Sum(contingent_accrual10) contingent_accrual10,
		Sum(contingent_paid1) contingent_paid1,
		Sum(contingent_paid2) contingent_paid2,
		Sum(contingent_paid3) contingent_paid3,
		Sum(contingent_paid4) contingent_paid4,
		Sum(contingent_paid5) contingent_paid5,
		Sum(contingent_paid6) contingent_paid6,
		Sum(contingent_paid7) contingent_paid7,
		Sum(contingent_paid8) contingent_paid8,
		Sum(contingent_paid9) contingent_paid9,
		Sum(contingent_paid10) contingent_paid10,
		Sum(current_lease_cost) current_lease_cost,
		Sum(begin_reserve) begin_reserve,
		Sum(end_reserve) end_reserve,
		is_actuals,
        Sum(rental_expense) rental_expense,
        Sum(beg_deferred_rent),
        Sum(deferred_rent),
        Sum(end_deferred_rent),
        Sum(beg_st_deferred_rent),
        Sum(end_st_deferred_rent)
	FROM
	(
		SELECT
			a.company_id,
			CASE
				WHEN A_ACTUALS = 1 THEN A_REVISION
				ELSE a.revision
			END
			revision,
             a.set_of_books_id,
			a.MONTH,
			CASE
				WHEN a.is_om = 1 THEN 0
				ELSE Nvl(
					Decode(
						ls_map.fasb_cap_type_id,
						1,
						Nvl(a.interest_accrual,0) + Nvl(a.depr_expense,0) + Nvl(a.depr_exp_alloc_adjust,0)
					),
					0
				)
				END
			AS type_a_expense,
			CASE
				WHEN a.is_om = 1 THEN 0
				ELSE Nvl(
					Decode(
						ls_map.fasb_cap_type_id,
						2,
						Nvl(a.interest_accrual,0) + Nvl(a.depr_expense,0) + Nvl(a.depr_exp_alloc_adjust,0)
					),
					0
				)
			END AS type_b_expense,
			Nvl(a.residual_amount,0) residual_amount,
			Nvl(a.term_penalty,0) term_penalty,
			Nvl(a.bpo_price,0) bpo_price,
			Nvl(a.beg_capital_cost,0) beg_capital_cost,
			Nvl(a.end_capital_cost,0) end_capital_cost,
			CASE
				WHEN a.is_om = 0 THEN Nvl(a.beg_obligation,0)
				ELSE 0
			END beg_obligation,
			CASE
				WHEN a.is_om = 0 THEN Nvl(a.end_obligation,0)
				ELSE 0
			END end_obligation,
			CASE
				WHEN a.is_om = 0 THEN Nvl(a.beg_lt_obligation,0)
				ELSE 0
			END beg_lt_obligation,
			CASE
				WHEN a.is_om = 0 THEN Nvl(a.end_lt_obligation,0)
				ELSE 0
			END end_lt_obligation,
			CASE
				WHEN a.is_om = 0 THEN Nvl(a.interest_accrual,0)
				ELSE 0
			END interest_accrual,
			Nvl(a.principal_accrual,0) principal_accrual,
			Nvl(a.interest_paid,0) interest_paid,
			Nvl(a.principal_paid,0) principal_paid,
			Nvl(a.executory_accrual1,0) executory_accrual1,
			Nvl(a.executory_accrual2,0) executory_accrual2,
			Nvl(a.executory_accrual3,0) executory_accrual3,
			Nvl(a.executory_accrual4,0) executory_accrual4,
			Nvl(a.executory_accrual5,0) executory_accrual5,
			Nvl(a.executory_accrual6,0) executory_accrual6,
			Nvl(a.executory_accrual7,0) executory_accrual7,
			Nvl(a.executory_accrual8,0) executory_accrual8,
			Nvl(a.executory_accrual9,0) executory_accrual9,
			Nvl(a.executory_accrual10,0) executory_accrual10,
			Nvl(a.executory_paid1,0) executory_paid1,
			Nvl(a.executory_paid2,0) executory_paid2,
			Nvl(a.executory_paid3,0) executory_paid3,
			Nvl(a.executory_paid4,0) executory_paid4,
			Nvl(a.executory_paid5,0) executory_paid5,
			Nvl(a.executory_paid6,0) executory_paid6,
			Nvl(a.executory_paid7,0) executory_paid7,
			Nvl(a.executory_paid8,0) executory_paid8,
			Nvl(a.executory_paid9,0) executory_paid9,
			Nvl(a.executory_paid10,0) executory_paid10,
			Nvl(a.contingent_accrual1,0) contingent_accrual1,
			Nvl(a.contingent_accrual2,0) contingent_accrual2,
			Nvl(a.contingent_accrual3,0) contingent_accrual3,
			Nvl(a.contingent_accrual4,0) contingent_accrual4,
			Nvl(a.contingent_accrual5,0) contingent_accrual5,
			Nvl(a.contingent_accrual6,0) contingent_accrual6,
			Nvl(a.contingent_accrual7,0) contingent_accrual7,
			Nvl(a.contingent_accrual8,0) contingent_accrual8,
			Nvl(a.contingent_accrual9,0) contingent_accrual9,
			Nvl(a.contingent_accrual10,0) contingent_accrual10,
			Nvl(a.contingent_paid1,0) contingent_paid1,
			Nvl(a.contingent_paid2,0) contingent_paid2,
			Nvl(a.contingent_paid3,0) contingent_paid3,
			Nvl(a.contingent_paid4,0) contingent_paid4,
			Nvl(a.contingent_paid5,0) contingent_paid5,
			Nvl(a.contingent_paid6,0) contingent_paid6,
			Nvl(a.contingent_paid7,0) contingent_paid7,
			Nvl(a.contingent_paid8,0) contingent_paid8,
			Nvl(a.contingent_paid9,0) contingent_paid9,
			Nvl(a.contingent_paid10,0) contingent_paid10,
			Nvl(a.current_lease_cost,0) current_lease_cost,
			Nvl(a.begin_reserve,0) begin_reserve,
			Nvl(a.end_reserve,0) end_reserve,
			Nvl(A_ACTUALS,0) is_actuals,
			CASE
				WHEN a.is_om = 1 THEN Nvl(a.interest_accrual,0)
				ELSE 0
            END AS rental_expense,
            Nvl(a.BEG_DEFERRED_RENT,0) beg_deferred_rent,
            Nvl(a.DEFERRED_RENT,0) deferred_rent,
            Nvl(a.END_DEFERRED_RENT,0) end_deferred_rent,
            Nvl(a.BEG_ST_DEFERRED_RENT,0) beg_st_deferred_rent,
            Nvl(a.END_ST_DEFERRED_RENT,0) end_st_deferred_rent
		FROM
			v_ls_asset_schedule_fx_vw a,
			ls_ilr ilr,
			ls_ilr_options ilro,
			ls_lease_cap_type lct,
			ls_forecast_ilr_master master,
			ls_fasb_cap_type_sob_map ls_map
		WHERE a.revision =
			CASE
				WHEN A_ACTUALS = 1 THEN a.approved_revision
				ELSE A_REVISION
			END
		AND a.ilr_id = ilro.ilr_id
		AND a.ilr_id = ilr.ilr_id
		AND ilro.revision =
			CASE
				WHEN A_ACTUALS = 1 THEN ilr.current_revision
				ELSE A_REVISION
			END
		AND ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
		AND lct.ls_lease_cap_type_id = ls_map.lease_cap_type_id
		AND ls_map.set_of_books_id = a.set_of_books_id
		AND a.set_of_books_id IN (
			SELECT Decode(
					set_of_books_id,
					-1,
					a.set_of_books_id,
					set_of_books_id
				)
			FROM ls_forecast_version
			WHERE revision = A_REVISION
		)
		AND a.ilr_id = master.ilr_id
		AND master.revision = A_REVISION
		AND a.ls_cur_type = A_CUR_TYPE
	) x
	GROUP BY company_id, revision, set_of_books_id, month, is_actuals;


  return 'OK';
  exception when others then
    return 'ERROR: Populating Summary Forecast: '||MSG||' ' || sqlerrm;
  end F_POPULATE_SUMMARY_FORECAST;

 function F_CLEAR_STAGING_TABLES return varchar2
    is
    L_STATUS                      varchar2(2000);

  begin

      L_STATUS := 'CLEARING OUT ls_ilr_asset_schedule_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_ASSET_SCHEDULE_STG;

      L_STATUS := 'CLEARING OUT ls_ilr_asset_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_ASSET_STG;

      L_STATUS := 'CLEARING OUT ls_ilr_schedule_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_SCHEDULE_STG;

      L_STATUS := 'CLEARING OUT ls_ilr_stg';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_ILR_STG;

      return 'OK';

  exception when others then
    return 'ERROR: Clearing staging tables: ' || sqlerrm;

  end F_CLEAR_STAGING_TABLES;

   --**************************************************************************
   --                            F_LOAD_DEFERRED_RENT
   -- Load deferred rent balance onto the schedule building tables
   -- If this is a summary forecast, need to load the deferred rent appropriately
   --**************************************************************************
   function F_LOAD_DEFERRED_RENT(A_SUMM_FCST IN BOOLEAN) return varchar2 is
      L_MSG    varchar2(2000);
      L_STATUS varchar2(2000);

   begin

      if not A_SUMM_FCST then

        L_STATUS := 'Loading Deferred Rent';
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
        update LS_ILR_STG STG
        set DEFERRED_RENT_BALANCE = nvl((
          select BEG_DEFERRED_RENT
		      from LS_ILR_SCHEDULE S, LS_ILR ILR
		      where S.ILR_ID = ILR.ILR_ID
		      and ILR.CURRENT_REVISION = S.REVISION
		      and S.ILR_ID = STG.ILR_ID
		      and S.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
		      and S.MONTH = STG.NPV_START_DATE), 0);

      else

        -- we've already loaded in deferred rent for each ILR in the forecast,
        --so pull them onto the ILR that's being used for summarization (has payment term records)
        L_STATUS := 'Loading Summary Forecast Deferred Rent';
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
        update LS_ILR_STG STG
        set DEFERRED_RENT_BALANCE = nvl((
          select DEFERRED_RENT
          from
            (select S.REVISION, S.SET_OF_BOOKS_ID, ILR.COMPANY_ID, ILRO.LEASE_CAP_TYPE_ID,
                S.PREPAY_SWITCH, sum(DEFERRED_RENT_BALANCE) DEFERRED_RENT
            from LS_ILR_STG S, LS_ILR ILR, LS_ILR_OPTIONS ILRO
            where S.ILR_ID = ILR.ILR_ID
            and S.ILR_ID = ILRO.ILR_ID
            and S.REVISION = ILRO.REVISION
            group by S.REVISION, S.SET_OF_BOOKS_ID, ILR.COMPANY_ID,
                ILRO.LEASE_CAP_TYPE_ID, S.PREPAY_SWITCH) AMTS,
            (select S.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, ILR.COMPANY_ID,
                ILRO.LEASE_CAP_TYPE_ID, S.PREPAY_SWITCH
            from LS_ILR_STG S, LS_ILR ILR, LS_ILR_OPTIONS ILRO, LS_ILR_PAYMENT_TERM PT
            where S.ILR_ID = ILR.ILR_ID
            and S.ILR_ID = ILRO.ILR_ID
            and S.REVISION = ILRO.REVISION
            and S.ILR_ID = PT.ILR_ID
            and S.REVISION = PT.REVISION
            and PT.PAYMENT_TERM_ID = 1) ILRS
          where AMTS.REVISION = ILRS.REVISION
          and AMTS.SET_OF_BOOKS_ID = ILRS.SET_OF_BOOKS_ID
          and AMTS.COMPANY_ID = ILRS.COMPANY_ID
          and AMTS.LEASE_CAP_TYPE_ID = ILRS.LEASE_CAP_TYPE_ID
          and AMTS.PREPAY_SWITCH = ILRS.PREPAY_SWITCH
          and ILRS.ILR_ID = STG.ILR_ID
          and ILRS.REVISION = STG.REVISION
          and ILRS.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
          ), 0);

      end if;

      return 'OK';

  exception when others then
    return 'ERROR: Loading deferred rent: ' || sqlerrm;

  end F_LOAD_DEFERRED_RENT;
  -- KRD 4/6/17 - Function to extend schedules for assets with BPOs out to the last month of the depreciable life
  -- KRD 10/30/17 - Copy and pasted the function from Jared's package in here
  FUNCTION F_BPO_SCHEDULE_EXTEND (
                                  A_LS_ASSET_ID IN NUMBER,
                                  A_ILR_ID      IN NUMBER,
                                  A_REVISION    IN NUMBER
                                 )
  RETURN VARCHAR2
  IS

  L_STATUS              VARCHAR2(1000);
  L_MAX_MONTH           DATE;
  L_MID_PERIOD_CONV     NUMBER;
  L_BPO_CHECK			NUMBER;

  BEGIN


    L_STATUS := 'Checking for purchase option';
	select PURCHASE_OPTION_TYPE_ID
	into L_BPO_CHECK
	from LS_ILR_OPTIONS
	where ILR_ID = A_ILR_ID
	and REVISION = A_REVISION;

    L_STATUS := 'Extending schedules for Asset: ' || A_LS_ASSET_ID || '  ILR: ' || A_ILR_ID || '  Revision: ' || A_REVISION;
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);


    L_STATUS := 'Calculating final month on amortization schedule';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

-- CJS 9/28/17 Need month - 1 for max month
-- CJS 10/3/17 Account for both purchase option extension and just MPM extension
-- KRD 12/11/17 - Change logic to use max Payment Start Date + Number of Months instead of pulling the current
--                  maximum schedule month which may have already been extended
    SELECT Decode(L_BPO_CHECK, 1, z.last_month, Add_Months(z.first_month, la.economic_life - 1))
    INTO L_MAX_MONTH
    FROM ls_asset la,
        (
          SELECT la.ls_asset_id,
                term.payment_term_date AS first_month,
                Add_Months(term2.payment_term_date, term2.number_of_terms * Decode(term2.payment_freq_id,1,12,2,6,3,3,1) - 1) AS last_month
          FROM ls_asset la,
              ls_ilr_payment_term term,
              ls_ilr_payment_term term2
          WHERE la.ilr_id = term.ilr_id
          AND   la.ilr_id = term2.ilr_id
          AND   la.ls_asset_id = A_LS_ASSET_ID
          AND   term.revision = A_REVISION
          AND   term2.revision = A_REVISION
          AND   term.payment_term_id = (SELECT Min(payment_term_id) FROM ls_ilr_payment_term WHERE ilr_id = term.ilr_id)
          AND   term2.payment_term_id = (SELECT Max(payment_term_id) FROM ls_ilr_payment_term WHERE ilr_id = term.ilr_id)
        ) z
    WHERE la.ls_asset_id = z.ls_asset_id;

    L_STATUS := 'Getting Mid Period Convention on Asset';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

	SELECT To_number( Nvl( mid_period_conv, -1 ) )
	INTO   l_mid_period_conv
	FROM   ( SELECT Nvl( dg.mid_period_conv, -1 ) mid_period_conv
			 FROM   LS_ASSET la,
					DEPR_GROUP dg
			 WHERE  la.depr_group_id = dg.depr_group_id AND
					la.ls_asset_id = a_ls_asset_id AND
					la.depr_group_id IS NOT NULL
			 UNION
			 SELECT 1
			 FROM   LS_ASSET
			 WHERE  depr_group_id IS NULL AND
					ls_asset_id = a_ls_asset_id ); 

    if L_BPO_CHECK = 1 and L_MID_PERIOD_CONV in (-1,1) then
        return 'OK';
    end if;

    L_STATUS := 'Inserting new schedule rows';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

-- KRD 4/17/17 - Adding decode over mid period conv to determine if we add 1 extra month at the end
--               NVL will handle cases where this function is called from F_SAVE_SCHEDULES, otherwise it will be called again
--                from F_ADD_ASSET after the DG has been found
    INSERT INTO LS_ASSET_SCHEDULE (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST, END_CAPITAL_COST,
                                  BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID,
                                  PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
                                  EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4,
                                  EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
                                  CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9,
                                  CONTINGENT_ACCRUAL10, CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7,
                                  CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, IS_OM, CURRENT_LEASE_COST,
                                  BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT)
      SELECT las.ls_asset_id as LS_ASSET_ID,
            las.revision as REVISION,
            las.set_of_books_id as SET_OF_BOOKS_ID,
            add_months(las.month, n.the_row) as MONTH,
            0 as RESIDUAL_AMOUNT,
            0 as TERM_PENALTY,
            0 as BPO_PRICE,
            las.END_CAPITAL_COST as BEG_CAPITAL_COST,
            las.END_CAPITAL_COST as END_CAPITAL_COST,
            0 as BEG_OBLIGATION,
            0 as END_OBLIGATION,
            0 as BEG_LT_OBLIGATION,
            0 as END_LT_OBLIGATION,
            0 as INTEREST_ACCRUAL,
            0 as PRINCIPAL_ACCRUAL,
            0 as INTEREST_PAID,
            0 as PRINCIPAL_PAID,
            0 as EXECUTORY_ACCRUAL1,
            0 as EXECUTORY_ACCRUAL2,
            0 as EXECUTORY_ACCRUAL3,
            0 as EXECUTORY_ACCRUAL4,
            0 as EXECUTORY_ACCRUAL5,
            0 as EXECUTORY_ACCRUAL6,
            0 as EXECUTORY_ACCRUAL7,
            0 as EXECUTORY_ACCRUAL8,
            0 as EXECUTORY_ACCRUAL9,
            0 as EXECUTORY_ACCRUAL10,
            0 as EXECUTORY_PAID1,
            0 as EXECUTORY_PAID2,
            0 as EXECUTORY_PAID3,
            0 as EXECUTORY_PAID4,
            0 as EXECUTORY_PAID5,
            0 as EXECUTORY_PAID6,
            0 as EXECUTORY_PAID7,
            0 as EXECUTORY_PAID8,
            0 as EXECUTORY_PAID9,
            0 as EXECUTORY_PAID10,
            0 as CONTINGENT_ACCRUAL1,
            0 as CONTINGENT_ACCRUAL2,
            0 as CONTINGENT_ACCRUAL3,
            0 as CONTINGENT_ACCRUAL4,
            0 as CONTINGENT_ACCRUAL5,
            0 as CONTINGENT_ACCRUAL6,
            0 as CONTINGENT_ACCRUAL7,
            0 as CONTINGENT_ACCRUAL8,
            0 as CONTINGENT_ACCRUAL9,
            0 as CONTINGENT_ACCRUAL10,
            0 as CONTINGENT_PAID1,
            0 as CONTINGENT_PAID2,
            0 as CONTINGENT_PAID3,
            0 as CONTINGENT_PAID4,
            0 as CONTINGENT_PAID5,
            0 as CONTINGENT_PAID6,
            0 as CONTINGENT_PAID7,
            0 as CONTINGENT_PAID8,
            0 as CONTINGENT_PAID9,
            0 as CONTINGENT_PAID10,
            LAS.IS_OM as IS_OM,
            las.CURRENT_LEASE_COST as CURRENT_LEASE_COST,
			0 as BEG_DEFERRED_RENT,
			0 as DEFERRED_RENT,
			0 as END_DEFERRED_RENT,
			0 as BEG_ST_DEFERRED_RENT,
			0 as END_ST_DEFERRED_RENT
      FROM ls_asset la,
           (
            SELECT la.ls_asset_id, las.revision, las.set_of_books_id, las.is_om, las.end_capital_cost, las.current_lease_cost, Max(las.month) month,
                row_number() over (partition by la.ls_asset_id, las.revision, las.set_of_books_id order by max(las.month) desc) the_row2
            FROM ls_asset_schedule las,
                 ls_asset la
            WHERE la.ls_asset_id = las.ls_asset_id
            AND las.revision = A_REVISION
            AND la.ls_asset_id = A_LS_ASSET_ID
            GROUP BY la.ls_asset_id, las.revision, las.set_of_books_id, las.is_om, las.end_capital_cost, las.current_lease_cost
           ) las,
           (
            SELECT ROWNUM AS the_row
            FROM dual
            CONNECT BY LEVEL <= 10000
           ) n
      WHERE la.ls_asset_id = las.ls_asset_id
	  AND   las.the_row2 = 1
      AND   Add_Months(las.MONTH, n.the_row) <= Decode(L_MID_PERIOD_CONV, 1, L_MAX_MONTH, -1, L_MAX_MONTH, Add_Months(L_MAX_MONTH,1));

    PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted to LS_ASSET_SCHEDULE');

    INSERT INTO ls_ilr_Schedule (ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST,
                                END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, INTEREST_ACCRUAL,
                                PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
                                EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7,
                                EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2,
                                EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
                                EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3,
                                CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
                                CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3,
                                CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9,
                                CONTINGENT_PAID10, IS_OM, CURRENT_LEASE_COST,
                                BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT)
      SELECT ilrs.ilr_id,
            ilrs.revision,
            ilrs.set_of_books_id,
            add_months(ilrs.month, n.the_row) as MONTH,
            0 as RESIDUAL_AMOUNT,
            0 as TERM_PENALTY,
            0 as BPO_PRICE,
            ilrs.END_CAPITAL_COST as BEG_CAPITAL_COST,
            ilrs.END_CAPITAL_COST as END_CAPITAL_COST,
            0 as BEG_OBLIGATION,
            0 as END_OBLIGATION,
            0 as BEG_LT_OBLIGATION,
            0 as END_LT_OBLIGATION,
            0 as INTEREST_ACCRUAL,
            0 as PRINCIPAL_ACCRUAL,
            0 as INTEREST_PAID,
            0 as PRINCIPAL_PAID,
            0 as EXECUTORY_ACCRUAL1,
            0 as EXECUTORY_ACCRUAL2,
            0 as EXECUTORY_ACCRUAL3,
            0 as EXECUTORY_ACCRUAL4,
            0 as EXECUTORY_ACCRUAL5,
            0 as EXECUTORY_ACCRUAL6,
            0 as EXECUTORY_ACCRUAL7,
            0 as EXECUTORY_ACCRUAL8,
            0 as EXECUTORY_ACCRUAL9,
            0 as EXECUTORY_ACCRUAL10,
            0 as EXECUTORY_PAID1,
            0 as EXECUTORY_PAID2,
            0 as EXECUTORY_PAID3,
            0 as EXECUTORY_PAID4,
            0 as EXECUTORY_PAID5,
            0 as EXECUTORY_PAID6,
            0 as EXECUTORY_PAID7,
            0 as EXECUTORY_PAID8,
            0 as EXECUTORY_PAID9,
            0 as EXECUTORY_PAID10,
            0 as CONTINGENT_ACCRUAL1,
            0 as CONTINGENT_ACCRUAL2,
            0 as CONTINGENT_ACCRUAL3,
            0 as CONTINGENT_ACCRUAL4,
            0 as CONTINGENT_ACCRUAL5,
            0 as CONTINGENT_ACCRUAL6,
            0 as CONTINGENT_ACCRUAL7,
            0 as CONTINGENT_ACCRUAL8,
            0 as CONTINGENT_ACCRUAL9,
            0 as CONTINGENT_ACCRUAL10,
            0 as CONTINGENT_PAID1,
            0 as CONTINGENT_PAID2,
            0 as CONTINGENT_PAID3,
            0 as CONTINGENT_PAID4,
            0 as CONTINGENT_PAID5,
            0 as CONTINGENT_PAID6,
            0 as CONTINGENT_PAID7,
            0 as CONTINGENT_PAID8,
            0 as CONTINGENT_PAID9,
            0 as CONTINGENT_PAID10,
            ilrs.is_om,
            ilrs.current_lease_cost,
			0 as BEG_DEFERRED_RENT,
			0 as DEFERRED_RENT,
			0 as END_DEFERRED_RENT,
			0 as BEG_ST_DEFERRED_RENT,
			0 as END_ST_DEFERRED_RENT
      FROM (
            SELECT ilr_id, revision, set_of_books_id, is_om, end_capital_cost, current_lease_cost, Max(MONTH) AS MONTH,
                row_number() over (partition by ilr_id, revision, set_of_books_id order by max(month) desc) the_row2
            FROM ls_ilr_schedule
            WHERE revision = A_REVISION
            AND   ilr_id = A_ILR_ID
            GROUP BY ilr_id, revision, set_of_books_id, is_om, end_capital_cost, current_lease_cost
           ) ilrs,
           (
            SELECT ROWNUM AS the_row
            FROM dual CONNECT BY LEVEL <= 10000
           ) n
      WHERE Add_Months(ilrs.MONTH, n.the_row) <= Decode(L_MID_PERIOD_CONV, 1, L_MAX_MONTH, -1, L_MAX_MONTH, Add_Months(L_MAX_MONTH,1))
      AND   ilrs.the_row2 = 1;


    PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted to LS_ILR_SCHEDULE');

    PKG_PP_LOG.P_WRITE_MESSAGE('Successfully extended schedules');

  RETURN 'OK';
    EXCEPTION WHEN OTHERS THEN
    RETURN 'SQL ERROR AT LOCATION: '||L_STATUS||' - ' || sqlerrm;

  END F_BPO_SCHEDULE_EXTEND;


end;
/ 

--**************************   
-- Log the run of the script   
--**************************  .  

insert into PP_SCHEMA_CHANGE_LOG      
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,       
	 SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)   
values      
	(4206, 0, 2017, 2, 0, 0, 0, 'C:\PlasticWKS\PowerPlant\sql\packages', '2017.2.0.0_PKG_LEASE_SCHEDULE.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));   
commit; 
