create or replace PACKAGE pkg_lessor_schedule AS
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LESSOR_SCHEDULE
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version     Date       Revised By     Reason for Change
   || ----------  ---------- -------------- ----------------------------------------
   || 2017.1.0.0  09/12/2017 A. Hill        Original Version
   || 2017.1.0.0  10/05/2017 A. Hill        Add Sales Type Calculation Logic
   ||==================================================================================================================================================
   */


  /*****************************************************************************
  * PROCEDURE: p_process_ilr
  * PURPOSE: Processes the Lessor ILR Schedule for the given ILR/Revision
  * PARAMETERS:
  *   a_ilr_id: ilr_id of the ILR for which to process the schedule
  *   a_revision: Revision of the ILR for which to process the schedule
  ******************************************************************************/

  PROCEDURE p_process_ilr(a_ilr_id NUMBER,
                          a_revision NUMBER);

  /*****************************************************************************
  * Function: f_build_op_schedule
  * PURPOSE: Builds the operating schedule for the given payment terms
  * PARAMETERS:
  *   a_payment_terms: The payment terms associated with this ILR. The schedule will
  *                     build for payment_month_frequency (i.e. the number of months between payments),
  *                               payment_term_start_date (i.e. the starting date of the payment term),
  *                               number_of_terms (i.e. the number of payments that will be made),
  *                               payment_amount (i.e. the amount of payment to apply)
  *                               is_prepay (0 = arrears / 1 = prepay)
  *   a_initial_direct_costs: The Initial Direct Costs associated with this ILR
  *   NOTE: Multiple payment terms can be defined. For example, for a 36 month, prepay monthly lease,
  *           with $500 payments in year 1, $550 payments in year 2, and $600 payments in year three,
  *           provide three payment terms (1, <year_1_start>, 12, 500, 1),
  *                                       (1, <year_2_start>, 12, 550, 1),
  *                                       (1, <year_3_start>, 12, 600, 1)
  *
  * RETURNS: Table with schedule results (pipelined)
  ******************************************************************************/
  function f_build_op_schedule(a_payment_terms lsr_ilr_op_sch_pay_term_tab, a_initial_direct_costs lsr_init_direct_cost_info_tab) return lsr_ilr_op_sch_result_tab pipelined;

  /*****************************************************************************
  * Function: f_get_op_schedule
  * PURPOSE: Builds and returns the operating schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule. The schedule will
  *             build for payment payment terms given in table lsr_ilr_payment_term
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule results (pipelined)
  ******************************************************************************/

  function f_get_op_schedule(a_ilr_id number, a_revision number) return lsr_ilr_op_sch_result_tab pipelined;

  /*****************************************************************************
  * Function: f_get_payment_terms
  * PURPOSE: Looks up and returns the payment terms for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve payment terms
  *   a_revision: The revision of the ILR for which to retrieve payment terms
  *
  * RETURNS: Table with payment terms (pipelined)
  ******************************************************************************/

  FUNCTION f_get_payment_terms(a_ilr_id NUMBER, a_revision NUMBER) RETURN lsr_ilr_op_sch_pay_term_tab PIPELINED;

  /*****************************************************************************
  * Function: f_get_initial_direct_costs
  * PURPOSE: Looks up and returns the initial direct costs for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve payment terms
  *   a_revision: The revision of the ILR for which to retrieve payment terms
  *
  * RETURNS: Table with initial direct costs (pipelined)
  ******************************************************************************/

  FUNCTION f_get_initial_direct_costs(a_ilr_id NUMBER, a_revision NUMBER) RETURN lsr_init_direct_cost_info_tab PIPELINED;

  /*****************************************************************************
  * Function: f_get_sales_type_info
  * PURPOSE: Looks up and returns information necessary to complete the building of sales-type schedules
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve infomration
  *   a_revision: The revision of the ILR for which to retrieve information
  *
  * RETURNS: Table with sales-type information (pipelined)
  ******************************************************************************/

  function f_get_sales_type_info(a_ilr_id number, a_revision number) return lsr_ilr_sales_sch_info_tab pipelined;

  /*****************************************************************************
  * Function: f_get_payment_info_from_terms
  * PURPOSE: Transforms payment terms into a month-by-month listing of payment information
  * PARAMETERS:
  *   a_payment_terms: The payment terms for which to generate payment info
  *
  * RETURNS: Table with payment information (pipelined)
  ******************************************************************************/

  FUNCTION f_get_payment_info_from_terms(a_payment_terms lsr_ilr_op_sch_pay_term_tab) RETURN lsr_ilr_op_sch_pay_info_tab PIPELINED;

  /*****************************************************************************
  * Function: f_get_payments_from_info
  * PURPOSE: Transforms month-by-month payment information listing into month-by-month list of calculated payment amounts
  * PARAMETERS:
  *   a_payment_info: The payment information for which to calculate payment amounts
  *
  * RETURNS: Table with calculated payment amounts (pipelined)
  ******************************************************************************/

  FUNCTION f_get_payments_from_info(a_payment_info lsr_ilr_op_sch_pay_info_tab) RETURN lsr_schedule_payment_def_tab PIPELINED;

  /*****************************************************************************
  * Function: f_calculate_buckets
  * PURPOSE: Calculates executory and contingent bucket accruals and payments
  * PARAMETERS:
  *   a_payment_terms: The payment terms for which to process buckets
  *
  * RETURNS: Table with bucket information (pipelined)
  ******************************************************************************/

  FUNCTION f_calculate_buckets(payment_info lsr_ilr_op_sch_pay_info_tab) RETURN lsr_bucket_result_tab PIPELINED;

  /*****************************************************************************
  * Function: f_build_sales_schedule
  * PURPOSE: Builds the sales-type schedule for the given payment terms and sales-type-specific information
  * PARAMETERS:
  *   a_payment_terms: The payment terms to use in generating the schedule
  *   a_initial_direct_costs: The Initial Direct Costs associated with this ILR
  *   a_sales_type_info: The sales-type-specific info to use in generating the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/

  FUNCTION f_build_sales_schedule(a_payment_terms lsr_ilr_op_sch_pay_term_tab, a_initial_direct_costs lsr_init_direct_cost_info_tab, a_sales_type_info lsr_ilr_sales_sch_info_tab) RETURN lsr_ilr_sales_sch_result_tab PIPELINED;

  /*****************************************************************************
  * Function: f_get_sales_schedule
  * PURPOSE: Builds and returns the operating schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/

  FUNCTION f_get_sales_schedule(a_ilr_id NUMBER, a_revision NUMBER) RETURN lsr_ilr_sales_sch_result_tab PIPELINED;

END pkg_lessor_schedule;
/

create or replace PACKAGE BODY pkg_lessor_schedule AS

  /*****************************************************************************
  * PROCEDURE: p_process_op_ilr
  * PURPOSE: Processes an operating-type ILR Schedule for the given ILR/Revision
  * PARAMETERS:
  *   a_ilr_id: ilr_id of the ILR for which to process the schedule
  *   a_revision: Revision of the ILR for which to process the schedule
  ******************************************************************************/

  PROCEDURE p_process_op_ilr( a_ilr_id NUMBER,
                              a_revision NUMBER)
  IS
  BEGIN
    insert into lsr_ilr_schedule( ilr_id,
                                  revision,
                                  set_of_books_id,
                                  MONTH,
                                  interest_income_received,
                                  interest_income_accrued,
                                  interest_rental_recvd_spread,
                                  beg_deferred_rev,
                                  deferred_rev_activity,
                                  end_deferred_rev,
                                  beg_receivable,
                                  end_receivable,
                                  beg_lt_receivable,
                                  end_lt_receivable,
                                  initial_direct_cost,
                                  executory_accrual1,
                                  executory_accrual2,
                                  executory_accrual3,
                                  executory_accrual4,
                                  executory_accrual5,
                                  executory_accrual6,
                                  executory_accrual7,
                                  executory_accrual8,
                                  executory_accrual9,
                                  executory_accrual10,
                                  executory_paid1,
                                  executory_paid2,
                                  executory_paid3,
                                  executory_paid4,
                                  executory_paid5,
                                  executory_paid6,
                                  executory_paid7,
                                  executory_paid8,
                                  executory_paid9,
                                  executory_paid10,
                                  contingent_accrual1,
                                  contingent_accrual2,
                                  contingent_accrual3,
                                  contingent_accrual4,
                                  contingent_accrual5,
                                  contingent_accrual6,
                                  contingent_accrual7,
                                  contingent_accrual8,
                                  contingent_accrual9,
                                  contingent_accrual10,
                                  contingent_paid1,
                                  contingent_paid2,
                                  contingent_paid3,
                                  contingent_paid4,
                                  contingent_paid5,
                                  contingent_paid6,
                                  contingent_paid7,
                                  contingent_paid8,
                                  contingent_paid9,
                                  contingent_paid10)
    SELECT  a_ilr_id,
            a_revision,
            fasb_sob.set_of_books_id,
            sch.MONTH,
            sch.interest_income_received,
            sch.interest_income_accrued,
            sch.interest_rental_recvd_spread,
            sch.begin_deferred_rev,
            sch.deferred_rev,
            sch.end_deferred_rev,
            sch.begin_receivable,
            sch.end_receivable,
            sch.begin_lt_receivable,
            sch.end_lt_receivable,
            sch.initial_direct_cost,
            sch.executory_accrual1,
            sch.executory_accrual2,
            sch.executory_accrual3,
            sch.executory_accrual4,
            sch.executory_accrual5,
            sch.executory_accrual6,
            sch.executory_accrual7,
            sch.executory_accrual8,
            sch.executory_accrual9,
            sch.executory_accrual10,
            sch.executory_paid1,
            sch.executory_paid2,
            sch.executory_paid3,
            sch.executory_paid4,
            sch.executory_paid5,
            sch.executory_paid6,
            sch.executory_paid7,
            sch.executory_paid8,
            sch.executory_paid9,
            sch.executory_paid10,
            sch.contingent_accrual1,
            sch.contingent_accrual2,
            sch.contingent_accrual3,
            sch.contingent_accrual4,
            sch.contingent_accrual5,
            sch.contingent_accrual6,
            sch.contingent_accrual7,
            sch.contingent_accrual8,
            sch.contingent_accrual9,
            sch.contingent_accrual10,
            sch.contingent_paid1,
            sch.contingent_paid2,
            sch.contingent_paid3,
            sch.contingent_paid4,
            sch.contingent_paid5,
            sch.contingent_paid6,
            sch.contingent_paid7,
            sch.contingent_paid8,
            sch.contingent_paid9,
            sch.contingent_paid10
    FROM TABLE(f_get_op_schedule(a_ilr_id, a_revision)) sch
    JOIN lsr_ilr_options ilro ON ilro.ilr_id = a_ilr_id AND ilro.revision = a_revision
    JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
    JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
    WHERE lower(trim(fasb_cap_type.description)) = 'operating';

    --Really should separate the calcs of these values into separate function
    --and call that here
    INSERT INTO lsr_ilr_amounts(ilr_id,
                                revision,
                                set_of_books_id,
                                npv_lease_payments,
                                npv_guaranteed_residual,
                                npv_unguaranteed_residual,
                                selling_profit_loss,
                                beginning_lease_receivable,
                                beginning_net_investment,
                                cost_of_goods_sold)
    SELECT  a_ilr_id AS ilr_id,
            a_revision AS revision,
            set_of_books_id,
            npv_lease_payments,
            npv_guaranteed_residual,
            npv_unguaranteed_residual,
            selling_profit_loss,
            begin_lease_receivable,
            begin_net_investment,
            cost_of_goods_sold
    FROM (SELECT  npv_lease_payments,
                  npv_guaranteed_residual,
                  npv_unguaranteed_residual,
                  selling_profit_loss,
                  begin_lease_receivable,
                  begin_net_investment,
                  cost_of_goods_sold
          FROM (SELECT  0 AS npv_lease_payments,
                        0 AS npv_guaranteed_residual,
                        0 AS npv_unguaranteed_residual,
                        0 AS selling_profit_loss,
                        begin_receivable AS begin_lease_receivable,
                        0 AS begin_net_investment,
                        0 AS cost_of_goods_sold
                FROM TABLE(f_get_op_schedule(a_ilr_id, a_revision))
                ORDER BY MONTH)
          WHERE ROWNUM = 1)
    CROSS JOIN (SELECT set_of_books_id
                FROM lsr_ilr_options ilro
                JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
                JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
                WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'operating'
                AND ilro.ilr_id = a_ilr_id AND ilro.revision = a_revision);

  END p_process_op_ilr;

  /*****************************************************************************
  * PROCEDURE: p_process_sales_ilr
  * PURPOSE: Processes a sales-type ILR Schedule for the given ILR/Revision
  * PARAMETERS:
  *   a_ilr_id: ilr_id of the ILR for which to process the schedule
  *   a_revision: Revision of the ILR for which to process the schedule
  ******************************************************************************/

  PROCEDURE p_process_sales_ilr(a_ilr_id NUMBER,
                                a_revision number)
  IS
  BEGIN
    INSERT ALL
    into lsr_ilr_schedule(ilr_id,
                          revision,
                          set_of_books_id,
                          MONTH,
                          interest_income_received,
                          interest_income_accrued,
                          interest_rental_recvd_spread,
                          beg_deferred_rev,
                          deferred_rev_activity,
                          end_deferred_rev,
                          beg_receivable,
                          end_receivable,
                          beg_lt_receivable,
                          end_lt_receivable,
                          initial_direct_cost,
                          executory_accrual1,
                          executory_accrual2,
                          executory_accrual3,
                          executory_accrual4,
                          executory_accrual5,
                          executory_accrual6,
                          executory_accrual7,
                          executory_accrual8,
                          executory_accrual9,
                          executory_accrual10,
                          executory_paid1,
                          executory_paid2,
                          executory_paid3,
                          executory_paid4,
                          executory_paid5,
                          executory_paid6,
                          executory_paid7,
                          executory_paid8,
                          executory_paid9,
                          executory_paid10,
                          contingent_accrual1,
                          contingent_accrual2,
                          contingent_accrual3,
                          contingent_accrual4,
                          contingent_accrual5,
                          contingent_accrual6,
                          contingent_accrual7,
                          contingent_accrual8,
                          contingent_accrual9,
                          contingent_accrual10,
                          contingent_paid1,
                          contingent_paid2,
                          contingent_paid3,
                          contingent_paid4,
                          contingent_paid5,
                          contingent_paid6,
                          contingent_paid7,
                          contingent_paid8,
                          contingent_paid9,
                          contingent_paid10) values (ilr_id,
                                                      revision,
                                                      set_of_books_id,
                                                      month,
                                                      interest_income_received,
                                                      interest_income_accrued,
                                                      0,
                                                      0,
                                                      0,
                                                      0,
                                                      begin_receivable,
                                                      end_receivable,
                                                      begin_lt_receivable,
                                                      end_lt_receivable,
                                                      initial_direct_cost,
                                                      executory_accrual1,
                                                      executory_accrual2,
                                                      executory_accrual3,
                                                      executory_accrual4,
                                                      executory_accrual5,
                                                      executory_accrual6,
                                                      executory_accrual7,
                                                      executory_accrual8,
                                                      executory_accrual9,
                                                      executory_accrual10,
                                                      executory_paid1,
                                                      executory_paid2,
                                                      executory_paid3,
                                                      executory_paid4,
                                                      executory_paid5,
                                                      executory_paid6,
                                                      executory_paid7,
                                                      executory_paid8,
                                                      executory_paid9,
                                                      executory_paid10,
                                                      contingent_accrual1,
                                                      contingent_accrual2,
                                                      contingent_accrual3,
                                                      contingent_accrual4,
                                                      contingent_accrual5,
                                                      contingent_accrual6,
                                                      contingent_accrual7,
                                                      contingent_accrual8,
                                                      contingent_accrual9,
                                                      contingent_accrual10,
                                                      contingent_paid1,
                                                      contingent_paid2,
                                                      contingent_paid3,
                                                      contingent_paid4,
                                                      contingent_paid5,
                                                      contingent_paid6,
                                                      contingent_paid7,
                                                      contingent_paid8,
                                                      contingent_paid9,
                                                      contingent_paid10)
    into lsr_ilr_schedule_sales_direct( ilr_id,
                                        revision,
                                        set_of_books_id,
                                        MONTH,
                                        principal_received,
                                        principal_accrued,
                                        beg_unguaranteed_residual,
                                        interest_unguaranteed_residual,
                                        ending_unguaranteed_residual,
                                        beg_net_investment,
                                        interest_net_investment,
                                        ending_net_investment,
                                        calculated_initial_rate,
                                        calculated_rate) VALUES ( ilr_id,
                                                                  revision,
                                                                  set_of_books_id,
                                                                  month,
                                                                  principal_received,
                                                                  principal_accrued,
                                                                  begin_unguaranteed_residual,
                                                                  int_on_unguaranteed_residual,
                                                                  end_unguaranteed_residual,
                                                                  begin_net_investment,
                                                                  int_on_net_investment,
                                                                  end_net_investment,
                                                                  initial_rate,
                                                                  rate)
    SELECT  a_ilr_id as ilr_id,
            a_revision AS revision,
            fasb_sob.set_of_books_id,
            sch.MONTH,
            sch.interest_income_received,
            sch.interest_income_accrued,
            sch.principal_accrued,
            sch.principal_received,
            sch.begin_receivable,
            sch.end_receivable,
            sch.begin_lt_receivable,
            sch.end_lt_receivable,
            sch.initial_direct_cost,
            sch.executory_accrual1,
            sch.executory_accrual2,
            sch.executory_accrual3,
            sch.executory_accrual4,
            sch.executory_accrual5,
            sch.executory_accrual6,
            sch.executory_accrual7,
            sch.executory_accrual8,
            sch.executory_accrual9,
            sch.executory_accrual10,
            sch.executory_paid1,
            sch.executory_paid2,
            sch.executory_paid3,
            sch.executory_paid4,
            sch.executory_paid5,
            sch.executory_paid6,
            sch.executory_paid7,
            sch.executory_paid8,
            sch.executory_paid9,
            sch.executory_paid10,
            sch.contingent_accrual1,
            sch.contingent_accrual2,
            sch.contingent_accrual3,
            sch.contingent_accrual4,
            sch.contingent_accrual5,
            sch.contingent_accrual6,
            sch.contingent_accrual7,
            sch.contingent_accrual8,
            sch.contingent_accrual9,
            sch.contingent_accrual10,
            sch.contingent_paid1,
            sch.contingent_paid2,
            sch.contingent_paid3,
            sch.contingent_paid4,
            sch.contingent_paid5,
            sch.contingent_paid6,
            sch.contingent_paid7,
            sch.contingent_paid8,
            sch.contingent_paid9,
            sch.contingent_paid10,
            sch.begin_unguaranteed_residual,
            sch.int_on_unguaranteed_residual,
            sch.end_unguaranteed_residual,
            sch.begin_net_investment,
            sch.int_on_net_investment,
            sch.end_net_investment,
            sch.initial_rate,
            sch.rate
    FROM TABLE(f_get_sales_schedule(a_ilr_id, a_revision)) sch
    JOIN lsr_ilr_options ilro ON ilro.ilr_id = a_ilr_id AND ilro.revision = a_revision
    JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
    JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
    WHERE lower(trim(fasb_cap_type.description)) = 'sales type';

    --Really should separate the calcs of these values into separate function
    --and call that here
    INSERT INTO lsr_ilr_amounts(ilr_id,
                                revision,
                                set_of_books_id,
                                npv_lease_payments,
                                npv_guaranteed_residual,
                                npv_unguaranteed_residual,
                                selling_profit_loss,
                                beginning_lease_receivable,
                                beginning_net_investment,
                                cost_of_goods_sold)
    SELECT  a_ilr_id AS ilr_id,
            a_revision AS revision,
            set_of_books_id,
            npv_lease_payments,
            npv_guaranteed_residual,
            npv_unguaranteed_residual,
            selling_profit_loss,
            begin_lease_receivable,
            begin_net_investment,
            cost_of_goods_sold
    FROM (SELECT  npv_lease_payments,
                  npv_guaranteed_residual,
                  npv_unguaranteed_residual,
                  selling_profit_loss,
                  begin_lease_receivable,
                  begin_net_investment,
                  cost_of_goods_sold
          FROM (SELECT  npv_lease_payments,
                        npv_guaranteed_residual,
                        npv_unguaranteed_residual,
                        selling_profit_loss,
                        begin_lease_receivable,
                        begin_net_investment,
                        cost_of_goods_sold
                FROM TABLE(f_get_sales_schedule(a_ilr_id, a_revision))
                ORDER BY MONTH)
          WHERE ROWNUM = 1)
    CROSS JOIN (SELECT set_of_books_id
                FROM lsr_ilr_options ilro
                JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
                JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
                WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'sales type'
                AND ilro.ilr_id = a_ilr_id AND ilro.revision = a_revision);

  END p_process_sales_ilr;


  /*****************************************************************************
  * PROCEDURE: p_process_ilr
  * PURPOSE: Processes the Lessor ILR Schedule for the given ILR/Revision
  * PARAMETERS:
  *   a_ilr_id: ilr_id of the ILR for which to process the schedule
  *   a_revision: Revision of the ILR for which to process the schedule
  ******************************************************************************/
  procedure p_process_ilr(  a_ilr_id NUMBER,
                            a_revision NUMBER)
  IS
    l_cap_type_desc VARCHAR2(254);
    l_message VARCHAR2(4000);
    l_date date;
  BEGIN
    pkg_pp_log.p_start_log(f_get_pp_process_id('Lessor - ILR Schedule'));
    pkg_pp_log.p_write_message('Starting lessor ilr_id/revision: ' || nvl(to_char(a_ilr_id), 'NULL') || ' / ' || nvl(to_char(a_revision), 'NULL'));

    DELETE FROM lsr_ilr_amounts
    WHERE ilr_id = a_ilr_id AND revision = a_revision;

    DELETE FROM lsr_ilr_schedule_sales_direct
    WHERE ilr_id = a_ilr_id
    AND revision = a_revision;

    DELETE FROM lsr_ilr_schedule
    WHERE ilr_id = a_ilr_id
    and revision = a_revision;

    /*p_process_op_ilr and p_process_sales_ilr join to lsr_fasb_cap_type and
      lsr_fasb_type_sob to build correct schedule based on set of books */
    p_process_op_ilr(a_ilr_id, a_revision);
    p_process_sales_ilr(a_ilr_id, a_revision);

    SELECT MIN(month) INTO l_date
    FROM lsr_ilr_schedule
    WHERE ilr_id = a_ilr_id
    AND revision = revision;

    l_message := pkg_lessor_var_payments.f_calc_ilr_var_payments(a_ilr_id, a_revision, l_date);

    IF l_message <> 'OK' THEN
      raise_application_error(-20000, substr('Error calculating variable payments: ' || l_message, 0, 2000));
    END IF;

    commit;

    pkg_pp_log.p_end_log;
  EXCEPTION
    WHEN OTHERS THEN
      IF SQLCODE BETWEEN -20999 AND -20000 THEN
        rollback;
        pkg_pp_log.p_write_message(sqlerrm);
        pkg_pp_log.p_end_log;
        RAISE;
      --PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
      ELSIF SQLCODE = 100 THEN
        ROLLBACK;
        pkg_pp_log.p_write_message('Error processing Lessor ILRs - ' || sqlerrm || f_get_call_stack);
        pkg_pp_log.p_end_log;
        raise_application_error(-20000, substr('Error processing Lessor ILRs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
      ELSE
        ROLLBACK;
        pkg_pp_log.p_write_message('Error processing lessor ILRs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
        pkg_pp_log.p_end_log;
        RAISE;
      END IF;
      pkg_pp_log.p_end_log;
  END p_process_ilr;

  /*****************************************************************************
  * Function: f_get_payment_terms
  * PURPOSE: Looks up and returns the payment terms for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve payment terms
  *   a_revision: The revision of the ILR for which to retrieve payment terms
  *
  * RETURNS: Table with payment terms (pipelined)
  * NOTE: This now looks to the variable payments package for logic to get payment terms
  ******************************************************************************/

  FUNCTION f_get_payment_terms(a_ilr_id NUMBER, a_revision NUMBER) RETURN lsr_ilr_op_sch_pay_term_tab PIPELINED IS
  BEGIN
    -- If there's a way to avoid this unpacking/repacking, that would be awesome
    FOR res IN (SELECT lsr_ilr_op_sch_pay_term( payment_month_frequency,
                                                payment_term_start_date,
                                                number_of_terms,
                                                payment_amount,
                                                executory_buckets,
                                                contingent_buckets,
                                                is_prepay) TERM
                FROM TABLE(pkg_lessor_var_payments.f_get_initial_pay_terms(a_ilr_id, a_revision)))
    LOOP
      pipe row(res.term);
    END LOOP;
  END f_get_payment_terms;

  /*****************************************************************************
  * Function: f_get_initial_direct_costs
  * PURPOSE: Looks up and returns the initial direct costs for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve payment terms
  *   a_revision: The revision of the ILR for which to retrieve payment terms
  *
  * RETURNS: Table with initial direct costs (pipelined)
  ******************************************************************************/
  FUNCTION f_get_initial_direct_costs(a_ilr_id NUMBER, a_revision NUMBER) RETURN lsr_init_direct_cost_info_tab PIPELINED IS
  BEGIN
    -- If there's a way to avoid this unpacking/repacking, that would be awesome
    FOR res IN (SELECT lsr_init_direct_cost_info( idc_group_id,
                                                  date_incurred,
                                                  amount,
                                                  DESCRIPTION) COST
                FROM lsr_ilr_initial_direct_cost
                WHERE ilr_id = a_ilr_id
                AND revision = a_revision
                UNION ALL
                SELECT lsr_init_direct_cost_info( NULL,
                                                  NULL,
                                                  0,
                                                  NULL)
                FROM lsr_ilr_initial_direct_cost
                WHERE ilr_id = a_ilr_id
                AND revision = a_revision
                having count(1) = 0)
    LOOP
      pipe row(res.cost);
    END LOOP;
  END f_get_initial_direct_costs;

  /*****************************************************************************
  * Function: f_get_sales_type_info
  * PURPOSE: Looks up and returns information necessary to complete the building of sales-type schedules
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve infomration
  *   a_revision: The revision of the ILR for which to retrieve information
  *
  * RETURNS: Table with sales-type information (pipelined)
  ******************************************************************************/

  FUNCTION f_get_sales_type_info(a_ilr_id NUMBER, a_revision NUMBER) RETURN lsr_ilr_sales_sch_info_tab PIPELINED IS
  BEGIN
    FOR rec IN (
      SELECT  lsr_ilr_sales_sch_info( SUM(asset.carrying_cost),
                                      rates.rate,
                                      SUM(asset.guaranteed_residual_amount),
                                      SUM(asset.fair_market_value * asset.estimated_residual_pct),
                                      coalesce(lease.days_in_year, 365),
                                      opt.purchase_option_amt,
                                      opt.termination_amt) as info
      FROM lsr_ilr_options opt
      JOIN lsr_ilr ilr ON opt.ilr_id = ilr.ilr_id
      JOIN lsr_lease lease ON ilr.lease_id = lease.lease_id
      JOIN lsr_asset asset ON opt.ilr_id = asset.ilr_id AND opt.revision = asset.revision
      JOIN lsr_ilr_rates rates ON opt.ilr_id = rates.ilr_id AND opt.revision = rates.revision
      JOIN lsr_ilr_rate_types rate_types on rates.rate_type_id = rate_types.rate_type_id
      WHERE opt.ilr_id = a_ilr_id AND opt.revision = a_revision
      AND lower(trim(rate_types.description)) = lower(trim('Sales Type Discount Rate'))
      GROUP BY  opt.ilr_id,
                opt.revision,
                rates.rate,
                lease.days_in_year,
                opt.purchase_option_amt,
                opt.termination_amt)
    LOOP
      pipe row(rec.info);
    END LOOP;
  END f_get_sales_type_info;

  /*****************************************************************************
  * Function: f_get_payment_info_from_terms
  * PURPOSE: Transforms payment terms into a month-by-month listing of payment information
  * PARAMETERS:
  *   a_payment_terms: The payment terms for which to generate payment info
  *
  * RETURNS: Table with payment information (pipelined)
  ******************************************************************************/

  FUNCTION f_get_payment_info_from_terms(a_payment_terms lsr_ilr_op_sch_pay_term_tab) RETURN lsr_ilr_op_sch_pay_info_tab PIPELINED
  IS
  BEGIN
    FOR rec IN
    (WITH payment_terms(payment_month_frequency,
                        payment_term_start_date,
                        MONTH,
                        number_of_terms,
                        payment_amount,
                        contingent_buckets,
                        executory_buckets,
                        is_prepay,
                        iter)
      AS (SELECT  payment_month_frequency,
                  payment_term_start_date,
                  payment_term_start_date as month,
                  number_of_terms,
                  payment_amount,
                  contingent_buckets,
                  executory_buckets,
                  is_prepay,
                  CASE is_prepay
                    WHEN 0 THEN 1
                    WHEN 1 THEN payment_month_frequency * number_of_terms
                    ELSE NULL
                  END AS iter --We want to count down/up when prepay/arrears (so that payments will take place at the beginning/end of period (see payments below)
          FROM table(a_payment_terms)
          ----^^ Base Case
          UNION ALL
          --Recurisve case
          SELECT  payment_month_frequency,
                  payment_term_start_date,
                  add_months(month, 1) as month,
                  number_of_terms,
                  payment_amount,
                  contingent_buckets,
                  executory_buckets,
                  is_prepay,
                  CASE is_prepay
                    WHEN 0 THEN iter + 1
                    WHEN 1 THEN iter - 1
                    ELSE NULL
                  END AS iter
          FROM payment_terms
          WHERE add_months(MONTH, 1) < add_months(payment_term_start_date, payment_month_frequency * number_of_terms))
      --Calculate payments based on payment terms
    SELECT  lsr_ilr_op_sch_pay_info(payment_month_frequency,
                                    payment_term_start_date,
                                    month,
                                    number_of_terms,
                                    payment_amount,
                                    contingent_buckets,
                                    executory_buckets,
                                    is_prepay,
                                    iter) as info
      FROM payment_terms)
    LOOP
      PIPE ROW(rec.info);
    END LOOP;
  END f_get_payment_info_from_terms;

  /*****************************************************************************
  * Function: f_get_payments_from_info
  * PURPOSE: Transforms month-by-month payment information listing into month-by-month list of calculated payment amounts
  * PARAMETERS:
  *   a_payment_info: The payment information for which to calculate payment amounts
  *
  * RETURNS: Table with calculated payment amounts (pipelined)
  ******************************************************************************/

  FUNCTION f_get_payments_from_info(a_payment_info lsr_ilr_op_sch_pay_info_tab) RETURN lsr_schedule_payment_def_tab PIPELINED IS
  BEGIN
    FOR rec IN (
      SELECT  lsr_schedule_payment_def( payment_month_frequency,
                                        is_prepay,
                                        DENSE_RANK() OVER (ORDER BY payment_term_start_date), --Group months based on date payment takes place
                                        month,
                                        number_of_terms,
                                        iter,
                                        CASE
                                          --We want payments to occur every ith month. The modulus of the current iteration will give us this (couning down/up for prepay/arrears)
                                          WHEN MOD(iter, payment_month_frequency) = 0 THEN payment_amount
                                          ELSE 0
                                        END) AS p
      FROM TABLE(a_payment_info))
    LOOP
      PIPE ROW (rec.P);
    END LOOP;
  END f_get_payments_from_info;

  /*****************************************************************************
  * Function: f_calculate_buckets
  * PURPOSE: Calculates executory and contingent bucket accruals and payments
  * PARAMETERS:
  *   a_payment_terms: The payment terms for which to process buckets
  *
  * RETURNS: Table with bucket information (pipelined)
  ******************************************************************************/

  FUNCTION f_calculate_buckets(payment_info lsr_ilr_op_sch_pay_info_tab) RETURN lsr_bucket_result_tab PIPELINED IS
  BEGIN
    for rec in (
      WITH buckets
      as (SELECT  A.month,
                  b.bucket_name,
                  --In order to prevent rounding errors for accumulating, we round during non-payment months
                  -- and "true-up" during payment months to account for the difference
                  CASE
                    WHEN MOD(A.iter, A.payment_month_frequency) <> 0
                      THEN round(amount / payment_month_frequency, 2)
                      ELSE round(amount - (round((amount / payment_month_frequency), 2) * (payment_month_frequency -  1)), 2)
                  END as accrued,
                  CASE
                    WHEN MOD(A.iter, A.payment_month_frequency) = 0 THEN b.amount
                    ELSE 0
                  END AS received
          FROM TABLE(payment_info) a, TABLE(A.executory_buckets) (+) b
          UNION ALL
          SELECT  A.month,
                  b.bucket_name,
                  --In order to prevent rounding errors for accumulating, we round during non-payment months
                  -- and "true-up" during payment months to account for the difference
                  CASE
                    WHEN MOD(A.iter, A.payment_month_frequency) <> 0
                      THEN round(amount / payment_month_frequency, 2)
                      ELSE round(amount - (round((amount / payment_month_frequency), 2) * (payment_month_frequency -  1)), 2)
                  END as accrued,
                  CASE
                    WHEN MOD(A.iter, A.payment_month_frequency) = 0 THEN b.amount
                    ELSE 0
                  END AS received
          FROM TABLE(payment_info) A, TABLE(A.contingent_buckets) (+) b)
      SELECT  lsr_bucket_result(month,
                                executory_accrued_1,
                                executory_accrued_2,
                                executory_accrued_3,
                                executory_accrued_4,
                                executory_accrued_5,
                                executory_accrued_6,
                                executory_accrued_7,
                                executory_accrued_8,
                                executory_accrued_9,
                                executory_accrued_10,
                                executory_received_1,
                                executory_received_2,
                                executory_received_3,
                                executory_received_4,
                                executory_received_5,
                                executory_received_6,
                                executory_received_7,
                                executory_received_8,
                                executory_received_9,
                                executory_received_10,
                                contingent_accrued_1,
                                contingent_accrued_2,
                                contingent_accrued_3,
                                contingent_accrued_4,
                                contingent_accrued_5,
                                contingent_accrued_6,
                                contingent_accrued_7,
                                contingent_accrued_8,
                                contingent_accrued_9,
                                contingent_accrued_10,
                                contingent_received_1,
                                contingent_received_2,
                                contingent_received_3,
                                contingent_received_4,
                                contingent_received_5,
                                contingent_received_6,
                                contingent_received_7,
                                contingent_received_8,
                                contingent_received_9,
                                contingent_received_10) as bucket_result
      FROM(SELECT MONTH,
                  COALESCE(executory_accrued_1, 0) as executory_accrued_1,
                  COALESCE(executory_received_1, 0) as executory_received_1,
                  COALESCE(executory_accrued_2, 0) AS executory_accrued_2,
                  COALESCE(executory_received_2, 0) as executory_received_2,
                  COALESCE(executory_accrued_3, 0) as executory_accrued_3,
                  COALESCE(executory_received_3, 0) as executory_received_3,
                  coalesce(executory_accrued_4, 0) as executory_accrued_4,
                  COALESCE(executory_received_4, 0) as executory_received_4,
                  coalesce(executory_accrued_5, 0) as executory_accrued_5,
                  COALESCE(executory_received_5, 0) as executory_received_5,
                  coalesce(executory_accrued_6, 0) as executory_accrued_6,
                  COALESCE(executory_received_6, 0) as executory_received_6,
                  coalesce(executory_accrued_7, 0) as executory_accrued_7,
                  COALESCE(executory_received_7, 0) as executory_received_7,
                  coalesce(executory_accrued_8, 0) as executory_accrued_8,
                  COALESCE(executory_received_8, 0) as executory_received_8,
                  coalesce(executory_accrued_9, 0) as executory_accrued_9,
                  COALESCE(executory_received_9, 0) as executory_received_9,
                  coalesce(executory_accrued_10, 0) as executory_accrued_10,
                  COALESCE(executory_received_10, 0) as executory_received_10,
                  COALESCE(contingent_accrued_1, 0) as contingent_accrued_1,
                  COALESCE(contingent_received_1, 0) as contingent_received_1,
                  COALESCE(contingent_accrued_2, 0) AS contingent_accrued_2,
                  coalesce(contingent_received_2, 0) as contingent_received_2,
                  coalesce(contingent_accrued_3, 0) as contingent_accrued_3,
                  COALESCE(contingent_received_3, 0) as contingent_received_3,
                  coalesce(contingent_accrued_4, 0) as contingent_accrued_4,
                  COALESCE(contingent_received_4, 0) as contingent_received_4,
                  coalesce(contingent_accrued_5, 0) as contingent_accrued_5,
                  COALESCE(contingent_received_5, 0) as contingent_received_5,
                  coalesce(contingent_accrued_6, 0) as contingent_accrued_6,
                  COALESCE(contingent_received_6, 0) as contingent_received_6,
                  coalesce(contingent_accrued_7, 0) as contingent_accrued_7,
                  COALESCE(contingent_received_7, 0) as contingent_received_7,
                  coalesce(contingent_accrued_8, 0) as contingent_accrued_8,
                  COALESCE(contingent_received_8, 0) as contingent_received_8,
                  coalesce(contingent_accrued_9, 0) as contingent_accrued_9,
                  COALESCE(contingent_received_9, 0) as contingent_received_9,
                  COALESCE(contingent_accrued_10, 0) AS contingent_accrued_10,
                  COALESCE(contingent_received_10, 0) as contingent_received_10
          FROM (SELECT month, bucket_name || '_a' as bucket, accrued as amount
                FROM buckets
                UNION ALL
                SELECT MONTH, bucket_name || '_r', received
                FROM buckets)
          PIVOT (
            SUM(amount) --PIVOT requires an aggregate here. Should only be one amount per bucket accrued/received per month
            FOR bucket in ( 'e_bucket_1_a' as executory_accrued_1,
                            'e_bucket_1_r' AS executory_received_1,
                            'e_bucket_2_a' AS executory_accrued_2,
                            'e_bucket_2_r' as executory_received_2,
                            'e_bucket_3_a' AS executory_accrued_3,
                            'e_bucket_3_r' AS executory_received_3,
                            'e_bucket_4_a' AS executory_accrued_4,
                            'e_bucket_4_r' AS executory_received_4,
                            'e_bucket_5_a' AS executory_accrued_5,
                            'e_bucket_5_r' AS executory_received_5,
                            'e_bucket_6_a' AS executory_accrued_6,
                            'e_bucket_6_r' AS executory_received_6,
                            'e_bucket_7_a' AS executory_accrued_7,
                            'e_bucket_7_r' AS executory_received_7,
                            'e_bucket_8_a' AS executory_accrued_8,
                            'e_bucket_8_r' AS executory_received_8,
                            'e_bucket_9_a' AS executory_accrued_9,
                            'e_bucket_9_r' AS executory_received_9,
                            'e_bucket_10_a' AS executory_accrued_10,
                            'e_bucket_10_r' as executory_received_10,
                            'c_bucket_1_a' as contingent_accrued_1,
                            'c_bucket_1_r' AS contingent_received_1,
                            'c_bucket_2_a' AS contingent_accrued_2,
                            'c_bucket_2_r' as contingent_received_2,
                            'c_bucket_3_a' AS contingent_accrued_3,
                            'c_bucket_3_r' AS contingent_received_3,
                            'c_bucket_4_a' AS contingent_accrued_4,
                            'c_bucket_4_r' AS contingent_received_4,
                            'c_bucket_5_a' AS contingent_accrued_5,
                            'c_bucket_5_r' AS contingent_received_5,
                            'c_bucket_6_a' AS contingent_accrued_6,
                            'c_bucket_6_r' AS contingent_received_6,
                            'c_bucket_7_a' AS contingent_accrued_7,
                            'c_bucket_7_r' AS contingent_received_7,
                            'c_bucket_8_a' AS contingent_accrued_8,
                            'c_bucket_8_r' AS contingent_received_8,
                            'c_bucket_9_a' AS contingent_accrued_9,
                            'c_bucket_9_r' AS contingent_received_9,
                            'c_bucket_10_a' AS contingent_accrued_10,
                            'c_bucket_10_r' AS contingent_received_10))))
    LOOP
      pipe row(rec.bucket_result);
    END LOOP;
  END f_calculate_buckets;

  /*****************************************************************************
  * Function: f_build_op_schedule
  * PURPOSE: Builds the operating schedule for the given payment terms
  * PARAMETERS:
  *   a_payment_terms: The payment terms associated with this ILR. The schedule will
  *                     build for payment_month_frequency (i.e. the number of months between payments),
  *                               payment_term_start_date (i.e. the starting date of the payment term),
  *                               number_of_terms (i.e. the number of payments that will be made),
  *                               payment_amount (i.e. the amount of payment to apply)
  *                               is_prepay (0 = arrears / 1 = prepay)
  *   NOTE: Multiple payment terms can be defined. For example, for a 36 month, prepay monthly lease,
  *           with $500 payments in year 1, $550 payments in year 2, and $600 payments in year three,
  *           provide three payment terms (1, <year_1_start>, 12, 500, 1),
  *                                       (1, <year_2_start>, 12, 550, 1),
  *                                       (1, <year_3_start>, 12, 600, 1)
  *
  * RETURNS: Table with schedule results
  ******************************************************************************/
  FUNCTION f_build_op_schedule(a_payment_terms lsr_ilr_op_sch_pay_term_tab, a_initial_direct_costs lsr_init_direct_cost_info_tab) RETURN lsr_ilr_op_sch_result_tab PIPELINED
  IS
  BEGIN
    --Open implicit cursor for pipelined output
    FOR res IN (
      --Put payment term info into SQL table for use in SQL functions
      WITH payments
      AS (SELECT  payment_month_frequency,
                  payment_group, --Group months based on date payment takes place
                  month,
                  number_of_terms,
                  is_prepay,
                  iter,
                  payment_amount
          FROM TABLE(f_get_payments_from_info(f_get_payment_info_from_terms(a_payment_terms)))),
      initial_direct_costs
      as (SELECT  idc_group_id,
                  date_incurred,
                  amount,
                  DESCRIPTION
          FROM TABLE(a_initial_direct_costs)),
    	--Build values of income section of schedule
      income_info
      AS (SELECT  MONTH,
                  payment_month_frequency,
                  number_of_terms,
                  is_prepay,
                  iter,
                  payment_amount AS interest_income_received,
                  --Add all amounts and divide by number of months to get the accrued income
                  SUM(payment_amount) OVER (PARTITION BY NULL) / (COUNT(1) OVER (PARTITION BY NULL)) AS interest_income_accrued,
                  --The spread is the amount over each group of months for a payment, divided by number of terms, divided by the frequency (based on schedule example)
                  (SUM(payment_amount) OVER (PARTITION BY payment_group) / number_of_terms) / payment_month_frequency AS interest_income_spread
          FROM payments),
      income_info_with_idc
      AS (SELECT  MONTH,
                  payment_month_frequency,
                  number_of_terms,
                  is_prepay,
                  iter,
                  interest_income_received,
                  interest_income_accrued,
                  interest_income_spread,
                  idc.total_amount / COUNT(1) OVER (PARTITION BY NULL) AS initial_direct_cost
          FROM income_info
          CROSS JOIN (SELECT SUM(amount) AS total_amount
                      FROM initial_direct_costs) idc),
      income_info_penny_plug
      AS (SELECT  MONTH,
                  payment_month_frequency,
                  number_of_terms,
                  iter,
                  interest_income_received,
                  -- Do this rounding in the last period of the "term",
                  -- whether or not the lease is prepay, so use row_number instead of iter
                  CASE
                    WHEN MOD(row_number() OVER (ORDER BY MONTH), payment_month_frequency) = 0
                      THEN round(SUM(interest_income_accrued) OVER (ORDER BY MONTH ROWS BETWEEN (payment_month_frequency - 1) PRECEDING AND CURRENT ROW) -
                                  --This will be null for monthly (nothing between 0 preceding and 1 preceding)
                                  COALESCE(SUM(round(interest_income_accrued, 2)) OVER (ORDER BY MONTH ROWS BETWEEN (payment_month_frequency - 1) PRECEDING AND 1 PRECEDING), 0), 2)
                    ELSE round(interest_income_accrued, 2)
                  END AS interest_income_accrued,
                  -- Do this rounding in the last period of the "term",
                  -- whether or not the lease is prepay, so use row_number instead of iter
                  CASE
                    WHEN MOD(row_number() OVER (ORDER BY MONTH), payment_month_frequency) = 0
                      THEN round(SUM(interest_income_spread) OVER (ORDER BY MONTH ROWS BETWEEN (payment_month_frequency - 1) PRECEDING AND CURRENT ROW) -
                                  --This will be null for monthly (nothing between 0 preceding and 1 preceding)
                                  COALESCE(SUM(round(interest_income_spread, 2)) OVER (ORDER BY MONTH ROWS BETWEEN (payment_month_frequency - 1) PRECEDING AND 1 PRECEDING), 0), 2)
                    ELSE round(interest_income_spread, 2)
                END AS interest_income_spread,
                CASE MONTH
                  WHEN LAST_VALUE(MONTH) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                    THEN round(SUM(initial_direct_cost) OVER (PARTITION BY NULL) - COALESCE(SUM(round(initial_direct_cost, 2)) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING),0), 2)
                    ELSE round(initial_direct_cost, 2)
                  END as initial_direct_cost
          FROM income_info_with_idc),
      income_info_final_plug
      AS (SELECT MONTH,
                 interest_income_received,
                 CASE MONTH
                  WHEN LAST_VALUE(MONTH) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                    THEN round(SUM(interest_income_received) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
                          COALESCE(SUM(round(interest_income_accrued, 2)) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING), 0), 2)
                  ELSE round(interest_income_accrued, 2)
                 END AS interest_income_accrued,
                 CASE MONTH
                  WHEN LAST_VALUE(MONTH) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                    THEN round(SUM(interest_income_received) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
                          COALESCE(SUM(round(interest_income_spread, 2)) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING), 0), 2)
                    ELSE round(interest_income_spread, 2)
                 END AS interest_income_spread,
                 initial_direct_cost
         FROM income_info_penny_plug),
      -- Build values for revenue section of schedule
      rev_info
      AS(SELECT month,
                interest_income_received,
                interest_income_accrued,
                interest_income_spread,
                interest_income_accrued - interest_income_spread AS deferred_rev,
                initial_direct_cost
      FROM income_info_final_plug),
      -- Build final schedule results
      schedule
      as (SELECT  month,
                  interest_income_received,
                  interest_income_accrued,
                  interest_income_spread,
                  --Beginning deferred revenue is the sum of deferred revenue from all previous months (first month will be null, convert to 0)
                  COALESCE(SUM(deferred_rev) OVER (ORDER BY month ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING), 0) AS begin_deferred_rev,
                  deferred_rev,
                  --Ending deferred revenue is the sum of deferred revenue from all previous months and the current month (first month will be null, convert to 0)
                  COALESCE(SUM(deferred_rev) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), 0) AS end_deferred_rev,
                  --The beginning receivable is the total receviable (first sum), minus the amount received in all previous months (second sum)
                  SUM(interest_income_received) OVER (PARTITION BY NULL) -
                  coalesce(sum(interest_income_received) over (order by month rows between unbounded preceding and 1 preceding), 0)
                  AS begin_receivable,
                  --The ending receivable is the total receviable (first sum), minus the amount received in all previous months and this month(second sum)
                  SUM(interest_income_received) OVER (PARTITION BY NULL) -
                  COALESCE(SUM(interest_income_received) OVER (ORDER BY month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), 0)
                  AS end_receivable,
                  initial_direct_cost
          FROM rev_info),
        schedule_with_lt
        AS (SELECT  MONTH,
                    interest_income_received,
                    interest_income_accrued,
                    interest_income_spread,
                    begin_deferred_rev,
                    deferred_rev,
                    end_deferred_rev,
                    begin_receivable,
                    end_receivable,
                    LEAD(begin_receivable, 12, 0) OVER (ORDER BY MONTH) AS begin_lt_receivable,
                    LEAD(end_receivable, 12, 0) OVER (ORDER BY MONTH) AS end_lt_receivable,
                    initial_direct_cost
            FROM schedule)
      --Select final results
      SELECT  lsr_ilr_op_sch_result(sch.MONTH,
                                    sch.interest_income_received,
                                    sch.interest_income_accrued,
                                    sch.interest_income_spread,
                                    sch.begin_deferred_rev,
                                    sch.deferred_rev,
                                    sch.end_deferred_rev,
                                    sch.begin_receivable,
                                    sch.end_receivable,
                                    sch.begin_lt_receivable,
                                    sch.end_lt_receivable,
                                    sch.initial_direct_cost,
                                    buckets.executory_accrued_1,
                                    buckets.executory_accrued_2,
                                    buckets.executory_accrued_3,
                                    buckets.executory_accrued_4,
                                    buckets.executory_accrued_5,
                                    buckets.executory_accrued_6,
                                    buckets.executory_accrued_7,
                                    buckets.executory_accrued_8,
                                    buckets.executory_accrued_9,
                                    buckets.executory_accrued_10,
                                    buckets.executory_received_1,
                                    buckets.executory_received_2,
                                    buckets.executory_received_3,
                                    buckets.executory_received_4,
                                    buckets.executory_received_5,
                                    buckets.executory_received_6,
                                    buckets.executory_received_7,
                                    buckets.executory_received_8,
                                    buckets.executory_received_9,
                                    buckets.executory_received_10,
                                    buckets.contingent_accrued_1,
                                    buckets.contingent_accrued_2,
                                    buckets.contingent_accrued_3,
                                    buckets.contingent_accrued_4,
                                    buckets.contingent_accrued_5,
                                    buckets.contingent_accrued_6,
                                    buckets.contingent_accrued_7,
                                    buckets.contingent_accrued_8,
                                    buckets.contingent_accrued_9,
                                    buckets.contingent_accrued_10,
                                    buckets.contingent_received_1,
                                    buckets.contingent_received_2,
                                    buckets.contingent_received_3,
                                    buckets.contingent_received_4,
                                    buckets.contingent_received_5,
                                    buckets.contingent_received_6,
                                    buckets.contingent_received_7,
                                    buckets.contingent_received_8,
                                    buckets.contingent_received_9,
                                    buckets.contingent_received_10) AS sch_line
      FROM schedule_with_lt sch
      JOIN TABLE(f_calculate_buckets(f_get_payment_info_from_terms(a_payment_terms))) buckets on sch.month = buckets.month)
      LOOP
        --Pipe results to caller
        PIPE ROW (res.sch_line);
      END LOOP;
    EXCEPTION
      WHEN no_data_needed THEN
        RAISE; --Oracle uses NO_DATA_NEEDED to signal end of pipeline
      WHEN OTHERS THEN
        IF SQLCODE BETWEEN -20999 AND -20000 THEN
          raise;
        ELSE
          raise_application_error(-20000, substr('Error building lessor operating schedule - ' || sqlerrm || CHR(10) || f_get_call_stack, 1, 2000));
        END IF;
  END f_build_op_schedule;

  /*****************************************************************************
  * Function: f_get_op_schedule
  * PURPOSE: Builds and returns the operating schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule. The schedule will
  *             build for payment payment terms given in table lsr_ilr_payment_term
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule results (pipelined)
  ******************************************************************************/

  FUNCTION f_get_op_schedule(a_ilr_id NUMBER, a_revision NUMBER) RETURN lsr_ilr_op_sch_result_tab PIPELINED IS
  BEGIN
    FOR res IN (SELECT  lsr_ilr_op_sch_result(MONTH,
                                              interest_income_received,
                                              interest_income_accrued,
                                              interest_rental_recvd_spread,
                                              begin_deferred_rev,
                                              deferred_rev,
                                              end_deferred_rev,
                                              begin_receivable,
                                              end_receivable,
                                              begin_lt_receivable,
                                              end_lt_receivable,
                                              initial_direct_cost,
                                              executory_accrual1,
                                              executory_accrual2,
                                              executory_accrual3,
                                              executory_accrual4,
                                              executory_accrual5,
                                              executory_accrual6,
                                              executory_accrual7,
                                              executory_accrual8,
                                              executory_accrual9,
                                              executory_accrual10,
                                              executory_paid1,
                                              executory_paid2,
                                              executory_paid3,
                                              executory_paid4,
                                              executory_paid5,
                                              executory_paid6,
                                              executory_paid7,
                                              executory_paid8,
                                              executory_paid9,
                                              executory_paid10,
                                              contingent_accrual1,
                                              contingent_accrual2,
                                              contingent_accrual3,
                                              contingent_accrual4,
                                              contingent_accrual5,
                                              contingent_accrual6,
                                              contingent_accrual7,
                                              contingent_accrual8,
                                              contingent_accrual9,
                                              contingent_accrual10,
                                              contingent_paid1,
                                              contingent_paid2,
                                              contingent_paid3,
                                              contingent_paid4,
                                              contingent_paid5,
                                              contingent_paid6,
                                              contingent_paid7,
                                              contingent_paid8,
                                              contingent_paid9,
                                              contingent_paid10) AS sch_line
                FROM TABLE(pkg_lessor_schedule.f_build_op_schedule(pkg_lessor_var_payments.f_get_initial_pay_terms(a_ilr_id, a_revision),
                                                                    f_get_initial_direct_costs(a_ilr_id, a_revision))))
    LOOP
      pipe row(res.sch_line);
    END LOOP;
  END f_get_op_schedule;

  /*****************************************************************************
  * Function: f_build_sales_schedule
  * PURPOSE: Builds the sales-type schedule for the given payment terms and sales-type-specific information
  * PARAMETERS:
  *   a_payment_terms: The payment terms to use in generating the schedule
  *   a_sales_type_info: The sales-type-specific info to use in generating the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/

  FUNCTION f_build_sales_schedule(a_payment_terms lsr_ilr_op_sch_pay_term_tab,
                                  a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                  a_sales_type_info lsr_ilr_sales_sch_info_tab) RETURN lsr_ilr_sales_sch_result_tab PIPELINED IS
  BEGIN
  FOR res in (
      WITH sales_type_info
      AS (SELECT  carrying_cost,
                  discount_rate / (days_in_year/30) AS discount_rate,
                  guaranteed_residual,
                  estimated_residual,
                  days_in_year,
                  purchase_option_amount,
                  termination_amount
          FROM TABLE(a_sales_type_info)),
      initial_direct_costs
      as (SELECT  idc_group_id,
                  date_incurred,
                  amount,
                  DESCRIPTION
          FROM TABLE(a_initial_direct_costs)),
      payments
      AS (SELECT  payment_month_frequency,
                  payment_group,
                  MONTH,
                  number_of_terms,
                  is_prepay,
                  iter,
                  payment_amount
        FROM TABLE(f_get_payments_from_info(f_get_payment_info_from_terms(a_payment_terms)))),
      npv
      AS (SELECT npv_lease_payments, npv_guaranteed_residual, npv_unguaranteed_residual
          FROM (
                SELECT  MONTH,
                        npv_lease_payments,
                        npv_guaranteed_residual,
                        npv_unguaranteed_residual
                FROM (SELECT  is_prepay,
                              MONTH,
                              CASE MONTH
                                WHEN LAST_VALUE(MONTH) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                                  THEN payment_amount + purchase_option_amount + termination_amount
                                ELSE payment_amount
                              END AS payment_amount,
                              discount_rate,
                              guaranteed_residual,
                              estimated_residual
                      FROM payments
                      CROSS JOIN sales_type_info)
                  MODEL
                    DIMENSION BY (row_number() OVER (ORDER BY MONTH) AS I)
                    MEASURES (MONTH,
                              payment_amount,
                              discount_rate,
                              guaranteed_residual,
                              estimated_residual,
                              0 npv_lease_payments,
                              0 AS npv_guaranteed_residual,
                              0 as npv_unguaranteed_residual,
                              is_prepay)
                    RULES ( npv_lease_payments[ I ] ORDER BY I  =
                              --Prepay leases start npv calc with t=0. Subtracting is_prepay from exponent will have this effect (is_prepay = 1 vs 0)
                              payment_amount[cv(i)] / POWER(1 + discount_rate[cv(I)], cv(I) - is_prepay[cv(I)]) + COALESCE(npv_lease_payments[cv(I)-1], 0),
                            npv_guaranteed_residual[I] ORDER BY I =
                              guaranteed_residual[cv()] / POWER(1 + discount_rate[cv(I)], cv(I) - is_prepay[cv(I)]),
                            npv_unguaranteed_residual[I] ORDER BY I =
                              (estimated_residual[cv()] - guaranteed_residual[cv()]) / POWER(1 + discount_rate[cv(i)], cv(i) - is_prepay[cv(i)]))
                ORDER BY MONTH DESC)
          WHERE ROWNUM = 1),
      sales_type_calc_info
      AS (SELECT  month,
                  payment_month_frequency,
                  payment_group,
                  number_of_terms,
                  is_prepay,
                  iter,
                  CASE WHEN MOD(iter, payment_month_frequency) = 0 THEN 1 ELSE 0 END AS is_payment_month,
                  payment_amount as fixed_payment,
                  npv_lease_payments,
                  discount_rate AS initial_rate,
                  (POWER(1 + discount_rate, payment_month_frequency) - 1) / payment_month_frequency AS rate,
                  guaranteed_residual,
                  purchase_option_amount,
                  termination_amount,
                  npv_guaranteed_residual,
                  npv_lease_payments + npv_guaranteed_residual AS initial_receivable,
                  npv_unguaranteed_residual,
                  npv_lease_payments + npv_guaranteed_residual + npv_unguaranteed_residual AS begin_net_investment,
                  (npv_lease_payments + npv_guaranteed_residual) - (carrying_cost - npv_unguaranteed_residual) AS selling_profit_loss,
                  carrying_cost - (estimated_residual - guaranteed_residual) AS cost_of_goods_sold,
                  case when row_number() over (order by month) = 1 THEN idc.amount ELSE 0 END as initial_direct_cost
          FROM payments
          CROSS JOIN (SELECT SUM(amount) AS amount
                      FROM initial_direct_costs) idc
          CROSS JOIN sales_type_info
          CROSS JOIN npv),
      schedule_calc
      AS (SELECT  MONTH,
                  payment_month_frequency,
                  iter,
                  payment_group,
                  number_of_terms,
                  is_prepay,
                  is_payment_month,
                  fixed_payment,
                  group_fixed_payment,
                  principal_received,
                  interest_income_received,
                  interest_income_accrued,
                  principal_accrued,
                  begin_receivable,
                  end_receivable,
                  COALESCE(begin_lt_receivable, 0) AS begin_lt_receivable,
                  COALESCE(end_lt_receivable, 0) AS end_lt_receivable,
                  begin_unguaranteed_residual,
                  int_on_unguaranteed_residual,
                  end_unguaranteed_residual,
                  begin_net_investment,
                  int_on_net_investment,
                  end_net_investment,
                  initial_rate,
                  rate,
                  guaranteed_residual,
                  purchase_option_amount,
                  termination_amount,
                  initial_receivable,
                  npv_lease_payments,
                  npv_guaranteed_residual,
                  npv_unguaranteed_residual,
                  selling_profit_loss,
                  cost_of_goods_sold,
                  initial_direct_cost
          FROM sales_type_calc_info
          MODEL
            DIMENSION BY (row_number() OVER (ORDER BY MONTH) as i)
            MEASURES( MONTH,
                      fixed_payment,
                      CASE is_prepay
                        WHEN 0 THEN LAST_VALUE(fixed_payment) OVER (PARTITION BY payment_group ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                        WHEN 1 THEN FIRST_VALUE(fixed_payment) OVER (PARTITION BY payment_group ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                      END as group_fixed_payment,
                      initial_rate,
                      rate,
                      payment_month_frequency,
                      iter,
                      payment_group,
                      number_of_terms,
                      is_payment_month,
                      is_prepay,
                      initial_receivable,
                      begin_net_investment,
                      guaranteed_residual,
                      purchase_option_amount,
                      termination_amount,
                      npv_lease_payments,
                      npv_guaranteed_residual,
                      npv_unguaranteed_residual,
                      selling_profit_loss,
                      cost_of_goods_sold,
                      initial_direct_cost,
                      count(1) over (partition by null) as cnt,
                      0 as int_on_net_investment,
                      0 as end_net_investment,
                      0 as principal_received,
                      0 as interest_income_received,
                      0 AS interest_income_accrued,
                      0 AS principal_accrued,
                      0 as begin_receivable,
                      0 AS end_receivable,
                      0 as begin_lt_receivable,
                      0 AS end_lt_receivable,
                      0 AS begin_unguaranteed_residual,
                      0 as int_on_unguaranteed_residual,
                      0 AS end_unguaranteed_residual)
            RULES AUTOMATIC ORDER ( begin_receivable[1] = initial_receivable[cv(I)], --First receivable is the initial calculated from npv
                                    interest_income_received[1] = CASE
                                                                    WHEN is_prepay[cv(i)] = 0 AND payment_month_frequency[cv(i)] = 1 THEN begin_receivable[cv(I)] * rate[cv(I)]
                                                                    ELSE 0
                                                                  END,
                                    interest_income_received[I != 1] =  CASE
                                                                          WHEN is_payment_month[cv(I)] = 1
                                                                            THEN begin_receivable[cv(I)] * rate[cv(I)] * payment_month_frequency[cv(I)]
                                                                            ELSE 0
                                                                        END,
                                    interest_income_accrued[I] =  CASE
                                                                    WHEN is_prepay[cv(I)] = 1
                                                                      THEN  CASE
                                                                              WHEN cv(I) = cnt[cv(I)] THEN 0
                                                                              ELSE end_receivable[cv(I)] * rate[cv(I)]
                                                                            END
                                                                    ELSE begin_receivable[cv(I)] * rate[cv(I)]
                                                                  END,
                                    principal_received[1] = CASE
                                                              WHEN is_prepay[cv(I)] = 1 THEN fixed_payment[cv(I)]
                                                              ELSE fixed_payment[cv(I)] - interest_income_received[cv(I)]
                                                            END,
                                    principal_received[I != 1] =  fixed_payment[cv(I)] - interest_income_received[cv(I)],
                                    principal_accrued[I] = COALESCE(( CASE
                                                                        WHEN is_prepay[cv(I)] = 0
                                                                          THEN group_fixed_payment[cv(I)]
                                                                        WHEN is_prepay[cv(I)] = 1
                                                                          --Prepay looks to the next "set of months" for a given payment period
                                                                          THEN group_fixed_payment[cv(i) + payment_month_frequency[cv(i)]]
                                                                      END - interest_income_accrued[cv(i)] * payment_month_frequency[cv(i)])
                                                                      / payment_month_frequency[cv(I)]
                                                                      , 0),
                                    end_receivable[I] = begin_receivable[cv(I)] - principal_received[cv(I)],
                                    begin_receivable[I != 1] = end_receivable[cv(I) - 1],
                                    begin_lt_receivable[I] = begin_receivable[cv(I) + 12],
                                    end_lt_receivable[I] = end_receivable[cv(I) + 12],
                                    begin_unguaranteed_residual[1] = npv_unguaranteed_residual[cv(I)],
                                    int_on_unguaranteed_residual[1] = CASE
                                                                        WHEN is_prepay[cv(I)] = 1 THEN 0
                                                                        ELSE begin_unguaranteed_residual[cv(I)] * initial_rate[cv(I)]
                                                                      END,
                                    int_on_unguaranteed_residual[I != 1] = begin_unguaranteed_residual[cv(I)] * initial_rate[cv(I)],
                                    end_unguaranteed_residual[I] = begin_unguaranteed_residual[cv(I)] + int_on_unguaranteed_residual[cv(I)],
                                    begin_unguaranteed_residual[I != 1] = end_unguaranteed_residual[cv(I) - 1],
                                    begin_net_investment[1] = begin_net_investment[cv(I)],
                                    int_on_net_investment[1] =  CASE
                                                                  WHEN is_prepay[cv(I)] = 1 THEN 0
                                                                  ELSE begin_net_investment[cv(I)] * initial_rate[cv(I)]
                                                                END,
                                    int_on_net_investment[I != 1] = begin_net_investment[cv(I)] * initial_rate[cv(I)],
                                    end_net_investment[I] = begin_net_investment[cv(I)] - fixed_payment[cv(I)] + int_on_net_investment[cv(I)] -
                                                              CASE cv(I)
                                                                WHEN cnt[cv(I)] THEN purchase_option_amount[cv(i)] + termination_amount[cv(i)]
                                                                ELSE 0
                                                              END,
                                    begin_net_investment[I != 1] = end_net_investment[cv(I) - 1]
                                  )),
      --Due to multiplication/division, there will be issues with rounding off pennies. We want everything to "tie out"
      schedule_first_penny_plug
      AS (SELECT  MONTH,
                  payment_month_frequency,
                  iter,
                  is_prepay,
                  CASE is_payment_month
                    WHEN 0 THEN round(principal_received, 2) -- This *should* always be 0
                    WHEN 1 THEN principal_received + (round(begin_receivable, 2) - round(principal_received, 2) - round(end_receivable, 2))
                  END AS principal_received,
                  CASE is_payment_month
                    WHEN 0 THEN round(interest_income_received, 2)
                    WHEN 1 THEN interest_income_received - (round(begin_receivable, 2) - round(principal_received, 2) - round(end_receivable, 2))
                  END as interest_income_received,
                  interest_income_accrued,
                  principal_accrued,
                  begin_receivable,
                  end_receivable,
                  begin_lt_receivable,
                  end_lt_receivable,
                  begin_unguaranteed_residual,
                  int_on_unguaranteed_residual,
                  end_unguaranteed_residual,
                  begin_net_investment,
                  int_on_net_investment,
                  end_net_investment,
                  initial_rate,
                  rate,
                  guaranteed_residual,
                  purchase_option_amount,
                  termination_amount,
                  initial_receivable,
                  npv_lease_payments,
                  npv_guaranteed_residual,
                  npv_unguaranteed_residual,
                  selling_profit_loss,
                  cost_of_goods_sold,
                  initial_direct_cost
          FROM schedule_calc),
      --Due to multiplication/division, there will be issues with rounding off pennies. We want everything to "tie out"
      schedule_second_penny_plug
      AS (SELECT  MONTH,
                  payment_month_frequency,
                  principal_received,
                  interest_income_received,
                  CASE
                    -- Lessee does this rounding in the last period of the "term",
                    -- whether or not the lease is prepay, so use row_number instead of iter
                    WHEN MOD(row_number() over (order by month), payment_month_frequency) = 0
                      THEN  CASE is_prepay
                              WHEN 0 THEN round(interest_income_received - (round(interest_income_accrued, 2) * (payment_month_frequency - 1)), 2)
                              WHEN 1 THEN round(coalesce(lead(interest_income_received, 1) OVER (order by month), 0) - (round(interest_income_accrued, 2) * (payment_month_frequency -1)), 2)
                            END
                      ELSE round(interest_income_accrued, 2)
                  END AS interest_income_accrued,
                  CASE
                    WHEN MOD(row_number() over (order by month), payment_month_frequency) = 0
                    THEN  CASE is_prepay
                            WHEN 0 THEN round(principal_received - ((round(principal_accrued, 2) * (payment_month_frequency - 1))), 2)
                            WHEN 1 THEN COALESCE(round(LEAD(principal_received, 1) OVER (ORDER BY MONTH) - (round(principal_accrued, 2) * (payment_month_frequency - 1)), 2), 0)
                          END
                    ELSE round(principal_accrued, 2)
                  END as principal_accrued,
                  begin_receivable,
                  end_receivable,
                  begin_lt_receivable,
                  end_lt_receivable,
                  begin_unguaranteed_residual,
                  int_on_unguaranteed_residual,
                  end_unguaranteed_residual,
                  begin_net_investment,
                  int_on_net_investment,
                  end_net_investment,
                  initial_rate,
                  rate,
                  guaranteed_residual,
                  purchase_option_amount,
                  termination_amount,
                  initial_receivable,
                  npv_lease_payments,
                  npv_guaranteed_residual,
                  npv_unguaranteed_residual,
                  selling_profit_loss,
                  cost_of_goods_sold,
                  initial_direct_cost,
                  is_prepay
          FROM schedule_first_penny_plug
          ORDER BY MONTH),
      --Due to multiplication/division, there will be issues with rounding off pennies. We want everything to "tie out"
      schedule_end_plug
      AS (SELECT  MONTH,
                  CASE row_number() over (order by month)
                    WHEN row_count THEN principal_received + end_plug
                    ELSE principal_received
                  END AS principal_received,
                  CASE row_number() over (order by month)
                    WHEN row_count THEN interest_income_received - end_plug
                    ELSE interest_income_received
                  END AS interest_income_received,
                  CASE
                    WHEN (is_prepay = 1 and payment_month_frequency <> 1 AND row_number() over (order by month) = row_count - 1)
                      THEN interest_income_accrued - ((interest_income_accrued * (payment_month_frequency - 1)) + end_plug)
                    ELSE interest_income_accrued
                  END AS interest_income_accrued,
                  CASE
                    WHEN (is_prepay = 0 AND row_number() over (order by month) = row_count)
                      THEN principal_accrued - end_plug
                    ELSE principal_accrued
                  END AS principal_accrued,
                  begin_receivable,
                  CASE
                    WHEN is_prepay = 0 AND row_number() over (order by month) = row_count THEN begin_receivable - (principal_received + end_plug)
                    ELSE end_receivable
                  END AS end_receivable,
                  begin_lt_receivable,
                  end_lt_receivable,
                  initial_direct_cost,
                  begin_unguaranteed_residual,
                  int_on_unguaranteed_residual,
                  end_unguaranteed_residual,
                  begin_net_investment,
                  int_on_net_investment,
                  end_net_investment,
                  initial_rate,
                  rate,
                  initial_receivable,
                  npv_lease_payments,
                  npv_guaranteed_residual,
                  npv_unguaranteed_residual,
                  selling_profit_loss,
                  cost_of_goods_sold
          FROM (SELECT  MONTH,
                        payment_month_frequency,
                        principal_received,
                        interest_income_received,
                        interest_income_accrued,
                        principal_accrued,
                        begin_receivable,
                        end_receivable,
                        begin_lt_receivable,
                        end_lt_receivable,
                        initial_direct_cost,
                        begin_unguaranteed_residual,
                        int_on_unguaranteed_residual,
                        end_unguaranteed_residual,
                        begin_net_investment,
                        int_on_net_investment,
                        end_net_investment,
                        is_prepay,
                        initial_rate,
                        rate,
                        initial_receivable,
                        npv_lease_payments,
                        npv_guaranteed_residual,
                        npv_unguaranteed_residual,
                        selling_profit_loss,
                        cost_of_goods_sold,
                        round(LAST_VALUE(begin_receivable) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING), 2) -
                          round(LAST_VALUE(principal_received) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING), 2) -
                          round(LAST_VALUE(guaranteed_residual) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING), 2) -
                          round(LAST_VALUE(purchase_option_amount) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING), 2) -
                          round(LAST_VALUE(termination_amount) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING), 2) AS end_plug,
                        count(1) over (partition by null) as row_count
                FROM schedule_second_penny_plug))
      SELECT  lsr_ilr_sales_sch_result( sch.MONTH,
                                        sch.principal_received,
                                        sch.interest_income_received,
                                        sch.interest_income_accrued,
                                        sch.principal_accrued,
                                        sch.begin_receivable,
                                        sch.end_receivable,
                                        sch.begin_lt_receivable,
                                        sch.end_lt_receivable,
                                        sch.initial_direct_cost,
                                        buckets.executory_accrued_1,
                                        buckets.executory_accrued_2,
                                        buckets.executory_accrued_3,
                                        buckets.executory_accrued_4,
                                        buckets.executory_accrued_5,
                                        buckets.executory_accrued_6,
                                        buckets.executory_accrued_7,
                                        buckets.executory_accrued_8,
                                        buckets.executory_accrued_9,
                                        buckets.executory_accrued_10,
                                        buckets.executory_received_1,
                                        buckets.executory_received_2,
                                        buckets.executory_received_3,
                                        buckets.executory_received_4,
                                        buckets.executory_received_5,
                                        buckets.executory_received_6,
                                        buckets.executory_received_7,
                                        buckets.executory_received_8,
                                        buckets.executory_received_9,
                                        buckets.executory_received_10,
                                        buckets.contingent_accrued_1,
                                        buckets.contingent_accrued_2,
                                        buckets.contingent_accrued_3,
                                        buckets.contingent_accrued_4,
                                        buckets.contingent_accrued_5,
                                        buckets.contingent_accrued_6,
                                        buckets.contingent_accrued_7,
                                        buckets.contingent_accrued_8,
                                        buckets.contingent_accrued_9,
                                        buckets.contingent_accrued_10,
                                        buckets.contingent_received_1,
                                        buckets.contingent_received_2,
                                        buckets.contingent_received_3,
                                        buckets.contingent_received_4,
                                        buckets.contingent_received_5,
                                        buckets.contingent_received_6,
                                        buckets.contingent_received_7,
                                        buckets.contingent_received_8,
                                        buckets.contingent_received_9,
                                        buckets.contingent_received_10,
                                        sch.begin_unguaranteed_residual,
                                        sch.int_on_unguaranteed_residual,
                                        sch.end_unguaranteed_residual,
                                        sch.begin_net_investment,
                                        sch.int_on_net_investment,
                                        sch.end_net_investment,
                                        sch.initial_rate,
                                        sch.rate,
                                        sch.initial_receivable,
                                        sch.npv_lease_payments,
                                        sch.npv_guaranteed_residual,
                                        sch.npv_unguaranteed_residual,
                                        sch.selling_profit_loss,
                                        sch.cost_of_goods_sold) AS sch_line
      FROM schedule_end_plug sch
      JOIN TABLE(f_calculate_buckets(f_get_payment_info_from_terms(a_payment_terms))) buckets ON sch.MONTH = buckets.MONTH)
    LOOP
      PIPE ROW (res.sch_line);
    END LOOP;
  end f_build_sales_schedule;

  /*****************************************************************************
  * Function: f_get_sales_schedule
  * PURPOSE: Builds and returns the operating schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/

  FUNCTION f_get_sales_schedule(a_ilr_id NUMBER, a_revision NUMBER) RETURN lsr_ilr_sales_sch_result_tab PIPELINED IS
  BEGIN
    FOR res IN (SELECT  lsr_ilr_sales_sch_result( MONTH,
                                                  principal_received,
                                                  interest_income_received,
                                                  interest_income_accrued,
                                                  principal_accrued,
                                                  begin_receivable,
                                                  end_receivable,
                                                  begin_lt_receivable,
                                                  end_lt_receivable,
                                                  initial_direct_cost,
                                                  executory_accrual1,
                                                  executory_accrual2,
                                                  executory_accrual3,
                                                  executory_accrual4,
                                                  executory_accrual5,
                                                  executory_accrual6,
                                                  executory_accrual7,
                                                  executory_accrual8,
                                                  executory_accrual9,
                                                  executory_accrual10,
                                                  executory_paid1,
                                                  executory_paid2,
                                                  executory_paid3,
                                                  executory_paid4,
                                                  executory_paid5,
                                                  executory_paid6,
                                                  executory_paid7,
                                                  executory_paid8,
                                                  executory_paid9,
                                                  executory_paid10,
                                                  contingent_accrual1,
                                                  contingent_accrual2,
                                                  contingent_accrual3,
                                                  contingent_accrual4,
                                                  contingent_accrual5,
                                                  contingent_accrual6,
                                                  contingent_accrual7,
                                                  contingent_accrual8,
                                                  contingent_accrual9,
                                                  contingent_accrual10,
                                                  contingent_paid1,
                                                  contingent_paid2,
                                                  contingent_paid3,
                                                  contingent_paid4,
                                                  contingent_paid5,
                                                  contingent_paid6,
                                                  contingent_paid7,
                                                  contingent_paid8,
                                                  contingent_paid9,
                                                  contingent_paid10,
                                                  begin_unguaranteed_residual,
                                                  int_on_unguaranteed_residual,
                                                  end_unguaranteed_residual,
                                                  begin_net_investment,
                                                  int_on_net_investment,
                                                  end_net_investment,
                                                  initial_rate,
                                                  rate,
                                                  begin_lease_receivable,
                                                  npv_lease_payments,
                                                  npv_guaranteed_residual,
                                                  npv_unguaranteed_residual,
                                                  selling_profit_loss,
                                                  cost_of_goods_sold) AS sch_line
                FROM TABLE( pkg_lessor_schedule.f_build_sales_schedule(pkg_lessor_var_payments.f_get_initial_pay_terms(a_ilr_id, a_revision),
                                                                        f_get_initial_direct_costs(a_ilr_id, a_revision),
                                                                        f_get_sales_type_info(a_ilr_id, a_revision))))
    LOOP
      pipe row(res.sch_line);
    END LOOP;
  END f_get_sales_schedule;

END pkg_lessor_schedule;
/

--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (4057, 0, 2017, 1, 0, 0, 0, 'C:\plasticwks\powerplant\sql\packages', '2017.1.0.0_PKG_LESSOR_SCHEDULE.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 