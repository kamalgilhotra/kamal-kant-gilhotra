CREATE OR REPLACE package pkg_pp_log as
   subtype G_RESULT_TYPE is pls_integer range 1 .. 3 not null;
   G_SUCCESS constant G_RESULT_TYPE := 1;
   G_FAIL    constant G_RESULT_TYPE := 2;
   G_WARNING constant G_RESULT_TYPE := 3;
   G_PROCESS_ID          PP_PROCESSES.PROCESS_ID%type;
   G_OCCURRENCE_ID       PP_PROCESSES_OCCURRENCES.OCCURRENCE_ID%type;
   G_MSG_ORDER           PP_PROCESSES_MESSAGES.MSG_ORDER%type;
   G_PROCESS_DESCRIPTION PP_PROCESSES.DESCRIPTION%type;

   procedure P_START_LOG(P_PROCESS_ID in PP_PROCESSES.PROCESS_ID%type);

   procedure P_END_LOG;

   procedure P_END_LOG(P_BATCH_ID     PP_PROCESSES_OCCURRENCES.BATCH_ID%type,
                       P_RETURN_VALUE PP_PROCESSES_OCCURRENCES.RETURN_VALUE%type);

   procedure P_WRITE_MESSAGE(P_MSG in varchar2);

   procedure P_WRITE_MESSAGE(P_MSG            in varchar2,
                             P_PP_ERROR_CODE  in varchar2,
                             P_SQL_ERROR_CODE in varchar2);

   procedure P_LOCK_PROCESS(P_PROCESS_ID in PP_PROCESSES.PROCESS_ID%type);

   function F_START_LOG(P_PROCESS_ID in PP_PROCESSES.PROCESS_ID%type)
      return PP_PROCESSES_OCCURRENCES.OCCURRENCE_ID%type;

   function F_MSG_LOOKUP(P_PROCESS_ID  in PP_PROCESSES.PROCESS_ID%type,
                         P_NEW_MESSAGE in varchar2,
                         P_SQLDB_CODE  in pls_integer)
      return PP_PROCESSES_ERROR_MSG.PP_ERROR_CODE%type;
end PKG_PP_LOG;
/

CREATE OR REPLACE package body pkg_pp_log as
   /*
   ||============================================================================
   || Subsystem: PowerPlant
   || Object Name: maint_030549_system_PKG_PP_LOG.sql
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 05/09/2013 B.Beck         Original Version
   || 10.4.1.1 10/07/2013 C.Shilling     Maint-33041
   ||============================================================================
   */

--   G_PROCESS_ID          PP_PROCESSES.PROCESS_ID%type;
--   G_OCCURRENCE_ID       PP_PROCESSES_OCCURRENCES.OCCURRENCE_ID%type;
--   G_MSG_ORDER           PP_PROCESSES_MESSAGES.MSG_ORDER%type;
--   G_PROCESS_DESCRIPTION PP_PROCESSES.DESCRIPTION%type;

   --**************************************************************************
   --                            P_START_LOG
   --**************************************************************************
   procedure P_START_LOG(P_PROCESS_ID in PP_PROCESSES.PROCESS_ID%type) is
      pragma autonomous_transaction;

      V_OCCURRENCE_ID PP_PROCESSES_OCCURRENCES.OCCURRENCE_ID%type;
   begin
      V_OCCURRENCE_ID := F_START_LOG(P_PROCESS_ID);
   exception
      when others then
         rollback;
         raise;
   end P_START_LOG;


   --**************************************************************************
   --                            F_START_LOG
   --**************************************************************************
   function F_START_LOG(P_PROCESS_ID in PP_PROCESSES.PROCESS_ID%type)
      return PP_PROCESSES_OCCURRENCES.OCCURRENCE_ID%type is
      pragma autonomous_transaction;

   begin
      if G_OCCURRENCE_ID is not null and G_PROCESS_ID = P_PROCESS_ID then
         return G_OCCURRENCE_ID;
      elsif G_OCCURRENCE_ID is not null and G_PROCESS_ID is not null and
            G_PROCESS_ID <> P_PROCESS_ID then
         P_END_LOG;
      end if;

      begin
         select DESCRIPTION
           into G_PROCESS_DESCRIPTION
           from PP_PROCESSES
          where PROCESS_ID = P_PROCESS_ID;
      exception
         when NO_DATA_FOUND then
            RAISE_APPLICATION_ERROR(-20000,
                                    'Specified process_id ' || TO_CHAR(G_PROCESS_ID) ||
                                    ' could not be found in PP_PROCESSES.');
      end;

      G_PROCESS_ID := P_PROCESS_ID;
      G_MSG_ORDER  := 0;

      -- Use sequence for now. Might go back to max()+1 later
      /*      select NVL(max(OCCURRENCE_ID), 0) + 1
       into G_OCCURRENCE_ID
       from PP_PROCESSES_OCCURRENCES
      where PROCESS_ID = G_PROCESS_ID;*/
      select PP_PROCESSES_OCURRENCES_SEQ.NEXTVAL into G_OCCURRENCE_ID from DUAL;

      insert into PP_PROCESSES_OCCURRENCES
         (PROCESS_ID, OCCURRENCE_ID, START_TIME)
      values
         (G_PROCESS_ID, G_OCCURRENCE_ID, sysdate);

      commit;

      P_WRITE_MESSAGE('*******************************');
      P_WRITE_MESSAGE('Log started for process ' || G_PROCESS_DESCRIPTION || ' occurrence ' ||
                      TO_CHAR(G_OCCURRENCE_ID) || '.');

      return G_OCCURRENCE_ID;
   exception
      when others then
         G_OCCURRENCE_ID       := null;
         G_PROCESS_ID          := null;
         G_PROCESS_DESCRIPTION := null;
         rollback;
         raise;
   end F_START_LOG;

   --**************************************************************************
   --                            P_END_LOG
   --**************************************************************************
   procedure P_END_LOG is
      pragma autonomous_transaction;

   begin
      P_END_LOG(null, null);
   exception
      when others then
         rollback;
         raise;
   end P_END_LOG;

   --**************************************************************************
   --                            P_END_LOG
   --**************************************************************************
   procedure P_END_LOG(P_BATCH_ID     PP_PROCESSES_OCCURRENCES.BATCH_ID%type,
                       P_RETURN_VALUE PP_PROCESSES_OCCURRENCES.RETURN_VALUE%type) is
      pragma autonomous_transaction;

   begin
    if G_OCCURRENCE_ID is not null and G_PROCESS_ID is not null then
      update PP_PROCESSES_OCCURRENCES
         set END_TIME = sysdate, BATCH_ID = P_BATCH_ID, RETURN_VALUE = P_RETURN_VALUE
       where PROCESS_ID = G_PROCESS_ID
         and OCCURRENCE_ID = G_OCCURRENCE_ID;

      P_WRITE_MESSAGE('Log ended for process ' || G_PROCESS_DESCRIPTION || ' occurrence ' ||
                      TO_CHAR(G_OCCURRENCE_ID) || '.');
      P_WRITE_MESSAGE('*******************************');

      G_PROCESS_ID          := null;
      G_OCCURRENCE_ID       := null;
      G_MSG_ORDER           := 0;
      G_PROCESS_DESCRIPTION := null;
    end if;

      commit;
   exception
      when others then
         rollback;
         raise;
   end P_END_LOG;

   --**************************************************************************
   --                            P_WRITE_MESSAGE
   --**************************************************************************
   procedure P_WRITE_MESSAGE(P_MSG in varchar2) is
      pragma autonomous_transaction;

   begin
      P_WRITE_MESSAGE(P_MSG, null, null);
   exception
      when others then
         rollback;
         raise;
   end P_WRITE_MESSAGE;

   --**************************************************************************
   --                            P_WRITE_MESSAGE
   --**************************************************************************
   procedure P_WRITE_MESSAGE(P_MSG            in varchar2,
                             P_PP_ERROR_CODE  in varchar2,
                             P_SQL_ERROR_CODE in varchar2) is
      pragma autonomous_transaction;

   begin
      for I in 0 .. FLOOR(LENGTH(P_MSG) / 2000)
      loop
         G_MSG_ORDER := G_MSG_ORDER + 1;

         insert into PP_PROCESSES_MESSAGES
            (PROCESS_ID, OCCURRENCE_ID, MSG, MSG_ORDER, PP_ERROR_CODE, SQL_ERROR_CODE, TIME_STAMP,
             USER_ID)
         values
            (G_PROCESS_ID, G_OCCURRENCE_ID, SUBSTR(P_MSG, 1 + I * 2000, 2000), G_MSG_ORDER,
             P_PP_ERROR_CODE, P_SQL_ERROR_CODE, sysdate, user);
      end loop;

      commit;
   exception
      when others then
         rollback;
         raise;
   end P_WRITE_MESSAGE;

   --**************************************************************************
   --                            P_LOCK_PROCESS
   --**************************************************************************
   procedure P_LOCK_PROCESS(P_PROCESS_ID in PP_PROCESSES.PROCESS_ID%type) is
      pragma autonomous_transaction;
   begin
      update PP_PROCESSES set SYSTEM_LOCK = 1 where PROCESS_ID = P_PROCESS_ID;

      commit;
   exception
      when others then
         rollback;
         raise;
   end P_LOCK_PROCESS;

   --**************************************************************************
   --                            F_MSG_LOOKUP
   --**************************************************************************
   function F_MSG_LOOKUP(P_PROCESS_ID  in PP_PROCESSES.PROCESS_ID%type,
                         P_NEW_MESSAGE in varchar2,
                         P_SQLDB_CODE  in pls_integer)
      return PP_PROCESSES_ERROR_MSG.PP_ERROR_CODE%type is
      pragma autonomous_transaction;

      V_PP_ERROR_CODE      PP_PROCESSES_ERROR_MSG.PP_ERROR_CODE%type;
      V_MAX_ERROR_CODE_LEN pls_integer;
   begin
      --did they pass in the error code?
      begin
         select PP_ERROR_CODE
           into V_PP_ERROR_CODE
           from PP_PROCESSES_ERROR_MSG
          where PROCESS_ID = P_PROCESS_ID
            and PP_ERROR_CODE = P_NEW_MESSAGE;
      exception
         when TOO_MANY_ROWS then
            RAISE_APPLICATION_ERROR(-20000,
                                    'Multiple rows found on PP_PROCESSES_ERROR_MSG for PROCESS_ID ' ||
                                    P_PROCESS_ID || ' and PP_ERROR_CODE ' || P_NEW_MESSAGE || '.');
         when NO_DATA_FOUND then
            begin
               --did they pass in the error message?
               select PP_ERROR_CODE
                 into V_PP_ERROR_CODE
                 from PP_PROCESSES_ERROR_MSG
                where PROCESS_ID = P_PROCESS_ID
                  and UPPER(trim(ERROR_MESSAGE)) = UPPER(trim(P_NEW_MESSAGE))
                  and ROWNUM = 1; --if two pp_error_codes have the same message, take whatever one Oracle sorts first.
            exception
               when NO_DATA_FOUND then
                  --new message - add it

                  --need to find the max length first.
                  begin
                     select DATA_LENGTH
                       into V_MAX_ERROR_CODE_LEN
                       from ALL_TAB_COLS
                      where TABLE_NAME = 'PP_PROCESSES_ERROR_MSG'
                        and COLUMN_NAME = 'PP_ERROR_CODE'
                        and OWNER = 'PWRPLANT';
                  exception
                     when NO_DATA_FOUND then
                        RAISE_APPLICATION_ERROR(-20000,
                                                'Cannot find length of PP_ERROR_CODE column on PP_PROCESSES_ERROR_MSG in ALL_TAB_COLS view.');
                     when TOO_MANY_ROWS then
                        RAISE_APPLICATION_ERROR(-20000,
                                                'Found multiple rows when searching for PP_ERROR_CODE column on PP_PROCESSES_ERROR_MSG in ALL_TAB_COLS view.');
                  end;

                  --if message is short enough, use it as the error code, otherwise use dummy
                  if LENGTH(P_NEW_MESSAGE) <= V_MAX_ERROR_CODE_LEN then
                     V_PP_ERROR_CODE := UPPER(trim(P_NEW_MESSAGE));
                  else
                     select NVL(max(PP_ERROR_CODE), 0) + 1
                       into V_PP_ERROR_CODE
                       from PP_PROCESSES_ERROR_MSG
                      where PROCESS_ID = P_PROCESS_ID
                        and IS_NUMBER(PP_ERROR_CODE) = 1;
                  end if;

                  --We have our error code now. Create new message
                  insert into PP_PROCESSES_ERROR_MSG
                     (PROCESS_ID, PP_ERROR_CODE, ERROR_MESSAGE)
                  values
                     (P_PROCESS_ID, V_PP_ERROR_CODE, P_NEW_MESSAGE);

                  insert into PP_PROCESSES_ERROR_INFO
                     (ERROR_INFO_ID, PROCESS_ID, PP_ERROR_CODE, SQL_ERROR_CODE, ORDER_ID)
                     select NVL(max(ERROR_INFO_ID), 0) + 1,
                            P_PROCESS_ID,
                            PP_ERROR_CODE,
                            P_SQLDB_CODE,
                            99
                       from PP_PROCESSES_ERROR_INFO;
            end;
      end;

      commit;

      return V_PP_ERROR_CODE;
   exception
      when others then
         rollback;
         raise;
   end F_MSG_LOOKUP;
end PKG_PP_LOG;
/
--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (3136, 0, 2015, 2, 2, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2015.2.2.0_PKG_PP_LOG.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
