/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038575_depr_PKG_DEPR_FCST.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------- ----------------------------------------------
|| 10.4.2.2 06/16/2014 Kyle Peterson
||========================================================================================
*/

create or replace package PKG_DEPR_FCST as

	/*
	||============================================================================
	|| Object Name: P_BUILD_VINTAGE_SUMMARY
	|| Description: Builds the vintaged data by month from fcst_depr_ledger
	||
	||	Arguments:
	||               A_FCST_DEPR_VERSION: number: The forecasted depreciation version to build the depr vintage summary
	||               A_START_MONTH: date: the start date for forecasting depreciation
	||               A_END_MONTH: date: the start date for forecasting depreciation
	||	Return: NONE
	||
	||============================================================================
	|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
	||============================================================================
	|| Version Date       Revised By     Reason for Change
	|| ------- ---------- -------------- -----------------------------------------
	|| 1.0     02/05/2014 Kyle Peterson    Original Version
	||============================================================================
	*/
	 	-- record to bulk collect fcst_depr_vintage_summary
	type FCST_DVS_REC is record
	(
		ID number,
		FCST_DEPR_VERSION_ID FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_VERSION_ID%type,
		SET_OF_BOOKS_ID FCST_DEPR_VINTAGE_SUMMARY.SET_OF_BOOKS_ID%type,
		FCST_DEPR_GROUP_ID FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_GROUP_ID%type,
		ACCOUNTING_MONTH FCST_DEPR_VINTAGE_SUMMARY.ACCOUNTING_MONTH%type,
		VINTAGE FCST_DEPR_VINTAGE_SUMMARY.VINTAGE%type,
		ACCUM_COST FCST_DEPR_VINTAGE_SUMMARY.ACCUM_COST%type,
		ADDITIONS FCST_DEPR_VINTAGE_SUMMARY.ADDITIONS%type,
		TRANSFERS_IN FCST_DEPR_VINTAGE_SUMMARY.TRANSFERS_IN%type,
		TRANSFERS_OUT FCST_DEPR_VINTAGE_SUMMARY.TRANSFERS_OUT%type,
		ADJUSTMENTS FCST_DEPR_VINTAGE_SUMMARY.ADJUSTMENTS%type,
		RETIREMENTS FCST_DEPR_VINTAGE_SUMMARY.RETIREMENTS%type
	);

	type FCST_DVS_TAB is table of FCST_DVS_REC index by PLS_INTEGER;

	type DMR_RR_REC is record (	DMR_ID number,
								SOB_ID number,
								MORTALITY_CURVE_ID number,
								EXPECTED_AVERAGE_LIFE number,
								END_OF_LIFE number,
								RESERVE_RATIO_ID number,
								ALLOCATION_PROCEDURE varchar2(5),
								MONTH date);
	type DMR_RR_TAB is table of DMR_RR_REC;
	type RES_RATIO_MATH_REC is record(
			POINT_BEFORE1 number := 1,
			POINT_AFTER1 number := 2,
			LOOKUP_POINT1 number := 1,
			REM_LIFE_PCT1 number := 100,
			SURV_PCT1 number := 1,
			TRUNC_RL_PCT1 number := 0,
			TRUNC_SV_PCT1 number := 0,
			POINT_BEFORE2 number,
			POINT_AFTER2 number,
			LOOKUP_POINT2 number,
			REM_LIFE_PCT2 number,
			SURV_PCT2 number,
			TRUNC_RL_PCT2 number,
			TRUNC_SV_PCT2 number,
			AREA1 number,
			AREA1TRUNC number,
			AREA2 number,
			AREA2TRUNC number,
			REMAINING_LIFE1 number,
			REMAINING_LIFE2 number,
			RESERVE_RATIO1 number,
			RESERVE_RATIO2 number,
			MONTH_RATIO number,
			MONTH_REM_LIFE number,
			INTEGER_AGE number,
			RESERVE_RATIO_ID number,
			MORTALITY_CURVE_ID number,
			MAX_REM_LIFE number,
			EXPECTED_AVG_LIFE number);
	type RES_RATIO_MATH_TABLE is table of RES_RATIO_MATH_REC index by pls_integer;

	G_DEBUG boolean := FALSE;
	G_DEBUG_DEEP boolean := FALSE;

	procedure P_BUILD_VINTAGE_SUMMARY(	A_FCST_DEPR_VERSION FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
										A_START_MONTH date,
										A_END_MONTH date);

	procedure P_FCST_RATE_RECALC( 		A_FCST_DEPR_VERSION FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
										A_START_MONTH date,
										A_END_MONTH date);

	/*These functions are exposed externally so that they can be called by SQL in a model statement*/
	function F_AUTO_RET_AMT_GROSS(	A_YR_BEGIN_BALANCE number,
									A_FACTOR_ID number,
									A_RET_PCT number,
									A_PREV_RETS number,
									A_FISCAL_MONTH number)  return number;

	function F_AUTO_RET_AMT_ADDITIONS(A_ADDITIONS number,
										A_RET_PCT number)  return number;

	function F_AUTO_RET_AMT_CURVE(A_YR_BEGIN_BALANCE number,
									A_FACTOR_ID number,
									A_PREV_RETS number,
									A_MONTH date,
									A_FCST_DEPR_GROUP_ID number,
									A_SET_OF_BOOKS_ID number,
									A_FCST_VERSION number,
									A_VINTAGE number,
									A_DEPR_METHOD_ID number,
									A_EXPECTED_AVERAGE_LIFE number,
									A_END_OF_LIFE number,
									A_RESERVE_RATIO_ID number,
									A_ALLOCATION_PROCEDURE varchar2,
									A_MORTALITY_CURVE_ID number	)  return number;

	--Exposed because called from combined_depr code
	function F_GET_RESERVE_RATIO_ID(A_DMR_REC in out DMR_RR_REC) return number;
	procedure P_LOAD_RESERVE_RATIOS_BROAD( 	A_DMR_REC DMR_RR_REC);
	procedure P_LOAD_RESERVE_RATIOS_ELG( 	A_DMR_REC DMR_RR_REC);

	--Exposed since it's called by SQL
	function F_BUILD_RESERVE_RATIOS_BROAD(A_DMR_REC  DMR_RR_REC) return RES_RATIO_MATH_TABLE;

	procedure P_SET_MONTHS(A_VERSION_ID number, A_START_MONTH date, A_END_MONTH date);

	procedure P_FILL_UOP_TABLES(A_VERSION_ID number, A_START_MONTH date, A_END_MONTH date);
    
    CALC_RETIRE number;
    
end PKG_DEPR_FCST;
/

create or replace package body PKG_DEPR_FCST as

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
	type SPREAD_FACTOR_TABLE is table of SPREAD_FACTOR%rowtype index by PLS_INTEGER;
	G_SPREAD_FACTORS SPREAD_FACTOR_TABLE;

	type MORTALITY_CURVE_POINTS_TABLE is table of MORTALITY_CURVE_POINTS%rowtype index by PLS_INTEGER;
	type MORTALITY_CURVE is table of MORTALITY_CURVE_POINTS_TABLE index by PLS_INTEGER;
	G_MORTALITY_CURVE MORTALITY_CURVE;

	type PP_CALENDAR_TABLE is table of PP_CALENDAR%rowtype index by PLS_INTEGER;
	G_PP_CALENDAR PP_CALENDAR_TABLE;

	type RESERVE_RATIO_TAB is table of RESERVE_RATIOS.RESERVE_RATIO_ID%TYPE index by VARCHAR2(100);
	G_RESERVE_RATIOS RESERVE_RATIO_TAB;

	G_VERSION_ID number;
	G_START_MONTH date;
	G_END_MONTH date;



	procedure P_SET_MONTHS(A_VERSION_ID number, A_START_MONTH date, A_END_MONTH date)
	is
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.P_SET_MONTHS');

		G_VERSION_ID := A_VERSION_ID;
		G_START_MONTH := A_START_MONTH;
		G_END_MONTH := A_END_MONTH;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	end P_SET_MONTHS;


	procedure P_FILL_UOP_TABLES(A_VERSION_ID number, A_START_MONTH date, A_END_MONTH date)
	is
		CURRENT_MONTH date;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.P_FILL_UOP_TABLES');

		PKG_PP_LOG.P_WRITE_MESSAGE('Populating FCST_DEPR_METHOD_UOP and FCST_DEPR_GROUP_UOP tables with missing data');

		CURRENT_MONTH := A_START_MONTH;
		while CURRENT_MONTH < A_END_MONTH loop
			--insert missing data into FCST_DEPR_METHOD_UOP
			insert into FCST_DEPR_METHOD_UOP (FCST_DEPR_METHOD_ID, FCST_DEPR_VERSION_ID,
				SET_OF_BOOKS_ID, GL_POST_MO_YR,
				PRODUCTION, ESTIMATED_PRODUCTION,
				PRODUCTION_2, ESTIMATED_PRODUCTION_2,
				MIN_CALC_OPTION, UOP_TYPE_ID, UOP_TYPE_ID2)
			select UOP.FCST_DEPR_METHOD_ID, UOP.FCST_DEPR_VERSION_ID,
				UOP.SET_OF_BOOKS_ID, Add_Months(UOP.GL_POST_MO_YR,1),
				nvl(UOP.PRODUCTION,0), Nvl(Decode(STG.NET_GROSS, 1, UOP.ESTIMATED_PRODUCTION - UOP.PRODUCTION, UOP.ESTIMATED_PRODUCTION),0),
				nvl(UOP.PRODUCTION_2,0), Nvl(Decode(STG.NET_GROSS, 1, UOP.ESTIMATED_PRODUCTION_2 - UOP.PRODUCTION_2, UOP.ESTIMATED_PRODUCTION_2),0),
				UOP.MIN_CALC_OPTION, UOP.UOP_TYPE_ID, UOP.UOP_TYPE_ID2
			from FCST_DEPR_METHOD_UOP UOP
				inner join DEPR_CALC_STG STG
					on UOP.FCST_DEPR_METHOD_ID = STG.DEPR_METHOD_ID
					and UOP.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
			where UOP.GL_POST_MO_YR = CURRENT_MONTH
			and STG.GL_POST_MO_YR = CURRENT_MONTH
			and UOP.FCST_DEPR_VERSION_ID = A_VERSION_ID
			and STG.FCST_DEPR_VERSION_ID = A_VERSION_ID
			and STG.MID_PERIOD_METHOD = 'uop'
			and not exists (
				select 1
				from FCST_DEPR_METHOD_UOP UOP2
				where UOP2.FCST_DEPR_METHOD_ID = UOP.FCST_DEPR_METHOD_ID
				and UOP2.SET_OF_BOOKS_ID = UOP.SET_OF_BOOKS_ID
				and UOP2.FCST_DEPR_VERSION_ID = A_VERSION_ID
				and UOP2.GL_POST_MO_YR = Add_Months(CURRENT_MONTH, 1));

			--insert missing data into FCST_DEPR_GROUP_UOP
			insert into FCST_DEPR_GROUP_UOP (FCST_DEPR_GROUP_ID, FCST_DEPR_VERSION_ID,
				SET_OF_BOOKS_ID, GL_POST_MO_YR, FCST_DEPR_METHOD_ID,
				MIN_ANNUAL_EXPENSE, MAX_ANNUAL_EXPENSE)
			select UOP.FCST_DEPR_GROUP_ID, UOP.FCST_DEPR_VERSION_ID,
				UOP.SET_OF_BOOKS_ID, Add_Months(UOP.GL_POST_MO_YR,1), UOP.FCST_DEPR_METHOD_ID,
				UOP.MIN_ANNUAL_EXPENSE, UOP.MAX_ANNUAL_EXPENSE
			from FCST_DEPR_GROUP_UOP UOP
				inner join DEPR_CALC_STG STG
					on UOP.FCST_DEPR_GROUP_ID = STG.DEPR_GROUP_ID
					and UOP.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
			where UOP.GL_POST_MO_YR = CURRENT_MONTH
			and STG.GL_POST_MO_YR = CURRENT_MONTH
			and UOP.FCST_DEPR_VERSION_ID = A_VERSION_ID
			and STG.FCST_DEPR_VERSION_ID = A_VERSION_ID
			and STG.MID_PERIOD_METHOD = 'uop'
			and not exists (
				select 1
				from FCST_DEPR_GROUP_UOP UOP2
				where UOP2.FCST_DEPR_GROUP_ID = UOP.FCST_DEPR_GROUP_ID
				and UOP2.SET_OF_BOOKS_ID = UOP.SET_OF_BOOKS_ID
				and UOP2.FCST_DEPR_VERSION_ID = A_VERSION_ID
				and UOP2.GL_POST_MO_YR = Add_Months(CURRENT_MONTH, 1));

			--increment our current month
			CURRENT_MONTH := Add_Months(CURRENT_MONTH, 1);
		end loop;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	end P_FILL_UOP_TABLES;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
	function F_AUTO_RET_AMT_GROSS(	A_YR_BEGIN_BALANCE number,
									A_FACTOR_ID number,
									A_RET_PCT number,
									A_PREV_RETS number,
									A_FISCAL_MONTH number)  return number is
		L_SPREAD_FACTOR SPREAD_FACTOR%rowtype;
		L_RET_AMT number;
		L_MO_NUM number;
		L_FACTOR number;
		L_FACTOR_SUM number;

	begin
		--I'm purposely not adding these functions to the call stack since they'll be called very many times in the model statement, all in parallel

		--check if we already have our spread factors
		if not G_SPREAD_FACTORS.exists(A_FACTOR_ID) then
			--get the factors
			select *
			into L_SPREAD_FACTOR
			from SPREAD_FACTOR
			where FACTOR_ID = A_FACTOR_ID;

			G_SPREAD_FACTORS(A_FACTOR_ID) := L_SPREAD_FACTOR;
		end if;


		case A_FISCAL_MONTH
			when 1 then
				L_FACTOR := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_1;
				L_FACTOR_SUM := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_1 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_2 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_3
						+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_4 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_5 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_6
						+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_7 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_8 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_9
						+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_10 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_11 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
			when 2 then
				L_FACTOR := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_2;
				L_FACTOR_SUM := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_2 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_3
						+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_4 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_5 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_6
						+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_7 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_8 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_9
						+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_10 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_11 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
			when 3 then
				L_FACTOR := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_3;
				L_FACTOR_SUM :=  G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_3 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_4 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_5
								+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_6 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_7 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_8
								+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_9 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_10
								+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_11 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
			when 4 then
				L_FACTOR := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_4;
				L_FACTOR_SUM :=  G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_4 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_5 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_6
						+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_7 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_8 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_9
						+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_10 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_11 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
			when 5 then
				L_FACTOR := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_5;
				L_FACTOR_SUM :=  G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_5 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_6 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_7
								+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_8 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_9
								+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_10 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_11
								+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
			when 6 then
				L_FACTOR := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_6;
				L_FACTOR_SUM := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_6 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_7 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_8
								+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_9 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_10
								+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_11 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
			when 7 then
				L_FACTOR := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_7;
				L_FACTOR_SUM := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_7 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_8 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_9
							+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_10 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_11 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
			when 8 then
				L_FACTOR := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_8;
				L_FACTOR_SUM := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_8 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_9 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_10
								+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_11 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
			when 9 then
				L_FACTOR := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_9;
				L_FACTOR_SUM := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_9 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_10 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_11
								+ G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
			when 10 then
				L_FACTOR := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_10;
				L_FACTOR_SUM := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_10 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_11 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
			when 11 then
				L_FACTOR := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_11;
				L_FACTOR_SUM := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_11 + G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
			when 12 then
				L_FACTOR := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
				L_FACTOR_SUM := G_SPREAD_FACTORS(A_FACTOR_ID).FACTOR_12;
		end case;

		if L_FACTOR_SUM = 0 then
			L_FACTOR_SUM := 1;
		end if;

		L_RET_AMT := (L_FACTOR/L_FACTOR_SUM) * ((A_YR_BEGIN_BALANCE * A_RET_PCT) + A_PREV_RETS);

		return L_RET_AMT;
	end F_AUTO_RET_AMT_GROSS;

	function F_AUTO_RET_AMT_ADDITIONS(	A_ADDITIONS number,
										A_RET_PCT number) return number is
	begin
		--I'm purposely not adding these functions to the call stack since they'll be called very many times in the model statement, all in parallel
		return A_ADDITIONS * A_RET_PCT;
	end F_AUTO_RET_AMT_ADDITIONS;

	function F_AUTO_RET_AMT_CURVE(A_YR_BEGIN_BALANCE number,
									A_FACTOR_ID number,
									A_PREV_RETS number,
									A_MONTH date,
									A_FCST_DEPR_GROUP_ID number,
									A_SET_OF_BOOKS_ID number,
									A_FCST_VERSION number,
									A_VINTAGE number,
									A_DEPR_METHOD_ID number,
									A_EXPECTED_AVERAGE_LIFE number,
									A_END_OF_LIFE number,
									A_RESERVE_RATIO_ID number,
									A_ALLOCATION_PROCEDURE varchar2,
									A_MORTALITY_CURVE_ID number
									) return number is
		L_MORTALITY_PCT number;
		L_FISCAL_MONTH number;
		L_DMR_RR_REC DMR_RR_REC;
		L_RES_RATIO_MATH_TAB RES_RATIO_MATH_TABLE;
		L_INDEX number;
		L_THEO number;
		L_HASH_MONTH number;
	begin
		--I'm purposely not adding these functions to the call stack since they'll be called very many times in the model statement, all in parallel

		if nvl(A_MORTALITY_CURVE_ID, 0) = 0 or nvl(A_EXPECTED_AVERAGE_LIFE, 0) = 0 then
			return 0;
		end if;

		L_HASH_MONTH := to_number(to_char(A_MONTH,'yyyymm'));

		if not G_PP_CALENDAR.exists(L_HASH_MONTH) then
			for R in (select * from PP_CALENDAR) loop
				G_PP_CALENDAR(to_number(to_char(R.MONTH,'yyyymm'))).FISCAL_MONTH := R.FISCAL_MONTH;
			end loop;
		end if;

		L_FISCAL_MONTH := G_PP_CALENDAR(L_HASH_MONTH).FISCAL_MONTH;

		--load a DMR record
		L_DMR_RR_REC.DMR_ID := A_DEPR_METHOD_ID;
		L_DMR_RR_REC.SOB_ID := A_SET_OF_BOOKS_ID;
		L_DMR_RR_REC.MORTALITY_CURVE_ID := A_MORTALITY_CURVE_ID;
		L_DMR_RR_REC.EXPECTED_AVERAGE_LIFE := A_EXPECTED_AVERAGE_LIFE;
		L_DMR_RR_REC.END_OF_LIFE := A_END_OF_LIFE;
		L_DMR_RR_REC.RESERVE_RATIO_ID := A_RESERVE_RATIO_ID;
		L_DMR_RR_REC.ALLOCATION_PROCEDURE := A_ALLOCATION_PROCEDURE;
		L_DMR_RR_REC.MONTH := A_MONTH;

		L_RES_RATIO_MATH_TAB := F_BUILD_RESERVE_RATIOS_BROAD(L_DMR_RR_REC);

		-- FIND MORTALITY_PCT
		--L_INDEX := floor(100/L_DMR_RR_REC.EXPECTED_AVERAGE_LIFE) * ((year(A_MONTH) + L_THEO) - A_VINTAGE);
		L_INDEX := to_number(to_char(A_MONTH, 'yyyy')) - A_VINTAGE;

		if L_INDEX < 1 then
			return 0;
		end if;

		if L_INDEX > L_RES_RATIO_MATH_TAB.COUNT then
			L_MORTALITY_PCT := 1;
		elsif L_RES_RATIO_MATH_TAB(L_INDEX).SURV_PCT1 <> 0 then
			L_MORTALITY_PCT := (L_RES_RATIO_MATH_TAB(L_INDEX).SURV_PCT1 - L_RES_RATIO_MATH_TAB(L_INDEX).SURV_PCT2)
								/L_RES_RATIO_MATH_TAB(L_INDEX).SURV_PCT1;
		else
			L_MORTALITY_PCT := 1;
		end if;

		if G_DEBUG then
			PKG_PP_LOG.P_WRITE_MESSAGE('INDEX = '||to_char(L_INDEX)||', '||
										'MAX AGE = '||to_char(L_RES_RATIO_MATH_TAB.COUNT)||', '||
										'BEG YEAR_BALANCE = '||to_char(A_YR_BEGIN_BALANCE)||', '||
										'SPREAD FACTOR = '||to_char(A_FACTOR_ID)||', '||
										'RETIREMENT PCT = '||to_char(L_MORTALITY_PCT)||', '||
										'CUMULATIVE RETS = '||to_char(A_PREV_RETS)
										);
		end if;

		return F_AUTO_RET_AMT_GROSS(A_YR_BEGIN_BALANCE, A_FACTOR_ID, L_MORTALITY_PCT, A_PREV_RETS, L_FISCAL_MONTH);
	end F_AUTO_RET_AMT_CURVE;


	/*
	*	THIS FUNCTION builds out the begin balances by set of books / depr group
	*/
	procedure P_GET_BEGIN_BALANCES(A_FCST_DEPR_VERSION FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%type, A_START_MONTH date, A_FISCAL_START_DATE date,
						A_AFTER_INITIAL_LOAD boolean)
	is
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.P_GET_BEGIN_BALANCES');

		delete from DEPR_BEG_BALS;

		if A_AFTER_INITIAL_LOAD then
			insert into DEPR_BEG_BALS
			(DEPR_GROUP_ID, SET_OF_BOOKS_ID, BEGIN_BALANCE, CUM_RETIREMENTS)
			select FDL.FCST_DEPR_GROUP_ID, FDL.SET_OF_BOOKS_ID, sum(FDL.ACCUM_COST), 0
			from FCST_DEPR_VINTAGE_SUMMARY FDL, depr_fcst_group_stg tt
			where FDL.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
			and FDL.FCST_DEPR_GROUP_ID = tt.FCST_DEPR_GROUP_ID
			and FDL.ACCOUNTING_MONTH = add_months(A_START_MONTH, -1)
			group by FDL.FCST_DEPR_GROUP_ID, FDL.SET_OF_BOOKS_ID
			;
		else
			-- IT IS BUILT FOR THE INITIAL TIME FOR FIFO...
		-- get the beginning year balance and the total retirements to date
		-- for the starting point
		-- FDL are the records in forecast depr ledger for the beginning of the fiscal year
		-- FLDA are the records in forecast depr ledger for all activity between the start of the fiscal year
		-- and the start month
		insert into DEPR_BEG_BALS
		(DEPR_GROUP_ID, SET_OF_BOOKS_ID, BEGIN_BALANCE, CUM_RETIREMENTS)
		select FDL.FCST_DEPR_GROUP_ID, FDL.SET_OF_BOOKS_ID,
				FDL.BEGIN_BALANCE, nvl((select sum(FDLA.RETIREMENTS)
									from FCST_DEPR_LEDGER FDLA
									where FDLA.GL_POST_MO_YR between A_FISCAL_START_DATE and add_months(A_START_MONTH,-1)
									and FDLA.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
									and FDLA.FCST_DEPR_GROUP_ID = FDL.FCST_DEPR_GROUP_ID
									and FDLA.SET_OF_BOOKS_ID = FDL.SET_OF_BOOKS_ID
									), 0)
		from FCST_DEPR_LEDGER FDL, depr_fcst_group_stg tt
		where FDL.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
		and FDL.FCST_DEPR_GROUP_ID = tt.FCST_DEPR_GROUP_ID
		and FDL.GL_POST_MO_YR = A_FISCAL_START_DATE
		;
		end if;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
	end P_GET_BEGIN_BALANCES;

	/*
	*	THIS FUNCTION returns the start of the fiscal year
	*/
	function F_GET_FISCAL_MONTH(A_START_MONTH DATE)
	return date
	is
		L_FISCAL_MONTH	date;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.F_GET_FISCAL_MONTH');

		--get the fiscal month
		select FISCAL_START.MONTH
		into L_FISCAL_MONTH
		from PP_CALENDAR CURRENT_MONTH, PP_CALENDAR FISCAL_START
		where CURRENT_MONTH.MONTH = A_START_MONTH
		and FISCAL_START.FISCAL_YEAR = CURRENT_MONTH.FISCAL_YEAR
		and FISCAL_START.FISCAL_MONTH = 1;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
		return L_FISCAL_MONTH;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
	end F_GET_FISCAL_MONTH;




	function F_GET_RESERVE_RATIO_ID(A_DMR_REC in out DMR_RR_REC) return number is
		RTN number;
		L_MAX_REM_LIFE number;
		L_INDEX varchar2(100);
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.F_GET_RESERVE_RATIO_ID');

		if A_DMR_REC.END_OF_LIFE is null then
			L_MAX_REM_LIFE := -1;
		else
			L_MAX_REM_LIFE := greatest(round(A_DMR_REC.END_OF_LIFE/100) - to_number(to_char(A_DMR_REC.MONTH,'yyyy')), 0);
		end if;

		L_INDEX := A_DMR_REC.ALLOCATION_PROCEDURE || lpad(to_char(L_MAX_REM_LIFE), 22, '0')
					|| lpad(to_char(A_DMR_REC.MORTALITY_CURVE_ID), 22, '0') || lpad(to_char(A_DMR_REC.EXPECTED_AVERAGE_LIFE), 22, '0');

		if G_RESERVE_RATIOS.exists(L_INDEX) then
			RTN := 1;
		else
			select nvl(min(reserve_ratio_id), -1)
			into RTN
			from reserve_ratios
			where MORTALITY_CURVE_ID = A_DMR_REC.MORTALITY_CURVE_ID
			and expected_average_life = A_DMR_REC.EXPECTED_AVERAGE_LIFE
			and integer_rem_life = L_MAX_REM_LIFE
			and allocation_procedure = A_DMR_REC.ALLOCATION_PROCEDURE
			;

			if RTN > 0 then
				G_RESERVE_RATIOS(L_INDEX) := RTN;
				RTN := 1;
			else
				G_RESERVE_RATIOS(L_INDEX) := RESERVE_RATIOS_SEQ.nextval;
			end if;
		end if;

		if G_DEBUG_DEEP then
			PKG_PP_LOG.P_WRITE_MESSAGE('MORTALITY_CURVE_ID = '||to_char(A_DMR_REC.MORTALITY_CURVE_ID)||', '||
										'EXPECTED_AVERAGE_LIFE = '||to_char(A_DMR_REC.EXPECTED_AVERAGE_LIFE)||', '||
										'L_MAX_REM_LIFE = '||to_char(L_MAX_REM_LIFE)||', '||
										'ALLOCATION_PROCEDURE = '||to_char(A_DMR_REC.ALLOCATION_PROCEDURE)||', '||
										'RTN = '||to_char(RTN)||', '||
										'RESERVE_RATIO_ID = '||to_char(G_RESERVE_RATIOS(L_INDEX)));
		end if;

		A_DMR_REC.RESERVE_RATIO_ID := G_RESERVE_RATIOS(L_INDEX);

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
		return RTN;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end F_GET_RESERVE_RATIO_ID;

    function F_BUILD_RESERVE_RATIOS_BROAD(A_DMR_REC  DMR_RR_REC) return RES_RATIO_MATH_TABLE is
		CURVE_ARRAY MORTALITY_CURVE_POINTS_TABLE;
		L_RATIO		number;
		L_TRUNCATE_POINT number;
		L_POINT_BEFORE_TRUNCATE number;
		L_POINT_AFTER_TRUNCATE number;
		L_EXPECTED_AVG_LIFE number;
		L_END_OF_LIFE number;
		L_THEO_CONVENTION number;
		L_RESERVE_RATIO_ID number;
		L_MORTALITY_CURVE_ID number;
		MAX_AGE number;
		L_MAX_REM_LIFE number;
		RES_RATIO_MATH_TAB RES_RATIO_MATH_TABLE;
	begin

		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.F_BUILD_RESERVE_RATIOS_BROAD');

		--Get the 'Theo reserve ratios convention' system control here. It seems its not in our network DBs, so I'm defaulting for now
		L_THEO_CONVENTION := .5;

		L_EXPECTED_AVG_LIFE := A_DMR_REC.EXPECTED_AVERAGE_LIFE;
		L_END_OF_LIFE := A_DMR_REC.END_OF_LIFE;
		L_MORTALITY_CURVE_ID := A_DMR_REC.MORTALITY_CURVE_ID;
		L_RESERVE_RATIO_ID := A_DMR_REC.RESERVE_RATIO_ID;
		if nvl(L_END_OF_LIFE, 0) = 0 then
			L_MAX_REM_LIFE := -1;
		else
			L_MAX_REM_LIFE := greatest(round(L_END_OF_LIFE/100) - to_number(to_char(A_DMR_REC.MONTH,'yyyy')), 0);
		end if;

		--Here's where we get the correct reserve ratio id
		if not G_MORTALITY_CURVE.exists(A_DMR_REC.MORTALITY_CURVE_ID) then
			select *
			bulk collect into CURVE_ARRAY
			from MORTALITY_CURVE_POINTS
			where MORTALITY_CURVE_POINTS.MORTALITY_CURVE_ID = A_DMR_REC.MORTALITY_CURVE_ID
			order by MORTALITY_CURVE_POINTS.DATA_POINT asc;

			CURVE_ARRAY(CURVE_ARRAY.LAST).REM_LIFE_PERCENTAGE := 0;
			CURVE_ARRAY(CURVE_ARRAY.LAST).SURVIVING_PERCENTAGE := 0;

			G_MORTALITY_CURVE(A_DMR_REC.MORTALITY_CURVE_ID) := CURVE_ARRAY;
		else
			CURVE_ARRAY := G_MORTALITY_CURVE(A_DMR_REC.MORTALITY_CURVE_ID);
		end if;

		MAX_AGE := ceil(CURVE_ARRAY.count/(100/L_EXPECTED_AVG_LIFE));

		--when we access CURVE_ARRAY we are using a (n - 1) offset since arrays in Oracle start at 0 but start at 1 in PB
		for j in 0 .. MAX_AGE loop
			if j <> 0 then --roll fwd the points
				RES_RATIO_MATH_TAB(j).POINT_BEFORE1 := RES_RATIO_MATH_TAB(j - 1).POINT_BEFORE2;
				RES_RATIO_MATH_TAB(j).POINT_AFTER1 := RES_RATIO_MATH_TAB(j - 1).POINT_AFTER2;
				RES_RATIO_MATH_TAB(j).LOOKUP_POINT1 := RES_RATIO_MATH_TAB(j - 1).LOOKUP_POINT2;
				RES_RATIO_MATH_TAB(j).REM_LIFE_PCT1 := RES_RATIO_MATH_TAB(j - 1).REM_LIFE_PCT2;
				RES_RATIO_MATH_TAB(j).SURV_PCT1 := RES_RATIO_MATH_TAB(j - 1).SURV_PCT2;
				RES_RATIO_MATH_TAB(j).TRUNC_RL_PCT1 := RES_RATIO_MATH_TAB(j - 1).TRUNC_RL_PCT2;
				RES_RATIO_MATH_TAB(j).TRUNC_SV_PCT1 := RES_RATIO_MATH_TAB(j - 1).TRUNC_SV_PCT2;
			end if;

			RES_RATIO_MATH_TAB(j).LOOKUP_POINT2 := (100/L_EXPECTED_AVG_LIFE) * (j + L_THEO_CONVENTION) + 1;
			RES_RATIO_MATH_TAB(j).POINT_BEFORE2 := floor(RES_RATIO_MATH_TAB(j).LOOKUP_POINT2);
			RES_RATIO_MATH_TAB(j).POINT_AFTER2 := RES_RATIO_MATH_TAB(j).POINT_BEFORE2 + 1;

			if RES_RATIO_MATH_TAB(j).POINT_AFTER2 <= CURVE_ARRAY.count then
				if RES_RATIO_MATH_TAB(j).POINT_BEFORE2 = 0 then --INTERPOLATE
					RES_RATIO_MATH_TAB(j).REM_LIFE_PCT2 := 100;
					RES_RATIO_MATH_TAB(j).SURV_PCT2 := 1;
				else
					L_RATIO := (RES_RATIO_MATH_TAB(j).LOOKUP_POINT2 - RES_RATIO_MATH_TAB(j).POINT_BEFORE2)
								/ (RES_RATIO_MATH_TAB(j).POINT_AFTER2 - RES_RATIO_MATH_TAB(j).POINT_BEFORE2);

					RES_RATIO_MATH_TAB(j).REM_LIFE_PCT2 := CURVE_ARRAY(RES_RATIO_MATH_TAB(j).POINT_BEFORE2).REM_LIFE_PERCENTAGE + L_RATIO
																* (CURVE_ARRAY(RES_RATIO_MATH_TAB(j).POINT_AFTER2).REM_LIFE_PERCENTAGE
																	- CURVE_ARRAY(RES_RATIO_MATH_TAB(j).POINT_BEFORE2).REM_LIFE_PERCENTAGE);

					RES_RATIO_MATH_TAB(j).SURV_PCT2 := CURVE_ARRAY(RES_RATIO_MATH_TAB(j).POINT_BEFORE2).SURVIVING_PERCENTAGE + L_RATIO
																* (CURVE_ARRAY(RES_RATIO_MATH_TAB(j).POINT_AFTER2).SURVIVING_PERCENTAGE
																	- CURVE_ARRAY(RES_RATIO_MATH_TAB(j).POINT_BEFORE2).SURVIVING_PERCENTAGE);
				end if;
			else --off the curve
				RES_RATIO_MATH_TAB(j).REM_LIFE_PCT2 := 0;
				RES_RATIO_MATH_TAB(j).SURV_PCT2 := 0;
			end if;

			if L_MAX_REM_LIFE = -1 then
				RES_RATIO_MATH_TAB(j).TRUNC_RL_PCT2 := 0;
				RES_RATIO_MATH_TAB(j).TRUNC_SV_PCT2 := 0;
			else
				if j = 0 then
					L_TRUNCATE_POINT := (100/L_EXPECTED_AVG_LIFE) * (L_MAX_REM_LIFE + L_THEO_CONVENTION) + 1;
					L_POINT_BEFORE_TRUNCATE := floor(L_TRUNCATE_POINT);
					L_POINT_AFTER_TRUNCATE := L_POINT_BEFORE_TRUNCATE + 1;

					if L_TRUNCATE_POINT <= CURVE_ARRAY.count then
						--interpolate
						if L_POINT_BEFORE_TRUNCATE = 0 then
							RES_RATIO_MATH_TAB(j).TRUNC_RL_PCT1 := 100;
							RES_RATIO_MATH_TAB(j).TRUNC_SV_PCT1 := 1;
						else
							L_RATIO := (L_TRUNCATE_POINT - L_POINT_BEFORE_TRUNCATE) / (L_POINT_AFTER_TRUNCATE - L_POINT_BEFORE_TRUNCATE);

							RES_RATIO_MATH_TAB(j).TRUNC_RL_PCT1 := CURVE_ARRAY(L_POINT_BEFORE_TRUNCATE).REM_LIFE_PERCENTAGE + L_RATIO
																		* (CURVE_ARRAY(L_POINT_AFTER_TRUNCATE).REM_LIFE_PERCENTAGE
																			- CURVE_ARRAY(L_POINT_BEFORE_TRUNCATE).REM_LIFE_PERCENTAGE);

							RES_RATIO_MATH_TAB(j).TRUNC_SV_PCT1 := CURVE_ARRAY(L_POINT_BEFORE_TRUNCATE).SURVIVING_PERCENTAGE + L_RATIO
																		* (CURVE_ARRAY(L_POINT_AFTER_TRUNCATE).SURVIVING_PERCENTAGE
																			- CURVE_ARRAY(L_POINT_BEFORE_TRUNCATE).SURVIVING_PERCENTAGE);
						end if;
					else --off the curve
						RES_RATIO_MATH_TAB(j).TRUNC_RL_PCT1 := 0;
						RES_RATIO_MATH_TAB(j).TRUNC_SV_PCT1 := 0;
					end if;
				end if;

				L_TRUNCATE_POINT := (100/L_EXPECTED_AVG_LIFE) * (L_MAX_REM_LIFE + j + 1) + 1;
				L_POINT_BEFORE_TRUNCATE := floor(L_TRUNCATE_POINT);
				L_POINT_AFTER_TRUNCATE := L_POINT_BEFORE_TRUNCATE + 1;

				if L_TRUNCATE_POINT <= CURVE_ARRAY.count then
				--INTERPOLATE
					if L_POINT_BEFORE_TRUNCATE = 0 then
						RES_RATIO_MATH_TAB(j).TRUNC_RL_PCT2 := 100;
						RES_RATIO_MATH_TAB(j).TRUNC_SV_PCT2 := 1;
					else
						L_RATIO := (L_TRUNCATE_POINT - L_POINT_BEFORE_TRUNCATE) / (L_POINT_AFTER_TRUNCATE - L_POINT_BEFORE_TRUNCATE);

						RES_RATIO_MATH_TAB(j).TRUNC_RL_PCT1 := CURVE_ARRAY(L_POINT_BEFORE_TRUNCATE).REM_LIFE_PERCENTAGE + L_RATIO
																		* (CURVE_ARRAY(L_POINT_BEFORE_TRUNCATE).REM_LIFE_PERCENTAGE
																			- CURVE_ARRAY(L_POINT_AFTER_TRUNCATE).REM_LIFE_PERCENTAGE);

						RES_RATIO_MATH_TAB(j).TRUNC_SV_PCT1 := CURVE_ARRAY(L_POINT_BEFORE_TRUNCATE).SURVIVING_PERCENTAGE + L_RATIO
																		* (CURVE_ARRAY(L_POINT_BEFORE_TRUNCATE).SURVIVING_PERCENTAGE
																			- CURVE_ARRAY(L_POINT_AFTER_TRUNCATE).SURVIVING_PERCENTAGE);
					end if;
				else
					RES_RATIO_MATH_TAB(j).TRUNC_RL_PCT2 := 0;
					RES_RATIO_MATH_TAB(j).TRUNC_SV_PCT2 := 0;
				end if;
			end if;

			RES_RATIO_MATH_TAB(j).AREA1 := RES_RATIO_MATH_TAB(j).REM_LIFE_PCT1 * RES_RATIO_MATH_TAB(j).SURV_PCT1/100;
			RES_RATIO_MATH_TAB(j).AREA1TRUNC := RES_RATIO_MATH_TAB(j).TRUNC_RL_PCT1 * RES_RATIO_MATH_TAB(j).TRUNC_SV_PCT1/100;
			RES_RATIO_MATH_TAB(j).AREA2 := RES_RATIO_MATH_TAB(j).REM_LIFE_PCT2 * RES_RATIO_MATH_TAB(j).SURV_PCT2/100;
			RES_RATIO_MATH_TAB(j).AREA2TRUNC := RES_RATIO_MATH_TAB(j).TRUNC_RL_PCT2 * RES_RATIO_MATH_TAB(j).TRUNC_SV_PCT2/100;

			if RES_RATIO_MATH_TAB(j).SURV_PCT1 = 0 then
				RES_RATIO_MATH_TAB(j).REMAINING_LIFE1 := 0;
				RES_RATIO_MATH_TAB(j).RESERVE_RATIO1 := 1;
			else
				RES_RATIO_MATH_TAB(j).REMAINING_LIFE1 := ((RES_RATIO_MATH_TAB(j).AREA1 - RES_RATIO_MATH_TAB(j).AREA1TRUNC)
															/ RES_RATIO_MATH_TAB(j).SURV_PCT1) * L_EXPECTED_AVG_LIFE;
				if RES_RATIO_MATH_TAB(j).AREA1TRUNC = 1 then
					RES_RATIO_MATH_TAB(j).RESERVE_RATIO1 := 1;
				else
					RES_RATIO_MATH_TAB(j).RESERVE_RATIO1 := 1 - (((RES_RATIO_MATH_TAB(j).AREA1 - RES_RATIO_MATH_TAB(j).AREA1TRUNC)
																		/RES_RATIO_MATH_TAB(j).SURV_PCT1)/(1 - RES_RATIO_MATH_TAB(j).AREA1TRUNC));
				end if;
			end if;

			if RES_RATIO_MATH_TAB(j).SURV_PCT2 = 0 then
				RES_RATIO_MATH_TAB(j).REMAINING_LIFE2 := 0;
				RES_RATIO_MATH_TAB(j).RESERVE_RATIO2 := 1;
			else
				if RES_RATIO_MATH_TAB(j).AREA1TRUNC >= RES_RATIO_MATH_TAB(j).AREA2 then
					RES_RATIO_MATH_TAB(j).REMAINING_LIFE2 := 0;
				else
					RES_RATIO_MATH_TAB(j).REMAINING_LIFE2 := ((RES_RATIO_MATH_TAB(j).AREA2 - RES_RATIO_MATH_TAB(j).AREA1TRUNC)
																	/ RES_RATIO_MATH_TAB(j).SURV_PCT2) * L_EXPECTED_AVG_LIFE;
				end if;
				if RES_RATIO_MATH_TAB(j).AREA1TRUNC = 1 then
					RES_RATIO_MATH_TAB(j).RESERVE_RATIO2 := 1;
				else
					RES_RATIO_MATH_TAB(j).RESERVE_RATIO2 := 1 - (((RES_RATIO_MATH_TAB(j).AREA2 - RES_RATIO_MATH_TAB(j).AREA1TRUNC)
																	/ RES_RATIO_MATH_TAB(j).SURV_PCT2) / (1 - RES_RATIO_MATH_TAB(j).AREA1TRUNC));
				end if;
			end if;

			RES_RATIO_MATH_TAB(j).MONTH_RATIO := (RES_RATIO_MATH_TAB(j).RESERVE_RATIO2 - RES_RATIO_MATH_TAB(j).RESERVE_RATIO1)/12;
			RES_RATIO_MATH_TAB(j).MONTH_REM_LIFE := (RES_RATIO_MATH_TAB(j).REMAINING_LIFE2 - RES_RATIO_MATH_TAB(j).REMAINING_LIFE1)/12;
			RES_RATIO_MATH_TAB(j).INTEGER_AGE := j;
			RES_RATIO_MATH_TAB(j).RESERVE_RATIO_ID := L_RESERVE_RATIO_ID;
			RES_RATIO_MATH_TAB(j).MORTALITY_CURVE_ID := L_MORTALITY_CURVE_ID;
			RES_RATIO_MATH_TAB(j).MAX_REM_LIFE := L_MAX_REM_LIFE;
			RES_RATIO_MATH_TAB(j).EXPECTED_AVG_LIFE := L_EXPECTED_AVG_LIFE;

			if G_DEBUG_DEEP then
				PKG_PP_LOG.P_WRITE_MESSAGE('Integer Age = '||to_char(j)||', MONTH_REM_LIFE = '||to_char(RES_RATIO_MATH_TAB(j).MONTH_REM_LIFE)||
											', MONTH_RATIO = '||to_char(RES_RATIO_MATH_TAB(j).MONTH_RATIO)||', RESERVE_RATIO_ID = '||to_char(RES_RATIO_MATH_TAB(j).RESERVE_RATIO_ID)||', MAX_REM_LIFE = '||to_char(RES_RATIO_MATH_TAB(j).MAX_REM_LIFE)||
											', MORTALITY_CURVE_ID = '||to_char(RES_RATIO_MATH_TAB(j).MORTALITY_CURVE_ID)||', EXPECTED_AVG_LIFE = '||to_char(RES_RATIO_MATH_TAB(j).EXPECTED_AVG_LIFE));
				PKG_PP_LOG.P_WRITE_MESSAGE('POINT_BEFORE1 = '||to_char(RES_RATIO_MATH_TAB(j).POINT_BEFORE1)||
											', POINT_AFTER1 = '||to_char(RES_RATIO_MATH_TAB(j).POINT_AFTER1)||
											', LOOKUP_POINT1 = '||to_char(RES_RATIO_MATH_TAB(j).LOOKUP_POINT1)||
											', REM_LIFE_PCT1 = '||to_char(RES_RATIO_MATH_TAB(j).REM_LIFE_PCT1)||
											', SURV_PCT1 = '||to_char(RES_RATIO_MATH_TAB(j).SURV_PCT1)||
											', TRUNC_RL_PCT1 = '||to_char(RES_RATIO_MATH_TAB(j).TRUNC_RL_PCT1)||
											', TRUNC_SV_PCT1 = '||to_char(RES_RATIO_MATH_TAB(j).TRUNC_SV_PCT1)||
											', POINT_BEFORE2 = '||to_char(RES_RATIO_MATH_TAB(j).POINT_BEFORE2)||
											', POINT_AFTER2 = '||to_char(RES_RATIO_MATH_TAB(j).POINT_AFTER2)||
											', LOOKUP_POINT2 = '||to_char(RES_RATIO_MATH_TAB(j).LOOKUP_POINT2)||
											', REM_LIFE_PCT2 = '||to_char(RES_RATIO_MATH_TAB(j).REM_LIFE_PCT2)||
											', SURV_PCT2 = '||to_char(RES_RATIO_MATH_TAB(j).SURV_PCT2)||
											', TRUNC_RL_PCT2 = '||to_char(RES_RATIO_MATH_TAB(j).TRUNC_RL_PCT2)||
											', TRUNC_SV_PCT2 = '||to_char(RES_RATIO_MATH_TAB(j).TRUNC_SV_PCT2)||
											', AREA1 = '||to_char(RES_RATIO_MATH_TAB(j).AREA1)||
											', AREA1TRUNC = '||to_char(RES_RATIO_MATH_TAB(j).AREA1TRUNC)||
											', AREA2 = '||to_char(RES_RATIO_MATH_TAB(j).AREA2)||
											', AREA2TRUNC = '||to_char(RES_RATIO_MATH_TAB(j).AREA2TRUNC)||
											', REMAINING_LIFE1 = '||to_char(RES_RATIO_MATH_TAB(j).REMAINING_LIFE1)||
											', REMAINING_LIFE2 = '||to_char(RES_RATIO_MATH_TAB(j).REMAINING_LIFE2)||
											', RESERVE_RATIO1 = '||to_char(RES_RATIO_MATH_TAB(j).RESERVE_RATIO1)||
											', RESERVE_RATIO2 = '||to_char(RES_RATIO_MATH_TAB(j).RESERVE_RATIO2));


			end if;

		end loop; --Looping over one integer age for this depr method

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
		return RES_RATIO_MATH_TAB;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation | '||sqlerrm);
	end F_BUILD_RESERVE_RATIOS_BROAD;

   	procedure P_BUILD_VINTAGE_SUMMARY_MAIN( 	A_FCST_DEPR_VERSION FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
										A_START_MONTH date,
										A_END_MONTH date) is
		L_FDVS FCST_DVS_TAB;
	begin
		/*
		||============================================================================
		|| Object Name: P_BUILD_VINTAGE_SUMMARY_MAIN
		|| Description: Builds the vintaged data by month from fcst_depr_ledger
		||		Handles all but FIFO retirements
		||
		||	Arguments:
		||               A_FCST_DEPR_VERSION: FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE: The forecasted depreciation version to build the depr vintage summary
		||               A_START_MONTH: date: the start date for forecasting depreciation
		||               A_END_MONTH: date: the start date for forecasting depreciation
		||	Return: NONE
		||
		||============================================================================
		|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
		||============================================================================
		|| Version Date       Revised By     Reason for Change
		|| ------- ---------- -------------- -----------------------------------------
		|| 1.0     02/05/2014 Kyle Peterson    Original Version
		||============================================================================
		*/

		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.P_BUILD_VINTAGE_SUMMARY_MAIN');
		PKG_PP_LOG.P_WRITE_MESSAGE('STARTING to build fcst_depr_vintage_summary');

		select *
		bulk collect
		into L_FDVS
		from
		(
			select ID, FCST_DEPR_VERSION_ID, SET_OF_BOOKS_ID, FCST_DEPR_GROUP_ID, GL_POST_MO_YR, VINTAGE,
				MY_ACCUM_COST, MY_ADDITIONS, MY_TRANSFERS_IN, MY_TRANSFERS_OUT, MY_ADJUSTMENTS, MY_RETIREMENTS
			from
			(
				select *
				from
				(
					select FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, FCST_DEPR_VERSION_ID, GL_POST_MO_YR,
							  VINTAGE, ACCUM_COST, ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS,
							  SORTER, ROW_NUMBER() over(partition by FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, FCST_DEPR_VERSION_ID, VINTAGE
														order by GL_POST_MO_YR, SORTER) as THE_ROW
					from
					(
						select FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, FCST_DEPR_VERSION_ID, ACCOUNTING_MONTH as GL_POST_MO_YR,
							  VINTAGE, case when ACCOUNTING_MONTH = add_months(A_START_MONTH,-1)
									then ACCUM_COST
									else ACCUM_COST - ADDITIONS - RETIREMENTS - TRANSFERS_IN - TRANSFERS_OUT - ADJUSTMENTS
									end as ACCUM_COST,
							  ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, 1 as SORTER
						from (
								select VINTAGE,
										FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_GROUP_ID,
										SET_OF_BOOKS_ID, FCST_DEPR_VERSION_ID,
										nvl(ACCUM_COST, 0) as ACCUM_COST,
									 nvl(ADDITIONS, 0) ADDITIONS, nvl(RETIREMENTS,0) RETIREMENTS,
									 nvl(TRANSFERS_IN, 0) TRANSFERS_IN, nvl(TRANSFERS_OUT, 0) TRANSFERS_OUT, nvl(ADJUSTMENTS, 0) ADJUSTMENTS,
										ACCOUNTING_MONTH, row_number()
												over(partition by VINTAGE,
																FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_GROUP_ID,
																SET_OF_BOOKS_ID, FCST_DEPR_VERSION_ID
																order by ACCOUNTING_MONTH) as THE_ROW
								from FCST_DEPR_VINTAGE_SUMMARY, depr_fcst_group_stg tt
								where FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
								and FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_GROUP_ID = tt.fcst_depr_group_id
								and ACCOUNTING_MONTH between add_months(A_START_MONTH,-1) and A_END_MONTH
							) FIRST_MONTH_VINTAGE
						where FIRST_MONTH_VINTAGE.THE_ROW = 1
						union all
						-- Check for new vintages in the manual input table for
						select FCST_DEPR_VINTAGE_INPUT.FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID,
							FCST_DEPR_VERSION_ID, min(ACCOUNTING_MONTH) as GL_POST_MO_YR,
							  VINTAGE,
							 0 as ACCUM_COST,
							 0 ADDITIONS, 0 RETIREMENTS,
							 0 TRANSFERS_IN, 0 TRANSFERS_OUT, 0 ADJUSTMENTS, 2 as SORTER
						from FCST_DEPR_VINTAGE_INPUT, depr_fcst_group_stg tt
						where FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
						and FCST_DEPR_VINTAGE_INPUT.FCST_DEPR_GROUP_ID = tt.fcst_depr_group_id
						and ACCOUNTING_MONTH between A_START_MONTH and A_END_MONTH
						group by FCST_DEPR_VERSION_ID,
								FCST_DEPR_VINTAGE_INPUT.FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, VINTAGE
						union all
						-- CHECK for new additions in current vintage in fcst_depr_ledger
						select FDL.DEPR_GROUP_ID, FDL.SET_OF_BOOKS_ID, A_FCST_DEPR_VERSION, min(FDL.GL_POST_MO_YR) as GL_POST_MO_YR,
							  to_number(to_char(FDL.GL_POST_MO_YR, 'yyyy')),
							 0 as ACCUM_COST,
							 0 as ADDITIONS, 0 RETIREMENTS,
							 0 TRANSFERS_IN, 0 TRANSFERS_OUT, 0 ADJUSTMENTS, 3 as SORTER
						from DEPR_CALC_STG FDL
						where (ADDITIONS <> 0 OR TRANSFERS_IN <> 0 OR TRANSFERS_OUT  <> 0 OR ADJUSTMENTS <> 0 )
						group by fdl.DEPR_GROUP_ID, fdl.SET_OF_BOOKS_ID, to_number(to_char(FDL.GL_POST_MO_YR, 'yyyy'))
					)
				)
				where THE_ROW = 1
			)
			model
			--references manually input activity by vintage by depr_group, user enters this info through the system
			--referenced in the main model statement to get the total input activity for a given vintage
			--in order to correctly vintage FCST_DEPR_LEDGER
			reference CLIENT_INPUT on (select ACCOUNTING_MONTH, FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, FCST_DEPR_VERSION_ID,
										VINTAGE, ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS,
										nvl(ratio_to_report(FDVI.ADDITIONS) over(partition by FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, ACCOUNTING_MONTH),0)
											as RATIO_ADD,
										nvl(ratio_to_report(FDVI.RETIREMENTS) over(partition by FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, ACCOUNTING_MONTH),0)
											as RATIO_RET,
										nvl(ratio_to_report(FDVI.TRANSFERS_IN) over(partition by FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, ACCOUNTING_MONTH),0)
											as RATIO_TRT,
										nvl(ratio_to_report(FDVI.TRANSFERS_OUT) over(partition by FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, ACCOUNTING_MONTH),0)
											as RATIO_TRF,
										nvl(ratio_to_report(FDVI.ADJUSTMENTS) over(partition by FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, ACCOUNTING_MONTH),0)
											as RATIO_ADJ
										from FCST_DEPR_VINTAGE_INPUT FDVI
										where FDVI.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
										and FDVI.ACCOUNTING_MONTH between A_START_MONTH and A_END_MONTH)
				DIMENSION BY (FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, VINTAGE, ACCOUNTING_MONTH)
				MEASURES (ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, RATIO_ADD, RATIO_RET, RATIO_TRT, RATIO_TRF, RATIO_ADJ)
			reference FCST_DEPR_LEDGER on (select DEPR_GROUP_ID, SET_OF_BOOKS_ID, A_FCST_DEPR_VERSION, GL_POST_MO_YR,
											to_number(to_char(GL_POST_MO_YR, 'yyyy')) as VINTAGE,
											RETIREMENTS, ADDITIONS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS,
											DEPR_METHOD_ID, EXPECTED_AVERAGE_LIFE, END_OF_LIFE, RESERVE_RATIO_ID,
											ALLOCATION_PROCEDURE, MORTALITY_CURVE_ID, BEGIN_BALANCE, fiscalyearstart
											from DEPR_CALC_STG)
				DIMENSION BY (DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR)
				MEASURES (ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS,
							DEPR_METHOD_ID, EXPECTED_AVERAGE_LIFE, END_OF_LIFE, RESERVE_RATIO_ID,
							ALLOCATION_PROCEDURE, MORTALITY_CURVE_ID, BEGIN_BALANCE, fiscalyearstart)
			reference FCST_DEPR_VINT_PRIOR on (
									select
										ffff.FCST_DEPR_GROUP_ID,
										ffff.SET_OF_BOOKS_ID,
										ffff.ACCOUNTING_MONTH,
										sum(ACCUM_COST) as ACCUM_COST
									from FCST_DEPR_VINTAGE_SUMMARY ffff, depr_fcst_group_stg tt
									where ffff.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
									and ffff.FCST_DEPR_GROUP_ID = tt.fcst_depr_group_id
									and ffff.ACCOUNTING_MONTH between add_months(A_START_MONTH,-1) and A_END_MONTH
									group by ffff.FCST_DEPR_GROUP_ID, ffff.SET_OF_BOOKS_ID, ffff.ACCOUNTING_MONTH
									)
				DIMENSION BY (FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, ACCOUNTING_MONTH)
				MEASURES (FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, ACCOUNTING_MONTH, ACCUM_COST)
			reference UNVINTAGED_INPUT on (select ACCOUNTING_MONTH, FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID,
										  nvl(SUM(FDVI.ADDITIONS),0)  as SUM_ADD,
										  nvl(SUM(FDVI.RETIREMENTS),0) as SUM_RET,
										  nvl(SUM(FDVI.TRANSFERS_IN),0) as SUM_TRT,
										  nvl(SUM(FDVI.TRANSFERS_OUT),0) as SUM_TRF,
										  nvl(SUM(FDVI.ADJUSTMENTS),0) as SUM_ADJ
										  from FCST_DEPR_VINTAGE_INPUT FDVI
										  where FDVI.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
										and FDVI.ACCOUNTING_MONTH between A_START_MONTH and A_END_MONTH
										group by ACCOUNTING_MONTH, FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID)
				DIMENSION BY (FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, ACCOUNTING_MONTH)
				MEASURES (SUM_ADD, SUM_RET, SUM_TRT, SUM_TRF, SUM_ADJ)
			MAIN main_model
				partition by (FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, FCST_DEPR_VERSION_ID, VINTAGE)
				DIMENSION by (ROW_NUMBER() OVER(partition by FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, FCST_DEPR_VERSION_ID, VINTAGE order by GL_POST_MO_YR) as ID)
			MEASURES
			(
				ACCUM_COST, 0 as MY_ACCUM_COST,
				months_between( A_END_MONTH , add_months(A_START_MONTH,-1)) as NUM_ITERS,
				0 as MY_ADDITIONS, 0 as MY_TRANSFERS_IN, 0 as MY_TRANSFERS_OUT,
				0 as MY_ADJUSTMENTS, 0 as MY_RETIREMENTS, GL_POST_MO_YR, SORTER
			)
			RULES UPSERT ITERATE(100000) UNTIL ITERATION_NUMBER = NUM_ITERS[1]
			(
				MY_ACCUM_COST[0] = ACCUM_COST[ 1 ],

				GL_POST_MO_YR[ ITERATION_NUMBER + 1 ] = nvl(GL_POST_MO_YR[CV()], add_months(GL_POST_MO_YR[CV() - 1], 1)),

				MY_ADDITIONS[ ITERATION_NUMBER + 1 ] = case
					when CV(VINTAGE) = to_number(to_char(GL_POST_MO_YR[CV()],'yyyy')) then
						nvl(FCST_DEPR_LEDGER.ADDITIONS[ CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()] ], 0)
						+ nvl(CLIENT_INPUT.ADDITIONS[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),CV(VINTAGE),GL_POST_MO_YR[CV()] ],0)
						- nvl(UNVINTAGED_INPUT.sum_add[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()] ],0)
					else
						nvl(CLIENT_INPUT.ADDITIONS[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),CV(VINTAGE),GL_POST_MO_YR[CV()] ],0)
					end,

				MY_TRANSFERS_IN[ ITERATION_NUMBER + 1 ] = case
					when nvl(UNVINTAGED_INPUT.sum_trt[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()] ],0) = 0 then
						case
							when CV(VINTAGE) = to_number(to_char(GL_POST_MO_YR[CV()],'yyyy')) then
							--This condition means no manual input, take all xfer in DEPR_LEDGER to current vintage
								nvl(FCST_DEPR_LEDGER.TRANSFERS_IN[ CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()] ], 0)
							else --no current vintage, and no manual input
								0
						end
					else --manual input for this accounting period, allocate to vintages based on input
						nvl(CLIENT_INPUT.TRANSFERS_IN[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),CV(VINTAGE),GL_POST_MO_YR[CV()] ],0)
						+ ((nvl(FCST_DEPR_LEDGER.TRANSFERS_IN[ CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()] ], 0)
						- nvl(UNVINTAGED_INPUT.SUM_TRT[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()] ],0))
						* nvl(CLIENT_INPUT.RATIO_TRT[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),CV(VINTAGE),GL_POST_MO_YR[CV()] ],0))
					end,

				MY_TRANSFERS_OUT[ ITERATION_NUMBER + 1 ] = case
					when nvl(UNVINTAGED_INPUT.SUM_TRF[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()] ],0) = 0 then
						case
							when CV(VINTAGE) = to_number(to_char(GL_POST_MO_YR[CV()],'yyyy')) then --No manual input
							--take all xfer in DEPR_LEDGER to current vintage
								nvl(FCST_DEPR_LEDGER.TRANSFERS_OUT[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()] ], 0)
						else --no current vintage, and no manual input
							0
						end
					else --manual input for this accounting period, allocate to vintages based on input
						nvl(client_input.TRANSFERS_OUT[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),CV(VINTAGE),GL_POST_MO_YR[CV()] ],0)
						+ ((nvl(FCST_DEPR_LEDGER.TRANSFERS_OUT[ CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()] ], 0)
						- nvl(UNVINTAGED_INPUT.sum_trf[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()] ],0))
						* nvl(CLIENT_INPUT.RATIO_TRF[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),CV(VINTAGE),GL_POST_MO_YR[CV()] ],0))
					end,

				MY_ADJUSTMENTS[ ITERATION_NUMBER + 1 ] = case
					when nvl(UNVINTAGED_INPUT.SUM_ADJ[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()] ],0) = 0 then
						case
							when CV(VINTAGE) = to_number(to_char(GL_POST_MO_YR[CV()],'yyyy')) then --No manual input
							--take all xfer in DEPR_LEDGER to current vintage
								nvl(FCST_DEPR_LEDGER.ADJUSTMENTS[ CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()] ], 0)
							else --no current vintage, and no manual input
								0
							end
						else --manual input for this accounting period, allocate to vintages based on input
							nvl(CLIENT_INPUT.ADJUSTMENTS[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),CV(VINTAGE),GL_POST_MO_YR[CV()] ],0)
							+ ((nvl(FCST_DEPR_LEDGER.ADJUSTMENTS[ CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()]],0)
							- nvl(UNVINTAGED_INPUT.sum_adj[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),GL_POST_MO_YR[CV()]],0))
							* nvl(CLIENT_INPUT.RATIO_ADJ[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),CV(VINTAGE),GL_POST_MO_YR[CV()]],0))
						end,

				-- only use the auto retirement amount IF no retirement manually input for vintage
				MY_RETIREMENTS[ ITERATION_NUMBER + 1] = nvl(CLIENT_INPUT.RETIREMENTS[CV(FCST_DEPR_GROUP_ID),CV(SET_OF_BOOKS_ID),CV(VINTAGE),GL_POST_MO_YR[CV()] ],0),

				MY_ACCUM_COST[ ITERATION_NUMBER + 1 ] = MY_ACCUM_COST[CV() - 1] + MY_ADDITIONS[CV()] + MY_TRANSFERS_IN[CV()] + MY_TRANSFERS_OUT[CV()] + MY_ADJUSTMENTS[CV()] + MY_RETIREMENTS[CV()]
			)
		)
		where id > 0
		and GL_POST_MO_YR between A_START_MONTH and A_END_MONTH
		;

		forall ndx in indices of L_FDVS
			merge into FCST_DEPR_VINTAGE_SUMMARY A
			using
			(
				select
					L_FDVS(ndx).FCST_DEPR_VERSION_ID as FCST_DEPR_VERSION_ID, L_FDVS(ndx).FCST_DEPR_GROUP_ID as FCST_DEPR_GROUP_ID,
					L_FDVS(ndx).ACCOUNTING_MONTH as ACCOUNTING_MONTH, L_FDVS(ndx).VINTAGE as VINTAGE,
					L_FDVS(ndx).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID, L_FDVS(ndx).ACCUM_COST as ACCUM_COST,
					L_FDVS(ndx).ADDITIONS as ADDITIONS, L_FDVS(ndx).RETIREMENTS as RETIREMENTS,
					L_FDVS(ndx).TRANSFERS_IN as TRANSFERS_IN, L_FDVS(ndx).TRANSFERS_OUT as TRANSFERS_OUT,
					L_FDVS(ndx).ADJUSTMENTS as ADJUSTMENTS
				from dual
			) B
			on (A.FCST_DEPR_GROUP_ID = B.FCST_DEPR_GROUP_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.VINTAGE = B.VINTAGE
				and A.FCST_DEPR_VERSION_ID  = B.FCST_DEPR_VERSION_ID and A.ACCOUNTING_MONTH = B.ACCOUNTING_MONTH)
			when matched then update set
					A.ACCUM_COST = B.ACCUM_COST,
					A.ADDITIONS = B.ADDITIONS,
					A.RETIREMENTS = B.RETIREMENTS,
					A.TRANSFERS_IN = B.TRANSFERS_IN,
					A.TRANSFERS_OUT = B.TRANSFERS_OUT,
					A.ADJUSTMENTS = B.ADJUSTMENTS
				delete where (B.ACCUM_COST = 0 and B.ADDITIONS = 0
							and B.RETIREMENTS = 0 and B.TRANSFERS_IN = 0
							and B.TRANSFERS_OUT = 0 and B.ADJUSTMENTS = 0)
			when not matched then
				insert(FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, VINTAGE, ACCOUNTING_MONTH, FCST_DEPR_VERSION_ID, ACCUM_COST, ADDITIONS,
					RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS)
				values(B.FCST_DEPR_GROUP_ID, B.SET_OF_BOOKS_ID, B.VINTAGE, B.ACCOUNTING_MONTH,
					B.FCST_DEPR_VERSION_ID, B.ACCUM_COST, B.ADDITIONS, B.RETIREMENTS,
					B.TRANSFERS_IN, B.TRANSFERS_OUT, B.ADJUSTMENTS)
				where (B.ACCUM_COST <> 0 or B.ADDITIONS <> 0 or B.RETIREMENTS <> 0
					or B.TRANSFERS_IN <> 0 or B.TRANSFERS_OUT <> 0 or B.ADJUSTMENTS <> 0);

		PKG_PP_LOG.P_WRITE_MESSAGE('DONE building fcst_depr_vintage_summary');

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
	end P_BUILD_VINTAGE_SUMMARY_MAIN;

   	procedure P_LOAD_RESERVE_RATIOS_BROAD( 	A_DMR_REC DMR_RR_REC) is
		RES_RATIO_MATH_TAB RES_RATIO_MATH_TABLE;
	begin
		/*
		||============================================================================
		|| Object Name: P_LOAD_RESERVE_RATIOS_BROAD
		|| Description: Builds out reserve ratios for BROAD
		||
		||	Arguments:
		||               A_FCST_DEPR_VERSION: number: The forecasted depreciation version to build the depr vintage summary
		||               A_START_MONTH: date: the start date for forecasting depreciation
		||               A_END_MONTH: date: the start date for forecasting depreciation
		||	Return: NONE
		||
		||============================================================================
		|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
		||============================================================================
		|| Version Date       Revised By     Reason for Change
		|| ------- ---------- -------------- -----------------------------------------
		|| 1.0     02/05/2014 Kyle Peterson    Original Version
		||============================================================================
		*/

		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.P_LOAD_RESERVE_RATIOS_BROAD');

		RES_RATIO_MATH_TAB := F_BUILD_RESERVE_RATIOS_BROAD(A_DMR_REC);

		forall j in indices of RES_RATIO_MATH_TAB
			merge into RESERVE_RATIOS A
			using (select RES_RATIO_MATH_TAB(j).RESERVE_RATIO_ID as RESERVE_RATIO_ID, RES_RATIO_MATH_TAB(j).INTEGER_AGE as INTEGER_AGE,
							RES_RATIO_MATH_TAB(j).MORTALITY_CURVE_ID as MORTALITY_CURVE_ID, 'BROAD' as ALLOCATION_PROCEDURE,
							RES_RATIO_MATH_TAB(j).REMAINING_LIFE1 as REMAINING_LIFE, RES_RATIO_MATH_TAB(j).RESERVE_RATIO1 as RESERVE_RATIO,
							RES_RATIO_MATH_TAB(j).MONTH_RATIO as MONTH_RATIO, RES_RATIO_MATH_TAB(j).MAX_REM_LIFE as INTEGER_REM_LIFE,
							RES_RATIO_MATH_TAB(j).EXPECTED_AVG_LIFE as EXPECTED_AVERAGE_LIFE,
							RES_RATIO_MATH_TAB(j).MONTH_REM_LIFE as MONTH_REMAINING_LIFE from dual) B
			on (A.RESERVE_RATIO_ID = B.RESERVE_RATIO_ID and A.INTEGER_AGE = B.INTEGER_AGE)
			when matched then update
				set A.MORTALITY_CURVE_ID = B.MORTALITY_CURVE_ID,
					A.EXPECTED_AVERAGE_LIFE = B.EXPECTED_AVERAGE_LIFE,
					A.ALLOCATION_PROCEDURE = B.ALLOCATION_PROCEDURE,
					A.REMAINING_LIFE = B.REMAINING_LIFE,
					A.RESERVE_RATIO = B.RESERVE_RATIO,
					A.MONTH_RESERVE_RATIO = B.MONTH_RATIO,
					A.INTEGER_REM_LIFE = B.INTEGER_REM_LIFE,
					A.MONTH_REMAINING_LIFE = B.MONTH_REMAINING_LIFE
			when not matched then insert
				(RESERVE_RATIO_ID, INTEGER_AGE, INTEGER_REM_LIFE, MORTALITY_CURVE_ID, EXPECTED_AVERAGE_LIFE, ALLOCATION_PROCEDURE, REMAINING_LIFE,
				RESERVE_RATIO, MONTH_RESERVE_RATIO, MONTH_REMAINING_LIFE)
				values
				(B.RESERVE_RATIO_ID, B.INTEGER_AGE, B.INTEGER_REM_LIFE, B.MORTALITY_CURVE_ID, B.EXPECTED_AVERAGE_LIFE,
				B.ALLOCATION_PROCEDURE, B.REMAINING_LIFE, B.RESERVE_RATIO, B.MONTH_RATIO, B.MONTH_REMAINING_LIFE);



		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
	end P_LOAD_RESERVE_RATIOS_BROAD;

   	procedure P_LOAD_RESERVE_RATIOS_ELG( 	A_DMR_REC DMR_RR_REC) is

		type CURVE_ARRAY_TABLE is table of MORTALITY_CURVE_POINTS%rowtype index by pls_integer;
		CURVE_ARRAY CURVE_ARRAY_TABLE;
		j			number;
		L_RATIO		number;
		L_TRUNCATE_POINT number;
		L_EXPECTED_AVG_LIFE number;
		L_END_OF_LIFE number;
		L_THEO_CONVENTION number;
		L_RESERVE_RATIO_ID number;
		L_MORTALITY_CURVE_ID number;
		MAX_AGE number;
		L_MONTH_REM_LIFE PKG_PP_COMMON.NUM_TABTYPE;
		L_RESERVE_RATIO PKG_PP_COMMON.NUM_TABTYPE;
		L_REM_LIFE PKG_PP_COMMON.NUM_TABTYPE;
		L_PROB_LIFE PKG_PP_COMMON.NUM_TABTYPE;
		L_RET_PCT PKG_PP_COMMON.NUM_TABTYPE;
		L_AVG_AGE PKG_PP_COMMON.NUM_TABTYPE;
		L_ACCRUAL PKG_PP_COMMON.NUM_TABTYPE;
		L_TRUNC_ACCRUAL PKG_PP_COMMON.NUM_TABTYPE;
		L_FUTURE_ACCRUAL PKG_PP_COMMON.NUM_TABTYPE;
		L_MONTH_RATIO PKG_PP_COMMON.NUM_TABTYPE;
		L_INTEGER_AGE PKG_PP_COMMON.NUM_TABTYPE;
		L_TOTAL_FUTURE number;
		L_MAX_REM_LIFE number;
		L_NEED_AGE0 boolean := FALSE;
		type RES_RATIO_MATH_REC is record(
			POINT_BEFORE1 number := 1,
			POINT_AFTER1 number := 2,
			LOOKUP_POINT1 number := 1,
			SURV_PCT1 number := 1,
			POINT_BEFORE2 number,
			POINT_AFTER2 number,
			LOOKUP_POINT2 number,
			SURV_PCT2 number,
			INTEGER_AGE number);
		type RES_RATIO_MATH_TABLE is table of RES_RATIO_MATH_REC index by pls_integer;
		RES_RATIO_MATH_TAB RES_RATIO_MATH_TABLE;

	begin
		/*
		||============================================================================
		|| Object Name: P_LOAD_RESERVE_RATIOS_ELG
		|| Description: Builds out reserve ratios for ELG (Equal Life Group)
		||
		||	Arguments:
		||               A_FCST_DEPR_VERSION: number: The forecasted depreciation version to build the depr vintage summary
		||               A_START_MONTH: date: the start date for forecasting depreciation
		||               A_END_MONTH: date: the start date for forecasting depreciation
		||	Return: NONE
		||
		||============================================================================
		|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
		||============================================================================
		|| Version Date       Revised By     Reason for Change
		|| ------- ---------- -------------- -----------------------------------------
		|| 1.0     02/05/2014 Kyle Peterson    Original Version
		||============================================================================
		*/

		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.P_LOAD_RESERVE_RATIOS_ELG');

		--Get the 'Theo reserve ratios convention' system control here. It seems its not in our network DBs, so I'm defaulting for now
		L_THEO_CONVENTION := .5;

		--How do I know which curve to use?
		--What do I do if the curve isnt there?
		select *
		bulk collect into CURVE_ARRAY
		from MORTALITY_CURVE_POINTS
		where MORTALITY_CURVE_POINTS.MORTALITY_CURVE_ID = A_DMR_REC.MORTALITY_CURVE_ID
		order by MORTALITY_CURVE_POINTS.DATA_POINT asc;

		--validation first to make sure the expected avg life is there for the depr method rate?
		--TODO: Protect against no rows found
		L_EXPECTED_AVG_LIFE := A_DMR_REC.EXPECTED_AVERAGE_LIFE;
		L_END_OF_LIFE := A_DMR_REC.END_OF_LIFE;
		L_MORTALITY_CURVE_ID := A_DMR_REC.MORTALITY_CURVE_ID;
		L_RESERVE_RATIO_ID := A_DMR_REC.RESERVE_RATIO_ID;
		if L_END_OF_LIFE is null then
			L_MAX_REM_LIFE := -1;
		else
			L_MAX_REM_LIFE := greatest(round(L_END_OF_LIFE/100) - to_number(to_char(A_DMR_REC.MONTH,'yyyy')), 0);
		end if;
		/*and make sure its the right effective date, is it the first date prior to the start of the fcst run? related to when the fcst is run?*/

		MAX_AGE := ceil(CURVE_ARRAY.count/(100/L_EXPECTED_AVG_LIFE));

		for j in 0 .. MAX_AGE loop
			if j <> 0 then --roll fwd the points
				RES_RATIO_MATH_TAB(j).POINT_BEFORE1 := RES_RATIO_MATH_TAB(j - 1).POINT_BEFORE2;
				RES_RATIO_MATH_TAB(j).POINT_AFTER1 := RES_RATIO_MATH_TAB(j - 1).POINT_AFTER2;
				RES_RATIO_MATH_TAB(j).LOOKUP_POINT1 := RES_RATIO_MATH_TAB(j - 1).LOOKUP_POINT2;
				RES_RATIO_MATH_TAB(j).SURV_PCT1 := RES_RATIO_MATH_TAB(j - 1).SURV_PCT2;
			end if;

			RES_RATIO_MATH_TAB(j).LOOKUP_POINT2 := (100/L_EXPECTED_AVG_LIFE) * (j + L_THEO_CONVENTION) + 1;
			RES_RATIO_MATH_TAB(j).POINT_BEFORE2 := floor(RES_RATIO_MATH_TAB(j).LOOKUP_POINT2);
			RES_RATIO_MATH_TAB(j).POINT_AFTER2 := RES_RATIO_MATH_TAB(j).POINT_BEFORE2 + 1;

			if RES_RATIO_MATH_TAB(j).POINT_AFTER2 <= CURVE_ARRAY.count then
				if RES_RATIO_MATH_TAB(j).POINT_BEFORE2 = 0 then --INTERPOLATE
					RES_RATIO_MATH_TAB(j).SURV_PCT2 := 1;
				else
					L_RATIO := (RES_RATIO_MATH_TAB(j).LOOKUP_POINT2 - RES_RATIO_MATH_TAB(j).POINT_BEFORE2)
								/ (RES_RATIO_MATH_TAB(j).POINT_AFTER2 - RES_RATIO_MATH_TAB(j).POINT_BEFORE2);

					RES_RATIO_MATH_TAB(j).SURV_PCT2 := CURVE_ARRAY(RES_RATIO_MATH_TAB(j).POINT_BEFORE2).SURVIVING_PERCENTAGE + L_RATIO
																* (CURVE_ARRAY(RES_RATIO_MATH_TAB(j).POINT_AFTER2).SURVIVING_PERCENTAGE
																	- CURVE_ARRAY(RES_RATIO_MATH_TAB(j).POINT_BEFORE2).SURVIVING_PERCENTAGE);
				end if;
			else --off the curve
				RES_RATIO_MATH_TAB(j).SURV_PCT2 := 0;
			end if;

			--get avg surviving PCT
			CURVE_ARRAY(j + 1).SURVIVING_PERCENTAGE := (RES_RATIO_MATH_TAB(j).SURV_PCT1 - RES_RATIO_MATH_TAB(j).SURV_PCT2)/2;
			L_RET_PCT(j + 1) := RES_RATIO_MATH_TAB(j).SURV_PCT1 - RES_RATIO_MATH_TAB(j).SURV_PCT2;

			if j = 0 then
				L_AVG_AGE(1) := L_THEO_CONVENTION/2;
			else
				L_AVG_AGE(j+1) := ((j - 1 + L_THEO_CONVENTION) + (j + L_THEO_CONVENTION))/2;
			end if;

			if L_AVG_AGE(j + 1) > 0 then
				if L_AVG_AGE(j+1) < 1 then
					L_ACCRUAL(j+1) := L_RET_PCT(j+1);
					L_TRUNC_ACCRUAL(j+1) := CURVE_ARRAY(j + 1).SURVIVING_PERCENTAGE;
				else
					L_ACCRUAL(j + 1) := L_RET_PCT(j+1)/L_AVG_AGE(j+1);
					L_TRUNC_ACCRUAL(j+1) := CURVE_ARRAY(j + 1).SURVIVING_PERCENTAGE/L_AVG_AGE(j+1);
				end if;
			else
				L_ACCRUAL(j+1) := 0;
				L_TRUNC_ACCRUAL(j+1) := 0;
			end if;

		end loop; --Looping over one integer age for this depr method

		for j in REVERSE 1 .. L_ACCRUAL.count loop

			--current year weighting is incorporated in prior loop when building the accrual array so it is NOT needed here
			--divide by 2 is because you assume mid-period retirements whether it is age interval 1.5-2.5 or 2.0-3.0
			if j = L_ACCRUAL.count then
				L_FUTURE_ACCRUAL(j) := L_ACCRUAL(j)/2;
			elsif j = 1 then
				--need year weighting for the 1st period since it is age interval 0 -- L_THEO_CONVENTION could be 0-0 or 0-.5
				L_FUTURE_ACCRUAL(j) := L_ACCRUAL(j)/2 + (L_TOTAL_FUTURE * L_THEO_CONVENTION);
			else
				L_FUTURE_ACCRUAL(j) := L_ACCRUAL(j)/2 + L_TOTAL_FUTURE;
			end if;

			L_TOTAL_FUTURE := L_TOTAL_FUTURE + L_ACCRUAL(j);

		end loop;

		for j in 1 .. L_ACCRUAL.count loop
			if L_MAX_REM_LIFE <> - 1 then
			-- need to get truncated accrual from termination age plus sum of accruals to there
			-- we've been accumulating future accruals and need to subtract beyond truncation date
			-- a_max_rem_life is offset
				if j + L_MAX_REM_LIFE <= L_ACCRUAL.count then
					if j = 1 then
					--Need year weight for the 1st period since it is age interval 0 - L_THEO_CONVENTION (could be 0-0 or 0-.5)
					L_FUTURE_ACCRUAL(j) := L_FUTURE_ACCRUAL(j) - (L_FUTURE_ACCRUAL(j + L_MAX_REM_LIFE) * L_THEO_CONVENTION)
											+ (L_TRUNC_ACCRUAL(j + L_MAX_REM_LIFE) * L_THEO_CONVENTION);
					else
						L_FUTURE_ACCRUAL(j) := L_FUTURE_ACCRUAL(j) - L_FUTURE_ACCRUAL(j + L_MAX_REM_LIFE) + L_TRUNC_ACCRUAL(j + L_MAX_REM_LIFE);
					end if;
				end if;
			end if;

			--now we have future accrual for Full Mortality or Life Span
			if L_FUTURE_ACCRUAL(j) <> 0 then
				if j = 1 then
				--need year weighting for the 1st period since it is age interval 0 - L_THEO_CONVENTION (could be 0-0 or 0-.5)
					L_PROB_LIFE(j) := CURVE_ARRAY(j).SURVIVING_PERCENTAGE/L_FUTURE_ACCRUAL(j) * L_THEO_CONVENTION;
				else
					L_PROB_LIFE(j) := CURVE_ARRAY(j).SURVIVING_PERCENTAGE/L_FUTURE_ACCRUAL(j);
				end if;
			else
				L_PROB_LIFE(j) := 0;
			end if;

			L_REM_LIFE(j) := greatest(0, L_PROB_LIFE(j) - greatest(0,(j - 2 + L_THEO_CONVENTION)));

			if L_PROB_LIFE(j) <> 0 then
				L_RESERVE_RATIO(j) := 1 - (L_REM_LIFE(j)/L_PROB_LIFE(j));
			else
				--if j = 1 then they must hve year_weight = 0 and we need to force this be equal to j = 2 prob_life
				--set a flag so we know this is needed next time through
				if j = 1 then
					L_NEED_AGE0 := TRUE;
					continue;
				end if;
				L_RESERVE_RATIO(j) := 1;
			end if;

			if L_NEED_AGE0 then
				L_PROB_LIFE(1) := L_PROB_LIFE(j);
				L_REM_LIFE(j) := L_PROB_LIFE(1);
				L_RESERVE_RATIO(1) := 0;
				L_NEED_AGE0 := FALSE;
			end if;
		end loop;

		--now we can save the reserve ratios
		for j in 1 .. L_ACCRUAL.count loop
			if j < L_ACCRUAL.count then
				L_MONTH_RATIO(j) := (L_RESERVE_RATIO(j+1) - L_RESERVE_RATIO(j))/12;
				L_MONTH_REM_LIFE(j) := (L_REM_LIFE(j+1) - L_REM_LIFE(j))/12;
			else
				L_MONTH_RATIO(j) := (1 - L_RESERVE_RATIO(j))/12;
				L_MONTH_REM_LIFE(j) := (0 - L_REM_LIFE(j))/12;
			end if;
			RES_RATIO_MATH_TAB(j).INTEGER_AGE := j - 1;
		end loop;


		--In the old code, the index switches from starting at 0 to starting at 1, hence the J+1 for RES_RATIO_MATH_TAB
		forall j in indices of L_REM_LIFE
			merge into RESERVE_RATIOS A
			using (select L_RESERVE_RATIO_ID as RESERVE_RATIO_ID, RES_RATIO_MATH_TAB(j).INTEGER_AGE as INTEGER_AGE,
							L_MORTALITY_CURVE_ID as MORTALITY_CURVE_ID, 'ELG' as ALLOCATION_PROCEDURE,
							L_REM_LIFE(j) as REMAINING_LIFE, L_RESERVE_RATIO(j) as RESERVE_RATIO,
							L_MONTH_RATIO(j) as MONTH_RATIO, L_MAX_REM_LIFE as INTEGER_REM_LIFE,
							L_MONTH_REM_LIFE(j) as MONTH_REMAINING_LIFE, L_EXPECTED_AVG_LIFE as EXPECTED_AVERAGE_LIFE from dual) B
			on (A.RESERVE_RATIO_ID = B.RESERVE_RATIO_ID and A.INTEGER_AGE = B.INTEGER_AGE)
			when matched then update
				set A.MORTALITY_CURVE_ID = B.MORTALITY_CURVE_ID,
					A.EXPECTED_AVERAGE_LIFE = B.EXPECTED_AVERAGE_LIFE,
					A.ALLOCATION_PROCEDURE = B.ALLOCATION_PROCEDURE,
					A.REMAINING_LIFE = B.REMAINING_LIFE,
					A.RESERVE_RATIO = B.RESERVE_RATIO,
					A.MONTH_RESERVE_RATIO = B.MONTH_RATIO,
					A.INTEGER_REM_LIFE = A.INTEGER_REM_LIFE,
					A.MONTH_REMAINING_LIFE = B.MONTH_REMAINING_LIFE
			when not matched then insert
				(RESERVE_RATIO_ID, INTEGER_AGE, INTEGER_REM_LIFE, MORTALITY_CURVE_ID, EXPECTED_AVERAGE_LIFE, ALLOCATION_PROCEDURE, REMAINING_LIFE,
				RESERVE_RATIO, MONTH_RESERVE_RATIO, MONTH_REMAINING_LIFE)
				values
				(RESERVE_RATIOS_SEQ.nextval, B.INTEGER_AGE, B.INTEGER_REM_LIFE, B.MORTALITY_CURVE_ID, B.EXPECTED_AVERAGE_LIFE,
				B.ALLOCATION_PROCEDURE, B.REMAINING_LIFE, B.RESERVE_RATIO, B.MONTH_RATIO, B.MONTH_REMAINING_LIFE);

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
	end P_LOAD_RESERVE_RATIOS_ELG;

	procedure P_SUMMARIZE_RETS is
		type FCST_VINT_REC is record(
			FCST_DEPR_VERSION_ID FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_VERSION_ID%type,
			FCST_DEPR_GROUP_ID FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_GROUP_ID%type,
			SET_OF_BOOKS_ID FCST_DEPR_VINTAGE_SUMMARY.SET_OF_BOOKS_ID%type,
			ACCOUNTING_MONTH FCST_DEPR_VINTAGE_SUMMARY.ACCOUNTING_MONTH%type,
			BEG_COST number,
			RETIREMENTS number,
			END_COST number);
		type FCST_VINT_TABLE is table of FCST_VINT_REC index by pls_integer;
		L_FCST_VINT FCST_VINT_TABLE;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.P_SUMMARIZE_RETS');
		PKG_PP_LOG.P_WRITE_MESSAGE('COLLECTING retirement activity from fcst_depr_vintage_summary');

		select
			FCST_DEPR_VERSION_ID, FDVS.FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, ACCOUNTING_MONTH,
			sum(ACCUM_COST) - sum(ADDITIONS) - sum(RETIREMENTS) - sum(TRANSFERS_IN) - sum(TRANSFERS_OUT) - sum(ADJUSTMENTS),
			sum(RETIREMENTS), sum(ACCUM_COST)
		bulk collect
		into L_FCST_VINT
		from FCST_DEPR_VINTAGE_SUMMARY FDVS, depr_fcst_group_stg tt
		where FDVS.FCST_DEPR_VERSION_ID = G_VERSION_ID
		and FDVS.FCST_DEPR_GROUP_ID = tt.FCST_DEPR_GROUP_ID
		and ACCOUNTING_MONTH between G_START_MONTH and G_END_MONTH
		group by FDVS.FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, FCST_DEPR_VERSION_ID, ACCOUNTING_MONTH
		;

--		PKG_PP_LOG.P_WRITE_MESSAGE('SAVING retirement activity into calculation table');

		forall ndx in indices of L_FCST_VINT
			update DEPR_CALC_STG DCS
			set BEGIN_BALANCE = L_FCST_VINT(ndx).BEG_COST,
				RETIREMENTS = L_FCST_VINT(ndx).RETIREMENTS,
				END_BALANCE = L_FCST_VINT(ndx).END_COST
			where DCS.DEPR_GROUP_ID = L_FCST_VINT(ndx).FCST_DEPR_GROUP_ID
			and DCS.SET_OF_BOOKS_ID = L_FCST_VINT(ndx).SET_OF_BOOKS_ID
			and DCS.GL_POST_MO_YR = L_FCST_VINT(ndx).ACCOUNTING_MONTH
			;

		PKG_PP_LOG.P_WRITE_MESSAGE('FINISHED summarize retirement activity');
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: '|| sqlerrm );
	end P_SUMMARIZE_RETS;


	procedure P_BACKFILL_RATES
	is
		l_curMonth date;
		l_num_months number;
	begin
		/*
		||============================================================================
		|| Object Name: P_BACKFILL_RATES
		|| Description: Backfills the fcst_depr_method_rates to the depr_calc_stg table
		||
		||	Arguments:
		||              NONE
		||	Return: NONE
		||
		||============================================================================
		|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
		||============================================================================
		|| Version Date       Revised By     Reason for Change
		|| ------- ---------- -------------- -----------------------------------------
		|| 1.0     02/05/2014 Beck		Original Version
		||============================================================================
		*/
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.P_BACKFILL_RATES');
		PKG_PP_LOG.P_WRITE_MESSAGE('STARTING to backfill rates for VERSION: ' || to_char(G_VERSION_ID) );

		l_num_months := months_between(G_END_MONTH, G_START_MONTH);
		for ndx in 0 .. l_num_months loop
			if G_DEBUG then
				PKG_PP_LOG.P_WRITE_MESSAGE('START MONTH: ' || to_char(G_START_MONTH, 'yyyymm') || ', NDX is: ' || to_char(ndx) );
			end if;

			l_curMonth := add_months(G_START_MONTH, ndx);

			if G_DEBUG then
				PKG_PP_LOG.P_WRITE_MESSAGE('PROCESSING MONTH: ' || to_char(l_curMonth, 'yyyymm'));
			end if;

			update DEPR_CALC_STG S
			set
			(
				RATE_USED_CODE, RATE, ALLOCATION_PROCEDURE, END_OF_LIFE, NET_GROSS,
				NET_SALVAGE_AMORT_LIFE, SALVAGE_TREATMENT, COR_TREATMENT, AMORTIZABLE_LIFE,
				INTEREST_RATE, SALVAGE_RATE, COST_OF_REMOVAL_PCT, COST_OF_REMOVAL_RATE, RESERVE_RATIO_ID,
				EXPECTED_AVERAGE_LIFE, NET_SALVAGE_PCT, OVER_DEPR_CHECK, MORTALITY_CURVE_ID,
				DMR_SALVAGE_RATE, DMR_COST_OF_REMOVAL_RATE
			) =
			(
				select RATE_USED_CODE, RATE, ALLOCATION_PROCEDURE, END_OF_LIFE,
				    CASE
					  WHEN Lower(Trim(S.MID_PERIOD_METHOD)) = 'end of life' THEN 1
					  WHEN Lower(Trim(S.MID_PERIOD_METHOD)) = 'curve' or Lower(Trim(S.MID_PERIOD_METHOD)) = 'curve_no_true_up' THEN 2
					  ELSE DMR3.NET_GROSS
				    END NET_GROSS,
					NET_SALVAGE_AMORT_LIFE, SALVAGE_TREATMENT, COR_TREATMENT, AMORTIZABLE_LIFE,
					INTEREST_RATE, SALVAGE_RATE, COST_OF_REMOVAL_PCT, COST_OF_REMOVAL_RATE, RESERVE_RATIO_ID,
					EXPECTED_AVERAGE_LIFE, NET_SALVAGE_PCT, OVER_DEPR_CHECK, MORTALITY_CURVE_ID,
					SALVAGE_RATE, COST_OF_REMOVAL_RATE
				from
				(
					with DMR2 as
					(
						select
							FCST_DEPR_VERSION_ID, SET_OF_BOOKS_ID, FCST_DEPR_METHOD_ID, EFFECTIVE_DATE,
							nvl(RATE_USED_CODE, 0) as RATE_USED_CODE, nvl(RATE, 0) as RATE,
							nvl(ALLOCATION_PROCEDURE, 'BROAD') as ALLOCATION_PROCEDURE,
							nvl(END_OF_LIFE, 0) as END_OF_LIFE, nvl(NET_GROSS, 1) as NET_GROSS,
							nvl(NET_SALVAGE_AMORT_LIFE, 0) as NET_SALVAGE_AMORT_LIFE,
							DECODE(LOWER(NVL(DMR.SALVAGE_TREATMENT, 'no')), 'no', 0, 1) as SALVAGE_TREATMENT,
							DECODE(LOWER(NVL(DMR.COR_TREATMENT, 'no')), 'no', 0, 1) as COR_TREATMENT,
							nvl(AMORTIZABLE_LIFE, 0) as AMORTIZABLE_LIFE,
							nvl(INTEREST_RATE, 0) as INTEREST_RATE, nvl(SALVAGE_RATE, 0) as SALVAGE_RATE,
							nvl(COST_OF_REMOVAL_PCT, 0) as COST_OF_REMOVAL_PCT, nvl(COST_OF_REMOVAL_RATE, 0) as COST_OF_REMOVAL_RATE,
							nvl(RESERVE_RATIO_ID, 0) as RESERVE_RATIO_ID, nvl(EXPECTED_AVERAGE_LIFE, 0) as EXPECTED_AVERAGE_LIFE,
							nvl(NET_SALVAGE_PCT, 0) as NET_SALVAGE_PCT, nvl(OVER_DEPR_CHECK, 0) as OVER_DEPR_CHECK,
							nvl(MORTALITY_CURVE_ID, 0) as MORTALITY_CURVE_ID,
							row_number() over(partition by FCST_DEPR_VERSION_ID, SET_OF_BOOKS_ID, FCST_DEPR_METHOD_ID
												order by DMR.EFFECTIVE_DATE desc)  as THE_MAX
						from FCST_DEPR_METHOD_RATES DMR
						where DMR.EFFECTIVE_DATE <= l_curMonth
						and DMR.FCST_DEPR_VERSION_ID = G_VERSION_ID
					)
					select RATE_USED_CODE, RATE, ALLOCATION_PROCEDURE, END_OF_LIFE, NET_GROSS,
							NET_SALVAGE_AMORT_LIFE, SALVAGE_TREATMENT, COR_TREATMENT, AMORTIZABLE_LIFE,
							INTEREST_RATE, SALVAGE_RATE, COST_OF_REMOVAL_PCT, COST_OF_REMOVAL_RATE, RESERVE_RATIO_ID,
							EXPECTED_AVERAGE_LIFE, NET_SALVAGE_PCT, OVER_DEPR_CHECK, MORTALITY_CURVE_ID,
							FCST_DEPR_VERSION_ID, SET_OF_BOOKS_ID, FCST_DEPR_METHOD_ID
					from dmr2
					where DMR2.THE_MAX = 1
				) DMR3
				where DMR3.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
				and DMR3.FCST_DEPR_METHOD_ID = S.DEPR_METHOD_ID
			)
			where S.GL_POST_MO_YR = l_curMonth;
		end loop;

		PKG_PP_LOG.P_WRITE_MESSAGE('DONE with backfilling rates');
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
	end P_BACKFILL_RATES;

	procedure P_FCST_RATE_RECALC( 	A_FCST_DEPR_VERSION FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
										A_START_MONTH date,
										A_END_MONTH date) is
		L_FCST_DMR DMR_RR_TAB;
	begin
		/*
		||============================================================================
		|| Object Name: P_FCST_RATE_RECALC
		|| Description: Recalcs forecast rates. Calls Broad method or ELG method to build reserve ratios
		||
		||	Arguments:
		||               A_FCST_DEPR_VERSION: FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE: The forecasted depreciation version to build the depr vintage summary
		||               A_START_MONTH: date: the start date for forecasting depreciation
		||               A_END_MONTH: date: the start date for forecasting depreciation
		||	Return: NONE
		||
		||============================================================================
		|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
		||============================================================================
		|| Version Date       Revised By     Reason for Change
		|| ------- ---------- -------------- -----------------------------------------
		|| 1.0     02/05/2014 Kyle Peterson    Original Version
		||============================================================================
		*/
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.P_FCST_RATE_RECALC');
		PKG_PP_LOG.P_WRITE_MESSAGE('STARTING to recalculate forecast rates');

		insert into FCST_RATE_RECALC_TEMP
		(FCST_DEPR_METHOD_ID, SET_OF_BOOKS_ID, EFFECTIVE_DATE, FCST_DEPR_VERSION_ID, RATE, NET_GROSS,
		 OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE, END_OF_LIFE, MORTALITY_CURVE_ID,
		 EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID, COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_PCT,
		 INTEREST_RATE, ALLOCATION_PROCEDURE)
		select FDMR.FCST_DEPR_METHOD_ID,
			   FDMR.SET_OF_BOOKS_ID,
			   DCS.GL_POST_MO_YR,
			   FDMR.FCST_DEPR_VERSION_ID,
			   FDMR.RATE,
			   FDMR.NET_GROSS,
			   FDMR.OVER_DEPR_CHECK,
			   FDMR.NET_SALVAGE_PCT,
			   FDMR.RATE_USED_CODE,
			   FDMR.END_OF_LIFE,
			   FDMR.MORTALITY_CURVE_ID,
			   FDMR.EXPECTED_AVERAGE_LIFE,
			   FDMR.RESERVE_RATIO_ID,
			   FDMR.COST_OF_REMOVAL_RATE,
			   FDMR.COST_OF_REMOVAL_PCT,
			   FDMR.INTEREST_RATE,
			   upper(nvl(FDMR.ALLOCATION_PROCEDURE, 'BROAD'))
		  from FCST_DEPR_METHOD_RATES FDMR, (select distinct DEPR_METHOD_ID, GL_POST_MO_YR from DEPR_CALC_STG) DCS, FCST_DEPRECIATION_METHOD FDM
		 where FDMR.EFFECTIVE_DATE =
			   (select max(R2.EFFECTIVE_DATE)
				  from FCST_DEPR_METHOD_RATES R2
				 where R2.FCST_DEPR_METHOD_ID = FDMR.FCST_DEPR_METHOD_ID
				   and R2.SET_OF_BOOKS_ID = FDMR.SET_OF_BOOKS_ID
				   and R2.FCST_DEPR_VERSION_ID = FDMR.FCST_DEPR_VERSION_ID
				   and R2.EFFECTIVE_DATE <= DCS.GL_POST_MO_YR
				 group by R2.FCST_DEPR_METHOD_ID, R2.SET_OF_BOOKS_ID, R2.FCST_DEPR_VERSION_ID)
		   and FDMR.MORTALITY_CURVE_ID is not null
		   and FDMR.EXPECTED_AVERAGE_LIFE is not null
		   and FDMR.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
		   and FDMR.FCST_DEPR_METHOD_ID = FDM.FCST_DEPR_METHOD_ID
		   and FDM.RATE_RECALC_OPTION in (14, to_number(to_char(DCS.GL_POST_MO_YR,'mm')))
		   and FDMR.FCST_DEPR_METHOD_ID = DCS.DEPR_METHOD_ID; --This last join gets all the DEPR methods

		--Build the reserve ratio for each depr method rate in DEPR_CALC_STG
		select distinct FRRT.FCST_DEPR_METHOD_ID, FRRT.SET_OF_BOOKS_ID, FRRT.MORTALITY_CURVE_ID, FRRT.EXPECTED_AVERAGE_LIFE, FRRT.END_OF_LIFE,
						FRRT.RESERVE_RATIO_ID, FRRT.ALLOCATION_PROCEDURE, FRRT.EFFECTIVE_DATE
		bulk collect into L_FCST_DMR
		from FCST_RATE_RECALC_TEMP FRRT;

		for i in 1 .. L_FCST_DMR.count loop
			if F_GET_RESERVE_RATIO_ID(L_FCST_DMR(i)) = -1 then
				if upper(L_FCST_DMR(i).ALLOCATION_PROCEDURE) = 'BROAD' or L_FCST_DMR(i).ALLOCATION_PROCEDURE is null then
					P_LOAD_RESERVE_RATIOS_BROAD(L_FCST_DMR(i));
				end if;
				if upper(L_FCST_DMR(i).ALLOCATION_PROCEDURE) = 'ELG' then
					P_LOAD_RESERVE_RATIOS_ELG(L_FCST_DMR(i));
				end if;
			end if;
		end loop;

		-- NEED TO SET the reserve ratio ids on FCST_RATE_RECALC_TEMP
		forall ndx in indices of L_FCST_DMR
			update FCST_RATE_RECALC_TEMP
			set RESERVE_RATIO_ID = L_FCST_DMR(ndx).RESERVE_RATIO_ID
			where FCST_DEPR_METHOD_ID = L_FCST_DMR(ndx).DMR_ID
			and SET_OF_BOOKS_ID = L_FCST_DMR(ndx).SOB_ID
			and EFFECTIVE_DATE = L_FCST_DMR(ndx).MONTH;

		--Ripped this monstrosity straight from the old code, don't blame me.
		insert into FCST_RATE_RECALC2_TEMP
		(FCST_DEPR_METHOD_ID, SET_OF_BOOKS_ID, EFFECTIVE_DATE, FCST_DEPR_VERSION_ID, NEW_RATE,
		 NEW_COST_OF_REMOVAL_RATE)
		select FCST_DEPR_METHOD_ID,
			   SET_OF_BOOKS_ID,
			   EFFECTIVE_DATE,
			   FCST_DEPR_VERSION_ID,
			   DECODE(AVG_REM_LIFE, 0, 12, ROUND(1 / AVG_REM_LIFE, 8)),
			   DECODE(AVG_REM_LIFE, 0, 12, ROUND(1 / AVG_REM_LIFE, 8))
		  from (select FCST_DEPR_METHOD_ID,
					   SET_OF_BOOKS_ID,
					   EFFECTIVE_DATE,
					   FCST_DEPR_VERSION_ID,
					   DECODE(METHOD_RECOVERABLE_COST,
							  0,
							  0,
							  ROUND(METHOD_REM_LIFE_COST / METHOD_RECOVERABLE_COST, 8)) AVG_REM_LIFE
				  from (select FCST_DEPR_METHOD_ID,
							   SET_OF_BOOKS_ID,
							   EFFECTIVE_DATE,
							   FCST_DEPR_VERSION_ID,
							   sum(RECOVERABLE_COST * REM_LIFE) METHOD_REM_LIFE_COST,
							   sum(RECOVERABLE_COST) METHOD_RECOVERABLE_COST
						  from (select FCST_DEPR_METHOD_ID,
									   SET_OF_BOOKS_ID,
									   EFFECTIVE_DATE,
									   FCST_DEPR_VERSION_ID,
									   ROUND(ACCUM_COST * (1 - NET_SALVAGE_PCT + COST_OF_REMOVAL_PCT), 2) RECOVERABLE_COST,
									   ROUND(DECODE(TO_CHAR(GL_POST_MO_YR, 'mm'),
													'12',
													REMAINING_LIFE,
													REMAINING_LIFE +
													(MONTH_REMAINING_LIFE * TO_NUMBER(TO_CHAR(GL_POST_MO_YR, 'mm')))), 8) REM_LIFE
								  from (select FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_GROUP_ID,
											   FCST_DEPR_VINTAGE_SUMMARY.SET_OF_BOOKS_ID,
											   FCST_DEPR_VINTAGE_SUMMARY.ACCOUNTING_MONTH GL_POST_MO_YR,
											   FCST_DEPR_GROUP_VERSION.FCST_COMBINED_DEPR_GROUP_ID,
											   FCST_DEPR_VINTAGE_SUMMARY.VINTAGE,
											   FCST_DEPR_VINTAGE_SUMMARY.ACCUM_COST,
											   U.NET_SALVAGE_PCT,
											   RESERVE_RATIOS.RESERVE_RATIO_ID,
											   RESERVE_RATIOS.INTEGER_AGE,
											   RESERVE_RATIOS.RESERVE_RATIO,
											   RESERVE_RATIOS.MONTH_RESERVE_RATIO,
											   RESERVE_RATIOS.REMAINING_LIFE,
											   RESERVE_RATIOS.MONTH_REMAINING_LIFE,
											   FCST_DEPR_GROUP_VERSION.FCST_DEPR_METHOD_ID,
											   NVL(U.COST_OF_REMOVAL_RATE, 0) COST_OF_REMOVAL_RATE,
											   NVL(U.COST_OF_REMOVAL_PCT, 0) COST_OF_REMOVAL_PCT,
											   U.FCST_DEPR_VERSION_ID,
											   U.EFFECTIVE_DATE
										  from FCST_RATE_RECALC_TEMP U,
											   FCST_DEPR_GROUP_VERSION,
											   RESERVE_RATIOS,
											   FCST_DEPR_VINTAGE_SUMMARY
										 where FCST_DEPR_GROUP_VERSION.FCST_DEPR_METHOD_ID = U.FCST_DEPR_METHOD_ID
										   and U.RESERVE_RATIO_ID = RESERVE_RATIOS.RESERVE_RATIO_ID
										   and FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_GROUP_ID = FCST_DEPR_GROUP_VERSION.FCST_DEPR_GROUP_ID
										   and FCST_DEPR_VINTAGE_SUMMARY.SET_OF_BOOKS_ID = U.SET_OF_BOOKS_ID
										   and FCST_DEPR_VINTAGE_SUMMARY.ACCOUNTING_MONTH = ADD_MONTHS(U.EFFECTIVE_DATE, -1)
										   and RESERVE_RATIOS.INTEGER_AGE = TO_NUMBER(TO_CHAR(U.EFFECTIVE_DATE, 'yyyy')) - FCST_DEPR_VINTAGE_SUMMARY.VINTAGE
										   and FCST_DEPR_GROUP_VERSION.FCST_DEPR_METHOD_ID = U.FCST_DEPR_METHOD_ID
										   and FCST_DEPR_GROUP_VERSION.FCST_DEPR_VERSION_ID = U.FCST_DEPR_VERSION_ID
										   and FCST_DEPR_GROUP_VERSION.FCST_DEPR_VERSION_ID = FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_VERSION_ID

										union all

										select FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_GROUP_ID,
											   FCST_DEPR_VINTAGE_SUMMARY.SET_OF_BOOKS_ID,
											   FCST_DEPR_VINTAGE_SUMMARY.ACCOUNTING_MONTH,
											   FCST_DEPR_GROUP_VERSION.FCST_COMBINED_DEPR_GROUP_ID,
											   FCST_DEPR_VINTAGE_SUMMARY.VINTAGE,
											   FCST_DEPR_VINTAGE_SUMMARY.ACCUM_COST,
											   U.NET_SALVAGE_PCT,
											   MAX_RR.RESERVE_RATIO_ID,
											   MAX_RR.MAX_AGE,
											   1,
											   0,
											   0,
											   0,
											   FCST_DEPR_GROUP_VERSION.FCST_DEPR_METHOD_ID,
											   U.COST_OF_REMOVAL_RATE,
											   U.COST_OF_REMOVAL_PCT,
											   U.FCST_DEPR_VERSION_ID,
											   U.EFFECTIVE_DATE

										  from FCST_RATE_RECALC_TEMP U,
											   FCST_DEPR_VINTAGE_SUMMARY,
											   FCST_DEPR_GROUP_VERSION,
											   (select RESERVE_RATIO_ID, max(INTEGER_AGE) MAX_AGE
												  from RESERVE_RATIOS
												 group by RESERVE_RATIO_ID) MAX_RR

										 where FCST_DEPR_GROUP_VERSION.FCST_DEPR_METHOD_ID = U.FCST_DEPR_METHOD_ID
										   and U.RESERVE_RATIO_ID = MAX_RR.RESERVE_RATIO_ID
										   and FCST_DEPR_VINTAGE_SUMMARY.SET_OF_BOOKS_ID = U.SET_OF_BOOKS_ID
										   and FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_GROUP_ID = FCST_DEPR_GROUP_VERSION.FCST_DEPR_GROUP_ID
										   and FCST_DEPR_VINTAGE_SUMMARY.ACCOUNTING_MONTH = ADD_MONTHS(U.EFFECTIVE_DATE, -1)
										   and MAX_RR.MAX_AGE < TO_NUMBER(TO_CHAR(U.EFFECTIVE_DATE, 'yyyy')) - FCST_DEPR_VINTAGE_SUMMARY.VINTAGE
										   and FCST_DEPR_GROUP_VERSION.FCST_DEPR_VERSION_ID = U.FCST_DEPR_VERSION_ID
										   and FCST_DEPR_GROUP_VERSION.FCST_DEPR_VERSION_ID = FCST_DEPR_VINTAGE_SUMMARY.FCST_DEPR_VERSION_ID))
						 group by FCST_DEPR_METHOD_ID,
								  SET_OF_BOOKS_ID,
								  EFFECTIVE_DATE,
								  FCST_DEPR_VERSION_ID));

		update FCST_RATE_RECALC_TEMP U
		set (RATE, COST_OF_REMOVAL_RATE) =
			(select case
						when NEW_RATE < -12 then
						 -12
						when NEW_RATE > 12 then
						 12
						else
						 NEW_RATE
					end,
					case
						when NEW_COST_OF_REMOVAL_RATE < -12 then
						 -12
						when NEW_COST_OF_REMOVAL_RATE > 12 then
						 12
						else
						 NEW_COST_OF_REMOVAL_RATE
					end
			   from FCST_RATE_RECALC2_TEMP RR2
			  where RR2.FCST_DEPR_METHOD_ID = U.FCST_DEPR_METHOD_ID
				and RR2.SET_OF_BOOKS_ID = U.SET_OF_BOOKS_ID
				and RR2.EFFECTIVE_DATE = U.EFFECTIVE_DATE
				and RR2.FCST_DEPR_VERSION_ID = U.FCST_DEPR_VERSION_ID)
		where exists (select 1
			  from FCST_RATE_RECALC2_TEMP RR2
			 where RR2.FCST_DEPR_METHOD_ID = U.FCST_DEPR_METHOD_ID
			   and RR2.SET_OF_BOOKS_ID = U.SET_OF_BOOKS_ID
			   and RR2.EFFECTIVE_DATE = U.EFFECTIVE_DATE
			   and RR2.FCST_DEPR_VERSION_ID = U.FCST_DEPR_VERSION_ID);

		merge into FCST_DEPR_METHOD_RATES A
		using (select FCST_DEPR_METHOD_ID, SET_OF_BOOKS_ID, EFFECTIVE_DATE, FCST_DEPR_VERSION_ID, RATE, NET_GROSS,
				OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE, END_OF_LIFE, MORTALITY_CURVE_ID, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID,
				COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_PCT, INTEREST_RATE, ALLOCATION_PROCEDURE from FCST_RATE_RECALC_TEMP) B
		on (A.FCST_DEPR_METHOD_ID = B.FCST_DEPR_METHOD_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.FCST_DEPR_VERSION_ID = B.FCST_DEPR_VERSION_ID
			and A.EFFECTIVE_DATE = B.EFFECTIVE_DATE)
		when matched then update
		set A.RATE = B.RATE,
			A.COST_OF_REMOVAL_RATE = B.COST_OF_REMOVAL_RATE,
			A.RESERVE_RATIO_ID = B.RESERVE_RATIO_ID,
			A.RATE_USED_CODE = 0
		when not matched then insert (FCST_DEPR_METHOD_ID, SET_OF_BOOKS_ID, EFFECTIVE_DATE, FCST_DEPR_VERSION_ID, RATE, NET_GROSS,
				OVER_DEPR_CHECK, NET_SALVAGE_PCT, RATE_USED_CODE, END_OF_LIFE, MORTALITY_CURVE_ID, EXPECTED_AVERAGE_LIFE, RESERVE_RATIO_ID,
				COST_OF_REMOVAL_RATE, COST_OF_REMOVAL_PCT, INTEREST_RATE, ALLOCATION_PROCEDURE)
			values
				(B.FCST_DEPR_METHOD_ID, B.SET_OF_BOOKS_ID, B.EFFECTIVE_DATE, B.FCST_DEPR_VERSION_ID, B.RATE, B.NET_GROSS, B.OVER_DEPR_CHECK,
				B.NET_SALVAGE_PCT, B.RATE_USED_CODE, B.END_OF_LIFE, B.MORTALITY_CURVE_ID, B.EXPECTED_AVERAGE_LIFE, B.RESERVE_RATIO_ID,
				B.COST_OF_REMOVAL_RATE, B.COST_OF_REMOVAL_PCT, B.INTEREST_RATE, B.ALLOCATION_PROCEDURE);


		P_BACKFILL_RATES();

		PKG_PP_LOG.P_WRITE_MESSAGE('DONE recalculating forecast rates');
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
	end P_FCST_RATE_RECALC;

	procedure P_AUTO_RET_FIFO( 	A_FCST_DEPR_VERSION FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
										A_START_MONTH date,
										A_END_MONTH date) is
		L_FISCAL_START_DATE date;

		L_FDVS FCST_DVS_TAB;

		l_month_to_process date;
		l_num_months number;
		l_fiscalMonth number;
	begin
		/*
		||============================================================================
		|| Object Name: P_AUTO_RET_FIFO
		|| Description: vintages retirements 
		||
		||	Arguments:
		||               A_FCST_DEPR_VERSION: FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE: The forecasted depreciation version to build the depr vintage summary
		||               A_START_MONTH: date: the start date for forecasting depreciation
		||               A_END_MONTH: date: the start date for forecasting depreciation
		||	Return: NONE
		||
		||============================================================================
		|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
		||============================================================================
		|| Version  Date       Revised By     Reason for Change
		|| -------- ---------- -------------- -----------------------------------------
		|| 1.0      02/05/2014 Kyle Peterson  Original Version
    || 2015.2.3 08/24/2016 Aaron Smith    PP-46215 auto-retirement fixes
		||============================================================================
		*/

		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.P_AUTO_RET_FIFO');
		PKG_PP_LOG.P_WRITE_MESSAGE('STARTING to FIFO remaining retirements');

    L_FISCAL_START_DATE := F_GET_FISCAL_MONTH(A_START_MONTH);

    -- get the fiscal_month
    select CAL.FISCAL_MONTH
    into L_FISCALMONTH
    from PP_CALENDAR CAL
    where CAL.MONTH = A_START_MONTH
    ;

    -- Store the begin balances and retirement activity for current fiscal year by depr group
    P_GET_BEGIN_BALANCES(A_FCST_DEPR_VERSION, A_START_MONTH, L_FISCAL_START_DATE, FALSE);

    l_num_months := months_between(A_END_MONTH, A_START_MONTH);
    for l_loop_counter in 0 .. l_num_months loop
      l_month_to_process := add_months(a_start_month, l_loop_counter);


      -- set the start of the fiscal year
      if L_FISCALMONTH = 1 then
        L_FISCAL_START_DATE := l_month_to_process;

        -- beginning of the fiscal year, get balances and set retirements to zero
        P_GET_BEGIN_BALANCES(A_FCST_DEPR_VERSION, l_month_to_process, L_FISCAL_START_DATE, TRUE);

        if G_DEBUG then
          PKG_PP_LOG.P_WRITE_MESSAGE('NEW FISCAL YEAR: ' || to_char(L_FISCAL_START_DATE, 'yyyymm'));
        end if;
      end if;

      -- figure out the FIFO retirements for the month
      select
        rownum, VINTAGED_GROUPS.FCST_DEPR_VERSION_ID, VINTAGED_GROUPS.SET_OF_BOOKS_ID,
        VINTAGED_GROUPS.FCST_DEPR_GROUP_ID, VINTAGED_GROUPS.ACCOUNTING_MONTH,
        VINTAGED_GROUPS.VINTAGE,
        0, 0, 0, 0, 0, -- only care about retirements
        -1 * sign( MONTHLY_RET.AMOUNT ) *
          least( abs(MONTHLY_RET.AMOUNT) - case when sign(MONTHLY_RET.AMOUNT) = 1
                            then abs(VINTAGED_GROUPS.POS_START_AMOUNT)
                            else abs(VINTAGED_GROUPS.NEG_START_AMOUNT)
                          end,
            abs(VINTAGED_GROUPS.ACCUM_COST) ) as RETIRED
      bulk collect
      into L_FDVS
      from
      (
        select
          FDVS.FCST_DEPR_VERSION_ID, FDVS.FCST_DEPR_GROUP_ID, FDVS.SET_OF_BOOKS_ID, FDVS.ACCOUNTING_MONTH, FDVS.VINTAGE,
          FDVS.ACCUM_COST, FDVS.ADDITIONS, FDVS.TRANSFERS_IN, FDVS.TRANSFERS_OUT, FDVS.ADJUSTMENTS,
          -- get the positive accum cost... these will get used to fifo negative retirements
          nvl(sum( case when FDVS.ACCUM_COST < 0 then 0 else FDVS.ACCUM_COST end )
            over(partition by FDVS.FCST_DEPR_GROUP_ID, FDVS.SET_OF_BOOKS_ID
              order by FDVS.VINTAGE -- because this is FIFOing
              rows between unbounded preceding and 1 preceding), 0) as POS_START_AMOUNT,
          -- get the negative accum cost... these will get used to fifo positivie retirements
          nvl(sum( case when FDVS.ACCUM_COST > 0 then 0 else FDVS.ACCUM_COST end )
            over(partition by FDVS.FCST_DEPR_GROUP_ID, FDVS.SET_OF_BOOKS_ID
              order by FDVS.VINTAGE -- because this is FIFOing
              rows between unbounded preceding and 1 preceding), 0) as NEG_START_AMOUNT
        from FCST_DEPR_VINTAGE_SUMMARY FDVS, DEPR_CALC_STG DCS, FCST_METHOD_RETIRE_PCT FMRP
        where FDVS.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
        and FMRP.FCST_DEPR_VERSION_ID = 
          ( select max(fmrp_in.fcst_depr_version_id) 
            from fcst_method_retire_pct fmrp_in 
            where fmrp_in.fcst_depr_version_id in (A_FCST_DEPR_VERSION, -1) 
            and fmrp_in.fcst_depr_method_id = fmrp.fcst_depr_method_id 
          )
        and DCS.DEPR_METHOD_ID = FMRP.FCST_DEPR_METHOD_ID
        and DCS.DEPR_GROUP_ID = FDVS.FCST_DEPR_GROUP_ID
        and DCS.SET_OF_BOOKS_ID = FDVS.SET_OF_BOOKS_ID
        and nvl(FMRP.BASIS_TYPE_ID,1) in (1, 2)
        and nvl(FMRP.RETIREMENT_PCT,0) <> 0
        and FDVS.ACCUM_COST <> 0 
        and FDVS.ACCOUNTING_MONTH = l_month_to_process
        and DCS.GL_POST_MO_YR = l_month_to_process
        UNION
        select
          FDVS.FCST_DEPR_VERSION_ID, FDVS.FCST_DEPR_GROUP_ID, FDVS.SET_OF_BOOKS_ID, FDVS.ACCOUNTING_MONTH, FDVS.VINTAGE,
          FDVS.ACCUM_COST, FDVS.ADDITIONS, FDVS.TRANSFERS_IN, FDVS.TRANSFERS_OUT, FDVS.ADJUSTMENTS,
          -- get the positive accum cost... these will get used to fifo negative retirements
          nvl(sum( case when FDVS.ACCUM_COST < 0 then 0 else FDVS.ACCUM_COST end )
            over(partition by FDVS.FCST_DEPR_GROUP_ID, FDVS.SET_OF_BOOKS_ID
              order by FDVS.VINTAGE -- because this is FIFOing
              rows between unbounded preceding and 1 preceding), 0) as POS_START_AMOUNT,
          -- get the negative accum cost... these will get used to fifo positivie retirements
          nvl(sum( case when FDVS.ACCUM_COST > 0 then 0 else FDVS.ACCUM_COST end )
            over(partition by FDVS.FCST_DEPR_GROUP_ID, FDVS.SET_OF_BOOKS_ID
              order by FDVS.VINTAGE -- because this is FIFOing
              rows between unbounded preceding and 1 preceding), 0) as NEG_START_AMOUNT
        from FCST_DEPR_VINTAGE_SUMMARY FDVS, DEPR_CALC_STG DCS
        where FDVS.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
        and DCS.DEPR_GROUP_ID = FDVS.FCST_DEPR_GROUP_ID
        and DCS.SET_OF_BOOKS_ID = FDVS.SET_OF_BOOKS_ID
        and FDVS.ACCUM_COST <> 0 
        and FDVS.ACCOUNTING_MONTH = l_month_to_process
        and DCS.GL_POST_MO_YR = l_month_to_process
        and DCS.retirements <> 0
        and not exists (select 1 from fcst_method_retire_pct p 
                        where p.fcst_depr_method_id = dcs.depr_method_id 
                          and p.fcst_depr_version_id in (A_FCST_DEPR_VERSION, -1) )
      ) VINTAGED_GROUPS,
      (
        select DCS.DEPR_GROUP_ID as FCST_DEPR_GROUP_ID, DCS.SET_OF_BOOKS_ID,
          case when FMRP.BASIS_TYPE_ID = 2 then
            -- based on current month adds
            DECODE(CALC_RETIRE, 1, F_AUTO_RET_AMT_ADDITIONS(DCS.ADDITIONS, FMRP.RETIREMENT_PCT), NVL(-1 * DCS.RETIREMENTS,0) )
          else
            -- Based on gross beginning balance and cumulative retirements
                 DECODE(CALC_RETIRE, 1, nvl((select F_AUTO_RET_AMT_GROSS(BB.BEGIN_BALANCE, FMRP.FACTOR_ID, FMRP.RETIREMENT_PCT, BB.CUM_RETIREMENTS, L_FISCALMONTH)
                 from DEPR_BEG_BALS BB
                 where BB.DEPR_GROUP_ID = DCS.DEPR_GROUP_ID
                 and BB.SET_OF_BOOKS_ID = DCS.SET_OF_BOOKS_ID
                 ), 0), NVL(-1 * DCS.RETIREMENTS, 0) )
          end as AMOUNT
        from DEPR_CALC_STG DCS, FCST_METHOD_RETIRE_PCT FMRP
        where FMRP.FCST_DEPR_VERSION_ID = 
          ( select max(fmrp_in.fcst_depr_version_id) 
            from fcst_method_retire_pct fmrp_in 
            where fmrp_in.fcst_depr_version_id in (A_FCST_DEPR_VERSION, -1) 
            and fmrp_in.fcst_depr_method_id = fmrp.fcst_depr_method_id 
          )          
        and DCS.DEPR_METHOD_ID = FMRP.FCST_DEPR_METHOD_ID
        and nvl(FMRP.BASIS_TYPE_ID,1) in (1, 2)
        and nvl(FMRP.RETIREMENT_PCT,0) <> 0
        and DCS.GL_POST_MO_YR = l_month_to_process
                UNION
                  select DCS.DEPR_GROUP_ID as FCST_DEPR_GROUP_ID, DCS.SET_OF_BOOKS_ID, NVL(-1 * DCS.RETIREMENTS, 0) as AMOUNT
        from DEPR_CALC_STG DCS
        where DCS.GL_POST_MO_YR = l_month_to_process
                     and DCS.retirements <> 0
                     and not exists (select 1 from fcst_method_retire_pct p 
                        where p.fcst_depr_method_id = dcs.depr_method_id 
                          and p.fcst_depr_version_id in ( A_FCST_DEPR_VERSION, -1) )
      ) MONTHLY_RET
      where MONTHLY_RET.AMOUNT <> 0
      and VINTAGED_GROUPS.FCST_DEPR_GROUP_ID = MONTHLY_RET.FCST_DEPR_GROUP_ID
      and VINTAGED_GROUPS.SET_OF_BOOKS_ID = MONTHLY_RET.SET_OF_BOOKS_ID
      -- limit the selection to only those vintages with the correct sign on accum cost
      and sign(VINTAGED_GROUPS.ACCUM_COST) = sign(MONTHLY_RET.AMOUNT)
      -- NOW limit to the rows needed to FIFO based on
      -- note that a sign of 1 for retirement amount really means a negative retirement
      -- since the -1 * is above as part of the main select
      and case when sign(MONTHLY_RET.AMOUNT) = 1
        then abs(VINTAGED_GROUPS.POS_START_AMOUNT) -- negative retirement so FIFO the positive accum costs
        else abs(VINTAGED_GROUPS.NEG_START_AMOUNT) -- positive retirement so FIFO the negative accum costs
        end < abs(MONTHLY_RET.AMOUNT)
      ;

      if L_FDVS.count > 0 then
        if G_DEBUG then
          PKG_PP_LOG.P_WRITE_MESSAGE('MONTH: ' || to_char(l_month_to_process, 'yyyymm') );
          PKG_PP_LOG.P_WRITE_MESSAGE('   NUM FIFO RETS: ' || to_char( L_FDVS.count ) );
          PKG_PP_LOG.P_WRITE_MESSAGE('   FIRST RET INFO: '
                        || ' DEPR_GROUP: ' || to_char(L_FDVS(1).FCST_DEPR_GROUP_ID)
                        || ' SET_OF_BOOKS_ID: ' || to_char(L_FDVS(1).SET_OF_BOOKS_ID)
                        || ' VINTAGE: ' || to_char(L_FDVS(1).VINTAGE)
                        || ' RETIREMENTS: ' || to_char(L_FDVS(1).RETIREMENTS) );
        end if;

        -- update fcst_depr_vintage summary retirements and accum cost (accum cost for all months >= current month)
        forall ndx in indices of L_FDVS
          update FCST_DEPR_VINTAGE_SUMMARY FDVS
          set FDVS.ACCUM_COST = FDVS.ACCUM_COST + L_FDVS(ndx).RETIREMENTS,
            FDVS.RETIREMENTS = FDVS.RETIREMENTS + decode(FDVS.ACCOUNTING_MONTH, l_month_to_process, L_FDVS(ndx).RETIREMENTS, 0)
          where  FDVS.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
          and FDVS.SET_OF_BOOKS_ID = L_FDVS(ndx).SET_OF_BOOKS_ID
          and FDVS.FCST_DEPR_GROUP_ID = L_FDVS(ndx).FCST_DEPR_GROUP_ID
          and FDVS.VINTAGE = L_FDVS(ndx).VINTAGE
          and FDVS.ACCOUNTING_MONTH >= l_month_to_process
          ;

        -- add to the cumulative retirements
        forall ndx in indices of L_FDVS
          update DEPR_BEG_BALS BB
          set BB.CUM_RETIREMENTS = BB.CUM_RETIREMENTS + L_FDVS(ndx).RETIREMENTS
          where BB.SET_OF_BOOKS_ID = L_FDVS(ndx).SET_OF_BOOKS_ID
          and BB.DEPR_GROUP_ID = L_FDVS(ndx).FCST_DEPR_GROUP_ID
          ;
      end if;

      -- increase fiscal month by 1
      L_FISCALMONTH := case when L_FISCALMONTH = 12 then 1 else L_FISCALMONTH + 1 end;
    end loop;

		PKG_PP_LOG.P_WRITE_MESSAGE('DONE FIFOing remaining retirements');
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
	end P_AUTO_RET_FIFO;



	procedure P_AUTO_RET_CURVE( 	A_FCST_DEPR_VERSION FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
										A_START_MONTH date,
										A_END_MONTH date) is
		L_FISCAL_START_DATE date;

		L_FDVS FCST_DVS_TAB;

		l_month_to_process date;
		l_num_months number;
		l_check number;
		l_fiscalMonth number;
	begin
		/*
		||============================================================================
		|| Object Name: P_AUTO_RET_CURVE
		|| Description: vintages retirements
		||
		||	Arguments:
		||               A_FCST_DEPR_VERSION: FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE: The forecasted depreciation version to build the depr vintage summary
		||               A_START_MONTH: date: the start date for forecasting depreciation
		||               A_END_MONTH: date: the start date for forecasting depreciation
		||	Return: NONE
		||
		||============================================================================
		|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
		||============================================================================
		|| Version  Date       Revised By     Reason for Change
		|| -------  ---------- -------------- -----------------------------------------
		|| 1.0      02/05/2014 Kyle Peterson  Original Version
    || 2015.2.3 08/24/2016 Aaron Smith    PP-46215 auto-retirement fixes
		||============================================================================
		*/

		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.P_AUTO_RET_CURVE');
		PKG_PP_LOG.P_WRITE_MESSAGE('STARTING to CURVE retirements');

		-- have to grab depr method rates prior to auto retirements occurring
		P_BACKFILL_RATES();

		select count(1)
		into l_check
		from FCST_DEPR_VINTAGE_SUMMARY FDVS, DEPR_CALC_STG DCS, FCST_METHOD_RETIRE_PCT FMRP
		where FDVS.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
		and DCS.DEPR_METHOD_ID = FMRP.FCST_DEPR_METHOD_ID
		and FMRP.FCST_DEPR_VERSION_ID = 
        ( select max(fmrp_in.fcst_depr_version_id) 
          from fcst_method_retire_pct fmrp_in 
          where fmrp_in.fcst_depr_version_id in (A_FCST_DEPR_VERSION, -1) 
          and fmrp_in.fcst_depr_method_id = fmrp.fcst_depr_method_id 
        )
		and DCS.DEPR_GROUP_ID = FDVS.FCST_DEPR_GROUP_ID
    and DCS.SET_OF_BOOKS_ID = FDVS.SET_OF_BOOKS_ID
		and FMRP.BASIS_TYPE_ID = 3
		and FDVS.ACCUM_COST <> 0
		and DCS.MORTALITY_CURVE_ID <> 0
		and DCS.EXPECTED_AVERAGE_LIFE <> 0
		and FDVS.ACCOUNTING_MONTH between A_START_MONTH and A_END_MONTH
    and rownum = 1
		;

		if l_check > 0 then
			L_FISCAL_START_DATE := F_GET_FISCAL_MONTH(A_START_MONTH);

			-- get the fiscal_month
			select CAL.FISCAL_MONTH
			into L_FISCALMONTH
			from PP_CALENDAR CAL
			where CAL.MONTH = A_START_MONTH
			;

			l_num_months := months_between(A_END_MONTH, A_START_MONTH);
			for l_loop_counter in 0 .. l_num_months loop
				l_month_to_process := add_months(a_start_month, l_loop_counter);

				-- set the start of the fiscal year
				if L_FISCALMONTH = 1 then
					L_FISCAL_START_DATE := l_month_to_process;

					if G_DEBUG then
						PKG_PP_LOG.P_WRITE_MESSAGE('NEW FISCAL YEAR: ' || to_char(L_FISCAL_START_DATE, 'yyyymm'));
					end if;
				end if;

				-- figure out the curve retirements for the month by vintage
				with beg_year as
				(
					select BB.FCST_DEPR_GROUP_ID, BB.SET_OF_BOOKS_ID, BB.VINTAGE,
						BB.ACCUM_COST, sum(A.RETIREMENTS) as CUM_RETIREMENTS
					from FCST_DEPR_VINTAGE_SUMMARY BB, FCST_DEPR_VINTAGE_SUMMARY A, depr_fcst_group_stg tt
					where BB.ACCOUNTING_MONTH = add_months(L_FISCAL_START_DATE, -1)
					and BB.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
					and A.FCST_DEPR_GROUP_ID = BB.FCST_DEPR_GROUP_ID
					and A.FCST_DEPR_GROUP_ID = tt.FCST_DEPR_GROUP_ID
					and A.SET_OF_BOOKS_ID = BB.SET_OF_BOOKS_ID
					and A.VINTAGE = BB.VINTAGE
					and A.ACCOUNTING_MONTH between L_FISCAL_START_DATE and l_month_to_process
					and A.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
					group by BB.FCST_DEPR_GROUP_ID, BB.SET_OF_BOOKS_ID, BB.VINTAGE, BB.ACCUM_COST
				)
				select
					rownum, FDVS.FCST_DEPR_VERSION_ID, FDVS.SET_OF_BOOKS_ID,
					FDVS.FCST_DEPR_GROUP_ID, FDVS.ACCOUNTING_MONTH,
					FDVS.VINTAGE,
					0, 0, 0, 0, 0, -- only care about retirements
					-1 * nvl(F_AUTO_RET_AMT_CURVE(BEG_YEAR.ACCUM_COST, FMRP.FACTOR_ID, BEG_YEAR.CUM_RETIREMENTS,
										l_month_to_process, DCS.DEPR_GROUP_ID, DCS.SET_OF_BOOKS_ID,
										A_FCST_DEPR_VERSION, FDVS.VINTAGE, DCS.DEPR_METHOD_ID,
										DCS.EXPECTED_AVERAGE_LIFE, DCS.END_OF_LIFE, DCS.RESERVE_RATIO_ID,
										DCS.ALLOCATION_PROCEDURE, DCS.MORTALITY_CURVE_ID), 0)
				bulk collect
				into L_FDVS
				from FCST_DEPR_VINTAGE_SUMMARY FDVS, DEPR_CALC_STG DCS, FCST_METHOD_RETIRE_PCT FMRP, BEG_YEAR
				where FDVS.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
				and FMRP.FCST_DEPR_VERSION_ID = 
            ( select max(fmrp_in.fcst_depr_version_id) 
              from fcst_method_retire_pct fmrp_in 
              where fmrp_in.fcst_depr_version_id in (A_FCST_DEPR_VERSION, -1) 
              and fmrp_in.fcst_depr_method_id = fmrp.fcst_depr_method_id 
            )
				and DCS.DEPR_METHOD_ID = FMRP.FCST_DEPR_METHOD_ID
				and DCS.DEPR_GROUP_ID = FDVS.FCST_DEPR_GROUP_ID
        and DCS.SET_OF_BOOKS_ID = FDVS.SET_OF_BOOKS_ID
				and FMRP.BASIS_TYPE_ID = 3
				and FDVS.ACCUM_COST <> 0
				and DCS.MORTALITY_CURVE_ID <> 0
				and DCS.EXPECTED_AVERAGE_LIFE <> 0
				and FDVS.ACCOUNTING_MONTH = l_month_to_process
				and DCS.GL_POST_MO_YR = l_month_to_process
				and BEG_YEAR.FCST_DEPR_GROUP_ID = FDVS.FCST_DEPR_GROUP_ID
				and BEG_YEAR.SET_OF_BOOKS_ID = FDVS.SET_OF_BOOKS_ID
				and BEG_YEAR.VINTAGE = FDVS.VINTAGE
				and BEG_YEAR.ACCUM_COST <> 0
				;

				if L_FDVS.count > 0 then
					if G_DEBUG then
						PKG_PP_LOG.P_WRITE_MESSAGE('MONTH: ' || to_char(l_month_to_process, 'yyyymm') );
						PKG_PP_LOG.P_WRITE_MESSAGE('   NUM CURVE RETS: ' || to_char( L_FDVS.count ) );
						PKG_PP_LOG.P_WRITE_MESSAGE('   FIRST CURVE INFO: '
													|| ' DEPR_GROUP: ' || to_char(L_FDVS(1).FCST_DEPR_GROUP_ID)
													|| ' SET_OF_BOOKS_ID: ' || to_char(L_FDVS(1).SET_OF_BOOKS_ID)
													|| ' VINTAGE: ' || to_char(L_FDVS(1).VINTAGE)
													|| ' RETIREMENTS: ' || to_char(L_FDVS(1).RETIREMENTS) );
					end if;

					-- update fcst_depr_vintage summary retirements and accum cost (accum cost for all months >= current month)
					forall ndx in indices of L_FDVS
						update FCST_DEPR_VINTAGE_SUMMARY FDVS
						set FDVS.ACCUM_COST = FDVS.ACCUM_COST + L_FDVS(ndx).RETIREMENTS,
							FDVS.RETIREMENTS = FDVS.RETIREMENTS + decode(FDVS.ACCOUNTING_MONTH, l_month_to_process, L_FDVS(ndx).RETIREMENTS, 0)
						where FDVS.FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
						and FDVS.SET_OF_BOOKS_ID = L_FDVS(ndx).SET_OF_BOOKS_ID
						and FDVS.FCST_DEPR_GROUP_ID = L_FDVS(ndx).FCST_DEPR_GROUP_ID
						and FDVS.VINTAGE = L_FDVS(ndx).VINTAGE
						and FDVS.ACCOUNTING_MONTH >= l_month_to_process
						;

				end if;

				-- increase fiscal month by 1
				L_FISCALMONTH := case when L_FISCALMONTH = 12 then 1 else L_FISCALMONTH + 1 end;
			end loop;
		end if;

		PKG_PP_LOG.P_WRITE_MESSAGE('DONE with CURVE retirements');
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation: ' || sqlerrm);
	end P_AUTO_RET_CURVE;

	procedure P_BUILD_VINTAGE_SUMMARY( 	A_FCST_DEPR_VERSION FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE,
										A_START_MONTH date,
										A_END_MONTH date) is              
	begin
		/*
		||============================================================================
		|| Object Name: P_BUILD_VINTAGE_SUMMARY
		|| Description: Builds the vintaged data by month from fcst_depr_ledger
		||
		||	Arguments:
		||               A_FCST_DEPR_VERSION: FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE: The forecasted depreciation version to build the depr vintage summary
		||               A_START_MONTH: date: the start date for forecasting depreciation
		||               A_END_MONTH: date: the start date for forecasting depreciation
		||	Return: NONE
		||
		||============================================================================
		|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
		||============================================================================
		|| Version Date       Revised By     Reason for Change
		|| ------- ---------- -------------- -----------------------------------------
		|| 1.0     02/05/2014 Kyle Peterson    Original Version
		||============================================================================
		*/
    
    
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_FCST.P_BUILD_VINTAGE_SUMMARY');

		-- handle the main insert into depr_vintage_summary
		P_BUILD_VINTAGE_SUMMARY_MAIN( A_FCST_DEPR_VERSION, A_START_MONTH, A_END_MONTH);

		-- handle retirements not processed via auto retirement process
		P_AUTO_RET_FIFO( A_FCST_DEPR_VERSION, A_START_MONTH, A_END_MONTH);
      	P_AUTO_RET_CURVE( A_FCST_DEPR_VERSION, A_START_MONTH, A_END_MONTH);

		delete from fcst_depr_vintage_summary
		where FCST_DEPR_VERSION_ID = A_FCST_DEPR_VERSION
		and ACCOUNTING_MONTH between A_START_MONTH and A_END_MONTH
		and ACCUM_COST = 0
		and ADDITIONS = 0
		and TRANSFERS_IN = 0
		and TRANSFERS_OUT = 0
		and ADJUSTMENTS = 0
		and RETIREMENTS = 0
		and fcst_depr_group_id in
		(
			select a.fcst_depr_group_id
			from depr_fcst_group_stg a
		)
		;

		--Move our retirements from FCST_DEPR_VINTAGE_SUMMARY to DEPR_CALC_STG
		P_SUMMARIZE_RETS();

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_BUILD_VINTAGE_SUMMARY;

end;

/

--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (3247, 0, 2016, 1, 0, 0, 0, 'C:\PlasticWKS\PowerPlant\sql\packages', '2016.1.0.0_PKG_DEPR_FCST.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
