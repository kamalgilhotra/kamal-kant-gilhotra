  CREATE OR REPLACE PACKAGE "PWRPLANT"."TAX_ACCRUAL_CONTROL_PKG"
as
   G_PKG_VERSION varchar(35) := '2018.2.0.0';
   TYPE id_t               IS TABLE OF number(22)                                         INDEX BY BINARY_INTEGER;
   TYPE gl_month_t         IS TABLE OF tax_accrual_gl_month_def.gl_month%TYPE             INDEX BY BINARY_INTEGER;
   TYPE currency_t         IS TABLE OF number(22,2)                                       INDEX BY BINARY_INTEGER;
   TYPE rate_t             IS TABLE OF number(22,8)                                       INDEX BY BINARY_INTEGER;
   TYPE indicator_t        IS TABLE OF number(2)                                          INDEX BY BINARY_INTEGER;
   TYPE long_t             IS TABLE OF number(22)                                         INDEX BY BINARY_INTEGER;
   TYPE spread_pct_t       IS TABLE OF tax_accrual_spread_template.ytd_percent%TYPE       INDEX BY BINARY_INTEGER; /*###PATCH(513)*/

   -- Sets the global ta_version_id variable
   PROCEDURE p_set_ta_version_id(a_ta_version_id in tax_accrual_version.ta_version_id%TYPE);

   -- Returns the current ta_version_id
   FUNCTION f_get_ta_version_id RETURN number;

   -- Sets the global variable that is used to determine whether or not we are in debug mode
   -- The value is retrieved from the tax_accrual_system_control wht control name 'Debug PL/SQL'
   PROCEDURE p_refresh_debug_status;

   -- Returns a TRUE/FALSE to indicate whether or not debugging is on or off
   FUNCTION f_get_debug_status RETURN boolean;

   -- Sets the global debug variable to TRUE, enabling debugging
   -- Serves as a session-level override if the debug system control is set to off
   PROCEDURE p_enable_debug;

   -- Sets the global debug variable to FALSE, disabling debugging
   PROCEDURE p_disable_debug;

end tax_accrual_control_pkg;
/

CREATE OR REPLACE PACKAGE BODY "PWRPLANT"."TAX_ACCRUAL_CONTROL_PKG"
AS

   g_ta_version_id   tax_accrual_version.ta_version_id%TYPE;
   g_debug           boolean := FALSE;

   --**************************************************************************************
   --
   -- p_set_ta_version_id
   --
   --**************************************************************************************
   PROCEDURE p_set_ta_version_id(a_ta_version_id in tax_accrual_version.ta_version_id%TYPE)
   is
   begin
      g_ta_version_id := a_ta_version_id;
   end p_set_ta_version_id;

   -- f_get_ta_version_id
   FUNCTION f_get_ta_version_id return number
   is
   begin
      return nvl(g_ta_version_id,0);
   end f_get_ta_version_id;

   --**************************************************************************************
   --
   -- p_refresh_debug_status
   --
   --**************************************************************************************
   PROCEDURE p_refresh_debug_status
   is
      ta_control_value  tax_accrual_system_control.control_value%TYPE;
   begin
      select upper(trim(control_value))
      into ta_control_value
      from tax_accrual_system_control
      where upper(trim(control_name)) = 'DEBUG PL/SQL';

      if ta_control_value = 'YES' then
         g_debug := TRUE;
      else
         g_debug := FALSE;
      end if;

   end p_refresh_debug_status;

   --**************************************************************************************
   --
   -- f_get_debug_status
   --
   --**************************************************************************************
   FUNCTION f_get_debug_status return boolean
   is
   begin
      return g_debug;
   end f_get_debug_status;

   --**************************************************************************************
   --
   -- p_enable_debug
   --
   --**************************************************************************************
   PROCEDURE p_enable_debug
   is
   begin
      g_debug := TRUE;
   end p_enable_debug;

   --**************************************************************************************
   --
   -- p_disable_debug
   --
   --**************************************************************************************
   PROCEDURE p_disable_debug
   is
   begin
      g_debug := FALSE;
   end p_disable_debug;

   --**************************************************************************************
   -- Initialization procedure (Called the first time this package is opened in a session)
   -- Initialize the debug variable
   --**************************************************************************************
   begin
      p_refresh_debug_status;

end tax_accrual_control_pkg;
--End of Package Body
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (16310, 0, 2018, 2, 0, 0, 0, 'c:\plasticwks\powerplant\sql\packages', 
    'TAX_ACCRUAL_CONTROL_PKG.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
