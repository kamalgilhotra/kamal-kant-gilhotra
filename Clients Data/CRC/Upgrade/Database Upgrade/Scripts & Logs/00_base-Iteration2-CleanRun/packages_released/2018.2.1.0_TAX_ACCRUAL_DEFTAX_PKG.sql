--||============================================================================
--|| Application: PowerPlant
--|| Object Name: tax_accrual_deftax_pkg
--|| Description: Calculates and updates deferred taxes in the tax_accrual_fas109
--||               and tax_accrual_def_tax tables.  This was originally done
--||               within PowerPlant, but the datawindow updates killed
--||               performance.  Using BULK COLLECT and FORALL statements
--||               in this code really speeds things up.
--||============================================================================
--|| Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
--||============================================================================
--|| Patch           Date       Revised By     Reason for Change
--|| -------------   ---------- -------------- ----------------------------------
--|| ###PATCH(403)   07.01.2010 Blake Andrews  Initial Creation
--|| ###PATCH(8048)  07.13.2011 Blake Andrews  See Webmaint
--|| 2017.2          3/8/2018   Crystal Yura   PP-50685 Penny Fas109 Calc Issue
--||============================================================================

--Main Tax Accrual Oracle Package Declaration
create or replace package pwrplant.tax_accrual_deftax_pkg
as
   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   SUBTYPE id_t            IS TAX_ACCRUAL_CONTROL_PKG.ID_T;
   SUBTYPE gl_month_t      IS TAX_ACCRUAL_CONTROL_PKG.GL_MONTH_T;
   SUBTYPE currency_t      IS TAX_ACCRUAL_CONTROL_PKG.CURRENCY_T;
   SUBTYPE rate_t          IS TAX_ACCRUAL_CONTROL_PKG.RATE_T;
   SUBTYPE indicator_t     IS TAX_ACCRUAL_CONTROL_PKG.INDICATOR_T;
   SUBTYPE long_t          IS TAX_ACCRUAL_CONTROL_PKG.LONG_T;
   SUBTYPE spread_pct_t    IS TAX_ACCRUAL_CONTROL_PKG.SPREAD_PCT_T; /*###PATCH(513)*/

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_fas109_calc: Calculate FAS109 deferred taxes, including grossup                                        --
   --                                                                                                          --
   -- Note:  All arrays are related.  For example, a_m_id[1], a_ta_norm_id[1], and a_gl_month[1] represent     --
   --                                  a record in tax_accrual_fas109.                                         --
   --                                                                                                          --
   -- Parameters: a_calc_beg_bal:   1 = Calculate beginning FAS109 balances; 0 = Do not calculate beg balances --
   --             a_update:         1 = Update the database with calculated amounts; 0 = Do not update         --
   --             a_m_id through a_entity_grossup_current:  These individual arrays represent the records in   --
   --                                                          tax_accrual_fas109 that are being processed.    --
   --             a_reg_ing:        The reg_indicator on tax_accrual_fas109_control that specifies whether or  --
   --                                  not grossup should be calculated on the current record.                 --
   --             a_rate_increment: The deferred income tax rate used to calculate the FAS109 liability.  This --
   --                                  is statutory_rate on deferred_income_tax_rates.                         --
   --             a_rate_entity_grossup:  The deferred_income_tax_rate used in the calculation of the entity   --
   --                                        grossup.  This is annual_rate on deferred_income_tax_rates.       --
   --             a_rate_total_grossup:   The deferred_income_tax_rate used in the calculation of total        --
   --                                        grossup.  This is grossup_rate on deferred_income_tax_rates.      --
   --             a_rate_total_increment: The total of all FAS109 increments for the entities that are to      --
   --                                        be included in the calculation of total grossup.                  --
   --             a_m_beg_bal:      The beginning balance for the month on tax_accrual_m_item.                 --
   --             a_m_end_bal:      The ending balance for the month on tax_accrual_m_item.                    --
   --             a_m_activity:     The total activity on the M (including non-current adjustments) for the    --
   --                                  month.                                                                  --
   --             a_dt_beg_bal:     The beginning balance for the month and ta_norm_id on tax_accrual_def_tax  --
   --             a_dt_end_bal:     The ending balance for the month and ta_norm_id on tax_accrual_def_tax     --
   --             a_dt_activity:    The total activity for the month and ta_norm_id on tax_accrual_def_tax     --
   --             a_first_month_ind:   A 1/0 indicator specifying whether or not this is the first month in the--
   --                                     case.  We only calculate beginning balances for the first month.     --
   --             a_ytd_include:    A 1/0 indicator specifying whether or not the current month activity should--
   --                                  be included in the calculation of year-to-date deferred taxes.  0 means --
   --                                  "include in year-to-date calculations."                                 --
   --             a_do_not_update:  A 1/0 indicator specifying whether or not we should calculate the FAS109   --
   --                                  deferred tax activity for the current item in the current month.  We    --
   --                                  do not want to calculate deferred taxes if this is an RTP month for     --
   --                                  example.                                                                --
   --             a_process_option_id: An identifier specifying whether this is a normal month, a standalone   --
   --                                     month, or an RTP month.  We only true up year-to-date activity when  --
   --                                     a_process_option_id = 2.                                             --
   --             a_calc_type:      1 = Trueup deferred tax balances.  0 = Do not trueup deferred tax balances --
   --             a_ytd_m_activity: The year-to-date M Item activity through the current month for all months  --
   --                                  where ytd_include = 0.  This includes activity in amount_adj.  Used in  --
   --                                  year-to-date calcs.
   --             a_total_dt_activity: The total current month activity in tax_accrual_def_tax for all         --
   --                                     ta_norm_ids.  Used in the calculation of the total grossup.          --
   --             a_ytd_total_dt_activity:   The total year-to-date deferred tax activity through the current  --
   --                                           month for all months where ytd_include = 0.  Used in the       --
   --                                           calculation of the total grossup for year-to-date calcs.       --
   --             a_total_dt_beg_bal:  The total beginning balance in tax_accrual_def_tax for all ta_norm_ids  --
   --                                     Used in the calculation of the total grossup.                        --
   --             a_total_dt_end_bal:  The total ending balance in tax_accrual_def_tax for all ta_norm_ids     --
   --                                     Used in the calculation of the total grossup.                        --
   --             a_ytd_dt_activity:   The year-to-date deferred tax activity through the current month.       --
   --                                     Used in year-to-date calcs.
   --             a_fas109_liab_ytd:   The total year-to-date activity through the prior month in              --
   --                                     tax_accrual_fas109.fas109_liab_end - fas109_liab_beg for all months  --
   --                                     where ytd_include = 0.  Used in year-to-date calcs.                  --
   --             a_fas109_increment_ytd: The total year-to-date activity through the prior month in           --
   --                                        tax_accrual_fas109.fas109_increment_end - fas109_increment_beg    --
   --                                        for all months where ytd_include = 0.  Used in year-to-date calcs.--
   --             a_total_grossup_ytd: The total year-to-date activity through the prior month in              --
   --                                     tax_accrual_fas109.total_grossup_end - total_grossup_beg             --
   --                                     for all months where ytd_include = 0.  Used in year-to-date calcs.   --
   --             a_entity_grossup_ytd:   The total year-to-date activity through the prior month in           --
   --                                        tax_accrual_fas109.entity_grossup_end - entity_grossup_beg        --
   --                                        for all months where ytd_include = 0.  Used in year-to-date calcs --
   --                                  current case and companies.      
   --			  a_prev_ytd_m_activity:	The one-period-lagged year-to-date M item activity. This amount is equivalent to the prior-
   --												month year-to-date balance and is used to re-calculate prior-month deferred taxes
   --												to compensate for rounding errors that caused penny variances in adjustment months.
   --			  a_prev_rate_increment:	The one-period-lagged FAS109 increment. Used to compensate for rounding errors that caused
   --												penny variances in adjustment months.
   --			  a_prev_ytd_dt_activity:	The one-period-lagged year-to-date APB11 deferred tax activity (tax_accrual_def_tax) used for calculations
   --												Used to compensate for rounding errors that caused penny variances in adjustment months.
   --			  a_prev_rate_entity_grossup:	The one-period-lagged entity grossup. Used to compensate for rounding errors that caused penny variances
   --												in adjustment months.	
   --			  a_prev_rate_total_grossup:  The one-period-lagged total grossup. Used to compensate for rounding errors that caused penny variances
   --												in adjustment months.		
   --			  a_prev_rate_total_increment: The one-period-lagged total FAS109 increment. Used to compensate for rounding errors that caused penny variances
   --												in adjustment months.			
   --			  a_prev_ytd_total_dt_activity: The one-period-lagged total APB11 deferred taxes. Used to compensate for rounding errors that caused penny variances
   --												in adjustment months.	   --
   --             a_msg:            A string passed back to the calling function containing any error messages.--
   -- ******************************************************************************************************** --
   FUNCTION f_fas109_calc  (  a_calc_beg_bal             in number,
                              a_update                   in number,
                              a_m_id                     in out id_t,
                              a_ta_norm_id               in out id_t,
                              a_gl_month                 in out gl_month_t,
                              a_accum_diff_beg           in out currency_t,
                              a_accum_diff_end           in out currency_t,
                              a_accum_reg_dit_beg        in out currency_t,
                              a_accum_reg_dit_end        in out currency_t,
                              a_fas109_liab_beg          in out currency_t,
                              a_fas109_liab_end          in out currency_t,
                              a_fas109_increment_beg     in out currency_t,
                              a_fas109_increment_end     in out currency_t,
                              a_total_grossup_beg        in out currency_t,
                              a_total_grossup_end        in out currency_t,
                              a_entity_grossup_beg       in out currency_t,
                              a_entity_grossup_end       in out currency_t,
                              a_fas109_liab_current      in out currency_t,
                              a_entity_grossup_current   in out currency_t,
                              a_reg_ind                  in id_t,
                              a_rate_increment           in rate_t,
                              a_rate_entity_grossup      in rate_t,
                              a_rate_total_grossup       in rate_t,
                              a_rate_total_increment     in rate_t,
                              a_m_beg_bal                in currency_t,
                              a_m_end_bal                in currency_t,
                              a_m_activity               in currency_t,
                              a_dt_beg_bal               in currency_t,
                              a_dt_end_bal               in currency_t,
                              a_dt_activity              in currency_t,
                              a_first_month_ind          in indicator_t,
                              a_ytd_include              in indicator_t,
                              a_do_not_update            in indicator_t,
                              a_process_option_id        in id_t,
                              a_calc_type                in indicator_t,
                              a_ytd_m_activity           in currency_t,
                              a_total_dt_activity        in currency_t,
                              a_ytd_total_dt_activity    in currency_t,
                              a_total_dt_beg_bal         in currency_t,
                              a_total_dt_end_bal         in currency_t,
                              a_ytd_dt_activity          in currency_t,
                              a_fas109_liab_ytd          in currency_t,
                              a_fas109_increment_ytd     in currency_t,
                              a_total_grossup_ytd        in currency_t,
                              a_entity_grossup_ytd       in currency_t,
                              a_prev_ytd_m_activity      in currency_t,
                    	      a_prev_rate_increment      in rate_t,
                              a_prev_ytd_dt_activity     in currency_t,
                              a_prev_rate_entity_grossup in rate_t,
                              a_prev_rate_total_grossup  in rate_t,
                              a_prev_rate_total_increment in rate_t,
                              a_prev_ytd_total_dt_activity in currency_t,
                              a_msg                      out varchar2
                           ) RETURN NUMBER;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_fas109_calc: Call the other f_fas109_calc function after retrieving the data necessary to complete the --
   --                   calc, and updating the RTP months with activity from other cases.                      --
   --                                                                                                          --
   --                                                                                                          --
   -- Parameters: a_fas109_sqls: An SQL statement that populates tax_accrual_fas109_proc_tmp with the data     --
   --                               necessary to complete the FAS109 calc.                                     --
   --             a_gl_month: The Provision month currently being processed.                                   --
   --             a_trueup_insert_sqls:   An SQL statement that populates tax_accrual_rtp_proc_tmp with the    --
   --                                        data necessary to populate the RTP months in                      --
   --                                        tax_accrual_fas109_proc_tmp.                                      --
   --             a_trueup_update_sqls:   An SQL statement that updates tax_accrual_fas109_proc_tmp with data  --
   --                                        from tax_accrual_rtp_proc_tmp.                                    --
   --             a_msg:            A string passed back to the calling function containing any error messages.--
   -- ******************************************************************************************************** --
   FUNCTION f_fas109_calc  (  a_fas109_sqls        in varchar2,
                              a_gl_month           in tax_accrual_gl_month_def.gl_month%TYPE,
                              a_trueup_insert_sqls in varchar2,
                              a_trueup_update_sqls in varchar2,
                              a_msg                OUT varchar2
                           ) RETURN NUMBER;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_fas109_roll: Roll FAS109 balances beginning with the month passed in.  Only m_ids that are in          --
   --                   tax_accrual_rep_criteria_tmp with criteria_type = 'FAS109_M_ID' will be rolled.        --
   --                                                                                                          --
   -- Parameters: a_gl_month: The Provision month that balances will be rolled from (balances prior to this    --
   --                            month will remain unchanged)                                                  --
   --             a_msg:            A string passed back to the calling function containing any error messages.--
   -- ******************************************************************************************************** --
   -- Function to roll FAS109 balances beginning with the month passed in
   FUNCTION f_fas109_roll  (  a_gl_month     in tax_accrual_gl_month_def.gl_month%TYPE,
                              a_msg          OUT varchar2
                           ) RETURN NUMBER;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_fas109_update:  Update tax_accrual_fas109 with the values passed in.                                   --
   --                                                                                                          --
   -- Parameters: a_m_id through a_entity_grossup_current:  Arrays of values that represent the FAS109 records --
   --                   being updated.
   --             a_msg:            A string passed back to the calling function containing any error messages.--
   -- ******************************************************************************************************** --
   FUNCTION f_fas109_update   (  a_m_id                     in id_t,
                                 a_ta_norm_id               in id_t,
                                 a_gl_month                 in gl_month_t,
                                 a_accum_diff_beg           in currency_t,
                                 a_accum_diff_end           in currency_t,
                                 a_accum_reg_dit_beg        in currency_t,
                                 a_accum_reg_dit_end        in currency_t,
                                 a_fas109_liab_beg          in currency_t,
                                 a_fas109_liab_end          in currency_t,
                                 a_fas109_increment_beg     in currency_t,
                                 a_fas109_increment_end     in currency_t,
                                 a_total_grossup_beg        in currency_t,
                                 a_total_grossup_end        in currency_t,
                                 a_entity_grossup_beg       in currency_t,
                                 a_entity_grossup_end       in currency_t,
                                 a_fas109_liab_current      in currency_t,
                                 a_entity_grossup_current   in currency_t,
                                 a_msg OUT varchar2
                              ) RETURN NUMBER;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_deftax_calc: Calculates the APB11 deferred taxes to be updated to tax_accrual_def_tax.                 --
   --                                                                                                          --
   -- Note:  All arrays are related.  For example, a_m_id[1], a_ta_norm_id[1], and a_gl_month[1] represent     --
   --                                  a record in tax_accrual_def_tax.                                        --
   -- Parameters: a_update:         1 = Update the database with calculated amounts; 0 = Do not update         --
   --             a_m_id through a_amount_manual:  Arrays of values that represent the tax_accrual_def_tax     --
   --                records                                                                                   --
   --             a_m_beg_bal:   The beginning balance on tax_accrual_m_item for the month.                    --
   --             a_m_activity:  The M activity (including amount_adj) in the current month.                   --
   --             a_rate:        The deferred tax rate to be used to calculate the APB11 deferred tax.  This   --
   --                               comes from monthly_rate on deferred_income_tax_rates.                      --
   --             a_first_month_ind:   1/0 indicator specifying whether or not this is the first month in the  --
   --                                     case.  This is not currently used in this function.                  --
   --             a_ytd_include: A 1/0 indicator specifying whether or not the current month activity should   --
   --                               be included in year-to-date calculations.  1 = exclude from year-to-date   --
   --                               calculations.                                                              --
   --             a_do_not_update:  A 1/0 indicator specifying whether or not we should calculate the          --
   --                                  deferred tax activity for the current item in the current month.  We    --
   --                                  do not want to calculate deferred taxes if this is an RTP month for     --
   --                                  example.                                                                --
   --             a_process_option_id: An identifier specifying whether this is a normal month, a standalone   --
   --                                     month, or an RTP month.  We only calculate APB11 deferred tax        --
   --                                     activity when a_process_option_id = 2.                               --
   --             a_ytd_m_activity: The year-to-date activity in tax_accrual_m_item (including amount_adj)     --
   --                                  for months where ytd_include = 0.                                       --
   --             a_ytd_m_activity_prov:  The year-to-date activity in tax_accrual_m_item (including           --
   --                                        amount_adj) for months where ytd_include = 0 and the activity is  --
   --                                        providing (sign on activity = sign on deferred tax beg balance)   --
   --             a_ytd_dt_activity:   The year-to-date activity in tax_accrual_def_tax for months where       --
   --                                     ytd_include = 0.                                                     --
   --             a_ytd_dt_activity_prov: The year-to-date activity in tax_accrual_def_tax for months where    --
   --                                        ytd_include = 0 and the activity is providing (sign on M activity --
   --                                        = sign on deferred tax beg balance)                               --
   --             a_aram_ind: 0 = Calculate deferred taxes using the current rate (a_rate)                     --
   --                         1 = Calculate deferred taxes using ARAM (Def Tax Beg Bal/M Item Beg Bal)         --
   --             a_msg:            A string passed back to the calling function containing any error messages.--
   -- ******************************************************************************************************** --
   FUNCTION f_deftax_calc  (  a_update                in number,
                              a_m_id                  in out id_t,
                              a_ta_norm_id            in out id_t,
                              a_gl_month              in out gl_month_t,
                              a_beg_balance           in out currency_t,
                              a_end_balance           in out currency_t,
                              a_amount_est            in out currency_t,
                              a_amount_act            in out currency_t,
                              a_amount_calc           in out currency_t,
                              a_amount_manual         in out currency_t,
                              a_m_beg_bal             in currency_t,
                              a_m_activity            in currency_t,
                              a_rate                  in rate_t,
                              a_first_month_ind       in indicator_t,
                              a_ytd_include           in indicator_t,
                              a_do_not_update         in indicator_t,
                              a_process_option_id     in id_t,
                              a_ytd_m_activity        in currency_t,
                              a_ytd_m_activity_prov   in currency_t,
                              a_ytd_dt_activity       in currency_t,
                              a_ytd_dt_activity_prov  in currency_t,
                              a_aram_ind              in id_t,
                              a_msg                   out varchar2
                           ) RETURN NUMBER;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_def_tax_calc:   Call the other f_def_tax_calc function after retrieving the data necessary to complete --
   --                   the calc, and updating the RTP months with activity from other cases.                  --
   --                                                                                                          --
   --                                                                                                          --
   -- Parameters: a_deftax_sqls: An SQL statement that populates tax_accrual_def_tax_proc_tmp with the data    --
   --                               necessary to complete the Def Tax calc.                                    --
   --             a_gl_month: The Provision month currently being processed.                                   --
   --             a_trueup_insert_sqls:   An SQL statement that populates tax_accrual_rtp_proc_tmp with the    --
   --                                        data necessary to populate the RTP months in                      --
   --                                        tax_accrual_def_tax_proc_tmp.                                     --
   --             a_trueup_update_sqls:   An SQL statement that updates tax_accrual_def_tax_proc_tmp with data --
   --                                        from tax_accrual_rtp_proc_tmp.                                    --
   --             a_msg:            A string passed back to the calling function containing any error messages.--
   -- ******************************************************************************************************** --
   FUNCTION f_deftax_calc  (  a_deftax_sqls in varchar2,
                              a_gl_month in tax_accrual_gl_month_def.gl_month%TYPE,
                              a_trueup_insert_sqls in varchar2,
                              a_trueup_update_sqls in varchar2,
                              a_msg OUT varchar2
                           ) RETURN NUMBER;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_fas109_update:  Update tax_accrual_def_tax with the values passed in.                                  --
   --                                                                                                          --
   -- Parameters: a_m_id through a_amount_manual:  Arrays of values that represent the Def Tax records         --
   --                   being updated.                                                                         --
   --             a_msg:            A string passed back to the calling function containing any error messages.--
   -- ******************************************************************************************************** --
   FUNCTION f_deftax_update(  a_m_id            in id_t,
                              a_ta_norm_id      in id_t,
                              a_gl_month        in gl_month_t,
                              a_beg_balance     in currency_t,
                              a_end_balance     in currency_t,
                              a_amount_est      in currency_t,
                              a_amount_act      in currency_t,
                              a_amount_calc     in currency_t,
                              a_amount_manual   in currency_t,
                              a_msg          OUT varchar2
                           ) RETURN NUMBER;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_deftax_roll: Roll Def Tax balances beginning with the month passed in.  Only m_ids that are in         --
   --                   tax_accrual_rep_criteria_tmp with criteria_type = 'DEF_TAX_M_ID' will be rolled.       --
   --                                                                                                          --
   -- Parameters: a_gl_month: The Provision month that balances will be rolled from (balances prior to this    --
   --                            month will remain unchanged)                                                  --
   --             a_msg:            A string passed back to the calling function containing any error messages.--
   -- ******************************************************************************************************** --
   FUNCTION f_deftax_roll  (  a_gl_month     in tax_accrual_gl_month_def.gl_month%TYPE,
                              a_msg          OUT varchar2
                           ) RETURN NUMBER;
end tax_accrual_deftax_pkg;
/

-- ******************************************************************************************************** --
--                                                                                                          --
--                                                                                                          --
--                                                                                                          --
--  PACKAGE BODY                                                                                            --
--                                                                                                          --
--                                                                                                          --
--                                                                                                          --
-- ******************************************************************************************************** --
create or replace package body tax_accrual_deftax_pkg
AS

   SUBTYPE currency_type   IS number(22,2);
   SUBTYPE rate_type       IS number(22,8);

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_fas109_update (Update tax_accrual_fas109 with values passed in)                                        --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_fas109_update   (  a_m_id                     in id_t,
                                 a_ta_norm_id               in id_t,
                                 a_gl_month                 in gl_month_t,
                                 a_accum_diff_beg           in currency_t,
                                 a_accum_diff_end           in currency_t,
                                 a_accum_reg_dit_beg        in currency_t,
                                 a_accum_reg_dit_end        in currency_t,
                                 a_fas109_liab_beg          in currency_t,
                                 a_fas109_liab_end          in currency_t,
                                 a_fas109_increment_beg     in currency_t,
                                 a_fas109_increment_end     in currency_t,
                                 a_total_grossup_beg        in currency_t,
                                 a_total_grossup_end        in currency_t,
                                 a_entity_grossup_beg       in currency_t,
                                 a_entity_grossup_end       in currency_t,
                                 a_fas109_liab_current      in currency_t,
                                 a_entity_grossup_current   in currency_t,
                                 a_msg OUT varchar2
                              ) RETURN NUMBER
   is
      num_rows number(22);
      sql_msg varchar2(254);
   begin
      num_rows := a_m_id.COUNT;
      sql_msg := 'Updating FAS109 Table with new amounts';
      FORALL indx IN 1 .. num_rows
         update tax_accrual_fas109
         set   accum_diff_beg = a_accum_diff_beg(indx),
               accum_diff_end = a_accum_diff_end(indx),
               accum_reg_dit_beg = a_accum_reg_dit_beg(indx),
               accum_reg_dit_end = a_accum_reg_dit_end(indx),
               fas109_liab_beg = a_fas109_liab_beg(indx),
               fas109_liab_end = a_fas109_liab_end(indx),
               fas109_increment_beg = a_fas109_increment_beg(indx),
               fas109_increment_end = a_fas109_increment_end(indx),
               total_grossup_beg = a_total_grossup_beg(indx),
               total_grossup_end = a_total_grossup_end(indx),
               entity_grossup_beg = a_entity_grossup_beg(indx),
               entity_grossup_end = a_entity_grossup_end(indx),
               fas109_liab_current = a_fas109_liab_current(indx),
               entity_grossup_current = a_entity_grossup_current(indx)
         where m_id = a_m_id(indx)
            and ta_norm_id = a_ta_norm_id(indx)
            and gl_month = a_gl_month(indx);

      a_msg := 'SUCCESS';
      return 1;
   EXCEPTION
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;

   end;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_fas109_calc (Overloaded function - first version)                                                      --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_fas109_calc  (  a_calc_beg_bal             in number,
                              a_update                   in number,
                              a_m_id                     in out id_t,
                              a_ta_norm_id               in out id_t,
                              a_gl_month                 in out gl_month_t,
                              a_accum_diff_beg           in out currency_t,
                              a_accum_diff_end           in out currency_t,
                              a_accum_reg_dit_beg        in out currency_t,
                              a_accum_reg_dit_end        in out currency_t,
                              a_fas109_liab_beg          in out currency_t,
                              a_fas109_liab_end          in out currency_t,
                              a_fas109_increment_beg     in out currency_t,
                              a_fas109_increment_end     in out currency_t,
                              a_total_grossup_beg        in out currency_t,
                              a_total_grossup_end        in out currency_t,
                              a_entity_grossup_beg       in out currency_t,
                              a_entity_grossup_end       in out currency_t,
                              a_fas109_liab_current      in out currency_t,
                              a_entity_grossup_current   in out currency_t,
                              a_reg_ind                  in id_t,
                              a_rate_increment           in rate_t,
                              a_rate_entity_grossup      in rate_t,
                              a_rate_total_grossup       in rate_t,
                              a_rate_total_increment     in rate_t,
                              a_m_beg_bal                in currency_t,
                              a_m_end_bal                in currency_t,
                              a_m_activity               in currency_t,
                              a_dt_beg_bal               in currency_t,
                              a_dt_end_bal               in currency_t,
                              a_dt_activity              in currency_t,
                              a_first_month_ind          in indicator_t,
                              a_ytd_include              in indicator_t,
                              a_do_not_update            in indicator_t,
                              a_process_option_id        in id_t,
                              a_calc_type                in indicator_t,
                              a_ytd_m_activity           in currency_t,
                              a_total_dt_activity        in currency_t,
                              a_ytd_total_dt_activity    in currency_t,
                              a_total_dt_beg_bal         in currency_t,
                              a_total_dt_end_bal         in currency_t,
                              a_ytd_dt_activity          in currency_t,
                              a_fas109_liab_ytd          in currency_t,
                              a_fas109_increment_ytd     in currency_t,
                              a_total_grossup_ytd        in currency_t,
                              a_entity_grossup_ytd       in currency_t,
                              a_prev_ytd_m_activity      in currency_t,
                    	      a_prev_rate_increment      in rate_t,
                    	      a_prev_ytd_dt_activity     in currency_t,
                    	      a_prev_rate_entity_grossup in rate_t,
                    	      a_prev_rate_total_grossup  in rate_t,
                    	      a_prev_rate_total_increment in rate_t,
                    	      a_prev_ytd_total_dt_activity in currency_t,
                              a_msg                      out varchar2
                           ) RETURN NUMBER
   IS

      sql_msg                       varchar2(254);
      msg                           varchar2(2000);
      rtn                           number(22);
      prev_m_id                     tax_accrual_fas109.m_id%TYPE := 0;
      prev_ta_norm_id               tax_accrual_fas109.ta_norm_id%TYPE := 0;
      accum_diff_roll               tax_accrual_fas109.accum_diff_end%TYPE := 0;
      accum_reg_dit_roll            tax_accrual_fas109.accum_reg_dit_end%TYPE := 0;
      fas109_liab_roll              tax_accrual_fas109.fas109_liab_end%TYPE := 0;
      fas109_increment_roll         tax_accrual_fas109.fas109_increment_end%TYPE := 0;
      total_grossup_roll            tax_accrual_fas109.total_grossup_end%TYPE := 0;
      entity_grossup_roll           tax_accrual_fas109.entity_grossup_end%TYPE := 0;
      l_fas109_liab_ytd             tax_accrual_fas109.fas109_liab_end%TYPE := 0;
      l_fas109_increment_ytd        tax_accrual_fas109.fas109_increment_end%TYPE := 0;
      l_total_grossup_ytd           tax_accrual_fas109.total_grossup_end%TYPE := 0;
      l_entity_grossup_ytd          tax_accrual_fas109.entity_grossup_end%TYPE := 0;
      total_increment               rate_type := 0;
      num_rows                      number(22);
      num_update_rows               number(22);
      i                             number(22) := 0;
      j                             number(22) := 0;
      m_id_update                   id_t;
      ta_norm_id_update             id_t;
      gl_month_update               gl_month_t;
      accum_diff_beg_update         currency_t;
      accum_diff_end_update         currency_t;
      accum_reg_dit_beg_update      currency_t;
      accum_reg_dit_end_update      currency_t;
      fas109_liab_beg_update        currency_t;
      fas109_liab_end_update        currency_t;
      fas109_increment_beg_update   currency_t;
      fas109_increment_end_update   currency_t;
      total_grossup_beg_update      currency_t;
      total_grossup_end_update      currency_t;
      entity_grossup_beg_update     currency_t;
      entity_grossup_end_update     currency_t;
      fas109_liab_current_update    currency_t;
      entity_grossup_current_update currency_t;
      accum_diff_beg_orig           currency_type;
      accum_diff_end_orig           currency_type;
      accum_reg_dit_beg_orig        currency_type;
      accum_reg_dit_end_orig        currency_type;
      fas109_liab_beg_orig          currency_type;
      fas109_liab_end_orig          currency_type;
      fas109_increment_beg_orig     currency_type;
      fas109_increment_end_orig     currency_type;
      total_grossup_beg_orig        currency_type;
      total_grossup_end_orig        currency_type;
      entity_grossup_beg_orig       currency_type;
      entity_grossup_end_orig       currency_type;
      fas109_liab_current_orig      currency_type;
      entity_grossup_current_orig   currency_type;
      m_bal_calc                    currency_type;
      dt_bal_calc                   currency_type;
      fas109_liab_ytd_calc          number(22,8);
      fas109_increment_ytd_calc     number(22,8);
      total_grossup_ytd_calc        number(22,8);
      total_increment_ytd_calc      number(22,8);
      entity_grossup_ytd_calc       number(22,8);
      fas109_liab_cm_calc           number(22,8);
      fas109_increment_cm_calc      number(22,8);
      total_grossup_cm_calc         number(22,8);
      total_increment_cm_calc       number(22,8);
      entity_grossup_cm_calc        number(22,8);
      prev_total_grossup_ytd_calc number(22,8);
      prev_entity_grossup_ytd_calc number(22,8);
      prev_total_increment_ytd_calc number(22,8);
      function_name                 varchar2(254) := 'f_fas109_calc(1)';

      err_fas109_update             exception;

   BEGIN

      num_rows := a_m_id.COUNT;

      -- Now do the calculation and roll balances
      -- NOTE: Still need to do something with beg balances if they need to be trued up
      for i in 1 .. num_rows loop

         --Save the original values so that we can confirm whether or not we actually need an update
         --This speeds up processing quite a bit by limiting the number of rows updated.
         accum_diff_beg_orig := a_accum_diff_beg(i);
         accum_diff_end_orig := a_accum_diff_end(i);
         accum_reg_dit_beg_orig := a_accum_reg_dit_beg(i);
         accum_reg_dit_end_orig := a_accum_reg_dit_end(i);
         fas109_liab_beg_orig := a_fas109_liab_beg(i);
         fas109_liab_end_orig := a_fas109_liab_end(i);
         fas109_increment_beg_orig := a_fas109_increment_beg(i);
         fas109_increment_end_orig := a_fas109_increment_end(i);
         total_grossup_beg_orig := a_total_grossup_beg(i);
         total_grossup_end_orig := a_total_grossup_end(i);
         entity_grossup_beg_orig := a_entity_grossup_beg(i);
         entity_grossup_end_orig := a_entity_grossup_end(i);
         fas109_liab_current_orig := a_fas109_liab_current(i);
         entity_grossup_current_orig := a_entity_grossup_current(i);

         -- FAS109 calculations are as follows:
         -- Deferred Rate = deferred_income_tax_rates.statutory_rate
         -- FAS109 Liability = M Item Balance * Deferred Rate
         -- FAS109 Increment = FAS109 Liability - APB11 Deferred Tax Balance (APB11 DefTax is zero for nonregs, so FAS109 increment = FAS109 liability)
         -- Grossup Rate = deferred_income_tax_rates..grossup_rate
         -- Total Grossup = FAS109 Increment * (1- (1/(1-Grossup Rate))) * reg_ind
         -- Rate Total Grossup = Sum of deferred tax rates of all entities with reg_ind = 1
         -- Entity Grossup Rate = deferred_income_tax_rates.annual_rate
         -- Entity Grossup = (Sum of All Increments) * (1/(1-(Rate Total Grossup))) * entity_grossup_rate * reg_ind

         -- If this is the first row for this m_id/ta_norm_id, then get the prior-month values from FAS109 from the
         --    retrieved data instead of using the amounts rolled forward from previous iterations of this loop.  If
         --    the retrieved data is not sorted by m_id, ta_norm_id, then gl_month, this logic falls apart.  This
         --    function assumes that the data passed in has already been sorted correctly.
         if a_m_id(i) <> prev_m_id OR a_ta_norm_id(i) <> prev_ta_norm_id then
            prev_m_id               := a_m_id(i);
            prev_ta_norm_id         := a_ta_norm_id(i);
            l_fas109_liab_ytd       := a_fas109_liab_ytd(i);
            l_fas109_increment_ytd  := a_fas109_increment_ytd(i);
            l_total_grossup_ytd     := a_total_grossup_ytd(i);
            l_entity_grossup_ytd    := a_entity_grossup_ytd(i);

            -- ********************** Beginning Balance Calc ****************************** --
            -- Only calculate the beginning balance if this is the first month in the case
            if a_calc_beg_bal = 1 and a_first_month_ind(i) = 1 then
               a_accum_diff_beg(i) := a_m_beg_bal(i);
               a_accum_reg_dit_beg(i) := a_dt_beg_bal(i);

               a_fas109_liab_beg(i) := round(a_m_beg_bal(i) * a_rate_increment(i),2);
               a_fas109_increment_beg(i) := a_fas109_liab_beg(i) - a_accum_reg_dit_beg(i);

               if a_reg_ind(i) = 1 then
                  if a_rate_total_grossup(i) = 1 then
                     a_total_grossup_beg(i) := 0;
                     a_entity_grossup_beg(i) := 0;
                  else
                     a_total_grossup_beg(i) := a_fas109_increment_beg(i) * (1/(1-a_rate_total_grossup(i)) - 1);
                     total_increment := a_m_beg_bal(i) * a_rate_total_increment(i) - a_total_dt_beg_bal(i);
                     a_entity_grossup_beg(i) := round(total_increment * (1/(1-a_rate_total_grossup(i))) * a_rate_entity_grossup(i),2);
                  end if;
               else -- reg_ind = 0
                  a_total_grossup_beg(i) := 0;
                  a_entity_grossup_beg(i) := 0;
               end if; -- reg_ind = 1
            end if; /*a_calc_beg_bal = 1*/
            -- ********************** End Beginning Balance Calc ****************************** --

            -- If this is the first row for a given m_id and ta_norm_id, then we're not rolling a balance
            --    from the prior row.
            accum_diff_roll         := a_accum_diff_beg(i);
            accum_reg_dit_roll      := a_accum_reg_dit_beg(i);
            fas109_liab_roll        := a_fas109_liab_beg(i);
            fas109_increment_roll   := a_fas109_increment_beg(i);
            total_grossup_roll      := a_total_grossup_beg(i);
            entity_grossup_roll     := a_entity_grossup_beg(i);
         end if;

         -- If we're not re-calculating (like if this month is pulled from another case for instance)
         --    Then just set the end balance = beg_balance + current difference between end and beg balance
         if a_do_not_update(i) = 1 then
            a_accum_diff_end(i) := a_accum_diff_end(i) - a_accum_diff_beg(i) + accum_diff_roll;
            a_accum_reg_dit_end(i) := a_accum_reg_dit_end(i) - a_accum_reg_dit_beg(i) + accum_reg_dit_roll;
            a_fas109_liab_end(i) := a_fas109_liab_end(i) - a_fas109_liab_beg(i) + fas109_liab_roll;
            a_fas109_increment_end(i) := a_fas109_increment_end(i) - a_fas109_increment_beg(i) + fas109_increment_roll;
            a_total_grossup_end(i) := a_total_grossup_end(i) - a_total_grossup_beg(i) + total_grossup_roll;
            a_entity_grossup_end(i) := a_entity_grossup_end(i) - a_entity_grossup_beg(i) + entity_grossup_roll;
         else --do_not_update(i) = 0
            a_accum_diff_end(i) := a_m_end_bal(i);
            a_accum_reg_dit_end(i) := a_dt_end_bal(i);

            -- Do calc based on year-to-date activity.  We may or may not use this year-to-date calc for this month depending on what our
            --    processing option and beg balance trueup option are.
            fas109_liab_ytd_calc       := round(a_ytd_m_activity(i) * a_rate_increment(i),8);
            fas109_increment_ytd_calc  := round(fas109_liab_ytd_calc - a_ytd_dt_activity(i),8);

            if a_reg_ind(i) = 1 then
               if a_rate_total_grossup(i) = 1 then
                  total_grossup_ytd_calc := 0;
                  entity_grossup_ytd_calc := 0;
               else
                  total_grossup_ytd_calc     := round(fas109_increment_ytd_calc * (1/(1-a_rate_total_grossup(i)) - 1),8);
                  total_increment_ytd_calc   := round(a_ytd_m_activity(i) * a_rate_total_increment(i) - a_ytd_total_dt_activity(i),8);
                  entity_grossup_ytd_calc    := round(total_increment_ytd_calc * (1/(1-a_rate_total_grossup(i))) * a_rate_entity_grossup(i),8);
               end if;
            else -- reg_ind = 0
               total_grossup_ytd_calc := 0;
               entity_grossup_ytd_calc := 0;
            end if; -- reg_ind = 1

            -- Do calc based on current month activity. We will only use this calc if this is a standalone month where we're not trueing up
            --    beginning balances.
            fas109_liab_cm_calc        := round(a_m_activity(i) * a_rate_increment(i),8);
            fas109_increment_cm_calc   := round(fas109_liab_cm_calc - a_dt_activity(i),8);

            if a_reg_ind(i) = 1 then
               if a_rate_total_grossup(i) = 1 then
                  total_grossup_cm_calc := 0;
                  entity_grossup_cm_calc := 0;
               else
                  total_grossup_cm_calc := round(fas109_increment_cm_calc * (1/(1-a_rate_total_grossup(i)) - 1),8);
                  total_increment_cm_calc := round(a_m_activity(i) * a_rate_total_increment(i) - a_total_dt_activity(i),8);
                  entity_grossup_cm_calc := round(total_increment_cm_calc * (1/(1-a_rate_total_grossup(i))) * a_rate_entity_grossup(i),8);
               end if;
            else -- reg_ind = 0
               total_grossup_cm_calc := 0;
               entity_grossup_cm_calc := 0;
            end if; -- reg_ind = 1

            -- Do the life-to-date calc.  The FAS109 calc is by definition a life-to-date calc so, except in special circumstances, we should
            --    true up life-to-date balances based on the currently-used deferred rate.
            --PP-50685 Rounding precision changed from 2 to 8 
            a_fas109_liab_end(i) := round(a_m_end_bal(i) * a_rate_increment(i),8);
            a_fas109_increment_end(i) := a_fas109_liab_end(i) - a_accum_reg_dit_end(i);

            if a_reg_ind(i) = 1 then
               if a_rate_total_grossup(i) = 1 then
                  a_total_grossup_end(i) := 0;
                  a_entity_grossup_end(i) := 0;
               else
                  a_total_grossup_end(i) := a_fas109_increment_end(i) * (1/(1-a_rate_total_grossup(i)) - 1);
                  total_increment := a_m_end_bal(i) * a_rate_total_increment(i) - a_total_dt_end_bal(i);
                  a_entity_grossup_end(i) := round(total_increment * (1/(1-a_rate_total_grossup(i))) * a_rate_entity_grossup(i),2);
               end if;
            else -- reg_ind = 0
               a_total_grossup_end(i) := 0;
               a_entity_grossup_end(i) := 0;
            end if; -- reg_ind = 1

	        -- If this is a non-current-year month and we are trueing up year-to-date deferred taxes, then back out the true-up of year-to-date deferred taxes
			if a_calc_type(i) = 1 and a_process_option_id(i) = 1 then
			   --Beginning balance only true-up, the calculation is:
			   --Ending Schedule M * Current Rate (Calculated above) - year-to-date activity at new rate + year-to-date booked     
				  
			   --PP-50685: This part of the FAS109 function was updated because the year-to-date booked amounts would sometimes cause rounding differences to appear in the form
			   --of pennies on Ms without activity. The root cause for this were rounding differences found in the FAS109 table columns. Since the trueup relies on accuracy
			   --to the hundredths and thousandths of a dollar, it was necessary to rework these calculations in order to have the necessary precision. To do this, we lagged
			   --by one period the values used to compute the different amounts by one period and re-calculated rather than just pulling the previous month's ending balance.
				  
			   a_fas109_liab_end(i) := a_fas109_liab_end(i) - fas109_liab_ytd_calc + round(a_prev_ytd_m_activity(i) * a_prev_rate_increment(i),8);
			   a_fas109_increment_end(i) := a_fas109_increment_end(i) - fas109_increment_ytd_calc + round((a_prev_ytd_m_activity(i) * a_prev_rate_increment(i)) - a_prev_ytd_dt_activity(i),8);
				  
			   if a_reg_ind(i) = 1 then
				  if a_prev_rate_total_grossup(i) = 1 then
					prev_total_grossup_ytd_calc := 0;
					prev_entity_grossup_ytd_calc := 0;
				  else
					prev_total_grossup_ytd_calc := round((round(a_prev_ytd_m_activity(i) * a_prev_rate_increment(i) - a_prev_ytd_dt_activity(i),8)) * (1/(1-a_prev_rate_total_grossup(i)) - 1),8);
					prev_total_increment_ytd_calc  := round(a_prev_ytd_m_activity(i) * a_prev_rate_total_increment(i) - a_prev_ytd_total_dt_activity(i),8);
					prev_entity_grossup_ytd_calc := round(prev_total_increment_ytd_calc * (1/(1-a_prev_rate_total_grossup(i))) * a_prev_rate_entity_grossup(i),8);
				  end if;
			   else -- reg_ind = 0
				  prev_total_grossup_ytd_calc := 0;
				  prev_entity_grossup_ytd_calc := 0;
			   end if; -- reg_ind = 1

			   a_total_grossup_end(i) := a_total_grossup_end(i) - total_grossup_ytd_calc + prev_total_grossup_ytd_calc;
			   a_entity_grossup_end(i) := a_entity_grossup_end(i) - entity_grossup_ytd_calc + prev_entity_grossup_ytd_calc;
			end if;

            -- If this is a non-current-year month, and we're not truing up beginning deferred tax balances, then just calculate the current month activity
            if a_calc_type(i) = 0 and a_process_option_id(i) = 1 then
               a_fas109_liab_end(i) := fas109_liab_cm_calc + fas109_liab_roll;
               a_fas109_increment_end(i) := fas109_increment_cm_calc + fas109_increment_roll;
               a_total_grossup_end(i) := total_grossup_cm_calc + total_grossup_roll;
               a_entity_grossup_end(i) := entity_grossup_cm_calc + entity_grossup_roll;
            end if;

            -- If this is a current year month and we're not trueing up beginning deferred taxes, then current month activity is the year-to-date calc
            --    minus year-to-date booked
            if a_calc_type(i) = 0 and a_process_option_id(i) = 2 then
               --Year-to-date only calc, the calculation is:
               --Year-to-date calculated minus year-to-date booked, plus beginning balance
               a_fas109_liab_end(i) := fas109_liab_ytd_calc - l_fas109_liab_ytd + fas109_liab_roll;
               a_fas109_increment_end(i) := fas109_increment_ytd_calc - l_fas109_increment_ytd + fas109_increment_roll;
               a_total_grossup_end(i) := total_grossup_ytd_calc - l_total_grossup_ytd + total_grossup_roll;
               a_entity_grossup_end(i) := entity_grossup_ytd_calc - l_entity_grossup_ytd + entity_grossup_roll;
            end if;

         end if; --do_not_update(i) = 1

         -- Roll the prior row's ending balance to the current row's beginning balance
         a_accum_diff_beg(i) := accum_diff_roll;
         a_accum_reg_dit_beg(i) := accum_reg_dit_roll;
         a_fas109_liab_beg(i) := fas109_liab_roll;
         a_fas109_increment_beg(i) := fas109_increment_roll;
         a_total_grossup_beg(i) := total_grossup_roll;
         a_entity_grossup_beg(i) := entity_grossup_roll;

         -- Populate the two current-month activity columns.  I don't know why these columns exist, but I'm scared that the system
         --    might explode if I don't populate them.
         a_fas109_liab_current(i) := a_fas109_liab_end(i) - a_fas109_liab_beg(i);
         a_entity_grossup_current(i) := a_entity_grossup_end(i) - a_entity_grossup_beg(i);

         -- Save the current month ending balance to roll to the beginning balance for the next row
         accum_diff_roll := a_accum_diff_end(i);
         accum_reg_dit_roll := a_accum_reg_dit_end(i);
         fas109_liab_roll := a_fas109_liab_end(i);
         fas109_increment_roll := a_fas109_increment_end(i);
         total_grossup_roll := a_total_grossup_end(i);
         entity_grossup_roll := a_entity_grossup_end(i);

         -- Roll current month amounts into the YTD amounts if this is a month that is included in year-to-date true-up calculations
         if a_ytd_include(i) = 0 then
            l_fas109_liab_ytd       := l_fas109_liab_ytd + a_fas109_liab_end(i) - a_fas109_liab_beg(i);
            l_fas109_increment_ytd  := l_fas109_increment_ytd + a_fas109_increment_end(i) - a_fas109_increment_beg(i);
            l_total_grossup_ytd     := l_total_grossup_ytd + a_total_grossup_end(i) - a_total_grossup_beg(i);
            l_entity_grossup_ytd    := l_entity_grossup_ytd + a_entity_grossup_end(i) - a_entity_grossup_beg(i);
         end if;

         -- Only update rows that have changed - this will save processing time
         if a_update = 1 and
            (  accum_diff_beg_orig <> a_accum_diff_beg(i) or accum_diff_end_orig <> a_accum_diff_end(i) or
               accum_reg_dit_beg_orig <> a_accum_reg_dit_beg(i) or accum_reg_dit_end_orig <> a_accum_reg_dit_end(i) or
               fas109_liab_beg_orig <> a_fas109_liab_beg(i) or fas109_liab_end_orig <> a_fas109_liab_end(i) or
               fas109_increment_beg_orig <> a_fas109_increment_beg(i) or fas109_increment_end_orig <> a_fas109_increment_end(i) or
               total_grossup_beg_orig <> a_total_grossup_beg(i) or total_grossup_end_orig <> a_total_grossup_end(i) or
               entity_grossup_beg_orig <> a_entity_grossup_beg(i) or entity_grossup_end_orig <> a_entity_grossup_end(i) or
               fas109_liab_current_orig <> a_fas109_liab_current(i) or  entity_grossup_current_orig <> a_entity_grossup_current(i) or
               a_do_not_update(i) = 1
            )
         then
            j := j + 1;
            m_id_update(j)                   := a_m_id(i);
            ta_norm_id_update(j)             := a_ta_norm_id(i);
            gl_month_update(j)               := a_gl_month(i);
            accum_diff_beg_update(j)         := a_accum_diff_beg(i);
            accum_diff_end_update(j)         := a_accum_diff_end(i);
            accum_reg_dit_beg_update(j)      := a_accum_reg_dit_beg(i);
            accum_reg_dit_end_update(j)      := a_accum_reg_dit_end(i);
            fas109_liab_beg_update(j)        := a_fas109_liab_beg(i);
            fas109_liab_end_update(j)        := a_fas109_liab_end(i);
            fas109_increment_beg_update(j)   := a_fas109_increment_beg(i);
            fas109_increment_end_update(j)   := a_fas109_increment_end(i);
            total_grossup_beg_update(j)      := a_total_grossup_beg(i);
            total_grossup_end_update(j)      := a_total_grossup_end(i);
            entity_grossup_beg_update(j)     := a_entity_grossup_beg(i);
            entity_grossup_end_update(j)     := a_entity_grossup_end(i);
            fas109_liab_current_update(j)    := a_fas109_liab_current(i);
            entity_grossup_current_update(j) := a_entity_grossup_current(i);
         end if;


      end loop; --i in 1 .. num_rows loop

      -- Update tax_accrual_fas109 if a_update = 1 (will not equal 1 if we're just calculating on the screen).
      if a_update = 1 then
         tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'Begin FAS109 Update');
         rtn := f_fas109_update  (  m_id_update, ta_norm_id_update, gl_month_update, accum_diff_beg_update,accum_diff_end_update,accum_reg_dit_beg_update,
                                    accum_reg_dit_end_update,fas109_liab_beg_update,fas109_liab_end_update,fas109_increment_beg_update,
                                    fas109_increment_end_update,total_grossup_beg_update,total_grossup_end_update,entity_grossup_beg_update,
                                    entity_grossup_end_update,fas109_liab_current_update,entity_grossup_current_update,msg
                                 );
         tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'End FAS109 Update');
         if rtn < 0 then
            raise err_fas109_update;
         end if;
      end if; -- a_update = 1

      a_msg := 'SUCCESS';
      return 1;

   EXCEPTION
      when err_fas109_update then
         a_msg := 'Updating FAS109 table: ' || msg;
         return -1;
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;

   END f_fas109_calc;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_fas109_calc (Overloaded function - second version)                                                     --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_fas109_calc  (  a_fas109_sqls in varchar2,
                              a_gl_month in tax_accrual_gl_month_def.gl_month%TYPE,
                              a_trueup_insert_sqls in varchar2,
                              a_trueup_update_sqls in varchar2,
                              a_msg OUT varchar2
                           ) RETURN NUMBER
   is
      msg                     varchar2(2000);
      sql_msg                 varchar2(254);
      rtn                     number(1);
      m_id                    id_t;
      ta_norm_id              id_t;
      gl_month                gl_month_t;
      accum_diff_beg          currency_t;
      accum_diff_end          currency_t;
      accum_reg_dit_beg       currency_t;
      accum_reg_dit_end       currency_t;
      fas109_liab_beg         currency_t;
      fas109_liab_end         currency_t;
      fas109_increment_beg    currency_t;
      fas109_increment_end    currency_t;
      total_grossup_beg       currency_t;
      total_grossup_end       currency_t;
      entity_grossup_beg      currency_t;
      entity_grossup_end      currency_t;
      fas109_liab_current     currency_t;
      entity_grossup_current  currency_t;
      reg_ind                 id_t;
      rate_increment          rate_t;
      rate_entity_grossup     rate_t;
      rate_total_grossup      rate_t;
      rate_total_increment    rate_t;
      m_beg_bal               currency_t;
      m_end_bal               currency_t;
      m_activity              currency_t;
      dt_beg_bal              currency_t;
      dt_end_bal              currency_t;
      dt_activity             currency_t;
      first_month_ind         indicator_t;
      ytd_include             indicator_t;
      do_not_update           indicator_t;
      process_option_id       id_t;
      calc_type               indicator_t;
      ytd_m_activity          currency_t;
      total_dt_activity       currency_t;
      ytd_total_dt_activity   currency_t;
      total_dt_beg_bal        currency_t;
      total_dt_end_bal        currency_t;
      ytd_dt_activity         currency_t;
      accum_diff_ytd          currency_t;
      accum_reg_dit_ytd       currency_t;
      fas109_liab_ytd         currency_t;
      fas109_increment_ytd    currency_t;
      total_grossup_ytd       currency_t;
      entity_grossup_ytd      currency_t;
      function_name           varchar2(254) := 'f_fas109_calc(2)';
      prev_ytd_m_activity     currency_t;
      prev_rate_increment     rate_t;
      prev_ytd_dt_activity    currency_t;
      prev_rate_entity_grossup rate_t;
      prev_rate_total_grossup  rate_t;
      prev_rate_total_increment rate_t;
      prev_ytd_total_dt_activity currency_t;
      err_fas109_calc         exception;

      cursor fas109_tmp_cur is
      select   m_id,
               ta_norm_id,
               gl_month,
               accum_diff_beg,
               accum_diff_end,
               accum_reg_dit_beg,
               accum_reg_dit_end,
               fas109_liab_beg,
               fas109_liab_end,
               fas109_increment_beg,
               fas109_increment_end,
               total_grossup_beg,
               total_grossup_end,
               entity_grossup_beg,
               entity_grossup_end,
               fas109_liab_current,
               entity_grossup_current,
               m_beg_bal,
               m_end_bal,
               m_activity,
               dt_beg_bal,
               dt_end_bal,
               dt_activity,
               reg_ind,
               rate_increment,
               rate_entity_grossup,
               rate_total_grossup,
               rate_total_increment,
               first_month_ind,
               ytd_include,
               do_not_update,
               process_option_id,
               calc_type,
               ytd_m_activity,
               total_dt_activity,
               ytd_total_dt_activity,
               total_dt_beg_bal,
               total_dt_end_bal,
               ytd_dt_activity,
               fas109_liab_ytd,
               fas109_increment_ytd,
               total_grossup_ytd,
               entity_grossup_ytd,
               prev_ytd_m_activity,
               prev_rate_increment,
               prev_ytd_dt_activity,
               prev_rate_entity_grossup,
               prev_rate_total_grossup,
               prev_rate_total_increment,
               prev_ytd_total_dt_activity
from
      (
        			    select  m_id,
					    ta_norm_id,
					    gl_month,
					    accum_diff_beg,
					    accum_diff_end,
					    accum_reg_dit_beg,
					    accum_reg_dit_end,
					    fas109_liab_beg,
					    fas109_liab_end,
					    fas109_increment_beg,
					    fas109_increment_end,
					    total_grossup_beg,
					    total_grossup_end,
					    entity_grossup_beg,
					    entity_grossup_end,
					    fas109_liab_current,
					    entity_grossup_current,
					    m_beg_bal,
					    m_end_bal,
					    m_activity,
					    dt_beg_bal,
					    dt_end_bal,
					    dt_activity,
					    reg_ind,
					    rate_increment,
					    rate_entity_grossup,
					    rate_total_grossup,
					    rate_total_increment,
					    first_month_ind,
					    ytd_include,
					    do_not_update,
					    process_option_id,
					    calc_type,
					    ytd_m_activity,
					    total_dt_activity,
					    ytd_total_dt_activity,
					    total_dt_beg_bal,
					    total_dt_end_bal,
					    ytd_dt_activity,
					    fas109_liab_ytd,
					    fas109_increment_ytd,
					    total_grossup_ytd,
					    entity_grossup_ytd,
          				    nvl(lag(ytd_m_activity,1,0) over (partition by m_id, ta_norm_id order by m_id, ta_norm_id, gl_month),0) prev_ytd_m_activity,
           				    nvl(lag(rate_increment,1,0) over (partition by m_id, ta_norm_id order by m_id, ta_norm_id, gl_month),rate_increment) prev_rate_increment,
             				    nvl(lag(ytd_dt_activity,1,0) over (partition by m_id, ta_norm_id order by m_id, ta_norm_id, gl_month),0) prev_ytd_dt_activity,
         			            nvl(lag(total_grossup_ytd,1,0) over (partition by m_id, ta_norm_id order by m_id, ta_norm_id, gl_month),0) prev_total_grossup_ytd,
              				    nvl(lag(entity_grossup_ytd,1,0) over (partition by m_id, ta_norm_id order by m_id, ta_norm_id, gl_month),0) prev_entity_grossup_ytd,
           				    nvl(lag(rate_entity_grossup,1,0) over (partition by m_id, ta_norm_id order by m_id, ta_norm_id, gl_month),rate_entity_grossup) prev_rate_entity_grossup,
              				    nvl(lag(rate_total_grossup,1,0) over (partition by m_id, ta_norm_id order by m_id, ta_norm_id, gl_month),rate_total_grossup) prev_rate_total_grossup,
            				    nvl(lag(rate_total_increment,1,0) over (partition by m_id, ta_norm_id order by m_id, ta_norm_id, gl_month),rate_total_increment) prev_rate_total_increment,
           				    nvl(lag(ytd_total_dt_activity,1,0) over (partition by m_id, ta_norm_id order by m_id, ta_norm_id, gl_month), 0) prev_ytd_total_dt_activity
		    from tax_accrual_fas109_proc_tmp
      )
		where gl_month >= a_gl_month
		order by m_id, ta_norm_id, gl_month;
   begin

      rtn := 1;
      -- The statement that inserts into the temporary table is built in PowerPlant and sent to this function
      sql_msg := 'Inserting into FAS109 Calc Temporary Table';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);
      execute immediate a_fas109_sqls;

      sql_msg := 'Inserting into RTP temporary table for FAS109 amounts';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);
      execute immediate a_trueup_insert_sqls;

      sql_msg := 'Updating FAS109 table with RTP amounts';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);
      execute immediate a_trueup_update_sqls;

      sql_msg := 'Retrieving FAS109 Data for calculation';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);
      open fas109_tmp_cur;

      fetch fas109_tmp_cur
      bulk collect into m_id,
                        ta_norm_id,
                        gl_month,
                        accum_diff_beg,
                        accum_diff_end,
                        accum_reg_dit_beg,
                        accum_reg_dit_end,
                        fas109_liab_beg,
                        fas109_liab_end,
                        fas109_increment_beg,
                        fas109_increment_end,
                        total_grossup_beg,
                        total_grossup_end,
                        entity_grossup_beg,
                        entity_grossup_end,
                        fas109_liab_current,
                        entity_grossup_current,
                        m_beg_bal,
                        m_end_bal,
                        m_activity,
                        dt_beg_bal,
                        dt_end_bal,
                        dt_activity,
                        reg_ind,
                        rate_increment,
                        rate_entity_grossup,
                        rate_total_grossup,
                        rate_total_increment,
                        first_month_ind,
                        ytd_include,
                        do_not_update,
                        process_option_id,
                        calc_type,
                        ytd_m_activity,
                        total_dt_activity,
                        ytd_total_dt_activity,
                        total_dt_beg_bal,
                        total_dt_end_bal,
                        ytd_dt_activity,
                        fas109_liab_ytd,
                        fas109_increment_ytd,
                        total_grossup_ytd,
                        entity_grossup_ytd,
                        prev_ytd_m_activity,
                        prev_rate_increment,
                        prev_ytd_dt_activity,
                        prev_rate_entity_grossup,
                        prev_rate_total_grossup,
                        prev_rate_total_increment,
                        prev_ytd_total_dt_activity;

      close fas109_tmp_cur;

		tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'Calculate FAS109 Deferred Taxes');
		rtn := f_fas109_calc	(	0, -- a_calc_beg_balance
						1, -- a_update
						m_id,
                			        ta_norm_id,
                    				gl_month,
                   			        accum_diff_beg,
                    				accum_diff_end,
                    				accum_reg_dit_beg,
                    				accum_reg_dit_end,
                    				fas109_liab_beg,
						fas109_liab_end,
                    				fas109_increment_beg,
                    				fas109_increment_end,
                    				total_grossup_beg,
                    				total_grossup_end,
                    				entity_grossup_beg,
						entity_grossup_end,
                    				fas109_liab_current,
                    				entity_grossup_current,
                    				reg_ind,
                    				rate_increment,
                    				rate_entity_grossup,
						rate_total_grossup,
                    				rate_total_increment,
                    				m_beg_bal,
                    				m_end_bal,
                    				m_activity,
                    				dt_beg_bal,
                    				dt_end_bal,
                    				dt_activity,
						first_month_ind,
                    				ytd_include,
                    				do_not_update,
                    				process_option_id,
                    				calc_type,
                    				ytd_m_activity,
                    				total_dt_activity,
						ytd_total_dt_activity,
                    				total_dt_beg_bal,
                    				total_dt_end_bal,
                    				ytd_dt_activity,
                    				fas109_liab_ytd,
                    				fas109_increment_ytd,
						total_grossup_ytd,
                    				entity_grossup_ytd,
                    				prev_ytd_m_activity, 
                    				prev_rate_increment, 
                    				prev_ytd_dt_activity,
                   				prev_rate_entity_grossup,
                    				prev_rate_total_grossup,
                    				prev_rate_total_increment,
                    				prev_ytd_total_dt_activity,
                    				msg
									);
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'End Calculate FAS109 Deferred Taxes');
      if rtn < 0 then
         raise err_fas109_calc;
      end if;

      return rtn;

   EXCEPTION
      when err_fas109_calc then
         a_msg := 'Calculating FAS109 Deferred Tax: ' || msg;
         return -1;
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;

   end f_fas109_calc;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_fas109_roll                                                                                            --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_fas109_roll  (  a_gl_month     in tax_accrual_gl_month_def.gl_month%TYPE,
                              a_msg          OUT varchar2
                           ) RETURN NUMBER

   IS
      sql_msg                       varchar2(254);
      msg                           varchar2(2000);
      rtn                           number(22);
      m_id                          id_t;
      ta_norm_id                    id_t;
      gl_month                      gl_month_t;
      accum_diff_beg                currency_t;
      accum_diff_end                currency_t;
      accum_reg_dit_beg             currency_t;
      accum_reg_dit_end             currency_t;
      fas109_liab_beg               currency_t;
      fas109_liab_end               currency_t;
      fas109_increment_beg          currency_t;
      fas109_increment_end          currency_t;
      total_grossup_beg             currency_t;
      total_grossup_end             currency_t;
      entity_grossup_beg            currency_t;
      entity_grossup_end            currency_t;
      fas109_liab_current           currency_t;
      entity_grossup_current        currency_t;
      prev_m_id                     tax_accrual_fas109.m_id%TYPE := 0;
      prev_ta_norm_id               tax_accrual_fas109.ta_norm_id%TYPE := 0;
      accum_diff_roll               tax_accrual_fas109.accum_diff_end%TYPE := 0;
      accum_reg_dit_roll            tax_accrual_fas109.accum_reg_dit_end%TYPE := 0;
      fas109_liab_roll              tax_accrual_fas109.fas109_liab_end%TYPE := 0;
      fas109_increment_roll         tax_accrual_fas109.fas109_increment_end%TYPE := 0;
      total_grossup_roll            tax_accrual_fas109.total_grossup_end%TYPE := 0;
      entity_grossup_roll           tax_accrual_fas109.entity_grossup_end%TYPE := 0;
      num_rows                      number(22);
      num_update_rows               number(22);
      i                             number(22) := 0;
      j                             number(22) := 0;
      m_id_update                   id_t;
      ta_norm_id_update             id_t;
      gl_month_update               gl_month_t;
      accum_diff_beg_update         currency_t;
      accum_diff_end_update         currency_t;
      accum_reg_dit_beg_update      currency_t;
      accum_reg_dit_end_update      currency_t;
      fas109_liab_beg_update        currency_t;
      fas109_liab_end_update        currency_t;
      fas109_increment_beg_update   currency_t;
      fas109_increment_end_update   currency_t;
      total_grossup_beg_update      currency_t;
      total_grossup_end_update      currency_t;
      entity_grossup_beg_update     currency_t;
      entity_grossup_end_update     currency_t;
      fas109_liab_current_update    currency_t;
      entity_grossup_current_update currency_t;
      accum_diff_beg_orig           currency_type;
      accum_diff_end_orig           currency_type;
      accum_reg_dit_beg_orig        currency_type;
      accum_reg_dit_end_orig        currency_type;
      fas109_liab_beg_orig          currency_type;
      fas109_liab_end_orig          currency_type;
      fas109_increment_beg_orig     currency_type;
      fas109_increment_end_orig     currency_type;
      total_grossup_beg_orig        currency_type;
      total_grossup_end_orig        currency_type;
      entity_grossup_beg_orig       currency_type;
      entity_grossup_end_orig       currency_type;
      fas109_liab_current_orig      currency_type;
      entity_grossup_current_orig   currency_type;

      err_fas109_update exception;

      cursor fas109_tmp_cur is
      select   m_id,
               ta_norm_id,
               gl_month,
               accum_diff_beg,
               accum_diff_end,
               accum_reg_dit_beg,
               accum_reg_dit_end,
               fas109_liab_beg,
               fas109_liab_end,
               fas109_increment_beg,
               fas109_increment_end,
               total_grossup_beg,
               total_grossup_end,
               entity_grossup_beg,
               entity_grossup_end,
               fas109_liab_current,
               entity_grossup_current
      from tax_accrual_fas109
      where m_id in  (  select id
                        from tax_accrual_rep_criteria_tmp
                        where criteria_type = 'FAS109_M_ID'
                     )
         and gl_month >= a_gl_month
      order by m_id, ta_norm_id, gl_month;

   BEGIN

      open fas109_tmp_cur;

      fetch fas109_tmp_cur
      bulk collect into m_id,
                        ta_norm_id,
                        gl_month,
                        accum_diff_beg,
                        accum_diff_end,
                        accum_reg_dit_beg,
                        accum_reg_dit_end,
                        fas109_liab_beg,
                        fas109_liab_end,
                        fas109_increment_beg,
                        fas109_increment_end,
                        total_grossup_beg,
                        total_grossup_end,
                        entity_grossup_beg,
                        entity_grossup_end,
                        fas109_liab_current,
                        entity_grossup_current;

      close fas109_tmp_cur;

      num_rows := m_id.COUNT;

      for i in 1 .. num_rows loop

         --Save the original values so that we can confirm whether or not we actually need an update
         accum_diff_beg_orig := accum_diff_beg(i);
         accum_diff_end_orig := accum_diff_end(i);
         accum_reg_dit_beg_orig := accum_reg_dit_beg(i);
         accum_reg_dit_end_orig := accum_reg_dit_end(i);
         fas109_liab_beg_orig := fas109_liab_beg(i);
         fas109_liab_end_orig := fas109_liab_end(i);
         fas109_increment_beg_orig := fas109_increment_beg(i);
         fas109_increment_end_orig := fas109_increment_end(i);
         total_grossup_beg_orig := total_grossup_beg(i);
         total_grossup_end_orig := total_grossup_end(i);
         entity_grossup_beg_orig := entity_grossup_beg(i);
         entity_grossup_end_orig := entity_grossup_end(i);
         fas109_liab_current_orig := fas109_liab_current(i);
         entity_grossup_current_orig := entity_grossup_current(i);

         if m_id(i) <> prev_m_id OR ta_norm_id(i) <> prev_ta_norm_id then
            prev_m_id               := m_id(i);
            prev_ta_norm_id         := ta_norm_id(i);

            -- If this is the first row for a given m_id and ta_norm_id, then we're not rolling a balance
            --    from the prior row.
            accum_diff_roll         := accum_diff_beg(i);
            accum_reg_dit_roll      := accum_reg_dit_beg(i);
            fas109_liab_roll        := fas109_liab_beg(i);
            fas109_increment_roll   := fas109_increment_beg(i);
            total_grossup_roll      := total_grossup_beg(i);
            entity_grossup_roll     := entity_grossup_beg(i);
         end if;

         accum_diff_end(i) := accum_diff_roll + accum_diff_end(i) - accum_diff_beg(i);
         accum_reg_dit_end(i) := accum_reg_dit_roll + accum_reg_dit_end(i) - accum_reg_dit_beg(i);
         fas109_liab_end(i) := fas109_liab_roll + fas109_liab_end(i) - fas109_liab_beg(i);
         fas109_increment_end(i) := fas109_increment_roll + fas109_increment_end(i) - fas109_increment_beg(i);
         total_grossup_end(i) := total_grossup_roll + total_grossup_end(i) - total_grossup_beg(i);
         entity_grossup_end(i) := entity_grossup_roll + entity_grossup_end(i) - entity_grossup_beg(i);
         fas109_liab_current(i) := fas109_liab_end(i) - fas109_liab_roll;
         entity_grossup_current(i) := entity_grossup_end(i) - entity_grossup_roll;

         accum_diff_beg(i) := accum_diff_roll;
         accum_reg_dit_beg(i) := accum_reg_dit_roll;
         fas109_liab_beg(i) := fas109_liab_roll;
         fas109_increment_beg(i) := fas109_increment_roll;
         total_grossup_beg(i) := total_grossup_roll;
         entity_grossup_beg(i) := entity_grossup_roll;

         accum_diff_roll         := accum_diff_end(i);
         accum_reg_dit_roll      := accum_reg_dit_end(i);
         fas109_liab_roll        := fas109_liab_end(i);
         fas109_increment_roll   := fas109_increment_end(i);
         total_grossup_roll      := total_grossup_end(i);
         entity_grossup_roll     := entity_grossup_end(i);

         if accum_diff_beg_orig <> accum_diff_beg(i) or accum_diff_end_orig <> accum_diff_end(i) or
            accum_reg_dit_beg_orig <> accum_reg_dit_beg(i) or accum_reg_dit_end_orig <> accum_reg_dit_end(i) or
            fas109_liab_beg_orig <> fas109_liab_beg(i) or fas109_liab_end_orig <> fas109_liab_end(i) or
            fas109_increment_beg_orig <> fas109_increment_beg(i) or fas109_increment_end_orig <> fas109_increment_end(i) or
            total_grossup_beg_orig <> total_grossup_beg(i) or total_grossup_end_orig <> total_grossup_end(i) or
            entity_grossup_beg_orig <> entity_grossup_beg(i) or entity_grossup_end_orig <> entity_grossup_end(i) or
            fas109_liab_current_orig <> fas109_liab_current(i) or entity_grossup_current_orig <> entity_grossup_current(i)
         then
            j := j + 1;
            m_id_update(j)                   := m_id(i);
            ta_norm_id_update(j)             := ta_norm_id(i);
            gl_month_update(j)               := gl_month(i);
            accum_diff_beg_update(j)         := accum_diff_beg(i);
            accum_diff_end_update(j)         := accum_diff_end(i);
            accum_reg_dit_beg_update(j)      := accum_reg_dit_beg(i);
            accum_reg_dit_end_update(j)      := accum_reg_dit_end(i);
            fas109_liab_beg_update(j)        := fas109_liab_beg(i);
            fas109_liab_end_update(j)        := fas109_liab_end(i);
            fas109_increment_beg_update(j)   := fas109_increment_beg(i);
            fas109_increment_end_update(j)   := fas109_increment_end(i);
            total_grossup_beg_update(j)      := total_grossup_beg(i);
            total_grossup_end_update(j)      := total_grossup_end(i);
            entity_grossup_beg_update(j)     := entity_grossup_beg(i);
            entity_grossup_end_update(j)     := entity_grossup_end(i);
            fas109_liab_current_update(j)    := fas109_liab_current(i);
            entity_grossup_current_update(j) := entity_grossup_current(i);
         end if;

      end loop; --i in 1 .. num_rows loop

      rtn := f_fas109_update  (  m_id_update, ta_norm_id_update, gl_month_update, accum_diff_beg_update,accum_diff_end_update,accum_reg_dit_beg_update,
                                 accum_reg_dit_end_update,fas109_liab_beg_update,fas109_liab_end_update,fas109_increment_beg_update,
                                 fas109_increment_end_update,total_grossup_beg_update,total_grossup_end_update,entity_grossup_beg_update,
                                 entity_grossup_end_update,fas109_liab_current_update,entity_grossup_current_update,msg
                              );

      if rtn < 0 then
         raise err_fas109_update;
      end if;

      a_msg := 'SUCCESS';
      return 1;

   EXCEPTION
      when err_fas109_update then
         a_msg := 'Updating FAS109 table: ' || msg;
         return -1;
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;

      return 1;

   END;


   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_deftax_update                                                                                          --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_deftax_update(  a_m_id            in id_t,
                              a_ta_norm_id      in id_t,
                              a_gl_month        in gl_month_t,
                              a_beg_balance     in currency_t,
                              a_end_balance     in currency_t,
                              a_amount_est      in currency_t,
                              a_amount_act      in currency_t,
                              a_amount_calc     in currency_t,
                              a_amount_manual   in currency_t,
                              a_msg          OUT varchar2
                           ) RETURN NUMBER
   is
      num_rows number(22);
      sql_msg varchar2(254);
   begin
      num_rows := a_m_id.COUNT;
      sql_msg := 'Updating DefTax Table with new amounts';
      FORALL indx IN 1 .. num_rows
         update tax_accrual_def_tax
         set   beg_balance = a_beg_balance(indx),
               end_balance = a_end_balance(indx),
               amount_act = a_amount_act(indx),
               amount_calc = a_amount_calc(indx),
               amount_manual = a_amount_manual(indx)
         where m_id = a_m_id(indx)
            and ta_norm_id = a_ta_norm_id(indx)
            and gl_month = a_gl_month(indx);

      a_msg := 'SUCCESS';
      return 1;
   EXCEPTION
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;

   end f_deftax_update;


   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_deftax_calc (Overloaded version 1)                                                                     --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_deftax_calc  (  a_update                in number,
                              a_m_id                  in out id_t,
                              a_ta_norm_id            in out id_t,
                              a_gl_month              in out gl_month_t,
                              a_beg_balance           in out currency_t,
                              a_end_balance           in out currency_t,
                              a_amount_est            in out currency_t,
                              a_amount_act            in out currency_t,
                              a_amount_calc           in out currency_t,
                              a_amount_manual         in out currency_t,
                              a_m_beg_bal             in currency_t,
                              a_m_activity            in currency_t,
                              a_rate                  in rate_t,
                              a_first_month_ind       in indicator_t,
                              a_ytd_include           in indicator_t,
                              a_do_not_update         in indicator_t,
                              a_process_option_id     in id_t,
                              a_ytd_m_activity        in currency_t,
                              a_ytd_m_activity_prov   in currency_t,
                              a_ytd_dt_activity       in currency_t,
                              a_ytd_dt_activity_prov  in currency_t,
                              a_aram_ind              in id_t,
                              a_msg                   out varchar2
                           ) RETURN NUMBER


   IS
      sql_msg                       varchar2(254);
      prev_m_id                     tax_accrual_def_tax.m_id%TYPE := 0;
      prev_ta_norm_id               tax_accrual_def_tax.ta_norm_id%TYPE := 0;
      balance_roll                  tax_accrual_def_tax.beg_balance%TYPE := 0;
      ytd_amount_roll               tax_accrual_def_tax.amount_act%TYPE := 0;
      rate_to_use                   rate_type := 0;
      num_rows                      number(22);
      num_update_rows               number(22);
      i                             number(22) := 0;
      j                             number(22) := 0;
      rtn                           number(22);
      msg                           varchar2(2000);
      rate                          rate_t;
      m_id_update                   id_t;
      ta_norm_id_update             id_t;
      gl_month_update               gl_month_t;
      beg_balance_update            currency_t;
      end_balance_update            currency_t;
      amount_est_update             currency_t;
      amount_act_update             currency_t;
      amount_calc_update            currency_t;
      amount_manual_update          currency_t;
      beg_balance_orig              currency_type;
      end_balance_orig              currency_type;
      amount_est_orig               currency_type;
      amount_act_orig               currency_type;
      amount_calc_orig              currency_type;
      amount_manual_orig            currency_type;
      ytd_m_activity                currency_type;
      use_aram                      boolean;
      function_name                 varchar2(30) := 'f_deftax_calc(1)';

      err_deftax_update             exception;

   begin


      --FINISHED COMMENTING TO HERE
      num_rows := a_m_id.COUNT;

      -- Now do the calculation and roll balances
      for i in 1 .. num_rows loop

         -- Save the original values so that we can confirm whether or not we actually need an update
         --    This should improve performance by reducing the number of rows to insert.
         beg_balance_orig := a_beg_balance(i);
         end_balance_orig := a_end_balance(i);
         amount_est_orig := a_amount_est(i);
         amount_calc_orig := a_amount_calc(i);
         amount_manual_orig := a_amount_manual(i);
         amount_act_orig := a_amount_act(i);

         if a_m_id(i) <> prev_m_id OR a_ta_norm_id(i) <> prev_ta_norm_id then
            prev_m_id               := a_m_id(i);
            prev_ta_norm_id         := a_ta_norm_id(i);
            -- If this is the first row for a given m_id and ta_norm_id, then we're not rolling a balance
            --    from the prior row.
            balance_roll := a_beg_balance(i);
            if a_aram_ind(i) = 1 then
               ytd_amount_roll := a_ytd_dt_activity_prov(i);
            else
               ytd_amount_roll := a_ytd_dt_activity(i);
            end if;

         end if;
         -- If we're not re-calculating (like if this month is pulled from another case for instance)
         -- Then just set the end balance = beg_balance + current difference between end and beg balance
         use_aram := FALSE;
         if a_do_not_update(i) = 1 then
            a_end_balance(i) := a_amount_act(i) + balance_roll;
         else --do_not_update(i) = 0
            ytd_m_activity := a_ytd_m_activity(i);
            if a_aram_ind(i) = 1 then -- 1 signifies reversing using aram, 0 signifies using current rate

               -- If the aram option is set to 1, then we only want to use the non-reversing M activity
               -- in our calculations.
               ytd_m_activity := a_ytd_m_activity_prov(i);

               if sign(a_m_beg_bal(i) * a_m_activity(i)) = -1 then
                  use_aram := TRUE;
               end if; --aram_ind(i) = 1

               if a_m_beg_bal(i) = 0 then
                  rate_to_use := 0;
               else
                  rate_to_use := round(balance_roll/a_m_beg_bal(i),8);
               end if; -- m_beg_bal(i) = 0

            end if; --aram_ind(i) = 1

            if not use_aram then
               rate_to_use := a_rate(i);
            end if;

            if a_ytd_include(i) = 1 or use_aram then
               -- 1 = For reversals at an ARAM rate, we only pay attention to the current month
               a_amount_calc(i) := round(rate_to_use *  a_m_activity(i),2);
            else
               -- 2 = Deftax trueup = calc is on M Item activity for the current year (for provisions only if rate type is ARAM)
               a_amount_calc(i) := round(rate_to_use * ytd_m_activity,2) - ytd_amount_roll;
            end if;

         end if; --do_not_update(i) = 1

         a_amount_act(i) := a_amount_manual(i) + a_amount_calc(i);
         a_beg_balance(i) := balance_roll;
         a_end_balance(i) := balance_roll + a_amount_act(i);
         balance_roll := a_end_balance(i);

         -- The YTD deferred tax is rolled forward so that we can subtract it from the next month(s) to determine
         -- the current month amount required to get to the correct year-to-date amount.  Adjustment months
         -- and activity reversing through ARAM should not be included in this number.
         if a_ytd_include(i) = 0 and not use_aram then
            ytd_amount_roll := ytd_amount_roll + a_amount_calc(i);
         end if;

         if a_update = 1 and
            (  beg_balance_orig <> a_beg_balance(i) or end_balance_orig <> a_end_balance(i) or
               amount_est_orig <> a_amount_est(i) or amount_act_orig <> a_amount_act(i) or
               amount_calc_orig <> a_amount_calc(i) or amount_manual_orig <> a_amount_manual(i) or
               a_do_not_update(i) = 1
            )
         then
            j := j + 1;
            m_id_update(j)          := a_m_id(i);
            ta_norm_id_update(j)    := a_ta_norm_id(i);
            gl_month_update(j)      := a_gl_month(i);
            beg_balance_update(j)   := a_beg_balance(i);
            end_balance_update(j)   := a_end_balance(i);
            amount_est_update(j)    := a_amount_est(i);
            amount_act_update(j)    := a_amount_act(i);
            amount_calc_update(j)   := a_amount_calc(i);
            amount_manual_update(j) := a_amount_manual(i);
         end if;

      end loop; -- i in 1 .. num_rows loop

      if a_update = 1 then
         tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'Begin DefTax Update');
         rtn := f_deftax_update  (  m_id_update,ta_norm_id_update,gl_month_update,beg_balance_update,end_balance_update,
                                    amount_est_update,amount_act_update,amount_calc_update,amount_manual_update,msg
                                 );

         if rtn < 0 then
            raise err_deftax_update;
         end if;
         tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'End DefTax Update');
      end if; -- a_update = 1


      a_msg := 'SUCCESS';
      return 1;

   EXCEPTION
      when err_deftax_update then
         a_msg := 'Updating Deferred Tax Table: ' || msg;
         return -1;
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;
   END f_deftax_calc;

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_def_tax_calc (Overloaded function - second version)                                                    --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_deftax_calc  (  a_deftax_sqls in varchar2,
                              a_gl_month in tax_accrual_gl_month_def.gl_month%TYPE,
                              a_trueup_insert_sqls in varchar2,
                              a_trueup_update_sqls in varchar2,
                              a_msg OUT varchar2
                           ) RETURN NUMBER
   is
      msg                     varchar2(2000);
      sql_msg                 varchar2(254);
      rtn                     number(1);
      m_id                    id_t;
      ta_norm_id              id_t;
      gl_month                gl_month_t;
      beg_balance             currency_t;
      end_balance             currency_t;
      amount_est              currency_t;
      amount_act              currency_t;
      amount_calc             currency_t;
      amount_manual           currency_t;
      m_beg_bal               currency_t;
      m_activity              currency_t;
      rate                    rate_t;
      first_month_ind         indicator_t;
      ytd_include             indicator_t;
      trueup_includes_input   indicator_t; --###PATCH(8048)
      do_not_update           indicator_t;
      trueup_option           indicator_t;
      activity_type_id        id_t;
      process_option_id       id_t;
      ytd_m_activity          currency_t;
      ytd_m_activity_prov     currency_t;
      ytd_dt_activity         currency_t;
      ytd_dt_activity_prov    currency_t;
      aram_ind                id_t;
      annual_amount_est       currency_t;
      ytd_est                 currency_t;
      ytd_percent             spread_pct_t; /*###PATCH(513) Changed from percent_t to spread_pct_t*/
      spread_est_ind          indicator_t;
      function_name           varchar2(30) := 'f_deftax_calc(2)';
      num_remaining_trueup_months   long_t;
      spread_remaining_estimate     long_t;

      err_deftax_calc         exception;

      cursor deftax_tmp_cur (a_spread_est_ind in number) is
         select   m_id,
                  ta_norm_id,
                  gl_month,
                  beg_balance,
                  end_balance,
                  amount_est,
                  amount_act,
                  amount_calc,
                  amount_manual,
                  m_beg_bal,
                  m_activity,
                  rate,
                  first_month_ind,
                  ytd_include,
            trueup_includes_input, --###PATCH(8048)
                  do_not_update,
                  process_option_id,
                  ytd_m_activity,
                  ytd_m_activity_prov,
                  ytd_dt_activity,
                  ytd_dt_activity_prov,
                  aram_ind,
                  trueup_option,
                  activity_type_id,
                  annual_amount_est,
                  ytd_est,
                  ytd_percent,
                  num_remaining_trueup_months,
                  spread_remaining_estimate
         from tax_accrual_def_tax_proc_tmp
         where gl_month >= a_gl_month
            and spread_est_ind = a_spread_est_ind
         order by m_id, ta_norm_id, gl_month;
   begin

      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'Begin Function');
      rtn := 1;
      -- The statement that inserts into the temporary table is built in PowerPlant and sent to this function
      sql_msg := 'Inserting into Def_Tax Calc Temporary Table';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);
      execute immediate a_deftax_sqls;

      sql_msg := 'Inserting into RTP temporary table for DefTax amounts';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      execute immediate a_trueup_insert_sqls;

      sql_msg := 'Updating DefTax table with RTP amounts';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      execute immediate a_trueup_update_sqls;

      sql_msg := 'Retrieving Def Tax Data for calculation';
      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, sql_msg);

      -- Retrieve deferred tax records that will be calculated using current rates
      open deftax_tmp_cur(0);

      fetch deftax_tmp_cur
      bulk collect into m_id,
                        ta_norm_id,
                        gl_month,
                        beg_balance,
                        end_balance,
                        amount_est,
                        amount_act,
                        amount_calc,
                        amount_manual,
                        m_beg_bal,
                        m_activity,
                        rate,
                        first_month_ind,
                        ytd_include,
                trueup_includes_input, --###PATCH(8048)
                        do_not_update,
                        process_option_id,
                        ytd_m_activity,
                        ytd_m_activity_prov,
                        ytd_dt_activity,
                        ytd_dt_activity_prov,
                        aram_ind,
                        trueup_option,
                        activity_type_id,
                        annual_amount_est,
                        ytd_est,
                        ytd_percent,
                        num_remaining_trueup_months,
                        spread_remaining_estimate;

      close deftax_tmp_cur;

      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'Calculated Deferred taxes using rates');
      rtn := f_deftax_calc (  1, -- a_update
                              m_id,ta_norm_id,gl_month,beg_balance,end_balance,amount_est,amount_act,amount_calc,
                              amount_manual,m_beg_bal,m_activity,rate,first_month_ind,ytd_include,
                              do_not_update,process_option_id,ytd_m_activity,ytd_m_activity_prov,ytd_dt_activity,
                              ytd_dt_activity_prov,aram_ind,msg
                           );

      -- Retrieve deferred tax records that will be spread based on estimates
      open deftax_tmp_cur(1);

      fetch deftax_tmp_cur
      bulk collect into m_id,
                        ta_norm_id,
                        gl_month,
                        beg_balance,
                        end_balance,
                        amount_est,
                        amount_act,
                        amount_calc,
                        amount_manual,
                        m_beg_bal,
                        m_activity,
                        rate,
                        first_month_ind,
                        ytd_include,
                trueup_includes_input, --###PATCH(8048)
                        do_not_update,
                        process_option_id,
                        ytd_m_activity,
                        ytd_m_activity_prov,
                        ytd_dt_activity,
                        ytd_dt_activity_prov,
                        aram_ind,
                        trueup_option,
                        activity_type_id,
                        annual_amount_est,
                        ytd_est,
                        ytd_percent,
                        num_remaining_trueup_months,
                        spread_remaining_estimate;

      close deftax_tmp_cur;

      tax_accrual_utility_pkg.p_pl_sql_debug(function_name, 'Spread deferred taxes based on annual estimates');
      rtn := tax_accrual_spread_pkg.f_calc_trueup_dt( m_id,
                                                      ta_norm_id,
                                                      gl_month,
                                                      annual_amount_est,
                                                      amount_est,
                                                      ytd_percent,
                                                      ytd_est,
                                                      ytd_dt_activity,
                                                      amount_calc,
                                                      amount_manual,
                                                      amount_act,
                                                      beg_balance,
                                                      end_balance,
                                                      trueup_option,
                                                      do_not_update,
                                                      ytd_include,
                                    trueup_includes_input, --###PATCH(8048)
                                                      num_remaining_trueup_months,
                                                      spread_remaining_estimate,
                                                      1,
                                                      a_msg
                                                   );

      if rtn < 0 then
         raise err_deftax_calc;
      end if;

      return rtn;

   EXCEPTION
      when err_deftax_calc then
         a_msg := 'Calculating APB11 Deferred Tax: ' || msg;
         return -1;
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;

   end f_deftax_calc; -- Overloaded second version

   -- ******************************************************************************************************** --
   --                                                                                                          --
   -- f_deftax_roll                                                                                            --
   --                                                                                                          --
   -- ******************************************************************************************************** --
   FUNCTION f_deftax_roll  (  a_gl_month     in tax_accrual_gl_month_def.gl_month%TYPE,
                              a_msg          OUT varchar2
                           ) RETURN NUMBER

   IS
      sql_msg                       varchar2(254);
      msg                           varchar2(2000);
      rtn                           number(22);
      m_id                          id_t;
      ta_norm_id                    id_t;
      gl_month                      gl_month_t;
      beg_balance                   currency_t;
      end_balance                   currency_t;
      amount_act                    currency_t;
      amount_calc                   currency_t;
      amount_manual                 currency_t;
      amount_est                    currency_t;
      prev_m_id                     tax_accrual_fas109.m_id%TYPE := 0;
      prev_ta_norm_id               tax_accrual_fas109.ta_norm_id%TYPE := 0;
      balance_roll                  tax_accrual_fas109.accum_diff_end%TYPE := 0;
      num_rows                      number(22);
      num_update_rows               number(22);
      i                             number(22) := 0;
      j                             number(22) := 0;
      m_id_update                   id_t;
      ta_norm_id_update             id_t;
      gl_month_update               gl_month_t;
      beg_balance_update            currency_t;
      end_balance_update            currency_t;
      amount_act_update             currency_t;
      amount_calc_update            currency_t;
      amount_manual_update          currency_t;
      amount_est_update             currency_t;
      beg_balance_orig              currency_type;
      end_balance_orig              currency_type;
      amount_act_orig               currency_type;

      err_deftax_update exception;

      cursor deftax_tmp_cur is
      select   m_id,
               ta_norm_id,
               gl_month,
               beg_balance,
               amount_est,
               amount_act,
               amount_manual,
               amount_calc,
               end_balance
      from tax_accrual_def_tax
      where m_id in  (  select id
                        from tax_accrual_rep_criteria_tmp
                        where criteria_type = 'DEF_TAX_M_ID'
                     )
         and gl_month >= a_gl_month
      order by m_id, ta_norm_id, gl_month;

   BEGIN

      open deftax_tmp_cur;

      fetch deftax_tmp_cur
      bulk collect into m_id,
                        ta_norm_id,
                        gl_month,
                        beg_balance,
                        amount_est,
                        amount_act,
                        amount_manual,
                        amount_calc,
                        end_balance;

      close deftax_tmp_cur;

      num_rows := m_id.COUNT;

      for i in 1 .. num_rows loop

         --Save the original values so that we can confirm whether or not we actually need an update
         beg_balance_orig := beg_balance(i);
         end_balance_orig := end_balance(i);
         amount_act_orig := amount_act(i);

         if m_id(i) <> prev_m_id OR ta_norm_id(i) <> prev_ta_norm_id then
            prev_m_id               := m_id(i);
            prev_ta_norm_id         := ta_norm_id(i);

            -- If this is the first row for a given m_id and ta_norm_id, then we're not rolling a balance
            --    from the prior row.
            balance_roll := beg_balance(i);
         end if;

         amount_act(i) := amount_manual(i) + amount_calc(i);

         beg_balance(i) := balance_roll;
         end_balance(i) := amount_act(i) + balance_roll;

         balance_roll := end_balance(i);

         if beg_balance_orig <> beg_balance(i) or end_balance_orig <> end_balance(i) or amount_act_orig <> amount_act(i)
         then
            j := j + 1;
            m_id_update(j)          := m_id(i);
            ta_norm_id_update(j)    := ta_norm_id(i);
            gl_month_update(j)      := gl_month(i);
            beg_balance_update(j)   := beg_balance(i);
            end_balance_update(j)   := end_balance(i);
            amount_act_update(j)    := amount_act(i);
            amount_calc_update(j)   := amount_calc(i);
            amount_manual_update(j) := amount_manual(i);
            amount_est_update(j)    := amount_est(i);
         end if;

      end loop; --i in 1 .. num_rows loop

      rtn := f_deftax_update  (  m_id_update, ta_norm_id_update, gl_month_update, beg_balance_update, end_balance_update, amount_est_update,
                                 amount_act_update, amount_calc_update, amount_manual_update, msg
                              );

      if rtn < 0 then
         raise err_deftax_update;
      end if;

      a_msg := 'SUCCESS';
      return 1;

   EXCEPTION
      when err_deftax_update then
         a_msg := 'Updating FAS109 table: ' || msg;
         return -1;
      when others then
         a_msg := 'Error ' || sql_msg || ': ' || SQLERRM;
         return -1;

      return 1;

   END f_deftax_roll;

end tax_accrual_deftax_pkg;
--End of Package Body
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (18225, 0, 2018, 2, 1, 0, 0, 'C:\BitBucketRepos\Classic_PB\scripts\00_base\packages', 
    'TAX_ACCRUAL_DEFTAX_PKG.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
