/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037397_depr_PKG_PP_CPR_DEPR.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 03/31/2014 Charlie Shilling
||============================================================================
*/

create or replace package PKG_PP_CPR_DEPR as
  --**********************************************
  --
  --  THIS PACKAGE HANDLES STAGING CPR DEPR RECORDS for:
  --    ARO assets
  --    Leased Assets
  --    CPR DEPR Assets
  --    and FCST CPR DEPR Assets
  --  RESULTS ARE HANDLED IN THIS PACKAGE FOR:
  --    ARO assets
  --    Leased Assets
  --    CPR DEPR Assets
  --    and FCST CPR DEPR Assets
  --
  --**********************************************
  procedure P_ARCHIVELEASEDEPR;
  
  procedure P_STAGEMONTHENDDEPR
  (
    A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
    A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE,
    A_SUBLEDGER   SUBLEDGER_CONTROL.SUBLEDGER_TYPE_ID%type
  );

  procedure P_STAGEMONTHENDDEPR
  (
    A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
    A_MONTH       date,
    A_SUBLEDGER   SUBLEDGER_CONTROL.SUBLEDGER_TYPE_ID%type
  );

  procedure P_FCSTDEPRSTAGE
  (
    A_MONTH date,
    A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE
  );

  procedure P_FCSTDEPRSTAGE
  (
    A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE,
    A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE
  );

end PKG_PP_CPR_DEPR;
/


create or replace package body PKG_PP_CPR_DEPR as
  --**************************************************************
  --       VARIABLES
  --**************************************************************
  G_RTN           NUMBER;

  --**************************************************************************
  --                            P_ARCHIVELEASEDEPR
  --**************************************************************************
  --
  -- Move the results so they're easily visible in MEC
  procedure P_ARCHIVELEASEDEPR is
  begin
    PKG_PP_ERROR.SET_MODULE_NAME('PKG_PP_CPR_DEPR.P_ARCHIVELEASEDEPR');
    
    merge into LS_MONTHLY_DEPR_STG LMDS
    using 
    (
      select   LCAM.LS_ASSET_ID as LS_ASSET_ID,
        CDCS.SET_OF_BOOKS_ID as SOB_ID,
        CDCS.CURR_DEPR_EXPENSE + CDCS.DEPR_EXP_ADJUST + CDCS.DEPR_EXP_ALLOC_ADJUST + CDCS.TRUEUP_ADJ as DEPR_EXP,
        CDCS.GL_POSTING_MO_YR as GL_POSTING_MO_YR
      from  LS_CPR_ASSET_MAP LCAM, CPR_DEPR_CALC_STG CDCS
      where  LCAM.ASSET_ID = CDCS.ASSET_ID
	  and	CDCS.SUBLEDGER_TYPE_ID = -100
	  and CDCS.CURR_DEPR_EXPENSE + CDCS.DEPR_EXP_ADJUST + CDCS.DEPR_EXP_ALLOC_ADJUST + CDCS.TRUEUP_ADJ <> 0
    ) A
    on ( LMDS.LS_ASSET_ID = A.LS_ASSET_ID and LMDS.GL_POSTING_MO_YR = A.GL_POSTING_MO_YR and LMDS.SET_OF_BOOKS_ID = A.SOB_ID)
    when matched then
    update set LMDS.DEPRECIATION_EXPENSE = A.DEPR_EXP
    when not matched then 
    insert (LS_ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, DEPRECIATION_EXPENSE)
    values (A.LS_ASSET_ID, A.SOB_ID, A.GL_POSTING_MO_YR, A.DEPR_EXP);
    
    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
  end P_ARCHIVELEASEDEPR;

  --**************************************************************************
  --                            P_STAGEMONTHENDDEPR
  --**************************************************************************
  --
  --  WRAPPER for the prep function to pass in an array of months.
  --
  procedure P_STAGEMONTHENDDEPR
  (
    A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
    A_MONTHS      PKG_PP_COMMON.DATE_TABTYPE,
    A_SUBLEDGER   SUBLEDGER_CONTROL.SUBLEDGER_TYPE_ID%type
  )
  is
  begin
    --don't add wrappers to the call stack
    for I in A_MONTHS.FIRST .. A_MONTHS.LAST
    loop
      P_STAGEMONTHENDDEPR(A_COMPANY_IDS, A_MONTHS(I), A_SUBLEDGER);
    end loop;
	
	G_RTN := analyze_table('CPR_DEPR_CALC_STG',100);
  end P_STAGEMONTHENDDEPR;

  --**************************************************************************
  --                            P_STAGEMONTHENDDEPR
  --**************************************************************************
  --
  --  Loads the global temp table for calculating depreciation.
  -- THE Load is based on depr ledger for a passed in array of company ids.
  -- AND a single month
  --
  procedure P_STAGEMONTHENDDEPR
  (
    A_COMPANY_IDS PKG_PP_COMMON.NUM_TABTYPE,
    A_MONTH       date,
    A_SUBLEDGER   SUBLEDGER_CONTROL.SUBLEDGER_TYPE_ID%type
  )
  is
  begin
    PKG_PP_ERROR.SET_MODULE_NAME('PKG_PP_CPR_DEPR.P_STAGEMONTHENDDEPR');
    PKG_PP_LOG.P_WRITE_MESSAGE('LOADING the calculation staging table for month:' ||
               TO_CHAR(A_MONTH, 'yyyymm'));

    forall I in indices of A_COMPANY_IDS
      insert into CPR_DEPR_CALC_STG
            (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, INIT_LIFE, REMAINING_LIFE,
             ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS,
             DEPR_CALC_STATUS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH,
             SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN,
             RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS,
             DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE,
             YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE, PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB,
             MONTH_RATE, COMPANY_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID,
             DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR, SALVAGE_EXPENSE,
             SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT,
             IMPAIRMENT_EXPENSE_AMOUNT, END_OF_LIFE, NET_GROSS, OVER_DEPR_CHECK, RATE,
             EFFECTIVE_DATE, SUBLEDGER_TYPE_ID, ENG_IN_SERVICE_YEAR, TRF_WEIGHT, MIN_MPC, TRUEUP_ADJ,
             NET_TRF, ACTIVITY, ACTIVITY_3, BEG_RES_AMT, NET_IMP_AMT, EXISTS_TWO_MONTHS, EXISTS_ARO, HAS_CFNU,
       HAS_NURV, HAS_NURV_LAST_MONTH, EXISTS_LAST_MONTH, NURV_ADJ,
       impairment_asset_activity_salv, impairment_asset_begin_balance)
            with CPR_DEPR_VIEW as
             (select *
                from CPR_DEPR
               where GL_POSTING_MO_YR = A_MONTH
                 and COMPANY_ID = A_COMPANY_IDS(I)),
            DEPR_METHOD_RATES_VIEW as
             (select DD.DEPR_METHOD_ID,
                     DD.SET_OF_BOOKS_ID,
                     DD.EFFECTIVE_DATE,
                     DD.RATE,
                     DD.OVER_DEPR_CHECK,
                     DD.NET_GROSS,
                     DD.END_OF_LIFE,
                     ROW_NUMBER() OVER(partition by DD.DEPR_METHOD_ID, DD.SET_OF_BOOKS_ID order by DD.EFFECTIVE_DATE desc) as THE_ROW
                from DEPR_METHOD_RATES DD
               where DD.EFFECTIVE_DATE <= A_MONTH)
            select A.ASSET_ID,
                   A.SET_OF_BOOKS_ID,
                   A.GL_POSTING_MO_YR,
                   nvl(A.INIT_LIFE,0),
                   nvl(A.REMAINING_LIFE,0),
                   nvl(A.ESTIMATED_SALVAGE,0),
                   nvl(A.BEG_ASSET_DOLLARS,0),
                   nvl(A.NET_ADDS_AND_ADJUST,0),
                   nvl(A.RETIREMENTS,0),
                   2,
                   nvl(A.TRANSFERS_IN,0),
                   nvl(A.TRANSFERS_OUT,0),
                   nvl(A.ASSET_DOLLARS,0),
                   A.BEG_RESERVE_MONTH,
                   nvl(A.SALVAGE_DOLLARS,0),
                   nvl(A.RESERVE_ADJUSTMENT,0),
                   nvl(A.COST_OF_REMOVAL,0),
                   nvl(A.RESERVE_TRANS_IN,0),
                   nvl(A.RESERVE_TRANS_OUT,0),
                   nvl(A.DEPR_EXP_ADJUST,0),
                   nvl(A.OTHER_CREDITS_AND_ADJUST,0),
                   nvl(A.GAIN_LOSS,0),
                   0,--A.DEPRECIATION_BASE,
                   0,--CURR_DEPR_EXPENSE
                   nvl(A.DEPR_RESERVE,0),
                   nvl(A.BEG_RESERVE_YEAR,0),
                   nvl(A.YTD_DEPR_EXPENSE,0),
                   nvl(A.YTD_DEPR_EXP_ADJUST,0),
                   nvl(A.PRIOR_YTD_DEPR_EXPENSE,0),
                   nvl(A.PRIOR_YTD_DEPR_EXP_ADJUST,0),
                   A.ACCT_DISTRIB,
                   nvl(A.MONTH_RATE,0),
                   A.COMPANY_ID,
                   lower(trim(B.MID_PERIOD_METHOD)),
                   NVL(B.MID_PERIOD_CONV, 0) as MID_PERIOD_CONV,
                   B.DEPR_GROUP_ID as DEPR_GROUP_ID,
                   0,--DEPR_EXP_ALLOC_ADJUST,
                   NVL(A.DEPR_METHOD_ID, B.DEPR_METHOD_ID) DEPR_METHOD_ID,
                   NVL(B.TRUE_UP_CPR_DEPR, 2) as TRUE_UP_CPR_DEPR,
                   0,--A.SALVAGE_EXPENSE,
                   nvl(A.SALVAGE_EXP_ADJUST,0),
                   0,--SALVAGE_EXP_ALLOC_ADJUST,
                   nvl(A.IMPAIRMENT_ASSET_AMOUNT,0),
                   nvl(A.IMPAIRMENT_EXPENSE_AMOUNT,0),
                   C.END_OF_LIFE,
                   C.NET_GROSS,
                   C.OVER_DEPR_CHECK,
                   nvl(C.RATE,0),
                   C.EFFECTIVE_DATE,
                   B.SUBLEDGER_TYPE_ID,
                   D.ENG_IN_SERVICE_YEAR,
                   DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                                                                      B.COMPANY_ID),
                                    'no')),
                          'no',
                          1,
                          0),
               DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('CPR DEPR: Allow Exp in Month Added',
                                                                                      B.COMPANY_ID),
                                    'yes')),
                          'no',
                          0,
                          -1
                          ),
                    0,--TRUEUP_ADJ
                    0,0,0,0,0,
                    0,--EXISTS_TWO_MONTHS
                    0,--EXISTS_ARO
          0, --HAS_CFNU
          0, --HAS_NURV
          0, --HAS_NURV_LAST_MONTH
          0, --EXISTS_LAST_MONTH
          0, --NuRV_ADJ
          nvl(a.impairment_asset_activity_salv, 0), nvl(a.impairment_asset_begin_balance, 0)
              from CPR_DEPR_VIEW A, DEPR_GROUP B, DEPR_METHOD_RATES_VIEW C, CPR_LEDGER D
             where B.COMPANY_ID = A.COMPANY_ID
               and A.DEPR_GROUP_ID = B.DEPR_GROUP_ID
               and B.SUBLEDGER_TYPE_ID = A_SUBLEDGER
               and A.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
               and NVL(A.DEPR_METHOD_ID, B.DEPR_METHOD_ID) = C.DEPR_METHOD_ID
               and A.ASSET_ID = D.ASSET_ID
               and C.THE_ROW = 1;
         
    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_STAGEMONTHENDDEPR;
   
   
    procedure P_MAKEFULL
	(
	a_months pkg_pp_common.date_tabtype,
	a_version_id fcst_depr_version.fcst_depr_version_id%type
	)
	is
		type makeFullRec is record(
		  asset_id cpr_depr_calc_stg.asset_id%type,
		  set_of_books_id cpr_depr_calc_stg.set_of_books_id%type,
		  gl_posting_mo_yr cpr_depr_calc_stg.gl_posting_mo_yr%type,
		  init_life cpr_depr_calc_stg.init_life%type);
		type makeFullTable is table of makeFullRec index by pls_integer;
		L_makeFull makeFullTable;
		L_emptyTable makeFullTable;

		L_startMonth date;
		L_endMonth date;
		l_numToFill number;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_PP_CPR_DEPR.P_MAKEFULL');
		
		L_startMonth := a_months( a_months.FIRST );
		L_endMonth := a_months( a_months.LAST );
		
		for i in A_MONTHS.first .. A_MONTHS.last loop
			l_makeFull := l_emptyTable;
			l_numToFill := 0;
		
			SELECT cpr.asset_id, cpr.set_of_books_id, cpr.gl_posting_mo_yr, nvl(cpr.init_life, 1)
			BULK COLLECT
			INTO L_makeFull
			FROM (
			  SELECT a.asset_id, a.set_of_books_id, a.gl_posting_mo_yr, a.init_life, a.asset_dollars, a.depr_reserve, 
				a.net_adds_and_adjust, a.beg_asset_dollars, a.retirements, a.transfers_in, a.transfers_out,
				row_number() over(partition by a.asset_id, a.set_of_books_id order by a.gl_posting_mo_yr desc) as the_row
			  FROM fcst_cpr_depr a, depr_fcst_group_stg tt, company_set_of_books csob, fcst_depr_group_version dd
			  WHERE a.fcst_depr_version_id = a_version_id
			  and a.fcst_depr_group_id = tt.fcst_depr_group_id
			  and a.gl_posting_mo_yr < A_MONTHS(i)
			  and a.fcst_depr_version_id = dd.fcst_depr_version_id
			  and a.fcst_depr_group_id = dd.fcst_depr_group_id
			  and dd.company_id = csob.company_id
			  and a.set_of_books_id = csob.set_of_books_id
			) cpr
			WHERE cpr.the_row = 1
			and ((cpr.beg_asset_dollars + cpr.net_adds_and_adjust + cpr.retirements + 
						cpr.transfers_in + cpr.transfers_out) <> 0 
				or cpr.depr_reserve <> 0 )
			AND NOT EXISTS
			(
				SELECT 1
				FROM cpr_depr_calc_stg stg
				WHERE stg.asset_id = cpr.asset_id
				AND stg.set_of_books_id = cpr.set_of_books_id
				AND stg.gl_posting_mo_yr = A_MONTHS(i)
			);
			
			l_numToFill := L_makeFull.COUNT ;
			PKG_PP_LOG.P_WRITE_MESSAGE('Number of gaps to fill for ' || to_char(A_MONTHS(i)) || ': ' || to_char( l_numToFill ));
			
			IF l_numToFill > 0 then
				FORALL ndx IN INDICES OF L_makeFull
				  INSERT INTO cpr_depr_calc_stg
				  (
					asset_id, set_of_books_id, gl_posting_mo_yr, init_life, remaining_life,
					estimated_salvage, beg_asset_dollars, net_adds_and_adjust, retirements,
					depr_calc_status, transfers_in, transfers_out, asset_dollars, beg_reserve_month,
					salvage_dollars, reserve_adjustment, cost_of_removal, reserve_trans_in,
					reserve_trans_out, depr_exp_adjust, other_credits_and_adjust, gain_loss,
					depreciation_base, curr_depr_expense, depr_reserve, beg_reserve_year, ytd_depr_expense,
					ytd_depr_exp_adjust, prior_ytd_depr_expense, prior_ytd_depr_exp_adjust, acct_distrib,
					month_rate, company_id, mid_period_method, mid_period_conv, depr_group_id,
					depr_exp_alloc_adjust, depr_method_id, true_up_cpr_depr, salvage_expense,
					salvage_exp_adjust, salvage_exp_alloc_adjust, impairment_asset_amount,
					impairment_expense_amount, end_of_life, net_gross, over_depr_check, rate,
					effective_date, subledger_type_id, eng_in_service_year, trf_weight, min_mpc, trueup_adj,
					net_trf, activity, activity_3, beg_res_amt, net_imp_amt, exists_two_months, exists_aro, has_cfnu,
					has_nurv, has_nurv_last_month, exists_last_month, nurv_adj,
					impairment_asset_activity_salv, impairment_asset_begin_balance
				  )
				  WITH DEPR_METHOD_RATES_VIEW as
				  (
					select DD.FCST_DEPR_METHOD_ID as DEPR_METHOD_ID,
					  DD.SET_OF_BOOKS_ID, DD.EFFECTIVE_DATE, DD.RATE,
					  DD.OVER_DEPR_CHECK, DD.NET_GROSS, DD.END_OF_LIFE,
					  ROW_NUMBER() OVER(partition by DD.FCST_DEPR_METHOD_ID, DD.SET_OF_BOOKS_ID order by DD.EFFECTIVE_DATE desc) as THE_ROW
					from FCST_DEPR_METHOD_RATES DD
					where DD.EFFECTIVE_DATE <= A_MONTHS(i)
					and DD.FCST_DEPR_VERSION_ID = A_VERSION_ID
				  )
				  select A.ASSET_ID,
					   A.SET_OF_BOOKS_ID,
					   A_MONTHS(i),
					   L_makeFull( ndx ).init_life,
					   nvl(A.REMAINING_LIFE,0),
					   0, 0, 0, 0,
					   2,
					   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					   0,--A.DEPRECIATION_BASE,
					   0,--CURR_DEPR_EXPENSE
					   0, 0, 0, 0, 0, 0, 'MAKEFULL',
					   nvl(A.MONTH_RATE,0),
					   A.COMPANY_ID,
					   lower(trim(B.MID_PERIOD_METHOD)),
					   NVL(B.MID_PERIOD_CONV, 0),
					   B.FCST_DEPR_GROUP_ID,
					   0,--DEPR_EXP_ALLOC_ADJUST,
					   NVL(B.FCST_DEPR_METHOD_ID, A.FCST_DEPR_METHOD_ID) DEPR_METHOD_ID,
					   0 TRUE_UP_CPR_DEPR,
					   0,--A.SALVAGE_EXPENSE,
					   0,
					   0,--SALVAGE_EXP_ALLOC_ADJUST,
					   0,
					   0,
					   C.END_OF_LIFE,
					   C.NET_GROSS,
					   C.OVER_DEPR_CHECK,
					   nvl(C.RATE,0),
					   C.EFFECTIVE_DATE,
					   B.SUBLEDGER_TYPE_ID,
					   nvl( ( select D.ENG_IN_SERVICE_YEAR from cpr_ledger d where d.asset_id = a.asset_id ), A_MONTHS(i) ),
					   DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
														B.COMPANY_ID),
							  'no')),
						  'no',
						  1,
						  0),
					 DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('CPR DEPR: Allow Exp in Month Added',
														B.COMPANY_ID),
							  'yes')),
						  'no',
						  0,
						  -1
						  ),
					  0,--TRUEUP_ADJ
					  0,0,0,0,0,
					  0,--EXISTS_TWO_MONTHS
					  0,--EXISTS_ARO
					  0, --HAS_CFNU
					  0, --HAS_NURV
					  0, --HAS_NURV_LAST_MONTH
					  1, --EXISTS_LAST_MONTH
					  0, --NuRV_ADJ
					  0, 0
					from fcst_cpr_depr A, FCST_DEPR_GROUP_VERSION B, DEPR_METHOD_RATES_VIEW C
				   where A.FCST_DEPR_GROUP_ID = B.FCST_DEPR_GROUP_ID
					 and A.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
					 and NVL(B.FCST_DEPR_METHOD_ID, A.FCST_DEPR_METHOD_ID) = C.DEPR_METHOD_ID
					 and A.FCST_DEPR_VERSION_ID = B.FCST_DEPR_VERSION_ID
					 and a.fcst_depr_version_id = A_VERSION_ID
					AND a.asset_id = L_makeFull( ndx ).asset_id
					AND a.set_of_books_id =  L_makeFull( ndx ).set_of_books_id
					and a.gl_posting_mo_yr = L_makeFull( ndx ).gl_posting_mo_yr
					and C.THE_ROW = 1;
				
				-- backfill the beg_asset_dollars and beg_reserve_month to be the asset_dollars and depr_reserve from prior month
				FORALL ndx IN INDICES OF L_makeFull
					UPDATE cpr_depr_calc_stg s
					SET (beg_asset_dollars, beg_reserve_month, remaining_life, 
						beg_reserve_year, ytd_depr_expense, acct_distrib) =
					(
						SELECT b.asset_dollars, b.depr_reserve, 
								greatest( b.remaining_life - 1, 0 ), b.beg_reserve_year, b.ytd_depr_expense,
								'MAKEFULL: BACKFILL'
						FROM fcst_cpr_depr b
						WHERE b.fcst_depr_version_id = A_VERSION_ID
						AND b.asset_id = L_makeFull( ndx ).asset_id
						AND b.set_of_books_id =  L_makeFull( ndx ).set_of_books_id
						AND b.gl_posting_mo_yr = add_months( A_MONTHS(i), -1 )
					)
					WHERE s.asset_id = L_makeFull( ndx ).asset_id
					AND s.set_of_books_id =  L_makeFull( ndx ).set_of_books_id
					AND s.gl_posting_mo_yr = A_MONTHS(i)
					;
			end if;
		end loop;
		
		G_RTN := analyze_table('CPR_DEPR_CALC_STG',100);

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
      when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation:');
	end P_MAKEFULL;

  --**************************************************************************
  --                            P_FCSTDEPRSTAGE
  --**************************************************************************
  --
  --  Loads the global temp table for calculating depreciation forecast.
  -- THE Load is based on fcst depr ledger for a version id
  -- AND a single month
  --
  procedure P_FCSTDEPRSTAGE
  (
    A_MONTH date,
    A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE
  )
  is
  begin
    PKG_PP_ERROR.SET_MODULE_NAME('PKG_PP_CPR_DEPR.P_FCSTDEPRSTAGE');
    PKG_PP_LOG.P_WRITE_MESSAGE('LOADING the forecast calculation staging table for month:' || TO_CHAR(A_MONTH, 'yyyymm'));

    insert into CPR_DEPR_CALC_STG
    (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, INIT_LIFE, REMAINING_LIFE,
     ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS,
     DEPR_CALC_STATUS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH,
     SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN,
     RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS,
     DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE,
     YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE, PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB,
     MONTH_RATE, COMPANY_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID,
     DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR, SALVAGE_EXPENSE,
     SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT,
     IMPAIRMENT_EXPENSE_AMOUNT, END_OF_LIFE, NET_GROSS, OVER_DEPR_CHECK, RATE,
     EFFECTIVE_DATE, SUBLEDGER_TYPE_ID, ENG_IN_SERVICE_YEAR, TRF_WEIGHT, MIN_MPC, TRUEUP_ADJ,
     NET_TRF, ACTIVITY, ACTIVITY_3, BEG_RES_AMT, NET_IMP_AMT, EXISTS_TWO_MONTHS, EXISTS_ARO, HAS_CFNU,
     HAS_NURV, HAS_NURV_LAST_MONTH, EXISTS_LAST_MONTH, NURV_ADJ,
     IMPAIRMENT_ASSET_ACTIVITY_SALV, IMPAIRMENT_ASSET_BEGIN_BALANCE)
    with CPR_DEPR_VIEW as
     (select a.*
      from FCST_CPR_DEPR a, depr_fcst_group_stg tt
       where a.GL_POSTING_MO_YR = A_MONTH
       and a.FCST_DEPR_VERSION_ID = A_VERSION_ID
	   and a.fcst_depr_group_id = tt.fcst_depr_group_id ),
    DEPR_METHOD_RATES_VIEW as
     (select DD.FCST_DEPR_METHOD_ID as DEPR_METHOD_ID,
         DD.SET_OF_BOOKS_ID,
         DD.EFFECTIVE_DATE,
         DD.RATE,
         DD.OVER_DEPR_CHECK,
         DD.NET_GROSS,
         DD.END_OF_LIFE,
         ROW_NUMBER() OVER(partition by DD.FCST_DEPR_METHOD_ID, DD.SET_OF_BOOKS_ID order by DD.EFFECTIVE_DATE desc) as THE_ROW
      from FCST_DEPR_METHOD_RATES DD
       where DD.EFFECTIVE_DATE <= A_MONTH
       and DD.FCST_DEPR_VERSION_ID = A_VERSION_ID)
    select A.ASSET_ID,
         A.SET_OF_BOOKS_ID,
         A.GL_POSTING_MO_YR,
         nvl(A.INIT_LIFE,0),
         nvl(A.REMAINING_LIFE,0),
         nvl(A.ESTIMATED_SALVAGE,0),
         nvl(A.BEG_ASSET_DOLLARS,0),
         nvl(A.NET_ADDS_AND_ADJUST,0),
         nvl(A.RETIREMENTS,0),
         2,
         nvl(A.TRANSFERS_IN,0),
         nvl(A.TRANSFERS_OUT,0),
         nvl(A.ASSET_DOLLARS,0),
         A.BEG_RESERVE_MONTH,
         nvl(A.SALVAGE_DOLLARS,0),
         nvl(A.RESERVE_ADJUSTMENT,0),
         nvl(A.COST_OF_REMOVAL,0),
         nvl(A.RESERVE_TRANS_IN,0),
         nvl(A.RESERVE_TRANS_OUT,0),
         nvl(A.DEPR_EXP_ADJUST,0),
         nvl(A.OTHER_CREDITS_AND_ADJUST,0),
         nvl(A.GAIN_LOSS,0),
         0,--A.DEPRECIATION_BASE,
         0,--CURR_DEPR_EXPENSE
         nvl(A.DEPR_RESERVE,0),
         nvl(A.BEG_RESERVE_YEAR,0),
         nvl(A.YTD_DEPR_EXPENSE,0),
         nvl(A.YTD_DEPR_EXP_ADJUST,0),
         nvl(A.PRIOR_YTD_DEPR_EXPENSE,0),
         nvl(A.PRIOR_YTD_DEPR_EXP_ADJUST,0),
         A.ACCT_DISTRIB,
         nvl(A.MONTH_RATE,0),
         B.COMPANY_ID,
         lower(trim(B.MID_PERIOD_METHOD)),
         DECODE(GL_POSTING_MO_YR, A_MONTH, NVL(B.MID_PERIOD_CONV, 0), A.MID_PERIOD_CONV) MID_PERIOD_CONV,
         DECODE(GL_POSTING_MO_YR, A_MONTH, B.FCST_DEPR_GROUP_ID, A.FCST_DEPR_GROUP_ID) DEPR_GROUP_ID,
         0,--DEPR_EXP_ALLOC_ADJUST,
         NVL(A.FCST_DEPR_METHOD_ID, B.FCST_DEPR_METHOD_ID) DEPR_METHOD_ID,
         0 TRUE_UP_CPR_DEPR,
         0,--A.SALVAGE_EXPENSE,
         nvl(A.SALVAGE_EXP_ADJUST,0),
         0,--SALVAGE_EXP_ALLOC_ADJUST,
         nvl(A.IMPAIRMENT_ASSET_AMOUNT,0),
         nvl(A.IMPAIRMENT_EXPENSE_AMOUNT,0),
         C.END_OF_LIFE,
         C.NET_GROSS,
         C.OVER_DEPR_CHECK,
         nvl(C.RATE,0),
         C.EFFECTIVE_DATE,
         B.SUBLEDGER_TYPE_ID,
         nvl( ( select D.ENG_IN_SERVICE_YEAR from cpr_ledger d where d.asset_id = a.asset_id ), A_MONTH ),
         DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
                                          B.COMPANY_ID),
                'no')),
            'no',
            1,
            0),
       DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('CPR DEPR: Allow Exp in Month Added',
                                          B.COMPANY_ID),
                'yes')),
            'no',
            0,
            -1
            ),
        0,--TRUEUP_ADJ
        0,0,0,0,0,
        0,--EXISTS_TWO_MONTHS
        0,--EXISTS_ARO
        0, --HAS_CFNU
        0, --HAS_NURV
        0, --HAS_NURV_LAST_MONTH
        1, --EXISTS_LAST_MONTH
        0, --NuRV_ADJ
        nvl(A.IMPAIRMENT_ASSET_ACTIVITY_SALV, 0), nvl(A.IMPAIRMENT_ASSET_BEGIN_BALANCE, 0)
      from CPR_DEPR_VIEW A, FCST_DEPR_GROUP_VERSION B, DEPR_METHOD_RATES_VIEW C,
			depr_fcst_group_stg tt, COMPANY_SET_OF_BOOKS CSOB
     where A.FCST_DEPR_GROUP_ID = B.FCST_DEPR_GROUP_ID
       --and B.SUBLEDGER_TYPE_ID = A_SUBLEDGER --Load all the subledgers for this FCST run
       and A.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
       and NVL(A.FCST_DEPR_METHOD_ID, B.FCST_DEPR_METHOD_ID) = C.DEPR_METHOD_ID
       and A.FCST_DEPR_VERSION_ID = A_VERSION_ID
       and A.FCST_DEPR_VERSION_ID = B.FCST_DEPR_VERSION_ID
	   and tt.fcst_depr_group_id = b.fcst_depr_group_id
       and C.THE_ROW = 1
		 and A.SET_OF_BOOKS_ID = CSOB.SET_OF_BOOKS_ID
		 and B.COMPANY_ID = CSOB.COMPANY_ID;
         
    PKG_PP_ERROR.REMOVE_MODULE_NAME;
  end P_FCSTDEPRSTAGE;

  --**************************************************************************
  --                            P_FCSTDEPRSTAGE
  --**************************************************************************
  --
  --  WRAPPER for the prep function to pass in an array of months.
  --
  procedure P_FCSTDEPRSTAGE
  (
    A_MONTHS PKG_PP_COMMON.DATE_TABTYPE,
    A_VERSION_ID FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE
  )
  is
  begin
	PKG_PP_ERROR.SET_MODULE_NAME('PKG_PP_CPR_DEPR.P_FCSTDEPRSTAGE_WRAPPER');
  
    for I in A_MONTHS.FIRST .. A_MONTHS.LAST loop
      P_FCSTDEPRSTAGE(A_MONTHS(I), A_VERSION_ID);
    end loop;
    
	G_RTN := analyze_table('CPR_DEPR_CALC_STG',100);
	
    P_MAKEFULL( A_MONTHS, A_VERSION_ID );
	
	PKG_PP_ERROR.REMOVE_MODULE_NAME;
  end P_FCSTDEPRSTAGE;

end PKG_PP_CPR_DEPR;
/

--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (2083, 0, 10, 4, 3, 2, 0, 'C:\PlasticWKS\PowerPlant\sql\packages', '10.4.3.2_PKG_PP_CPR_DEPR.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
