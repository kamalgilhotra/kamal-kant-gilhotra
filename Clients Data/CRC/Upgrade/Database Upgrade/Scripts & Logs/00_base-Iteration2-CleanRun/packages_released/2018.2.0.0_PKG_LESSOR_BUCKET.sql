/*
||============================================================================
|| Application: PowerPlant
|| File Name:   PKG_LESSOR_BUCKET.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2017.1.0.0 11/07/2017 Charlie Shilling Create new package
||============================================================================
*/

CREATE OR REPLACE PACKAGE pkg_lessor_bucket AS
   G_PKG_VERSION varchar(35) := '2018.2.0.0';
   
	procedure p_calc_exec_accruals(a_ilr_schedule_table IN pkg_lessor_common.ilr_schedule_line_table);

	procedure p_calc_cont_accruals(a_ilr_schedule_table IN pkg_lessor_common.ilr_schedule_line_table);

	PROCEDURE p_calc_exec_invoices(ilr_schedule_table IN pkg_lessor_common.ilr_schedule_line_table);

	PROCEDURE p_calc_cont_invoices(ilr_schedule_table IN pkg_lessor_common.ilr_schedule_line_table);

END pkg_lessor_bucket;
/

CREATE OR REPLACE PACKAGE BODY pkg_lessor_bucket AS
	procedure p_calc_exec_accruals(a_ilr_schedule_table IN pkg_lessor_common.ilr_schedule_line_table)
	IS
	BEGIN
		/*Accrual_type_id = 2 is for interest accrual. If payments had accruals they'd be 1. Exec buckets 1-10 are 3-12. Offset by 2*/

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					3 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY1, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY1, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					4 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY2, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY2, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					5 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY3, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY3, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					6 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY4, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY4, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					7 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY5, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY5, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					8 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY6, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY6, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					9 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY7, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY7, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					10 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY8, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY8, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					11 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY9, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY9, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					12 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY10, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).EXECUTORY10, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

	end p_calc_exec_accruals;


	procedure p_calc_cont_accruals(a_ilr_schedule_table IN pkg_lessor_common.ilr_schedule_line_table)
	IS
	BEGIN
		/*Accrual_type_id = 2 is for interest accrual. If payments had accruals they'd be 1. Cont buckets 1-10 are 13-22. Offset by 12*/

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					13 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT1, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT1, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					14 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT2, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT2, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					15 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT3, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT3, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					16 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT4, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT4, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					17 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT5, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT5, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					18 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT6, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT6, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					19 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT7, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT7, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					20 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT8, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT8, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					21 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT9, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT9, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

		forall SCHEDULEINDEX in indices of A_ILR_SCHEDULE_TABLE
			merge into LSR_MONTHLY_ACCRUAL_STG STG
			using (select 0 as ACCRUAL_ID,
					22 as ACCRUAL_TYPE_ID,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ILR_ID,
					nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT10, 0) as AMOUNT,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).MONTH as GL_POSTING_MO_YR,
					A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as SET_OF_BOOKS_ID
					from DUAL
					where nvl(A_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).CONTINGENT10, 0)  <> 0) B
			on (b.ACCRUAL_TYPE_ID = STG.ACCRUAL_TYPE_ID
				and b.ILR_ID = STG.ILR_ID
				and b.GL_POSTING_MO_YR = STG.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID)
			when matched then update set STG.AMOUNT = B.AMOUNT
			when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, B.ACCRUAL_TYPE_ID, B.ILR_ID, B.AMOUNT, B.GL_POSTING_MO_YR, B.SET_OF_BOOKS_ID);

	end p_calc_cont_accruals;

	PROCEDURE p_calc_exec_invoices(ilr_schedule_table IN pkg_lessor_common.ilr_schedule_line_table)
	IS
	BEGIN
		-------------------------------------------------------------------------
		--Create invoice lines for executory_paid1
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for executory_paid1');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				3, --3 = executory_paid1
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory1,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory1 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for executory_paid2
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for executory_paid2');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				4, --4 = executory_paid2
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory2,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory2 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for executory_paid3
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for executory_paid3');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				5, --5 = executory_paid3
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory3,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory3 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for executory_paid4
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for executory_paid4');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				6, --6 = executory_paid4
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory4,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory4 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for executory_paid5
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for executory_paid5');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				7, --7 = executory_paid5
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory5,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory5 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for executory_paid6
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for executory_paid6');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				8, --8 = executory_paid6
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory6,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory6 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for executory_paid7
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for executory_paid7');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				9, --9 = executory_paid7
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory7,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory7 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for executory_paid8
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for executory_paid8');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				10, --10 = executory_paid8
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory8,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory8 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for executory_paid9
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for executory_paid9');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				11, --11 = executory_paid9
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory9,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory9 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for executory_paid10
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for executory_paid10');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				12, --12 = executory_paid10
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory10,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).executory10 <> 0
			AND app.approval_status_id = 1; --1 = initiated
	END p_calc_exec_invoices;

	PROCEDURE p_calc_cont_invoices(ilr_schedule_table IN pkg_lessor_common.ilr_schedule_line_table)
	IS
	BEGIN
		-------------------------------------------------------------------------
		--Create invoice lines for contingent_paid1
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for contingent_paid1');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				13, --13 = contingent_paid1
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent1,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent1 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for contingent_paid2
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for contingent_paid2');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				14, --14 = contingent_paid2
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent2,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent2 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for contingent_paid3
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for contingent_paid3');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				15, --15 = contingent_paid3
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent3,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent3 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for contingent_paid4
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for contingent_paid4');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				16, --16 = contingent_paid4
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent4,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent4 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for contingent_paid5
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for contingent_paid5');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				17, --17 = contingent_paid5
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent5,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent5 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for contingent_paid6
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for contingent_paid6');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				18, --18 = contingent_paid6
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent6,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent6 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for contingent_paid7
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for contingent_paid7');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				19, --19 = contingent_paid7
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent7,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent7 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for contingent_paid8
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for contingent_paid8');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				20, --20 = contingent_paid8
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent8,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent8 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for contingent_paid9
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for contingent_paid9');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				21, --21 = contingent_paid9
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent9,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent9 <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for contingent_paid10
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for contingent_paid10');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				22, --22 = contingent_paid10
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent10,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).month
			AND ilr.company_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).company_id
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).contingent10 <> 0
			AND app.approval_status_id = 1; --1 = initiated
	END p_calc_cont_invoices;

END pkg_lessor_bucket;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (16278, 0, 2018, 2, 0, 0, 0, 'c:\plasticwks\powerplant\sql\packages', 
    'PKG_LESSOR_BUCKET.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
