create or replace PACKAGE pkg_financial_calcs AS
  /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_FINANCIAL_CALCS
   || Description:
   ||============================================================================
   || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version     Date       Revised By     Reason for Change
   || ----------  ---------- -------------- ----------------------------------------
   || 2017.1.0.0  09/12/2017 A. Hill        Original Version
   || 2018.1.0.0  11/16/2018 Anand R        PP-52621 Create new version of IRR calculation f_pp_irr
   ||==================================================================================================================================================
   */
   
  /*****************************************************************************
  * Function: f_npv
  * PURPOSE: Calculates the NPV (Net Present Value) of the given cash flows/discount rate
  * PARAMETERS:
  *   a_cash_flows: Table of cash flow values ordered by period
  *   a_discount_rate: The discount rate
  *
  * RETURNS: NPV
  * NOTE: Cash flows start at period 0 (The NPV function in Excel starts at period 1)
  *       Insert a 0 for the first value in a_cash_flows to start at period 1
  ******************************************************************************/
  G_PKG_VERSION varchar(35) := '2018.2.0.0';
  
  FUNCTION f_npv(a_cash_flows    t_number_22_2_tab, 
                 a_discount_rate FLOAT,
                 a_exponent      t_number_22_8_tab) RETURN FLOAT;
  
  /*****************************************************************************
  * Function: f_npv_prime
  * PURPOSE: Calculates the first derivate of the NPV (Net Present Value) function 
              of the given cash flows/discount rate
  * PARAMETERS:
  *   a_cash_flows: Table of cash flow values ordered by period
  *   a_discount_rate: The discount rate
  *
  * RETURNS: First derivative of NPV function
  * NOTE: Cash flows start at period 0 (The NPV function in Excel starts at period 1)
  *       Insert a 0 for the first value in a_cash_flows to start at period 1
  ******************************************************************************/
  FUNCTION f_npv_prime(a_cash_flows    t_number_22_2_tab, a_discount_rate FLOAT) RETURN FLOAT;
  
  /*****************************************************************************
  * Function: f_irr
  * PURPOSE: Calculates the Interal Rate of Return (rate at which NPV of given cash flows is 0)
              of the given cash flows
  * PARAMETERS:
  *   a_cash_flows: Table of cash flow values ordered by period
  *   a_guess: An initial guess of the IRR (see notes)
  *
  * RETURNS: IRR -- If the IRR cannot be determined using the numerical solver, 
                    NULL is returned (see notes)
  * NOTES:  Cash flows start as period 0
  *         This method uses the Newton-Raphson Method for calculating the root of NPV.
  *         This method is not guaranteed to converge to a root for every initial guess.
  *         The method will attempt additional initial guesses if the supplied initial guess
  *           does not converge to a root within the iteration limit.
  *         Also note the NPV function may have multiple roots. This function will return
  *           only one root (if any at all). Which root is found depends on the initial guess.
  ******************************************************************************/
  FUNCTION f_irr(a_cash_flows t_number_22_2_tab, a_guess FLOAT DEFAULT 0.1) RETURN FLOAT;
  
  /*****************************************************************************
  * Function: f_fv_balance
  * PURPOSE: Calculates the future value of a given balance/rate over the specified
                number of periods
  * PARAMETERS:
  *   a_original_balance: The original balance
  *   a_rate: The interest rate applied to the balance over the given number of periods
  *   a_num_periods: The number of periods
  *
  * RETURNS: Future Value of the balance
  ******************************************************************************/
  FUNCTION f_fv_balance(a_original_balance NUMBER, a_rate NUMBER, a_num_periods NUMBER) RETURN NUMBER;
  
  /*****************************************************************************
  * Function: f_fv_annuity
  * PURPOSE: Calculates the future value of an annunity given with the specified
                payment amount, rate, and number of payments
  * PARAMETERS:
  *   a_payment_amount: The payment amount per period
  *   a_rate: The interest rate applied to the balance over the given number of periods
  *   a_num_payments: The number of payments
  *
  * RETURNS: Future value of the annuity
  ******************************************************************************/
  FUNCTION f_fv_annuity(a_payment_amount number, a_rate number, a_num_payments number) return number;

  /*****************************************************************************
  * Function: f_remaining_balance
  * PURPOSE: Calculates the balance remaning on a simple loan with the given original balance,
              payment amount, and rate after the given number of periods
  * PARAMETERS:
  *   a_original_balance: The original balance
  *   a_rate: The interest rate applied to the balance over the given number of periods
  *   a_num_periods: The number of periods
  *
  * RETURNS: Future value of the annuity
  ******************************************************************************/
  FUNCTION f_remaining_balance_simple(a_original_balance NUMBER, a_payment_amount NUMBER, a_rate NUMBER, a_num_periods NUMBER) RETURN NUMBER;
  
  /*****************************************************************************
  * Function: F_PP_IRR
  * PURPOSE: Calculates the internal rate of retunn for the passed in amounts.
              The logic in this function mimics pkg_lease_schedulel.f_calc_irr()
  * PARAMETERS:
  *   a_cash_flows: array of cash flow 
  *   a_exponent: the exponent to use for calculating the NPV; added to support
  *               partial month logic
  *
  * RETURNS: Internal rate of return
  ******************************************************************************/
  FUNCTION F_PP_IRR(A_CASH_FLOWS T_NUMBER_22_2_TAB, A_EXPONENT T_NUMBER_22_8_TAB) RETURN NUMBER;
  
END pkg_financial_calcs;
/
create or replace PACKAGE BODY pkg_financial_calcs AS
  /*****************************************************************************
  * Function: f_npv
  * PURPOSE: Calculates the NPV (Net Present Value) of the given cash flows/discount rate
  * PARAMETERS:
  *   a_cash_flows: Table of cash flow values ordered by period
  *   a_discount_rate: The discount rate
  *   a_exponent: the exponent to use for calculating the NPV; added to support
  *               partial month logic
  *
  * RETURNS: NPV
  * NOTE: Cash flows start at period 0 (The NPV function in Excel starts at period 1)
  *       Insert a 0 for the first value in a_cash_flows to start at period 1
  ******************************************************************************/
  function f_npv(a_cash_flows    t_number_22_2_tab, 
                 a_discount_rate FLOAT,
                 a_exponent      t_number_22_8_tab) RETURN FLOAT
  IS
    l_npv FLOAT := 0;
  BEGIN
  
    FOR I IN 1..a_cash_flows.COUNT LOOP
      l_npv := l_npv + (a_cash_flows(I) / 
                        POWER(1 + to_binary_double(a_discount_rate), a_exponent(I) - 1));
    END LOOP;

    RETURN l_npv;
    
    EXCEPTION
      WHEN value_error THEN return null;
  END f_npv;
  
  /*****************************************************************************
  * Function: f_npv_prime
  * PURPOSE: Calculates the first derivate of the NPV (Net Present Value) function 
              of the given cash flows/discount rate
  * PARAMETERS:
  *   a_cash_flows: Table of cash flow values ordered by period
  *   a_discount_rate: The discount rate
  *
  * RETURNS: First derivative of NPV function
  * NOTE: Cash flows start at period 0 (The NPV function in Excel starts at period 1)
  *       Insert a 0 for the first value in a_cash_flows to start at period 1
  ******************************************************************************/
  function f_npv_prime(a_cash_flows t_number_22_2_tab, a_discount_rate FLOAT) return FLOAT
  IS
    l_npv_prime FLOAT := 0;
  BEGIN

    FOR I IN 1..a_cash_flows.COUNT LOOP
      l_npv_prime := l_npv_prime + ((a_cash_flows(I) * to_number(I) * -1) / 
                                    POWER(1 + to_binary_double(a_discount_rate), i));
    END LOOP;

    RETURN l_npv_prime;
    
    EXCEPTION
      WHEN value_error THEN RETURN NULL;
  END f_npv_prime;
  
  /*****************************************************************************
  * Function: f_irr
  * PURPOSE: Calculates the Interal Rate of Return (rate at which NPV of given cash flows is 0)
              of the given cash flows
  * PARAMETERS:
  *   a_cash_flows: Table of cash flow values ordered by period
  *   a_guess: An initial guess of the IRR (see notes)
  *
  * RETURNS: IRR -- If the IRR cannot be determined using the numerical solver, 
                    NULL is returned (see notes)
  * NOTES:  Cash flows start as period 0
  *         This method uses the Newton-Raphson Method for calculating the root of NPV.
  *         This method is not guaranteed to converge to a root for every initial guess.
  *         The method will attempt additional initial guesses if the supplied initial guess
  *           does not converge to a root within the iteration limit.
  *         Also note the NPV function may have multiple roots. This function will return
  *           only one root (if any at all). Which root is found depends on the initial guess.
  *         This function only finds roots between 0% and 250% (0-2.5)
  ******************************************************************************/
  FUNCTION f_irr(a_cash_flows t_number_22_2_tab, a_guess FLOAT DEFAULT 0.1) RETURN FLOAT
  IS
    l_j NUMBER := 0;
    l_y FLOAT;
    l_dy FLOAT;
    l_x FLOAT;
    l_epsilon CONSTANT FLOAT:= 1E-8;
    l_max_iters CONSTANT NUMBER := 40;
    l_max_attempts CONSTANT number := 6;
    l_min_rate CONSTANT NUMBER := 0;
    l_max_rate CONSTANT number := 2.5;
    l_exponent t_number_22_8_tab;
  BEGIN
    -- Create a table of exponents that are just 1..a_cash_flows.COUNT
    FOR I IN 1..a_cash_flows.COUNT LOOP
      l_exponent(I) := I;
    END LOOP;

    <<retry>>
    FOR I IN 0..(l_max_attempts-1)
    LOOP
      l_j := 0;
      l_x := a_guess * POWER(10, I * CASE MOD(I, 2) WHEN 0 THEN 1 ELSE -1 END);
      <<calc>>
      LOOP
        l_j := l_j + 1;

        l_y := f_npv(a_cash_flows, l_x, l_exponent);

        EXIT retry WHEN ABS(l_y) <= l_epsilon AND (l_x BETWEEN l_min_rate AND l_max_rate);

        l_dy := f_npv_prime(a_cash_flows, l_x);

        IF l_dy = 0 THEN
          l_x := NULL;
          EXIT calc;
        END IF;

        l_x := l_x - (l_y/l_dy);

        IF l_j > l_max_iters THEN
          l_x := NULL;
          EXIT calc;
        END IF;

      END LOOP calc;
    END LOOP retry;

    return l_x;
  END f_irr;
  
  /*****************************************************************************
  * Function: f_pp_irr
  * PURPOSE: Calculates the internal rate of retunn for the passed in amounts.
              The logic in this function mimics pkg_lease_schedulel.f_calc_irr()
  * PARAMETERS:
  *   a_cash_flows: array of cash flow 
  *   a_exponent: the exponent to use for calculating the NPV; added to support
  *               partial month logic
  *
  * RETURNS: Internal rate of return
  ******************************************************************************/
  FUNCTION F_PP_IRR(A_CASH_FLOWS T_NUMBER_22_2_TAB, A_EXPONENT T_NUMBER_22_8_TAB) RETURN NUMBER
  IS
  L_IRR NUMBER;
  BEGIN
    SELECT (1/X) - 1 
    INTO L_IRR
    FROM (
        WITH INPUT_AMOUNTS AS ( 
               SELECT COLUMN_VALUE AMOUNT, 
                  ROWNUM AS MONTH_NUM 
                  FROM TABLE(CAST(A_CASH_FLOWS AS T_NUMBER_22_2_TAB)) 
             ),
             EXPONENTS AS (
               SELECT COLUMN_VALUE EXPONENT, 
                      ROWNUM AS MONTH_NUM 
                 FROM TABLE(CAST(A_EXPONENT AS T_NUMBER_22_8_TAB))
          )
        SELECT X, 
               THE_ROW 
          FROM INPUT_AMOUNTS I
          JOIN EXPONENTS E ON E.MONTH_NUM = I.MONTH_NUM
       MODEL 
        DIMENSION by(I.MONTH_NUM as THE_ROW) 
    MEASURES(
          (E.EXPONENT - 1) as month_num, 
          I.AMOUNT S,
         0 SS, 
         0 F_A, 
         0 F_B, 
         0 F_X, 
         0 A, 
         1 B, 
         0 X, 
         0 ITER) 
    RULES ITERATE(10000) UNTIL(ABS(F_X [ 1 ]) < POWER(10, -20))
        ( 
           SS [ any ] = S [ CV() ] * POWER(A [ 1 ], month_num [ CV() ]), 
      F_A [ 1 ] = sum(SS) [ any ], 
      SS [ any ] = S [ CV() ] * POWER(B [ 1 ], month_num [ CV() ]), 
      F_B [ 1 ] = sum(SS) [ any ], 
      X [ 1 ] = A [ 1 ] - F_A [ 1 ] * (B [ 1 ] - A [ 1 ]) / (F_B [ 1 ] - F_A [ 1 ]), 
      SS [ any ] = S [ CV() ] * POWER(X [ 1 ], month_num [ CV() ]), 
      F_X [ 1 ] = sum(SS) [ any ], 
      A [ 1 ] = DECODE(SIGN(F_A [ 1 ] * F_X [ 1 ]), 1, X [ 1 ], A [ 1 ]), 
      B [ 1 ] = DECODE(SIGN(F_A [ 1 ] * F_X [ 1 ]), 1, B [ 1 ], X [ 1 ]), 
           ITER [ 1 ] = ITERATION_NUMBER + 1 
        )
      ) DS 
      WHERE DS.THE_ROW = 1 
	    AND DS.X <> 0 ; 
       
	-- Make sure IRR is within range to calculate Annual Rate.
	IF (L_IRR > 4.0) THEN
         L_IRR := 4.0;
       ELSIF (L_IRR < -4.0) THEN
           L_IRR :=-4.0;
     END IF;
       
     RETURN ROUND(L_IRR,18);
     
     EXCEPTION
      WHEN others THEN 
        return NULL;

  END F_PP_IRR;


  /*****************************************************************************
  * Function: f_fv_balance
  * PURPOSE: Calculates the future value of a given balance/rate over the specified
                number of periods
  * PARAMETERS:
  *   a_original_balance: The original balance
  *   a_rate: The interest rate applied to the balance over the given number of periods
  *   a_num_periods: The number of periods
  *
  * RETURNS: Future Value of the balance
  ******************************************************************************/   
  FUNCTION f_fv_balance(a_original_balance NUMBER, a_rate NUMBER, a_num_periods NUMBER) RETURN NUMBER
  IS
  BEGIN
    RETURN a_original_balance * POWER(1 + a_rate, a_num_periods);
  END f_fv_balance;
  
  /*****************************************************************************
  * Function: f_fv_annuity
  * PURPOSE: Calculates the future value of an annunity given with the specified
                payment amount, rate, and number of payments
  * PARAMETERS:
  *   a_payment_amount: The payment amount per period
  *   a_rate: The interest rate applied to the balance over the given number of periods
  *   a_num_payments: The number of payments
  *
  * RETURNS: Future value of the annuity
  ******************************************************************************/
  FUNCTION f_fv_annuity(a_payment_amount NUMBER, a_rate number, a_num_payments NUMBER) RETURN NUMBER
  IS
  BEGIN
    RETURN a_payment_amount*(POWER(1+a_rate, a_num_payments) - 1)/a_rate;
  END f_fv_annuity;
  
  /*****************************************************************************
  * Function: f_remaining_balance
  * PURPOSE: Calculates the balance remaning on a simple loan with the given original balance,
              payment amount, and rate after the given number of periods
  * PARAMETERS:
  *   a_original_balance: The original balance
  *   a_rate: The interest rate applied to the balance over the given number of periods
  *   a_num_periods: The number of periods
  *
  * RETURNS: Future value of the annuity
  ******************************************************************************/
  FUNCTION f_remaining_balance_simple(a_original_balance NUMBER, a_payment_amount NUMBER, a_rate NUMBER, a_num_periods NUMBER) RETURN NUMBER
  IS
  BEGIN
    RETURN f_fv_balance(a_original_balance, a_rate, a_num_periods) - f_fv_annuity(a_payment_amount, a_rate, a_num_periods);
  END f_remaining_balance_simple;
END pkg_financial_calcs;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (16268, 0, 2018, 2, 0, 0, 0, 'c:\plasticwks\powerplant\sql\packages', 
    'pkg_financial_calcs.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
