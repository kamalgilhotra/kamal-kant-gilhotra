/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040405_lease_PKG_LEASE_DEPR.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 07/07/2014 Charlie Shilling
||============================================================================
*/

create or replace package PKG_LEASE_DEPR as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_DEPR
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.3.0 06/25/2014 B.Beck         Original Version
   ||============================================================================
   */
	procedure P_DEPR_SLE( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_LIFE number, A_MSG out varchar2);
				  
	procedure P_DEPR_FERC( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_MSG out varchar2);
				  
	procedure P_FCST_LEASE(A_ILR_ID in number, A_REVISION in number);
	
	procedure P_GET_LEASE_DEPR(A_ILR_ID in number, A_REVISION in number);
   
end PKG_LEASE_DEPR;
/


create or replace package body PKG_LEASE_DEPR as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_DEPR
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.3.0 06/25/2014 B.Beck         Original Version
   ||============================================================================
   */


   

	--**************************************************************************
	--                            Start Body
	--**************************************************************************
	--**************************************************************************
	--                            PROCEDURES
	--**************************************************************************
	   procedure P_DEPR_SLE( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_LIFE number, A_MSG out varchar2, A_IS_FCST number)
   is
      L_MONTHLY_OBL number(22,2);
      L_TOTAL_OBL number(22,2);
      L_TOTAL_CHECK number(22, 2);
      L_NUM_MONTHS number(22,0);
      L_ROUND number(22,2);
    L_MAX_MONTH  LS_ASSET_SCHEDULE."MONTH"%type;
    L_CUR_MONTH  LS_ASSET_SCHEDULE."MONTH"%type;
    L_START_MONTH  LS_ASSET_SCHEDULE."MONTH"%type;
    L_MONTHLOOP number;
	l_is_om number;
   begin
		if a_is_fcst = 0 then
		  A_MSG := 'DELETING from depr_method_sle for current month through end of lease';
		  delete from DEPR_METHOD_SLE
		  where ASSET_ID = A_ASSET_ID
		  and SET_OF_BOOKS_ID = A_SOB_ID
		  ;
		end if;
		
      a_msg := 'GET number of months';
	  select abc."MONTH",  add_months( abc."MONTH", A_LIFE), num_months, total_expense, abc.is_om
	   into L_START_MONTH, L_MAX_MONTH, L_NUM_MONTHS, L_TOTAL_OBL, l_is_om
		from
		(
			select 
				las.month, las.is_om,
				count(1) over() as num_months,
				sum(las.INTEREST_PAID) over() + 
				las.end_capital_cost - /* WMD */ 
					case when la.estimated_residual = 0 then nvl(la.guaranteed_residual_amount, 0)
					else la.fmv * la.estimated_residual end as total_expense, row_number() over(order by month) as the_row
			from ls_asset_schedule las, ls_asset la
			where las.ls_asset_id = A_LS_ASSET_ID
			and las.set_of_books_id = A_SOB_ID
			and las.revision = A_REVISION
			and la.ls_asset_id = A_LS_ASSET_ID
		) abc
		where the_row = 1;

      if A_LIFE > 0 then
         L_MONTHLY_OBL := round( L_TOTAL_OBL / A_LIFE, 2);
         L_TOTAL_CHECK := A_LIFE * L_MONTHLY_OBL;
         L_ROUND := L_TOTAL_OBL - L_TOTAL_CHECK;

		 
		 if a_is_fcst = 0 then
			  A_MSG := 'INSERTING depreciation expense for SLE Method';
			 insert into DEPR_METHOD_SLE
			 (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, DEPR_EXPENSE)
			 select A_ASSET_ID, A_SOB_ID, LAS."MONTH",
					decode(las.is_om, 1, 0,
						case when LAS."MONTH" <> L_MAX_MONTH then L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL
						else L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL + L_ROUND
						end)
			 from LS_ASSET_SCHEDULE LAS
			 where LAS.LS_ASSET_ID = A_LS_ASSET_ID
			 and LAS.REVISION = A_REVISION
			 and LAS.SET_OF_BOOKS_ID = A_SOB_ID
			 ;
		else
			A_MSG := 'INSERTING depreciation expense for SLE Method';
			 insert into LS_DEPR_FORECAST
			(LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, MONTH, begin_reserve, depr_expense, depr_exp_alloc_adjust, end_reserve)
			select las.ls_asset_id, las.set_of_books_id, las.revision, las."MONTH", 
				sum( case when LAS."MONTH" <> L_MAX_MONTH then L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL
				   else L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL + L_ROUND
				   end ) over(partition by  las.ls_asset_id,  las.set_of_books_id,  las.revision order by  las.month) - 
					 case when LAS."MONTH" <> L_MAX_MONTH then L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL
				   else L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL + L_ROUND
				   end as begin_reserve,
				 case when LAS."MONTH" <> L_MAX_MONTH then L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL
				   else L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL + L_ROUND
				   end as depr_expense, 0 as depr_exp_alloc_adjust, 
				sum( case when LAS."MONTH" <> L_MAX_MONTH then L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL
				   else L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL + L_ROUND
				   end) over(partition by  las.ls_asset_id,  las.set_of_books_id,  las.revision order by  las.month) as end_reserve
			from ls_asset_schedule las
			where LAS.LS_ASSET_ID = A_LS_ASSET_ID
			 and LAS.REVISION = A_REVISION
			 and LAS.SET_OF_BOOKS_ID = A_SOB_ID
			 and las.is_om = 0
			 ;
		end if;

       -- add extra months if A_LIFE > L_NUM_MONTHS.  This is the portion after the lease term
       -- and when the asset is "an owned asset"
       if A_LIFE > L_NUM_MONTHS then
        for L_MONTHLOOP in L_NUM_MONTHS .. (A_LIFE - 1) loop
			L_CUR_MONTH := add_months( L_START_MONTH, L_MONTHLOOP );

			if a_is_fcst = 0 then
				insert into DEPR_METHOD_SLE
				(ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, DEPR_EXPENSE)
				values (A_ASSET_ID, A_SOB_ID, L_CUR_MONTH,
					decode(l_is_om, 1, 0, CASE WHEN L_MONTHLOOP <> (A_LIFE - 1) then L_MONTHLY_OBL else L_MONTHLY_OBL + L_ROUND end));
			else
				if l_is_om = 0 then
					insert into LS_DEPR_FORECAST
					(LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, MONTH, begin_reserve, depr_expense, depr_exp_alloc_adjust, end_reserve)
					select LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, L_CUR_MONTH, dg.end_reserve, 
						CASE WHEN L_MONTHLOOP <> (A_LIFE - 1) then L_MONTHLY_OBL else L_MONTHLY_OBL + L_ROUND end, 0, 
						dg.end_reserve + CASE WHEN L_MONTHLOOP <> (A_LIFE - 1) then L_MONTHLY_OBL else L_MONTHLY_OBL + L_ROUND end
					from LS_DEPR_FORECAST dg
					where dg.LS_ASSET_ID = A_LS_ASSET_ID
					and dg.REVISION = A_REVISION
					and dg.SET_OF_BOOKS_ID = A_SOB_ID
					and dg."MONTH" = add_months(L_CUR_MONTH, -1)
					;
				end if;
			end if;
        end loop;
       end if;
      end if;

      A_MSG := 'OK';
   end P_DEPR_SLE;

   procedure P_DEPR_FERC( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_MSG out varchar2)
   is
   begin
      A_MSG := 'DELETING from depr_method_sle for current month through end of lease';
      delete from DEPR_METHOD_SLE
      where ASSET_ID = A_ASSET_ID
      and SET_OF_BOOKS_ID = A_SOB_ID;

    A_MSG := 'INSERTING depreciation expense for FERC Method';
    insert into DEPR_METHOD_SLE
      (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, DEPR_EXPENSE)
      select A_ASSET_ID, A_SOB_ID, LAS."MONTH", decode(las.is_om, 1, 0, LAS.PRINCIPAL_ACCRUAL)
       from LS_ASSET_SCHEDULE LAS
      where LAS.LS_ASSET_ID = A_LS_ASSET_ID
        and LAS.REVISION = A_REVISION
        and LAS.SET_OF_BOOKS_ID = A_SOB_ID;


      A_MSG := 'OK';
   end P_DEPR_FERC;
   
   
   procedure P_DEPR_SLE( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_LIFE number, A_MSG out varchar2)
   is
   begin
      P_DEPR_SLE( A_LS_ASSET_ID, A_REVISION, A_ASSET_ID, A_SOB_ID, A_MONTH, A_LIFE, A_MSG, 0);
   end P_DEPR_SLE;

   
   
   
   procedure P_DEPR_FERC_FCST( A_ILR_ID in number, A_REVISION in number )
   is
   begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_DEPR.P_DEPR_FERC_FCST');
		
		insert into LS_DEPR_FORECAST
		(LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, MONTH, begin_reserve, depr_expense, depr_exp_alloc_adjust, end_reserve)
		select las.ls_asset_id, las.set_of_books_id,  las.revision,  las.month, 
			sum( las.principal_accrual) over(partition by  las.ls_asset_id,  las.set_of_books_id,  las.revision order by  las.month) - 
				 las.principal_accrual as begin_reserve,
			 las.principal_accrual as depr_expense, 0 as depr_exp_alloc_adjust, 
			sum( las.principal_accrual) over(partition by  las.ls_asset_id,  las.set_of_books_id,  las.revision order by  las.month) as end_reserve
		from ls_asset_schedule las, ls_asset la, depr_group dg
		where la.depr_group_id = dg.depr_group_id
		and lower(dg.mid_period_method) = 'ferc'
		and las.ls_asset_id = la.ls_asset_id
		and las.revision = A_REVISION
		and la.ilr_id = A_ILR_ID
		and las.is_om = 0
		;
		
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	end P_DEPR_FERC_FCST;
	
	procedure P_GET_LEASE_DEPR( A_ILR_ID in number, A_REVISION in number)
	is
		L_DG_ID DEPR_GROUP.DEPR_GROUP_ID%TYPE;
		L_MSG varchar2(2000);
			
		type ASSET_REC is record(
			LS_ASSET_ID LS_ASSET.LS_ASSET_ID%type,
			COMPANY_ID COMPANY_SETUP.COMPANY_ID%type,
			GL_ACCOUNT_ID GL_ACCOUNT.GL_ACCOUNT_ID%type,
			MAJOR_LOCATION_ID MAJOR_LOCATION.MAJOR_LOCATION_ID%type,
			UTILITY_ACCOUNT_ID UTILITY_ACCOUNT.UTILITY_ACCOUNT_ID%type,
			BUS_SEGMENT_ID BUSINESS_SEGMENT.BUS_SEGMENT_ID%type,
			SUB_ACCOUNT_ID SUB_ACCOUNT.SUB_ACCOUNT_ID%type,
			VINTAGE number,
			ASSET_LOCATION_ID ASSET_LOCATION.ASSET_LOCATION_ID%type,
			LOCATION_TYPE_ID LOCATION_TYPE.LOCATION_TYPE_ID%type,
			RETIREMENT_UNIT_ID RETIREMENT_UNIT.RETIREMENT_UNIT_ID%type,
			PROPERTY_UNIT_ID PROPERTY_UNIT.PROPERTY_UNIT_ID%type
		);

		type ASSET_TABLE is table of ASSET_REC index by pls_integer;
		L_ASSET_TABLE ASSET_TABLE;
	begin
		L_MSG   := 'Finding depr group';

		-- get attributes required to find depreciation group and calc depreciation
    /* WMD */

		select LA.LS_ASSET_ID, LA.COMPANY_ID, LA.GL_ACCOUNT_ID,
			AL.MAJOR_LOCATION_ID, LA.UTILITY_ACCOUNT_ID, LA.BUS_SEGMENT_ID,
			LA.SUB_ACCOUNT_ID, TO_NUMBER(TO_CHAR(LI.EST_IN_SVC_DATE, 'YYYY')),
			LA.ASSET_LOCATION_ID, ML.LOCATION_TYPE_ID, LA.RETIREMENT_UNIT_ID,
			RU.PROPERTY_UNIT_ID
		bulk collect
		into L_ASSET_TABLE
		from LS_ILR_ACCOUNT IA, LS_ASSET LA, ASSET_LOCATION AL, MAJOR_LOCATION ML,
			LS_ILR LI, RETIREMENT_UNIT RU
		where IA.ILR_ID = A_ILR_ID
		and LI.ILR_ID = A_ILR_ID
         and LA.ILR_ID = A_ILR_ID
		and LA.ASSET_LOCATION_ID = AL.ASSET_LOCATION_ID
		and AL.MAJOR_LOCATION_ID = ML.MAJOR_LOCATION_ID
		and LA.RETIREMENT_UNIT_ID = RU.RETIREMENT_UNIT_ID
		;


		forall ndx in indices of L_ASSET_TABLE
			update ls_asset la
			set depr_group_id = PP_DEPR_PKG.F_FIND_DEPR_GROUP
									(
										L_ASSET_TABLE(ndx).COMPANY_ID,
										L_ASSET_TABLE(ndx).GL_ACCOUNT_ID,
										L_ASSET_TABLE(ndx).MAJOR_LOCATION_ID,
										L_ASSET_TABLE(ndx).UTILITY_ACCOUNT_ID,
										L_ASSET_TABLE(ndx).BUS_SEGMENT_ID,
										L_ASSET_TABLE(ndx).SUB_ACCOUNT_ID,
										-100, --SUBLEDGER_TYPE_ID
										L_ASSET_TABLE(ndx).VINTAGE,
										L_ASSET_TABLE(ndx).ASSET_LOCATION_ID,
										L_ASSET_TABLE(ndx).LOCATION_TYPE_ID,
										L_ASSET_TABLE(ndx).RETIREMENT_UNIT_ID,
										L_ASSET_TABLE(ndx).PROPERTY_UNIT_ID,
										-1, --A_CLASS_CODE_ID
										'NO CLASS CODE'
									)
			where la.ls_asset_id = L_ASSET_TABLE(ndx).LS_ASSET_ID
			AND PP_DEPR_PKG.F_FIND_DEPR_GROUP
									(
										L_ASSET_TABLE(ndx).COMPANY_ID,
										L_ASSET_TABLE(ndx).GL_ACCOUNT_ID,
										L_ASSET_TABLE(ndx).MAJOR_LOCATION_ID,
										L_ASSET_TABLE(ndx).UTILITY_ACCOUNT_ID,
										L_ASSET_TABLE(ndx).BUS_SEGMENT_ID,
										L_ASSET_TABLE(ndx).SUB_ACCOUNT_ID,
										-100, --SUBLEDGER_TYPE_ID
										L_ASSET_TABLE(ndx).VINTAGE,
										L_ASSET_TABLE(ndx).ASSET_LOCATION_ID,
										L_ASSET_TABLE(ndx).LOCATION_TYPE_ID,
										L_ASSET_TABLE(ndx).RETIREMENT_UNIT_ID,
										L_ASSET_TABLE(ndx).PROPERTY_UNIT_ID,
										-1, --A_CLASS_CODE_ID
										'NO CLASS CODE'
									) > 0
			;
	end P_GET_LEASE_DEPR;
	
	function f_lease_stg(A_ILR_ID in number, A_REVISION in number)
	return number
	is
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_DEPR.f_lease_stg');
		--
		insert into CPR_DEPR_CALC_STG
		(ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, INIT_LIFE, REMAINING_LIFE,
		 ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS,
		 DEPR_CALC_STATUS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH,
		 SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN,
		 RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS,
		 DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE,
		 YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE, PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB,
		 MONTH_RATE, COMPANY_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID,
		 DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR, SALVAGE_EXPENSE,
		 SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT,
		 IMPAIRMENT_EXPENSE_AMOUNT, END_OF_LIFE, NET_GROSS, OVER_DEPR_CHECK, RATE,
		 EFFECTIVE_DATE, SUBLEDGER_TYPE_ID, ENG_IN_SERVICE_YEAR, TRF_WEIGHT, MIN_MPC, TRUEUP_ADJ,
		 NET_TRF, ACTIVITY, ACTIVITY_3, BEG_RES_AMT, NET_IMP_AMT, EXISTS_TWO_MONTHS, EXISTS_ARO, HAS_CFNU,
		 HAS_NURV, HAS_NURV_LAST_MONTH, EXISTS_LAST_MONTH, NURV_ADJ,
		 IMPAIRMENT_ASSET_ACTIVITY_SALV, IMPAIRMENT_ASSET_BEGIN_BALANCE)
		with lease_assets as
		(
			select las.ls_asset_id, las.set_of_books_id, las.revision, las.month, las.end_capital_cost, la.depr_group_id, la.economic_life, /* WMD */ 
				dg.mid_period_conv, dg.mid_period_method, la.company_id, dg.depr_method_id,
				case when la.ESTIMATED_RESIDUAL = 0 
					then la.GUARANTEED_RESIDUAL_AMOUNT
					else round(la.ESTIMATED_RESIDUAL * la.FMV, 2) end / las.end_capital_cost as estimated_salvage, /* WMD */ 
				min("MONTH") over(partition by las.ls_asset_id, las.set_of_books_id, las.revision) as in_service,
				count(1) over(partition by las.ls_asset_id, las.set_of_books_id, las.revision) as init_life,
				count(1) over(partition by las.ls_asset_id, las.set_of_books_id, las.revision) -
				row_number() over(partition by las.ls_asset_id, las.set_of_books_id, las.revision order by las.month) + 1 as remaining_life
			from ls_asset_schedule las, ls_asset la, depr_group dg
			where la.depr_group_id = dg.depr_group_id
			and lower(dg.mid_period_method) not in ('sle', 'ferc')
			and dg.subledger_type_id = -100
			and las.ls_asset_id = la.ls_asset_id
			and la.ilr_id = a_ilr_id
			and las.revision = a_revision
			and las.is_om = 0
			and las.end_capital_cost <> 0 /* WMD */ 
		), DEPR_METHOD_RATES_VIEW as
		 (select DD.DEPR_METHOD_ID as DEPR_METHOD_ID,
			 DD.SET_OF_BOOKS_ID,
			 DD.EFFECTIVE_DATE,
			 DD.RATE,
			 DD.OVER_DEPR_CHECK,
			 DD.NET_GROSS,
			 DD.END_OF_LIFE,
			 ROW_NUMBER() OVER(partition by DD.DEPR_METHOD_ID, DD.SET_OF_BOOKS_ID order by DD.EFFECTIVE_DATE desc) as THE_ROW
		  from DEPR_METHOD_RATES DD
		   where DD.EFFECTIVE_DATE <= sysdate)
		select la.ls_asset_id,
			 la.SET_OF_BOOKS_ID,
			 la."MONTH",
			 nvl(la.INIT_LIFE,0),
			 nvl(la.REMAINING_LIFE,0),
			 nvl(la.ESTIMATED_SALVAGE,0),
			 nvl(la.end_capital_cost,0), /* WMD */ 
			 0, 0, 2,0,0,
			 nvl(la.end_capital_cost,0), /* WMD */ 
			 0,0,0,0,0,0,0,0,0,
			 0,--A.DEPRECIATION_BASE,
			 0,--CURR_DEPR_EXPENSE
			 0,0,0,0,0,0,
			 null,
			 0,
			 la.COMPANY_ID,
			 lower(trim(la.MID_PERIOD_METHOD)),
			 la.mid_period_conv MID_PERIOD_CONV,
			 la.depr_group_id DEPR_GROUP_ID,
			 0,--DEPR_EXP_ALLOC_ADJUST,
			 la.depr_method_id DEPR_METHOD_ID,
			 0 TRUE_UP_CPR_DEPR,
			 0,--A.SALVAGE_EXPENSE,
			 0, 0,--SALVAGE_EXP_ALLOC_ADJUST,
			 0, 0,
			 c.END_OF_LIFE, c.NET_GROSS, c.OVER_DEPR_CHECK, nvl(c.RATE,0), c.EFFECTIVE_DATE,
			 -100,
			 la.in_service,
			 DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Transfers In Est Adds',
											  la.COMPANY_ID),
					'no')),
				'no',
				1,
				0),
		   DECODE(LOWER(NVL(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('CPR DEPR: Allow Exp in Month Added',
											  la.COMPANY_ID),
					'yes')),
				'no',
				0,
				-1
				),
			0,--TRUEUP_ADJ
			0,0,0,0,0,
			0,--EXISTS_TWO_MONTHS
			0,--EXISTS_ARO
			0, --HAS_CFNU
			0, --HAS_NURV
			0, --HAS_NURV_LAST_MONTH
			0, --EXISTS_LAST_MONTH
			0, --NuRV_ADJ
			0, 0
		  from DEPR_METHOD_RATES_VIEW c, lease_assets la
		 where la.depr_method_id = c.depr_method_id
		 and la.set_of_books_id = c.set_of_books_id
		 and c.THE_ROW = 1;
         
		 return sql%rowcount;
		 
		 PKG_PP_ERROR.REMOVE_MODULE_NAME;
	end f_lease_stg;
	
	procedure p_lease_calc
	is
	begin
		--
		PKG_DEPR_IND_CALC.P_DEPRCALC_LEASE();
	end p_lease_calc;
	
	procedure p_lease_results(A_ILR_ID in number, A_REVISION in number)
	is
	begin
		--
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_DEPR.p_lease_results');
		
		insert into LS_DEPR_FORECAST
		(LS_ASSET_ID, SET_OF_BOOKS_ID, REVISION, MONTH, begin_reserve, depr_expense, depr_exp_alloc_adjust, end_reserve)
		select c.asset_id, c.set_of_books_id, a_revision, c.gl_posting_mo_yr, 
			c.beg_reserve_month, c.curr_depr_expense, c.depr_exp_alloc_adjust, c.depr_reserve
		from cpr_depr_calc_stg c
		;
		
		
		delete from cpr_depr_calc_stg;
		
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	end p_lease_results;
	
	procedure P_FCST_LEASE(A_ILR_ID in number, A_REVISION in number)
	is
		l_msg varchar2(2000);
		l_check number;
		L_MID_PERIOD_METHOD DEPR_GROUP.MID_PERIOD_METHOD%TYPE;
		l_init_life number;
		l_proc_id number;
		l_num number;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_DEPR.P_FCST_LEASE');
	
		select PROCESS_ID
		into L_PROC_ID
		from PP_PROCESSES
		where lower(DESCRIPTION) = 'lessee calculations'
		;
	
		PKG_PP_LOG.P_START_LOG(L_PROC_ID);
	
		delete from LS_DEPR_FORECAST
		where revision = a_revision
		and ls_asset_id in
		(
			select la.ls_asset_id
			from ls_asset la
			where la.ilr_id = a_ilr_id
		);
	
		-- calc ferc
		P_DEPR_FERC_FCST( A_ILR_ID, A_REVISION );
		
		-- calc sle
		for l_calc_depr in
		(
			select *
			from
			(
				select las.ls_asset_id, las.set_of_books_id, las.revision, las.month, las.end_capital_cost, la.depr_group_id, la.economic_life, /* WMD */ 
					row_number() over(partition by las.ls_asset_id, las.set_of_books_id, las.revision order by las.month) as the_row
				from ls_asset_schedule las, ls_asset la, depr_group dg
				where la.depr_group_id = dg.depr_group_id
				and lower(dg.mid_period_method) = 'sle'
				and las.ls_asset_id = la.ls_asset_id
				and la.ilr_id = A_ILR_ID
				and las.revision = A_REVISION
			)
			where the_row = 1
		)
		loop
			l_init_life := pkg_lease_common.f_get_init_life(l_calc_depr.ls_asset_id, l_calc_depr.revision,
																	a_ilr_id, l_calc_depr.economic_life);
					
			P_DEPR_SLE( l_calc_depr.ls_asset_id, l_calc_depr.revision, 0, l_calc_depr.SET_OF_BOOKS_ID,
						l_calc_depr.MONTH, l_init_life, L_MSG, 1 );
		end loop;
		
		-- calc rest of depr
		l_num := f_lease_stg( a_ilr_id, a_revision);
		if l_num > 0 then
			p_lease_calc();
			p_lease_results( a_ilr_id, a_revision);
		end if;
		
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
   exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_FCST_LEASE;

end PKG_LEASE_DEPR;
/

--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (2429, 0, 2015, 1, 0, 0, 0, 'C:\PlasticWKS\PowerPlant\sql\packages', '2015.1.0.0_PKG_LEASE_DEPR.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
