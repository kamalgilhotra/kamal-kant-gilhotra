  CREATE OR REPLACE PACKAGE "PWRPLANT"."PP_LDAP" AS
   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   TYPE grouprectype IS RECORD(
      username  VARCHAR2(200),
      groupname VARCHAR2(200));
   TYPE grouptable IS TABLE OF grouprectype INDEX BY BINARY_INTEGER;
   FUNCTION login(a_user   VARCHAR2,
                  a_passwd VARCHAR2,
                  a_host   VARCHAR2,
                  a_port   NUMBER) RETURN VARCHAR2;
   FUNCTION search(a_session   VARCHAR2,
                   a_class     VARCHAR2,
                   a_attr      VARCHAR2,
                   a_ldap_path VARCHAR2,
                   a_level     NUMBER) RETURN NUMBER;
   FUNCTION search_list(a_session   VARCHAR2,
                        a_class     VARCHAR2,
                        a_ldap_path VARCHAR2,
                        a_level     NUMBER) RETURN NUMBER;
   FUNCTION verify_user(a_user          VARCHAR2,
                        a_passwd        VARCHAR2,
                        a_ldap_path     VARCHAR2,
                        a_host          VARCHAR2,
                        a_port          NUMBER,
                        a_verify_user   VARCHAR2,
                        a_verify_passwd VARCHAR2) RETURN VARCHAR2;
   FUNCTION logout(a_session VARCHAR2) RETURN NUMBER;
   FUNCTION update_ad_user(a_user      VARCHAR2,
                           a_pw        VARCHAR2,
                           a_server    VARCHAR2,
                           a_dn        VARCHAR2,
                           a_server_dn VARCHAR2,
                           a_port      NUMBER) RETURN NUMBER;
   FUNCTION getpasswd RETURN VARCHAR2;
   FUNCTION profile_passwd(a_name VARCHAR2) RETURN VARCHAR2;
   FUNCTION update_profile(a_name      VARCHAR2,
                           a_user      VARCHAR2,
                           a_pw        VARCHAR2,
                           a_server    VARCHAR2,
                           a_dn        VARCHAR2,
                           a_server_dn VARCHAR2,
                           a_port      NUMBER) RETURN NUMBER;
   FUNCTION update_user(a_session   VARCHAR2,
                        a_user      VARCHAR2,
                        a_server_dn VARCHAR2) RETURN NUMBER;
   FUNCTION get_username(a_username   VARCHAR2,
                         a_first_name OUT VARCHAR2,
                         a_last_name  OUT VARCHAR2) RETURN NUMBER;
   PROCEDURE sync_users;
END;
/

CREATE OR REPLACE PACKAGE BODY "PWRPLANT"."PP_LDAP" AS
   FUNCTION verify_user(a_user          VARCHAR2,
                        a_passwd        VARCHAR2,
                        a_ldap_path     VARCHAR2,
                        a_host          VARCHAR2,
                        a_port          NUMBER,
                        a_verify_user   VARCHAR2,
                        a_verify_passwd VARCHAR2) RETURN VARCHAR2 IS
      num        NUMBER(22,
                        0);
      code       INT;
      retval     PLS_INTEGER;
      my_session dbms_ldap.session;
      my_message dbms_ldap.message;
      CLASS      VARCHAR2(100);
      my_attrs   dbms_ldap.string_collection;
      my_dn      VARCHAR2(256);
      sqls       VARCHAR2(256);
   BEGIN

      dbms_output.enable(200000);
      dbms_ldap.use_exception := TRUE;
      BEGIN
         my_session := dbms_ldap.init(a_host,
                                      a_port);
      EXCEPTION
         WHEN OTHERS THEN
            code := SQLCODE;
            raise_application_error(-20000,
                                    'Failed Connect to  ' || a_host ||
                                    ' Port:' || to_char(a_port) || ' Code=' ||
                                    to_char(code) || ' Msg: ' || SQLERRM);
            RETURN 'failed';
      END;
      BEGIN
         retval := dbms_ldap.simple_bind_s(my_session,
                                           a_user,
                                           a_passwd);
      EXCEPTION
         WHEN OTHERS THEN
            code := SQLCODE;
            raise_application_error(-20000,
                                    'LDAP Login  ' || a_user || ' Code=' ||
                                    to_char(code) || ' Msg: ' || SQLERRM);
            RETURN 'failed';
      END;

      /* Find the user in the ldap server */
      CLASS := 'sAMAccountName=' || a_verify_user;
      my_attrs(1) := '*';
      BEGIN

         retval := dbms_ldap.search_s(my_session,
                                      a_ldap_path,
                                      dbms_ldap.scope_subtree,
                                      CLASS,
                                      my_attrs,
                                      0,
                                      my_message);
      EXCEPTION
         WHEN OTHERS THEN
            raise_application_error(-20000,
                                    'Error searching for  ' ||
                                    a_verify_user || ' Code=' ||
                                    to_char(code) || ' Msg: ' || SQLERRM);
            RETURN - 1;
      END;

      /* check the user's password */
      my_dn := dbms_ldap.get_dn(my_session,
                                my_message);
      dbms_output.put_line(' ID DN: ' || my_dn);

      BEGIN
         retval := dbms_ldap.unbind_s(my_session);
         retval := dbms_ldap.simple_bind_s(my_session,
                                           my_dn,
                                           a_verify_passwd);

      EXCEPTION
         WHEN OTHERS THEN
            raise_application_error(-20000,
                                    'Password Invalid for  ' ||
                                    a_verify_user || ' Code=' ||
                                    to_char(code) || ' Msg: ' || SQLERRM);
            RETURN - 1;
      END;
      retval := dbms_ldap.unbind_s(my_session);

      /* the user's password against the database */
      BEGIN
         sqls := 'alter user pwrtest  IDENTIFIED BY ' || a_verify_passwd;
         EXECUTE IMMEDIATE sqls;

         SELECT COUNT(*)
           INTO num
           FROM (SELECT password
                   FROM dba_users
                  WHERE username = 'PWRTEST'
                 MINUS
                 SELECT password
                   FROM dba_users
                  WHERE username = upper(a_verify_user));
      EXCEPTION
         WHEN OTHERS THEN
            raise_application_error(-20000,
                                    'Verifying password for  ' ||
                                    a_verify_user || ' Code=' ||
                                    to_char(code) || ' Msg: ' || SQLERRM);
            RETURN - 1;
      END;
      IF num = 0
      THEN
         /* password is the same th database password */
         RETURN 0;
      ELSIF num = 1
      THEN
         raise_application_error(-20000,
                                 'No Oracle ID exist for  ' ||
                                 a_verify_user);
         RETURN - 1;

      END IF;
      /* sync the user's Active Dir password with the Oracle password */
      BEGIN
         sqls := 'alter user "' || upper(a_verify_user) ||
                 '"  IDENTIFIED BY ' || a_verify_passwd;
         EXECUTE IMMEDIATE sqls;
      EXCEPTION
         WHEN OTHERS THEN
            raise_application_error(-20000,
                                    'Sync Oracle Password  ' ||
                                    a_verify_user || ' Code=' ||
                                    to_char(code) || ' Msg: ' || SQLERRM);
            RETURN - 1;
      END;
      RETURN 0;

   END;
   /**************************************** Login *****************************************************/

   FUNCTION login(a_user   VARCHAR2,
                  a_passwd VARCHAR2,
                  a_host   VARCHAR2,
                  a_port   NUMBER) RETURN VARCHAR2

    IS
      code       INT;
      retval     PLS_INTEGER;
      my_session dbms_ldap.session;
      host       VARCHAR2(100);
      port       NUMBER(22,
                        0);
      passwd     VARCHAR(100);
      ldap_user  VARCHAR(100);
   BEGIN

      retval    := -1;
      host      := a_host;
      port      := a_port;
      ldap_user := a_user;
      passwd    := a_passwd;

      dbms_ldap.use_exception := TRUE;
      BEGIN
         my_session := dbms_ldap.init(a_host,
                                      a_port);
         dbms_output.put_line(rpad('Ldap session ',
                                   25,
                                   ' ') || ': ' ||
                              rawtohex(substr(my_session,
                                              1,
                                              8)) ||
                              '(returned from init)');
      EXCEPTION
         WHEN OTHERS THEN
            code := SQLCODE;
            raise_application_error(-20000,
                                    'Failed Connect to  ' || a_host ||
                                    ' Port:' || to_char(a_port) || ' Code=' ||
                                    to_char(code) || ' Msg: ' || SQLERRM);
            RETURN 'failed';
      END;

      BEGIN
         retval := dbms_ldap.simple_bind_s(my_session,
                                           a_user,
                                           a_passwd);
         dbms_output.put_line(rpad('simple_bind_s Returns ',
                                   25,
                                   ' ') || ': ' || to_char(retval));
      EXCEPTION
         WHEN OTHERS THEN
            code := SQLCODE;
            raise_application_error(-20000,
                                    'Login  ' || a_user || ' Code=' ||
                                    to_char(code) || ' Msg: ' || SQLERRM);
            RETURN 'failed';
      END;
      RETURN to_char(rawtohex(my_session));
   END;
   /**************************** logout ***********************************************/
   FUNCTION logout(a_session VARCHAR2) RETURN NUMBER IS
      code       INT;
      my_session dbms_ldap.session;
   BEGIN
      my_session := hextoraw(a_session);
      code       := dbms_ldap.unbind_s(my_session);
      RETURN code;
   EXCEPTION
      WHEN OTHERS THEN
         code := SQLCODE;
         raise_application_error(-20000,
                                 'Logout Error  Code=' || to_char(code) ||
                                 ' Msg: ' || SQLERRM);
         RETURN code;
   END logout;

   /************************ Search ************************/
   FUNCTION search(a_session   VARCHAR2,
                   a_class     VARCHAR2,
                   a_attr      VARCHAR2,
                   a_ldap_path VARCHAR2,
                   a_level     NUMBER) RETURN NUMBER

    IS
      retval     PLS_INTEGER;
      my_session dbms_ldap.session;

      emp_dn       VARCHAR2(256);
      my_attrs     dbms_ldap.string_collection;
      my_message   dbms_ldap.message;
      my_entry     dbms_ldap.message;
      entry_index  PLS_INTEGER;
      my_attr_name VARCHAR2(256);
      my_ber_elmt  dbms_ldap.ber_element;
      my_dn        VARCHAR2(256);
      i            PLS_INTEGER;
      code         INT;
      my_vals      dbms_ldap.string_collection;
      attr_index   PLS_INTEGER;
      ldap_host    VARCHAR2(256);
      ldap_port    VARCHAR2(256);
      ldap_user    VARCHAR2(256);
      ldap_passwd  VARCHAR2(256);
      ldap_base    VARCHAR2(256);
      first_name   VARCHAR2(256);
      last_name    VARCHAR2(256);
      user_pwd     VARCHAR2(256);
      i1           VARCHAR2(3000);
      i3           VARCHAR2(1000);
      i4           VARCHAR2(200);
      CLASS        VARCHAR(256);
      gt           grouptable;
   BEGIN

      my_session := hextoraw(a_session);
      CLASS := a_class;
      my_attrs(1) := a_attr;
      ldap_base := a_ldap_path;
      IF a_level = 1
      THEN
         retval := dbms_ldap.search_s(my_session,
                                      ldap_base,
                                      dbms_ldap.scope_base,
                                      CLASS,
                                      my_attrs,
                                      0,
                                      my_message);
      ELSIF a_level = 2
      THEN
         retval := dbms_ldap.search_s(my_session,
                                      ldap_base,
                                      dbms_ldap.scope_onelevel,
                                      CLASS,
                                      my_attrs,
                                      0,
                                      my_message);
      ELSIF a_level = 3
      THEN
         retval := dbms_ldap.search_s(my_session,
                                      ldap_base,
                                      dbms_ldap.scope_subtree,
                                      CLASS,
                                      my_attrs,
                                      0,
                                      my_message);
      END IF;
      retval := dbms_ldap.count_entries(my_session,
                                        my_message);
      IF retval = 0
      THEN
         RETURN 0;
      END IF;

      -- get the first entry
      my_entry    := dbms_ldap.first_entry(my_session,
                                           my_message);
      entry_index := 1;
      DELETE temp_ldap_search;
      -- Loop through each of the entries one by one
      WHILE my_entry IS NOT NULL
      LOOP
         -- get the current entry
         my_dn        := dbms_ldap.get_dn(my_session,
                                          my_entry);
         my_attr_name := dbms_ldap.first_attribute(my_session,
                                                   my_entry,
                                                   my_ber_elmt);
         attr_index   := 1;
         WHILE my_attr_name IS NOT NULL
         LOOP
            BEGIN
               my_vals := dbms_ldap.get_values(my_session,
                                               my_entry,
                                               my_attr_name);
               IF my_vals.COUNT > 0
               THEN
                  FOR i IN my_vals.FIRST .. my_vals.LAST
                  LOOP
                     i1 := my_vals(i);
                     INSERT INTO temp_ldap_search
                        (dn,
                         attr_name,
                         attr_value,
                         pos)
                     VALUES
                        (my_dn,
                         my_attr_name,
                         i1,
                         i);
                  END LOOP;
               END IF;
            EXCEPTION
               WHEN OTHERS THEN
                  dbms_output.put_line(SQLERRM);
            END;
            my_attr_name := dbms_ldap.next_attribute(my_session,
                                                     my_entry,
                                                     my_ber_elmt);
            attr_index   := attr_index + 1;
         END LOOP;
         -- Free ber_element
         IF my_attr_name IS NOT NULL
         THEN
            dbms_ldap.ber_free(my_ber_elmt,
                               0);
         END IF;
         my_entry    := dbms_ldap.next_entry(my_session,
                                             my_entry);
         entry_index := entry_index + 1;
      END LOOP;
      --retval := DBMS_LDAP.unbind_s(my_session);
      RETURN entry_index;
   EXCEPTION
      WHEN OTHERS THEN
         code := SQLCODE;
         raise_application_error(-20000,
                                 'Search Error  Code=' || to_char(code) ||
                                 ' Msg: ' || SQLERRM);
         RETURN code;
   END;

   /************************ Search ************************/
   FUNCTION search_list(a_session   VARCHAR2,
                        a_class     VARCHAR2,
                        a_ldap_path VARCHAR2,
                        a_level     NUMBER) RETURN NUMBER

    IS
      retval       PLS_INTEGER;
      my_session   dbms_ldap.session;
      attrib       VARCHAR2(256);
      emp_dn       VARCHAR2(256);
      my_attrs     dbms_ldap.string_collection;
      my_message   dbms_ldap.message;
      my_entry     dbms_ldap.message;
      entry_index  PLS_INTEGER;
      my_attr_name VARCHAR2(256);
      my_ber_elmt  dbms_ldap.ber_element;
      my_dn        VARCHAR2(256);
      i            PLS_INTEGER;
      code         INT;
      num          NUMBER(22,
                          0);
      my_vals      dbms_ldap.string_collection;
      attr_index   PLS_INTEGER;
      ldap_host    VARCHAR2(256);
      ldap_port    VARCHAR2(256);
      ldap_user    VARCHAR2(256);
      ldap_passwd  VARCHAR2(256);
      ldap_base    VARCHAR2(256);
      first_name   VARCHAR2(256);
      last_name    VARCHAR2(256);
      user_pwd     VARCHAR2(256);
      i1           VARCHAR2(3000);
      i3           VARCHAR2(1000);
      i4           VARCHAR2(200);
      CLASS        VARCHAR(256);
      gt           grouptable;
      CURSOR attrib_list_cur IS
         SELECT NAME
           FROM temp_ldap_attrib_list;
   BEGIN

      my_session := hextoraw(a_session);
      OPEN attrib_list_cur;
      num := 0;
      LOOP
         FETCH attrib_list_cur
            INTO attrib;
         IF (attrib_list_cur%NOTFOUND)
         THEN
            EXIT;
         END IF;
         num := num + 1;
         my_attrs(num) := attrib;
      END LOOP;
      CLASS := a_class;

      IF a_level = 1
      THEN
         retval := dbms_ldap.search_s(my_session,
                                      ldap_base,
                                      dbms_ldap.scope_base,
                                      CLASS,
                                      my_attrs,
                                      0,
                                      my_message);
      ELSIF a_level = 2
      THEN
         retval := dbms_ldap.search_s(my_session,
                                      ldap_base,
                                      dbms_ldap.scope_onelevel,
                                      CLASS,
                                      my_attrs,
                                      0,
                                      my_message);
      ELSIF a_level = 3
      THEN
         retval := dbms_ldap.search_s(my_session,
                                      ldap_base,
                                      dbms_ldap.scope_subtree,
                                      CLASS,
                                      my_attrs,
                                      0,
                                      my_message);
      END IF;

      retval := dbms_ldap.count_entries(my_session,
                                        my_message);

      -- get the first entry
      my_entry    := dbms_ldap.first_entry(my_session,
                                           my_message);
      entry_index := 1;

      -- Loop through each of the entries one by one
      WHILE my_entry IS NOT NULL
      LOOP
         -- get the current entry
         my_dn        := dbms_ldap.get_dn(my_session,
                                          my_entry);
         my_attr_name := dbms_ldap.first_attribute(my_session,
                                                   my_entry,
                                                   my_ber_elmt);
         attr_index   := 1;
         WHILE my_attr_name IS NOT NULL
         LOOP
            my_vals := dbms_ldap.get_values(my_session,
                                            my_entry,
                                            my_attr_name);
            IF my_vals.COUNT > 0
            THEN
               FOR i IN my_vals.FIRST .. my_vals.LAST
               LOOP
                  i1 := my_vals(i);
                  INSERT INTO temp_ldap_search
                     (dn,
                      attr_name,
                      attr_value)
                  VALUES
                     (my_dn,
                      my_attr_name,
                      i1);
               END LOOP;
            END IF;
            my_attr_name := dbms_ldap.next_attribute(my_session,
                                                     my_entry,
                                                     my_ber_elmt);
            attr_index   := attr_index + 1;
         END LOOP;
         -- Free ber_element
         dbms_ldap.ber_free(my_ber_elmt,
                            0);
         my_entry    := dbms_ldap.next_entry(my_session,
                                             my_entry);
         entry_index := entry_index + 1;
      END LOOP;
      --retval := DBMS_LDAP.unbind_s(my_session);

      RETURN entry_index;
   EXCEPTION
      WHEN OTHERS THEN
         code := SQLCODE;
         raise_application_error(-20000,
                                 'Search List Error  Code=' ||
                                 to_char(code) || ' Msg: ' || SQLERRM);
         RETURN code;
   END;

   /************************ Update user  **************************************************************/
   FUNCTION update_user(a_session   VARCHAR2,
                        a_user      VARCHAR2,
                        a_server_dn VARCHAR2) RETURN NUMBER

    IS
      retval     PLS_INTEGER;
      my_session dbms_ldap.session;

      emp_dn       VARCHAR2(256);
      my_attrs     dbms_ldap.string_collection;
      my_message   dbms_ldap.message;
      my_entry     dbms_ldap.message;
      entry_index  PLS_INTEGER;
      my_attr_name VARCHAR2(256);
      my_ber_elmt  dbms_ldap.ber_element;
      my_dn        VARCHAR2(256);
      i            PLS_INTEGER;
      code         INT;
      num          INT;
      j            INT;
      TYPE attr_type IS TABLE OF VARCHAR2(200) INDEX BY BINARY_INTEGER;
      int_attrs   attr_type;
      ext_attrs   attr_type;
      ext_attr    VARCHAR2(256);
      int_attr    VARCHAR2(256);
      attr_val    VARCHAR2(256);
      attr_vals   attr_type;
      attr_count  NUMBER(22,
                         0);
      attr        VARCHAR2(200);
      sqls        VARCHAR2(4000);
      sqls2       VARCHAR2(4000);
      arg         VARCHAR2(2000);
      len         INT;
      id          NUMBER(22,
                         0);
      my_vals     dbms_ldap.string_collection;
      attr_index  PLS_INTEGER;
      ldap_host   VARCHAR2(256);
      ldap_port   VARCHAR2(256);
      ldap_user   VARCHAR2(256);
      ldap_passwd VARCHAR2(256);
      ldap_base   VARCHAR2(256);
      first_name  VARCHAR2(256);
      last_name   VARCHAR2(256);
      user_pwd    VARCHAR2(256);
      i1          VARCHAR2(3000);
      i3          VARCHAR2(1000);
      i4          VARCHAR2(200);
      CLASS       VARCHAR(256);
      gt          grouptable;
      CURSOR ldap_cur IS
         SELECT attr_name,
                attr_value,
                internal_attribute
           FROM temp_ldap_search,
                pp_ldap_attributes
          WHERE attr_name = external_attribute;
   BEGIN

      my_session := hextoraw(a_session);
      CLASS := 'sAMAccountName=' || a_user;
      my_attrs(1) := '*';
      ldap_base := a_server_dn;

      retval := dbms_ldap.search_s(my_session,
                                   ldap_base,
                                   dbms_ldap.scope_subtree,
                                   CLASS,
                                   my_attrs,
                                   0,
                                   my_message);

      retval := dbms_ldap.count_entries(my_session,
                                        my_message);
      IF retval = 0
      THEN
         RETURN 0;
      END IF;

      -- get the first entry
      my_entry    := dbms_ldap.first_entry(my_session,
                                           my_message);
      entry_index := 1;
      DELETE temp_ldap_search;
      -- Loop through each of the entries one by one
      WHILE my_entry IS NOT NULL
      LOOP
         -- get the current entry
         my_dn        := dbms_ldap.get_dn(my_session,
                                          my_entry);
         my_attr_name := dbms_ldap.first_attribute(my_session,
                                                   my_entry,
                                                   my_ber_elmt);
         attr_index   := 1;
         WHILE my_attr_name IS NOT NULL
         LOOP
            BEGIN
               my_vals := dbms_ldap.get_values(my_session,
                                               my_entry,
                                               my_attr_name);
               IF my_vals.COUNT > 0
               THEN
                  FOR i IN my_vals.FIRST .. my_vals.LAST
                  LOOP
                     i1 := my_vals(i);
                     INSERT INTO temp_ldap_search
                        (dn,
                         attr_name,
                         attr_value,
                         pos)
                     VALUES
                        (my_dn,
                         my_attr_name,
                         i1,
                         i);
                     attr_vals(attr_index) := i4;

                  END LOOP;
               END IF;
            EXCEPTION
               WHEN OTHERS THEN
                  dbms_output.put_line(SQLERRM);
                  RETURN - 1;
            END;
            dbms_output.put_line(my_attr_name || ' ' || i1);
            my_attr_name := dbms_ldap.next_attribute(my_session,
                                                     my_entry,
                                                     my_ber_elmt);
            attr_index   := attr_index + 1;
         END LOOP;
         -- Free ber_element
         IF my_attr_name IS NOT NULL
         THEN
            dbms_ldap.ber_free(my_ber_elmt,
                               0);
         END IF;
         my_entry    := dbms_ldap.next_entry(my_session,
                                             my_entry);
         entry_index := entry_index + 1;
      END LOOP;
      COMMIT;
      OPEN ldap_cur;

      sqls       := 'update PP_SECURITY_USERS set ';
      num        := 0;
      attr_count := attr_index;
      LOOP
         FETCH ldap_cur
            INTO ext_attr, attr_val, int_attr;
         IF (ldap_cur%NOTFOUND)
         THEN
            EXIT;
         END IF;
         IF int_attr = 'full_name'
         THEN
            code  := get_username(attr_val,
                                  first_name,
                                  last_name);
            sqls2 := sqls || ' first_name  = ''' || first_name || ''',';
            sqls  := sqls2 || ' last_name = ''' || last_name || ''',';
         ELSIF int_attr = 'class_name'
         THEN
            NULL;
         ELSIF int_attr = 'ADsPath'
         THEN
            NULL;
         ELSIF int_attr = 'distinguishedName'
         THEN
            NULL;
         ELSIF int_attr = 'user_id'
         THEN
            NULL;
         ELSIF int_attr = 'company'
         THEN
            BEGIN
               SELECT company_id
                 INTO id
                 FROM company_setup
                WHERE lower(description) = lower(int_attr);
               attr_val := to_char(id);
            EXCEPTION
               WHEN OTHERS THEN
                  NULL;
            END;
         ELSE
            sqls := sqls || int_attr || '=''' || attr_val || ''' ';
            sqls := sqls || ',';
         END IF;
         num := num + 1;
      END LOOP;

      CLOSE ldap_cur;
      -- remove last comma
      len  := length(sqls);
      sqls := substr(sqls,
                     1,
                     len - 1);
      sqls := sqls || ' where  users = ''' || a_user || '''';
      IF num > 0
      THEN
         EXECUTE IMMEDIATE sqls;
         NULL;
      ELSE
         raise_application_error(-20000,
                                 sqls ||
                                 ' Search Error, No Attributes found for ' ||
                                 a_user);
         RETURN - 1;
      END IF;

      RETURN entry_index;
   EXCEPTION
      WHEN OTHERS THEN
         code := SQLCODE;
         raise_application_error(-20001,
                                 sqls || ' Search Error  Attr = ' ||
                                 to_char(attr_count) || ' Code=' ||
                                 to_char(code) || ' Msg: ' || SQLERRM);
         RETURN code;
   END;
   /************************************** get User Name **************************************/
   FUNCTION get_username(a_username   VARCHAR2,
                         a_first_name OUT VARCHAR2,
                         a_last_name  OUT VARCHAR2) RETURN NUMBER IS
      pos  NUMBER(22,
                  0);
      pos2 NUMBER(22,
                  0);
   BEGIN

      -- is it last first
      pos := instr(a_username,
                   ',');
      IF pos <> 0
      THEN
         --/yes

         a_last_name := substr(a_username,
                               1,
                               pos - 1);
         pos2        := instr(a_username,
                              ' ',
                              pos + 1);
         IF pos2 > 0
         THEN
            -- must have middle name
            a_first_name := substr(a_username,
                                   pos + 1,
                                   pos2 - pos);
         ELSE
            a_first_name := substr(a_username,
                                   pos + 1);
         END IF;
      ELSE

         pos  := instr(a_username,
                       ' ',
                       pos + 1);
         pos2 := instr(a_username,
                       ' ',
                       pos + 1);
         IF pos2 = 0
         THEN
            a_last_name  := substr(a_username,
                                   pos);
            a_first_name := substr(a_username,
                                   1,
                                   pos - 1);
         ELSE
            a_first_name := substr(a_username,
                                   1,
                                   pos - 1);
            a_last_name  := substr(a_username,
                                   pos2);
         END IF;
      END IF;
      RETURN 0;
   END get_username;

   /****************************************** Update AD User ***********************************/
   FUNCTION update_ad_user(a_user      VARCHAR2,
                           a_pw        VARCHAR2,
                           a_server    VARCHAR2,
                           a_dn        VARCHAR2,
                           a_server_dn VARCHAR2,
                           a_port      NUMBER) RETURN NUMBER IS
      pw_raw       RAW(254);
      key_raw      RAW(254);
      data_out_raw RAW(254);
      token        VARCHAR2(100) := 'PPCSSOKEYPPC';
      code         INT;
   BEGIN
      DELETE pp_ldap_user;

      key_raw := utl_raw.cast_to_raw(rpad(token,
                                          24));
      pw_raw  := utl_raw.cast_to_raw(rpad(a_pw,
                                          48));
      dbms_obfuscation_toolkit.desencrypt(input          => pw_raw,
                                          key            => key_raw,
                                          encrypted_data => data_out_raw);

      INSERT INTO pp_ldap_user
         (NAME,
          password,
          server,
          dn,
          server_dn,
          port)
      VALUES
         (a_user,
          data_out_raw,
          a_server,
          a_dn,
          a_server_dn,
          a_port);
      COMMIT;
      RETURN 0;
   EXCEPTION
      WHEN OTHERS THEN
         code := SQLCODE;
         raise_application_error(-20000,
                                 ' Error updating pp_ldap_user Code=' ||
                                 to_char(code) || ' Msg: ' || SQLERRM);
         RETURN code;
   END update_ad_user;

   /**************************************** Get Password *****************************************************/
   FUNCTION getpasswd RETURN VARCHAR2 IS
      num          NUMBER(22,
                          0);
      code         INT;
      retval       PLS_INTEGER;
      pw_raw       RAW(2049);
      pw           VARCHAR(1024);
      key_raw      RAW(128);
      data_in_raw  RAW(2049);
      data_out_raw RAW(2049);
      token        VARCHAR2(100) := 'PPCSSOKEYPPC';
      CURSOR sso_user_cur IS
         SELECT password
           FROM pp_ldap_user;
   BEGIN

      OPEN sso_user_cur;
      LOOP
         FETCH sso_user_cur
            INTO pw_raw;
         --   if(sso_user_cur%NOTFOUND) then
         EXIT;
         --   end if;

      END LOOP;
      CLOSE sso_user_cur;
      dbms_output.enable(200000);
      dbms_ldap.use_exception := TRUE;
      key_raw                 := utl_raw.cast_to_raw(rpad(token,
                                                          24));
      data_in_raw             := pw_raw;
      dbms_obfuscation_toolkit.desdecrypt(input          => data_in_raw,
                                          key            => key_raw,
                                          decrypted_data => data_out_raw);
      pw := rtrim(utl_raw.cast_to_varchar2(data_out_raw));
      RETURN pw;
   EXCEPTION
      WHEN OTHERS THEN
         code := SQLCODE;
         raise_application_error(-20000,
                                 ' Error ldap password Code=' ||
                                 to_char(code) || ' Msg: ' || SQLERRM);
         RETURN code;
   END getpasswd;

   /**************************************** Profile password *****************************************************/
   FUNCTION profile_passwd(a_name VARCHAR2) RETURN VARCHAR2 IS
      num          NUMBER(22,
                          0);
      code         INT;
      retval       PLS_INTEGER;
      pw_raw       RAW(2049);
      pw           VARCHAR(1024);
      key_raw      RAW(128);
      data_in_raw  RAW(2049);
      data_out_raw RAW(2049);
      token        VARCHAR2(100) := 'PPCSSOKEYPPC';
      CURSOR sso_user_cur IS
         SELECT password
           FROM pp_ldap_profiles
          WHERE NAME = a_name;
   BEGIN

      OPEN sso_user_cur;
      LOOP
         FETCH sso_user_cur
            INTO pw_raw;
         --   if(sso_user_cur%NOTFOUND) then
         EXIT;
         --   end if;

      END LOOP;
      CLOSE sso_user_cur;
      dbms_output.enable(200000);
      dbms_ldap.use_exception := TRUE;
      key_raw                 := utl_raw.cast_to_raw(rpad(token,
                                                          24));
      data_in_raw             := pw_raw;
      dbms_obfuscation_toolkit.desdecrypt(input          => data_in_raw,
                                          key            => key_raw,
                                          decrypted_data => data_out_raw);
      pw := rtrim(utl_raw.cast_to_varchar2(data_out_raw));
      RETURN pw;
   EXCEPTION
      WHEN OTHERS THEN
         code := SQLCODE;
         raise_application_error(-20000,
                                 ' Error ldap password Code=' ||
                                 to_char(code) || ' Msg: ' || SQLERRM);
         RETURN code;
   END profile_passwd;

   /****************************************** Update Profile ***********************************/
   FUNCTION update_profile(a_name      VARCHAR2,
                           a_user      VARCHAR2,
                           a_pw        VARCHAR2,
                           a_server    VARCHAR2,
                           a_dn        VARCHAR2,
                           a_server_dn VARCHAR2,
                           a_port      NUMBER) RETURN NUMBER IS
      pw_raw       RAW(254);
      key_raw      RAW(254);
      data_out_raw RAW(254);
      token        VARCHAR2(100) := 'PPCSSOKEYPPC';
      code         INT;
      num          INT;
   BEGIN
      DELETE pp_ldap_user;

      key_raw := utl_raw.cast_to_raw(rpad(token,
                                          24));
      pw_raw  := utl_raw.cast_to_raw(rpad(a_pw,
                                          48));
      dbms_obfuscation_toolkit.desencrypt(input          => pw_raw,
                                          key            => key_raw,
                                          encrypted_data => data_out_raw);
      SELECT COUNT(*)
        INTO num
        FROM pp_ldap_profiles
       WHERE NAME = a_name;
      IF num = 0
      THEN
         INSERT INTO pp_ldap_profiles
            (NAME,
             logid,
             password,
             server,
             dn,
             server_dn,
             port)
         VALUES
            (a_name,
             a_user,
             data_out_raw,
             a_server,
             a_dn,
             a_server_dn,
             a_port);
      ELSE
         UPDATE pp_ldap_profiles
            SET logid     = a_user,
                password  = data_out_raw,
                server    = a_server,
                dn        = a_dn,
                server_dn = a_server_dn,
                port      = a_port
          WHERE NAME = a_name;
      END IF;

      COMMIT;
      RETURN 0;
   EXCEPTION
      WHEN OTHERS THEN
         code := SQLCODE;
         raise_application_error(-20000,
                                 ' Error updating pp_ldap_profiles Code=' ||
                                 to_char(code) || ' Msg: ' || SQLERRM);
         RETURN code;
   END update_profile;

   /**************************************** Sync Users *****************************************************/
   PROCEDURE sync_users IS
      code       INT;
      num        INT;
      my_session dbms_ldap.session;
      userid     VARCHAR2(200);
      host       VARCHAR2(200);
      server_dn2 VARCHAR2(200);
      session2   VARCHAR2(1000);
      NAME       VARCHAR2(100);
      msg_str    VARCHAR(200);
      passwd     VARCHAR2(100);
      port       INT;
      CURSOR user_cur IS
         SELECT users
           FROM pp_security_users
          WHERE adsi_path IS NOT NULL;
   BEGIN
      BEGIN
         SELECT NAME,
                server,
                server_dn,
                port
           INTO userid,
                host,
                server_dn2,
                port
           FROM pp_ldap_user
          WHERE rownum = 1;
      EXCEPTION
         WHEN OTHERS THEN
            code    := SQLCODE;
            msg_str := 'Error getting userid and server from pp_ldap_user.   Msg=' ||
                       SQLERRM;
            INSERT INTO pp_ldap_user_sync_log
               (log_date,
                code,
                msg,
                user_name)
            VALUES
               (SYSDATE,
                code,
                msg_str,
                USER);
            raise_application_error(-20000,
                                    ' Error Syncing Users Code=' ||
                                    to_char(code) || ' Msg: ' || SQLERRM);
            RETURN;

      END;
      BEGIN
         passwd := getpasswd();
      EXCEPTION
         WHEN OTHERS THEN
            code    := SQLCODE;
            msg_str := 'Error getting password  ' || userid || ' Msg=' ||
                       SQLERRM;
            INSERT INTO pp_ldap_user_sync_log
               (log_date,
                code,
                msg,
                user_name)
            VALUES
               (SYSDATE,
                code,
                msg_str,
                USER);
            raise_application_error(-20000,
                                    ' Error Syncing Users Code=' ||
                                    to_char(code) || ' Msg: ' || SQLERRM);
            RETURN;
      END;
      BEGIN
         session2 := login(userid,
                           passwd,
                           host,
                           port);
      EXCEPTION
         WHEN OTHERS THEN
            code    := SQLCODE;
            msg_str := 'Error Login  ' || userid || ' Msg=' || SQLERRM;
            INSERT INTO pp_ldap_user_sync_log
               (log_date,
                code,
                msg,
                user_name)
            VALUES
               (SYSDATE,
                code,
                msg_str,
                USER);
            raise_application_error(-20000,
                                    ' Error Syncing Users Code=' ||
                                    to_char(code) || ' Msg: ' || SQLERRM);
            RETURN;
      END;

      my_session := hextoraw(session2);
      OPEN user_cur;
      LOOP
         FETCH user_cur
            INTO NAME;
         IF (user_cur%NOTFOUND)
         THEN
            EXIT;
         END IF;
         BEGIN
            code := update_user(session2,
                                NAME,
                                server_dn2);
         EXCEPTION
            WHEN OTHERS THEN
               code    := SQLCODE;
               msg_str := 'Error Updating user ' || NAME || ' Msg=' ||
                          SQLERRM;
               INSERT INTO pp_ldap_user_sync_log
                  (log_date,
                   code,
                   msg,
                   user_name)
               VALUES
                  (SYSDATE,
                   code,
                   msg_str,
                   USER);
         END;
      END LOOP;
      INSERT INTO pp_ldap_user_sync_log
         (log_date,
          msg,
          user_name)
      VALUES
         (SYSDATE,
          'Finshed OK',
          USER);
      COMMIT;
      RETURN;
   EXCEPTION
      WHEN OTHERS THEN
         code    := SQLCODE;
         msg_str := SQLERRM;
         INSERT INTO pp_ldap_user_sync_log
            (log_date,
             code,
             msg,
             user_name)
         VALUES
            (SYSDATE,
             code,
             msg_str,
             USER);
         COMMIT;
         raise_application_error(-20000,
                                 ' Error Syncing Users Code=' ||
                                 to_char(code) || ' Msg: ' || SQLERRM);
         RETURN;
   END sync_users;
END;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (18215, 0, 2018, 2, 1, 0, 0, 'C:\BitBucketRepos\Classic_PB\scripts\00_base\packages', 
    'PP_LDAP.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
