create or replace package pkg_lessor_approval AS
   /*
   ||============================================================================
   || Application: PowerPlan
   || Object Name: PKG_LESSOR_APPROVAL
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan Inc, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 2017.1.0 10/02/2015 Shane "C" Ward Original Version
   || 2017.1.0 10/04/2015 Shane "C" Ward Add ILR Workflows
   || 2017.3.0 31/01/2018 Anand R        Add JEs to Lessor ILR
   || 2017.3.0 02/07/2018 J Sisouphanh   Add Function to Derecognized CPR Assets
   || 2017.3.0 02/19/2018 Anand R        PP-50513 Remove ILR approval logic from Send
   || 2017.3.0 02/22/2018 Anand R        PP-505376 Add JEs for Direct Finance ILR approval
   || 2017.3.0 05/07/2018 Anand R        PP-51092 Do not restrict Initial direct cost by In service date   
   ||============================================================================
   */

   function F_APPROVE_MLA(A_LEASE_ID number,
                          A_REVISION number) return number;

   function F_REJECT_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_SEND_MLA(A_LEASE_ID number,
                       A_REVISION number) return number;

   function F_UNREJECT_MLA(A_LEASE_ID number,
                           A_REVISION number) return number;

   function F_UNSEND_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID number,
                                  A_REVISION number) return number;

   function F_APPROVE_ILR(A_ILR_ID   number,
                          A_REVISION NUMBER,
                          A_SEND_JES BOOLEAN DEFAULT TRUE) return number;

   function F_REJECT_ILR(A_ILR_ID   number,
                         A_REVISION number) return number;

   function F_SEND_ILR(A_ILR_ID   number,
                       A_REVISION number) return number;

   function F_UNREJECT_ILR(A_ILR_ID   number,
                           A_REVISION number) return number;

   function F_UNSEND_ILR(A_ILR_ID   number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID   number,
                                  A_REVISION number) return number;


   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   OUT VARCHAR2,
                                    A_SEND_JES IN BOOLEAN DEFAULT TRUE) return number;

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2,
                                    A_LOG in boolean,
                                    a_is_transfer IN BOOLEAN DEFAULT FALSE,
                                    A_SEND_JES IN BOOLEAN DEFAULT TRUE) return number;

   function F_INVOICE_APPROVE(A_COMPANY_ID in number,
                              A_MONTH      in date,
                              A_END_LOG    in number:=null) return varchar2;

   function F_SEND_INVOICE(A_INVOICE_ID number) return number;

   function F_UPDATE_WORKFLOW_INVOICE(A_INVOICE_ID number) return number;

   function F_REJECT_INVOICE(A_INVOICE_ID number) return number;

   function F_UNREJECT_INVOICE(A_INVOICE_ID number) return number;

   function F_APPROVE_INVOICE(A_INVOICE_ID number) return number;

   function F_UNSEND_INVOICE(A_INVOICE_ID number) return number;

   function F_CREATE_JE_FOR_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number;
                           
   PROCEDURE p_mass_approve_ilrs( a_ilrs t_lsr_ilr_id_revision_tab,
                                  a_send_jes_0_1 number);
													 
end PKG_Lessor_approval;
/
   --**************************************************************************
--                            Initialize Package
--**************************************************************************

CREATE OR REPLACE PACKAGE BODY pkg_lessor_approval AS

   /*
   ||============================================================================
   || Application: PowerPlan
   || Object Name: PKG_LESSOR_APPROVAL
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan Inc, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 2017.1.0 10/02/2015 Shane "C" Ward Original Version
   || 2017.3.0 02/07/2018 J Sisouphanh   Add Function to Derecognized CPR Assets
   ||============================================================================
   */
   
     
  TYPE t_kickout IS RECORD (ilr_id lsr_ilr_schedule_kickouts.ilr_id%TYPE,
                            revision lsr_ilr_schedule_kickouts.revision%TYPE,
                            message lsr_ilr_schedule_kickouts.message%TYPE,
                            occurrence_id lsr_ilr_schedule_kickouts.occurrence_id%type);
  TYPE t_kickout_tab IS TABLE OF t_kickout;

  L_ILR_ID number;
  
  /*****************************************************************************
  * Procedure: p_log_kickouts
  * PURPOSE: Logs kickouts to the lsr_ilr_approvals_kickouts table
  * PARAMETERS:
  *   a_kickouts: The kickouts to log to the table
  * NOTES: Uses autonomous transaction
  ******************************************************************************/
  PROCEDURE p_log_kickouts(a_kickouts t_kickout_tab) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    l_ids t_number_22_2_tab;
    l_count number;
  BEGIN    
    l_count := a_kickouts.COUNT;

    SELECT lsr_ilr_approval_kickouts_seq.NEXTVAL
    BULK COLLECT INTO l_ids
    FROM dual
    WHERE l_count <> 0
    CONNECT BY LEVEL <= l_count;

    FORALL I IN 1..a_kickouts.COUNT
    INSERT INTO lsr_ilr_approval_kickouts(kickout_id, 
                                          ilr_id, 
                                          revision, 
                                          message,
                                          occurrence_id)
    VALUES( l_ids(I), 
            a_kickouts(I).ilr_id, 
            a_kickouts(I).revision, 
            a_kickouts(I).message,
            a_kickouts(i).occurrence_id);
            
    FOR I IN 1..a_kickouts.COUNT
    LOOP
      pkg_pp_log.p_write_message('ILR ID/Revision: ' || to_char(a_kickouts(I).ilr_id || '/' || to_char(a_kickouts(I).revision) || ' - ' || a_kickouts(I).message));
    END LOOP;

    COMMIT;
  END p_log_kickouts;


   --**************************************************************************
   --                            P_SET_ILR_ID
   --**************************************************************************

   procedure P_SET_ILR_ID(A_ILR_ID number) is

   begin
      L_ILR_ID := A_ILR_ID;
   end P_SET_ILR_ID;

   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   procedure P_LOGERRORMESSAGE(A_ILR_ID in number,
                               A_MSG    in varchar2) is
      pragma autonomous_transaction;
   begin
      update LS_PEND_TRANSACTION set ERROR_MESSAGE = A_MSG where ILR_ID = A_ILR_ID;

      commit;
   end P_LOGERRORMESSAGE;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************

  function F_EXECUTE_IMMEDIATE(A_SQLS in varchar2, A_START_LOG in number:=null) return varchar2
    is
    counter number;
    begin
    if nvl(a_start_log,0) = 1 then
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
    end if;

    if lower(trim(pkg_pp_system_control.f_pp_system_control('Lease Debug Execute Immediate'))) = 'yes' then
      pkg_pp_log.p_write_message(a_sqls);
    end if;

    execute immediate A_SQLS;

    return 'OK';

    exception when others then
      return 'Error Executing SQL: ' || sqlerrm || ' ' || sqlcode || chr(13) || chr(10) || a_sqls;
    end f_execute_immediate;
   
   /*--**************************************************************************
                               F_DERECOGNIZE_CPR_ASSETS
  * PURPOSE: Automatically derecognizes CPR assets when the ILR is approved and goes In Service
  * PARAMETERS:
  *   a_ilr_id: ilr_id of the ILR for which to process the schedule
  *   a_revision: Revision of the ILR for which to process the schedule
  * RETURNS: 1
   --**************************************************************************/
   
   function F_DERECOGNIZE_CPR_ASSETS(A_ILR_ID   in number, A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL
      
   L_MSG varchar2(2000);
   L_GL_JE_CODE varchar(35);
   L_RTN number;
   L_DEPR_IND number;
   L_RESERVE_AMT number;
   L_GAINLOSS_AMT number;
   L_GL_POSTING_MO_YR     date;   

   begin
     
      PKG_PP_LOG.P_WRITE_MESSAGE('  Derecognizing CPR Assets for ILR: ' || TO_CHAR(A_ILR_ID) || ' Revision: ' || TO_CHAR(A_REVISION));   
   
      L_MSG := '  Getting the minimum open month for Lessor';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);   
     
     select MIN(ACCOUNTING_MONTH)
     into L_GL_POSTING_MO_YR
     from CPR_CONTROL, LSR_ILR ILR
     where CPR_CONTROL.COMPANY_ID = ILR.COMPANY_ID 
       and ILR.ILR_ID = A_ILR_ID
       and ACCOUNTING_MONTH > (
           select max(accounting_month) from cpr_control 
           where company_id = ILR.COMPANY_ID 
           and cpr_closed is not null
           );

      L_MSG := '    Minimum Accounting Month: ' || TO_CHAR(L_GL_POSTING_MO_YR);
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);   

       
     if L_GL_POSTING_MO_YR is null then
       L_MSG := '    Error getting Minimum Accounting Month from CPR Control: Null';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);   
       return -1;
     end if;
     
     SELECT s.gl_je_code into L_GL_JE_CODE 
     FROM standard_journal_entries s, gl_je_control g 
     WHERE g.process_id = 'LSR Derecognition' 
       and g.je_id = s.je_id;
       
      L_MSG := '  GL JE Code: ' || L_GL_JE_CODE;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);       
       
     if L_GL_JE_CODE is null then
       L_MSG := '  Error getting GL JE Code from Standard Journal Entries: Null';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);   
       return -1;
     end if;  
      
     --FOR LOOP for each CPR Asset
      for L_CPRS in (
									select distinct asset.cpr_asset_id, 
												 asset.description,
												 cpr.work_order_number as Work_Order,
												 cpr.accum_quantity * -1 as Posting_Quantity,
												 cpr.accum_cost * -1 as Posting_Amount,
												 subledger_indicator,
												 cpr.retirement_unit_id,
												 cpr.utility_account_id,
												 cpr.bus_segment_id,
												 cpr.func_class_id,
												 cpr.sub_account_id,
												 cpr.asset_location_id,
												 cpr.company_id,
												 cpr.eng_in_service_year,
												 cpr.depr_group_id,
												 cpr.gl_account_id,
												 cpr.long_description,
												 cpr.property_group_id,
												 ilr.est_in_svc_date
									from lsr_asset asset
												INNER JOIN cpr_ledger cpr ON asset.cpr_asset_id = cpr.asset_id
												INNER JOIN lsr_ilr ilr ON asset.ilr_id = ilr.ilr_id and asset.revision = ilr.current_revision
												INNER JOIN lsr_ilr_options options on options.ilr_id = ilr.ilr_id
			                  INNER JOIN lsr_fasb_type_SOB sob on sob.cap_type_id = options.lease_cap_type_id
			                  INNER JOIN lsr_fasb_cap_type cap_type on cap_type.fasb_cap_type_id = sob.fasb_cap_type_id
									where ilr.ilr_id = A_ILR_ID	
									  and cap_type.fasb_cap_type_id in (2,3)
                     )
       loop
           
           --Read the depreciation indicator
           L_MSG:='  Pulling Depreciation Indicator';
           PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);   
           begin
             select S.DEPRECIATION_INDICATOR
             into L_DEPR_IND
             from SUBLEDGER_CONTROL S 
             where S.SUBLEDGER_TYPE_ID = L_CPRS.Subledger_Indicator;
           
           exception
             when NO_DATA_FOUND then
              L_MSG := '    Depreciation Indicator From SUBLEDGER_CONTROL not found.';
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);  
              L_MSG := '  Continuing to the next CPR Asset.';
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);  
             continue;
           end;
        
           --Calculate Reserve Depending on Subledger Indicator
           IF L_DEPR_IND = 3 THEN
             begin  
               L_MSG:='    Depreciation Indicator = 3: Calculate Reserve Calculation via Individual Depreciation Method';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);   
               select (cpr_depr.beg_reserve_month + 
                      cpr_depr.retirements +
                      cpr_depr.depr_exp_adjust + 
                      cpr_depr.salvage_dollars + 
                      cpr_depr.cost_of_removal + 
                      cpr_depr.other_credits_and_adjust + 
                      cpr_depr.gain_loss + 
                      cpr_depr.reserve_trans_in + 
                      cpr_depr.reserve_trans_out) into L_RESERVE_AMT
               from cpr_depr 
               where cpr_depr.set_of_books_id = 1
                     and cpr_depr.asset_id = L_CPRS.CPR_ASSET_ID
                     and cpr_depr.gl_posting_mo_yr =   L_GL_POSTING_MO_YR;
               
               L_MSG := '    Reserve Amount Calculation:' || TO_CHAR(L_RESERVE_AMT);
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);  
                      
                     
             exception
               when NO_DATA_FOUND then
                L_MSG := '    No data found while calculating the reserve for ILR:' || TO_CHAR(A_ILR_ID);
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);  
                L_MSG := '    Continuing to the next CPR Asset.';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);  
               continue;
             end;       

           ELSE

             begin
               L_MSG:='    Depreciation Indicator = 0. Calculate Reserve Calculation via Group Depreciation Method';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);   
               select depr_res_allo_factors.factor into L_RESERVE_AMT
               from depr_res_allo_factors
               where L_CPRS.depr_group_id = depr_res_allo_factors.depr_group_id 
                and depr_res_allo_factors.set_of_books_id = 1
                and to_number(to_char(L_CPRS.eng_in_service_year,'YYYY')) = depr_res_allo_factors.vintage 
                and depr_res_allo_factors.month = L_GL_POSTING_MO_YR;
             exception
               when NO_DATA_FOUND then
                L_MSG := '    No data found while calculating reserve through the table depr_res_allo_factors for ILR:' || TO_CHAR(A_ILR_ID);
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);  
			 			   L_MSG := '  Continuing to the next CPR Asset.';
			         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);	
							 continue;
						 end;

					END IF;

					 L_GAINLOSS_AMT := (L_CPRS.POSTING_AMOUNT * -1) - L_RESERVE_AMT;	
					 L_MSG := '    Gain Loss Calculation:' || TO_CHAR(L_GAINLOSS_AMT);
	         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);				 				 
					 
					 --Insert into Pending Transaction
					 INSERT INTO pend_transaction(
									pend_trans_id,
									ldg_asset_id,
									ldg_activity_id,
									gl_posting_mo_yr,
									work_order_number,
									activity_code,
									user_id1,
									gl_je_code,
									description,
									posting_quantity,
									posting_amount,
									retire_method_id,
									posting_status,
									reserve,
									gain_loss,
									misc_description,
									retirement_unit_id,
									utility_account_id,
									bus_segment_id,
									func_class_id,
									sub_account_id, 
									asset_location_id,
									company_id,
									ferc_activity_code,
									disposition_code,
									gl_account_id,
									subledger_indicator,
									in_service_year,
									long_description,
									property_group_id,
									ldg_depr_group_id,
									adjusted_reserve,
									gain_loss_reversal)
						SELECT 
									PWRPLANT1.NEXTVAL,
									L_CPRS.CPR_ASSET_ID,
									NULL,
									L_GL_POSTING_MO_YR,
									L_CPRS.WORK_ORDER,
									'SAGL',
									user,
									L_GL_JE_CODE,
									'Asset Derecognition',
									L_CPRS.POSTING_QUANTITY,
									L_CPRS.POSTING_AMOUNT,
									1,
									1,
									L_RESERVE_AMT,
									L_GAINLOSS_AMT,
									'Lessor ILR:' || TO_CHAR(A_ILR_ID) || '; REVISION:' || TO_CHAR(A_REVISION), 
									L_CPRS.RETIREMENT_UNIT_ID,
									L_CPRS.UTILITY_ACCOUNT_ID,
									L_CPRS.BUS_SEGMENT_ID, 
									L_CPRS.FUNC_CLASS_ID, 
									L_CPRS.SUB_ACCOUNT_ID, 
									L_CPRS.ASSET_LOCATION_ID, 
									L_CPRS.COMPANY_ID,
									2 as FERC_ACTIVITY_CODE,
									19 as DISPOSITION_CODE,
									L_CPRS.GL_ACCOUNT_ID,
									L_CPRS.SUBLEDGER_INDICATOR,
									L_CPRS.EST_IN_SVC_DATE,									
									L_CPRS.LONG_DESCRIPTION,
									L_CPRS.PROPERTY_GROUP_ID,
									L_CPRS.DEPR_GROUP_ID,
									L_RESERVE_AMT,
									0
						FROM DUAL;
						
						PKG_PP_LOG.P_WRITE_MESSAGE('  Records inserted into Pend_Transaction: ' || TO_CHAR(sql%rowcount));		
						PKG_PP_LOG.P_WRITE_MESSAGE('  CPR Asset ID: ' || TO_CHAR(L_CPRS.CPR_ASSET_ID));		
						
			 end loop;
			 
			 commit;

			 return 1;

		 exception
				when others then
					 rollback;
					 RAISE_APPLICATION_ERROR(-20000, 'Error Auto Derecognizing CPR in function F_DERECOGNIZE_CPR()' || ': ' || SQLERRM);
					 return -1;			  
												 
	 end F_DERECOGNIZE_CPR_ASSETS;	 
	

   --**************************************************************************
   --                            F_APPROVE_ILR_NO_COMMIT
   --************************************************************************** 

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   OUT VARCHAR2,
                                    A_SEND_JES IN BOOLEAN DEFAULT TRUE) return number is
      RTN    number;
      MY_STR varchar2(2000);

   begin
      rtn := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, A_STATUS, TRUE, FALSE, A_SEND_JES);
      return rtn;
   exception
      when others then
         P_LOGERRORMESSAGE(A_ILR_ID, A_STATUS);

         return -1;
   end F_APPROVE_ILR_NO_COMMIT;


   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2,
                                    A_LOG in boolean,
                                    a_is_transfer IN BOOLEAN DEFAULT FALSE,
                                    A_SEND_JES IN BOOLEAN DEFAULT TRUE) return number is
      RTN    number;
      MY_STR varchar2(2000);
	  L_COUNT number;	  

   begin

      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());

      A_STATUS := 'APPROVE ILR:';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := '   ILR:' || TO_CHAR(A_ILR_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := '   REVISION: ' || TO_CHAR(A_REVISION);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Updating LSR_ILR_APPROVAL1';

      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LSR_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Updating LSR_ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      update LSR_ILR L set ILR_STATUS_ID = 2, CURRENT_REVISION = A_REVISION where ILR_ID = A_ILR_ID;

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
	  
      --Derecognize CPR Assets for any Sales Type or Direct Finance ILR--
      SELECT COUNT(cap_type.fasb_cap_type_id)
          INTO l_count
          FROM lsr_ilr
         INNER JOIN lsr_ilr_options options
            ON options.ilr_id = lsr_ilr.ilr_id
         INNER JOIN lsr_fasb_type_sob sob
            ON sob.cap_type_id = options.lease_cap_type_id
         INNER JOIN lsr_fasb_cap_type cap_type
            ON cap_type.fasb_cap_type_id = sob.fasb_cap_type_id
         WHERE lsr_ilr.ilr_id = A_ILR_ID
           AND lsr_ilr.current_revision = A_REVISION
           AND cap_type.description IN ('Sales Type', 'Direct Finance');	
       
	   if l_count > 0 then  
			RTN := F_DERECOGNIZE_CPR_ASSETS(A_ILR_ID, A_REVISION);
			If RTN = -1 then
				return -1;
			end if;
	   end if;

      IF A_SEND_JES THEN
        RTN := F_CREATE_JE_FOR_ILR(A_ILR_ID, A_REVISION);
        If RTN = -1 then
          return -1;
        end if;
      END IF;
      
      return 1;
   exception
      when others then
       PKG_PP_LOG.P_WRITE_MESSAGE(sqlerrm);

         return -1;
   end F_APPROVE_ILR_NO_COMMIT;

   --**************************************************************************
   --                            F_CREATE_JE_FOR_ILR
   --**************************************************************************

   function F_CREATE_JE_FOR_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
   L_MSG varchar2(2000);
   L_RTN number;
   L_GL_POSTING_MO_YR date;
   L_MONTH date;
   L_CONT_COMP_RATE  number;
   L_CURRENCY_FROM  number;
   L_CURRENCY_TO    number;
   L_GL_JE_CODE     VARCHAR2(35);
   L_COMPANY_ID     NUMBER;
   L_LSR_ACCOUNT       LSR_ILR_ACCOUNT%rowtype;
   L_LSR_ILR_SCHEDULE  LSR_ILR_SCHEDULE%rowtype;
   L_AMOUNT         NUMBER(22,2);
   L_FAIR_MARKET_VALUE NUMBER(22,2);
   L_CARRYING_COST 		NUMBER(22,2);
   begin
   
   L_MSG := 'Getting the first month of the schedule';
   select MIN(lis.month)
        into L_MONTH
        from LSR_ILR_SCHEDULE LIS
       where LIS.ILR_ID = A_ILR_ID
          and LIS.REVISION = A_REVISION;
          
   if L_MONTH is null then
       L_MSG := 'Error getting first month of the ILR schedule';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       return -1;
     end if;
          
   L_MSG:='Retrieving L_COMPANY_ID';
   select company_id into L_COMPANY_ID
   from lsr_ilr
   where ilr_id = A_ILR_ID ;

   L_MSG := 'Getting the minimum open month for Lessor';   
     
   select min(gl_posting_mo_yr) 
   into L_GL_POSTING_MO_YR
   from lsr_process_control 
   where open_next is null
   and company_id=L_COMPANY_ID;

     if L_GL_POSTING_MO_YR is null then
       L_MSG := 'Error getting minimum open month for Lessor company';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       return -1;
     end if;

     -- Lessor ILR accounts
      select * into L_LSR_ACCOUNT from LSR_ILR_ACCOUNT where ILR_ID = A_ILR_ID;

      -- L_CURRENCY_FROM
          L_MSG:='Retrieving L_CURRENCY_FROM';
          SELECT contract_currency_id
          INTO L_CURRENCY_FROM
          FROM lsr_lease l, lsr_ilr i
          WHERE l.lease_id = i.lease_id
            AND i.ilr_id = A_ILR_ID;

            -- L_CURRENCY_TO
          L_MSG:='Retrieving L_CURRENCY_TO';
          SELECT currency_id
          INTO L_CURRENCY_TO
          FROM lsr_ilr ilr, currency_schema cs
          WHERE ilr.company_id = cs.company_id
            AND currency_type_id = 1
            AND ilr_id = A_ILR_ID;

          -- L_CONT_COMP_RATE. 
          L_MSG:='Retrieving contract to company currency rate';
          SELECT cont_to_comp_rate 
          INTO L_CONT_COMP_RATE
          FROM (
            SELECT DISTINCT First_Value(rate) OVER (PARTITION BY currency_from, currency_to ORDER BY exchange_date DESC) cont_to_comp_rate
            FROM currency_rate_default
            WHERE trunc(exchange_date) <= L_MONTH
            AND currency_from = L_CURRENCY_FROM
            AND currency_to = L_CURRENCY_TO
               ) ;
               
          IF L_CONT_COMP_RATE IS NULL THEN
            L_MSG := 'Error Retrieving exchange rate for contract to company currencies';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            RETURN -1;
          END IF;
          
          PKG_PP_LOG.P_WRITE_MESSAGE('CONVERSION RATE: ' || TO_CHAR(L_CONT_COMP_RATE));

          L_MSG:='Retrieving L_GL_JE_CODE';
          select gl_je_code
          into L_GL_JE_CODE
          from standard_journal_entries, gl_je_control
          where standard_journal_entries.je_id = gl_je_control.je_id
          and upper(ltrim(rtrim(process_id))) = 'LSR COMMENCEMENT';

          IF L_GL_JE_CODE IS NULL THEN
            L_MSG := 'Error Retrieving L_GL_JE_CODE';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            RETURN -1;
          END IF;
          
      L_MSG:='Retrieving JE Method, Set of books for ILR approval';

      for L_SOBS in
        ( select distinct JM.JE_METHOD_ID        as JE_METHOD_ID,
			JM.AMOUNT_TYPE         as AMOUNT_TYPE,
			JMSOB.SET_OF_BOOKS_ID     as SET_OF_BOOKS_ID,
			JMSOB.REVERSAL_CONVENTION as REVERSAL_CONVENTION,
			LFTS.FASB_CAP_TYPE_ID as FASB_CAP_TYPE_ID,
			LFCT.DESCRIPTION as CAP_TYPE_DESCRIPTION
          from JE_METHOD JM
          inner join JE_METHOD_SET_OF_BOOKS JMSOB ON JMSOB.JE_METHOD_ID = JM.JE_METHOD_ID 
          inner join JE_METHOD_TRANS_TYPE JMTT ON JMTT.JE_METHOD_ID = JM.JE_METHOD_ID
          inner join LSR_FASB_TYPE_SOB LFTS ON LFTS.SET_OF_BOOKS_ID = JMSOB.SET_OF_BOOKS_ID
          INNER JOIN LSR_FASB_CAP_TYPE LFCT ON LFCT.FASB_CAP_TYPE_ID = LFTS.FASB_CAP_TYPE_ID
          INNER JOIN LSR_ILR_OPTIONS LIO ON LIO.LEASE_CAP_TYPE_ID = LFTS.CAP_TYPE_ID
          INNER JOIN LSR_ILR ILR ON ILR.ILR_ID = LIO.ILR_ID
          INNER JOIN COMPANY_JE_METHOD_VIEW CJMV ON CJMV.COMPANY_ID = ILR.COMPANY_ID AND CJMV.JE_METHOD_ID = JM.JE_METHOD_ID
          where ILR.ILR_ID = A_ILR_ID
          and LIO.revision = A_REVISION
          and JMTT.TRANS_TYPE IN 
              ( SELECT DISTINCT JE_METHOD_ID FROM JE_METHOD_TRANS_TYPE 
                WHERE TRANS_TYPE BETWEEN 4001 and 4009 OR TRANS_TYPE = 4055)
		  AND LFCT.DESCRIPTION IN ('Operating', 'Sales Type', 'Direct Finance') )	 					 					 
      loop
        L_MSG:='Retrieving Schedule for the first month';
        select LIS.*
            into L_LSR_ILR_SCHEDULE
            from LSR_ILR_SCHEDULE LIS
            where LIS.ILR_ID = A_ILR_ID
            and LIS.REVISION = A_REVISION
            and LIS.SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
            and LIS.MONTH = L_MONTH;

        L_MSG:='Retrieving carrying cost in company currency';
        select sum(nvl(CARRYING_COST_COMP_CURR,0) ) into L_AMOUNT
        from lsr_asset
        where ilr_id = A_ILR_ID
             and revision = A_REVISION;
						 
		    L_CARRYING_COST:=L_AMOUNT;
		
        L_MSG:='Retrieving fair market value in company currency';
        select sum(nvl(FAIR_MARKET_VALUE_COMP_CURR,0) ) into L_FAIR_MARKET_VALUE
        from lsr_asset
        where ilr_id = A_ILR_ID
             and revision = A_REVISION;		

		--Operating Schedule Specific JE Creations
		IF L_SOBS.CAP_TYPE_DESCRIPTION = 'Operating' THEN

			L_MSG:='Retrieving INITIAL DIRECT COST';
			SELECT NVL(SUM(AMOUNT), 0) INTO L_AMOUNT
			FROM LSR_ILR_INITIAL_DIRECT_COST
			WHERE ILR_ID = A_ILR_ID
			  AND REVISION  = A_REVISION;

			if L_AMOUNT <> 0 then
			
				L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4007, L_AMOUNT,L_LSR_ACCOUNT.INCURRED_COSTS_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
					0, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, L_SOBS.REVERSAL_CONVENTION, L_CONT_COMP_RATE,
					L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);
					if L_RTN = -1 then
						PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
						return -1;
					end if;

				L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4008, L_AMOUNT, L_LSR_ACCOUNT.DEF_COSTS_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
					1, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, L_SOBS.REVERSAL_CONVENTION, L_CONT_COMP_RATE,
					L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);
					if L_RTN = -1 then
						PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
						return -1;
					end if;
				
			end if;					

		--Sales Type and Direct Finance Specific JE Creations
		ELSIF ( L_SOBS.CAP_TYPE_DESCRIPTION = 'Sales Type' OR L_SOBS.CAP_TYPE_DESCRIPTION = 'Direct Finance') then
     
			if l_amount <> 0 then 
				L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4001, L_AMOUNT ,
					L_LSR_ACCOUNT.PROP_PLANT_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
					0, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, L_SOBS.REVERSAL_CONVENTION, 1.0,
					L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);

				if L_RTN = -1 then
				   PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				   return -1;
				end if;
			end if;

			L_AMOUNT := NVL( (L_LSR_ILR_SCHEDULE.BEG_RECEIVABLE - L_LSR_ILR_SCHEDULE.BEG_LT_RECEIVABLE), 0);

			if L_AMOUNT <> 0 then
			   L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4002, L_AMOUNT,
					 L_LSR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
					 1, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, L_SOBS.REVERSAL_CONVENTION, L_CONT_COMP_RATE,
					 L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);

			   if L_RTN = -1 then
				 PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				 return -1;
			   end if;
			end if;

			L_AMOUNT := nvl(L_LSR_ILR_SCHEDULE.BEG_LT_RECEIVABLE, 0);
			if L_AMOUNT <> 0 then
			   L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4003, L_AMOUNT,
					 L_LSR_ACCOUNT.LT_RECEIVABLE_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
					 1, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, L_SOBS.REVERSAL_CONVENTION, L_CONT_COMP_RATE,
					 L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);

				if L_RTN = -1 then
				   return -1;
				   PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				end if;
			end if;

			L_MSG:='Retrieving NPV UNGUARANTEED RESIDUAL';
			SELECT NVL(NPV_UNGUARANTEED_RESIDUAL, 0) INTO L_AMOUNT
			FROM LSR_ILR_AMOUNTS
			WHERE ILR_ID = A_ILR_ID
				 AND REVISION  = A_REVISION
				 AND SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID;

			if L_AMOUNT <> 0 then
			   L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4004, L_AMOUNT,
					 L_LSR_ACCOUNT.UNGUARAN_RES_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
					 1, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, L_SOBS.REVERSAL_CONVENTION, L_CONT_COMP_RATE,
					 L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);

				if L_RTN = -1 then
				  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				   return -1;
				end if;
			end if;

			L_MSG:='Retrieving SELLING PROFIT LOSS';
			SELECT NVL(SELLING_PROFIT_LOSS,0) INTO L_AMOUNT
			FROM LSR_ILR_AMOUNTS
			WHERE ILR_ID = A_ILR_ID
				 AND REVISION  = A_REVISION
				 AND SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID;

			IF L_AMOUNT > 0 AND L_SOBS.FASB_CAP_TYPE_ID = 2 THEN
				 L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4005, L_AMOUNT,
						 L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
						 0, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, L_SOBS.REVERSAL_CONVENTION, L_CONT_COMP_RATE,
						 L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);

				 if L_RTN = -1 then
				   PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				   return -1;
				 end if;
			END IF;
			
			IF L_AMOUNT > 0 AND L_SOBS.FASB_CAP_TYPE_ID = 3 THEN
				 L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4055, L_AMOUNT,
						 L_LSR_ACCOUNT.DEF_SELLING_PROFIT_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
						 0, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, L_SOBS.REVERSAL_CONVENTION, L_CONT_COMP_RATE,
						 L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);

				 if L_RTN = -1 then
				   PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				   return -1;
				 end if;
			END IF;

			IF L_AMOUNT < 0 THEN
			   L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4006, ABS(L_AMOUNT),
					 L_LSR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
					 1, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, L_SOBS.REVERSAL_CONVENTION, L_CONT_COMP_RATE,
					 L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);

			   if L_RTN = -1 then
				 PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				 return -1;
			   end if;
			 END IF;

			L_MSG:='Retrieving INITIAL DIRECT COST';
			SELECT NVL(SUM(AMOUNT), 0) INTO L_AMOUNT
			FROM LSR_ILR_INITIAL_DIRECT_COST
			WHERE ILR_ID = A_ILR_ID
				 AND REVISION  = A_REVISION;

			if L_AMOUNT <> 0 then
				 L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4007, L_AMOUNT,
					L_LSR_ACCOUNT.INCURRED_COSTS_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
					0, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, L_SOBS.REVERSAL_CONVENTION, L_CONT_COMP_RATE,
					L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);
					if L_RTN = -1 then
						PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
						return -1;
					end if;
					
				if L_FAIR_MARKET_VALUE <> L_CARRYING_COST and L_SOBS.FASB_CAP_TYPE_ID = 2 /*Sales type*/ then
					L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(A_ILR_ID, 4008, L_AMOUNT, L_LSR_ACCOUNT.DEF_COSTS_ACCOUNT_ID, 0, L_COMPANY_ID, L_GL_POSTING_MO_YR,
					1, L_GL_JE_CODE, L_SOBS.SET_OF_BOOKS_ID, L_SOBS.JE_METHOD_ID, L_SOBS.AMOUNT_TYPE, L_SOBS.REVERSAL_CONVENTION, L_CONT_COMP_RATE,
					L_CURRENCY_FROM, L_CURRENCY_TO, L_MSG);
					if L_RTN = -1 then
						PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
						return -1;
					end if;
				end if;
			end if;
		
		END IF;

      end loop;

     return 1;
     exception
       when others then
         ROLLBACK;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving ILR: ' || L_MSG);
         return -1;
   end F_CREATE_JE_FOR_ILR;

   --**************************************************************************
   --                            F_APPROVE_ILR
   --**************************************************************************
   function F_APPROVE_ILR(A_ILR_ID   in number,
                          A_REVISION IN NUMBER,
                          A_SEND_JES IN BOOLEAN DEFAULT TRUE) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      MY_STR   varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      -- start by clearing our pend transaction
      RTN := F_APPROVE_ILR_NO_COMMIT( a_ilr_id => A_ILR_ID, 
                                      a_revision => A_REVISION, 
                                      a_status => L_STATUS, 
                                      a_send_jes => A_SEND_JES);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending For Approval: ' || L_STATUS);
         return -1;
      end if;

      commit;
      return 1;
   exception
      when others then
         rollback;
         P_LOGERRORMESSAGE(A_ILR_ID, L_STATUS);
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving ILR: ' || L_STATUS);
         return -1;
   end F_APPROVE_ILR;


   --**************************************************************************
   --                            F_REJECT_ILR
   --**************************************************************************

   function F_REJECT_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin

      update LSR_ILR_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_REJECT_ILR;

   --**************************************************************************
   --                            F_SEND_ILR
   -- Called when a user clicks send for approval (on an ILR)
   -- Need to mark the approval status to be sent.  And update the ILR to
   -- pending approval.  IN addition need to update the capitalized cost to
   -- be the beginning obligation on the asset schedule for the revision being
   -- sent for approval
   --**************************************************************************
   function F_SEND_ILR_NO_COMMIT(A_ILR_ID   in number,
                                 A_REVISION in number,
                                 A_STATUS   OUT VARCHAR2) return number is
      RTN     number;
      ILR     number;
      WFS     number;
      SQLS    varchar2(32000);
      IS_AUTO WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
      APPROVED_DATE date;
      START_DATE date;
	  GL_POSTING_MO_YR date;
      v_component_count number;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_STATUS := 'SEND for approval:';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   ILR_ID: ' || TO_CHAR(A_ILR_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   REVISION: ' || TO_CHAR(A_REVISION);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

     --Check that the lease is valid ie in Open/New Revision status
	 -- /* WMD Have to be able to create new revisions on "Closed" leases
	 -- since new revisions are created for retirements and transfers */
     select count(1)
     into RTN
     from LSR_LEASE LL, LSR_ILR LI
     where LI.ILR_ID = A_ILR_ID
     and LI.LEASE_ID = LL.LEASE_ID
     and (LL.LEASE_STATUS_ID in (3,7)
	 or (LL.LEASE_STATUS_ID = 5
           and A_REVISION <> 1));

     if RTN <> 1 then
      A_STATUS := 'MLA for this ILR is not in Open or New Revision status, so the ILR cannot be posted to the MLA.';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      return -1;
     end if;

      A_STATUS := 'Getting start date';
      select min(PAYMENT_TERM_DATE)
        into START_DATE
      from LSR_ILR_PAYMENT_TERM
        where ILR_ID = A_ILR_ID
          and REVISION = A_REVISION;

      A_STATUS := 'Getting approved date';
      select min(PAYMENT_TERM_DATE)
        into APPROVED_DATE
      from lsr_ilr_payment_term pt, lsr_ilr_approval appr
      where pt.ilr_id = appr.ilr_id
      and pt.revision = appr.revision
      and appr.ilr_id = A_ILR_ID
      and appr.revision = (
         select max(revision)
         from lsr_ilr_approval
         where ilr_id = appr.ilr_id
           and approval_status_id in (3,6)
           and revision <> A_REVISION
      );

      PKG_PP_LOG.P_WRITE_MESSAGE('   Start Date: ' || to_char(START_DATE, 'YYYY-MM-DD'));
      PKG_PP_LOG.P_WRITE_MESSAGE('   Approved Date: ' || to_char(APPROVED_DATE, 'YYYY-MM-DD'));

	   --  Can change the start date as long as the prior start date is not yet been reached
      if APPROVED_DATE <> START_DATE and trunc(APPROVED_DATE, 'mm') < trunc(sysdate, 'mm') and APPROVED_DATE is not null then
        A_STATUS := 'Cannot change the in service date of the ILR after the in service date has been reached';
        PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

        return -1;
      end if;


      A_STATUS := 'updating LSR_ILR_APPROVAL';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LSR_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      --Update status of non-current revisions
      A_STATUS := 'update workflow';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update WORKFLOW
         set APPROVAL_STATUS_ID = 5
       where ID_FIELD1 = A_ILR_ID
         and ID_FIELD2 <> A_REVISION
         and APPROVAL_STATUS_ID = 2
         and Lower(SUBSYSTEM) = 'lessor_ilr_approval';

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'update ls_ilr_approval';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LSR_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 5
       where ILR_ID = A_ILR_ID
         and REVISION <> A_REVISION
         and APPROVAL_STATUS_ID = 2;

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');

      return 1;
   exception
      when others then
         PKG_PP_LOG.P_WRITE_MESSAGE('Error Sending For Approval: ' || sqlerrm);

         return -1;
   end F_SEND_ILR_NO_COMMIT;


   function F_SEND_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL
      RTN      number;
      L_STATUS varchar2(2000);
   begin
      -- start by clearing our pend transaction
      RTN := F_SEND_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending For Approval: ' || L_STATUS);
         return -1;
      end if;

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_SEND_ILR;

   --**************************************************************************
   --                            F_UNSEND_ILR
   --**************************************************************************

   function F_UNSEND_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin

      update LSR_ILR_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_UNSEND_ILR;

   --**************************************************************************
   --                            F_UNREJECT_ILR
   --**************************************************************************

   function F_UNREJECT_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_UNREJECT_ILR;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_ILR
   --**************************************************************************

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_ILR_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_ILR_ID)
                                        and ID_FIELD2 = TO_CHAR(A_REVISION)
                                        and Lower(SUBSYSTEM) = 'lessor_ilr_approval'),
                                     0)
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_UPDATE_WORKFLOW_ILR;

   --**************************************************************************
   --                            F_APPROVE_MLA
   --**************************************************************************

   function F_APPROVE_MLA(A_LEASE_ID in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this revision
      update LSR_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L
         set LEASE_STATUS_ID = 3, APPROVAL_DATE = sysdate, CURRENT_REVISION = A_REVISION
       where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving MLA');
         return -1;
   end F_APPROVE_MLA;

   --**************************************************************************
   --                            F_REJECT_MLA
   --**************************************************************************

   function F_REJECT_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_LEASE_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L set LEASE_STATUS_ID = 4 where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Rejecting MLA');
         return -1;
   end F_REJECT_MLA;

   --**************************************************************************
   --                            F_SEND_MLA
   --**************************************************************************

   function F_SEND_MLA(A_LEASE_ID in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN     number;
      IS_AUTO WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
   begin

      update LSR_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L set LEASE_STATUS_ID = 2 where L.LEASE_ID = A_LEASE_ID;

      --Reject other revisions
      update LSR_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 5, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) <> NVL(A_REVISION, 0)
         and APPROVAL_STATUS_ID in (1, 2);

      --Call F_APPROVE_MLA for auto-approved assets
      commit;

      select NVL(EXTERNAL_WORKFLOW_TYPE, 'NA')
        into IS_AUTO
        from WORKFLOW_TYPE A, LSR_LEASE L
       where A.WORKFLOW_TYPE_ID = L.WORKFLOW_TYPE_ID
         and L.LEASE_ID = A_LEASE_ID;

      if IS_AUTO = 'AUTO' then
         return F_APPROVE_MLA(A_LEASE_ID, A_REVISION);
      end if;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending MLA');
         return -1;
   end F_SEND_MLA;

   --**************************************************************************
   --                            F_UNREJECT_MLA
   --**************************************************************************

   function F_UNREJECT_MLA(A_LEASE_ID in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_LEASE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 7
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L set LEASE_STATUS_ID = 2 where L.LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Unrejecting MLA');
         return -1;
   end F_UNREJECT_MLA;

   --**************************************************************************
   --                            F_UNSEND_MLA
   --**************************************************************************

   function F_UNSEND_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_LEASE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L set LEASE_STATUS_ID = 1, APPROVAL_DATE = null where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error UnSending MLA');
         return -1;
   end F_UNSEND_MLA;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_MLA
   --**************************************************************************

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LSR_LEASE_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_LEASE_ID)
                                        and ID_FIELD2 = TO_CHAR(A_REVISION)
                                        and SUBSYSTEM = 'lessor_mla_approval'),
                                     0)
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Updating Workflow MLA');
         return -1;
   end F_UPDATE_WORKFLOW_MLA;

   --**************************************************************************
   --                            F_INVOICE_APPROVE
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will approve Lessor Invoices
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_INVOICE_APPROVE(A_COMPANY_ID in number,
                              A_MONTH      in date,
                              A_END_LOG    in number:=null) return varchar2 is
      L_STATUS varchar2(30000);
	  L_LOCATION varchar2(30000);
      L_RTN    number;
      L_COUNTER number;
      L_GL_JE_CODE   varchar2(35);
	  BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
	  COUNTER number;
	  TAX_PAYMENT_TYPE number;
	  L_ACCTS PKG_LEASE_BUCKET.BUCKET_ACCTS;
   begin
--    PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
--  PKG_PP_LOG.P_WRITE_MESSAGE('Begin Payment Journals Process - Company ID: ' || a_company_id || ' - Month: ' || to_char(a_month, 'mon-yyyy'));

--  /* CJS 3/16/15 Adding check to make sure everything is approved */
--  select count(1)
--  into L_COUNTER
--  from LSR_INVOICE LI, LSR_INVOICE_APPROVAL LIA
--  where LPH.PAYMENT_ID = LIA.INVOICE_ID
--  and LPH.COMPANY_ID = A_COMPANY_ID
--  and LPH.GL_POSTING_MO_YR = A_MONTH
--  and LPA.APPROVAL_STATUS_ID <> 3
--  and APPROVAL_TYPE_ID <> (select WORKFLOW_TYPE_ID from WORKFLOW_TYPE where lower(description) = 'auto approve');

--  if L_COUNTER <> 0 then
--     PKG_PP_LOG.P_WRITE_MESSAGE('Payments exist that need to be approved for this company and month');
--     PKG_PP_LOG.P_END_LOG();
--     return 'Payments exist that need to be approved for this company and month';
--  end if;

--  PKG_PP_LOG.P_WRITE_MESSAGE('Sending Interim Interest Journals');
--  L_STATUS := F_PAYMENT_II(A_COMPANY_ID, A_MONTH);
--  if L_STATUS <> 'OK' THEN
--    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
--    PKG_PP_LOG.P_END_LOG();
--    return L_RTN;
--  end if;

--     select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LAMPAY'))
--      into L_GL_JE_CODE
--      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
--      where E.JE_ID = G.JE_ID
--      and G.PROCESS_ID = 'LAMPAY';


--    PKG_PP_LOG.P_WRITE_MESSAGE('Principal and Interest Journals');
--      for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
--                                H.VENDOR_ID as VENDOR_ID,
--                                /* WMD ADJUSTMENT */
--                                CASE WHEN L.payment_type_id = 2 THEN nvl(ii.ii_amount,0) ELSE 0 END +
--                                NVL(L.AMOUNT,0) + NVL(L.ADJUSTMENT_AMOUNT,0) as PAYMENT_AMOUNT, --mc_bookje will reconvert based on rate
--                                A.WORK_ORDER_ID as WORK_ORDER_ID,
--                                A.COMPANY_ID as COMPANY_ID,
--                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
--                                M.INVOICE_ID as INVOICE_ID,
--                                case
--                                   when L.PAYMENT_TYPE_ID = 1 then
--                                    3018
--                                   when L.PAYMENT_TYPE_ID in (2) then
--                                    3019
--                                end as TRANS_TYPE,
--                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
--                                I.INT_ACCRUAL_ACCOUNT_ID as INT_ACCRUAL_ACCOUNT,
--                I.ap_account_id AS ap_account_id,
--                                h.rate,
--                                h.company_currency_id,
--                                H.contract_currency_id
--                           from V_LS_PAYMENT_HDR_FX H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M, LS_ILR_ACCOUNT I, LS_PAYMENT_APPROVAL LPA,
--                                (select la.ls_asset_id, -sum(nvl(ii.amount,0)) as ii_amount
--                                from LS_COMPONENT_MONTHLY_II_STG II, ls_asset la, ls_component lc
--                                where la.ls_asset_id = lc.ls_asset_id
--                                  and lc.component_id = ii.component_id
--                                  and la.company_id = A_COMPANY_ID
--                                  and II.MONTH = A_MONTH
--                                group by la.ls_asset_id ) II
--                          where H.GL_POSTING_MO_YR =  A_MONTH
--                            and H.PAYMENT_ID = L.PAYMENT_ID
--                            and H.PAYMENT_ID = M.PAYMENT_ID
--                            and case when L.PAYMENT_TYPE_ID = 2 then NVL(II.II_AMOUNT,0) else 0 end +
--                                NVL(L.AMOUNT,0) + NVL(L.ADJUSTMENT_AMOUNT,0) <> 0

--                            and L.LS_ASSET_ID = A.LS_ASSET_ID
--                            and A.ILR_ID = I.ILR_ID
--                            and A.COMPANY_ID = A_COMPANY_ID
--                            and L.PAYMENT_TYPE_ID in (1,2)
--                            and L.LS_ASSET_ID = II.LS_ASSET_ID (+)
--                     AND H.payment_id = lpa.payment_id
--                     AND H.ls_cur_type = 2
--                     --and nvl(LPA.REJECTED,0) <> 1
--                     and (LPA.APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select /* CJS Fix APPROVAL_TYPE_ID */WORKFLOW_TYPE_ID
--                                                               from WORKFLOW_TYPE
--                                                               where lower(description) = 'auto approve')
--                        and LPA.APPROVAL_STATUS_ID  = 1)))

--      loop
--         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
--                     to_char(l_payments.trans_type);
--         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
--                                               L_PAYMENTS.TRANS_TYPE,
--                                               L_PAYMENTS.PAYMENT_AMOUNT,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.WORK_ORDER_ID,
--                                               case
--                           when L_PAYMENTS.TRANS_TYPE in (3018) then
--                            -1
--                           when L_PAYMENTS.TRANS_TYPE = 3019 then
--                            L_PAYMENTS.INT_ACCRUAL_ACCOUNT
--                        end,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.COMPANY_ID,
--                                               A_MONTH,
--                                               1,
--                                               L_GL_JE_CODE,
--                                               L_PAYMENTS.SET_OF_BOOKS_ID,
--                                               l_payments.rate,
--                                               l_payments.contract_currency_id,
--                                               l_payments.company_currency_id,
--                                               to_char(l_payments.invoice_id),
--                                               L_STATUS);
--         if L_RTN = -1 then
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
--            PKG_PP_LOG.P_END_LOG();
--            return L_LOCATION || ' : ' || L_STATUS;
--         end if;

--         -- process the credit.  Payment credits all hit 3022
--         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
--                     ' trans type: 3022';
--         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
--                                               3022,
--                                               L_PAYMENTS.PAYMENT_AMOUNT,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.WORK_ORDER_ID,
--                                               L_PAYMENTS.AP_ACCOUNT_ID,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.COMPANY_ID,
--                                               A_MONTH,
--                                               0,
--                                               L_GL_JE_CODE,
--                                               L_PAYMENTS.SET_OF_BOOKS_ID,
--                                               l_payments.rate,
--                                               l_payments.contract_currency_id,
--                                               l_payments.company_currency_id,
--                                               to_char(l_payments.invoice_id),
--                                               L_STATUS);
--         if L_RTN = -1 then
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
--            PKG_PP_LOG.P_END_LOG();
--            return L_LOCATION || ' : ' || L_STATUS;
--         end if;
--       L_COUNTER := L_COUNTER + 1;
--      end loop;

--    --TAXES
--    --get the tax bucket
--    L_STATUS := 'Insert tax amounts on schedule';
--      select count(*)
--      into COUNTER
--      from LS_RENT_BUCKET_ADMIN
--      where TAX_EXPENSE_BUCKET =1;

--      if COUNTER <> 1 then
--         return 'Error: there must be exactly one bucket defined as the tax expense bucket';
--      end if;

--      select *
--      into BUCKET
--      from LS_RENT_BUCKET_ADMIN
--      where TAX_EXPENSE_BUCKET = 1;

--    TAX_PAYMENT_TYPE := PKG_LEASE_BUCKET.F_GET_TAX_TYPE(BUCKET.RENT_TYPE,BUCKET.BUCKET_NUMBER);

--    PKG_PP_LOG.P_WRITE_MESSAGE('Tax Payment Journals');
--    for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
--                                H.VENDOR_ID as VENDOR_ID,
--                                /* WMD ADJUSTMENT */
--                                NVL(LMT.AMOUNT,0) + NVL(LMT.ADJUSTMENT_AMOUNT,0) as PAYMENT_AMOUNT, --Note that this is off the monthly tax table and not payment lines
--                                A.WORK_ORDER_ID as WORK_ORDER_ID,
--                                A.COMPANY_ID as COMPANY_ID,
--                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
--                                M.INVOICE_ID as INVOICE_ID,
--                                3045 as TRANS_TYPE,
--                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
--                                LTL.ACCRUAL_ACCT_ID as ACCRUAL_ACCOUNT,
--                LTL.EXPENSE_ACCT_ID as EXPENSE_ACCOUNT,
--                LTL.AP_ACCT_ID as AP_ACCOUNT,
--                ltl.tax_local_id AS tax_local_id,
--                                H.rate,
--                                H.contract_currency_id,
--                                h.company_currency_id
--                           from V_LS_PAYMENT_HDR_FX H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M, LS_ILR_ACCOUNT I,
--                LS_PAYMENT_APPROVAL LPA, LS_TAX_LOCAL LTL, LS_MONTHLY_TAX LMT
--                          where H.GL_POSTING_MO_YR = A_MONTH
--                            and H.PAYMENT_ID = L.PAYMENT_ID
--                            and H.PAYMENT_ID = M.PAYMENT_ID
--              and h.vendor_id = decode(lmt.vendor_id, -1, h.vendor_id, lmt.vendor_id)
--                            and NVL(LMT.AMOUNT,0) + NVL(LMT.ADJUSTMENT_AMOUNT,0) <> 0 /* WMD */
--                            and L.LS_ASSET_ID = A.LS_ASSET_ID
--                            and A.ILR_ID = I.ILR_ID
--                            and A.COMPANY_ID = A_COMPANY_ID
--              and L.PAYMENT_TYPE_ID = TAX_PAYMENT_TYPE
--              and LTL.TAX_LOCAL_ID = LMT.TAX_LOCAL_ID
--              and LMT.ACCRUAL = 0
--              and LMT.LS_ASSET_ID = L.LS_ASSET_ID
--              and LMT.GL_POSTING_MO_YR = L.GL_POSTING_MO_YR
--              and LMT.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
--              AND H.payment_id = lpa.payment_id
--              AND H.ls_cur_type = 2
--              --and nvl(LPA.REJECTED,0) <> 1
--              and (LPA.APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select /* CJS Fix APPROVAL_TYPE_ID */WORKFLOW_TYPE_ID
--                                     from WORKFLOW_TYPE
--                                     where lower(description) = 'auto approve')
--                              and LPA.APPROVAL_STATUS_ID  = 1)))
--      loop
--         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
--                     to_char(l_payments.trans_type);
--         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
--                                               L_PAYMENTS.TRANS_TYPE,
--                                               L_PAYMENTS.PAYMENT_AMOUNT,
--                                               L_PAYMENTS.TAX_LOCAL_ID,
--                                               -1,
--                                               L_PAYMENTS.WORK_ORDER_ID,
--                                               L_PAYMENTS.ACCRUAL_ACCOUNT,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.COMPANY_ID,
--                                               A_MONTH,
--                                               1,
--                                               L_GL_JE_CODE,
--                                               L_PAYMENTS.SET_OF_BOOKS_ID,
--                                               l_payments.rate,
--                                               l_payments.contract_currency_id,
--                                               l_payments.company_currency_id,
--                                               to_char(l_payments.invoice_id),
--                                               L_STATUS);
--         if L_RTN = -1 then
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
--            PKG_PP_LOG.P_END_LOG();
--            return L_LOCATION || ' : ' || L_STATUS;
--         end if;

--         -- process the credit.  Payment credits all hit 3046
--         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
--                     ' trans type: 3046';
--         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
--                                               L_PAYMENTS.TRANS_TYPE + 1,
--                                               L_PAYMENTS.PAYMENT_AMOUNT,
--                                               L_PAYMENTS.TAX_LOCAL_ID,
--                                               -1,
--                                               L_PAYMENTS.WORK_ORDER_ID,
--                                               L_PAYMENTS.AP_ACCOUNT,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.COMPANY_ID,
--                                               A_MONTH,
--                                               0,
--                                               L_GL_JE_CODE,
--                                               L_PAYMENTS.SET_OF_BOOKS_ID,
--                                               l_payments.rate,
--                                               l_payments.contract_currency_id,
--                                               l_payments.company_currency_id,
--                                               to_char(l_payments.invoice_id),
--                                               L_STATUS);
--         if L_RTN = -1 then
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
--            PKG_PP_LOG.P_END_LOG();
--            return L_LOCATION || ' : ' || L_STATUS;
--         end if;
--       L_COUNTER := L_COUNTER + 1;
--      end loop;

--    L_ACCTS := PKG_LEASE_BUCKET.F_GET_ACCTS;

--    PKG_PP_LOG.P_WRITE_MESSAGE('Executory and Contingent Journals');
--    for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
--                                H.VENDOR_ID as VENDOR_ID,
--                                /* WMD */
--                                NVL(L.AMOUNT,0) + NVL(L.ADJUSTMENT_AMOUNT,0) as PAYMENT_AMOUNT,
--                                A.WORK_ORDER_ID as WORK_ORDER_ID,
--                                A.COMPANY_ID as COMPANY_ID,
--                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
--                                M.INVOICE_ID as INVOICE_ID,
--                                case
--                                   when L.PAYMENT_TYPE_ID between 3 and 12 then
--                                    3020
--                                   when L.PAYMENT_TYPE_ID between 13 and 22 then
--                                    3021
--                                end as TRANS_TYPE,
--                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
--                I.ap_account_id AS ap_account_id,
--                L.payment_type_id AS payment_type_id,
--                                h.rate,
--                                h.contract_currency_id,
--                                h.company_currency_id
--                           from V_LS_PAYMENT_HDR_FX H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M, LS_ILR_ACCOUNT I,
--                LS_PAYMENT_APPROVAL LPA
--                          where H.GL_POSTING_MO_YR = A_MONTH
--                            and H.PAYMENT_ID = L.PAYMENT_ID
--                            and H.PAYMENT_ID = M.PAYMENT_ID
--                            and NVL(L.AMOUNT,0) + NVL(L.ADJUSTMENT_AMOUNT,0) <> 0 /* WMD */
--                            and L.LS_ASSET_ID = A.LS_ASSET_ID
--                            and A.ILR_ID = I.ILR_ID
--                            and A.COMPANY_ID = A_COMPANY_ID
--              AND L.payment_type_id BETWEEN 3 AND 22
--              AND H.ls_cur_type = 2
--                     and H.PAYMENT_ID = LPA.PAYMENT_ID
--                     --and nvl(LPA.REJECTED,0) <> 1
--                     and (LPA.APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select /* CJS Fix APPROVAL_TYPE_ID */WORKFLOW_TYPE_ID
--                                                               from WORKFLOW_TYPE
--                                                               where lower(description) = 'auto approve')
--                        and LPA.APPROVAL_STATUS_ID  = 1))
--           )
--      loop

--     --skip the taxes since we already did them
--     if L_PAYMENTS.PAYMENT_TYPE_ID = TAX_PAYMENT_TYPE then
--      continue;
--     end if;

--         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
--                     to_char(l_payments.trans_type);
--         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
--                                               L_PAYMENTS.TRANS_TYPE,
--                                               L_PAYMENTS.PAYMENT_AMOUNT,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.WORK_ORDER_ID,
--                                               L_ACCTS(L_PAYMENTS.PAYMENT_TYPE_ID).ACCRUAL_ACCT_ID,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.COMPANY_ID,
--                                               A_MONTH,
--                                               1,
--                                               L_GL_JE_CODE,
--                                               l_payments.set_of_books_id,
--                                               l_payments.rate,
--                                               l_payments.contract_currency_id,
--                                               l_payments.company_currency_id,
--                                               to_char(l_payments.invoice_id),
--                                               L_STATUS);
--         if L_RTN = -1 then
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
--            PKG_PP_LOG.P_END_LOG();
--            return L_LOCATION || ' : ' || L_STATUS||' '||sqlerrm;
--         end if;

--         -- process the credit.  Payment credits all hit 3022
--         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
--                     ' trans type: 3022';
--         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
--                                               3022,
--                                               L_PAYMENTS.PAYMENT_AMOUNT,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.WORK_ORDER_ID,
--                                               L_PAYMENTS.AP_ACCOUNT_ID,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.COMPANY_ID,
--                                               A_MONTH,
--                                               0,
--                                               L_GL_JE_CODE,
--                                               L_PAYMENTS.SET_OF_BOOKS_ID,
--                                               l_payments.rate,
--                                               l_payments.contract_currency_id,
--                                               l_payments.company_currency_id,
--                                               to_char(l_payments.invoice_id),
--                                               L_STATUS);
--         if L_RTN = -1 then
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
--            PKG_PP_LOG.P_END_LOG();
--            return L_LOCATION || ' : ' || L_STATUS;
--         end if;
--       L_COUNTER := L_COUNTER + 1;
--      end loop;

--     if L_COUNTER > 0 then
--      PKG_PP_LOG.P_WRITE_MESSAGE(L_COUNTER||' JE pairs sent.');
--     end if;

--     L_LOCATION:='Updating auto approvals to approved - auto status';
--     update ls_payment_hdr
--    set payment_status_id = 6
--    where gl_posting_mo_yr = A_MONTH
--      and lease_id in (select lease_id from ls_lease where lease_group_id in
--      (select lease_group_id from ls_lease_group where nvl(pymt_approval_flag,0) = 0))
--      and company_id = A_COMPANY_ID;


--    if A_END_LOG=1 THEN
--        PKG_PP_LOG.P_END_LOG();
--    end if;

      return 'OK';
----     else
----      PKG_PP_LOG.P_WRITE_MESSAGE('No JEs sent. Please confirm that payments have been approved.');
----      PKG_PP_LOG.P_END_LOG();
----      return 'No JEs sent. Please confirm that payments have been approved.';
----     end if;

--   exception
--      when others then
--         return L_STATUS || ' : ' || sqlerrm;
   end F_INVOICE_APPROVE;

   --**************************************************************************
   --                            F_SEND_INVOICE
   --**************************************************************************

   function F_SEND_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   BEGIN

      update LSR_INVOICE_APPROVAL set APPROVAL_STATUS_ID = 2 where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending Invoice');
         return -1;
   end F_SEND_INVOICE;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_INVOICE
   --**************************************************************************

   function F_UPDATE_WORKFLOW_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LSR_INVOICE_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_INVOICE_ID)
                                        and SUBSYSTEM = 'lsr_invoice_approval'),
                                     0)
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_UPDATE_WORKFLOW_INVOICE;

   --**************************************************************************
   --                            F_REJECT_INVOICE
   --**************************************************************************

   function F_REJECT_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_INVOICE_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Rejecting Invoice');
         return -1;
   end F_REJECT_INVOICE;

   --**************************************************************************
   --                            F_UNREJECT_INVOICE
   --**************************************************************************

   function F_UNREJECT_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_INVOICE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 7
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Unrejecting Invoice');
         return -1;
   end F_UNREJECT_INVOICE;

   --**************************************************************************
   --                            F_APPROVE_INVOICE
   --**************************************************************************

   function F_APPROVE_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this LSR_INVOICE_APPROVAL
      update LSR_INVOICE_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving Invoice');
         return -1;
   end F_APPROVE_INVOICE;

   --**************************************************************************
   --                            F_UNSEND_INVOICE
   --**************************************************************************

   function F_UNSEND_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_INVOICE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error UnSending Invoice');
         return -1;
   end F_UNSEND_INVOICE;
   
   PROCEDURE p_mass_approve_ilrs( a_ilrs t_lsr_ilr_id_revision_tab,
                                  a_send_jes_0_1 number) IS
    l_ret NUMBER;
    l_send_jes boolean;
   BEGIN
    DELETE FROM lsr_ilr_approval_kickouts
    WHERE t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs;
   
    IF a_send_jes_0_1 = 0 THEN
      l_send_jes := false;
    ELSE
      l_send_jes := true;
    END IF;
   
    pkg_lessor_schedule.p_process_ilrs(a_ilrs);
    
    --Approval runs in autonomous transaction. We want the schedule results committed
    --in order to match what is sent to approval
    commit;
    
    
    FOR ilr IN (SELECT ilr_id, revision
                FROM TABLE(a_ilrs)
                WHERE (ilr_id, revision) NOT IN ( SELECT ilr_id, revision
                                                  FROM lsr_ilr_schedule_kickouts))
    LOOP
      BEGIN
        l_ret := pkg_lessor_approval.f_send_ilr(ilr.ilr_id, ilr.revision);
        IF l_ret <> 1 THEN
          raise_application_error(-20000, 'Error sending ILR for approval');
        END IF;
        
        l_ret := pkg_lessor_approval.f_approve_ilr(ilr.ilr_id, ilr.revision, l_send_jes);
        IF l_ret <> 1 THEN
          raise_application_error(-20000, 'Error approving ILR');
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          DECLARE
            l_kickout t_kickout;
          BEGIN
            l_kickout.ilr_id := ilr.ilr_id;
            l_kickout.revision := ilr.revision;
            l_kickout.message := 'Error processing approval: ' || sqlerrm;
            l_kickout.occurrence_id := pkg_pp_log.g_occurrence_id;
            p_log_kickouts(t_kickout_tab(l_kickout));
          END;
          CONTINUE;
      END;
      
    END LOOP;
   END;

END pkg_lessor_approval;
/

--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (4513, 0, 2017, 3, 0, 0, 0, 'C:\PlasticWks\powerplant\sql\packages', '2017.3.0.0_pkg_lessor_approval.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
