CREATE OR REPLACE PACKAGE pkg_lessor_calc AS
	/*
	||============================================================================
	|| Application: PowerPlant
	|| Object Name: PKG_LESSOR_CALC
	|| Description:
	||============================================================================
	|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved
	||============================================================================
	|| Version    Date       Revised By      Reason For Change
	|| ---------- ---------- --------------  -------------------------------------
	|| 2017.1.0.0 11/01/2017 C.Shilling		 Create package for month-end processes
	||============================================================================
	*/
	TYPE currency_gain_loss_rec IS RECORD(
		company_id                    	company_setup.company_id%TYPE,
		ilr_id                        	lsr_ilr.ilr_id%TYPE,
		ilr_number                    	lsr_ilr.ilr_number%TYPE,
		iso_code                      	currency.iso_code%TYPE,
		currency_display_symbol       	currency.currency_display_symbol%TYPE,
		gain_loss_fx                  	v_lsr_ilr_mc_schedule.gain_loss_fx%TYPE,
		MONTH                         	v_lsr_ilr_mc_schedule.month%TYPE,
		curr_gain_loss_acct_id 			lsr_ilr_account.curr_gain_loss_acct_id%TYPE,
		curr_gain_loss_offset_acct_id 	lsr_ilr_account.curr_gain_loss_offset_acct_id%TYPE
	);
	TYPE currency_gain_loss_tbl IS TABLE OF currency_gain_loss_rec;

	PROCEDURE p_variable_payments_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_calc_type  IN VARCHAR2);

	PROCEDURE p_accruals_calc(
			a_company_id  NUMBER,
			a_month       DATE,
			a_end_log     NUMBER:=NULL);

	PROCEDURE p_accruals_approve(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log    IN NUMBER:=NULL)
	;

	PROCEDURE p_invoice_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log    IN NUMBER:=NULL)
	;

	PROCEDURE p_invoice_approve(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log    IN NUMBER:=NULL)
	;

	PROCEDURE p_auto_termination_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
            a_end_log    IN NUMBER:=NULL)
	;

	PROCEDURE p_auto_termination_approve(
			a_company_id IN NUMBER,
        	a_month      IN DATE,
        	a_end_log    IN NUMBER:=NULL)
	;

	PROCEDURE p_lsr_closed(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log IN NUMBER:=NULL)
	;

	FUNCTION f_currency_gain_loss(
			a_company_ids     IN t_num_array,
			a_month           IN DATE,
			a_set_of_books_id IN NUMBER)
		RETURN currency_gain_loss_tbl
		PIPELINED;

	PROCEDURE p_currency_gain_loss_approve(
			a_company_id      IN NUMBER,
			a_month           IN DATE,
			a_set_of_books_id IN NUMBER,
			a_end_log         IN NUMBER := NULL)
	;

END pkg_lessor_calc;
/

CREATE OR REPLACE PACKAGE BODY pkg_lessor_calc AS
	--**************************************************************************
	--                            p_variable_payments_calc
	--             --------------------------------
	-- @@ description
	--    this package will create a pseduo asset schedule by allocating ILR amounts
	--    to the asset level and then callthe variable payments calculation.
	-- @@params
	--    company: a_company_id
	--       the company to process accruals for
	--    date: a_month
	--       the month to process accruals for
	--    calc type: a_calc_type
	--       'invoices' or 'accruals'
	--
	--**************************************************************************
	PROCEDURE p_variable_payments_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_calc_type  IN VARCHAR2)
	IS
		rtn_str					VARCHAR2(2000);
	BEGIN
		----------------------------------------------------------
		--Populate lessor asset temp table
		----------------------------------------------------------
		DELETE FROM lsr_asset_schedule_tmp;

		INSERT INTO lsr_asset_schedule_tmp(
			lsr_asset_id,
			revision,
			set_of_books_id,
			MONTH,
			interest_income_received,
			interest_income_accrued,
			interest_rental_recvd_spread,
			beg_deferred_rev,
			deferred_rev_activity,
			end_deferred_rev,
			beg_receivable,
			end_receivable,
			executory_accrual1,
			executory_accrual2,
			executory_accrual3,
			executory_accrual4,
			executory_accrual5,
			executory_accrual6,
			executory_accrual7,
			executory_accrual8,
			executory_accrual9,
			executory_accrual10,
			executory_paid1,
			executory_paid2,
			executory_paid3,
			executory_paid4,
			executory_paid5,
			executory_paid6,
			executory_paid7,
			executory_paid8,
			executory_paid9,
			executory_paid10,
			contingent_accrual1,
			contingent_accrual2,
			contingent_accrual3,
			contingent_accrual4,
			contingent_accrual5,
			contingent_accrual6,
			contingent_accrual7,
			contingent_accrual8,
			contingent_accrual9,
			contingent_accrual10,
			contingent_paid1,
			contingent_paid2,
			contingent_paid3,
			contingent_paid4,
			contingent_paid5,
			contingent_paid6,
			contingent_paid7,
			contingent_paid8,
			contingent_paid9,
			contingent_paid10
		)
		SELECT sch.lsr_asset_id,
			sch.revision,
			sch.set_of_books_id,
			sch.MONTH,
			sch.interest_income_received,
			sch.interest_income_accrued,
			sch.interest_rental_recvd_spread,
			sch.beg_deferred_rev,
			sch.deferred_rev_activity,
			sch.end_deferred_rev,
			sch.beg_receivable,
			sch.end_receivable,
			sch.executory_accrual1,
			sch.executory_accrual2,
			sch.executory_accrual3,
			sch.executory_accrual4,
			sch.executory_accrual5,
			sch.executory_accrual6,
			sch.executory_accrual7,
			sch.executory_accrual8,
			sch.executory_accrual9,
			sch.executory_accrual10,
			sch.executory_paid1,
			sch.executory_paid2,
			sch.executory_paid3,
			sch.executory_paid4,
			sch.executory_paid5,
			sch.executory_paid6,
			sch.executory_paid7,
			sch.executory_paid8,
			sch.executory_paid9,
			sch.executory_paid10,
			sch.contingent_accrual1,
			sch.contingent_accrual2,
			sch.contingent_accrual3,
			sch.contingent_accrual4,
			sch.contingent_accrual5,
			sch.contingent_accrual6,
			sch.contingent_accrual7,
			sch.contingent_accrual8,
			sch.contingent_accrual9,
			sch.contingent_accrual10,
			sch.contingent_paid1,
			sch.contingent_paid2,
			sch.contingent_paid3,
			sch.contingent_paid4,
			sch.contingent_paid5,
			sch.contingent_paid6,
			sch.contingent_paid7,
			sch.contingent_paid8,
			sch.contingent_paid9,
			sch.contingent_paid10
		FROM v_lsr_pseudo_asset_schedule sch
		INNER JOIN lsr_asset la
			ON la.lsr_asset_id = sch.lsr_asset_id
			AND sch.revision = la.revision
		INNER JOIN lsr_ilr ilr
			ON ilr.ilr_id = la.ilr_id
			AND ilr.current_revision = sch.revision
		WHERE ilr.company_id = A_COMPANY_ID
		AND ilr.ilr_status_id IN (2,3) -- 2 = in-service; 3 = retired
		;

		----------------------------------------------------------
		--Update VP buckets
		----------------------------------------------------------
		rtn_str := pkg_lessor_var_payments.f_calc_asset_buckets_month_end(a_company_id, a_month, a_calc_type);
		IF rtn_str <> 'OK' THEN
			pkg_pp_log.p_write_message('PKG_LESSOR_VAR_PAYMENTS.F_CALC_ASSET_BUCKET_MONTH_END returned an error: ' || rtn_str);
			Raise_Application_Error(-20000, 'PKG_LESSOR_VAR_PAYMENTS.F_CALC_ASSET_BUCKET_MONTH_END returned an error: ' || rtn_str);
		END IF ;
	END p_variable_payments_calc;

	--**************************************************************************
	--                            p_accruals_calc
	--             --------------------------------
	-- @@ description
	--    this package will stage the monthly accrual numbers by lsr_ilr.
	--    it will load from the ILR schedule into lsr_monthly_accrual_stg
	--  this package uses merge statements, so it can be run multiple times.
	-- @@params
	--    company: a_company_id
	--       the company to process accruals for
	--    date: a_month
	--       the month to process accruals for
	--    end log: a_end_log
	--       whether or not to end the log. 0 = continue logging, 1 = end log
	--
	--**************************************************************************
	PROCEDURE p_accruals_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log    IN NUMBER:=NULL)
	IS
		L_ILR_SCHEDULE_TABLE		pkg_lessor_common.ilr_schedule_line_table;
		SCHEDULEINDEX number;
	BEGIN
		pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lessor - Calculate Accruals'));
		pkg_pp_log.p_write_message('Starting p_accruals_calc');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		----------------------------------------------------------
		--Clearing prior runs for this company/month combination
		----------------------------------------------------------
--		L_STATUS:='Clearing prior runs for this company/month combination';
		delete from lsr_monthly_accrual_stg
		where ILR_ID IN (SELECT ILR_ID FROM LSR_ILR WHERE company_id = A_COMPANY_ID)
		and gl_posting_mo_yr = A_MONTH;

		----------------------------------------------------------
		--Calculate the accrual variable payments
		----------------------------------------------------------
		PKG_PP_LOG.P_WRITE_MESSAGE('Handling Variable Payments - Accruals');
		p_variable_payments_calc(A_COMPANY_ID, A_MONTH, 'accruals');

		-------------------------------------------------------------------------
		--get collection of schedule rows to build accruals from
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Pulling schedule rows to use for accruals');
		SELECT sch.ilr_id,
			sch.set_of_books_id,
			1,
			sch.company_id,
			sch.month,
			0,
			sch.interest_income_accrued,
			sch.executory_accrual1,
			sch.executory_accrual2,
			sch.executory_accrual3,
			sch.executory_accrual4,
			sch.executory_accrual5,
			sch.executory_accrual6,
			sch.executory_accrual7,
			sch.executory_accrual8,
			sch.executory_accrual9,
			sch.executory_accrual10,
			sch.contingent_accrual1,
			sch.contingent_accrual2,
			sch.contingent_accrual3,
			sch.contingent_accrual4,
			sch.contingent_accrual5,
			sch.contingent_accrual6,
			sch.contingent_accrual7,
			sch.contingent_accrual8,
			sch.contingent_accrual9,
			sch.contingent_accrual10
		BULK COLLECT INTO L_ILR_SCHEDULE_TABLE
		FROM (
			SELECT sch.ilr_id,
				sch.set_of_books_id,
				ilr.company_id,
				sch.month,
				sch.interest_income_accrued,
				sch.executory_accrual1,
				sch.executory_accrual2,
				sch.executory_accrual3,
				sch.executory_accrual4,
				sch.executory_accrual5,
				sch.executory_accrual6,
				sch.executory_accrual7,
				sch.executory_accrual8,
				sch.executory_accrual9,
				sch.executory_accrual10,
				sch.contingent_accrual1,
				sch.contingent_accrual2,
				sch.contingent_accrual3,
				sch.contingent_accrual4,
				sch.contingent_accrual5,
				sch.contingent_accrual6,
				sch.contingent_accrual7,
				sch.contingent_accrual8,
				sch.contingent_accrual9,
				sch.contingent_accrual10
			FROM lsr_ilr_schedule sch
			INNER JOIN lsr_ilr ilr
				ON sch.ilr_id = ilr.ilr_id
				AND sch.revision = ilr.current_revision
			WHERE ilr.company_id = A_COMPANY_ID
			AND sch.month = A_MONTH
			AND ilr.ilr_status_id IN (2,3) -- 2 = in-service; 3 = retired
		) sch;

		-------------------------------------------------------------------------
		--Inserting interest accrual
		-------------------------------------------------------------------------
		PKG_PP_LOG.P_WRITE_MESSAGE('Inserting interest accrual');
    forall SCHEDULEINDEX in indices of L_ILR_SCHEDULE_TABLE
      merge into LSR_MONTHLY_ACCRUAL_STG stg
      using (select 0 as accrual_id,
							(SELECT Decode(Lower(fasb_ct.description),'operating',25,'sales type',2,2) accrual_type_id
								FROM lsr_ilr_options o
									JOIN lsr_ilr ilr ON ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision
									JOIN lsr_fasb_type_sob fasb_sob ON fasb_sob.cap_type_id = o.lease_cap_type_id
									JOIN lsr_fasb_cap_type fasb_ct ON fasb_ct.fasb_cap_type_id = fasb_sob.fasb_cap_type_id
								WHERE o.ilr_id = L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID
								AND fasb_sob.set_of_books_id = L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID) AS accrual_type_id,
--							2 AS accrual_type_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ilr_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).INTEREST_INCOME as amount,
              A_MONTH as gl_posting_mo_yr,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
              from DUAL
          where L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).INTEREST_INCOME <> 0) b
      on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
          and b.ILR_ID = stg.ILR_ID
          and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
          and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
      when matched then update set stg.AMOUNT = b.AMOUNT
      when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
        values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.ILR_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);


		-------------------------------------------------------------------------
		--Inserting executory accrual
		-------------------------------------------------------------------------
		PKG_PP_LOG.P_WRITE_MESSAGE('Inserting executory accrual');
		PKG_LESSOR_BUCKET.P_CALC_EXEC_ACCRUALS(L_ILR_SCHEDULE_TABLE);

		-------------------------------------------------------------------------
		--Inserting contingent accrual
		-------------------------------------------------------------------------
		PKG_PP_LOG.P_WRITE_MESSAGE('Inserting contingent accrual');
		PKG_LESSOR_BUCKET.P_CALC_CONT_ACCRUALS(L_ILR_SCHEDULE_TABLE);

		IF a_end_log = 1 THEN
			pkg_pp_log.p_write_message('Closing log');
			pkg_pp_log.p_end_log;
		END IF;
	exception
		when OTHERS THEN
			IF SQLCODE BETWEEN -20999 AND -20000 THEN
				rollback;
				pkg_pp_log.p_write_message(sqlerrm);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			--PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
			ELSIF SQLCODE = 100 THEN
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing Lessor ILRs - ' || sqlerrm || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				raise_application_error(-20000, substr('Error processing Lessor ILRs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
			ELSE
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing lessor ILRs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			END IF;
	END p_accruals_calc;

	--**************************************************************************
	--                            f_accruals_approve
	--             --------------------------------
	-- @@ description
	--    this function will approve and post the monthly accrual numbers by ls_asset.
	--    it will load from ls_asset_schedule into
	-- @@params
	--    date: a_month
	--       the month to process accruals for
	-- @@return
	--    varchar2: a message back to the caller
	--       'ok' = success
	--       all else = failure
	--
	--**************************************************************************
	PROCEDURE p_accruals_approve(
			a_company_id IN NUMBER,
        	a_month      IN DATE,
        	a_end_log    IN NUMBER:=NULL)

	IS
	BEGIN
		pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lessor - Approve Accruals'));
		pkg_pp_log.p_write_message('Starting p_accruals_approve');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		IF a_end_log = 1 THEN
			pkg_pp_log.p_write_message('Closing log');
			pkg_pp_log.p_end_log;
		END IF;
	END p_accruals_approve;

	--**************************************************************************
	--                            p_invoice_calc
	--             --------------------------------
	-- @@ description
	--    this function generates Lessor invoices based on the schedule lines for a company and month
	-- @@params
	--	  company: a_company_id
	--			the company to process for
	--    date: a_month
	--       the month to process accruals for
	-- @@return
	--		none - any errors are returned via exceptions.
	--
	--**************************************************************************
	PROCEDURE p_invoice_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log    IN NUMBER:=NULL)

	IS
		missing_workflows		VARCHAR2(4000);
		ilr_schedule_table		pkg_lessor_common.ilr_schedule_line_table;
	BEGIN
		----------------------------------------------------------
		--Start log and print inputs
		----------------------------------------------------------
		pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lessor - Calculate Invoices'));
		pkg_pp_log.p_write_message('Starting p_invoice_calc');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		----------------------------------------------------------
		--Calculate the invoice variable payments
		----------------------------------------------------------
		PKG_PP_LOG.P_WRITE_MESSAGE('Handling Variable Payments - Invoices');
		p_variable_payments_calc(A_COMPANY_ID, A_MONTH, 'invoices');

		----------------------------------------------------------
		--Clear previous records if they're initiated or rejected
		----------------------------------------------------------
		pkg_pp_log.p_write_message('Deleting records from LSR_INVOICE_LINE');
		DELETE FROM lsr_invoice_line line
		WHERE EXISTS (
			SELECT 1
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON inv.invoice_id = app.invoice_id
			WHERE line.invoice_id = inv.invoice_id
			AND ilr.company_id = A_COMPANY_ID
			AND inv.gl_posting_mo_yr = A_MONTH
			AND app.approval_status_id IN (1, 4, 7) -- 1 = initiated; 4 = rejected; 7 = unrejected
		);

		pkg_pp_log.p_write_message('Deleting records from WORKFLOW_DETAIL');
		DELETE FROM WORKFLOW_DETAIL
			WHERE  workflow_id IN
				   ( SELECT workflow_id
					 FROM   WORKFLOW
					 WHERE  id_field1 IN
							( SELECT To_char( invoice_id )
							  FROM   LSR_INVOICE_APPROVAL app
							  WHERE  approval_status_id in (1,4,7) ) AND
							subsystem = 'lsr_invoice_approval' );


		pkg_pp_log.p_write_message('Deleting records from WORKFLOW');
		DELETE FROM WORKFLOW
		WHERE  id_field1 IN
			   ( SELECT To_char( invoice_id )
				 FROM   LSR_INVOICE_APPROVAL
				 WHERE  approval_status_id in (1,4,7) ) AND
			   subsystem = 'lsr_invoice_approval';

		pkg_pp_log.p_write_message('Deleting records from LSR_INVOICE_APPROVAL');
		DELETE FROM lsr_invoice_approval app
		WHERE app.invoice_id IN (
			SELECT inv.invoice_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON inv.invoice_id = app.invoice_id
			WHERE ilr.company_id = A_COMPANY_ID
			AND inv.gl_posting_mo_yr = A_MONTH
		)
		AND app.approval_status_id IN (1, 4, 7); -- 1 = initiated; 4 = rejected; 7 = unrejected

		pkg_pp_log.p_write_message('Deleting records from LSR_INVOICE');
		DELETE FROM lsr_invoice inv
		WHERE EXISTS (
			SELECT 1
			FROM lsr_ilr ilr
			WHERE inv.ilr_id = ilr.ilr_id
			AND ilr.company_id = A_COMPANY_ID
		)
		AND NOT EXISTS ( --we've already removed these records from the approval table, so just remove anything not in the approval table
			SELECT 1
			FROM lsr_invoice_approval app
			WHERE app.invoice_id = inv.invoice_id
		)
		AND inv.gl_posting_mo_yr = A_MONTH;

		-------------------------------------------------------------------------
		--get collection of schedule rows to build invoice lines from
		--	This needs to be before we insert into LSR_INVOICE because of the NOT EXISTS clauses
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Pulling schedule rows to use for invoice lines');
		SELECT sch.ilr_id,
			sch.set_of_books_id,
			Row_Number() OVER (PARTITION BY sch.ilr_id, sch.set_of_books_id ORDER BY sch.ilr_id, sch.set_of_books_id),
			sch.company_id,
			sch.month,
			sch.principal_received,
			sch.interest_income_received,
			sch.executory_paid1,
			sch.executory_paid2,
			sch.executory_paid3,
			sch.executory_paid4,
			sch.executory_paid5,
			sch.executory_paid6,
			sch.executory_paid7,
			sch.executory_paid8,
			sch.executory_paid9,
			sch.executory_paid10,
			sch.contingent_paid1,
			sch.contingent_paid2,
			sch.contingent_paid3,
			sch.contingent_paid4,
			sch.contingent_paid5,
			sch.contingent_paid6,
			sch.contingent_paid7,
			sch.contingent_paid8,
			sch.contingent_paid9,
			sch.contingent_paid10
		BULK COLLECT INTO ilr_schedule_table
		FROM (
			SELECT sch.ilr_id,
				sch.set_of_books_id,
				ilr.company_id,
				sch.month,
				sch_sd.principal_received,
				sch.interest_income_received,
				sch.executory_paid1,
				sch.executory_paid2,
				sch.executory_paid3,
				sch.executory_paid4,
				sch.executory_paid5,
				sch.executory_paid6,
				sch.executory_paid7,
				sch.executory_paid8,
				sch.executory_paid9,
				sch.executory_paid10,
				sch.contingent_paid1,
				sch.contingent_paid2,
				sch.contingent_paid3,
				sch.contingent_paid4,
				sch.contingent_paid5,
				sch.contingent_paid6,
				sch.contingent_paid7,
				sch.contingent_paid8,
				sch.contingent_paid9,
				sch.contingent_paid10
			FROM lsr_ilr_schedule_sales_direct sch_sd
			INNER JOIN lsr_ilr_schedule sch
				ON sch.ilr_id = sch_sd.ilr_id
				AND sch.set_of_books_id = sch_sd.set_of_books_id
				AND sch.month = sch_sd.month
				AND sch.revision = sch_sd.revision
			INNER JOIN lsr_ilr ilr
				ON sch.ilr_id = ilr.ilr_id
				AND sch.revision = ilr.current_revision
			INNER JOIN lsr_ilr_options opt
				ON ilr.ilr_id = opt.ilr_id
				AND ilr.current_revision = opt.revision
			INNER JOIN lsr_fasb_type_sob fasb_sob
				ON opt.lease_cap_type_id = fasb_sob.cap_type_id
				AND sch.set_of_books_id = fasb_sob.set_of_books_id
			WHERE ilr.company_id = A_COMPANY_ID
			AND sch.month = A_MONTH
			AND ilr.ilr_status_id IN (2,3) -- 2 = in-service; 3 = retired
			AND NOT EXISTS (
				SELECT 1
				FROM lsr_invoice inv
				WHERE inv.gl_posting_mo_yr = A_MONTH
				AND inv.ilr_id = ilr.ilr_id
			)
			AND fasb_sob.fasb_cap_type_id IN (2,3)
			UNION
			SELECT sch.ilr_id,
				sch.set_of_books_id,
				ilr.company_id,
				sch.month,
				0 AS principal_received,
				sch.interest_income_received,
				sch.executory_paid1,
				sch.executory_paid2,
				sch.executory_paid3,
				sch.executory_paid4,
				sch.executory_paid5,
				sch.executory_paid6,
				sch.executory_paid7,
				sch.executory_paid8,
				sch.executory_paid9,
				sch.executory_paid10,
				sch.contingent_paid1,
				sch.contingent_paid2,
				sch.contingent_paid3,
				sch.contingent_paid4,
				sch.contingent_paid5,
				sch.contingent_paid6,
				sch.contingent_paid7,
				sch.contingent_paid8,
				sch.contingent_paid9,
				sch.contingent_paid10
			FROM lsr_ilr_schedule sch
			INNER JOIN lsr_ilr ilr
				ON sch.ilr_id = ilr.ilr_id
				AND sch.revision = ilr.current_revision
			INNER JOIN lsr_ilr_options opt
				ON ilr.ilr_id = opt.ilr_id
				AND ilr.current_revision = opt.revision
			INNER JOIN lsr_fasb_type_sob fasb_sob
				ON opt.lease_cap_type_id = fasb_sob.cap_type_id
				AND sch.set_of_books_id = fasb_sob.set_of_books_id
			WHERE ilr.company_id = A_COMPANY_ID
			AND sch.MONTH = A_MONTH
			AND ilr.ilr_status_id IN (2,3) -- 2 = in-service; 3 = retired
			AND NOT EXISTS (
				SELECT 1
				FROM lsr_invoice inv
				WHERE inv.gl_posting_mo_yr = A_MONTH
				AND inv.ilr_id = ilr.ilr_id
			)
			AND fasb_sob.fasb_cap_type_id IN (1) -- 1 = operating
		) sch;

		----------------------------------------------------------
		--Create invoice headers
		----------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice header rows');
		INSERT INTO lsr_invoice (invoice_id, invoice_number, ilr_id, gl_posting_mo_yr)
		SELECT lsr_invoice_seq.NEXTVAL,
			SubStr('AUTO ' || To_Char(A_MONTH, 'MM/YYYY') || '-' || To_Char(A_COMPANY_ID) || '-' || To_Char(a.ilr_id), 0, 35),
			a.ilr_id,
			A_MONTH
		FROM (
			SELECT DISTINCT ilr.ilr_id AS ilr_id,
				ilr.company_id AS company_id
			FROM lsr_ilr_schedule sch
			INNER JOIN lsr_ilr ilr
				ON ilr.ilr_id = sch.ilr_id
				AND sch.revision = ilr.current_revision
			WHERE ilr.ilr_status_id IN (2,3) -- 2 = in-service; 3 = retired
			AND sch.MONTH = A_MONTH
			AND ilr.company_id = A_COMPANY_ID
			AND NOT EXISTS (
				SELECT 1
				FROM lsr_invoice inv
				WHERE inv.gl_posting_mo_yr = A_MONTH
				AND inv.ilr_id = ilr.ilr_id
			)
		) a;

		----------------------------------------------------------
		--Create approval rows
		----------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice approval rows');
		INSERT INTO lsr_invoice_approval (invoice_id, approval_type_id, approval_status_id)
		SELECT inv.invoice_id,
			Decode(Nvl(lg.requires_approval, 1),
				0,
				auto.workflow_type_id,
				man.workflow_type_id),
			1
		FROM lsr_invoice inv
		INNER JOIN lsr_ilr ilr
			ON inv.ilr_id = ilr.ilr_id
		INNER JOIN lsr_lease lease
			ON ilr.lease_id = lease.lease_id
		INNER JOIN lsr_lease_group lg
			ON lease.lease_group_id = lg.lease_group_id
		FULL OUTER JOIN workflow_type auto
			ON lower(nvl(auto.external_workflow_type, ' ')) = 'auto'
			AND Lower(auto.subsystem) LIKE '%lsr_invoice_approval%'
			AND auto.active = 1
		FULL OUTER JOIN workflow_type man
			ON lower(nvl(man.external_workflow_type, ' ')) <> 'auto'
			AND Lower(man.subsystem) LIKE '%lsr_invoice_approval%'
			AND man.active = 1
		WHERE ilr.company_id = A_COMPANY_ID
		AND inv.gl_posting_mo_yr = A_MONTH
		AND NOT EXISTS (
			SELECT 1
			FROM lsr_invoice_approval app
			WHERE inv.invoice_id = app.invoice_id
		);

		-------------------------------------------------------------------------
		--Validate that all approval records were able to find a workflow_type_id
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Validating that all approval workflows were found');
		missing_workflows := '';
		FOR rec IN (SELECT DISTINCT Nvl(lg.requires_approval, 1) AS requires_approval
					FROM lsr_invoice_approval app
					INNER JOIN lsr_invoice inv
						ON inv.invoice_id = app.invoice_id
					INNER JOIN lsr_ilr ilr
						ON ilr.ilr_id = inv.ilr_id
					INNER JOIN lsr_lease lease
						ON lease.lease_id = ilr.lease_id
					INNER JOIN lsr_lease_group lg
						ON lg.lease_group_id = lease.lease_group_id
					WHERE app.approval_type_id IS NULL
					AND inv.gl_posting_mo_yr = A_MONTH
					AND ilr.company_id = A_COMPANY_ID
					AND app.approval_status_id = 1) --1 = initiated
		LOOP
			--if this isn't our first record, add the comma
			IF Nvl(Length(missing_workflows),0) <> 0 THEN
				missing_workflows := missing_workflows || ', ';
			END IF;

			IF rec.requires_approval = 0 THEN
				missing_workflows := missing_workflows || 'Could not find auto approval workflow for "lsr_invoice_approval.';
			ELSE
				missing_workflows := missing_workflows || 'Could not find manual approval workflow for "lsr_invoice_approval.';
			END IF;
		END LOOP;

		IF Nvl(Length(missing_workflows), 0) <> 0 THEN
			--there was at least one missing workflow - throw error
			Raise_Application_Error(-20000, missing_workflows);
		END IF;

		-------------------------------------------------------------------------
		--Create invoice lines for principal received
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for principal_received');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				1, --1 = principal
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).principal_received,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = A_MONTH
			AND ilr.company_id = A_COMPANY_ID
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).principal_received <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for interest received
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for interest_income_received');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				(SELECT Decode(Lower(fasb_ct.description),'operating',25,'sales type',2,2) invoice_type_id --2 = Interest, 25 = Income
								FROM lsr_ilr_options o
									JOIN lsr_ilr ilr ON ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision
									JOIN lsr_fasb_type_sob fasb_sob ON fasb_sob.cap_type_id = o.lease_cap_type_id
									JOIN lsr_fasb_cap_type fasb_ct ON fasb_ct.fasb_cap_type_id = fasb_sob.fasb_cap_type_id
								WHERE o.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID
								AND fasb_sob.set_of_books_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID) AS invoice_type_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).interest_income,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = A_MONTH
			AND ilr.company_id = A_COMPANY_ID
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).interest_income <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Call procedures for executory and contingent buckets
		-------------------------------------------------------------------------
		pkg_lessor_bucket.p_calc_exec_invoices(ilr_schedule_table);
		pkg_lessor_bucket.p_calc_cont_invoices(ilr_schedule_table);

		-------------------------------------------------------------------------
		--Rollup payment lines to the header
		-------------------------------------------------------------------------
		UPDATE lsr_invoice inv
		SET (inv.invoice_principal, inv.invoice_interest, inv.invoice_executory, inv.invoice_contingent) = (
			SELECT
				Sum(CASE WHEN line.invoice_type_id = 1 THEN line.amount ELSE 0 END) AS invoice_principal,
				Sum(CASE WHEN line.invoice_type_id IN (2,25) THEN line.amount ELSE 0 END) AS invoice_interest,
				Sum(CASE WHEN line.invoice_type_id IN (3,4,5,6,7,8,9,10,11,12) THEN line.amount ELSE 0 END) AS invoice_executory,
				Sum(CASE WHEN line.invoice_type_id IN (13,14,15,16,17,18,19,20,21,22) THEN line.amount ELSE 0 END) AS invoice_contingent
			FROM lsr_invoice_line line
			WHERE line.invoice_id = inv.invoice_id
			AND line.set_of_books_id = (
				SELECT Min(set_of_books_id)
				FROM lsr_invoice_line line2
				WHERE line.invoice_id = line2.invoice_id
			)
			GROUP BY line.invoice_id
		)
		WHERE EXISTS (
			SELECT 1
			FROM lsr_ilr ilr
			WHERE ilr.ilr_id = inv.ilr_id
			AND ilr.company_id = A_COMPANY_ID
		)
		AND inv.gl_posting_mo_yr = A_MONTH
		AND EXISTS (
			SELECT 1
			FROM lsr_invoice_approval app
			WHERE app.invoice_id = inv.invoice_id
			AND app.approval_status_id = 1
		);

		-----------------------------------------------------------------------
		--Delete approvals and headers with no invoice lines
		-----------------------------------------------------------------------
		DELETE FROM lsr_invoice_approval app
		WHERE NOT EXISTS (
			SELECT 1
			FROM lsr_invoice_line line
			WHERE app.invoice_id = line.invoice_id
		)
		AND EXISTS ( --only remove invoices from this company and month
			SELECT 1
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON ilr.ilr_id = inv.ilr_id
			WHERE app.invoice_id = inv.invoice_id
			AND ilr.company_id = A_COMPANY_ID
			AND inv.gl_posting_mo_yr = A_MONTH
		)
		AND app.approval_status_id = 1;

		DELETE FROM lsr_invoice inv
		WHERE NOT EXISTS (
			SELECT 1
			FROM lsr_invoice_line line
			WHERE inv.invoice_id = line.invoice_id
		)
		AND NOT EXISTS ( --we've already removed these records from the approval table, so just remove anything not in the approval table
			SELECT 1
			FROM lsr_invoice_approval app
			WHERE app.invoice_id = inv.invoice_id
		)
		AND EXISTS ( --make sure we're only looking at this company's ILRs
			SELECT 1
			FROM lsr_ilr ilr
			WHERE ilr.ilr_id = inv.ilr_id
			AND ilr.company_id = A_COMPANY_ID
		)
		AND inv.gl_posting_mo_yr = A_MONTH;

		IF a_end_log = 1 THEN
			pkg_pp_log.p_end_log;
		END IF;
	exception
		when OTHERS THEN
			IF SQLCODE BETWEEN -20999 AND -20000 THEN
				rollback;
				pkg_pp_log.p_write_message(sqlerrm);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			--PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
			ELSIF SQLCODE = 100 THEN
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing Lessor ILRs - ' || sqlerrm || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				raise_application_error(-20000, substr('Error processing Lessor ILRs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
			ELSE
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing lessor ILRs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			END IF;
	END p_invoice_calc;

	--**************************************************************************
	--                            f_invoice_approve
	--             --------------------------------
	-- @@ description
	--    this function will approve and post the monthly accrual numbers by ls_asset.
	--    it will load from ls_asset_schedule into
	-- @@params
	--    date: a_month
	--       the month to process accruals for
	-- @@return
	--    varchar2: a message back to the caller
	--       'ok' = success
	--       all else = failure
	--
	--**************************************************************************
	PROCEDURE p_invoice_approve(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log    IN NUMBER:=NULL)

	IS
		l_unapproved_count 	INTEGER;
	BEGIN
		pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lessor - Approve Invoices'));
		pkg_pp_log.p_write_message('Starting p_invoice_approve');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		-----------------------------------------------------------------------
		--Auto approve invoices that meeting the following criteria:
		--	1. Lease group is set to auto approve
		--	2. Workflow type is auto approve
		--	3. Status is not in (pending approval, approved, rejected, auto-approved)
		-----------------------------------------------------------------------
		UPDATE lsr_invoice_approval app
		SET app.approval_status_id = 6,
			app.approval_date = SYSDATE
		WHERE EXISTS ( --lease group set to auto_approve
			SELECT 1
			FROM lsr_invoice invoice
			JOIN lsr_ilr ilr ON invoice.ilr_id = ilr.ilr_id
			JOIN lsr_lease lease ON ilr.lease_id = lease.lease_id
			JOIN lsr_lease_group grp ON lease.lease_group_id = grp.lease_group_id
			WHERE app.invoice_id = invoice.invoice_id
			AND ilr.company_id = a_company_id
			AND invoice.gl_posting_mo_yr = a_month
			AND coalesce(grp.requires_approval, 1) = 0
		)
		AND EXISTS ( -- workflow type is auto approve
			SELECT 1
			FROM workflow_type wt
			WHERE wt.workflow_type_id = app.approval_type_id
			AND Lower(Nvl(wt.external_workflow_type, '')) = 'auto'
			AND Lower(wt.subsystem) LIKE '%lsr_invoice_approval%'
		)
		AND app.approval_status_id NOT IN (2,3,4,6); --respectively: Pending Approval, Approved, Rejected, Approved - Auto

		-----------------------------------------------------------------------
		--Validate that all invoices for the month are either approved or auto approved
		-----------------------------------------------------------------------
		SELECT Count(1)
		INTO l_unapproved_count
		FROM lsr_invoice_approval app
		WHERE EXISTS (
			SELECT 1
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			WHERE app.invoice_id = inv.invoice_id
			AND ilr.company_id = A_COMPANY_ID
			AND inv.gl_posting_mo_yr = A_MONTH
		)
		AND app.approval_status_id NOT IN (3,6); --3 = approved, 6 = auto-approved

		IF l_unapproved_count <> 0 THEN
			Raise_Application_Error(-20000, 'ERROR: ' || l_unapproved_count || ' lessor invoices have not been approved for company_id ' || A_COMPANY_ID || ' and month ' || A_MONTH || '.');
		END IF;

		IF a_end_log = 1 THEN
			pkg_pp_log.p_write_message('Closing log');
			pkg_pp_log.p_end_log;
		END IF;
	END p_invoice_approve;

	--**************************************************************************
	--                            f_auto_termination_calc
	--             --------------------------------
	-- @@ description
	--    this function will stage the monthly accrual numbers by ls_asset.
	--    it will load from ls_asset_schedule into ls_monthly_accrual_stg
	--  this function uses merge statements, so it can be run multiple times.
	-- @@params
	--    date: a_month
	--       the month to process auto_termination for
	-- @@return
	--    varchar2: a message back to the caller
	--       'ok' = success
	--       all else = failure
	--
	--**************************************************************************
	PROCEDURE p_auto_termination_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
            a_end_log    IN NUMBER:=NULL)
	IS
	BEGIN
		pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lessor - Calculate Auto Termination'));
		pkg_pp_log.p_write_message('Starting p_auto_termination_calc');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		IF a_end_log = 1 THEN
			pkg_pp_log.p_write_message('Closing log');
			pkg_pp_log.p_end_log;
		END IF;
	END p_auto_termination_calc;

	--**************************************************************************
	--                            f_auto_termination_approve
	--             --------------------------------
	-- @@ description
	--    this function will approve and post the monthly accrual numbers by ls_asset.
	--    it will load from ls_asset_schedule into
	-- @@params
	--    date: a_month
	--       the month to process auto_termination for
	-- @@return
	--    varchar2: a message back to the caller
	--       'ok' = success
	--       all else = failure
	--
	--**************************************************************************
	PROCEDURE p_auto_termination_approve(
			a_company_id IN NUMBER,
        	a_month      IN DATE,
        	a_end_log    IN NUMBER:=NULL)

	IS
	BEGIN
		pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lessor - Approve Auto Termination'));
		pkg_pp_log.p_write_message('Starting p_auto_termination_approve');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		IF a_end_log = 1 THEN
			pkg_pp_log.p_write_message('Closing log');
			pkg_pp_log.p_end_log;
		END IF;
	END p_auto_termination_approve;

	--**************************************************************************
	--                            f_currency_gain_loss
	--             --------------------------------
	-- @@ description
	--    this function will return the currency gain/loss for the given companies, month, and set of books.
	--    this is used to generate journal entries associated with the gain/loss.
	-- @@params
	--    t_num_array: a_company_ids
	--      the companies for which to return currency gain/loss
	--    date: a_month
	--       the month for which to return currency gain/loss
	-- @@return
	--    currency_gain_loss_tbl pipelined:
	--       a pipelined pl/sql table containing the results of the calculation
	--**************************************************************************
	FUNCTION f_currency_gain_loss(
			a_company_ids     IN t_num_array,
			a_month           IN DATE,
			a_set_of_books_id IN NUMBER)
		RETURN currency_gain_loss_tbl
		pipelined
	IS
	BEGIN
		FOR item IN (
			SELECT sch.company_id,
				sch.ilr_id,
				sch.ilr_number,
				sch.iso_code,
				sch.currency_display_symbol,
				sch.gain_loss_fx,
				sch.month,
				acct.curr_gain_loss_acct_id,
				acct.curr_gain_loss_offset_acct_id
			FROM v_lsr_ilr_mc_schedule sch
			INNER JOIN ls_lease_currency_type cur_type
				ON sch.ls_cur_type = cur_type.ls_currency_type_id
			INNER JOIN lsr_ilr ilr
				ON ilr.ilr_id = sch.ilr_id
				AND ilr.current_revision = sch.revision
			INNER JOIN lsr_ilr_account acct
				ON acct.ilr_id = ilr.ilr_id
			WHERE sch.company_id MEMBER OF A_COMPANY_IDS
			AND sch.MONTH = A_MONTH
			AND sch.set_of_books_id = A_SET_OF_BOOKS_ID
			AND Lower(cur_type.description) = 'company'
			AND sch.gain_loss_fx <> 0)
		LOOP
			PIPE ROW(item);
		END LOOP;
	END f_currency_gain_loss;

	--**************************************************************************
	--                            f_currency_gain_loss_approve
	--             --------------------------------
	-- @@ description
	--    this function will approve and post the monthly gain/loss entries resulting from
	--         fluctuations in currency exchange rates.
	-- @@params
	--    date: a_month
	--       the month to process currency gain/loss for
	-- @@return
	--    varchar2: a message back to the caller
	--       'ok' = success
	--       all else = failure
	--
	--**************************************************************************
	PROCEDURE p_currency_gain_loss_approve(
			a_company_id      in number,
			a_month           in date,
			a_set_of_books_id in number,
			a_end_log         in number := null)

	IS
	BEGIN
		pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lessor - Approve Currency Gain/Loss'));
		pkg_pp_log.p_write_message('Starting p_currency_gain_loss_approve');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_set_of_books_id: ' || a_set_of_books_id);
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		----------------------------------------------------------
		--Validate inputs
		----------------------------------------------------------
		IF a_company_id IS NULL THEN
			Raise_Application_Error(-20000, 'NULL company ID passed to pkg_lease_calc.p_currency_gain_loss_approve.');
		END IF;

		IF a_month IS NULL THEN
			Raise_Application_Error(-20000, 'NULL month passed to pkg_lease_calc.p_currency_gain_loss_approve.');
		END IF;

		IF a_set_of_books_id IS NULL THEN
			Raise_Application_Error(-20000, 'NULL set of books ID passed to pkg_lease_calc.p_currency_gain_loss_approve.');
		END IF;

		----------------------------------------------------------
		--Loop over schedule lines
		----------------------------------------------------------
		FOR l_ilr_info IN (
			SELECT sch.company_id,
				sch.ilr_id,
				sch.ilr_number,
				sch.iso_code,
				sch.currency_display_symbol,
				sch.gain_loss_fx,
				sch.month,
				sch.curr_gain_loss_acct_id,
				sch.curr_gain_loss_offset_acct_id
			FROM TABLE(f_currency_gain_loss(t_num_array(a_company_id), a_month, a_set_of_books_id)) sch
			)
		LOOP
			IF l_ilr_info.gain_loss_fx IS NULL THEN
				Raise_Application_Error(-20000, 'NULL gain_loss_fx in pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
					', ' || a_month || ', ' || a_set_of_books_id || '). Ensure all amounts are populated.');
			end if;

			--copied from Lessee but since Lessor is at ILR-level, not sure where to get work_order_id from. Will need to reconsider when implementing JEs.
--			IF l_ilr_info.work_order_id IS NULL THEN
--				Raise_Application_Error(-20000, 'NULL work_order_id in pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
--					', ' || a_month || ', ' || a_set_of_books_id || '). Ensure all work_order_ids are populated.');
--			END IF;

			IF l_ilr_info.company_id IS NULL THEN
				Raise_Application_Error(-20000, 'NULL company_id in pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
					', ' || a_month || ', ' || a_set_of_books_id || '). Ensure all company_ids are populated.');
			end if;

			IF l_ilr_info.month IS NULL THEN
				Raise_Application_Error(-20000, 'NULL month in pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
					', ' || a_month || ', ' || a_set_of_books_id || '). Ensure all months are populated.');
			end if;

			IF l_ilr_info.curr_gain_loss_acct_id IS NULL THEN
				Raise_Application_Error(-20000, 'NULL curr_gain_loss_acct_id in ' ||
					'pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
					', ' || a_month || ', ' || a_set_of_books_id || '). Ensure all currency_gain_loss_dr_account_ids are populated. (Check lsr_ilr_account)');
			end if;

			IF l_ilr_info.curr_gain_loss_offset_acct_id IS NULL THEN
				Raise_Application_Error(-20000, 'NULL curr_gain_loss_offset_acct_id in ' ||
					'pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
					', ' || a_month || ', ' || a_set_of_books_id || '). Ensure all currency_gain_loss_cr_account_ids are populated. (Check lsr_ilr_account)');
			end if;
		END LOOP;

		IF a_end_log = 1 THEN
			pkg_pp_log.p_write_message('Closing log');
			pkg_pp_log.p_end_log;
		END IF;
	EXCEPTION
		when OTHERS THEN
			IF SQLCODE BETWEEN -20999 AND -20000 THEN
				rollback;
				pkg_pp_log.p_write_message(sqlerrm);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			--PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
			ELSIF SQLCODE = 100 THEN
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing Lessor ILRs - ' || sqlerrm || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				raise_application_error(-20000, substr('Error processing Lessor ILRs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
			ELSE
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing lessor ILRs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			END IF;
	END p_currency_gain_loss_approve;

	--**************************************************************************
	--                            f_lsr_closed
	--             --------------------------------
	-- @@ description
	--    this function will return the currency gain/loss for the given companies, month, and set of books.
	--    this is used to generate journal entries associated with the gain/loss.
	-- @@params
	--    t_num_array: a_company_ids
	--      the companies for which to return currency gain/loss
	--    date: a_month
	--       the month for which to return currency gain/loss
	-- @@return
	--    currency_gain_loss_tbl pipelined:
	--       a pipelined pl/sql table containing the results of the calculation
	--**************************************************************************
	PROCEDURE p_lsr_closed(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log IN NUMBER:=NULL)
	IS
		l_count		INTEGER;
	BEGIN
		pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lessor - Close Month'));
		pkg_pp_log.p_write_message('Starting p_lsr_closed');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		-------------------------------------------------------------------------
		--Validate that the CPR is open for the next month
		-------------------------------------------------------------------------
		SELECT Count(1)
		INTO l_count
		FROM cpr_control
		WHERE company_id = A_COMPANY_ID
		AND accounting_month = Add_Months(A_MONTH, 1);

		IF l_count = 0 THEN
			Raise_Application_Error(-20000, 'The CPR must be open for the next month in order to close the Lessor module.');
		END IF;

		IF a_end_log = 1 THEN
			pkg_pp_log.p_write_message('Closing log');
			pkg_pp_log.p_end_log;
		END IF;
	exception
		when OTHERS THEN
			IF SQLCODE BETWEEN -20999 AND -20000 THEN
				rollback;
				pkg_pp_log.p_write_message(sqlerrm);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			--PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
			ELSIF SQLCODE = 100 THEN
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing Lessor ILRs - ' || sqlerrm || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				raise_application_error(-20000, substr('Error processing Lessor ILRs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
			ELSE
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing lessor ILRs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			END IF;
	END p_lsr_closed;
END pkg_lessor_calc;
/

--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (4067, 0, 2017, 1, 0, 0, 0, 'C:\plasticwks\powerplant\sql\packages', '2017.1.0.0_PKG_LESSOR_CALC.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 