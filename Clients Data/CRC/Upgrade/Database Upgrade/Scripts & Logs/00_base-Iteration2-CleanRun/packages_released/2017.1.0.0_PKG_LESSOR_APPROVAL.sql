
CREATE OR REPLACE package pkg_lessor_approval AS
   /*
   ||============================================================================
   || Application: PowerPlan
   || Object Name: PKG_LESSOR_APPROVAL
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan Inc, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 2017.1.0 10/02/2015 Shane "C" Ward Original Version
   || 2017.1.0 10/04/2015 Shane "C" Ward Add ILR Workflows
   ||============================================================================
   */

   function F_APPROVE_MLA(A_LEASE_ID number,
                          A_REVISION number) return number;

   function F_REJECT_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_SEND_MLA(A_LEASE_ID number,
                       A_REVISION number) return number;

   function F_UNREJECT_MLA(A_LEASE_ID number,
                           A_REVISION number) return number;

   function F_UNSEND_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID number,
                                  A_REVISION number) return number;
                                  
   function F_APPROVE_ILR(A_ILR_ID   number,
                          A_REVISION number) return number;

   function F_REJECT_ILR(A_ILR_ID   number,
                         A_REVISION number) return number;

   function F_SEND_ILR(A_ILR_ID   number,
                       A_REVISION number) return number;

   function F_UNREJECT_ILR(A_ILR_ID   number,
                           A_REVISION number) return number;

   function F_UNSEND_ILR(A_ILR_ID   number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID   number,
                                  A_REVISION number) return number;
                                  

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2) return number;

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2,
                                    A_LOG in boolean,
									a_is_transfer in boolean default false) return number;
                  
   function F_INVOICE_APPROVE(A_COMPANY_ID in number,
                              A_MONTH      in date,
                              A_END_LOG    in number:=null) return varchar2;

   function F_SEND_INVOICE(A_INVOICE_ID number) return number;

   function F_UPDATE_WORKFLOW_INVOICE(A_INVOICE_ID number) return number;

   function F_REJECT_INVOICE(A_INVOICE_ID number) return number;

   function F_UNREJECT_INVOICE(A_INVOICE_ID number) return number;

   function F_APPROVE_INVOICE(A_INVOICE_ID number) return number;

   function F_UNSEND_INVOICE(A_INVOICE_ID number) return number;

end PKG_Lessor_approval;
/
   --**************************************************************************
--                            Initialize Package
--**************************************************************************

CREATE OR REPLACE package body pkg_lessor_approval AS

   /*
   ||============================================================================
   || Application: PowerPlan
   || Object Name: PKG_LESSOR_APPROVAL
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan Inc, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 2017.1.0 10/02/2015 Shane "C" Ward Original Version
   ||============================================================================
   */

   L_ILR_ID number;


   --**************************************************************************
   --                            P_SET_ILR_ID
   --**************************************************************************

   procedure P_SET_ILR_ID(A_ILR_ID number) is

   begin
      L_ILR_ID := A_ILR_ID;
   end P_SET_ILR_ID;
   
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   procedure P_LOGERRORMESSAGE(A_ILR_ID in number,
                               A_MSG    in varchar2) is
      pragma autonomous_transaction;
   begin
      update LS_PEND_TRANSACTION set ERROR_MESSAGE = A_MSG where ILR_ID = A_ILR_ID;

      commit;
   end P_LOGERRORMESSAGE;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   
  function F_EXECUTE_IMMEDIATE(A_SQLS in varchar2, A_START_LOG in number:=null) return varchar2
    is
    counter number;
    begin
    if nvl(a_start_log,0) = 1 then
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
    end if;

    if lower(trim(pkg_pp_system_control.f_pp_system_control('Lease Debug Execute Immediate'))) = 'yes' then
      pkg_pp_log.p_write_message(a_sqls);
    end if;

    execute immediate A_SQLS;

    return 'OK';

    exception when others then
      return 'Error Executing SQL: ' || sqlerrm || ' ' || sqlcode || chr(13) || chr(10) || a_sqls;
    end f_execute_immediate;
   
   --**************************************************************************
   --                            F_APPROVE_ILR_NO_COMMIT
   --************************************************************************** 

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2) return number is
      RTN    number;
      MY_STR varchar2(2000);

   begin
      rtn := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, A_STATUS, TRUE);
      return rtn;
   exception
      when others then
         P_LOGERRORMESSAGE(A_ILR_ID, A_STATUS);

         return -1;
   end F_APPROVE_ILR_NO_COMMIT;
   
   
   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2,
                                    A_LOG in boolean,
									a_is_transfer in boolean default false) return number is
      RTN    number;
      MY_STR varchar2(2000);

   begin
   
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
	  
      A_STATUS := 'APPROVE ILR:';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
	  
      A_STATUS := '   ILR:' || TO_CHAR(A_ILR_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
	  
      A_STATUS := '   REVISION: ' || TO_CHAR(A_REVISION);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Updating LSR_ILR_APPROVAL1';
	  
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LSR_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);
		 
      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Updating LSR_ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
	  
      update LSR_ILR L set ILR_STATUS_ID = 2, CURRENT_REVISION = A_REVISION where ILR_ID = A_ILR_ID;
	  
      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      return 1;
   exception
      when others then
       PKG_PP_LOG.P_WRITE_MESSAGE(sqlerrm);

         return -1;
   end F_APPROVE_ILR_NO_COMMIT;

   
   --**************************************************************************
   --                            F_APPROVE_ILR
   --**************************************************************************
   function F_APPROVE_ILR(A_ILR_ID   in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      MY_STR   varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      -- start by clearing our pend transaction
      RTN := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending For Approval: ' || L_STATUS);
         return -1;
      end if;

      commit;
      return 1;
   exception
      when others then
         rollback;
         P_LOGERRORMESSAGE(A_ILR_ID, L_STATUS);
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving ILR: ' || L_STATUS);
         return -1;
   end F_APPROVE_ILR;

   
   --**************************************************************************
   --                            F_REJECT_ILR
   --**************************************************************************

   function F_REJECT_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin

      update LSR_ILR_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_REJECT_ILR;
   
   --**************************************************************************
   --                            F_SEND_ILR
   -- Called when a user clicks send for approval (on an ILR)
   -- Need to mark the approval status to be sent.  And update the ILR to
   -- pending approval.  IN addition need to update the capitalized cost to
   -- be the beginning obligation on the asset schedule for the revision being
   -- sent for approval
   --**************************************************************************
   function F_SEND_ILR_NO_COMMIT(A_ILR_ID   in number,
                                 A_REVISION in number,
                                 A_STATUS   out varchar2) return number is
      RTN     number;
      ILR     number;
      WFS     number;
      SQLS    varchar2(32000);
      IS_AUTO WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
      APPROVED_DATE date;
      START_DATE date;
	  GL_POSTING_MO_YR date;
      v_component_count number;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_STATUS := 'SEND for approval:';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   ILR_ID: ' || TO_CHAR(A_ILR_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   REVISION: ' || TO_CHAR(A_REVISION);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

     --Check that the lease is valid ie in Open/New Revision status
	 -- /* WMD Have to be able to create new revisions on "Closed" leases
	 -- since new revisions are created for retirements and transfers */
     select count(1)
     into RTN
     from LSR_LEASE LL, LSR_ILR LI
     where LI.ILR_ID = A_ILR_ID
     and LI.LEASE_ID = LL.LEASE_ID
     and (LL.LEASE_STATUS_ID in (3,7)
	 or (LL.LEASE_STATUS_ID = 5
           and A_REVISION <> 1));

     if RTN <> 1 then
      A_STATUS := 'MLA for this ILR is not in Open or New Revision status, so the ILR cannot be posted to the MLA.';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      return -1;
     end if;

      A_STATUS := 'Getting start date';
      select min(PAYMENT_TERM_DATE)
        into START_DATE
      from LSR_ILR_PAYMENT_TERM
        where ILR_ID = A_ILR_ID
          and REVISION = A_REVISION;

      A_STATUS := 'Getting approved date';
      select min(PAYMENT_TERM_DATE)
        into APPROVED_DATE
      from lsr_ilr_payment_term pt, lsr_ilr_approval appr
      where pt.ilr_id = appr.ilr_id
      and pt.revision = appr.revision
      and appr.ilr_id = A_ILR_ID
      and appr.revision = (
         select max(revision)
         from lsr_ilr_approval
         where ilr_id = appr.ilr_id
           and approval_status_id in (3,6)
           and revision <> A_REVISION
      );

      PKG_PP_LOG.P_WRITE_MESSAGE('   Start Date: ' || to_char(START_DATE, 'YYYY-MM-DD'));
      PKG_PP_LOG.P_WRITE_MESSAGE('   Approved Date: ' || to_char(APPROVED_DATE, 'YYYY-MM-DD'));

	   --  Can change the start date as long as the prior start date is not yet been reached
      if APPROVED_DATE <> START_DATE and trunc(APPROVED_DATE, 'mm') < trunc(sysdate, 'mm') and APPROVED_DATE is not null then
        A_STATUS := 'Cannot change the in service date of the ILR after the in service date has been reached';
        PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

        return -1;
      end if;


      A_STATUS := 'updating LSR_ILR_APPROVAL';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LSR_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      --Update status of non-current revisions
      A_STATUS := 'update workflow';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update WORKFLOW
         set APPROVAL_STATUS_ID = 5
       where ID_FIELD1 = A_ILR_ID
         and ID_FIELD2 <> A_REVISION
         and APPROVAL_STATUS_ID = 2
         and Lower(SUBSYSTEM) = 'lessor_ilr_approval';

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'update ls_ilr_approval';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LSR_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 5
       where ILR_ID = A_ILR_ID
         and REVISION <> A_REVISION
         and APPROVAL_STATUS_ID = 2;

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');

      return 1;
   exception
      when others then
         PKG_PP_LOG.P_WRITE_MESSAGE('Error Sending For Approval: ' || sqlerrm);

         return -1;
   end F_SEND_ILR_NO_COMMIT;


   function F_SEND_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL
      RTN      number;
      ILR      number;
      WFS      number;
      SQLS     varchar2(32000);
      L_STATUS varchar2(2000);
      IS_AUTO  WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
   begin
      -- start by clearing our pend transaction
      RTN := F_SEND_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending For Approval: ' || L_STATUS);
         return -1;
      end if;

      --Call F_APPROVE_ILR for auto-approved assets
      L_STATUS := 'selecting if auto';
      select NVL(EXTERNAL_WORKFLOW_TYPE, 'NA')
        into IS_AUTO
        from WORKFLOW_TYPE A, LSR_ILR L
       where A.WORKFLOW_TYPE_ID = L.WORKFLOW_TYPE_ID
         and L.ILR_ID = A_ILR_ID;

      if IS_AUTO = 'AUTO' then
         L_STATUS := 'sending for approval';
         RTN      := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
         if RTN <> 1 then
            rollback;
            RAISE_APPLICATION_ERROR(-20000, 'Error Approving: ' || L_STATUS);
            return -1;
         end if;
      end if;

      commit;

      return 1;
	  
   exception
      when others then
         rollback;
         return -1;
   end F_SEND_ILR;
   
   --**************************************************************************
   --                            F_UNSEND_ILR
   --**************************************************************************

   function F_UNSEND_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin

      update LSR_ILR_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_UNSEND_ILR;

   --**************************************************************************
   --                            F_UNREJECT_ILR
   --**************************************************************************

   function F_UNREJECT_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_UNREJECT_ILR;
   
   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_ILR
   --**************************************************************************

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_ILR_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_ILR_ID)
                                        and ID_FIELD2 = TO_CHAR(A_REVISION)
                                        and Lower(SUBSYSTEM) = 'lessor_ilr_approval'),
                                     0)
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_UPDATE_WORKFLOW_ILR;

   --**************************************************************************
   --                            F_APPROVE_MLA
   --**************************************************************************

   function F_APPROVE_MLA(A_LEASE_ID in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this revision
      update LSR_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L
         set LEASE_STATUS_ID = 3, APPROVAL_DATE = sysdate, CURRENT_REVISION = A_REVISION
       where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving MLA');
         return -1;
   end F_APPROVE_MLA;

   --**************************************************************************
   --                            F_REJECT_MLA
   --**************************************************************************

   function F_REJECT_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_LEASE_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L set LEASE_STATUS_ID = 4 where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Rejecting MLA');
         return -1;
   end F_REJECT_MLA;

   --**************************************************************************
   --                            F_SEND_MLA
   --**************************************************************************

   function F_SEND_MLA(A_LEASE_ID in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN     number;
      IS_AUTO WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
   begin

      update LSR_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L set LEASE_STATUS_ID = 2 where L.LEASE_ID = A_LEASE_ID;

      --Reject other revisions
      update LSR_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 5, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) <> NVL(A_REVISION, 0)
         and APPROVAL_STATUS_ID in (1, 2);

      --Call F_APPROVE_MLA for auto-approved assets
      commit;

      select NVL(EXTERNAL_WORKFLOW_TYPE, 'NA')
        into IS_AUTO
        from WORKFLOW_TYPE A, LSR_LEASE L
       where A.WORKFLOW_TYPE_ID = L.WORKFLOW_TYPE_ID
         and L.LEASE_ID = A_LEASE_ID;

      if IS_AUTO = 'AUTO' then
         return F_APPROVE_MLA(A_LEASE_ID, A_REVISION);
      end if;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending MLA');
         return -1;
   end F_SEND_MLA;

   --**************************************************************************
   --                            F_UNREJECT_MLA
   --**************************************************************************

   function F_UNREJECT_MLA(A_LEASE_ID in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_LEASE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 7
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L set LEASE_STATUS_ID = 2 where L.LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Unrejecting MLA');
         return -1;
   end F_UNREJECT_MLA;

   --**************************************************************************
   --                            F_UNSEND_MLA
   --**************************************************************************

   function F_UNSEND_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_LEASE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LSR_LEASE L set LEASE_STATUS_ID = 1, APPROVAL_DATE = null where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error UnSending MLA');
         return -1;
   end F_UNSEND_MLA;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_MLA
   --**************************************************************************

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LSR_LEASE_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_LEASE_ID)
                                        and ID_FIELD2 = TO_CHAR(A_REVISION)
                                        and SUBSYSTEM = 'lessor_mla_approval'),
                                     0)
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Updating Workflow MLA');
         return -1;
   end F_UPDATE_WORKFLOW_MLA;
   
   --**************************************************************************
   --                            F_INVOICE_APPROVE
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will approve Lessor Invoices
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_INVOICE_APPROVE(A_COMPANY_ID in number,
                              A_MONTH      in date,
                              A_END_LOG    in number:=null) return varchar2 is
      L_STATUS varchar2(30000);
	  L_LOCATION varchar2(30000);
      L_RTN    number;
      L_COUNTER number;
      L_GL_JE_CODE   varchar2(35);
	  BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
	  COUNTER number;
	  TAX_PAYMENT_TYPE number;
	  L_ACCTS PKG_LEASE_BUCKET.BUCKET_ACCTS;
   begin
--    PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
--	PKG_PP_LOG.P_WRITE_MESSAGE('Begin Payment Journals Process - Company ID: ' || a_company_id || ' - Month: ' || to_char(a_month, 'mon-yyyy'));

--	/* CJS 3/16/15 Adding check to make sure everything is approved */
--	select count(1)
--	into L_COUNTER
--	from LSR_INVOICE LI, LSR_INVOICE_APPROVAL LIA
--	where LPH.PAYMENT_ID = LIA.INVOICE_ID
--	and LPH.COMPANY_ID = A_COMPANY_ID
--	and LPH.GL_POSTING_MO_YR = A_MONTH
--	and LPA.APPROVAL_STATUS_ID <> 3
--	and APPROVAL_TYPE_ID <> (select WORKFLOW_TYPE_ID from WORKFLOW_TYPE where lower(description) = 'auto approve');

--	if L_COUNTER <> 0 then
--     PKG_PP_LOG.P_WRITE_MESSAGE('Payments exist that need to be approved for this company and month');
--     PKG_PP_LOG.P_END_LOG();
--	   return 'Payments exist that need to be approved for this company and month';
--	end if;

--  PKG_PP_LOG.P_WRITE_MESSAGE('Sending Interim Interest Journals');
--	L_STATUS := F_PAYMENT_II(A_COMPANY_ID, A_MONTH);
--  if L_STATUS <> 'OK' THEN
--    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
--    PKG_PP_LOG.P_END_LOG();
--    return L_RTN;
--  end if;

--     select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LAMPAY'))
--      into L_GL_JE_CODE
--      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
--      where E.JE_ID = G.JE_ID
--      and G.PROCESS_ID = 'LAMPAY';


--	  PKG_PP_LOG.P_WRITE_MESSAGE('Principal and Interest Journals');
--      for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
--                                H.VENDOR_ID as VENDOR_ID,
--                                /* WMD ADJUSTMENT */
--                                CASE WHEN L.payment_type_id = 2 THEN nvl(ii.ii_amount,0) ELSE 0 END +
--                                NVL(L.AMOUNT,0) + NVL(L.ADJUSTMENT_AMOUNT,0) as PAYMENT_AMOUNT, --mc_bookje will reconvert based on rate
--                                A.WORK_ORDER_ID as WORK_ORDER_ID,
--                                A.COMPANY_ID as COMPANY_ID,
--                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
--                                M.INVOICE_ID as INVOICE_ID,
--                                case
--                                   when L.PAYMENT_TYPE_ID = 1 then
--                                    3018
--                                   when L.PAYMENT_TYPE_ID in (2) then
--                                    3019
--                                end as TRANS_TYPE,
--                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
--                                I.INT_ACCRUAL_ACCOUNT_ID as INT_ACCRUAL_ACCOUNT,
--								I.ap_account_id AS ap_account_id,
--                                h.rate,
--                                h.company_currency_id,
--                                H.contract_currency_id
--                           from V_LS_PAYMENT_HDR_FX H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M, LS_ILR_ACCOUNT I, LS_PAYMENT_APPROVAL LPA,
--                                (select la.ls_asset_id, -sum(nvl(ii.amount,0)) as ii_amount
--                                from LS_COMPONENT_MONTHLY_II_STG II, ls_asset la, ls_component lc
--                                where la.ls_asset_id = lc.ls_asset_id
--                                  and lc.component_id = ii.component_id
--                                  and la.company_id = A_COMPANY_ID
--                                  and II.MONTH = A_MONTH
--                                group by la.ls_asset_id ) II
--                          where H.GL_POSTING_MO_YR =  A_MONTH
--                            and H.PAYMENT_ID = L.PAYMENT_ID
--                            and H.PAYMENT_ID = M.PAYMENT_ID
--                            and case when L.PAYMENT_TYPE_ID = 2 then NVL(II.II_AMOUNT,0) else 0 end +
--                                NVL(L.AMOUNT,0) + NVL(L.ADJUSTMENT_AMOUNT,0) <> 0

--                            and L.LS_ASSET_ID = A.LS_ASSET_ID
--                            and A.ILR_ID = I.ILR_ID
--                            and A.COMPANY_ID = A_COMPANY_ID
--							              and L.PAYMENT_TYPE_ID in (1,2)
--                            and L.LS_ASSET_ID = II.LS_ASSET_ID (+)
--                     AND H.payment_id = lpa.payment_id
--                     AND H.ls_cur_type = 2
--                     --and nvl(LPA.REJECTED,0) <> 1
--                     and (LPA.APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select /* CJS Fix APPROVAL_TYPE_ID */WORKFLOW_TYPE_ID
--                                                               from WORKFLOW_TYPE
--                                                               where lower(description) = 'auto approve')
--                        and LPA.APPROVAL_STATUS_ID  = 1)))

--      loop
--         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
--                     to_char(l_payments.trans_type);
--         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
--                                               L_PAYMENTS.TRANS_TYPE,
--                                               L_PAYMENTS.PAYMENT_AMOUNT,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.WORK_ORDER_ID,
--                                               case
--												   when L_PAYMENTS.TRANS_TYPE in (3018) then
--													  -1
--												   when L_PAYMENTS.TRANS_TYPE = 3019 then
--													  L_PAYMENTS.INT_ACCRUAL_ACCOUNT
--												end,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.COMPANY_ID,
--                                               A_MONTH,
--                                               1,
--                                               L_GL_JE_CODE,
--                                               L_PAYMENTS.SET_OF_BOOKS_ID,
--                                               l_payments.rate,
--                                               l_payments.contract_currency_id,
--                                               l_payments.company_currency_id,
--                                               to_char(l_payments.invoice_id),
--                                               L_STATUS);
--         if L_RTN = -1 then
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
--            PKG_PP_LOG.P_END_LOG();
--            return L_LOCATION || ' : ' || L_STATUS;
--         end if;

--         -- process the credit.  Payment credits all hit 3022
--         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
--                     ' trans type: 3022';
--         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
--                                               3022,
--                                               L_PAYMENTS.PAYMENT_AMOUNT,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.WORK_ORDER_ID,
--                                               L_PAYMENTS.AP_ACCOUNT_ID,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.COMPANY_ID,
--                                               A_MONTH,
--                                               0,
--                                               L_GL_JE_CODE,
--                                               L_PAYMENTS.SET_OF_BOOKS_ID,
--                                               l_payments.rate,
--                                               l_payments.contract_currency_id,
--                                               l_payments.company_currency_id,
--                                               to_char(l_payments.invoice_id),
--                                               L_STATUS);
--         if L_RTN = -1 then
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
--            PKG_PP_LOG.P_END_LOG();
--            return L_LOCATION || ' : ' || L_STATUS;
--         end if;
--       L_COUNTER := L_COUNTER + 1;
--      end loop;

--	  --TAXES
--	  --get the tax bucket
--	  L_STATUS := 'Insert tax amounts on schedule';
--      select count(*)
--      into COUNTER
--      from LS_RENT_BUCKET_ADMIN
--      where TAX_EXPENSE_BUCKET =1;

--      if COUNTER <> 1 then
--         return 'Error: there must be exactly one bucket defined as the tax expense bucket';
--      end if;

--      select *
--      into BUCKET
--      from LS_RENT_BUCKET_ADMIN
--      where TAX_EXPENSE_BUCKET = 1;

--	  TAX_PAYMENT_TYPE := PKG_LEASE_BUCKET.F_GET_TAX_TYPE(BUCKET.RENT_TYPE,BUCKET.BUCKET_NUMBER);

--	  PKG_PP_LOG.P_WRITE_MESSAGE('Tax Payment Journals');
--	  for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
--                                H.VENDOR_ID as VENDOR_ID,
--                                /* WMD ADJUSTMENT */
--                                NVL(LMT.AMOUNT,0) + NVL(LMT.ADJUSTMENT_AMOUNT,0) as PAYMENT_AMOUNT, --Note that this is off the monthly tax table and not payment lines
--                                A.WORK_ORDER_ID as WORK_ORDER_ID,
--                                A.COMPANY_ID as COMPANY_ID,
--                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
--                                M.INVOICE_ID as INVOICE_ID,
--                                3045 as TRANS_TYPE,
--                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
--                                LTL.ACCRUAL_ACCT_ID as ACCRUAL_ACCOUNT,
--								LTL.EXPENSE_ACCT_ID as EXPENSE_ACCOUNT,
--								LTL.AP_ACCT_ID as AP_ACCOUNT,
--								ltl.tax_local_id AS tax_local_id,
--                                H.rate,
--                                H.contract_currency_id,
--                                h.company_currency_id
--                           from V_LS_PAYMENT_HDR_FX H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M, LS_ILR_ACCOUNT I,
--								LS_PAYMENT_APPROVAL LPA, LS_TAX_LOCAL LTL, LS_MONTHLY_TAX LMT
--                          where H.GL_POSTING_MO_YR = A_MONTH
--                            and H.PAYMENT_ID = L.PAYMENT_ID
--                            and H.PAYMENT_ID = M.PAYMENT_ID
--							and h.vendor_id = decode(lmt.vendor_id, -1, h.vendor_id, lmt.vendor_id)
--                            and NVL(LMT.AMOUNT,0) + NVL(LMT.ADJUSTMENT_AMOUNT,0) <> 0 /* WMD */
--                            and L.LS_ASSET_ID = A.LS_ASSET_ID
--                            and A.ILR_ID = I.ILR_ID
--                            and A.COMPANY_ID = A_COMPANY_ID
--							and L.PAYMENT_TYPE_ID = TAX_PAYMENT_TYPE
--							and LTL.TAX_LOCAL_ID = LMT.TAX_LOCAL_ID
--							and LMT.ACCRUAL = 0
--							and LMT.LS_ASSET_ID = L.LS_ASSET_ID
--							and LMT.GL_POSTING_MO_YR = L.GL_POSTING_MO_YR
--							and LMT.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
--							AND H.payment_id = lpa.payment_id
--              AND H.ls_cur_type = 2
--							--and nvl(LPA.REJECTED,0) <> 1
--							and (LPA.APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select /* CJS Fix APPROVAL_TYPE_ID */WORKFLOW_TYPE_ID
--																	   from WORKFLOW_TYPE
--																	   where lower(description) = 'auto approve')
--															and LPA.APPROVAL_STATUS_ID  = 1)))
--      loop
--         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
--                     to_char(l_payments.trans_type);
--         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
--                                               L_PAYMENTS.TRANS_TYPE,
--                                               L_PAYMENTS.PAYMENT_AMOUNT,
--                                               L_PAYMENTS.TAX_LOCAL_ID,
--                                               -1,
--                                               L_PAYMENTS.WORK_ORDER_ID,
--                                               L_PAYMENTS.ACCRUAL_ACCOUNT,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.COMPANY_ID,
--                                               A_MONTH,
--                                               1,
--                                               L_GL_JE_CODE,
--                                               L_PAYMENTS.SET_OF_BOOKS_ID,
--                                               l_payments.rate,
--                                               l_payments.contract_currency_id,
--                                               l_payments.company_currency_id,
--                                               to_char(l_payments.invoice_id),
--                                               L_STATUS);
--         if L_RTN = -1 then
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
--            PKG_PP_LOG.P_END_LOG();
--            return L_LOCATION || ' : ' || L_STATUS;
--         end if;

--         -- process the credit.  Payment credits all hit 3046
--         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
--                     ' trans type: 3046';
--         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
--                                               L_PAYMENTS.TRANS_TYPE + 1,
--                                               L_PAYMENTS.PAYMENT_AMOUNT,
--                                               L_PAYMENTS.TAX_LOCAL_ID,
--                                               -1,
--                                               L_PAYMENTS.WORK_ORDER_ID,
--                                               L_PAYMENTS.AP_ACCOUNT,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.COMPANY_ID,
--                                               A_MONTH,
--                                               0,
--                                               L_GL_JE_CODE,
--                                               L_PAYMENTS.SET_OF_BOOKS_ID,
--                                               l_payments.rate,
--                                               l_payments.contract_currency_id,
--                                               l_payments.company_currency_id,
--                                               to_char(l_payments.invoice_id),
--                                               L_STATUS);
--         if L_RTN = -1 then
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
--            PKG_PP_LOG.P_END_LOG();
--            return L_LOCATION || ' : ' || L_STATUS;
--         end if;
--       L_COUNTER := L_COUNTER + 1;
--      end loop;

--	  L_ACCTS := PKG_LEASE_BUCKET.F_GET_ACCTS;

--	  PKG_PP_LOG.P_WRITE_MESSAGE('Executory and Contingent Journals');
--	  for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
--                                H.VENDOR_ID as VENDOR_ID,
--                                /* WMD */
--                                NVL(L.AMOUNT,0) + NVL(L.ADJUSTMENT_AMOUNT,0) as PAYMENT_AMOUNT,
--                                A.WORK_ORDER_ID as WORK_ORDER_ID,
--                                A.COMPANY_ID as COMPANY_ID,
--                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
--                                M.INVOICE_ID as INVOICE_ID,
--                                case
--                                   when L.PAYMENT_TYPE_ID between 3 and 12 then
--                                    3020
--                                   when L.PAYMENT_TYPE_ID between 13 and 22 then
--                                    3021
--                                end as TRANS_TYPE,
--                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
--								I.ap_account_id AS ap_account_id,
--								L.payment_type_id AS payment_type_id,
--                                h.rate,
--                                h.contract_currency_id,
--                                h.company_currency_id
--                           from V_LS_PAYMENT_HDR_FX H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M, LS_ILR_ACCOUNT I,
--								LS_PAYMENT_APPROVAL LPA
--                          where H.GL_POSTING_MO_YR = A_MONTH
--                            and H.PAYMENT_ID = L.PAYMENT_ID
--                            and H.PAYMENT_ID = M.PAYMENT_ID
--                            and NVL(L.AMOUNT,0) + NVL(L.ADJUSTMENT_AMOUNT,0) <> 0 /* WMD */
--                            and L.LS_ASSET_ID = A.LS_ASSET_ID
--                            and A.ILR_ID = I.ILR_ID
--                            and A.COMPANY_ID = A_COMPANY_ID
--							AND L.payment_type_id BETWEEN 3 AND 22
--              AND H.ls_cur_type = 2
--                     and H.PAYMENT_ID = LPA.PAYMENT_ID
--                     --and nvl(LPA.REJECTED,0) <> 1
--                     and (LPA.APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select /* CJS Fix APPROVAL_TYPE_ID */WORKFLOW_TYPE_ID
--                                                               from WORKFLOW_TYPE
--                                                               where lower(description) = 'auto approve')
--                        and LPA.APPROVAL_STATUS_ID  = 1))
--					 )
--      loop

--		 --skip the taxes since we already did them
--		 if L_PAYMENTS.PAYMENT_TYPE_ID = TAX_PAYMENT_TYPE then
--			continue;
--		 end if;

--         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
--                     to_char(l_payments.trans_type);
--         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
--                                               L_PAYMENTS.TRANS_TYPE,
--                                               L_PAYMENTS.PAYMENT_AMOUNT,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.WORK_ORDER_ID,
--                                               L_ACCTS(L_PAYMENTS.PAYMENT_TYPE_ID).ACCRUAL_ACCT_ID,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.COMPANY_ID,
--                                               A_MONTH,
--                                               1,
--                                               L_GL_JE_CODE,
--                                               l_payments.set_of_books_id,
--                                               l_payments.rate,
--                                               l_payments.contract_currency_id,
--                                               l_payments.company_currency_id,
--                                               to_char(l_payments.invoice_id),
--                                               L_STATUS);
--         if L_RTN = -1 then
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
--            PKG_PP_LOG.P_END_LOG();
--            return L_LOCATION || ' : ' || L_STATUS||' '||sqlerrm;
--         end if;

--         -- process the credit.  Payment credits all hit 3022
--         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
--                     ' trans type: 3022';
--         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
--                                               3022,
--                                               L_PAYMENTS.PAYMENT_AMOUNT,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.WORK_ORDER_ID,
--                                               L_PAYMENTS.AP_ACCOUNT_ID,
--                                               0,
--                                               -1,
--                                               L_PAYMENTS.COMPANY_ID,
--                                               A_MONTH,
--                                               0,
--                                               L_GL_JE_CODE,
--                                               L_PAYMENTS.SET_OF_BOOKS_ID,
--                                               l_payments.rate,
--                                               l_payments.contract_currency_id,
--                                               l_payments.company_currency_id,
--                                               to_char(l_payments.invoice_id),
--                                               L_STATUS);
--         if L_RTN = -1 then
--            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
--            PKG_PP_LOG.P_END_LOG();
--            return L_LOCATION || ' : ' || L_STATUS;
--         end if;
--       L_COUNTER := L_COUNTER + 1;
--      end loop;

--     if L_COUNTER > 0 then
--      PKG_PP_LOG.P_WRITE_MESSAGE(L_COUNTER||' JE pairs sent.');
--     end if;

--     L_LOCATION:='Updating auto approvals to approved - auto status';
--     update ls_payment_hdr
--    set payment_status_id = 6
--    where gl_posting_mo_yr = A_MONTH
--      and lease_id in (select lease_id from ls_lease where lease_group_id in
--      (select lease_group_id from ls_lease_group where nvl(pymt_approval_flag,0) = 0))
--      and company_id = A_COMPANY_ID;


--	  if A_END_LOG=1 THEN
--        PKG_PP_LOG.P_END_LOG();
--    end if;

      return 'OK';
----     else
----	    PKG_PP_LOG.P_WRITE_MESSAGE('No JEs sent. Please confirm that payments have been approved.');
----      PKG_PP_LOG.P_END_LOG();
----      return 'No JEs sent. Please confirm that payments have been approved.';
----     end if;

--   exception
--      when others then
--         return L_STATUS || ' : ' || sqlerrm;
   end F_INVOICE_APPROVE;

   --**************************************************************************
   --                            F_SEND_INVOICE
   --**************************************************************************

   function F_SEND_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   BEGIN
   
      update LSR_INVOICE_APPROVAL set APPROVAL_STATUS_ID = 2 where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending Invoice');
         return -1;
   end F_SEND_INVOICE;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_INVOICE
   --**************************************************************************

   function F_UPDATE_WORKFLOW_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LSR_INVOICE_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_INVOICE_ID)
                                        and SUBSYSTEM = 'lsr_invoice_approval'),
                                     0)
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_UPDATE_WORKFLOW_INVOICE;

   --**************************************************************************
   --                            F_REJECT_INVOICE
   --**************************************************************************

   function F_REJECT_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_INVOICE_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Rejecting Invoice');
         return -1;
   end F_REJECT_INVOICE;

   --**************************************************************************
   --                            F_UNREJECT_INVOICE
   --**************************************************************************

   function F_UNREJECT_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_INVOICE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 7
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Unrejecting Invoice');
         return -1;
   end F_UNREJECT_INVOICE;

   --**************************************************************************
   --                            F_APPROVE_INVOICE
   --**************************************************************************

   function F_APPROVE_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this LSR_INVOICE_APPROVAL
      update LSR_INVOICE_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where INVOICE_ID = A_INVOICE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving Invoice');
         return -1;
   end F_APPROVE_INVOICE;

   --**************************************************************************
   --                            F_UNSEND_INVOICE
   --**************************************************************************

   function F_UNSEND_INVOICE(A_INVOICE_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LSR_INVOICE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where INVOICE_ID = A_INVOICE_ID;
       
      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error UnSending Invoice');
         return -1;
   end F_UNSEND_INVOICE;

END pkg_lessor_approval;
/

--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (4002, 0, 2017, 1, 0, 0, 0, 'C:\plasticwks\powerplant\sql\packages', '2017.1.0.0_PKG_LESSOR_APPROVAL.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 