/*
||============================================================================
|| Application: PowerPlant
|| File Name:   PKG_TBBS_ACCOUNT.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version	Date		 Revised By	 Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0 02/08/2016 Andrew Hill	Original Version
|| 2016.1.0.0 09/02/2016 Jared Watkins	Harvest package into base
|| 2016.1.0.0 09/22/2016 Andrew Hill	Changes to account setup process
|| 2016.1.0.0 09/23/2016 Andrew Hill  Automatically update TBBS GL Accounts on Balance pull
||============================================================================
*/
CREATE OR REPLACE PACKAGE pkg_tbbs_account AS
  G_PKG_VERSION varchar(35) := '2018.1.0.0';
  TYPE t_tbbs_vc2_tab_idx_pi IS TABLE OF VARCHAR2(4000) INDEX BY PLS_INTEGER;
  TYPE t_tbbs_num_tab_idx_pi IS TABLE OF NUMBER INDEX BY PLS_INTEGER;
  PROCEDURE p_setprocess;
  PROCEDURE p_clear_accounts;
  PROCEDURE p_load_book_accounts;
  PROCEDURE p_process_book_balances;
  PROCEDURE p_delete_child_accounts(a_primary_accounts t_tbbs_vc2_tab_idx_pi);
  PROCEDURE p_delete_child_accounts(a_primary_account VARCHAR2);
  FUNCTION f_num_accounts_assigned_field(field_id NUMBER) RETURN NUMBER;
  PROCEDURE p_remove_accts_assigned_fields(a_field_ids t_tbbs_num_tab_idx_pi);
  PROCEDURE p_remove_accts_assigned_field(a_field_id NUMBER);
  FUNCTION f_num_duplicate_accounts RETURN NUMBER;
  
END pkg_tbbs_account;
/

CREATE OR REPLACE PACKAGE BODY pkg_tbbs_account AS
  
--**************************************************************************
--       VARIABLES
--**************************************************************************
  g_process_id pp_processes.process_id%TYPE;
  g_msg VARCHAR2(254);
--**************************************************************************
--       PROCEDURES
--**************************************************************************
  PROCEDURE p_setprocess
  IS
  BEGIN
    g_msg := 'Unable to find the Process ID for "TBBS - PROCESS BOOK ACCOUNTS" in pp_processes';
    SELECT process_id
    INTO g_process_id
    FROM pp_processes
    WHERE upper(description) = 'TBBS - PROCESS BOOK ACCOUNTS';
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error(- 20000, g_msg || ': ' || SQLERRM);
  END p_setprocess;
--**********************************************************************************************************************************************
--**********************************************************************************************************************************************
  
  PROCEDURE p_clear_accounts
  IS
  BEGIN
    PKG_TBBS_ACCOUNT.P_SETPROCESS;
    PKG_PP_LOG.P_START_LOG(G_PROCESS_ID);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.P_CLEAR_ACCOUNTS');
    g_msg := 'Clearing tbbs_book_reclass_doc_relate';
    DELETE
    FROM tbbs_book_reclass_doc_relate;
    G_MSG := 'Clearing tbbs_book_reclass';
    DELETE
    FROM tbbs_book_reclass;
    G_MSG := 'Clearing tbbs_book_adjust_doc_relate';
    DELETE
    FROM tbbs_book_adjust_doc_relate;
    G_MSG := 'Clearing tbbs_book_adjust';
    DELETE
    FROM tbbs_book_adjust;
    G_MSG := 'Clearing tbbs_documents for documents associated only with book accounts associated with given primary accounts';
    DELETE
    FROM tbbs_document A
    WHERE NOT EXISTS (SELECT document_id
                      FROM tbbs_book_reclass_doc_relate b
                      WHERE A.document_id   = b.document_id
                      UNION ALL
                      SELECT document_id
                      FROM tbbs_book_adjust_doc_relate b
                      WHERE A.document_id   = b.document_id
                      UNION ALL
                      SELECT document_id
                      FROM tbbs_tax_adjust_doc_relate b
                      WHERE A.document_id   = b.document_id
                      );
    G_MSG := 'Clearing tbbs_book_balance';
    DELETE FROM tbbs_book_balance;
    G_MSG := 'Clearing tbbs_book_assign';
    DELETE FROM tbbs_book_assign;
    G_MSG := 'Clearing tbbs_unused_book_account';
    DELETE FROM tbbs_unused_book_account;
    G_MSG := 'Clearing tbbs_book_account_value';
    DELETE FROM tbbs_book_account_value;
    G_MSG := 'Clearing tbbs_book_account';
    DELETE FROM tbbs_book_account;
    G_MSG := 'Clearing tbbs_account_field_assign';
    DELETE FROM tbbs_account_field_assign;
    -- Error handling using PKG_PP_ERROR
    PKG_PP_ERROR.REMOVE_MODULE_NAME;
    EXCEPTION
    WHEN OTHERS THEN
    pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
END P_CLEAR_ACCOUNTS;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  PROCEDURE p_delete_child_accounts(a_primary_accounts t_tbbs_vc2_tab_idx_pi)
  IS
  acct_list t_varchar2_array := t_varchar2_array();
  BEGIN
    pkg_tbbs_account.p_setprocess;
    pkg_pp_log.p_start_log(g_process_id);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.P_DELETE_CHILD_ACCOUNTS');
    
    g_msg := 'Upcasing and Copying all account strings into nested table for use in SQL engine';
    acct_list.extend(a_primary_accounts.count);
    FOR i IN a_primary_accounts.FIRST..a_primary_accounts.LAST loop
      acct_list(i) := upper(a_primary_accounts(i));
    END LOOP;
    g_msg := 'Clearing tbbs_book_reclass_doc_relate for book accounts associated with given primary accounts';
    DELETE
    FROM tbbs_book_reclass_doc_relate a
    WHERE EXISTS
    (SELECT 1
     FROM tbbs_book_reclass b
     join tbbs_book_account tba ON (b.src_book_account_id = tba.book_account_id OR b.dst_book_account_id = tba.book_account_id)
     WHERE a.book_reclass_id = b.book_reclass_id
     AND upper(tba.primary_account) MEMBER OF acct_list);
    
    g_msg := 'Clearing tbbs_book_reclass for book accounts associated with given primary accounts';
    DELETE
    FROM tbbs_book_reclass a
    WHERE EXISTS
    (SELECT book_account_id
     FROM tbbs_book_account b
     WHERE upper(b.primary_account) MEMBER OF acct_list
     AND (a.src_book_account_id = b.book_account_id OR a.dst_book_account_id = b.book_account_id)
     );
    g_msg := 'Clearing tbbs_book_adjust_doc_relate for book accounts associated with given primary accounts';
    DELETE
    FROM tbbs_book_adjust_doc_relate a
    WHERE EXISTS
    (SELECT 1
     FROM tbbs_book_adjust b
     join tbbs_book_account tba ON (b.dr_book_account_id = tba.book_account_id OR b.cr_book_account_id = tba.book_account_id)
     WHERE a.book_adjust_id = b.book_adjust_id
     AND upper(tba.primary_account) MEMBER OF acct_list
     );
    g_msg := 'Clearing tbbs_book_adjust for book accounts associated with given primary accounts';
    DELETE
    FROM tbbs_book_adjust a
    WHERE EXISTS
    (SELECT book_account_id
     FROM tbbs_book_account b
     WHERE upper(b.primary_account) MEMBER OF acct_list
     AND (a.cr_book_account_id = b.book_account_id OR a.dr_book_account_id = b.book_account_id)
     );
    g_msg := 'Clearing tbbs_documents for documents associated only with book accounts associated with given primary accounts';
    DELETE
    FROM tbbs_document a
    WHERE NOT EXISTS
    (SELECT document_id
     FROM tbbs_book_reclass_doc_relate b
     WHERE a.document_id = b.document_id
     UNION ALL
     SELECT document_id
     FROM tbbs_book_adjust_doc_relate b
     WHERE a.document_id = b.document_id
     UNION ALL
     SELECT document_id
     FROM tbbs_tax_adjust_doc_relate b
     WHERE a.document_id = b.document_id
     );
    g_msg := 'Clearing tbbs_book_balance for book accounts associated with given primary accounts';
    DELETE
    FROM tbbs_book_balance a
    WHERE EXISTS
    (SELECT book_account_id
     FROM tbbs_book_account b
     WHERE a.book_account_id = b.book_account_id
     AND upper(b.primary_account) MEMBER OF acct_list
     );
    g_msg := 'Clearing tbbs_book_assign for book accounts associated with given primary accounts';
    DELETE
    FROM tbbs_book_assign a
    WHERE EXISTS
    (SELECT book_account_id
     FROM tbbs_book_account b
     WHERE a.book_account_id = b.book_account_id
     AND upper(b.primary_account) MEMBER OF acct_list
     );
    g_msg := 'Clearing tbbs_unused_book_account for book accounts associated with given primary accounts';
    DELETE
    FROM tbbs_unused_book_account a
    WHERE EXISTS
    (SELECT book_account_id
     FROM tbbs_book_account b
     WHERE a.book_account_id = b.book_account_id
     AND upper(b.primary_account) MEMBER OF acct_list
     );
    g_msg := 'Clearing tbbs_book_account_value for book accounts associated with given primary accounts';
    DELETE
    FROM tbbs_book_account_value a
    WHERE EXISTS
    (SELECT 1
     FROM tbbs_book_account b
     WHERE a.book_account_id = b.book_account_id
     AND upper(b.primary_account) MEMBER OF acct_list
     );
    g_msg := 'Clearing tbbs_book_account for book accounts associated with primary accounts';
    DELETE
    FROM tbbs_book_account
    WHERE upper(primary_account) MEMBER OF acct_list;
-- Error handling using PKG_PP_ERROR
    pkg_pp_error.remove_module_name;
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
  END p_delete_child_accounts;
--------------------------------------------------------------------------------
  PROCEDURE p_delete_child_accounts(a_primary_account VARCHAR2) IS
  accounts t_tbbs_vc2_tab_idx_pi;
  BEGIN
    accounts(1) := a_primary_account;
    p_delete_child_accounts(accounts);
  END p_delete_child_accounts;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  FUNCTION f_num_accounts_assigned_field(field_id NUMBER)
  RETURN NUMBER
  IS
  cnt NUMBER(22, 0);
  BEGIN
    pkg_tbbs_account.p_setprocess;
    pkg_pp_log.p_start_log(g_process_id);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.F_NUM_ACCTS_ASSIGNED_FIELD');
    g_msg := 'Selecting count of accounts assigned field_id:' || field_id || ' from tbbs_account_field_assign';
    SELECT COUNT(1)
    INTO cnt
    FROM tbbs_account_field_assign
    WHERE account_field_id = field_id;
    pkg_pp_error.remove_module_name;
    RETURN cnt;
-- Error handling using PKG_PP_ERROR
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
      RETURN - 1;
  END f_num_accounts_assigned_field;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  PROCEDURE p_remove_accts_assigned_fields(a_field_ids t_tbbs_num_tab_idx_pi)
  IS
  field_ids t_num_array := t_num_array();
  BEGIN
    pkg_tbbs_account.p_setprocess;
    pkg_pp_log.p_start_log(g_process_id);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.P_REMOVE_ACCTS_ASSIGNED_FIELD');
    
    g_msg := 'Copying associated array into nested table for use in SQL engine.';
    field_ids.extend(a_field_ids.count);
    FOR i IN a_field_ids.first..a_field_ids.last LOOP
      field_ids(i) := a_field_ids(i);
    END LOOP;
    
    g_msg := 'Deleting from tbbs_book_reclass_doc_relate for book accounts assigned given field_ids';
    DELETE
    FROM tbbs_book_reclass_doc_relate a
    WHERE EXISTS
    (SELECT 1
     FROM tbbs_book_reclass b
     join tbbs_book_account tba ON (b.src_book_account_id = tba.book_account_id OR b.dst_book_account_id = tba.book_account_id)
     join tbbs_account_field_assign tafa ON tba.primary_account = tafa.primary_account
     WHERE a.book_reclass_id = b.book_reclass_id
     AND tafa.account_field_id MEMBER OF field_ids
     );
    g_msg := 'Deleting from tbbs_book_reclass for book accounts associated with given field_ids';
    DELETE
    FROM tbbs_book_reclass a
    WHERE EXISTS
    (SELECT 1
     FROM tbbs_book_account tba
     join tbbs_account_field_assign tafa ON tba.primary_account = tafa.primary_account
     WHERE tafa.account_field_id MEMBER OF field_ids
     AND (a.src_book_account_id = tba.book_account_id OR a.dst_book_account_id = tba.book_account_id)
     );
    g_msg := 'Clearing tbbs_book_adjust_doc_relate for book accounts associated with given field_ids';
    DELETE
    FROM tbbs_book_adjust_doc_relate a
    WHERE EXISTS
    (SELECT 1
     FROM tbbs_book_adjust b
     join tbbs_book_account tba ON (b.dr_book_account_id = tba.book_account_id OR b.cr_book_account_id = tba.book_account_id)
     join tbbs_account_field_assign tafa ON tba.primary_account = tafa.primary_account
     WHERE a.book_adjust_id = b.book_adjust_id
     AND tafa.account_field_id MEMBER OF field_ids
     );
    g_msg := 'Clearing tbbs_book_adjust for book accounts associated with given field_ids';
    DELETE
    FROM tbbs_book_adjust a
    WHERE EXISTS
    (SELECT book_account_id
     FROM tbbs_book_account b
     join tbbs_account_field_assign tafa ON b.primary_account = tafa.primary_account
     WHERE tafa.account_field_id MEMBER OF field_ids
     AND (a.cr_book_account_id = b.book_account_id OR a.dr_book_account_id = b.book_account_id)
     );
    g_msg := 'Clearing tbbs_documents for documents associated only with book accounts associated with given field_ids';
    DELETE
    FROM tbbs_document a
    WHERE NOT EXISTS
    (SELECT document_id
     FROM tbbs_book_reclass_doc_relate b
     WHERE a.document_id = b.document_id
     UNION ALL
     SELECT document_id
     FROM tbbs_book_adjust_doc_relate b
     WHERE a.document_id = b.document_id
     UNION ALL
     SELECT document_id
     FROM tbbs_tax_adjust_doc_relate b
     WHERE a.document_id = b.document_id
     );
    g_msg := 'Clearing tbbs_book_balance';
    DELETE FROM tbbs_book_balance;
    g_msg := 'Deleting from tbbs_book_assign for book accounts assigned given field_ids';
    DELETE
    FROM tbbs_book_assign a
    WHERE EXISTS
    (SELECT 1
     FROM tbbs_book_account tba
     join tbbs_account_field_assign tafa
     ON tba.primary_account = tafa.primary_account
     WHERE tba.book_account_id = a.book_account_id
     AND tafa.account_field_id MEMBER OF field_ids
     );
    g_msg := 'Deleting from tbbs_book_assign for book accounts assigned given field_ids';
    DELETE
    FROM tbbs_unused_book_account a
    WHERE EXISTS
    (SELECT 1
     FROM tbbs_book_account tba
     join tbbs_account_field_assign tafa
     ON tba.primary_account = tafa.primary_account
     WHERE tba.book_account_id = a.book_account_id
     AND tafa.account_field_id MEMBER OF field_ids
     );
    g_msg := 'Deleting from tbbs_book_account_value for book accounts assigned given field_ids';
    DELETE
    FROM tbbs_book_account_value a
    WHERE account_field_id MEMBER OF field_ids
    OR EXISTS (SELECT 1
               FROM tbbs_book_account tba
               join tbbs_account_field_assign tafa ON tba.primary_account = tafa.primary_account
               WHERE a.book_account_id = tba.book_account_id
               AND tafa.account_field_id MEMBER OF field_ids); 
    g_msg := 'Deleting from tbbs_book_account for book accounts assigned given field_ids';
    DELETE
    FROM tbbs_book_account a
    WHERE EXISTS
    (SELECT 1
     FROM tbbs_book_account tba
     join tbbs_account_field_assign tafa
     ON upper(tba.primary_account) = upper(tafa.primary_account)
     WHERE tba.book_account_id = a.book_account_id
     AND tafa.account_field_id MEMBER OF field_ids
     );
    g_msg := 'Deleting assignments from tbbs_account_field_assign for given field_ids';
    DELETE FROM tbbs_account_field_assign WHERE account_field_id MEMBER OF field_ids;
-- Error handling using PKG_PP_ERROR
    pkg_pp_error.remove_module_name;
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
  END p_remove_accts_assigned_fields;
--------------------------------------------------------------------------------
  PROCEDURE p_remove_accts_assigned_field(a_field_id NUMBER) IS
  ids t_tbbs_num_tab_idx_pi;
  BEGIN
    ids(1) := a_field_id;
    p_remove_accts_assigned_fields(ids);
  END p_remove_accts_assigned_field;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  FUNCTION f_get_balance_source
  RETURN CLOB
  IS
  balance_source CLOB;
  BEGIN
    pkg_tbbs_account.p_setprocess;
    pkg_pp_log.p_start_log(g_process_id);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.F_GET_BALANCE_SOURCE');
    WITH sources AS
    (SELECT 'query' AS src,
     CASE lower(table_name)
       WHEN 'dynamic view' THEN
         '(' || to_clob(SQL) || to_clob(sql2) || to_clob(sql3) || to_clob(sql4) || to_clob(sql5) || to_clob(sql6) || ')'
       ELSE
         to_clob(table_name)
     END
     AS src_stmt
     FROM pp_any_query_criteria
     WHERE id =
     (SELECT control_value
      FROM tbbs_system_control
      WHERE upper(control_name) = 'BALANCE TABLE QUERY ID'
      )
     UNION ALL
     SELECT 'table' AS src, to_clob(control_value) AS src_stmt
     FROM tbbs_system_control
     WHERE upper(control_name) = 'BALANCE TABLE NAME'
     )
    SELECT src_stmt
    INTO balance_source
    FROM sources a
    join
    (SELECT control_value AS src
     FROM tbbs_system_control tsc
     WHERE upper(tsc.control_name) = 'BALANCE SOURCE'
     ) b
    ON upper(a.src) = upper(b.src);
    pkg_pp_error.remove_module_name;
    RETURN balance_source;
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
  END f_get_balance_source;
  FUNCTION f_get_account_id_column
  RETURN VARCHAR2
  AS
  account_col VARCHAR2(30);
  BEGIN
    pkg_tbbs_account.p_setprocess;
    pkg_pp_log.p_start_log(g_process_id);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.F_GET_ACCOUNT_ID_COLUMN');
    g_msg := 'Getting ACCOUNT ID COLUMN from tbbs_account_stg_tmp';
    SELECT control_value
    INTO account_col
    FROM tbbs_system_control
    WHERE upper(control_name) = 'ACCOUNT ID COLUMN';
    IF account_col IS NULL THEN
      g_msg := g_msg || ' - Invalid tbbs_system_control entry for Account ID Column';
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
    END IF;
    pkg_pp_error.remove_module_name;
    RETURN account_col;
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
      RETURN '';
  END f_get_account_id_column;
-- refactored
  FUNCTION f_get_account_description_col
  RETURN VARCHAR2
  AS
  account_description_col VARCHAR2(30);
  BEGIN
    pkg_tbbs_account.p_setprocess;
    pkg_pp_log.p_start_log(g_process_id);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.F_GET_ACCOUNT_DESCRIPTION_COL');
    g_msg := 'Getting ACCOUNT DESCRIPTION COLUMN from tbbs_account_stg_tmp';
    SELECT control_value
    INTO account_description_col
    FROM tbbs_system_control
    WHERE upper(control_name) = 'ACCOUNT DESCRIPTION COLUMN';
    IF account_description_col IS NULL THEN
      g_msg := g_msg || ' - Invalid tbbs_system_control entry for Account Description Column';
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
    END IF;
    pkg_pp_error.remove_module_name;
    RETURN account_description_col;
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
      RETURN '';
  END f_get_account_description_col;
-- refactored
  FUNCTION f_get_bal_table_acct_id_col
  RETURN VARCHAR2
  AS
  balance_account_col VARCHAR2(30);
  BEGIN
    pkg_tbbs_account.p_setprocess;
    pkg_pp_log.p_start_log(g_process_id);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.F_GET_BAL_TABLE_ACCT_ID_COL');
    g_msg := 'Getting BALANCE TABLE ACCOUNT ID COLUMN from tbbs_account_stg_tmp';
    SELECT control_value
    INTO balance_account_col
    FROM tbbs_system_control
    WHERE upper(control_name) = 'BALANCE TABLE ACCOUNT ID COLUMN';
    IF balance_account_col IS NULL THEN
      g_msg := g_msg || ' - Invalid tbbs_system_control entry for Balance Table Account ID Column';
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
    END IF;
    pkg_pp_error.remove_module_name;
    RETURN balance_account_col;
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
      RETURN '';
  END f_get_bal_table_acct_id_col;
-- refactored
  FUNCTION f_check_account_column_exists
  RETURN BOOLEAN
  AS
  cnt NUMBER;
  balance_account_col VARCHAR2(30);
  BEGIN
    pkg_tbbs_account.p_setprocess;
    pkg_pp_log.p_start_log(g_process_id);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.F_CHECK_ACCOUNT_COLUMN_EXISTS');
    g_msg := 'Getting Account Field count';
    balance_account_col := f_get_bal_table_acct_id_col;
    SELECT COUNT(1)
    INTO cnt
    FROM tbbs_account_field
    WHERE upper(account_field) = upper(balance_account_col);
    IF cnt < 1 THEN
      g_msg := ' - Account column not in tbbs_account_field';
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
      RETURN FALSE;
    END IF;
    pkg_pp_error.remove_module_name;
    RETURN TRUE;
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
      RETURN FALSE;
  END f_check_account_column_exists;
-- refactored
  FUNCTION f_get_account_source
  RETURN CLOB
  AS
  account_source CLOB;
  BEGIN
    pkg_tbbs_account.p_setprocess;
    pkg_pp_log.p_start_log(g_process_id);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.F_GET_ACCOUNT_SOURCE');
    WITH sources AS
    (SELECT 'query' AS src,
     CASE lower(table_name)
       WHEN 'dynamic view' THEN
         '(' || to_clob(SQL) || to_clob(sql2) || to_clob(sql3) || to_clob(sql4) || to_clob(sql5) || to_clob(sql6) || ')'
       ELSE
         to_clob(table_name)
     END
     AS src_stmt
     FROM pp_any_query_criteria
     WHERE id =
     (SELECT control_value
      FROM tbbs_system_control
      WHERE upper(control_name) = 'ACCOUNT TABLE QUERY ID'
      )
     UNION ALL
     SELECT 'table' AS src, to_clob(control_value) AS src_stmt
     FROM tbbs_system_control
     WHERE upper(control_name) = 'ACCOUNT TABLE NAME'
     )
    SELECT src_stmt
    INTO account_source
    FROM sources a
    join
    (SELECT control_value AS src
     FROM tbbs_system_control tsc
     WHERE upper(tsc.control_name) = 'ACCOUNT SOURCE'
     ) b
    ON upper(a.src) = upper(b.src);
    pkg_pp_error.remove_module_name;
    RETURN account_source;
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
      RETURN '';
  END f_get_account_source;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  PROCEDURE p_load_book_accounts
  IS
  in_field_list CLOB;
  select_field_list CLOB;
  balance_source CLOB;
  insert_statement CLOB;
  balance_account_col VARCHAR2(30);
  account_source CLOB;
  account_col VARCHAR2(30);
  account_description_col VARCHAR2(30);
  new_id NUMBER(22, 0);
  col_exists BOOLEAN;
  BEGIN
    pkg_tbbs_account.p_setprocess;
    pkg_pp_log.p_start_log(g_process_id);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.P_LOAD_BOOK_ACCOUNTS');
--We want to speed things up with materialized views, if possible
    g_msg := 'Altering session to allow query rewrite';
    EXECUTE IMMEDIATE 'alter session set QUERY_REWRITE_ENABLED = TRUE';
--We don't need old staging values
    g_msg := 'Clearing tbbs_account_stg_tmp';
    EXECUTE IMMEDIATE 'delete from tbbs_account_stg_tmp';
--Grab the user choices for columns, tables, queries, etc.
    account_col := f_get_account_id_column();
    account_description_col := f_get_account_description_col();
    balance_account_col := f_get_bal_table_acct_id_col();
    col_exists := f_check_account_column_exists();
    g_msg := 'Getting list of fields from tbbs_account_field';
    SELECT listagg(account_field, ', ') within GROUP (
                                                      ORDER BY account_field)
    INTO in_field_list
    FROM tbbs_account_field;
    g_msg := 'Getting list of fields to be selected from tbbs_account_field';
    SELECT listagg('to_char('
                   || account_field
                   || ') as '
                   || account_field, ', ') within GROUP (
                                                         ORDER BY account_field)
    INTO select_field_list
    FROM tbbs_account_field;
    balance_source := f_get_balance_source();
    account_source := f_get_account_source();
--Primary account must be an assigned field for all accounts
    g_msg := 'Ensuring primary account field is assigned to all accounts';
    EXECUTE IMMEDIATE 'INSERT INTO tbbs_account_field_assign(primary_account, account_field_id)      
WITH missing_accounts AS       
(SELECT DISTINCT upper(to_char(account)) as account FROM (SELECT ' || balance_account_col
    || ' as account FROM ' || balance_source || ') MINUS ' || 'SELECT upper(primary_account) as account ' || 'FROM (tbbs_account_field_assign tafa ' ||
    'JOIN tbbs_account_field taf on tafa.account_field_id = taf.account_field_id )' || 'WHERE upper(taf.account_field) = ''' || upper(balance_account_col) || ''') ' ||
    'SELECT account, account_field_id ' || 'FROM missing_accounts ' || 'cross join (select account_field_id ' || 'from tbbs_account_field ' ||
    'where upper(account_field) = ''' || upper(balance_account_col) || ''')';
--Account/Account2 business because we want to both piviot the account field AND select it
    g_msg := 'Inserting account information into tbbs_account_stg_tmp';
    EXECUTE IMMEDIATE 'INSERT INTO tbbs_account_stg_tmp(primary_account, account_field, field_value, grp, primary_description) ' ||
    'with field_vals as (SELECT upper(account2) as account, upper(field) as field, ' || 'upper(field_value) as field_value, grp ' || 'FROM (SELECT rownum as grp, account2, ' ||
    select_field_list || ' FROM (SELECT distinct to_char(' || balance_account_col || ') as account2, ' || select_field_list || ' FROM ' || balance_source || ')) ' || 
    'UNPIVOT INCLUDE NULLS (field_value FOR field IN (' || in_field_list || ')) a' || ' JOIN tbbs_account_field_assign b on upper(a.account2) = upper(b.primary_account) ' ||
    'JOIN tbbs_account_field c on b.account_field_id = c.account_field_id ' || '        and upper(a.field) = upper(c.account_field)),' ||
    'group_vals as (SELECT grp, listagg(account || field || field_value, '','') within group (order by null) as vals ' || 'FROM field_vals ' ||
    'GROUP BY grp), ' || 'accts as (SELECT to_char(a_src.' || account_description_col || ') as description, to_char(a_src.' || account_col || ') as account ' || ' FROM ' ||
    account_source || ' a_src JOIN (SELECT upper(to_char(account)) as account FROM (SELECT DISTINCT ' || balance_account_col || ' as account FROM ' || balance_source 
    || ')) b_src ON ' || 'upper(to_char(a_src.' || account_col || ')) = b_src.account' ||
    ') ' || 'SELECT a.account, a.field, a.field_value, a.grp, nvl(b.description, a.account) as description ' || ' FROM field_vals a' || ' LEFT JOIN accts b on a.account = b.account' ||
    ' WHERE grp in (SELECT grp ' || 'FROM (SELECT grp, row_number() over (partition by vals order by null) as rn ' || 'FROM group_vals) ' || 'WHERE rn = 1)';

    g_msg := 'Deferring constraints';
    EXECUTE IMMEDIATE 'set constraint all deferred';
    g_msg := 'Finding next book_account_id';
    new_id := tbbs_book_account_seq.nextval;
    g_msg := 'Merging book account field values into tbbs_book_account_value';
    merge INTO tbbs_book_account_value a USING
    (WITH existing_groups AS
     (SELECT tba1.book_account_id, listagg(upper(tba1.primary_account)
                                           || upper(taf1.account_field)
                                           || upper(tbav1.field_value), ',') within GROUP (
                                                                                           ORDER BY tba1.primary_account, upper(account_field), upper(field_value)) AS combo
      FROM tbbs_book_account_value tbav1
      join tbbs_account_field taf1
      ON tbav1.account_field_id = taf1.account_field_id
      join tbbs_book_account tba1
      ON tbav1.book_account_id = tba1.book_account_id
      GROUP BY tba1.book_account_id
      )
     SELECT DISTINCT tas.account_field, tas.field_value, tas.grp, taf.account_field_id, z.existing_book_account
     FROM tbbs_account_stg_tmp tas
     join tbbs_account_field taf
     ON upper(tas.account_field) = upper(taf.account_field)
     left join
     (SELECT y.book_account_id AS existing_book_account, x.grp AS grp
      FROM
      (SELECT grp, listagg(primary_account
                           || upper(account_field)
                           || upper(field_value), ',') within GROUP (
                                                                     ORDER BY primary_account, upper(account_field), upper(field_value)) AS combo
       FROM tbbs_account_stg_tmp tas1
       GROUP BY grp
       ) x
      join existing_groups y
      ON x.combo = y.combo
      ) z ON tas.grp = z.grp
     ) b ON (a.account_field_id = b.account_field_id AND upper(a.field_value) = upper(b.field_value) AND a.book_account_id = existing_book_account)
    WHEN NOT matched THEN
      INSERT
      (
       book_account_id, account_field_id, field_value
       )
      VALUES
      (
       b.grp + new_id, b.account_field_id, b.field_value
       );
      g_msg := 'Merging book accounts into tbbs_book_account';
      EXECUTE IMMEDIATE
      'MERGE INTO tbbs_book_account A 
USING (SELECT DISTINCT tbav.book_account_id, tas.primary_account, tas.primary_description, ' ||
      'listagg(decode(upper(taf.account_field), upper(''' || balance_account_col ||
      '''), tbav.field_value, taf.abbreviation || tbav.field_value), '''') ' ||
      'WITHIN GROUP (ORDER BY taf.sort_order, upper(taf.account_field)) AS description, ' || 'listagg(decode(upper(taf.account_field), upper(''' || balance_account_col ||
      '''), '''', taf.abbreviation || tbav.field_value), '', '') ' ||
      'WITHIN GROUP (ORDER BY taf.sort_order, upper(taf.account_field)) AS additional_field_description ' || 'FROM tbbs_book_account_value tbav ' ||
      'JOIN tbbs_account_field taf ON tbav.account_field_id = taf.account_field_id ' || 'JOIN tbbs_account_stg_tmp tas ON upper(taf.account_field) = upper(tas.account_field) '
      || 'AND upper(tbav.field_value) = upper(tas.field_value) ' || 'AND tbav.book_account_id = tas.grp +' || new_id || ' ' ||
      'GROUP BY tas.primary_account, tbav.book_account_id, tas.primary_description) b ' || 'ON (A.book_account_id = b.book_account_id) ' ||
      'WHEN NOT MATCHED THEN ' ||
      'INSERT(book_account_id, description, primary_account, primary_description, additional_field_description)  
VALUES(b.book_account_id, b.description, b.primary_account, b.primary_description, b.additional_field_description)';
      g_msg := 'Merging Book Account Description Changes';
      EXECUTE IMMEDIATE
      'MERGE INTO tbbs_book_account A ' ||
      'USING ( SELECT  book_account_id, ' ||
      'listagg(decode(upper(taf.account_field), upper(''' || balance_account_col || '''), ' ||
      'tbav.field_value, taf.abbreviation || tbav.field_value), '''') ' ||
      'WITHIN GROUP (ORDER BY taf.sort_order, upper(taf.account_field)) AS description, ' ||
      'listagg(decode(upper(taf.account_field), upper(''' || balance_account_col || '''), ' || 
      ''''', taf.abbreviation || tbav.field_value), '', '') ' ||
      'WITHIN GROUP (ORDER BY taf.sort_order, upper(taf.account_field)) AS additional_field_description ' ||
      'FROM tbbs_book_account_value tbav ' ||
      'JOIN tbbs_account_field taf ON tbav.account_field_id = taf.account_field_id ' ||
      'GROUP BY tbav.book_account_id) b ON (A.book_account_id = b.book_account_id) ' ||
      'WHEN MATCHED THEN ' ||
      'UPDATE SET A.description = b.description, A.additional_field_description = b.additional_field_description ' ||
      'WHERE a.description <> b.description or a.additional_field_description <> b.additional_field_description';
      g_msg := 'Merging Primary Account Description Changes';
      EXECUTE IMMEDIATE
      'MERGE INTO tbbs_book_account A ' ||
      'USING (SELECT ' || account_col || ' as account, ' || account_description_col || ' as description ' ||
      'FROM ' || account_source || ') b on (upper(a.primary_account) = upper(b.account)) ' ||
      'WHEN MATCHED THEN ' ||
      'UPDATE SET a.primary_description = b.description ' ||
      'WHERE a.primary_description <> b.description';
      g_msg := 'Clearing tbbs_account_stg_tmp';
      EXECUTE IMMEDIATE 'delete from tbbs_account_stg_tmp';
-- Error handling using PKG_PP_ERROR
      pkg_pp_error.remove_module_name;
    EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
  END p_load_book_accounts;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  PROCEDURE p_process_book_balances
  IS
  full_statement CLOB;
  select_list CLOB;
  balance_source CLOB;
  grouping_clause CLOB;
  having_clause CLOB;
  join_clause CLOB;
  month_column VARCHAR2(30);
  company_column VARCHAR2(30);
  amount_column VARCHAR2(30);
  account_id_column VARCHAR2(30);
  magic_null_string VARCHAR2(20);
  BEGIN
    pkg_tbbs_account.p_setprocess;
    pkg_pp_log.p_start_log(g_process_id);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.P_PROCESS_BOOK_BALANCES');
--Because we would like to join tables based on a set of columns, and would like the join to succeed if
--if the column is null in both tables, we will use and NVL and this "magic" string to test equality
--could cause an issue if "$MAGICNULLSTRING$" appears as valid data in a table
    magic_null_string := '$MAGICNULLSTRING$';
--We want to speed things up with materialized views, if possible
    g_msg := 'Altering session to allow query rewrite';
    EXECUTE IMMEDIATE 'alter session set QUERY_REWRITE_ENABLED = TRUE';
    g_msg := 'Getting Month Number Column from tbbs_system_control';
    SELECT control_value
    INTO month_column
    FROM tbbs_system_control
    WHERE upper(control_name) = 'MONTH NUMBER COLUMN';
    IF month_column IS NULL THEN
      g_msg := g_msg || ' - Invalid tbbs_system_control entry for Month Number Column';
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
    END IF;
    g_msg := 'Getting GL Company Column from tbbs_system_control';
    SELECT control_value
    INTO company_column
    FROM tbbs_system_control
    WHERE upper(control_name) = 'GL COMPANY COLUMN';
    IF company_column IS NULL THEN
      g_msg := g_msg || ' - Invalid tbbs_system_control entry for Company Column';
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
    END IF;
    g_msg := 'Getting Amount Column from tbbs_system_control';
    SELECT control_value
    INTO amount_column
    FROM tbbs_system_control
    WHERE upper(control_name) = 'AMOUNT COLUMN';
    IF amount_column IS NULL THEN
      g_msg := g_msg || ' - Invalid tbbs_system_control entry for Amount Column';
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
    END IF;
    balance_source := f_get_balance_source();
    account_id_column := f_get_bal_table_acct_id_col();
    g_msg := 'Adding GL Companies to TBBS GL Company';
    --Update TBBS GL Company Table
    --Use distinct to take advange of index, then use upper(to_char) with lower volume
    EXECUTE IMMEDIATE 'MERGE INTO tbbs_gl_company A ' ||
                      'USING (SELECT to_char(gl_company) as gl_company ' ||
                              'FROM (SELECT DISTINCT ' || company_column || ' as gl_company ' ||
                                    'FROM ' || balance_source || ')
                              UNION ALL
                              SELECT ''ALL_COMPANIES''
                              FROM dual) b ON (Upper(To_Char(a.gl_company)) = Upper(To_Char(b.gl_company))) ' ||
                                    'WHEN NOT MATCHED THEN ' ||
                                    'INSERT(A.gl_company, a.description) ' ||
                                    'VALUES(b.gl_company, b.gl_company)';
    g_msg := 'Generating balance table field selection list';
    WITH fields AS
    (SELECT tba.book_account_id, taf.account_field, tbav.field_value
     FROM tbbs_book_account tba
     join tbbs_book_account_value tbav
     ON tba.book_account_id = tbav.book_account_id
     right outer join tbbs_account_field taf
     ON tbav.account_field_id = taf.account_field_id
     )
    SELECT field_list
    INTO select_list
    FROM
    (SELECT listagg(account_field, ', ') within GROUP (
                                                       ORDER BY NULL) AS field_list
     FROM
     (SELECT DISTINCT account_field FROM fields
      )
     );
    g_msg := 'Generating grouping clause of balance table select statement';
    WITH fields AS
    (SELECT tba.book_account_id, taf.account_field, tbav.field_value
     FROM tbbs_book_account tba
     join tbbs_book_account_value tbav
     ON tba.book_account_id = tbav.book_account_id
     right outer join tbbs_account_field taf
     ON tbav.account_field_id = taf.account_field_id
     )
    SELECT 'GROUPING SETS (('
    || listagg(field_list, '), (') within GROUP (
                                                 ORDER BY NULL)
    || ')'
    || ', ())'
    INTO grouping_clause
    FROM
    (SELECT DISTINCT field_list
     FROM
     (SELECT listagg(account_field, ', ') within GROUP (
                                                        ORDER BY NULL) AS field_list
      FROM fields
      GROUP BY book_account_id
      )
     );
--Select entire having statement (xmlgen.convert to fix issues wrt XML and single quotes, XMLAGG because
-- the list of accounts can be very long, so we need a CLOB, which listagg doesn't support.
-- rtrim removes the last ' OR ' that xmlagg tacks on to the end
    g_msg := 'Generating having clause of balance table select statment';
    WITH current_assignments AS
    (SELECT a.primary_account, haves.account_field haves, have_nots.account_field have_nots
     FROM (SELECT primary_account, account_field_id
           FROM tbbs_account_field_assign tafa) a
     left join(SELECT tafa.primary_account, taf.account_field_id
               FROM tbbs_account_field_assign tafa
               cross join tbbs_account_field taf
               MINUS
               SELECT primary_account, account_field_id
               FROM tbbs_account_field_assign) b ON a.primary_account = b.primary_account
     left join tbbs_account_field haves ON a.account_field_id = haves.account_field_id
     left join tbbs_account_field have_nots ON b.account_field_id = have_nots.account_field_id)
    SELECT 'HAVING (' || dbms_xmlgen.convert(rtrim(xmlagg(xmlelement(c, '(' || not_null_part || null_part || ') AND ' || grouping_0_part || grouping_1_part || ') OR ' 
                                                                     || chr(10)) ORDER BY NULL).extract('//text()').getclobval(), 
                                                   ' OR ' || chr(10)), dbms_xmlgen.entity_decode) || ')' INTO having_clause
    FROM(SELECT DISTINCT 'to_char(' || account_id_column || ') IN (SELECT upper(primary_account) from account_combos ' ||
         'WHERE ' || listagg(haves || ' IS NOT NULL ', ' AND ') within GROUP (ORDER BY NULL) AS not_null_part,
         listagg(nvl2(have_nots, ' AND ' || have_nots || ' IS NULL ', ''), '') within GROUP (ORDER BY NULL) AS null_part,
         listagg('GROUPING(' || haves || ') = 0', ' AND ') within GROUP (ORDER BY NULL) AS grouping_0_part,
         listagg(nvl2(have_nots, ' AND GROUPING(' || have_nots || ') = 1 ', ''), '') within GROUP (ORDER BY NULL) AS grouping_1_part
         FROM current_assignments
         GROUP BY primary_account);
    g_msg := 'Combining having and grouping clauses of balance table select statment';
    full_statement := 'SELECT to_char(a.company) as company, a.month_number, b.book_account_id, a.total ' || ' FROM (SELECT ' || select_list || ', ' || month_column ||
    ' as month_number, ' || company_column || ' as company, sum(' || amount_column || ') as total ' || ' FROM ' || balance_source || ' GROUP BY GROUPING SETS ((' || month_column ||
    ', ' || company_column || ')), ' || grouping_clause || ' ' || having_clause || ') a';
--Join back to this representation of accounts in order to find the correct balance for a given "book_account_id"
--Need to pivot to get the field values back into columns on which we can join
--The regexp adds quotes around and "as <field_name>" after the field list, which is needed for the in clause of pivot to get expected column
-- names
-- Using MAX in pivot because the Oracle pivot function requires an aggregation in the pivot clause
    g_msg := 'Generating join clause of balance table select statment to tbbs book account tables';
    join_clause := ' JOIN (SELECT book_account_id, ' || select_list ||
    ' FROM (SELECT tba.book_account_id, upper(taf.account_field) AS account_field, upper(tbav.field_value) as field_value' || ' FROM tbbs_book_account tba ' ||
    ' JOIN tbbs_book_account_value tbav ON tba.book_account_id = tbav.book_account_id ' || ' JOIN tbbs_account_field taf ON tbav.account_field_id = taf.account_field_id) ' ||
    ' pivot (MAX(field_value) FOR account_field IN (' || regexp_replace(upper(select_list), '(,?)([^,^ ]+)(,?)', '''\2'' as \2\3') || '))) b ' || ' ON ' || rtrim(regexp_replace(
                                                                                                                                                                                 upper(select_list), '(,?)([^,^ ]+)(,?)', 'nvl2(a.\2, upper(to_char(a.\2)), ''' || magic_null_string || ''') = nvl2(b.\2, upper(to_char(b.\2)), ''' || magic_null_string ||
                                                                                                                                                                                 ''') AND'), ' AND');
    g_msg := 'Adding join clause to balance table select statment';
    full_statement := full_statement || join_clause;
    g_msg := 'Adding with clause for account assignments';
    full_statement := 'with account_combos as( ' ||
    'SELECT distinct primary_account, ' || regexp_replace(upper(select_list), '(,?)([^,^ ]+)(,?)', 'MAX(\2) as \2\3') || ' ' ||
    'FROM(SELECT primary_account, ' || select_list || ' ' ||
    'FROM( SELECT primary_account, upper(account_field) as account_field ' ||
    'FROM tbbs_account_field_assign tafa ' ||
    'JOIN tbbs_account_field taf ON tafa.account_field_id = taf.account_field_id) ' ||
    'pivot (MAX(account_field) FOR account_field IN (' || regexp_replace(upper(select_list), '(,?)([^,^ ]+)(,?)', '''\2'' as \2\3') || '))) ' ||
    'GROUP BY primary_account)' || full_statement;
    g_msg := 'Generating tbbs_book_balance insert statement using balance table select statement';
    full_statement := 'INSERT INTO tbbs_book_balance(gl_company, month_number, book_account_id, amount) ' || chr(10) || full_statement;
    g_msg := 'Clearing tbbs_book_balance';
    EXECUTE IMMEDIATE 'delete from tbbs_book_balance';
    g_msg := 'Executing balance table insert statement';
    EXECUTE IMMEDIATE full_statement;
-- Error handling using PKG_PP_ERROR
    pkg_pp_error.remove_module_name;
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
  END p_process_book_balances;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  FUNCTION f_num_duplicate_accounts
  RETURN NUMBER
  IS
  account_source CLOB;
  account_id_column VARCHAR2(30);
  num_duplicates NUMBER;
  BEGIN
    pkg_tbbs_account.p_setprocess;
    pkg_pp_log.p_start_log(g_process_id);
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.F_NUM_DUPLICATE_ACCOUNTS');
    
    g_msg := 'Finding account source';
    account_source := f_get_account_source();
    
    g_msg := 'Finding account ID column';
    account_id_column := f_get_account_id_column();
    
    g_msg := 'Checking for duplicate accounts';
    EXECUTE IMMEDIATE 'select nvl((select count(' || account_id_column || ' ) from ' || account_source || ' group by ' || account_id_column || 
    ' having count(' || account_id_column || ') > 1), 0) as count from dual' INTO num_duplicates
    ;
-- Error handling using PKG_PP_ERROR
    pkg_pp_error.remove_module_name;
    RETURN num_duplicates;
  EXCEPTION
    WHEN OTHERS THEN
      pkg_pp_error.raise_error('ERROR_ENDLOG', g_msg);
      RETURN - 1;
  END f_num_duplicate_accounts;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
END pkg_tbbs_account;
/

--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (11680, 0, 2018, 1, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2018.1.0.0_PKG_TBBS_ACCOUNT.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
