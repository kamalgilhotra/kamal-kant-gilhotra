CREATE OR REPLACE package pkg_reg_common
/*
||============================================================================
|| Application: PowerPlant (Rates and Regulatory)
|| Object Name: PKG_REG_COMMON
|| Description: Reg Annualization Functions.
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date        Revised By         Reason for Change
|| ------- ----------  ---------------    ------------------------------------
|| 1.0     02/05/2014  Shane "C" Ward     Create
|| 1.1     02/24/2014  Shane "C" Ward     Updated Call to round
||============================================================================
*/

 as
   function f_RoundNumber(a_percent_flag      NUMBER,
                          a_scale             NUMBER,
                          A_INPUT_NUMBER NUMBER
                          ) RETURN number;
                          
   FUNCTION f_RoundAny(a_id               NUMBER,
                       a_id_type          VARCHAR2,
                       a_lookup_value     VARCHAR2,
                       a_input_number     NUMBER
                       ) RETURN NUMBER;                          

end PKG_REG_COMMON;
/

CREATE OR REPLACE package body pkg_reg_common as

   --**************************************************************
   --       TYPE DECLARATIONS
   --**************************************************************


   --**************************************************************
   --       VARIABLES
   --**************************************************************


   --**************************************************************
   --       FUNCTIONS
   --**************************************************************
   FUNCTION f_roundAny(a_id NUMBER,
                      a_id_type VARCHAR2,
                      a_lookup_value VARCHAR2,
                      a_input_number NUMBER
                      ) RETURN NUMBER IS
     percentage_flag NUMBER;
     scale           NUMBER;
     round_number    NUMBER;
   BEGIN
   
    IF a_id IS NULL THEN
        Raise_Application_Error(-20001, 'ID is null');
    END IF;

    if a_id_type is null OR a_id_type = '' then
        RAISE_APPLICATION_ERROR(-20002, 'ID Type is null');
    end if;

    IF a_input_number IS NULL THEN
        RETURN NULL;
    END IF;
   
   
   --Get Input Scale
    CASE Upper(a_id_type)
      WHEN 'COMPANY' THEN
      
        BEGIN
        
        SELECT input_scale, percentage_flag
        INTO scale, percentage_flag
        FROM reg_co_input_scale s, reg_input_scale i
        WHERE s.reg_company_id = a_id
        AND i.scale_id = s.scale_id
        AND Upper(i.description) = Upper(a_lookup_value);
        
        EXCEPTION
          WHEN No_Data_Found THEN
            RETURN a_input_number;
        END;
      WHEN 'CASE' THEN
        BEGIN
        
        SELECT input_scale, percentage_flag
        INTO scale, percentage_flag
        FROM reg_jur_input_scale s, reg_case c, reg_input_scale i
        WHERE c.reg_case_id = a_id
        AND i.scale_id = s.scale_id
        AND c.reg_jur_template_Id = s.reg_jur_template_id
        AND Upper(i.description) = Upper(a_lookup_value);
        
        EXCEPTION
          WHEN No_Data_Found THEN
            RETURN a_input_number;
        END;
      WHEN 'JUR_TEMPLATE' THEN
        BEGIN
        SELECT input_scale, percentage_flag
        INTO scale, percentage_flag
        FROM reg_Jur_input_scale s, reg_input_scale i
        WHERE s.reg_jur_template_id = a_id
        AND i.scale_id = s.scale_id
        AND Upper(i.description) = Upper(a_lookup_value);
        
        EXCEPTION
          WHEN No_Data_Found THEN
            RETURN a_input_number;
        END;
    END CASE;
    
    round_number := f_roundNumber(percentage_flag, scale, a_input_number);
    
    RETURN round_number;
    
   END f_roundAny;
   
   
   
   
   function F_ROUNDNUMBER(a_percent_flag      NUMBER,
                          a_scale             NUMBER,
                          A_INPUT_NUMBER NUMBER
                          ) return number is

      ROUND_NUMBER NUMBER;
      scale        NUMBER;
   begin

      IF a_percent_flag = 1 THEN
        scale := a_scale+2;
      ELSE
        scale := a_scale;
      END IF;

      ROUND_NUMBER := ROUND(A_INPUT_NUMBER, SCALE);

      return ROUND_NUMBER;

   end F_ROUNDNUMBER;


end PKG_REG_COMMON;

/
--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (3146, 0, 2016, 1, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2016.1.0.0_PKG_REG_COMMON.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
