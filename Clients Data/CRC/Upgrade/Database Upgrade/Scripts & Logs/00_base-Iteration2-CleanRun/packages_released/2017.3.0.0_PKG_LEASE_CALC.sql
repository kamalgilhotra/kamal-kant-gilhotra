CREATE OR REPLACE package pkg_lease_calc as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_CALC
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 10.4.1.0 07/10/2013 Ryan Oliveria  MLA functions (workflows and revisions)
   || 10.4.1.0 07/22/2013 Brandon Beck   Modified Approval for ILRs
   || 10.4.1.0 07/30/2013 Kyle Peterson  Payments and Invoices
   || 2015.2   12/01/2015 Andrew Scott   Tax calc error fix.
   || 2017.1.0 03/20/2017 Anand R        Add month end calculations for variable payments
   || 2017.1.0 04/28/2017 Anand R        PP-47670  save currency effective date when payments are processed
   || 2017.1.0 06/12/2017 Johnny Sisouph PP-47393  update in service exchange rate when ilr is approved
   || 2017.1.0 07/06/2017 Anand R        PP-48415  use system date for service exchange rate when ilr is approved
   || 2017.1.0 07/17/2017 Andrew Hill    PP-47966  Add currency gain/loss approval functions
   || 2017.1.0 07/19/2017 Jared Schwantz PP-48330 Deferred Rent changes
   || 2017.1.0 07/21/2017 Andand R       PP-48218  Remove logic to update ls_lease_calculated_date_rates. Moved to PB.
   || 2017.1.0 07/21/2017 Andrew Hill    PP-47967  Change payments and accruals to use muticurrency posting function
   || 2017.1.0 07/29/2017 Anand R        PP-47249  Modify residual processing logic to with with no calculated date rate
   || 2017.3.0 02/19/2018 Anand R        PP-50513 Remove ILR approval logic from Send
   || 2017.3.0 04/04/2018 Powers K       PP-50847 Process new JE GL Lease Remeasurement Codes
   || 2017.3.0 05/01/2018 Crystal Yura	 PP-50619 Send ILR Error
   || 2017.3.0 05/01/2018 Sisouphanh	 PP-51070 Lessee Payment Calculation can't find manual workflow
   ||============================================================================
   */
   type NUM_ARRAY is table of number(22) index by binary_integer;
   type ASSET_SCHEDULE_LINE_TYPE is table of LS_ASSET_SCHEDULE%rowtype index by pls_integer;
   type ASSET_SCHEDULE_LINE_TYPE2 is record(
      LS_ASSET_ID     LS_ASSET_SCHEDULE.LS_ASSET_ID%type,
      INTEREST_PAID   LS_ASSET_SCHEDULE.INTEREST_PAID%type,
      PRINCIPAL_PAID  LS_ASSET_SCHEDULE.PRINCIPAL_PAID%type,
      LEASE_ID        LS_LEASE.LEASE_ID%type,
      VENDOR_ID       LS_LEASE_VENDOR.VENDOR_ID%type,
      PAYMENT_PCT     LS_LEASE_VENDOR.PAYMENT_PCT%type,
	  EXECUTORY_PAID1 LS_ASSET_SCHEDULE.EXECUTORY_PAID1%type,
	  EXECUTORY_PAID2 LS_ASSET_SCHEDULE.EXECUTORY_PAID2%type,
	  EXECUTORY_PAID3 LS_ASSET_SCHEDULE.EXECUTORY_PAID3%type,
	  EXECUTORY_PAID4 LS_ASSET_SCHEDULE.EXECUTORY_PAID4%type,
	  EXECUTORY_PAID5 LS_ASSET_SCHEDULE.EXECUTORY_PAID5%type,
	  EXECUTORY_PAID6 LS_ASSET_SCHEDULE.EXECUTORY_PAID6%type,
	  EXECUTORY_PAID7 LS_ASSET_SCHEDULE.EXECUTORY_PAID7%type,
	  EXECUTORY_PAID8 LS_ASSET_SCHEDULE.EXECUTORY_PAID8%type,
	  EXECUTORY_PAID9 LS_ASSET_SCHEDULE.EXECUTORY_PAID9%type,
	  EXECUTORY_PAID10 LS_ASSET_SCHEDULE.EXECUTORY_PAID10%type,
	  CONTINGENT_PAID1 LS_ASSET_SCHEDULE.CONTINGENT_PAID1%type,
	  CONTINGENT_PAID2 LS_ASSET_SCHEDULE.CONTINGENT_PAID2%type,
	  CONTINGENT_PAID3 LS_ASSET_SCHEDULE.CONTINGENT_PAID3%type,
	  CONTINGENT_PAID4 LS_ASSET_SCHEDULE.CONTINGENT_PAID4%type,
	  CONTINGENT_PAID5 LS_ASSET_SCHEDULE.CONTINGENT_PAID5%type,
	  CONTINGENT_PAID6 LS_ASSET_SCHEDULE.CONTINGENT_PAID6%type,
	  CONTINGENT_PAID7 LS_ASSET_SCHEDULE.CONTINGENT_PAID7%type,
	  CONTINGENT_PAID8 LS_ASSET_SCHEDULE.CONTINGENT_PAID8%type,
	  CONTINGENT_PAID9 LS_ASSET_SCHEDULE.CONTINGENT_PAID9%type,
	  CONTINGENT_PAID10 LS_ASSET_SCHEDULE.CONTINGENT_PAID10%type,
      ROWNUMBER       number,
      SET_OF_BOOKS_ID LS_ASSET_SCHEDULE.SET_OF_BOOKS_ID%type,
	  COMPANY_ID LS_ASSET.COMPANY_ID%type,
	  MONTH date, ILR_ID LS_ILR.ILR_ID%type, LS_RECONCILE_TYPE_ID LS_RECONCILE_TYPE.ls_reconcile_type_id%type,
	  ACTUAL_TERMINATION_AMOUNT LS_ASSET.ACTUAL_TERMINATION_AMOUNT%type,
      sale_proceed_amount LS_ASSET.sale_proceed_amount%type);
   type ASSET_SCHEDULE_LINE_TABLE is table of ASSET_SCHEDULE_LINE_TYPE2 index by pls_integer;

   TYPE currency_gain_loss_rec IS RECORD(
	company_id                    company_setup.company_id%TYPE,
	ilr_id                        ls_ilr.ilr_id%TYPE,
	ilr_number                    ls_ilr.ilr_number%TYPE,
	ls_asset_id                   ls_asset.ls_asset_id%TYPE,
	leased_asset_number           ls_asset.leased_asset_number%TYPE,
	iso_code                      currency.iso_code%TYPE,
	currency_display_symbol       currency.currency_display_symbol%TYPE,
	gain_loss_fx                  v_ls_asset_schedule_fx_vw.gain_loss_fx%TYPE,
	MONTH                         v_ls_asset_schedule_fx_vw.month%TYPE,
	currency_gain_loss_dr_acct_id ls_ilr_account.currency_gain_loss_dr_acct_id%TYPE,
	currency_gain_loss_cr_acct_id ls_ilr_account.currency_gain_loss_cr_acct_id%TYPE
	);

   TYPE currency_gain_loss_tbl IS TABLE OF currency_gain_loss_rec;

   procedure P_SET_ILR_ID(A_ILR_ID number);

   procedure P_INVOICE_COMPARE(MYINVOICE in LS_INVOICE%rowtype,
                               MYPAYMENT in LS_PAYMENT_HDR%rowtype);

   procedure P_PAYMENT_ROLLUP(A_LEASE_ID in number, A_COMPANY_ID in number, A_MONTH date);

   function F_PAYMENT_MONTH(A_ILR_ID   number,
                            A_REVISION number,
                            A_MONTH    date) return number;

   function F_APPROVE_MLA(A_LEASE_ID number,
                          A_REVISION number) return number;

   function F_REJECT_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_SEND_MLA(A_LEASE_ID number,
                       A_REVISION number) return number;

   function F_UNREJECT_MLA(A_LEASE_ID number,
                           A_REVISION number) return number;

   function F_UNSEND_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID number,
                                  A_REVISION number) return number;

   function F_APPROVE_ILR(A_ILR_ID   number,
                          A_REVISION number) return number;

   function F_REJECT_ILR(A_ILR_ID   number,
                         A_REVISION number) return number;

   function F_SEND_ILR(A_ILR_ID   number,
                       A_REVISION number) return number;

   function F_UNREJECT_ILR(A_ILR_ID   number,
                           A_REVISION number) return number;

   function F_UNSEND_ILR(A_ILR_ID   number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID   number,
                                  A_REVISION number) return number;

   function F_ACCRUALS_CALC(A_COMPANY_ID  number,
                            A_MONTH       date,
                            A_END_LOG     number:=null) return varchar2;

   function F_ACCRUALS_APPROVE(A_COMPANY_ID in number,
                               A_MONTH      in date,
                               A_END_LOG    in number:=null) return varchar2;

   function F_PAYMENT_CALC(A_COMPANY_ID in number,
                           A_MONTH      in date,
                           A_END_LOG    in number:=null) return varchar2;

	function F_PAYMENT_CALC(A_LEASE_ID in number,
							A_COMPANY_ID in number,
                            A_MONTH      in date,
                            A_END_LOG    in number:=null) return varchar2;

   function F_PAYMENT_APPROVE(A_COMPANY_ID in number,
                              A_MONTH      in date,
                              A_END_LOG    in number:=null) return varchar2;

   function F_SEND_PAYMENT(A_PAYMENT_ID number) return number;

   function F_UPDATE_WORKFLOW_PAYMENT(A_PAYMENT_ID number) return number;

   function F_REJECT_PAYMENT(A_PAYMENT_ID number) return number;

   function F_UNREJECT_PAYMENT(A_PAYMENT_ID number) return number;
   --TO APPROVE SINGLE PAYMENT THROUGH WORKFLOW
   function F_APPROVE_PAYMENT(A_PAYMENT_ID number) return number;

   function F_UNSEND_PAYMENT(A_PAYMENT_ID number) return number;

   function F_MATCH_INVOICES(A_MONTH in date) return varchar2;

   function F_GET_ILR_ID return number;

   function F_SEND_ILR_NO_COMMIT(A_ILR_ID   in number,
                                 A_REVISION in number,
                                 A_STATUS   out varchar2) return number;

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2) return number;

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2,
                                    A_LOG in boolean,
									a_is_transfer in boolean default false) return number;

   function F_LAM_CLOSED(A_COMPANY_ID in number,
                         A_MONTH      in date,
                         A_END_LOG in number:=null) return varchar2;

   function F_DEPR_APPROVE(A_COMPANY_ID in number,
                           A_MONTH      in date,
                           A_END_LOG    in number:=null) return varchar2;

   function F_PROCESS_RESIDUAL(A_LS_ASSET_IDS in NUM_ARRAY) return varchar2;

   function F_PROCESS_RESIDUAL(A_LS_ASSET_IDS in NUM_ARRAY,
                           A_END_LOG in number) return varchar2;

   procedure P_AUTO_GENERATE_INVOICES(A_LEASE_ID in number, A_COMPANY_ID number,
                              A_MONTH date);
  function f_adjust_taxes(A_INVOICE_ID in number,
                          A_ADJUST_AMOUNT in number,
                          A_TAX_LOCAL_ID in number,
                          A_LINE_TYPE in number,
                          A_LS_ASSET_ID in number)
  return varchar2;
  function F_GET_GL_POSTING_MO_YR
  (A_ILR_ID in number, A_REVISION in number, A_MONTH_OUT out date)
  return varchar2;
  function F_FLOATING_RATE_SF_ACCRUAL(A_LEASE_ID in number,
                               A_COMPANY_ID in number,
                               A_MONTH      in date,
                               A_ILR_ID in number:=null,
                               A_REVISION in number:=null) return varchar2;
  function F_FLOATING_RATE_SF_PAYMENT(A_LEASE_ID in number,
                               A_COMPANY_ID in number,
                               A_MONTH      in date,
                               A_ILR_ID in number:=null,
                               A_REVISION in number:=null) return varchar2;
  function F_FLOATING_RATE_ACCRUAL(A_LEASE_ID in number,
                               A_COMPANY_ID  in number,
                               A_MONTH        in date,
                               A_ILR_ID in number:=null,
                               A_REVISION in number:=null) return varchar2;
  function F_FLOATING_RATE_PAYMENT(A_LEASE_ID in number,
                               A_COMPANY_ID  in number,
                               A_MONTH        in date,
                               A_ILR_ID in number:=null,
                               A_REVISION in number:=null) return varchar2;
 	function F_TAX_CALC
	(a_lease_id in number, a_company_id in number, a_month in date, a_accrual in number) return varchar2;
    function f_execute_immediate(a_sqls in varchar2, a_start_log in number:=null) return varchar2;
    function f_auto_retirements(A_COMPANY_ID IN NUMBER, A_MONTH IN DATE, A_END_LOG IN NUMBER:=NULL) RETURN VARCHAR2;
-- KRD 1/31/17 - Adding function for lease standalone implementations
--               This function will automate the code in the CPR Control buttons
--                 and be called from the "Close Month" button in the Lease module
  FUNCTION F_LAM_CPR_CLOSE(
                           A_COMPANY_ID IN NUMBER,
                           A_MONTH      IN DATE
                          )
    RETURN VARCHAR2;
-- END KRD 1/31/17
-- KRD 2/1/17 - Separating out the balancing functions. Call from F_LAM_CPR_CLOSE
  FUNCTION F_BAL_LEASE_DEPR(
                            A_COMPANY_ID IN NUMBER
                           )
    RETURN NUMBER;

  FUNCTION F_BAL_LEASE_CPR(
                           A_COMPANY_ID IN NUMBER
                          )
    RETURN NUMBER;
-- END KRD 2/1/17

	function F_CURRENCY_GAIN_LOSS(	a_company_ids     IN t_num_array,
									a_month           IN DATE,
									a_set_of_books_id IN NUMBER)
    RETURN currency_gain_loss_tbl
    PIPELINED;

	FUNCTION F_CURRENCY_GAIN_LOSS_APPROVE(	a_company_id      IN NUMBER,
											a_month           IN DATE,
											a_set_of_books_id IN NUMBER,
											a_end_log         IN NUMBER := NULL)
    RETURN VARCHAR2;

end PKG_LEASE_CALC;
/
CREATE OR REPLACE package body pkg_lease_calc as


   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_CALC
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 10.4.1.0 07/22/2013 Brandon Beck   Modified Approval for ILRs
   || 10.4.1.0 07/22/2013 Brandon Beck   Modified Approval for ILRs
   || 10.4.1.0 07/22/2013 Brandon Beck   Modified Approval for ILRs
   || 2015.2   12/01/2015 Andrew Scott   Tax calc error fix.
   || 2017.1.0 03/20/2017 Anand R        Add month end calculations for variable payments
   || 2017.1.0 04/28/2017 Anand R        PP-47670  save currency effective date when payments are processed
   || 2017.1.0 07/06/2017 Anand R        PP-48415  use system date for service exchange rate when ilr is approved
   || 2017.1.0 07/17/2017 Andrew Hill    PP-47966  Add currency gain/loss approval functions
   || 2017.1.0 07/19/2017 Jared Schwantz PP-48330 Deferred Rent changes
   || 2017.1.0 07/21/2017 Andand R       PP-48218  Remove logic to update ls_lease_calculated_date_rates. Moved to PB.
   || 2017.1.0 07/21/2017 Andrew Hill    PP-47967  Change payments and accruals to use muticurrency posting function
   || 2017.3.0 05/01/2018 Sisouphanh	 PP-51070 Lessee Payment Calculation can't find manual workflow
   ||============================================================================
   */

   L_ILR_ID number;
   type PAYMENT_HDR_TYPE is table of LS_PAYMENT_HDR%rowtype index by pls_integer;
   type INVOICE_HDR_TYPE is table of LS_INVOICE%rowtype index by pls_integer;
   type PAYMENT_LINE_TYPE is table of LS_PAYMENT_LINE%rowtype index by pls_integer;
   type ASSET_TAX_LINE_TYPE is record(
      RATE LS_TAX_STATE_RATES.RATE%type,
      LS_ASSET_ID LS_ASSET.LS_ASSET_ID%type,
      TAX_LOCAL_ID LS_TAX_LOCAL.TAX_LOCAL_ID%type,
      SET_OF_BOOKS_ID SET_OF_BOOKS.SET_OF_BOOKS_ID%type,
      AMOUNT LS_ASSET_SCHEDULE.INTEREST_ACCRUAL%type,
	  SHIFT LS_ILR_OPTIONS.PAYMENT_SHIFT%type,
	  VENDOR_ID LS_LEASE_VENDOR.VENDOR_ID%type,
	  TAX_DISTRICT_ID LS_MONTHLY_TAX.TAX_DISTRICT_ID%type
	  );
   type ASSET_SCHEDULE_HEADER_TYPE is record(
	  LEASE_ID LS_LEASE.LEASE_ID%type,
	  ILR_ID LS_ILR.ILR_ID%type,
	  LS_ASSET_ID LS_ASSET.LS_ASSET_ID %type,
	  VENDOR_ID LS_VENDOR.VENDOR_ID%type);
   type ASSET_TAX_TABLE is table of ASSET_TAX_LINE_TYPE index by pls_integer;
   type ASSET_SCHEDULE_HEADER_TABLE is table of ASSET_SCHEDULE_HEADER_TYPE index by pls_integer;

   --FORWARD DECLARATIONS
   function F_TAX_CALC_ACCRUALS(A_COMPANY_ID in number,
								A_MONTH      in date) return varchar2;
   function F_TAX_CALC_PAYMENTS(A_LEASE_ID in number, A_COMPANY_ID in number,
								A_MONTH      in date) return varchar2;





   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   procedure P_LOGERRORMESSAGE(A_ILR_ID in number,
                               A_MSG    in varchar2) is
      pragma autonomous_transaction;
   begin
      update LS_PEND_TRANSACTION set ERROR_MESSAGE = A_MSG where ILR_ID = A_ILR_ID;

      commit;
   end P_LOGERRORMESSAGE;

   --**************************************************************************
   --                            P_SET_ILR_ID
   --**************************************************************************

   procedure P_SET_ILR_ID(A_ILR_ID number) is

   begin
      L_ILR_ID := A_ILR_ID;
   end P_SET_ILR_ID;

   --**************************************************************************
   --                            P_AUTO_GENERATE_INVOICES
   --**************************************************************************

   procedure P_AUTO_GENERATE_INVOICES(A_LEASE_ID in number, A_COMPANY_ID number,
                              A_MONTH date) is
   begin

      merge into LS_INVOICE LI
      using (select LPH.*
             from LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL
            where LPH.COMPANY_ID = A_COMPANY_ID
              and LPH.GL_POSTING_MO_YR = A_MONTH
              and LPH.LEASE_ID = LL.LEASE_ID
              and LL.LEASE_ID = LLO.LEASE_ID
			  and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
              and LL.CURRENT_REVISION = LLO.REVISION
              and LLO.AUTO_GENERATE_INVOICES = 1
              and LPH.PAYMENT_STATUS_ID = 1) LPH
      on (LPH.VENDOR_ID = LI.VENDOR_ID and LPH.COMPANY_ID = LI.COMPANY_ID and LPH.GL_POSTING_MO_YR = LI.GL_POSTING_MO_YR
		and LPH.LEASE_ID = LI.LEASE_ID and LPH.ILR_ID = LI.ILR_ID and LPH.LS_ASSET_ID = LI.LS_ASSET_ID
		)
      when matched then
         update
            /* WMD need substring here */
            set LI.INVOICE_NUMBER = substr('AUTO ' || TO_CHAR(LPH.GL_POSTING_MO_YR) || '-' ||
                              TO_CHAR(LPH.LEASE_ID) || '-' || TO_CHAR(LPH.COMPANY_ID) || '-' ||
                              TO_CHAR(LPH.VENDOR_ID), 0, 35)
      when not matched then
         insert
            (INVOICE_ID, COMPANY_ID, LEASE_ID, GL_POSTING_MO_YR, VENDOR_ID, INVOICE_NUMBER, ILR_ID, LS_ASSET_ID)
         values
            (LS_INVOICE_SEQ.NEXTVAL, LPH.COMPANY_ID, LPH.LEASE_ID, LPH.GL_POSTING_MO_YR, LPH.VENDOR_ID,
             /* WMD another substring here */
             substr('AUTO ' || TO_CHAR(LPH.GL_POSTING_MO_YR) || '-' || TO_CHAR(LPH.LEASE_ID) || '-' ||
              TO_CHAR(LPH.COMPANY_ID) || '-' || TO_CHAR(LPH.VENDOR_ID), 0, 35), LPH.ILR_ID, LPH.LS_ASSET_ID
			  );

      merge into LS_INVOICE_PAYMENT_MAP LIPM
      using (select LPH.PAYMENT_ID, LI.INVOICE_ID
             from LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL, LS_INVOICE LI
            where LPH.COMPANY_ID = A_COMPANY_ID
              and LPH.GL_POSTING_MO_YR = A_MONTH
              and LPH.LEASE_ID = LL.LEASE_ID
              and LL.LEASE_ID = LLO.LEASE_ID
              and LL.CURRENT_REVISION = LLO.REVISION
              and LLO.AUTO_GENERATE_INVOICES = 1
              and LPH.COMPANY_ID = LI.COMPANY_ID
              and LPH.VENDOR_ID = LI.VENDOR_ID
              and LPH.LEASE_ID = LI.LEASE_ID
			  and LPH.ILR_ID = LI.ILR_ID
			  and LPH.LS_ASSET_ID = LI.LS_ASSET_ID
			  and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
              and LPH.GL_POSTING_MO_YR = LI.GL_POSTING_MO_YR
              and LPH.PAYMENT_STATUS_ID = 1) B
      on (LIPM.INVOICE_ID = B.INVOICE_ID and LIPM.PAYMENT_ID = B.PAYMENT_ID)
      when matched then
         update set LIPM.IN_TOLERANCE = 1
      when not matched then
         insert (INVOICE_ID, PAYMENT_ID, IN_TOLERANCE) values (B.INVOICE_ID, B.PAYMENT_ID, 1);

      --Delete from LS_INVOICE_LINE where the (line, type, asset) is not in LS_PAYMENT_LINE
      delete from LS_INVOICE_LINE
       where INVOICE_ID in
            (select LIPM.INVOICE_ID
              from LS_INVOICE_PAYMENT_MAP LIPM, LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL
             where LPH.COMPANY_ID = A_COMPANY_ID
               and LPH.GL_POSTING_MO_YR = A_MONTH
               and LPH.LEASE_ID = LL.LEASE_ID
               and LL.LEASE_ID = LLO.LEASE_ID
               and LL.CURRENT_REVISION = LLO.REVISION
               and LLO.AUTO_GENERATE_INVOICES = 1
			   and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
               and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
               and LPH.PAYMENT_STATUS_ID = 1)
         and not exists (select 1
              from LS_INVOICE_LINE        LII,
                   LS_INVOICE_PAYMENT_MAP LIPM,
                   LS_PAYMENT_HDR         LPH,
                   LS_LEASE_OPTIONS       LLO,
                   LS_LEASE               LL,
                   LS_PAYMENT_LINE        LPL
             where LII.INVOICE_ID = LIPM.INVOICE_ID
               and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
               and LPH.COMPANY_ID = A_COMPANY_ID
               and LPH.GL_POSTING_MO_YR = A_MONTH
               and LPH.LEASE_ID = LL.LEASE_ID
               and LL.LEASE_ID = LLO.LEASE_ID
               and LL.CURRENT_REVISION = LLO.REVISION
               and LLO.AUTO_GENERATE_INVOICES = 1
               and LPL.PAYMENT_ID = LPH.PAYMENT_ID
			   and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
               and LPL.PAYMENT_LINE_NUMBER = LII.INVOICE_LINE_NUMBER
               and LPL.PAYMENT_TYPE_ID = LII.PAYMENT_TYPE_ID
               and LPL.LS_ASSET_ID = LII.LS_ASSET_ID);

      --merge into LS_INVOICE_LINE from LS_PAYMENT_LINE
      merge into LS_INVOICE_LINE A
      using (select LPL.*, LIPM.INVOICE_ID
             from LS_PAYMENT_LINE        LPL,
                  LS_PAYMENT_HDR         LPH,
                  LS_LEASE               LL,
                  LS_LEASE_OPTIONS       LLO,
                  LS_INVOICE_PAYMENT_MAP LIPM
            where LPL.PAYMENT_ID = LPH.PAYMENT_ID
              and LPH.COMPANY_ID = A_COMPANY_ID
              and LPH.GL_POSTING_MO_YR = A_MONTH
              and LPH.LEASE_ID = LL.LEASE_ID
              and LL.LEASE_ID = LLO.LEASE_ID
              and LL.CURRENT_REVISION = LLO.REVISION
			  and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
              and LLO.AUTO_GENERATE_INVOICES = 1
              and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
              and LPL.SET_OF_BOOKS_ID = (select min(SET_OF_BOOKS_ID) from SET_OF_BOOKS)
              and LPH.PAYMENT_STATUS_ID = 1) B
      /* Match on primary keys of LS_INVOICE_LINE */
      on (A.INVOICE_ID = B.INVOICE_ID and A.INVOICE_LINE_NUMBER = B.PAYMENT_LINE_NUMBER and A.PAYMENT_TYPE_ID = B.PAYMENT_TYPE_ID and A.LS_ASSET_ID = B.LS_ASSET_ID)
      when matched then
         update /* WMD Adding adjustment amount */
            set A.AMOUNT = NVL(B.AMOUNT,0) + NVL(B.ADJUSTMENT_AMOUNT, 0), A.DESCRIPTION = '',
               A.NOTES = 'Automatically generated on ' || TO_CHAR(sysdate) || '.',
               A.GL_POSTING_MO_YR = B.GL_POSTING_MO_YR
      when not matched then
         insert
            (INVOICE_ID, INVOICE_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, GL_POSTING_MO_YR, AMOUNT,
             NOTES)
         values
            (B.INVOICE_ID, B.PAYMENT_LINE_NUMBER, B.PAYMENT_TYPE_ID, B.LS_ASSET_ID, B.GL_POSTING_MO_YR,
             NVL(B.AMOUNT,0) + NVL(B.ADJUSTMENT_AMOUNT,0), 'Automatically generated on ' || TO_CHAR(sysdate) || '.');

      --Do the backfills for the amounts.
      update LS_INVOICE
         set (INVOICE_AMOUNT) =
            (select sum(A.AMOUNT)
               from LS_INVOICE_LINE A
              where A.INVOICE_ID = LS_INVOICE.INVOICE_ID
              group by INVOICE_ID)
       where INVOICE_ID in
            (select LIPM.INVOICE_ID
              from LS_INVOICE_PAYMENT_MAP LIPM, LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL
             where LPH.COMPANY_ID = A_COMPANY_ID
               and LPH.GL_POSTING_MO_YR = A_MONTH
               and LPH.LEASE_ID = LL.LEASE_ID
               and LL.LEASE_ID = LLO.LEASE_ID
               and LL.CURRENT_REVISION = LLO.REVISION
               and LLO.AUTO_GENERATE_INVOICES = 1
			   and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
               and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
               and LPH.PAYMENT_STATUS_ID = 1);

      update LS_INVOICE
         set (INVOICE_INTEREST) =
            (select sum(A.AMOUNT)
               from LS_INVOICE_LINE A
              where A.INVOICE_ID = LS_INVOICE.INVOICE_ID
               and A.PAYMENT_TYPE_ID in (2)
              group by INVOICE_ID)
       where INVOICE_ID in
            (select LIPM.INVOICE_ID
              from LS_INVOICE_PAYMENT_MAP LIPM, LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL
             where LPH.COMPANY_ID = A_COMPANY_ID
               and LPH.GL_POSTING_MO_YR = A_MONTH
               and LPH.LEASE_ID = LL.LEASE_ID
               and LL.LEASE_ID = LLO.LEASE_ID
               and LL.CURRENT_REVISION = LLO.REVISION
               and LLO.AUTO_GENERATE_INVOICES = 1
			   and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
               and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
               and LPH.PAYMENT_STATUS_ID = 1);

       update LS_INVOICE set(INVOICE_EXECUTORY) = (select sum(A.AMOUNT)
                                 from LS_INVOICE_LINE A
                                where A.INVOICE_ID = LS_INVOICE.INVOICE_ID
                                 and A.PAYMENT_TYPE_ID between 3 and 12
                                group by INVOICE_ID)
       where INVOICE_ID in
            (select LIPM.INVOICE_ID
              from LS_INVOICE_PAYMENT_MAP LIPM, LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL
             where LPH.COMPANY_ID = A_COMPANY_ID
               and LPH.GL_POSTING_MO_YR = A_MONTH
               and LPH.LEASE_ID = LL.LEASE_ID
               and LL.LEASE_ID = LLO.LEASE_ID
               and LL.CURRENT_REVISION = LLO.REVISION
               and LLO.AUTO_GENERATE_INVOICES = 1
			   and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
               and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
               and LPH.PAYMENT_STATUS_ID = 1);

       update LS_INVOICE set(INVOICE_CONTINGENT) = (select sum(A.AMOUNT)
                                 from LS_INVOICE_LINE A
                                 where A.INVOICE_ID = LS_INVOICE.INVOICE_ID
                                  and A.PAYMENT_TYPE_ID between 13 and 22
                                 group by INVOICE_ID)
       where INVOICE_ID in
            (select LIPM.INVOICE_ID
              from LS_INVOICE_PAYMENT_MAP LIPM, LS_PAYMENT_HDR LPH, LS_LEASE_OPTIONS LLO, LS_LEASE LL
             where LPH.COMPANY_ID = A_COMPANY_ID
               and LPH.GL_POSTING_MO_YR = A_MONTH
               and LPH.LEASE_ID = LL.LEASE_ID
               and LL.LEASE_ID = LLO.LEASE_ID
               and LL.CURRENT_REVISION = LLO.REVISION
               and LLO.AUTO_GENERATE_INVOICES = 1
			   and case when a_lease_id = -1 then -1 else ll.lease_id end = a_lease_id
               and LIPM.PAYMENT_ID = LPH.PAYMENT_ID
               and LPH.PAYMENT_STATUS_ID = 1);

   end P_AUTO_GENERATE_INVOICES;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
 function F_EXECUTE_IMMEDIATE(A_SQLS in varchar2, A_START_LOG in number:=null) return varchar2
  is
  counter number;
  begin
  if nvl(a_start_log,0) = 1 then
    PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
  end if;

  if lower(trim(pkg_pp_system_control.f_pp_system_control('Lease Debug Execute Immediate'))) = 'yes' then
    pkg_pp_log.p_write_message(a_sqls);
  end if;

  execute immediate A_SQLS;

  return 'OK';

  exception when others then
    return 'Error Executing SQL: ' || sqlerrm || ' ' || sqlcode || chr(13) || chr(10) || a_sqls;
  end f_execute_immediate;

   function F_PAYMENT_MONTH(A_ILR_ID in number,
                            A_REVISION in number,
                            A_MONTH in date) return number is
      rtn      number;
      i        number;
      loc_date date;
   begin
      for L_PAYMENT_TERMS in (select   LIP.PAYMENT_TERM_ID as PAYMENT_TERM_ID,
                              LIP.PAYMENT_TERM_TYPE_ID as PAYMENT_TERM_TYPE_ID,
                              LIP.PAYMENT_TERM_DATE as PAYMENT_TERM_DATE,
                              LIP.PAYMENT_FREQ_ID as PAYMENT_FREQ_ID,
                              LIP.NUMBER_OF_TERMS as NUMBER_OF_TERMS,
                              LL.PRE_PAYMENT_SW as PRE_PAYMENT_SW
                        from LS_ILR_PAYMENT_TERM LIP, LS_ILR LI, LS_LEASE LL
                        where LIP.ILR_ID = LI.ILR_ID
                        and LI.LEASE_ID = LL.LEASE_ID
                        and LIP.ILR_ID = A_ILR_ID
                        and LIP.REVISION = A_REVISION
                        and LIP.PAYMENT_TERM_TYPE_ID <> 1
                        order by LIP.PAYMENT_TERM_ID)
      loop
         if A_MONTH between L_PAYMENT_TERMS.PAYMENT_TERM_DATE
                  and add_months(L_PAYMENT_TERMS.PAYMENT_TERM_DATE, L_PAYMENT_TERMS.NUMBER_OF_TERMS *
                     case L_PAYMENT_TERMS.PAYMENT_FREQ_ID
                        when 1 then 12
                        when 2 then 6
                        when 3 then 3
                        else 1
                     end) then
            loc_date := case L_PAYMENT_TERMS.PRE_PAYMENT_SW
                        when 1 then L_PAYMENT_TERMS.PAYMENT_TERM_DATE
                        when 0 then add_months(L_PAYMENT_TERMS.PAYMENT_TERM_DATE, case L_PAYMENT_TERMS.PAYMENT_FREQ_ID --Shift our start point
                                          when 1 then 11                                        -- to the end of the term
                                          when 2 then 5                                         --when doing arrears
                                          when 3 then 2
                                          else 0
                                       end)
                        end;
            for i in 1 .. L_PAYMENT_TERMS.NUMBER_OF_TERMS
            loop
               if loc_date = A_MONTH then return 1; end if;
               loc_date := add_months(loc_date, case L_PAYMENT_TERMS.PAYMENT_FREQ_ID
                                          when 1 then 12
                                          when 2 then 6
                                          when 3 then 3
                                          else 1
                                       end);
            end loop;
            return 0;
         end if;
      end loop;
      return 0;
   exception
      when others then
         rollback;
         return -1;
   end F_PAYMENT_MONTH;
   --**************************************************************************
   --                            F_PAYMENT_II
   --**************************************************************************
   function F_PAYMENT_II( A_COMPANY_ID in number,
						  A_MONTH in date) return varchar2 is
		L_STATUS varchar2(30000);
		L_GL_JE_CODE varchar2(35);
		L_COUNTER number := 0;
		L_LOCATION varchar2(30000);
		L_RTN number;
	begin

		select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LAMPAY'))
		into L_GL_JE_CODE
		from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
		where E.JE_ID = G.JE_ID
		and G.PROCESS_ID = 'LAMPAY';

		for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
									LV.VENDOR_ID as VENDOR_ID,
									LV.PAYMENT_PCT as PAYMENT_PCT,
									LCM.AMOUNT as AMOUNT,
									L.WORK_ORDER_ID as WORK_ORDER_ID,
									L.COMPANY_ID as COMPANY_ID,
									3009 as TRANS_TYPE, /* CJS 3/30/15 New Trans Type for Interim Interest Debit */
									SB.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
									LIA.AP_ACCOUNT_ID as AP_ACCOUNT_ID
							 from LS_ASSET L
							 join LS_ILR LI on L.ILR_ID = LI.ILR_ID
							 join LS_LEASE LL on LL.LEASE_ID = LI.LEASE_ID
							 join LS_LEASE_VENDOR LV on LL.LEASE_ID = LV.LEASE_ID and L.COMPANY_ID = LV.COMPANY_ID
							 join LS_COMPONENT LC on L.LS_ASSET_ID = LC.LS_ASSET_ID
							 join LS_COMPONENT_MONTHLY_II_STG LCM on LCM.COMPONENT_ID = LC.COMPONENT_ID
							 join LS_ILR_ACCOUNT LIA on LI.ILR_ID = LIA.ILR_ID
							 cross join SET_OF_BOOKS SB
							 where L.COMPANY_ID = A_COMPANY_ID
							 and LCM.MONTH = A_MONTH
							 and nvl(lcm.amount,0) <> 0
							 and l.ls_asset_status_id = 3 or (l.ls_asset_status_id = 4 and L.retirement_date >= A_MONTH)
							 and l.ls_asset_id not in (select from_ls_asset_id from ls_asset_transfer_history where month = A_MONTH))
		loop
			L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
						TO_CHAR(L_PAYMENTS.TRANS_TYPE);
			L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
												   L_PAYMENTS.TRANS_TYPE,
												   nvl(L_PAYMENTS.AMOUNT,0) * nvl(L_PAYMENTS.PAYMENT_PCT,0),
												   0,
												   -1,
												   L_PAYMENTS.WORK_ORDER_ID,
												   -1, --No accrual account
												   0,
												   -1,
												   L_PAYMENTS.COMPANY_ID,
												   A_MONTH,
												   1,
												   L_GL_JE_CODE,
												   L_PAYMENTS.SET_OF_BOOKS_ID,
												   'ii',
												   L_STATUS);
			if L_RTN = -1 then
				return L_LOCATION || ' : ' || L_STATUS;
			end if;

			-- process the credit.  Payment credits all hit 3022
			L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
						' trans type: 3022';
			L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
												   3022,
												   nvl(L_PAYMENTS.AMOUNT,0) * nvl(L_PAYMENTS.PAYMENT_PCT,0),
												   0,
												   -1,
												   L_PAYMENTS.WORK_ORDER_ID,
												   L_PAYMENTS.AP_ACCOUNT_ID,
												   0,
												   -1,
												   L_PAYMENTS.COMPANY_ID,
												   A_MONTH,
												   0,
												   L_GL_JE_CODE,
												   L_PAYMENTS.SET_OF_BOOKS_ID,
												   'ii',
												   L_STATUS);
			if L_RTN = -1 then
				return L_LOCATION || ' : ' || L_STATUS;
			end if;
			L_COUNTER := L_COUNTER + 1;
		end loop;

		return 'OK';
    exception when others then
      return 'Error sending component interest journals : ' || sqlerrm;
	end F_PAYMENT_II;
   --**************************************************************************
   --                            F_APPROVE_MLA
   --**************************************************************************

   function F_APPROVE_MLA(A_LEASE_ID in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this revision
      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L
         set LEASE_STATUS_ID = 3, APPROVAL_DATE = sysdate, CURRENT_REVISION = A_REVISION
       where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving MLA');
         return -1;
   end F_APPROVE_MLA;

   --**************************************************************************
   --                            F_REJECT_MLA
   --**************************************************************************

   function F_REJECT_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L set LEASE_STATUS_ID = 4 where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Rejecting MLA');
         return -1;
   end F_REJECT_MLA;

   --**************************************************************************
   --                            F_SEND_MLA
   --**************************************************************************

   function F_SEND_MLA(A_LEASE_ID in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN     number;
      IS_AUTO WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
   begin

      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L set LEASE_STATUS_ID = 2 where L.LEASE_ID = A_LEASE_ID;

      --Reject other revisions
      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 5, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) <> NVL(A_REVISION, 0)
         and APPROVAL_STATUS_ID in (1, 2);

      --Call F_APPROVE_MLA for auto-approved assets
      commit;

      select NVL(EXTERNAL_WORKFLOW_TYPE, 'NA')
        into IS_AUTO
        from WORKFLOW_TYPE A, LS_LEASE L
       where A.WORKFLOW_TYPE_ID = L.WORKFLOW_TYPE_ID
         and L.LEASE_ID = A_LEASE_ID;

      if IS_AUTO = 'AUTO' then
         return F_APPROVE_MLA(A_LEASE_ID, A_REVISION);
      end if;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending MLA');
         return -1;
   end F_SEND_MLA;

   --**************************************************************************
   --                            F_UNREJECT_MLA
   --**************************************************************************

   function F_UNREJECT_MLA(A_LEASE_ID in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 7
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L set LEASE_STATUS_ID = 2 where L.LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Unrejecting MLA');
         return -1;
   end F_UNREJECT_MLA;

   --**************************************************************************
   --                            F_UNSEND_MLA
   --**************************************************************************

   function F_UNSEND_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L set LEASE_STATUS_ID = 1, APPROVAL_DATE = null where LEASE_ID = A_LEASE_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error UnSending MLA');
         return -1;
   end F_UNSEND_MLA;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_MLA
   --**************************************************************************

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_LEASE_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_LEASE_ID)
                                        and ID_FIELD2 = TO_CHAR(A_REVISION)
                                        and SUBSYSTEM = 'lessee_mla_approval'),
                                     0)
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Updating Workflow MLA');
         return -1;
   end F_UPDATE_WORKFLOW_MLA;

   --**************************************************************************
   --                            F_deletePendTrans
   --**************************************************************************
   function F_DELETEPENDTRANS(A_ILR_ID number,
                              A_MSG    out varchar2) return number is
      MY_RTN number;
   begin
      A_MSG := 'Clearing out ls_pend_class_code';
      delete from LS_PEND_CLASS_CODE
       where LS_PEND_TRANS_ID in
             (select A.LS_PEND_TRANS_ID from LS_PEND_TRANSACTION A where A.ILR_ID = A_ILR_ID);

      A_MSG := 'Clearing out ls_pend_basis';
      delete from LS_PEND_BASIS
       where LS_PEND_TRANS_ID in
             (select A.LS_PEND_TRANS_ID from LS_PEND_TRANSACTION A where A.ILR_ID = A_ILR_ID);

      A_MSG := 'Clearing out ls_pend_set_of_books';
      delete from LS_PEND_SET_OF_BOOKS
       where LS_PEND_TRANS_ID in
             (select A.LS_PEND_TRANS_ID from LS_PEND_TRANSACTION A where A.ILR_ID = A_ILR_ID);

      A_MSG := 'Clearing out ls_pend_transaction';
      delete from LS_PEND_TRANSACTION A where A.ILR_ID = A_ILR_ID;

      return 1;
   exception
      when others then
         return -1;
   end F_DELETEPENDTRANS;

   --**************************************************************************
   --                            F_archivePendTrans
   --**************************************************************************
   function F_ARCHIVEPENDTRANS(A_ILR_ID   in number,
                               A_REVISION in number,
                               A_MSG      out varchar2) return number is
      MY_RTN number;
   begin
      --archive
      A_MSG := 'Archiving ls_pend_transaction';
      insert into LS_PEND_TRANSACTION_ARC
         (LS_PEND_TRANS_ID, TIME_STAMP, USER_ID, LS_ASSET_ID, POSTING_AMOUNT, POSTING_QUANTITY,
          ACTIVITY_CODE, GL_JE_CODE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID,
          FUNC_CLASS_ID, ILR_ID, COMPANY_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID,
          LEASED_ASSET_NUMBER, SUB_ACCOUNT_ID, WORK_ORDER_ID, SERIAL_NUMBER, REVISION,
		  in_service_date, gl_posting_mo_yr /* WMD 20150225 */)
         select LS_PEND_TRANS_ID,
                TIME_STAMP,
                USER_ID,
                LS_ASSET_ID,
                POSTING_AMOUNT,
                POSTING_QUANTITY,
                ACTIVITY_CODE,
                GL_JE_CODE,
                RETIREMENT_UNIT_ID,
                UTILITY_ACCOUNT_ID,
                BUS_SEGMENT_ID,
                FUNC_CLASS_ID,
                ILR_ID,
                COMPANY_ID,
                ASSET_LOCATION_ID,
                PROPERTY_GROUP_ID,
                LEASED_ASSET_NUMBER,
                SUB_ACCOUNT_ID,
                WORK_ORDER_ID,
                SERIAL_NUMBER,
                REVISION,
				in_service_date,
				gl_posting_mo_yr /* WMD */
           from LS_PEND_TRANSACTION L
          where L.ILR_ID = A_ILR_ID
            and L.REVISION = A_REVISION;

      A_MSG := 'Archiving ls_pend_basis';
      insert into LS_PEND_BASIS_ARC
         (LS_PEND_TRANS_ID, TIME_STAMP, USER_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5,
          BASIS_6, BASIS_7, BASIS_8, BASIS_9, BASIS_10, BASIS_11, BASIS_12, BASIS_13, BASIS_14,
          BASIS_15, BASIS_16, BASIS_17, BASIS_18, BASIS_19, BASIS_20, BASIS_21, BASIS_22, BASIS_23,
          BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32,
          BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41,
          BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
          BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59,
          BASIS_60, BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68,
          BASIS_69, BASIS_70)
         select B.LS_PEND_TRANS_ID,
                B.TIME_STAMP,
                B.USER_ID,
                BASIS_1,
                BASIS_2,
                BASIS_3,
                BASIS_4,
                BASIS_5,
                BASIS_6,
                BASIS_7,
                BASIS_8,
                BASIS_9,
                BASIS_10,
                BASIS_11,
                BASIS_12,
                BASIS_13,
                BASIS_14,
                BASIS_15,
                BASIS_16,
                BASIS_17,
                BASIS_18,
                BASIS_19,
                BASIS_20,
                BASIS_21,
                BASIS_22,
                BASIS_23,
                BASIS_24,
                BASIS_25,
                BASIS_26,
                BASIS_27,
                BASIS_28,
                BASIS_29,
                BASIS_30,
                BASIS_31,
                BASIS_32,
                BASIS_33,
                BASIS_34,
                BASIS_35,
                BASIS_36,
                BASIS_37,
                BASIS_38,
                BASIS_39,
                BASIS_40,
                BASIS_41,
                BASIS_42,
                BASIS_43,
                BASIS_44,
                BASIS_45,
                BASIS_46,
                BASIS_47,
                BASIS_48,
                BASIS_49,
                BASIS_50,
                BASIS_51,
                BASIS_52,
                BASIS_53,
                BASIS_54,
                BASIS_55,
                BASIS_56,
                BASIS_57,
                BASIS_58,
                BASIS_59,
                BASIS_60,
                BASIS_61,
                BASIS_62,
                BASIS_63,
                BASIS_64,
                BASIS_65,
                BASIS_66,
                BASIS_67,
                BASIS_68,
                BASIS_69,
                BASIS_70
           from LS_PEND_BASIS B, LS_PEND_TRANSACTION A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION
            and A.LS_PEND_TRANS_ID = B.LS_PEND_TRANS_ID;

      A_MSG := 'Archiving ls_pend_set_of_books';
      insert into LS_PEND_SET_OF_BOOKS_ARC
         (LS_PEND_TRANS_ID, SET_OF_BOOKS_ID, POSTING_AMOUNT)
         select B.LS_PEND_TRANS_ID, B.SET_OF_BOOKS_ID, B.POSTING_AMOUNT
           from LS_PEND_SET_OF_BOOKS B, LS_PEND_TRANSACTION A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION
            and A.LS_PEND_TRANS_ID = B.LS_PEND_TRANS_ID;

      A_MSG := 'Archiving ls_pend_class_code';
      insert into LS_PEND_CLASS_CODE_ARC
         (CLASS_CODE_ID, LS_PEND_TRANS_ID, TIME_STAMP, USER_ID, value)
         select B.CLASS_CODE_ID, B.LS_PEND_TRANS_ID, B.TIME_STAMP, B.USER_ID, B.VALUE
           from LS_PEND_CLASS_CODE B, LS_PEND_TRANSACTION A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION
            and A.LS_PEND_TRANS_ID = B.LS_PEND_TRANS_ID;

      return F_DELETEPENDTRANS(A_ILR_ID, A_MSG);
   exception
      when others then
         return -1;
   end F_ARCHIVEPENDTRANS;

   --**************************************************************************
   --                            F_APPROVE_ILR
   --**************************************************************************
   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2) return number is
      RTN    number;
      MY_STR varchar2(2000);

   begin
      rtn := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, A_STATUS, TRUE);
      return rtn;
   exception
      when others then
         P_LOGERRORMESSAGE(A_ILR_ID, A_STATUS);

         return -1;
   end F_APPROVE_ILR_NO_COMMIT;

   function F_APPROVE_ILR_NO_COMMIT(A_ILR_ID   in number,
                                    A_REVISION in number,
                                    A_STATUS   out varchar2,
                                    A_LOG in boolean,
									a_is_transfer in boolean default false) return number is
      RTN    number;
      MY_STR varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_STATUS := 'APPROVE ILR:';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   ILR:' || TO_CHAR(A_ILR_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   REVISION: ' || TO_CHAR(A_REVISION);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      --Approve this revision
      -- blank out the error message
      --if A_LOG then
         --A_STATUS := 'Clearing out error message';
         --PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
         --P_LOGERRORMESSAGE(A_ILR_ID, '');
      --end if;

      A_STATUS := 'Updating LS_ILR_APPROVAL1';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);
      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Updating LS_ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_ILR L set ILR_STATUS_ID = 2, CURRENT_REVISION = A_REVISION where ILR_ID = A_ILR_ID;
      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

		if not a_is_transfer then
		  A_STATUS := 'Updating LS_ILR_OPTIONS';
		  PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
		  update LS_ILR_OPTIONS set IN_SERVICE_EXCHANGE_RATE =
		  (
			select RATE from CURRENCY_RATE_DEFAULT CR
			where EXCHANGE_DATE =
			  (
				select MAX(EXCHANGE_DATE)
				from CURRENCY_RATE_DEFAULT CR2
				where CR2.CURRENCY_FROM = CR.CURRENCY_FROM
				  and CR2.CURRENCY_TO   = CR.CURRENCY_TO
				  and CR2.EXCHANGE_DATE <= (sysdate)
				  and CR2.EXCHANGE_RATE_TYPE_ID = 1
			   )
			   and CURRENCY_FROM =
			   (
				 select CONTRACT_CURRENCY_ID as CURRENCY_FROM
				 from LS_LEASE L, LS_ILR I
				 where L.LEASE_ID = I.LEASE_ID
				   and I.ILR_ID = A_ILR_ID
			   )
			   and CURRENCY_TO =
			   (
				 select CS.CURRENCY_ID AS CURRENCY_TO
				 from LS_ILR ILR, CURRENCY_SCHEMA CS
				 where ILR_ID = A_ILR_ID
				   and ILR.COMPANY_ID = CS.COMPANY_ID
				   and CURRENCY_TYPE_ID = 1
				)
			  and CR.EXCHANGE_RATE_TYPE_ID = 1
		  )
		  where ILR_ID = A_ILR_ID and REVISION = A_REVISION
		  AND EXISTS
		  (SELECT 1 FROM ls_asset WHERE ILR_ID = A_ILR_ID
			  AND ls_asset_status_id in (1,2) )
      OR remeasurement_date IS NOT NULL;

		  A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
		  PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
		else
			pkg_pp_log.p_write_message('Transfer transaction - skip in_service_exchange_rate update');
		end IF;

		  -- LOAD THE CPR TABLES and create JEs... this is a call to the lease asset package
		  A_STATUS := 'Loop over assets';
		  PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
		  for L_ASSET_ID in (select LS_ASSET_ID
							   from LS_PEND_TRANSACTION
							  where ILR_ID = A_ILR_ID
								and REVISION = A_REVISION)
		  loop
			A_STATUS := '   Processing asset_id: ' || TO_CHAR(L_ASSET_ID.LS_ASSET_ID);
			if not a_is_transfer then
				 PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
				 pkg_pp_log.p_write_message('Processing asset_id: '|| TO_CHAR(L_ASSET_ID.LS_ASSET_ID) ||' Revision: '||to_char(a_revision));
				 MY_STR := PKG_LEASE_ASSET_POST.F_ADD_ASSET(L_ASSET_ID.LS_ASSET_ID);
				 PKG_PP_LOG.P_WRITE_MESSAGE('   Returned: ' || MY_STR);
				 if MY_STR <> 'OK' then
					-- log the error message
					A_STATUS := MY_STR;
					RAISE_APPLICATION_ERROR(-20000, 'Error Adding Leased Asset: ' || A_STATUS);
					return -1;
				 end if;
			else
				pkg_pp_log.p_write_message('Transfer transaction - skip adjustment; just update asset status and approved revision.');

				--update approved revision and asset status
				---- this is the only thing from the PKG_LEASE_ASSET_POST.F_ADD_ASSET() call in the IF case that we need to do here too.
				update LS_ASSET
				set APPROVED_REVISION = A_REVISION,
					LS_ASSET_STATUS_ID = 3
				where LS_ASSET_ID = L_ASSET_ID.LS_ASSET_ID;
			end IF;
		  end loop;

      A_STATUS := 'Archiving';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      RTN := F_ARCHIVEPENDTRANS(A_ILR_ID, A_REVISION, A_STATUS);
      if RTN <> 1 then
         RAISE_APPLICATION_ERROR(-20000, 'Error Archiving Transactions: ' || A_STATUS);
         return -1;
      end if;

      return 1;
   exception
      when others then
      --if A_LOG then
         --P_LOGERRORMESSAGE(A_ILR_ID, A_STATUS);
      -- end if;
       PKG_PP_LOG.P_WRITE_MESSAGE(sqlerrm);

         return -1;
   end F_APPROVE_ILR_NO_COMMIT;

   function F_APPROVE_ILR(A_ILR_ID   in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      MY_STR   varchar2(2000);
      L_STATUS varchar2(2000);

   begin
      -- start by clearing our pend transaction
      RTN := F_APPROVE_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending For Approval: ' || L_STATUS);
         return -1;
      end if;

      commit;
      return 1;
   exception
      when others then
         rollback;
         P_LOGERRORMESSAGE(A_ILR_ID, L_STATUS);
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving ILR: ' || L_STATUS);
         return -1;
   end F_APPROVE_ILR;

   --**************************************************************************
   --                            F_REJECT_ILR
   --**************************************************************************

   function F_REJECT_ILR(A_ILR_ID   in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      L_STATUS varchar2(2000);
   begin
      RTN := F_DELETEPENDTRANS(A_ILR_ID, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Removing Transactions: ' || L_STATUS);
         return -1;
      end if;

      update LS_ILR_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

 --     update LS_ILR L set ILR_STATUS_ID = 1 where ILR_ID = A_ILR_ID;

      update LS_ASSET
         set LS_ASSET_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and LS_ASSET_STATUS_ID = 2;

      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_REJECT_ILR;

   --**************************************************************************
   --                            F_SEND_ILR
   -- Called when a user clicks send for approval (on an ILR)
   -- Need to mark the approval status to be sent.  And update the ILR to
   -- pending approval.  IN addition need to update the capitalized cost to
   -- be the beginning obligation on the asset schedule for the revision being
   -- sent for approval
   --**************************************************************************
   function F_SEND_ILR_NO_COMMIT(A_ILR_ID   in number,
                                 A_REVISION in number,
                                 A_STATUS   out varchar2) return number is
      RTN     number;
      ILR     number;
      WFS     number;
      SQLS    varchar2(32000);
      IS_AUTO WORKFLOW_TYPE.EXTERNAL_WORKFLOW_TYPE%type;
      APPROVED_DATE date;
      START_DATE date;
	  OPEN_MONTH date;
	  GL_POSTING_MO_YR date;
      v_component_count number;
      L_REMEASUREMENT_DATE date;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_STATUS := 'SEND for approval:';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   ILR_ID: ' || TO_CHAR(A_ILR_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      A_STATUS := '   REVISION: ' || TO_CHAR(A_REVISION);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

     --Check that the lease is valid ie in Open/New Revision status
	 -- /* WMD Have to be able to create new revisions on "Closed" leases
	 -- since new revisions are created for retirements and transfers */
     select count(1)
     into RTN
     from LS_LEASE LL, LS_ILR LI
     where LI.ILR_ID = A_ILR_ID
     and LI.LEASE_ID = LL.LEASE_ID
     and (LL.LEASE_STATUS_ID in (3,7)
	 or (LL.LEASE_STATUS_ID = 5
           and A_REVISION <> 1));

     if RTN <> 1 then
      A_STATUS := 'MLA for this ILR is not in Open or New Revision status, so the ILR cannot be posted to the MLA.';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      return -1;
     end if;


      A_STATUS := 'Remove Prior pending transactions for ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      -- start by clearing our pend transaction
      RTN := F_DELETEPENDTRANS(A_ILR_ID, A_STATUS);
      if RTN <> 1 then
         return -1;
      end if;

      A_STATUS := 'Getting start date';
      select min(PAYMENT_TERM_DATE)
        into START_DATE
      from LS_ILR_PAYMENT_TERM
        where ILR_ID = A_ILR_ID
          and REVISION = A_REVISION;

      A_STATUS := 'Getting approved date';
      select min(PAYMENT_TERM_DATE)
        into APPROVED_DATE
      from ls_ilr_payment_term pt, ls_ilr_approval appr
      where pt.ilr_id = appr.ilr_id
      and pt.revision = appr.revision
      and appr.ilr_id = A_ILR_ID
      and appr.revision = (
         select max(revision)
         from ls_ilr_approval
         where ilr_id = appr.ilr_id
           and approval_status_id in (3,6)
           and revision <> A_REVISION
      );

      A_STATUS := 'Getting remeasurement date';
      select Trunc(remeasurement_date, 'month') remeasurement_date
        into L_REMEASUREMENT_DATE
      from LS_ILR_OPTIONS
      where ILR_ID = A_ILR_ID
      and REVISION = A_REVISION;

     A_STATUS := 'Getting open month';
     PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
     select min(gl_posting_mo_yr)
      into OPEN_MONTH
     from ls_process_control
     where company_id = (select company_id from ls_ilr where ilr_id = A_ILR_ID)
     and lam_closed is null;

      PKG_PP_LOG.P_WRITE_MESSAGE('   Start Date: ' || to_char(START_DATE, 'YYYY-MM-DD'));
      PKG_PP_LOG.P_WRITE_MESSAGE('   Approved Date: ' || to_char(APPROVED_DATE, 'YYYY-MM-DD'));
      PKG_PP_LOG.P_WRITE_MESSAGE('   Open Month: ' || to_char(OPEN_MONTH, 'YYYY-MM-DD'));


      if L_REMEASUREMENT_DATE is not null then
        PKG_PP_LOG.P_WRITE_MESSAGE('   Remeasurement Date (Month): ' || to_char(L_REMEASUREMENT_DATE, 'YYYY-MM-DD'));
      end if;

	   --  Can change the start date as long as the prior start date is not yet been reached
      if APPROVED_DATE <> START_DATE and trunc(APPROVED_DATE, 'mm') < trunc(OPEN_MONTH, 'mm') and APPROVED_DATE is not null then
        A_STATUS := 'Cannot change the in service date of the ILR after the in service date has been reached';
        PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

        return -1;
      end if;


      A_STATUS := 'updating LS_ILR_APPROVAL';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      A_STATUS := 'updating LS_ASSET';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      -- update the asset status to be pending.  Also update the cap cost here.
      update LS_ASSET L
         set LS_ASSET_STATUS_ID = 2
       where ILR_ID = A_ILR_ID
         and LS_ASSET_STATUS_ID = 1;

      A_STATUS := 'updating LS_ILR';
	  update ls_ilr
	  set est_in_svc_date = START_DATE
	  where ilr_id = a_ilr_id;

      -- identify whether or not it is an adjustment or addition
      A_STATUS := 'insert into LS_PEND_TRANSACTION';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      insert into LS_PEND_TRANSACTION
         (LS_PEND_TRANS_ID, LS_ASSET_ID, POSTING_AMOUNT, POSTING_QUANTITY, ACTIVITY_CODE,
          GL_JE_CODE, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, FUNC_CLASS_ID, ILR_ID,
          COMPANY_ID, ASSET_LOCATION_ID, PROPERTY_GROUP_ID, LEASED_ASSET_NUMBER, SUB_ACCOUNT_ID,
          WORK_ORDER_ID, SERIAL_NUMBER, REVISION, IN_SERVICE_DATE, GL_POSTING_MO_YR)
         select LS_PEND_TRANSACTION_SEQ.NEXTVAL,
                L.LS_ASSET_ID,
                NVL(AA.END_CAPITAL_COST, 0) - NVL((select min(AA2.END_CAPITAL_COST)
                                                    from LS_ASSET_SCHEDULE AA2, ls_asset la2

                                                   where AA2.LS_ASSET_ID = L.LS_ASSET_ID
                                                     and AA2.REVISION = L.APPROVED_REVISION
                                                     and AA2.SET_OF_BOOKS_ID = 1
                                                     and nvl(GL_POSTING_MO_YR, AA2.MONTH) = AA2.MONTH
                                                     and la2.ls_asset_id = aa2.ls_asset_id
                                                     and aa2.month>=(select min(gl_posting_mo_yr) from ls_process_control where company_id = la2.company_id and lam_closed is null)),
                                                  0),
                NVL(L.QUANTITY, 1),
                NVL((select case
                              when L.APPROVED_REVISION > 0 then
                               11
                              else
                               2
                           end
                      from LS_CPR_ASSET_MAP M
                     where M.LS_ASSET_ID = L.LS_ASSET_ID),
                    2),
             case when NVL(L.APPROVED_REVISION,0) > 0 then
                              (select S.GL_JE_CODE
                               from STANDARD_JOURNAL_ENTRIES S, GL_JE_CONTROL G
                              where G.PROCESS_ID = 'LAM REM'
                                and G.JE_ID = S.JE_ID)
                            else
                               (select S.GL_JE_CODE
                               from STANDARD_JOURNAL_ENTRIES S, GL_JE_CONTROL G
                              where G.PROCESS_ID = 'LAM ADDS'
                                and G.JE_ID = S.JE_ID)
                           end,
                L.RETIREMENT_UNIT_ID,
                L.UTILITY_ACCOUNT_ID,
                L.BUS_SEGMENT_ID,
                UA.FUNC_CLASS_ID,
                L.ILR_ID,
                L.COMPANY_ID,
                L.ASSET_LOCATION_ID,
                L.PROPERTY_GROUP_ID,
                L.LEASED_ASSET_NUMBER,
                L.SUB_ACCOUNT_ID,
                L.WORK_ORDER_ID,
                L.SERIAL_NUMBER,
                A_REVISION,
				START_DATE,
				GL_POSTING_MO_YR
           from LS_ASSET L,
                UTILITY_ACCOUNT UA,
                (select A.*,
                        ROW_NUMBER() OVER(partition by A.LS_ASSET_ID, A.SET_OF_BOOKS_ID, A.REVISION order by month) as THE_ROW
                   from LS_ASSET_SCHEDULE A, LS_ASSET LA
                  where A.LS_ASSET_ID = LA.LS_ASSET_ID
                    and LA.ILR_ID = A_ILR_ID
                    and A.REVISION = A_REVISION
                    and A.SET_OF_BOOKS_ID = 1
                    and a.month >= (select min(gl_posting_mo_yr) from ls_process_control where company_id = la.company_id and lam_closed is null)) AA
          where L.ILR_ID = A_ILR_ID
            and DECODE(GL_POSTING_MO_YR, NULL, AA.THE_ROW, 1) = 1 /* WMD */
			and nvl(GL_POSTING_MO_YR, AA.MONTH) = AA.MONTH
			and L.LS_ASSET_STATUS_ID <> 4
            and AA.LS_ASSET_ID = L.LS_ASSET_ID
            and UA.UTILITY_ACCOUNT_ID = L.UTILITY_ACCOUNT_ID
            and UA.BUS_SEGMENT_ID = L.BUS_SEGMENT_ID
            and exists (select 1
                   from LS_ASSET_SCHEDULE S
                  where S.REVISION = A_REVISION
                    and S.LS_ASSET_ID = L.LS_ASSET_ID);

      A_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Getting Books Summary';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      select A.BOOK_SUMMARY_ID
        into RTN
        from LS_LEASE_CAP_TYPE A, LS_ILR_OPTIONS B
       where A.LS_LEASE_CAP_TYPE_ID = B.LEASE_CAP_TYPE_ID
         and B.ILR_ID = A_ILR_ID
         and B.REVISION = A_REVISION;

      PKG_PP_LOG.P_WRITE_MESSAGE('   ' || TO_CHAR(RTN));

      A_STATUS := 'insert into LS_PEND_BASIS';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      insert into LS_PEND_BASIS
         (LS_PEND_TRANS_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6, BASIS_7, BASIS_8,
          BASIS_9, BASIS_10, BASIS_11, BASIS_12, BASIS_13, BASIS_14, BASIS_15, BASIS_16, BASIS_17,
          BASIS_18, BASIS_19, BASIS_20, BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25, BASIS_26,
          BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32, BASIS_33, BASIS_34, BASIS_35,
          BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41, BASIS_42, BASIS_43, BASIS_44,
          BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50, BASIS_51, BASIS_52, BASIS_53,
          BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60, BASIS_61, BASIS_62,
          BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68, BASIS_69, BASIS_70)
         select B.LS_PEND_TRANS_ID,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
           from LS_PEND_TRANSACTION B
          where B.ILR_ID = A_ILR_ID;

      A_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      -- backfill prior cpr_ldg_basis values
      A_STATUS := 'backfill buckets from cpr';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_PEND_BASIS PB
         set (BASIS_1,
               BASIS_2,
               BASIS_3,
               BASIS_4,
               BASIS_5,
               BASIS_6,
               BASIS_7,
               BASIS_8,
               BASIS_9,
               BASIS_10,
               BASIS_11,
               BASIS_12,
               BASIS_13,
               BASIS_14,
               BASIS_15,
               BASIS_16,
               BASIS_17,
               BASIS_18,
               BASIS_19,
               BASIS_20,
               BASIS_21,
               BASIS_22,
               BASIS_23,
               BASIS_24,
               BASIS_25,
               BASIS_26,
               BASIS_27,
               BASIS_28,
               BASIS_29,
               BASIS_30,
               BASIS_31,
               BASIS_32,
               BASIS_33,
               BASIS_34,
               BASIS_35,
               BASIS_36,
               BASIS_37,
               BASIS_38,
               BASIS_39,
               BASIS_40,
               BASIS_41,
               BASIS_42,
               BASIS_43,
               BASIS_44,
               BASIS_45,
               BASIS_46,
               BASIS_47,
               BASIS_48,
               BASIS_49,
               BASIS_50,
               BASIS_51,
               BASIS_52,
               BASIS_53,
               BASIS_54,
               BASIS_55,
               BASIS_56,
               BASIS_57,
               BASIS_58,
               BASIS_59,
               BASIS_60,
               BASIS_61,
               BASIS_62,
               BASIS_63,
               BASIS_64,
               BASIS_65,
               BASIS_66,
               BASIS_67,
               BASIS_68,
               BASIS_69,
               BASIS_70) =
              (select -1 * (C.BASIS_1 + P.BASIS_1) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_2 + P.BASIS_2) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_3 + P.BASIS_3) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_4 + P.BASIS_4) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_5 + P.BASIS_5) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_6 + P.BASIS_6) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_7 + P.BASIS_7) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_8 + P.BASIS_8) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_9 + P.BASIS_9) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_10 + P.BASIS_10) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_11 + P.BASIS_11) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_12 + P.BASIS_12) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_13 + P.BASIS_13) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_14 + P.BASIS_14) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_15 + P.BASIS_15) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_16 + P.BASIS_16) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_17 + P.BASIS_17) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_18 + P.BASIS_18) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_19 + P.BASIS_19) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_20 + P.BASIS_20) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_21 + P.BASIS_21) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_22 + P.BASIS_22) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_23 + P.BASIS_23) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_24 + P.BASIS_24) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_25 + P.BASIS_25) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_26 + P.BASIS_26) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_27 + P.BASIS_27) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_28 + P.BASIS_28) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_29 + P.BASIS_29) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_30 + P.BASIS_30) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_31 + P.BASIS_31) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_32 + P.BASIS_32) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_33 + P.BASIS_33) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_34 + P.BASIS_34) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_35 + P.BASIS_35) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_36 + P.BASIS_36) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_37 + P.BASIS_37) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_38 + P.BASIS_38) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_39 + P.BASIS_39) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_40 + P.BASIS_40) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_41 + P.BASIS_41) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_42 + P.BASIS_42) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_43 + P.BASIS_43) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_44 + P.BASIS_44) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_45 + P.BASIS_45) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_46 + P.BASIS_46) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_47 + P.BASIS_47) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_48 + P.BASIS_48) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_49 + P.BASIS_49) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_50 + P.BASIS_50) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_51 + P.BASIS_51) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_52 + P.BASIS_52) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_53 + P.BASIS_53) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_54 + P.BASIS_54) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_55 + P.BASIS_55) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_56 + P.BASIS_56) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_57 + P.BASIS_57) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_58 + P.BASIS_58) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_59 + P.BASIS_59) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_60 + P.BASIS_60) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_61 + P.BASIS_61) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_62 + P.BASIS_62) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_63 + P.BASIS_63) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_64 + P.BASIS_64) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_65 + P.BASIS_65) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_66 + P.BASIS_66) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_67 + P.BASIS_67) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_68 + P.BASIS_68) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_69 + P.BASIS_69) / p.in_service_exchange_rate,
                      -1 * (C.BASIS_70 + P.BASIS_70) / p.in_service_exchange_rate
                 from CPR_LDG_BASIS C,
                      (select M1.ASSET_ID as ASSET_ID,
                              PT.LS_PEND_TRANS_ID as LS_PEND_TRANS_ID,
							  ilro.in_service_exchange_rate AS in_service_exchange_rate,
                              sum(NVL(PB.BASIS_1, 0)) as BASIS_1,
                              sum(NVL(PB.BASIS_2, 0)) as BASIS_2,
                              sum(NVL(PB.BASIS_3, 0)) as BASIS_3,
                              sum(NVL(PB.BASIS_4, 0)) as BASIS_4,
                              sum(NVL(PB.BASIS_5, 0)) as BASIS_5,
                              sum(NVL(PB.BASIS_6, 0)) as BASIS_6,
                              sum(NVL(PB.BASIS_7, 0)) as BASIS_7,
                              sum(NVL(PB.BASIS_8, 0)) as BASIS_8,
                              sum(NVL(PB.BASIS_9, 0)) as BASIS_9,
                              sum(NVL(PB.BASIS_10, 0)) as BASIS_10,
                              sum(NVL(PB.BASIS_11, 0)) as BASIS_11,
                              sum(NVL(PB.BASIS_12, 0)) as BASIS_12,
                              sum(NVL(PB.BASIS_13, 0)) as BASIS_13,
                              sum(NVL(PB.BASIS_14, 0)) as BASIS_14,
                              sum(NVL(PB.BASIS_15, 0)) as BASIS_15,
                              sum(NVL(PB.BASIS_16, 0)) as BASIS_16,
                              sum(NVL(PB.BASIS_17, 0)) as BASIS_17,
                              sum(NVL(PB.BASIS_18, 0)) as BASIS_18,
                              sum(NVL(PB.BASIS_19, 0)) as BASIS_19,
                              sum(NVL(PB.BASIS_20, 0)) as BASIS_20,
                              sum(NVL(PB.BASIS_21, 0)) as BASIS_21,
                              sum(NVL(PB.BASIS_22, 0)) as BASIS_22,
                              sum(NVL(PB.BASIS_23, 0)) as BASIS_23,
                              sum(NVL(PB.BASIS_24, 0)) as BASIS_24,
                              sum(NVL(PB.BASIS_25, 0)) as BASIS_25,
                              sum(NVL(PB.BASIS_26, 0)) as BASIS_26,
                              sum(NVL(PB.BASIS_27, 0)) as BASIS_27,
                              sum(NVL(PB.BASIS_28, 0)) as BASIS_28,
                              sum(NVL(PB.BASIS_29, 0)) as BASIS_29,
                              sum(NVL(PB.BASIS_30, 0)) as BASIS_30,
                              sum(NVL(PB.BASIS_31, 0)) as BASIS_31,
                              sum(NVL(PB.BASIS_32, 0)) as BASIS_32,
                              sum(NVL(PB.BASIS_33, 0)) as BASIS_33,
                              sum(NVL(PB.BASIS_34, 0)) as BASIS_34,
                              sum(NVL(PB.BASIS_35, 0)) as BASIS_35,
                              sum(NVL(PB.BASIS_36, 0)) as BASIS_36,
                              sum(NVL(PB.BASIS_37, 0)) as BASIS_37,
                              sum(NVL(PB.BASIS_38, 0)) as BASIS_38,
                              sum(NVL(PB.BASIS_39, 0)) as BASIS_39,
                              sum(NVL(PB.BASIS_40, 0)) as BASIS_40,
                              sum(NVL(PB.BASIS_41, 0)) as BASIS_41,
                              sum(NVL(PB.BASIS_42, 0)) as BASIS_42,
                              sum(NVL(PB.BASIS_43, 0)) as BASIS_43,
                              sum(NVL(PB.BASIS_44, 0)) as BASIS_44,
                              sum(NVL(PB.BASIS_45, 0)) as BASIS_45,
                              sum(NVL(PB.BASIS_46, 0)) as BASIS_46,
                              sum(NVL(PB.BASIS_47, 0)) as BASIS_47,
                              sum(NVL(PB.BASIS_48, 0)) as BASIS_48,
                              sum(NVL(PB.BASIS_49, 0)) as BASIS_49,
                              sum(NVL(PB.BASIS_50, 0)) as BASIS_50,
                              sum(NVL(PB.BASIS_51, 0)) as BASIS_51,
                              sum(NVL(PB.BASIS_52, 0)) as BASIS_52,
                              sum(NVL(PB.BASIS_53, 0)) as BASIS_53,
                              sum(NVL(PB.BASIS_54, 0)) as BASIS_54,
                              sum(NVL(PB.BASIS_55, 0)) as BASIS_55,
                              sum(NVL(PB.BASIS_56, 0)) as BASIS_56,
                              sum(NVL(PB.BASIS_57, 0)) as BASIS_57,
                              sum(NVL(PB.BASIS_58, 0)) as BASIS_58,
                              sum(NVL(PB.BASIS_59, 0)) as BASIS_59,
                              sum(NVL(PB.BASIS_60, 0)) as BASIS_60,
                              sum(NVL(PB.BASIS_61, 0)) as BASIS_61,
                              sum(NVL(PB.BASIS_62, 0)) as BASIS_62,
                              sum(NVL(PB.BASIS_63, 0)) as BASIS_63,
                              sum(NVL(PB.BASIS_64, 0)) as BASIS_64,
                              sum(NVL(PB.BASIS_65, 0)) as BASIS_65,
                              sum(NVL(PB.BASIS_66, 0)) as BASIS_66,
                              sum(NVL(PB.BASIS_67, 0)) as BASIS_67,
                              sum(NVL(PB.BASIS_68, 0)) as BASIS_68,
                              sum(NVL(PB.BASIS_69, 0)) as BASIS_69,
                              sum(NVL(PB.BASIS_70, 0)) as BASIS_70
                         from PEND_BASIS          PB,
                              PEND_TRANSACTION    PP,
                              LS_CPR_ASSET_MAP    M1,
                              LS_PEND_TRANSACTION PT,
							  ls_ilr li,
							  ls_ilr_options ilro
                        where PP.LDG_ASSET_ID(+) = M1.ASSET_ID
                          and PP.PEND_TRANS_ID = PB.PEND_TRANS_ID(+)
                          and pp.ferc_activity_code (+) = 2
                          and PT.ILR_ID = A_ILR_ID
                          and M1.LS_ASSET_ID = PT.LS_ASSET_ID
						  AND li.ilr_id = a_ilr_id
						  AND ilro.ilr_id = a_ilr_id
						  AND ilro.revision = li.current_revision
                        group by M1.ASSET_ID, PT.LS_PEND_TRANS_ID, ilro.in_service_exchange_rate) P
                where C.ASSET_ID = P.ASSET_ID
                  and P.LS_PEND_TRANS_ID = PB.LS_PEND_TRANS_ID)
       where exists (select 1
                from LS_CPR_ASSET_MAP M, LS_PEND_TRANSACTION PT
               where M.LS_ASSET_ID = PT.LS_ASSET_ID
                 and PT.ILR_ID = A_ILR_ID
                 and PT.LS_PEND_TRANS_ID = PB.LS_PEND_TRANS_ID);

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      ILR      := A_ILR_ID;
	  /* WMD had to update to use end capital cost */
      A_STATUS := 'dynamic update of basis bucket';
      SQLS     := 'UPDATE LS_PEND_BASIS B' || ' SET BASIS_' || TO_CHAR(RTN) || ' = ' || ' (' ||
                  ' SELECT B.BASIS_' || TO_CHAR(RTN) ||
                  '   + case when max(end_capital_cost) = 0 then max(beg_obligation) else max(end_capital_cost) end' ||
                  ' from' || ' (' ||
                  '  select pt.ls_pend_trans_id, las.beg_obligation, las.end_capital_cost, las.month,' ||
                  '     row_number() over(partition by las.ls_asset_id, las.revision, las.set_of_books_id order by las.month) as the_row' ||
                  '  from ls_asset_schedule las, ls_pend_transaction pt' ||
                  '  where pt.ls_asset_id = las.ls_asset_id' ||
                  '  and las.month >= (select min(gl_posting_mo_yr) from ls_process_control where company_id = pt.company_id and lam_closed is null) ' ||
                  '  and las.revision = ' ||
                  TO_CHAR(A_REVISION) || '  and pt.ilr_id = ' || TO_CHAR(A_ILR_ID) || ' ) bbb' ||
                  ' where ';
          if L_REMEASUREMENT_DATE is not null then
            sqls:=sqls || 'bbb.month >= to_date('|| to_char(L_REMEASUREMENT_DATE,'YYYYMM') ||', ''yyyymm'')';
          else
          /* WMD */
          if gl_posting_mo_yr is null then
            sqls:=sqls || 'bbb.the_row = 1';
          else
            sqls:=sqls || 'bbb.month = to_date('|| to_char(GL_POSTING_MO_YR,'YYYYMM') ||', ''yyyymm'')';
          end if;
          end if;
            sqls:=sqls || ' and bbb.ls_pend_trans_id = b.ls_pend_trans_id' ||
                  ' group by bbb.ls_pend_trans_id' || ' )' ||
                  ' where exists (select 1 from ls_pend_transaction d where d.ilr_id = ' ||
                  TO_CHAR(A_ILR_ID) || ' and b.ls_pend_trans_id = d.ls_pend_trans_id)';

	  A_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF A_STATUS <> 'OK' THEN
        PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
        RETURN -1;
      END IF;

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(SQLS);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'insert into ls_pend_class_code';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      insert into LS_PEND_CLASS_CODE
         (CLASS_CODE_ID, LS_PEND_TRANS_ID, value)
         select A.CLASS_CODE_ID, B.LS_PEND_TRANS_ID, A.VALUE
           from LS_ASSET_CLASS_CODE A, LS_PEND_TRANSACTION B
          where A.LS_ASSET_ID = B.LS_ASSET_ID
            and B.ILR_ID = A_ILR_ID;

      A_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      -- insert into ls_pend_set_of_books
      A_STATUS := 'insert into ls_pend_set_of_books';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      insert into LS_PEND_SET_OF_BOOKS
         (LS_PEND_TRANS_ID, SET_OF_BOOKS_ID, POSTING_AMOUNT)
         select L.LS_PEND_TRANS_ID,
                S.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                S.BASIS_1_INDICATOR * PB.BASIS_1 + S.BASIS_2_INDICATOR * PB.BASIS_2 +
                S.BASIS_3_INDICATOR * PB.BASIS_3 + S.BASIS_4_INDICATOR * PB.BASIS_4 +
                S.BASIS_5_INDICATOR * PB.BASIS_5 + S.BASIS_6_INDICATOR * PB.BASIS_6 +
                S.BASIS_7_INDICATOR * PB.BASIS_7 + S.BASIS_8_INDICATOR * PB.BASIS_8 +
                S.BASIS_9_INDICATOR * PB.BASIS_9 + S.BASIS_10_INDICATOR * PB.BASIS_10 +
                S.BASIS_11_INDICATOR * PB.BASIS_11 + S.BASIS_12_INDICATOR * PB.BASIS_12 +
                S.BASIS_13_INDICATOR * PB.BASIS_13 + S.BASIS_14_INDICATOR * PB.BASIS_14 +
                S.BASIS_15_INDICATOR * PB.BASIS_15 + S.BASIS_16_INDICATOR * PB.BASIS_16 +
                S.BASIS_17_INDICATOR * PB.BASIS_17 + S.BASIS_18_INDICATOR * PB.BASIS_18 +
                S.BASIS_19_INDICATOR * PB.BASIS_19 + S.BASIS_20_INDICATOR * PB.BASIS_20 +
                S.BASIS_21_INDICATOR * PB.BASIS_21 + S.BASIS_22_INDICATOR * PB.BASIS_22 +
                S.BASIS_23_INDICATOR * PB.BASIS_23 + S.BASIS_24_INDICATOR * PB.BASIS_24 +
                S.BASIS_25_INDICATOR * PB.BASIS_25 + S.BASIS_26_INDICATOR * PB.BASIS_26 +
                S.BASIS_27_INDICATOR * PB.BASIS_27 + S.BASIS_28_INDICATOR * PB.BASIS_28 +
                S.BASIS_29_INDICATOR * PB.BASIS_29 + S.BASIS_30_INDICATOR * PB.BASIS_30 +
                S.BASIS_31_INDICATOR * PB.BASIS_31 + S.BASIS_32_INDICATOR * PB.BASIS_32 +
                S.BASIS_33_INDICATOR * PB.BASIS_33 + S.BASIS_34_INDICATOR * PB.BASIS_34 +
                S.BASIS_35_INDICATOR * PB.BASIS_35 + S.BASIS_36_INDICATOR * PB.BASIS_36 +
                S.BASIS_37_INDICATOR * PB.BASIS_37 + S.BASIS_38_INDICATOR * PB.BASIS_38 +
                S.BASIS_39_INDICATOR * PB.BASIS_39 + S.BASIS_40_INDICATOR * PB.BASIS_40 +
                S.BASIS_41_INDICATOR * PB.BASIS_41 + S.BASIS_42_INDICATOR * PB.BASIS_42 +
                S.BASIS_43_INDICATOR * PB.BASIS_43 + S.BASIS_44_INDICATOR * PB.BASIS_44 +
                S.BASIS_45_INDICATOR * PB.BASIS_45 + S.BASIS_46_INDICATOR * PB.BASIS_46 +
                S.BASIS_47_INDICATOR * PB.BASIS_47 + S.BASIS_48_INDICATOR * PB.BASIS_48 +
                S.BASIS_49_INDICATOR * PB.BASIS_49 + S.BASIS_50_INDICATOR * PB.BASIS_50 +
                S.BASIS_51_INDICATOR * PB.BASIS_51 + S.BASIS_52_INDICATOR * PB.BASIS_52 +
                S.BASIS_53_INDICATOR * PB.BASIS_53 + S.BASIS_54_INDICATOR * PB.BASIS_54 +
                S.BASIS_55_INDICATOR * PB.BASIS_55 + S.BASIS_56_INDICATOR * PB.BASIS_56 +
                S.BASIS_57_INDICATOR * PB.BASIS_57 + S.BASIS_58_INDICATOR * PB.BASIS_58 +
                S.BASIS_59_INDICATOR * PB.BASIS_59 + S.BASIS_60_INDICATOR * PB.BASIS_60 +
                S.BASIS_61_INDICATOR * PB.BASIS_61 + S.BASIS_62_INDICATOR * PB.BASIS_62 +
                S.BASIS_63_INDICATOR * PB.BASIS_63 + S.BASIS_64_INDICATOR * PB.BASIS_64 +
                S.BASIS_65_INDICATOR * PB.BASIS_65 + S.BASIS_66_INDICATOR * PB.BASIS_66 +
                S.BASIS_67_INDICATOR * PB.BASIS_67 + S.BASIS_68_INDICATOR * PB.BASIS_68 +
                S.BASIS_69_INDICATOR * PB.BASIS_69 + S.BASIS_70_INDICATOR * PB.BASIS_70 as AMOUNT
           from SET_OF_BOOKS S, COMPANY_SET_OF_BOOKS C, LS_PEND_BASIS PB, LS_PEND_TRANSACTION L
          where PB.LS_PEND_TRANS_ID = L.LS_PEND_TRANS_ID
            and C.COMPANY_ID = L.COMPANY_ID
            and S.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
            and L.ILR_ID = A_ILR_ID;
      A_STATUS := '   Rows inserted: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'Setting posting amount on ls_pend_transaction';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_PEND_TRANSACTION PT
         set PT.POSTING_AMOUNT =
              (select S.POSTING_AMOUNT
                 from LS_PEND_SET_OF_BOOKS S
                where S.SET_OF_BOOKS_ID = 1
                  and S.LS_PEND_TRANS_ID = PT.LS_PEND_TRANS_ID)
       where ILR_ID = A_ILR_ID;

      --Update status of non-current revisions
      A_STATUS := 'update workflow';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update WORKFLOW
         set APPROVAL_STATUS_ID = 5
       where ID_FIELD1 = A_ILR_ID
         and ID_FIELD2 <> A_REVISION
         and APPROVAL_STATUS_ID = 2
         and SUBSYSTEM = 'lessee_ilr_approval';

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      A_STATUS := 'update ls_ilr_approval';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 5
       where ILR_ID = A_ILR_ID
         and REVISION <> A_REVISION
         and APPROVAL_STATUS_ID = 2;

      A_STATUS := '   Rows updated: ' || TO_CHAR(sql%rowcount);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_STATUS);

      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');

      return 1;
   exception
      when others then
         PKG_PP_LOG.P_WRITE_MESSAGE('Error Sending For Approval: ' || sqlerrm);

         return -1;
   end F_SEND_ILR_NO_COMMIT;

   function F_SEND_ILR(A_ILR_ID   in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      L_STATUS varchar2(2000);
   begin
      -- start by clearing our pend transaction
      RTN := F_SEND_ILR_NO_COMMIT(A_ILR_ID, A_REVISION, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending For Approval: ' || L_STATUS);
         return -1;
      end if;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending ILR: ' || L_STATUS);
         return -1;
   end F_SEND_ILR;

   --**************************************************************************
   --                            F_UNREJECT_ILR
   --**************************************************************************

   function F_UNREJECT_ILR(A_ILR_ID   in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_UNREJECT_ILR;

   --**************************************************************************
   --                            F_UNSEND_ILR
   --**************************************************************************

   function F_UNSEND_ILR(A_ILR_ID   in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN      number;
      L_STATUS varchar2(2000);
   begin
      RTN := F_DELETEPENDTRANS(A_ILR_ID, L_STATUS);
      if RTN <> 1 then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Removing Transactions: ' || L_STATUS);
         return -1;
      end if;

      update LS_ILR_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

--      update LS_ILR L set ILR_STATUS_ID = 1 where ILR_ID = A_ILR_ID;

      update LS_ASSET
         set LS_ASSET_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and LS_ASSET_STATUS_ID = 2;

      --update workflow and ilr status

-- RO: This had to be removed to prevent deadlocks
--     Workflow gets updated in uo_workflow_tools
--
--      update WORKFLOW
--         set APPROVAL_STATUS_ID = 1
--       where ID_FIELD1 = A_ILR_ID
--         and ID_FIELD2 = A_REVISION
--         and APPROVAL_STATUS_ID = 2
--         and SUBSYSTEM = 'ilr_approval';

      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and REVISION = A_REVISION
         and APPROVAL_STATUS_ID = 2;

      commit;

      return 1;
   exception
      when others then
         rollback;
         return -1;
   end F_UNSEND_ILR;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_ILR
   --**************************************************************************

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID   in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_ILR_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_ILR_ID)
                                        and ID_FIELD2 = TO_CHAR(A_REVISION)
                                        and SUBSYSTEM = 'lessee_ilr_approval'),
                                     0)
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_UPDATE_WORKFLOW_ILR;

   function F_FLOATING_RATE_SF_ACCRUAL(A_LEASE_ID in number,
                               A_COMPANY_ID in number,
                               A_MONTH      in date,
                               A_ILR_ID in number:=null,
                               A_REVISION in number:=null) return varchar2 is
      COUNTER  number;
      BUCKET   LS_RENT_BUCKET_ADMIN%ROWTYPE;
      SQLS     varchar2(10000);
      L_STATUS varchar2(10000);
      L_MAX    varchar2(8);
   begin
      /* Get the floating rates bucket */
      L_STATUS := 'Insert floating rate amounts on schedule';
      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET = 1;

      if COUNTER > 1 then
         return 'Error: there must be exactly one bucket defined as the floating rate bucket';
		elsif COUNTER = 0 then
			return 'OK';
      end if;

      L_STATUS := 'Build SQL string';
      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET = 1;

      /* Get maximum month */
	  COUNTER := 0;
      select to_char(max(month),'yyyymm'), count(1)
      into L_MAX, COUNTER
      from ls_ilr_schedule
      where ilr_id in
         (select a.ilr_id from ls_ilr a, ls_lease l
         where l.lease_id = a.lease_id and l.lease_type_id = 3 and a.company_id = A_COMPANY_ID
		 and case when A_LEASE_ID = -1 then -1 else l.lease_id end = A_LEASE_ID)
        and ilr_id = decode(A_ILR_ID, null, ilr_id, A_ILR_ID)
        and revision = decode(A_REVISION, null, revision, A_REVISION); /* WMD */

	if COUNTER = 0 then
		return 'OK';
	end if;

	 /* Do asset accruals for interim interest */
	 /* CJS 2/17/15 change to effective rate with payment frequency; Move interim rates first */
   /* WMD had to fix places where it was looking for effective date in ls_lease_interim_rates table */
   /* WMD Changed to update current month >= */
      sqls :=  'Update LS_ASSET_SCHEDULE LAS '||
               'set LAS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
				  'nvl((select LAS.BEG_OBLIGATION * ILR.RATE/(nvl(LL.DAYS_IN_YEAR, 360)/ case when ll.days_in_month_sw = 0 then extract(day from last_day(las.month)) else 30 end)
          - decode(LAS.IS_OM, 1, LAS.INTEREST_ACCRUAL - (LAS.BEG_OBLIGATION-LAS.END_OBLIGATION), LAS.INTEREST_ACCRUAL) '||
                  'from ls_lease_interim_rates ILR, LS_ASSET LA, LS_ILR LI, LS_LEASE LL '||
                  'where LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LA.ILR_ID = LI.ILR_ID and LI.LEASE_ID = LL.LEASE_ID '||
                  'and LI.lease_id = ILR.lease_id ' ||
                  'and to_char(LI.est_in_svc_date, ''yyyymm'') <= ' || to_char(A_MONTH,'yyyymm');
			   if A_REVISION is not null then
			      sqls := sqls || 'and LAS.REVISION = ' || to_char(A_REVISION) || ' ';
			   else
			      sqls := sqls || 'and LAS.REVISION = LA.APPROVED_REVISION ';
			   end if;
               sqls := sqls || 'and ILR.month = '||
                     '(select max(ILR2.MONTH) from ls_lease_interim_rates ILR2 '||
                     'where trunc(ILR2.MONTH,''month'') <= las.month ' ||
                     'and ILR2.lease_id = ILR.lease_id)),0) '||
               'where (LAS.LS_ASSET_ID, LAS.REVISION) in (select LS_ASSET.LS_ASSET_ID, ';
						   if A_REVISION is not null then
						     sqls := sqls || to_char(A_REVISION) || ' ';
						   else
						     sqls := sqls || 'LS_ASSET.APPROVED_REVISION ';
						   end if;
						   sqls := sqls || 'from LS_ASSET, LS_ILR, LS_ILR_GROUP, LS_LEASE '||
                  'where (LS_ASSET.LS_ASSET_STATUS_ID = 3 or (LS_ASSET.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LS_ASSET.RETIREMENT_DATE,''yyyymm'')))) '||
                  'and LS_ASSET.ILR_ID = LS_ILR.ILR_ID '||
                  'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID and LS_ASSET.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
				  'and case when ' || to_char(a_lease_id) || ' = -1 then -1 else ls_ilr.lease_id end = '||to_char(a_lease_id)||' '||
                  'and LS_ILR_GROUP.USE_FLOATING_RATE = 1 '||
                  'and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID '||
                  'and LS_LEASE.LEASE_TYPE_ID = 3) '||
               'and to_number(to_char(LAS.MONTH,''yyyymm'')) >= to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
               'and to_number(to_char(LAS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ' ||
               'and las.ls_asset_id not in (select from_ls_asset_id from ls_asset_transfer_history where to_number(to_char(month, ''yyyymm'')) = to_number('''||to_char(A_MONTH,'yyyymm')||''')) ';

               /* WMD */
               if A_ILR_ID is not null then
                sqls:= sqls || 'and las.ls_asset_id in (select ls_asset_id from ls_asset where ilr_id = ' || to_char(A_ILR_ID) || ') ';
               end if;

			   if A_REVISION is not null then
			    sqls:= sqls || 'and las.revision = ' || to_char(A_REVISION) || ' ';
			   end if;

      L_STATUS := SQLS;
--      PKG_PP_LOG.P_WRITE_MESSAGE('SINKING FUND INTERIM INTEREST SQL:');
--      PKG_PP_LOG.P_WRITE_MESSAGE(SQLS);
      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;

      /* Do asset accruals */
	  /* CJS 4/22/15 Changing divide by; changed to include lease ID */
	  /* CJS 3/24/15 subtract interest accrual from sinking fund accrual */
      /* WMD Changed to LS_LEASE_RATE_GROUP TABLE */
      /* WMD Changed to update current month >= */
      sqls :=  'Update LS_ASSET_SCHEDULE LAS '||
               'set LAS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
                  'LAS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||
				  ' + nvl((select LAS.BEG_OBLIGATION * GR.RATE/(nvl(LL.DAYS_IN_YEAR, 360)/case when ll.days_in_month_sw = 0 then extract(day from last_day(las.month)) else 30 end ) '||
                  'from LS_LEASE_RATE_GROUP GR, LS_ASSET LA, LS_ILR LI, LS_ILR_PAYMENT_TERM PT, LS_LEASE LL '||
                  'where LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LA.ILR_ID = LI.ILR_ID and LI.LEASE_ID = LL.LEASE_ID '||
                  'and LI.rate_group_id = GR.rate_group_id and LI.lease_id = GR.lease_id ' ||
                  'and to_char(LI.est_in_svc_date, ''yyyymm'') <= ' || to_char(A_MONTH,'yyyymm');
                  if a_revision is not null then
                    sqls:=sqls || 'and LI.ilr_id = PT.ilr_id and ' || to_char(A_REVISION) || ' = PT.revision and PT.payment_Term_id = 1 and LAS.REVISION = PT.REVISION ';
                  else
				    sqls:=sqls || 'and LI.ilr_id = PT.ilr_id and LI.current_revision = PT.revision and PT.payment_Term_id = 1 and LAS.REVISION = PT.REVISION ';
                  end if;
                  sqls:= sqls || 'and GR.EFFECTIVE_DATE = '||
                     '(select max(GR2.EFFECTIVE_DATE) from LS_LEASE_RATE_GROUP GR2 '||
                     'where to_number(to_char(GR2.EFFECTIVE_DATE,''yyyymm'')) <= to_number(to_char(PT.payment_term_date,''yyyymm'')) '||
                     'and GR2.rate_group_id = GR.rate_group_id and GR2.lease_id = GR.lease_id)), 0) '||
               'where (LAS.LS_ASSET_ID, LAS.REVISION) in (select LS_ASSET.LS_ASSET_ID, ';
						   if A_REVISION is not null then
						     sqls := sqls || to_char(A_REVISION) || ' ';
						   else
						     sqls := sqls || 'LS_ASSET.APPROVED_REVISION ';
						   end if;
						   sqls := sqls || 'from LS_ASSET, LS_ILR, LS_ILR_GROUP, LS_LEASE '||
                  'where (LS_ASSET.LS_ASSET_STATUS_ID = 3 or (LS_ASSET.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LS_ASSET.RETIREMENT_DATE,''yyyymm'')))) '||
                  'and LS_ASSET.ILR_ID = LS_ILR.ILR_ID '||
                  'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID and LS_ASSET.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
				  'and case when ' || to_char(a_lease_id) || ' = -1 then -1 else ls_ilr.lease_id end = '||to_char(a_lease_id)||' '||
                  'and LS_ILR_GROUP.USE_FLOATING_RATE = 1 '||
                  'and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID '||
                  'and LS_LEASE.LEASE_TYPE_ID = 3) '||
               'and to_number(to_char(LAS.MONTH,''yyyymm'')) >= to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
               'and to_number(to_char(LAS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ' ||
               'and las.ls_asset_id not in (select from_ls_asset_id from ls_asset_transfer_history where to_number(to_char(month, ''yyyymm'')) = to_number('''||to_char(A_MONTH,'yyyymm')||''')) ';

               /* WMD */
               if A_ILR_ID is not null then
                sqls:= sqls || 'and las.ls_asset_id in (select ls_asset_id from ls_asset where ilr_id = ' || to_char(A_ILR_ID) || ') ';
               end if;

			   if A_REVISION is not null then
			    sqls:= sqls || 'and las.revision = ' || to_char(A_REVISION) || ' ';
			   end if;

      L_STATUS := SQLS;
--      PKG_PP_LOG.P_WRITE_MESSAGE('SINKING FUND SQL:');
--      PKG_PP_LOG.P_WRITE_MESSAGE(SQLS);
      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;

      /* Do ILR accruals */
      /* WMD Changed to update current month >= */
      SQLS :=  'Update LS_ILR_SCHEDULE LIS '||
               'set LIS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
                  '(select sum(LAS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||') '||
                  'from LS_ASSET_SCHEDULE LAS, LS_ASSET LA ' ||
                  'where LAS.REVISION = LIS.REVISION and LA.ILR_ID = LIS.ILR_ID '||
                  'and LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LAS.MONTH = LIS.MONTH ';
                  if A_REVISION is not null then
                    sqls:=sqls || 'and ' || to_char(A_REVISION) || ' = LAS.REVISION ';
                  else
                    sqls:=sqls || 'and LA.APPROVED_REVISION = LAS.REVISION ';
                  end if;
                  sqls:=sqls || '
                   and LAS.SET_OF_BOOKS_ID = LIS.SET_OF_BOOKS_ID '||
                  'and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LA.RETIREMENT_DATE,''yyyymm'')))) '||
                  'group by LA.ILR_ID, LAS.MONTH, LAS.REVISION, LAS.SET_OF_BOOKS_ID) ';
				  if A_REVISION is null then
			        sqls:= sqls || 'where (LIS.ILR_ID, LIS.REVISION) in '||
                    '(select LS_ILR.ILR_ID, LS_ILR.CURRENT_REVISION from LS_ILR, LS_ILR_GROUP, LS_LEASE ';
			      else
			        sqls:= sqls || 'where LIS.ILR_ID in '||
                    '(select LS_ILR.ILR_ID from LS_ILR, LS_ILR_GROUP, LS_LEASE ';
			      end if;
			   sqls:= sqls ||
                  'where LS_ILR.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
                  'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID '||
				  'and case when ' || to_char(a_lease_id) || ' = -1 then -1 else ls_ilr.lease_id end = '||to_char(a_lease_id)||' '||
                  'and LS_ILR_GROUP.USE_FLOATING_RATE = 1 '||
                  'and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID '||
                  'and LS_LEASE.LEASE_TYPE_ID = 3) '||
               'and to_number(to_char(LIS.MONTH,''yyyymm'')) >= to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
               'and to_number(to_char(LIS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ';

               /* WMD */
               if A_ILR_ID is not null then
                sqls:= sqls || 'and lis.ilr_id = ' || to_char(A_ILR_ID);
               end if;

      /* WMD if we passed in revision and ILR then we should run payments too */
      if A_ILR_ID is not null and A_REVISION is not null then
        L_STATUS:= F_FLOATING_RATE_SF_PAYMENT(-1, A_COMPANY_ID, A_MONTH, A_ILR_ID, A_REVISION);
      else
        L_STATUS:= F_FLOATING_RATE_SF_PAYMENT(A_LEASE_ID, A_COMPANY_ID, A_MONTH, A_ILR_ID, A_REVISION);
      end if;


        if L_STATUS <> 'OK' THEN
          return L_STATUS;
        end if;

      L_STATUS := SQLS;
--      PKG_PP_LOG.P_WRITE_MESSAGE('ILR SCHEDULE UPDATE SQL:');
--      PKG_PP_LOG.P_WRITE_MESSAGE(SQLS);
      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;

      return 'OK';
   exception
      when others then
         return sqlerrm || ' : ' || L_STATUS;
   end F_FLOATING_RATE_SF_ACCRUAL;

   function F_FLOATING_RATE_SF_PAYMENT(A_LEASE_ID in number, A_COMPANY_ID in number,
                               A_MONTH      in date,
                               A_ILR_ID in number:=null,
                               A_REVISION in number:=null) return varchar2 is
      COUNTER  number;
      BUCKET   LS_RENT_BUCKET_ADMIN%ROWTYPE;
      SQLS     varchar2(10000);
      L_STATUS varchar2(10000);
      L_MAX    varchar2(8);
   begin
      /* Get the floating rates bucket */
      L_STATUS := 'Insert floating rate amounts on schedule';

      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET = 1;

      if COUNTER > 1 then
         return 'Error: there must be exactly one bucket defined as the floating rate bucket';
		elsif COUNTER = 0 then
			return 'OK';
      end if;

      L_STATUS := 'Build SQL string';
      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET = 1;

      /* Get maximum month */
      select to_char(max(month),'yyyymm')
      into L_MAX
      from ls_ilr_schedule
      where ilr_id in
         (select a.ilr_id from ls_ilr a, ls_lease l
         where l.lease_id = a.lease_id and l.lease_type_id = 3 and a.company_id = a_company_id
		 and case when A_LEASE_ID = -1 then -1 else l.lease_id end = A_LEASE_ID)
     and ilr_id = decode(A_ILR_ID, null, ilr_id, A_ILR_ID)
     and revision = decode(A_REVISION, null, revision, A_REVISION); /* WMD */

      /* Do asset payments */
      if A_REVISION is not null then
      SQLS :=  'Update (select LAS.*, LAST.ILR_ID, ' || to_char(A_REVISION);
      else
      SQLS :=  'Update (select LAS.*, LAST.ILR_ID, LAST.APPROVED_REVISION ';
      end if;
      sqls:= sqls || '
                from LS_ASSET_SCHEDULE LAS, LS_ASSET LAST where LAS.LS_ASSET_ID = LAST.LS_ASSET_ID) LAS '||
               'set LAS.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
                  '(select nvl(sum(LAS2.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||') '||
               'over(partition by LAS2.LS_ASSET_ID, LAS2.SET_OF_BOOKS_ID, LAS2.REVISION),0) '||
                  'from LS_ASSET_SCHEDULE LAS2 '||
                  'where LAS.LS_ASSET_ID = LAS2.LS_ASSET_ID and LAS.SET_OF_BOOKS_ID = LAS2.SET_OF_BOOKS_ID '||
                  'and LAS2.MONTH = LAS.MONTH and LAS.REVISION = LAS2.REVISION) '||
                  'where (LAS.LS_ASSET_ID, LAS.REVISION) in (select LS_ASSET.LS_ASSET_ID, ';
						   if A_REVISION is not null then
						     sqls := sqls || to_char(A_REVISION) || ' ';
						   else
						     sqls := sqls || 'LS_ASSET.APPROVED_REVISION ';
						   end if;
						   sqls := sqls || 'from LS_ASSET, LS_ILR, LS_ILR_GROUP, LS_LEASE '||
                              'where (LS_ASSET.LS_ASSET_STATUS_ID = 3 or (LS_ASSET.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LS_ASSET.RETIREMENT_DATE,''yyyymm'')))) '||
                              'and LS_ASSET.ILR_ID = LS_ILR.ILR_ID '||
                              'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID and LS_ASSET.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
                              'and case when ' || to_char(a_lease_id) || ' = -1 then -1 else ls_ilr.lease_id end = '||to_char(a_lease_id)||' '||
							  'and LS_ILR_GROUP.USE_FLOATING_RATE = 1 '||
                              'and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID '||
                              'and LS_LEASE.LEASE_TYPE_ID = 3) '||
                  'and to_number(to_char(LAS.MONTH,''yyyymm'')) >= to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
                  'and to_number(to_char(LAS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ';

                  /* WMD */
                  if A_ILR_ID is not null then
                    sqls:= sqls || ' and las.ls_asset_id in (select ls_asset_id from ls_asset where ilr_id = ' || to_char(A_ILR_ID) || ')';
                  end if;

			      if A_REVISION is not null then
			        sqls:= sqls || 'and las.revision = ' || to_char(A_REVISION) || ' ';
			      end if;

      L_STATUS := SQLS;

	  L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;

      /* Do ILR payments */
      SQLS :=  'Update LS_ILR_SCHEDULE LIS '||
               'set LIS.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
                  '(select sum(LAS.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||') '||
                  'from LS_ASSET_SCHEDULE LAS, LS_ASSET LA '||
                  'where LAS.REVISION = LIS.REVISION and LA.ILR_ID = LIS.ILR_ID '||
                  'and LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LAS.MONTH = LIS.MONTH ';
                  if A_REVISION is not null then
                  sqls:=sqls || 'and ' || to_char(A_REVISION) || ' = LAS.REVISION ';
                  else
                  sqls:=sqls || 'and LA.APPROVED_REVISION = LAS.REVISION ';
                  end if;
                  sqls:=sqls || 'and LAS.SET_OF_BOOKS_ID = LIS.SET_OF_BOOKS_ID '||
                  'and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LA.RETIREMENT_DATE,''yyyymm'')))) '||
                  'group by LA.ILR_ID, LAS.MONTH, LAS.REVISION, LAS.SET_OF_BOOKS_ID) ';
				  if A_REVISION is null then
			        sqls:= sqls || 'where (LIS.ILR_ID, LIS.REVISION) in '||
                    '(select LS_ILR.ILR_ID, LS_ILR.CURRENT_REVISION from LS_ILR, LS_ILR_GROUP, LS_LEASE ';
			      else
			        sqls:= sqls || 'where LIS.ILR_ID in '||
                    '(select LS_ILR.ILR_ID from LS_ILR, LS_ILR_GROUP, LS_LEASE ';
			      end if;
			   sqls:= sqls ||
                  'where LS_ILR.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
                  'and case when ' || to_char(a_lease_id) || ' = -1 then -1 else ls_ilr.lease_id end = '||to_char(a_lease_id)||' '||
				  'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID '||
                  'and LS_ILR_GROUP.USE_FLOATING_RATE = 1 '||
                  'and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID '||
                  'and LS_LEASE.LEASE_TYPE_ID = 3) '||
               'and to_number(to_char(LIS.MONTH,''yyyymm'')) >= to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
               'and to_number(to_char(LIS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ';

               /* WMD */
                  if A_ILR_ID is not null then
                    sqls:= sqls || ' and ilr_id = ' || to_char(A_ILR_ID) || ' ';
                  end if;

				  if A_REVISION is not null then
				    sqls:= sqls || ' and revision = ' || to_char(A_REVISION) || ' ';
				  end if;

      L_STATUS := SQLS;

      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;


      return 'OK';
   exception
      when others then
         return L_STATUS;
   end F_FLOATING_RATE_SF_PAYMENT;

   function F_FLOATING_RATE_ACCRUAL(A_LEASE_ID in number, A_COMPANY_ID  in number,
                               A_MONTH        in date,
                               A_ILR_ID in number:=null,
                               A_REVISION in number:=null) return varchar2 is
      counter number;
      bucket   LS_RENT_BUCKET_ADMIN%ROWTYPE;
      sqls     varchar2(10000);
      L_STATUS varchar2(10000);
	  L_MAX    varchar2(8);

   begin
      --get the floating rates bucket
      L_STATUS := 'Insert floating rate amounts on schedule';

      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET =1;

      if COUNTER > 1 then
         return 'Error: there must be exactly one bucket defined as the floating rate bucket';
		elsif COUNTER = 0 then
			return 'OK';
      end if;

      L_STATUS := 'Build SQL string';
      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET = 1;

	  /* Get maximum month */
      select to_char(max(month),'yyyymm')
      into L_MAX
      from ls_ilr_schedule
      where ilr_id in
         (select a.ilr_id from ls_ilr a, ls_lease l
         where l.lease_id = a.lease_id and a.company_id = a_company_id
		 and case when A_LEASE_ID = -1 then -1 else l.lease_id end = A_LEASE_ID)
     and ilr_id = decode(A_ILR_ID, null, ilr_id, A_ILR_ID)
     and revision = decode(A_REVISION, null, revision, A_REVISION); /* WMD */

	  sqls :=  'Update LS_ASSET_SCHEDULE LAS '||
               'set LAS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
				'nvl((select (LAS.BEG_OBLIGATION * ILR.RATE/(nvl(LL.DAYS_IN_YEAR, 360)/case when ll.days_in_month_sw = 0 then extract(day from last_day(las.month)) else 30 end )) - LAS.INTEREST_ACCRUAL '||
                  'from LS_LEASE_INTERIM_RATES ILR, LS_ASSET LA, LS_ILR LI, LS_LEASE LL '||
                  'where LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LA.ILR_ID = LI.ILR_ID ';
				  if A_REVISION is not null then
			        sqls := sqls || 'and LAS.REVISION = ' || to_char(A_REVISION) || ' ';
				  else
			        sqls := sqls || 'and LAS.REVISION = LA.APPROVED_REVISION ';
				  end if;
				  sqls := sqls || 'and LI.lease_id = ILR.lease_id and LI.LEASE_ID = LL.LEASE_ID '||
                  'and ILR.MONTH = '||
                     '(select max(ILR2.MONTH) from ls_lease_interim_rates ILR2 '||
                     'where to_number(to_char(ILR2.MONTH,''yyyymm'')) <= to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
                     'and ILR2.lease_id = ILR.lease_id)), 0) '||
               'where (LAS.LS_ASSET_ID, LAS.REVISION) in (select LS_ASSET.LS_ASSET_ID, ';
						   if A_REVISION is not null then
						     sqls := sqls || to_char(A_REVISION) || ' ';
						   else
						     sqls := sqls || 'LS_ASSET.APPROVED_REVISION ';
						   end if;
						   sqls := sqls || 'from LS_ASSET, LS_ILR, LS_ILR_GROUP '||
                           'where (LS_ASSET.LS_ASSET_STATUS_ID = 3 or (LS_ASSET.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LS_ASSET.RETIREMENT_DATE,''yyyymm'')))) '||
                           'and LS_ASSET.ILR_ID = LS_ILR.ILR_ID '||
                           'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID and LS_ASSET.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
						   'and case when ' || to_char(a_lease_id) || ' = -1 then -1 else ls_ilr.lease_id end = '||to_char(a_lease_id)||' '||
                           'and LS_ILR_GROUP.USE_FLOATING_RATE = 1) '||
               'and to_char(LAS.MONTH,''yyyymm'') >= '''||to_char(A_MONTH,'yyyymm')||''' '||
			   'and to_number(to_char(LAS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ';

               /* WMD */
               if A_ILR_ID is not null then
                sqls:= sqls || 'and las.ls_asset_id in (select ls_asset_id from ls_asset where ilr_id = ' || to_char(A_ILR_ID) || ') ';
               end if;

			   if A_REVISION is not null then
			    sqls:= sqls || 'and las.revision = ' || to_char(A_REVISION) || ' ';
			   end if;

      L_STATUS := SQLS;
      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;






	  /* CJS 4/27/15 Change to calc IGFR as difference between amount and original interest amount; Move after interim rates */
	  /* CJS 4/23/15 Change to include lease ID, ilr, days in year; 2nd update was using floating rates instead of interim rates */
      sqls :=  'Update LS_ASSET_SCHEDULE LAS '||
               'set LAS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
			   ' LAS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||
               ' + nvl((select (LAS.BEG_OBLIGATION * ILR.RATE/(nvl(LL.DAYS_IN_YEAR, 360)/case when ll.days_in_month_sw = 0 then extract(day from last_day(las.month)) else 30 end )) - LAS.INTEREST_ACCRUAL '||
               'from LS_ILR_GROUP_FLOATING_RATES ILR, LS_ASSET LA, LS_ILR LI, LS_LEASE LL '||
               'where LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LA.ILR_ID = LI.ILR_ID and LI.LEASE_ID = LL.LEASE_ID ';
			   if A_REVISION is not null then
			      sqls := sqls || 'and LAS.REVISION = ' || to_char(A_REVISION) || ' ';
			   else
			      sqls := sqls || 'and LAS.REVISION = LA.APPROVED_REVISION ';
			   end if;
               sqls := sqls || 'and LI.ILR_GROUP_ID = ILR.ILR_GROUP_ID and LI.lease_id = ILR.lease_id '||
               'and ILR.EFFECTIVE_DATE =  (select max(ILR2.EFFECTIVE_DATE) from LS_ILR_GROUP_FLOATING_RATES ILR2 '||
                                    'where to_number(to_char(ILR2.EFFECTIVE_DATE,''yyyymm'')) <= to_number('''||to_char(A_MONTH,'yyyymm')||''') '||
                                    'and ILR2.ILR_GROUP_ID = ILR.ILR_GROUP_ID and ILR2.lease_id = ILR.lease_id)), 0) '||
               'where (LAS.LS_ASSET_ID, LAS.REVISION) in (select LS_ASSET.LS_ASSET_ID, ';
						   if A_REVISION is not null then
						     sqls := sqls || to_char(A_REVISION) || ' ';
						   else
						     sqls := sqls || 'LS_ASSET.APPROVED_REVISION ';
						   end if;
						   sqls := sqls || 'from LS_ASSET, LS_ILR, LS_ILR_GROUP '||
                           'where (LS_ASSET.LS_ASSET_STATUS_ID = 3 or (LS_ASSET.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LS_ASSET.RETIREMENT_DATE,''yyyymm'')))) '||
                           'and LS_ASSET.ILR_ID = LS_ILR.ILR_ID '||
                           'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID and LS_ASSET.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
						   'and case when ' || to_char(a_lease_id) || ' = -1 then -1 else ls_ilr.lease_id end = '||to_char(a_lease_id)||' '||
                           'and LS_ILR_GROUP.USE_FLOATING_RATE = 1) '||
               'and to_char(LAS.MONTH,''yyyymm'') >= '''||to_char(A_MONTH,'yyyymm')||''' '||
			   'and to_number(to_char(LAS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ';

               /* WMD */
               if A_ILR_ID is not null then
                sqls:= sqls || 'and las.ls_asset_id in (select ls_asset_id from ls_asset where ilr_id = ' || to_char(A_ILR_ID) || ') ';
               end if;

			   if A_REVISION is not null then
			    sqls:= sqls || 'and las.revision = ' || to_char(A_REVISION) || ' ';
			   end if;

      L_STATUS := SQLS;

      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;

      SQLS := 'Update LS_ILR_SCHEDULE LIS '||
               'set LIS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
               '(select sum(LAS.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||') '||
               'from LS_ASSET_SCHEDULE LAS, LS_ASSET LA '||
               'where LAS.REVISION = LIS.REVISION and LA.ILR_ID = LIS.ILR_ID '||
               'and LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LAS.MONTH = LIS.MONTH ';
               if A_REVISION is not null then
                  sqls:=sqls || 'and ' || to_char(A_REVISION) || ' = LAS.REVISION ';
                  else
                  sqls:=sqls || 'and LA.APPROVED_REVISION = LAS.REVISION ';
                  end if;
                  sqls:=sqls || 'and LAS.SET_OF_BOOKS_ID = LIS.SET_OF_BOOKS_ID '||
               'and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LA.RETIREMENT_DATE,''yyyymm'')))) '||
               'group by LA.ILR_ID, LAS.MONTH, LAS.REVISION, LAS.SET_OF_BOOKS_ID) ';
			   if A_REVISION is null then
			     sqls:= sqls || 'where (LIS.ILR_ID, LIS.REVISION) in (select LS_ILR.ILR_ID, LS_ILR.CURRENT_REVISION from LS_ILR, LS_ILR_GROUP ';
			   else
			     sqls:= sqls || 'where LIS.ILR_ID in (select LS_ILR.ILR_ID from LS_ILR, LS_ILR_GROUP ';
			   end if;
			   sqls:= sqls ||
                           'where LS_ILR.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
                           'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID '||
						   'and case when ' || to_char(a_lease_id) || ' = -1 then -1 else ls_ilr.lease_id end = '||to_char(a_lease_id)||' '||
                           'and LS_ILR_GROUP.USE_FLOATING_RATE = 1) '||
            'and to_char(LIS.MONTH,''yyyymm'') >= '''||to_char(A_MONTH,'yyyymm')||''' '||
			'and to_number(to_char(LIS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ';

               /* WMD */
                  if A_ILR_ID is not null then
                    sqls:= sqls || ' and ilr_id = ' || to_char(A_ILR_ID) || ' ';
                  end if;

				  if A_REVISION is not null then
				    sqls:= sqls || ' and revision = ' || to_char(A_REVISION) || ' ';
				  end if;

      L_STATUS := SQLS;
      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;

	  if A_ILR_ID is null then
         return F_FLOATING_RATE_SF_ACCRUAL(A_LEASE_ID, A_COMPANY_ID, A_MONTH);
	  end if;

	  return 'OK';
   exception
      when others then
         return L_STATUS;
   end F_FLOATING_RATE_ACCRUAL;

   function F_FLOATING_RATE_PAYMENT(A_LEASE_ID in number, A_COMPANY_ID  in number,
                               A_MONTH        in date,
                               A_ILR_ID in number:=null,
                               A_REVISION in number:=null) return varchar2 is
      counter number;
      floating_rate_count number;
      sinking_fund_count number;
      bucket   LS_RENT_BUCKET_ADMIN%ROWTYPE;
      sqls  varchar2(10000);
      L_STATUS      varchar2(10000);
	  L_MAX    varchar2(8);

   begin
  L_STATUS:='Checking to see if there are floating rate ILR''s to calc';
  select count(1)
   into floating_rate_count
   from ls_ilr ilr, ls_lease ll, ls_asset la, ls_ilr_group ilrg
   where ll.lease_type_id <> 3
    and ilr.ilr_group_id = ilrg.ilr_group_id
    and ilrg.use_floating_rate = 1
    and la.ilr_id= ilr.ilr_id
    and ilr.lease_id = ll.lease_id
    and (la.ls_asset_status_id = 3 or (ls_asset_status_id = 4 and retirement_date <=A_MONTH))
    and nvl(A_ILR_ID, ilr.ilr_id) = ilr.ilr_id
    and ilr.company_id = A_COMPANY_ID
    and decode(A_LEASE_ID, -1, ll.lease_id, A_LEASE_ID) = ll.lease_id;

  L_STATUS:='Checking to see if there are sinking fund ILR''s to calc';
  select count(1)
   into sinking_fund_count
   from ls_ilr ilr, ls_lease ll, ls_asset la, ls_ilr_group ilrg
   where ll.lease_type_id = 3
    and ilr.ilr_group_id = ilrg.ilr_group_id
    and la.ilr_id= ilr.ilr_id
    and ilr.lease_id = ll.lease_id
    and (la.ls_asset_status_id = 3 or (ls_asset_status_id = 4 and retirement_date <=A_MONTH))
    and nvl(A_ILR_ID, ilr.ilr_id) = ilr.ilr_id
    and ilr.company_id = A_COMPANY_ID
    and decode(A_LEASE_ID, -1, ll.lease_id, A_LEASE_ID) = ll.lease_id;

    if floating_rate_count > 0 then
      L_STATUS := F_FLOATING_RATE_ACCRUAL(A_LEASE_ID, A_COMPANY_ID, A_MONTH, A_ILR_ID, A_REVISION);

	  if L_STATUS <> 'OK' then
	     return L_STATUS;
	  end if;

      --get the floating rates bucket
      L_STATUS := 'Insert floating rate amounts on schedule';
      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET =1;

      if COUNTER > 1 then
         return 'Error: there must be exactly one bucket defined as the floating rate bucket';
		elsif COUNTER = 0 then
			return 'OK';
      end if;

      L_STATUS := 'Build SQL string';
      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where FLOATING_RATE_BUCKET = 1;

	  	/* Get maximum month */
      select to_char(max(month),'yyyymm')
      into L_MAX
      from ls_ilr_schedule
      where ilr_id in
         (select a.ilr_id from ls_ilr a, ls_lease l
         where l.lease_id = a.lease_id and a.company_id = a_company_id
		 and case when A_LEASE_ID = -1 then -1 else l.lease_id end = A_LEASE_ID)
     and ilr_id = decode(A_ILR_ID, null, ilr_id, A_ILR_ID)
     and revision = decode(A_REVISION, null, revision, A_REVISION); /* WMD */

      sqls :=  'Update (select LAS.*, LAST.ILR_ID, LAST.APPROVED_REVISION '||
            '  from LS_ASSET_SCHEDULE LAS, LS_ASSET LAST where LAS.LS_ASSET_ID = LAST.LS_ASSET_ID) LAS '||
            'set LAS.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
               '(select nvl(sum(LAS2.'||BUCKET.RENT_TYPE||'_ACCRUAL'||to_char(BUCKET.BUCKET_NUMBER)||') '||
            'over(partition by LAS2.LS_ASSET_ID, LAS2.SET_OF_BOOKS_ID, LAS2.REVISION),0) '||
               'from LS_ASSET_SCHEDULE LAS2 '||
               'where LAS.LS_ASSET_ID = LAS2.LS_ASSET_ID and LAS.SET_OF_BOOKS_ID = LAS2.SET_OF_BOOKS_ID '||
               'and LAS2.MONTH = LAS.MONTH and LAS.REVISION = LAS2.REVISION ';
			   if A_REVISION is not null then
			      sqls := sqls || 'and LAS2.REVISION = ' || to_char(A_REVISION) || ' ';
			   else
			      sqls := sqls || 'and LAS2.REVISION = LAS.APPROVED_REVISION ';
			   end if;
               sqls := sqls || ') '||
               'where (LAS.LS_ASSET_ID, LAS.REVISION) in (select LS_ASSET.LS_ASSET_ID, ';
						   if A_REVISION is not null then
						     sqls := sqls || to_char(A_REVISION) || ' ';
						   else
						     sqls := sqls || 'LS_ASSET.APPROVED_REVISION ';
						   end if;
						   sqls := sqls || 'from LS_ASSET, LS_ILR, LS_ILR_GROUP '||
                           'where (LS_ASSET.LS_ASSET_STATUS_ID = 3 or (LS_ASSET.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LS_ASSET.RETIREMENT_DATE,''yyyymm'')))) '||
                           'and LS_ASSET.ILR_ID = LS_ILR.ILR_ID '||
                           'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID and LS_ASSET.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
                           'and case when ' || to_char(a_lease_id) || ' = -1 then -1 else ls_ilr.lease_id end = '||to_char(a_lease_id)||' '||
						   'and LS_ILR_GROUP.USE_FLOATING_RATE = 1) '||
               'and to_char(LAS.MONTH,''yyyymm'') >= '''||to_char(A_MONTH,'yyyymm')||''' '||
			   'and to_number(to_char(LAS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') '||
               'and PKG_LEASE_CALC.F_PAYMENT_MONTH(LAS.ILR_ID, LAS.APPROVED_REVISION, LAS.MONTH) = 1 ';

			   /* WMD */
               if A_ILR_ID is not null then
                sqls:= sqls || 'and las.ls_asset_id in (select ls_asset_id from ls_asset where ilr_id = ' || to_char(A_ILR_ID) || ') ';
               end if;

			   if A_REVISION is not null then
			    sqls:= sqls || 'and las.revision = ' || to_char(A_REVISION) || ' ';
			   end if;

      L_STATUS := SQLS;
      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;

      SQLS := 'Update LS_ILR_SCHEDULE LIS '||
               'set LIS.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||' = '||
               '(select sum(LAS.'||BUCKET.RENT_TYPE||'_PAID'||to_char(BUCKET.BUCKET_NUMBER)||') '||
               'from LS_ASSET_SCHEDULE LAS, LS_ASSET LA '||
               'where LAS.REVISION = LIS.REVISION and LA.ILR_ID = LIS.ILR_ID '||
               'and LA.LS_ASSET_ID = LAS.LS_ASSET_ID and LAS.MONTH = LIS.MONTH ';
               if A_REVISION is not null then
                  sqls:=sqls || 'and ' || to_char(A_REVISION) || ' = LAS.REVISION ';
                  else
                  sqls:=sqls || 'and LA.APPROVED_REVISION = LAS.REVISION ';
                  end if;
                  sqls:=sqls || 'and LAS.SET_OF_BOOKS_ID = LIS.SET_OF_BOOKS_ID '||
               'and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND to_number('''||to_char(A_MONTH,'yyyymm')||''') <= to_number(to_char(LA.RETIREMENT_DATE,''yyyymm'')))) '||
               'group by LA.ILR_ID, LAS.MONTH, LAS.REVISION, LAS.SET_OF_BOOKS_ID) ';
            if A_REVISION is null then
			     sqls:= sqls || 'where (LIS.ILR_ID, LIS.REVISION) in (select LS_ILR.ILR_ID, LS_ILR.CURRENT_REVISION from LS_ILR, LS_ILR_GROUP ';
			   else
			     sqls:= sqls || 'where LIS.ILR_ID in (select LS_ILR.ILR_ID from LS_ILR, LS_ILR_GROUP ';
			   end if;
			   sqls:= sqls ||
                           'where LS_ILR.COMPANY_ID = '||to_char(A_COMPANY_ID)||' '||
						   'and case when ' || to_char(a_lease_id) || ' = -1 then -1 else ls_ilr.lease_id end = '||to_char(a_lease_id)||' '||
                           'and LS_ILR.ILR_GROUP_ID = LS_ILR_GROUP.ILR_GROUP_ID '||
                           'and LS_ILR_GROUP.USE_FLOATING_RATE = 1) '||
            'and to_char(LIS.MONTH,''yyyymm'') >= '''||to_char(A_MONTH,'yyyymm')||''' '||
			'and to_number(to_char(LIS.MONTH,''yyyymm'')) <= to_number('''||L_MAX||''') ';

			/* WMD */
                  if A_ILR_ID is not null then
                    sqls:= sqls || ' and ilr_id = ' || to_char(A_ILR_ID) || ' ';
                  end if;

				  if A_REVISION is not null then
				    sqls:= sqls || ' and revision = ' || to_char(A_REVISION) || ' ';
				  end if;

      L_STATUS := SQLS;
      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;

    end if; --use floating rate if statement from the top of the function

	  if A_ILR_ID is null and sinking_fund_count > 0 and floating_rate_count = 0 then
         return F_FLOATING_RATE_SF_ACCRUAL(A_LEASE_ID, A_COMPANY_ID, A_MONTH);
	  end if;

	  return 'OK';
   exception
      when others then
         return L_STATUS;
   end F_FLOATING_RATE_PAYMENT;

   --**************************************************************************
   --                            F_ACCRUALS_CALC
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will stage the monthly accrual numbers by ls_asset.
   --    It will load from ls_asset_schedule into ls_monthly_accrual_stg
   --  This function uses merge statements, so it can be run multiple times.
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_ACCRUALS_CALC(A_COMPANY_ID in number,
							A_MONTH      in date,
                            A_END_LOG    in number:=null) return varchar2 is
      L_STATUS      varchar2(2000);
      L_ASSET_SCH   ASSET_SCHEDULE_LINE_TYPE;
      SCHEDULEINDEX number;
      EXECACCRUAL   number;
      CONTACCRUAL   number;
      l_incentive_math_amount NUMBER;
      l_incentive_amount NUMBER;
   begin

   /* WMD Do logging */
   PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
   PKG_PP_LOG.P_WRITE_MESSAGE('Calc Accruals - Company ID: ' || a_company_id || ' - Month: ' || to_char(a_month, 'mon-yyyy'));
   /* WMD Had to move floating rates above Tax Calc Accruals. Sinking fund interest is included in taxes, so it needs to go first */

   L_STATUS:='Clearing prior runs for this company/ month combination';
   delete from ls_monthly_accrual_stg
   where LS_ASSET_ID IN (SELECT LS_ASSET_ID FROM LS_ASSET WHERE company_id = A_COMPANY_ID)
    and gl_posting_mo_yr = A_MONTH;

   L_STATUS := 'Handling floating rates';
   PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

   L_STATUS := F_FLOATING_RATE_ACCRUAL(-1, A_COMPANY_ID, A_MONTH);
     if L_STATUS <> 'OK' then
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      PKG_PP_LOG.P_END_LOG();
      return L_STATUS;
     end if;

    -- calculate the accrual variable payments for every asset asset.
    L_STATUS := 'Handling Variable Payments - Accruals';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

    L_STATUS := PKG_LEASE_VAR_PAYMENTS.F_CALC_ASSET_BUCKETS_MONTH_END(A_COMPANY_ID, A_MONTH, 'Accruals');
    IF L_STATUS <> 'OK' THEN
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		PKG_PP_LOG.P_END_LOG();
		return L_STATUS;
    END IF;

	L_STATUS:= 'Tax accruals';
	PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
	L_STATUS := F_TAX_CALC_ACCRUALS(A_COMPANY_ID, A_MONTH);
	 if L_STATUS <> 'OK' then
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		PKG_PP_LOG.P_END_LOG();
		return L_STATUS;
	 end if;

      L_STATUS := 'Bulk collecting';
      select S.* bulk collect
        into L_ASSET_SCH
        from LS_ASSET_SCHEDULE S, LS_ASSET A, LS_ILR I
       where I.COMPANY_ID = A_COMPANY_ID
         and I.ILR_ID = A.ILR_ID
         and S.LS_ASSET_ID = A.LS_ASSET_ID
         and A.APPROVED_REVISION = S.REVISION
         and S.MONTH = A_MONTH
         and (A.LS_ASSET_STATUS_ID = 3 or (A.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= A.RETIREMENT_DATE));

      L_STATUS := 'Deleting retired assets from staging table';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      delete from LS_MONTHLY_ACCRUAL_STG STG
       where STG.GL_POSTING_MO_YR = A_MONTH
         and STG.LS_ASSET_ID in (select LS_ASSET_ID from LS_ASSET
                                 where LS_ASSET_STATUS_ID = 4 and (A_MONTH > RETIREMENT_DATE
                                                                    or retirement_date is null)); /* WMD need to delete transfers */

      L_STATUS := 'Inserting interest/rent accrual';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      forall SCHEDULEINDEX in indices of L_ASSET_SCH
       merge into LS_MONTHLY_ACCRUAL_STG stg
       using (select 0 as accrual_id,
               2 as accrual_type_id,
               L_ASSET_SCH(SCHEDULEINDEX).LS_ASSET_ID as ls_asset_id,
               L_ASSET_SCH(SCHEDULEINDEX).INTEREST_ACCRUAL as amount,
                    A_MONTH as gl_posting_mo_yr,
                    L_ASSET_SCH(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
               from DUAL
            where L_ASSET_SCH(SCHEDULEINDEX).INTEREST_ACCRUAL <> 0) b
       on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
            and b.LS_ASSET_ID = stg.LS_ASSET_ID
            and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
            and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
       when matched then update set stg.AMOUNT = b.AMOUNT
       when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, LS_ASSET_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
         values (LS_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.LS_ASSET_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);


      L_STATUS := 'Inserting executory accrual';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      PKG_LEASE_BUCKET.P_CALC_EXEC_ACCRUALS(L_ASSET_SCH);

      L_STATUS := 'Inserting contingent accrual';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      PKG_LEASE_BUCKET.P_CALC_CONT_ACCRUALS(L_ASSET_SCH);

	  if lower(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Generate Deferred Rent JEs', A_COMPANY_ID)) = 'yes' then

		  L_STATUS := 'Inserting short term deferred rent amounts';
		  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		  forall SCHEDULEINDEX in indices of L_ASSET_SCH
		   merge into LS_MONTHLY_ACCRUAL_STG stg
		   using (select 0 as accrual_id,
				   23 as accrual_type_id,
				   L_ASSET_SCH(SCHEDULEINDEX).LS_ASSET_ID as ls_asset_id,
				   L_ASSET_SCH(SCHEDULEINDEX).END_ST_DEFERRED_RENT - L_ASSET_SCH(SCHEDULEINDEX).BEG_ST_DEFERRED_RENT as amount,
						A_MONTH as gl_posting_mo_yr,
						L_ASSET_SCH(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
				   from DUAL
				where L_ASSET_SCH(SCHEDULEINDEX).END_ST_DEFERRED_RENT - L_ASSET_SCH(SCHEDULEINDEX).BEG_ST_DEFERRED_RENT <> 0) b
		   on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
				and b.LS_ASSET_ID = stg.LS_ASSET_ID
				and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
		   when matched then update set stg.AMOUNT = b.AMOUNT
		   when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, LS_ASSET_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			 values (LS_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.LS_ASSET_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);

		  L_STATUS := 'Inserting long term deferred rent amounts';
		  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		  forall SCHEDULEINDEX in indices of L_ASSET_SCH
		   merge into LS_MONTHLY_ACCRUAL_STG stg
		   using (select 0 as accrual_id,
				   24 as accrual_type_id,
				   L_ASSET_SCH(SCHEDULEINDEX).LS_ASSET_ID as ls_asset_id,
				   L_ASSET_SCH(SCHEDULEINDEX).DEFERRED_RENT - (L_ASSET_SCH(SCHEDULEINDEX).END_ST_DEFERRED_RENT - L_ASSET_SCH(SCHEDULEINDEX).BEG_ST_DEFERRED_RENT) as amount,
						A_MONTH as gl_posting_mo_yr,
						L_ASSET_SCH(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
				   from DUAL
				where L_ASSET_SCH(SCHEDULEINDEX).DEFERRED_RENT - (L_ASSET_SCH(SCHEDULEINDEX).END_ST_DEFERRED_RENT - L_ASSET_SCH(SCHEDULEINDEX).BEG_ST_DEFERRED_RENT) <> 0) b
		   on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
				and b.LS_ASSET_ID = stg.LS_ASSET_ID
				and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
		   when matched then update set stg.AMOUNT = b.AMOUNT
		   when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, LS_ASSET_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			 values (LS_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.LS_ASSET_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);

		  L_STATUS := 'Inserting prepaid rent amounts';
		  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		  forall SCHEDULEINDEX in indices of L_ASSET_SCH
		   merge into LS_MONTHLY_ACCRUAL_STG stg
		   using (select 0 as accrual_id,
				   25 as accrual_type_id,
				   L_ASSET_SCH(SCHEDULEINDEX).LS_ASSET_ID as ls_asset_id,
				   L_ASSET_SCH(SCHEDULEINDEX).END_PREPAID_RENT - L_ASSET_SCH(SCHEDULEINDEX).BEG_PREPAID_RENT as amount,
						A_MONTH as gl_posting_mo_yr,
						L_ASSET_SCH(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
				   from DUAL
				where L_ASSET_SCH(SCHEDULEINDEX).END_PREPAID_RENT - L_ASSET_SCH(SCHEDULEINDEX).BEG_PREPAID_RENT <> 0) b
		   on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
				and b.LS_ASSET_ID = stg.LS_ASSET_ID
				and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
				and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
		   when matched then update set stg.AMOUNT = b.AMOUNT
		   when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, LS_ASSET_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
			 values (LS_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.LS_ASSET_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);
			END IF;

		  L_STATUS := 'Inserting Incentive amounts';
		  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		FOR SCHEDULEINDEX in 1 .. L_ASSET_SCH.Count LOOP
          --Retrieve Incentive amounts, need to do this since you have to do some special retrieval on incentive_math
		      --to ignore it when the month being accrued is the in-service date (since math at in-service date doesn't include the spread)
		      -- Amounts are same accross all SOB's so don't need to leverage that

		begin
         SELECT Decode(est_in_svc_date, a_month, 0, incentive_math_amount), incentive_amount
         INTO l_incentive_math_amount, l_incentive_amount
         FROM ls_asset_schedule s, ls_ilr i, ls_ilr_asset_map m, ls_asset a
         WHERE m.ls_asset_id = L_ASSET_SCH(SCHEDULEINDEX).LS_ASSET_ID
         AND s.ls_asset_id = m.ls_asset_id
         AND m.ilr_id = i.ilr_id
         AND m.ls_asset_id = a.ls_asset_id
         AND m.revision = s.revision
         AND s.set_of_books_id = L_ASSET_SCH(SCHEDULEINDEX).SET_OF_BOOKS_ID
         AND s.MONTH = A_MONTH
         and A.APPROVED_REVISION = S.REVISION;

		exception
            when NO_DATA_FOUND then
               continue;
         end;


			   merge into LS_MONTHLY_ACCRUAL_STG stg
			   using (select 0 as accrual_id,
					   27 as accrual_type_id,
					   L_ASSET_SCH(SCHEDULEINDEX).LS_ASSET_ID as ls_asset_id,
					    L_INCENTIVE_AMOUNT - Nvl(L_INCENTIVE_MATH_AMOUNT,0)  as amount,
							A_MONTH as gl_posting_mo_yr,
							L_ASSET_SCH(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
					   from DUAL
					where  L_INCENTIVE_AMOUNT - Nvl(L_INCENTIVE_MATH_AMOUNT,0) <> 0
					and L_ASSET_SCH(SCHEDULEINDEX).IS_OM = 1) b
			   on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
					and b.LS_ASSET_ID = stg.LS_ASSET_ID
					and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
					and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
			   when matched then update set stg.AMOUNT = b.AMOUNT
			   when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, LS_ASSET_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
				 values (LS_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.LS_ASSET_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);

       END LOOP;

	  if A_END_LOG = 1 then
        PKG_PP_LOG.P_END_LOG();
      end if;
      return 'OK';
   exception
      when others then
		 PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS || sqlerrm);
		 PKG_PP_LOG.P_END_LOG();
         return L_STATUS||sqlerrm;
   end F_ACCRUALS_CALC;

   --**************************************************************************
   --                            F_ACCRUALS_APPROVE
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will approve and post the monthly accrual numbers by ls_asset.
   --    It will load from ls_asset_schedule into
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_ACCRUALS_APPROVE(A_COMPANY_ID in number,
                               A_MONTH      in date,
                               A_END_LOG    in number:=null) return varchar2 is
      L_STATUS varchar2(2000);
	  L_LOCATION varchar2(2000);
      L_RTN    number;
      L_GL_JE_CODE   varchar2(35);
	  BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
	  COUNTER number;
	  TAX_ACCRUAL_TYPE number;
	  L_ACCTS PKG_LEASE_BUCKET.BUCKET_ACCTS;
   begin
    PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
    PKG_PP_LOG.P_WRITE_MESSAGE('Starting Logs for Accruals Approval Process. Company ID: ' || TO_CHAR(A_COMPANY_ID) || ' - ' || 'Month: ' || to_char(A_MONTH,'MON-YYYY'));
    if L_STATUS <> 'OK' then
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      PKG_PP_LOG.P_END_LOG();
      RETURN L_STATUS;
    end if;
	  L_STATUS := 'Getting GL JE Code';
      select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LAMACC'))
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
      and G.PROCESS_ID = 'LAMACC';

      FOR l_accruals IN (SELECT LS_ASSET_ID,
                                AMOUNT,
                                WORK_ORDER_ID,
                                COMPANY_ID,
                                IN_SERVICE_DATE,
                                TRANS_TYPE,
                                SET_OF_BOOKS_ID,
                                INT_ACCRUAL_ACCOUNT,
                                INT_EXPENSE_ACCOUNT,
                                ST_DEFERRED_ACCOUNT,
                                LT_DEFERRED_ACCOUNT,
                                PREPAID_RENT_ACCOUNT,
								INITIAL_DIRECT_COST_ACCOUNT,
								INCENTIVE_ACCOUNT,
                                Nvl(ST_DEFERRED_AMOUNT, 0) AS ST_DEFERRED_AMOUNT,
                                Nvl(LT_DEFERRED_AMOUNT, 0) AS LT_DEFERRED_AMOUNT,
                                Nvl(PREPAID_RENT_AMOUNT, 0) AS PREPAID_RENT_AMOUNT,
                                Nvl(INCENTIVE_AMOUNT, 0) AS INCENTIVE_AMOUNT,
                                CONTRACT_CURRENCY_ID,
                                COMPANY_CURRENCY_ID,
                                RATE
                          FROM (
                          SELECT H.ls_asset_id AS ls_asset_id,
                                round(H.AMOUNT / h.rate, 2) as AMOUNT, --book_mc_je will reconvert amount
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                3010 TRANS_TYPE,
                                H.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                                I.INT_ACCRUAL_ACCOUNT_ID as INT_ACCRUAL_ACCOUNT,
                                I.INT_EXPENSE_ACCOUNT_ID as INT_EXPENSE_ACCOUNT,
                                I.ST_DEFERRED_ACCOUNT_ID as ST_DEFERRED_ACCOUNT,
                                I.LT_DEFERRED_ACCOUNT_ID as LT_DEFERRED_ACCOUNT,
                                I.PREPAID_RENT_ACCOUNT_ID AS PREPAID_RENT_ACCOUNT,
								I.INCENTIVE_ACCOUNT_ID AS INCENTIVE_ACCOUNT,
								I.INIT_DIRECT_COST_ACCOUNT_ID AS INITIAL_DIRECT_COST_ACCOUNT,
                                H.contract_currency_id,
                                H.company_currency_id,
                                h.rate,
                                h.accrual_type_id
                            from v_ls_monthly_accrual_stg_fx H, LS_ASSET A, LS_ILR_ACCOUNT I
                          where H.GL_POSTING_MO_YR = A_MONTH
                            and ((H.ACCRUAL_TYPE_ID = 2 AND Nvl(H.AMOUNT,0) <> 0) OR H.ACCRUAL_TYPE_ID IN (23, 24, 25, 27))
                            and H.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.ILR_ID = I.ILR_ID
                            and A.COMPANY_ID = A_COMPANY_ID
                            and h.ls_cur_type = 2
                          )
                          PIVOT (
                            Max(AMOUNT) --Pivot requires an aggregate function, but there should only ever be one row so max(amount) is just the amount
                            FOR ACCRUAL_TYPE_ID IN (2 AS AMOUNT,23 AS ST_DEFERRED_AMOUNT,24 AS LT_DEFERRED_AMOUNT,25 AS PREPAID_RENT_AMOUNT, 27 AS INCENTIVE_AMOUNT)
                          ))
      loop
         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     to_char(l_accruals.trans_type);
         l_rtn    := pkg_lease_common.f_mc_bookje(l_accruals.ls_asset_id,
                                                  l_accruals.trans_type,
                                                  l_accruals.amount,
                                                  0,
                                                  -1,
                                                  L_ACCRUALS.WORK_ORDER_ID,
                                                  L_ACCRUALS.INT_EXPENSE_ACCOUNT,
                                                  0,
                                                  -1,
                                                  L_ACCRUALS.COMPANY_ID,
                                                  A_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                  l_accruals.set_of_books_id,
                                                  l_accruals.rate,
                                                  l_accruals.contract_currency_id,
                                                  l_accruals.company_currency_id,
                                                  L_STATUS);
         if L_RTN = -1 then
			PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_STATUS;
         end if;

		 -- process accruals like normal if not generating deferred rent entries
		 if lower(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Generate Deferred Rent JEs', A_COMPANY_ID)) = 'no' then
			 -- process the credit (1 more than dr trans type
			 L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
						 TO_CHAR(L_ACCRUALS.TRANS_TYPE + 1);
             L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
												   L_ACCRUALS.TRANS_TYPE + 1,
												   L_ACCRUALS.AMOUNT,
												   0,
												   -1,
												   L_ACCRUALS.WORK_ORDER_ID,
												   L_ACCRUALS.INT_ACCRUAL_ACCOUNT,
												   0,
												   -1,
												   L_ACCRUALS.COMPANY_ID,
												   A_MONTH,
												   0,
												   L_GL_JE_CODE,
												   L_ACCRUALS.SET_OF_BOOKS_ID,
                                                   L_ACCRUALS.rate,
                                                   L_ACCRUALS.contract_currency_id,
                                                   L_ACCRUALS.company_currency_id,
                                                   L_STATUS);
			 if L_RTN = -1 then
				PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
				PKG_PP_LOG.P_END_LOG();
				return L_STATUS;
			 end if;
		 else
			 if L_ACCRUALS.AMOUNT - L_ACCRUALS.ST_DEFERRED_AMOUNT - L_ACCRUALS.LT_DEFERRED_AMOUNT + L_ACCRUALS.PREPAID_RENT_AMOUNT <> 0 then
			    L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
						    TO_CHAR(L_ACCRUALS.TRANS_TYPE + 1);
                L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
												      L_ACCRUALS.TRANS_TYPE + 1,
												      L_ACCRUALS.AMOUNT - L_ACCRUALS.ST_DEFERRED_AMOUNT - L_ACCRUALS.LT_DEFERRED_AMOUNT + L_ACCRUALS.PREPAID_RENT_AMOUNT,
												      0,
												      -1,
												      L_ACCRUALS.WORK_ORDER_ID,
												      L_ACCRUALS.INT_ACCRUAL_ACCOUNT,
												      0,
												      -1,
												      L_ACCRUALS.COMPANY_ID,
												      A_MONTH,
												      0,
												      L_GL_JE_CODE,
												      L_ACCRUALS.SET_OF_BOOKS_ID,
                                                      L_ACCRUALS.rate,
                                                      L_ACCRUALS.contract_currency_id,
                                                      L_ACCRUALS.company_currency_id,
                                                      L_STATUS);
			    if L_RTN = -1 then
				    PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
				    PKG_PP_LOG.P_END_LOG();
				    return L_STATUS;
			    end if;
			 end if;

			 if L_ACCRUALS.ST_DEFERRED_AMOUNT <> 0 then
			    L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
						    TO_CHAR(3053);
                L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                              3053,
                              L_ACCRUALS.ST_DEFERRED_AMOUNT,
                              0,
                              -1,
                              L_ACCRUALS.WORK_ORDER_ID,
                              L_ACCRUALS.ST_DEFERRED_ACCOUNT,
                              0,
                              -1,
                              L_ACCRUALS.COMPANY_ID,
                              A_MONTH,
                              0,
                              L_GL_JE_CODE,
                              L_ACCRUALS.SET_OF_BOOKS_ID,
                              L_ACCRUALS.rate,
                              L_ACCRUALS.contract_currency_id,
                              L_ACCRUALS.company_currency_id,
                              L_STATUS);
			    if L_RTN = -1 then
				    PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
				    PKG_PP_LOG.P_END_LOG();
				    return L_STATUS;
			    end if;
			 end if;

			 if L_ACCRUALS.LT_DEFERRED_AMOUNT <> 0 then
			    L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
						    TO_CHAR(3054);
                L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                              3054,
                              L_ACCRUALS.LT_DEFERRED_AMOUNT,
                              0,
                              -1,
                              L_ACCRUALS.WORK_ORDER_ID,
                              L_ACCRUALS.LT_DEFERRED_ACCOUNT,
                              0,
                              -1,
                              L_ACCRUALS.COMPANY_ID,
                              A_MONTH,
                              0,
                              L_GL_JE_CODE,
                              L_ACCRUALS.SET_OF_BOOKS_ID,
                              L_ACCRUALS.rate,
                              L_ACCRUALS.contract_currency_id,
                              L_ACCRUALS.company_currency_id,
                              L_STATUS);
			    if L_RTN = -1 then
				    PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
				    PKG_PP_LOG.P_END_LOG();
				    return L_STATUS;
			    end if;
			 end if;




       if L_ACCRUALS.PREPAID_RENT_AMOUNT <> 0 then
			    L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
						    TO_CHAR(3060);
                L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                              3060,
                              L_ACCRUALS.PREPAID_RENT_AMOUNT,
                              0,
                              -1,
                              L_ACCRUALS.WORK_ORDER_ID,
                              L_ACCRUALS.PREPAID_RENT_ACCOUNT,
                              0,
                              -1,
                              L_ACCRUALS.COMPANY_ID,
                              A_MONTH,
                              1,
                              L_GL_JE_CODE,
                              L_ACCRUALS.SET_OF_BOOKS_ID,
                              L_ACCRUALS.rate,
                              L_ACCRUALS.contract_currency_id,
                              L_ACCRUALS.company_currency_id,
                              L_STATUS);
			    if L_RTN = -1 then
				    PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
				    PKG_PP_LOG.P_END_LOG();
				    return L_STATUS;
			    end if;
			 end if;



		 end if;

     --Book incentive if we have any
      IF L_ACCRUALS.INCENTIVE_AMOUNT <> 0 THEN
       L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' || TO_CHAR(3064);
	   --credit incentive
        L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                      3064,
                      L_ACCRUALS.INCENTIVE_AMOUNT,
                      0,
                      -1,
                      L_ACCRUALS.WORK_ORDER_ID,
                      L_ACCRUALS.INCENTIVE_ACCOUNT,
                      0,
                      -1,
                      L_ACCRUALS.COMPANY_ID,
                      A_MONTH,
                      0,
                      L_GL_JE_CODE,
                      L_ACCRUALS.SET_OF_BOOKS_ID,
                      L_ACCRUALS.rate,
                      L_ACCRUALS.contract_currency_id,
                      L_ACCRUALS.company_currency_id,
                      L_STATUS);
			  if L_RTN = -1 then
				  PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
				  PKG_PP_LOG.P_END_LOG();
				  return L_STATUS;
			  end if;

		--Debit Int Expense
       L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' || TO_CHAR(3010);
        L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                      3010,
                      L_ACCRUALS.INCENTIVE_AMOUNT,
                      0,
                      -1,
                      L_ACCRUALS.WORK_ORDER_ID,
                      L_ACCRUALS.INT_EXPENSE_ACCOUNT,
                      0,
                      -1,
                      L_ACCRUALS.COMPANY_ID,
                      A_MONTH,
                      1,
                      L_GL_JE_CODE,
                      L_ACCRUALS.SET_OF_BOOKS_ID,
                      L_ACCRUALS.rate,
                      L_ACCRUALS.contract_currency_id,
                      L_ACCRUALS.company_currency_id,
                      L_STATUS);
			  if L_RTN = -1 then
				  PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
				  PKG_PP_LOG.P_END_LOG();
				  return L_STATUS;
			  end if;
      END IF; --Incentives
     end loop;

	  --get the tax bucket
	  L_STATUS := 'Insert tax amounts on schedule for accruals';
      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET =1;

      if COUNTER <> 1 then
         return 'Error: there must be exactly one bucket defined as the tax expense bucket';
      end if;

      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET = 1;

	  TAX_ACCRUAL_TYPE := PKG_LEASE_BUCKET.F_GET_TAX_TYPE(BUCKET.RENT_TYPE,BUCKET.BUCKET_NUMBER);

	  PKG_PP_LOG.P_WRITE_MESSAGE('Processing Accruals Journals for Taxes');
	  FOR l_accruals IN (SELECT H.ls_asset_id AS ls_asset_id,
                                sum(LMT.AMOUNT) as AMOUNT, --Note that this is the amount off the monthly tax table, not the accrual table
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                3040 TRANS_TYPE,
                                H.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                                LTL.ACCRUAL_ACCT_ID as ACCRUAL_ACCOUNT,
								LTL.EXPENSE_ACCT_ID as EXPENSE_ACCOUNT,
								LTL.AP_ACCT_ID as AP_ACCOUNT,
                                LTL.TAX_LOCAL_ID as TAX_LOCAL_ID,
                                H.rate,
                                H.company_currency_id,
                                h.contract_currency_id
                          from v_LS_MONTHLY_ACCRUAL_STG_fx H, LS_ASSET A, LS_TAX_LOCAL LTL, LS_MONTHLY_TAX LMT
                          where H.GL_POSTING_MO_YR = A_MONTH
                            and H.AMOUNT <> 0
                            and H.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.COMPANY_ID = A_COMPANY_ID
							and H.ACCRUAL_TYPE_ID = TAX_ACCRUAL_TYPE
							and LTL.TAX_LOCAL_ID = LMT.TAX_LOCAL_ID
							and LMT.ACCRUAL = 1
							and LMT.LS_ASSET_ID = H.LS_ASSET_ID
							and LMT.GL_POSTING_MO_YR = H.GL_POSTING_MO_YR
							and LMT.SET_OF_BOOKS_ID = H.SET_OF_BOOKS_ID
                            and h.ls_cur_type = 2
						   group by H.LS_ASSET_ID, A.WORK_ORDER_ID, A.COMPANY_ID,
                             A.in_service_date, H.set_of_books_id, ltl.accrual_acct_id,
                             ltl.expense_acct_id, ltl.ap_acct_id, ltl.tax_local_id, H.rate,
                             h.company_currency_id, h.contract_currency_id)
      loop
         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     to_char(l_accruals.trans_type);
         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                                               L_ACCRUALS.TRANS_TYPE,
                                               L_ACCRUALS.AMOUNT,
                                               L_ACCRUALS.TAX_LOCAL_ID,
                                               -1,
                                               L_ACCRUALS.WORK_ORDER_ID,
											   L_ACCRUALS.EXPENSE_ACCOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.COMPANY_ID,
                                               A_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               l_accruals.set_of_books_id,
                                               l_accruals.rate,
                                               l_accruals.contract_currency_id,
                                               l_accruals.company_currency_id,
                                               L_STATUS);
         if L_RTN = -1 then
			PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_STATUS;
         end if;

         -- process the credit (1 more than dr trans type
         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     to_char(l_accruals.trans_type + 1);
         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                                                  l_accruals.trans_type + 1,
                                                  l_accruals.amount,
                                                  L_ACCRUALS.TAX_LOCAL_ID,
                                                  -1,
                                                  L_ACCRUALS.WORK_ORDER_ID,
                                                  L_ACCRUALS.ACCRUAL_ACCOUNT,
                                                  0,
                                                  -1,
                                                  L_ACCRUALS.COMPANY_ID,
                                                  A_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  l_accruals.set_of_books_id,
                                                  l_accruals.rate,
                                                  l_accruals.contract_currency_id,
                                                  l_accruals.company_currency_id,
                                                  L_STATUS);
         if L_RTN = -1 then
			PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_STATUS;
         end if;
      end loop;

	  L_ACCTS := PKG_LEASE_BUCKET.F_GET_ACCTS;

	PKG_PP_LOG.P_WRITE_MESSAGE('Processing Accruals Journals for Executory and Contingent');
	  FOR l_accruals IN (SELECT H.ls_asset_id AS ls_asset_id,
                                round(H.AMOUNT / h.rate, 2) as AMOUNT, -- mc_bookje will reconvert based on rate
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                case
                                   when H.ACCRUAL_TYPE_ID between 3 and 12 then
                                    3012 --executory
                                   when H.ACCRUAL_TYPE_ID between 13 and 22 then
                                    3014 --contingent
                                end as TRANS_TYPE,
                                H.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
								H.accrual_type_id AS accrual_type_id,
                                H.rate,
                                H.contract_currency_id,
                                h.company_currency_id
						from V_LS_MONTHLY_ACCRUAL_STG_fx H, LS_ASSET A
                          where H.GL_POSTING_MO_YR = A_MONTH
                            AND H.amount <> 0
                            and h.ls_cur_type = 2
                            and H.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.COMPANY_ID = A_COMPANY_ID
							and H.ACCRUAL_TYPE_ID between 3 and 22)


      loop

		 --we already did the taxes so skip em here
		 if L_ACCRUALS.ACCRUAL_TYPE_ID = TAX_ACCRUAL_TYPE then
			continue;
		end if;

         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     to_char(l_accruals.trans_type);
         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                                               L_ACCRUALS.TRANS_TYPE,
                                               L_ACCRUALS.AMOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.WORK_ORDER_ID,
											   L_ACCTS(L_ACCRUALS.ACCRUAL_TYPE_ID).EXPENSE_ACCT_ID,
                                               0,
                                               -1,
                                               L_ACCRUALS.COMPANY_ID,
                                               A_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               l_accruals.set_of_books_id,
                                               l_accruals.rate,
                                               l_accruals.contract_currency_id,
                                               l_accruals.company_currency_id,
                                               L_STATUS);
         if L_RTN = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_STATUS;
         end if;

         -- process the credit (1 more than dr trans type
         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_ACCRUALS.LS_ASSET_ID) || ' trans type: ' ||
                     to_char(l_accruals.trans_type + 1);
         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_ACCRUALS.LS_ASSET_ID,
                                               L_ACCRUALS.TRANS_TYPE + 1,
                                               L_ACCRUALS.AMOUNT,
                                               0,
                                               -1,
                                               L_ACCRUALS.WORK_ORDER_ID,
											   L_ACCTS(L_ACCRUALS.ACCRUAL_TYPE_ID).ACCRUAL_ACCT_ID,
                                               0,
                                               -1,
                                               L_ACCRUALS.COMPANY_ID,
                                               A_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               l_accruals.set_of_books_id,
                                               l_accruals.rate,
                                               l_accruals.contract_currency_id,
                                               l_accruals.company_currency_id,
                                               L_STATUS);
         if L_RTN = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_STATUS;
         end if;
      end loop;

      PKG_PP_LOG.P_WRITE_MESSAGE('Accruals Processing Complete for Company: ' || TO_CHAR(A_COMPANY_ID));
      if A_END_LOG = 1 then
        PKG_PP_LOG.P_END_LOG();
      end if;
      return 'OK';
   exception
      when others then
         return L_STATUS||sqlerrm;
   end F_ACCRUALS_APPROVE;

   --**************************************************************************
   --                            F_PAYMENT_CALC
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will stage the monthly accrual numbers by ls_asset.
   --    It will load from ls_asset_schedule into
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_PAYMENT_CALC(A_LEASE_ID in number, A_COMPANY_ID in number,
						   A_MONTH      in date,
                           A_END_LOG    in number:=null) return varchar2 is


      L_STATUS                varchar2(2000);
      L_RTN                   number;
      L_ASSET_SCHEDULE_LINE   ASSET_SCHEDULE_LINE_TABLE;
      L_ASSET_SCHEDULE_HEADER ASSET_SCHEDULE_HEADER_TABLE;
      SCHEDULEINDEX           number;
	  l_missing_workflow	VARCHAR2(2000);

   begin
    PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());

    PKG_PP_LOG.P_WRITE_MESSAGE('Calc Payments - Company ID: ' || a_company_id || ' - Month: ' || to_char(a_month, 'mon-yyyy') || ' - Lease ID: ' || to_char(a_lease_id));
     /* WMD Had to move floating rates ahead of taxes. Taxes apply to floating rates */
     L_STATUS := 'Handling floating rates';
     PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
     L_STATUS := F_FLOATING_RATE_PAYMENT(A_LEASE_ID, A_COMPANY_ID, A_MONTH);
     if L_STATUS <> 'OK' then
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      PKG_PP_LOG.P_END_LOG();
      return L_STATUS;
     end if;

    -- calculate the Paid variable payments for every asset
    L_STATUS := 'Handling Variable Payments - Payments';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

    L_STATUS := PKG_LEASE_VAR_PAYMENTS.F_CALC_ASSET_BUCKETS_MONTH_END(A_COMPANY_ID, A_MONTH, 'Payments');
    IF L_STATUS <> 'OK' THEN
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		PKG_PP_LOG.P_END_LOG();
		return L_STATUS;
    END IF;

     L_STATUS:='Tax Calc Payments';
     PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
     L_STATUS := F_TAX_CALC_payments(A_LEASE_ID, A_COMPANY_ID, A_MONTH);
	    if L_STATUS <> 'OK' then
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
        PKG_PP_LOG.P_END_LOG();
		    return L_STATUS;
	    end if;

      L_STATUS:='Calculating Interim Interest';
      select count(1)
      into L_RTN
      from ls_ilr
      where case when A_LEASE_ID = -1 then lease_id else A_LEASE_ID end = lease_id
        and company_id = A_COMPANY_ID
        and trunc(est_in_svc_date,'month') >= A_MONTH;

      if L_RTN>0 THEN
        PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS || ' for ' || L_RTN || ' ILR''s');
        for i in (select ilr_id, current_revision
                  from ls_ilr
                  where case when A_LEASE_ID = -1 then lease_id else A_LEASE_ID end = lease_id
                    and company_id = A_COMPANY_ID
                  and trunc(est_in_svc_date,'month') >= A_MONTH)
        loop
        PKG_LEASE_ILR.P_CALC_II(i.ilr_id, i.current_revision);
        end loop;

        PKG_PP_LOG.P_WRITE_MESSAGE('Interim Interest Calc Complete');

      end if;

      L_STATUS := 'Deleting';
      delete from LS_PAYMENT_APPROVAL
       where PAYMENT_ID in
	   (
			select PAYMENT_ID
			from LS_PAYMENT_HDR
			where GL_POSTING_MO_YR = A_MONTH
			and COMPANY_ID = A_COMPANY_ID
			and case when a_lease_id = -1 then -1 else LEASE_ID end = A_LEASE_ID
         and (PAYMENT_STATUS_ID = 1 or PAYMENT_STATUS_ID = 4) /* WMD Changed because we don't want to delete payments pending approval */
         and payment_id not in (select payment_id from ls_invoice_payment_map where in_tolerance = 1)
		);


      --Cascades to LS_INVOICE_PAYMENT_MAP and LS_PAYMENT_LINE
		delete from LS_PAYMENT_HDR
		where GL_POSTING_MO_YR = A_MONTH
		and COMPANY_ID = A_COMPANY_ID
		and case when a_lease_id = -1 then -1 else LEASE_ID end = A_LEASE_ID
      and (PAYMENT_STATUS_ID = 1 or PAYMENT_STATUS_ID = 4) /* WMD Changed because we don't want to delete payments pending approval */
      and payment_id not in (select payment_id from ls_invoice_payment_map where in_tolerance = 1);

      --delete retired assets from ls_payment_line
      delete from LS_PAYMENT_LINE
      where GL_POSTING_MO_YR = A_MONTH
      and LS_ASSET_ID in (select LS_ASSET_ID from LS_ASSET
                          where LS_ASSET_STATUS_ID = 4 and A_MONTH > RETIREMENT_DATE);

	PKG_PP_LOG.P_WRITE_MESSAGE('Adding Payment Headers');
		--insert empty headers. One header for each {lease,vendor} combo for this company
      select distinct L.LEASE_ID,
				case when LO.ls_reconcile_type_id in (1, 2) then i.ilr_id else -1 end,
				case when LO.ls_reconcile_type_id = 1 then a.ls_asset_id else -1 end,
				LV.VENDOR_ID
		bulk collect
        into L_ASSET_SCHEDULE_HEADER
        from LS_ASSET_SCHEDULE S, LS_ASSET A, LS_ILR I, LS_LEASE L, LS_LEASE_VENDOR LV,
            LS_ILR_OPTIONS O, LS_LEASE_OPTIONS LO
       where I.COMPANY_ID = A_COMPANY_ID
         and I.ILR_ID = A.ILR_ID
         and I.LEASE_ID = L.LEASE_ID
         and S.LS_ASSET_ID = A.LS_ASSET_ID
         and A.APPROVED_REVISION = S.REVISION
         and LV.LEASE_ID = L.LEASE_ID
         and LV.COMPANY_ID = I.COMPANY_ID
		 and I.ILR_ID = O.ILR_ID
		 and S.REVISION = O.REVISION
		 and L.lease_id = LO.lease_id
		 and L.current_revision = LO.revision
       and (A.LS_ASSET_STATUS_ID = 3 or (A.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= A.RETIREMENT_DATE))
       and not exists
       (
         select 1
         from ls_payment_hdr ph
         where ph.gl_posting_mo_yr = a_month
         and l.lease_id = ph.lease_id
         and i.company_id = ph.company_id
         and lv.vendor_id = ph.vendor_id
         and case when LO.ls_reconcile_type_id in (1, 2) then i.ilr_id else -1 end = ph.ilr_id
			and case when LO.ls_reconcile_type_id = 1 then a.ls_asset_id else -1 end = ph.ls_asset_id
       )
		 and case when a_lease_id = -1 then -1 else l.lease_id end = A_LEASE_ID
         and S.MONTH = add_months(A_MONTH, nvl(O.PAYMENT_SHIFT,0));

      select S.LS_ASSET_ID,
             S.INTEREST_PAID,
             S.PRINCIPAL_PAID,
             L.LEASE_ID,
             LV.VENDOR_ID,
             LV.PAYMENT_PCT,
             EXECUTORY_PAID1,
		     EXECUTORY_PAID2,
		     EXECUTORY_PAID3,
		     EXECUTORY_PAID4,
		     EXECUTORY_PAID5,
		     EXECUTORY_PAID6,
		     EXECUTORY_PAID7,
		     EXECUTORY_PAID8,
		     EXECUTORY_PAID9,
		     EXECUTORY_PAID10,
		     CONTINGENT_PAID1,
		     CONTINGENT_PAID2,
		     CONTINGENT_PAID3,
		     CONTINGENT_PAID4,
		     CONTINGENT_PAID5,
		     CONTINGENT_PAID6,
		     CONTINGENT_PAID7,
		     CONTINGENT_PAID8,
		     CONTINGENT_PAID9,
		     CONTINGENT_PAID10,
             ROW_NUMBER() OVER(partition by L.LEASE_ID, case when LO.ls_reconcile_type_id in (1, 2) then I.ILR_ID else -1 end,
											case when LO.ls_reconcile_type_id = 1 then a.ls_asset_id else -1 end,
											LV.VENDOR_ID, S.SET_OF_BOOKS_ID order by S.LS_ASSET_ID),
             S.SET_OF_BOOKS_ID,
			 A_COMPANY_ID,
			 A_MONTH, case when LO.ls_reconcile_type_id in (1, 2) then I.ILR_ID else -1 end, lo.ls_reconcile_type_id,
       nvl(A.ACTUAL_TERMINATION_AMOUNT,0),
       case when A.ACTUAL_RESIDUAL_AMOUNT is not null and A.ACTUAL_RESIDUAL_AMOUNT <> A.GUARANTEED_RESIDUAL_AMOUNT then
       A.GUARANTEED_RESIDUAL_AMOUNT - A.ACTUAL_RESIDUAL_AMOUNT
       else 0 end AS SALE_PROCEED_AMOUNT
			 bulk collect
        into L_ASSET_SCHEDULE_LINE
        from LS_ASSET_SCHEDULE S, LS_ASSET A, LS_ILR I, LS_LEASE L, LS_LEASE_VENDOR LV,
         LS_ILR_OPTIONS O, LS_LEASE_OPTIONS LO
       where I.COMPANY_ID = A_COMPANY_ID
         and I.ILR_ID = A.ILR_ID
         and I.LEASE_ID = L.LEASE_ID
         and S.LS_ASSET_ID = A.LS_ASSET_ID
         and A.APPROVED_REVISION = S.REVISION
         and LV.LEASE_ID = L.LEASE_ID
         and LV.COMPANY_ID = I.COMPANY_ID
         and O.ILR_ID = I.ILR_ID
         and O.REVISION = I.CURRENT_REVISION
		 and L.lease_id = LO.lease_id
		 and L.current_revision = LO.revision
		 and case when a_lease_id = -1 then -1 else l.lease_id end = A_LEASE_ID
         and S.MONTH = add_months(A_MONTH, nvl(O.PAYMENT_SHIFT,0))
         and (A.LS_ASSET_STATUS_ID = 3 or (A.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= A.RETIREMENT_DATE))
         and not exists
       (
         select 1
         from ls_payment_hdr ph
         where ph.gl_posting_mo_yr = a_month
         and l.lease_id = ph.lease_id
         and i.company_id = ph.company_id
         and lv.vendor_id = ph.vendor_id
         and case when LO.ls_reconcile_type_id in (1, 2) then i.ilr_id else -1 end = ph.ilr_id
			and case when LO.ls_reconcile_type_id = 1 then a.ls_asset_id else -1 end = ph.ls_asset_id
       );

      L_STATUS := 'Inserting dummy headers';
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_HEADER
         insert into LS_PAYMENT_HDR
            (PAYMENT_ID, LEASE_ID, VENDOR_ID, COMPANY_ID, GL_POSTING_MO_YR, PAYMENT_STATUS_ID, AP_STATUS_ID, ILR_ID, LS_ASSET_ID)
            select LS_PAYMENT_HDR_SEQ.NEXTVAL,
                   L_ASSET_SCHEDULE_HEADER   (SCHEDULEINDEX).LEASE_ID,
                   L_ASSET_SCHEDULE_HEADER   (SCHEDULEINDEX).VENDOR_ID,
                   A_COMPANY_ID,
                   A_MONTH,
                   1,
               1, L_ASSET_SCHEDULE_HEADER(SCHEDULEINDEX).ILR_ID, L_ASSET_SCHEDULE_HEADER(SCHEDULEINDEX).LS_ASSET_ID
              from DUAL;

      --Insert into approval tables
      L_STATUS := 'Inserting into LS_PAYMENT_APPROVAL';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		INSERT INTO ls_payment_approval (payment_id, time_stamp, approval_type_id, approval_status_id, approver, approval_date,	rejected)
		SELECT hdr.payment_id,
			SYSDATE,
			Decode (Nvl(lg.pymt_approval_flag, 1),
				0,
				auto.workflow_type_id,
				man.workflow_type_id),
			1,
			NULL,
			NULL,
			NULL
		FROM ls_payment_hdr hdr
		INNER JOIN ls_lease l
			ON hdr.lease_id = l.lease_id
		INNER JOIN ls_lease_group lg
			ON l.lease_group_id = lg.lease_group_id
        FULL OUTER JOIN workflow_type auto
			ON Lower(auto.external_workflow_type) = 'auto'
			AND Lower(auto.subsystem) LIKE '%ls_payment_approval%'
		FULL OUTER JOIN workflow_type man
			ON Lower(Nvl(man.external_workflow_type, ' ')) <> 'auto'
			AND Lower(man.subsystem) LIKE '%ls_payment_approval%'
		WHERE gl_posting_mo_yr = a_month
		AND company_id = a_company_id
		AND CASE
			WHEN a_lease_id = -1 THEN
				-1
			ELSE
				hdr.lease_id
			END = a_lease_id
		AND (payment_status_id = 1 OR payment_status_id = 4) /* wmd changed because we don't want to delete payments pending approval */
		AND payment_id NOT IN (
			SELECT payment_id
			FROM ls_invoice_payment_map
			WHERE in_tolerance = 1
		);

		L_STATUS := 'Checking for missing approval types on LS_PAYMENT_APPROVAL.';
		pkg_pp_log.P_write_message(L_STATUS);
		l_missing_workflow := '';
		FOR rec IN (SELECT DISTINCT Nvl(lg.pymt_approval_flag, 1) flag
					FROM ls_payment_approval app
					INNER JOIN ls_payment_hdr hdr
						ON app.payment_id = hdr.payment_id
					INNER JOIN ls_lease l
						ON hdr.lease_id = l.lease_id
					INNER JOIN ls_lease_group lg
						ON l.lease_group_id = lg.lease_group_id
					WHERE hdr.gl_posting_mo_yr = a_month
					AND hdr.company_id = a_company_id
					AND CASE
						WHEN a_lease_id = -1 THEN
							-1
						ELSE
							hdr.lease_id
						END = a_lease_id
					AND (hdr.payment_status_id = 1 OR hdr.payment_status_id = 4)
					AND hdr.payment_id NOT IN (
						SELECT payment_id
						FROM ls_invoice_payment_map
						WHERE in_tolerance = 1
					)
					AND app.approval_type_id IS NULL)
		LOOP
			--if there's already something in the string, add a comma and space
			IF Nvl(Length(l_missing_workflow),0) <> 0 THEN
				l_missing_workflow := l_missing_workflow || ', ';
			END IF;

			IF rec.flag = 0 THEN
				l_missing_workflow := l_missing_workflow || 'Could not find auto approval workflow for "ls_payment_approval".';
			ELSE
				l_missing_workflow := l_missing_workflow || 'Could not find manual approval workflow for "ls_payment_approval".';
			END IF;
		END LOOP;

		IF Nvl(Length(l_missing_workflow),0) <> 0 THEN
			--there was an error. print to logs and raise exception
			PKG_PP_LOG.P_WRITE_MESSAGE(l_missing_workflow);
			Raise_Application_Error(-20000,'Could not find one or more approval workflows. See logs for details.');
		END IF;

      L_STATUS := 'Adding Principal to Payment Line';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_LINE
         insert into LS_PAYMENT_LINE
            (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, AMOUNT,
             GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select PAYMENT_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ROWNUMBER,
                    1,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PRINCIPAL_PAID * L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PAYMENT_PCT,
                    A_MONTH,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from LS_PAYMENT_HDR
              where GL_POSTING_MO_YR = A_MONTH
                and LEASE_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LEASE_ID
				    and ILR_ID = case when L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_RECONCILE_TYPE_ID in (1,2) then L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ILR_ID else -1 end
                and LS_ASSET_ID = case when L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_RECONCILE_TYPE_ID = 1 then L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID else -1 end
				    and COMPANY_ID = A_COMPANY_ID
                and VENDOR_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).VENDOR_ID
                and L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PRINCIPAL_PAID <> 0
                and (PAYMENT_STATUS_ID = 1 or PAYMENT_STATUS_ID = 4) /* WMD Changed because we don't want to delete payments pending approval */
                and payment_id not in (select payment_id from ls_invoice_payment_map where in_tolerance = 1));


	 /* WMD removed interim interest part now that we're including it in the schedule */
      L_STATUS := 'Adding Interest to Payment Line';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_LINE
         insert into LS_PAYMENT_LINE
            (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, AMOUNT,
             GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select PAYMENT_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ROWNUMBER,
                    2,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID,
                    (L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).INTEREST_PAID * L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PAYMENT_PCT),
                    A_MONTH,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from LS_PAYMENT_HDR
              where GL_POSTING_MO_YR = A_MONTH
                and LEASE_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LEASE_ID
				and ILR_ID = case when L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_RECONCILE_TYPE_ID in (1,2)
							then L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ILR_ID else -1 end
                and LS_ASSET_ID = case when L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_RECONCILE_TYPE_ID = 1
							then L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID else -1 end
                and COMPANY_ID = A_COMPANY_ID
                and VENDOR_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).VENDOR_ID
				and (L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).INTEREST_PAID * L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PAYMENT_PCT) <> 0);

	  L_STATUS := 'Inserting executory payment lines';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      PKG_LEASE_BUCKET.P_CALC_EXEC_PAYMENTS(L_ASSET_SCHEDULE_LINE);

      L_STATUS := 'Inserting contingent payment lines';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      PKG_LEASE_BUCKET.P_CALC_CONT_PAYMENTS(L_ASSET_SCHEDULE_LINE);

/* CJS 4/20/15 Adding term penalty to payment calc based on system control */
	  if lower(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('LEASE TERM PENALTY AMOUNT INCLUDED', A_COMPANY_ID)) = 'yes' then
	  L_STATUS := 'Adding Term Penalty Amount to Payment Line';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_LINE
         insert into LS_PAYMENT_LINE
            (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, AMOUNT,
             GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select PAYMENT_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ROWNUMBER,
                    23,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ACTUAL_TERMINATION_AMOUNT * L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PAYMENT_PCT,
                    A_MONTH,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from LS_PAYMENT_HDR
              where GL_POSTING_MO_YR = A_MONTH
                and LEASE_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LEASE_ID
				    and ILR_ID = case when L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_RECONCILE_TYPE_ID in (1,2) then L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ILR_ID else -1 end
                and LS_ASSET_ID = case when L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_RECONCILE_TYPE_ID = 1 then L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID else -1 end
				    and COMPANY_ID = A_COMPANY_ID
                and VENDOR_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).VENDOR_ID
                and L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ACTUAL_TERMINATION_AMOUNT <> 0
                and (PAYMENT_STATUS_ID = 1 or PAYMENT_STATUS_ID = 4)
                and payment_id not in (select payment_id from ls_invoice_payment_map where in_tolerance = 1));

	  end if;

	  if lower(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('LEASE SALES PROCEED AMOUNT INCLUDED', A_COMPANY_ID)) = 'yes' then
	  L_STATUS := 'Adding Sales Proceeds Amount to Payment Line';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      forall SCHEDULEINDEX in indices of L_ASSET_SCHEDULE_LINE
         insert into LS_PAYMENT_LINE
            (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, AMOUNT,
             GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
            (select PAYMENT_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ROWNUMBER,
                    24,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).sale_proceed_amount * L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).PAYMENT_PCT,
                    A_MONTH,
                    L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).SET_OF_BOOKS_ID
               from LS_PAYMENT_HDR
              where GL_POSTING_MO_YR = A_MONTH
                and LEASE_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LEASE_ID
				    and ILR_ID = case when L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_RECONCILE_TYPE_ID in (1,2) then L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).ILR_ID else -1 end
                and LS_ASSET_ID = case when L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_RECONCILE_TYPE_ID = 1 then L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).LS_ASSET_ID else -1 end
				    and COMPANY_ID = A_COMPANY_ID
                and VENDOR_ID = L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).VENDOR_ID
                and L_ASSET_SCHEDULE_LINE(SCHEDULEINDEX).sale_proceed_amount <> 0
                and (PAYMENT_STATUS_ID = 1 or PAYMENT_STATUS_ID = 4)
                and payment_id not in (select payment_id from ls_invoice_payment_map where in_tolerance = 1));

	  end if;

      L_STATUS := 'Looping complete';
      P_PAYMENT_ROLLUP( A_LEASE_ID, A_COMPANY_ID, A_MONTH );
      L_STATUS := 'Roll up Complete';

      L_STATUS := 'Delete the payments with $0';
      delete from LS_PAYMENT_APPROVAL
       where PAYMENT_ID in (select PAYMENT_ID
                              from LS_PAYMENT_HDR
                             where AMOUNT = 0
                               and COMPANY_ID = A_COMPANY_ID
                               and GL_POSTING_MO_YR = A_MONTH
							   and case when a_lease_id = -1 then -1 else lease_id end = A_LEASE_ID
                        and PAYMENT_STATUS_ID < 4); /* WMD we need to get rejected stuff too */

      delete from LS_PAYMENT_HDR
       where AMOUNT = 0
         and COMPANY_ID = A_COMPANY_ID
         and GL_POSTING_MO_YR = A_MONTH
		 and case when a_lease_id = -1 then -1 else lease_id end = A_LEASE_ID
       and PAYMENT_STATUS_ID < 4; /* WMD we need to get rejected stuff too */

      L_STATUS := 'Generating invoices for auto MLAs';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      P_AUTO_GENERATE_INVOICES(A_LEASE_ID, A_COMPANY_ID, A_MONTH);

      if A_LEASE_ID = -1 then
		L_STATUS := 'Matching Invoices';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         L_STATUS:= F_MATCH_INVOICES(A_MONTH);
         /* WMD */
         IF L_STATUS <> 'OK' THEN
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            PKG_PP_LOG.P_END_LOG();
           RETURN L_STATUS;
         end if;

	  end if;

      PKG_PP_LOG.P_WRITE_MESSAGE('Payment Calculation Completed Successfully for Company: '|| to_char(A_COMPANY_ID));
      IF A_END_LOG=1 THEN
        PKG_PP_LOG.P_END_LOG;
	  end if;

	  return 'OK';
   exception
      when others then
		 PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS || ' : ' || sqlerrm);
		 PKG_PP_LOG.P_END_LOG;
         return L_STATUS || sqlerrm;
   end F_PAYMENT_CALC;


   -- wrapper function.  Pass in -1 as lease id meaning all leases for the company
   function F_PAYMENT_CALC(A_COMPANY_ID in number,
                           A_MONTH      in date,
						   A_END_LOG    in number:=null) return varchar2 is
   begin
		return F_PAYMENT_CALC( -1, a_company_id, a_month, a_end_log);
   exception
      when others then
         return sqlerrm;
   end F_PAYMENT_CALC;

   --**************************************************************************
   --                            F_PAYMENT_APPROVE
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will approve and post the monthly accrual numbers by ls_asset.
   --    It will load from ls_asset_schedule into
   -- @@PARAMS
   --    date: a_month
   --       The month to process accruals for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************

   function F_PAYMENT_APPROVE(A_COMPANY_ID in number,
                              A_MONTH      in date,
                              A_END_LOG    in number:=null) return varchar2 is
      L_STATUS varchar2(30000);
	  L_LOCATION varchar2(30000);
      L_RTN    number;
      L_COUNTER number;
      L_GL_JE_CODE   varchar2(35);
	  BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
	  COUNTER number;
	  TAX_PAYMENT_TYPE number;
	  L_ACCTS PKG_LEASE_BUCKET.BUCKET_ACCTS;
   begin
    PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
	PKG_PP_LOG.P_WRITE_MESSAGE('Begin Payment Journals Process - Company ID: ' || a_company_id || ' - Month: ' || to_char(a_month, 'mon-yyyy'));

	/* CJS 3/16/15 Adding check to make sure everything is approved */
	select count(1)
	into L_COUNTER
	from LS_PAYMENT_HDR LPH, LS_PAYMENT_APPROVAL LPA
	where LPH.PAYMENT_ID = LPA.PAYMENT_ID
	and LPH.COMPANY_ID = A_COMPANY_ID
	and LPH.GL_POSTING_MO_YR = A_MONTH
	and LPA.APPROVAL_STATUS_ID <> 3
	and APPROVAL_TYPE_ID <> (select WORKFLOW_TYPE_ID from WORKFLOW_TYPE where lower(description) = 'auto approve');

	if L_COUNTER <> 0 then
     PKG_PP_LOG.P_WRITE_MESSAGE('Payments exist that need to be approved for this company and month');
     PKG_PP_LOG.P_END_LOG();
	   return 'Payments exist that need to be approved for this company and month';
	end if;

  PKG_PP_LOG.P_WRITE_MESSAGE('Sending Interim Interest Journals');
	L_STATUS := F_PAYMENT_II(A_COMPANY_ID, A_MONTH);
  if L_STATUS <> 'OK' THEN
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    PKG_PP_LOG.P_END_LOG();
    return L_RTN;
  end if;

     select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LAMPAY'))
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
      and G.PROCESS_ID = 'LAMPAY';


	  PKG_PP_LOG.P_WRITE_MESSAGE('Principal and Interest Journals');
      for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
                                H.VENDOR_ID as VENDOR_ID,
                                /* WMD ADJUSTMENT */
                                CASE WHEN L.payment_type_id = 2 THEN nvl(ii.ii_amount,0) ELSE 0 END +
                                NVL(L.AMOUNT,0) + NVL(L.ADJUSTMENT_AMOUNT,0) as PAYMENT_AMOUNT,
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                M.INVOICE_ID as INVOICE_ID,
                                case
                                   when L.PAYMENT_TYPE_ID = 1 then
                                    3018
                                   when L.PAYMENT_TYPE_ID in (2) then
                                    3019
                                end as TRANS_TYPE,
                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                                I.INT_ACCRUAL_ACCOUNT_ID as INT_ACCRUAL_ACCOUNT,
                                I.AP_ACCOUNT_ID as AP_ACCOUNT_ID,
                                /* CJS 6/8/17 Book expense adjustments */
                                NVL(L.ADJUSTMENT_AMOUNT,0) as ADJUSTMENT_AMOUNT,
                                I.INT_EXPENSE_ACCOUNT_ID AS INT_EXPENSE_ACCOUNT,
                                h.rate,
                                h.company_currency_id,
                                H.contract_currency_id
                           from V_LS_PAYMENT_HDR_FX H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M, LS_ILR_ACCOUNT I, LS_PAYMENT_APPROVAL LPA,
                                (select la.ls_asset_id, -sum(nvl(ii.amount,0)) as ii_amount
                                from LS_COMPONENT_MONTHLY_II_STG II, ls_asset la, ls_component lc
                                where la.ls_asset_id = lc.ls_asset_id
                                  and lc.component_id = ii.component_id
                                  and la.company_id = A_COMPANY_ID
                                  and II.MONTH = A_MONTH
                                group by la.ls_asset_id ) II
                          where H.GL_POSTING_MO_YR =  A_MONTH
                            and H.PAYMENT_ID = L.PAYMENT_ID
                            and H.PAYMENT_ID = M.PAYMENT_ID
                            and case when L.PAYMENT_TYPE_ID = 2 then NVL(II.II_AMOUNT,0) else 0 end +
                                NVL(L.AMOUNT,0) + NVL(L.ADJUSTMENT_AMOUNT,0) <> 0

                            and L.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.ILR_ID = I.ILR_ID
                            and A.COMPANY_ID = A_COMPANY_ID
							              and L.PAYMENT_TYPE_ID in (1,2)
                            and L.LS_ASSET_ID = II.LS_ASSET_ID (+)
                     AND H.payment_id = lpa.payment_id
                     AND H.ls_cur_type = 2
                     --and nvl(LPA.REJECTED,0) <> 1
                     and (LPA.APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select /* CJS Fix APPROVAL_TYPE_ID */WORKFLOW_TYPE_ID
                                                               from WORKFLOW_TYPE
                                                               where lower(description) = 'auto approve')
                        and LPA.APPROVAL_STATUS_ID  = 1)))

      loop
         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
                     to_char(l_payments.trans_type);
         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               L_PAYMENTS.TRANS_TYPE,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               case
												   when L_PAYMENTS.TRANS_TYPE in (3018) then
													  -1
												   when L_PAYMENTS.TRANS_TYPE = 3019 then
													  L_PAYMENTS.INT_ACCRUAL_ACCOUNT
												end,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               A_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               l_payments.rate,
                                               l_payments.contract_currency_id,
                                               l_payments.company_currency_id,
                                               to_char(l_payments.invoice_id),
                                               L_STATUS);
         if L_RTN = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_LOCATION || ' : ' || L_STATUS;
         end if;

         -- process the credit.  Payment credits all hit 3022
         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
                     ' trans type: 3022';
         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               3022,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               L_PAYMENTS.AP_ACCOUNT_ID,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               A_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               l_payments.rate,
                                               l_payments.contract_currency_id,
                                               l_payments.company_currency_id,
                                               to_char(l_payments.invoice_id),
                                               L_STATUS);
         if L_RTN = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_LOCATION || ' : ' || L_STATUS;
         end if;

         /* CJS 6/8/17 Book Expense Adjustments Start */
         if L_PAYMENTS.ADJUSTMENT_AMOUNT <> 0 then
            L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
                        ' trans type: 3010';
            L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE( L_PAYMENTS.LS_ASSET_ID,
                                                  3010,
                                                      L_PAYMENTS.ADJUSTMENT_AMOUNT,
                                                  0,
                                                  -1,
                                                  L_PAYMENTS.WORK_ORDER_ID,
                                                  L_PAYMENTS.INT_EXPENSE_ACCOUNT,
                                                  0,
                                                  -1,
                                                  L_PAYMENTS.COMPANY_ID,
                                                  A_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                      l_payments.set_of_books_id,
                                                      l_payments.rate,
                                                      l_payments.contract_currency_id,
                                                      l_payments.company_currency_id,
                                                  TO_CHAR(L_PAYMENTS.INVOICE_ID),
                                                  L_STATUS);
            if L_RTN = -1 then
                PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
                PKG_PP_LOG.P_END_LOG();
                return L_LOCATION || ' : ' || L_STATUS;
            end if;

            -- process the credit.  Payment credits all hit 3022
            L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
                        ' trans type: 3011';
            L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE( L_PAYMENTS.LS_ASSET_ID,
                                                  3011,
                                                      L_PAYMENTS.ADJUSTMENT_AMOUNT,
                                                  0,
                                                  -1,
                                                  L_PAYMENTS.WORK_ORDER_ID,
                                                  L_PAYMENTS.INT_ACCRUAL_ACCOUNT,
                                                  0,
                                                  -1,
                                                  L_PAYMENTS.COMPANY_ID,
                                                  A_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  L_PAYMENTS.SET_OF_BOOKS_ID,
                                                      l_payments.rate,
                                                      l_payments.contract_currency_id,
                                                      l_payments.company_currency_id,
                                                  TO_CHAR(L_PAYMENTS.INVOICE_ID),
                                                  L_STATUS);
            if L_RTN = -1 then
                PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
                PKG_PP_LOG.P_END_LOG();
                return L_LOCATION || ' : ' || L_STATUS;
            end if;
         end if;
         /* CJS 6/8/17 End */

       L_COUNTER := L_COUNTER + 1;
      end loop;

	  --TAXES
	  --get the tax bucket
	  L_STATUS := 'Insert tax amounts on schedule';
      select count(*)
      into COUNTER
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET =1;

      if COUNTER <> 1 then
         return 'Error: there must be exactly one bucket defined as the tax expense bucket';
      end if;

      select *
      into BUCKET
      from LS_RENT_BUCKET_ADMIN
      where TAX_EXPENSE_BUCKET = 1;

	  TAX_PAYMENT_TYPE := PKG_LEASE_BUCKET.F_GET_TAX_TYPE(BUCKET.RENT_TYPE,BUCKET.BUCKET_NUMBER);

	  PKG_PP_LOG.P_WRITE_MESSAGE('Tax Payment Journals');
	  for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
                                H.VENDOR_ID as VENDOR_ID,
                                /* WMD ADJUSTMENT */
                                NVL(LMT.AMOUNT,0) + NVL(LMT.ADJUSTMENT_AMOUNT,0) as PAYMENT_AMOUNT, --Note that this is off the monthly tax table and not payment lines
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                M.INVOICE_ID as INVOICE_ID,
                                3045 as TRANS_TYPE,
                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                                LTL.ACCRUAL_ACCT_ID as ACCRUAL_ACCOUNT,
								LTL.EXPENSE_ACCT_ID as EXPENSE_ACCOUNT,
								LTL.AP_ACCT_ID as AP_ACCOUNT,
								ltl.tax_local_id AS tax_local_id,
                                H.rate,
                                H.contract_currency_id,
                                h.company_currency_id
                           from V_LS_PAYMENT_HDR_FX H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M, LS_ILR_ACCOUNT I,
								LS_PAYMENT_APPROVAL LPA, LS_TAX_LOCAL LTL, LS_MONTHLY_TAX LMT
                          where H.GL_POSTING_MO_YR = A_MONTH
                            and H.PAYMENT_ID = L.PAYMENT_ID
                            and H.PAYMENT_ID = M.PAYMENT_ID
							and h.vendor_id = decode(lmt.vendor_id, -1, h.vendor_id, lmt.vendor_id)
                            and NVL(LMT.AMOUNT,0) + NVL(LMT.ADJUSTMENT_AMOUNT,0) <> 0 /* WMD */
                            and L.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.ILR_ID = I.ILR_ID
                            and A.COMPANY_ID = A_COMPANY_ID
							and L.PAYMENT_TYPE_ID = TAX_PAYMENT_TYPE
							and LTL.TAX_LOCAL_ID = LMT.TAX_LOCAL_ID
							and LMT.ACCRUAL = 0
							and LMT.LS_ASSET_ID = L.LS_ASSET_ID
							and LMT.GL_POSTING_MO_YR = L.GL_POSTING_MO_YR
							and LMT.SET_OF_BOOKS_ID = L.SET_OF_BOOKS_ID
							AND H.payment_id = lpa.payment_id
              AND H.ls_cur_type = 2
							--and nvl(LPA.REJECTED,0) <> 1
							and (LPA.APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select /* CJS Fix APPROVAL_TYPE_ID */WORKFLOW_TYPE_ID
																	   from WORKFLOW_TYPE
																	   where lower(description) = 'auto approve')
															and LPA.APPROVAL_STATUS_ID  = 1)))
      loop
         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
                     to_char(l_payments.trans_type);
         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               L_PAYMENTS.TRANS_TYPE,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               L_PAYMENTS.TAX_LOCAL_ID,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               L_PAYMENTS.ACCRUAL_ACCOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               A_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               l_payments.rate,
                                               l_payments.contract_currency_id,
                                               l_payments.company_currency_id,
                                               to_char(l_payments.invoice_id),
                                               L_STATUS);
         if L_RTN = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_LOCATION || ' : ' || L_STATUS;
         end if;

         -- process the credit.  Payment credits all hit 3046
         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
                     ' trans type: 3046';
         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               L_PAYMENTS.TRANS_TYPE + 1,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               L_PAYMENTS.TAX_LOCAL_ID,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               L_PAYMENTS.AP_ACCOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               A_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               l_payments.rate,
                                               l_payments.contract_currency_id,
                                               l_payments.company_currency_id,
                                               to_char(l_payments.invoice_id),
                                               L_STATUS);
         if L_RTN = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_LOCATION || ' : ' || L_STATUS;
         end if;
       L_COUNTER := L_COUNTER + 1;
      end loop;

	  L_ACCTS := PKG_LEASE_BUCKET.F_GET_ACCTS;

	  PKG_PP_LOG.P_WRITE_MESSAGE('Executory and Contingent Journals');
	  for L_PAYMENTS in (select L.LS_ASSET_ID as LS_ASSET_ID,
                                H.VENDOR_ID as VENDOR_ID,
                                /* WMD */
                                NVL(L.AMOUNT,0) + NVL(L.ADJUSTMENT_AMOUNT,0) as PAYMENT_AMOUNT,
                                A.WORK_ORDER_ID as WORK_ORDER_ID,
                                A.COMPANY_ID as COMPANY_ID,
                                A.IN_SERVICE_DATE as IN_SERVICE_DATE,
                                M.INVOICE_ID as INVOICE_ID,
                                case
                                   when L.PAYMENT_TYPE_ID between 3 and 12 then
                                    3020
                                   when L.PAYMENT_TYPE_ID between 13 and 22 then
                                    3021
                                end as TRANS_TYPE,
                                L.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                                I.AP_ACCOUNT_ID as AP_ACCOUNT_ID,
                                L.PAYMENT_TYPE_ID as PAYMENT_TYPE_ID,
                                /* CJS 6/8/17 Book Expense Adjustments */
                                NVL(L.ADJUSTMENT_AMOUNT,0) as ADJUSTMENT_AMOUNT,
                                case
                                   when L.PAYMENT_TYPE_ID between 3 and 12 then
                                    3012
                                   when L.PAYMENT_TYPE_ID between 13 and 22 then
                                    3014
                                end as ACC_TRANS_TYPE,
                                h.rate,
                                h.contract_currency_id,
                                h.company_currency_id
                           from V_LS_PAYMENT_HDR_FX H, LS_PAYMENT_LINE L, LS_ASSET A, LS_INVOICE_PAYMENT_MAP M, LS_ILR_ACCOUNT I,
								LS_PAYMENT_APPROVAL LPA
                          where H.GL_POSTING_MO_YR = A_MONTH
                            and H.PAYMENT_ID = L.PAYMENT_ID
                            and H.PAYMENT_ID = M.PAYMENT_ID
                            and NVL(L.AMOUNT,0) + NVL(L.ADJUSTMENT_AMOUNT,0) <> 0 /* WMD */
                            and L.LS_ASSET_ID = A.LS_ASSET_ID
                            and A.ILR_ID = I.ILR_ID
                            and A.COMPANY_ID = A_COMPANY_ID
							AND L.payment_type_id BETWEEN 3 AND 22
              AND H.ls_cur_type = 2
                     and H.PAYMENT_ID = LPA.PAYMENT_ID
                     --and nvl(LPA.REJECTED,0) <> 1
                     and (LPA.APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select /* CJS Fix APPROVAL_TYPE_ID */WORKFLOW_TYPE_ID
                                                               from WORKFLOW_TYPE
                                                               where lower(description) = 'auto approve')
                        and LPA.APPROVAL_STATUS_ID  = 1))
					 )
      loop

		 --skip the taxes since we already did them
		 if L_PAYMENTS.PAYMENT_TYPE_ID = TAX_PAYMENT_TYPE then
			continue;
		 end if;

         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
                     to_char(l_payments.trans_type);
         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               L_PAYMENTS.TRANS_TYPE,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               L_ACCTS(L_PAYMENTS.PAYMENT_TYPE_ID).ACCRUAL_ACCT_ID,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               A_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               l_payments.set_of_books_id,
                                               l_payments.rate,
                                               l_payments.contract_currency_id,
                                               l_payments.company_currency_id,
                                               to_char(l_payments.invoice_id),
                                               L_STATUS);
         if L_RTN = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_LOCATION || ' : ' || L_STATUS||' '||sqlerrm;
         end if;

         -- process the credit.  Payment credits all hit 3022
         L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
                     ' trans type: 3022';
         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_PAYMENTS.LS_ASSET_ID,
                                               3022,
                                               L_PAYMENTS.PAYMENT_AMOUNT,
                                               0,
                                               -1,
                                               L_PAYMENTS.WORK_ORDER_ID,
                                               L_PAYMENTS.AP_ACCOUNT_ID,
                                               0,
                                               -1,
                                               L_PAYMENTS.COMPANY_ID,
                                               A_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_PAYMENTS.SET_OF_BOOKS_ID,
                                               l_payments.rate,
                                               l_payments.contract_currency_id,
                                               l_payments.company_currency_id,
                                               to_char(l_payments.invoice_id),
                                               L_STATUS);
         if L_RTN = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_LOCATION || ' : ' || L_STATUS;
         end if;

         /* CJS 6/8/17 Book expense adjustments start */
         if L_PAYMENTS.ADJUSTMENT_AMOUNT <> 0 then
            L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) || ' trans type: ' ||
                        to_char(l_payments.acc_trans_type);
            L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE( L_PAYMENTS.LS_ASSET_ID,
                                                      l_payments.acc_trans_type,
                                                      L_PAYMENTS.ADJUSTMENT_AMOUNT,
                                                  0,
                                                  -1,
                                                  L_PAYMENTS.WORK_ORDER_ID,
                                                  L_ACCTS(L_PAYMENTS.PAYMENT_TYPE_ID).EXPENSE_ACCT_ID,
                                                  0,
                                                  -1,
                                                  L_PAYMENTS.COMPANY_ID,
                                                  A_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                  L_PAYMENTS.SET_OF_BOOKS_ID,
                                                      l_payments.rate,
                                                      l_payments.contract_currency_id,
                                                      l_payments.company_currency_id,
                                                  TO_CHAR(L_PAYMENTS.INVOICE_ID),
                                                  L_STATUS);
            if L_RTN = -1 then
                PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
                PKG_PP_LOG.P_END_LOG();
                return L_LOCATION || ' : ' || L_STATUS||' '||sqlerrm;
            end if;

            -- process the credit.  Payment credits all hit 3022
            L_LOCATION := 'Processing asset_id: ' || TO_CHAR(L_PAYMENTS.LS_ASSET_ID) ||
                        to_char(l_payments.acc_trans_type + 1);
            L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE( L_PAYMENTS.LS_ASSET_ID,
                                                      l_payments.acc_trans_type + 1,
                                                      L_PAYMENTS.ADJUSTMENT_AMOUNT,
                                                  0,
                                                  -1,
                                                  L_PAYMENTS.WORK_ORDER_ID,
                                                  L_ACCTS(L_PAYMENTS.PAYMENT_TYPE_ID).ACCRUAL_ACCT_ID,
                                                  0,
                                                  -1,
                                                  L_PAYMENTS.COMPANY_ID,
                                                  A_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  L_PAYMENTS.SET_OF_BOOKS_ID,
                                                      l_payments.rate,
                                                      l_payments.contract_currency_id,
                                                      l_payments.company_currency_id,
                                                  TO_CHAR(L_PAYMENTS.INVOICE_ID),
                                                  L_STATUS);
            if L_RTN = -1 then
                PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
                PKG_PP_LOG.P_END_LOG();
                return L_LOCATION || ' : ' || L_STATUS;
          end if;
       end if;
       /* CJS 6/8/17 End */

       L_COUNTER := L_COUNTER + 1;
      end loop;

     if L_COUNTER > 0 then
      PKG_PP_LOG.P_WRITE_MESSAGE(L_COUNTER||' JE pairs sent.');
     end if;

     L_LOCATION:='Updating auto approvals to approved - auto status';
     update ls_payment_hdr
    set payment_status_id = 6
    where gl_posting_mo_yr = A_MONTH
      and lease_id in (select lease_id from ls_lease where lease_group_id in
      (select lease_group_id from ls_lease_group where nvl(pymt_approval_flag,0) = 0))
      and company_id = A_COMPANY_ID;


	  if A_END_LOG=1 THEN
        PKG_PP_LOG.P_END_LOG();
    end if;

      return 'OK';
--     else
--	    PKG_PP_LOG.P_WRITE_MESSAGE('No JEs sent. Please confirm that payments have been approved.');
--      PKG_PP_LOG.P_END_LOG();
--      return 'No JEs sent. Please confirm that payments have been approved.';
--     end if;

   exception
      when others then
         return L_STATUS || ' : ' || sqlerrm;
   end F_PAYMENT_APPROVE;

   --**************************************************************************
   --                            F_SEND_PAYMENT
   --**************************************************************************

   function F_SEND_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin

      update LS_PAYMENT_HDR set PAYMENT_STATUS_ID = 2 where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_APPROVAL set APPROVAL_STATUS_ID = 2 where PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Sending Payment');
         return -1;
   end F_SEND_PAYMENT;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_PAYMENT
   --**************************************************************************

   function F_UPDATE_WORKFLOW_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_PAYMENT_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_PAYMENT_ID)
                                        and SUBSYSTEM = 'ls_payment_approval'),
                                     0)
       where PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;

   exception
      when others then
         rollback;
         return -1;
   end F_UPDATE_WORKFLOW_PAYMENT;

   --**************************************************************************
   --                            F_REJECT_PAYMENT
   --**************************************************************************

   function F_REJECT_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_PAYMENT_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_HDR L set PAYMENT_STATUS_ID = 4 where L.PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Rejecting Payment');
         return -1;
   end F_REJECT_PAYMENT;

   --**************************************************************************
   --                            F_UNREJECT_PAYMENT
   --**************************************************************************

   function F_UNREJECT_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_PAYMENT_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 7
       where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_HDR L set PAYMENT_STATUS_ID = 2 where L.PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Unrejecting Payment');
         return -1;
   end F_UNREJECT_PAYMENT;

   --**************************************************************************
   --                            F_APPROVE_PAYMENT
   --**************************************************************************

   function F_APPROVE_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this payment
      update LS_PAYMENT_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_HDR L
         set PAYMENT_STATUS_ID = 3
       where PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error Approving Payment');
         return -1;
   end F_APPROVE_PAYMENT;

   --**************************************************************************
   --                            F_UNSEND_PAYMENT
   --**************************************************************************

   function F_UNSEND_PAYMENT(A_PAYMENT_ID in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_PAYMENT_APPROVAL
         set REJECTED = 2, APPROVAL_STATUS_ID = 1
       where PAYMENT_ID = A_PAYMENT_ID;

      update LS_PAYMENT_HDR L
         set PAYMENT_STATUS_ID = 1, GL_POSTING_MO_YR = null
       where PAYMENT_ID = A_PAYMENT_ID;

      commit;

      return 1;
   exception
      when others then
         rollback;
         RAISE_APPLICATION_ERROR(-20000, 'Error UnSending Payment');
         return -1;
   end F_UNSEND_PAYMENT;

   --**************************************************************************
   --                            F_MATCH_INVOICES
   --**************************************************************************
   function F_MATCH_INVOICES(A_MONTH in date) return varchar2 is
      L_STATUS      varchar2(2000);
      L_RTN         number;
      MYPAYMENTHDRS PAYMENT_HDR_TYPE;
      MYINVOICEHDRS INVOICE_HDR_TYPE;
      INVOICEINDEX  number;
      PAYMENTINDEX  number;
   begin

      select h.* bulk collect
        into MYPAYMENTHDRS
        from LS_PAYMENT_HDR h
       where h.GL_POSTING_MO_YR = A_MONTH
	   and not exists
	   (
			select 1
			from ls_invoice_payment_map m
			where m.payment_id = h.payment_id
	   );

      select h.* bulk collect
	  into MYINVOICEHDRS
	  from LS_INVOICE h
	  where h.GL_POSTING_MO_YR = A_MONTH
	  and not exists
	   (
			select 1
			from ls_invoice_payment_map m
			where m.invoice_id = h.invoice_id
	   );

      for INVOICEINDEX in 1 .. MYINVOICEHDRS.COUNT
      loop
         for PAYMENTINDEX in 1 .. MYPAYMENTHDRS.COUNT
         loop
            P_INVOICE_COMPARE(MYINVOICEHDRS(INVOICEINDEX), MYPAYMENTHDRS(PAYMENTINDEX));
         end loop;
      end loop;
      return 'OK';
   end F_MATCH_INVOICES;

   --**************************************************************************
   --                            P_INVOICE_COMPARE
   --**************************************************************************

   procedure P_INVOICE_COMPARE(MYINVOICE in LS_INVOICE%rowtype,
                               MYPAYMENT in LS_PAYMENT_HDR%rowtype) is
      TOLERANCE_PCT    number;
      LG_TOLERANCE_PCT number;
      IS_IN_TOLERANCE  number;
	  adj_amount number;
	  total_amount number;
    counter number;
   begin


      if MYINVOICE.VENDOR_ID = MYPAYMENT.VENDOR_ID and MYINVOICE.COMPANY_ID = MYPAYMENT.COMPANY_ID and
         MYINVOICE.LEASE_ID = MYPAYMENT.LEASE_ID and MYINVOICE.ILR_ID = MYPAYMENT.ILR_ID and
		 MYINVOICE.LS_ASSET_ID = MYPAYMENT.LS_ASSET_ID		 then

			/* CJS 8/17/17 Should only check if the invoice has already been matched, not if it's found an in_tolerance match; allows duplicates to match */
			select count(1)
            into counter
            from ls_invoice_payment_map
            where payment_id = MYPAYMENT.payment_id
              -- and in_tolerance = 1
              ;

            if counter > 0 then
              return;
            end if;

         --Check the tolerance status
         if MYPAYMENT.AMOUNT <> 0 then
            TOLERANCE_PCT := (MYPAYMENT.AMOUNT - MYINVOICE.INVOICE_AMOUNT) / MYPAYMENT.AMOUNT;
         else
            TOLERANCE_PCT := 1;
         end if;

         --
         select min(NVL(TOLERANCE_PCT, 0))
           into LG_TOLERANCE_PCT
           from LS_LEASE_GROUP
          where LEASE_GROUP_ID in
                (select AA.LEASE_GROUP_ID from LS_LEASE AA where AA.LEASE_ID = MYPAYMENT.LEASE_ID);

         --Accept it as being in tolerance if the field is null or zero on LS_LEASE_GROUP
          /* CJS 2/13/15 Fix for 0 percent tolerance */
         if ABS(TOLERANCE_PCT) <= LG_TOLERANCE_PCT then
            IS_IN_TOLERANCE := 1;
         else
            IS_IN_TOLERANCE := 0;
         end if;

         --INSERT INTO LS_INVOICE_PAYMENT_MAP (invoice_id, payment_id, in_tolerance) values (myInvoice.Invoice_id, myPayment.Payment_id, is_in_tolerance);
         insert into LS_INVOICE_PAYMENT_MAP LIPM
             (INVOICE_ID, PAYMENT_ID, IN_TOLERANCE)
         values
             (MYINVOICE.INVOICE_ID, MYPAYMENT.PAYMENT_ID, IS_IN_TOLERANCE);

		if is_in_tolerance = 1 and MYPAYMENT.AMOUNT <> MYINVOICE.INVOICE_AMOUNT then
			-- update the payment line and payment hdr
			adj_amount := MYINVOICE.INVOICE_AMOUNT - MYPAYMENT.AMOUNT;

			select sum(nvl(l.amount,0) + nvl(l.adjustment_amount,0)) /* WMD */
			into total_amount
			from ls_payment_line l, ls_invoice_payment_map m
			where l.payment_id = m.payment_id
			and m.invoice_id = MYINVOICE.INVOICE_ID
			and l.payment_type_id = 2
			;

			update ls_payment_line lpl
			set lpl.adjustment_amount = lpl.adjustment_amount + /* WMD */
			(
				with rounder as
				(
					select l.payment_line_number, l.ls_asset_id, l.payment_id,
						round(adj_amount * l.amount / total_amount, 2) as amount_wo_rounding,
						adj_amount - case when row_number() over(order by l.amount desc, l.ls_asset_id) = 1
							then sum(round(adj_amount * l.amount / total_amount, 2)) over() else adj_amount end as diff
					from ls_payment_line l, ls_invoice_payment_map m
					where l.payment_id = m.payment_id
					and m.invoice_id = MYINVOICE.INVOICE_ID
					and l.payment_type_id = 2
					and l.set_of_books_id = 1
				)
				select amount_wo_rounding + diff
				from rounder
				where rounder.ls_asset_id = lpl.ls_asset_id
				and rounder.payment_id = lpl.payment_id
				and rounder.payment_line_number = lpl.payment_line_number
			)
			where lpl.payment_type_id = 2
			and set_of_books_id = 1
			and exists
			(
				select 1
				from ls_invoice_payment_map m
				where m.payment_id = lpl.payment_id
				and m.invoice_id = MYINVOICE.INVOICE_ID
			);

			update ls_payment_hdr lpl
			set amount =
			(
				select sum(nvl(a.amount,0) + nvl(a.adjustment_amount,0)) /* WMD */
				from ls_payment_line a
				where a.payment_id = lpl.payment_id
            and a.set_of_books_id = 1
			)
			where exists
			(
				select 1
				from ls_invoice_payment_map m
				where m.payment_id = lpl.payment_id
				and m.invoice_id = MYINVOICE.INVOICE_ID
			)
			;
		end if;

      end if;
   end P_INVOICE_COMPARE;

   --**************************************************************************
   --                            P_PAYMENT_ROLLUP
   --**************************************************************************
   procedure P_PAYMENT_ROLLUP (A_LEASE_ID in number, A_COMPANY_ID in number, A_MONTH in date) is
      MYPAYMENTLINES PAYMENT_LINE_TYPE;
      LINESINDEX     number;
   begin
      select * bulk collect
        into MYPAYMENTLINES
        from LS_PAYMENT_LINE LP1
		where LP1.payment_id in
		(
			select a.payment_id
			from LS_PAYMENT_HDR a
			where a.GL_POSTING_MO_YR = A_MONTH
            and a.COMPANY_ID = A_COMPANY_ID
			and case when a_lease_id = -1 then -1 else a.lease_id end = A_LEASE_ID
		)
		and LP1.SET_OF_BOOKS_ID =
		(
			select min(SET_OF_BOOKS_ID)
			from LS_PAYMENT_LINE LP2
			where LP1.PAYMENT_ID = LP2.PAYMENT_ID
			and LP1.GL_POSTING_MO_YR = LP2.GL_POSTING_MO_YR
		)
      and LP1.PAYMENT_ID not in
      (
         select PAYMENT_ID
         from LS_PAYMENT_HDR
         where PAYMENT_STATUS_ID in (3, 6)
      );

		update LS_PAYMENT_HDR a
		set AMOUNT = 0
		where a.GL_POSTING_MO_YR = A_MONTH
		and a.COMPANY_ID = A_COMPANY_ID
		and case when a_lease_id = -1 then -1 else a.lease_id end = A_LEASE_ID
      and PAYMENT_STATUS_ID < 3;

      for LINESINDEX in 1 .. MYPAYMENTLINES.COUNT
      loop
         update LS_PAYMENT_HDR
            set AMOUNT = AMOUNT + NVL(MYPAYMENTLINES(LINESINDEX).AMOUNT,0) + NVL(MYPAYMENTLINES(LINESINDEX).ADJUSTMENT_AMOUNT,0) /* WMD */
          where PAYMENT_ID = MYPAYMENTLINES(LINESINDEX).PAYMENT_ID;
      end loop;
   end P_PAYMENT_ROLLUP;

   --**************************************************************************
   --                            F_GET_ILR_ID
   --**************************************************************************

   function F_GET_ILR_ID return number is

   begin
      return L_ILR_ID;
   end F_GET_ILR_ID;

   --**************************************************************************
   --                            F_TAX_CALC_ACCRUALS
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will stage the monthly tax amounts by ls_asset
   -- @@PARAMS
   --    date: a_month
   --       The month to process taxes for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************
        function F_TAX_CALC_ACCRUALS(A_COMPANY_ID in number,
								A_MONTH      in date, A_ACTUAL_CALC in number) return varchar2 is
      L_STATUS      varchar2(2000);
      TAX_TABLE ASSET_TAX_TABLE;
      L_INDEX number;
      counter number;
      SQLS varchar2(4000);
      BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
   begin
      L_STATUS:=F_TAX_CALC(-1, A_COMPANY_ID, A_MONTH, 1);
      RETURN L_STATUS;

   exception
      when others then
         return L_STATUS||' '||sqlerrm;
   end F_TAX_CALC_ACCRUALS;


    function F_TAX_CALC_ACCRUALS(A_COMPANY_ID in number,
								A_MONTH      in date) return varchar2 is
      L_STATUS      varchar2(2000);
	  L_RTN varchar2(2000);
	  L_TEMP_RTN varchar2(2000);
   begin
      L_STATUS := 'PROCESSING';
	  L_TEMP_RTN := F_TAX_CALC(-1, A_COMPANY_ID, A_MONTH, 1);
	 return L_TEMP_RTN;

   exception
      when others then
         return L_STATUS||' '||sqlerrm;
   end F_TAX_CALC_ACCRUALS;

	   --**************************************************************************
   --                            F_TAX_CALC_PAYMENTS
   --             --------------------------------
   -- @@ DESCRIPTION
   --    This function will stage the monthly tax amounts by ls_asset
   -- @@PARAMS
   --    date: a_month
   --       The month to process taxes for
   -- @@RETURN
   --    varchar2: A message back to the caller
   --       'OK' = SUCCESS
   --       all else = FAILURE
   --
   --**************************************************************************
   function F_TAX_CALC_PAYMENTS(A_LEASE_ID in number, A_COMPANY_ID in number,
								                A_MONTH      in date, A_ACTUAL_CALC in number) return varchar2 is
      L_STATUS      varchar2(2000);
      TAX_TABLE ASSET_TAX_TABLE;
      L_INDEX number;
      counter number;
      SQLS varchar2(4000);
      BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
   begin
     L_STATUS:=F_TAX_CALC(A_LEASE_ID, A_COMPANY_ID, A_MONTH, 0);
      return L_STATUS;
   exception
      when others then
         return L_STATUS||' '||sqlerrm;
   end F_TAX_CALC_PAYMENTS;

   function F_TAX_CALC_PAYMENTS(A_LEASE_ID in number, A_COMPANY_ID in number,
								A_MONTH      in date) return varchar2 is
      L_STATUS      varchar2(2000);
	  L_RTN varchar2(2000);
	  L_TEMP_RTN varchar2(2000);
   begin
      L_RTN:=F_TAX_CALC(A_LEASE_ID, A_COMPANY_ID, A_MONTH, 0);
	 return l_rtn;
   exception
      when others then
         return L_STATUS||' '||sqlerrm;
   end F_TAX_CALC_PAYMENTS;

   function F_LAM_CLOSED(  A_COMPANY_ID   in number,
						   A_MONTH     in date,
                           A_END_LOG in number:=null) return varchar2 is
      L_STATUS      varchar2(2000);
      L_RTN      number;
      L_GL_JE_CODE   varchar2(35);
      L_LOCATION    varchar2(2000);
	  L_STANDALONE  varchar2(100);
	  L_COMPANY		varchar2(100);
	  L_SQLS		varchar2(2000);
   begin
    PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
    PKG_PP_LOG.P_WRITE_MESSAGE('Starting Logs for Lock Lease Module Process. Company ID: ' || TO_CHAR(A_COMPANY_ID) || ' - ' || 'Month: ' || to_char(A_MONTH,'MON-YYYY'));

    --CJS 5/17/17 Adding System Control for toggling Lease Standalone Changes
	L_STANDALONE:=lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_company('Lease Standalone', A_COMPANY_ID)));

	IF nvl(L_STANDALONE, 'no') = 'yes' THEN
	-- KRD 1/30/17 - Adding code to roll forward CPR Control in the "Lock Lease" button for Lease stand alone implementations
	--               as well as other code in the CPR "Open Month" button
	--
	-- JSKim 02/19/17 - Adding in logic to insert into cr_open_month_number
	--                There doesn't really seem to be a reason to ever close the month so I'm just going to keep inserting new months
	--                Run a conversion script to get the first month open
		PKG_PP_LOG.P_WRITE_MESSAGE('Opening new month in CR Open Months');

		select upper(trim(replace(replace(replace(control_value,' ', '_'),'/','_'),'-','_')))
		into L_COMPANY
		from CR_SYSTEM_CONTROL
		where upper(trim(control_name)) = 'COMPANY FIELD';

		L_SQLS := 'INSERT INTO cr_open_month_number('||L_COMPANY||', source_id, month_number, status) ' ||
		  'SELECT gl_company_no, source_id, '||To_Char(a_month,'yyyymm')||', 1 AS status ' ||
		  'FROM company_setup, cr_sources WHERE company_id = '|| A_COMPANY_ID ||' ' ||
		  'AND (gl_company_no, source_id, '||To_Char(a_month,'yyyymm')||') NOT IN ' ||
		  '(SELECT '||L_COMPANY||', source_id, month_number FROM cr_open_month_number)';

		L_STATUS:=F_EXECUTE_IMMEDIATE(L_SQLS);
		IF L_STATUS <> 'OK' THEN
		  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		  RETURN L_STATUS;
		END IF;

		PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted to CR Open Months for Month: ' || Add_Months(A_MONTH, 1) || '.');

	-- end JSKim 02/19/17
		PKG_PP_LOG.P_WRITE_MESSAGE('Opening new month in CPR_CONTROL');

		INSERT INTO cpr_control (company_id, accounting_month)
		  SELECT A_COMPANY_ID AS company_id,
				 Add_Months(A_MONTH, 1) AS accounting_month
		  FROM dual
		  WHERE NOT EXISTS (SELECT 1
							FROM cpr_control
							WHERE company_id = A_COMPANY_ID
							AND   accounting_month = Add_Months(A_MONTH, 1)
						   );

		PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted to CPR_CONTROL for Company ID: ' || A_COMPANY_ID || ', Month: ' || Add_Months(A_MONTH, 1) || '.');

		INSERT INTO PP_INTERFACE_DATES (INTERFACE_ID, COMPANY_ID, ACCOUNTING_MONTH, LAST_RUN)
			SELECT (SELECT Nvl(Max(interface_id),999) FROM pp_interface WHERE subsystem = 'Asset Management') AS interface_id,
				  A_COMPANY_ID AS company_id,
				  Add_Months(A_MONTH, 1) AS accounting_month,
				  NULL AS last_run
			 FROM dual
			 WHERE NOT EXISTS (SELECT 1
							 FROM pp_interface_dates
							 WHERE interface_id = (SELECT Nvl(Max(interface_id),999) FROM pp_interface WHERE subsystem = 'Asset Management')
							 AND company_id = A_COMPANY_ID
							 AND accounting_month = Add_Months(A_MONTH, 1)
							);
		PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted to PP_INTERFACE_DATES.');

		INSERT INTO depr_ledger (depr_group_id, set_of_books_id, gl_post_mo_yr, depr_ledger_status)
		  SELECT dl.depr_group_id,
				dl.set_of_books_id,
				Add_Months(A_MONTH, 1) AS gl_post_mo_yr,
				9
		  FROM depr_ledger dl,
			  depr_group dg
		  WHERE dl.depr_group_id = dg.depr_group_id
		  AND   dg.company_id = A_COMPANY_ID
		  AND   dl.gl_post_mo_yr = A_MONTH
		  AND NOT EXISTS (SELECT 1
						  FROM depr_ledger
						  WHERE depr_group_id = dg.depr_group_id
						  AND   gl_post_mo_yr = Add_Months(A_MONTH, 1)
						);

		PKG_PP_LOG.P_WRITE_MESSAGE('Added new month to the Depreciation Ledger for ' || SQL%ROWCOUNT || ' Depr Groups.');

		INSERT INTO depr_net_salvage_amort (
												set_of_books_id, depr_group_id, vintage, gl_post_mo_yr,
												cor_treatment, salvage_treatment, net_salvage_amort_life,
												cost_of_removal_bal, cost_of_removal_reserve, salvage_bal, salvage_reserve
										  )
		  SELECT set_of_books_id, depr_net_salvage_amort.depr_group_id, vintage, Add_Months(A_MONTH, 1),
			  depr_net_salvage_amort.cor_treatment, depr_net_salvage_amort.salvage_treatment, depr_net_salvage_amort.net_salvage_amort_life,
			  cost_of_removal_bal, cost_of_removal_reserve, salvage_bal, salvage_reserve
		  FROM depr_net_salvage_amort, depr_group
		  WHERE depr_net_salvage_amort.depr_group_id = depr_group.depr_group_id
		  AND company_id = A_COMPANY_ID
		  AND gl_post_mo_yr = A_MONTH
		  AND NOT EXISTS (
							SELECT 1 FROM depr_net_salvage_amort d
							WHERE d.set_of_books_id = depr_net_salvage_amort.set_of_books_id
								AND d.depr_group_id = depr_net_salvage_amort.depr_group_id
								AND d.gl_post_mo_yr = Add_Months(A_MONTH, 1)
								AND d.vintage = depr_net_salvage_amort.vintage
					  );

		PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted for new month for Depr Net Salvage Amort');

			DELETE FROM depr_res_allo_factors df
			WHERE df.depr_group_id IN
			(
				SELECT dg.depr_group_id
				FROM depr_group dg
				WHERE dg.company_id = A_COMPANY_ID
			)
			AND MONTH = Add_Months(A_MONTH, 1);

		PKG_PP_LOG.P_WRITE_MESSAGE('Cleared ' || SQL%ROWCOUNT || ' rows in Depr Res Allo Factors for the new month');

		INSERT INTO DEPR_RES_ALLO_FACTORS (SET_OF_BOOKS_ID, DEPR_GROUP_ID, VINTAGE, MONTH, FACTOR, THEO_FACTOR,
											REMAINING_LIFE, LIFE_FACTOR, COR_FACTOR)
			SELECT SET_OF_BOOKS_ID, DEPR_RES_ALLO_FACTORS.DEPR_GROUP_ID, VINTAGE, Add_Months(A_MONTH, 1),
					NVL(FACTOR,0) , NVL(THEO_FACTOR,0), remaining_life, life_factor, cor_factor
			FROM DEPR_RES_ALLO_FACTORS, DEPR_GROUP
			WHERE DEPR_RES_ALLO_FACTORS.DEPR_GROUP_ID = DEPR_GROUP.DEPR_GROUP_ID
		  AND COMPANY_ID = A_COMPANY_ID
			AND MONTH = A_MONTH;

		PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted to Depr Res Allo Factors for the new month.');

		DELETE FROM depr_vintage_summary df
			WHERE df.depr_group_id IN
			(
				SELECT dg.depr_group_id
				FROM depr_group dg
				WHERE dg.company_id = A_COMPANY_ID
			)
			AND accounting_month = Add_Months(A_MONTH, 1);

		PKG_PP_LOG.P_WRITE_MESSAGE('Cleared ' || SQL%ROWCOUNT || ' rows in Depr Vintage Summary for the new month');

		INSERT INTO DEPR_VINTAGE_SUMMARY (SET_OF_BOOKS_ID, DEPR_GROUP_ID, VINTAGE, ACCOUNTING_MONTH, ACCUM_COST, combined_depr_group_id)
			SELECT SET_OF_BOOKS_ID, DEPR_VINTAGE_SUMMARY.DEPR_GROUP_ID, VINTAGE, Add_Months(A_MONTH, 1), NVL(ACCUM_COST,0),
				depr_vintage_summary.combined_depr_group_id
			FROM DEPR_VINTAGE_SUMMARY, DEPR_GROUP
			WHERE DEPR_VINTAGE_SUMMARY.DEPR_GROUP_ID = DEPR_GROUP.DEPR_GROUP_ID
		  AND COMPANY_ID = A_COMPANY_ID
			AND ACCOUNTING_MONTH = A_MONTH;

		PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted to Depr Vintage Summary for the new month.');

		DELETE FROM account_summary
			WHERE company_id = A_COMPANY_ID
		  AND gl_posting_mo_yr = Add_Months(A_MONTH, 1);

		PKG_PP_LOG.P_WRITE_MESSAGE('Cleared ' || SQL%ROWCOUNT || ' rows in Account Summary for the new month');

		INSERT INTO account_summary (company_id, gl_account_id, bus_segment_id, sub_account_id,
									 utility_account_id, major_location_id, set_of_books_id, gl_posting_mo_yr,
									   acct_summ_status, beginning_balance, additions, retirements, transfers_in,
									   transfers_out, adjustments, ending_balance
									)
			SELECT company_id, gl_account_id, bus_segment_id, sub_account_id,
				  utility_account_id, major_location_id, set_of_books_id,
				  Add_Months(A_MONTH, 1) , 1 acct_summ_status,
				  ending_balance beginning_balance, 0 additions, 0 retirements,
				  0 transfers_in, 0 transfers_out, 0 adjustments, 0 ending_balance
			  FROM account_summary
			WHERE company_id = A_COMPANY_ID
			AND gl_posting_mo_yr = A_MONTH;

		PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted to Account Summary for the new month.');

		PKG_PP_LOG.P_WRITE_MESSAGE('Setting ending balances on CPR Depr for the current month');

	-- update cpr depr for current month
		UPDATE cpr_depr
		SET depr_reserve = nvl(beg_reserve_month,0) + nvl(retirements,0) + nvl(depr_exp_alloc_adjust,0) + nvl(depr_exp_adjust,0) + nvl(salvage_dollars,0) +
						  Nvl(cost_of_removal,0) + nvl(other_credits_and_adjust,0) + nvl(gain_loss,0) + nvl(reserve_trans_in,0) +
						  Nvl(reserve_trans_out,0) + nvl(curr_depr_expense,0) + nvl(reserve_adjustment,0) +nvl(impairment_asset_amount,0) +
						  Nvl(impairment_expense_amount,0),
			asset_dollars = Nvl(beg_asset_dollars,0) + nvl(net_adds_and_adjust,0) + nvl(retirements,0) + nvl(transfers_in,0) + nvl(transfers_out,0)
									  + Nvl(impairment_asset_amount,0)
		WHERE gl_posting_mo_yr = A_MONTH
		AND company_id = A_COMPANY_ID;

		PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows updated Plant and Reserve totals.');

		UPDATE cpr_depr a
		SET (ytd_depr_expense, ytd_depr_exp_adjust ) =
				(SELECT Decode(To_Number(to_char(b.gl_posting_mo_yr,'MM')),12,0, nvl(b.ytd_depr_expense,0)) + nvl(a.curr_depr_expense,0),
					  decode(to_number(to_char(b.gl_posting_mo_yr,'MM')),12,0, nvl(b.ytd_depr_exp_adjust,0)) + nvl(a.depr_exp_adjust,0) + nvl(a.depr_exp_alloc_adjust,0)
				FROM cpr_depr b
				WHERE a.asset_id = b.asset_id
				AND a.set_of_books_id = b.set_of_books_id
				AND b.gl_posting_mo_yr = add_months(a.gl_posting_mo_yr,-1)
				)
		WHERE company_id = A_COMPANY_ID
		AND a.gl_posting_mo_yr = A_MONTH
		AND EXISTS (SELECT 1 FROM cpr_depr z
							WHERE a.asset_id = z.asset_id
							AND a.set_of_books_id  = z.set_of_books_id
							AND z.gl_posting_mo_yr = Add_Months(a.gl_posting_mo_yr,-1)
				  );

		PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows updated year to date expense totals for existing assets.');

		UPDATE cpr_depr a
		SET ytd_depr_expense = nvl(curr_depr_expense,0),
			ytd_depr_exp_adjust = nvl(depr_exp_adjust,0) + nvl(depr_exp_alloc_adjust,0)
		WHERE company_id = A_COMPANY_ID
		AND a.gl_posting_mo_yr = A_MONTH
		AND NOT EXISTS (SELECT 1
						FROM cpr_depr z
								WHERE a.asset_id = z.asset_id
								AND a.set_of_books_id  = z.set_of_books_id
								AND z.gl_posting_mo_yr = Add_Months(a.gl_posting_mo_yr,-1)
					);

		PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows updated year to date expense totals for new assets.');

	-- insert to cpr depr for new month
		INSERT INTO cpr_depr (asset_id, set_of_books_id, gl_posting_mo_yr, init_life, remaining_life, estimated_salvage, beg_asset_dollars,
								  beg_reserve_month, acct_distrib, company_id, ytd_depr_expense, ytd_depr_exp_adjust,
										prior_ytd_depr_expense, prior_ytd_depr_exp_adjust, beg_reserve_year, mid_period_method, mid_period_conv,
										depr_group_id
							)
				(
			SELECT a.asset_id, set_of_books_id, Add_Months(A_MONTH, 1), init_life, decode(remaining_life,0,0,remaining_life - 1),
					estimated_salvage, asset_dollars, depr_reserve, acct_distrib, a.company_id, ytd_depr_expense, ytd_depr_exp_adjust,
							  prior_ytd_depr_expense, prior_ytd_depr_exp_adjust, beg_reserve_year, mid_period_method, mid_period_conv, a.depr_group_id
				FROM cpr_depr a, cpr_ledger l
				WHERE a.gl_posting_mo_yr = A_MONTH
			  AND NOT ((asset_dollars = 0 and depr_reserve = 0))
			  AND a.company_id = A_COMPANY_ID
				AND a.asset_id = l.asset_id
				AND l.subledger_indicator = -100
				AND NOT EXISTS
								  (SELECT 1
							FROM cpr_depr z
									WHERE z.asset_id=a.asset_id
									AND z.set_of_books_id=a.set_of_books_id
									AND z.gl_posting_mo_yr=Add_Months(A_MONTH, 1)
								  )
				);

		PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted to the CPR Depreciation table for the new month.');

	-- update cpr depr for the new month
		UPDATE cpr_depr
		SET depr_reserve = nvl(beg_reserve_month,0) + nvl(retirements,0) + nvl(depr_exp_alloc_adjust,0) + nvl(depr_exp_adjust,0) + nvl(salvage_dollars,0) +
						  Nvl(cost_of_removal,0) + nvl(other_credits_and_adjust,0) + nvl(gain_loss,0) + nvl(reserve_trans_in,0) +
						  Nvl(reserve_trans_out,0) + nvl(curr_depr_expense,0) + nvl(reserve_adjustment,0) +nvl(impairment_asset_amount,0) +
						  Nvl(impairment_expense_amount,0),
			asset_dollars = Nvl(beg_asset_dollars,0) + nvl(net_adds_and_adjust,0) + nvl(retirements,0) + nvl(transfers_in,0) + nvl(transfers_out,0)
									  + Nvl(impairment_asset_amount,0)
		WHERE gl_posting_mo_yr = Add_Months(A_MONTH, 1)
		AND company_id = A_COMPANY_ID;

		UPDATE cpr_depr a
		SET (ytd_depr_expense, ytd_depr_exp_adjust ) =
				(SELECT Decode(To_Number(to_char(b.gl_posting_mo_yr,'MM')),12,0, nvl(b.ytd_depr_expense,0)) + nvl(a.curr_depr_expense,0),
					  decode(to_number(to_char(b.gl_posting_mo_yr,'MM')),12,0, nvl(b.ytd_depr_exp_adjust,0)) + nvl(a.depr_exp_adjust,0) + nvl(a.depr_exp_alloc_adjust,0)
				FROM cpr_depr b
				WHERE a.asset_id = b.asset_id
				AND a.set_of_books_id = b.set_of_books_id
				AND b.gl_posting_mo_yr = add_months(a.gl_posting_mo_yr,-1)
				)
		WHERE company_id = A_COMPANY_ID
		AND a.gl_posting_mo_yr = Add_Months(A_MONTH, 1)
		AND EXISTS (SELECT 1 FROM cpr_depr z
							WHERE a.asset_id = z.asset_id
							AND a.set_of_books_id  = z.set_of_books_id
							AND z.gl_posting_mo_yr = Add_Months(a.gl_posting_mo_yr,-1)
				  );

		UPDATE cpr_depr a
		SET ytd_depr_expense = nvl(curr_depr_expense,0),
			ytd_depr_exp_adjust = nvl(depr_exp_adjust,0) + nvl(depr_exp_alloc_adjust,0)
		WHERE company_id = A_COMPANY_ID
		AND a.gl_posting_mo_yr = Add_Months(A_MONTH, 1)
		AND NOT EXISTS (SELECT 1
						FROM cpr_depr z
								WHERE a.asset_id = z.asset_id
								AND a.set_of_books_id  = z.set_of_books_id
								AND z.gl_posting_mo_yr = Add_Months(a.gl_posting_mo_yr,-1)
					  );

		PKG_PP_LOG.P_WRITE_MESSAGE('Rolled forward CPR Depr for ' || SQL%ROWCOUNT || ' assets.');

		PKG_PP_LOG.P_WRITE_MESSAGE('Successfully opened new CPR month.');

	-- END KRD 1/30/17
	END IF; --CJS 5/17/17 L_STANDALONE


     --verify that the next month is open on the CPR.
     select count(*)
     into L_RTN
     from CPR_CONTROL
     where COMPANY_ID = A_COMPANY_ID
     and ACCOUNTING_MONTH = add_months(A_MONTH, 1);

     if L_RTN = 0 then
      PKG_PP_LOG.P_END_LOG;
      return 'The CPR must be open for the next month in order to close the Lease module.';
     end if;

     L_LOCATION:='Checking to make sure there are no pending transactions for prior period retirements';
     PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION);
     SELECT Count(1)
     INTO L_RTN
     FROM pend_transaction
     WHERE subledger_indicator = -100
      AND in_service_year < A_MONTH
      AND COMPANY_ID = A_COMPANY_ID;

     IF L_RTN > 0 THEN
      L_LOCATION:='ERROR: Cannot lock the month because there are pending transactions from the prior period that need to be posted';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION);
      PKG_PP_LOG.P_END_LOG;
      RETURN L_LOCATION;
     END IF;
     --Verify that all our Lease DGs are there for the next month
     PKG_PP_LOG.P_WRITE_MESSAGE('Checking that depreciation groups are rolled forward for the next month');

     select count(1)
     into L_RTN
     from
     (
      select A.DEPR_GROUP_ID
      from DEPR_LEDGER A, DEPR_GROUP B
      where A.GL_POST_MO_YR = A_MONTH
      and A.DEPR_GROUP_ID = B.DEPR_GROUP_ID
      and B.SUBLEDGER_TYPE_ID = -100
      and B.COMPANY_ID = A_COMPANY_ID
      minus
      select A.DEPR_GROUP_ID
      from DEPR_LEDGER A, DEPR_GROUP B
      where A.GL_POST_MO_YR = add_months(A_MONTH,1)
      and A.DEPR_GROUP_ID = B.DEPR_GROUP_ID
      and B.SUBLEDGER_TYPE_ID = -100
      and B.COMPANY_ID = A_COMPANY_ID
     );

     if L_RTN > 0 then
	  PKG_PP_LOG.P_END_LOG;
      return 'There are '||to_char(L_RTN)||' lease depreciation groups for this month that do not have rows in DEPR_LEDGER for next month. Please confirm that the next month is open on the CPR.';
     end if;

    select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LAMRECLASS'))
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
      and G.PROCESS_ID = 'LAMRECLASS';

	  pkg_pp_log.p_write_message('Writing Reclass JE''s');
      FOR l_reclass IN (SELECT  la.ls_asset_id AS ls_asset_id,
                                round((las.beg_lt_obligation - las.end_lt_obligation) / las.rate, 2) AS delta_lt_oblig, --mc_bookje reconverts based on rate
                                (LAS.BEG_OBLIGATION - LAS.BEG_LT_OBLIGATION) - (LAS.END_OBLIGATION - LAS.END_LT_OBLIGATION) as DELTA_ST_OBLIG,
                                LAS.SET_OF_BOOKS_ID as SOB_ID,
                                IA.ST_OBLIG_ACCOUNT_ID as ST_OBLIG_ACCOUNT_ID,
                                ia.lt_oblig_account_id AS lt_oblig_account_id,
                                las.rate,
                                las.contract_currency_id,
                                las.currency_id as company_currency_id
                        from LS_ASSET LA, V_LS_ASSET_SCHEDULE_FX_VW LAS, LS_ILR_ACCOUNT IA
                        where LA.COMPANY_ID = A_COMPANY_ID
                        and LAS.MONTH = A_MONTH
                        and LAS.REVISION = LA.APPROVED_REVISION
                        and LAS.LS_ASSET_ID = LA.LS_ASSET_ID
                        AND las.beg_lt_obligation - las.end_lt_obligation <> 0
                        AND las.ls_cur_type = 2
                        and (LA.LS_ASSET_STATUS_ID = 3
                        /* CJS 4/21/15 *//*or (LA.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= LA.RETIREMENT_DATE)*/
                        )
                        and LA.ILR_ID = IA.ILR_ID)
      loop
         l_location := 'Writing Reclass Debit JEs for ls_asset_id: ' || to_char(l_reclass.ls_asset_id);
         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_RECLASS.LS_ASSET_ID,
                                               3023,
                                               L_RECLASS.DELTA_LT_OBLIG,
                                               0,
                                               -1,
                                               null,
                                               L_RECLASS.LT_OBLIG_ACCOUNT_ID, --put account here in the future. Should be LT account from LS_ILR_ACCOUNT
                                               0,
                                               -1,
                                               A_COMPANY_ID,
                                               A_month,
                                               1,
                                               L_GL_JE_CODE,
                                               l_reclass.sob_id,
                                               l_reclass.rate,
                                               l_reclass.contract_currency_id,
                                               l_reclass.company_currency_id,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_LOCATION || ' : ' || L_STATUS;
         end if;

         -- process the credit (1 more than dr trans type
         l_location := 'Writing Reclass Credit JEs for ls_asset_id: ' || to_char(l_reclass.ls_asset_id);
         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_RECLASS.LS_ASSET_ID,
                                               3024,
                                               L_RECLASS.DELTA_LT_OBLIG,
                                               0,
                                               -1,
                                               null,
                                               L_RECLASS.ST_OBLIG_ACCOUNT_ID, --put account here in the future. Should be ST account from LS_ILR_ACCOUNT
                                               0,
                                               -1,
                                               A_COMPANY_ID,
                                               A_month,
                                               0,
                                               L_GL_JE_CODE,
                                               l_reclass.sob_id,
                                               l_reclass.rate,
                                               l_reclass.contract_currency_id,
                                               l_reclass.company_currency_id,
                                               L_STATUS);
         if L_RTN = -1 then
            return L_LOCATION || ' : ' || L_STATUS;
         end if;
      end loop;

   PKG_PP_LOG.P_WRITE_MESSAGE('Success! Company ID: ' || TO_CHAR(A_COMPANY_ID));

   IF A_END_LOG = 1 then
     PKG_PP_LOG.P_END_LOG();
   END IF;

   return 'OK';
   exception
      when others then
		 PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS||' '||sqlerrm);
		 PKG_PP_LOG.P_END_LOG();
         return L_STATUS||' '||sqlerrm;
   end F_LAM_CLOSED;

   function F_DEPR_APPROVE( A_COMPANY_ID in number,
                            A_MONTH in date,
                            A_END_LOG in number:=null) return varchar2 is
      L_STATUS      varchar2(2000);
      L_RTN      number;
      L_GL_JE_CODE   varchar2(35);
      L_LOCATION     varchar2(2000);
      v_row          PEND_BASIS%ROWTYPE;
   begin
    PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
    PKG_PP_LOG.P_WRITE_MESSAGE('Starting Logs for Depreciation JE Process. Company ID: ' || TO_CHAR(A_COMPANY_ID) || ' - ' || 'Month: ' || to_char(A_MONTH,'MON-YYYY'));

      select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LAMDEPR'))
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
      and G.PROCESS_ID = 'LAMDEPR';

      PKG_PP_LOG.P_WRITE_MESSAGE('Generating depreciation journals');
      for L_DEPR in (select  LCAM.LS_ASSET_ID as LS_ASSET_ID,
                        CD.SET_OF_BOOKS_ID as SOB_ID,
                        CD.CURR_DEPR_EXPENSE + CD.DEPR_EXP_ADJUST
                        /* CJS 3/7/17 Add system control to account for conversion true-up amounts */
                          + Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Disable Lease Depr True-up Journals', LA.COMPANY_ID))), 'yes', 0, CD.DEPR_EXP_ALLOC_ADJUST) as DEPR_EXP,
                        CD.DEPR_GROUP_ID,
                        DG.RESERVE_ACCT_ID as RESERVE_ACCT_ID,
                        DG.EXPENSE_ACCT_ID as EXPENSE_ACCT_ID
                    from CPR_DEPR CD, CPR_LEDGER CL, LS_CPR_ASSET_MAP LCAM, LS_ASSET LA, DEPR_GROUP DG
                   where CD.ASSET_ID = CL.ASSET_ID
                     and CL.SUBLEDGER_INDICATOR = -100
                     and LCAM.ASSET_ID = CL.ASSET_ID
                     and (CD.CURR_DEPR_EXPENSE + CD.DEPR_EXP_ADJUST
                     + Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Disable Lease Depr True-up Journals', LA.COMPANY_ID))), 'yes', 0, CD.DEPR_EXP_ALLOC_ADJUST)) <> 0
                     and LA.LS_ASSET_ID = LCAM.LS_ASSET_ID
                     and LA.COMPANY_ID = A_COMPANY_ID
                     and CD.GL_POSTING_MO_YR = A_MONTH
                     AND CL.DEPR_GROUP_ID = DG.DEPR_GROUP_ID)
      loop
         L_LOCATION := 'Writing depreciation JEs for ls_asset_id: ' || TO_CHAR(L_DEPR.LS_ASSET_ID);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_DEPR.LS_ASSET_ID,
                                               3032,
                                               L_DEPR.DEPR_EXP,
                                               0,
                                               -1,
                                               null,
                                               L_DEPR.EXPENSE_ACCT_ID, --is this the right account?
                                               0,
                                               -1,
                                               A_COMPANY_ID,
                                               A_month,
                                               1,
                                               L_GL_JE_CODE,
                                               L_DEPR.SOB_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_LOCATION || ' : ' || L_STATUS;
         end if;

         -- process the credit (1 more than dr trans type
         L_LOCATION := 'Writing depreciation JEs for ls_asset_id: ' || TO_CHAR(L_DEPR.LS_ASSET_ID);
         L_RTN    := PKG_LEASE_COMMON.F_BOOKJE(L_DEPR.LS_ASSET_ID,
                                               3033,
                                               L_DEPR.DEPR_EXP,
                                               0,
                                               -1,
                                               null,
                                               L_DEPR.RESERVE_ACCT_ID, --what account goes here?
                                               0,
                                               -1,
                                               A_COMPANY_ID,
                                               A_month,
                                               0,
                                               L_GL_JE_CODE,
                                               L_DEPR.SOB_ID,
                                               L_STATUS);
         if L_RTN = -1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_LOCATION || ' : ' || L_STATUS);
            PKG_PP_LOG.P_END_LOG();
            return L_STATUS;
         end if;
      end loop;


	PKG_PP_LOG.P_WRITE_MESSAGE('Depreciation JE Process Complete for Company ID: ' || TO_CHAR(A_COMPANY_ID));


/* WMD This is kind of ghetto but we're higjacking the pend basis trigger to reset the reserve amount on pending transactions that were staged before depreciation was calculated */
update ls_process_control
set depr_approved = sysdate
where company_id = A_COMPANY_ID
  and gl_posting_mo_yr = A_MONTH;

for i in (select pend_trans_id
          from pend_transaction
          where subledger_indicator = -100
            and trim(activity_code) = 'URGL'
            and gl_posting_mo_yr = A_MONTH
            and company_id = A_COMPANY_ID)
loop

    update pend_transaction
    set posting_status =1
    where pend_trans_id = i.pend_trans_id;

    select * into v_row
    from pend_basis
    where pend_trans_id = i.pend_trans_id;
    delete from pend_transaction_set_of_books where pend_trans_id = i.pend_trans_id;
    delete from pend_basis where pend_trans_id = i.pend_trans_id;
    insert into pend_basis
    (PEND_TRANS_ID, TIME_STAMP, USER_ID, BASIS_1, GL_ACCT_1, BASIS_2, GL_ACCT_2, BASIS_3, GL_ACCT_3, BASIS_4, GL_ACCT_4, BASIS_5, GL_ACCT_5, BASIS_6, GL_ACCT_6, BASIS_7, GL_ACCT_7, BASIS_8, GL_ACCT_8, BASIS_9, GL_ACCT_9, BASIS_10, GL_ACCT_10, BASIS_11, GL_ACCT_11, BASIS_12, GL_ACCT_12, BASIS_13, GL_ACCT_13, BASIS_14, GL_ACCT_14, BASIS_15, GL_ACCT_15, BASIS_16, GL_ACCT_16, BASIS_17, GL_ACCT_17, BASIS_18, GL_ACCT_18, BASIS_19, GL_ACCT_19, BASIS_20, GL_ACCT_20, BASIS_21, GL_ACCT_21, BASIS_22, GL_ACCT_22, BASIS_23, GL_ACCT_23, BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32, BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41, BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50, BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60, BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68, BASIS_69, BASIS_70, GL_ACCT_24, GL_ACCT_25, GL_ACCT_26, GL_ACCT_27, GL_ACCT_28, GL_ACCT_29, GL_ACCT_30, GL_ACCT_31, GL_ACCT_32, GL_ACCT_33, GL_ACCT_34, GL_ACCT_35, GL_ACCT_36, GL_ACCT_37, GL_ACCT_38, GL_ACCT_39, GL_ACCT_40, GL_ACCT_41, GL_ACCT_42, GL_ACCT_43, GL_ACCT_44, GL_ACCT_45, GL_ACCT_46, GL_ACCT_47, GL_ACCT_48, GL_ACCT_49, GL_ACCT_50, GL_ACCT_51, GL_ACCT_52, GL_ACCT_53, GL_ACCT_54, GL_ACCT_55, GL_ACCT_56, GL_ACCT_57, GL_ACCT_58, GL_ACCT_59, GL_ACCT_60, GL_ACCT_61, GL_ACCT_62, GL_ACCT_63, GL_ACCT_64, GL_ACCT_65, GL_ACCT_66, GL_ACCT_67, GL_ACCT_68, GL_ACCT_69, GL_ACCT_70)
    select
    v_row.PEND_TRANS_ID,
    v_row.TIME_STAMP,
    v_row.USER_ID,
    v_row.BASIS_1,
    v_row.GL_ACCT_1,
    v_row.BASIS_2,
    v_row.GL_ACCT_2,
    v_row.BASIS_3,
    v_row.GL_ACCT_3,
    v_row.BASIS_4,
    v_row.GL_ACCT_4,
    v_row.BASIS_5,
    v_row.GL_ACCT_5,
    v_row.BASIS_6,
    v_row.GL_ACCT_6,
    v_row.BASIS_7,
    v_row.GL_ACCT_7,
    v_row.BASIS_8,
    v_row.GL_ACCT_8,
    v_row.BASIS_9,
    v_row.GL_ACCT_9,
    v_row.BASIS_10,
    v_row.GL_ACCT_10,
    v_row.BASIS_11,
    v_row.GL_ACCT_11,
    v_row.BASIS_12,
    v_row.GL_ACCT_12,
    v_row.BASIS_13,
    v_row.GL_ACCT_13,
    v_row.BASIS_14,
    v_row.GL_ACCT_14,
    v_row.BASIS_15,
    v_row.GL_ACCT_15,
    v_row.BASIS_16,
    v_row.GL_ACCT_16,
    v_row.BASIS_17,
    v_row.GL_ACCT_17,
    v_row.BASIS_18,
    v_row.GL_ACCT_18,
    v_row.BASIS_19,
    v_row.GL_ACCT_19,
    v_row.BASIS_20,
    v_row.GL_ACCT_20,
    v_row.BASIS_21,
    v_row.GL_ACCT_21,
    v_row.BASIS_22,
    v_row.GL_ACCT_22,
    v_row.BASIS_23,
    v_row.GL_ACCT_23,
    v_row.BASIS_24,
    v_row.BASIS_25,
    v_row.BASIS_26,
    v_row.BASIS_27,
    v_row.BASIS_28,
    v_row.BASIS_29,
    v_row.BASIS_30,
    v_row.BASIS_31,
    v_row.BASIS_32,
    v_row.BASIS_33,
    v_row.BASIS_34,
    v_row.BASIS_35,
    v_row.BASIS_36,
    v_row.BASIS_37,
    v_row.BASIS_38,
    v_row.BASIS_39,
    v_row.BASIS_40,
    v_row.BASIS_41,
    v_row.BASIS_42,
    v_row.BASIS_43,
    v_row.BASIS_44,
    v_row.BASIS_45,
    v_row.BASIS_46,
    v_row.BASIS_47,
    v_row.BASIS_48,
    v_row.BASIS_49,
    v_row.BASIS_50,
    v_row.BASIS_51,
    v_row.BASIS_52,
    v_row.BASIS_53,
    v_row.BASIS_54,
    v_row.BASIS_55,
    v_row.BASIS_56,
    v_row.BASIS_57,
    v_row.BASIS_58,
    v_row.BASIS_59,
    v_row.BASIS_60,
    v_row.BASIS_61,
    v_row.BASIS_62,
    v_row.BASIS_63,
    v_row.BASIS_64,
    v_row.BASIS_65,
    v_row.BASIS_66,
    v_row.BASIS_67,
    v_row.BASIS_68,
    v_row.BASIS_69,
    v_row.BASIS_70,
    v_row.GL_ACCT_24,
    v_row.GL_ACCT_25,
    v_row.GL_ACCT_26,
    v_row.GL_ACCT_27,
    v_row.GL_ACCT_28,
    v_row.GL_ACCT_29,
    v_row.GL_ACCT_30,
    v_row.GL_ACCT_31,
    v_row.GL_ACCT_32,
    v_row.GL_ACCT_33,
    v_row.GL_ACCT_34,
    v_row.GL_ACCT_35,
    v_row.GL_ACCT_36,
    v_row.GL_ACCT_37,
    v_row.GL_ACCT_38,
    v_row.GL_ACCT_39,
    v_row.GL_ACCT_40,
    v_row.GL_ACCT_41,
    v_row.GL_ACCT_42,
    v_row.GL_ACCT_43,
    v_row.GL_ACCT_44,
    v_row.GL_ACCT_45,
    v_row.GL_ACCT_46,
    v_row.GL_ACCT_47,
    v_row.GL_ACCT_48,
    v_row.GL_ACCT_49,
    v_row.GL_ACCT_50,
    v_row.GL_ACCT_51,
    v_row.GL_ACCT_52,
    v_row.GL_ACCT_53,
    v_row.GL_ACCT_54,
    v_row.GL_ACCT_55,
    v_row.GL_ACCT_56,
    v_row.GL_ACCT_57,
    v_row.GL_ACCT_58,
    v_row.GL_ACCT_59,
    v_row.GL_ACCT_60,
    v_row.GL_ACCT_61,
    v_row.GL_ACCT_62,
    v_row.GL_ACCT_63,
    v_row.GL_ACCT_64,
    v_row.GL_ACCT_65,
    v_row.GL_ACCT_66,
    v_row.GL_ACCT_67,
    v_row.GL_ACCT_68,
    v_row.GL_ACCT_69,
    v_row.GL_ACCT_70
  From dual;
end loop;

/* THIS IS DONE LATER SO I DON'T WANT TO SET IT TOO EARLY */
update ls_process_control
set depr_approved = NULL
where company_id = A_COMPANY_ID
  and gl_posting_mo_yr = A_MONTH;
	PKG_PP_LOG.P_WRITE_MESSAGE('Depreciation JE Process Complete for Company ID: ' || TO_CHAR(A_COMPANY_ID));

    if A_END_LOG = 1 then
      PKG_PP_LOG.P_END_LOG();
    end if;
   return 'OK';
   exception
      when others then
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS||' '||sqlerrm);
         return L_STATUS||' '||sqlerrm;
   end F_DEPR_APPROVE;



   function F_PROCESS_RESIDUAL( A_LS_ASSET_IDS in NUM_ARRAY, A_END_LOG in number) return varchar2 is
      L_STATUS    varchar2(2000);
      L_RTN       number;
      L_ERR       number;
      L_RETURN    varchar2(2000);
      L_ID_ARRAY     T_NUM_ARRAY;
      L_GL_JE_CODE   varchar2(35);
	  L_LOCATION    varchar2(2000);
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('Processing residual for ' || A_LS_ASSET_IDS.COUNT || ' assets.');

      L_ERR := 0;

      -- Use this "SQL Type" array so that we can use it in cursor query
      -- Had to use PLSQL type array in argument so we can connect with uo_sqlca
      L_ID_ARRAY := T_NUM_ARRAY();
      for I in 1..A_LS_ASSET_IDS.COUNT
      loop
         L_ID_ARRAY.extend;
         L_ID_ARRAY(I) := A_LS_ASSET_IDS(I);
      end loop;

      select NVL(E.EXTERNAL_JE_CODE, NVL(E.GL_JE_CODE, 'LEASERESIDUAL'))
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
      and G.PROCESS_ID = 'LEASERESIDUAL';

      for REC in (WITH cr AS (
					  SELECT /*+ materialize */ exchange_date, currency_from, currency_to, rate
					  FROM currency_rate_default a
					  WHERE exchange_date = (select max(exchange_date)
											  from currency_rate_default b
											  where to_char(a.exchange_date, 'yyyymm') = to_char(b.exchange_date, 'yyyymm')
											  and a.currency_from = b.currency_from and a.currency_to = b.currency_to)
					)
					SELECT ls_asset.ls_asset_id,
						ls_asset.guaranteed_residual_amount - ls_asset.actual_residual_amount AS je_amount,
                        cr.rate AS rate,
                        cr.currency_from AS currency_from,
                        cr.currency_to as currency_to,
						ls_asset.company_id,
						set_of_books.set_of_books_id,
						month_view.MONTH,
						ls_ilr_account.res_debit_account_id,
						ls_ilr_account.res_credit_account_id,
						ls_ilr.ilr_number
					FROM ls_asset,
						set_of_books,
						ls_ilr_account,
						ls_ilr,
						(
							SELECT company_id, Min(gl_posting_mo_yr) AS MONTH
							FROM ls_process_control
							WHERE lam_closed IS NULL
							OR (lam_closed IS NOT NULL AND open_next IS NULL)
							GROUP BY company_id
						) month_view,
						currency_schema,
						cr
					where LS_ASSET_ID in (select COLUMN_VALUE from table(L_ID_ARRAY))
					AND ls_ilr_account.ilr_id(+) = ls_ilr.ilr_id
					AND ls_ilr.ilr_id = ls_asset.ilr_id
					AND month_view.company_id = ls_asset.company_id
					AND ls_ilr.company_id = currency_schema.company_id
					AND currency_schema.currency_id = cr.currency_to
					AND ls_asset.contract_currency_id = cr.currency_from
					AND currency_schema.currency_type_id = 1
					AND cr.exchange_date = (
						SELECT Max(exchange_date)
						FROM cr cr2
						WHERE cr.currency_from = cr2.currency_from
						AND cr.currency_to = cr2.currency_to
						AND cr2.exchange_date < Add_Months(month_view.MONTH, 1)
					)
					ORDER BY ls_asset.ls_asset_id, set_of_books.set_of_books_id)
      loop
         PKG_PP_LOG.P_WRITE_MESSAGE('Processing Residual for Asset: ' || to_char(REC.LS_ASSET_ID) || ', Set of Books: ' || to_char(REC.SET_OF_BOOKS_ID));

         -- Validate the account fields are set
         if REC.RES_DEBIT_ACCOUNT_ID is null then
            PKG_PP_LOG.P_WRITE_MESSAGE('ERROR.  ILR '||REC.ILR_NUMBER||' is missing a Residual Debit Account.');
            L_ERR := L_ERR + 1;
         end if;
         if REC.RES_CREDIT_ACCOUNT_ID is null then
            PKG_PP_LOG.P_WRITE_MESSAGE('ERROR.  ILR '||REC.ILR_NUMBER||' is missing a Residual Credit Account.');
            L_ERR := L_ERR + 1;
         end if;

         -- Continue (not break) if there has ever been an error.
         -- This way, we will see all errors at once, instead of fixing one, rerunning, and repeating.
         continue when L_ERR > 0;

         -- process the debit
         L_LOCATION := 'Processing residual debit for asset_id: ' || TO_CHAR(REC.LS_ASSET_ID);

            L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(  REC.LS_ASSET_ID,
                                       3038, --A_TRANS_TYPE
                                       REC.JE_AMOUNT, --A_AMT
                                       0, --A_ASSET_ACT_ID
                                       -1, --A_DG_ID
                                       0, --A_WO_ID
                                       REC.RES_DEBIT_ACCOUNT_ID, --A_GL_ACCT_ID
                                       0, --A_GAIN_LOSS
                                       -1, --A_PEND_TRANS_ID
                                       REC.COMPANY_ID, --A_COMPANY_ID
                                       REC.MONTH, --A_MONTH
                                       1, --A_DR_CR
                                       L_GL_JE_CODE, --A_GL_JC
                                       rec.set_of_books_id, --A_SOB_ID,
                                       rec.rate, --a_rate
                                       rec.currency_from, --a_currency_from
                                       rec.currency_to, --a_currency_to
                                       L_STATUS);
         if L_RTN = -1 then
            if A_END_LOG = 1 then
				PKG_PP_LOG.P_END_LOG();
			 end if;
            return L_LOCATION || ' : ' || L_STATUS;
         end if;

            -- process the credit
            L_LOCATION := 'Processing residual credit for asset_id: ' || TO_CHAR(REC.LS_ASSET_ID);

         L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(  REC.LS_ASSET_ID,
                                       3039, --A_TRANS_TYPE
                                       REC.JE_AMOUNT, --A_AMT
                                       0, --A_ASSET_ACT_ID
                                       -1, --A_DG_ID
                                       0, --A_WO_ID
                                       REC.RES_CREDIT_ACCOUNT_ID, --A_GL_ACCT_ID
                                       0, --A_GAIN_LOSS
                                       -1, --A_PEND_TRANS_ID
                                       REC.COMPANY_ID, --A_COMPANY_ID
                                       REC.MONTH, --A_MONTH
                                       0, --A_DR_CR
                                       L_GL_JE_CODE, --A_GL_JC
                                       rec.set_of_books_id, --A_SOB_ID
                                       rec.rate, --a_rate
                                       rec.currency_from, --a_currency_from
                                       rec.currency_to, --a_currency_to
                                       L_STATUS);
         if L_RTN = -1 then
            if A_END_LOG = 1 then
				PKG_PP_LOG.P_END_LOG();
			 end if;
            return L_LOCATION || ' : ' || L_STATUS;
         end if;

      end loop;

      if L_ERR = 0 then
         L_RETURN := 'OK';
      elsif L_ERR = 1 then
         L_RETURN := 'There was 1 validation error.  No Journal Entries were booked.';
      else
         L_RETURN := 'There were '||L_ERR||' validation errors.  No Journal Entries were booked.';
      end if;

      if A_END_LOG = 1 then
      PKG_PP_LOG.P_END_LOG();
     end if;

      return L_RETURN;

   exception
      when others then
         return L_STATUS||' '||sqlerrm;
   end F_PROCESS_RESIDUAL;



   function F_PROCESS_RESIDUAL( A_LS_ASSET_IDS in NUM_ARRAY) return varchar2 is
   begin
      return F_PROCESS_RESIDUAL(A_LS_ASSET_IDS => A_LS_ASSET_IDS, A_END_LOG => 1);
   end F_PROCESS_RESIDUAL;
   --**************************************************************************
   --                            F_TAX_CALC
   --**************************************************************************
	function F_TAX_CALC
	(a_lease_id in number, a_company_id in number, a_month in date, a_accrual in number)
	RETURN VARCHAR2
	is
	L_STATUS VARCHAR2(30000);
	sqls varchar2(30000);
	counter number;
	BUCKET LS_RENT_BUCKET_ADMIN%rowtype;
	v_accrual_string varchar2(35);
	begin

	if A_ACCRUAL = 1 then
	  v_accrual_string:='Accrual';
	  elsif A_ACCRUAL= 0 then
	  v_accrual_string:='Payments';
	end if;

	PKG_PP_LOG.P_WRITE_MESSAGE('STARTING F_TAX_CALC for ' || v_accrual_string);

	L_STATUS:= 'Processing Tax ' || v_accrual_string || ' ' ||
			   'for Company ID: ' || to_char(A_COMPANY_ID) || ' - LEASE ID: ' || case when A_LEASE_ID = -1 THEN 'ALL' ELSE to_char(A_LEASE_ID) END || ' '||
			   'Month: ' || TO_CHAR(A_MONTH, 'YYYYMM');

	PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

	if v_accrual_string = 'Payments' then
	  v_accrual_string := 'Paid';
	end if;

	L_STATUS:='Deleting existing rows from Monthly Tax Table';

  if A_ACCRUAL = 0 THEN
	delete from LS_MONTHLY_TAX
	where GL_POSTING_MO_YR >= A_MONTH
	  and LS_ASSET_ID in
		  (select la.LS_ASSET_ID
			 from LS_ASSET la, ls_ilr li
			 where la.COMPANY_ID = A_COMPANY_ID
			  and la.ilr_id = li.ilr_id
			  and case when a_lease_id = -1 then -1 else li.lease_id end = a_lease_id)
	  and ACCRUAL = A_ACCRUAL
    and ls_asset_id not in (select ls_asset_id
                            from ls_payment_line
                            where gl_posting_mo_yr = A_MONTH
                              and payment_id in (
                                select payment_id
                                from ls_payment_hdr
                                where gl_posting_mo_yr = A_MONTH
                                  and ((PAYMENT_STATUS_ID = 2 or PAYMENT_STATUS_ID = 3)
                                       or payment_id in (select payment_id from ls_invoice_payment_map where in_tolerance = 1))));
  else
  	delete from LS_MONTHLY_TAX
	  where GL_POSTING_MO_YR >= A_MONTH
	    and LS_ASSET_ID in
		    (select la.LS_ASSET_ID
			  from LS_ASSET la, ls_ilr li
			  where la.COMPANY_ID = A_COMPANY_ID
			    and la.ilr_id = li.ilr_id
			  and case when a_lease_id = -1 then -1 else li.lease_id end = a_lease_id)
	  and ACCRUAL = A_ACCRUAL;
   end if;



	L_STATUS:='Inserting into monthly tax table for current and future rows';
	insert into LS_MONTHLY_TAX
	(LS_ASSET_ID, TAX_LOCAL_ID, GL_POSTING_MO_YR, SET_OF_BOOKS_ID,
	AMOUNT, ACCRUAL, SCHEDULE_MONTH, VENDOR_ID, tax_district_id, rate, tax_base)
	WITH TAXES AS (select
			 las.month,
			 RATES.RATE,
			 RATES.LS_ASSET_ID,
			 RATES.TAX_LOCAL_ID,
			 LAS.SET_OF_BOOKS_ID,
			 llv.payment_pct * (LAS.INTEREST_ACCRUAL + LAS.PRINCIPAL_ACCRUAL + NVL(LTBB.CONT1 * LAS.CONTINGENT_ACCRUAL1, 0) +
			 NVL(LTBB.CONT2 * LAS.CONTINGENT_ACCRUAL2, 0) + NVL(LTBB.CONT3 * LAS.CONTINGENT_ACCRUAL3, 0) +
			 NVL(LTBB.CONT4 * LAS.CONTINGENT_ACCRUAL4, 0) + NVL(LTBB.CONT5 * LAS.CONTINGENT_ACCRUAL5, 0) +
			 NVL(LTBB.CONT6 * LAS.CONTINGENT_ACCRUAL6, 0) + NVL(LTBB.CONT7 * LAS.CONTINGENT_ACCRUAL7, 0) +
			 NVL(LTBB.CONT8 * LAS.CONTINGENT_ACCRUAL8, 0) + NVL(LTBB.CONT9 * LAS.CONTINGENT_ACCRUAL9, 0) +
			 NVL(LTBB.CONT10 * LAS.CONTINGENT_ACCRUAL10, 0) + NVL(LTBB.EXEC1 * LAS.EXECUTORY_ACCRUAL1, 0) +
			 NVL(LTBB.EXEC2 * LAS.EXECUTORY_ACCRUAL2, 0) + NVL(LTBB.EXEC3 * LAS.EXECUTORY_ACCRUAL3, 0) +
			 NVL(LTBB.EXEC4 * LAS.EXECUTORY_ACCRUAL4, 0) + NVL(LTBB.EXEC5 * LAS.EXECUTORY_ACCRUAL5, 0) +
			 NVL(LTBB.EXEC6 * LAS.EXECUTORY_ACCRUAL6, 0) + NVL(LTBB.EXEC7 * LAS.EXECUTORY_ACCRUAL7, 0) +
			 NVL(LTBB.EXEC8 * LAS.EXECUTORY_ACCRUAL8, 0) + NVL(LTBB.EXEC9 * LAS.EXECUTORY_ACCRUAL9, 0) +
			 NVL(LTBB.EXEC10 * LAS.EXECUTORY_ACCRUAL10, 0) +
			 nvl(early_term_fees.net_residual, 0) + nvl(early_term_fees.actual_termination_amount,0)) amount,

				 decode(A_ACCRUAL, 1, 0, nvl(LIO.PAYMENT_SHIFT,0)) SHIFT,
			 decode(ltl2.pay_lessor, 0, -1, llv.vendor_id) vendor_id, /* WMD if pay_lessor = 1 put the vendor on there, if not put -1 to show remittance */
			 rates.tax_district_id
		  from (
		  select sum(TAB1.RATE) RATE,
				   TAB1.LS_ASSET_ID LS_ASSET_ID,
				   TAB1.TAX_LOCAL_ID TAX_LOCAL_ID,
				   TAB1.REVISION REVISION,
						 /* WMD removed 'STATE' grouping and added actual state */
				   TAB1.tax_district_id as tax_district_id,
				   TAB1.month
			   from (
			   select LTSR.RATE            RATE,
						 LATM.STATUS_CODE_ID  STATUS_CODE_ID,
						 LATM.LS_ASSET_ID     LS_ASSET_ID,
						 LATM.TAX_LOCAL_ID    TAX_LOCAL_ID,
						 LA.APPROVED_REVISION REVISION,
								   /* WMD */
						 trim(al.state_id) as tax_district_id,
						 MONTHS.MONTH
					 from LS_TAX_STATE_RATES LTSR,
						 ASSET_LOCATION     AL,
						 LS_TAX_LOCAL       LTL,
						 LS_ASSET_TAX_MAP   LATM,
						 LS_ASSET           LA,
								   LS_ILR_OPTIONS		LIO,
								   LS_ILR LI2,
						 (SELECT DISTINCT MONTH
						  FROM LS_ASSET_SCHEDULE LAS, LS_ASSET LA, LS_ILR ILR
						  WHERE LAS.LS_ASSET_ID = LA.LS_ASSET_ID
							AND LAS.REVISION = LA.APPROVED_REVISION
							AND LA.LS_ASSET_STATUS_ID = 3
							AND LA.COMPANY_ID = A_COMPANY_ID
							AND LA.ILR_ID = ILR.ILR_ID
							AND LAS.MONTH>=A_MONTH
							AND case when A_LEASE_ID = -1 then -1 else ILR.lease_id end = A_LEASE_ID) months
					where LTL.TAX_LOCAL_ID = LTSR.TAX_LOCAL_ID
					  and AL.STATE_ID = LTSR.STATE_ID
					  and AL.ASSET_LOCATION_ID = LA.TAX_ASSET_LOCATION_ID
					  and LA.LS_ASSET_ID = LATM.LS_ASSET_ID
					  and LTL.TAX_LOCAL_ID = LATM.TAX_LOCAL_ID
					  and LA.COMPANY_ID = A_COMPANY_ID
					  and LATM.status_code_id = 1
					  and LA.ILR_ID = LI2.ILR_ID
					  and case when A_LEASE_ID = -1 then -1 else li2.lease_id end = A_LEASE_ID
					  and LA.APPROVED_REVISION = LIO.REVISION
					  and LA.ILR_ID = LIO.ILR_ID
				  and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= LA.RETIREMENT_DATE))
					  and LTSR.EFFECTIVE_DATE =
						 (select max(T2.EFFECTIVE_DATE)
						   from LS_TAX_STATE_RATES T2, LS_LEASE LL, LS_ILR LI, LS_LEASE_OPTIONS LLO
						  where T2.TAX_LOCAL_ID = LTSR.TAX_LOCAL_ID
							and T2.STATE_ID = LTSR.STATE_ID
							and LA.ILR_ID = LI.ILR_ID
							and LI.LEASE_ID = LL.LEASE_ID
							and LL.CURRENT_REVISION = LLO.REVISION
							and LLO.LEASE_ID = LL.LEASE_ID
							and trunc(T2.EFFECTIVE_DATE, 'day') <= case
								 when LLO.TAX_RATE_OPTION_ID = 1 then
								  last_day(add_months(MONTHS.MONTH, nvl(LIO.PAYMENT_SHIFT,0)))
								 when LLO.TAX_RATE_OPTION_ID = 2 then
								  LL.MASTER_AGREEMENT_DATE
							   end)
				   union all
				   select LTDR.RATE            RATE,
						 LATM.STATUS_CODE_ID  STATUS_CODE_ID,
						 LATM.LS_ASSET_ID     LS_ASSET_ID,
						 LATM.TAX_LOCAL_ID    TAX_LOCAL_ID,
						 LA.APPROVED_REVISION REVISION,
								   to_char(AL.tax_district_id),
						 months.month
					 from LS_TAX_DISTRICT_RATES LTDR,
						 LS_LOCATION_TAX_DISTRICT        AL,
						 LS_TAX_LOCAL          LTL,
						 LS_ASSET_TAX_MAP      LATM,
						 LS_ASSET              LA,
								   LS_ILR_OPTIONS		   LIO,
								   LS_ILR LI2,
						 (SELECT DISTINCT MONTH
						  FROM LS_ASSET_SCHEDULE LAS, LS_ASSET LA, LS_ILR ILR
						  WHERE LAS.LS_ASSET_ID = LA.LS_ASSET_ID
							AND LAS.REVISION = LA.APPROVED_REVISION
							AND LA.LS_ASSET_STATUS_ID = 3
							AND LA.COMPANY_ID = A_COMPANY_ID
							AND LA.ILR_ID = ILR.ILR_ID
							AND LAS.MONTH>=A_MONTH
							AND case when A_LEASE_ID = -1 then -1 else ILR.lease_id end = A_LEASE_ID) months
					where LTL.TAX_LOCAL_ID = LTDR.TAX_LOCAL_ID
					  and AL.TAX_DISTRICT_ID = LTDR.LS_TAX_DISTRICT_ID
					  and AL.ASSET_LOCATION_ID = LA.TAX_ASSET_LOCATION_ID
					  and LA.LS_ASSET_ID = LATM.LS_ASSET_ID
					  and LTL.TAX_LOCAL_ID = LATM.TAX_LOCAL_ID
					  and LA.COMPANY_ID = A_COMPANY_ID
					  and LA.ILR_ID = LI2.ILR_ID
					  and LATM.status_code_id = 1
					  and case when A_LEASE_ID = -1 then -1 else li2.lease_id end = A_LEASE_ID
					  and LA.APPROVED_REVISION = LIO.REVISION
					  and LA.ILR_ID = LIO.ILR_ID
				  and (LA.LS_ASSET_STATUS_ID = 3 or (LA.LS_ASSET_STATUS_ID = 4 AND A_MONTH <= LA.RETIREMENT_DATE))
					  and LTDR.EFFECTIVE_DATE =
						 (select max(T2.EFFECTIVE_DATE)
						   from LS_TAX_DISTRICT_RATES T2, LS_LEASE LL, LS_ILR LI, LS_LEASE_OPTIONS LLO
						  where T2.TAX_LOCAL_ID = LTDR.TAX_LOCAL_ID
							and T2.LS_TAX_DISTRICT_ID = LTDR.LS_TAX_DISTRICT_ID
							and LA.ILR_ID = LI.ILR_ID
							and LI.LEASE_ID = LL.LEASE_ID
							and LL.CURRENT_REVISION = LLO.REVISION
							and LLO.LEASE_ID = LL.LEASE_ID
							and trunc(t2.EFFECTIVE_DATE, 'day') <= case
								 when LLO.TAX_RATE_OPTION_ID = 1 then
								  last_day(add_months(MONTHS.MONTH,nvl(LIO.PAYMENT_SHIFT,0)))
								 when LLO.TAX_RATE_OPTION_ID = 2 then
								  LL.MASTER_AGREEMENT_DATE
							   end)) TAB1
			  where TAB1.STATUS_CODE_ID = 1
			  group by TAB1.LS_ASSET_ID, TAB1.TAX_LOCAL_ID, TAB1.REVISION,
					TAB1.tax_district_id, tab1.month
			) RATES,
			 LS_ASSET_SCHEDULE LAS, LS_ILR_OPTIONS LIO,
			 LS_TAX_BASIS_BUCKETS LTBB, LS_ASSET LA, LS_ILR LI, ls_lease_vendor llv, LS_TAX_LOCAL LTL2,
       (select ls_asset_id, nvl(actual_termination_amount,0) actual_termination_amount,
              nvl(guaranteed_residual_amount,0) -  nvl(actual_residual_amount, nvl(guaranteed_residual_amount,0)) net_residual /* if actual is null then they haven't input it yet. Set to 0 */
       from ls_asset
       where retirement_date=A_MONTH) EARLY_TERM_FEES




		  where LAS.LS_ASSET_ID = RATES.LS_ASSET_ID
		  and LAS.REVISION = RATES.REVISION
		  AND LAS.MONTH = RATES.MONTH
		  and LA.LS_ASSET_ID = LAS.LS_ASSET_ID
		  and RATES.REVISION = LIO.REVISION
		  and LA.ILR_ID = LIO.ILR_ID
		  and LA.ILR_ID = LI.ILR_ID
		  and li.lease_id = llv.lease_id and li.company_id = llv.company_id
		  and case when A_LEASE_ID = -1 then -1 else li.lease_id end = A_LEASE_ID
		  and LTL2.tax_local_id = rates.tax_local_id
      and LA.LS_ASSET_ID = early_term_fees.ls_asset_id(+) )
	  SELECT
		TAXES.LS_ASSET_ID,
		TAXES.TAX_LOCAL_ID,
		MONTH,
		TAXES.SET_OF_BOOKS_ID,
		sum(TAXES.RATE * TAXES.AMOUNT),
		A_ACCRUAL,
		ADD_MONTHS(MONTH, TAXES.SHIFT),
		TAXES.VENDOR_ID,
		TAXES.tax_district_id, TAXES.RATE, sum(taxes.amount) as tax_base
	  FROM TAXES
      where ls_asset_id not in (select ls_asset_id
                            from ls_payment_line
                            where gl_posting_mo_yr = A_MONTH
                              and payment_id in (
                                select payment_id
                                from ls_payment_hdr
                                where gl_posting_mo_yr = A_MONTH
                                  and ((PAYMENT_STATUS_ID = 2 or PAYMENT_STATUS_ID = 3)
                                       or payment_id in (select payment_id from ls_invoice_payment_map where in_tolerance = 1))))
	 GROUP BY
		TAXES.LS_ASSET_ID,
		TAXES.TAX_LOCAL_ID,
		MONTH,
		TAXES.SET_OF_BOOKS_ID,
		A_ACCRUAL,
		ADD_MONTHS(MONTH, TAXES.SHIFT),
		TAXES.VENDOR_ID,
		TAXES.tax_district_id, TAXES.RATE;




















	 L_STATUS := 'Insert tax amounts on schedule';
		  select count(*)
		  into COUNTER
		  from LS_RENT_BUCKET_ADMIN
		  where TAX_EXPENSE_BUCKET =1;
		  if COUNTER <> 1 then
			 return 'Error: there must be exactly one bucket defined as the tax expense bucket';
		  end if;

		  L_STATUS := 'Build SQL string';
		  select *
		  into BUCKET
		  from LS_RENT_BUCKET_ADMIN
		  where TAX_EXPENSE_BUCKET = 1;

		L_STATUS:= 'Clearing Asset Schedule Tax ' || v_accrual_string || ' for Future Months';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		sqls:= '
		update ls_asset_schedule a
		set ' ||BUCKET.RENT_TYPE || '_' || v_accrual_string || to_char(BUCKET.BUCKET_NUMBER) || ' = 0
		where exists (select 1
					  from ls_asset la2, ls_ilr ilr2
					  where la2.ls_asset_id = a.ls_asset_id
						and la2.ilr_id = ilr2.ilr_id
						and la2.ls_asset_status_id = 3
						and la2.company_id = ' || TO_CHAR(A_COMPANY_ID) || '
						and case when ' || to_char(A_LEASE_ID) || '  = -1 then -1 else ilr2.lease_id end = ' || to_char(A_LEASE_ID) || '
						and la2.approved_revision = a.revision)
		  and a.month>=to_date(' || TO_CHAR(A_MONTH,'YYYYMM') || ', ''YYYYMM'')
      and ' ||BUCKET.RENT_TYPE || '_' || v_accrual_string || to_char(BUCKET.BUCKET_NUMBER) || ' <> 0
      and ls_asset_id in (select ls_asset_id
                          from ls_asset la, ls_ilr ilr
                          where la.ilr_id = ilr.ilr_id
                            and la.company_id = ' || to_char(A_COMPANY_ID) || '
                            and ilr.lease_id = case when ' || to_char(A_LEASE_ID) || '  = -1 then lease_id else ' || to_char(A_LEASE_ID) || ' end)';

		L_STATUS:= SQLS;
      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;

		L_STATUS:='Updating Asset Schedule';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

		sqls:= '
		update ls_asset_schedule a
		set ' ||BUCKET.RENT_TYPE || '_' || v_accrual_string || to_char(BUCKET.BUCKET_NUMBER) || ' =
		(select sum(amount)
		from ls_monthly_tax tax, ls_asset la, ls_ilr ilr
		where tax.ls_asset_id = a.ls_asset_id
		  and la.ls_asset_id = tax.ls_asset_id
		  and la.ilr_id = ilr.ilr_id
		  and decode(' || TO_CHAR(A_ACCRUAL) || ' , 1, tax.gl_posting_mo_yr, tax.schedule_month) = a.month
		  and la.company_id = ' || TO_CHAR(A_COMPANY_ID) || '
		  and case when ' || to_char(A_LEASE_ID) || '  = -1 then -1 else ilr.lease_id end = ' || to_char(A_LEASE_ID) || '
		  and la.approved_revision = a.revision
		  and tax.set_of_books_id = a.set_of_books_id
		  and tax.accrual = ' || to_char(A_ACCRUAL) || ' )
		where exists (select 1
					  from ls_monthly_tax tax2, ls_asset la2, ls_ilr ilr2
					  where tax2.ls_asset_id = a.ls_asset_id
						and la2.ls_asset_id = tax2.ls_asset_id
						and la2.ilr_id = ilr2.ilr_id
						and decode(' || to_char(A_ACCRUAL) || ' , 1, tax2.gl_posting_mo_yr, tax2.schedule_month) = a.month
						and la2.company_id = ' || TO_CHAR(A_COMPANY_ID) || '
						and case when ' || to_char(A_LEASE_ID) || '  = -1 then -1 else ilr2.lease_id end = ' || to_char(A_LEASE_ID) || '
						and la2.approved_revision = a.revision
						and tax2.set_of_books_id = a.set_of_books_id
						and tax2.accrual = ' || to_char(A_ACCRUAL) || ' )
		  and a.month>=to_date(' || TO_CHAR(A_MONTH,'YYYYMM') || ', ''YYYYMM'')';

		L_STATUS:= SQLS;
      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;

		L_STATUS:= 'Clearing ILR Schedule Tax ' || v_accrual_string || ' for future months';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

		sqls:= '
		update ls_ilr_schedule a
		set ' ||BUCKET.RENT_TYPE || '_' || v_accrual_string || to_char(BUCKET.BUCKET_NUMBER) || ' = 0
		where exists (select 1
					  from ls_ilr ilr2
					  where ilr2.ilr_id=a.ilr_id
						and ilr2.ilr_status_id = 2
						and ilr2.current_revision=a.revision
						and ilr2.company_id = ' || TO_CHAR(A_COMPANY_ID) || '
						and case when ' || to_char(A_LEASE_ID) || '  = -1 then -1 else ilr2.lease_id end = ' || to_char(A_LEASE_ID) || ')
		  and a.month>=to_date(' || TO_CHAR(A_MONTH,'YYYYMM') || ', ''YYYYMM'')
      and ' ||BUCKET.RENT_TYPE || '_' || v_accrual_string || to_char(BUCKET.BUCKET_NUMBER) || ' <> 0
      and a.ilr_id in (select ilr_id from ls_ilr where company_id = ' || to_char(A_COMPANY_ID) || ' and
      lease_id = case when ' || to_char(A_LEASE_ID) || '  = -1 then lease_id else ' || to_char(A_LEASE_ID) || ' end)';

		L_STATUS:= SQLS;
      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;

		L_STATUS:='Updating ILR Schedule';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		sqls:='
		update ls_ilr_schedule a
		set ' ||BUCKET.RENT_TYPE || '_' || v_accrual_string || to_char(BUCKET.BUCKET_NUMBER) || ' =
		(select sum(amount)
		from ls_monthly_tax tax, ls_asset la, ls_ilr ilr
		where la.ls_asset_id = tax.ls_asset_id
		  and la.ilr_id = ilr.ilr_id
		  and ilr.ilr_id = a.ilr_id
		  and decode(' || to_char(A_ACCRUAL) || ' , 1, tax.gl_posting_mo_yr, tax.schedule_month) = a.month
		  and la.company_id = ' || to_char(A_COMPANY_ID) || '
		  and case when ' || to_char(A_LEASE_ID) || '  = -1 then -1 else ilr.lease_id end = ' || to_char(A_LEASE_ID) || '
		  and la.approved_revision = a.revision
		  and tax.set_of_books_id = a.set_of_books_id
		  and tax.accrual = ' || to_char(A_ACCRUAL) || ' )
		where exists (select 1
					  from ls_monthly_tax tax2, ls_asset la2, ls_ilr ilr2
					  where la2.ls_asset_id = tax2.ls_asset_id
						and la2.ilr_id = ilr2.ilr_id
						and ilr2.ilr_id = a.ilr_id
						and decode(' || to_char(A_ACCRUAL) || ' , 1, tax2.gl_posting_mo_yr, tax2.schedule_month) = a.month
						and la2.company_id = ' || to_char(A_COMPANY_ID) || '
						and case when ' || to_char(A_LEASE_ID) || '  = -1 then -1 else ilr2.lease_id end = ' || to_char(A_LEASE_ID) || '
						and la2.approved_revision = a.revision
						and tax2.set_of_books_id = a.set_of_books_id
						and tax2.accrual = ' || to_char(A_ACCRUAL) || ' )
		and a.month>=to_date(' || TO_CHAR(A_MONTH,'YYYYMM') || ', ''YYYYMM'')';

	  L_STATUS:= SQLS;
      L_STATUS:=F_EXECUTE_IMMEDIATE(SQLS);
      IF L_STATUS <> 'OK' THEN
        RETURN L_STATUS;
      END IF;

	  return 'OK';
	   exception
		  when others then
			 return 'Error in F_TAX_CALC: ' || L_STATUS||' '||sqlerrm || sqlcode;
	   end F_TAX_CALC;
   --**************************************************************************
   --                            F_ADJUST_TAXES
   --**************************************************************************
	function f_adjust_taxes(A_INVOICE_ID in number,
						A_ADJUST_AMOUNT in number,
						A_TAX_LOCAL_ID in number,
						A_LINE_TYPE in number,
						A_LS_ASSET_ID in number)
	return varchar2
	is
	  L_STATUS varchar2(5000);
	  L_COUNT number(22,0);
	  L_LOOP_COUNT number(22,0):=0;
	  L_TOTAL_PAYMENT number(22,2);
	  L_TOTAL_ADJUSTMENT number(22,2):=0;
	begin
	  PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
	  PKG_PP_LOG.P_WRITE_MESSAGE(A_INVOICE_ID || ':' || A_ADJUST_AMOUNT || ':'|| A_TAX_LOCAL_ID || ':' || A_LINE_TYPE);

	  L_STATUS:='Getting total tax payment for assets associated with this invoice';
	  select sum(nvl(tax.amount,0) + nvl(tax.adjustment_amount,0))
	  into L_TOTAL_PAYMENT
	  from ls_payment_line lpl, ls_invoice_payment_map map, ls_monthly_tax tax
	  where map.invoice_id = A_INVOICE_ID
		and lpl.payment_id=map.payment_id
		and lpl.payment_type_id =A_LINE_TYPE
		and tax.gl_posting_mo_yr=lpl.gl_posting_mo_yr
		and tax.accrual=0
		and tax.ls_asset_id = lpl.ls_asset_id
		and tax.tax_local_id = (decode(A_TAX_LOCAL_ID, -1, tax.tax_local_id, A_TAX_LOCAL_ID))
		and lpl.ls_asset_id = decode(A_LS_ASSET_ID, -1, lpl.ls_asset_id, A_LS_ASSET_ID);

	  PKG_PP_LOG.P_WRITE_MESSAGE('Total Taxes for this invoice: ' || l_total_payment);

	  if L_TOTAL_PAYMENT = 0 then
		PKG_PP_LOG.P_END_LOG();
		return 'No tax lines found for this invoice';
	  end if;

	  L_STATUS:='Counting Monthly Tax Rows';
	  select count(*)
	  into L_COUNT
				from ls_invoice_payment_map map, ls_payment_line lpl, ls_monthly_tax tax
				where map.invoice_id = A_INVOICE_ID
				  and lpl.payment_type_id = A_LINE_TYPE
				  and lpl.payment_id = map.payment_id
				  and tax.gl_posting_mo_yr=lpl.gl_posting_mo_yr
				  and tax.accrual=0
				  and tax.ls_asset_id = lpl.ls_asset_id
				  and tax.tax_local_id = (decode(A_TAX_LOCAL_ID, -1, tax.tax_local_id, A_TAX_LOCAL_ID))
				  and nvl(tax.amount,0) + nvl(tax.adjustment_amount,0) <> 0
				  and lpl.ls_asset_id = decode(A_LS_ASSET_ID, -1, lpl.ls_asset_id, A_LS_ASSET_ID);

	  if L_COUNT = 0 then
		PKG_PP_LOG.P_WRITE_MESSAGE('No Tax lines found under this invoice for the select tax type');
		PKG_PP_LOG.P_END_LOG();
		return 'No Tax lines found under this invoice for the select tax type';
	  end if;

	  L_STATUS:='Getting a list of assets associated with the invoice';
	  for i in (select map.payment_id, tax.*
				from ls_invoice_payment_map map, ls_payment_line lpl, ls_monthly_tax tax
				where map.invoice_id = A_INVOICE_ID
				  and lpl.payment_type_id = A_LINE_TYPE
				  and lpl.payment_id = map.payment_id
				  and tax.gl_posting_mo_yr=lpl.gl_posting_mo_yr
				  and tax.accrual=0
				  and tax.ls_asset_id = lpl.ls_asset_id
				  and tax.tax_local_id = (decode(A_TAX_LOCAL_ID, -1, tax.tax_local_id, A_TAX_LOCAL_ID))
				  and nvl(tax.amount,0) + nvl(tax.adjustment_amount,0) <> 0
				  and lpl.ls_asset_id = decode(A_LS_ASSET_ID, -1, lpl.ls_asset_id, A_LS_ASSET_ID)
				 order by nvl(tax.amount,0) + nvl(tax.adjustment_amount,0))
	  loop

	  L_LOOP_COUNT:=L_LOOP_COUNT+1;
	  L_STATUS:='Updating ls_monthly_tax';
	  update ls_monthly_tax
	  set adjustment_amount = nvl(adjustment_amount,0) + (
	  CASE WHEN L_LOOP_COUNT <> L_COUNT THEN
		((nvl(i.amount,0) + nvl(i.adjustment_amount, 0))/ L_TOTAL_PAYMENT) * A_ADJUST_AMOUNT
	  ELSE
		A_ADJUST_AMOUNT - L_TOTAL_ADJUSTMENT END
	  )
	  where ls_asset_id = i.ls_asset_id
		and set_of_books_id = i.set_of_books_id
		and gl_posting_mo_yr=i.gl_posting_mo_yr
		and tax_local_id = i.tax_local_id
		and tax_district_id = i.tax_district_id
		and accrual = 0
		and vendor_id = i.vendor_id
		and ls_asset_id = decode(A_LS_ASSET_ID, -1, ls_asset_id, A_LS_ASSET_ID);

	  L_TOTAL_ADJUSTMENT:=L_TOTAL_ADJUSTMENT + ((nvl(i.amount,0) + nvl(i.adjustment_amount, 0))/ L_TOTAL_PAYMENT) * A_ADJUST_AMOUNT;

	  PKG_PP_LOG.P_WRITE_MESSAGE(L_TOTAL_ADJUSTMENT || ' Total Adjustment.  Loop Count ' || L_LOOP_COUNT || ' Total lines: ' || L_COUNT);
	  end loop;

	  PKG_PP_LOG.P_END_LOG();
	  return 'OK';

	  exception when others then
		PKG_PP_LOG.P_END_LOG();
		return 'Error in F_ADJUST_TAXES: ' || L_STATUS||' '||sqlerrm || sqlcode;
	end f_adjust_taxes;

   --**************************************************************************
   --                            F_GET_GL_POSTING_MO_YR
   --**************************************************************************
function F_GET_GL_POSTING_MO_YR
(A_ILR_ID in number, A_REVISION in number, A_MONTH_OUT out date)
return varchar2
is
  v_month_count number;
  L_STATUS varchar2(5000);
begin
  L_STATUS:= 'Counting distinct posting months for the associated ILR';
  select count(distinct cap_posting_month)
  into v_month_count
  from
  (select trunc(add_months(cc.interim_interest_start_date,
          case when extract(day from cc.interim_interest_start_date) >= ll.cut_off_day
          then 1
          else 0 end), 'month') cap_posting_month
   from ls_asset la, ls_component lc, ls_component_charge cc, ls_ilr ilr, ls_lease ll
   where cc.interim_interest_start_date is not null
    and nvl(cc.posted_to_gl,0)=0
    and cc.component_id = lc.component_id
    and lc.ls_asset_id = la.ls_asset_id
    and la.ilr_id = ilr.ilr_id
    and ilr.ilr_id = A_ILR_ID
    and ilr.lease_id = ll.lease_id);

  if v_month_count = 0 then
    L_STATUS:='Getting current open month';
    select min(gl_posting_mo_yr)
    into A_MONTH_OUT
    from ls_process_control
    where company_id = (select company_id from ls_ilr where ilr_id = A_ILR_ID)
      and lam_closed is null;
    return 'OK';
  elsif v_month_count > 1 then
    return 'Multiple months found for invoices associated with this ILR.  Only one month can be posted to the GL at a time';
  end if;

  L_STATUS:='Getting gl posting month';
  select distinct trunc(add_months(cc.interim_interest_start_date,
          case when extract(day from cc.interim_interest_start_date) >= ll.cut_off_day
          then 1
          else 0 end), 'month') cap_posting_month
  into A_MONTH_OUT
   from ls_asset la, ls_component lc, ls_component_charge cc, ls_ilr ilr, ls_lease ll
   where cc.interim_interest_start_date is not null
    and nvl(cc.posted_to_gl,0)=0
    and cc.component_id = lc.component_id
    and lc.ls_asset_id = la.ls_asset_id
    and la.ilr_id = ilr.ilr_id
    and ilr.ilr_id = A_ILR_ID
    and ilr.lease_id = ll.lease_id;


  RETURN 'OK';
  EXCEPTION WHEN OTHERS THEN
    RETURN 'Error in F_GET_GL_POSTING_MO_YR.  ' || L_STATUS || ' : ' || sqlerrm || sqlcode;
end F_GET_GL_POSTING_MO_YR;

function f_auto_retirements(A_COMPANY_ID IN NUMBER, A_MONTH IN DATE, A_END_LOG IN NUMBER:=NULL) RETURN VARCHAR2
IS
close_wo VARCHAR2(500);
rem_months NUMBER;
v_check NUMBER;
l_status varchar2(5000);
BEGIN

L_STATUS:='Checking if depreciation has already been approved';
PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
select decode(depr_Approved, null, 0, 1)
into rem_months
from LS_PROCESS_CONTROL
where company_id = A_COMPANY_ID
and GL_POSTING_MO_YR = A_MONTH;

L_STATUS:='Getting close work order';
PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

close_wo:= 'ZZ_LIFE_AUTO';
select trim(auto_close_wo_num)
into close_wo
from company
where company_id = A_COMPANY_ID;

if close_wo is null or close_wo = '' then
	close_wo:= 'ZZ_LIFE_AUTO';
else
  L_STATUS:='Checking that work order exists for the company';
PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
	-- check it exists
	select count(*)
	into v_check
	from work_order_control
	where company_id = A_COMPANY_ID
	and work_order_number = close_wo;

	if v_check = 0 and close_wo <>  'ZZ_LIFE_AUTO' then
		return 'The Auto retiremens work order ' || close_wo || ' does not exist for company_id ' || a_company_id;
	end if;
end if;

----//////////////////////
-- cpr depr retirements



L_STATUS:='Inserting into pending transaction for capital leases';
PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
insert into pend_transaction
(pend_trans_id,ldg_asset_id,ldg_activity_id,ldg_depr_group_id,books_schema_id,
 retirement_unit_id,utility_account_id,bus_segment_id,func_class_id,sub_account_id,
 asset_location_id,gl_account_id,company_id,gl_posting_mo_yr,subledger_indicator,
 activity_code,gl_je_code,work_order_number,posting_quantity,user_id1,posting_amount,
 user_id2,in_service_year,description,long_description,property_group_id,retire_method_id,
 posting_error,posting_status,cost_of_removal,salvage_cash,salvage_returns,gain_loss,
 reserve,misc_description,ferc_activity_code,serial_number,reserve_credits,replacement_amount, disposition_code
)
SELECT	PWRPLANT1.NEXTVAL				/* PEND_TRANS_ID */,
			A.ASSET_ID						/* ASSET_ID */,
			NULL								/* LDG_ACTIVITY_ID */,
			B.DEPR_GROUP_ID				/* DEPR_GROUP_ID */,
			B.BOOKS_SCHEMA_ID				/*	BOOKS_SCHEMA_ID */,
			B.RETIREMENT_UNIT_ID			/*	RETIREMENT_UNIT_ID */,
			B.UTILITY_ACCOUNT_ID			/*	UTILITY_ACCOUNT_ID */,
			B.BUS_SEGMENT_ID				/*	BUS_SEGMENT_ID */,
			B.FUNC_CLASS_ID				/*	FUNC_CLASS_ID */,
			B.SUB_ACCOUNT_ID				/* SUB_ACCOUNT_ID */,
			B.ASSET_LOCATION_ID			/* ASSET_LOCATION_ID */,
			B.GL_ACCOUNT_ID				/* GL_ACCOUNT_ID */,
			B.COMPANY_ID					/* COMPANY_ID */,
			A_MONTH	/* GL_POSTING_MO_YR*/,
			-100									/* SUBLEDGER_INDICATOR*/,
			'URGL'							/* ACTIVITY_CODE*/,
			c.GL_JE_CODE					/*	GL_JE_CODE */,
			nvl(c.work_order_number,close_wo)	/* WORK_ORDER_NUMBER */,
			B.ACCUM_QUANTITY * -1		/* POSTING_QUANTITY */,
			NULL								/* USER_ID1 */,
			B.ACCUM_COST * -1				/* POSTING_AMOUNT */,
			USER								/* USER_ID2 */,
			A_MONTH					/* IN SERVICE YEAR */,
			'Retirement'					/* DESCRIPTION */,
			'LEASE AUTO RETIRE (remaing life <= 1)' /* LONG_DESCRIPTION */,
			B.PROPERTY_GROUP_ID			/* PROPERTY_GROUP */,
			1									/*	RETIRE_METHOD_ID */,
			NULL								/*	POSTING ERROR */,
			1									/*	POSTING_STATUS */,
			0									/*	COST_OF_REMOVAL */,
			0									/*	SALVAGE_CASH */,
			0									/*	SALVAGE_RETURNS */,
			A.DEPR_RESERVE	- A.ASSET_DOLLARS			/* GAIN_LOSS */,
			A.DEPR_RESERVE					/* DEPR RESERVE */,
			B.LONG_DESCRIPTION			/* MISC DESCRIPTION */,
			2									/* FERC ACTIVITY CODE */,
			B.SERIAL_NUMBER				/* SERIAL NUMBER */,
			0									/* RESERVE CREDITS */,
			0									/* REPLACEMENT AMOUNT */,
			(select disposition_code from disposition_code where upper(trim(description)) = 'END OF LEASE')
FROM		CPR_DEPR a, CPR_LEDGER B, depr_group dg, depreciation_method dm,
			(	select	b.gl_je_code, a.custom_calc work_order_number
				from		gl_je_control a, standard_journal_entries b
				where		a.je_id = b.je_id
					and	upper(trim(a.process_id)) = 'LEASE AUTO RETIRE'
			) c
WHERE		A.ASSET_ID = B.ASSET_ID
	AND	A.GL_POSTING_MO_YR = A_MONTH
	AND	REMAINING_LIFE <= rem_months
	AND	B.ACCUM_COST > 0
	and   b.retirement_unit_id > 5
	and	b.company_id = A_COMPANY_ID
	and	b.depr_group_id = dg.depr_group_id
	and   dg.depr_method_id = dm.depr_method_id
	and   nvl(auto_retire,0) = 1 and set_of_books_id = 1
	and dg.subledger_type_id = -100
	/* exclude it if it's already in pending to be retired */
	and	b.asset_id not in
				(	select	ldg_asset_id
					from		pend_transaction
					where		company_id = A_COMPANY_ID
						and	ferc_activity_code = 2
						and	trim(ldg_asset_id) is not null
				)
  /* CJS 4/12/17 Assets with shorter depreciable lives than the lease term were getting retired prematurely; NOT IN seems safer long term vs NOT EXISTS */
  AND b.asset_id NOT IN (
      SELECT asset_id
      FROM ls_cpr_asset_map map, ls_asset la, ls_asset_schedule las
      WHERE map.ls_asset_id = la.ls_asset_id
      AND la.company_id = A_COMPANY_ID
      AND la.ls_asset_id = las.ls_asset_id
      AND Nvl(la.approved_revision,1) = las.revision
      AND las.set_of_books_id = 1
      AND las.MONTH = A_MONTH);

PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' Rows Added');


L_STATUS:='Inserting into pending transaction for operating leases';
PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
--You can't use a UNION with a sequence, so these have to be separate
insert into pend_transaction
(pend_trans_id,ldg_asset_id,ldg_activity_id,ldg_depr_group_id,books_schema_id,
 retirement_unit_id,utility_account_id,bus_segment_id,func_class_id,sub_account_id,
 asset_location_id,gl_account_id,company_id,gl_posting_mo_yr,subledger_indicator,
 activity_code,gl_je_code,work_order_number,posting_quantity,user_id1,posting_amount,
 user_id2,in_service_year,description,long_description,property_group_id,retire_method_id,
 posting_error,posting_status,cost_of_removal,salvage_cash,salvage_returns,gain_loss,
 reserve,misc_description,ferc_activity_code,serial_number,reserve_credits,replacement_amount, disposition_code
)
SELECT
	PWRPLANT1.NEXTVAL  /* PEND_TRANS_ID */,
	cpr.ASSET_ID /* ASSET_ID */,
	NULL /* LDG_ACTIVITY_ID */,
	cpr.DEPR_GROUP_ID /* DEPR_GROUP_ID */,
	cpr.BOOKS_SCHEMA_ID /* BOOKS_SCHEMA_ID */,
	cpr.RETIREMENT_UNIT_ID /* RETIREMENT_UNIT_ID */,
	cpr.UTILITY_ACCOUNT_ID /* UTILITY_ACCOUNT_ID */,
	cpr.BUS_SEGMENT_ID /* BUS_SEGMENT_ID */,
	cpr.FUNC_CLASS_ID /* FUNC_CLASS_ID */,
	cpr.SUB_ACCOUNT_ID /* SUB_ACCOUNT_ID */,
	cpr.ASSET_LOCATION_ID /* ASSET_LOCATION_ID */,
	cpr.GL_ACCOUNT_ID /* GL_ACCOUNT_ID */,
	cpr.COMPANY_ID /* COMPANY_ID */,
	A_MONTH /* GL_POSTING_MO_YR*/,
	-100 /* SUBLEDGER_INDICATOR*/,
	'URGL' /* ACTIVITY_CODE*/,
	c.GL_JE_CODE /* GL_JE_CODE */,
	nvl(c.work_order_number,close_wo) /* WORK_ORDER_NUMBER */,
	cpr.ACCUM_QUANTITY * -1 /* POSTING_QUANTITY */,
	NULL /* USER_ID1 */,
	cpr.ACCUM_COST * -1 /* POSTING_AMOUNT */,
	USER /* USER_ID2 */,
	A_MONTH 		/* IN SERVICE YEAR */,
	'Retirement' /* DESCRIPTION */,
	'LEASE AUTO RETIRE (remaing life <= 1)' /* LONG_DESCRIPTION */,
	cpr.PROPERTY_GROUP_ID /* PROPERTY_GROUP */,
	1 /* RETIRE_METHOD_ID */,
	NULL /* POSTING ERROR */,
	1 /* POSTING_STATUS */,
	0 /* COST_OF_REMOVAL */,
	0 /* SALVAGE_CASH */,
	0 /* SALVAGE_RETURNS */,
	0 /* GAIN_LOSS */,
	0 /* DEPR RESERVE */,
	cpr.LONG_DESCRIPTION /* MISC DESCRIPTION */,
	2 /* FERC ACTIVITY CODE */,
	cpr.SERIAL_NUMBER /* SERIAL NUMBER */,
	0 /* RESERVE CREDITS */,
	0 /* REPLACEMENT AMOUNT */,
	(select disposition_code from disposition_code where upper(trim(description)) = 'END OF LEASE')
FROM
	CPR_LEDGER cpr, ls_cpr_asset_map map, ls_asset la,
	(
		select b.gl_je_code, a.custom_calc work_order_number
		from
		gl_je_control a, standard_journal_entries b
		where
		a.je_id = b.je_id
		and
		upper(trim(a.process_id)) = 'LEASE AUTO RETIRE'
	) c,
	(
		select aa.ls_asset_id, aa.month, row_number() over(partition by aa.ls_asset_id, aa.revision, aa.set_of_books_id order by month desc) as the_row
		from ls_asset_schedule aa, ls_asset ab
		where aa.set_of_books_id = 1
		and ab.ls_asset_id = aa.ls_asset_id
		and aa.revision = ab.approved_revision
		and ab.company_id = A_COMPANY_ID
		and ab.ls_asset_status_id = 3 /* WMD added these two lines for significant perfomance improvement */
	) xy
WHERE
	cpr.asset_id = map.asset_id
	and map.ls_asset_id = la.ls_asset_id
	and la.ls_asset_status_id = 3 -- (only assets in service)
	And cpr.accum_cost = 0 -- (OM ones)
	and la.ls_asset_id = xy.ls_asset_id
	and xy.month = A_MONTH
	AND xy.the_row = 1
	and cpr.retirement_unit_id > 5
	and cpr.company_id = A_COMPANY_ID
	/* exclude it if it's already in pending to be retired */
	and cpr.asset_id not in
	(
		select ldg_asset_id
		from pend_transaction
		where company_id = A_COMPANY_ID
		and ferc_activity_code = 2
		and trim(ldg_asset_id) is not null);

PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' Rows Added');

L_STATUS:='Inseting into pend basis';
insert into pend_basis
(pend_trans_id,basis_1, gl_acct_1,basis_2,gl_acct_2,basis_3,gl_acct_3,basis_4,gl_acct_4,
 basis_5,    gl_acct_5,    basis_6,    gl_acct_6,    basis_7,    gl_acct_7,    basis_8,    gl_acct_8,
 basis_9,    gl_acct_9,    basis_10,  gl_acct_10,  basis_11,   gl_acct_11,  basis_12,   gl_acct_12,
 basis_13,  gl_acct_13,  basis_14,  gl_acct_14,  basis_15,   gl_acct_15,  basis_16,  gl_acct_16,
 basis_17,  gl_acct_17,  basis_18,  gl_acct_18,  basis_19,   gl_acct_19,  basis_20,  gl_acct_20,
 basis_21,  gl_acct_21,  basis_22,  gl_acct_22,  basis_23,   gl_acct_23,  basis_24,  gl_acct_24,
 basis_25,  gl_acct_25,  basis_26,  gl_acct_26,  basis_27,   gl_acct_27,  basis_28,   gl_acct_28,
 basis_29,  gl_acct_29,  basis_30,  gl_acct_30,  basis_31,   gl_acct_31,  basis_32,   gl_acct_32,
 basis_33,  gl_acct_33,  basis_34,  gl_acct_34,  basis_35,   gl_acct_35,   basis_36,  gl_acct_36,
 basis_37,  gl_acct_37,  basis_38,  gl_acct_38, 	 basis_39,  gl_acct_39,   basis_40,  gl_acct_40,
 basis_41,  gl_acct_41,  basis_42,  gl_acct_42,  basis_43,   gl_acct_43,   basis_44,  gl_acct_44,
 basis_45,  gl_acct_45,  basis_46,  gl_acct_46,
 basis_47,  gl_acct_47,  basis_48,  gl_acct_48,  basis_49,  gl_acct_49,   basis_50,   gl_acct_50,
 basis_51,  gl_acct_51,  basis_52,  gl_acct_52,  basis_53,  gl_acct_53,   basis_54,   gl_acct_54,
 basis_55,  gl_acct_55,  basis_56,  gl_acct_56,  basis_57,  gl_acct_57,   basis_58,   gl_acct_58,
 basis_59,  gl_acct_59,  basis_60,  gl_acct_60,  basis_61,  gl_acct_61,   basis_62,   gl_acct_62,
 basis_63,  gl_acct_63,  basis_64,  gl_acct_64,  basis_65,  gl_acct_65,   basis_66,   gl_acct_66,
 basis_67,  gl_acct_67,  basis_68,  gl_acct_68, 	basis_69,  gl_acct_69,   basis_70,   gl_acct_70
)
select	a.pend_trans_id,c.basis_1 * -1, null, c.basis_2 * -1, null,
c.basis_3 * -1, null, c.basis_4 * -1, null, c.basis_5 * -1, null, c.basis_6 * -1, null,
c.basis_7 * -1, null, c.basis_8 * -1, null, c.basis_9 * -1, null, c.basis_10 * -1, null,
c.basis_11 * -1, null, c.basis_12 * -1, null, c.basis_13 * -1, null, c.basis_14 * -1, null,
c.basis_15 * -1, null, c.basis_16 * -1, null, c.basis_17 * -1, null, c.basis_18 * -1, null,
c.basis_19 * -1, null, c.basis_20 * -1, null, c.basis_21 * -1, null, c.basis_22 * -1, null,
c.basis_23 * -1, null, c.basis_24 * -1, null, c.basis_25 * -1, null, c.basis_26 * -1, null,
c.basis_27 * -1, null, c.basis_28 * -1, null, c.basis_29 * -1, null, c.basis_30 * -1, null,
c.basis_31 * -1, null, c.basis_32 * -1, null, c.basis_33 * -1, null, c.basis_34 * -1, null,
c.basis_35 * -1, null, c.basis_36 * -1, null, c.basis_37 * -1, null, c.basis_38 * -1, null,
c.basis_39 * -1, null, c.basis_40 * -1, null, c.basis_41 * -1, null, c.basis_42 * -1, null,
c.basis_43 * -1, null, c.basis_44 * -1, null, c.basis_45 * -1, null, c.basis_46 * -1, null,
c.basis_47 * -1, null, c.basis_48 * -1, null, c.basis_49 * -1, null, c.basis_50 * -1, null,
c.basis_51 * -1, null, c.basis_52 * -1, null, c.basis_53 * -1, null, c.basis_54 * -1, null,
c.basis_55 * -1, null, c.basis_56 * -1, null, c.basis_57 * -1, null, c.basis_58 * -1, null,
c.basis_59 * -1, null, c.basis_60 * -1, null, c.basis_61 * -1, null, c.basis_62 * -1, null,
c.basis_63 * -1, null, c.basis_64 * -1, null, c.basis_65 * -1, null, c.basis_66 * -1, null,
c.basis_67 * -1, null, c.basis_68 * -1, null, c.basis_69 * -1, null, c.basis_70 * -1, null
from	(	/* these guys need a basis record */
			select	a.pend_trans_id, b.ldg_asset_id
			from		(	SELECT	A.PEND_TRANS_ID
							FROM		PEND_TRANSACTION A
							where		a.gl_posting_mo_yr = A_MONTH
								and 	subledger_indicator = -100
								and	upper(trim(a.activity_code)) = 'URGL'
								and	upper(trim(a.gl_je_code)) in
											(	select	upper(trim(c.gl_je_code))
												from		gl_je_control b, standard_journal_entries c
												where		b.je_id = c.je_id
													and	upper(trim(b.process_id)) = 'LEASE AUTO RETIRE'
											)
							MINUS
							SELECT	PEND_TRANS_ID
							FROM		PEND_BASIS
						) a, pend_transaction b
			where		a.pend_trans_id = b.pend_trans_id
		) a, cpr_ldg_basis c
where	a.ldg_asset_id = c.asset_id;



RETURN 'OK';
  EXCEPTION WHEN OTHERS THEN
    RETURN 'Error in F_AUTO_RETIREMENTS.  ' || L_STATUS || ' : ' || sqlerrm || sqlcode;
END F_AUTO_RETIREMENTS;

-- KRD 1/31/17
FUNCTION F_LAM_CPR_CLOSE(
                         A_COMPANY_ID IN NUMBER,
                         A_MONTH      IN DATE
                        )
RETURN VARCHAR2 IS
L_LOC VARCHAR2(1000);
L_RTN VARCHAR2(1000);
L_CHECK NUMBER;
L_CHECK2 VARCHAR2(1000);
L_STATUS VARCHAR2(10);

L_BEG_BAL NUMBER(10,2);
L_BEG_RES NUMBER(10,2);
L_END_BAL NUMBER(10,2);
L_END_RES NUMBER(10,2);

CAP_BASIS_INDICATOR NUMBER;
LDG_BASIS_AMT NUMBER(22,2);

L_DG_BASIS SYS_REFCURSOR;

L_STANDALONE VARCHAR2(35);

BEGIN
-- This is the last code that will be called from nvo_ls_mec.of_openNext (the Close Month button)
-- From here we will run any code from the CPR Control buttons that is applicable to the Lease module
-- The normal Lease close is fully completed at this point, and we are now closing out the CPR
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('Starting Logs for Close Month Lease Module Process. Company ID: ' || TO_CHAR(A_COMPANY_ID) || ' - ' || 'Month: ' || to_char(A_MONTH,'MON-YYYY'));

    L_STATUS := 'OK';

	--CJS 5/17/17 Adding System Control for toggling Lease Standalone Changes
	L_STANDALONE:=lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_company('Lease Standalone', A_COMPANY_ID)));

	IF nvl(L_STANDALONE, 'no') = 'no' THEN
		return L_STATUS;
	END IF;

-- Stuff from "Close CPR" button first
--
    L_LOC := 'Checking previous month is closed in CPR_CONTROL';

      PKG_PP_LOG.P_WRITE_MESSAGE('Verifying last month is closed in the CPR.');

-- Make sure previous month is closed
      SELECT Count(*)
      INTO L_CHECK
      FROM cpr_control
      WHERE company_id = A_COMPANY_ID
      AND To_Char(accounting_month, 'yyyymm') = To_Char(Add_Months(A_MONTH, -1), 'yyyymm')
      AND cpr_closed IS NULL;

      IF L_CHECK = 1 THEN
        PKG_PP_LOG.P_WRITE_MESSAGE('The current month cannot be closed because the prior month is open for this company.');
        RETURN 'ERROR IN LOCATION: ' || L_LOC;
      END IF;

-- Make sure the next month is open
--   Just going to run an insert instead of checking and failing the process like the CPR does.
--   Doesn't really matter for Lease purposes.
    L_LOC := 'Inserting new month into CPR_CONTROL';

      INSERT INTO cpr_control (company_id, accounting_month)
        SELECT A_COMPANY_ID AS company_id,
              Add_Months(A_MONTH, 1) AS accounting_month
        FROM dual
        WHERE NOT EXISTS (SELECT 1
                          FROM cpr_control
                          WHERE company_id = A_COMPANY_ID
                          AND   accounting_month = Add_Months(A_MONTH, 1)
                         );

      PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows inserted to CPR_CONTROL for new month');


    L_LOC := 'Setting ending balances on Account Summary';

      PKG_PP_LOG.P_WRITE_MESSAGE('Setting ending balances on Account Summary.');

      UPDATE account_summary
      SET ending_balance = beginning_balance + additions + retirements + transfers_in + transfers_out + adjustments
      WHERE A_MONTH = gl_posting_mo_yr
      AND company_id = A_COMPANY_ID;

      PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows updated on ACCOUNT_SUMMARY');


    L_LOC := 'Rolling forward balance Account Summary to new month';

      PKG_PP_LOG.P_WRITE_MESSAGE('Rolling forward balances on Account Summary to new month ' || To_Char(A_MONTH, 'YYYYMM'));

-- Need to make sure this works. No idea what the to number, 33, and 27 thing is doing
      UPDATE account_summary a
      SET beginning_balance =
                            (
                            SELECT b.ending_balance
                            FROM  account_summary b
                            WHERE a.set_of_books_id = b.set_of_books_id
                            AND   a.company_id = b.company_id
                            AND   a.bus_segment_id = b.bus_segment_id
                            AND   a.gl_account_id = b.gl_account_id
                            AND   a.utility_account_id = b.utility_account_id
                            AND   a.sub_account_id = b.sub_account_id
                            AND   a.major_location_id = b.major_location_id
                            AND  (To_Number(To_Char(a.gl_posting_mo_yr, 'J')) - 33) < To_Number(To_Char(b.gl_posting_mo_yr , 'J'))
                            AND  (To_Number(To_Char(a.gl_posting_mo_yr, 'J')) - 27) > To_Number(To_Char(b.gl_posting_mo_yr , 'J'))
                            AND  A_MONTH = a.gl_posting_mo_yr
                            AND  b.company_id  = A_COMPANY_ID
                            )
      WHERE a.gl_posting_mo_yr = Add_Months(A_MONTH, 1)
      AND   a.company_id = A_COMPANY_ID
	    AND EXISTS (
                  SELECT 1
	                FROM account_summary b
				          WHERE a.set_of_books_id = b.set_of_books_id
				          AND   a.company_id = b.company_id
				          AND   a.bus_segment_id = b.bus_segment_id
				          AND   a.gl_account_id = b.gl_account_id
				          AND   a.utility_account_id = b.utility_account_id
				          AND   a.sub_account_id = b.sub_account_id
				          AND   a.major_location_id  = b.major_location_id
				          AND   (To_Number(To_Char(a.gl_posting_mo_yr, 'J')) - 33) < To_Number(To_Char(b.gl_posting_mo_yr , 'J'))
				          AND   (To_Number(To_Char(a.gl_posting_mo_yr, 'J')) - 27) > To_Number(To_Char(b.gl_posting_mo_yr , 'J'))
				          AND   A_MONTH = a.gl_posting_mo_yr
                  AND   b.company_id  = A_COMPANY_ID
                );

      PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows updated on ACCOUNT_SUMMARY');


    L_LOC := 'Update current month depr ledger status to 8';

      UPDATE depr_ledger
      SET depr_ledger_status = 8
      WHERE gl_post_mo_yr =  A_MONTH
      AND depr_group_id IN (SELECT depr_group_id
                            FROM depr_group
                            WHERE company_id = A_COMPANY_ID
                          );

      PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows updated in DEPR_LEDGER');

-- Balance depr if the control is enabled
-- ****************************************************** REVIEW: I think this should always be enabled. Delete system control check
--                                                                and just do it anyways?
    L_LOC := 'Getting system control value for "Balance Depr On Close"';

      SELECT Lower(Trim(control_value))
      INTO L_CHECK2
      FROM pp_system_control
      WHERE Lower(Trim(control_name)) = 'balance depr on close';

      IF L_CHECK2 = 'yes' THEN
        PKG_PP_LOG.P_WRITE_MESSAGE('Beginning Lease to Depreciation balance check');

    L_LOC := 'Calling F_BAL_LEASE_DEPR';

      L_RTN := F_BAL_LEASE_DEPR(A_COMPANY_ID);

      IF L_RTN = -1 THEN
        PKG_PP_LOG.P_WRITE_MESSAGE(' ');
        PKG_PP_LOG.P_WRITE_MESSAGE('One or more out of balance conditions exist for Company ' || A_COMPANY_ID || '. Review and fix before proceeding with month end close');
        RETURN 'LEASE TO DEPRECIATION OUT OF BALANCE';
      END IF;

      PKG_PP_LOG.P_WRITE_MESSAGE('Succcessfully balanced Lease Depreciation');


      END IF;

    L_CHECK2 := ' ';
    L_STATUS := 'OK';

-- Balance CPR if the control is enabled
-- ****************************************************** REVIEW: I think this should always be enabled. Delete system control check
--                                                                and just do it anyways?
    L_LOC := 'Getting system control value for "Balance CPR On Close"';

      SELECT Lower(Trim(control_value))
      INTO L_CHECK2
      FROM pp_system_control
      WHERE Lower(Trim(control_name)) = 'balance cpr on close';

      IF L_CHECK2 = 'yes' THEN
        PKG_PP_LOG.P_WRITE_MESSAGE('Beginning Lease to CPR balance checks');

    L_LOC := 'Calling F_BAL_LEASE_CPR';

      L_RTN := F_BAL_LEASE_CPR(A_COMPANY_ID);

      IF L_RTN = -1 THEN
        PKG_PP_LOG.P_WRITE_MESSAGE(' ');
        PKG_PP_LOG.P_WRITE_MESSAGE('One or more out of balance conditions exist for Company ' || A_COMPANY_ID || '. Review and fix before proceeding with month end close');
        RETURN 'LEASE TO CPR OUT OF BALANCE';
      END IF;

    L_LOC := 'Updating CPR_BALANCED on CPR_CONTROL';

      UPDATE cpr_control
      SET cpr_balanced = SYSDATE
      WHERE company_id = A_COMPANY_ID
      AND   accounting_month = A_MONTH;

      PKG_PP_LOG.P_WRITE_MESSAGE('Succcessfully balanced Lease to CPR');

      END IF;

    L_LOC := ' Updating CPR_CLOSED on CPR_CONTROL';

      UPDATE cpr_control
      SET cpr_closed = SYSDATE
      WHERE company_id = A_COMPANY_ID
      AND   accounting_month = A_MONTH;

-- End of "Close CPR" button code"
-- From "Approve Depreciation" button:
-- Set the current month depr_ledger row status to 1
-- ******************************************************* Need to test this. Stole it from ssp_depr_approval and changed subledger
    L_LOC := 'Updating balances on Depr Ledger for current month';

      UPDATE depr_ledger u
	      SET (depr_ledger_status, end_balance, end_reserve, reserve_bal_cor, reserve_bal_adjust,
		        reserve_bal_retirements, reserve_bal_tran_in, reserve_bal_tran_out,
		        reserve_bal_gain_loss, reserve_bal_other_credits, current_net_salvage_reserve,
		        cor_end_reserve, reserve_bal_salvage_exp, salvage_balance, reserve_bal_impairment
		        ) =
		            (SELECT  1, c.begin_balance + c.additions + c.retirements + c.transfers_in
					            + c.transfers_out + c.adjustments + c.impairment_asset_amount
				            , c.begin_reserve + c.retirements + c.gain_loss + c.salvage_cash
					            + c.salvage_returns + c.reserve_credits + c.reserve_tran_in + c.reserve_tran_out
					            + c.reserve_adjustments + c.depr_exp_alloc_adjust + c.depr_exp_adjust + c.depreciation_expense
					            - nvl(c.current_net_salvage_amort,0) + decode(nvl(subledger_type_id,0), 0, 0, c.cost_of_removal)
					            + c.salvage_expense + c.salvage_exp_adjust + c.salvage_exp_alloc_adjust  + c.reserve_blending_transfer
					            + c.impairment_expense_amount + c.impairment_asset_amount
				            , nvl(p.reserve_bal_cor,0) + c.cost_of_removal
				            , nvl(p.reserve_bal_adjust,0) + c.reserve_adjustments + c.reserve_blending_adjustment + c.reserve_blending_transfer
				            , nvl(p.reserve_bal_retirements,0) + c.retirements
				            , nvl(p.reserve_bal_tran_in,0) + c.reserve_tran_in
				            , nvl(p.reserve_bal_tran_out,0) + c.reserve_tran_out
				            , nvl(p.reserve_bal_gain_loss,0) + c.gain_loss
				            , nvl(p.reserve_bal_other_credits,0) + c.reserve_credits
				            , nvl(p.current_net_salvage_reserve,0) + nvl(c.current_net_salvage_amort,0)
				            , decode(nvl(subledger_type_id,0), 0, c.cor_beg_reserve + c.cor_expense + c.cor_exp_adjust + c.cost_of_removal
					            + c.cor_res_tran_in + c.cor_res_tran_out + c.cor_res_adjust + c.cor_exp_alloc_adjust  + c.cor_blending_transfer, 0)
				            , nvl(p.reserve_bal_salvage_exp, 0) + c.salvage_expense + c.salvage_exp_adjust + c.salvage_exp_alloc_adjust
				            , nvl(p.salvage_balance, 0) + c.salvage_cash + c.salvage_returns
				            , (nvl(p.reserve_bal_impairment, 0) + c.impairment_expense_amount + c.impairment_asset_amount)
		            from  depr_ledger p, depr_ledger c, depr_group g
		            where p.depr_group_id (+) = c.depr_group_id
		            and   p.set_of_books_id (+) = c.set_of_books_id
		            and   c.depr_group_id = g.depr_group_id
		            and   to_char(p.gl_post_mo_yr (+), 'mm/yyyy') = to_char(add_months(c.gl_post_mo_yr,-1), 'mm/yyyy' )
		            and   u.depr_group_id = c.depr_group_id
		            and   u.set_of_books_id = c.set_of_books_id
		            and   u.gl_post_mo_yr = c.gl_post_mo_yr
		          )
	      WHERE u.gl_post_mo_yr = A_MONTH
	      AND	u.depr_group_id IN (
			                          SELECT Depr_group_id
			                          FROM   depr_group
			                          WHERE  company_id = A_COMPANY_ID
				                          AND	Nvl(subledger_type_id,-100) = -100
                              );

    L_LOC := 'Update beginning balances on Depr Ledger for next month';

      UPDATE depr_ledger a
      SET (begin_reserve, begin_balance, depr_ledger_status, impairment_reserve_beg, cor_beg_reserve, impairment_asset_begin_balance) =
	      (SELECT b.end_reserve, b.end_balance, 8, impairment_reserve_end, cor_end_reserve, impairment_asset_begin_balance + impairment_asset_activity_salv
		      FROM  depr_ledger b
		      WHERE a.set_of_books_id = b.set_of_books_id and
				      a.depr_group_id   = b.depr_group_id   and
				      Add_Months(a.gl_post_mo_yr, -1) =b.gl_post_mo_yr
	      )
      WHERE a.gl_post_mo_yr = add_months(A_MONTH,1)
      AND a.depr_group_id IN (
				                      SELECT depr_group_id
                              FROM depr_group
				                      WHERE company_id = A_COMPANY_ID
				                        AND	Nvl(subledger_type_id,0) > -1
                            );


    L_LOC := ' Verify beginning balances of next month equal ending balances of this month';

      SELECT Round(Sum(begin_balance),2), Round(Sum(begin_reserve),2)
      INTO L_BEG_BAL, L_BEG_RES
      FROM depr_ledger, depr_group
      WHERE depr_ledger.depr_group_id = depr_group.depr_group_id
      AND gl_post_mo_yr = A_MONTH
      AND company_id = A_COMPANY_ID
      AND	Nvl(subledger_type_id,-100) = -100;

      SELECT Round(Sum(end_balance),2), Round(Sum(end_reserve),2)
      INTO L_END_BAL, L_END_RES
      FROM depr_ledger,	depr_group
      WHERE depr_ledger.depr_group_id = depr_group.depr_group_id
      AND company_id = A_COMPANY_ID
      AND gl_post_mo_yr = Add_Months(A_MONTH,-1)
      AND	Nvl(subledger_type_id,0) > -1;

      IF L_BEG_BAL <> L_END_BAL THEN
        PKG_PP_LOG.P_WRITE_MESSAGE('Beginning Plant balance of new month does not equal ending Plant balance of current month');
        PKG_PP_LOG.P_WRITE_MESSAGE('Review and fix before proceeding with month end close');
        RETURN 'PLANT BALANCES OUT OF BALANCE';
      END IF;

      IF L_BEG_RES <> L_END_RES THEN
        PKG_PP_LOG.P_WRITE_MESSAGE('Beginning Reserve balance of new month does not equal ending Reserve balance of current month');
        PKG_PP_LOG.P_WRITE_MESSAGE('Review and fix before proceeding with month end close');
        RETURN 'DEPR RESERVE OUT OF BALANCE';
      END IF;

      PKG_PP_LOG.P_WRITE_MESSAGE('Finalized the Depreciation Ledger for the current month');


-- Done with the "Approve Depreciation" button stuff
-- "Close PowerPlant" button:
    L_LOC := 'Deleting from Depr Vintage Summary for current month';

      PKG_PP_LOG.P_WRITE_MESSAGE('Rebuilding Depr Vintage Summary for the new month');

      DELETE FROM depr_vintage_summary
      WHERE accounting_month = A_MONTH
      AND depr_group_id IN (SELECT depr_group_id FROM depr_group WHERE company_id = A_COMPANY_ID);


    L_LOC := 'Getting capital lease basis bucket';

      /* JKIM - 04102017 - joining on lease cap type
      SELECT book_summary_id
      INTO CAP_BASIS_INDICATOR
      FROM book_summary
      WHERE book_summary_type = 'LESSEE'
      AND InStr(Lower(summary_name),'capital',1,1) <> 0;
      */
	  -- CJS 5/17/17 Loop through all on-balance sheet basis buckets
	FOR i IN (
			  SELECT DISTINCT a.book_summary_id CAP_BASIS_INDICATOR
			  FROM book_summary a, ls_lease_cap_type b
			  WHERE a.book_summary_id = b.book_summary_id
			    AND is_om = 0
			    AND active = 1
			  )
	LOOP
    L_LOC := 'Inserting into Depr Vintage Summary for current month';

      FOR j IN (
                SELECT DISTINCT depr_group_id AS DG
                FROM depr_group
                WHERE company_id = A_COMPANY_ID
                AND   subledger_type_id = -100
              )
      LOOP
        OPEN L_DG_BASIS FOR 'SELECT Sum(basis_' || i.CAP_BASIS_INDICATOR || ')
                            FROM set_of_books sob,
                                  cpr_ldg_basis cprb,
                                  company_set_of_books csob,
                                  cpr_ledger cpr,
                                  depr_group dg
                            WHERE cpr.asset_id = cprb.asset_id
                            AND   cpr.depr_group_id = dg.depr_group_id
                            AND   csob.company_id = dg.company_id
                            AND   csob.set_of_books_id = sob.set_of_books_id
                            AND   cpr.subledger_indicator = -100
                            AND   dg.company_id = ' || A_COMPANY_ID || '
                            AND   dg.depr_group_id = ' || J.DG;

        FETCH L_DG_BASIS INTO LDG_BASIS_AMT;

        INSERT INTO depr_vintage_summary (SET_OF_BOOKS_ID, DEPR_GROUP_ID, VINTAGE, ACCOUNTING_MONTH, ACCUM_COST)
          SELECT DISTINCT csob.set_of_books_id,
                J.DG AS depr_group_id,
                To_Number(To_Char(cpr.eng_in_service_year, 'yyyy')) AS vintage,
                A_MONTH AS accounting_month,
                LDG_BASIS_AMT AS accum_cost
          FROM company_set_of_books csob,
              cpr_ledger cpr
          WHERE csob.company_id = cpr.company_id
          AND   cpr.depr_group_id = J.DG
          AND   csob.company_id = A_COMPANY_ID
          AND   cpr.subledger_indicator = -100
          AND NOT EXISTS (SELECT 1 FROM depr_vintage_summary WHERE depr_group_id = J.DG AND accounting_month = A_MONTH);

      END LOOP;
	END LOOP; -- CJS 5/17/17 basis bucket loop
    L_LOC := 'Archiving GL Transactions for the current month';

      PKG_PP_LOG.P_WRITE_MESSAGE('Archiving GL Transactions');


      INSERT INTO arc_gl_transaction (gl_trans_id,month,company_number,gl_account,debit_credit_indicator,
		                                  amount,gl_je_code,gl_status_id,description,source,pend_trans_id,
		                                  asset_id,originator,comments, trans_type
                                     )
	      SELECT gl_trans_id, month, company_number, gl_account, debit_credit_indicator,
		           amount, gl_je_code, gl_status_id, gl.description, source, pend_trans_id,
		           asset_id, originator, comments, trans_type
	      FROM gl_transaction gl,
             company
	      WHERE trim(gl.company_number) = company.gl_company_no
        AND gl_status_id = 3
        AND MONTH <= A_MONTH
	      AND company_id = A_COMPANY_ID
        AND NOT EXISTS (
                        SELECT 1
                        FROM arc_gl_transaction arc
		                    WHERE gl.gl_trans_id = arc.gl_trans_id
                       );

      PKG_PP_LOG.P_WRITE_MESSAGE(SQL%ROWCOUNT || ' rows archived to ARC_GL_TRANSACTION.');

-- In the SSP CLOSE POWERPLANT function for this it deleted from GL_TRANSACTION. I didn't think this was supposed to happen
--    so leaving this DELETE out for now
    L_LOC := 'Updating CPR_CONTROL time stamps';

      UPDATE CPR_CONTROL
      SET DEPR_CALCULATED = SYSDATE,
          DEPR_APPROVED = SYSDATE,
          JE_RELEASED = SYSDATE,
          GL_RECONCILED = SYSDATE,
          POWERPLANT_CLOSED = SYSDATE,
          RETIREMENTS_AMORTIZED = SYSDATE,
          ARO_CALCULATED = SYSDATE,
          ARO_APPROVED = SYSDATE
      WHERE company_id = A_COMPANY_ID
      AND   accounting_month = A_MONTH;


      PKG_PP_LOG.P_WRITE_MESSAGE('Month end processing complete for Company: ' || A_COMPANY_ID || '    Month: ' || To_Char(A_MONTH, 'yyyymm'));

-- Not sure if we need to end the logs here or if the PB code will do it. Leaving commented out for now.
--      PKG_PP_LOG.P_END_LOG();
RETURN 'OK';

  EXCEPTION WHEN OTHERS THEN
    PKG_PP_LOG.P_WRITE_MESSAGE('ERROR IN FUNCTION: PKG_LEASE_CALC.F_LAM_CPR_CLOSE');
    PKG_PP_LOG.P_WRITE_MESSAGE('Error location: ' || L_LOC);
    PKG_PP_LOG.P_WRITE_MESSAGE('Error message: ' || SQLCODE || ' ' || SQLERRM);
    RETURN 'ERROR IN LOCATION: ' || L_LOC;

END F_LAM_CPR_CLOSE;
-- END KRD 1/31/17
-- KRD 2/1/17
FUNCTION F_BAL_LEASE_DEPR(
                          A_COMPANY_ID IN NUMBER
                         )
RETURN NUMBER
IS
L_STATUS VARCHAR2(10);

BEGIN
      L_STATUS := 'OK';

      PKG_PP_LOG.P_WRITE_MESSAGE('Balancing CPR to Depr Ledger');

      FOR i IN (
                SELECT depr_group_id, Sum(end_balance) end_bal, Sum(accum_cost) accum_cost
                FROM (
                      SELECT depr_ledger.depr_group_id,
                            end_balance ,
                            0 AS accum_cost ,
                            'depr_closed' AS TYPE,
                            to_char(gl_post_mo_yr, 'mm/yyyy') AS MONTH
                      FROM depr_ledger, depr_group
                      WHERE depr_ledger.set_of_books_id = 1
                      AND depr_ledger.depr_group_id = depr_group.depr_group_id
                      AND depr_group.company_id = A_COMPANY_ID
                      AND To_Char(depr_ledger.gl_post_mo_yr, 'mm/yyyy') =
                            (
                            SELECT to_char(max(accounting_month), 'mm/yyyy')
                            FROM cpr_control
                            WHERE cpr_control.company_id = A_COMPANY_ID
                            AND cpr_closed IS NOT NULL
                          )
                    UNION
                      SELECT depr_ledger.depr_group_id,
                            Nvl(additions,0) + nvl(retirements,0) +
                            Nvl(transfers_in,0)  + nvl(transfers_out,0) + nvl(adjustments,0) end_balance,
                            0 accum_cost,
                            'depr_open' TYPE,
                            To_Char(gl_post_mo_yr, 'mm/yyyy') AS MONTH
                      FROM  depr_ledger, cpr_control, depr_group
                      WHERE cpr_control.company_id = A_COMPANY_ID
                      AND   depr_ledger.set_of_books_id = 1
                      AND   cpr_closed IS NULL
                      AND   To_Char(gl_post_mo_yr, 'mm/yyyy')  =
                            To_Char(accounting_month, 'mm/yyyy')
                      AND depr_ledger.depr_group_id = depr_group.depr_group_id
                      AND depr_group.company_id = A_COMPANY_ID
                    UNION
                      SELECT DEPR_GROUP_ID,
                            0 end_balance,
                            Sum(CPR_LEDGER.ACCUM_COST),
                            'cpr' type,
                            'current'
                      FROM CPR_LEDGER
                      WHERE company_id = A_COMPANY_ID
                      AND   ledger_status < 100
                    GROUP BY DEPR_GROUP_ID
                  )
                GROUP BY depr_group_id
                HAVING Sum(end_balance) <> Sum(accum_cost)
               )
      LOOP
        PKG_PP_LOG.P_WRITE_MESSAGE('DEPR_GROUP_ID: ' || i.depr_group_id || ' is not in balance. CPR Total: $' || i.accum_cost || '    Depr Ledger Total: $' || i.end_bal);
        L_STATUS := 'OOB';

      END LOOP;

      IF L_STATUS = 'OOB' THEN
        PKG_PP_LOG.P_WRITE_MESSAGE(' ');
        PKG_PP_LOG.P_WRITE_MESSAGE('Review out of balance conditions before closing the month.');

        ROLLBACK;
        RETURN -1;

      END IF;

      RETURN 1;

END F_BAL_LEASE_DEPR;


FUNCTION F_BAL_LEASE_CPR(
                         A_COMPANY_ID IN NUMBER
                        )
RETURN NUMBER
IS
L_STATUS VARCHAR2(100);
L_CHECK2 VARCHAR2(100);

CAP_BASIS_INDICATOR NUMBER;
LDG_BASIS_AMT NUMBER;
ACT_BASIS_AMT NUMBER;

L_LDG_BASIS SYS_REFCURSOR;
L_ACT_BASIS SYS_REFCURSOR;

BEGIN
      L_CHECK2 := ' ';
      L_STATUS := 'OK';

      PKG_PP_LOG.P_WRITE_MESSAGE('Balancing CPR Ledger to CPR Activity');

      FOR i IN (
                SELECT cpr.asset_id, la.ls_asset_id, la.leased_asset_number, cpr.accum_cost, cpra.act_cost
                FROM (
                      SELECT asset_id, accum_cost
                      FROM cpr_ledger
                      WHERE subledger_indicator = -100
                      AND   company_id = A_COMPANY_ID
                    ) cpr,
                    (
                      SELECT asset_id, Sum(activity_cost) act_cost
                      FROM cpr_activity
                      GROUP BY asset_id
                    ) cpra,
                    ls_asset la,
                    ls_cpr_asset_map map
                WHERE cpr.asset_id = cpra.asset_id
                AND   cpr.asset_id = map.asset_id
                AND   la.ls_asset_id = map.ls_asset_id
                AND cpr.accum_cost <> cpra.act_cost
               )
      LOOP
        PKG_PP_LOG.P_WRITE_MESSAGE('Leased Asset Number: ' || i.leased_asset_number || ' does not balance. CPR Cost: $' || i.accum_cost || '     CPR Activity Total: $' || i.act_cost);

        L_STATUS := 'OOB';

      END LOOP;

      IF L_STATUS = 'OOB' THEN
        PKG_PP_LOG.P_WRITE_MESSAGE(' ');
        PKG_PP_LOG.P_WRITE_MESSAGE('Review out of balance conditions before closing the month.');

        ROLLBACK;
        RETURN -1;

      END IF;


      L_CHECK2 := ' ';
      L_STATUS := 'OK';


      PKG_PP_LOG.P_WRITE_MESSAGE('Balancing CPR Ledger to CPR Ledger Basis');

        SELECT book_summary_id
        INTO CAP_BASIS_INDICATOR
        FROM book_summary
        WHERE book_summary_type = 'LESSEE'
        AND InStr(Lower(summary_name),'capital',1,1) <> 0;

        FOR i IN (
                  SELECT cpr.asset_id, la.leased_asset_number, cpr.accum_cost
                  FROM cpr_ledger cpr,
                       ls_cpr_asset_map map,
                       ls_asset la
                  WHERE cpr.asset_id = map.asset_id
                  AND   map.ls_asset_id = la.ls_asset_id
                  AND   cpr.subledger_indicator = -100
                  AND   cpr.company_id = A_COMPANY_ID
                 )
        LOOP
          OPEN L_LDG_BASIS FOR 'select basis_' || CAP_BASIS_INDICATOR || ' from cpr_ldg_basis where asset_id = ' || i.asset_id;

          FETCH L_LDG_BASIS INTO LDG_BASIS_AMT;

          IF LDG_BASIS_AMT <> i.ACCUM_COST THEN
            PKG_PP_LOG.P_WRITE_MESSAGE('Leased Asset Number: ' || i.leased_asset_number || ' is not in balance. CPR Cost: $' || i.accum_cost || '     CPR Ledger Basis: $' || LDG_BASIS_AMT);
            L_STATUS := 'OOB';
          END IF;

        END LOOP;

      IF L_STATUS = 'OOB' THEN
        PKG_PP_LOG.P_WRITE_MESSAGE(' ');
        PKG_PP_LOG.P_WRITE_MESSAGE('Review out of balance conditions before closing the month.');

        ROLLBACK;
        RETURN -1;

      END IF;

      L_CHECK2 := ' ';
      L_STATUS := 'OK';


      PKG_PP_LOG.P_WRITE_MESSAGE('Balancing CPR Activity to CPR Activity Basis');

        FOR i IN (
                  SELECT cpra.asset_id, la.leased_asset_number, Sum(cpra.activity_cost) act_cost
                  FROM cpr_activity cpra,
                       ls_cpr_asset_map map,
                       ls_asset la
                  WHERE cpra.asset_id = map.asset_id
                  AND   map.ls_asset_id = la.ls_asset_id
                  AND   cpra.asset_id IN (SELECT asset_id FROM cpr_ledger WHERE subledger_indicator = -100 AND company_id = A_COMPANY_ID)
                  GROUP BY cpra.asset_id, la.leased_asset_number
                 )
        LOOP
          OPEN L_ACT_BASIS FOR 'select sum(basis_' || CAP_BASIS_INDICATOR || ') from cpr_act_basis where asset_id = ' || i.asset_id;

          FETCH L_ACT_BASIS INTO ACT_BASIS_AMT;

          IF ACT_BASIS_AMT <> i.act_cost THEN
            PKG_PP_LOG.P_WRITE_MESSAGE('Leased Asset Number: ' || i.leased_asset_number || ' is not in balance. CPR Activity Cost: $' || i.act_cost || '     CPR Activity Basis: $' || ACT_BASIS_AMT);
            L_STATUS := 'OOB';
          END IF;

        END LOOP;

      IF L_STATUS = 'OOB' THEN
        PKG_PP_LOG.P_WRITE_MESSAGE(' ');
        PKG_PP_LOG.P_WRITE_MESSAGE('Review out of balance conditions before closing the month.');

        ROLLBACK;
        RETURN -1;

      END IF;

      L_CHECK2 := ' ';
      L_STATUS := 'OK';


      PKG_PP_LOG.P_WRITE_MESSAGE('Balancing CPR Quantities to CPR Activity Quantities');

      FOR i IN (
                SELECT cpr.asset_id, la.ls_asset_id, la.leased_asset_number, cpr.cpr_quant, cpra.act_quant
                FROM (
                      SELECT asset_id, accum_quantity cpr_quant
                      FROM cpr_ledger
                      WHERE subledger_indicator = -100
                      AND   company_id = A_COMPANY_ID
                    ) cpr,
                    (
                      SELECT asset_id, Sum(activity_quantity) act_quant
                      FROM cpr_activity
                      GROUP BY asset_id
                    ) cpra,
                    ls_asset la,
                    ls_cpr_asset_map map
                WHERE cpr.asset_id = cpra.asset_id
                AND   cpr.asset_id = map.asset_id
                AND   la.ls_asset_id = map.ls_asset_id
                AND cpr.cpr_quant <> cpra.act_quant
               )
      LOOP
        PKG_PP_LOG.P_WRITE_MESSAGE('Leased Asset Number: ' || i.leased_asset_number || ' does not balance. CPR Quantity: ' || i.cpr_quant || '     CPR Activity Quantity: ' || i.act_quant);

        L_STATUS := 'OOB';

      END LOOP;

      IF L_STATUS = 'OOB' THEN
        PKG_PP_LOG.P_WRITE_MESSAGE(' ');
        PKG_PP_LOG.P_WRITE_MESSAGE('Review out of balance conditions before closing the month.');

        ROLLBACK;
        RETURN -1;

      END IF;

      L_CHECK2 := ' ';
      L_STATUS := 'OK';

RETURN 1;

END F_BAL_LEASE_CPR;
-- END KRD 2/1/17

--**************************************************************************
  --                            F_CURRENCY_GAIN_LOSS
  --             --------------------------------
  -- @@ DESCRIPTION
  --    This function will return the currency gain/loss for the given companies, month, and set of books.
  --    This is used to generate journal entries associated with the gain/loss.
  -- @@PARAMS
  --    t_num_array: a_company_ids
  --      The companies for which to return currency gain/loss
  --    date: a_month
  --       The month for which to return currency gain/loss
  -- @@RETURN
  --    currency_gain_loss_tbl PIPELINED:
  --       A pipelined pl/sql table containing the results of the calculation
  --**************************************************************************
  FUNCTION f_currency_gain_loss(a_company_ids     IN t_num_array,
                                a_month           IN DATE,
                                a_set_of_books_id IN NUMBER)
    RETURN currency_gain_loss_tbl
    PIPELINED IS
  BEGIN
    FOR item IN (SELECT v_ls_asset_schedule_fx_vw.company_id,
                        v_ls_asset_schedule_fx_vw.ilr_id,
                        ls_ilr.ilr_number,
                        v_ls_asset_schedule_fx_vw.ls_asset_id,
                        v_ls_asset_schedule_fx_vw.leased_asset_number,
                        v_ls_asset_schedule_fx_vw.iso_code,
                        v_ls_asset_schedule_fx_vw.currency_display_symbol,
                        v_ls_asset_schedule_fx_vw.gain_loss_fx,
                        v_ls_asset_schedule_fx_vw.month,
                        ls_ilr_account.currency_gain_loss_dr_acct_id,
                        ls_ilr_account.currency_gain_loss_cr_acct_id
                   FROM v_ls_asset_schedule_fx_vw
                   JOIN ls_lease_currency_type
                     ON v_ls_asset_schedule_fx_vw.ls_cur_type =
                        ls_lease_currency_type.ls_currency_type_id
                   JOIN ls_ilr
                     ON v_ls_asset_schedule_fx_vw.ilr_id = ls_ilr.ilr_id
                    AND ls_ilr.current_revision =
                        v_ls_asset_schedule_fx_vw.revision
                   JOIN ls_ilr_account
                     ON ls_ilr.ilr_id = ls_ilr_account.ilr_id
                  WHERE lower(ls_lease_currency_type.description) =
                        'company'
                    AND v_ls_asset_schedule_fx_vw.gain_loss_fx <> 0
                    AND v_ls_asset_schedule_fx_vw.set_of_books_id =
                        a_set_of_books_id
                    AND v_ls_asset_schedule_fx_vw.month = a_month
                    AND v_ls_asset_schedule_fx_vw.company_id MEMBER OF
                  a_company_ids) LOOP
      PIPE ROW(item);
    END LOOP;
    RETURN;
  END f_currency_gain_loss;

--**************************************************************************
  --                            F_CURRENCY_GAIN_LOSS_APPROVE
  --             --------------------------------
  -- @@ DESCRIPTION
  --    This function will approve and post the monthly gain/loss entries resulting from
  --         fluctuations in currency exchange rates.
  -- @@PARAMS
  --    date: a_month
  --       The month to process currency gain/loss for
  -- @@RETURN
  --    varchar2: A message back to the caller
  --       'OK' = SUCCESS
  --       all else = FAILURE
  --
  --**************************************************************************
  FUNCTION f_currency_gain_loss_approve(a_company_id      IN NUMBER,
                                        a_month           IN DATE,
                                        a_set_of_books_id IN NUMBER,
                                        a_end_log         IN NUMBER := NULL)
    RETURN VARCHAR2 IS
    l_status     VARCHAR(30000);
    l_location   VARCHAR2(30000);
    l_rtn        VARCHAR2(4000);
    l_counter    NUMBER;
    l_gl_je_code VARCHAR2(35);
  BEGIN
    pkg_pp_log.p_start_log(p_process_id => pkg_lease_common.f_getprocess);
    pkg_pp_log.p_write_message('Begin Currency Gain/Loss Journals Process - Company ID: ' ||
                               a_company_id || ' - Month: ' ||
                               to_char(a_month, 'MON-yyyy'));

    --Need to determine correct values for this
    SELECT nvl(e.external_je_code, nvl(e.gl_je_code, 'LEASECURGL'))
      INTO l_gl_je_code
      FROM gl_je_control g
      JOIN standard_journal_entries e
        ON G.je_id = E.je_id
       AND g.process_id = 'LEASECURGL';

    l_counter := 0;

    l_rtn := 0;

    pkg_pp_log.p_write_message('Sending Currency Gain/Loss Journals');

    FOR l_asset_info IN (SELECT v.ls_asset_id,
                                v.gain_loss_fx AS amount,
                                0 AS asset_act_id,
                                -1 AS dg_id,
                                a.work_order_id,
                                0 AS gain_loss,
                                -1 AS pend_trans_id,
                                v.company_id,
                                V.MONTH,
                                l_gl_je_code AS gl_je_code,
                                a_set_of_books_id AS set_of_books_id,
                                v.currency_gain_loss_dr_acct_id,
                                v.currency_gain_loss_cr_acct_id
                           FROM TABLE(f_currency_gain_loss(t_num_array(a_company_id),
                                                           a_month,
                                                           a_set_of_books_id)) v
                           JOIN ls_asset a
                             ON V.ls_asset_id = A.ls_asset_id) LOOP

      IF a_set_of_books_id IS NULL THEN
        pkg_pp_log.p_write_message(l_location || ' : ' || CHR(10) || 'NULL set of books ID passed to pkg_lease_calc.f_currency_gain_loss_approved.');
        l_rtn := -1;
      end if;

      IF a_company_id IS NULL THEN
        pkg_pp_log.p_write_message(l_location || ' : ' || CHR(10) || 'NULL company ID passed to pkg_lease_calc.f_currency_gain_loss_approved.');
        l_rtn := -1;
      end if;


      IF a_month IS NULL THEN
        pkg_pp_log.p_write_message(l_location || ' : ' || CHR(10) || 'NULL month passed to pkg_lease_calc.f_currency_gain_loss_approved.');
        l_rtn := -1;
      end if;

      IF l_asset_info.ls_asset_id IS NULL THEN
        pkg_pp_log.p_write_message(l_location || ' : ' || CHR(10) || 'NULL asset ID in pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
                                    ', ' || a_month || ', ' || a_set_of_books_id ||'). Ensure all asset_ids are populated.');
        l_rtn := -1;
      END IF;

      IF l_asset_info.amount IS NULL THEN
        pkg_pp_log.p_write_message(l_location || ' : ' || CHR(10) || 'NULL gain_loss_fx in pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
                                    ', ' || a_month || ', ' || a_set_of_books_id || '). Ensure all amounts are populated.');
        l_rtn := -1;
      end if;

      IF l_asset_info.work_order_id IS NULL THEN
        pkg_pp_log.p_write_message(l_location || ' : ' || CHR(10) || 'NULL work_order_id in pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
                                    ', ' || a_month || ', ' || a_set_of_books_id || '). Ensure all work_order_ids are populated.');
            l_rtn := -1;
      end if;

      IF l_asset_info.company_id IS NULL THEN
        pkg_pp_log.p_write_message(l_location || ' : ' || CHR(10) || 'NULL company_id in pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
                                    ', ' || a_month || ', ' || a_set_of_books_id || '). Ensure all company_ids are populated.');
        l_rtn := -1;
      end if;


      IF l_asset_info.month IS NULL THEN
        pkg_pp_log.p_write_message(l_location || ' : ' || CHR(10) || 'NULL month in pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
                                    ', ' || a_month || ', ' || a_set_of_books_id || '). Ensure all months are populated.');
        l_rtn := -1;
      end if;

      IF l_asset_info.currency_gain_loss_dr_acct_id IS NULL THEN
        pkg_pp_log.p_write_message(l_location || ' : ' || CHR(10) || 'NULL currency_gain_loss_dr_acct_id in ' ||
                                    'pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
                                    ', ' || a_month || ', ' || a_set_of_books_id || '). Ensure all currency_gain_loss_dr_account_ids are populated. (Check ls_ilr_account)');
        l_rtn := -1;
      end if;

      IF l_asset_info.currency_gain_loss_cr_acct_id IS NULL THEN
        pkg_pp_log.p_write_message(l_location || ' : ' || CHR(10) || 'NULL currency_gain_loss_cr_acct_id in ' ||
                                    'pkg_lease_calc.f_currency_gain_loss(t_num_array(' || a_company_id ||
                                    ', ' || a_month || ', ' || a_set_of_books_id || '). Ensure all currency_gain_loss_cr_account_ids are populated. (Check ls_ilr_account)');
        l_rtn := -1;
      end if;

      IF l_rtn = -1 THEN
        pkg_pp_log.p_end_log();
        RETURN l_location || ' : ' || 'Invalid parameters';
      END IF;
      --process the debit
      l_location := 'Processing ls_asset_id: ' ||
                    to_char(l_asset_info.ls_asset_id) ||
                    ' trans type: 3051' || ' gl_acct: ' || l_asset_info.currency_gain_loss_dr_acct_id;

      l_rtn := pkg_lease_common.f_bookje(l_asset_info.ls_asset_id,
                                         3051,
                                         l_asset_info.amount,
                                         l_asset_info.asset_act_id,
                                         l_asset_info.dg_id,
                                         l_asset_info.work_order_id,
                                         l_asset_info.currency_gain_loss_dr_acct_id,
                                         l_asset_info.gain_loss,
                                         l_asset_info.pend_trans_id,
                                         l_asset_info.company_id,
                                         l_asset_info.month,
                                         1,
                                         l_asset_info.gl_je_code,
                                         l_asset_info.set_of_books_id,
                                         l_status);

      IF l_rtn = -1 THEN
        pkg_pp_log.p_write_message(l_location || ' : ' || CHR(10) || l_status);
        pkg_pp_log.p_end_log();
        RETURN l_location || ' : ' || l_status;
      END IF;

      --process the credit
      l_location := 'Processing ls_asset_id: ' ||
                    to_char(l_asset_info.ls_asset_id) || 'trans type: 3052';

      l_rtn := pkg_lease_common.f_bookje(l_asset_info.ls_asset_id,
                                         3052,
                                         l_asset_info.amount,
                                         l_asset_info.asset_act_id,
                                         l_asset_info.dg_id,
                                         l_asset_info.work_order_id,
                                         l_asset_info.currency_gain_loss_cr_acct_id,
                                         l_asset_info.gain_loss,
                                         l_asset_info.pend_trans_id,
                                         l_asset_info.company_id,
                                         l_asset_info.month,
                                         0,
                                         l_asset_info.gl_je_code,
                                         l_asset_info.set_of_books_id,
                                         l_status);

      IF l_rtn = -1 THEN
        pkg_pp_log.p_write_message(l_location || ' : ' || l_status);
        pkg_pp_log.p_end_log();
        RETURN l_location || ' : ' || l_status;
      END IF;

      l_counter := l_counter + l_rtn;
    END LOOP;

    pkg_pp_log.p_write_message(l_counter || ' JE pairs sent.');

    IF a_end_log = 1 THEN
      pkg_pp_log.p_end_log();
    END IF;

    RETURN 'OK';

  EXCEPTION
    WHEN OTHERS THEN
      RETURN l_status || ' : ' || SQLERRM;

END f_currency_gain_loss_approve;
--**************************************************************************
--                            Initialize Package
--**************************************************************************
begin
   L_ILR_ID := 0;

END pkg_lease_calc;
/

--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (5262, 0, 2017, 3, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2017.3.0.0_PKG_LEASE_CALC.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
