  CREATE OR REPLACE PACKAGE "PWRPLANT"."PP_PARSE" AS
   G_PKG_VERSION varchar(35) := '2018.2.0.0';
   PROCEDURE parse_table(cur      IN INTEGER,
                         a_name   IN VARCHAR2,
                         linefeed IN NUMBER);
END pp_parse;
/

CREATE OR REPLACE PACKAGE BODY "PWRPLANT"."PP_PARSE" AS
   PROCEDURE parse_table(cur      IN INTEGER,
                         a_name   IN VARCHAR2,
                         linefeed IN NUMBER) IS
      CURSOR tablist_cur IS
         SELECT sql_str
           FROM temp_procedure_list
          WHERE NAME = a_name
          ORDER BY line_no;
      lines       dbms_sql.varchar2s;
      table_count INT;
      str         VARCHAR2(254);
   BEGIN
      table_count := 0;
      OPEN tablist_cur;
      LOOP
         FETCH tablist_cur
            INTO str;
         IF (tablist_cur%NOTFOUND)
         THEN
            EXIT;
         END IF;
         table_count := table_count + 1;
         lines(table_count) := str;
      END LOOP;
      CLOSE tablist_cur;
      dbms_sql.parse(cur,
                     lines,
                     1,
                     table_count,
                     TRUE,
                     2);
   END parse_table;
END pp_parse;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (16305, 0, 2018, 2, 0, 0, 0, 'c:\plasticwks\powerplant\sql\packages', 
    'PP_PARSE.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
