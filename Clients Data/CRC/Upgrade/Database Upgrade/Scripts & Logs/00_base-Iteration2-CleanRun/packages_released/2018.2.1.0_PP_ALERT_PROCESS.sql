/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036012_system_PP_ALERT_PROCESS.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/27/2014 Chris Mardis
||============================================================================
*/

create or replace package PP_ALERT_PROCESS is
  
   -- Author  : RROACH
   -- Created : 7/3/2013 12:22:24 PM
   -- Purpose :
   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   G_PROCESS_NAME varchar2(100);
   G_SERVER_TYPE  boolean;
   G_ALERT        varchar2(100);

   function INITIALIZE(A_NAME varchar2,
                       A_TYPE varchar2) return pls_integer;

   function MESSAGEBOX(A_MSG      varchar2,
                       A_MSG_TYPE varchar2) return pls_integer;

   procedure STATUSBOX(A_MSG varchar2);

   function F_WO_VALIDATION_MODIFY(A_VALIDATION_TYPE_DESC varchar2,
                                   A_OBJECT               varchar2,
                                   A_RUN_SYNTAX           varchar2) return pls_integer;
   function REGISTER(A_ALERT varchar2) return pls_integer;

   function WAITONE(A_ALERT   varchar2,
                    A_MSG     out varchar2,
                    A_STATUS  out integer,
                    A_TIMEOUT in number default DBMS_ALERT.MAXWAIT) return pls_integer;

   function WAITANY(A_ALERT   out varchar2,
                    A_MSG     out varchar2,
                    A_STATUS  out integer,
                    A_TIMEOUT in number default DBMS_ALERT.MAXWAIT) return pls_integer;

   function SIGNAL(A_ALERT varchar2,
                   A_MSG   varchar2) return pls_integer;

   function REMOVE(A_ALERT varchar2) return pls_integer;

   function REMOVEALL return pls_integer;

   function SET_DEFAULTS(A_SENSITIVITY in number) return pls_integer;

end PP_ALERT_PROCESS;
/

create or replace package body PP_ALERT_PROCESS is

   -- Author  : RROACH
   -- Created : 7/3/2013 12:22:24 PM
   -- Purpose :

   /*****************************  INITIALIZE *********************************/

   function INITIALIZE(A_NAME varchar2,
                       A_TYPE varchar2) return pls_integer is

      L_CODE       pls_integer;
      L_STATUS     pls_integer;
      L_ALERT_NAME varchar2(100);
      cursor SERVER_ALERT_CUR is
         select ALERT || '_' || USERENV('sessionid'), SERVER_ALERT || '_' || USERENV('sessionid')
           from PP_ALERT_PROCESSES
          where PROCESS_NAME = G_PROCESS_NAME;

   begin
      G_PROCESS_NAME := A_NAME;
      if A_TYPE = 'SERVER' then
         G_SERVER_TYPE := true;
      else
         G_SERVER_TYPE := false;
      end if;
      open SERVER_ALERT_CUR;
      loop
         fetch SERVER_ALERT_CUR
            into G_ALERT, L_ALERT_NAME;
         exit when SERVER_ALERT_CUR%notfound;
         L_STATUS := REGISTER(L_ALERT_NAME);
      end loop;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'pp_alert_process initialize ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end INITIALIZE;

   /*****************************  Messagebox *********************************/
   function MESSAGEBOX(A_MSG      varchar2,
                       A_MSG_TYPE varchar2) return pls_integer is

      L_CODE         pls_integer;
      L_STATUS       pls_integer;
      L_SERVER_ALERT varchar2(100);
      L_MSG          varchar(50);
      pragma autonomous_transaction;

   begin

      select SERVER_ALERT || '_' || USERENV('sessionid')
        into L_SERVER_ALERT
        from PP_ALERT_PROCESSES
       where ALERT || '_' || USERENV('sessionid') = G_ALERT;
      L_STATUS := 1;
      while L_STATUS = 1
      loop
         L_STATUS := SIGNAL(G_ALERT, UPPER(A_MSG_TYPE) || ':' || A_MSG);
         L_CODE   := WAITONE(L_SERVER_ALERT, L_MSG, L_STATUS, 30);
         insert into PP_ALERT_PROCESS_LOG
            (PROCESS, ALERT, SESSIONID, LOG_DATE, STATUS)
         values
            (G_PROCESS_NAME, G_ALERT, USERENV('SESSIONID'), sysdate, 'Waiting');
         commit;
      end loop;
      insert into PP_ALERT_PROCESS_LOG
         (PROCESS, ALERT, SESSIONID, LOG_DATE, STATUS)
      values
         (G_PROCESS_NAME, G_ALERT, USERENV('SESSIONID'), sysdate, 'ACK');
      commit;
      case
         when UPPER(A_MSG_TYPE) = 'OK' then
            return 1;
         when UPPER(A_MSG_TYPE) = 'OKCANCEL' then
            if L_MSG = 'OK' then
               return 1;
            else
               return 2;
            end if;
         when UPPER(A_MSG_TYPE) = 'YESNO' then
            if L_MSG = 'YES' then
               return 1;
            else
               return 2;
            end if;
         when UPPER(A_MSG_TYPE) = 'YESNOCANCEL' then
            if L_MSG = 'YES' then
               return 1;
            elsif L_MSG = 'NO' then
               return 2;
            else
               return 3;
            end if;
         when UPPER(A_MSG_TYPE) = 'ABORTRETRYIGNORE' then
            if L_MSG = 'ABORT' then
               return 1;
            elsif L_MSG = 'RETRY' then
               return 2;
            else
               return 3;
            end if;
      end case;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'pp_alert_process messagebox ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end MESSAGEBOX;

   /*****************************  Statusbox *********************************/
   procedure STATUSBOX(A_MSG varchar2) is

      L_CODE         pls_integer;
      L_STATUS       pls_integer;
      L_SERVER_ALERT varchar2(100);
      L_MSG          varchar(50);
      pragma autonomous_transaction;

   begin

      select SERVER_ALERT || '_' || USERENV('sessionid')
        into L_SERVER_ALERT
        from PP_ALERT_PROCESSES
       where ALERT || '_' || USERENV('sessionid') = G_ALERT;
      L_STATUS := 1;
      while L_STATUS = 1
      loop
         L_STATUS := SIGNAL(G_ALERT, 'STATUSBOX' || ':' || A_MSG);
         L_CODE   := WAITONE(L_SERVER_ALERT, L_MSG, L_STATUS, 30);
         insert into PP_ALERT_PROCESS_LOG
            (PROCESS, ALERT, SESSIONID, LOG_DATE, STATUS)
         values
            (G_PROCESS_NAME, G_ALERT, USERENV('SESSIONID'), sysdate, 'Waiting');
         commit;
      end loop;
      insert into PP_ALERT_PROCESS_LOG
         (PROCESS, ALERT, SESSIONID, LOG_DATE, STATUS)
      values
         (G_PROCESS_NAME, G_ALERT, USERENV('SESSIONID'), sysdate, 'ACK');
      commit;
      return;

   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'pp_alert_process statusbox ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return;
   end STATUSBOX;

   function F_WO_VALIDATION_MODIFY(A_VALIDATION_TYPE_DESC varchar2,
                                   A_OBJECT               varchar2,
                                   A_RUN_SYNTAX           varchar2) return pls_integer is

      L_CODE         pls_integer;
      L_STATUS       pls_integer;
      L_SERVER_ALERT varchar2(100);
      L_MSG          varchar(50);
      pragma autonomous_transaction;

   begin

      select SERVER_ALERT || '_' || USERENV('sessionid')
        into L_SERVER_ALERT
        from PP_ALERT_PROCESSES
       where ALERT || '_' || USERENV('sessionid') = G_ALERT;
      L_STATUS := 1;
      while L_STATUS = 1
      loop
         L_STATUS := SIGNAL(G_ALERT,
                            'VALIDATION_MODIFY' || ':' || A_VALIDATION_TYPE_DESC || '|' || A_OBJECT || '|' ||
                            A_RUN_SYNTAX);
         L_CODE   := WAITONE(L_SERVER_ALERT, L_MSG, L_STATUS, 30);
         insert into PP_ALERT_PROCESS_LOG
            (PROCESS, ALERT, SESSIONID, LOG_DATE, STATUS)
         values
            (G_PROCESS_NAME, G_ALERT, USERENV('SESSIONID'), sysdate, 'Waiting');
         commit;
      end loop;
      insert into PP_ALERT_PROCESS_LOG
         (PROCESS, ALERT, SESSIONID, LOG_DATE, STATUS)
      values
         (G_PROCESS_NAME, G_ALERT, USERENV('SESSIONID'), sysdate, 'ACK');
      commit;
      return 1;

   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'pp_alert_process statusbox ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end F_WO_VALIDATION_MODIFY;

   /*****************************  REGISTER *********************************/
   function REGISTER(A_ALERT varchar2) return pls_integer is

      L_CODE pls_integer;

   begin
      SYS.DBMS_ALERT.REGISTER(A_ALERT);
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'pp_alert_process register ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end REGISTER;

   /***************************** WAITONE *********************************/
   function WAITONE(A_ALERT   varchar2,
                    A_MSG     out varchar2,
                    A_STATUS  out integer,
                    A_TIMEOUT in number default DBMS_ALERT.MAXWAIT) return pls_integer is

      L_CODE   pls_integer;
      L_STATUS pls_integer;

   begin
      SYS.DBMS_ALERT.WAITONE(A_ALERT, A_MSG, A_STATUS, A_TIMEOUT);
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'pp_alert_process waitone ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end WAITONE;

   /***************************** WAITANY *********************************/
   function WAITANY(A_ALERT   out varchar2,
                    A_MSG     out varchar2,
                    A_STATUS  out integer,
                    A_TIMEOUT in number default DBMS_ALERT.MAXWAIT) return pls_integer is

      L_CODE   pls_integer;
      L_STATUS pls_integer;

   begin
      SYS.DBMS_ALERT.WAITANY(A_ALERT, A_MSG, A_STATUS, A_TIMEOUT);
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'pp_alert_process waitany ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end WAITANY;

   /***************************** SIGNAL *********************************/
   function SIGNAL(A_ALERT varchar2,
                   A_MSG   varchar2) return pls_integer is

      L_CODE   pls_integer;
      L_STATUS pls_integer;
      pragma autonomous_transaction;

   begin
      SYS.DBMS_ALERT.SIGNAL(A_ALERT, A_MSG);
      commit;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'pp_alert_process signal ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SIGNAL;

   /***************************** REMOVE *********************************/
   function REMOVE(A_ALERT varchar2) return pls_integer is

      L_CODE   pls_integer;
      L_STATUS pls_integer;

   begin
      DBMS_ALERT.REMOVE(A_ALERT);
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'pp_alert_process remove ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end REMOVE;

   /***************************** REMOVE *********************************/
   function REMOVEALL return pls_integer is

      L_CODE   pls_integer;
      L_STATUS pls_integer;

   begin
      DBMS_ALERT.REMOVEALL;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'pp_alert_process removeall ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end REMOVEALL;

   /***************************** SET_DEFAULTS*********************************/
   function SET_DEFAULTS(A_SENSITIVITY in number) return pls_integer is

      L_CODE   pls_integer;
      L_STATUS pls_integer;

   begin
      DBMS_ALERT.SET_DEFAULTS(A_SENSITIVITY);
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20021,
                                 'pp_alert_process set_defaults ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end SET_DEFAULTS;

end PP_ALERT_PROCESS;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (18211, 0, 2018, 2, 1, 0, 0, 'C:\BitBucketRepos\Classic_PB\scripts\00_base\packages', 
    'PP_ALERT_PROCESS.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
