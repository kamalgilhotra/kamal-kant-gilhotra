/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037642_system_PKG_PP_COMMON.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.1.0 07/08/2013 Daniel Motter    Point release
|| 10.4.1.2 10/10/2013 C. Shilling      Add P_AUTONOMOUS_SQL
|| 10.4.2.0 01/15/2014 Brandon Beck
|| 10.4.2.0 03/17/2014 Brandon Beck
|| 10.4.3.0 05/08/2014 Shane "C" Ward   Update PKG_PP_COMMON to include p_set_ssp_proxy
||============================================================================
*/

create or replace package PKG_PP_COMMON as
   G_PKG_VERSION varchar(35) := '2018.1.0.0';
	--associative arrays (index-by tables)
   type NUM_TABTYPE is table of number index by binary_integer;
   type DATE_TABTYPE is table of date index by binary_integer;
   type INT_TABTYPE is table of pls_integer index by binary_integer;
   type VARCHAR_TABTYPE is table of varchar2(2000) index by binary_integer;

   	--Nested Tables
   type NUMBER_ARR is table of number;

   type PP_SYSTEM_CONTROL_ARR is table of PP_SYSTEM_CONTROL.CONTROL_VALUE%type index by PP_SYSTEM_CONTROL.CONTROL_NAME%type;

   SYSTEM_CONTROLS PP_SYSTEM_CONTROL_ARR;

   G_USER_ID varchar2(30) := user;

   subtype G_YES_NO_TYPE is char(1) not null;
   G_YES constant G_YES_NO_TYPE := 'Y';
   G_NO  constant G_YES_NO_TYPE := 'N';

   subtype G_TRUE_FALSE_TYPE is pls_integer range 0 .. 1 not null;
   G_TRUE  constant G_TRUE_FALSE_TYPE := 1;
   G_FALSE constant G_TRUE_FALSE_TYPE := 0;

   subtype G_RESULT_TYPE is pls_integer range 1 .. 3 not null;
   G_SUCCESS constant G_RESULT_TYPE := 1;
   G_FAIL    constant G_RESULT_TYPE := 2;
   G_WARNING constant G_RESULT_TYPE := 3;

   procedure P_INITIALIZE_SYSTEM_CONTROLS;

   procedure P_AUTONOMOUS_SQL(P_SQL_IN in varchar2);

   function F_CLEAN_STRING(P_IN_STRING in varchar2) return varchar2;

   function F_CLIENT_EXTENSION_EXISTS(P_IN_STRING in varchar2) return number RESULT_CACHE;

   procedure P_SET_SSP_PROXY(A_PROXY varchar2,
                             A_USERS PKG_PP_COMMON.VARCHAR_TABTYPE);

end PKG_PP_COMMON;
/

create or replace package body PKG_PP_COMMON as

   /*******************Initialize system controils ***************************/
   procedure P_INITIALIZE_SYSTEM_CONTROLS is

   begin
      if SYSTEM_CONTROLS.COUNT > 0 then
         SYSTEM_CONTROLS.DELETE;
      end if;

      for REC in (select CONTROL_NAME, CONTROL_VALUE from PP_SYSTEM_CONTROL)
      loop
         SYSTEM_CONTROLS(trim(UPPER(REC.CONTROL_NAME))) := REC.CONTROL_VALUE;
      end loop;
   end P_INITIALIZE_SYSTEM_CONTROLS;

   /********** Clean_String ***************************************************/
   -- This function takes in a SQL statement and executes it without doing
   --    any validations or sanitation on the statement itself. So BE VERY CAREFUL
   --    that the SQL is OK to execute before calling this function.
   -- IF the SQL executes successfully, changes will be committed.
   -- ELSE a rollback is issued and the error is returned to the caller
   procedure P_AUTONOMOUS_SQL(P_SQL_IN in varchar2) is
      pragma autonomous_transaction;

   begin
      if P_SQL_IN is not null then
         begin
            execute immediate P_SQL_IN;
         exception
            when others then
               rollback;
               raise;
         end;
      end if;

      commit;
   end P_AUTONOMOUS_SQL;

   /********** Clean_String ***************************************************/
   function F_CLEAN_STRING(P_IN_STRING in varchar2) return varchar2 as

      V_BAD_CHARS varchar2(200) := '/ -';

   begin

      return TRANSLATE(P_IN_STRING, V_BAD_CHARS, LPAD('_', LENGTH(V_BAD_CHARS), '_'));

   end F_CLEAN_STRING;

   function F_CLIENT_EXTENSION_EXISTS(P_IN_STRING in varchar2) return number RESULT_CACHE RELIES_ON(PP_CLIENT_EXTENSIONS) is
      L_EXISTS number;
   begin
      select count(1)
        into L_EXISTS
        from PP_CLIENT_EXTENSIONS
       where UPPER(FUNCTION_NAME) = UPPER(P_IN_STRING)
         and IS_ACTIVE = 1;

      return L_EXISTS;

   exception
      when others then
         return 0;
   end F_CLIENT_EXTENSION_EXISTS;

   /************* Proxy User Assign *******************************************/
   procedure P_SET_SSP_PROXY(A_PROXY varchar2,
                             A_USERS PKG_PP_COMMON.VARCHAR_TABTYPE) is

   begin

      for I in A_USERS.FIRST .. A_USERS.LAST
      loop
         execute immediate 'alter user ' || A_USERS(I) || ' grant connect through ' || A_PROXY;
      end loop;

   end P_SET_SSP_PROXY;

   /************* Generic Get Next Sequence  *******************************************/
	FUNCTION get_Next_Seq_Val (
		  p_sequence_name VARCHAR2 )
	   RETURN INTEGER
	AS
	   v_nextval INTEGER;
	   v_select  VARCHAR2 ( 100 ) ;
	BEGIN
	   v_select := 'select '||p_sequence_name||'.nextval from dual';

	   EXECUTE immediate v_select INTO v_nextval;

	   --dbms_output.put_line('Nextval is: '||TO_CHAR(v_nextval));
	   RETURN v_nextval;
	END;

	FUNCTION get_Next_Change_Script_Seq_Val
	   RETURN INTEGER
	AS
	BEGIN
	   RETURN get_Next_Seq_Val ( 'CHANGE_SCRIPT_SEQ' ) ;
	END;

/****************** Initialization ********************************************/
begin
   G_USER_ID := user;

   P_INITIALIZE_SYSTEM_CONTROLS;

end PKG_PP_COMMON;
/

--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (11665, 0, 2018, 1, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2018.1.0.0_PKG_PP_COMMON.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
