/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036012_system_PP_PARM_TRANSLATE.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/27/2014 Chris Mardis
||============================================================================
*/

create or replace package PP_PARM_TRANSLATE as
   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   type LONG_ARRAY is table of number;
   type STRING_ARRAY is table of varchar2(32767); --or clob?
   type INTEGER_ARRAY is table of integer;
   type DECIMAL_ARRAY is table of number;
   type DOUBLE_ARRAY is table of number;
   type DATE_ARRAY is table of date;
   type DATETIME_ARRAY is table of date;
   type BOOLEAN_ARRAY is table of boolean;

   type S_PARM_ARRAYS is record(
      LONG_ARG      LONG_ARRAY,
      LONG_ARG2     LONG_ARRAY,
      LONG_ARG3     LONG_ARRAY,
      LONG_ARG4     LONG_ARRAY,
      LONG_ARG5     LONG_ARRAY,
      STRING_ARG    STRING_ARRAY,
      STRING_ARG2   STRING_ARRAY,
      STRING_ARG3   STRING_ARRAY,
      STRING_ARG4   STRING_ARRAY,
      STRING_ARG5   STRING_ARRAY,
      INTEGER_ARG   INTEGER_ARRAY,
      INTEGER_ARG2  INTEGER_ARRAY,
      INTEGER_ARG3  INTEGER_ARRAY,
      INTEGER_ARG4  INTEGER_ARRAY,
      INTEGER_ARG5  INTEGER_ARRAY,
      DECIMAL_ARG   DECIMAL_ARRAY,
      DECIMAL_ARG2  DECIMAL_ARRAY,
      DECIMAL_ARG3  DECIMAL_ARRAY,
      DECIMAL_ARG4  DECIMAL_ARRAY,
      DECIMAL_ARG5  DECIMAL_ARRAY,
      DOUBLE_ARG    DOUBLE_ARRAY,
      DOUBLE_ARG2   DOUBLE_ARRAY,
      DOUBLE_ARG3   DOUBLE_ARRAY,
      DOUBLE_ARG4   DOUBLE_ARRAY,
      DOUBLE_ARG5   DOUBLE_ARRAY,
      DATE_ARG      DATE_ARRAY,
      DATE_ARG2     DATE_ARRAY,
      DATE_ARG3     DATE_ARRAY,
      DATE_ARG4     DATE_ARRAY,
      DATE_ARG5     DATE_ARRAY,
      DATETIME_ARG  DATETIME_ARRAY,
      DATETIME_ARG2 DATETIME_ARRAY,
      DATETIME_ARG3 DATETIME_ARRAY,
      DATETIME_ARG4 DATETIME_ARRAY,
      DATETIME_ARG5 DATETIME_ARRAY,
      BOOLEAN_ARG   BOOLEAN_ARRAY,
      BOOLEAN_ARG2  BOOLEAN_ARRAY,
      BOOLEAN_ARG3  BOOLEAN_ARRAY,
      BOOLEAN_ARG4  BOOLEAN_ARRAY,
      BOOLEAN_ARG5  BOOLEAN_ARRAY);

   type S_PARM_LABELS is record(
      LONG_LABEL      varchar2(4000),
      LONG_LABEL2     varchar2(4000),
      LONG_LABEL3     varchar2(4000),
      LONG_LABEL4     varchar2(4000),
      LONG_LABEL5     varchar2(4000),
      STRING_LABEL    varchar2(4000),
      STRING_LABEL2   varchar2(4000),
      STRING_LABEL3   varchar2(4000),
      STRING_LABEL4   varchar2(4000),
      STRING_LABEL5   varchar2(4000),
      INTEGER_LABEL   varchar2(4000),
      INTEGER_LABEL2  varchar2(4000),
      INTEGER_LABEL3  varchar2(4000),
      INTEGER_LABEL4  varchar2(4000),
      INTEGER_LABEL5  varchar2(4000),
      DECIMAL_LABEL   varchar2(4000),
      DECIMAL_LABEL2  varchar2(4000),
      DECIMAL_LABEL3  varchar2(4000),
      DECIMAL_LABEL4  varchar2(4000),
      DECIMAL_LABEL5  varchar2(4000),
      DOUBLE_LABEL    varchar2(4000),
      DOUBLE_LABEL2   varchar2(4000),
      DOUBLE_LABEL3   varchar2(4000),
      DOUBLE_LABEL4   varchar2(4000),
      DOUBLE_LABEL5   varchar2(4000),
      DATE_LABEL      varchar2(4000),
      DATE_LABEL2     varchar2(4000),
      DATE_LABEL3     varchar2(4000),
      DATE_LABEL4     varchar2(4000),
      DATE_LABEL5     varchar2(4000),
      DATETIME_LABEL  varchar2(4000),
      DATETIME_LABEL2 varchar2(4000),
      DATETIME_LABEL3 varchar2(4000),
      DATETIME_LABEL4 varchar2(4000),
      DATETIME_LABEL5 varchar2(4000),
      BOOLEAN_LABEL   varchar2(4000),
      BOOLEAN_LABEL2  varchar2(4000),
      BOOLEAN_LABEL3  varchar2(4000),
      BOOLEAN_LABEL4  varchar2(4000),
      BOOLEAN_LABEL5  varchar2(4000));

   function STRUCTS_TO_STRINGS(ARGS   in S_PARM_ARRAYS,
                               LABELS in S_PARM_LABELS) return varchar2;
   procedure STRING_TO_STRUCTS(JSON   in varchar2,
                               ARGS   in out S_PARM_ARRAYS,
                               LABELS in out S_PARM_LABELS);

end PP_PARM_TRANSLATE;
/


create or replace package body PP_PARM_TRANSLATE as

   function SANITIZE_STRING(ASTRING in varchar2) return varchar2 is
      RETVAL varchar2(32767);
   begin
      RETVAL := replace(ASTRING, '{', '~ob~');
      RETVAL := replace(RETVAL, '}', '~cb~');
      RETVAL := replace(RETVAL, ',', '~comma~');
      RETVAL := replace(RETVAL, '\', '~slash~');
      RETVAL := replace(RETVAL, '"', '\"');
      return RETVAL;
   end;

   function UNSANITIZE_STRING(ASTRING in varchar) return varchar2 is
      RETVAL varchar2(32767);
   begin
      RETVAL := replace(ASTRING, '\"', '"');
      RETVAL := replace(RETVAL, '~ob~', '{');
      RETVAL := replace(RETVAL, '~cb~', '}');
      RETVAL := replace(RETVAL, '~comma~', ',');
      RETVAL := replace(RETVAL, '~slash~', '\');
      return RETVAL;
   end;

   function STRING_TO_STRING_ARRAY(ASTRING in varchar2) return STRING_ARRAY is
      RETVAL     STRING_ARRAY;
      UNIT_START number;
      UNIT_STOP  number;
      UNIT_VAL   varchar2(32767);
      IDX        number;
   begin
      RETVAL := STRING_ARRAY();

      IDX        := 1;
      UNIT_START := 1;
      UNIT_STOP  := INSTR(ASTRING, ',');

      while UNIT_STOP > 0
      loop
         UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START, UNIT_STOP - UNIT_START));
         RETVAL.EXTEND(1);
         RETVAL(IDX) := UNSANITIZE_STRING(UNIT_VAL);

         UNIT_START := UNIT_STOP + 1;
         UNIT_STOP  := INSTR(ASTRING, ',', UNIT_START);
         IDX        := IDX + 1;
      end loop;

      UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START));
      RETVAL.EXTEND(1);
      RETVAL(IDX) := UNSANITIZE_STRING(UNIT_VAL);

      return RETVAL;
   end;

   function STRING_TO_LONG_ARRAY(ASTRING in varchar2) return LONG_ARRAY is
      RETVAL     LONG_ARRAY;
      UNIT_START number;
      UNIT_STOP  number;
      UNIT_VAL   varchar2(255);
      IDX        number;
   begin
      RETVAL := LONG_ARRAY();

      IDX        := 1;
      UNIT_START := 1;
      UNIT_STOP  := INSTR(ASTRING, ',');

      while UNIT_STOP > 0
      loop
         UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START, UNIT_STOP - UNIT_START));
         RETVAL.EXTEND(1);
         RETVAL(IDX) := TO_NUMBER(UNIT_VAL);

         UNIT_START := UNIT_STOP + 1;
         UNIT_STOP  := INSTR(ASTRING, ',', UNIT_START);
         IDX        := IDX + 1;
      end loop;

      UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START));
      RETVAL.EXTEND(1);
      RETVAL(IDX) := TO_NUMBER(UNIT_VAL);

      return RETVAL;
   end;

   function STRING_TO_INTEGER_ARRAY(ASTRING in varchar2) return INTEGER_ARRAY is
      RETVAL     INTEGER_ARRAY;
      UNIT_START number;
      UNIT_STOP  number;
      UNIT_VAL   varchar2(255);
      IDX        number;
   begin
      RETVAL := INTEGER_ARRAY();

      IDX        := 1;
      UNIT_START := 1;
      UNIT_STOP  := INSTR(ASTRING, ',');

      while UNIT_STOP > 0
      loop
         UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START, UNIT_STOP - UNIT_START));
         RETVAL.EXTEND(1);
         RETVAL(IDX) := TO_NUMBER(UNIT_VAL);

         UNIT_START := UNIT_STOP + 1;
         UNIT_STOP  := INSTR(ASTRING, ',', UNIT_START);
         IDX        := IDX + 1;
      end loop;

      UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START));
      RETVAL.EXTEND(1);
      RETVAL(IDX) := TO_NUMBER(UNIT_VAL);

      return RETVAL;
   end;

   function STRING_TO_DECIMAL_ARRAY(ASTRING in varchar2) return DECIMAL_ARRAY is
      RETVAL     DECIMAL_ARRAY;
      UNIT_START number;
      UNIT_STOP  number;
      UNIT_VAL   varchar2(255);
      IDX        number;
   begin
      RETVAL := DECIMAL_ARRAY();

      IDX        := 1;
      UNIT_START := 1;
      UNIT_STOP  := INSTR(ASTRING, ',');

      while UNIT_STOP > 0
      loop
         UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START, UNIT_STOP - UNIT_START));
         RETVAL.EXTEND(1);
         RETVAL(IDX) := TO_NUMBER(UNIT_VAL);

         UNIT_START := UNIT_STOP + 1;
         UNIT_STOP  := INSTR(ASTRING, ',', UNIT_START);
         IDX        := IDX + 1;
      end loop;

      UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START));
      RETVAL.EXTEND(1);
      RETVAL(IDX) := TO_NUMBER(UNIT_VAL);

      return RETVAL;
   end;

   function STRING_TO_DOUBLE_ARRAY(ASTRING in varchar2) return DOUBLE_ARRAY is
      RETVAL     DOUBLE_ARRAY;
      UNIT_START number;
      UNIT_STOP  number;
      UNIT_VAL   varchar2(255);
      IDX        number;
   begin
      RETVAL := DOUBLE_ARRAY();

      IDX        := 1;
      UNIT_START := 1;
      UNIT_STOP  := INSTR(ASTRING, ',');

      while UNIT_STOP > 0
      loop
         UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START, UNIT_STOP - UNIT_START));
         RETVAL.EXTEND(1);
         RETVAL(IDX) := TO_NUMBER(UNIT_VAL);

         UNIT_START := UNIT_STOP + 1;
         UNIT_STOP  := INSTR(ASTRING, ',', UNIT_START);
         IDX        := IDX + 1;
      end loop;

      UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START));
      RETVAL.EXTEND(1);
      RETVAL(IDX) := TO_NUMBER(UNIT_VAL);

      return RETVAL;
   end;

   function STRING_TO_DATE_ARRAY(ASTRING in varchar2) return DATE_ARRAY is
      RETVAL     DATE_ARRAY;
      UNIT_START number;
      UNIT_STOP  number;
      UNIT_VAL   varchar2(255);
      IDX        number;
   begin
      RETVAL := DATE_ARRAY();

      IDX        := 1;
      UNIT_START := 1;
      UNIT_STOP  := INSTR(ASTRING, ',');

      while UNIT_STOP > 0
      loop
         UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START, UNIT_STOP - UNIT_START));
         RETVAL.EXTEND(1);
         RETVAL(IDX) := TO_DATE(UNIT_VAL, 'MM/DD/YYYY');

         UNIT_START := UNIT_STOP + 1;
         UNIT_STOP  := INSTR(ASTRING, ',', UNIT_START);
         IDX        := IDX + 1;
      end loop;

      UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START));
      RETVAL.EXTEND(1);
      RETVAL(IDX) := TO_DATE(UNIT_VAL, 'MM/DD/YYYY');

      return RETVAL;
   end;

   function STRING_TO_DATETIME_ARRAY(ASTRING in varchar2) return DATETIME_ARRAY is
      RETVAL     DATETIME_ARRAY;
      UNIT_START number;
      UNIT_STOP  number;
      UNIT_VAL   varchar2(255);
      IDX        number;
   begin
      RETVAL := DATETIME_ARRAY();

      IDX        := 1;
      UNIT_START := 1;
      UNIT_STOP  := INSTR(ASTRING, ',');

      while UNIT_STOP > 0
      loop
         UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START, UNIT_STOP - UNIT_START));
         RETVAL.EXTEND(1);
         RETVAL(IDX) := TO_DATE(UNIT_VAL, 'MM/DD/YYYY HH24:MI:SS');

         UNIT_START := UNIT_STOP + 1;
         UNIT_STOP  := INSTR(ASTRING, ',', UNIT_START);
         IDX        := IDX + 1;
      end loop;

      UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START));
      RETVAL.EXTEND(1);
      RETVAL(IDX) := TO_DATE(UNIT_VAL, 'MM/DD/YYYY HH24:MI:SS');

      return RETVAL;
   end;

   function STRING_TO_BOOLEAN_ARRAY(ASTRING in varchar2) return BOOLEAN_ARRAY is
      RETVAL     BOOLEAN_ARRAY;
      UNIT_START number;
      UNIT_STOP  number;
      UNIT_VAL   varchar2(255);
      IDX        number;
   begin
      RETVAL := BOOLEAN_ARRAY();

      IDX        := 1;
      UNIT_START := 1;
      UNIT_STOP  := INSTR(ASTRING, ',');

      while UNIT_STOP > 0
      loop
         UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START, UNIT_STOP - UNIT_START));
         RETVAL.EXTEND(1);
         RETVAL(IDX) := case UNIT_VAL
                           when 'true' then
                            true
                           else
                            false
                        end;

         UNIT_START := UNIT_STOP + 1;
         UNIT_STOP  := INSTR(ASTRING, ',', UNIT_START);
         IDX        := IDX + 1;
      end loop;

      UNIT_VAL := trim(SUBSTR(ASTRING, UNIT_START));
      RETVAL.EXTEND(1);
      RETVAL(IDX) := case UNIT_VAL
                        when 'true' then
                         true
                        else
                         false
                     end;

      return RETVAL;
   end;

   function LONG_ARRAY_TO_STRING(ARG in LONG_ARRAY) return varchar2 is
      RETVAL varchar2(32767);
   begin
      RETVAL := '<' || ARG(1);
      for IDX in 2 .. ARG.COUNT
      loop
         RETVAL := RETVAL || ', ' || ARG(IDX);
      end loop;
      RETVAL := RETVAL || '>';
      return RETVAL;
   end;

   function STRING_ARRAY_TO_STRING(ARG in STRING_ARRAY) return varchar2 is
      RETVAL varchar2(32767);
   begin
      RETVAL := '<' || SANITIZE_STRING(ARG(1));
      for IDX in 2 .. ARG.COUNT
      loop
         RETVAL := RETVAL || ', ' || SANITIZE_STRING(ARG(IDX));
      end loop;
      RETVAL := RETVAL || '>';
      return RETVAL;
   end;

   function INTEGER_ARRAY_TO_STRING(ARG in INTEGER_ARRAY) return varchar2 is
      RETVAL varchar2(32767);
   begin
      RETVAL := '<' || ARG(1);
      for IDX in 2 .. ARG.COUNT
      loop
         RETVAL := RETVAL || ', ' || ARG(IDX);
      end loop;
      RETVAL := RETVAL || '>';
      return RETVAL;
   end;

   function DECIMAL_ARRAY_TO_STRING(ARG in DECIMAL_ARRAY) return varchar2 is
      RETVAL varchar2(32767);
   begin
      RETVAL := '<' || ARG(1);
      for IDX in 2 .. ARG.COUNT
      loop
         RETVAL := RETVAL || ', ' || ARG(IDX);
      end loop;
      RETVAL := RETVAL || '>';
      return RETVAL;
   end;

   function DOUBLE_ARRAY_TO_STRING(ARG in DOUBLE_ARRAY) return varchar2 is
      RETVAL varchar2(32767);
   begin
      RETVAL := '<' || ARG(1);
      for IDX in 2 .. ARG.COUNT
      loop
         RETVAL := RETVAL || ', ' || ARG(IDX);
      end loop;
      RETVAL := RETVAL || '>';
      return RETVAL;
   end;

   function DATE_ARRAY_TO_STRING(ARG in DATE_ARRAY) return varchar2 is
      RETVAL varchar2(32767);
   begin
      RETVAL := '<' || TO_CHAR(ARG(1), 'MM/DD/YYYY');
      for IDX in 2 .. ARG.COUNT
      loop
         RETVAL := RETVAL || ', ' || TO_CHAR(ARG(IDX), 'MM/DD/YYYY');
      end loop;
      RETVAL := RETVAL || '>';
      return RETVAL;
   end;

   function DATETIME_ARRAY_TO_STRING(ARG in DATETIME_ARRAY) return varchar2 is
      RETVAL varchar2(32767);
   begin
      RETVAL := '<' || TO_CHAR(ARG(1), 'MM/DD/YYYY HH24:MI:SS');
      for IDX in 2 .. ARG.COUNT
      loop
         RETVAL := RETVAL || ', ' || TO_CHAR(ARG(IDX), 'MM/DD/YYYY HH24:MI:SS');
      end loop;
      RETVAL := RETVAL || '>';
      return RETVAL;
   end;

   function BOOLEAN_ARRAY_TO_STRING(ARG in BOOLEAN_ARRAY) return varchar2 is
      RETVAL varchar2(32767);
   begin
      RETVAL := '<' || case
                   when ARG(1) then
                    'true'
                   else
                    'false'
                end;
      for IDX in 2 .. ARG.COUNT
      loop
         RETVAL := RETVAL || ', ' || case
                      when ARG(IDX) then
                       'true'
                      else
                       'false'
                   end;
      end loop;
      RETVAL := RETVAL || '>';
      return RETVAL;
   end;

   function STRUCTS_TO_STRINGS(ARGS   in S_PARM_ARRAYS,
                               LABELS in S_PARM_LABELS) return varchar2 is
      RETVAL varchar2(32767);
   begin
      RETVAL := '{"parms":[';

      if ARGS.LONG_ARG is not null and ARGS.LONG_ARG.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"long","value":"' || LONG_ARRAY_TO_STRING(ARGS.LONG_ARG) ||
                   '","label":"' || LABELS.LONG_LABEL || '"}';
      end if;
      if ARGS.LONG_ARG2 is not null and ARGS.LONG_ARG2.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"long2","value":"' || LONG_ARRAY_TO_STRING(ARGS.LONG_ARG2) ||
                   '","label":"' || LABELS.LONG_LABEL2 || '"}';
      end if;
      if ARGS.LONG_ARG3 is not null and ARGS.LONG_ARG3.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"long3","value":"' || LONG_ARRAY_TO_STRING(ARGS.LONG_ARG3) ||
                   '","label":"' || LABELS.LONG_LABEL3 || '"}';
      end if;
      if ARGS.LONG_ARG4 is not null and ARGS.LONG_ARG4.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"long4","value":"' || LONG_ARRAY_TO_STRING(ARGS.LONG_ARG4) ||
                   '","label":"' || LABELS.LONG_LABEL4 || '"}';
      end if;
      if ARGS.LONG_ARG5 is not null and ARGS.LONG_ARG5.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"long5","value":"' || LONG_ARRAY_TO_STRING(ARGS.LONG_ARG5) ||
                   '","label":"' || LABELS.LONG_LABEL5 || '"}';
      end if;
      if ARGS.STRING_ARG is not null and ARGS.STRING_ARG.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"string","value":"' ||
                   STRING_ARRAY_TO_STRING(ARGS.STRING_ARG) || '","label":"' || LABELS.STRING_LABEL || '"}';
      end if;
      if ARGS.STRING_ARG2 is not null and ARGS.STRING_ARG2.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"string2","value":"' ||
                   STRING_ARRAY_TO_STRING(ARGS.STRING_ARG2) || '","label":"' ||
                   LABELS.STRING_LABEL2 || '"}';
      end if;
      if ARGS.STRING_ARG3 is not null and ARGS.STRING_ARG3.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"string3","value":"' ||
                   STRING_ARRAY_TO_STRING(ARGS.STRING_ARG3) || '","label":"' ||
                   LABELS.STRING_LABEL3 || '"}';
      end if;
      if ARGS.STRING_ARG4 is not null and ARGS.STRING_ARG4.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"string4","value":"' ||
                   STRING_ARRAY_TO_STRING(ARGS.STRING_ARG4) || '","label":"' ||
                   LABELS.STRING_LABEL4 || '"}';
      end if;
      if ARGS.STRING_ARG5 is not null and ARGS.STRING_ARG5.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"string5","value":"' ||
                   STRING_ARRAY_TO_STRING(ARGS.STRING_ARG5) || '","label":"' ||
                   LABELS.STRING_LABEL5 || '"}';
      end if;
      if ARGS.INTEGER_ARG is not null and ARGS.INTEGER_ARG.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"integer","value":"' ||
                   INTEGER_ARRAY_TO_STRING(ARGS.INTEGER_ARG) || '","label":"' ||
                   LABELS.INTEGER_LABEL || '"}';
      end if;
      if ARGS.INTEGER_ARG2 is not null and ARGS.INTEGER_ARG2.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"integer2","value":"' ||
                   INTEGER_ARRAY_TO_STRING(ARGS.INTEGER_ARG2) || '","label":"' ||
                   LABELS.INTEGER_LABEL2 || '"}';
      end if;
      if ARGS.INTEGER_ARG3 is not null and ARGS.INTEGER_ARG3.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"integer3","value":"' ||
                   INTEGER_ARRAY_TO_STRING(ARGS.INTEGER_ARG3) || '","label":"' ||
                   LABELS.INTEGER_LABEL3 || '"}';
      end if;
      if ARGS.INTEGER_ARG4 is not null and ARGS.INTEGER_ARG4.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"integer4","value":"' ||
                   INTEGER_ARRAY_TO_STRING(ARGS.INTEGER_ARG4) || '","label":"' ||
                   LABELS.INTEGER_LABEL4 || '"}';
      end if;
      if ARGS.INTEGER_ARG5 is not null and ARGS.INTEGER_ARG5.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"integer5","value":"' ||
                   INTEGER_ARRAY_TO_STRING(ARGS.INTEGER_ARG5) || '","label":"' ||
                   LABELS.INTEGER_LABEL5 || '"}';
      end if;
      if ARGS.DECIMAL_ARG is not null and ARGS.DECIMAL_ARG.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"decimal","value":"' ||
                   DECIMAL_ARRAY_TO_STRING(ARGS.DECIMAL_ARG) || '","label":"' ||
                   LABELS.DECIMAL_LABEL || '"}';
      end if;
      if ARGS.DECIMAL_ARG2 is not null and ARGS.DECIMAL_ARG2.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"decimal2","value":"' ||
                   DECIMAL_ARRAY_TO_STRING(ARGS.DECIMAL_ARG2) || '","label":"' ||
                   LABELS.DECIMAL_LABEL2 || '"}';
      end if;
      if ARGS.DECIMAL_ARG3 is not null and ARGS.DECIMAL_ARG3.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"decimal3","value":"' ||
                   DECIMAL_ARRAY_TO_STRING(ARGS.DECIMAL_ARG3) || '","label":"' ||
                   LABELS.DECIMAL_LABEL3 || '"}';
      end if;
      if ARGS.DECIMAL_ARG4 is not null and ARGS.DECIMAL_ARG4.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"decimal4","value":"' ||
                   DECIMAL_ARRAY_TO_STRING(ARGS.DECIMAL_ARG4) || '","label":"' ||
                   LABELS.DECIMAL_LABEL4 || '"}';
      end if;
      if ARGS.DECIMAL_ARG5 is not null and ARGS.DECIMAL_ARG5.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"decimal5","value":"' ||
                   DECIMAL_ARRAY_TO_STRING(ARGS.DECIMAL_ARG5) || '","label":"' ||
                   LABELS.DECIMAL_LABEL5 || '"}';
      end if;
      if ARGS.DOUBLE_ARG is not null and ARGS.DOUBLE_ARG.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"double","value":"' ||
                   DOUBLE_ARRAY_TO_STRING(ARGS.DOUBLE_ARG) || '","label":"' || LABELS.DOUBLE_LABEL || '"}';
      end if;
      if ARGS.DOUBLE_ARG2 is not null and ARGS.DOUBLE_ARG2.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"double2","value":"' ||
                   DOUBLE_ARRAY_TO_STRING(ARGS.DOUBLE_ARG2) || '","label":"' ||
                   LABELS.DOUBLE_LABEL2 || '"}';
      end if;
      if ARGS.DOUBLE_ARG3 is not null and ARGS.DOUBLE_ARG3.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"double3","value":"' ||
                   DOUBLE_ARRAY_TO_STRING(ARGS.DOUBLE_ARG3) || '","label":"' ||
                   LABELS.DOUBLE_LABEL3 || '"}';
      end if;
      if ARGS.DOUBLE_ARG4 is not null and ARGS.DOUBLE_ARG4.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"double4","value":"' ||
                   DOUBLE_ARRAY_TO_STRING(ARGS.DOUBLE_ARG4) || '","label":"' ||
                   LABELS.DOUBLE_LABEL4 || '"}';
      end if;
      if ARGS.DOUBLE_ARG5 is not null and ARGS.DOUBLE_ARG5.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"double5","value":"' ||
                   DOUBLE_ARRAY_TO_STRING(ARGS.DOUBLE_ARG5) || '","label":"' ||
                   LABELS.DOUBLE_LABEL5 || '"}';
      end if;
      if ARGS.DATE_ARG is not null and ARGS.DATE_ARG.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"date","value":"' || DATE_ARRAY_TO_STRING(ARGS.DATE_ARG) ||
                   '","label":"' || LABELS.DATE_LABEL || '"}';
      end if;
      if ARGS.DATE_ARG2 is not null and ARGS.DATE_ARG2.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"date2","value":"' || DATE_ARRAY_TO_STRING(ARGS.DATE_ARG2) ||
                   '","label":"' || LABELS.DATE_LABEL2 || '"}';
      end if;
      if ARGS.DATE_ARG3 is not null and ARGS.DATE_ARG3.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"date3","value":"' || DATE_ARRAY_TO_STRING(ARGS.DATE_ARG3) ||
                   '","label":"' || LABELS.DATE_LABEL3 || '"}';
      end if;
      if ARGS.DATE_ARG4 is not null and ARGS.DATE_ARG4.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"date4","value":"' || DATE_ARRAY_TO_STRING(ARGS.DATE_ARG4) ||
                   '","label":"' || LABELS.DATE_LABEL4 || '"}';
      end if;
      if ARGS.DATE_ARG5 is not null and ARGS.DATE_ARG5.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"date5","value":"' || DATE_ARRAY_TO_STRING(ARGS.DATE_ARG5) ||
                   '","label":"' || LABELS.DATE_LABEL5 || '"}';
      end if;
      if ARGS.DATETIME_ARG is not null and ARGS.DATETIME_ARG.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"datetime","value":"' ||
                   DATETIME_ARRAY_TO_STRING(ARGS.DATETIME_ARG) || '","label":"' ||
                   LABELS.DATETIME_LABEL || '"}';
      end if;
      if ARGS.DATETIME_ARG2 is not null and ARGS.DATETIME_ARG2.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"datetime2","value":"' ||
                   DATETIME_ARRAY_TO_STRING(ARGS.DATETIME_ARG2) || '","label":"' ||
                   LABELS.DATETIME_LABEL2 || '"}';
      end if;
      if ARGS.DATETIME_ARG3 is not null and ARGS.DATETIME_ARG3.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"datetime3","value":"' ||
                   DATETIME_ARRAY_TO_STRING(ARGS.DATETIME_ARG3) || '","label":"' ||
                   LABELS.DATETIME_LABEL3 || '"}';
      end if;
      if ARGS.DATETIME_ARG4 is not null and ARGS.DATETIME_ARG4.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"datetime4","value":"' ||
                   DATETIME_ARRAY_TO_STRING(ARGS.DATETIME_ARG4) || '","label":"' ||
                   LABELS.DATETIME_LABEL4 || '"}';
      end if;
      if ARGS.DATETIME_ARG5 is not null and ARGS.DATETIME_ARG5.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"datetime5","value":"' ||
                   DATETIME_ARRAY_TO_STRING(ARGS.DATETIME_ARG5) || '","label":"' ||
                   LABELS.DATETIME_LABEL5 || '"}';
      end if;
      if ARGS.BOOLEAN_ARG is not null and ARGS.BOOLEAN_ARG.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"boolean","value":"' ||
                   BOOLEAN_ARRAY_TO_STRING(ARGS.BOOLEAN_ARG) || '","label":"' ||
                   LABELS.BOOLEAN_LABEL || '"}';
      end if;
      if ARGS.BOOLEAN_ARG2 is not null and ARGS.BOOLEAN_ARG2.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"boolean2","value":"' ||
                   BOOLEAN_ARRAY_TO_STRING(ARGS.BOOLEAN_ARG2) || '","label":"' ||
                   LABELS.BOOLEAN_LABEL2 || '"}';
      end if;
      if ARGS.BOOLEAN_ARG3 is not null and ARGS.BOOLEAN_ARG3.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"boolean3","value":"' ||
                   BOOLEAN_ARRAY_TO_STRING(ARGS.BOOLEAN_ARG3) || '","label":"' ||
                   LABELS.BOOLEAN_LABEL3 || '"}';
      end if;
      if ARGS.BOOLEAN_ARG4 is not null and ARGS.BOOLEAN_ARG4.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"boolean4","value":"' ||
                   BOOLEAN_ARRAY_TO_STRING(ARGS.BOOLEAN_ARG4) || '","label":"' ||
                   LABELS.BOOLEAN_LABEL4 || '"}';
      end if;
      if ARGS.BOOLEAN_ARG5 is not null and ARGS.BOOLEAN_ARG5.COUNT > 0 then
         RETVAL := RETVAL || ',{"type":"boolean5","value":"' ||
                   BOOLEAN_ARRAY_TO_STRING(ARGS.BOOLEAN_ARG5) || '","label":"' ||
                   LABELS.BOOLEAN_LABEL5 || '"}';
      end if;

      RETVAL := replace(RETVAL, '[,', '[') || ']}';
      return RETVAL;
   end;

   procedure STRING_TO_STRUCTS(JSON   in varchar2,
                               ARGS   in out S_PARM_ARRAYS,
                               LABELS in out S_PARM_LABELS) is
      APARM      varchar2(32767);
      ATYPE      varchar2(15);
      AVALUE     varchar2(32767);
      ALABEL     varchar2(4000);
      TYPE_START number;
      TYPE_STOP  number;
      VAL_START  number;
      VAL_STOP   number;
      LAB_START  number;
      LAB_STOP   number;
      PARM_START number;
      PARM_STOP  number;
   begin
      PARM_START := INSTR(JSON, '{', INSTR(JSON, 'parms', 1)) + 1;
      PARM_STOP  := INSTR(JSON, '}', PARM_START);
      while PARM_START > 1
      loop
         APARM      := SUBSTR(JSON, PARM_START, PARM_STOP - PARM_START);
         TYPE_START := INSTR(APARM, '"type":"') + 8;
         TYPE_STOP  := INSTR(APARM, '"', TYPE_START);
         ATYPE      := SUBSTR(APARM, TYPE_START, TYPE_STOP - TYPE_START);
         VAL_START  := INSTR(APARM, '"value":"<') + 10;
         VAL_STOP   := INSTR(APARM, '>",', VAL_START);
         AVALUE     := SUBSTR(APARM, VAL_START, VAL_STOP - VAL_START);
         LAB_START  := INSTR(APARM, '"label":"') + 9;
         LAB_STOP   := INSTR(APARM, '"', LAB_START);
         ALABEL     := SUBSTR(APARM, LAB_START, LAB_STOP - LAB_START);

         if ATYPE = 'long' then
            ARGS.LONG_ARG     := STRING_TO_LONG_ARRAY(AVALUE);
            LABELS.LONG_LABEL := ALABEL;
         elsif ATYPE = 'long2' then
            ARGS.LONG_ARG2     := STRING_TO_LONG_ARRAY(AVALUE);
            LABELS.LONG_LABEL2 := ALABEL;
         elsif ATYPE = 'long3' then
            ARGS.LONG_ARG3     := STRING_TO_LONG_ARRAY(AVALUE);
            LABELS.LONG_LABEL3 := ALABEL;
         elsif ATYPE = 'long4' then
            ARGS.LONG_ARG4     := STRING_TO_LONG_ARRAY(AVALUE);
            LABELS.LONG_LABEL4 := ALABEL;
         elsif ATYPE = 'long5' then
            ARGS.LONG_ARG5     := STRING_TO_LONG_ARRAY(AVALUE);
            LABELS.LONG_LABEL5 := ALABEL;
         elsif ATYPE = 'string' then
            ARGS.STRING_ARG     := STRING_TO_STRING_ARRAY(AVALUE);
            LABELS.STRING_LABEL := ALABEL;
         elsif ATYPE = 'string2' then
            ARGS.STRING_ARG2     := STRING_TO_STRING_ARRAY(AVALUE);
            LABELS.STRING_LABEL2 := ALABEL;
         elsif ATYPE = 'string3' then
            ARGS.STRING_ARG3     := STRING_TO_STRING_ARRAY(AVALUE);
            LABELS.STRING_LABEL3 := ALABEL;
         elsif ATYPE = 'string4' then
            ARGS.STRING_ARG4     := STRING_TO_STRING_ARRAY(AVALUE);
            LABELS.STRING_LABEL4 := ALABEL;
         elsif ATYPE = 'string5' then
            ARGS.STRING_ARG5     := STRING_TO_STRING_ARRAY(AVALUE);
            LABELS.STRING_LABEL5 := ALABEL;
         elsif ATYPE = 'integer' then
            ARGS.INTEGER_ARG     := STRING_TO_INTEGER_ARRAY(AVALUE);
            LABELS.INTEGER_LABEL := ALABEL;
         elsif ATYPE = 'integer2' then
            ARGS.INTEGER_ARG2     := STRING_TO_INTEGER_ARRAY(AVALUE);
            LABELS.INTEGER_LABEL2 := ALABEL;
         elsif ATYPE = 'integer3' then
            ARGS.INTEGER_ARG3     := STRING_TO_INTEGER_ARRAY(AVALUE);
            LABELS.INTEGER_LABEL3 := ALABEL;
         elsif ATYPE = 'integer4' then
            ARGS.INTEGER_ARG4     := STRING_TO_INTEGER_ARRAY(AVALUE);
            LABELS.INTEGER_LABEL4 := ALABEL;
         elsif ATYPE = 'integer5' then
            ARGS.INTEGER_ARG5     := STRING_TO_INTEGER_ARRAY(AVALUE);
            LABELS.INTEGER_LABEL5 := ALABEL;
         elsif ATYPE = 'decimal' then
            ARGS.DECIMAL_ARG     := STRING_TO_DECIMAL_ARRAY(AVALUE);
            LABELS.DECIMAL_LABEL := ALABEL;
         elsif ATYPE = 'decimal2' then
            ARGS.DECIMAL_ARG2     := STRING_TO_DECIMAL_ARRAY(AVALUE);
            LABELS.DECIMAL_LABEL2 := ALABEL;
         elsif ATYPE = 'decimal3' then
            ARGS.DECIMAL_ARG3     := STRING_TO_DECIMAL_ARRAY(AVALUE);
            LABELS.DECIMAL_LABEL3 := ALABEL;
         elsif ATYPE = 'decimal4' then
            ARGS.DECIMAL_ARG4     := STRING_TO_DECIMAL_ARRAY(AVALUE);
            LABELS.DECIMAL_LABEL4 := ALABEL;
         elsif ATYPE = 'decimal5' then
            ARGS.DECIMAL_ARG5     := STRING_TO_DECIMAL_ARRAY(AVALUE);
            LABELS.DECIMAL_LABEL5 := ALABEL;
         elsif ATYPE = 'double' then
            ARGS.DOUBLE_ARG     := STRING_TO_DOUBLE_ARRAY(AVALUE);
            LABELS.DOUBLE_LABEL := ALABEL;
         elsif ATYPE = 'double2' then
            ARGS.DOUBLE_ARG2     := STRING_TO_DOUBLE_ARRAY(AVALUE);
            LABELS.DOUBLE_LABEL2 := ALABEL;
         elsif ATYPE = 'double3' then
            ARGS.DOUBLE_ARG3     := STRING_TO_DOUBLE_ARRAY(AVALUE);
            LABELS.DOUBLE_LABEL3 := ALABEL;
         elsif ATYPE = 'double4' then
            ARGS.DOUBLE_ARG4     := STRING_TO_DOUBLE_ARRAY(AVALUE);
            LABELS.DOUBLE_LABEL4 := ALABEL;
         elsif ATYPE = 'double5' then
            ARGS.DOUBLE_ARG5     := STRING_TO_DOUBLE_ARRAY(AVALUE);
            LABELS.DOUBLE_LABEL5 := ALABEL;
         elsif ATYPE = 'date' then
            ARGS.DATE_ARG     := STRING_TO_DATE_ARRAY(AVALUE);
            LABELS.DATE_LABEL := ALABEL;
         elsif ATYPE = 'date2' then
            ARGS.DATE_ARG2     := STRING_TO_DATE_ARRAY(AVALUE);
            LABELS.DATE_LABEL2 := ALABEL;
         elsif ATYPE = 'date3' then
            ARGS.DATE_ARG3     := STRING_TO_DATE_ARRAY(AVALUE);
            LABELS.DATE_LABEL3 := ALABEL;
         elsif ATYPE = 'date4' then
            ARGS.DATE_ARG4     := STRING_TO_DATE_ARRAY(AVALUE);
            LABELS.DATE_LABEL4 := ALABEL;
         elsif ATYPE = 'date5' then
            ARGS.DATE_ARG5     := STRING_TO_DATE_ARRAY(AVALUE);
            LABELS.DATE_LABEL5 := ALABEL;
         elsif ATYPE = 'datetime' then
            ARGS.DATETIME_ARG     := STRING_TO_DATETIME_ARRAY(AVALUE);
            LABELS.DATETIME_LABEL := ALABEL;
         elsif ATYPE = 'datetime2' then
            ARGS.DATETIME_ARG2     := STRING_TO_DATETIME_ARRAY(AVALUE);
            LABELS.DATETIME_LABEL2 := ALABEL;
         elsif ATYPE = 'datetime3' then
            ARGS.DATETIME_ARG3     := STRING_TO_DATETIME_ARRAY(AVALUE);
            LABELS.DATETIME_LABEL3 := ALABEL;
         elsif ATYPE = 'datetime4' then
            ARGS.DATETIME_ARG4     := STRING_TO_DATETIME_ARRAY(AVALUE);
            LABELS.DATETIME_LABEL4 := ALABEL;
         elsif ATYPE = 'datetime5' then
            ARGS.DATETIME_ARG5     := STRING_TO_DATETIME_ARRAY(AVALUE);
            LABELS.DATETIME_LABEL5 := ALABEL;
         elsif ATYPE = 'boolean' then
            ARGS.BOOLEAN_ARG     := STRING_TO_BOOLEAN_ARRAY(AVALUE);
            LABELS.BOOLEAN_LABEL := ALABEL;
         elsif ATYPE = 'boolean2' then
            ARGS.BOOLEAN_ARG2     := STRING_TO_BOOLEAN_ARRAY(AVALUE);
            LABELS.BOOLEAN_LABEL2 := ALABEL;
         elsif ATYPE = 'boolean3' then
            ARGS.BOOLEAN_ARG3     := STRING_TO_BOOLEAN_ARRAY(AVALUE);
            LABELS.BOOLEAN_LABEL3 := ALABEL;
         elsif ATYPE = 'boolean4' then
            ARGS.BOOLEAN_ARG4     := STRING_TO_BOOLEAN_ARRAY(AVALUE);
            LABELS.BOOLEAN_LABEL4 := ALABEL;
         elsif ATYPE = 'boolean5' then
            ARGS.BOOLEAN_ARG5     := STRING_TO_BOOLEAN_ARRAY(AVALUE);
            LABELS.BOOLEAN_LABEL5 := ALABEL;
         end if;

         PARM_START := INSTR(JSON, '{', PARM_STOP, 1) + 1;
         PARM_STOP  := INSTR(JSON, '}', PARM_START, 1);
      end loop;
   end;

end PP_PARM_TRANSLATE;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (18217, 0, 2018, 2, 1, 0, 0, 'C:\BitBucketRepos\Classic_PB\scripts\00_base\packages', 
    'PP_PARM_TRANSLATE.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
