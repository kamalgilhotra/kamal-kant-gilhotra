CREATE OR replace PACKAGE pkg_lessor_import
AS
  /*
  ||============================================================================
  || Application: PowerPlant
  || Object Name: PKG_LESSOR_IMPORT
  || Description:
  ||============================================================================
  || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
  ||============================================================================
  || Version  Date       Revised By              Reason for Change
  || -------- ---------- ---------------------   ---------------------------------
  || 2017.1.0 09/29/2017 Shane "C" Ward          Original Version
  || 2017.1.0 10/06/2017 Shane "C" Ward          Add MLA Import
  || 2017.1.0 10/18/2017 Shane "C" Ward          Add ILR Import
  || 2017.1.0 10/30/2017 Shane "C" Ward          Add Asset Imports
  ||============================================================================
  */
  G_PKG_VERSION varchar(35) := '2018.2.0.0';

  TYPE num_array
    IS TABLE OF NUMBER(22) INDEX BY BINARY_INTEGER;

  FUNCTION F_import_lessees(
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_validate_lessees(
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_import_mlas(
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_validate_mlas(
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_import_ilrs(
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_validate_ilrs(
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_import_ilr_renewals(
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_validate_ilr_renewals(
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_import_cpr_assets(
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_validate_cpr_assets (
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_import_external_assets(
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_validate_external_assets (
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_import_ilr_idc(
    a_run_id NUMBER)
  RETURN VARCHAR2;

  FUNCTION F_validate_ilr_idc(
    a_run_id NUMBER)
  RETURN VARCHAR2;

END pkg_lessor_import;
/
CREATE OR replace PACKAGE BODY pkg_lessor_import
AS
  /*
  ||============================================================================
  || Application: PowerPlant
  || Object Name: PKG_LESSOR_IMPORT
  || Description:
  ||============================================================================
  || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
  ||============================================================================
  || Version  Date       Revised By              Reason for Change
  || -------- ---------- ---------------------   ---------------------------------
  || 2017.1.0 09/29/2017 Shane "C" Ward          Original Version
  || 2017.1.0 10/06/2017 Shane "C" Ward          Add MLA Import
  || 2017.1.0 10/18/2017 Shane "C" Ward          Add ILR Import
  || 2017.1.0 10/30/2017 Shane "C" Ward          Add Asset Imports
  ||============================================================================
  */

  TYPE mla_company_line IS RECORD(
    lease_id lsr_lease_company.lease_id%TYPE,
    company_id lsr_lease_company.company_id%TYPE);
  TYPE mla_company_table
    IS TABLE OF MLA_COMPANY_LINE INDEX BY PLS_INTEGER;

  --**************************************************************************
  --                            Start Body
  --**************************************************************************
  --**************************************************************************
  --                            PROCEDURES
  --**************************************************************************

  --**************************************************************************
  --                            FUNCTIONS
  --**************************************************************************

  --##########################################################################
  --                            LESSEES
  --##########################################################################
  FUNCTION F_import_lessees( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    TYPE lessee_import_table
      IS TABLE OF lsr_import_lessee%ROWTYPE INDEX BY PLS_INTEGER;
    l_lessee_import LESSEE_IMPORT_TABLE;
    l_index         NUMBER;
    l_msg           VARCHAR2(32000);
  BEGIN
      --here we're populating our PK for the LESSEES. If the previous row has a different unique identifier than the current
      --row we give it a new id, else we give it the previous row's lessor_id but we make it negative so we don't attempt to insert
      --on that row
      UPDATE LSR_IMPORT_LESSEE
      SET    lessee_id = ls_lessor_seq.NEXTVAL
      WHERE  line_id IN
             ( SELECT line_id
               FROM   ( SELECT line_id,
                               Row_number( )
                                 over(
                                   PARTITION BY unique_lessee_identifier
                                   ORDER BY unique_lessee_identifier, line_id) NUM
                        FROM   LSR_IMPORT_LESSEE
                        WHERE  import_run_id = a_run_id )
               WHERE  num = 1 ) AND
             import_run_id = a_run_id;

      UPDATE LSR_IMPORT_LESSEE a
      SET    a.lessee_id = ( SELECT -b.lessee_id
                             FROM   LSR_IMPORT_LESSEE b
                             WHERE  a.unique_lessee_identifier = b.unique_lessee_identifier AND
                                    b.lessee_id IS NOT NULL AND
                                    import_run_id = a_run_id )
      WHERE  a.lessee_id IS NULL AND
             import_run_id = a_run_id;

      l_msg := 'Collecting for lessee insert';

      SELECT *
      bulk   collect INTO l_lessee_import
      FROM   LSR_IMPORT_LESSEE
      WHERE  import_run_id = a_run_id AND
             Nvl( lessee_id, -1 ) > 0;

      l_msg := 'Inserting lessee';

      forall l_index IN indices OF l_lessee_import
        INSERT INTO LSR_LESSEE
                    (lessee_id,
                     description,
                     long_description,
                     address1,
                     address2,
                     address3,
                     address4,
                     zip,
                     postal,
                     city,
                     county_id,
                     state_id,
                     country_id,
                     phone_number,
                     extension,
                     fax_number,
                     external_lessee_number,
                     site_code,
                     status_code_id,
                     lease_group_id)
        ( SELECT L_lessee_import( l_index ).lessee_id,
                 L_lessee_import( l_index ).description,
                 L_lessee_import( l_index ).long_description,
                 L_lessee_import( l_index ).address1,
                 L_lessee_import( l_index ).address2,
                 L_lessee_import( l_index ).address3,
                 L_lessee_import( l_index ).address4,
                 L_lessee_import( l_index ).zip,
                 L_lessee_import( l_index ).postal,
                 L_lessee_import( l_index ).city,
                 L_lessee_import( l_index ).county_id,
                 L_lessee_import( l_index ).state_id,
                 L_lessee_import( l_index ).country_id,
                 L_lessee_import( l_index ).phone_number,
                 L_lessee_import( l_index ).extension,
                 L_lessee_import( l_index ).fax_number,
                 L_lessee_import( l_index ).external_lessee_number,
                 L_lessee_import( l_index ).site_code,
                 1,
                 L_lessee_import( l_index ).lease_group_id
          FROM   DUAL );

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_import_lessees;

  FUNCTION F_validate_lessees( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --check for duplicate lessee descriptions
      UPDATE LSR_IMPORT_LESSEE
      SET    error_message = error_message
                             || ' Duplicate lessee description.'
      WHERE  import_run_id = a_run_id AND
             description IN
             ( SELECT description
               FROM   ( SELECT description,
                               Count( * ) AS MY_COUNT
                        FROM   ( SELECT description
                                 FROM   LSR_LESSEE
                                 UNION ALL
                                 SELECT description
                                 FROM   ( SELECT DISTINCT unique_lessee_identifier,
                                                          description
                                          FROM   LSR_IMPORT_LESSEE
                                          WHERE  import_run_id = a_run_id ) )
                        GROUP  BY description )
               WHERE  my_count > 1 );

      --check for duplicate external_lessee_number
      UPDATE LSR_IMPORT_LESSEE
      SET    error_message = error_message
                             || ' Duplicate external lessee number.'
      WHERE  import_run_id = a_run_id AND
             external_lessee_number IN
             ( SELECT external_lessee_number
               FROM   ( SELECT external_lessee_number,
                               Count( * ) AS MY_COUNT
                        FROM   ( SELECT external_lessee_number
                                 FROM   LSR_LESSEE
                                 UNION ALL
                                 SELECT external_lessee_number
                                 FROM   ( SELECT DISTINCT unique_lessee_identifier,
                                                          external_lessee_number
                                          FROM   LSR_IMPORT_LESSEE
                                          WHERE  import_run_id = a_run_id ) )
                        GROUP  BY external_lessee_number )
               WHERE  my_count > 1 );

      -- Validate no 's or ''s
      UPDATE LSR_IMPORT_LESSEE a
      SET    error_message = error_message
                             || ' Single and Double Quotes are not allowed in Description.'
      WHERE  import_run_id = a_run_id AND
             ( Instr( description, '''' ) > 1  OR
               Instr( description, '"' ) > 1 );

      -- Validate County-State combo
      UPDATE LSR_IMPORT_LESSEE a
      SET    error_message = error_message
                             || ' Invalid County-State combination.'
      WHERE  import_run_id = a_run_id AND
             ( county_id, state_id ) NOT IN ( SELECT county_id,
                                                     state_id
                                              FROM   COUNTY );

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_validate_lessees;

  --##########################################################################
  --                            MLAS
  --##########################################################################
  FUNCTION F_import_mlas( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg              VARCHAR2(32000);
    TYPE mla_import_table
      IS TABLE OF lsr_import_lease%ROWTYPE INDEX BY PLS_INTEGER;
    l_index            NUMBER;
    mla_company        MLA_COMPANY_TABLE;
    l_mla_import_table MLA_IMPORT_TABLE;
    l_cc_sql           CLOB;
    cc_needed          BOOLEAN;
    rtn                NUMBER;
  BEGIN
      UPDATE LSR_IMPORT_LEASE
      SET    lease_id = ls_lease_seq.NEXTVAL
      WHERE  line_id IN
             ( SELECT line_id
               FROM   ( SELECT line_id,
                               Row_number( )
                                 over(
                                   PARTITION BY unique_lease_identifier
                                   ORDER BY unique_lease_identifier, line_id) NUM
                        FROM   LSR_IMPORT_LEASE
                        WHERE  import_run_id = a_run_id )
               WHERE  num = 1 ) AND
             import_run_id = a_run_id;

      UPDATE LSR_IMPORT_LEASE a
      SET    a.lease_id = ( SELECT -b.lease_id
                            FROM   LSR_IMPORT_LEASE b
                            WHERE  a.unique_lease_identifier = b.unique_lease_identifier AND
                                   b.lease_id IS NOT NULL )
      WHERE  a.lease_id IS NULL AND
             import_run_id = a_run_id;

      SELECT *
      bulk   collect INTO l_mla_import_table
      FROM   LSR_IMPORT_LEASE
      WHERE  import_run_id = a_run_id AND
             Nvl( lease_id, -1 ) > 0;

      /* CJS 2/23/15 Changing month to day for master agreement date; Adding new columns*/

      l_msg := 'LSR_LEASE';

      forall l_index IN indices OF l_mla_import_table
        INSERT INTO LSR_LEASE
                    (lease_id,
                     lease_status_id,
                     lease_number,
                     description,
                     long_description,
                     lessee_id,
                     lease_type_id,
                     payment_due_day,
                     pre_payment_sw,
                     lease_group_id,
                     lease_cap_type_id,
                     workflow_type_id,
                     master_agreement_date,
                     notes,
                     current_revision,
                     initiation_date,
                     lease_end_date,
                     contract_currency_id)
        SELECT L_mla_import_table( l_index ).lease_id,
               1,
               L_mla_import_table( l_index ).lease_number,
               L_mla_import_table( l_index ).description,
               L_mla_import_table( l_index ).long_description,
               L_mla_import_table( l_index ).lessee_id,
               L_mla_import_table( l_index ).lease_type_id,
               L_mla_import_table( l_index ).payment_due_day,
               L_mla_import_table( l_index ).pre_payment_sw,
               L_mla_import_table( l_index ).lease_group_id,
               L_mla_import_table( l_index ).lease_cap_type_id,
               L_mla_import_table( l_index ).workflow_type_id,
               To_date( L_mla_import_table( l_index ).master_agreement_date, 'yyyymmdd' ),
               L_mla_import_table( l_index ).notes,
               1,
               SYSDATE,
               To_date( L_mla_import_table( l_index ).lease_end_date, 'yyyymmdd' ),
               L_mla_import_table( l_index ).contract_currency_id
        FROM   DUAL;

      l_msg := 'LSR_LEASE_APPROVAL';

      forall l_index IN indices OF l_mla_import_table
        INSERT INTO LSR_LEASE_APPROVAL
                    (lease_id,
                     revision,
                     approval_type_id,
                     approval_status_id)
        VALUES      (L_mla_import_table( l_index ).lease_id,
                     1,
                     L_mla_import_table( l_index ).workflow_type_id,
                     1);

      l_msg := 'LSR_LEASE_OPTIONS';

      forall l_index IN indices OF l_mla_import_table
        INSERT INTO LSR_LEASE_OPTIONS
                    (lease_id,
                     revision,
                     purchase_option_type_id,
                     purchase_option_amt,
                     renewal_option_type_id,
                     cancelable_type_id,
                     itc_sw,
                     intent_to_purchase,
                     likely_to_collect,
                     specialized_asset,
                     sublease,
                     sublease_lease_id)
        VALUES      (L_mla_import_table( l_index ).lease_id,
                     1,
                     L_mla_import_table( l_index ).purchase_option_type_id,
                     L_mla_import_table( l_index ).purchase_option_amt,
                     L_mla_import_table( l_index ).renewal_option_type_id,
                     L_mla_import_table( l_index ).cancelable_type_id,
                     L_mla_import_table( l_index ).itc_sw,
                     L_mla_import_table( l_index ).intent_to_purchase,
                     L_mla_import_table( l_index ).likely_to_collect,
                     L_mla_import_table( l_index ).specialized_asset,
                     L_mla_import_table( l_index ).sublease,
                     L_mla_import_table( l_index ).sublease_lease_id );

      l_cc_sql := 'DECLARE BEGIN ';

      FOR l_index IN 1 .. l_mla_import_table.count LOOP
          IF L_mla_import_table( l_index ).class_code_id1 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value1 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id1 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value1 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id2 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value2 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id2 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value2 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id3 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value3 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id3 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value3 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id4 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value4 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id4 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value4 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id5 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value5 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id5 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value5 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id6 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value6 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id6 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value6 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id7 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value7 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id7 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value7 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id8 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value8 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id8 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value8 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id9 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value9 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id9 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value9 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id10 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value10 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id10 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value10 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id11 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value11 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id11 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value11 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id12 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value12 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id12 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value12 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id13 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value13 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id13 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value13 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id14 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value14 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id14 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value14 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id15 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value15 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id15 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value15 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id16 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value16 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id16 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value16 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id17 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value17 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id17 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value17 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id18 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value18 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id18 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value18 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id19 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value19 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id19 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value19 )
                        || ''');';
          END IF;

          IF L_mla_import_table( l_index ).class_code_id20 IS NOT NULL AND
             L_mla_import_table( l_index ).class_code_value20 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_LEASE_CLASS_CODE (class_code_id, lease_id, value)
                      values ('
                        || To_char( L_mla_import_table( l_index ).class_code_id20 )
                        || ', '
                        || To_char( L_mla_import_table( l_index ).lease_id )
                        || ', '''
                        || To_char( L_mla_import_table( l_index ).class_code_value20 )
                        || ''');';
          END IF;
      END LOOP;

      l_cc_sql := l_cc_sql
                  || ' END;';

      IF cc_needed THEN
        l_msg := Substr( l_cc_sql, 0, 32000 );

        EXECUTE IMMEDIATE l_cc_sql;
      END IF;

      SELECT DISTINCT Abs( lease_id ),
                      company_id
      bulk   collect INTO mla_company
      FROM   LSR_IMPORT_LEASE
      WHERE  import_run_id = a_run_id;

      l_msg := 'LSR_LEASE_COMPANY';

      forall l_index IN indices OF mla_company
        INSERT INTO LSR_LEASE_COMPANY
                    (lease_id,
                     company_id)
        VALUES      (Mla_company( l_index ).lease_id,
                     Mla_company( l_index ).company_id);

      --Auto approve it
      select * bulk collect
        into l_mla_import_table
        from LSR_IMPORT_LEASE
       where IMPORT_RUN_ID = A_RUN_ID
         and NVL(LEASE_ID, -1) > 0
         and (AUTO_APPROVE = 1)
         and (WORKFLOW_TYPE_ID in
             (select WORKFLOW_TYPE_ID
                 from WORKFLOW_TYPE
                where lower(subsystem) like '%lessor_mla_approval%'
                ));

      --Instead of calling pkg_lessor_approval.F_APPROVE_MLA, I'm copying the SQL here since F_APPROVE_MLA is pragma autonomous
      --Since we can't commit before approving, the autonomous transaction won't be to update anything, so we can't use it
      L_MSG := 'Auto Approving';
      for L_INDEX in 1 .. L_MLA_IMPORT_TABLE.COUNT
      loop
         update LSR_LEASE_APPROVAL
            set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
          where LEASE_ID = L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID
            and NVL(REVISION, 0) = 1;

         update LSR_LEASE L
            set LEASE_STATUS_ID = 3, APPROVAL_DATE = sysdate, CURRENT_REVISION = 1
          where LEASE_ID = L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID;
      end loop;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_import_mlas;

  FUNCTION F_validate_mlas( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
	type MLA_IMPORT_TABLE is table of LSR_IMPORT_LEASE%rowtype index by pls_integer;
	L_MLA_IMPORT_TABLE MLA_IMPORT_TABLE;
	L_INDEX   		number;
	L_CC_SQL 			clob;
	CC_IMPORTED 		boolean;
	CC_REQUIRED_NUM  	number(22, 0);
	CC_MISSING 		varchar2(2000);
  BEGIN
      --check for duplicate lease number
      UPDATE LSR_IMPORT_LEASE
      SET    error_message = error_message
                             || ' Duplicate Lease Number.'
      WHERE  import_run_id = a_run_id AND
             lease_number IN
             ( SELECT lease_number
               FROM   ( SELECT lease_number,
                               Count( * ) AS MY_COUNT
                        FROM   ( SELECT lease_number
                                 FROM   LSR_LEASE
                                 UNION ALL
                                 SELECT lease_number
                                 FROM   ( SELECT DISTINCT lease_number,
                                                          unique_lease_identifier
                                          FROM   LSR_IMPORT_LEASE
                                          WHERE  import_run_id = a_run_id ) )
                        GROUP  BY lease_number )
               WHERE  my_count > 1 );

      UPDATE LSR_IMPORT_LEASE
      SET    error_message = error_message
                             || ' Cut off Day not a valid option.'
      WHERE  import_run_id = a_run_id AND
             cut_off_day NOT BETWEEN 0 AND 31;

      UPDATE LSR_IMPORT_LEASE a
      SET    error_message = error_message
                             || ' Selected Currency is not a valid option.'
      WHERE  import_run_id = a_run_id AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   CURRENCY b
                   WHERE  b.currency_id = a.contract_currency_id );

      --Validate if Sublease Flag is no, there is no sublease
      UPDATE LSR_IMPORT_LEASE a
      SET    error_message = error_message
                             || ' Sublease populated when Sublease Flag set to "No".'
      WHERE  import_run_id = a_run_id AND
             sublease = 0 AND
             sublease_lease_id IS NOT NULL;

      -- Validate Likely to collect is 1
      UPDATE LSR_IMPORT_LEASE a
      SET    error_message = error_message
                             || ' Likely to Collect must be set to "Yes".'
      WHERE  import_run_id = a_run_id AND
             likely_to_collect = 0;

      -- Validate no 's or ''s
      UPDATE LSR_IMPORT_LEASE a
      SET    error_message = error_message
                             || ' Single and Double Quotes are not allowed in Description.'
      WHERE  import_run_id = a_run_id AND
             ( Instr( description, '''' ) > 1  OR
               Instr( description, '"' ) > 1 );

      -- Validate Workflow type specified is Auto Approve type if the auto_approve fields is yes
      UPDATE LSR_IMPORT_LEASE
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Selected workflow type cannot be automatically approved.'
       where IMPORT_RUN_ID = A_RUN_ID
         and AUTO_APPROVE = 1
         and WORKFLOW_TYPE_ID not in
             (select WORKFLOW_TYPE_ID from WORKFLOW_TYPE where lower(subsystem) like '%lessor_mla_approval%');

      -- PP-51229 - Add validation for class codes
      for L_CC_NO in 1 .. 20
       loop
	     L_CC_SQL := 'DECLARE BEGIN
	     update LSR_IMPORT_LEASE a
		    set ERROR_MESSAGE = ERROR_MESSAGE || '' Class Code '||to_char(L_CC_NO)||' is not a valid MLA class code.''
		  where IMPORT_RUN_ID = '||to_char(A_RUN_ID)||'
			and CLASS_CODE_ID'||to_char(L_CC_NO)||' is not null
			and CLASS_CODE_VALUE'||to_char(L_CC_NO)||' is not null
			and  not exists (select 1 from CLASS_CODE b where a.CLASS_CODE_ID'||to_char(L_CC_NO)||' = b.CLASS_CODE_ID and b.LSR_MLA_INDICATOR = 1);

	     update LSR_IMPORT_LEASE a
		    set ERROR_MESSAGE = ERROR_MESSAGE || '' Class Code Value '||to_char(L_CC_NO)||' is not a valid value.''
		  where IMPORT_RUN_ID = '||to_char(A_RUN_ID)||'
			and CLASS_CODE_ID'||to_char(L_CC_NO)||' is not null
			and CLASS_CODE_VALUE'||to_char(L_CC_NO)||' is not null
			and exists (select 1 from CLASS_CODE b
			             where b.CLASS_CODE_ID = a.CLASS_CODE_ID'||to_char(L_CC_NO)||'
				           and b.LSR_MLA_INDICATOR = 1
                           and lower(CLASS_CODE_TYPE) = ''standard'')
			and not exists (select 1 from CLASS_CODE_VALUES C where C.CLASS_CODE_ID = a.CLASS_CODE_ID'||to_char(L_CC_NO)||' and c.VALUE = a.CLASS_CODE_VALUE'||to_char(L_CC_NO)||');

	     END;';

	   execute immediate L_CC_SQL;
      end loop;
  -- PP-51674 - Check for Required Class Codes

     select count(*) into CC_REQUIRED_NUM
     from class_code
     where lsr_mla_indicator = 1
     and required = 1;

     if CC_REQUIRED_NUM > 0 then
            L_MSG := 'Check for required class codes';

            select * bulk collect
                into L_MLA_IMPORT_TABLE
                from LSR_IMPORT_LEASE
            where IMPORT_RUN_ID = A_RUN_ID;

            for L_INDEX in 1 .. L_MLA_IMPORT_TABLE.COUNT
            loop
                CC_IMPORTED := false;
                L_CC_SQL := 'select listagg(description, '','') within group (order by description) description
                                from class_Code where class_code_id in (
                                select class_code_id from class_code where lsr_mla_indicator = 1 and required = 1
                                minus
                                select class_code_id from class_code where class_code_id in (';

                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE1 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE2 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE3 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE4 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE5 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE6 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE7 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE8 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE9 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE10 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE11 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE12 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE13 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE14 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE15 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE16 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE17 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE18 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE19 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE20 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20) || ',';
                end if;

                L_CC_SQL :=  trim(TRAILING ',' FROM L_CC_SQL);
                L_CC_SQL := L_CC_SQL || '))';

                if CC_IMPORTED then
                      execute immediate L_CC_SQL into CC_MISSING;

                      if CC_MISSING is not null then
                              UPDATE LSR_IMPORT_LEASE a
                              SET    error_message = error_message
                                                    || ' Required Lessor MLA Class Codes are Missing:' || CC_MISSING
                              WHERE  import_run_id = a_run_id
                              and line_id = L_MLA_IMPORT_TABLE(L_INDEX).LINE_ID;
                      end if;
                else
                  --There are required class codes and none were imported, so all are missing
                  L_CC_SQL := 'select listagg(description, '','') within group (order by description) description
                                from class_Code where lsr_mla_indicator = 1 and required = 1';

                  execute immediate L_CC_SQL into CC_MISSING;

                  if CC_MISSING is not null then
                          UPDATE LSR_IMPORT_LEASE a
                          SET    error_message = error_message
                                                || ' Required Lessor MLA Class Codes are Missing:' || CC_MISSING
                          WHERE  import_run_id = a_run_id
                          and line_id = L_MLA_IMPORT_TABLE(L_INDEX).LINE_ID;
                  end if;
                end if;
              end loop;

     end if;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_validate_mlas;

  --##########################################################################
  --                            ILRS
  --##########################################################################
  FUNCTION F_import_ilrs( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg        VARCHAR2(32000);
    TYPE ilr_import_table
      IS TABLE OF lsr_import_ilr%ROWTYPE;
    l_ilr_import ILR_IMPORT_TABLE;
    l_index      NUMBER;
    l_cc_sql     CLOB;
    cc_needed    BOOLEAN;
    l_rate_type_id number;
  BEGIN
      --Assign ID's to first instance of ILRs
      UPDATE LSR_IMPORT_ILR
      SET    ilr_id = ls_ilr_seq.NEXTVAL
      WHERE  line_id IN
             ( SELECT line_id
               FROM   ( SELECT line_id,
                               Row_number( )
                                 over(
                                   PARTITION BY unique_ilr_identifier
                                   ORDER BY unique_ilr_identifier, line_id) NUM
                        FROM   LSR_IMPORT_ILR
                        WHERE  import_run_id = a_run_id )
               WHERE  num = 1 ) AND
             import_run_id = a_run_id;

      --Assign negative ID where Unique ILR ID are the same
      UPDATE LSR_IMPORT_ILR a
      SET    a.ilr_id = ( SELECT -b.ilr_id
                          FROM   LSR_IMPORT_ILR b
                          WHERE  a.unique_ilr_identifier = b.unique_ilr_identifier AND
                                 b.ilr_id IS NOT NULL )
      WHERE  a.ilr_id IS NULL AND
             import_run_id = a_run_id;

      --Assign Workflow Type based on ILR Group if user didn't import
      UPDATE LSR_IMPORT_ILR a
      SET    a.workflow_type_id = ( SELECT workflow_type_id
                                    FROM   LSR_ILR_GROUP
                                    WHERE  ilr_group_id = a.ilr_group_id )
      WHERE  a.workflow_type_id IS NULL AND
             a.import_run_id = a_run_id;

      --Assign In Service Exchange Rate based on In Service date
      UPDATE LSR_IMPORT_ILR a
      SET    a.in_service_exchange_rate = ( SELECT Decode( cont_to_comp_rate, 0, 0,
                                                                              Round( 1 / cont_to_comp_rate, 8 ) ) AS contract_to_company_rate
                                            FROM   ( SELECT DISTINCT First_value( rate )
                                                                       over (
                                                                         PARTITION BY currency_from, currency_to, ilr_id, est_in_svc_date
                                                                         ORDER BY exchange_date DESC) cont_to_comp_rate,
                                                                     ilr_id,
                                                                     est_in_svc_date
                                                     FROM   CURRENCY_RATE_DEFAULT d,
                                                            LSR_LEASE l,
                                                            LSR_IMPORT_ILR i,
                                                            CURRENCY_SCHEMA s
                                                     WHERE  l.lease_id = i.lease_id AND
                                                            l.contract_currency_id = d.currency_from AND
                                                            I.company_id = S.company_id AND
                                                            s.currency_type_id = 1 AND
                                                            exchange_date <= case when length(est_in_svc_date) = 6 then
                                                                               To_date( est_in_svc_date, 'yyyymm' ) 
                                                                             else
                                                                               To_date( est_in_svc_date, 'yyyymmdd' ) 
                                                                             end AND
                                                            currency_to = s.currency_id AND
                                                            i.import_run_id = a_run_id ) b
                                            WHERE  b.ilr_id = a.ilr_id
											AND b.est_in_svc_date = a.est_in_svc_date)
      WHERE  a.import_run_id = a_run_id;

      --Collect all ILR's don't include the negative ID's
      SELECT *
      bulk   collect INTO l_ilr_import
      FROM   LSR_IMPORT_ILR
      WHERE  import_run_id = a_run_id AND
             Nvl( ilr_id, -1 ) > 0;

      --Loop and insert
      forall l_index IN indices OF l_ilr_import
        --Insert to LSR_ILR
        INSERT INTO LSR_ILR
                    (ilr_id,
                     ilr_status_id,
                     ilr_number,
                     lease_id,
                     company_id,
                     est_in_svc_date,
                     external_ilr,
                     ilr_group_id,
                     notes,
                     current_revision,
                     workflow_type_id)
        VALUES      (L_ilr_import( l_index ).ilr_id,
                     1,
                     L_ilr_import( l_index ).ilr_number,
                     L_ilr_import( l_index ).lease_id,
                     L_ilr_import( l_index ).company_id,
                     case when length(L_ilr_import( l_index ).est_in_svc_date) = 6 then
                       To_date( L_ilr_import( l_index ).est_in_svc_date, 'yyyymm' ) 
                     else
                       To_date( L_ilr_import( l_index ).est_in_svc_date, 'yyyymmdd' ) 
                     end,
                     L_ilr_import( l_index ).external_ilr,
                     L_ilr_import( l_index ).ilr_group_id,
                     L_ilr_import( l_index ).notes,
                     1,
                     L_ilr_import( l_index ).workflow_type_id);

      forall l_index IN indices OF l_ilr_import
        --Insert into Approval
        INSERT INTO LSR_ILR_APPROVAL
                    (ilr_id,
                     revision,
                     approval_type_id,
                     approval_status_id)
        VALUES      (L_ilr_import( l_index ).ilr_id,
                     1,
                     L_ilr_import( l_index ).workflow_type_id,
                     1);

      forall l_index IN indices OF l_ilr_import
        --Insert to Options based on ILR Group?
        INSERT INTO LSR_ILR_OPTIONS
                    (ilr_id,
                     revision,
                     purchase_option_type_id,
                     purchase_option_amt,
                     renewal_option_type_id,
                     cancelable_type_id,
                     itc_sw,
                     lease_cap_type_id,
                     termination_amt,
                     sublease_flag,
                     sublease_id,
                     likely_to_collect,
                     intent_to_purchase,
                     specialized_asset,
                     interco_lease_flag,
                     interco_lease_company,
                     in_service_exchange_rate,
                     import_run_id)
        ( SELECT L_ilr_import ( l_index ).ilr_id,
                 1,
                 L_ilr_import ( l_index ).purchase_option_type_id,
                 NVL(L_ilr_import ( l_index ).purchase_option_amt, 0),
                 L_ilr_import ( l_index ).renewal_option_type_id,
                 L_ilr_import ( l_index ).cancelable_type_id,
                 L_ilr_import ( l_index ).itc_sw,
                 L_ilr_import ( l_index ).cap_type_id,
                 NVL(L_ilr_import ( l_index ).termination_amt, 0),
                 L_ilr_import ( l_index ).sublease_flag,
                 L_ilr_import ( l_index ).sublease_id,
                 L_ilr_import ( l_index ).likely_to_collect,
                 L_ilr_import ( l_index ).intent_to_purch,
                 L_ilr_import ( l_index ).specialized_asset,
                 L_ilr_import ( l_index ).intercompany_lease,
                 L_ilr_import ( l_index ).intercompany_company,
                 L_ilr_import ( l_index ).in_service_exchange_rate,
                 a_run_id
          FROM   LSR_ILR_GROUP
          WHERE  ilr_group_id = L_ilr_import( l_index ).ilr_group_id );

      -- Default ILR Accounts
      forall L_INDEX in indices of L_ILR_IMPORT
         insert into LSR_ILR_ACCOUNT
            (ILR_ID, INT_ACCRUAL_ACCOUNT_ID, INT_EXPENSE_ACCOUNT_ID, EXEC_ACCRUAL_ACCOUNT_ID,
             EXEC_EXPENSE_ACCOUNT_ID, CONT_ACCRUAL_ACCOUNT_ID, CONT_EXPENSE_ACCOUNT_ID,
             st_receivable_account_id, lt_receivable_account_id, ar_account_id,
             unguaran_res_account_id, int_unguaran_res_account_id,
             sell_profit_loss_account_id, ini_direct_cost_account_id, prop_plant_account_id,
             DEFERRED_RENT_ACCT_ID, ACCRUED_RENT_ACCT_ID,
             CURR_GAIN_LOSS_ACCT_ID, CURR_GAIN_LOSS_offset_ACCT_ID,
             def_selling_profit_account_id, incurred_costs_account_id, def_costs_account_id,
             cost_of_goods_sold_account_id, revenue_account_id, lt_def_sell_profit_account_id)
            select L_ILR_IMPORT(L_INDEX).ILR_ID,
                   INT_ACCRUAL_ACCOUNT_ID,
                   INT_EXPENSE_ACCOUNT_ID,
                   EXEC_ACCRUAL_ACCOUNT_ID,
                   EXEC_EXPENSE_ACCOUNT_ID,
                   CONT_ACCRUAL_ACCOUNT_ID,
                   CONT_EXPENSE_ACCOUNT_ID,
                   st_receivable_account_id,
                   lt_receivable_account_id,
                   ar_account_id,
                   unguaran_res_account_id,
                   int_unguaran_res_account_id,
                   sell_profit_loss_account_id,
                   ini_direct_cost_account_id,
                   prop_plant_account_id,
				           DEFERRED_RENT_ACCT_ID,
                   ACCRUED_RENT_ACCT_ID,
                   CURR_GAIN_LOSS_ACCT_ID,
                   CURR_GAIN_LOSS_OFFSET_ACCT_ID,
                   def_selling_profit_account_id,
                   incurred_costs_account_id,
                   def_costs_account_id,
                   cost_of_goods_sold_account_id,
                   revenue_account_id,
                   lt_def_sell_profit_account_id
              from LSR_ILR_GROUP
             where ILR_GROUP_ID = L_ILR_IMPORT(L_INDEX).ILR_GROUP_ID;

	  --Insert to Class Codes
      cc_needed := FALSE;

      l_cc_sql := 'DECLARE BEGIN ';

      FOR l_index IN 1 .. l_ilr_import.count LOOP
          IF L_ilr_import( l_index ).class_code_id1 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value1 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id1 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value1 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id2 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value2 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id2 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value2 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id3 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value3 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id3 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value3 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id4 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value4 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id4 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value4 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id5 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value5 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id5 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value5 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id6 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value6 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id6 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value6 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id7 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value7 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id7 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value7 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id8 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value8 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id8 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value8 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id9 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value9 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id9 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value9 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id10 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value10 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id10 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value10 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id11 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value11 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id11 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value11 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id12 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value12 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id12 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value12 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id13 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value13 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id13 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value13 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id14 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value14 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id14 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value14 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id15 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value15 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id15 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value15 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id16 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value16 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id16 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value16 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id17 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value17 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id17 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value17 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id18 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value18 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id18 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value18 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id19 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value19 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id19 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value19 )
                        || ''');';
          END IF;

          IF L_ilr_import( l_index ).class_code_id20 IS NOT NULL AND
             L_ilr_import( l_index ).class_code_value20 IS NOT NULL THEN
            cc_needed := TRUE;

            l_cc_sql := l_cc_sql
                        || ' INSERT INTO LSR_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values ('
                        || To_char( L_ilr_import( l_index ).class_code_id20 )
                        || ', '
                        || To_char( L_ilr_import( l_index ).ilr_id )
                        || ', '''
                        || To_char( L_ilr_import( l_index ).class_code_value20 )
                        || ''');';
          END IF;
      END LOOP;

      l_cc_sql := l_cc_sql
                  || ' END;';

      IF cc_needed THEN
        l_msg := Substr( l_cc_sql, 0, 32000 );

        EXECUTE IMMEDIATE l_cc_sql;
      END IF;

      --Insert to ILR Rates lsr_ilr_rate_types

      --Sales Type Discount Rate
      SELECT rate_type_id
      INTO l_rate_type_id
      FROM lsr_ilr_rate_types
      WHERE LOWER(TRIM(DESCRIPTION)) = 'sales type discount rate override';

      SELECT *
      bulk   collect INTO l_ilr_import
      FROM   LSR_IMPORT_ILR
      WHERE  import_run_id = a_run_id AND
             sales_type_disc_rate_override IS NOT NULL AND
             Nvl( ilr_id, -1 ) > 0;

      forall l_index IN indices OF l_ilr_import
        INSERT INTO LSR_ILR_RATES
                    (ilr_id,
                     revision,
                     rate_type_id,
                     rate)
        VALUES      ( L_ilr_import( l_index ).ilr_id,
                     1,
                     l_rate_type_id,
                     L_ilr_import( l_index ).sales_type_disc_rate_override);

      --Direct Finance Discount Rate
      SELECT rate_type_id
      INTO l_rate_type_id
      FROM lsr_ilr_rate_types
      WHERE LOWER(TRIM(DESCRIPTION)) = 'direct finance discount rate override';

      SELECT *
      bulk   collect INTO l_ilr_import
      FROM   LSR_IMPORT_ILR
      WHERE  import_run_id = a_run_id AND
             direct_fin_disc_rate_override IS NOT NULL AND
             Nvl( ilr_id, -1 ) > 0;

      forall l_index IN indices OF l_ilr_import
        INSERT INTO LSR_ILR_RATES
                    (ilr_id,
                     revision,
                     rate_type_id,
                     rate)
        VALUES      ( L_ilr_import( l_index ).ilr_id,
                     1,
                     l_rate_type_id,
                     L_ilr_import( l_index ).direct_fin_disc_rate_override);

      --Direct Finance Interest on Net Inv Rate
      SELECT rate_type_id
      INTO l_rate_type_id
      FROM lsr_ilr_rate_types
      WHERE LOWER(TRIM(DESCRIPTION)) = 'direct finance interest on net inv rate override';

      SELECT *
      bulk   collect INTO l_ilr_import
      FROM   LSR_IMPORT_ILR
      WHERE  import_run_id = a_run_id AND
             direct_fin_int_rate_override IS NOT NULL AND
             Nvl( ilr_id, -1 ) > 0;

      forall l_index IN indices OF l_ilr_import
        INSERT INTO LSR_ILR_RATES
                    (ilr_id,
                     revision,
                     rate_type_id,
                     rate)
        VALUES      ( L_ilr_import( l_index ).ilr_id,
                     1,
                     l_rate_type_id,
                     L_ilr_import( l_index ).direct_fin_int_rate_override);

      --Insert to Payment Terms using the c_bucket_amounts collection
      SELECT *
      bulk   collect INTO l_ilr_import
      FROM   LSR_IMPORT_ILR
      WHERE  import_run_id = a_run_id;

      forall l_index IN indices OF l_ilr_import
        INSERT INTO LSR_ILR_PAYMENT_TERM
                    (ilr_id,
                     payment_term_id,
                     payment_term_type_id,
                     payment_term_date,
                     payment_freq_id,
                     number_of_terms,
                     est_executory_cost,
                     payment_amount,
                     contingent_amount,
                     revision,
                     c_bucket_1,
                     c_bucket_2,
                     c_bucket_3,
                     c_bucket_4,
                     c_bucket_5,
                     c_bucket_6,
                     c_bucket_7,
                     c_bucket_8,
                     c_bucket_9,
                     c_bucket_10,
                     e_bucket_1,
                     e_bucket_2,
                     e_bucket_3,
                     e_bucket_4,
                     e_bucket_5,
                     e_bucket_6,
                     e_bucket_7,
                     e_bucket_8,
                     e_bucket_9,
                     e_bucket_10)
        VALUES      (Abs( L_ilr_import( l_index ).ilr_id ),
                     L_ilr_import( l_index ).payment_term_id,
                     nvl(l_ilr_import( l_index ).payment_term_type_id, 2),
                     case when length(L_ilr_import( l_index ).payment_term_date) = 6 then
                       To_date( L_ilr_import( l_index ).payment_term_date, 'yyyymm' )
                     else
                       To_date( L_ilr_import( l_index ).payment_term_date, 'yyyymmdd' )
                     end,
                     L_ilr_import( l_index ).payment_freq_id,
                     L_ilr_import( l_index ).number_of_terms,
                     L_ilr_import( l_index ).est_executory_cost,
                     L_ilr_import( l_index ).paid_amount,
                     L_ilr_import( l_index ).contingent_amount,
                     1,
                     L_ilr_import( l_index ).c_bucket_1,
                     L_ilr_import( l_index ).c_bucket_2,
                     L_ilr_import( l_index ).c_bucket_3,
                     L_ilr_import( l_index ).c_bucket_4,
                     L_ilr_import( l_index ).c_bucket_5,
                     L_ilr_import( l_index ).c_bucket_6,
                     L_ilr_import( l_index ).c_bucket_7,
                     L_ilr_import( l_index ).c_bucket_8,
                     L_ilr_import( l_index ).c_bucket_9,
                     L_ilr_import( l_index ).c_bucket_10,
                     L_ilr_import( l_index ).e_bucket_1,
                     L_ilr_import( l_index ).e_bucket_2,
                     L_ilr_import( l_index ).e_bucket_3,
                     L_ilr_import( l_index ).e_bucket_4,
                     L_ilr_import( l_index ).e_bucket_5,
                     L_ilr_import( l_index ).e_bucket_6,
                     L_ilr_import( l_index ).e_bucket_7,
                     L_ilr_import( l_index ).e_bucket_8,
                     L_ilr_import( l_index ).e_bucket_9,
                     L_ilr_import( l_index ).e_bucket_10);

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_import_ilrs;

  FUNCTION F_validate_ilrs( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
	type ILR_IMPORT_TABLE is table of LSR_IMPORT_ILR%rowtype;
    L_ILR_IMPORT_TABLE ILR_IMPORT_TABLE;
	L_INDEX   		number;
    L_CC_SQL 			clob;
    CC_IMPORTED 		boolean;
    CC_REQUIRED_NUM  	number(22, 0);
    CC_MISSING 		varchar2(2000);
  BEGIN
      --check we don't have duplicate ILR numbers
      -- CJS 3/12/15 by lease/company
      UPDATE LSR_IMPORT_ILR z
      SET    error_message = error_message
                             || ' Duplicate ilr number.'
      WHERE  import_run_id = a_run_id AND
             ( ilr_number, lease_id, company_id ) IN
             ( SELECT ilr_number,
                      lease_id,
                      company_id
               FROM   ( SELECT ilr_number,
                               lease_id,
                               company_id,
                               Count( * ) AS MY_COUNT
                        FROM   ( SELECT ilr_number,
                                        lease_id,
                                        company_id
                                 FROM   LSR_ILR
                                 WHERE  ( lease_id, company_id ) IN
                                        ( SELECT DISTINCT lease_id,
                                                          company_id
                                          FROM   LSR_IMPORT_ILR )
                                 UNION ALL
                                 SELECT ilr_number,
                                        lease_id,
                                        company_id
                                 FROM   ( SELECT DISTINCT ilr_number,
                                                          lease_id,
                                                          company_id,
                                                          unique_ilr_identifier
                                          FROM   LSR_IMPORT_ILR
                                          WHERE  import_run_id = a_run_id ) )
                        GROUP  BY ilr_number,lease_id,company_id )
               WHERE  my_count > 1 );

      -- check the payment term types
      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Invalid Payment Term Type - only Normal or Partial Month are allowed.'
      WHERE  import_run_id = a_run_id AND
             line_id IN
             ( SELECT line_id
               FROM   LSR_IMPORT_ILR 
               WHERE  payment_term_type_id not in (2,4));
               
      -- Check to make sure that only one payment term type is used for the payment terms
      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Payment Term Types cannot be combined - only a single value can be used for all terms.'
      WHERE  import_run_id = a_run_id AND
             (import_run_id, line_id) IN
             ( SELECT import_run_id, line_id
               FROM   LSR_IMPORT_ILR a
               WHERE  exists (
                      select b.unique_ilr_identifier, count(distinct b.payment_term_type_id)
                        from LSR_IMPORT_ILR b
                       where a.unique_ilr_identifier = b.unique_ilr_identifier
                       group by b.unique_ilr_identifier 
                      having count(distinct b.payment_term_type_id) > 1));
      
      --check the payment terms have sequential payment term ids
      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Non sequential payment term ids.'
      WHERE  import_run_id = a_run_id AND
             line_id IN
             ( SELECT line_id
               FROM   ( SELECT line_id,
                               payment_term_id,
                               Row_number( )
                                 over(
                                   PARTITION BY unique_ilr_identifier
                                   ORDER BY unique_ilr_identifier, payment_term_id) MY_TERMS
                        FROM   LSR_IMPORT_ILR
                        WHERE  import_run_id = a_run_id )
               WHERE  payment_term_id <> my_terms );

      --check that one term ends where the next begins
      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Start of ILR term does not match with end of previous term.'
      WHERE  import_run_id = a_run_id AND
             line_id IN
             ( SELECT line_id
               FROM   ( SELECT line_id,
                               term_start,
                               Lag( term_end, 1, To_date( '07/04/1776', 'mm/dd/yyyy' ) )
                                 over(
                                   PARTITION BY unique_ilr_identifier
                                   ORDER BY unique_ilr_identifier, term_start) AS TERM_END
                        FROM   ( SELECT line_id,
                                        unique_ilr_identifier,
                                        case when length(payment_term_date) = 6 then
                                          To_date( payment_term_date, 'yyyymm' ) 
                                        else
                                          To_date( payment_term_date, 'yyyymmdd' ) 
                                        end AS TERM_START,
                                        Add_months( case when length(payment_term_date) = 6 then
                                                      To_date( payment_term_date, 'yyyymm' ) 
                                                    else
                                                      To_date( payment_term_date, 'yyyymmdd' ) 
                                                    end, number_of_terms * Decode( payment_freq_id, 1, 12,
                                                                                                                                       2, 6,
                                                                                                                                       3, 3,
                                                                                                                                       4, 1 ) ) AS TERM_END
                                 FROM   LSR_IMPORT_ILR
                                 WHERE  import_run_id = a_run_id ) )
               WHERE  term_start <> term_end AND
                      term_end <> To_date( '07/04/1776', 'mm/dd/yyyy' ) );

      --make sure the company is valid for the lease
      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Company not valid for this MLA.'
      WHERE  import_run_id = a_run_id AND
             company_id NOT IN ( SELECT company_id
                                 FROM   LSR_LEASE_COMPANY
                                 WHERE  lease_id = LSR_IMPORT_ILR.lease_id );

      --check that any buckets being imported are enabled.
      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Executory bucket 1 is not enabled.'
      WHERE  NOT ( e_bucket_1 IS NULL  OR
                   e_bucket_1 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Executory' AND
                        bucket_number = 1 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Executory' AND
                            bucket_number = 1 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Executory bucket 2 is not enabled.'
      WHERE  NOT ( e_bucket_2 IS NULL  OR
                   e_bucket_2 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Executory' AND
                        bucket_number = 2 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Executory' AND
                            bucket_number = 2 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Executory bucket 3 is not enabled.'
      WHERE  NOT ( e_bucket_3 IS NULL  OR
                   e_bucket_3 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Executory' AND
                        bucket_number = 3 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Executory' AND
                            bucket_number = 3 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Executory bucket 4 is not enabled.'
      WHERE  NOT ( e_bucket_4 IS NULL  OR
                   e_bucket_4 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Executory' AND
                        bucket_number = 4 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Executory' AND
                            bucket_number = 4 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Executory bucket 5 is not enabled.'
      WHERE  NOT ( e_bucket_5 IS NULL  OR
                   e_bucket_5 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Executory' AND
                        bucket_number = 5 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Executory' AND
                            bucket_number = 5 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Executory bucket 6 is not enabled.'
      WHERE  NOT ( e_bucket_6 IS NULL  OR
                   e_bucket_6 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Executory' AND
                        bucket_number = 6 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Executory' AND
                            bucket_number = 6 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Executory bucket 7 is not enabled.'
      WHERE  NOT ( e_bucket_7 IS NULL  OR
                   e_bucket_7 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Executory' AND
                        bucket_number = 7 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Executory' AND
                            bucket_number = 7 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Executory bucket 8 is not enabled.'
      WHERE  NOT ( e_bucket_8 IS NULL  OR
                   e_bucket_8 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Executory' AND
                        bucket_number = 8 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Executory' AND
                            bucket_number = 8 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Executory bucket 9 is not enabled.'
      WHERE  NOT ( e_bucket_9 IS NULL  OR
                   e_bucket_9 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Executory' AND
                        bucket_number = 9 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Executory' AND
                            bucket_number = 9 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Executory bucket 10 is not enabled.'
      WHERE  NOT ( e_bucket_10 IS NULL  OR
                   e_bucket_10 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Executory' AND
                        bucket_number = 10 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Executory' AND
                            bucket_number = 10 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Contingent bucket 1 is not enabled.'
      WHERE  NOT ( c_bucket_1 IS NULL  OR
                   c_bucket_1 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Contingent' AND
                        bucket_number = 1 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Contingent' AND
                            bucket_number = 1 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Contingent bucket 2 is not enabled.'
      WHERE  NOT ( c_bucket_2 IS NULL  OR
                   c_bucket_2 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Contingent' AND
                        bucket_number = 2 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Contingent' AND
                            bucket_number = 2 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Contingent bucket 3 is not enabled.'
      WHERE  NOT ( c_bucket_3 IS NULL  OR
                   c_bucket_3 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Contingent' AND
                        bucket_number = 3 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Contingent' AND
                            bucket_number = 3 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Contingent bucket 4 is not enabled.'
      WHERE  NOT ( c_bucket_4 IS NULL  OR
                   c_bucket_4 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Contingent' AND
                        bucket_number = 4 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Contingent' AND
                            bucket_number = 4 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Contingent bucket 5 is not enabled.'
      WHERE  NOT ( c_bucket_5 IS NULL  OR
                   c_bucket_5 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Contingent' AND
                        bucket_number = 5 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Contingent' AND
                            bucket_number = 5 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Contingent bucket 6 is not enabled.'
      WHERE  NOT ( c_bucket_6 IS NULL  OR
                   c_bucket_6 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Contingent' AND
                        bucket_number = 6 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Contingent' AND
                            bucket_number = 6 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Contingent bucket 7 is not enabled.'
      WHERE  NOT ( c_bucket_7 IS NULL  OR
                   c_bucket_7 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Contingent' AND
                        bucket_number = 7 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Contingent' AND
                            bucket_number = 7 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Contingent bucket 8 is not enabled.'
      WHERE  NOT ( c_bucket_8 IS NULL  OR
                   c_bucket_8 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Contingent' AND
                        bucket_number = 8 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Contingent' AND
                            bucket_number = 8 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Contingent bucket 9 is not enabled.'
      WHERE  NOT ( c_bucket_9 IS NULL  OR
                   c_bucket_9 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Contingent' AND
                        bucket_number = 9 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Contingent' AND
                            bucket_number = 9 ) );

      UPDATE LSR_IMPORT_ILR
      SET    error_message = error_message
                             || ' Contingent bucket 10 is not enabled.'
      WHERE  NOT ( c_bucket_10 IS NULL  OR
                   c_bucket_10 = 0 ) AND
             import_run_id = a_run_id AND
             ( EXISTS
               ( SELECT *
                 FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                 WHERE  receivable_type = 'Contingent' AND
                        bucket_number = 10 AND
                        status_code_id = 0 )  OR
               NOT EXISTS
                   ( SELECT *
                     FROM   LSR_RECEIVABLE_BUCKET_ADMIN
                     WHERE  receivable_type = 'Contingent' AND
                            bucket_number = 10 ) );

      --Validate if Sublease Flag is no, there is no sublease
      UPDATE LSR_IMPORT_ILR a
      SET    error_message = error_message
                             || ' Sublease populated when Sublease Flag set to "No".'
      WHERE  import_run_id = a_run_id AND
             sublease_flag = 0 AND
             sublease_id IS NOT NULL;

      -- Validate Likely to collect is 1
      UPDATE LSR_IMPORT_ILR a
      SET    error_message = error_message
                             || ' Likely to Collect must be set to "Yes".'
      WHERE  import_run_id = a_run_id AND
             likely_to_collect = 0;

      -- Validate Cap Type is active
      UPDATE LSR_IMPORT_ILR a
      SET    error_message = error_message
                             || ' Capital Type is not active.'
      WHERE  import_run_id = a_run_id AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LSR_CAP_TYPE b
                   WHERE  a.cap_type_id = b.cap_type_id AND
                          active = 1 );

      --Validate intercompany Lease company is a lease company
      UPDATE LSR_IMPORT_ILR a
      SET    error_message = error_message
                             || ' Intercompany Lease Company must be a Lease Company.'
      WHERE  import_run_id = a_run_id AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   COMPANY_SETUP
                   WHERE  intercompany_company = company_id AND
                          is_lease_company = 1 ) AND
             intercompany_company IS NOT NULL;

      --Validate rates aren't < 0
      UPDATE LSR_IMPORT_ILR A
      SET error_message = error_message || ' Override Rates cannot be negative. Please enter a Rate Override of 0% or greater.'
      WHERE import_run_id = a_run_id
      AND (sales_type_disc_rate_override < 0
            OR direct_fin_disc_rate_override < 0
            OR direct_fin_int_rate_override < 0);

      -- PP-51229 - Add validation for class codes
      for L_CC_NO in 1 .. 20
       loop
	     L_CC_SQL := 'DECLARE BEGIN
	     update LSR_IMPORT_ILR a
		    set ERROR_MESSAGE = ERROR_MESSAGE || '' Class Code '||to_char(L_CC_NO)||' is not a valid ILR class code.''
		   where IMPORT_RUN_ID = '||to_char(A_RUN_ID)||'
			  and CLASS_CODE_ID'||to_char(L_CC_NO)||' is not null
			  and CLASS_CODE_VALUE'||to_char(L_CC_NO)||' is not null
			  and  not exists (select 1 from CLASS_CODE b where a.CLASS_CODE_ID'||to_char(L_CC_NO)||' = b.CLASS_CODE_ID and b.LSR_ILR_INDICATOR = 1);

	     update LSR_IMPORT_ILR a
		    set ERROR_MESSAGE = ERROR_MESSAGE || '' Class Code Value '||to_char(L_CC_NO)||' is not a valid value.''
		   where IMPORT_RUN_ID = '||to_char(A_RUN_ID)||'
			  and CLASS_CODE_ID'||to_char(L_CC_NO)||' is not null
			  and CLASS_CODE_VALUE'||to_char(L_CC_NO)||' is not null
			  and exists (select 1 from CLASS_CODE b
			               where b.CLASS_CODE_ID = a.CLASS_CODE_ID'||to_char(L_CC_NO)||'
				            and b.LSR_ILR_INDICATOR = 1
                            and lower(CLASS_CODE_TYPE) = ''standard'')
			  and not exists (select 1 from CLASS_CODE_VALUES C where C.CLASS_CODE_ID = a.CLASS_CODE_ID'||to_char(L_CC_NO)||' and c.VALUE = a.CLASS_CODE_VALUE'||to_char(L_CC_NO)||');

	      END;';

	     execute immediate L_CC_SQL;
      end loop;

   -- PP-51674 - Check for Required Class Codes

     select count(*) into CC_REQUIRED_NUM
     from class_code
     where lsr_ilr_indicator = 1
     and required = 1;

     if CC_REQUIRED_NUM > 0 then
            L_MSG := 'Check for required class codes';

            select * bulk collect
                into L_ILR_IMPORT_TABLE
                from LSR_IMPORT_ILR
            where IMPORT_RUN_ID = A_RUN_ID;

            for L_INDEX in 1 .. L_ILR_IMPORT_TABLE.COUNT
            loop
                CC_IMPORTED := false;
                L_CC_SQL := 'select listagg(description, '','') within group (order by description) description
                                from class_Code where class_code_id in (
                                select class_code_id from class_code where lsr_ilr_indicator = 1 and required = 1
                                minus
                                select class_code_id from class_code where class_code_id in (';

                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE1 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE2 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE3 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE4 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE5 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE6 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE7 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE8 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE9 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE10 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE11 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE12 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE13 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE14 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE15 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE16 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE17 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE18 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE19 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE20 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20) || ',';
                end if;

                L_CC_SQL :=  trim(TRAILING ',' FROM L_CC_SQL);
                L_CC_SQL := L_CC_SQL || '))';

                if CC_IMPORTED then
                      execute immediate L_CC_SQL into CC_MISSING;

                      if CC_MISSING is not null then
                              UPDATE LSR_IMPORT_ILR a
                              SET    error_message = error_message
                                                    || ' Required Lessor ILR Class Codes are Missing:' || CC_MISSING
                              WHERE  import_run_id = a_run_id
                              and line_id = L_ILR_IMPORT_TABLE(L_INDEX).LINE_ID;
                      end if;
                else
                  --There are required class codes and none were imported, so all are missing
                  L_CC_SQL := 'select listagg(description, '','') within group (order by description) description
                                from class_Code where lsr_ilr_indicator = 1 and required = 1';

                  execute immediate L_CC_SQL into CC_MISSING;

                  if CC_MISSING is not null then
                          UPDATE LSR_IMPORT_ILR a
                          SET    error_message = error_message
                                                || ' Required Lessor ILR Class Codes are Missing:' || CC_MISSING
                          WHERE  import_run_id = a_run_id
                          and line_id = L_ILR_IMPORT_TABLE(L_INDEX).LINE_ID;
                  end if;
                end if;
              end loop;

     end if;




      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_validate_ilrs;

  --##########################################################################
  --                            ILR RENEWALS
  --##########################################################################

  FUNCTION F_import_ilr_renewals( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Assign Revisions to those not supplied with the max() Initiated Revision for the ILR
      UPDATE LSR_IMPORT_ILR_RENEWAL a
      SET    revision = ( SELECT Max( revision )
                          FROM   LSR_ILR_APPROVAL b
                          WHERE  a.ilr_id = b.ilr_id AND
                                 b.approval_status_id IN ( 1 ) )
      WHERE  import_run_id = a_run_id AND
             a.revision IS NULL;

      --Remove from Renewal Options
      DELETE FROM LSR_ILR_RENEWAL_OPTIONS
      WHERE  ilr_renewal_id IN
             ( SELECT ilr_renewal_id
               FROM   LSR_ILR_RENEWAL a
               WHERE  ( a.ilr_id, a.revision ) IN
                      ( SELECT b.ilr_id,
                               b.revision
                        FROM   LSR_IMPORT_ILR_RENEWAL b
                        WHERE  import_run_id = a_run_id ) );

      --Remove from Renewal
      DELETE FROM LSR_ILR_RENEWAL a
      WHERE  ( a.ilr_id, a.revision ) IN
             ( SELECT b.ilr_id,
                      b.revision
               FROM   LSR_IMPORT_ILR_RENEWAL b
               WHERE  import_run_id = a_run_id );

      --Assign new Renewal ID from sequence to first rows for the matching ILR and Revision
      UPDATE LSR_IMPORT_ILR_RENEWAL
      SET    ilr_renewal_id = lsr_ilr_renewal_seq.NEXTVAL
      WHERE  line_id IN
             ( SELECT line_id
               FROM   ( SELECT line_id,
                               Row_number( )
                                 over (
                                   PARTITION BY ilr_id, revision, ilr_renewal_option_id
                                   ORDER BY ilr_id, revision, ilr_renewal_option_id) NUM
                        FROM   LSR_IMPORT_ILR_RENEWAL
                        WHERE  import_run_id = a_run_id )
               WHERE  num = 1 ) AND
             import_run_id = a_run_id;

      --Insert headers to lsr_ilr_renewal
      INSERT INTO LSR_ILR_RENEWAL
                  (ilr_renewal_id,
                   ilr_id,
                   revision,
                   renewal_options_id)
      SELECT ilr_renewal_id,
             ilr_id,
             revision,
             ilr_renewal_option_id
      FROM   LSR_IMPORT_ILR_RENEWAL
      WHERE  import_run_id = a_run_id AND
             ilr_renewal_id IS NOT NULL;

      --assign the renewal id to all the other renewal options
      UPDATE LSR_IMPORT_ILR_RENEWAL a
      SET    ilr_renewal_id = ( SELECT DISTINCT ilr_renewal_id
                                FROM   LSR_IMPORT_ILR_RENEWAL b
                                WHERE  b.import_run_id = a_run_id AND
                                       a.ilr_id = b.ilr_id AND
                                       a.revision = b.revision AND
                                       a.ilr_renewal_option_id = b.ilr_renewal_option_id AND
                                       b.ilr_renewal_id IS NOT NULL )
      WHERE  a.import_run_id = a_run_id AND
             ilr_renewal_id IS NULL;

      --Insert to lsr_ilr_renewal_options
      INSERT INTO LSR_ILR_RENEWAL_OPTIONS
                  (ilr_renewal_option_id,
                   ilr_renewal_id,
                   ilr_renewal_probability_id,
                   payment_freq_id,
                   number_of_terms,
                   amount_per_term,
                   renewal_start_date,
                   notice_requirement_months)
      SELECT lsr_ilr_renewal_options_seq.NEXTVAL,
             ilr_renewal_id,
             ilr_renewal_prob_id,
             renewal_frequency_id,
             renewal_number_of_terms,
             renewal_amount_per_term,
             To_date( renewal_start_date, 'mm/dd/yyyy' ),
             renewal_notice
      FROM   LSR_IMPORT_ILR_RENEWAL
      WHERE  ilr_renewal_id IS NOT NULL AND
             import_run_id = a_run_id;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_import_ilr_renewals;

  FUNCTION F_validate_ilr_renewals( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Validate this is a valid Revision for this ILR if provided
      UPDATE LSR_IMPORT_ILR_RENEWAL a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision does not exist. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LSR_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision );

      --Check supplied Revision is open
      UPDATE LSR_IMPORT_ILR_RENEWAL a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision is in a non-editable status. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LSR_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision AND
                          b.approval_status_id IN ( 1 ) );

      --Validate where we have revisions in "Initiated" status for the ILR for us to apply these to
      UPDATE LSR_IMPORT_ILR_RENEWAL a
      SET    error_message = error_message
                             || ' No editable revision exists for the ILR. Please create a new revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LSR_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          b.approval_status_id IN ( 1 ) );

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_validate_ilr_renewals;

  --##########################################################################
  --                            CPR ASSETS
  --##########################################################################
  FUNCTION F_import_cpr_assets( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    TYPE cpr_asset_import_table
      IS TABLE OF lsr_import_cpr_asset%ROWTYPE INDEX BY PLS_INTEGER;
    l_asset_import CPR_ASSET_IMPORT_TABLE;
    l_index        NUMBER;
    l_msg          VARCHAR2(32000);
  BEGIN
      --Get Description and Serial Number off of the CPR Ledger for the asset
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    ( a.description, a.serial_number ) =
             ( SELECT b.description,
                      b.serial_number
               FROM   CPR_LEDGER b
               WHERE  b.asset_id = a.cpr_asset_id )
      WHERE  import_run_id = a_run_id;

      --Get Carrying Cost from CPR
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    carrying_cost = ( SELECT ds.carrying_cost
                               FROM   ( SELECT asset_dollars - depr_reserve                    AS carrying_cost,
                                               Row_number( )
                                                 over (
                                                   PARTITION BY d.asset_id
                                                   ORDER BY d.asset_id, gl_posting_mo_yr DESC) AS max_gl_date,
                                               d.asset_id
                                        FROM   CPR_DEPR d,
                                               LSR_IMPORT_CPR_ASSET b,
                                               LSR_ILR i
                                        WHERE  d.asset_id = b.cpr_asset_id AND
                                               set_of_books_id = 1 AND
                                               i.ilr_id = b.ilr_id AND
                                               gl_posting_mo_yr < i.est_in_svc_date AND
                                               b.import_run_id = a_run_id ) ds
                               WHERE  ds.max_gl_date = 1 AND
                                      a.cpr_asset_id = ds.asset_id )
      WHERE  import_run_id = a_run_id;

      --Calc the Estimated Residual Pct based on the Supplied Residual and FMV where Pct not supplied
      --If Fair Market value 0 then estimated residual % = 0
      UPDATE LSR_IMPORT_CPR_ASSET
      SET    estimated_residual_pct = Decode( Nvl( fair_market_value, 0 ), 0, 0,
                                                                           estimated_residual / fair_market_value )
      WHERE  estimated_residual_pct IS NULL AND
             import_run_id = a_run_id;

      UPDATE LSR_IMPORT_CPR_ASSET
      SET    estimated_residual = estimated_residual_pct * Nvl( fair_market_value, 0 )
      WHERE  estimated_residual IS NULL AND
             import_run_id = a_run_id;

      --Do Calcs for stuff that is calculated
      UPDATE LSR_IMPORT_CPR_ASSET
      SET    unguaranteed_residual = Nvl( estimated_residual, 0 ) - Nvl( guaranteed_residual, 0 )
      WHERE  import_run_id = a_run_id;

      --Assign In Service Exchange Rate
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    in_svc_exchange_rate = ( SELECT i.in_service_exchange_rate
                                      FROM   LSR_ILR_OPTIONS i
                                      WHERE  i.ilr_id = a.ilr_id AND
                                             i.revision = a.revision )
      WHERE  import_run_id = a_run_id;

      --Insert the Assets
      INSERT INTO LSR_ASSET
                  (lsr_asset_id,
                   ilr_id,
                   revision,
                   description,
                   fair_market_value,
                   carrying_cost,
                   guaranteed_residual_amount,
                   actual_residual_amount,
                   unguaranteed_residual_amount,
                   estimated_residual_pct,
                   estimated_residual_amount,
                   expected_life,
                   economic_life,
                   serial_number,
                   long_description,
                   cpr_asset_id,
                   fair_market_value_comp_curr,
                   carrying_cost_comp_curr,
                   guaranteed_res_amt_comp_curr,
                   actual_residual_amt_comp_curr,
                   unguaranteed_res_amt_comp_curr,
                   estimated_res_amt_comp_curr)
      SELECT ls_asset_seq.NEXTVAL,
             ilr_id,
             revision,
             description,
             Nvl( fair_market_value, 0 ) * in_svc_exchange_rate,
             Nvl( carrying_cost, 0 ) * in_svc_exchange_rate,
             Nvl( guaranteed_residual, 0 ) * in_svc_exchange_rate,
             Nvl( actual_residual, 0 ) * in_svc_exchange_rate,
             Nvl( unguaranteed_residual, 0 ) * in_svc_exchange_rate,
             Nvl( estimated_residual_pct, 0 ),
             Nvl( estimated_residual, 0 ) * in_svc_exchange_rate,
             Nvl( expected_life, 0 ),
             Nvl( economic_life, 0 ),
             serial_number,
             long_description,
             cpr_asset_id,
             Nvl( fair_market_value, 0 ),
             Nvl( carrying_cost, 0 ),
             Nvl( guaranteed_residual, 0 ),
             Nvl( actual_residual, 0 ),
             Nvl( unguaranteed_residual, 0 ),
             Nvl( estimated_residual, 0)
      FROM   LSR_IMPORT_CPR_ASSET
      WHERE  import_run_id = a_run_id;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_import_cpr_assets;

  FUNCTION F_validate_cpr_assets( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      -- Check CPR Asset is valid
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    error_message = error_message
                             || ' Invalid CPR Asset ID Supplied.'
      WHERE  import_run_id = a_run_id AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   CPR_LEDGER b
                   WHERE  a.cpr_asset_id = b.asset_id );

      -- Check CPR Asset only exists once in our import
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    error_message = error_message
                             || ' Duplicate CPR Asset ID used in import. Please check that all Asset ID''s are only assigned to one ILR.'
      WHERE  import_run_id = a_run_id AND
             EXISTS
             ( SELECT 1
               FROM   LSR_IMPORT_CPR_ASSET b
               WHERE  a.cpr_asset_id = b.cpr_asset_id AND
                      a.import_run_id = b.import_run_id AND
                      a.line_id <> b.line_id );

      --Check CPR Asset isn't associated already with any ILR
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    error_message = error_message
                             || ' This CPR Asset already associated to an ILR.'
      WHERE  import_run_id = a_run_id AND
             EXISTS
             ( SELECT 1
               FROM   LSR_ASSET b
               WHERE  a.cpr_asset_id = b.cpr_asset_id );

      --Check ILR-Company vs CPR-Company
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    error_message = error_message
                             || ' The Company for the Asset does not match the Company for the ILR.'
      WHERE  import_run_id = a_run_id AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   CPR_LEDGER b,
                          LSR_ILR i
                   WHERE  a.cpr_asset_id = b.asset_id AND
                          a.ilr_id = i.ilr_id AND
                          i.company_id = b.company_id );

      --Check that supplied revision is valid for the ILR
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision does not exist. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LSR_ILR_OPTIONS b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision );

      --Check supplied Revision is open
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision is in a non-editable status. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LSR_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision AND
                          b.approval_status_id IN ( 1 ) );

      --Assign Revision to Assets imported without Revision (Assign Max Initiated Revision for ILR)
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    revision = ( SELECT Max( revision )
                          FROM   LSR_ILR_APPROVAL b
                          WHERE  a.ilr_id = b.ilr_id AND
                                 b.approval_status_id IN ( 1 ) )
      WHERE  import_run_id = a_run_id AND
             a.revision IS NULL;

      --Check everyone has a revision assigned
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    error_message = error_message
                             || ' No editable revision exists for the ILR. Please create a new revision.'
      WHERE  import_run_id = a_run_id AND
             a.revision IS NULL;

      --Check that ILR has an exchange rate
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    error_message = error_message
                             || ' No currency exchange rates exists to convert the asset. Please enter an exchange rate before associating assets.'
      WHERE  import_run_id = a_run_id AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   ( SELECT DISTINCT First_value( rate )
                                              over (
                                                PARTITION BY currency_from, currency_to
                                                ORDER BY exchange_date DESC) cont_to_comp_rate,
                                            ilr_id
                            FROM   CURRENCY_RATE_DEFAULT d,
                                   LSR_LEASE l,
                                   LSR_ILR i,
                                   CURRENCY_SCHEMA s
                            WHERE  l.lease_id = i.lease_id AND
                                   l.contract_currency_id = d.currency_from AND
                                   i.company_id = s.company_id AND
                                   exchange_date <= i.est_in_svc_date AND
                                   currency_to = s.currency_id ) b
                   WHERE  b.ilr_id = a.ilr_id );

      --Check that either Estimated Residual Percent or Amount is populated. Can't be both
      UPDATE LSR_IMPORT_CPR_ASSET a
      SET    error_message = error_message
                             || ' Cannot load both Estimated Residual Amount and Estimated Residual Percent. Please populate only one of these fields.'
      WHERE  import_run_id = a_run_id AND
             estimated_residual IS NOT NULL AND
             estimated_residual_pct IS NOT NULL;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_validate_cpr_assets;

  --##########################################################################
  --                            EXTERNAL ASSETS
  --##########################################################################
  FUNCTION F_import_external_assets( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    TYPE cpr_asset_import_table
      IS TABLE OF lsr_import_ext_asset%ROWTYPE INDEX BY PLS_INTEGER;
    l_asset_import CPR_ASSET_IMPORT_TABLE;
    l_index        NUMBER;
    l_msg          VARCHAR2(32000);
  BEGIN
      --Assign In Service Exchange Rate
      UPDATE LSR_IMPORT_EXT_ASSET a
      SET    in_svc_exchange_rate = ( SELECT i.in_service_exchange_rate
                                      FROM   LSR_ILR_OPTIONS i
                                      WHERE  i.ilr_id = a.ilr_id AND
                                             i.revision = a.revision )
      WHERE  import_run_id = a_run_id;

      --Calc the Estimated Residual Pct based on the Supplied Residual and FMV where Pct not supplied
      --If Fair Market value 0 then estimated residual % = 0
      UPDATE LSR_IMPORT_EXT_ASSET
      SET    estimated_residual_pct = Decode( Nvl( fair_market_value, 0 ), 0, 0,
                                                                           estimated_residual / fair_market_value )
      WHERE  estimated_residual_pct IS NULL AND
             import_run_id = a_run_id;

      UPDATE LSR_IMPORT_EXT_ASSET
      SET    estimated_residual = estimated_residual_pct * fair_market_value
      WHERE  estimated_residual IS NULL AND
             import_run_id = a_run_id;

      --Do Calcs for stuff that is calculated
      UPDATE LSR_IMPORT_EXT_ASSET
      SET    unguaranteed_residual = estimated_residual - guaranteed_residual
      WHERE  import_run_id = a_run_id;

      --Insert the Assets
      INSERT INTO LSR_ASSET
                  (lsr_asset_id,
                   ilr_id,
                   revision,
                   description,
                   fair_market_value,
                   carrying_cost,
                   guaranteed_residual_amount,
                   actual_residual_amount,
                   unguaranteed_residual_amount,
                   estimated_residual_pct,
                   estimated_residual_amount,
                   expected_life,
                   economic_life,
                   serial_number,
                   long_description,
                   fair_market_value_comp_curr,
                   carrying_cost_comp_curr,
                   guaranteed_res_amt_comp_curr,
                   actual_residual_amt_comp_curr,
                   unguaranteed_res_amt_comp_curr,
                   estimated_res_amt_comp_curr)
      SELECT ls_asset_seq.NEXTVAL,
             ilr_id,
             revision,
             description,
             Nvl( fair_market_value, 0 ) * in_svc_exchange_rate,
             Nvl( carrying_cost, 0 ) * in_svc_exchange_rate,
             Nvl( guaranteed_residual, 0 ) * in_svc_exchange_rate,
             Nvl( actual_residual, 0 ) * in_svc_exchange_rate,
             Nvl( unguaranteed_residual, 0 ) * in_svc_exchange_rate,
             Nvl( estimated_residual_pct, 0 ),
             Nvl( estimated_residual, 0 ) * in_svc_exchange_rate,
             Nvl( expected_life, 0 ),
             Nvl( economic_life, 0 ),
             serial_number,
             long_description,
             Nvl( fair_market_value, 0 ),
             Nvl( carrying_cost, 0 ),
             Nvl( guaranteed_residual, 0 ),
             Nvl( actual_residual, 0 ),
             Nvl( unguaranteed_residual, 0 ),
             Nvl( estimated_residual, 0)
      FROM   LSR_IMPORT_EXT_ASSET
      WHERE  import_run_id = a_run_id;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_import_external_assets;

  FUNCTION F_validate_external_assets( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Check that supplied revision is valid for the ILR
      UPDATE LSR_IMPORT_EXT_ASSET a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision does not exist. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LSR_ILR_OPTIONS b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision );

      --Check supplied Revision is open
      UPDATE LSR_IMPORT_EXT_ASSET a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision is in a non-editable status. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LSR_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision AND
                          b.approval_status_id IN ( 1 ) );

      --Assign Revision to Assets imported without Revision (Assign Max Initiated Revision for ILR)
      UPDATE LSR_IMPORT_EXT_ASSET a
      SET    revision = ( SELECT Max( revision )
                          FROM   LSR_ILR_APPROVAL b
                          WHERE  a.ilr_id = b.ilr_id AND
                                 b.approval_status_id IN ( 1 ) )
      WHERE  import_run_id = a_run_id AND
             a.revision IS NULL;

      --Check everyone has a revision assigned
      UPDATE LSR_IMPORT_EXT_ASSET a
      SET    error_message = error_message
                             || ' No editable revision exists for the ILR. Please create a new revision.'
      WHERE  import_run_id = a_run_id AND
             a.revision IS NULL;

      --Check that either Estimated Residual Percent or Amount is populated. Can't be both
      UPDATE LSR_IMPORT_EXT_ASSET a
      SET    error_message = error_message
                             || ' Cannot load both Estimated Residual Amount and Estimated Residual Percent. Please populate only one of these fields.'
      WHERE  import_run_id = a_run_id AND
             estimated_residual IS NOT NULL AND
             estimated_residual_pct IS NOT NULL;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_validate_external_assets;

  FUNCTION F_import_ilr_idc( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Remove any IDC set up for what's being imported
      DELETE FROM LSR_ILR_INITIAL_DIRECT_COST
      WHERE  ( ilr_id, revision ) IN
             ( SELECT ilr_id,
                      revision
               FROM   LSR_IMPORT_ILR_INIT_DIRECT b
               WHERE  b.import_run_id = a_run_id );

      --Insert to lsr_ilr_initial_direct_cost
      INSERT INTO LSR_ILR_INITIAL_DIRECT_COST
                  (ilr_initial_direct_cost_id,
                   ilr_id,
                   revision,
                   idc_group_id,
                   amount,
                   description,
                   date_incurred)
      SELECT lsr_ilr_idc_seq.NEXTVAL,
             a.ilr_id,
             a.revision,
             a.idc_group_id,
             a.amount,
             a.description,
             To_date( a.date_incurred, 'mm/dd/yyyy' )
      FROM   LSR_IMPORT_ILR_INIT_DIRECT a
      WHERE  a.import_run_id = a_run_id;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_import_ilr_idc;

  FUNCTION F_validate_ilr_idc( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Check that supplied revision is valid for the ILR
      UPDATE LSR_IMPORT_ILR_INIT_DIRECT a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision does not exist. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LSR_ILR_OPTIONS b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision );

      --Check supplied Revision is open
      UPDATE LSR_IMPORT_ILR_INIT_DIRECT a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision is in a non-editable status. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LSR_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision AND
                          b.approval_status_id IN ( 1 ) );

      --Assign Revision to Assets imported without Revision (Assign Max Initiated Revision for ILR)
      UPDATE LSR_IMPORT_ILR_INIT_DIRECT a
      SET    revision = ( SELECT Max( revision )
                          FROM   LSR_ILR_APPROVAL b
                          WHERE  a.ilr_id = b.ilr_id AND
                                 b.approval_status_id IN ( 1 ) )
      WHERE  import_run_id = a_run_id AND
             a.revision IS NULL;

      --Check everyone has a revision assigned
      UPDATE LSR_IMPORT_ILR_INIT_DIRECT a
      SET    error_message = error_message
                             || ' No editable revision exists for the ILR. Please create a new revision.'
      WHERE  import_run_id = a_run_id AND
             a.revision IS NULL;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_validate_ilr_idc;
END pkg_lessor_import;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (16882, 0, 2018, 2, 0, 0, 0, 'C:\PlasticWKS\PowerPlant\sql\packages', 
    'PKG_LESSOR_IMPORT.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
