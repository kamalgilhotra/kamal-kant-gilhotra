--||============================================================================
--|| Application: PowerPlant
--|| Object Name: tax_accrual_utility_pkg
--|| Description: Contains several utilities used in processing PL/SQL for
--||               provision.
--||============================================================================
--|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
--||============================================================================
--|| Patch           Date       Revised By     Reason for Change
--|| -------------   ---------- -------------- ----------------------------------
--|| ###PATCH(403)   08/04/2010 Blake Andrews  Initial Creation
--||============================================================================

--Main Tax Accrual Oracle Package Declaration
create or replace package pwrplant.tax_accrual_utility_pkg
as
   G_PKG_VERSION varchar(35) := '2018.2.0.0';
   -- Populates a table that stores the description of a particular processing step
   -- along with the amount of time (in 1/100 sec) that has expired since the
   -- debug process was started.
   PROCEDURE p_pl_sql_debug(a_proc_name in varchar2, a_proc_step in varchar2);

   -- 1) Sets the session name that will be used when populating the debug table.  If the
   --       value in this parameter is a space, then the session name remains unchanged
   -- 2) Resets the processing start time to 0
   -- 3) Inserts a row into the debug table indicating the start of a new session.
   PROCEDURE p_reset_session(a_session_name in varchar2);

   -- Clears the debug table of a particular session
   -- If the session name parameter is a space, then the entire table is cleared out
   PROCEDURE p_clear_debug(a_session_name in varchar2);

end tax_accrual_utility_pkg;
/

create or replace public synonym tax_accrual_utility_pkg for pwrplant.tax_accrual_utility_pkg;
grant execute on tax_accrual_utility_pkg to pwrplant_role_dev;

-- ******************************************************************************************************** --
--                                                                                                          --
--                                                                                                          --
--                                                                                                          --
-- PACKAGE BODY                                                                                             --
--                                                                                                          --
--                                                                                                          --
--                                                                                                          --
-- ******************************************************************************************************** --
create or replace package body tax_accrual_utility_pkg
AS

   g_start_time   number(22);
   g_next_id      number(22);
   g_session_name tax_accrual_plsql_debug.session_name%TYPE;

   --**************************************************************************************
   --
   -- p_pl_sql_debug_update
   --
   --**************************************************************************************
   procedure p_pl_sql_debug_update(a_proc_name in varchar2, a_proc_step in varchar2, a_elapsed_time in long)
   is
      id    number(22);
      PRAGMA AUTONOMOUS_TRANSACTION;   -- This makes Oracle treat this as a standalone session, so the commit will not
                                       --
   begin

      g_next_id := g_next_id + 1;

      insert into tax_accrual_plsql_debug
      (  id,
         session_name,
         proc_name,
         proc_step,
         elapsed_time
      )
      values
      (  g_next_id,
         g_session_name,
         a_proc_name,
         a_proc_step,
         a_elapsed_time
      );
      commit;
   end p_pl_sql_debug_update;

   --**************************************************************************************
   --
   -- p_pl_sql_debug
   --
   --**************************************************************************************
   procedure p_pl_sql_debug(a_proc_name in varchar2, a_proc_step in varchar2)
   is
      proc_time number(22);
   begin
      if not tax_accrual_control_pkg.f_get_debug_status then
         return;
      end if;

      -- Calculate the total time since this debugger session began (in 1/100s of a second)
      proc_time := dbms_utility.GET_TIME - g_start_time;

      p_pl_sql_debug_update(a_proc_name, a_proc_step, proc_time);

   end p_pl_sql_debug;

   --**************************************************************************************
   --
   -- p_clear_debug
   --
   --**************************************************************************************
   procedure p_clear_debug(a_session_name in varchar2)
   is
      PRAGMA AUTONOMOUS_TRANSACTION;
   begin

      if a_session_name <> ' ' then
         delete from tax_accrual_plsql_debug
         where session_name = a_session_name;
      else
         delete from tax_accrual_plsql_debug;
      end if;

      commit;

      p_reset_session(' ');

   end p_clear_debug;

   --**************************************************************************************
   --
   -- p_reset_session
   --
   --**************************************************************************************
   procedure p_reset_session(a_session_name in varchar2)
   is
   begin
      if a_session_name <> ' ' then
         g_session_name := a_session_name;
      end if;

      select max(id)
      into g_next_id
      from tax_accrual_plsql_debug;

      if g_next_id is null then
         g_next_id := 0;
      end if;

      g_start_time := dbms_utility.GET_TIME;
      p_pl_sql_debug_update('Debug Header', 'Begin debugging session ' || userenv('SESSIONID') || ' at ' || to_char(sysdate,'mm/dd/yyyy hh24:mi:ss'), 0);

   end p_reset_session;

   --**************************************************************************************
   -- Initialization procedure (Called the first time this package is opened in a session)
   -- 1) Set the start time so that the running time can be calculated
   -- 2) Set the debug session name to the default (<username>:<sessionid>)
   -- 3) Get the max ID from the debug table so that the ID column can be populated.
   -- 4) Insert the debugger header record into the debug table.
   --**************************************************************************************
   begin
      g_start_time := dbms_utility.GET_TIME;

      g_session_name := user || ':' || sys_context('USERENV','SESSIONID');

      select max(id)
      into g_next_id
      from tax_accrual_plsql_debug;

      if g_next_id is null then
         g_next_id := 0;
      end if;

      p_pl_sql_debug_update('Debug Header', 'Begin debugging session ' || userenv('SESSIONID') || ' at ' || to_char(sysdate,'mm/dd/yyyy hh24:mi:ss'), 0);

end tax_accrual_utility_pkg;
--End of Package Body
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (16313, 0, 2018, 2, 0, 0, 0, 'c:\plasticwks\powerplant\sql\packages', 
    'TAX_ACCRUAL_UTILITY_PKG.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
