
create or replace package PKG_TAX_INT_XFER as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_TAX_INT_XFER
   || Description: PowerTax Transfers Interface
   ||============================================================================
   || Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.2.0 10/18/2013 Andrew Scott   Original Version
   || 10.4.2.0 11/20/2013 Andrew Scott   non-FIFO final xfer issue where sum book > trid book
   ||                                    causes allocation ratios to blow up the xfer amount
   || 10.4.2.0 11/25/2013 Andrew Scott   changed decode fix from 11/20 to use sign of tbt.trans_amount
   ||                                    instead of trying to compare sum book to trid book.  also changed
   ||                                    order by to process negatives first, then by abs of book balance
   || 10.4.2.0 12/4/2013  Andrew Scott   Added trace back logic to step through a transfer's history (this
   ||                                    reduces the number of rows and amounts to process).  If A > B > C
   ||                                    transfers of $100--each step--happen in a year, we
   ||                                    only want to see A > C of $100
   || 10.4.2.0 12/5/2013  Andrew Scott   modified to add functions to do the Load Activity
   || 10.4.2.0 12/11/2013 Andrew Scott   QA fixes: modified load activity to use PowerTax company where possible
   || 10.4.2.0 1/8/2014   Andrew Scott   tax_transfer_control backfill with proceeds and to_trid was missing
   ||                                    join to company.
   || 10.4.2.0 01/14/2014 Andrew Scott   maint-35448  MLP changes merging tax_control,
   ||                                    tax_depreciation, tax_depr_adjust all into
   ||                                    tax_depreciation.
   || 10.4.2.0 01/18/2014 Andrew Scott   maint-35448  MLP gui changes. Specifically:
   ||                                    1. all updates to tax_transfer_control
   ||                                       changed to use package_id = -1 (interface only)
   ||                                    2. listagg was not ordering appropriately in the
   ||                                       to record backfill.
   ||                                    3. insert into tax_transfer_control now sets
   ||                                       status_id = 0 to be consistent with pb code.
   || 10.4.2.0 02/19/2014 Andrew Scott   maint-36082  putting Transfer interface into base gui for MLP.
   ||                                    1. put spaces in process name
   ||                                    2. change transfer type, status
   ||                                    to be more descriptive
   ||                                    3. final calcs : pull deferred tax schema ids from
   ||                                    tax_co_def_tax_schema
   ||                                    4. final calcs : pull tax_depr_schema_ids in
   ||                                    from tax book translate mappings.
   ||
   || PACKAGE RELEASED FOR 10.4.2.0
   ||
   || PP Version  Version    Date           Revised By     Reason for Change
   || ----------  ---------  -------------  -------------  -------------------------
   || 10.4.2.1    2.01       05/15/2014     Andrew Scott   maint-37925 : 1. filtering functionality from retire rules
   ||                                                      was not brought over to pl/sql.  fixed using f_filter_records
   ||                                                      2. adding the package version similar to run fast packages.
   ||                                                      This allows us to maintain version history for the package and
   ||                                                      can more easily patch in the future.  3. retire rules look back
   ||                                                      was using -1 * look forward, not the look back field.  fixed this.
   ||                                                      4. tbt group map table altered to have pkey including effective
   ||                                                      vintage.
   || 10.4.3.0    2.02       05/29/2014     Andrew Scott   Alter adds, rets, xfer intfcs to use
   ||                                                      asset id if configured for company.
   || 10.4.3.0    2.03       06/12/2014     Andrew Scott   QA items related to asset id changes
   ||                                                      1. add asset id to audit trails
   ||                                                      2. speed up batching
   || 10.4.3.0    2.04       08/13/2014     Andrew Scott   maint-38887.  turn off auditing.
   || 2015.2.0.0  2.05       09/17/2015     Sarah Byers    PP-44918: Added GL_POSTING_MO_YR to TAX_BOOK_TRANSFERS_GRP and 
   ||                                                      TAX_TRANS_AUDIT_TRAIL_GRP and TRANSFER_MONTH to TAX_TRANSFER_CONTROL
   ||
   || Version     Date       Revised By     Reason for Change
   || --------    ---------- -------------- ----------------------------------------
   || 2015.2.0.0  11/30/2015 Andrew Scott   maint-45240.  Update version to be 2015.2.0.0.
   ||============================================================================
   */

   -- function to get the version of this package.
   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   function F_GET_VERSION return varchar2;

   type CO_IDS is table of number(22, 0) index by binary_integer;

   G_TAX_RECORD_IDS table_list_id_type;
   G_TRANS_GRP_IDS table_list_id_type;

   -- function the return the process id for transfers interface
   function F_GETPROCESS return number;

   -- tax retire rules table holds a set of string values that the
   -- older datawindow logic used to filter a subset of tax records
   -- and link them to another filtered set of transactions.  This
   -- cannot exactly be mimiced in the new PL/SQL logic.  The closest,
   -- hopefully fastest way to mimic this will be to use this function,
   -- called from the iteration function, to build out global table
   -- variables of the tax records that can be used and another variable
   -- of the transactions that can be used.  The batch functions will be
   -- modified to join into these table variables.
   function F_FILTER_RECORDS(
                        A_TAX_YEAR         number,
                        A_VERSION_ID       number,
                        A_ITERATION        number) return varchar2;

   -- function to process a batch of data (each iteration could run many batches of transfer froms)
   function F_RUN_BATCH(A_TAX_YEAR   number,
                        A_START_MO   number,
                        A_END_MO     number,
                        A_VERSION_ID number,
                        A_ITERATION  number,
                        A_BATCH      number) return varchar2;

   -- function to : after the looping, go through some final calculations:
   -- 1.  allocate the "proceeds from" per trans group however the amount was allocated.
   -- 2.  find and update the transfer "tos" for the processed transfer "froms"
   -- 3.  update the calc_deferred_gain, transfer type, and status comment
   -- 4.  update the tax_class_id to use for newly created tax records
   -- 5.  if company has changed, update the deferred_tax_schema_id (pull from
   --     tax_co_def_tax_schema)
   -- 6.  update the tax depr schema id (from and to for now) using the
   --     active tax book translate's tax depr schema id of the "from" records
   function F_FINAL_CALCS(A_TAX_YEAR   number,
                          A_VERSION_ID number) return varchar2;

   -- function to start the log, process the transfers, end the log
   function F_POWERTAX_XFERS(A_TAX_YEAR   number,
                             A_START_MO   number,
                             A_END_MO     number,
                             A_VERSION_ID number) return varchar2;


   -- Functions to load the activity through the transfer history in a given year.
   --    This function populates the staging table with the original transfer from plant
   function F_TRACEBACK_LOAD(A_TAX_YEAR   number,
                             A_START_MO   number,
                             A_END_MO     number) return varchar2;

   -- This function loops through all the records and tries to trace whatever can be.
   function F_TRACEBACK_PROCESS(A_TAX_YEAR   number,
                                A_DEBUG_FLAG boolean) return varchar2;

   -- This function populates tax book transfers from the final traced back data, plus
   --    joins to the plant tables and uses a temporary table.
   function F_POPULATE_TAX_BOOK_XFERS(A_TAX_YEAR   number,
                                      A_START_MO   number,
                                      A_END_MO     number) return varchar2;

   -- This function calls the traceback functions, then calls the process function to
   -- load the activity.
   function F_POWERTAX_XFER_LOAD(A_TAX_YEAR   number,
                                 A_START_MO   number,
                                 A_END_MO     number,
                                 A_DEBUG_FLAG_YN number) return varchar2;

end PKG_TAX_INT_XFER;
/

create or replace package body PKG_TAX_INT_XFER as

   --****************************************************************************************************
   --               VARIABLES
   --****************************************************************************************************
   L_PROCESS_ID number;

   --****************************************************************************************************
   --               Start Body
   --****************************************************************************************************

   -- function to get the version of this package.
   function F_GET_VERSION return varchar2 is
   begin
      return '2015.2.0.0';
   end F_GET_VERSION;

   --****************************************************************************************************
   --               PROCEDURES
   --****************************************************************************************************

   --procedure grabs the transfers interface process id for the online logs
   procedure P_SETPROCESS is
   begin
      select PROCESS_ID
        into L_PROCESS_ID
        from PP_PROCESSES
       where UPPER(DESCRIPTION) = 'POWERTAX - TRANSFERS';
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         L_PROCESS_ID := -1;
   end P_SETPROCESS;

   --****************************************************************************************************
   --                FUNCTIONS
   --****************************************************************************************************

   -- function the return the process id for transfers interface
   function F_GETPROCESS return number is
   begin
      return L_PROCESS_ID;
   end F_GETPROCESS;

   -- tax retire rules table holds a set of string values that the
   -- older datawindow logic used to filter a subset of tax records
   -- and link them to another filtered set of transactions.  This
   -- cannot exactly be mimiced in the new PL/SQL logic.  The closest,
   -- hopefully fastest way to mimic this will be to use this function,
   -- called from the iteration function, to build out global table
   -- variables of the tax records that can be used and another variable
   -- of the transactions that can be used.  The batch functions will be
   -- modified to join into these table variables.
   function F_FILTER_RECORDS(
                        A_TAX_YEAR         number,
                        A_VERSION_ID       number,
                        A_ITERATION        number) return varchar2 is
      L_MSG       varchar2(2000);
      SQLS        varchar2(2000);

      L_TAX_RECORD_IDS table_list_id_type;
      L_TRANS_GRP_IDS table_list_id_type;

      type TRR_REC_TYPE is record(
         COMPANY_ID       TAX_RETIRE_RULES.COMPANY_ID%type,
         FFORMAT_STRING   TAX_RETIRE_RULES.FFORMAT_STRING%type,
         GFORMAT_STRING   TAX_RETIRE_RULES.GFORMAT_STRING%type);
      type TRR_TBL_TYPE is table of TRR_REC_TYPE;
      TRR_TBL TRR_TBL_TYPE;

   begin

      ----initialize/reset the global table variables
      G_TAX_RECORD_IDS     := table_list_id_type(-1);
      G_TAX_RECORD_IDS.delete;
      G_TRANS_GRP_IDS      := table_list_id_type(-1);
      G_TRANS_GRP_IDS.delete;

      ----initialize/reset the local table variables used in the loops
      L_TAX_RECORD_IDS     := table_list_id_type(-1);
      L_TAX_RECORD_IDS.delete;
      L_TRANS_GRP_IDS      := table_list_id_type(-1);
      L_TRANS_GRP_IDS.delete;

      L_MSG := 'Gathering retire rule filters...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      begin
         select TRR.COMPANY_ID,
                nvl(trim(TRR.FFORMAT_STRING),'1=1') FFORMAT_STRING,
                nvl(trim(TRR.GFORMAT_STRING),'1=1') GFORMAT_STRING bulk collect
           into TRR_TBL
           from TAX_RETIRE_RULES TRR
          where TRR.ITERATION = A_ITERATION
          group by TRR.COMPANY_ID,
                TRR.FFORMAT_STRING,
                TRR.GFORMAT_STRING;
      ----this function is entered from 1 to max(iteration).  put an exception catch here
      ----in case the companies selected have fewer iterations than the max.
      exception
         when NO_DATA_FOUND then
            L_MSG := 'No data found.  Continuing without error...';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            return 'OK';
         when others then
            L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            return L_MSG;
      end;

      L_MSG := 'Gathering tax records to filter...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      ----loop through the selected retire rules and build out the tax records available to use.
      FOR indx IN 1 .. TRR_TBL.count
      LOOP
         begin
            ----populate the tax record ids for this filter and company into the
            ----local id table
            SQLS :=
               'SELECT TAX_RECORD_CONTROL.TAX_RECORD_ID '||
               'FROM TAX_RECORD_CONTROL, VINTAGE '||
               'WHERE TAX_RECORD_CONTROL.VINTAGE_ID = VINTAGE.VINTAGE_ID '||
               'AND TAX_RECORD_CONTROL.VERSION_ID = '||A_VERSION_ID||' '||
               'AND TAX_RECORD_CONTROL.COMPANY_ID = '||TRR_TBL(indx).COMPANY_ID||' '||
               'AND '||TRR_TBL(indx).FFORMAT_STRING;

            EXECUTE IMMEDIATE SQLS
            BULK COLLECT INTO L_TAX_RECORD_IDS;

         ----in case no records come back, put an exception catch here and continue
         exception
            when NO_DATA_FOUND then
               L_MSG := 'No tax records found.  Continuing to next iteration...';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               continue;
            when others then
               L_MSG := 'Error encountered on filtering tax records in retire rules loop '||
                  indx||', iteration '||A_ITERATION||', company id '||TRR_TBL(indx).COMPANY_ID||','||
                  ' attempted SQLS :"'||SQLS||'".';
               L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               return L_MSG;
         end;

         L_MSG    := L_TAX_RECORD_IDS.count || ' tax record ids gathered on retire rules loop '||
            indx||', iteration '||A_ITERATION||', company id '||TRR_TBL(indx).COMPANY_ID||'.';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         ----append the gathered local tax record ids to the global tax record ids
         G_TAX_RECORD_IDS := G_TAX_RECORD_IDS
                              MULTISET UNION
                             L_TAX_RECORD_IDS;

         L_MSG    := G_TAX_RECORD_IDS.count || ' : running total of tax records gathered on retire rules loop '||
            indx||', iteration '||A_ITERATION||', company id '||TRR_TBL(indx).COMPANY_ID||'.';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      END LOOP;


      L_MSG := 'Gathering transactions to filter...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      ----loop through the selected retire rules and build out the transactions available to use.
      FOR indx IN 1 .. TRR_TBL.count
      LOOP
         begin
            ----populate the transaction grp ids for this filter and company into the
            ----local id table
            SQLS :=
               'SELECT TAX_BOOK_TRANSFERS_GRP.TRANS_GRP_ID '||
               'FROM TAX_BOOK_TRANSFERS_GRP '||
               'WHERE TAX_BOOK_TRANSFERS_GRP.TAX_YEAR = '||A_TAX_YEAR||' '||
               'AND TAX_BOOK_TRANSFERS_GRP.COMPANY_ID = '||TRR_TBL(indx).COMPANY_ID||' '||
               'AND '||TRR_TBL(indx).GFORMAT_STRING;

            EXECUTE IMMEDIATE SQLS
            BULK COLLECT INTO L_TRANS_GRP_IDS;

         ----in case no records come back, put an exception catch here and continue
         exception
            when NO_DATA_FOUND then
               L_MSG := 'No transaction groupings found.  Continuing to next iteration...';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               continue;
            when others then
               L_MSG := 'Error encountered on filtering transactions in retire rules loop '||
                  indx||', iteration '||A_ITERATION||', company id '||TRR_TBL(indx).COMPANY_ID||','||
                  ' attempted SQLS :"'||SQLS||'".';
               L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               return L_MSG;
         end;

         L_MSG    := L_TRANS_GRP_IDS.count || ' trans group ids gathered on retire rules loop '||
            indx||', iteration '||A_ITERATION||', company id '||TRR_TBL(indx).COMPANY_ID||'.';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         ----append the gathered local trans grp ids to the global trans grps ids
         G_TRANS_GRP_IDS := G_TRANS_GRP_IDS
                              MULTISET UNION
                             L_TRANS_GRP_IDS;

         L_MSG    := G_TRANS_GRP_IDS.count || ' : running total of tax records gathered on retire rules loop '||
            indx||', iteration '||A_ITERATION||', company id '||TRR_TBL(indx).COMPANY_ID||'.';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      END LOOP;

      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;

   end;

   -- function to process a batch of data (each iteration could run many batches of transfer froms)
   function F_RUN_BATCH(A_TAX_YEAR   number,
                        A_START_MO   number,
                        A_END_MO     number,
                        A_VERSION_ID number,
                        A_ITERATION  number,
                        A_BATCH      number) return varchar2 is
      L_MSG        varchar2(2000);
      NUM_ROWS     number;
      ANALYZE_CODE number(22, 0);
      type TTC_REC_TYPE is record(
         FROM_TRID   TAX_TRANSFER_CONTROL.FROM_TRID%type,
         TAX_YEAR    TAX_TRANSFER_CONTROL.TAX_YEAR%type,
         COMPANY_ID  TAX_TRANSFER_CONTROL.COMPANY_ID%type,
         ASSET_ID    TAX_TRANSFER_CONTROL.ASSET_ID%type,
         BOOK_AMOUNT TAX_TRANSFER_CONTROL.BOOK_AMOUNT%type);
      type TTC_TBL_TYPE is table of TTC_REC_TYPE;
      TTC_TBL TTC_TBL_TYPE;

   begin

      L_MSG := 'Regathering stats for tax_book_transfers_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      ANALYZE_CODE := ANALYZE_TABLE('tax_book_transfers_grp');

      L_MSG := 'Regathering stats for tax_transfer_control...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      ANALYZE_CODE := ANALYZE_TABLE('tax_transfer_control');

      L_MSG := 'Inserting into tax_trans_audit_trail_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      insert into TAX_TRANS_AUDIT_TRAIL_GRP
         (TRANS_GRP_ID, TAX_YEAR, FROM_TRID, TAX_ACTIVITY_CODE_ID, IN_SERVICE_YEAR, START_MONTH,
          END_MONTH, TBT_GROUP_ID, START_EFF_BOOK_VINTAGE, END_EFF_BOOK_VINTAGE, BOOK_BALANCE,
          HOLD_SUM_BOOK_BALANCE, ALLOC_AMOUNT, TRANS_AMOUNT, TRANS_SALES_AMOUNT, VINTAGE_YEAR,
          TRANSFER_STATUS, FROM_TRID_VINTAGE_YEAR, ALLOC_SALES_AMOUNT, CREATE_TO_TRID, BATCH,
          ITERATION, ASSET_ID, TO_TRID, COMPANY_ID_TO, TBT_GROUP_ID_TO, ASSET_ID_TO, GL_POSTING_MO_YR)
         select TRANS_GRP_ID,
                TAX_YEAR,
                TAX_RECORD_ID          FROM_TRID,
                TAX_ACTIVITY_CODE_ID,
                IN_SERVICE_YEAR,
                START_MONTH,
                END_MONTH,
                TBT_GROUP_ID,
                START_EFF_BOOK_VINTAGE,
                END_EFF_BOOK_VINTAGE,
                BOOK_BALANCE,
                BALANCE,
                NEW_XFER               ALLOC_AMOUNT,
                TRANS_AMOUNT,
                TRANS_SALES_AMOUNT,
                BOOK_VINTAGE_YEAR,
                TRANSFER_STATUS,
                VINTAGE_YEAR,
                0                      ALLOC_SALES_AMOUNT,
                1                      CREATE_TO_TRID,
                BATCH,
                ITERATION,
                ASSET_ID,
                -1                     TO_TRID,
                COMPANY_ID_TO,
                TBT_GROUP_ID_TO,
                ASSET_ID_TO,
                GL_POSTING_MO_YR
           from (select COMPANY_ID,
                        TAX_ACTIVITY_CODE_ID,
                        IN_SERVICE_YEAR,
                        TAX_YEAR,
                        START_MONTH,
                        END_MONTH,
                        TRANS_GRP_ID,
                        START_EFF_BOOK_VINTAGE,
                        END_EFF_BOOK_VINTAGE,
                        BOOK_BALANCE,
                        BALANCE,
                        DECODE(SIGN(TRANS_AMOUNT),
                               -1,
                               NEW_XFER,
                               DECODE(SIGN(BALANCE - NEW_XFER), -1, BALANCE, NEW_XFER)) NEW_XFER,
                        TRANS_AMOUNT,
                        TRANS_SALES_AMOUNT,
                        BOOK_VINTAGE_YEAR,
                        TAX_RECORD_ID,
                        DECODE(SIGN(TRANS_AMOUNT),
                               -1,
                               ITERATION,
                               DECODE(SIGN(BALANCE - NEW_XFER), -1, -98, ITERATION)) TRANSFER_STATUS,
                        VINTAGE_YEAR,
                        (A_BATCH) BATCH,
                        ITERATION,
                        TBT_GROUP_ID,
                        ASSET_ID,
                        ALLOC_SUBTOTAL,
                        FIFO,
                        COMPANY_ID_TO,
                        TBT_GROUP_ID_TO,
                        ASSET_ID_TO,
                        max(TRID_RANK) OVER(partition by TRANS_GRP_ID) TRANS_RANK,
                        GL_POSTING_MO_YR
                   from (select distinct TBT.COMPANY_ID COMPANY_ID,
                                         TBT.TAX_ACTIVITY_CODE_ID TAX_ACTIVITY_CODE_ID,
                                         TBT.IN_SERVICE_YEAR IN_SERVICE_YEAR,
                                         TBT.TAX_YEAR TAX_YEAR,
                                         (A_START_MO) START_MONTH,
                                         (A_END_MO) END_MONTH,
                                         TBT.TRANS_GRP_ID TRANS_GRP_ID,
                                         TBX.START_EFF_BOOK_VINTAGE,
                                         TBX.END_EFF_BOOK_VINTAGE,
                                         TD.BOOK_BALANCE,
                                         (NVL(TD.BOOK_BALANCE, 0) + NVL(XFER_TAKEN_FROM.AMOUNT, 0)) BALANCE,
                                         DECODE(( sum( NVL(TD.BOOK_BALANCE, 0) + NVL(XFER_TAKEN_FROM.AMOUNT, 0)  )
                                                   OVER(partition by TRANS_GRP_ID) ) ,
                                                  0,
                                                  0,
                                                  DECODE(TRR.FIFO,
                                                       0,
                                                       decode(
                                                          sign(TBT.AMOUNT),
                                                          -1,
                                                          DECODE(DENSE_RANK()
                                                                 OVER(partition by TBT.TRANS_GRP_ID
                                                                   order by
                                                                    decode(SIGN(TD.BOOK_BALANCE),-1,1,2) ASC,
                                                                    abs(TD.BOOK_BALANCE) desc,
                                                                    TRC.TAX_RECORD_ID),
                                                                 1,
                                                                 1,
                                                                 0) * TBT.AMOUNT_UNPROCESSED,
                                                          ( NVL(TD.BOOK_BALANCE, 0) + NVL(XFER_TAKEN_FROM.AMOUNT, 0) ) /
                                                            (sum(NVL(TD.BOOK_BALANCE, 0) + NVL(XFER_TAKEN_FROM.AMOUNT, 0) )
                                                              OVER(partition by TRANS_GRP_ID)) * TBT.AMOUNT_UNPROCESSED
                                                        ) ,
                                                        DECODE(DENSE_RANK()
                                                               OVER(partition by TBT.TRANS_GRP_ID
                                                                    order by
                                                                    V.YEAR,
                                                                    ABS(TD.BOOK_BALANCE) desc,
                                                                    TRC.TAX_RECORD_ID),
                                                               1,
                                                               1,
                                                               0) * TBT.AMOUNT_UNPROCESSED   ) ) NEW_XFER,
                                         TBT.AMOUNT TRANS_AMOUNT,
                                         TBT.BOOK_SALE_AMOUNT TRANS_SALES_AMOUNT,
                                         TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR, 'YYYY')) BOOK_VINTAGE_YEAR,
                                         TRC.TAX_RECORD_ID TAX_RECORD_ID,
                                         A_ITERATION ITERATION,
                                         TBT.TBT_GROUP_ID TBT_GROUP_ID,
                                         TBT.ASSET_ID ASSET_ID,
                                         V.YEAR VINTAGE_YEAR,
                                         TBT.AMOUNT_UNPROCESSED,
                                         sum(NVL(TD.BOOK_BALANCE, 0) + NVL(XFER_TAKEN_FROM.AMOUNT, 0)) OVER(partition by TRANS_GRP_ID) ALLOC_SUBTOTAL,
                                         TRR.FIFO,
                                         TBT.COMPANY_ID_TO,
                                         TBT.TBT_GROUP_ID_TO,
                                         TBT.ASSET_ID_TO,
                                         RANK() OVER(partition by TRC.TAX_RECORD_ID order by DECODE(SIGN(TBT.AMOUNT), -1, 1, 2) asc, TBT.AMOUNT desc) TRID_RANK,
                                         TBT.GL_POSTING_MO_YR
                           from TAX_BOOK_TRANSFERS_GRP TBT,
                                TAX_BOOK_TRANSLATE_GROUP_MAP TBX,
                                COMPANY C,
                                (select COMPANY_ID,
                                        ITERATION,
                                        VINTAGE_FORWARD,
                                        VINTAGE_BACK,
                                        FIFO,
                                        MIN_VINTAGE
                                   from TAX_RETIRE_RULES TRR
                                 union
                                 select distinct COMPANY_ID,
                                                 1 ITERATION,
                                                 0 VINTAGE_FORWARD,
                                                 0 VINTAGE_BACK,
                                                 0 FIFO,
                                                 1969 MIN_VINTAGE
                                   from (select distinct COMPANY_ID COMPANY_ID
                                           from TAX_BOOK_TRANSFERS_GRP
                                         minus
                                         select COMPANY_ID
                                           from TAX_RETIRE_RULES)) TRR,
                                TAX_DEPRECIATION TD,
                                TAX_RECORD_CONTROL TRC,
                                VINTAGE V,
                                (select FROM_TRID TAX_RECORD_ID,
                                        TAX_YEAR,
                                        (sum(BOOK_AMOUNT) * -1) AMOUNT
                                   from TAX_TRANSFER_CONTROL TTC
                                  where TTC.VERSION_ID = A_VERSION_ID
                                    and TTC.TAX_YEAR = A_TAX_YEAR
                                    and TTC.PACKAGE_ID = -1
                                  group by FROM_TRID, TAX_YEAR) XFER_TAKEN_FROM
                          where TBT.TBT_GROUP_ID = TBX.TBT_GROUP_ID
                            and TBT.TAX_YEAR = A_TAX_YEAR
                            and TBT.COMPANY_ID = C.COMPANY_ID
                            and TBT.COMPANY_ID = TRR.COMPANY_ID
                            and TBT.AMOUNT <> 0
                            and TBT.TAX_ACTIVITY_CODE_ID in (6)
                            and TBT.TRANSFER_STATUS in (-99, -98)
                            and TRR.ITERATION = A_ITERATION
                            and TD.TAX_RECORD_ID = TRC.TAX_RECORD_ID
                            and TD.TAX_BOOK_ID = 10
                            and TRC.COMPANY_ID = C.COMPANY_ID
                            and TRC.VINTAGE_ID = V.VINTAGE_ID
                            and TD.TAX_RECORD_ID = XFER_TAKEN_FROM.TAX_RECORD_ID(+)
                            and TD.TAX_YEAR = XFER_TAKEN_FROM.TAX_YEAR(+)
                            and TRC.COMPANY_ID = TBT.COMPANY_ID
                            and TRC.ASSET_ID = TBT.ASSET_ID
                            and TRC.VERSION_ID = A_VERSION_ID
                            and TBX.TAX_CLASS_ID = TRC.TAX_CLASS_ID
                            and TBT.TAX_YEAR = TD.TAX_YEAR
                            and TRC.TAX_RECORD_ID in ( select * from table(G_TAX_RECORD_IDS)) --filter records from retire rules and filter function
                            and TBT.TRANS_GRP_ID in ( select * from table(G_TRANS_GRP_IDS)) --filter transactions from retire rules and filter function
                            and TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR,'YYYY')) >= TBX.START_EFF_BOOK_VINTAGE
                            and TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR,'YYYY')) <= TBX.END_EFF_BOOK_VINTAGE
                            and V.YEAR <= DECODE(SIGN(TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR,
                                                                        'YYYY')) - MIN_VINTAGE),
                                                 -1,
                                                 MIN_VINTAGE,
                                                 TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR, 'YYYY'))) +
                                TRR.VINTAGE_FORWARD
                            and V.YEAR >= DECODE(SIGN(TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR,
                                                                        'YYYY')) - MIN_VINTAGE),
                                                 -1,
                                                 MIN_VINTAGE,
                                                 TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR, 'YYYY'))) -
                                TRR.VINTAGE_BACK
                            and 1 =
                                DECODE(SIGN(TBT.AMOUNT),
                                       -1,
                                       1,
                                       SIGN(NVL(TD.BOOK_BALANCE, 0) + NVL(XFER_TAKEN_FROM.AMOUNT, 0)))
                            )
                  where DECODE(SIGN(TRANS_AMOUNT),
                               -1,
                               NEW_XFER,
                               DECODE(SIGN(BALANCE - NEW_XFER), -1, BALANCE, NEW_XFER)) <> 0)
          where TRANS_RANK = 1;

      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows inserted into tax_trans_audit_trail_grp.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      if NUM_ROWS = 0 then
         L_MSG := 'Exiting function on 0 rows inserts';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return 'ZERO ROWS';
      end if;

      L_MSG := 'Updating tax_book_transfers_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      update TAX_BOOK_TRANSFERS_GRP TBT
         set TRANSFER_STATUS = NVL((select max(TRANSFER_STATUS)
                                           from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
                                          where TTAT.TRANS_GRP_ID = TBT.TRANS_GRP_ID
                                            and TTAT.TAX_YEAR = TBT.TAX_YEAR
                                            and TBT.TAX_YEAR = A_TAX_YEAR),
                                         -99),
             AMOUNT_UNPROCESSED = AMOUNT_UNPROCESSED -
                                        NVL((select sum(ALLOC_AMOUNT)
                                              from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
                                             where TTAT.TRANS_GRP_ID = TBT.TRANS_GRP_ID
                                               and TTAT.TAX_YEAR = TBT.TAX_YEAR
                                               and TBT.TAX_YEAR = A_TAX_YEAR
                                               and TTAT.ITERATION = A_ITERATION
                                               and TTAT.BATCH = A_BATCH),
                                            0)
       where TAX_YEAR = A_TAX_YEAR
         and TRANSFER_STATUS in (-99, -98)
         and exists (select 1
                from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
               where TTAT.TRANS_GRP_ID = TBT.TRANS_GRP_ID
                 and TTAT.TAX_YEAR = TBT.TAX_YEAR
                 and TTAT.ITERATION = A_ITERATION
                 and TBT.TAX_YEAR = A_TAX_YEAR);
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_book_transfers_grp.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating tax_transfer_control...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select TTAT.FROM_TRID, TTAT.TAX_YEAR, TTAT.COMPANY_ID_TO, NVL(TTAT.ASSET_ID_TO,0), sum(TTAT.ALLOC_AMOUNT) bulk collect
        into TTC_TBL
        from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
       where TTAT.TAX_YEAR = A_TAX_YEAR
         and TTAT.ITERATION = A_ITERATION
         and TTAT.BATCH = A_BATCH
       group by TTAT.FROM_TRID, TTAT.TAX_YEAR, TTAT.COMPANY_ID_TO, NVL(TTAT.ASSET_ID_TO,0);

      forall I in TTC_TBL.FIRST .. TTC_TBL.LAST
         update TAX_TRANSFER_CONTROL
            set BOOK_AMOUNT = BOOK_AMOUNT + TTC_TBL(I).BOOK_AMOUNT
          where FROM_TRID = TTC_TBL(I).FROM_TRID
            and TAX_YEAR = TTC_TBL(I).TAX_YEAR
            and VERSION_ID = A_VERSION_ID
            and COMPANY_ID = TTC_TBL(I).COMPANY_ID
            and NVL(ASSET_ID,0) = TTC_TBL(I).ASSET_ID
            and PACKAGE_ID = -1 ;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Inserting into tax_transfer_control...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      insert into TAX_TRANSFER_CONTROL TTC
         (TAX_TRANSFER_ID, FROM_TRID, TAX_YEAR, BOOK_AMOUNT, PACKAGE_ID, VERSION_ID, TO_TRID,
          CREATE_TO_TRID, COMPANY_ID, ASSET_ID, PROCEEDS, STATUS_ID, TRANSFER_MONTH)
         select TAX_TRANSFER_SEQ.NEXTVAL,
                FROM_TRID,
                TAX_YEAR,
                BOOK_AMOUNT,
                -1,
                A_VERSION_ID,
                -1,
                1,
                COMPANY_ID_TO,
                ASSET_ID_TO,
                0,
                0, 
                TRANSFER_MONTH
           from (select TTAT.FROM_TRID,
                        TTAT.TAX_YEAR,
                        TTAT.COMPANY_ID_TO,
                        TTAT.ASSET_ID_TO,
                        sum(TTAT.ALLOC_AMOUNT) BOOK_AMOUNT,
                        DECODE(PARTIAL_XFER.OPTION_VALUE, 'YES', TTAT.GL_POSTING_MO_YR, NULL) TRANSFER_MONTH
                   from TAX_TRANS_AUDIT_TRAIL_GRP TTAT,
                        (
                            select distinct company_id, option_value from (
                                 /** Company System Option is in Use **/
                                 select COMPANY_SETUP.COMPANY_ID,
                                            UPPER(trim(NVL(PPBASE_SYSTEM_OPTIONS_COMPANY.OPTION_VALUE,
                                                                PPBASE_SYSTEM_OPTIONS.PP_DEFAULT_VALUE))) OPTION_VALUE
                                    from COMPANY_SETUP,
                                            PPBASE_SYSTEM_OPTIONS,
                                            PPBASE_SYSTEM_OPTIONS_MODULE,
                                            PPBASE_SYSTEM_OPTIONS_COMPANY
                                  where UPPER(trim(PPBASE_SYSTEM_OPTIONS_MODULE.MODULE)) = 'POWERTAX'
                                     and PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                                          PPBASE_SYSTEM_OPTIONS_MODULE.SYSTEM_OPTION_ID
                                     and UPPER(trim(PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID)) =
                                          UPPER(trim('Partial Year Depreciation Transfers'))
                                     and PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                                          PPBASE_SYSTEM_OPTIONS_COMPANY.SYSTEM_OPTION_ID
                                     and PPBASE_SYSTEM_OPTIONS_COMPANY.COMPANY_ID = COMPANY_SETUP.COMPANY_ID
                                 union all
                                 /** Main System Option used for all other companies **/
                                 select COMPANY_SETUP.COMPANY_ID,
                                            UPPER(trim(NVL(PPBASE_SYSTEM_OPTIONS.OPTION_VALUE,
                                                                PPBASE_SYSTEM_OPTIONS.PP_DEFAULT_VALUE)))
                                    from COMPANY_SETUP, PPBASE_SYSTEM_OPTIONS, PPBASE_SYSTEM_OPTIONS_MODULE
                                  where UPPER(trim(PPBASE_SYSTEM_OPTIONS_MODULE.MODULE)) = 'POWERTAX'
                                     and PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                                          PPBASE_SYSTEM_OPTIONS_MODULE.SYSTEM_OPTION_ID
                                     and UPPER(trim(PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID)) =
                                          UPPER(trim('Partial Year Depreciation Transfers'))
                                     and not exists
                                  (select 1
                                              from PPBASE_SYSTEM_OPTIONS_COMPANY
                                             where PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                                                     PPBASE_SYSTEM_OPTIONS_COMPANY.SYSTEM_OPTION_ID
                                                and PPBASE_SYSTEM_OPTIONS_COMPANY.COMPANY_ID = COMPANY_SETUP.COMPANY_ID))
                        ) PARTIAL_XFER
                  where TTAT.TAX_YEAR = A_TAX_YEAR
                    and TTAT.ITERATION = A_ITERATION
                    and TTAT.BATCH = A_BATCH
                    and TTAT.COMPANY_ID_TO = PARTIAL_XFER.COMPANY_ID
                    and not exists (select 1
                           from TAX_TRANSFER_CONTROL TTC
                          where TTC.FROM_TRID = TTAT.FROM_TRID
                            and TTC.TAX_YEAR = TTAT.TAX_YEAR
                            and TTC.VERSION_ID = A_VERSION_ID
                            and TTC.COMPANY_ID = TTAT.COMPANY_ID_TO
                            and NVL(TTC.ASSET_ID,0) = NVL(TTAT.ASSET_ID_TO,0)
                            and TTC.PACKAGE_ID = -1 )
                  group by TTAT.FROM_TRID, TTAT.TAX_YEAR, COMPANY_ID_TO, TTAT.ASSET_ID_TO, DECODE(PARTIAL_XFER.OPTION_VALUE, 'YES', TTAT.GL_POSTING_MO_YR, NULL));
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows inserted into tax_transfer_control.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
   end F_RUN_BATCH;

   -- function to process the transfer froms
   function F_RUN_ITERATION(A_TAX_YEAR   number,
                            A_START_MO   number,
                            A_END_MO     number,
                            A_VERSION_ID number) return varchar2 is
      L_MSG     varchar2(2000);
      MAX_ITER  number;
      BATCH_NO  number;
      ZERO_ROWS boolean;
   begin

      L_MSG := 'Retrieving max number of iterations to run...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      select max(NVL(ITERATION, 0)) into MAX_ITER from TAX_RETIRE_RULES TRR;

      if MAX_ITER < 1 or MAX_ITER is null then
         L_MSG := 'Error on selecting the max iteration from tax retire rules.';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
      end if;

      for I in 1 .. MAX_ITER
      loop
         L_MSG := 'Entering iteration loop ' || I || ' of ' || MAX_ITER || '...';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         L_MSG := ' ';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         L_MSG := 'Entering filter function. Iteration ' || I || '.';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         L_MSG := F_FILTER_RECORDS(A_TAX_YEAR, A_VERSION_ID, I);
         if L_MSG <> 'OK' then
            PKG_PP_LOG.P_END_LOG();
            return L_MSG;
         end if;

         BATCH_NO  := 1;
         ZERO_ROWS := false;

         while ZERO_ROWS = false
         loop

            if BATCH_NO > 100000 then
               L_MSG := 'Infinite Loop Error Encountered! Exiting!';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               return L_MSG;
            end if;

            L_MSG := ' ';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            L_MSG := 'Entering batch function. Iteration ' || I || ', Batch ' || BATCH_NO || '...';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            L_MSG := F_RUN_BATCH(A_TAX_YEAR, A_START_MO, A_END_MO, A_VERSION_ID, I, BATCH_NO);
            if L_MSG = 'ZERO ROWS' then
               ZERO_ROWS := true;
            elsif L_MSG <> 'OK' then
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               return L_MSG;
            end if;

            BATCH_NO := BATCH_NO + 1;

         end loop;
      end loop;

      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
   end F_RUN_ITERATION;

   -- function to : after the looping, go through some final calculations:
   -- 1.  allocate the "proceeds from" per trans group however the amount was allocated.
   -- 2.  find and update the transfer "tos" for the processed transfer "froms"
   -- 3.  update the calc_deferred_gain, transfer type, and status comment
   -- 4.  update the tax_class_id to use for newly created tax records
   -- 5.  if company has changed, update the deferred_tax_schema_id (pull from
   --     tax_co_def_tax_schema)
   -- 6.  update the tax depr schema id (from and to for now) using the
   --     active tax book translate's tax depr schema id of the "from" records
   function F_FINAL_CALCS(A_TAX_YEAR   number,
                          A_VERSION_ID number) return varchar2 is
      L_MSG     varchar2(2000);
      NUM_ROWS  number;

      type AUDIT_SELECT_REC_TYPE is record(
         TRANS_GRP_ID       TAX_TRANS_AUDIT_TRAIL_GRP.TRANS_GRP_ID%type,
         TAX_YEAR           TAX_TRANS_AUDIT_TRAIL_GRP.TAX_YEAR%type,
         FROM_TRID          TAX_TRANS_AUDIT_TRAIL_GRP.FROM_TRID%type,
         BATCH              TAX_TRANS_AUDIT_TRAIL_GRP.BATCH%type,
         ITERATION          TAX_TRANS_AUDIT_TRAIL_GRP.ITERATION%type,
         ALLOC_SALES_AMOUNT TAX_TRANS_AUDIT_TRAIL_GRP.ALLOC_SALES_AMOUNT%type);
      type AUDIT_SELECT_TBL_TYPE is table of AUDIT_SELECT_REC_TYPE;
      AUDIT_SELECT_TBL AUDIT_SELECT_TBL_TYPE;

      type TTAT_GRP_REC_TYPE is record(
         TRANS_GRP_ID TAX_TRANS_AUDIT_TRAIL_GRP.TRANS_GRP_ID%type,
         TAX_YEAR     TAX_TRANS_AUDIT_TRAIL_GRP.TAX_YEAR%type,
         TO_TRID      TAX_TRANS_AUDIT_TRAIL_GRP.TO_TRID%type,
         FROM_TRID    TAX_TRANS_AUDIT_TRAIL_GRP.FROM_TRID%type);
      type TTAT_GRP_TBL_TYPE is table of TTAT_GRP_REC_TYPE;
      TTAT_GRP_TBL TTAT_GRP_TBL_TYPE;

      type TTC_REC_TYPE is record(
         TAX_YEAR  TAX_TRANSFER_CONTROL.TAX_YEAR%type,
         TO_TRID   TAX_TRANSFER_CONTROL.TO_TRID%type,
         FROM_TRID TAX_TRANSFER_CONTROL.FROM_TRID%type,
         COMPANY_ID  TAX_TRANSFER_CONTROL.COMPANY_ID%type,
         ASSET_ID    TAX_TRANSFER_CONTROL.ASSET_ID%type,
         PROCEEDS  TAX_TRANSFER_CONTROL.PROCEEDS%type);
      type TTC_TBL_TYPE is table of TTC_REC_TYPE;
      TTC_TBL      TTC_TBL_TYPE;

      ANALYZE_CODE number(22, 0);

      type TTC_TCLASS_REC_TYPE is record(
         TAX_TRANSFER_ID TAX_TRANSFER_CONTROL.TAX_TRANSFER_ID%type,
         TAX_CLASS_ID    TAX_TRANSFER_CONTROL.TAX_CLASS_ID%type);
      type TTC_TCLASS_TBL_TYPE is table of TTC_TCLASS_REC_TYPE;
      TTC_TCLASS_TBL TTC_TCLASS_TBL_TYPE;

      type TTC_DEFTAXSCH_REC_TYPE is record(
         TAX_TRANSFER_ID TAX_TRANSFER_CONTROL.TAX_TRANSFER_ID%type,
         DEFERRED_TAX_SCHEMA_ID    TAX_TRANSFER_CONTROL.DEFERRED_TAX_SCHEMA_ID%type);
      type TTC_DEFTAXSCH_TBL_TYPE is table of TTC_DEFTAXSCH_REC_TYPE;
      TTC_DEFTAXSCH_TBL TTC_DEFTAXSCH_TBL_TYPE;

      type TTC_DEPRSCHEMA_REC_TYPE is record(
         TAX_TRANSFER_ID TAX_TRANSFER_CONTROL.TAX_TRANSFER_ID%type,
         TAX_DEPR_SCHEMA_ID    TAX_TRANSFER_CONTROL.TAX_DEPR_SCHEMA_ID_FROM%type);
      type TTC_DEPRSCHEMA_TBL_TYPE is table of TTC_DEPRSCHEMA_REC_TYPE;
      TTC_DEPRSCHEMA_TBL TTC_DEPRSCHEMA_TBL_TYPE;

   begin

      L_MSG := 'Regathering stats for tax_trans_audit_trail_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      ANALYZE_CODE := ANALYZE_TABLE('tax_trans_audit_trail_grp');

      L_MSG := 'Regathering stats for tax_transfer_control...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      ANALYZE_CODE := ANALYZE_TABLE('tax_transfer_control');

      -- 1.  allocate the "proceeds from" per trans group however the amount was allocated.
      L_MSG := 'Updating alloc_sales_amount on tax_trans_audit_trail_grp ...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select TTAT.TRANS_GRP_ID,
             TAX_YEAR,
             FROM_TRID,
             BATCH,
             ITERATION,
             DECODE(AMOUNT_PROCESSED, 0, 0, ALLOC_AMOUNT / AMOUNT_PROCESSED) *
             (TRANS_SALES_AMOUNT * NVL(PCNT_ALLOCATED, 1)) bulk collect
        into AUDIT_SELECT_TBL
        from TAX_TRANS_AUDIT_TRAIL_GRP TTAT,
             (select TRANS_GRP_ID,
                     ((AMOUNT - AMOUNT_UNPROCESSED) / AMOUNT) PCNT_ALLOCATED,
                     (AMOUNT - AMOUNT_UNPROCESSED) AMOUNT_PROCESSED
                from TAX_BOOK_TRANSFERS_GRP
               where TAX_YEAR = A_TAX_YEAR
                 and NVL(AMOUNT, 0) <> 0) PPV /*we need this view to mimic however much the book amount was transferred, for the sales amount.*/
      /*if there's a $100 transfer, with $50 sales amount, but only $90 of the trans was processed,*/
      /*then we should only process $45 of the sales amount.  If users/product ever decides to exclude*/
      /*the sales corresponding to trans amounts not fully processed, then this view can just be stripped out.*/
       where TAX_YEAR = A_TAX_YEAR
         and NVL(TRANS_SALES_AMOUNT, 0) <> 0
         and TTAT.TRANS_GRP_ID = PPV.TRANS_GRP_ID(+);

      forall I in AUDIT_SELECT_TBL.FIRST .. AUDIT_SELECT_TBL.LAST
         update TAX_TRANS_AUDIT_TRAIL_GRP
            set ALLOC_SALES_AMOUNT = AUDIT_SELECT_TBL(I).ALLOC_SALES_AMOUNT
          where TAX_YEAR = A_TAX_YEAR
            and NVL(TRANS_SALES_AMOUNT, 0) <> 0
            and TRANS_GRP_ID = AUDIT_SELECT_TBL(I).TRANS_GRP_ID
            and TAX_YEAR = AUDIT_SELECT_TBL(I).TAX_YEAR
            and FROM_TRID = AUDIT_SELECT_TBL(I).FROM_TRID
            and BATCH = AUDIT_SELECT_TBL(I).BATCH
            and ITERATION = AUDIT_SELECT_TBL(I).ITERATION;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_trans_audit_trail_grp with allocated sales amount.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating alloc_sales_amount on tax_transfer_control ...';
      select TTAT.TAX_YEAR, 0 TO_TRID, TTAT.FROM_TRID, TTAT.COMPANY_ID_TO, nvl(TTAT.ASSET_ID_TO,0), sum(ALLOC_SALES_AMOUNT) bulk collect
        into TTC_TBL
        from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
       where TTAT.TAX_YEAR = A_TAX_YEAR
         and NVL(ALLOC_SALES_AMOUNT, 0) <> 0
       group by TTAT.TAX_YEAR, TTAT.FROM_TRID, TTAT.COMPANY_ID_TO, nvl(TTAT.ASSET_ID_TO,0);

      forall I in TTC_TBL.FIRST .. TTC_TBL.LAST
         update TAX_TRANSFER_CONTROL
            set PROCEEDS = TTC_TBL(I).PROCEEDS
          where FROM_TRID = TTC_TBL(I).FROM_TRID
            and COMPANY_ID = TTC_TBL(I).COMPANY_ID
            and nvl(ASSET_ID,0) = TTC_TBL(I).ASSET_ID
            and TAX_YEAR = TTC_TBL(I).TAX_YEAR
            and TAX_YEAR = A_TAX_YEAR
            and VERSION_ID = A_VERSION_ID
            and PACKAGE_ID = -1 ;

      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control with proceeds.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      -- 2.  find and update the transfer "tos" for the processed transfer "froms"
      L_MSG := 'Updating to_trid on tax_trans_audit_trail_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select TTAT.TRANS_GRP_ID,
             TTAT.TAX_YEAR,
             TRC_TO.TAX_RECORD_ID TO_TRID,
             TTAT.FROM_TRID       bulk collect
        into TTAT_GRP_TBL
        from TAX_TRANS_AUDIT_TRAIL_GRP TTAT,
             TAX_RECORD_CONTROL TRC_FROM,
             (select TAX_RECORD_ID,
                     LISTAGG(TO_CHAR(TAX_BOOK_ID) || ':') WITHIN group(order by TAX_BOOK_ID) TAX_BOOKS,
                     LISTAGG(TO_CHAR(TAX_RATE_ID) || ':') WITHIN group(order by TAX_RATE_ID) TAX_RATES
                from TAX_DEPRECIATION
                WHERE TAX_YEAR = A_TAX_YEAR
               group by TAX_RECORD_ID
                     ) BOOKS_RATES_FROM,
             TAX_BOOK_TRANSFERS_GRP TBT,
             TAX_BOOK_TRANSLATE_GROUP_MAP TBX,
             TAX_RECORD_CONTROL TRC_TO,
             (select TAX_RECORD_ID,
                     LISTAGG(TO_CHAR(TAX_BOOK_ID) || ':') WITHIN group(order by TAX_BOOK_ID) TAX_BOOKS,
                     LISTAGG(TO_CHAR(TAX_RATE_ID) || ':') WITHIN group(order by TAX_RATE_ID) TAX_RATES
                from TAX_DEPRECIATION
                WHERE TAX_YEAR = A_TAX_YEAR
               group by TAX_RECORD_ID
                     ) BOOKS_RATES_TO
       where TTAT.TAX_YEAR = A_TAX_YEAR
         and TTAT.FROM_TRID = TRC_FROM.TAX_RECORD_ID
         and TRC_FROM.TAX_RECORD_ID = BOOKS_RATES_FROM.TAX_RECORD_ID
         and TTAT.TRANS_GRP_ID = TBT.TRANS_GRP_ID
         and TBT.TAX_YEAR = A_TAX_YEAR
         and TBT.TBT_GROUP_ID_TO = TBX.TBT_GROUP_ID
         and TBT.TAX_ACTIVITY_CODE_ID_TO in (5)
         and TBX.TAX_CLASS_ID = TRC_TO.TAX_CLASS_ID
         and TRC_TO.COMPANY_ID = TBT.COMPANY_ID_TO
         and nvl(TRC_TO.ASSET_ID,0) = nvl(TBT.ASSET_ID_TO,0) /*join into asset id if populated.*/
         and TRC_TO.VINTAGE_ID = TRC_FROM.VINTAGE_ID /*the from and to must have the same vintage*/
         and TRC_TO.TAX_RECORD_ID = BOOKS_RATES_TO.TAX_RECORD_ID
         and BOOKS_RATES_TO.TAX_BOOKS = BOOKS_RATES_FROM.TAX_BOOKS /*the from and to records must have the same books*/
         and BOOKS_RATES_TO.TAX_RATES = BOOKS_RATES_FROM.TAX_RATES /*the from and to records must have the same rates*/
         and TRC_FROM.VERSION_ID = A_VERSION_ID
         and TRC_TO.VERSION_ID = A_VERSION_ID
         and TRC_FROM.TAX_RECORD_ID <> TRC_TO.TAX_RECORD_ID;

      forall I in TTAT_GRP_TBL.FIRST .. TTAT_GRP_TBL.LAST
         update TAX_TRANS_AUDIT_TRAIL_GRP
            set TO_TRID = TTAT_GRP_TBL(I).TO_TRID, CREATE_TO_TRID = 0
          where TRANS_GRP_ID = TTAT_GRP_TBL(I).TRANS_GRP_ID
            and FROM_TRID = TTAT_GRP_TBL(I).FROM_TRID
            and TAX_YEAR = TTAT_GRP_TBL(I).TAX_YEAR
            and TO_TRID < 0;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_trans_audit_trail_grp with found "to" tax records.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating to_trid on tax_transfer_control...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select distinct TTAT.TAX_YEAR, TTAT.TO_TRID TO_TRID, TTAT.FROM_TRID, TTAT.COMPANY_ID_TO,
            nvl(TTAT.ASSET_ID_TO,0), 0 bulk collect
        into TTC_TBL
        from TAX_TRANS_AUDIT_TRAIL_GRP TTAT
       where TTAT.TAX_YEAR = A_TAX_YEAR
         and NVL(TO_TRID, -1) > 0;

      forall I in TTC_TBL.FIRST .. TTC_TBL.LAST
         update TAX_TRANSFER_CONTROL
            set TO_TRID = TTC_TBL(I).TO_TRID, CREATE_TO_TRID = 0
          where FROM_TRID = TTC_TBL(I).FROM_TRID
            and TAX_YEAR = TTC_TBL(I).TAX_YEAR
            and COMPANY_ID = TTC_TBL(I).COMPANY_ID
            and nvl(ASSET_ID,0) = TTC_TBL(I).ASSET_ID
            and TAX_YEAR = A_TAX_YEAR
            and VERSION_ID = A_VERSION_ID
            and TO_TRID < 0
            and PACKAGE_ID = -1 ;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control with found "to" tax records.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      -- 3.  update the calc_deferred_gain, transfer type, and status comment
      L_MSG := 'Updating calc_deferred_gain, transfer type, and status comment (part 1)...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      update TAX_TRANSFER_CONTROL TTC
         set CALC_DEFERRED_GAIN = 1, TRANSFER_TYPE = 'Interface Def Gain Transfer',
             STATUS_COMMENT = 'OK, Interface Inter-Company Deferred Gain Transfer'
       where TAX_YEAR = A_TAX_YEAR
         and VERSION_ID = A_VERSION_ID
         and NVL(PROCEEDS, 0) <> 0
         and exists (select 1
                from TAX_RECORD_CONTROL TRC_FROM
               where TRC_FROM.TAX_RECORD_ID = TTC.FROM_TRID
                 and TTC.COMPANY_ID <> TRC_FROM.COMPANY_ID)
         and PACKAGE_ID = -1 ;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating calc_deferred_gain, transfer type, and status comment (part 2)...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      update TAX_TRANSFER_CONTROL TTC
         set CALC_DEFERRED_GAIN = 0, TRANSFER_TYPE = 'Interface Transfer',
             STATUS_COMMENT = 'OK, Interface Section 351 Transfer'
       where TAX_YEAR = A_TAX_YEAR
         and VERSION_ID = A_VERSION_ID
         and CALC_DEFERRED_GAIN is null
         and PACKAGE_ID = -1 ;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      -- 4.  update the tax_class_id to use for newly created tax records
      L_MSG := 'Updating tax_class_id on tax_transfer_control for rows that will require new tax records (where active found) ...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      select TTC.TAX_TRANSFER_ID, max(TBTGM.TAX_CLASS_ID) bulk collect
        into TTC_TCLASS_TBL
        from TAX_TRANSFER_CONTROL         TTC,
             TAX_TRANS_AUDIT_TRAIL_GRP    TTAT,
             TAX_BOOK_TRANSLATE_GROUP_MAP TBTGM
       where TTC.TAX_YEAR = A_TAX_YEAR
         and TTAT.TAX_YEAR = A_TAX_YEAR
         and TTC.VERSION_ID = A_VERSION_ID
         and TTC.CREATE_TO_TRID = 1
         and TTC.FROM_TRID = TTAT.FROM_TRID
         and TTC.COMPANY_ID = TTAT.COMPANY_ID_TO
         and TTAT.TBT_GROUP_ID_TO = TBTGM.TBT_GROUP_ID
         and TBTGM.ACTIVE <> 0
         and NVL(TTC.TAX_CLASS_ID, -999) = -999
         and TTC.PACKAGE_ID = -1
       group by TTC.TAX_TRANSFER_ID;

      forall I in TTC_TCLASS_TBL.FIRST .. TTC_TCLASS_TBL.LAST
         update TAX_TRANSFER_CONTROL
            set TAX_CLASS_ID = TTC_TCLASS_TBL(I).TAX_CLASS_ID
          where TAX_TRANSFER_ID = TTC_TCLASS_TBL(I).TAX_TRANSFER_ID;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control with tax class ids needed for new tax records (where active found).';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating tax_class_id on tax_transfer_control for rows that will require new tax records (remaining rows) ...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      select TTC.TAX_TRANSFER_ID, max(TBTGM.TAX_CLASS_ID) bulk collect
        into TTC_TCLASS_TBL
        from TAX_TRANSFER_CONTROL         TTC,
             TAX_TRANS_AUDIT_TRAIL_GRP    TTAT,
             TAX_BOOK_TRANSLATE_GROUP_MAP TBTGM
       where TTC.TAX_YEAR = A_TAX_YEAR
         and TTAT.TAX_YEAR = A_TAX_YEAR
         and TTC.VERSION_ID = A_VERSION_ID
         and TTC.CREATE_TO_TRID = 1
         and TTC.FROM_TRID = TTAT.FROM_TRID
         and TTC.COMPANY_ID = TTAT.COMPANY_ID_TO
         and TTAT.TBT_GROUP_ID_TO = TBTGM.TBT_GROUP_ID
         and NVL(TTC.TAX_CLASS_ID, -999) = -999
         and TTC.PACKAGE_ID = -1
       group by TTC.TAX_TRANSFER_ID;

      forall I in TTC_TCLASS_TBL.FIRST .. TTC_TCLASS_TBL.LAST
         update TAX_TRANSFER_CONTROL
            set TAX_CLASS_ID = TTC_TCLASS_TBL(I).TAX_CLASS_ID
          where TAX_TRANSFER_ID = TTC_TCLASS_TBL(I).TAX_TRANSFER_ID;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control with tax class ids needed for new tax records (remaining rows).';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      -- 5.  if company has changed, update the deferred_tax_schema_id (pull from
      --     tax_co_def_tax_schema)
      --     Don't worry if all rows aren't updated.  validate in the app, after committing.
      L_MSG := 'Updating deferred tax schema id...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      select TAX_TRANSFER_ID, DEFERRED_TAX_SCHEMA_ID bulk collect
        into TTC_DEFTAXSCH_TBL
        from (select TTC.TAX_TRANSFER_ID,
                     TCDTS.DEFERRED_TAX_SCHEMA_ID,
                     DENSE_RANK() OVER(partition by TTC.TAX_TRANSFER_ID order by TCDTS.EFFECTIVE_YEAR desc) EFF_YEAR_RANK
                from TAX_TRANSFER_CONTROL  TTC,
                     TAX_RECORD_CONTROL    TRC_FROM,
                     TAX_CO_DEF_TAX_SCHEMA TCDTS,
                     VINTAGE               V
               where TAX_YEAR = A_TAX_YEAR
                 and TTC.VERSION_ID = A_VERSION_ID
                 and TRC_FROM.TAX_RECORD_ID = TTC.FROM_TRID
                 and TTC.COMPANY_ID <> TRC_FROM.COMPANY_ID
                 and PACKAGE_ID = -1
                 and TRC_FROM.VINTAGE_ID = V.VINTAGE_ID
                 and TCDTS.COMPANY_ID = TTC.COMPANY_ID
                 and TCDTS.EFFECTIVE_YEAR <= V.YEAR)
       where EFF_YEAR_RANK = 1;


      forall I in TTC_DEFTAXSCH_TBL.FIRST .. TTC_DEFTAXSCH_TBL.LAST
         update TAX_TRANSFER_CONTROL
            set DEFERRED_TAX_SCHEMA_ID = TTC_DEFTAXSCH_TBL(I).DEFERRED_TAX_SCHEMA_ID
          where TAX_TRANSFER_ID = TTC_DEFTAXSCH_TBL(I).TAX_TRANSFER_ID;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control with deferred tax schema ids from tax co def tax schema.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      -- 6.  for those affected by the calc def gain records, find the tax_depr_schema_id from and to to use
      --     do this by looking at tax book translate, active, linked back to the "from" records.
      --     Don't worry if all rows aren't updated.  validate in the app, after committing.
      L_MSG := 'Updating tax depr schema ids...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select TTC.TAX_TRANSFER_ID, max(TBT.TAX_DEPR_SCHEMA_ID) bulk collect
        into TTC_DEPRSCHEMA_TBL
        from TAX_TRANSFER_CONTROL         TTC,
             TAX_TRANS_AUDIT_TRAIL_GRP    TTAT,
             TAX_BOOK_TRANSLATE_GROUP_MAP TBTGM,
             TAX_BOOK_TRANSLATE           TBT
       where TTC.TAX_YEAR = A_TAX_YEAR
         and TTAT.TAX_YEAR = A_TAX_YEAR
         and TTC.VERSION_ID = A_VERSION_ID
         and TTC.FROM_TRID = TTAT.FROM_TRID
         and TTAT.TBT_GROUP_ID = TBTGM.TBT_GROUP_ID
         and TBTGM.TBT_GROUP_ID = TBT.TBT_GROUP_ID
         and TBT.ACTIVE = 1
         and TBTGM.ACTIVE = 1
         and TTC.PACKAGE_ID = -1
       group by TTC.TAX_TRANSFER_ID;

      forall I in TTC_DEPRSCHEMA_TBL.FIRST .. TTC_DEPRSCHEMA_TBL.LAST
         update TAX_TRANSFER_CONTROL
            set TAX_DEPR_SCHEMA_ID_FROM = TTC_DEPRSCHEMA_TBL(I).TAX_DEPR_SCHEMA_ID,
                TAX_DEPR_SCHEMA_ID_TO = TTC_DEPRSCHEMA_TBL(I).TAX_DEPR_SCHEMA_ID
          where TAX_TRANSFER_ID = TTC_DEPRSCHEMA_TBL(I).TAX_TRANSFER_ID;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_transfer_control with tax depr shema ids (from, to) using tax book translate.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
   end F_FINAL_CALCS;

   -- function to start the log, process the transfers, end the log
   function F_POWERTAX_XFERS(A_TAX_YEAR   number,
                             A_START_MO   number,
                             A_END_MO     number,
                             A_VERSION_ID number) return varchar2 is
      L_MSG     varchar2(2000);
      L_PKG_VERSION  varchar2(2000);
      L_RTN_CODE     number(22, 0);

   begin

      if L_PROCESS_ID = -1 or L_PROCESS_ID is null then
         P_SETPROCESS();
      end if;

      PKG_PP_LOG.P_START_LOG(L_PROCESS_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE('This log is generated from the PowerTax Transfer Inteface,');
      PKG_PP_LOG.P_WRITE_MESSAGE('from clicking "Assign and Validate".');
      PKG_PP_LOG.P_WRITE_MESSAGE(' ');
      PKG_PP_LOG.P_WRITE_MESSAGE('Starting...');

      L_PKG_VERSION := F_GET_VERSION();
      PKG_PP_LOG.P_WRITE_MESSAGE('Transfers Package version : '||L_PKG_VERSION);
      PKG_PP_LOG.P_WRITE_MESSAGE('Processing with the following variable definitions:');
      PKG_PP_LOG.P_WRITE_MESSAGE('Tax Year : ' || A_TAX_YEAR);
      PKG_PP_LOG.P_WRITE_MESSAGE('Version ID : ' || A_VERSION_ID);

      ---- turn off auditing.
      L_RTN_CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      PKG_PP_LOG.P_WRITE_MESSAGE('Turn off auditing return code : ' || L_RTN_CODE);

      PKG_PP_LOG.P_WRITE_MESSAGE('Entering processing function...');
      L_MSG := F_RUN_ITERATION(A_TAX_YEAR, A_START_MO, A_END_MO, A_VERSION_ID);
      if L_MSG <> 'OK' then
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE('Function Result : ' || L_MSG);
      PKG_PP_LOG.P_WRITE_MESSAGE('Entering final calculations function...');
      L_MSG := F_FINAL_CALCS(A_TAX_YEAR, A_VERSION_ID);
      if L_MSG <> 'OK' then
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE('Function Result : ' || L_MSG);

      ---- turn auditing back on.
      L_RTN_CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'yes');
      PKG_PP_LOG.P_WRITE_MESSAGE('Turn on auditing return code : ' || L_RTN_CODE);

      PKG_PP_LOG.P_WRITE_MESSAGE('Finished...');
      PKG_PP_LOG.P_END_LOG();
      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
   end F_POWERTAX_XFERS;



   -- Functions to load the activity through the transfer history in a given year.
   --    This function populates the staging table with the original transfer from plant
   function F_TRACEBACK_LOAD(A_TAX_YEAR   number,
                             A_START_MO   number,
                             A_END_MO     number) return varchar2 is
      L_MSG     varchar2(2000);

   begin

      L_MSG := 'Deleting from transfer traceback staging table.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      delete from TAX_XFER_TRACEBACK_STG
      where TAX_YEAR = A_TAX_YEAR;

      L_MSG := 'Inserting into transfer traceback staging table.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      insert into TAX_XFER_TRACEBACK_STG
         (TAX_YEAR, FROM_ASSET_ID, TO_ASSET_ID, GL_POSTING_MO_YR, TO_ASSET_ACTIVITY_ID,
          FROM_TRANSFER_AMOUNT, TO_TRANSFER_AMOUNT, MIN_FROM_STATUS)
         select A_TAX_YEAR,
                TRANSFERS_FROM.ASSET_ID FROM_ASSET_ID,
                TRANSFERS_TO.ASSET_ID TO_ASSET_ID,
                TRANSFERS_TO.GL_POSTING_MO_YR GL_POSTING_MO_YR,
                TRANSFERS_TO.ASSET_ACTIVITY_ID TO_ASSET_ACTIVITY_ID,
                sum(TRANSFERS_FROM.TRANSFER_AMOUNT) FROM_TRANSFER_AMOUNT,
                sum(TRANSFERS_TO.TRANSFER_AMOUNT) TO_TRANSFER_AMOUNT,
                MIN_FROM_STATUS
           from (select CPR_ACTIVITY.ASSET_ID,
                        CPR_ACTIVITY.ASSET_ACTIVITY_ID,
                        CPR_ACTIVITY.ACTIVITY_STATUS,
                        CPR_ACTIVITY.GL_POSTING_MO_YR,
                        CPR_ACTIVITY.ACTIVITY_COST TRANSFER_AMOUNT
                   from CPR_LEDGER, CPR_ACTIVITY, ACTIVITY_CODE
                  where CPR_LEDGER.ASSET_ID = CPR_ACTIVITY.ASSET_ID
                    and RTRIM(UPPER(CPR_ACTIVITY.ACTIVITY_CODE)) =
                        RTRIM(UPPER(ACTIVITY_CODE.DESCRIPTION))
                    and TO_CHAR(CPR_ACTIVITY.GL_POSTING_MO_YR, 'YYYY') = TRUNC(A_TAX_YEAR)
                    and SUBSTR(CPR_ACTIVITY.MONTH_NUMBER, 1, 4) = TRUNC(A_TAX_YEAR)
                    and TO_NUMBER(SUBSTR(CPR_ACTIVITY.MONTH_NUMBER, 5, 2)) between A_START_MO and
                        A_END_MO
                    and ACTIVITY_CODE.TAX_ACTIVITY_CODE_ID in (5) /*Transfer To*/
                 ) TRANSFERS_TO,
                (select CPR_ACTIVITY.ASSET_ID,
                        CPR_ACTIVITY.GL_POSTING_MO_YR,
                        CPR_ACTIVITY.ACTIVITY_COST TRANSFER_AMOUNT,
                        min(CPR_ACTIVITY.ACTIVITY_STATUS) MIN_FROM_STATUS
                   from CPR_LEDGER, CPR_ACTIVITY, ACTIVITY_CODE
                  where CPR_LEDGER.ASSET_ID = CPR_ACTIVITY.ASSET_ID
                    and RTRIM(UPPER(CPR_ACTIVITY.ACTIVITY_CODE)) =
                        RTRIM(UPPER(ACTIVITY_CODE.DESCRIPTION))
                    and TO_CHAR(CPR_ACTIVITY.GL_POSTING_MO_YR, 'YYYY') = TRUNC(A_TAX_YEAR)
                    and SUBSTR(CPR_ACTIVITY.MONTH_NUMBER, 1, 4) = TRUNC(A_TAX_YEAR)
                    and TO_NUMBER(SUBSTR(CPR_ACTIVITY.MONTH_NUMBER, 5, 2)) between A_START_MO and
                        A_END_MO
                    and ACTIVITY_CODE.TAX_ACTIVITY_CODE_ID in (6) /*Transfer From*/
                  group by CPR_ACTIVITY.ASSET_ID,
                           CPR_ACTIVITY.GL_POSTING_MO_YR,
                           CPR_ACTIVITY.ACTIVITY_COST) TRANSFERS_FROM
          where TRANSFERS_FROM.GL_POSTING_MO_YR = TRANSFERS_TO.GL_POSTING_MO_YR
            and TRANSFERS_FROM.ASSET_ID = TRANSFERS_TO.ACTIVITY_STATUS
            and TRANSFERS_FROM.TRANSFER_AMOUNT * -1 = TRANSFERS_TO.TRANSFER_AMOUNT
            and NVL(TRANSFERS_FROM.TRANSFER_AMOUNT, 0) <> 0
          group by TRANSFERS_FROM.ASSET_ID,
                   TRANSFERS_TO.ASSET_ID,
                   TRANSFERS_FROM.GL_POSTING_MO_YR,
                   TRANSFERS_TO.ASSET_ACTIVITY_ID,
                   TRANSFERS_TO.GL_POSTING_MO_YR,
                   TRANSFERS_FROM.MIN_FROM_STATUS;

      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
   end F_TRACEBACK_LOAD;

   -- This function loops through all the records and tries to trace whatever can be.
   function F_TRACEBACK_PROCESS(A_TAX_YEAR   number,
                                A_DEBUG_FLAG boolean) return varchar2 is
      NUM_ROWS                 number;
      COUNTER                  number;
      PROCESSING_ITERATION     number;
      FULLY_PROCESSED          boolean;
      AMOUNT_LEFT_TO_PROCESS   number;
      FOUND_ROW_ORIG_AMOUNT    number;
      FOUND_ROW_FROM_ASSET_ID  number;
      FOUND_ROW_TO_ASSET_ID    number;
      FOUND_ROW_AMOUNT         number;
      HYBRID_ROW_FROM_ASSET_ID number;
      HYBRID_ROW_TO_ASSET_ID   number;
      HYBRID_ROW_AMOUNT        number;
      NEW_ROW_FROM_ASSET_ID    number;
      NEW_ROW_TO_ASSET_ID      number;
      ANALYZE_CODE             number;
      L_MSG                    varchar2(2000);
      cursor XFER_CUR is
         select FROM_ASSET_ID,
                TO_ASSET_ID,
                TO_NUMBER(TO_CHAR(GL_POSTING_MO_YR, 'yyyymm')) GL_POSTING_MO_NUM,
                TO_TRANSFER_AMOUNT,
                DENSE_RANK() OVER(partition by 1 order by TO_CHAR(GL_POSTING_MO_YR, 'yyyymm'),
                                                          MIN_FROM_STATUS,
                                                          FROM_ASSET_ID,
                                                          TO_ASSET_ID,
                                                          TO_ASSET_ACTIVITY_ID,
                                                          FROM_TRANSFER_AMOUNT
                                  ) PROCESSING_ORDER
           from TAX_XFER_TRACEBACK_STG
           where TAX_YEAR = A_TAX_YEAR
          order by 5;
      XFER_REC XFER_CUR%rowtype;

   begin

      L_MSG := 'Deleting from transfer traceback final table.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      delete from TAX_XFER_TRACEBACK_FINAL
      where TAX_YEAR = A_TAX_YEAR;

      L_MSG := 'Entering trace back processing cursor loop...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      open XFER_CUR;

      loop
         fetch XFER_CUR
            into XFER_REC;
         exit when XFER_CUR%notfound;

         ----this cursor contains all the transfers in the order in which they
         ----were processed.  (See the cursor's dense rank).
         if A_DEBUG_FLAG then
            L_MSG := '  ';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            L_MSG := 'Processing row ' || XFER_REC.PROCESSING_ORDER || '.';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         end if;

         ---- Initialize those variables that may be used in the processing loop or after the processing loop.
         NEW_ROW_FROM_ASSET_ID  := XFER_REC.FROM_ASSET_ID;
         NEW_ROW_TO_ASSET_ID    := XFER_REC.TO_ASSET_ID;
         AMOUNT_LEFT_TO_PROCESS := XFER_REC.TO_TRANSFER_AMOUNT;

         if A_DEBUG_FLAG then
            L_MSG := 'From ID : ' || NEW_ROW_FROM_ASSET_ID;
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            L_MSG := 'To ID : ' || NEW_ROW_TO_ASSET_ID;
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            L_MSG := 'Amount to Process : ' || AMOUNT_LEFT_TO_PROCESS;
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         end if;

         FULLY_PROCESSED      := false;
         PROCESSING_ITERATION := 0;
         while not FULLY_PROCESSED
         loop

            PROCESSING_ITERATION := PROCESSING_ITERATION + 1;
            if A_DEBUG_FLAG then
               L_MSG := 'Processing Iteration : ' || PROCESSING_ITERATION || '.';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            end if;
            ----
            ----The variables set up and used for processing may use:
            ----FOUND_ROW
            ----HYBRID_ROW
            ----NEW_ROW
            ----EXAMPLE:
            ----The cursor row being processed has from_id = 2, to_id = 3.
            ----this links back to a row found in the final processed table of from_id = 1, to_id = 2.
            ----there are three different row combinations that may or may not be processed here:
            ----FOUND_ROW: the row found to use on the final processed table:  from_id = 1, to_id = 2.
            ----HYBRID_ROW: the row needed to handle the traced back record: from_id = 1, to_id = 3.
            ----NEW_ROW: the row needed for dollars not traced back: from_id = 2, to_id = 3.

            ----grab the earliest row, with any amount to use, from a previously processed record.
            ----the dense rank will sort the ones rows in the final table that can be used for this cur record.
            begin
               select FROM_ASSET_ID, TO_ASSET_ID, RUNNING_TO_TRANSFER_AMOUNT
                 into FOUND_ROW_FROM_ASSET_ID, FOUND_ROW_TO_ASSET_ID, FOUND_ROW_ORIG_AMOUNT
                 from (select XFER_FINAL.FROM_ASSET_ID,
                              XFER_FINAL.TO_ASSET_ID,
                              XFER_FINAL.RUNNING_TO_TRANSFER_AMOUNT,
                              DENSE_RANK() OVER(partition by(XFER_FINAL.TO_ASSET_ID) order by FIRST_PROCESSING_ORDER) WHILE_LOOP_RANK
                         from TAX_XFER_TRACEBACK_FINAL XFER_FINAL
                        where TAX_YEAR = A_TAX_YEAR
                          and XFER_FINAL.TO_ASSET_ID = NEW_ROW_FROM_ASSET_ID
                          and AMOUNT_LEFT_TO_PROCESS > 0 /* not processing negative transfers */
                          and XFER_FINAL.RUNNING_TO_TRANSFER_AMOUNT > 0 /* we need dollars to trace back*/
                       )
                where WHILE_LOOP_RANK = 1
                and rownum = 1;
            exception
               when NO_DATA_FOUND then
                  ----no rows were found to use, so change the FULLY_PROCESSED boolean on this cur record and exit the loop
                  ----also, on exiting, we need to process the remaining amount, placing whatever is left into the original
                  ----from and to asset ids on the final processing table...
                  if A_DEBUG_FLAG then
                     L_MSG := 'No more rows found to trace back to.  Flagging as fully processed.';
                     PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  end if;
                  FULLY_PROCESSED := true;
                  CONTINUE;
            end;

            ----FOUND_ROW: the row found to use on the final processed table:  from_id = 1, to_id = 2.
            ----HYBRID_ROW: the row needed to handle the traced back record: from_id = 1, to_id = 3.
            ----NEW_ROW: the row needed for dollars not traced back (cursor record's from and to): from_id = 2, to_id = 3.
            HYBRID_ROW_FROM_ASSET_ID := FOUND_ROW_FROM_ASSET_ID;
            HYBRID_ROW_TO_ASSET_ID   := XFER_REC.TO_ASSET_ID;

            if A_DEBUG_FLAG then
               L_MSG := 'Found Row From ID : ' || FOUND_ROW_FROM_ASSET_ID;
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               L_MSG := 'Found Row To ID : ' || FOUND_ROW_TO_ASSET_ID;
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               L_MSG := 'Found Row Orig Amount : ' || FOUND_ROW_ORIG_AMOUNT;
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               L_MSG := 'Hybrid Row From ID : ' || HYBRID_ROW_FROM_ASSET_ID;
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               L_MSG := 'Hybrid Row To ID : ' || HYBRID_ROW_TO_ASSET_ID;
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            end if;

            if FOUND_ROW_ORIG_AMOUNT >= AMOUNT_LEFT_TO_PROCESS then
               ----this means that the full amount on the cur record can be taken
               ----from a previously processed record.

               FOUND_ROW_AMOUNT       := FOUND_ROW_ORIG_AMOUNT - AMOUNT_LEFT_TO_PROCESS;
               HYBRID_ROW_AMOUNT      := AMOUNT_LEFT_TO_PROCESS;
               AMOUNT_LEFT_TO_PROCESS := 0;

            elsif FOUND_ROW_ORIG_AMOUNT < AMOUNT_LEFT_TO_PROCESS then
               ----this means there is some money available to use on this record

               FOUND_ROW_AMOUNT       := 0;
               HYBRID_ROW_AMOUNT      := FOUND_ROW_ORIG_AMOUNT;
               AMOUNT_LEFT_TO_PROCESS := AMOUNT_LEFT_TO_PROCESS - FOUND_ROW_ORIG_AMOUNT;

            end if;

            if A_DEBUG_FLAG then
               L_MSG := 'Calculated amounts : ';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               L_MSG := '   Found Row Amount : ' || FOUND_ROW_AMOUNT;
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               L_MSG := '   Hybrid Row Amount : ' || HYBRID_ROW_AMOUNT;
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               L_MSG := '   Amount Left to Process : ' || AMOUNT_LEFT_TO_PROCESS;
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            end if;

            ----update or insert the first two record sets.  The final record set will be processed outside the loop.

            ----FOUND ROW (1 => 2)
            update TAX_XFER_TRACEBACK_FINAL XFER_FINAL
               set XFER_FINAL.RUNNING_TO_TRANSFER_AMOUNT = FOUND_ROW_AMOUNT,
                   XFER_FINAL.LAST_PROCESSING_ORDER = XFER_REC.PROCESSING_ORDER
             where TAX_YEAR = A_TAX_YEAR
               and XFER_FINAL.FROM_ASSET_ID = FOUND_ROW_FROM_ASSET_ID
               and XFER_FINAL.TO_ASSET_ID = FOUND_ROW_TO_ASSET_ID;
            NUM_ROWS := sql%rowcount;
            if A_DEBUG_FLAG then
               L_MSG := NUM_ROWS || ' row updated on found row.';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            end if;

            ----HYBRID ROW (1 => 3)
            COUNTER := 0;

            select count(*)
              into COUNTER
              from TAX_XFER_TRACEBACK_FINAL XFER_FINAL
             where TAX_YEAR = A_TAX_YEAR
               and XFER_FINAL.FROM_ASSET_ID = HYBRID_ROW_FROM_ASSET_ID
               and XFER_FINAL.TO_ASSET_ID = HYBRID_ROW_TO_ASSET_ID
               and rownum = 1;
            if A_DEBUG_FLAG then
               L_MSG := COUNTER || ' row found matching hybrid row ids.';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            end if;

            if COUNTER = 0 then
               insert into TAX_XFER_TRACEBACK_FINAL XFER_FINAL
                  (TAX_YEAR, FROM_ASSET_ID, TO_ASSET_ID, RUNNING_TO_TRANSFER_AMOUNT, FIRST_PROCESSING_ORDER,
                   LAST_PROCESSING_ORDER, GL_POSTING_MO_NUM)
               values
                  (A_TAX_YEAR, HYBRID_ROW_FROM_ASSET_ID, HYBRID_ROW_TO_ASSET_ID, HYBRID_ROW_AMOUNT,
                   XFER_REC.PROCESSING_ORDER, XFER_REC.PROCESSING_ORDER, XFER_REC.GL_POSTING_MO_NUM);
               NUM_ROWS := sql%rowcount;
               if A_DEBUG_FLAG then
                  L_MSG := NUM_ROWS || ' row inserted for hybrid row.';
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               end if;
            elsif HYBRID_ROW_AMOUNT <> 0 then
               update TAX_XFER_TRACEBACK_FINAL XFER_FINAL
                  set XFER_FINAL.RUNNING_TO_TRANSFER_AMOUNT = XFER_FINAL.RUNNING_TO_TRANSFER_AMOUNT +
                                                               HYBRID_ROW_AMOUNT,
                      XFER_FINAL.LAST_PROCESSING_ORDER = XFER_REC.PROCESSING_ORDER
                where TAX_YEAR = A_TAX_YEAR
                  and XFER_FINAL.FROM_ASSET_ID = HYBRID_ROW_FROM_ASSET_ID
                  and XFER_FINAL.TO_ASSET_ID = HYBRID_ROW_TO_ASSET_ID;
               NUM_ROWS := sql%rowcount;
               if A_DEBUG_FLAG then
                  L_MSG := NUM_ROWS || ' row updated for hybrid row.';
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               end if;
            end if;

         end loop;

         ----whatever dollars have not been processed (could not trace back everything, or anything) need to go somewhere, so just
         ----insert whatever is left into the final processed table.

         ----NEW ROW (2 => 3)
         COUNTER := 0;

         select count(*)
           into COUNTER
           from TAX_XFER_TRACEBACK_FINAL XFER_FINAL
          where TAX_YEAR = A_TAX_YEAR
            and XFER_FINAL.FROM_ASSET_ID = NEW_ROW_FROM_ASSET_ID
            and XFER_FINAL.TO_ASSET_ID = NEW_ROW_TO_ASSET_ID
            and rownum = 1;
         if A_DEBUG_FLAG then
            L_MSG := COUNTER || ' row found matching new row ids.';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         end if;

         if COUNTER = 0 then
            insert into TAX_XFER_TRACEBACK_FINAL XFER_FINAL
               (TAX_YEAR, FROM_ASSET_ID, TO_ASSET_ID, RUNNING_TO_TRANSFER_AMOUNT, FIRST_PROCESSING_ORDER,
                LAST_PROCESSING_ORDER, GL_POSTING_MO_NUM)
            values
               (A_TAX_YEAR, NEW_ROW_FROM_ASSET_ID, NEW_ROW_TO_ASSET_ID, AMOUNT_LEFT_TO_PROCESS,
                XFER_REC.PROCESSING_ORDER, XFER_REC.PROCESSING_ORDER, XFER_REC.GL_POSTING_MO_NUM);
            NUM_ROWS := sql%rowcount;
            if A_DEBUG_FLAG then
               L_MSG := NUM_ROWS || ' row inserted for new row.';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            end if;
         elsif AMOUNT_LEFT_TO_PROCESS <> 0 then
            update TAX_XFER_TRACEBACK_FINAL XFER_FINAL
               set XFER_FINAL.RUNNING_TO_TRANSFER_AMOUNT = XFER_FINAL.RUNNING_TO_TRANSFER_AMOUNT +
                                                            AMOUNT_LEFT_TO_PROCESS,
                   XFER_FINAL.LAST_PROCESSING_ORDER = XFER_REC.PROCESSING_ORDER
             where TAX_YEAR = A_TAX_YEAR
               and XFER_FINAL.FROM_ASSET_ID = NEW_ROW_FROM_ASSET_ID
               and XFER_FINAL.TO_ASSET_ID = NEW_ROW_TO_ASSET_ID;
            NUM_ROWS := sql%rowcount;
            if A_DEBUG_FLAG then
               L_MSG := NUM_ROWS || ' row updated for new row.';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            end if;
         end if;

         ----ANALYZE THE FINAL TABLE EVERY 1000 ROWS
         if mod( XFER_REC.PROCESSING_ORDER, 1000) = 0 then
            if A_DEBUG_FLAG then
               L_MSG := 'Regathering stats for final table...';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            end if;
            ANALYZE_CODE := ANALYZE_TABLE('TAX_XFER_TRACEBACK_FINAL');
         end if;

      end loop;

      close XFER_CUR;

      --
      -- verification.  does the amount actually used by each asset id (before and after trace back) tie out?
      --
      COUNTER := 0;

      select count(*)
        into COUNTER
        from (select FROM_ASSET_ID, sum(ORIG_XFER_AMT), sum(FINAL_XFER_AMT)
                from (select FROM_ASSET_ID, sum(TO_TRANSFER_AMOUNT) ORIG_XFER_AMT, 0 FINAL_XFER_AMT
                        from (select FROM_ASSET_ID, (-1 * TO_TRANSFER_AMOUNT) TO_TRANSFER_AMOUNT
                                from TAX_XFER_TRACEBACK_STG
                                where TAX_YEAR = A_TAX_YEAR
                              union all
                              select TO_ASSET_ID, TO_TRANSFER_AMOUNT
                                from TAX_XFER_TRACEBACK_STG
                                where TAX_YEAR = A_TAX_YEAR)
                       group by FROM_ASSET_ID

                      union all

                      select FROM_ASSET_ID, 0, sum(TO_TRANSFER_AMOUNT)
                        from (select FROM_ASSET_ID, (-1 * RUNNING_TO_TRANSFER_AMOUNT) TO_TRANSFER_AMOUNT
                                from TAX_XFER_TRACEBACK_FINAL
                                where TAX_YEAR = A_TAX_YEAR
                              union all
                              select TO_ASSET_ID, RUNNING_TO_TRANSFER_AMOUNT
                                from TAX_XFER_TRACEBACK_FINAL
                                where TAX_YEAR = A_TAX_YEAR)
                       group by FROM_ASSET_ID)
               group by FROM_ASSET_ID
              having sum(ORIG_XFER_AMT) <> sum(FINAL_XFER_AMT));

      if COUNTER = 0 then
         L_MSG := 'Passed Validation.';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      else
         L_MSG := 'Validation Error!';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
      end if;

      L_MSG := 'Finished processing tracebacks.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return 'OK';

   exception
      when others then
         L_MSG := 'Exception encountered on :';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         L_MSG := '  Processing ID:'||XFER_REC.PROCESSING_ORDER;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         L_MSG := '  From Asset ID:'||XFER_REC.FROM_ASSET_ID;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         L_MSG := '  To Asset ID:'||XFER_REC.TO_ASSET_ID;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         L_MSG := SUBSTR( sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
   end F_TRACEBACK_PROCESS;


   -- This function populates tax book transfers from the final traced back data, plus
   --    joins to the plant tables and uses a temporary table.
   function F_POPULATE_TAX_BOOK_XFERS(A_TAX_YEAR   number,
                                      A_START_MO   number,
                                      A_END_MO     number) return varchar2 is
      L_MSG     varchar2(2000);

   begin

      L_MSG := 'Truncating temporary table used to stage tax book transfers.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      TRUNCATE_TABLE('TAX_TEMP_BOOK_TRANSFERS_STG');

      L_MSG := 'Inserting into temporary table used to stage tax book transfers.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      ----join in to company system options to see if asset id needs to be used
      ----in the processing...

      insert into TAX_TEMP_BOOK_TRANSFERS_STG
      (TRANSFER_GROUP, COMPANY_ID_FROM, BUS_SEGMENT_ID_FROM, UTILITY_ACCOUNT_ID_FROM,
      SUB_ACCOUNT_ID_FROM, TAX_LOCATION_ID_FROM, GL_ACCOUNT_ID_FROM, TAX_DISTINCTION_ID_FROM,
      CLASS_CODE_ID_FROM, CC_TAX_INDICATOR_FROM, CC_VALUE_FROM, IN_SERVICE_YEAR_FROM,
      GL_POSTING_MO_YR_FROM, ASSET_ID_FROM, TAX_ACTIVITY_CODE_ID_FROM, COMPANY_ID_TO, BUS_SEGMENT_ID_TO,
      UTILITY_ACCOUNT_ID_TO, SUB_ACCOUNT_ID_TO, TAX_LOCATION_ID_TO, GL_ACCOUNT_ID_TO,
      TAX_DISTINCTION_ID_TO, CLASS_CODE_ID_TO, CC_TAX_INDICATOR_TO, CC_VALUE_TO,
      IN_SERVICE_YEAR_TO, GL_POSTING_MO_YR_TO, ASSET_ID_TO, TAX_ACTIVITY_CODE_ID_TO, AMOUNT,
      BOOK_SALE_AMOUNT)
      select ROWNUM,
             COMPANY_ID_FROM,
             BUS_SEGMENT_ID_FROM,
             UTILITY_ACCOUNT_ID_FROM,
             SUB_ACCOUNT_ID_FROM,
             TAX_LOCATION_ID_FROM,
             GL_ACCOUNT_ID_FROM,
             TAX_DISTINCTION_ID_FROM,
             CLASS_CODE_ID_FROM,
             TAX_INDICATOR_FROM,
             CC_VALUE_FROM,
             IN_SERVICE_YEAR_FROM,
             GL_POSTING_MO_YR_FROM,
             ASSET_ID_FROM,
             6 TAX_ACTIVITY_CODE_ID_FROM,
             COMPANY_ID_TO,
             BUS_SEGMENT_ID_TO,
             UTILITY_ACCOUNT_ID_TO,
             SUB_ACCOUNT_ID_TO,
             TAX_LOCATION_ID_TO,
             GL_ACCOUNT_ID_TO,
             TAX_DISTINCTION_ID_TO,
             CLASS_CODE_ID_TO,
             TAX_INDICATOR_TO,
             CC_VALUE_TO,
             IN_SERVICE_YEAR_TO,
             GL_POSTING_MO_YR_TO,
             ASSET_ID_TO,
             5 TAX_ACTIVITY_CODE_ID_TO,
             AMOUNT,
             BOOK_SALE_AMOUNT
        from (select NVL(FROM_COMPANY.POWERTAX_COMPANY_ID,FROM_TRANSFER.COMPANY_ID) COMPANY_ID_FROM,
                     FROM_TRANSFER.BUS_SEGMENT_ID BUS_SEGMENT_ID_FROM,
                     FROM_TRANSFER.UTILITY_ACCOUNT_ID UTILITY_ACCOUNT_ID_FROM,
                     FROM_TRANSFER.SUB_ACCOUNT_ID SUB_ACCOUNT_ID_FROM,
                     NVL(FROM_LOCATION.TAX_LOCATION_ID, 1) TAX_LOCATION_ID_FROM,
                     NVL(FROM_GL_ACCOUNT.TAX_GL_ACCOUNT_ID, 0) GL_ACCOUNT_ID_FROM,
                     NVL(FROM_RETIREMENT_UNIT.TAX_DISTINCTION_ID, 1) TAX_DISTINCTION_ID_FROM,
                     NVL(FROM_CLASS_CODE_CPR_LEDGER.CLASS_CODE_ID, 0) CLASS_CODE_ID_FROM,
                     NVL(FROM_CLASS_CODE_CPR_LEDGER.TAX_INDICATOR, 0) TAX_INDICATOR_FROM,
                     NVL(FROM_CLASS_CODE_CPR_LEDGER.VALUE, ' ') CC_VALUE_FROM,
                     FROM_TRANSFER.ENG_IN_SERVICE_YEAR IN_SERVICE_YEAR_FROM,
                     NVL(X.GL_POSTING_MO_YR, TO_DATE(TO_CHAR(TRUNC( A_TAX_YEAR )) || '01', 'YYYYMM')) GL_POSTING_MO_YR_FROM,
                     DECODE(FROM_ASSET_ID_USEAGE.COMPANY_ID, NULL, NULL, FROM_TRANSFER.ASSET_ID) ASSET_ID_FROM,
                     NVL(TO_COMPANY.POWERTAX_COMPANY_ID,TO_TRANSFER.COMPANY_ID) COMPANY_ID_TO,
                     TO_TRANSFER.BUS_SEGMENT_ID BUS_SEGMENT_ID_TO,
                     TO_TRANSFER.UTILITY_ACCOUNT_ID UTILITY_ACCOUNT_ID_TO,
                     TO_TRANSFER.SUB_ACCOUNT_ID SUB_ACCOUNT_ID_TO,
                     NVL(TO_LOCATION.TAX_LOCATION_ID, 1) TAX_LOCATION_ID_TO,
                     NVL(TO_GL_ACCOUNT.TAX_GL_ACCOUNT_ID, 0) GL_ACCOUNT_ID_TO,
                     NVL(TO_RETIREMENT_UNIT.TAX_DISTINCTION_ID, 1) TAX_DISTINCTION_ID_TO,
                     NVL(TO_CLASS_CODE_CPR_LEDGER.CLASS_CODE_ID, 0) CLASS_CODE_ID_TO,
                     NVL(TO_CLASS_CODE_CPR_LEDGER.TAX_INDICATOR, 0) TAX_INDICATOR_TO,
                     NVL(TO_CLASS_CODE_CPR_LEDGER.VALUE, ' ') CC_VALUE_TO,
                     TO_TRANSFER.ENG_IN_SERVICE_YEAR IN_SERVICE_YEAR_TO,
                     NVL(X.GL_POSTING_MO_YR, TO_DATE(TO_CHAR(TRUNC( A_TAX_YEAR )) || '01', 'YYYYMM')) GL_POSTING_MO_YR_TO,
                     DECODE(TO_ASSET_ID_USEAGE.COMPANY_ID, NULL, NULL, TO_TRANSFER.ASSET_ID) ASSET_ID_TO,
                     sum(X.TO_TRANSFER_AMOUNT) AMOUNT,
                     0 BOOK_SALE_AMOUNT
                from (select FROM_ASSET_ID,
                             TO_ASSET_ID,
                             TO_DATE(GL_POSTING_MO_NUM, 'yyyymm') GL_POSTING_MO_YR,
                             sum(RUNNING_TO_TRANSFER_AMOUNT) TO_TRANSFER_AMOUNT
                        from TAX_XFER_TRACEBACK_FINAL
                       where TAX_YEAR = A_TAX_YEAR
                         and RUNNING_TO_TRANSFER_AMOUNT <> 0 /*the rows eliminated by the trace back logic are left in this table with $0*/
                       group by FROM_ASSET_ID, TO_ASSET_ID, TO_DATE(GL_POSTING_MO_NUM, 'yyyymm')) X,
                     CPR_LEDGER FROM_TRANSFER,
                     ASSET_LOCATION FROM_LOCATION,
                     GL_ACCOUNT FROM_GL_ACCOUNT,
                     RETIREMENT_UNIT FROM_RETIREMENT_UNIT,
                     COMPANY FROM_COMPANY,
                     (select CLASS_CODE_CPR_LEDGER.ASSET_ID,
                             CLASS_CODE_CPR_LEDGER.CLASS_CODE_ID,
                             CLASS_CODE.TAX_INDICATOR,
                             CLASS_CODE_CPR_LEDGER.VALUE
                        from CLASS_CODE, CLASS_CODE_CPR_LEDGER
                       where CLASS_CODE.TAX_INDICATOR = 1
                         and CLASS_CODE.CLASS_CODE_ID = CLASS_CODE_CPR_LEDGER.CLASS_CODE_ID) FROM_CLASS_CODE_CPR_LEDGER,
                     (select distinct COMPANY_ID
                      from (
                            /** Company System Option is in Use **/
                            select COMPANY_SETUP.COMPANY_ID,
                                    UPPER(trim(NVL(PPBASE_SYSTEM_OPTIONS_COMPANY.OPTION_VALUE,
                                                   PPBASE_SYSTEM_OPTIONS.PP_DEFAULT_VALUE))) OPTION_VALUE
                              from COMPANY_SETUP,
                                    PPBASE_SYSTEM_OPTIONS,
                                    PPBASE_SYSTEM_OPTIONS_MODULE,
                                    PPBASE_SYSTEM_OPTIONS_COMPANY
                             where UPPER(trim(PPBASE_SYSTEM_OPTIONS_MODULE.MODULE)) = 'POWERTAX'
                               and PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                                   PPBASE_SYSTEM_OPTIONS_MODULE.SYSTEM_OPTION_ID
                               and UPPER(trim(PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID)) =
                                   UPPER(trim('Extract Asset IDs with Interfaces'))
                               and PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                                   PPBASE_SYSTEM_OPTIONS_COMPANY.SYSTEM_OPTION_ID
                               and PPBASE_SYSTEM_OPTIONS_COMPANY.COMPANY_ID = COMPANY_SETUP.COMPANY_ID
                            union all
                            /** Main System Option used for all other companies **/
                            select COMPANY_SETUP.COMPANY_ID,
                                    UPPER(trim(NVL(PPBASE_SYSTEM_OPTIONS.OPTION_VALUE,
                                                   PPBASE_SYSTEM_OPTIONS.PP_DEFAULT_VALUE)))
                              from COMPANY_SETUP, PPBASE_SYSTEM_OPTIONS, PPBASE_SYSTEM_OPTIONS_MODULE
                             where UPPER(trim(PPBASE_SYSTEM_OPTIONS_MODULE.MODULE)) = 'POWERTAX'
                               and PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                                   PPBASE_SYSTEM_OPTIONS_MODULE.SYSTEM_OPTION_ID
                               and UPPER(trim(PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID)) =
                                   UPPER(trim('Extract Asset IDs with Interfaces'))
                               and not exists
                             (select 1
                                      from PPBASE_SYSTEM_OPTIONS_COMPANY
                                     where PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                                           PPBASE_SYSTEM_OPTIONS_COMPANY.SYSTEM_OPTION_ID
                                       and PPBASE_SYSTEM_OPTIONS_COMPANY.COMPANY_ID = COMPANY_SETUP.COMPANY_ID)
                           )
                      where OPTION_VALUE = 'YES' ) FROM_ASSET_ID_USEAGE,
                     CPR_LEDGER TO_TRANSFER,
                     ASSET_LOCATION TO_LOCATION,
                     GL_ACCOUNT TO_GL_ACCOUNT,
                     RETIREMENT_UNIT TO_RETIREMENT_UNIT,
                     COMPANY TO_COMPANY,
                     (select CLASS_CODE_CPR_LEDGER.ASSET_ID,
                             CLASS_CODE_CPR_LEDGER.CLASS_CODE_ID,
                             CLASS_CODE.TAX_INDICATOR,
                             CLASS_CODE_CPR_LEDGER.VALUE
                        from CLASS_CODE, CLASS_CODE_CPR_LEDGER
                       where CLASS_CODE.TAX_INDICATOR = 1
                         and CLASS_CODE.CLASS_CODE_ID = CLASS_CODE_CPR_LEDGER.CLASS_CODE_ID) TO_CLASS_CODE_CPR_LEDGER,
                     (select distinct COMPANY_ID
                      from (
                            /** Company System Option is in Use **/
                            select COMPANY_SETUP.COMPANY_ID,
                                    UPPER(trim(NVL(PPBASE_SYSTEM_OPTIONS_COMPANY.OPTION_VALUE,
                                                   PPBASE_SYSTEM_OPTIONS.PP_DEFAULT_VALUE))) OPTION_VALUE
                              from COMPANY_SETUP,
                                    PPBASE_SYSTEM_OPTIONS,
                                    PPBASE_SYSTEM_OPTIONS_MODULE,
                                    PPBASE_SYSTEM_OPTIONS_COMPANY
                             where UPPER(trim(PPBASE_SYSTEM_OPTIONS_MODULE.MODULE)) = 'POWERTAX'
                               and PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                                   PPBASE_SYSTEM_OPTIONS_MODULE.SYSTEM_OPTION_ID
                               and UPPER(trim(PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID)) =
                                   UPPER(trim('Extract Asset IDs with Interfaces'))
                               and PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                                   PPBASE_SYSTEM_OPTIONS_COMPANY.SYSTEM_OPTION_ID
                               and PPBASE_SYSTEM_OPTIONS_COMPANY.COMPANY_ID = COMPANY_SETUP.COMPANY_ID
                            union all
                            /** Main System Option used for all other companies **/
                            select COMPANY_SETUP.COMPANY_ID,
                                    UPPER(trim(NVL(PPBASE_SYSTEM_OPTIONS.OPTION_VALUE,
                                                   PPBASE_SYSTEM_OPTIONS.PP_DEFAULT_VALUE)))
                              from COMPANY_SETUP, PPBASE_SYSTEM_OPTIONS, PPBASE_SYSTEM_OPTIONS_MODULE
                             where UPPER(trim(PPBASE_SYSTEM_OPTIONS_MODULE.MODULE)) = 'POWERTAX'
                               and PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                                   PPBASE_SYSTEM_OPTIONS_MODULE.SYSTEM_OPTION_ID
                               and UPPER(trim(PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID)) =
                                   UPPER(trim('Extract Asset IDs with Interfaces'))
                               and not exists
                             (select 1
                                      from PPBASE_SYSTEM_OPTIONS_COMPANY
                                     where PPBASE_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                                           PPBASE_SYSTEM_OPTIONS_COMPANY.SYSTEM_OPTION_ID
                                       and PPBASE_SYSTEM_OPTIONS_COMPANY.COMPANY_ID = COMPANY_SETUP.COMPANY_ID)
                           )
                      where OPTION_VALUE = 'YES' ) TO_ASSET_ID_USEAGE

               where X.FROM_ASSET_ID = FROM_TRANSFER.ASSET_ID
                 and X.TO_ASSET_ID = TO_TRANSFER.ASSET_ID

                 and FROM_TRANSFER.ASSET_LOCATION_ID = FROM_LOCATION.ASSET_LOCATION_ID
                 and FROM_TRANSFER.GL_ACCOUNT_ID = FROM_GL_ACCOUNT.GL_ACCOUNT_ID
                 and FROM_TRANSFER.RETIREMENT_UNIT_ID = FROM_RETIREMENT_UNIT.RETIREMENT_UNIT_ID
                 and FROM_TRANSFER.COMPANY_ID = FROM_COMPANY.COMPANY_ID
                 and FROM_TRANSFER.ASSET_ID = FROM_CLASS_CODE_CPR_LEDGER.ASSET_ID(+)

                 and TO_TRANSFER.ASSET_LOCATION_ID = TO_LOCATION.ASSET_LOCATION_ID
                 and TO_TRANSFER.GL_ACCOUNT_ID = TO_GL_ACCOUNT.GL_ACCOUNT_ID
                 and TO_TRANSFER.RETIREMENT_UNIT_ID = TO_RETIREMENT_UNIT.RETIREMENT_UNIT_ID
                 and TO_TRANSFER.COMPANY_ID = TO_COMPANY.COMPANY_ID
                 and TO_TRANSFER.ASSET_ID = TO_CLASS_CODE_CPR_LEDGER.ASSET_ID(+)

                 and FROM_TRANSFER.COMPANY_ID = FROM_ASSET_ID_USEAGE.COMPANY_ID (+)
                 and TO_TRANSFER.COMPANY_ID = TO_ASSET_ID_USEAGE.COMPANY_ID (+)

               group by NVL(FROM_COMPANY.POWERTAX_COMPANY_ID,FROM_TRANSFER.COMPANY_ID),
                        FROM_TRANSFER.BUS_SEGMENT_ID,
                        FROM_TRANSFER.UTILITY_ACCOUNT_ID,
                        FROM_TRANSFER.SUB_ACCOUNT_ID,
                        NVL(FROM_LOCATION.TAX_LOCATION_ID, 1),
                        NVL(FROM_GL_ACCOUNT.TAX_GL_ACCOUNT_ID, 0),
                        NVL(FROM_RETIREMENT_UNIT.TAX_DISTINCTION_ID, 1),
                        NVL(FROM_CLASS_CODE_CPR_LEDGER.CLASS_CODE_ID, 0),
                        NVL(FROM_CLASS_CODE_CPR_LEDGER.TAX_INDICATOR, 0),
                        NVL(FROM_CLASS_CODE_CPR_LEDGER.VALUE, ' '),
                        FROM_TRANSFER.ENG_IN_SERVICE_YEAR,
                        NVL(X.GL_POSTING_MO_YR, TO_DATE(TO_CHAR(TRUNC( A_TAX_YEAR )) || '01', 'YYYYMM')),
                        DECODE(FROM_ASSET_ID_USEAGE.COMPANY_ID, NULL, NULL, FROM_TRANSFER.ASSET_ID),
                        NVL(TO_COMPANY.POWERTAX_COMPANY_ID,TO_TRANSFER.COMPANY_ID),
                        TO_TRANSFER.BUS_SEGMENT_ID,
                        TO_TRANSFER.UTILITY_ACCOUNT_ID,
                        TO_TRANSFER.SUB_ACCOUNT_ID,
                        NVL(TO_LOCATION.TAX_LOCATION_ID, 1),
                        NVL(TO_GL_ACCOUNT.TAX_GL_ACCOUNT_ID, 0),
                        NVL(TO_RETIREMENT_UNIT.TAX_DISTINCTION_ID, 1),
                        NVL(TO_CLASS_CODE_CPR_LEDGER.CLASS_CODE_ID, 0),
                        NVL(TO_CLASS_CODE_CPR_LEDGER.TAX_INDICATOR, 0),
                        NVL(TO_CLASS_CODE_CPR_LEDGER.VALUE, ' '),
                        TO_TRANSFER.ENG_IN_SERVICE_YEAR,
                        NVL(X.GL_POSTING_MO_YR, TO_DATE(TO_CHAR(TRUNC( A_TAX_YEAR )) || '01', 'YYYYMM')),
                        DECODE(TO_ASSET_ID_USEAGE.COMPANY_ID, NULL, NULL, TO_TRANSFER.ASSET_ID)
              having sum(X.TO_TRANSFER_AMOUNT) <> 0);


      L_MSG := 'Deleting from tax book transfers table.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      delete from TAX_BOOK_TRANSFERS
      where YEAR = trunc(A_TAX_YEAR);

      L_MSG := 'Inserting into tax book transfers table.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      insert into TAX_BOOK_TRANSFERS
      (TRANS_ID, YEAR, START_MONTH, END_MONTH, TAX_ACTIVITY_CODE_ID, UTILITY_ACCOUNT_ID,
       SUB_ACCOUNT_ID, COMPANY_ID, BUS_SEGMENT_ID, IN_SERVICE_YEAR, GL_POSTING_MO_YR, TAX_LOCATION_ID,
       GL_ACCOUNT_ID, TAX_DISTINCTION_ID, CLASS_CODE_ID, CC_TAX_INDICATOR, CC_VALUE, ASSET_ID,
       TRANSFER_GROUP, BOOK_SALE_AMOUNT, AMOUNT)
      select ROWNUM + MAX_ID,
             A_TAX_YEAR,
             A_START_MO,
             A_END_MO,
             TAX_ACTIVITY_CODE_ID,
             UTILITY_ACCOUNT_ID,
             SUB_ACCOUNT_ID,
             COMPANY_ID,
             BUS_SEGMENT_ID,
             IN_SERVICE_YEAR,
             GL_POSTING_MO_YR,
             TAX_LOCATION_ID,
             GL_ACCOUNT_ID,
             TAX_DISTINCTION_ID,
             CLASS_CODE_ID,
             CC_TAX_INDICATOR,
             CC_VALUE,
             ASSET_ID,
             TRANSFER_GROUP,
             BOOK_SALE_AMOUNT,
             AMOUNT
        from (select TAX_ACTIVITY_CODE_ID_FROM TAX_ACTIVITY_CODE_ID,
                     UTILITY_ACCOUNT_ID_FROM UTILITY_ACCOUNT_ID,
                     SUB_ACCOUNT_ID_FROM SUB_ACCOUNT_ID,
                     COMPANY_ID_FROM COMPANY_ID,
                     BUS_SEGMENT_ID_FROM BUS_SEGMENT_ID,
                     IN_SERVICE_YEAR_FROM IN_SERVICE_YEAR,
                     GL_POSTING_MO_YR_FROM GL_POSTING_MO_YR,
                     TAX_LOCATION_ID_FROM TAX_LOCATION_ID,
                     GL_ACCOUNT_ID_FROM GL_ACCOUNT_ID,
                     TAX_DISTINCTION_ID_FROM TAX_DISTINCTION_ID,
                     CLASS_CODE_ID_FROM CLASS_CODE_ID,
                     CC_TAX_INDICATOR_FROM CC_TAX_INDICATOR,
                     CC_VALUE_FROM CC_VALUE,
                     ASSET_ID_FROM ASSET_ID,
                     TRANSFER_GROUP,
                     (-1 * BOOK_SALE_AMOUNT) BOOK_SALE_AMOUNT,
                     (-1 * AMOUNT) AMOUNT
                from TAX_TEMP_BOOK_TRANSFERS_STG
              union all
              select TAX_ACTIVITY_CODE_ID_TO TAX_ACTIVITY_CODE_ID,
                     UTILITY_ACCOUNT_ID_TO,
                     SUB_ACCOUNT_ID_TO,
                     COMPANY_ID_TO,
                     BUS_SEGMENT_ID_TO,
                     IN_SERVICE_YEAR_TO,
                     GL_POSTING_MO_YR_TO,
                     TAX_LOCATION_ID_TO,
                     GL_ACCOUNT_ID_TO,
                     TAX_DISTINCTION_ID_TO,
                     CLASS_CODE_ID_TO,
                     CC_TAX_INDICATOR_TO,
                     CC_VALUE_TO,
                     ASSET_ID_TO ASSET_ID,
                     TRANSFER_GROUP,
                     BOOK_SALE_AMOUNT,
                     AMOUNT
                from TAX_TEMP_BOOK_TRANSFERS_STG),
             (select NVL(max(TRANS_ID), 0) MAX_ID from TAX_BOOK_TRANSFERS);

      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
   end F_POPULATE_TAX_BOOK_XFERS;


   -- This function calls the traceback functions, then calls the process function.
   function F_POWERTAX_XFER_LOAD(A_TAX_YEAR   number,
                                 A_START_MO   number,
                                 A_END_MO     number,
                                 A_DEBUG_FLAG_YN number) return varchar2 is
      L_MSG     varchar2(2000);
      L_PKG_VERSION  varchar2(2000);
      DEBUG_FLAG_BLN boolean;
      L_RTN_CODE     number(22, 0);

   begin

      if L_PROCESS_ID = -1 or L_PROCESS_ID is null then
         P_SETPROCESS();
      end if;

      if A_DEBUG_FLAG_YN = 1 then
         DEBUG_FLAG_BLN := true;
      else
         DEBUG_FLAG_BLN := false;
      end if;
      PKG_PP_LOG.P_START_LOG(L_PROCESS_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE('This log is generated from the PowerTax Transfer Inteface,');
      PKG_PP_LOG.P_WRITE_MESSAGE('from clicking "Load Activity".');
      PKG_PP_LOG.P_WRITE_MESSAGE(' ');
      PKG_PP_LOG.P_WRITE_MESSAGE('Starting...');

      L_PKG_VERSION := F_GET_VERSION();
      PKG_PP_LOG.P_WRITE_MESSAGE('Transfers Package version : '||L_PKG_VERSION);
      PKG_PP_LOG.P_WRITE_MESSAGE('Processing with the following variable definitions:');
      PKG_PP_LOG.P_WRITE_MESSAGE('Tax Year : ' || A_TAX_YEAR);
      PKG_PP_LOG.P_WRITE_MESSAGE('Start Month : ' || A_START_MO);
      PKG_PP_LOG.P_WRITE_MESSAGE('End Month : ' || A_END_MO);
      if DEBUG_FLAG_BLN then
         PKG_PP_LOG.P_WRITE_MESSAGE('Debug Flag : True');
      else
         PKG_PP_LOG.P_WRITE_MESSAGE('Debug Flag : False');
      end if;

      ---- turn off auditing.
      L_RTN_CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      PKG_PP_LOG.P_WRITE_MESSAGE('Turn off auditing return code : ' || L_RTN_CODE);

      PKG_PP_LOG.P_WRITE_MESSAGE('Entering traceback loading function...');
      L_MSG := F_TRACEBACK_LOAD(A_TAX_YEAR, A_START_MO, A_END_MO);
      if L_MSG <> 'OK' then
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE('Function Result : ' || L_MSG);
      PKG_PP_LOG.P_WRITE_MESSAGE('Entering traceback processing function...');
      L_MSG := F_TRACEBACK_PROCESS(A_TAX_YEAR, DEBUG_FLAG_BLN);
      if L_MSG <> 'OK' then
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE('Function Result : ' || L_MSG);
      PKG_PP_LOG.P_WRITE_MESSAGE('Entering tax book transfer populating function...');
      L_MSG := F_POPULATE_TAX_BOOK_XFERS(A_TAX_YEAR, A_START_MO, A_END_MO);
      if L_MSG <> 'OK' then
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE('Function Result : ' || L_MSG);

      ---- turn auditing back on.
      L_RTN_CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'yes');
      PKG_PP_LOG.P_WRITE_MESSAGE('Turn on auditing return code : ' || L_RTN_CODE);

      PKG_PP_LOG.P_WRITE_MESSAGE('Finished...');
      PKG_PP_LOG.P_END_LOG();
      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
   end F_POWERTAX_XFER_LOAD;

end PKG_TAX_INT_XFER;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (18205, 0, 2018, 2, 1, 0, 0, 'C:\BitBucketRepos\Classic_PB\scripts\00_base\packages', 
    'PKG_TAX_INT_XFER.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
