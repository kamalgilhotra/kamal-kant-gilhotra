/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_039519_lease_PKG_LEASE_IMPORT.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------- ----------------------------------------------
|| 10.4.3.0 06/23/2014 Shane Ward
|| 2015.2   06/10/2015 Will Davis
|| 2017.1.0 06/08/2017 Jared Watkins        Add contract currency to the MLA import
|| 2017.1.0 06/22/2017 Jared Watkins        Add contract currency to the Lease Asset import
|| 2017.1.0 06/27/2017 Jared Schwantz       Add ST/LT Deferred Accounts
|| 2017.1.0 08/04/2017 Josh Sandler         Add currency gain/loss and currency gain/loss offset accounts to ILR import
|| 2017.4.0 02/20/2018 Shane "C" Ward		Add IDC ILR Import
|| 2017.4.0 05/30/2018 Alex Healey			Add Purchase/Termination options Import
|| 2017.4.0 06/05/2018 David Levine     	Add Asset UOP
|| 2017.4.0 06/27/2018 Shane "C" Ward		Update Floating Rates Import
|| 2017.4.0.1 08/03/2018 C.Yura		        Add validations for Date incurred to Incentive and IDC imports
|| 2018.1.0.0 10/11/2018 K.Powers           Add impairment accounts to import
|| 2018.2.0.0 01/21/2019 Shane "C" Ward     Add Default Discount Rate Import
|| 2018.1.0.0 02/18/2019 Chris Trigg	    Update import for new default discount rate functionality
||========================================================================================
*/

create or replace package PKG_LEASE_IMPORT as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_IMPORT
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 08/19/2013 K.Peterson     Original Version
   || 2017.1.0 06/08/2017 Jared Watkins  Add contract currency to the MLA
   || 2017.1.0 06/22/2017 Jared Watkins  Add contract currency to the Lease Asset import
   || 2017.1.0 08/04/2017 Josh Sandler   Add currency gain/loss and currency gain/loss offset accounts to ILR import
   || 2017.4.0 02/20/2018 Shane "C" Ward Add IDC ILR Import
   || 2017.4.0 05/30/2018 Alex Healey	 Add Purchase/Termination options Import
   || 2017.4.0 06/05/2018 David Levine   Add Asset UOP
   || 2017.4.0 06/27/2018 Shane "C" Ward Update Floating Rates Import
   || 2017.4.0.1 08/03/2018 C.Yura		 Add validations for Date incurred to Incentive and IDC imports
   || 2018.2.0.0 01/21/2019 Shane "C" Ward     Add Default Discount Rate Import
   || 2018.2.0.0 01/21/2019 Chris Trigg	    Update import for new default discount rate functionality
   ||============================================================================
   */
   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   
   type NUM_ARRAY is table of number(22) index by binary_integer;

   procedure P_INVOICE_ROLLUP;

   procedure P_BUILDILRS(A_ILR_IDS   NUM_ARRAY,
                         A_REVISIONS NUM_ARRAY,
                         A_SEND_JES  number);

function F_IMPORT_INVOICES(A_RUN_ID number) return varchar2;

function F_IMPORT_LESSORS(A_RUN_ID number) return varchar2;

function F_VALIDATE_INVOICES(A_RUN_ID number) return varchar2;

function F_VALIDATE_LESSORS(A_RUN_ID number) return varchar2;

function F_IMPORT_ILRS(A_RUN_ID number) return varchar2;

function F_VALIDATE_ILRS(A_RUN_ID number) return varchar2;

function F_IMPORT_MLAS(A_RUN_ID number) return varchar2;

function F_VALIDATE_MLAS(A_RUN_ID number) return varchar2;

function F_IMPORT_ASSETS(A_RUN_ID number) return varchar2;

function F_VALIDATE_ASSETS(A_RUN_ID number) return varchar2;

function F_IMPORT_COMPONENTS(A_RUN_ID number) return varchar2;

function F_VALIDATE_COMPONENTS(A_RUN_ID number) return varchar2;

function F_IMPORT_INTERIM_RATES(A_RUN_ID number) return varchar2;

function F_VALIDATE_INTERIM_RATES(A_RUN_ID number) return varchar2;

function F_IMPORT_LEASE_FLOAT_RATES(A_RUN_ID number) return varchar2;

function F_VALIDATE_LEASE_FLOAT_RATES(A_RUN_ID number) return varchar2;

function F_IMPORT_DISTRICT_TAX_RATES(A_RUN_ID number) return varchar2;

function F_VALIDATE_DISTRICT_TAX_RATES(A_RUN_ID number) return varchar2;

function F_IMPORT_STATE_TAX_RATES(A_RUN_ID number) return varchar2;

function F_VALIDATE_STATE_TAX_RATES(A_RUN_ID number) return varchar2;

function F_IMPORT_ALLOCATION_ROWS(A_RUN_ID number) return varchar2;

function F_VALIDATE_ALLOCATION_ROWS(A_RUN_ID number) return varchar2;

function F_IMPORT_ASSET_TAXES(A_RUN_ID in number) return varchar2;

function F_VALIDATE_ASSET_TAXES(A_RUN_ID in number) return varchar2;

function F_IMPORT_ILR_RENEWALS(a_run_id NUMBER)  RETURN VARCHAR2;

function F_VALIDATE_ILR_RENEWALS(a_run_id NUMBER)  RETURN VARCHAR2;

function F_IMPORT_ILR_IDC(a_run_id NUMBER)	RETURN VARCHAR2;

function F_VALIDATE_ILR_IDC(a_run_id NUMBER) RETURN VARCHAR2;

function F_IMPORT_ILR_INCENTIVE(a_run_id NUMBER)	RETURN VARCHAR2;

function F_VALIDATE_ILR_INCENTIVE(a_run_id NUMBER) RETURN VARCHAR2;

function F_IMPORT_ASSET_UOP(a_run_id NUMBER) RETURN VARCHAR2;

function F_VALIDATE_ASSET_UOP(a_run_id NUMBER) RETURN VARCHAR2;

function F_IMPORT_ILR_PURCHASE(a_run_id NUMBER) RETURN VARCHAR2;

function F_VALIDATE_ILR_PURCHASE(a_run_id NUMBER) RETURN VARCHAR2;

function F_IMPORT_ILR_TERMINATION(a_run_id NUMBER) RETURN VARCHAR2;

function F_VALIDATE_ILR_TERMINATION(a_run_id NUMBER) RETURN VARCHAR2;

function F_IMPORT_DEFAULT_DISC_RATE(a_run_id NUMBER) RETURN VARCHAR2;

function F_VALIDATE_DEFAULT_DISC_RATE(a_run_id NUMBER) RETURN VARCHAR2;

function F_IMPORT_ILR_REVISION(a_run_id NUMBER) RETURN VARCHAR2;

function F_VALIDATE_ILR_REVISION(a_run_id NUMBER) RETURN VARCHAR2;

end PKG_LEASE_IMPORT;
/
create or replace package body PKG_LEASE_IMPORT as

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_IMPORT
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 08/19/2013 K.Peterson    Original Version
   ||============================================================================
   */

   --DECLARE VARIABLES HERE
   type INVOICE_IMPORT_TYPE is table of LS_IMPORT_INVOICE%rowtype index by pls_integer;
   type INVOICE_IMPORT_HDR is record(
      VENDOR_ID        LS_IMPORT_INVOICE.VENDOR_ID%type,
      COMPANY_ID       LS_IMPORT_INVOICE.COMPANY_ID%type,
      LS_ASSET_ID      LS_INVOICE.LS_ASSET_ID%type,
      ILR_ID           LS_INVOICE.ILR_ID%type,
      LEASE_ID         LS_IMPORT_INVOICE.LEASE_ID%type,
      GL_POSTING_MO_YR LS_INVOICE.GL_POSTING_MO_YR%type,
      INVOICE_NUMBER   LS_INVOICE.INVOICE_NUMBER%type);
   type INVOICE_IMPORT_LINE is record(
      INVOICE_ID          LS_INVOICE.INVOICE_ID%type,
      INVOICE_LINE_NUMBER LS_INVOICE_LINE.INVOICE_LINE_NUMBER%type,
      PAYMENT_TYPE_ID     LS_IMPORT_INVOICE.PAYMENT_TYPE_ID%type,
      GL_POSTING_MO_YR    LS_INVOICE_LINE.GL_POSTING_MO_YR%type,
      LS_ASSET_ID         LS_IMPORT_INVOICE.LS_ASSET_ID%type,
      AMOUNT              LS_IMPORT_INVOICE.AMOUNT%type,
      DESCRIPTION         LS_IMPORT_INVOICE.DESCRIPTION%type,
      NOTES               LS_IMPORT_INVOICE.NOTES%type);
   type INVOICE_IMPORT_HDR_TABLE is table of INVOICE_IMPORT_HDR index by pls_integer;
   type INVOICE_LINE_TABLE is table of INVOICE_IMPORT_LINE index by pls_integer;
   type ASSET_IMPORT_TABLE is table of LS_IMPORT_ASSET%rowtype index by pls_integer;
   type COMPONENT_IMPORT_TABLE is table of LS_IMPORT_COMPONENT%rowtype index by pls_integer;
   type MLA_COMPANY_LINE is record(
      LEASE_ID       LS_LEASE_COMPANY.LEASE_ID%type,
      COMPANY_ID     LS_LEASE_COMPANY.COMPANY_ID%type,
      MAX_LEASE_LINE LS_LEASE_COMPANY.MAX_LEASE_LINE%type,
      VENDOR_ID      LS_LEASE_VENDOR.VENDOR_ID%type,
      PAYMENT_PCT    LS_LEASE_VENDOR.PAYMENT_PCT%type);
   type MLA_COMPANY_TABLE is table of MLA_COMPANY_LINE index by pls_integer;

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   procedure P_INVOICE_ROLLUP is
      L_INDEX number;
      type INVOICE_LINES_TABLE is table of LS_INVOICE_LINE%rowtype index by pls_integer;
      INVOICE_LINES INVOICE_LINES_TABLE;
   begin
      update LS_INVOICE
         set INVOICE_AMOUNT = 0, INVOICE_INTEREST = 0, INVOICE_EXECUTORY = 0, INVOICE_CONTINGENT = 0;

      select * bulk collect into INVOICE_LINES from LS_INVOICE_LINE;

      forall L_INDEX in indices of INVOICE_LINES
         update LS_INVOICE
            set INVOICE_AMOUNT = INVOICE_AMOUNT + INVOICE_LINES(L_INDEX).AMOUNT
          where INVOICE_ID = INVOICE_LINES(L_INDEX).INVOICE_ID;

      forall L_INDEX in indices of INVOICE_LINES
         update LS_INVOICE
            set INVOICE_INTEREST = INVOICE_INTEREST + INVOICE_LINES(L_INDEX).AMOUNT
          where INVOICE_ID = INVOICE_LINES(L_INDEX).INVOICE_ID
            and INVOICE_LINES(L_INDEX).PAYMENT_TYPE_ID in (2, 5);

      forall L_INDEX in indices of INVOICE_LINES
         update LS_INVOICE
            set INVOICE_EXECUTORY = INVOICE_EXECUTORY + INVOICE_LINES(L_INDEX).AMOUNT
          where INVOICE_ID = INVOICE_LINES(L_INDEX).INVOICE_ID
            and INVOICE_LINES(L_INDEX).PAYMENT_TYPE_ID in (3, 6);

      forall L_INDEX in indices of INVOICE_LINES
         update LS_INVOICE
            set INVOICE_CONTINGENT = INVOICE_CONTINGENT + INVOICE_LINES(L_INDEX).AMOUNT
          where INVOICE_ID = INVOICE_LINES(L_INDEX).INVOICE_ID
            and INVOICE_LINES(L_INDEX).PAYMENT_TYPE_ID in (4, 7);

   end P_INVOICE_ROLLUP;

   --**************************************************************************
   --                            P_BUILDILRS
   --**************************************************************************
   --
   procedure P_BUILDILRS(A_ILR_IDS   NUM_ARRAY,
                         A_REVISIONS NUM_ARRAY,
                         A_SEND_JES  number) is
      RTN      varchar2(4000);
      RTN_NUM  number(22, 0);
      L_STATUS varchar2(2000);
      L_IRR    number(22,4);
	  L_SCHEDULE_CHECK number(22,0);

   begin

      --Turn off the JEs if we don't want them
      if A_SEND_JES = 0 then
         PKG_LEASE_ASSET_POST.G_SEND_JES := false;
      end if;

      RTN := PKG_LEASE_SCHEDULE.F_MASS_PROCESS_ILRS(A_ILR_IDS, A_REVISIONS);
      if RTN <> 'OK' then
		rollback;
         return;
	  else
		--Succesfully ran build schedules. go ahead and commit so users dont lose processing time
		commit;
      end if;

	  --Run Validations on the ILRs after building schedules
	  for i in A_ILR_IDS.first .. A_ILR_IDS.last loop	  
	  
		  --Check for schedule being built (only revision 1 can be built right now
		  select count(1)
		  into l_schedule_check
		  from ls_ilr_schedule
		  where ilr_id = a_ilr_ids(i)
		  and revision = a_revisions(i);
		  
		  if l_schedule_check = 0 or l_schedule_check is null then
			RAISE_APPLICATION_ERROR(-20000, 'Schedule was not successfully built for ILR ID, all schedules in selection must build correctly before Approving ILRs (ILR ID: '||A_ILR_IDS(i)||' Revision: '||a_ilr_ids(i)||')');
			return;
		  end if;
		  
	  --CJS 4/16/15
	  --Add component interim interest if the lease group has components
	    select REQUIRE_COMPONENTS
		  into RTN_NUM
		  from LS_LEASE_GROUP LG, LS_LEASE LL, LS_ILR ILR
		 where ILR.ILR_ID = A_ILR_IDS(i)
		   and ILR.LEASE_ID = LL.LEASE_ID
		   and LL.LEASE_GROUP_ID = LG.LEASE_GROUP_ID;

		   if RTN_NUM = 1 then
			  select count(1)
			    into RTN_NUM
				from LS_COMPONENT LC, LS_ASSET LA
			   where LA.ILR_ID = A_ILR_IDS(i)
			     and LA.LS_ASSET_ID = LC.LS_ASSET_ID;

			  if RTN_NUM = 0 then
			     rollback;
				 RAISE_APPLICATION_ERROR(-20000, 'Components haven''t been imported yet for this ILR ID and are required: '||A_ILR_IDS(i));
				 return;
			  end if;

		      PKG_LEASE_ILR.P_CALC_II(A_ILR_IDS(i), A_REVISIONS(i));

		   end if;

		   RTN_NUM:=null;

	  end loop;

	  --Skip workflow objects. make sure workflow, and workflow_detail end up with right statuses
      for I in 1 .. A_ILR_IDS.COUNT
      loop

         L_STATUS := 'Sending ILR for approval';
         RTN_NUM  := PKG_LEASE_CALC.F_SEND_ILR_NO_COMMIT(A_ILR_IDS(I), A_REVISIONS(I), L_STATUS);
         if RTN_NUM <> 1 then
            rollback;
            RAISE_APPLICATION_ERROR(-20000, 'Error Sending: ' || L_STATUS);
            return;
         end if;

         L_STATUS := 'Auto Approving ILR';
         RTN_NUM  := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(A_ILR_IDS(I),
                                                            A_REVISIONS(I),
                                                            L_STATUS,
                                                            false);
         if RTN_NUM <> 1 then
            rollback;
            RAISE_APPLICATION_ERROR(-20000, 'Error Approving: ' || L_STATUS);
            return;
         end if;

		 --Update workflows to be Auto-Approved status. All the workflows for these ILRs should have the current Auto Approve Workflow in the db
         update WORKFLOW
            set DATE_SENT = sysdate, DATE_APPROVED = sysdate, user_sent = user, approval_status_id = 3
          where ID_FIELD1 = A_ILR_IDS(I)
            and ID_FIELD2 = A_REVISIONS(I)
            and SUBSYSTEM = 'lessee_ilr_approval';

         update WORKFLOW_DETAIL
            set DATE_APPROVED = sysdate, approval_status_id = 3
			where WORKFLOW_ID IN (select WORKFLOW_ID from WORKFLOW
									where ID_FIELD1 = A_ILR_IDS(I)
									and ID_FIELD2 = A_REVISIONS(I)
									and SUBSYSTEM = 'lessee_ilr_approval');

      end loop;

      commit;

      --Turn the JEs back on
      PKG_LEASE_ASSET_POST.G_SEND_JES := true;

      /* CJS 2/27/15 Moving p_end_log here */
      PKG_PP_LOG.P_END_LOG();
   end P_BUILDILRS;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   --##########################################################################
   --                            INVOICES
   --##########################################################################
   function F_IMPORT_INVOICES(A_RUN_ID in number) return varchar2 is
      L_INVOICE_LINE             INVOICE_LINE_TABLE;
      L_INDEX                    number;
      L_INVOICE_IMPORT_HDR_TABLE INVOICE_IMPORT_HDR_TABLE;
      L_MSG                      varchar2(32000);
      L_RECON_LEVEL              number;
   begin
      L_MSG := 'Collecting unique vendor/company/asset/ilr/lease/month combinations.';
      select distinct VENDOR_ID,
                      COMPANY_ID,
                      nvl(LS_ASSET_ID, -1),
                      nvl(ILR_ID, -1),
                      LEASE_ID,
                      TO_DATE(GL_POSTING_MO_YR, 'yyyymm'),
                      INVOICE_NUMBER bulk collect
        into L_INVOICE_IMPORT_HDR_TABLE
        from LS_IMPORT_INVOICE
       where IMPORT_RUN_ID = A_RUN_ID;

      L_MSG := 'Updating dummy headers with reconcile type information.';
      for L_INDEX in 1..L_INVOICE_IMPORT_HDR_TABLE.count loop
         --Get reconcile type for the lease each invoice is associated with
         select LS_RECONCILE_TYPE_ID into L_RECON_LEVEL from LS_LEASE_OPTIONS where LEASE_ID = L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).LEASE_ID and REVISION = (select CURRENT_REVISION from LS_LEASE where LEASE_ID = L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).LEASE_ID);
         if L_RECON_LEVEL = 1 then
            --Recon level is asset. We need to update header with ILR and MLA information.
            select a.ilr_id, i.lease_id into L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).ILR_ID, L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).LEASE_ID
              from ls_ilr i, ls_asset a where L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).LS_ASSET_ID = a.ls_asset_id and a.ilr_id = i.ilr_id;
         elsif L_RECON_LEVEL = 2 then
            --Recon level is ilr. We need to update header with MLA information, and put -1 for asset
            select lease_id into L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).LEASE_ID
              from ls_ilr where L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).ILR_ID = ilr_id;
            L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).LS_ASSET_ID := -1;
         else
            --Recon level is MLA. We need to put -1 for asset and ILR.
            L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).LS_ASSET_ID := -1;
            L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).ILR_ID := -1;
         end if;
      end loop;

      L_MSG := 'Inserting dummy invoices into LS_INVOICE';
      forall L_INDEX in indices of L_INVOICE_IMPORT_HDR_TABLE
         insert into LS_INVOICE
            (INVOICE_ID, COMPANY_ID, LS_ASSET_ID, ILR_ID, LEASE_ID, GL_POSTING_MO_YR, VENDOR_ID, INVOICE_NUMBER)
            select LS_INVOICE_SEQ.NEXTVAL,
                   L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).COMPANY_ID,
                   L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).LS_ASSET_ID,
                   L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).ILR_ID,
                   L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).LEASE_ID,
                   L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).GL_POSTING_MO_YR,
                   L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).VENDOR_ID,
                   L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).INVOICE_NUMBER
              from DUAL;

      L_MSG := 'Collecting for Invoice lines';
      select LI.INVOICE_ID,
             ROW_NUMBER() OVER(partition by TO_DATE(LII.GL_POSTING_MO_YR, 'yyyymm'), LII.VENDOR_ID, LII.COMPANY_ID, nvl(LII.LS_ASSET_ID,-1), nvl(LII.ILR_ID,-1), LII.LEASE_ID order by TO_DATE(LII.GL_POSTING_MO_YR, 'yyyymm')),
             LII.PAYMENT_TYPE_ID,
             TO_DATE(LII.GL_POSTING_MO_YR, 'yyyymm'),
             LII.LS_ASSET_ID,
             LII.AMOUNT,
             LII.DESCRIPTION,
             LII.NOTES bulk collect
        into L_INVOICE_LINE
        from LS_IMPORT_INVOICE LII, LS_INVOICE LI
       where LII.IMPORT_RUN_ID = A_RUN_ID
         and LII.VENDOR_ID = LI.VENDOR_ID
         and LII.COMPANY_ID = LI.COMPANY_ID
         and TO_DATE(LII.GL_POSTING_MO_YR, 'yyyymm') = LI.GL_POSTING_MO_YR
         and case (select LO.LS_RECONCILE_TYPE_ID from LS_LEASE_OPTIONS LO where LO.LEASE_ID = LI.LEASE_ID and LO.REVISION = (select L.CURRENT_REVISION from LS_LEASE L where L.LEASE_ID = LI.LEASE_ID))
             when 1 then LII.LS_ASSET_ID
             when 2 then -1
             else -1
             end = LI.LS_ASSET_ID
         and case (select LO.LS_RECONCILE_TYPE_ID from LS_LEASE_OPTIONS LO where LO.LEASE_ID = LI.LEASE_ID and LO.REVISION = (select L.CURRENT_REVISION from LS_LEASE L where L.LEASE_ID = LI.LEASE_ID))
             when 1 then (select A.ILR_ID from LS_ASSET A where A.LS_ASSET_ID = LII.LS_ASSET_ID)
             when 2 then LII.ILR_ID
             else -1
             end = LI.ILR_ID
         and LII.LEASE_ID = LI.LEASE_ID;

      L_MSG := 'Inserting into LS_INVOICE_LINE';
      forall L_INDEX in indices of L_INVOICE_LINE
         insert into LS_INVOICE_LINE
            (INVOICE_ID, INVOICE_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, GL_POSTING_MO_YR,
             AMOUNT, DESCRIPTION, NOTES)
            select L_INVOICE_LINE(L_INDEX).INVOICE_ID,
                   L_INVOICE_LINE(L_INDEX).INVOICE_LINE_NUMBER,
                   L_INVOICE_LINE(L_INDEX).PAYMENT_TYPE_ID,
                   L_INVOICE_LINE(L_INDEX).LS_ASSET_ID,
                   L_INVOICE_LINE(L_INDEX).GL_POSTING_MO_YR,
                   L_INVOICE_LINE(L_INDEX).AMOUNT,
                   L_INVOICE_LINE(L_INDEX).DESCRIPTION,
                   L_INVOICE_LINE(L_INDEX).NOTES
              from DUAL;

      P_INVOICE_ROLLUP;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_INVOICES;

   function F_VALIDATE_INVOICES(A_RUN_ID in number) return varchar2 is
      L_INDEX                    number;
      L_INVOICE_IMPORT_HDR_TABLE INVOICE_IMPORT_HDR_TABLE;
      L_MSG                      varchar2(2000);
      NUM_UPDATED                number;
      L_RECON_LEVEL              number;
   begin
	  
	  update LS_IMPORT_INVOICE
         set ERROR_MESSAGE =  'Invoice already exists for this company/month/vendor/asset combination and Recon Level = Asset.'
         where exists (
						select 1
						From LS_LEASE_OPTIONS, LS_ILR, LS_ASSET, LS_INVOICE
						where LS_IMPORT_INVOICE.IMPORT_RUN_ID = A_RUN_ID
						and nvl(LS_IMPORT_INVOICE.LS_ASSET_ID,-1) <> -1
						and LS_IMPORT_INVOICE.LEASE_ID = LS_LEASE_OPTIONS.LEASE_ID
						and LS_LEASE_OPTIONS.REVISION =(select CURRENT_REVISION from LS_LEASE where LS_LEASE.LEASE_ID = LS_IMPORT_INVOICE.LEASE_ID)
						and LS_LEASE_OPTIONS.LS_RECONCILE_TYPE_ID = 1
						and LS_IMPORT_INVOICE.LS_ASSET_ID = LS_ASSET.LS_ASSET_ID
						and LS_ASSET.ILR_ID = LS_ILR.ILR_ID
						and LS_INVOICE.GL_POSTING_MO_YR = TO_DATE(LS_IMPORT_INVOICE.GL_POSTING_MO_YR, 'yyyymm')
						and LS_INVOICE.COMPANY_ID = LS_IMPORT_INVOICE.COMPANY_ID
						and LS_INVOICE.LS_ASSET_ID = LS_IMPORT_INVOICE.LS_ASSET_ID
						and LS_INVOICE.ILR_ID = LS_ILR.ILR_ID
						and LS_INVOICE.LEASE_ID = LS_ILR.LEASE_ID
						and LS_INVOICE.VENDOR_ID = LS_IMPORT_INVOICE.VENDOR_ID);


      update LS_IMPORT_INVOICE
         set ERROR_MESSAGE =  'Invoice already exists for this company/month/vendor/ilr combination and Recon Level = ILR.'
         where exists (
						select 1
						From LS_LEASE_OPTIONS, LS_ILR, LS_INVOICE
						where LS_IMPORT_INVOICE.IMPORT_RUN_ID = A_RUN_ID
						and nvl(LS_IMPORT_INVOICE.ILR_ID,-1) <> -1
						and LS_IMPORT_INVOICE.LEASE_ID = LS_LEASE_OPTIONS.LEASE_ID
						and LS_LEASE_OPTIONS.REVISION =(select CURRENT_REVISION from LS_LEASE where LS_LEASE.LEASE_ID = LS_IMPORT_INVOICE.LEASE_ID)
						and LS_LEASE_OPTIONS.LS_RECONCILE_TYPE_ID = 2
						and LS_IMPORT_INVOICE.ILR_ID = LS_ILR.ILR_ID
						and LS_INVOICE.GL_POSTING_MO_YR = TO_DATE(LS_IMPORT_INVOICE.GL_POSTING_MO_YR, 'yyyymm')
						and LS_INVOICE.COMPANY_ID = LS_IMPORT_INVOICE.COMPANY_ID
						and LS_INVOICE.LS_ASSET_ID = -1
						and LS_INVOICE.ILR_ID = LS_ILR.ILR_ID
						and LS_INVOICE.LEASE_ID = LS_ILR.LEASE_ID
						and LS_INVOICE.VENDOR_ID = LS_IMPORT_INVOICE.VENDOR_ID);


      update LS_IMPORT_INVOICE
	     set ERROR_MESSAGE =  'Invoice already exists for this company/month/vendor/MLA combination and Recon Level = MLA.'
         where exists (
						select 1
						From LS_LEASE_OPTIONS, LS_INVOICE
						where LS_IMPORT_INVOICE.IMPORT_RUN_ID = A_RUN_ID
						and LS_IMPORT_INVOICE.LEASE_ID = LS_LEASE_OPTIONS.LEASE_ID
						and LS_LEASE_OPTIONS.REVISION =(select CURRENT_REVISION from LS_LEASE where LS_LEASE.LEASE_ID = LS_IMPORT_INVOICE.LEASE_ID)
						and LS_LEASE_OPTIONS.LS_RECONCILE_TYPE_ID = 3
						and LS_INVOICE.GL_POSTING_MO_YR = TO_DATE(LS_IMPORT_INVOICE.GL_POSTING_MO_YR, 'yyyymm')
						and LS_INVOICE.COMPANY_ID = LS_IMPORT_INVOICE.COMPANY_ID
						and LS_INVOICE.LS_ASSET_ID = -1
						and LS_INVOICE.ILR_ID = -1
						and LS_INVOICE.LEASE_ID = LS_IMPORT_INVOICE.LEASE_ID
						and LS_INVOICE.VENDOR_ID = LS_IMPORT_INVOICE.VENDOR_ID);

      update LS_IMPORT_INVOICE A
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Invoice Number must be the same for this company/month/vendor/(asset, ilr, lease) combination.'
       where IMPORT_RUN_ID = A_RUN_ID
         and exists
             (select 1
                from LS_IMPORT_INVOICE B
               where B.GL_POSTING_MO_YR = A.GL_POSTING_MO_YR
                 and B.COMPANY_ID = A.COMPANY_ID
                 and nvl(B.LS_ASSET_ID,-1) = nvl(A.LS_ASSET_ID,-1)
                 and nvl(B.ILR_ID,-1) = nvl(A.ILR_ID,-1)
                 and B.LEASE_ID = A.LEASE_ID
                 and B.VENDOR_ID = A.VENDOR_ID
                 and NVL(B.INVOICE_NUMBER, '*~*~INVALIDNUMBER~*~*') <> NVL(A.INVOICE_NUMBER, '*~*~INVALIDNUMBER~*~*'));


      update LS_IMPORT_INVOICE
         set ERROR_MESSAGE = ERROR_MESSAGE || ' No ILR exists for the given asset.'
       where IMPORT_RUN_ID = A_RUN_ID
         and nvl(LS_ASSET_ID, -1) <> -1
         and not exists (select ilr_id from ls_asset a where a.ls_asset_id = LS_ASSET_ID);

      update LS_IMPORT_INVOICE ii
         set ii.ERROR_MESSAGE = ii.ERROR_MESSAGE || ' The given asset and Lease are not associated.'
       where ii.IMPORT_RUN_ID = A_RUN_ID
         and nvl(ii.LS_ASSET_ID, -1) <> -1
         and ii.lease_id <> (select distinct i.lease_id from ls_ilr i, ls_asset a where ii.ls_asset_id = a.ls_asset_id and a.ilr_id = i.ilr_id);

      update LS_IMPORT_INVOICE ii
         set ii.ERROR_MESSAGE = ii.ERROR_MESSAGE || ' The given ILR and Lease are not associated.'
       where ii.IMPORT_RUN_ID = A_RUN_ID
         and nvl(ii.ILR_ID, -1) <> -1
         and ii.lease_id <> (select distinct i.lease_id from ls_ilr i where ii.ilr_id = i.ilr_id);

      update LS_IMPORT_INVOICE ii
         set ii.ERROR_MESSAGE = ii.ERROR_MESSAGE || ' Recon level for the given lease is ASSET, but no asset is given.'
       where ii.IMPORT_RUN_ID = A_RUN_ID
         and nvl(ii.ls_asset_id, -1) = -1
         and (select lo.LS_RECONCILE_TYPE_ID
                from LS_LEASE_OPTIONS lo
               where lo.LEASE_ID = ii.LEASE_ID
                 and lo.REVISION = (select l.CURRENT_REVISION from LS_LEASE l where l.LEASE_ID = ii.LEASE_ID)) = 1;

      update LS_IMPORT_INVOICE ii
         set ii.ERROR_MESSAGE = ii.ERROR_MESSAGE || ' Recon level for the given lease is ILR, but no ILR is given.'
       where ii.IMPORT_RUN_ID = A_RUN_ID
         and nvl(ii.ilr_id, -1) = -1
         and (select lo.LS_RECONCILE_TYPE_ID
                from LS_LEASE_OPTIONS lo
               where lo.LEASE_ID = ii.LEASE_ID
                 and lo.REVISION = (select l.CURRENT_REVISION from LS_LEASE l where l.LEASE_ID = ii.LEASE_ID)) = 2;


      update ls_import_invoice ii
      set ii.error_message = ii.error_message || ' Payments are already approved for the Invoice Month for this company.'
      where ii.IMPORT_RUN_ID = A_RUN_ID and
	  EXISTS (SELECT 1
              FROM ls_process_control lpc
              where lpc.company_id = ii.company_id
              AND lpc.payment_approved is NOT null
              AND lpc.gl_posting_mo_yr = to_date(ii.gl_posting_mo_yr,'yyyymm'));

      update ls_import_invoice ii
      set ii.error_message = ii.error_message || ' Error: This invoice is associated with a lease that auto-generates invoices'
      where ii.IMPORT_RUN_ID = A_RUN_ID and exists (select 1
                    from ls_lease ll, ls_lease_options llo
                    where ll.lease_id = llo.lease_id
                      and ll.current_revision = llo.revision
                      and ii.lease_id = ll.lease_id
                      and llo.auto_generate_invoices = 1);
					  
	  update LS_IMPORT_INVOICE ii
      set ii.ERROR_MESSAGE = ii.ERROR_MESSAGE || ' Recon level for the given lease is ILR, but asset is provided in import. Remove asset from file or change the Recon Level on MLA.'
      where ii.IMPORT_RUN_ID = A_RUN_ID
					and nvl(ii.LS_ASSET_ID, -1) <> -1
					and (select lo.LS_RECONCILE_TYPE_ID
							from LS_LEASE_OPTIONS lo
							where lo.LEASE_ID = ii.LEASE_ID
							and lo.REVISION = (select l.CURRENT_REVISION from LS_LEASE l where l.LEASE_ID = ii.LEASE_ID)) = 2;

	  update LS_IMPORT_INVOICE ii
      set ii.ERROR_MESSAGE = ii.ERROR_MESSAGE || ' Recon level for the given lease is MLA, but asset and/or ILR is provided in import. Remove asset/ILR from file or change the Recon Level on MLA.'
      where ii.IMPORT_RUN_ID = A_RUN_ID
					and (nvl(ii.LS_ASSET_ID, -1) <> -1 or nvl(ii.ILR_ID, -1) <> -1)
					and (select lo.LS_RECONCILE_TYPE_ID
							from LS_LEASE_OPTIONS lo
							where lo.LEASE_ID = ii.LEASE_ID
							and lo.REVISION = (select l.CURRENT_REVISION from LS_LEASE l where l.LEASE_ID = ii.LEASE_ID)) = 3;
       

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_INVOICES;
   --##########################################################################
   --                            LESSORS
   --##########################################################################
   function F_IMPORT_LESSORS(A_RUN_ID in number) return varchar2 is
      type LESSOR_IMPORT_TABLE is table of LS_IMPORT_LESSOR%rowtype index by pls_integer;
      L_LESSOR_IMPORT LESSOR_IMPORT_TABLE;
      L_INDEX         number;
      L_MSG           varchar2(32000);
   begin

      --here we're populating our PK for the lessors. If the previous row has a different unique identifier than the current
      --row we give it a new id, else we give it the previous row's lessor_id but we make it negative so we don't attempt to insert
      --on that row
      update LS_IMPORT_LESSOR
         set LESSOR_ID = LS_LESSOR_SEQ.NEXTVAL
       where LINE_ID in (select LINE_ID
                           from (select LINE_ID,
                                        ROW_NUMBER() OVER(partition by UNIQUE_LESSOR_IDENTIFIER order by UNIQUE_LESSOR_IDENTIFIER, LINE_ID) NUM
                                   from LS_IMPORT_LESSOR
                                  where IMPORT_RUN_ID = A_RUN_ID)
                          where NUM = 1)
         and IMPORT_RUN_ID = A_RUN_ID;

      update LS_IMPORT_LESSOR A
         set A.LESSOR_ID =
              (select -B.LESSOR_ID
                 from LS_IMPORT_LESSOR B
                where A.UNIQUE_LESSOR_IDENTIFIER = B.UNIQUE_LESSOR_IDENTIFIER
                  and B.LESSOR_ID is not null
                  and IMPORT_RUN_ID = A_RUN_ID)
       where A.LESSOR_ID is null
         and IMPORT_RUN_ID = A_RUN_ID;

      L_MSG := 'Collecting for lessor insert';
      select * bulk collect
        into L_LESSOR_IMPORT
        from LS_IMPORT_LESSOR
       where IMPORT_RUN_ID = A_RUN_ID
         and NVL(LESSOR_ID, -1) > 0;

      L_MSG := 'Inserting Lessors';
      forall L_INDEX in indices of L_LESSOR_IMPORT
         insert into LS_LESSOR
            (LESSOR_ID, DESCRIPTION, LONG_DESCRIPTION, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, ZIP,
             POSTAL, CITY, COUNTY_ID, STATE_ID, COUNTRY_ID, PHONE_NUMBER, EXTENSION, FAX_NUMBER,
             EXTERNAL_LESSOR_NUMBER, SITE_CODE, STATUS_CODE_ID, MULTI_VENDOR_YN_SW, LEASE_GROUP_ID)
            (select L_LESSOR_IMPORT(L_INDEX).LESSOR_ID,
                    L_LESSOR_IMPORT(L_INDEX).DESCRIPTION,
                    L_LESSOR_IMPORT(L_INDEX).LONG_DESCRIPTION,
                    L_LESSOR_IMPORT(L_INDEX).ADDRESS1,
                    L_LESSOR_IMPORT(L_INDEX).ADDRESS2,
                    L_LESSOR_IMPORT(L_INDEX).ADDRESS3,
                    L_LESSOR_IMPORT(L_INDEX).ADDRESS4,
                    L_LESSOR_IMPORT(L_INDEX).ZIP,
                    L_LESSOR_IMPORT(L_INDEX).POSTAL,
                    L_LESSOR_IMPORT(L_INDEX).CITY,
                    L_LESSOR_IMPORT(L_INDEX).COUNTY_ID,
                    L_LESSOR_IMPORT(L_INDEX).STATE_ID,
                    L_LESSOR_IMPORT(L_INDEX).COUNTRY_ID,
                    L_LESSOR_IMPORT(L_INDEX).PHONE_NUMBER,
                    L_LESSOR_IMPORT(L_INDEX).EXTENSION,
                    L_LESSOR_IMPORT(L_INDEX).FAX_NUMBER,
                    L_LESSOR_IMPORT(L_INDEX).EXTERNAL_LESSOR_NUMBER,
                    L_LESSOR_IMPORT(L_INDEX).SITE_CODE,
                    1,
                    L_LESSOR_IMPORT(L_INDEX).MULTI_VENDOR_YN_SW,
                    L_LESSOR_IMPORT(L_INDEX).LEASE_GROUP_ID
               from DUAL);

      update LS_IMPORT_LESSOR set VENDOR_ID = LS_VENDOR_SEQ.NEXTVAL where IMPORT_RUN_ID = A_RUN_ID;

      select * bulk collect
        into L_LESSOR_IMPORT
        from LS_IMPORT_LESSOR
       where IMPORT_RUN_ID = A_RUN_ID;

      forall L_INDEX in indices of L_LESSOR_IMPORT
         insert into LS_VENDOR
            (VENDOR_ID, LESSOR_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_VENDOR_NUMBER,
             REMIT_ADDR, STATUS_CODE_ID, PRIMARY)
            select L_LESSOR_IMPORT(L_INDEX).VENDOR_ID,
                   ABS(L_LESSOR_IMPORT(L_INDEX).LESSOR_ID),
                   L_LESSOR_IMPORT(L_INDEX).VENDOR_DESCRIPTION,
                   L_LESSOR_IMPORT(L_INDEX).VENDOR_LONG_DESCRIPTION,
                   L_LESSOR_IMPORT(L_INDEX).EXTERNAL_VENDOR_NUMBER,
                   L_LESSOR_IMPORT(L_INDEX).REMIT_ADDR,
                   1,
                   L_LESSOR_IMPORT(L_INDEX).PRIMARY
              from DUAL;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_LESSORS;

   function F_VALIDATE_LESSORS(A_RUN_ID in number) return varchar2 is
      L_MSG varchar2(2000);
   begin
      --check for duplicate lessor descriptions
      update LS_IMPORT_LESSOR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate lessor description.'
       where IMPORT_RUN_ID = A_RUN_ID
         and DESCRIPTION in
             (select DESCRIPTION
                from (select DESCRIPTION, count(*) as MY_COUNT
                        from (select DESCRIPTION
                                from LS_LESSOR
                              union all
                              select DESCRIPTION
                                from (select distinct UNIQUE_LESSOR_IDENTIFIER, DESCRIPTION
                                        from LS_IMPORT_LESSOR
                                       where IMPORT_RUN_ID = A_RUN_ID))
                       group by DESCRIPTION)
               where MY_COUNT > 1);

      --check for duplicate external_lessor_number
      update LS_IMPORT_LESSOR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate external lessor number.'
       where IMPORT_RUN_ID = A_RUN_ID
         and EXTERNAL_LESSOR_NUMBER in
             (select EXTERNAL_LESSOR_NUMBER
                from (select EXTERNAL_LESSOR_NUMBER, count(*) as MY_COUNT
                        from (select EXTERNAL_LESSOR_NUMBER
                                from LS_LESSOR
                              union all
                              select EXTERNAL_LESSOR_NUMBER
                                from (select distinct UNIQUE_LESSOR_IDENTIFIER, EXTERNAL_LESSOR_NUMBER
                                        from LS_IMPORT_LESSOR
                                       where IMPORT_RUN_ID = A_RUN_ID))
                       group by EXTERNAL_LESSOR_NUMBER)
               where MY_COUNT > 1);

      --check for duplicate vendor_descriptions
      update LS_IMPORT_LESSOR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate vendor description.'
       where IMPORT_RUN_ID = A_RUN_ID
         and VENDOR_DESCRIPTION in (select DESCRIPTION as VENDOR_DESCRIPTION
                                      from (select DESCRIPTION, count(*) as MY_COUNT
                                              from (select DESCRIPTION
                                                      from LS_VENDOR
                                                    union all
                                                    select VENDOR_DESCRIPTION as DESCRIPTION
                                                      from LS_IMPORT_LESSOR
                                                     where IMPORT_RUN_ID = A_RUN_ID)
                                             group by DESCRIPTION)
                                     where MY_COUNT > 1);

      --check that only one vendor is primary
      update LS_IMPORT_LESSOR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Exactly one vendor must be primary for each lessor.'
       where IMPORT_RUN_ID = A_RUN_ID
         and UNIQUE_LESSOR_IDENTIFIER in (select UNIQUE_LESSOR_IDENTIFIER
                                            from (select UNIQUE_LESSOR_IDENTIFIER, sum(nvl(PRIMARY,0)) SUMS
                                                    from LS_IMPORT_LESSOR
                                                   where IMPORT_RUN_ID = A_RUN_ID
                                                   group by UNIQUE_LESSOR_IDENTIFIER)
                                           where SUMS <> 1);

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_LESSORS;

   --##########################################################################
   --                            MLAS
   --##########################################################################
   function F_IMPORT_MLAS(A_RUN_ID in number) return varchar2 is
      L_MSG varchar2(32000);
      type MLA_IMPORT_TABLE is table of LS_IMPORT_LEASE%rowtype index by pls_integer;
      L_INDEX            number;
      MLA_COMPANY        MLA_COMPANY_TABLE;
      L_MLA_IMPORT_TABLE MLA_IMPORT_TABLE;
      L_CC_SQL           clob;
      CC_NEEDED          boolean;
      RTN                number;
   begin

      update LS_IMPORT_LEASE
         set LEASE_ID = LS_LEASE_SEQ.NEXTVAL
       where LINE_ID in (select LINE_ID
                           from (select LINE_ID,
                                        ROW_NUMBER() OVER(partition by UNIQUE_LEASE_IDENTIFIER order by UNIQUE_LEASE_IDENTIFIER, LINE_ID) NUM
                                   from LS_IMPORT_LEASE
                                  where IMPORT_RUN_ID = A_RUN_ID)
                          where NUM = 1)
         and IMPORT_RUN_ID = A_RUN_ID;

      update LS_IMPORT_LEASE A
         set A.LEASE_ID =
              (select -B.LEASE_ID
                 from LS_IMPORT_LEASE B
                where A.UNIQUE_LEASE_IDENTIFIER = B.UNIQUE_LEASE_IDENTIFIER
                  and B.LEASE_ID is not null)
       where A.LEASE_ID is null
         and IMPORT_RUN_ID = A_RUN_ID;

      select * bulk collect
        into L_MLA_IMPORT_TABLE
        from LS_IMPORT_LEASE
       where IMPORT_RUN_ID = A_RUN_ID
         and NVL(LEASE_ID, -1) > 0;
/* CJS 2/23/15 Changing month to day for master agreement date; Adding new columns*/

      L_MSG := 'LS_LEASE';
      forall L_INDEX in indices of L_MLA_IMPORT_TABLE
         insert into LS_LEASE
            (LEASE_ID, LEASE_STATUS_ID, LEASE_NUMBER, DESCRIPTION, LONG_DESCRIPTION, LESSOR_ID,
             LEASE_TYPE_ID, PAYMENT_DUE_DAY, PRE_PAYMENT_SW, LEASE_GROUP_ID, LEASE_CAP_TYPE_ID,
             WORKFLOW_TYPE_ID, MASTER_AGREEMENT_DATE, NOTES, CURRENT_REVISION, INITIATION_DATE, DAYS_IN_YEAR, CUT_OFF_DAY,
			 LEASE_END_DATE, DAYS_IN_MONTH_SW, CONTRACT_CURRENCY_ID)
            select L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID,
                   1,
                   L_MLA_IMPORT_TABLE(L_INDEX).LEASE_NUMBER,
                   L_MLA_IMPORT_TABLE(L_INDEX).DESCRIPTION,
                   L_MLA_IMPORT_TABLE(L_INDEX).LONG_DESCRIPTION,
                   L_MLA_IMPORT_TABLE(L_INDEX).LESSOR_ID,
                   L_MLA_IMPORT_TABLE(L_INDEX).LEASE_TYPE_ID,
                   L_MLA_IMPORT_TABLE(L_INDEX).PAYMENT_DUE_DAY,
                   L_MLA_IMPORT_TABLE(L_INDEX).PRE_PAYMENT_SW,
                   L_MLA_IMPORT_TABLE(L_INDEX).LEASE_GROUP_ID,
                   L_MLA_IMPORT_TABLE(L_INDEX).LEASE_CAP_TYPE_ID,
                   L_MLA_IMPORT_TABLE(L_INDEX).WORKFLOW_TYPE_ID,
                   TO_DATE(L_MLA_IMPORT_TABLE(L_INDEX).MASTER_AGREEMENT_DATE, 'yyyymmdd'),
                   L_MLA_IMPORT_TABLE(L_INDEX).NOTES,
                   1,
                   sysdate,
                   NVL(L_MLA_IMPORT_TABLE(L_INDEX).DAYS_IN_YEAR, 360),
                   NVL(L_MLA_IMPORT_TABLE(L_INDEX).CUT_OFF_DAY, 0),
				   TO_DATE(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_END_DATE,'yyyymmdd'),
				   NVL(L_MLA_IMPORT_TABLE(L_INDEX).DAYS_IN_MONTH_SW, 1),
                   L_MLA_IMPORT_TABLE(L_INDEX).CONTRACT_CURRENCY_ID
              from DUAL;

      L_MSG := 'LS_LEASE_APPROVAL';
      forall L_INDEX in indices of L_MLA_IMPORT_TABLE
		INSERT INTO LS_LEASE_APPROVAL
						(lease_id,
						 revision,
						 approval_type_id,
						 approval_status_id)
			VALUES      (L_mla_import_table( l_index ).lease_id,
						 1,
						 L_mla_import_table( l_index ).workflow_type_id,
						 1);

      L_MSG := 'LS_LEASE_OPTIONS';
      forall L_INDEX in indices of L_MLA_IMPORT_TABLE
         insert into LS_LEASE_OPTIONS
            (LEASE_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT,
             RENEWAL_OPTION_TYPE_ID, CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW,
             TAX_SUMMARY_ID, TAX_RATE_OPTION_ID, AUTO_GENERATE_INVOICES, LS_RECONCILE_TYPE_ID,
             specialized_asset, intent_to_purchase, sublease, sublease_lease_id)
         values
            (L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID, 1,
             L_MLA_IMPORT_TABLE(L_INDEX).PURCHASE_OPTION_TYPE_ID,
             L_MLA_IMPORT_TABLE(L_INDEX).PURCHASE_OPTION_AMT,
             L_MLA_IMPORT_TABLE(L_INDEX).RENEWAL_OPTION_TYPE_ID,
             L_MLA_IMPORT_TABLE(L_INDEX).CANCELABLE_TYPE_ID, L_MLA_IMPORT_TABLE(L_INDEX).ITC_SW,
             L_MLA_IMPORT_TABLE(L_INDEX).PARTIAL_RETIRE_SW, L_MLA_IMPORT_TABLE(L_INDEX).SUBLET_SW,
             NVL(L_MLA_IMPORT_TABLE(L_INDEX).TAX_SUMMARY_ID, 0),
             NVL(L_MLA_IMPORT_TABLE(L_INDEX).TAX_RATE_OPTION_ID, 0),
             NVL(L_MLA_IMPORT_TABLE(L_INDEX).AUTO_GENERATE_INVOICES, (select NVL(AUTO_GENERATE_INVOICES,0) from LS_LEASE_GROUP where LEASE_GROUP_ID = L_MLA_IMPORT_TABLE(L_INDEX).LEASE_GROUP_ID)),
             NVL(L_MLA_IMPORT_TABLE(L_INDEX).LS_RECONCILE_TYPE_ID, (select LS_RECONCILE_TYPE_ID from LS_LEASE_GROUP where LEASE_GROUP_ID = L_MLA_IMPORT_TABLE(L_INDEX).LEASE_GROUP_ID)),
             L_MLA_IMPORT_TABLE(L_INDEX).specialized_asset,
             L_MLA_IMPORT_TABLE(L_INDEX).intent_to_purchase,
             L_MLA_IMPORT_TABLE(L_INDEX).sublease,
             L_MLA_IMPORT_TABLE(L_INDEX).sublease_lease_id
             );

      L_CC_SQL := 'DECLARE BEGIN ';
      for L_INDEX in 1 .. L_MLA_IMPORT_TABLE.COUNT
      loop
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE1 is not null then
              CC_NEEDED := true;
              L_CC_SQL  := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE1) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE2 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE2) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE3 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE3) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE4 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE4) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE5 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE5) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE6 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE6) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE7 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE7) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE8 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE8) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE9 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE9) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE10 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE10) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE11 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE11) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE12 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE12) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE13 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE13) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE14 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE14) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE15 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE15) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE16 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE16) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE17 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE17) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE18 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE18) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE19 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE19) || ''');';
          end if;
          if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE20 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                      values (' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20) || ', ' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                          TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE20) || ''');';
          end if;
      end loop;
      L_CC_SQL := L_CC_SQL || ' END;';
      if CC_NEEDED then
         execute immediate L_CC_SQL;
      end if;

      select distinct ABS(LEASE_ID), COMPANY_ID, MAX_LEASE_LINE, 0, 0 bulk collect
        into MLA_COMPANY
        from LS_IMPORT_LEASE
       where IMPORT_RUN_ID = A_RUN_ID;

      L_MSG := 'LS_LEASE_COMPANY';
      forall L_INDEX in indices of MLA_COMPANY
         insert into LS_LEASE_COMPANY
            (LEASE_ID, COMPANY_ID, MAX_LEASE_LINE)
         values
            (MLA_COMPANY(L_INDEX).LEASE_ID, MLA_COMPANY(L_INDEX).COMPANY_ID,
             MLA_COMPANY(L_INDEX).MAX_LEASE_LINE);

      select distinct LEASE_ID, COMPANY_ID, MAX_LEASE_LINE, VENDOR_ID, PAYMENT_PCT bulk collect
        into MLA_COMPANY
        from LS_IMPORT_LEASE
       where IMPORT_RUN_ID = A_RUN_ID;

      L_MSG := 'LS_LEASE_VENDOR';
      forall L_INDEX in indices of MLA_COMPANY
         insert into LS_LEASE_VENDOR
            (LEASE_ID, COMPANY_ID, VENDOR_ID, PAYMENT_PCT)
         values
            (ABS(MLA_COMPANY(L_INDEX).LEASE_ID), MLA_COMPANY(L_INDEX).COMPANY_ID,
             MLA_COMPANY(L_INDEX).VENDOR_ID, MLA_COMPANY(L_INDEX).PAYMENT_PCT);

     --Auto approve it
      select * bulk collect
        into L_MLA_IMPORT_TABLE
        from LS_IMPORT_LEASE
       where IMPORT_RUN_ID = A_RUN_ID
         and NVL(LEASE_ID, -1) > 0
         and (AUTO_APPROVE = 1)
         and (WORKFLOW_TYPE_ID in
             (select WORKFLOW_TYPE_ID
                 from WORKFLOW_TYPE
                where lower(subsystem) like '%lessee_mla_approval%'));

      --Instead of calling PKG_LEASE_CALC.F_APPROVE_MLA, I'm copying the SQL here since F_APPROVE_MLA is pragma autonomous
      --Since we can't commit before approving, the autonomous transaction won't be to update anything, so we can't use it
      L_MSG := 'Auto Approving';
      for L_INDEX in 1 .. L_MLA_IMPORT_TABLE.COUNT
      loop
         update LS_LEASE_APPROVAL
            set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
          where LEASE_ID = L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID
            and NVL(REVISION, 0) = 1;

         update LS_LEASE L
            set LEASE_STATUS_ID = 3, APPROVAL_DATE = sysdate, CURRENT_REVISION = 1
          where LEASE_ID = L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID;
      end loop;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_MLAS;

   function F_VALIDATE_MLAS(A_RUN_ID in number) return varchar2 is
      L_MSG 			varchar2(2000);
	  type MLA_IMPORT_TABLE is table of LS_IMPORT_LEASE%rowtype index by pls_integer;
      L_MLA_IMPORT_TABLE MLA_IMPORT_TABLE;
	  L_INDEX   		number;
      L_CC_SQL 			clob;
      CC_IMPORTED 		boolean;
      CC_REQUIRED_NUM  	number(22, 0);
      CC_MISSING 		varchar2(2000);
   begin
      --check for duplicate lease number
      update LS_IMPORT_LEASE
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate Lease Number.'
       where IMPORT_RUN_ID = A_RUN_ID
         and LEASE_NUMBER in
             (select LEASE_NUMBER
                from (select LEASE_NUMBER, count(*) as MY_COUNT
                        from (select LEASE_NUMBER
                                from LS_LEASE
                              union all
                              select LEASE_NUMBER
                                from (select distinct LEASE_NUMBER, UNIQUE_LEASE_IDENTIFIER
                                        from LS_IMPORT_LEASE
                                       where IMPORT_RUN_ID = A_RUN_ID))
                       group by LEASE_NUMBER)
               where MY_COUNT > 1);

      --check that payment_pct makes sense
      update LS_IMPORT_LEASE
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Payment Percentage must be between 0 and 1.'
       where IMPORT_RUN_ID = A_RUN_ID
         and PAYMENT_PCT not between 0.000000001 and 1.000000001; --we dont want a 0 but 1 is ok

      --check that the payment_pct adds up to 1 across the companies
      update LS_IMPORT_LEASE
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Total payment percentage must be 100% per company.'
       where IMPORT_RUN_ID = A_RUN_ID
         and (UNIQUE_LEASE_IDENTIFIER, COMPANY_ID) in
             (select UNIQUE_LEASE_IDENTIFIER, COMPANY_ID
                from (select UNIQUE_LEASE_IDENTIFIER, COMPANY_ID, sum(PAYMENT_PCT) TOTAL_PCT
                        from LS_IMPORT_LEASE
                       where IMPORT_RUN_ID = A_RUN_ID
                       group by UNIQUE_LEASE_IDENTIFIER, COMPANY_ID)
               where TOTAL_PCT <> 1);

      update LS_IMPORT_LEASE
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Vendor is not valid for this Lessor.'
       where IMPORT_RUN_ID = A_RUN_ID
         and (LESSOR_ID, VENDOR_ID) not in
             (select distinct LESSOR_ID, VENDOR_ID from LS_VENDOR where STATUS_CODE_ID <> 2);

      update LS_IMPORT_LEASE
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Selected workflow type cannot be automatically approved.'
       where IMPORT_RUN_ID = A_RUN_ID
         and AUTO_APPROVE = 1
         and WORKFLOW_TYPE_ID not in
             (select WORKFLOW_TYPE_ID from WORKFLOW_TYPE where lower(subsystem) like '%lessee_mla_approval%');

		/* CJS 3/12/15 */
	  update LS_IMPORT_LEASE
	     set ERROR_MESSAGE = ERROR_MESSAGE || ' Days in Year not a valid option.'
	   where IMPORT_RUN_ID = A_RUN_ID
	     and DAYS_IN_YEAR not between 0 and 366;

	  update LS_IMPORT_LEASE
	     set ERROR_MESSAGE = ERROR_MESSAGE || ' Cut off Day not a valid option.'
	   where IMPORT_RUN_ID = A_RUN_ID
	     and CUT_OFF_DAY not between 0 and 31;

	  update LS_IMPORT_LEASE a
	     set ERROR_MESSAGE = ERROR_MESSAGE || ' Selected Currency is not a valid option.'
	   where IMPORT_RUN_ID = A_RUN_ID
	     and not exists (select 1 from currency b where b.currency_id = a.contract_currency_id);

      --Validate if Sublease Flag is no, there is no sublease
      UPDATE LS_IMPORT_LEASE a
      SET    error_message = error_message
                             || ' Sublease populated when Sublease Flag set to "No".'
      WHERE  import_run_id = a_run_id AND
             sublease = 0 AND
             sublease_lease_id IS NOT NULL;

   -- PP-51229 - Add validation for class codes
      for L_CC_NO in 1 .. 20
       loop
	     L_CC_SQL := 'DECLARE BEGIN
	     update LS_IMPORT_LEASE a
		    set ERROR_MESSAGE = ERROR_MESSAGE || '' Class Code '||to_char(L_CC_NO)||' is not a valid MLA class code.''
		  where IMPORT_RUN_ID = '||to_char(A_RUN_ID)||'
			and CLASS_CODE_ID'||to_char(L_CC_NO)||' is not null
			and CLASS_CODE_VALUE'||to_char(L_CC_NO)||' is not null
			and  not exists (select 1 from CLASS_CODE b where a.CLASS_CODE_ID'||to_char(L_CC_NO)||' = b.CLASS_CODE_ID and b.MLA_INDICATOR = 1);

	     update LS_IMPORT_LEASE a
		    set ERROR_MESSAGE = ERROR_MESSAGE || '' Class Code Value '||to_char(L_CC_NO)||' is not a valid value.''
		  where IMPORT_RUN_ID = '||to_char(A_RUN_ID)||'
			and CLASS_CODE_ID'||to_char(L_CC_NO)||' is not null
			and CLASS_CODE_VALUE'||to_char(L_CC_NO)||' is not null
			and exists (select 1 from CLASS_CODE b 
			             where b.CLASS_CODE_ID = a.CLASS_CODE_ID'||to_char(L_CC_NO)||' 
				           and b.MLA_INDICATOR = 1 
                           and lower(CLASS_CODE_TYPE) = ''standard'')
			and not exists (select 1 from CLASS_CODE_VALUES C where C.CLASS_CODE_ID = a.CLASS_CODE_ID'||to_char(L_CC_NO)||' and c.VALUE = a.CLASS_CODE_VALUE'||to_char(L_CC_NO)||');

	     END;';

	   execute immediate L_CC_SQL;
      end loop; 			 
	
   -- PP-51674 - Check for Required Class Codes

     select count(*) into CC_REQUIRED_NUM
     from class_code
     where mla_indicator = 1
     and required = 1;

     if CC_REQUIRED_NUM > 0 then
            L_MSG := 'Check for required class codes';

            select * bulk collect
                into L_MLA_IMPORT_TABLE
                from LS_IMPORT_LEASE
            where IMPORT_RUN_ID = A_RUN_ID;

            for L_INDEX in 1 .. L_MLA_IMPORT_TABLE.COUNT
            loop
                CC_IMPORTED := false;
                L_CC_SQL := 'select listagg(description, '','') within group (order by description) description
                                from class_Code where class_code_id in (
                                select class_code_id from class_code where mla_indicator = 1 and required = 1
                                minus
                                select class_code_id from class_code where class_code_id in (';

                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE1 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE2 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE3 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE4 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE5 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE6 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE7 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE8 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE9 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE10 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE11 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE12 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE13 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE14 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE15 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE16 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE17 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE18 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE19 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19) || ',';
                end if;
                if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20 is not null and L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE20 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20) || ',';
                end if;

                L_CC_SQL :=  trim(TRAILING ',' FROM L_CC_SQL);
                L_CC_SQL := L_CC_SQL || '))';

                if CC_IMPORTED then
                      execute immediate L_CC_SQL into CC_MISSING;

                      if CC_MISSING is not null then
                              UPDATE LS_IMPORT_LEASE a
                              SET    error_message = error_message
                                                    || ' Required MLA Class Codes are Missing:' || CC_MISSING
                              WHERE  import_run_id = a_run_id
                              and line_id = L_MLA_IMPORT_TABLE(L_INDEX).LINE_ID;
                      end if;
                else
                  --There are required class codes and none were imported, so all are missing
                  L_CC_SQL := 'select listagg(description, '','') within group (order by description) description
                                from class_Code where mla_indicator = 1 and required = 1';

                  execute immediate L_CC_SQL into CC_MISSING;

                  if CC_MISSING is not null then
                          UPDATE LS_IMPORT_LEASE a
                          SET    error_message = error_message
                                                || ' Required MLA Class Codes are Missing:' || CC_MISSING
                          WHERE  import_run_id = a_run_id
                          and line_id = L_MLA_IMPORT_TABLE(L_INDEX).LINE_ID;
                  end if;
                end if;
              end loop;

     end if;
	 
   return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_MLAS;

   --##########################################################################
   --                            ILRS
   --##########################################################################
   function F_IMPORT_ILRS(A_RUN_ID in number) return varchar2 is
      L_MSG varchar2(32000);
      type ILR_IMPORT_TABLE is table of LS_IMPORT_ILR%rowtype;
      L_ILR_IMPORT ILR_IMPORT_TABLE;
      L_INDEX      number;
      L_CC_SQL     clob;
      CC_NEEDED    boolean;
	  
	  L_DEFAULT_RATE number;
	  L_TERM_LENGTH number;
	  L_ILR_ID varchar2(20);
	  L_EST_IN_SVC_DATE varchar2(20);
	  L_BORROWING_CURR varchar2(20);
	  
	  disc_rate_error exception;
	  
   begin
	  PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      L_MSG:= 'Setting ILR ID';
      update LS_IMPORT_ILR
         set ILR_ID = LS_ILR_SEQ.NEXTVAL
       where LINE_ID in (select LINE_ID
                           from (select LINE_ID,
                                        ROW_NUMBER() OVER(partition by UNIQUE_ILR_IDENTIFIER order by UNIQUE_ILR_IDENTIFIER, LINE_ID) NUM
                                   from LS_IMPORT_ILR
                                  where IMPORT_RUN_ID = A_RUN_ID)
                          where NUM = 1)
         and IMPORT_RUN_ID = A_RUN_ID;

      update LS_IMPORT_ILR A
         set A.ILR_ID =
              (select -B.ILR_ID
                 from LS_IMPORT_ILR B
                where A.UNIQUE_ILR_IDENTIFIER = B.UNIQUE_ILR_IDENTIFIER
                  and B.ILR_ID is not null
				  and IMPORT_RUN_ID = A_RUN_ID) -- CJS 5/17/17
       where A.ILR_ID is null
         and IMPORT_RUN_ID = A_RUN_ID;

      L_MSG:= 'Setting Workflow Type';
      update LS_IMPORT_ILR A
         set A.WORKFLOW_TYPE_ID =
              (select WORKFLOW_TYPE_ID from LS_ILR_GROUP where ILR_GROUP_ID = A.ILR_GROUP_ID)
       where A.WORKFLOW_TYPE_ID is null
         and A.IMPORT_RUN_ID = A_RUN_ID;

      select * bulk collect
        into L_ILR_IMPORT
        from LS_IMPORT_ILR
       where IMPORT_RUN_ID = A_RUN_ID
         and NVL(ILR_ID, -1) > 0;
/* CJS 2/23/15 Adding Funding Status */
--$$$KES added day option to estimated in service date
      L_MSG:= 'Importing ILR Header';
      forall L_INDEX in indices of L_ILR_IMPORT
         insert into LS_ILR
            (ILR_ID, ILR_STATUS_ID, ILR_NUMBER, LEASE_ID, COMPANY_ID, EST_IN_SVC_DATE, EXTERNAL_ILR,
             ILR_GROUP_ID, NOTES, CURRENT_REVISION, WORKFLOW_TYPE_ID, FUNDING_STATUS_ID)
         values
            (L_ILR_IMPORT(L_INDEX).ILR_ID, 1, L_ILR_IMPORT(L_INDEX).ILR_NUMBER,
             L_ILR_IMPORT(L_INDEX).LEASE_ID, L_ILR_IMPORT(L_INDEX).COMPANY_ID,
             TO_DATE(L_ILR_IMPORT(L_INDEX).EST_IN_SVC_DATE, 'yyyymmdd'),
             L_ILR_IMPORT(L_INDEX).EXTERNAL_ILR, L_ILR_IMPORT(L_INDEX).ILR_GROUP_ID,
             L_ILR_IMPORT(L_INDEX).NOTES, 1, L_ILR_IMPORT(L_INDEX).WORKFLOW_TYPE_ID,
             L_ILR_IMPORT(L_INDEX).FUNDING_STATUS_ID);

      forall L_INDEX in indices of L_ILR_IMPORT
         insert into LS_ILR_APPROVAL
            (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         values
            (L_ILR_IMPORT(L_INDEX).ILR_ID, 1, 4, 1);

      PKG_PP_LOG.P_WRITE_MESSAGE('Insert into ls_ilr_options');
      L_MSG:= 'Importing ILR Options';
	  forall L_INDEX in indices of L_ILR_IMPORT
         insert into LS_ILR_OPTIONS
            (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
             CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, INCEPTION_AIR,
             LEASE_CAP_TYPE_ID, TERMINATION_AMT,
             INTENT_TO_PURCHASE, SPECIALIZED_ASSET, SUBLEASE_FLAG, SUBLEASE_ID, INTERCO_LEASE_FLAG, INTERCO_LEASE_COMPANY,
			 BORROWING_CURR_ID, DEFAULT_RATE_FLAG,
             IMPORT_RUN_ID, PAYMENT_SHIFT, DEFERRED_RENT_SW, DEPR_CALC_METHOD, SCHEDULE_PAYMENT_SHIFT)
            (select L_ILR_IMPORT (L_INDEX).ILR_ID,
                    1,
                    L_ILR_IMPORT (L_INDEX).PURCHASE_OPTION_TYPE_ID,
                    L_ILR_IMPORT (L_INDEX).PURCHASE_OPTION_AMT,
                    L_ILR_IMPORT (L_INDEX).RENEWAL_OPTION_TYPE_ID,
                    L_ILR_IMPORT (L_INDEX).CANCELABLE_TYPE_ID,
                    L_ILR_IMPORT (L_INDEX).ITC_SW,
                    L_ILR_IMPORT (L_INDEX).PARTIAL_RETIRE_SW,
                    L_ILR_IMPORT (L_INDEX).SUBLET_SW,
                    L_ILR_IMPORT (L_INDEX).INCEPTION_AIR,
                    L_ILR_IMPORT (L_INDEX).LEASE_CAP_TYPE_ID,
                    L_ILR_IMPORT (L_INDEX).TERMINATION_AMT,
                    L_ILR_IMPORT (L_INDEX).intent_to_purch,
                    L_ILR_IMPORT (L_INDEX).specialized_asset,
                    L_ILR_IMPORT (L_INDEX).sublease_flag,
                    L_ILR_IMPORT (L_INDEX).sublease_id,
                    L_ILR_IMPORT (L_INDEX).intercompany_lease,
                    L_ILR_IMPORT (L_INDEX).intercompany_company,
					case when nvl(L_ILR_IMPORT (L_INDEX).default_rate_flag, 0) = 1 then
						nvl(L_ILR_IMPORT (L_INDEX).borrowing_curr_id, 0)
					else
						0
					end,
                    L_ILR_IMPORT (L_INDEX).default_rate_flag,
                    A_RUN_ID,
                    case when Nvl(L_ILR_IMPORT (L_INDEX).PAYMENT_SHIFT,PAYMENT_SHIFT) >= 0 
                         then Nvl(L_ILR_IMPORT (L_INDEX).PAYMENT_SHIFT,PAYMENT_SHIFT)
                         else
                           case when Nvl(L_ILR_IMPORT (L_INDEX).PAYMENT_SHIFT,PAYMENT_SHIFT) < 0 
                                and LS_LEASE.PRE_PAYMENT_SW = 0
                           then Nvl(L_ILR_IMPORT (L_INDEX).PAYMENT_SHIFT,PAYMENT_SHIFT)
                           else
                             0
                           end end,
                    Nvl(L_ILR_IMPORT (L_INDEX).deferred_rent_sw, 1),
                    Nvl(L_ILR_IMPORT(L_INDEX).DEPR_CALC_METHOD, 0),
                    case when Nvl(L_ILR_IMPORT (L_INDEX).PAYMENT_SHIFT,PAYMENT_SHIFT) < 0 
                              and LS_LEASE.PRE_PAYMENT_SW = 1
                         then abs(Nvl(L_ILR_IMPORT (L_INDEX).PAYMENT_SHIFT,PAYMENT_SHIFT))
                         else 0 end
               from LS_ILR_GROUP
               join LS_ILR on LS_ILR.ILR_ID = L_ILR_IMPORT (L_INDEX).ILR_ID
               join LS_LEASE on LS_LEASE.LEASE_ID = LS_ILR.LEASE_ID
              where LS_ILR_GROUP.ILR_GROUP_ID = L_ILR_IMPORT(L_INDEX).ILR_GROUP_ID);

      L_MSG:= 'Importing ILR Accounts';
      forall L_INDEX in indices of L_ILR_IMPORT
         insert into LS_ILR_ACCOUNT
            (ILR_ID, INT_ACCRUAL_ACCOUNT_ID, INT_EXPENSE_ACCOUNT_ID, EXEC_ACCRUAL_ACCOUNT_ID,
             EXEC_EXPENSE_ACCOUNT_ID, CONT_ACCRUAL_ACCOUNT_ID, CONT_EXPENSE_ACCOUNT_ID,
             CAP_ASSET_ACCOUNT_ID, ST_OBLIG_ACCOUNT_ID, LT_OBLIG_ACCOUNT_ID, AP_ACCOUNT_ID,
             RES_DEBIT_ACCOUNT_ID, RES_CREDIT_ACCOUNT_ID, ST_DEFERRED_ACCOUNT_ID, LT_DEFERRED_ACCOUNT_ID,
             CURRENCY_GAIN_LOSS_DR_ACCT_ID, CURRENCY_GAIN_LOSS_CR_ACCT_ID, PREPAID_RENT_ACCOUNT_ID,
             INCENTIVE_ACCOUNT_ID, INIT_DIRECT_COST_ACCOUNT_ID,IMPAIR_EXPENSE_ACCOUNT_ID,
             IMPAIR_REVERSAL_ACCOUNT_ID,IMPAIR_ACCUM_AMORT_ACCOUNT_ID)
            select L_ILR_IMPORT(L_INDEX).ILR_ID,
                   NVL(L_ILR_IMPORT(L_INDEX).INT_ACCRUAL_ACCOUNT_ID, INT_ACCRUAL_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).INT_EXPENSE_ACCOUNT_ID, INT_EXPENSE_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).EXEC_ACCRUAL_ACCOUNT_ID, EXEC_ACCRUAL_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).EXEC_EXPENSE_ACCOUNT_ID, EXEC_EXPENSE_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).CONT_ACCRUAL_ACCOUNT_ID, CONT_ACCRUAL_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).CONT_EXPENSE_ACCOUNT_ID, CONT_EXPENSE_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).CAP_ASSET_ACCOUNT_ID, CAP_ASSET_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).ST_OBLIG_ACCOUNT_ID, ST_OBLIG_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).LT_OBLIG_ACCOUNT_ID, LT_OBLIG_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).AP_ACCOUNT_ID, AP_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).RES_DEBIT_ACCOUNT_ID, RES_DEBIT_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).RES_CREDIT_ACCOUNT_ID, RES_CREDIT_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).ST_DEFERRED_ACCOUNT_ID, ST_DEFERRED_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).LT_DEFERRED_ACCOUNT_ID, LT_DEFERRED_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).CURRENCY_GAIN_LOSS_DR_ACCT_ID, CURR_GAIN_LOSS_ACCT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).CURRENCY_GAIN_LOSS_CR_ACCT_ID, CURR_GAIN_LOSS_OFFSET_ACCT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).PREPAID_RENT_ACCOUNT_ID, PREPAID_RENT_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).INCENTIVE_ACCOUNT_ID, INCENTIVE_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).INIT_DIRECT_COST_ACCOUNT_ID, INIT_DIRECT_COST_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).IMPAIR_EXPENSE_ACCOUNT_ID, IMPAIR_EXPENSE_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).IMPAIR_REVERSAL_ACCOUNT_ID, IMPAIR_REVERSAL_ACCOUNT_ID),
                   NVL(L_ILR_IMPORT(L_INDEX).IMPAIR_ACCUM_AMORT_ACCOUNT_ID, IMPAIR_ACCUM_AMORT_ACCOUNT_ID)
              from LS_ILR_GROUP
             where ILR_GROUP_ID = L_ILR_IMPORT(L_INDEX).ILR_GROUP_ID;

      PKG_PP_LOG.P_WRITE_MESSAGE('Class code sql build');
      L_MSG:= 'Importing Class Codes';
	  L_CC_SQL := 'DECLARE BEGIN ';
      for L_INDEX in 1 .. L_ILR_IMPORT.COUNT
      loop
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID1 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE1 is not null then
              CC_NEEDED := true;
              L_CC_SQL  := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID1) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE1) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID2 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE2 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID2) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE2) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID3 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE3 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID3) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE3) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID4 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE4 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID4) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE4) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID5 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE5 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID5) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE5) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID6 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE6 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID6) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE6) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID7 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE7 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID7) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE7) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID8 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE8 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID8) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE8) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID9 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE9 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID9) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE9) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID10 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE10 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID10) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE10) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID11 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE11 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID11) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE11) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID12 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE12 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID12) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE12) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID13 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE13 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID13) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE13) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID14 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE14 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID14) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE14) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID15 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE15 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID15) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE15) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID16 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE16 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID16) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE16) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID17 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE17 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID17) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE17) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID18 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE18 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID18) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE18) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID19 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE19 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID19) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE19) || ''');';
          end if;
          if L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID20 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE20 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                      values (' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID20) || ', ' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                          TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE20) || ''');';
          end if;
      end loop;
      L_CC_SQL := L_CC_SQL || ' END;';
      if CC_NEEDED then
         execute immediate L_CC_SQL;
      end if;

      --$$$KES: added NVL before buckets for summary purposes in ILR details
	  select * bulk collect into L_ILR_IMPORT from LS_IMPORT_ILR where IMPORT_RUN_ID = A_RUN_ID;

      L_MSG:= 'Importing Payment Terms';
      forall L_INDEX in indices of L_ILR_IMPORT
         insert into LS_ILR_PAYMENT_TERM
            (ILR_ID, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE, PAYMENT_FREQ_ID,
             NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT, REVISION,
             C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6, C_BUCKET_7,
             C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3, E_BUCKET_4,
             E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10,
             ESCALATION, ESCALATION_FREQ_ID, ESCALATION_PCT)
         values
            (ABS(L_ILR_IMPORT(L_INDEX).ILR_ID), L_ILR_IMPORT(L_INDEX).PAYMENT_TERM_ID, nvl(L_ILR_IMPORT(L_INDEX).PAYMENT_TERM_TYPE_ID,2),
             TO_DATE(L_ILR_IMPORT(L_INDEX).PAYMENT_TERM_DATE, 'yyyymmdd'),
             L_ILR_IMPORT(L_INDEX).PAYMENT_FREQ_ID, L_ILR_IMPORT(L_INDEX).NUMBER_OF_TERMS,
             L_ILR_IMPORT(L_INDEX).EST_EXECUTORY_COST, L_ILR_IMPORT(L_INDEX).PAID_AMOUNT,
             L_ILR_IMPORT(L_INDEX).CONTINGENT_AMOUNT, 1, Nvl(L_ILR_IMPORT(L_INDEX).C_BUCKET_1,0),
             Nvl(L_ILR_IMPORT(L_INDEX).C_BUCKET_2,0), Nvl(L_ILR_IMPORT(L_INDEX).C_BUCKET_3,0),
             Nvl(L_ILR_IMPORT(L_INDEX).C_BUCKET_4,0), Nvl(L_ILR_IMPORT(L_INDEX).C_BUCKET_5,0),
             Nvl(L_ILR_IMPORT(L_INDEX).C_BUCKET_6,0), Nvl(L_ILR_IMPORT(L_INDEX).C_BUCKET_7,0),
             Nvl(L_ILR_IMPORT(L_INDEX).C_BUCKET_8,0), Nvl(L_ILR_IMPORT(L_INDEX).C_BUCKET_9,0),
             Nvl(L_ILR_IMPORT(L_INDEX).C_BUCKET_10,0), Nvl(L_ILR_IMPORT(L_INDEX).E_BUCKET_1,0),
             Nvl(L_ILR_IMPORT(L_INDEX).E_BUCKET_2,0), Nvl(L_ILR_IMPORT(L_INDEX).E_BUCKET_3,0),
             Nvl(L_ILR_IMPORT(L_INDEX).E_BUCKET_4,0), Nvl(L_ILR_IMPORT(L_INDEX).E_BUCKET_5,0),
             Nvl(L_ILR_IMPORT(L_INDEX).E_BUCKET_6,0), Nvl(L_ILR_IMPORT(L_INDEX).E_BUCKET_7,0),
             Nvl(L_ILR_IMPORT(L_INDEX).E_BUCKET_8,0), Nvl(L_ILR_IMPORT(L_INDEX).E_BUCKET_9,0),
             Nvl(L_ILR_IMPORT(L_INDEX).E_BUCKET_10,0),
             L_ILR_IMPORT(L_INDEX).ESCALATION, L_ILR_IMPORT(L_INDEX).ESCALATION_FREQ_ID, L_ILR_IMPORT(L_INDEX).ESCALATION_PCT)
             ;

      L_MSG:= 'Checking ILRs for Default Discount Rate';
	  select * bulk collect into L_ILR_IMPORT from LS_IMPORT_ILR where IMPORT_RUN_ID = A_RUN_ID and NVL(ILR_ID, -1) > 0 and NVL(DEFAULT_RATE_FLAG,0) = 1;
	  for L_INDEX in 1 .. L_ILR_IMPORT.COUNT
		  loop 
			L_MSG:= 'Updating Default Discount Rate - Pulling Term Length for ILR';
			
			L_ILR_ID := L_ILR_IMPORT(L_INDEX).ILR_ID;
			L_TERM_LENGTH := PKG_LEASE_ILR.F_PULL_ILR_PAYMENT_TERM_MONTHS(TO_NUMBER(L_ILR_ID),1);
			
			if L_TERM_LENGTH IS NULL then
			  RAISE disc_rate_error;
			end if;
			
			L_EST_IN_SVC_DATE := L_ILR_IMPORT(L_INDEX).EST_IN_SVC_DATE;
			L_BORROWING_CURR := NVL(L_ILR_IMPORT (L_INDEX).borrowing_curr_id, 0);
			
			L_MSG:= 'Updating Default Discount Rate - Getting Discount Rate for ILR';
			L_DEFAULT_RATE := PKG_LEASE_ILR.F_GET_DEFAULT_DISC_RATE(TO_NUMBER(L_ILR_ID), L_TERM_LENGTH, TO_DATE(L_EST_IN_SVC_DATE, 'yyyymmdd'), NULL, L_BORROWING_CURR);
			
			--Multiply *100 since ls_ilr_options stores the decimal as a casted percentage
			L_DEFAULT_RATE := L_DEFAULT_RATE*100;
			
			L_MSG:= 'Updating Default Discount Rate - Setting Discount Rate for ILR';
			UPDATE ls_ilr_options
				SET inception_air = L_DEFAULT_RATE
			  WHERE ls_ilr_options.ilr_id = L_ILR_IMPORT(L_INDEX).ILR_ID AND
					ls_ilr_options.revision = 1;
	  end loop;
		  
		  
      return 'OK';
	  
   exception
	  when disc_rate_error then
		 L_MSG := L_MSG || ' - Unable to confirm default discount rate';
	     return L_MSG;
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_ILRS;

   function F_VALIDATE_ILRS(A_RUN_ID in number) return varchar2 is
      L_MSG varchar2(2000);
	  type ILR_IMPORT_TABLE is table of LS_IMPORT_ILR%rowtype;
      L_ILR_IMPORT_TABLE ILR_IMPORT_TABLE;
	  L_INDEX   		number;
      L_CC_SQL 			clob;
      CC_IMPORTED 		boolean;
      CC_REQUIRED_NUM  	number(22, 0);
      CC_MISSING 		varchar2(2000);
   begin
      --check we don't have duplicate ILR numbers
	  -- CJS 3/12/15 by lease/company
      update LS_IMPORT_ILR Z
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate ilr number.'
       where IMPORT_RUN_ID = A_RUN_ID
         and (ILR_NUMBER, LEASE_ID, COMPANY_ID) in (select ILR_NUMBER, LEASE_ID, COMPANY_ID
                              from (select ILR_NUMBER, LEASE_ID, COMPANY_ID, count(*) as MY_COUNT
                                      from (select ILR_NUMBER, LEASE_ID, COMPANY_ID
                                              from LS_ILR
											 where (LEASE_ID, COMPANY_ID) in (
											  select distinct LEASE_ID, COMPANY_ID from LS_IMPORT_ILR)
                                            union all
                                            select ILR_NUMBER, LEASE_ID, COMPANY_ID
                                              from (select distinct ILR_NUMBER, LEASE_ID, COMPANY_ID, UNIQUE_ILR_IDENTIFIER
                                                      from LS_IMPORT_ILR
                                                     where IMPORT_RUN_ID = A_RUN_ID))
                                     group by ILR_NUMBER, LEASE_ID, COMPANY_ID)
                             where MY_COUNT > 1);

         --check the payment terms have sequential payment term ids
      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non sequential payment term ids.'
       where IMPORT_RUN_ID = A_RUN_ID
         and LINE_ID in (select LINE_ID
                           from (select LINE_ID,
                                        PAYMENT_TERM_ID,
                                        ROW_NUMBER() OVER(partition by UNIQUE_ILR_IDENTIFIER order by UNIQUE_ILR_IDENTIFIER, PAYMENT_TERM_ID) MY_TERMS
                                   from LS_IMPORT_ILR
                                  where IMPORT_RUN_ID = A_RUN_ID)
                          where PAYMENT_TERM_ID <> MY_TERMS);

      --check that one term ends where the next begins
      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Start of ILR term does not match with end of previous term.'
       where IMPORT_RUN_ID = A_RUN_ID
         and LINE_ID in
             (select LINE_ID
                from (select LINE_ID,
                             TERM_START,
                             LAG(TERM_END, 1, TO_DATE('07/04/1776', 'mm/dd/yyyy')) OVER(partition by UNIQUE_ILR_IDENTIFIER order by UNIQUE_ILR_IDENTIFIER, TERM_START) as TERM_END
                        from (select LINE_ID,
                                     UNIQUE_ILR_IDENTIFIER,
                                     TO_DATE(PAYMENT_TERM_DATE, 'yyyymmdd') as TERM_START,
                                     ADD_MONTHS(TO_DATE(PAYMENT_TERM_DATE, 'yyyymmdd'),
                                                NUMBER_OF_TERMS *
                                                DECODE(PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 4, 1)) as TERM_END
                                from LS_IMPORT_ILR
                               where IMPORT_RUN_ID = A_RUN_ID))
               where TERM_START <> TERM_END
                 and TERM_END <> TO_DATE('07/04/1776', 'mm/dd/yyyy'));

      --make sure the company is valid for the lease
      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Company not valid for this MLA.'
       where IMPORT_RUN_ID = A_RUN_ID
         and COMPANY_ID not in
             (select COMPANY_ID from LS_LEASE_COMPANY where LEASE_ID = LS_IMPORT_ILR.LEASE_ID);

      --check that any buckets being imported are enabled.
      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 1 is not enabled.'
       where not (E_BUCKET_1 is null or E_BUCKET_1 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 1
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 1));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 2 is not enabled.'
       where not (E_BUCKET_2 is null or E_BUCKET_2 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 2
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 2));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 3 is not enabled.'
       where not (E_BUCKET_3 is null or E_BUCKET_3 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 3
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 3));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 4 is not enabled.'
       where not (E_BUCKET_4 is null or E_BUCKET_4 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 4
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 4));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 5 is not enabled.'
       where not (E_BUCKET_5 is null or E_BUCKET_5 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 5
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 5));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 6 is not enabled.'
       where not (E_BUCKET_6 is null or E_BUCKET_6 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 6
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 6));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 7 is not enabled.'
       where not (E_BUCKET_7 is null or E_BUCKET_7 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 7
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 7));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 8 is not enabled.'
       where not (E_BUCKET_8 is null or E_BUCKET_8 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 8
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 8));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 9 is not enabled.'
       where not (E_BUCKET_9 is null or E_BUCKET_9 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 9
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 9));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 10 is not enabled.'
       where not (E_BUCKET_10 is null or E_BUCKET_10 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 10
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 10));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 1 is not enabled.'
       where not (C_BUCKET_1 is null or C_BUCKET_1 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 1
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 1));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 2 is not enabled.'
       where not (C_BUCKET_2 is null or C_BUCKET_2 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 2
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 2));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 3 is not enabled.'
       where not (C_BUCKET_3 is null or C_BUCKET_3 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 3
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 3));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 4 is not enabled.'
       where not (C_BUCKET_4 is null or C_BUCKET_4 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 4
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 4));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 5 is not enabled.'
       where not (C_BUCKET_5 is null or C_BUCKET_5 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 5
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 5));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 6 is not enabled.'
       where not (C_BUCKET_6 is null or C_BUCKET_6 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 6
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 6));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 7 is not enabled.'
       where not (C_BUCKET_7 is null or C_BUCKET_7 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 7
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 7));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 8 is not enabled.'
       where not (C_BUCKET_8 is null or C_BUCKET_8 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 8
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 8));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 9 is not enabled.'
       where not (C_BUCKET_9 is null or C_BUCKET_9 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 9
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 9));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 10 is not enabled.'
       where not (C_BUCKET_10 is null or C_BUCKET_10 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 10
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 10));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Interest Accrual Account.'
       where INT_ACCRUAL_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = INT_ACCRUAL_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Interest Expense Account.'
       where INT_EXPENSE_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = INT_EXPENSE_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Executory Accrual Account.'
       where EXEC_ACCRUAL_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = EXEC_ACCRUAL_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Executory Expense Account.'
       where EXEC_EXPENSE_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = EXEC_EXPENSE_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Contingent Accrual Account.'
       where CONT_ACCRUAL_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = CONT_ACCRUAL_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Contingent Expense Account.'
       where CONT_ACCRUAL_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = CONT_ACCRUAL_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Capital Asset Account.'
       where CAP_ASSET_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = CAP_ASSET_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Short-term Obligation Account.'
       where ST_OBLIG_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = ST_OBLIG_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Long-term Obligation Account.'
       where LT_OBLIG_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = LT_OBLIG_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease AP Account.'
       where AP_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = AP_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Residual Debit Account.'
       where RES_DEBIT_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = RES_DEBIT_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Residual Credit Account.'
       where RES_CREDIT_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = RES_CREDIT_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Short Term Deferred Account.'
       where ST_DEFERRED_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = ST_DEFERRED_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Long Term Deferred Account.'
       where LT_DEFERRED_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = LT_DEFERRED_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Currency Gain/Loss Account.'
       where CURRENCY_GAIN_LOSS_DR_ACCT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = CURRENCY_GAIN_LOSS_DR_ACCT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Currency Gain/Loss Offset Account.'
       where CURRENCY_GAIN_LOSS_CR_ACCT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = CURRENCY_GAIN_LOSS_CR_ACCT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Prepaid Rent Account.'
       where PREPAID_RENT_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = PREPAID_RENT_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Incentive Account.'
       where INCENTIVE_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = INCENTIVE_ACCOUNT_ID) <> 11;

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Initial Direct Cost Account.'
       where INIT_DIRECT_COST_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = INIT_DIRECT_COST_ACCOUNT_ID) <> 11;
               
      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Impairment Expense Account.'
       where IMPAIR_EXPENSE_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = IMPAIR_EXPENSE_ACCOUNT_ID) <> 11;
               
       update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Impairment Reversal Account.'
       where IMPAIR_REVERSAL_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = IMPAIR_REVERSAL_ACCOUNT_ID) <> 11;
               
        update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Impairment Accumulated Amortization Account.'
       where IMPAIR_ACCUM_AMORT_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
                from GL_ACCOUNT
               where GL_ACCOUNT.GL_ACCOUNT_ID = IMPAIR_ACCUM_AMORT_ACCOUNT_ID) <> 11;

      --Validate if Sublease Flag is no, there is no sublease
      UPDATE LS_IMPORT_ILR a
      SET    error_message = error_message
                             || ' Sublease populated when Sublease Flag set to "No".'
      WHERE  import_run_id = a_run_id AND
             sublease_flag = 0 AND
             sublease_id IS NOT NULL;

      --Validate intercompany Lease company is a lease company
      UPDATE LS_IMPORT_ILR a
      SET    error_message = error_message
                             || ' Intercompany Lease Company must be a Lease Company.'
      WHERE  import_run_id = a_run_id AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   COMPANY_SETUP
                   WHERE  intercompany_company = company_id AND
                          is_lease_company = 1 ) AND
             intercompany_company IS NOT NULL;

   -- PP-51229 - Add validation for class codes
      for L_CC_NO in 1 .. 20
       loop
	     L_CC_SQL := 'DECLARE BEGIN
	     update LS_IMPORT_ILR a
		    set ERROR_MESSAGE = ERROR_MESSAGE || '' Class Code '||to_char(L_CC_NO)||' is not a valid ILR class code.''
		   where IMPORT_RUN_ID = '||to_char(A_RUN_ID)||'
			  and CLASS_CODE_ID'||to_char(L_CC_NO)||' is not null
			  and CLASS_CODE_VALUE'||to_char(L_CC_NO)||' is not null
			  and  not exists (select 1 from CLASS_CODE b where a.CLASS_CODE_ID'||to_char(L_CC_NO)||' = b.CLASS_CODE_ID and b.ILR_INDICATOR = 1);

	     update LS_IMPORT_ILR a
		    set ERROR_MESSAGE = ERROR_MESSAGE || '' Class Code Value '||to_char(L_CC_NO)||' is not a valid value.''
		   where IMPORT_RUN_ID = '||to_char(A_RUN_ID)||'
			  and CLASS_CODE_ID'||to_char(L_CC_NO)||' is not null
			  and CLASS_CODE_VALUE'||to_char(L_CC_NO)||' is not null
			  and exists (select 1 from CLASS_CODE b 
			               where b.CLASS_CODE_ID = a.CLASS_CODE_ID'||to_char(L_CC_NO)||' 
				            and b.ILR_INDICATOR = 1 
                            and lower(CLASS_CODE_TYPE) = ''standard'')
			  and not exists (select 1 from CLASS_CODE_VALUES C where C.CLASS_CODE_ID = a.CLASS_CODE_ID'||to_char(L_CC_NO)||' and c.VALUE = a.CLASS_CODE_VALUE'||to_char(L_CC_NO)||');

	      END;';

	     execute immediate L_CC_SQL;
      end loop; 
   -- PP-51674 - Check for Required Class Codes

     select count(*) into CC_REQUIRED_NUM
     from class_code
     where ilr_indicator = 1
     and required = 1;

     if CC_REQUIRED_NUM > 0 then
            L_MSG := 'Check for required class codes';

            select * bulk collect
                into L_ILR_IMPORT_TABLE
                from LS_IMPORT_ILR
            where IMPORT_RUN_ID = A_RUN_ID;

            for L_INDEX in 1 .. L_ILR_IMPORT_TABLE.COUNT
            loop
                CC_IMPORTED := false;
                L_CC_SQL := 'select listagg(description, '','') within group (order by description) description
                                from class_Code where class_code_id in (
                                select class_code_id from class_code where ilr_indicator = 1 and required = 1
                                minus
                                select class_code_id from class_code where class_code_id in (';

                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE1 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE2 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE3 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE4 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE5 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE6 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE7 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE8 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE9 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE10 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE11 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE12 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE13 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE14 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE15 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE16 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE17 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE18 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE19 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19) || ',';
                end if;
                if L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20 is not null and L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE20 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ILR_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20) || ',';
                end if;

                L_CC_SQL :=  trim(TRAILING ',' FROM L_CC_SQL);
                L_CC_SQL := L_CC_SQL || '))';

                if CC_IMPORTED then
                      execute immediate L_CC_SQL into CC_MISSING;

                      if CC_MISSING is not null then
                              UPDATE LS_IMPORT_ILR a
                              SET    error_message = error_message
                                                    || ' Required ILR Class Codes are Missing:' || CC_MISSING
                              WHERE  import_run_id = a_run_id
                              and line_id = L_ILR_IMPORT_TABLE(L_INDEX).LINE_ID;
                      end if;
                else
                  --There are required class codes and none were imported, so all are missing
                  L_CC_SQL := 'select listagg(description, '','') within group (order by description) description
                                from class_Code where ilr_indicator = 1 and required = 1';

                  execute immediate L_CC_SQL into CC_MISSING;

                  if CC_MISSING is not null then
                          UPDATE LS_IMPORT_ILR a
                          SET    error_message = error_message
                                                || ' Required ILR Class Codes are Missing:' || CC_MISSING
                          WHERE  import_run_id = a_run_id
                          and line_id = L_ILR_IMPORT_TABLE(L_INDEX).LINE_ID;
                  end if;
                end if;
              end loop;

     end if;

	  
	  
      --Validate variable escalation has no escalation percent
      UPDATE LS_IMPORT_ILR a
      SET    error_message = error_message
                             || ' An Escalation Percent may not be assigned to an escalation which is a Variable Escalation.'
      WHERE  import_run_id = a_run_id
      AND escalation IS NOT NULL
      AND escalation_pct <> 0
      AND escalation_pct IS NOT NULL;

      --Validate escalation is not more frequent than payment
      UPDATE LS_IMPORT_ILR a
      SET    error_message = error_message
                             || ' An escalation frequency may not be more frequent than the payment.'
      WHERE  import_run_id = a_run_id
      AND escalation_freq_id > payment_freq_id;

      --Validate escalation has a frequency assigned
      UPDATE LS_IMPORT_ILR a
      SET    error_message = error_message
                             || ' An escalation frequency is required if escalations are assigned.'
      WHERE  import_run_id = a_run_id
      AND escalation_freq_id IS NULL
      AND (escalation IS NOT NULL
        OR (escalation IS NULL AND escalation_pct IS NOT NULL AND escalation_pct <> 0) );

     --PP-53118 We don not want to load purchase or terminations through ILR import. The option imports should be used instead.
      UPDATE LS_IMPORT_ILR a
      SET    error_message = error_message
                             || ' Purchase Option Type or Amount Has Been Populated. Set Type to None and Amount to Zero and use the Purchase Option Import Type to load PO information.'
      WHERE  import_run_id = a_run_id
      AND (nvl(purchase_option_amt,0) <> 0 or nvl(purchase_option_type_id,1) <> 1);
      
      UPDATE LS_IMPORT_ILR a
      SET    error_message = error_message
                             || ' Termination Amount Has Been Populated. Set Amount to Zero and use the Termination Option Import Type to load Termination information.'
      WHERE  import_run_id = a_run_id
      AND nvl(termination_amt,0) <> 0;



      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_ILRS;
   --##########################################################################
   --                            ASSETS
   --##########################################################################
   function F_IMPORT_ASSETS(A_RUN_ID in number) return varchar2 is
      L_MSG          varchar2(32000);
      L_ASSET_IMPORT ASSET_IMPORT_TABLE;
      L_INDEX        number;
      L_CC_SQL       clob;
      CC_NEEDED      boolean;
	  
	  CURSOR import_asset_cur
	      IS
	    select * from ls_import_asset
        where IMPORT_RUN_ID = A_RUN_ID;
   begin

      update LS_IMPORT_ASSET set LS_ASSET_ID = LS_ASSET_SEQ.NEXTVAL;

      OPEN import_asset_cur;

      LOOP
       FETCH  import_asset_cur
          BULK COLLECT INTO L_ASSET_IMPORT LIMIT 5000;
	   
	   for L_INDEX in 1 .. L_ASSET_IMPORT.COUNT
	    loop
         insert into LS_ASSET
            (LS_ASSET_ID, LS_ASSET_STATUS_ID, LEASED_ASSET_NUMBER, DESCRIPTION, LONG_DESCRIPTION,
             QUANTITY, FMV, COMPANY_ID, BUS_SEGMENT_ID, UTILITY_ACCOUNT_ID, SUB_ACCOUNT_ID,
             RETIREMENT_UNIT_ID, PROPERTY_GROUP_ID, WORK_ORDER_ID, ASSET_LOCATION_ID,
             GUARANTEED_RESIDUAL_AMOUNT, EXPECTED_LIFE, ECONOMIC_LIFE, NOTES, ILR_ID, SERIAL_NUMBER,
             IMPORT_RUN_ID, TAX_ASSET_LOCATION_ID, DEPARTMENT_ID, ESTIMATED_RESIDUAL, TAX_SUMMARY_ID,
			 USED_YN_SW, PROPERTY_TAX_DATE, PROPERTY_TAX_AMOUNT, CONTRACT_CURRENCY_ID)
            select L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID,
                   1,
                   L_ASSET_IMPORT(L_INDEX).LEASED_ASSET_NUMBER,
                   L_ASSET_IMPORT(L_INDEX).DESCRIPTION,
                   L_ASSET_IMPORT(L_INDEX).LONG_DESCRIPTION,
                   L_ASSET_IMPORT(L_INDEX).QUANTITY,
                   L_ASSET_IMPORT(L_INDEX).FMV,
                   L_ASSET_IMPORT(L_INDEX).COMPANY_ID,
                   L_ASSET_IMPORT(L_INDEX).BUS_SEGMENT_ID,
                   L_ASSET_IMPORT(L_INDEX).UTILITY_ACCOUNT_ID,
                   L_ASSET_IMPORT(L_INDEX).SUB_ACCOUNT_ID,
                   L_ASSET_IMPORT(L_INDEX).RETIREMENT_UNIT_ID,
                   L_ASSET_IMPORT(L_INDEX).PROPERTY_GROUP_ID,
                   L_ASSET_IMPORT(L_INDEX).WORK_ORDER_ID,
                   L_ASSET_IMPORT(L_INDEX).ASSET_LOCATION_ID,
                   L_ASSET_IMPORT(L_INDEX).GUARANTEED_RESIDUAL_AMOUNT,
                   L_ASSET_IMPORT(L_INDEX).EXPECTED_LIFE,
                   L_ASSET_IMPORT(L_INDEX).ECONOMIC_LIFE,
                   L_ASSET_IMPORT(L_INDEX).NOTES,
                   L_ASSET_IMPORT(L_INDEX).ILR_ID,
                   L_ASSET_IMPORT(L_INDEX).SERIAL_NUMBER,
                   A_RUN_ID,
                   NVL(L_ASSET_IMPORT(L_INDEX).TAX_ASSET_LOCATION_ID,
                       L_ASSET_IMPORT(L_INDEX).ASSET_LOCATION_ID),
                   L_ASSET_IMPORT(L_INDEX).DEPARTMENT_ID,
                   NVL(L_ASSET_IMPORT(L_INDEX).ESTIMATED_RESIDUAL, 0),
                   L_ASSET_IMPORT(L_INDEX).TAX_SUMMARY_ID,
				   L_ASSET_IMPORT(L_INDEX).USED_YN_SW,
				   TO_DATE(L_ASSET_IMPORT(L_INDEX).PROPERTY_TAX_DATE,'yyyymmdd'),
				   L_ASSET_IMPORT(L_INDEX).PROPERTY_TAX_AMOUNT,
				   L_ASSET_IMPORT(L_INDEX).CONTRACT_CURRENCY_ID
              from DUAL;
				   
       if  L_ASSET_IMPORT(L_INDEX).ILR_ID is not null THEN		
		   update ls_asset
			   set tax_summary_id = (select lo.tax_summary_id 
											   from ls_lease_options lo, ls_ilr i, ls_lease l
											  where i.ilr_id = L_ASSET_IMPORT(L_INDEX).ILR_ID
												 and i.lease_id = l.lease_id
												 and l.lease_id = lo.lease_id
												 and l.current_revision = lo.revision
												 and lo.tax_summary_id <> 0
												 and lo.tax_summary_id is not null) 
			 where ls_asset.ls_asset_id in (L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID)
			 and (ls_asset.tax_summary_id is null or ls_asset.tax_summary_id = 0);
	   END IF;
	  
      --Do class code inserts here
		 CC_NEEDED := false;
      L_CC_SQL := 'DECLARE BEGIN ';
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID1 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE1 is not null then
              CC_NEEDED := true;
              L_CC_SQL  := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                    values (' ||
                        TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID1) || ', ' ||
                        TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                        TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE1) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID2 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE2 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID2) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE2) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID3 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE3 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID3) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE3) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID4 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE4 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID4) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE4) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID5 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE5 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID5) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE5) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID6 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE6 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID6) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE6) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID7 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE7 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID7) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE7) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID8 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE8 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID8) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE8) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID9 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE9 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID9) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE9) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID10 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE10 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID10) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE10) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID11 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE11 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID11) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE11) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID12 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE12 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID12) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE12) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID13 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE13 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID13) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE13) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID14 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE14 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID14) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE14) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID15 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE15 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID15) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE15) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID16 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE16 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID16) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE16) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID17 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE17 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID17) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE17) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID18 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE18 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID18) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE18) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID19 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE19 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID19) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE19) || ''');';
          end if;
          if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID20 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE20 is not null then
              CC_NEEDED := true;
              L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                      values (' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID20) || ', ' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                          TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE20) || ''');';
          end if;
      L_CC_SQL := L_CC_SQL || ' END;';
      if CC_NEEDED then
         execute immediate L_CC_SQL;
      end if;
			
	    end loop;
	    EXIT WHEN L_ASSET_IMPORT.COUNT < 5000;
	  END LOOP;
	  CLOSE import_asset_cur;
			  
     
	  --Insert into LS_ILR_ASSET_MAP
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, LS_ASSET_ID, REVISION)
         select IA.ILR_ID, IA.LS_ASSET_ID, IO.REVISION
           from LS_IMPORT_ASSET IA, LS_ILR_OPTIONS IO
          where IA.IMPORT_RUN_ID = A_RUN_ID
            and IA.ILR_ID = IO.ILR_ID
            and IO.REVISION > 0 
            and IA.ILR_ID is not null;

      insert into LS_ASSET_TAX_MAP
         (LS_ASSET_ID, TAX_LOCAL_ID, STATUS_CODE_ID)
         (select LA.LS_ASSET_ID LS_ASSET_ID, TL.TAX_LOCAL_ID TAX_LOCAL_ID, 1 STATUS_CODE_ID
            from LS_ASSET           LA,
                 LS_TAX_LOCAL       TL,
                 LS_ILR,
                 LS_LEASE,
                 LS_LEASE_OPTIONS,
                 LS_TAX_STATE_RATES,
                 ASSET_LOCATION,
                 LS_IMPORT_ASSET
           where LA.ILR_ID = LS_ILR.ILR_ID
             and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID
             and LS_LEASE.LEASE_ID = LS_LEASE_OPTIONS.LEASE_ID
             and LS_LEASE.CURRENT_REVISION = LS_LEASE_OPTIONS.REVISION
             and LS_LEASE_OPTIONS.TAX_SUMMARY_ID = TL.TAX_SUMMARY_ID
             and TL.TAX_LOCAL_ID = LS_TAX_STATE_RATES.TAX_LOCAL_ID
             and LS_TAX_STATE_RATES.STATE_ID = ASSET_LOCATION.STATE_ID
             and LA.TAX_ASSET_LOCATION_ID = ASSET_LOCATION.ASSET_LOCATION_ID
             and LA.LS_ASSET_ID = LS_IMPORT_ASSET.LS_ASSET_ID
             and LS_IMPORT_ASSET.IMPORT_RUN_ID = A_RUN_ID
             and LS_IMPORT_ASSET.ILR_ID is not null
          union
          select LA.LS_ASSET_ID LS_ASSET_ID, TL.TAX_LOCAL_ID TAX_LOCAL_ID, 1 STATUS_CODE_ID
            from LS_ASSET              LA,
                 LS_TAX_LOCAL          TL,
                 LS_ILR,
                 LS_LEASE,
                 LS_LEASE_OPTIONS,
                 LS_TAX_DISTRICT_RATES,
                 ASSET_LOCATION,
                 LS_IMPORT_ASSET
           where LA.ILR_ID = LS_ILR.ILR_ID
             and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID
             and LS_LEASE.LEASE_ID = LS_LEASE_OPTIONS.LEASE_ID
             and LS_LEASE.CURRENT_REVISION = LS_LEASE_OPTIONS.REVISION
             and LS_LEASE_OPTIONS.TAX_SUMMARY_ID = TL.TAX_SUMMARY_ID
             and TL.TAX_LOCAL_ID = LS_TAX_DISTRICT_RATES.TAX_LOCAL_ID
             and LS_TAX_DISTRICT_RATES.LS_TAX_DISTRICT_ID = ASSET_LOCATION.TAX_DISTRICT_ID
             and LA.TAX_ASSET_LOCATION_ID = ASSET_LOCATION.ASSET_LOCATION_ID
             and LA.LS_ASSET_ID = LS_IMPORT_ASSET.LS_ASSET_ID
             and LS_IMPORT_ASSET.IMPORT_RUN_ID = A_RUN_ID
             and LS_IMPORT_ASSET.ILR_ID is not null);

			 update LS_ASSET set FMV = 0
			 where exists (
          select 1 from ls_import_asset a, ls_ilr i, ls_lease l, ls_lease_group g
            where a.ls_asset_id = LS_ASSET.ls_asset_id
			  and a.import_run_id = A_RUN_ID
			  and a.ilr_id = i.ilr_id
              and i.lease_id = l.lease_id
              and l.lease_group_id = g.lease_group_id
              and g.require_components = 1);
			 			 
		     MERGE INTO ls_ilr_options o 
				USING (SELECT SUM(Nvl(la.quantity, 0)) asset_quantity, la.ilr_id, m.revision 
							FROM ls_asset la, ls_ilr_asset_map m, ls_import_asset ia
						  WHERE la.ls_asset_id = m.ls_asset_id 
						     AND la.ls_asset_id = ia.ls_asset_id
							 AND la.ilr_id = m.ilr_id 
							 AND m.ilr_id = ia.ilr_id
							 AND ia.import_run_id = A_RUN_ID
							 AND m.revision > 0 
							 AND ia.ilr_id is not null
						  GROUP BY la.ilr_id, m.revision) a 
				ON (a.ilr_id = o.ilr_id AND a.revision = o.revision)
				WHEN MATCHED THEN 
				  UPDATE SET o.asset_quantity = a.asset_quantity;
      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_ASSETS;

   function F_VALIDATE_ASSETS(A_RUN_ID in number) return varchar2 is
      L_MSG          varchar2(2000);
      type ASSET_IMPORT_TABLE is table of LS_IMPORT_ASSET%rowtype;
      L_ASSET_IMPORT_TABLE ASSET_IMPORT_TABLE;
	  L_INDEX   		number;
      L_CC_SQL 			clob;
      CC_IMPORTED 		boolean;
      CC_REQUIRED_NUM  	number(22, 0);
      CC_MISSING 		varchar2(2000);
   begin
      --check we don't have duplicate LEASED_ASSET_NUMBERs
	  -- CJS 3/12/15 by company
      update LS_IMPORT_ASSET Z
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate leased asset number.'
       where IMPORT_RUN_ID = A_RUN_ID
         and (LEASED_ASSET_NUMBER, COMPANY_ID) in
             (select LEASED_ASSET_NUMBER, COMPANY_ID
                from (select LEASED_ASSET_NUMBER, COMPANY_ID, count(*) as MY_COUNT
                        from (select LEASED_ASSET_NUMBER, COMPANY_ID
                                from LS_ASSET
							   where COMPANY_ID in (select distinct COMPANY_ID from LS_IMPORT_ASSET)
                              union all
                              select LEASED_ASSET_NUMBER, COMPANY_ID
                                from (select distinct LEASED_ASSET_NUMBER, COMPANY_ID, UNIQUE_ASSET_IDENTIFIER
                                        from LS_IMPORT_ASSET
                                       where IMPORT_RUN_ID = A_RUN_ID))
                       group by LEASED_ASSET_NUMBER, COMPANY_ID)
               where MY_COUNT > 1);

      --check our CPR validations.
      --valid lease depr group
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Retirement unit not in sub_ledger -100 property unit.'
       where IMPORT_RUN_ID = A_RUN_ID
         and RETIREMENT_UNIT_ID not in
             (select RETIREMENT_UNIT_ID
                from RETIREMENT_UNIT
               where PROPERTY_UNIT_ID in
                     (select PROPERTY_UNIT_ID from PROPERTY_UNIT where SUBLEDGER_TYPE_ID = -100));

      --valid bus_segment
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' This business segment is not valid for this company.'
       where IMPORT_RUN_ID = A_RUN_ID
         and BUS_SEGMENT_ID not in
             (select BUS_SEGMENT_ID
                from COMPANY_BUS_SEGMENT_CONTROL
               where COMPANY_ID = LS_IMPORT_ASSET.COMPANY_ID);

      --valid WO for this bus_Segment/company
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Invalid work order for this company/business segment combination.'
       where IMPORT_RUN_ID = A_RUN_ID
         and WORK_ORDER_ID not in
             (select WORK_ORDER_ID
                from WORK_ORDER_CONTROL
               where BUS_SEGMENT_ID = LS_IMPORT_ASSET.BUS_SEGMENT_ID
                 and COMPANY_ID = LS_IMPORT_ASSET.COMPANY_ID);
      --valid asset locations
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE || ' This asset location is not valid for this company.'
       where IMPORT_RUN_ID = A_RUN_ID
         and ASSET_LOCATION_ID not in
             (select ASSET_LOCATION_ID
                from ASSET_LOCATION
               where MAJOR_LOCATION_ID in
                     (select MAJOR_LOCATION_ID
                        from COMPANY_MAJOR_LOCATION
                       where LS_IMPORT_ASSET.COMPANY_ID = COMPANY_ID));
      --valid utility_account
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' This utility account is not valid for this business segment.'
       where IMPORT_RUN_ID = A_RUN_ID
         and UTILITY_ACCOUNT_ID not in
             (select UTILITY_ACCOUNT_ID
                from UTILITY_ACCOUNT
               where BUS_SEGMENT_ID = LS_IMPORT_ASSET.BUS_SEGMENT_ID);

      --valid retirement_unit
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' This retirement unit is not valid for this utility account/business segment.'
       where IMPORT_RUN_ID = A_RUN_ID
         and RETIREMENT_UNIT_ID not in
             (select RETIREMENT_UNIT_ID
                from RETIREMENT_UNIT
               where PROPERTY_UNIT_ID in
                     (select PROPERTY_UNIT_ID
                        from UTIL_ACCT_PROP_UNIT
                       where BUS_SEGMENT_ID = LS_IMPORT_ASSET.BUS_SEGMENT_ID
                         and UTILITY_ACCOUNT_ID = LS_IMPORT_ASSET.UTILITY_ACCOUNT_ID));

      --valid sub account...
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' This sub account is not valid for this utility account/business segment.'
       where IMPORT_RUN_ID = A_RUN_ID
         and SUB_ACCOUNT_ID not in
             (select SUB_ACCOUNT_ID
                from SUB_ACCOUNT
               where BUS_SEGMENT_ID = LS_IMPORT_ASSET.BUS_SEGMENT_ID
                 and UTILITY_ACCOUNT_ID = LS_IMPORT_ASSET.UTILITY_ACCOUNT_ID);

      --valid property group
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' This property group is not valid for this retirement unit.'
       where IMPORT_RUN_ID = A_RUN_ID
         and PROPERTY_GROUP_ID not in
             (select PROPERTY_GROUP_ID
                from PROP_GROUP_PROP_UNIT
               where PROPERTY_UNIT_ID =
                     (select PROPERTY_UNIT_ID
                        from RETIREMENT_UNIT
                       where RETIREMENT_UNIT_ID = LS_IMPORT_ASSET.RETIREMENT_UNIT_ID));

      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Quantities must be positive.'
       where IMPORT_RUN_ID = A_RUN_ID
         and QUANTITY < 1;

      --If an ILR is defined then make sure the company is valid
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Not a valid ILR for this company.'
       where IMPORT_RUN_ID = A_RUN_ID
         and ILR_ID is not null
         and COMPANY_ID not in
             (select COMPANY_ID from LS_ILR where ILR_ID = LS_IMPORT_ASSET.ILR_ID);

      --make sure that the contract currency entered is valid in the case they don't use a translation
      update LS_IMPORT_ASSET a
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Selected currency is not a valid option.'
       where IMPORT_RUN_ID = A_RUN_ID
         and not exists
             (select 1 from CURRENCY b where a.contract_currency_id = b.currency_id);

      --If an ILR is defined then make sure the contract currency matches that of the asset
      update LS_IMPORT_ASSET a
         set ERROR_MESSAGE = ERROR_MESSAGE || ' ILR and Asset must have the same contract currency.'
       where IMPORT_RUN_ID = A_RUN_ID
         and ILR_ID is not null
         and CONTRACT_CURRENCY_ID not in
             (select CONTRACT_CURRENCY_ID from LS_ILR b inner join LS_LEASE c on b.LEASE_ID = c.LEASE_ID where b.ILR_ID = a.ILR_ID);

	--Front end does not allow 0 economic or expected lives so don't allow on import PP-50984
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Non-Zero Economic and Expected Lives are Required.'
       where IMPORT_RUN_ID = A_RUN_ID
         and (nvl(expected_life,0) = 0 or nvl(economic_life,0) = 0);

	--Don't allow 0 FMV import for non-component lease groups
      update LS_IMPORT_ASSET a
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-Zero FMV Required'
       where IMPORT_RUN_ID = A_RUN_ID
       and exists (select 1 from ls_ilr b, ls_lease c, ls_lease_group d
                          where a.ilr_id = b.ilr_id
                          and b.lease_id = c.lease_id
                          and c.lease_group_id = d.lease_group_id
                          and nvl(d.require_components,0) = 0
                          and nvl(a.fmv,0) = 0 ) ; 

	--Don't allow import of new asset to ILR already in In Service
      update LS_IMPORT_ASSET a
         set ERROR_MESSAGE = ERROR_MESSAGE || ' ILR mapped to new asset must be in Initiated status.'
       where IMPORT_RUN_ID = A_RUN_ID
         and a.ILR_ID is not null
         and not exists
             (select 1 from LS_ILR b where b.ILR_ID = a.ILR_ID and b.ilr_status_id = 1);

	-- PP-51229 - Add validation for class codes
      for L_CC_NO in 1 .. 20
       loop
	     L_CC_SQL := 'DECLARE BEGIN
	     update LS_IMPORT_ASSET a
		    set ERROR_MESSAGE = ERROR_MESSAGE || '' Class Code '||to_char(L_CC_NO)||' is not a valid Asset class code.''
		   where IMPORT_RUN_ID = '||to_char(A_RUN_ID)||'
			  and CLASS_CODE_ID'||to_char(L_CC_NO)||' is not null
			  and CLASS_CODE_VALUE'||to_char(L_CC_NO)||' is not null
			  and  not exists (select 1 from CLASS_CODE b where a.CLASS_CODE_ID'||to_char(L_CC_NO)||' = b.CLASS_CODE_ID and b.ASSET_INDICATOR = 1); 

	     update LS_IMPORT_ASSET a
		    set ERROR_MESSAGE = ERROR_MESSAGE || '' Class Code Value '||to_char(L_CC_NO)||' is not a valid value.''
		   where IMPORT_RUN_ID = '||to_char(A_RUN_ID)||'
			  and CLASS_CODE_ID'||to_char(L_CC_NO)||' is not null
			  and CLASS_CODE_VALUE'||to_char(L_CC_NO)||' is not null
			  and exists (select 1 from CLASS_CODE b 
			               where b.CLASS_CODE_ID = a.CLASS_CODE_ID'||to_char(L_CC_NO)||' 
				            and b.ASSET_INDICATOR = 1 
                            and lower(CLASS_CODE_TYPE) = ''standard'')
			  and not exists (select 1 from CLASS_CODE_VALUES C where C.CLASS_CODE_ID = a.CLASS_CODE_ID'||to_char(L_CC_NO)||' and c.VALUE = a.CLASS_CODE_VALUE'||to_char(L_CC_NO)||');

	      END;';

	   execute immediate L_CC_SQL;
      end loop; 
   -- PP-51674 - Check for Required Class Codes

     select count(*) into CC_REQUIRED_NUM
     from class_code
     where asset_indicator = 1
     and required = 1;

     if CC_REQUIRED_NUM > 0 then
            L_MSG := 'Check for required class codes';

            select * bulk collect
                into L_ASSET_IMPORT_TABLE
                from LS_IMPORT_ASSET
            where IMPORT_RUN_ID = A_RUN_ID;

            for L_INDEX in 1 .. L_ASSET_IMPORT_TABLE.COUNT
            loop
                CC_IMPORTED := false;
                L_CC_SQL := 'select listagg(description, '','') within group (order by description) description
                                from class_Code where class_code_id in (
                                select class_code_id from class_code where asset_indicator = 1 and required = 1
                                minus
                                select class_code_id from class_code where class_code_id in (';

                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE1 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE2 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE3 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE4 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE5 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE6 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE7 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE8 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE9 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE10 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE11 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE12 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE13 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE14 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE15 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE16 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE17 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE18 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE19 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19) || ',';
                end if;
                if L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20 is not null and L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE20 is not null then
                    CC_IMPORTED := true;
                    L_CC_SQL  := L_CC_SQL || TO_CHAR(L_ASSET_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20) || ',';
                end if;

                L_CC_SQL :=  trim(TRAILING ',' FROM L_CC_SQL);
                L_CC_SQL := L_CC_SQL || '))';

                if CC_IMPORTED then
                      execute immediate L_CC_SQL into CC_MISSING;

                      if CC_MISSING is not null then
                              UPDATE LS_IMPORT_ASSET a
                              SET    error_message = error_message
                                                    || ' Required Asset Class Codes are Missing:' || CC_MISSING
                              WHERE  import_run_id = a_run_id
                              and line_id = L_ASSET_IMPORT_TABLE(L_INDEX).LINE_ID;
                      end if;
                else
                  --There are required class codes and none were imported, so all are missing
                  L_CC_SQL := 'select listagg(description, '','') within group (order by description) description
                                from class_Code where asset_indicator = 1 and required = 1';

                  execute immediate L_CC_SQL into CC_MISSING;

                  if CC_MISSING is not null then
                          UPDATE LS_IMPORT_ASSET a
                          SET    error_message = error_message
                                                || ' Required Asset Class Codes are Missing:' || CC_MISSING
                          WHERE  import_run_id = a_run_id
                          and line_id = L_ASSET_IMPORT_TABLE(L_INDEX).LINE_ID;
                  end if;
                end if;
              end loop;

     end if;
	  
      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_ASSETS;

   --##########################################################################
   --                            Components
   --##########################################################################
   function F_IMPORT_COMPONENTS(A_RUN_ID in number) return varchar2 is
      L_MSG              varchar2(2000);
      L_COMPONENT_IMPORT COMPONENT_IMPORT_TABLE;
      L_INDEX            number;
   begin
	/* CJS 2/18/15 Adding portion for component invoices */
	  update ls_import_component
	  set component_id = null
	  where IMPORT_RUN_ID = A_RUN_ID;

      update ls_import_component z
	  set component_id = ls_component_seq.nextval
	  where LINE_ID in (select LINE_ID
                        from (select LINE_ID,
                                     ROW_NUMBER() OVER(partition by UNIQUE_COMPONENT_IDENTIFIER order by UNIQUE_COMPONENT_IDENTIFIER, LINE_ID) NUM
                                from LS_IMPORT_COMPONENT
                               where IMPORT_RUN_ID = A_RUN_ID)
                        where NUM = 1)
        and IMPORT_RUN_ID = A_RUN_ID;

	  update ls_import_component z set component_id = (
		select -component_id
		from ls_import_component a
		where a.unique_component_identifier = z.unique_component_identifier
		  and a.component_id is not null)
	  where z.component_id is null
	    and import_run_id = A_RUN_ID;

      update LS_IMPORT_COMPONENT
         set LS_COMP_STATUS_ID = 1
       where IMPORT_RUN_ID = A_RUN_ID;

      update LS_IMPORT_COMPONENT
         set LS_COMP_STATUS_ID = 2
       where DATE_RECEIVED is not null
         and IMPORT_RUN_ID = A_RUN_ID;

      update LS_IMPORT_COMPONENT
         set LS_COMP_STATUS_ID = 3
       where DATE_RECEIVED is not null
         and LS_ASSET_ID is not null
		 and INVOICE_NUMBER is not null
         and IMPORT_RUN_ID = A_RUN_ID;

      select * bulk collect
        into L_COMPONENT_IMPORT
        from LS_IMPORT_COMPONENT
       where IMPORT_RUN_ID = A_RUN_ID
	     and nvl(component_id,-1) > 0;

      forall L_INDEX in indices of L_COMPONENT_IMPORT
         insert into LS_COMPONENT
            (COMPONENT_ID, LS_COMP_STATUS_ID, COMPANY_ID, DATE_RECEIVED, DESCRIPTION,
             LONG_DESCRIPTION, SERIAL_NUMBER, PO_NUMBER, AMOUNT, LS_ASSET_ID,
             INTERIM_INTEREST, manufacturer, model)

            select L_COMPONENT_IMPORT(L_INDEX).COMPONENT_ID,
                   L_COMPONENT_IMPORT(L_INDEX).LS_COMP_STATUS_ID,
                   L_COMPONENT_IMPORT(L_INDEX).COMPANY_ID,
                   TO_DATE(L_COMPONENT_IMPORT(L_INDEX).DATE_RECEIVED, 'yyyymmdd'),
                   L_COMPONENT_IMPORT(L_INDEX).DESCRIPTION,
                   L_COMPONENT_IMPORT(L_INDEX).LONG_DESCRIPTION,
                   L_COMPONENT_IMPORT(L_INDEX).SERIAL_NUMBER,
                   L_COMPONENT_IMPORT(L_INDEX).PO_NUMBER,
                   L_COMPONENT_IMPORT(L_INDEX).AMOUNT,
                   L_COMPONENT_IMPORT(L_INDEX).LS_ASSET_ID,
                   L_COMPONENT_IMPORT(L_INDEX).INTERIM_INTEREST,
                   L_COMPONENT_IMPORT(L_INDEX).MANUFACTURER,
                   L_COMPONENT_IMPORT(L_INDEX).MODEL
              from DUAL;

	  select * bulk collect
        into L_COMPONENT_IMPORT
        from LS_IMPORT_COMPONENT
       where IMPORT_RUN_ID = A_RUN_ID
	   and trim(INVOICE_NUMBER) is not null;

	   forall L_INDEX in indices of L_COMPONENT_IMPORT
         insert into LS_COMPONENT_CHARGE
            (ID, COMPONENT_ID, INTERIM_INTEREST_START_DATE, INVOICE_DATE, INVOICE_NUMBER, AMOUNT)
            select (select max(ID) from ls_component_charge)+ROWNUM,
				   ABS(L_COMPONENT_IMPORT(L_INDEX).COMPONENT_ID),
                   TO_DATE(L_COMPONENT_IMPORT(L_INDEX).INTERIM_INTEREST_START_DATE, 'yyyymmdd'),
                   TO_DATE(L_COMPONENT_IMPORT(L_INDEX).INVOICE_DATE, 'yyyymmdd'),
                   L_COMPONENT_IMPORT(L_INDEX).INVOICE_NUMBER,
                   L_COMPONENT_IMPORT(L_INDEX).INVOICE_AMOUNT
              from DUAL;

	 update LS_ASSET A
        set FMV =
              (select sum(CC.AMOUNT)
			   from LS_COMPONENT_CHARGE CC, LS_COMPONENT C
			   where A.LS_ASSET_ID = C.LS_ASSET_ID
				and C.COMPONENT_ID = CC.COMPONENT_ID
				and INTERIM_INTEREST_START_DATE is not null)
      where exists (
	     select 1 from LS_COMPONENT C, LS_IMPORT_COMPONENT IC, LS_COMPONENT_CHARGE CC
		 where A.LS_ASSET_ID = C.LS_ASSET_ID and C.COMPONENT_ID = ABS(IC.COMPONENT_ID)
		 and C.COMPONENT_ID = CC.COMPONENT_ID
		 and cc.interim_interest_start_date is not null
		 and import_run_id = A_RUN_ID);
      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;

   end F_IMPORT_COMPONENTS;

   function F_VALIDATE_COMPONENTS(A_RUN_ID in number) return varchar2 is
      L_MSG              varchar2(2000);
      L_COMPONENT_IMPORT COMPONENT_IMPORT_TABLE;
      L_INDEX            number;
   begin
      --    update ls_import_component
      --    set error_message = error_message || ' duplicate component description.'
      --    where import_run_id = a_run_id
      --     and description in
      --       (select description
      --         from (select description, count(*) as my_count
      --            from (select description
      --                 from ls_import_component
      --               union all
      --               select description
      --                 from (select distinct description
      --                     from ls_import_component
      --                    where import_run_id = a_run_id))
      --             group by description)
      --        where my_count > 1);
      --Cannot have asset assigned and no Date Received

	  L_MSG:= 'Checking for Serial Number on Received Assets';
      update LS_IMPORT_COMPONENT
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Components with a received date populated must have serial number also populated.'
       where IMPORT_RUN_ID = A_RUN_ID
         and DATE_RECEIVED is not null
		 and trim(serial_number) is null;

	  L_MSG:= 'Checking for invoice required fields';
      update LS_IMPORT_COMPONENT
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' If importing component invoices, invoice number/amount/date, II start date, and leased asset are all required.'
       where IMPORT_RUN_ID = A_RUN_ID
         and (trim(invoice_number) is not null
		   or invoice_amount is not null
		   or invoice_date is not null
		   or interim_interest_start_date is not null)
         and (trim(invoice_number) is null
		   or invoice_amount is null
		   or invoice_date is null
		   or interim_interest_start_date is null
		   or ls_asset_xlate is null);

	  L_MSG:= 'Checking for aligned unique component identifier';
      update LS_IMPORT_COMPONENT z
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Unique component identifier must have identical description, long description, company, date received, asset, serial number, po number, and amount (comm. cost).'
       where IMPORT_RUN_ID = A_RUN_ID
         and exists(
               select 1
                 from ls_import_component a
                where a.unique_component_identifier = z.unique_component_identifier
                  and a.IMPORT_RUN_ID = A_RUN_ID
                  and (upper(trim(a.company_xlate)) <> upper(trim(z.company_xlate))
                    or upper(trim(a.description)) <> upper(trim(z.description))
                    or upper(trim(a.long_description)) <> upper(trim(z.long_description))
                    or upper(trim(a.serial_number)) <> upper(trim(z.serial_number))
                    or upper(trim(a.po_number)) <> upper(trim(z.po_number))
                  or a.amount <> z.amount));
	  return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_COMPONENTS;

   --##########################################################################
   --                     LEASE INTERIM INTEREST RATES
   --##########################################################################

   function F_IMPORT_INTERIM_RATES(A_RUN_ID in number) return varchar2 is
      type RATE_IMPORT is table of LS_IMPORT_INTERIM_RATES%rowtype;
	  L_RATE_IMPORT RATE_IMPORT;
      L_INDEX           number;
	  L_MSG             varchar2(2000);

   begin
      select * bulk collect
        into L_RATE_IMPORT
        from LS_IMPORT_INTERIM_RATES
       where IMPORT_RUN_ID = A_RUN_ID;

   /* CJS 2/23/15 Changing to day instead of month */
	L_MSG:='Inserting Interim Interest Rates';

      forall L_INDEX in indices of L_RATE_IMPORT
         insert into LS_LEASE_INTERIM_RATES
            (LEASE_ID, month, RATE)
            select L_RATE_IMPORT(L_INDEX).LEASE_ID,
                   TO_DATE(L_RATE_IMPORT(L_INDEX).MONTH, 'yyyymmdd'),
                   L_RATE_IMPORT(L_INDEX).RATE
              from DUAL;
      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_INTERIM_RATES;

   function F_VALIDATE_INTERIM_RATES(A_RUN_ID in number) return varchar2 is
   begin
      return 'OK';
   end;

   --##########################################################################
   --                     Lease FLOATING RATES
   --##########################################################################

	FUNCTION F_IMPORT_LEASE_FLOAT_RATES(A_RUN_ID IN NUMBER) RETURN VARCHAR2 IS
	  L_MSG VARCHAR2(2000);
	  TYPE RATE_IMPORT IS TABLE OF LS_IMPORT_FLOAT_RATES%ROWTYPE;
	  L_RATE_IMPORT RATE_IMPORT;
	  L_INDEX       NUMBER;
	BEGIN
	  SELECT * BULK COLLECT INTO L_RATE_IMPORT FROM LS_IMPORT_FLOAT_RATES WHERE IMPORT_RUN_ID = A_RUN_ID;

	  L_MSG := 'Merging Floating Rates';

	  FORALL L_INDEX IN INDICES OF L_RATE_IMPORT
					   
						MERGE INTO ls_lease_floating_rates a USING
						(SELECT L_RATE_IMPORT(L_INDEX).LEASE_ID lease_id, To_Date(L_RATE_IMPORT(L_INDEX).EFFECTIVE_DATE, 'mmddyyyy') effective_date,L_RATE_IMPORT(L_INDEX).RATE rate
						   FROM dual) b ON(a.lease_id = b.lease_id AND a.effective_date = b.effective_date)
		WHEN MATCHED THEN UPDATE SET a.rate = b.rate WHEN NOT MATCHED THEN INSERT(a.lease_id, a.effective_date, a.rate) VALUES(b.lease_id, b.effective_date, b.rate);

	  RETURN 'OK';
	EXCEPTION
	  WHEN OTHERS THEN
		L_MSG := SUBSTR(L_MSG || ': ' || SQLERRM, 1, 2000);
		RETURN L_MSG;
	END F_IMPORT_LEASE_FLOAT_RATES;

	FUNCTION F_VALIDATE_LEASE_FLOAT_RATES(A_RUN_ID IN NUMBER) RETURN VARCHAR2 IS
	BEGIN
		--No Validations at this time... Should probably prevent rates being uploaded for some reason or another.
	  RETURN 'OK';
	END;


   --##########################################################################
   --                     DISTRICT TAX RATES
   --##########################################################################

   function F_IMPORT_DISTRICT_TAX_RATES(A_RUN_ID in number) return varchar2 is
      L_MSG             varchar2(2000);
      type RATE_IMPORT is table of LS_IMPORT_D_TAX_RATES%rowtype;
	  L_RATE_IMPORT RATE_IMPORT;
      L_INDEX           number;
   begin
      select * bulk collect
        into L_RATE_IMPORT
        from LS_IMPORT_D_TAX_RATES
       where IMPORT_RUN_ID = A_RUN_ID;
/* CJS 2/23/15 Changing to day instead of month */
	L_MSG:='Inserting Tax District Rates';

      forall L_INDEX in indices of L_RATE_IMPORT
         insert into LS_TAX_DISTRICT_RATES
            (TAX_LOCAL_ID, EFFECTIVE_DATE, RATE, LS_TAX_DISTRICT_ID)
            select L_RATE_IMPORT(L_INDEX).TAX_LOCAL_ID,
                   TO_DATE(L_RATE_IMPORT(L_INDEX).EFFECTIVE_DATE, 'yyyymmdd'),
                   L_RATE_IMPORT(L_INDEX).RATE,
				   L_RATE_IMPORT(L_INDEX).LS_TAX_DISTRICT_ID
              from DUAL;
      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_DISTRICT_TAX_RATES;

   function F_VALIDATE_DISTRICT_TAX_RATES(A_RUN_ID in number) return varchar2 is
   begin
      return 'OK';
   end;

   --##########################################################################
   --                     STATE TAX RATES
   --##########################################################################

   function F_IMPORT_STATE_TAX_RATES(A_RUN_ID in number) return varchar2 is
      L_MSG             varchar2(2000);
      type RATE_IMPORT is table of LS_IMPORT_S_TAX_RATES%rowtype;
	  L_RATE_IMPORT RATE_IMPORT;
      L_INDEX           number;
   begin
      select * bulk collect
        into L_RATE_IMPORT
        from LS_IMPORT_S_TAX_RATES
       where IMPORT_RUN_ID = A_RUN_ID;
/* CJS 2/23/15 Changing to day instead of month */
	L_MSG:='Importing State Tax Rates';
      forall L_INDEX in indices of L_RATE_IMPORT
         insert into LS_TAX_STATE_RATES
            (TAX_LOCAL_ID, EFFECTIVE_DATE, RATE, STATE_ID)
            select L_RATE_IMPORT(L_INDEX).TAX_LOCAL_ID,
                   TO_DATE(L_RATE_IMPORT(L_INDEX).EFFECTIVE_DATE, 'yyyymmdd'),
                   L_RATE_IMPORT(L_INDEX).RATE,
				   L_RATE_IMPORT(L_INDEX).STATE_ID
              from DUAL;
      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_STATE_TAX_RATES;

   function F_VALIDATE_STATE_TAX_RATES(A_RUN_ID in number) return varchar2 is
   begin
      return 'OK';
   end;

   /* CJS 2/23/15 Adding Allocation Row Import */
   --##########################################################################
   --                     JE ALLOCATION ROWS
   --##########################################################################

   function F_IMPORT_ALLOCATION_ROWS(A_RUN_ID in number) return varchar2 is
      L_MSG             varchar2(32000);
      type ROW_IMPORT is table of LS_IMPORT_ALLOC_ROWS%rowtype;
	  L_ROW_IMPORT ROW_IMPORT;
      L_INDEX           number;
	  L_FIELDS			varchar2(2000);
	  L_USED            varchar2(2000);
	  L_SQLS			varchar2(2000);
   begin
       L_MSG    := 'Collecting Allocation Row columns';
	   L_FIELDS := 'SCO_BILLING_TYPE_ID, VALIDATION_MESSAGE, DESCRIPTION, TYPE, LS_JE_TRANS_TYPE, TAX_LOCAL_ID, PERCENT, ';
      for COL in (select PKG_PP_COMMON.F_CLEAN_STRING(Description) ELEMENT_COLUMN from CR_ELEMENTS where ELEMENT_COLUMN is not null order by "ORDER")
      loop
         L_FIELDS := L_FIELDS || COL.ELEMENT_COLUMN || ', ';
      end loop;

      L_FIELDS := RTRIM(L_FIELDS, ', ');

	  select listagg('nvl(trim('||COLUMN_NAME||'),''*'')',', ') within group (order by FIELD_ID)
	    into L_USED
	    from PP_IMPORT_TEMPLATE_FIELDS, PP_IMPORT_RUN
	   where IMPORT_TYPE_ID = 261
	     and COLUMN_NAME like 'element%'
       and IMPORT_RUN_ID = A_RUN_ID
       and PP_IMPORT_TEMPLATE_FIELDS.IMPORT_TEMPLATE_ID = PP_IMPORT_RUN.IMPORT_TEMPLATE_ID;

	  /* CJS 6/22/15 */
      L_MSG := 'Clearing out old allocation row records by asset/trans type combination';

	  delete from CR_DERIVER_CONTROL
      where upper(trim(TYPE)) IN ('LESSEE','LESSEE OFFSET')
      and (STRING) in (
         select LS_ASSET_ID||':'||TRANS_TYPE
         from LS_IMPORT_ALLOC_ROWS
         where IMPORT_RUN_ID = A_RUN_ID);
	  L_MSG    := 'Building Deriver Control insert';
      L_SQLS   := 'insert into CR_DERIVER_CONTROL (STRING, ID, ' || L_FIELDS || ')';
      L_SQLS   := L_SQLS || ' select LS_ASSET_ID || '':'' || TRANS_TYPE ' ||
                   ', costrepository.nextval, null, null, ''Lease Import Run:' || TO_CHAR(A_RUN_ID) || ''', ''Lessee'', ';
      L_SQLS   := L_SQLS ||
	              'TRANS_TYPE, TAX_LOCAL_ID, PERCENT, ' || L_USED;
	  L_SQLS   := L_SQLS ||
                  ' from LS_IMPORT_ALLOC_ROWS where IMPORT_RUN_ID = '||TO_CHAR(A_RUN_ID);
      execute immediate L_SQLS;

      L_USED   := '';

      select listagg(decode(to_char(instr(lower(PP_IMPORT_COLUMN.COLUMN_TYPE), 'char')),'0', 'null','''*'''),', ') within group (order by FIELD_ID)
	    into L_USED
	    from PP_IMPORT_TEMPLATE_FIELDS, PP_IMPORT_RUN, PP_IMPORT_COLUMN
	   where PP_IMPORT_TEMPLATE_FIELDS.IMPORT_TYPE_ID = 261
	     and PP_IMPORT_TEMPLATE_FIELDS.COLUMN_NAME like 'element%'
       and PP_IMPORT_RUN.IMPORT_RUN_ID = A_RUN_ID
       and PP_IMPORT_TEMPLATE_FIELDS.IMPORT_TEMPLATE_ID = PP_IMPORT_RUN.IMPORT_TEMPLATE_ID
       and PP_IMPORT_COLUMN.IMPORT_TYPE_ID = PP_IMPORT_TEMPLATE_FIELDS.IMPORT_TYPE_ID
       and lower(trim(PP_IMPORT_COLUMN.COLUMN_NAME)) = lower(trim(PP_IMPORT_TEMPLATE_FIELDS.COLUMN_NAME));
		 
      L_MSG    := 'Building Deriver Control Offset insert';
      L_SQLS   := '';

	  L_SQLS   := 'insert into CR_DERIVER_CONTROL (STRING, ID, ' || L_FIELDS || ')';
      L_SQLS   := L_SQLS || ' select LS_ASSET_ID || '':'' || TRANS_TYPE ' ||
                   ', costrepository.nextval, null, null, ''Lease Import Run:' || TO_CHAR(A_RUN_ID) || ''', ''Lessee Offset'', ';
      L_SQLS   := L_SQLS ||
	              'TRANS_TYPE, null, -1, ' || L_USED;
	  L_SQLS   := L_SQLS ||
                  ' from (select distinct LS_ASSET_ID, TRANS_TYPE from LS_IMPORT_ALLOC_ROWS where IMPORT_RUN_ID = ' || TO_CHAR(A_RUN_ID) || ')';
      execute immediate L_SQLS;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || L_SQLS || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_ALLOCATION_ROWS;

   function F_VALIDATE_ALLOCATION_ROWS(A_RUN_ID in number) return varchar2 is
      L_MSG             varchar2(2000);
      type ROW_IMPORT is table of LS_IMPORT_ALLOC_ROWS%rowtype;
	  L_ROW_IMPORT ROW_IMPORT;
      L_INDEX           number;
	  L_FIELDS			varchar2(2000);
	  L_USED            varchar2(2000);
	  L_SQLS			varchar2(2000);
   begin

	L_MSG := 'Checking allocation percentages';
   update LS_IMPORT_ALLOC_ROWS z
   set ERROR_MESSAGE = ERROR_MESSAGE || ' Percentages do not sum to 1 for Asset/Trans Type/Tax Local Combination.'
   where IMPORT_RUN_ID = A_RUN_ID
   and exists(
      select 1
	  from LS_IMPORT_ALLOC_ROWS
	  where TRANS_TYPE = z.TRANS_TYPE
	  and LS_ASSET_ID = z.LS_ASSET_ID
	  and nvl(TAX_LOCAL_ID,-1) = nvl(z.TAX_LOCAL_ID,-1)
	  and IMPORT_RUN_ID = A_RUN_ID
	  group by LS_ASSET_ID, TRANS_TYPE, TAX_LOCAL_ID
	  having sum(PERCENT) <> 1);

	L_MSG := 'Checking for Tax Local ID';
   update LS_IMPORT_ALLOC_ROWS z
   set ERROR_MESSAGE = ERROR_MESSAGE || ' Tax Trans Types should have a Tax Local ID specified.'
   where IMPORT_RUN_ID = A_RUN_ID
   and exists(
      select 1
	  from LS_IMPORT_ALLOC_ROWS A, JE_TRANS_TYPE JE
	  where A.TRANS_TYPE = JE.TRANS_TYPE
	  and upper(JE.DESCRIPTION) like '%TAX%'
	  and A.TRANS_TYPE = Z.TRANS_TYPE
	  and A.LS_ASSET_ID = Z.LS_ASSET_ID
	  and Z.TAX_LOCAL_ID is null
	  and IMPORT_RUN_ID = A_RUN_ID);

   L_MSG  := 'Checking for valid CR elements';
   for col in (select ELEMENT_ID, ELEMENT_COLUMN, GL_ELEMENT, ELEMENT_TABLE, "ORDER" from CR_ELEMENTS order by "ORDER")
   loop
   L_MSG  := 'Checking for valid CR elements:'||col.ELEMENT_COLUMN||':'||col.ELEMENT_TABLE||':'||col."ORDER";
   L_SQLS := '';
   L_USED := '';

   select COLUMN_NAME
   into L_USED
   from ALL_TAB_COLS
   where table_name = upper(col.ELEMENT_TABLE)
   and COLUMN_ID = 1;

   L_SQLS := 'update LS_IMPORT_ALLOC_ROWS Z set ERROR_MESSAGE = ERROR_MESSAGE || ''' ||
				col.GL_ELEMENT || ' not a valid element in CR table ' || TO_CHAR(UPPER(col.ELEMENT_TABLE));
   L_SQLS := L_SQLS || '. '' where IMPORT_RUN_ID = ' || TO_CHAR(A_RUN_ID) || ' and not exists ('||
				'select 1 from LS_IMPORT_ALLOC_ROWS A, ' || TO_CHAR(col.ELEMENT_TABLE) || ' ET ';
   L_SQLS := L_SQLS || ' where A.TRANS_TYPE = Z.TRANS_TYPE and A.LS_ASSET_ID = Z.LS_ASSET_ID '||
				' and ET.' || TO_CHAR(L_USED) || ' = Z.ELEMENT_' || TO_CHAR(col."ORDER") || ') ';
   L_SQLS := L_SQLS || ' and trim(Z.ELEMENT_' || TO_CHAR(col."ORDER") ||') is not null';
   L_SQLS := L_SQLS || ' AND NOT EXISTS (SELECT 1 FROM cr_validation_exclusion WHERE element_id = ' || To_Char(col.ELEMENT_ID) || ') ';


   execute immediate L_SQLS;

   end loop;

   return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || L_SQLS || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_ALLOCATION_ROWS;

   /* CJS 3/23/15 Adding Asset Tax Import */
   function F_IMPORT_ASSET_TAXES(A_RUN_ID in number) return varchar2 is
      L_MSG             varchar2(2000);
   begin
	L_MSG:='Importing Asset Taxes';
         merge into LS_ASSET_TAX_MAP Z
            using
            (select distinct LS_ASSET_ID,
			       TAX_LOCAL_ID,
				   STATUS_CODE_ID
              from LS_IMPORT_ASSET_TAXES
			  where IMPORT_RUN_ID = A_RUN_ID) I
			  on (Z.LS_ASSET_ID = I.LS_ASSET_ID and Z.TAX_LOCAL_ID = I.TAX_LOCAL_ID)
			  when matched then
			  update set Z.STATUS_CODE_ID = I.STATUS_CODE_ID
			  when not matched then
			  insert (Z.LS_ASSET_ID, Z.TAX_LOCAL_ID, Z.STATUS_CODE_ID)
			  values (I.LS_ASSET_ID, I.TAX_LOCAL_ID, I.STATUS_CODE_ID);

   return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_ASSET_TAXES;

   function F_VALIDATE_ASSET_TAXES(A_RUN_ID in number) return varchar2 is
      L_MSG             varchar2(2000);
   begin
   L_MSG:='Checking for valid active taxes.';
   update LS_IMPORT_ASSET_TAXES Z
      set ERROR_MESSAGE = ' Tax not a valid active tax for that asset. Check Asset Location on asset.'
    where IMPORT_RUN_ID = A_RUN_ID
	  and STATUS_CODE_ID = 1
	  and (LS_ASSET_ID, TAX_LOCAL_ID) not in (
	   select
		A.LS_ASSET_ID,
		TL.TAX_LOCAL_ID
	from LS_ASSET A, ASSET_LOCATION AL, LS_TAX_LOCAL TL,
		(select tsr2.*, row_number() over(partition by tsr2.tax_local_id, tsr2.state_id order by tsr2.effective_date desc) as the_row
		from LS_TAX_STATE_RATES tsr2) TSR
	where TSR.TAX_LOCAL_ID = TL.TAX_LOCAL_ID
	and TL.TAX_SUMMARY_ID = 1
	and TSR.STATE_ID = AL.STATE_ID
	and AL.ASSET_LOCATION_ID = A.ASSET_LOCATION_ID
	and A.LS_ASSET_ID = Z.LS_ASSET_ID
	and tsr.the_row = 1
	union
	select
		A.LS_ASSET_ID,
		TL.TAX_LOCAL_ID
	from LS_ASSET A, LS_LOCATION_TAX_DISTRICT AL, LS_TAX_LOCAL TL,
		(select tsr2.*, row_number() over(partition by tsr2.tax_local_id, tsr2.ls_tax_district_id order by tsr2.effective_date desc) as the_row
		from LS_TAX_DISTRICT_RATES tsr2) TDR
	where TDR.TAX_LOCAL_ID = TL.TAX_LOCAL_ID
	and TL.TAX_SUMMARY_ID = 1
	and TDR.LS_TAX_DISTRICT_ID = AL.TAX_DISTRICT_ID
	and AL.ASSET_LOCATION_ID = A.ASSET_LOCATION_ID
	and A.LS_ASSET_ID = Z.LS_ASSET_ID
	and tdr.the_row = 1);

	L_MSG:='Checking for duplicate tax rows with different statuses.';
	update LS_IMPORT_ASSET_TAXES Z
	   set ERROR_MESSAGE = ' Duplicate Tax row for asset with different status_code_id.'
	 where IMPORT_RUN_ID = A_RUN_ID
	   and (LS_ASSET_ID, TAX_LOCAL_ID) in (
	    select
		LS_ASSET_ID, TAX_LOCAL_ID
		from (
		   select LS_ASSET_ID, TAX_LOCAL_ID, COUNT(*)
		   from LS_IMPORT_ASSET_TAXES
		   where IMPORT_RUN_ID = A_RUN_ID
		   group by LS_ASSET_ID, TAX_LOCAL_ID
		   having count(distinct status_code_id) >1));

   return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_ASSET_TAXES;

    --##########################################################################
  --                            ILR RENEWALS
  --##########################################################################

    --##########################################################################
  --                            ILR RENEWALS
  --##########################################################################

  FUNCTION F_import_ilr_renewals( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Assign Revisions to those not supplied with the max() Initiated Revision for the ILR
      UPDATE LS_IMPORT_ILR_RENEWAL a
      SET    revision = ( SELECT Max( revision )
                          FROM   LS_ILR_APPROVAL b
                          WHERE  a.ilr_id = b.ilr_id AND
                                 b.approval_status_id IN ( 1 ) AND
								 b.revision > 0)
      WHERE  import_run_id = a_run_id AND
             a.revision IS NULL;

      --Remove from Renewal Options
      DELETE FROM LS_ILR_RENEWAL_OPTIONS
      WHERE  ilr_renewal_id IN
             ( SELECT ilr_renewal_id
               FROM   LS_ILR_RENEWAL a
               WHERE  ( a.ilr_id, a.revision ) IN
                      ( SELECT b.ilr_id,
                               b.revision
                        FROM   LS_IMPORT_ILR_RENEWAL b
                        WHERE  import_run_id = a_run_id ) );

      --Remove from Renewal
      DELETE FROM LS_ILR_RENEWAL a
      WHERE  ( a.ilr_id, a.revision ) IN
             ( SELECT b.ilr_id,
                      b.revision
               FROM   LS_IMPORT_ILR_RENEWAL b
               WHERE  import_run_id = a_run_id );

      --Assign new Renewal ID from sequence to first rows for the matching ILR and Revision
      UPDATE LS_IMPORT_ILR_RENEWAL
      SET    ilr_renewal_id = ls_ilr_renewal_seq.NEXTVAL
      WHERE  line_id IN
             ( SELECT line_id
               FROM   ( SELECT line_id,
                               Row_number( )
                                 over (
                                   PARTITION BY ilr_id, revision, ilr_renewal_option_id
                                   ORDER BY ilr_id, revision, ilr_renewal_option_id) NUM
                        FROM   LS_IMPORT_ILR_RENEWAL
                        WHERE  import_run_id = a_run_id )
               WHERE  num = 1 ) AND
             import_run_id = a_run_id;

      --Insert headers to lsr_ilr_renewal
      INSERT INTO LS_ILR_RENEWAL
                  (ilr_renewal_id,
                   ilr_id,
                   revision,
                   renewal_options_id)
      SELECT ilr_renewal_id,
             ilr_id,
             revision,
             ilr_renewal_option_id
      FROM   LS_IMPORT_ILR_RENEWAL
      WHERE  import_run_id = a_run_id AND
             ilr_renewal_id IS NOT NULL;

      --assign the renewal id to all the other renewal options
      UPDATE LS_IMPORT_ILR_RENEWAL a
      SET    ilr_renewal_id = ( SELECT DISTINCT ilr_renewal_id
                                FROM   LS_IMPORT_ILR_RENEWAL b
                                WHERE  b.import_run_id = a_run_id AND
                                       a.ilr_id = b.ilr_id AND
                                       a.revision = b.revision AND
                                       a.ilr_renewal_option_id = b.ilr_renewal_option_id AND
                                       b.ilr_renewal_id IS NOT NULL )
      WHERE  a.import_run_id = a_run_id AND
             ilr_renewal_id IS NULL;

      --Insert to ls_ilr_renewal_options
      INSERT INTO LS_ILR_RENEWAL_OPTIONS
                  (ilr_renewal_option_id,
                   ilr_renewal_id,
                   ilr_renewal_probability_id,
                   payment_freq_id,
                   number_of_terms,
                   amount_per_term,
                   renewal_start_date,
                   notice_requirement_months)
      SELECT ls_ilr_renewal_options_seq.NEXTVAL,
             ilr_renewal_id,
             ilr_renewal_prob_id,
             renewal_frequency_id,
             renewal_number_of_terms,
             renewal_amount_per_term,
             To_date( renewal_start_date, 'mm/dd/yyyy' ),
             renewal_notice
      FROM   LS_IMPORT_ILR_RENEWAL
      WHERE  ilr_renewal_id IS NOT NULL AND
             import_run_id = a_run_id;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_import_ilr_renewals;

  FUNCTION F_validate_ilr_renewals( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Validate this is a valid Revision for this ILR if provided
      UPDATE LS_IMPORT_ILR_RENEWAL a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision does not exist. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision );

      --Check supplied Revision is open
      UPDATE LS_IMPORT_ILR_RENEWAL a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision is in a non-editable status. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision AND
                          b.approval_status_id IN ( 1 ) );

      --Validate where we have revisions in "Initiated" status for the ILR for us to apply these to
      UPDATE LS_IMPORT_ILR_RENEWAL a
      SET    error_message = error_message
                             || ' No editable revision exists for the ILR. Please create a new revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          b.approval_status_id IN ( 1 ) AND	
						  b.revision > 0);

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_validate_ilr_renewals;

  FUNCTION F_import_ilr_idc( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
    
	 --Assign Revision to IDCs imported without Revision (Assign Max Initiated Revision for ILR)
      UPDATE LS_IMPORT_ILR_INIT_DIRECT a
      SET    revision = ( SELECT Max( revision )
                          FROM   LS_ILR_APPROVAL b
                          WHERE  a.ilr_id = b.ilr_id AND
                                 b.approval_status_id IN ( 1 ) AND
								 b.revision > 0)
      WHERE  import_run_id = a_run_id AND
             a.revision IS NULL;
			 
      --Remove any IDC set up for what's being imported
      DELETE FROM LS_ILR_INITIAL_DIRECT_COST
      WHERE  ( ilr_id, revision ) IN
             ( SELECT ilr_id,
                      revision
               FROM   LS_IMPORT_ILR_INIT_DIRECT b
               WHERE  b.import_run_id = a_run_id );

      --Insert to ls_ilr_initial_direct_cost
      INSERT INTO LS_ILR_INITIAL_DIRECT_COST
                  (ilr_idc_id,
                   ilr_id,
                   revision,
                   idc_group_id,
                   amount,
                   description,
                   date_incurred)
      SELECT ls_ilr_idc_seq.NEXTVAL,
             a.ilr_id,
             a.revision,
             a.idc_group_id,
             a.amount,
             a.description,
             To_date( a.date_incurred, 'mm/dd/yyyy' )
      FROM   LS_IMPORT_ILR_INIT_DIRECT a
      WHERE  a.import_run_id = a_run_id;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_import_ilr_idc;

  FUNCTION F_validate_ilr_idc( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Check that supplied revision is valid for the ILR
      UPDATE LS_IMPORT_ILR_INIT_DIRECT a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision does not exist. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_OPTIONS b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision );

      --Check supplied Revision is open
      UPDATE LS_IMPORT_ILR_INIT_DIRECT a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision is in a non-editable status. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision AND
                          b.approval_status_id IN ( 1 ) );


      --Validate where we have revisions in "Initiated" status for the ILR for us to apply these to
      UPDATE LS_IMPORT_ILR_INIT_DIRECT a
      SET    error_message = error_message
                             || ' No editable revision exists for the ILR. Please create a new revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          b.approval_status_id IN ( 1 ) AND	
						  b.revision > 0);


      --Validate the date is a real one
	  UPDATE LS_IMPORT_ILR_INIT_DIRECT a
      SET    error_message = error_message
                             || ' Invalid Date Incurred provided.'
      WHERE  import_run_id = a_run_id AND
             To_date( a.date_incurred, 'mm/dd/yyyy' ) not between to_date('01/01/1900', 'mm/dd/yyyy') and to_date('01/01/2999', 'mm/dd/yyyy');
			 
	  --Validate the date is prior or equal to in service date
	  UPDATE LS_IMPORT_ILR_INIT_DIRECT a
      SET    error_message = error_message
                             || ' Date Incurred for Initial Direct Costs must be before or on first payment date.'
      WHERE  import_run_id = a_run_id AND
             To_date( a.date_incurred, 'mm/dd/yyyy' )  > (select min(payment_term_date) 
														  from ls_ilr_payment_term b 
														  where a.ilr_id  = b.ilr_id and
														  nvl(a.revision,(SELECT Max( revision )
																		  FROM   LS_ILR_APPROVAL b
																		  WHERE  a.ilr_id = b.ilr_id AND
																		  b.approval_status_id IN ( 1 ) AND
																		  b.revision > 0)
															 ) = b.revision );


      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_validate_ilr_idc;


  FUNCTION f_import_ilr_incentive( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
	  --Assign Revision to Incentives imported without Revision (Assign Max Initiated Revision for ILR)
      UPDATE LS_IMPORT_ILR_INCENTIVE a
      SET    revision = ( SELECT Max( revision )
                          FROM   LS_ILR_APPROVAL b
                          WHERE  a.ilr_id = b.ilr_id AND
                                 b.approval_status_id IN ( 1 ) AND
								 b.revision > 0)
      WHERE  import_run_id = a_run_id AND
             a.revision IS NULL;
  
      --Remove any Incentives set up for what's being imported
      DELETE FROM ls_ilr_incentive
      WHERE  ( ilr_id, revision ) IN
             ( SELECT ilr_id,
                      revision
               FROM   LS_IMPORT_ILR_INIT_DIRECT b
               WHERE  b.import_run_id = a_run_id );

      --Insert to ls_ilr_incentive
      INSERT INTO ls_ilr_incentive
                  (ilr_incentive_id,
                   ilr_id,
                   revision,
                   incentive_group_id,
                   amount,
                   description,
                   date_incurred)
      SELECT ls_ilr_incentive_seq.NEXTVAL,
             a.ilr_id,
             a.revision,
             a.incentive_group_id,
             a.amount,
             a.description,
             To_date( a.date_incurred, 'mm/dd/yyyy' )
      FROM   LS_IMPORT_ILR_INCENTIVE a
      WHERE  a.import_run_id = a_run_id;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_import_ilr_incentive;

  FUNCTION F_validate_ilr_incentive( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Check that supplied revision is valid for the ILR
      UPDATE LS_IMPORT_ILR_INCENTIVE a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision does not exist. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_OPTIONS b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision );

      --Check supplied Revision is open
      UPDATE LS_IMPORT_ILR_INCENTIVE a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision is in a non-editable status. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision AND
                          b.approval_status_id IN ( 1 ) );


      --Validate where we have revisions in "Initiated" status for the ILR for us to apply these to
      UPDATE LS_IMPORT_ILR_INCENTIVE a
      SET    error_message = error_message
                             || ' No editable revision exists for the ILR. Please create a new revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          b.approval_status_id IN ( 1 ) AND
						  b.revision > 0);

      --Validate the date is a real one
	  UPDATE LS_IMPORT_ILR_INCENTIVE a
      SET    error_message = error_message
                             || ' Invalid Date Incurred provided.'
      WHERE  import_run_id = a_run_id AND
             To_date( a.date_incurred, 'mm/dd/yyyy' ) not between to_date('01/01/1900', 'mm/dd/yyyy') and to_date('01/01/2999', 'mm/dd/yyyy');
			 
	  --Validate the date is prior or equal to in service date
	  UPDATE LS_IMPORT_ILR_INCENTIVE a
      SET    error_message = error_message
                             || ' Date Incurred for Incentives must be before or on last payment date.'
      WHERE  import_run_id = a_run_id AND
              To_date( a.date_incurred, 'mm/dd/yyyy' ) > (select (max(add_months(payment_term_date, number_of_terms*decode(payment_freq_id,1,12,2,6,3,3,1,1,1)-1)))
														  from ls_ilr_payment_term b 
														  where a.ilr_id  = b.ilr_id and
														  nvl(a.revision,(SELECT Max( revision )
																		  FROM   LS_ILR_APPROVAL b
																		  WHERE  a.ilr_id = b.ilr_id AND
																		  b.approval_status_id IN ( 1 ) AND
																		  b.revision > 0)
															 ) = b.revision );


      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END f_validate_ilr_incentive;
  
  --##########################################################################
  --                            LS Asset UOP
  --##########################################################################
  FUNCTION F_IMPORT_ASSET_UOP( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN

      --Update existing records but only where the rates are NOT locked.
      UPDATE ls_asset_uop a
      SET (a.production,a.estimated_production) = (
          select c.production,c.estimated_production
          from ls_import_asset_uop c
          where c.import_run_id = a_run_id
          and a.ls_asset_id = c.ls_asset_id
          and a.gl_posting_mo_yr = To_date( c.gl_posting_mo_yr, 'yyyymm' ))
      WHERE nvl(a."LOCKED",0) = 0
      AND exists
            (SELECT 1
             FROM ls_import_asset_uop b
             where b.import_run_id = a_run_id
             and a.ls_asset_id = b.ls_asset_id
             and a.gl_posting_mo_yr = To_date( b.gl_posting_mo_yr, 'yyyymm' ));

      --Insert new records to ls_asset_uop
      --Oddly, the design called for gl_posting_mo_yr to be a number in the import table
      INSERT INTO ls_asset_uop
                  (ls_asset_id,
                   gl_posting_mo_yr,
                   production,
                   estimated_production,
                   "LOCKED")
      SELECT a.ls_asset_id,
             To_date( a.gl_posting_mo_yr, 'yyyymm' ),
             a.production,
             a.estimated_production,
             0
      FROM   ls_import_asset_uop a
      WHERE  a.import_run_id = a_run_id
      and not exists (
          select 1
          from ls_asset_uop b
          where a.ls_asset_id = b.ls_asset_id
          and b.gl_posting_mo_yr = To_date( a.gl_posting_mo_yr, 'yyyymm' ));

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END F_IMPORT_ASSET_UOP;
  
  FUNCTION F_VALIDATE_ASSET_UOP( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN

      --if the min month in the import is the min month on asset uop make sure there is an estimated amount populated.
      UPDATE LS_IMPORT_ASSET_UOP a
      SET    error_message = error_message
                             || 'Estimated_production must be populated for the minimum month in ls_asset_uop.'
      WHERE  import_run_id = a_run_id
      AND    ls_asset_id in (
             select ls_asset_id from (
                select ls_asset_id, sum(estimated_production) sumep
                from ls_import_asset_uop
                where import_run_id = a_run_id
                and estimated_production = 0
                and (ls_asset_id, to_date(gl_posting_mo_yr,'yyyymm')) in (
                  select ls_asset_id, min(to_date(gl_posting_mo_yr,'yyyymm')) mingl
                  from ls_import_asset_uop
                  where import_run_id = a_run_id
                  group by ls_asset_id
                  intersect
                  select ls_asset_id, min(gl_posting_mo_yr)
                  from ls_asset_uop
                  group by ls_asset_id)
                group by ls_asset_id
              ));

      --for any new asset/month combinations, make sure there is an estimated production that is not 0
      UPDATE LS_IMPORT_ASSET_UOP a
      SET    error_message = error_message
                             || 'Estimated_production must be populated for the minimum month in ls_asset_uop.'
      WHERE  import_run_id = a_run_id
      AND    (ls_asset_id, to_date(gl_posting_mo_yr,'yyyymm')) in (
      select ls_asset_id, glpost from (
        select ls_asset_id, to_date(gl_posting_mo_yr,'yyyymm') glpost,sum(estimated_production) sumep
        from ls_import_asset_uop
        where import_run_id = a_run_id
        group by ls_asset_id, gl_posting_mo_yr
        having sum(estimated_production) = 0
        minus
        select ls_asset_id,gl_posting_mo_yr, estimated_production
        from ls_asset_uop
      ));

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END F_VALIDATE_ASSET_UOP;

  FUNCTION F_import_ilr_purchase( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Assign Revisions to those not supplied with the max() Initiated Revision for the ILR
      UPDATE ls_import_ilr_purchase_opt a
      SET    revision = ( SELECT Max( revision )
                          FROM   LS_ILR_APPROVAL b
                          WHERE  a.ilr_id = b.ilr_id AND
                                 b.approval_status_id IN ( 1 ) AND
								 b.revision > 0)
      WHERE  import_run_id = a_run_id AND
             a.revision IS NULL;

      --Remove from Purchase Options
	  DELETE FROM ls_ilr_purchase_options
      WHERE  ilr_purchase_option_id IN
             ( SELECT ilr_purchase_option_id
               FROM   ls_ilr_purchase_options a
               WHERE  ( a.ilr_id, a.revision ) IN
                      ( SELECT b.ilr_id,
                               b.revision
                        FROM   ls_import_ilr_purchase_opt b
                        WHERE  import_run_id = a_run_id ) );



      --Insert to ls_ilr_purchase_options
      INSERT INTO ls_ilr_purchase_options
                  (ilr_id,
				           revision,
				           ilr_purchase_option_id,
                   ilr_purchase_probability_id,
                   decision_notice,
                   decision_date,
                   purchase_date,
                   purchase_option_type,
                   purchase_amt)
      SELECT  ilr_id,
			       revision,
			       ls_ilr_purchase_options_seq.NEXTVAL,
             ilr_purchase_probability_id,
             decision_notice,
             to_date(decision_date, 'MM/DD/YYYY'),
             to_date(purchase_date, 'MM/DD/YYYY'),
             purchase_option_type,
			       purchase_amt
      FROM   ls_import_ilr_purchase_opt
      WHERE  import_run_id = a_run_id;
	  
	  --Update Purchase Option Amount on ILR Options
		UPDATE LS_ILR_OPTIONS A
		   SET (PURCHASE_OPTION_AMT, PURCHASE_OPTION_TYPE_ID) =
			   (SELECT NVL(B.PURCHASE_AMT, 0), NVL(B.PURCHASE_OPTION_TYPE, 1)
				  FROM ls_import_ilr_purchase_opt B
				 WHERE B.IMPORT_RUN_ID = a_run_id
				   AND A.ILR_ID = B.ILR_ID
				   AND A.REVISION = B.REVISION
				   AND ILR_PURCHASE_PROBABILITY_ID = 1)
		 WHERE EXISTS (SELECT 1
				  FROM ls_import_ilr_purchase_opt C
				 WHERE C.IMPORT_RUN_ID = a_run_id
				   AND A.ILR_ID = C.ILR_ID
				   AND A.REVISION = C.REVISION
				   AND ILR_PURCHASE_PROBABILITY_ID = 1);



      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END F_import_ilr_purchase;

  FUNCTION F_validate_ilr_purchase( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Validate this is a valid Revision for this ILR if provided
      UPDATE ls_import_ilr_purchase_opt a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision does not exist. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision );

      --Check supplied Revision is open
      UPDATE ls_import_ilr_purchase_opt a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision is in a non-editable status. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision AND
                          b.approval_status_id IN ( 1 ) );

      --Validate where we have revisions in "Initiated" status for the ILR for us to apply these to
      UPDATE ls_import_ilr_purchase_opt a
      SET    error_message = error_message
                             || ' No editable revision exists for the ILR. Please create a new revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          b.approval_status_id IN ( 1 ) AND
						  b.revision > 0);

	  --Validate that only one purchase option is 'Likely' at any time for an ILR and revision.
  Update ls_import_ilr_purchase_opt a
	  SET  error_message = error_message
                             || 'You cannot have multiple ''Likely'' purchase options for an ILR. Please change other purchase options on the ILR.'
			WHERE import_run_id = a_run_id AND
			revision is not NULL
      AND a.ilr_purchase_probability_id = 1
			AND  exists
          (SELECT 1 FROM ls_import_ilr_purchase_opt c
           WHERE a.ilr_id = c.ilr_id
           AND a.revision = c.revision
           AND c.ilr_purchase_probability_id = 1
           AND a.ilr_purchase_probability_id = c.ilr_purchase_probability_id
           AND a.import_run_id = c.import_run_id
           AND a.line_id <> c.line_id) ;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END F_validate_ilr_purchase;

 --##########################################################################
  --                            ILR Termination Options
 --##########################################################################

  FUNCTION F_IMPORT_ILR_TERMINATION( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Assign Revisions to those not supplied with the max() Initiated Revision for the ILR
      UPDATE ls_import_ilr_term_opt a
      SET    revision = ( SELECT Max( revision )
                          FROM   LS_ILR_APPROVAL b
                          WHERE  a.ilr_id = b.ilr_id AND
                                 b.approval_status_id IN ( 1 ) AND
								 b.revision > 0)
      WHERE  import_run_id = a_run_id AND
             a.revision IS NULL;

      --Remove from Termination Options
      DELETE FROM ls_ilr_termination_options
      WHERE  ilr_termination_option_id IN
             ( SELECT ilr_termination_option_id
               FROM   ls_ilr_termination_options a
               WHERE  ( a.ilr_id, a.revision ) IN
                      ( SELECT b.ilr_id,
                               b.revision
                        FROM   ls_import_ilr_term_opt b
                        WHERE  import_run_id = a_run_id ) );



      --Insert to ls_ilr_termination_options
      INSERT INTO ls_ilr_termination_options
                  (ilr_id,
				    revision,
				    ilr_termination_option_id,
                   ilr_termination_probability_id,
                   decision_notice,
                   decision_date,
                   termination_date,
                   termination_amt)
      SELECT  ilr_id,
			  revision,
			 ls_ilr_termination_options_seq.NEXTVAL,
             ilr_term_probability_id,
             decision_notice,
             to_date(decision_date, 'MM/DD/YYYY'),
             to_date(termination_date, 'MM/DD/YYYY'),
              termination_amt
      FROM   ls_import_ilr_term_opt
      WHERE  import_run_id = a_run_id;
	  
	  	  --Update Termination Amount on ILR Options
		UPDATE LS_ILR_OPTIONS A
		   SET TERMINATION_AMT =
			   (SELECT NVL(B.TERMINATION_AMT, 0)
				  FROM ls_import_ilr_term_opt B
				 WHERE B.IMPORT_RUN_ID = a_run_id
				   AND A.ILR_ID = B.ILR_ID
				   AND A.REVISION = B.REVISION
				   AND ILR_TERM_PROBABILITY_ID = 1)
		 WHERE EXISTS (SELECT 1
				  FROM ls_import_ilr_term_opt C
				 WHERE C.IMPORT_RUN_ID = a_run_id
				   AND A.ILR_ID = C.ILR_ID
				   AND A.REVISION = C.REVISION
				   AND ILR_TERM_PROBABILITY_ID = 1);

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END F_IMPORT_ILR_TERMINATION;

  FUNCTION F_VALIDATE_ILR_TERMINATION( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
      --Validate this is a valid Revision for this ILR if provided
      UPDATE ls_import_ilr_term_opt a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision does not exist. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision );

      --Check supplied Revision is open
      UPDATE ls_import_ilr_term_opt a
      SET    error_message = error_message
                             || ' Cannot load data to revision entered. The revision is in a non-editable status. Please enter a valid revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NOT NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          a.revision = b.revision AND
                          b.approval_status_id IN ( 1 ) );

      --Validate where we have revisions in "Initiated" status for the ILR for us to apply these to
      UPDATE ls_import_ilr_term_opt a
      SET    error_message = error_message
                             || ' No editable revision exists for the ILR. Please create a new revision.'
      WHERE  import_run_id = a_run_id AND
             revision IS NULL AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   LS_ILR_APPROVAL b
                   WHERE  a.ilr_id = b.ilr_id AND
                          b.approval_status_id IN ( 1 ) AND
						  b.revision > 0 );

	  --Validate that only one purchase option is 'Likely' at any time for an ILR and revision.
	  Update ls_import_ilr_term_opt a
	  SET  error_message = error_message
                             || 'You cannot have multiple ''Likely'' termination options for an ILR. Please change other termination options on the ILR.'
			WHERE import_run_id = a_run_id AND
			revision is not NULL
      AND a.ilr_term_probability_id = 1
			and  exists
          (SELECT 1 FROM ls_import_ilr_term_opt c
           WHERE a.ilr_id = c.ilr_id
           AND a.revision = c.revision
           AND c.ilr_term_probability_id = 1
           AND a.ilr_term_probability_id = c.ilr_term_probability_id
           AND a.import_run_id = c.import_run_id
           AND a.line_id <> c.line_id) ;

      RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END F_VALIDATE_ILR_TERMINATION;

 --##########################################################################
 --                            Default Discount Rates
 --##########################################################################

  FUNCTION F_IMPORT_DEFAULT_DISC_RATE( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
    -- We Validated everything already so just do a direct load into the table
    INSERT INTO ls_ilr_default_rates
      (rate_type_id, term_length, effective_date, borrowing_curr_id, rate)
      SELECT b.rate_type_id, b.term_length,
         To_Date(b.effective_date, 'mm/dd/yyyy'), nvl(b.borrowing_curr_id,0), rate
      FROM ls_import_default_rates b
       WHERE b.import_run_id = a_run_id;

    RETURN 'OK';
	
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END F_IMPORT_DEFAULT_DISC_RATE;
  
  FUNCTION F_VALIDATE_DEFAULT_DISC_RATE( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
  BEGIN
    --Validate Rate Type is Active
    l_msg := 'Validating Rate Type is Active';
    UPDATE ls_import_default_rates a
      SET    error_message = error_message
                             || ' The Default Discount Rate Type specified is inactive. Rates can only be added to active Rate Types. '
      WHERE  import_run_id = a_run_id 
    AND exists 
                 ( SELECT 1
                   FROM   ls_ilr_default_rate_type b
                   WHERE  a.rate_type_id = b.rate_type_id AND
                          b.active = 0 );
	
	--Validate Rate Type is configured (exists in map table)
    l_msg := 'Validating Rate Type is configured';
    UPDATE ls_import_default_rates a
      SET    error_message = error_message
                             || ' The Default Discount Rate Type specified is not mapped. Rates can only be added to mapped Rate Types. '
      WHERE  import_run_id = a_run_id 
    AND not exists 
                 ( SELECT 1
                   FROM   ls_ilr_default_rate_type_map b
                   WHERE  a.rate_type_id = b.rate_type_id );
						  
    --Validate Effective Date is on or after current open month for companies assigned to Default Rate Type
    l_msg := 'Validating Effective Date is before current open month';
    UPDATE ls_import_default_rates a
      SET    error_message = error_message
                             || ' The Effective Date supplied is before the maximum current open month for all Companies assigned to the Default Discount Rate Type. '
      WHERE  import_run_id = a_run_id 
    AND exists 
                 ( SELECT 1
            FROM (SELECT m.rate_type_id, Max(gl_posting_mo_yr) current_month
                 FROM (SELECT DISTINCT rate_type_id, company_id
                    FROM ls_ilr_default_rate_type_map) m, company c,
                  ls_process_control p
                WHERE m.company_id = c.company_id
                AND p.company_id = c.company_id
                GROUP BY m.rate_type_id) b
           where a.rate_type_id = b.rate_type_id
             AND to_date(to_char(a.effective_date), 'mm/dd/yyyy') < b.current_month );
              
  
    --Validate Permutation of Effective Date, Term Length and Borrowing Currency within this import
    l_msg := 'Validating distinct permutation of Effective Date, Term Length, and Borrowing Currency';
    UPDATE ls_import_default_rates a
        SET    error_message = error_message
                               || ' The combination of Effective Date, Term Length, and Borrowing Currency is not unique within the Import. '
        WHERE  import_run_id = a_run_id 
      AND exists 
                   ( SELECT 1
                     FROM   ls_import_default_rates b
                     WHERE  a.import_run_id = b.import_run_id AND
					 a.rate_type_id = b.rate_type_id AND
                    a.term_length = b.term_length AND
                nvl(a.borrowing_curr_id, -1) = nvl(b.borrowing_curr_id, -1) AND
                to_date(to_char(a.effective_date), 'mm/dd/yyyy') = to_date(to_char(b.effective_date), 'mm/dd/yyyy') AND
                            a.line_id <> b.line_id );
  
    --Validate Permutation of Effective Date, Term Length and Borrowing Currency hasnt already been loaded into LS_ILR_DEFAULT_RATES
    l_msg := 'Validating distinct permutation of Effective Date, Term Length, and Borrowing Currency already existing in rates table';
    UPDATE ls_import_default_rates a
        SET    error_message = error_message
                               || ' The combination of Effective Date, Term Length, and Borrowing Currency already exists in the system. '
        WHERE  import_run_id = a_run_id 
      AND exists 
                   ( SELECT 1
                     FROM   ls_ilr_default_rates b
                     WHERE  a.rate_type_id = b.rate_type_id AND
                    a.term_length = b.term_length AND
                nvl(a.borrowing_curr_id, 0) = nvl(b.borrowing_curr_id, 0) AND
                to_date(to_char(a.effective_date), 'mm/dd/yyyy') = b.effective_date);
              
    RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END F_VALIDATE_DEFAULT_DISC_RATE;
  
  
 --##########################################################################
 --                            ILR Revision Import
 --##########################################################################

	FUNCTION f_import_ilr_revision(a_run_id IN NUMBER) RETURN VARCHAR2 IS
	  l_msg             VARCHAR2(2000);
	  l_sqls            VARCHAR2(4000);
	  l_index           NUMBER;
	  l_curr_ilr        NUMBER;
	  l_new_revision    NUMBER;
	  l_new_workflow_id NUMBER;
	  l_new_rate 		number(22,8);
	  l_term_length 	number;
	  l_in_svc_date 	date;
	  
	  l_ilr_id          		   ls_import_ilr_rev.ilr_id%TYPE;
	  l_default_rate_flag          ls_import_ilr_rev.default_rate_flag%TYPE;
	  l_borrowing_curr_id          ls_import_ilr_rev.borrowing_curr_id%TYPE;
	  l_remeasurement_date          ls_import_ilr_rev.remeasurement_date%TYPE;
	  
	  TYPE ilr_import_table IS TABLE OF ls_import_ilr_rev%ROWTYPE;
	  l_ilr_pay_terms ilr_import_table;

	  CURSOR l_ilr_import IS
		SELECT ilr_id, 
			   nvl(default_rate_flag,0) default_rate_flag,
			   nvl(borrowing_curr_id, 0) borrowing_curr_id,
			   to_date(remeasurement_date, 'mm/dd/yyyy') remeasurement_date
		FROM   ls_import_ilr_rev
		WHERE  import_run_id = a_run_id
		AND    sign(ilr_id) = 1;

	BEGIN

	  l_msg := 'Identifying ILR Revision Headers';
	  --Set all rows ILR ID to negative where it's not the first payment term for processing distinct rows
	  UPDATE ls_import_ilr_rev
	  SET    ilr_id = -1*(ilr_id)
	  WHERE  payment_term_id <> 1
	  AND    import_run_id = a_run_id;

	  l_msg := 'Collect ILR Revision Headers';
	  OPEN l_ilr_import;
	  LOOP
		FETCH l_ilr_import
		  INTO l_ilr_id, l_default_rate_flag, l_borrowing_curr_id, l_remeasurement_date;
		EXIT WHEN l_ilr_import%NOTFOUND;
		  	  
		l_curr_ilr     := to_number(l_ilr_id);
		l_msg          := 'Calling Copy Revision for ILR:' ||
						  to_char(l_curr_ilr);
		l_new_revision := pkg_lease_ilr.f_copyrevision(l_curr_ilr);
	  
		l_msg := 'Loading ILR Options from ILR Header rows';
		--Update ls_ilr_options where they provided us values in the import
		MERGE INTO ls_ilr_options o
		USING (SELECT * FROM ls_import_ilr_rev WHERE ilr_id = l_curr_ilr and import_run_id = a_run_id) import
		ON (import.ilr_id = o.ilr_id AND o.revision = l_new_revision)
		WHEN MATCHED THEN
		  UPDATE
		  SET    o.inception_air            = nvl(import.discount_rate*100, --Only multiply by 100 if its supplied which it always should be...
												  o.inception_air),
				 o.lease_cap_type_id        = nvl(import.lease_cap_type_id,
												  o.lease_cap_type_id),
				 o.itc_sw                   = nvl(import.itc_sw, o.itc_sw),
				 o.cancelable_type_id       = nvl(import.cancelable_type_id,
												  o.cancelable_type_id),
				 o.specialized_asset        = nvl(import.specialized_asset,
												  o.specialized_asset),
				 o.intent_to_purchase       = nvl(import.intent_to_purch,
												  o.intent_to_purchase),
				 o.interco_lease_company    = nvl(import.interco_company_id,
												  o.interco_lease_company),
				 o.interco_lease_flag       = nvl(import.interco_lease,
												  o.interco_lease_flag),
				 o.sublease_flag            = nvl(import.sublease_flag,
												  o.sublease_flag),
				 o.sublease_id              = nvl(import.sublease_id,
												  o.sublease_id),
				 o.depr_calc_method         = nvl(import.depr_calc_method,
												  o.depr_calc_method),
				 o.remeasure_calc_gain_loss = nvl(import.remeasure_calc_gain_loss,
													decode(import.remeasurement_date, null, null, 1)), -- If remeasure_calc_gain_loss not supplied default to 1 when remeasurement_date populated
				 o.default_rate_flag        = nvl(import.default_rate_flag,
												  o.default_rate_flag),
				 o.borrowing_curr_id        = nvl(import.borrowing_curr_id,
												  o.borrowing_curr_id),
				 o.remeasurement_date       = to_date(import.remeasurement_date, 'mm/dd/yyyy'); -- Remeasure date set to null if not supplied;
		
		--Remove any prior existing workflow if it got left behind from something else
		l_msg := 'Removing prior workflow detail rows if existing';
		delete from workflow_detail
		where workflow_id in (select workflow_id from workflow 
		where lower(trim(subsystem)) = 'lessee_ilr_approval'
		and id_field1 = l_curr_ilr
		and id_field2 = l_new_revision);
		
		l_msg := 'Removing prior workflow rows if existing';
		delete from workflow 
		where lower(trim(subsystem)) = 'lessee_ilr_approval'
		and id_field1 = l_curr_ilr
		and id_field2 = l_new_revision;
		
		--Create Workflow entry for revision based on prior revision
		l_new_workflow_id := pwrplant1.nextval;
		
		l_msg := 'Inserting new workflow entry for ILR Revisions';
		INSERT INTO workflow
		  (workflow_id,
		   workflow_type_id,
		   workflow_type_desc,
		   subsystem,
		   id_field1,
		   id_field2,
		   approval_status_id,
		   use_limits,
		   sql_approval_amount)
		  SELECT l_new_workflow_id,
				 workflow_type_id,
				 workflow_type_desc,
				 subsystem,
				 l_curr_ilr,
				 l_new_revision,
				 1,
				 use_limits,
				 sql_approval_amount
		  FROM   workflow w
		  JOIN   (SELECT ilr_id, current_revision
				  FROM   ls_ilr
				  WHERE  ilr_id = l_curr_ilr) i ON i.ilr_id = id_field1
											AND    i.current_revision =
												   id_field2
		  WHERE  lower(trim(subsystem)) = 'lessee_ilr_approval';
	  
		l_msg := 'Inserting new workflow entry for ILR Revisions';
		INSERT INTO workflow_detail
		  (workflow_id,
		   id,
		   workflow_rule_id,
		   workflow_rule_desc,
		   rule_order,
		   users,
		   approval_status_id,
		   required,
		   notes,
		   authority_limit,
		   num_approvers,
		   num_required,
		   SQL,
		   allow_edit,
		   orig_authority_limit,
		   is_custom_limit,
		   mobile_approval,
		   group_approval,
		   sql_users)
		  SELECT l_new_workflow_id,
				 id,
				 workflow_rule_id,
				 workflow_rule_desc,
				 rule_order,
				 users,
				 1,
				 required,
				 notes,
				 authority_limit,
				 num_approvers,
				 num_required,
				 SQL,
				 allow_edit,
				 orig_authority_limit,
				 is_custom_limit,
				 mobile_approval,
				 group_approval,
				 sql_users
		  FROM   workflow_detail
		  WHERE  workflow_id IN
				 (SELECT workflow_id
				  FROM   workflow w
				  JOIN   (SELECT ilr_id, current_revision
						 FROM   ls_ilr
						 WHERE  ilr_id = l_curr_ilr) i ON i.ilr_id = w.id_field1
												   AND    i.current_revision =
														  w.id_field2
				  WHERE  lower(trim(subsystem)) = 'lessee_ilr_approval');
	  
		--Wipe the copied payment terms and rebuild with what was provided
		l_msg := 'Reloading ILR Payment terms from Import';
		DELETE FROM ls_ilr_payment_term
		WHERE  ilr_id = l_curr_ilr
		AND    revision = l_new_revision;
	  
		--ReLoad from import
		l_msg := 'Collecting ILR Payment terms from Import';
		SELECT *
		BULK   COLLECT
		INTO   l_ilr_pay_terms
		FROM   ls_import_ilr_rev
		WHERE  import_run_id = a_run_id
		AND    abs(ilr_id) = l_curr_ilr;
	  
		l_msg := 'Importing Payment Terms';
		l_index := 1;
		FORALL l_index IN INDICES OF l_ilr_pay_terms
		  INSERT INTO ls_ilr_payment_term
			(ilr_id,
			 payment_term_id,
			 payment_term_type_id,
			 payment_term_date,
			 payment_freq_id,
			 number_of_terms,
			 paid_amount,
			 revision,
			 c_bucket_1,
			 c_bucket_2,
			 c_bucket_3,
			 c_bucket_4,
			 c_bucket_5,
			 c_bucket_6,
			 c_bucket_7,
			 c_bucket_8,
			 c_bucket_9,
			 c_bucket_10,
			 e_bucket_1,
			 e_bucket_2,
			 e_bucket_3,
			 e_bucket_4,
			 e_bucket_5,
			 e_bucket_6,
			 e_bucket_7,
			 e_bucket_8,
			 e_bucket_9,
			 e_bucket_10,
			 escalation,
			 escalation_freq_id,
			 escalation_pct)
		  VALUES
			(abs(l_ilr_pay_terms(l_index).ilr_id),
			 l_ilr_pay_terms(l_index).payment_term_id,
			 nvl(l_ilr_pay_terms(l_index).payment_term_type_id, 2),
			 to_date(l_ilr_pay_terms(l_index).payment_term_date, 'mm/dd/yyyy'),
			 l_ilr_pay_terms(l_index).payment_freq_id,
			 l_ilr_pay_terms(l_index).number_of_terms,
			 l_ilr_pay_terms(l_index).paid_amount,
			 l_new_revision,
			 nvl(l_ilr_pay_terms(l_index).c_bucket_1, 0),
			 nvl(l_ilr_pay_terms(l_index).c_bucket_2, 0),
			 nvl(l_ilr_pay_terms(l_index).c_bucket_3, 0),
			 nvl(l_ilr_pay_terms(l_index).c_bucket_4, 0),
			 nvl(l_ilr_pay_terms(l_index).c_bucket_5, 0),
			 nvl(l_ilr_pay_terms(l_index).c_bucket_6, 0),
			 nvl(l_ilr_pay_terms(l_index).c_bucket_7, 0),
			 nvl(l_ilr_pay_terms(l_index).c_bucket_8, 0),
			 nvl(l_ilr_pay_terms(l_index).c_bucket_9, 0),
			 nvl(l_ilr_pay_terms(l_index).c_bucket_10, 0),
			 nvl(l_ilr_pay_terms(l_index).e_bucket_1, 0),
			 nvl(l_ilr_pay_terms(l_index).e_bucket_2, 0),
			 nvl(l_ilr_pay_terms(l_index).e_bucket_3, 0),
			 nvl(l_ilr_pay_terms(l_index).e_bucket_4, 0),
			 nvl(l_ilr_pay_terms(l_index).e_bucket_5, 0),
			 nvl(l_ilr_pay_terms(l_index).e_bucket_6, 0),
			 nvl(l_ilr_pay_terms(l_index).e_bucket_7, 0),
			 nvl(l_ilr_pay_terms(l_index).e_bucket_8, 0),
			 nvl(l_ilr_pay_terms(l_index).e_bucket_9, 0),
			 nvl(l_ilr_pay_terms(l_index).e_bucket_10, 0),
			 l_ilr_pay_terms(l_index).escalation,
			 l_ilr_pay_terms(l_index).escalation_freq_id,
			 l_ilr_pay_terms(l_index).escalation_pct);
			 
		-- Loop through contingent includes and load if theyre populated. 
		-- ls_ilr_payment_term_var_payment includes sob so bring in company_set_of_books
		l_msg := 'Populate Include in Initial Measure on Variable Payments';		
		FOR l_index in 1..10 
		loop
			l_sqls := 'INSERT INTO ls_ilr_payment_term_var_paymnt '||
						'  (ilr_id, '||
   						' revision, '||
   						' payment_term_id, '||
   						' rent_type, '||
   						' bucket_number, '||
   						' incl_in_initial_measure, '||
   						' run_order, '||
   						' set_of_books_id) '||
  						' SELECT '||to_char(l_curr_ilr)||', '||
         						' '||to_char(l_new_revision)||', '||
         						' import.payment_term_id, '||
         						' ''Contingent'', '||
         						' '||to_char(l_index)||', '||
         						' 1, '||
         						' 0, '||
         						' csob.set_of_books_id '||
  						' FROM   (SELECT * FROM ls_import_ilr_rev WHERE abs(ilr_id) = '||to_char(l_curr_ilr)||' and import_run_id = '||to_char(a_run_id)||') import '||
  						' JOIN   ls_ilr i ON abs(import.ilr_id) = i.ilr_id '||
  						' JOIN   company_set_of_books csob ON i.company_id = csob.company_id '||
  						' WHERE  import.c_'||to_char(l_index)||'_incl_initial_measure = 1';
			
			execute immediate l_sqls;

		END LOOP; -- End Include Initial Measure Loop
		
		if l_default_rate_flag = 1 then
			--Call Default Disc Rate Logic for this ILR
			--Cant call till everything prior for the ILR has been put in since it leverages DB info to get the Default Disc Rate
			L_TERM_LENGTH := PKG_LEASE_ILR.F_PULL_ILR_PAYMENT_TERM_MONTHS(l_curr_ilr,l_new_revision);
			
			if l_term_length is null then
				l_msg := 'Error retrieving Payment Terms for Default Discount Rate retrieval';
				return l_msg;
			end if;
			
			l_msg := 'Retrieving In Service Date for Default Discount Rate retrieval';
			--Pull ISD off the ILR
			select est_in_svc_date
			into l_in_svc_date
			from ls_ilr
			where ilr_id = l_ilr_id;
			
			l_msg := 'Retrieving Default Discount Rate';
			l_new_rate := PKG_LEASE_ILR.F_GET_DEFAULT_DISC_RATE(L_ILR_ID, L_TERM_LENGTH, L_IN_SVC_DATE, L_REMEASUREMENT_DATE, l_borrowing_curr_id);
			
			l_msg := 'Updating Discount Rate from Default Discount Rate result';
			update ls_ilr_options
			set inception_air = (l_new_rate) * 100
			where ilr_id = l_ilr_id
			and revision = l_new_revision;
			
		end if;	--End Default Disc Rate get
		
	  END LOOP; -- End l_ilr_import loop
	  
	  CLOSE l_ilr_import;
	  
	  --All Done
	  RETURN 'OK';

	EXCEPTION
	  WHEN OTHERS THEN
		l_msg := substr(l_msg || ': ' || SQLERRM, 1, 2000);
	  
		RETURN l_msg;
	  
	END f_import_ilr_revision;

  
  FUNCTION F_VALIDATE_ILR_REVISION( a_run_id IN NUMBER )
  RETURN VARCHAR2
  IS
    l_msg VARCHAR2(2000);
	l_sqls Varchar2(4000);
	
	l_index NUMBER;
  BEGIN

	l_msg := 'Checking for ILR Current Status';
	  UPDATE LS_IMPORT_ILR_REV a
      SET    error_message = error_message || ' ILR is in a status that does not allow for new revisions.'
      WHERE  import_run_id = a_run_id 
	  AND payment_term_id = 1 -- Its messy to write the same message on all the payment lines
	  AND NOT EXISTS
                 ( SELECT 1
					FROM  ls_ilr i
					where i.ilr_id = a.ilr_id
					and ilr_status_id in (2, 6) -- In Service and Pending In Service are only ones can be imported to
				 );
	
	l_msg := 'Checking for Sequential Payment Term IDs';
      update LS_IMPORT_ILR_REV
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non sequential payment term ids.'
       where IMPORT_RUN_ID = A_RUN_ID
         and LINE_ID in (select LINE_ID
                           from (select LINE_ID,
                                        PAYMENT_TERM_ID,
                                        ROW_NUMBER() OVER(partition by ILR_ID order by ILR_ID, PAYMENT_TERM_ID) MY_TERMS
                                   from LS_IMPORT_ILR_REV
                                  where IMPORT_RUN_ID = A_RUN_ID)
                          where PAYMENT_TERM_ID <> MY_TERMS);
						  
	l_msg := 'Checking Payment start date matches prior revision';
	  UPDATE LS_IMPORT_ILR_REV a
      SET    error_message = error_message
                             || ' New Revision''s Payment term start does not match current approved revision''s.'
      WHERE  import_run_id = a_run_id 
	  AND payment_term_id = 1
	  AND NOT EXISTS
                 ( SELECT 1
					FROM   (SELECT ilr_id, revision, payment_term_date
							FROM   ls_ilr_payment_term
							WHERE  payment_term_id = 1) term
					JOIN   ls_ilr i ON term.ilr_id = i.ilr_id
								AND    term.revision = i.current_revision
					WHERE  term.payment_term_date = to_date(a.payment_term_date, 'mm/dd/yyyy')
				 );
				 
						  
	l_msg := 'Checking for Sequential Payment Terms with no overlapping dates';
      --check that one term ends where the next begins
      update LS_IMPORT_ILR_REV
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Start of ILR term does not match with end of previous term.'
       where IMPORT_RUN_ID = A_RUN_ID
	     and payment_term_id = 1
         and LINE_ID in
             (select LINE_ID
                from (select LINE_ID,
                             TERM_START,
                             LAG(TERM_END, 1, TO_DATE('07/04/1776', 'mm/dd/yyyy')) OVER(partition by ILR_ID order by ILR_ID, TERM_START) as TERM_END
                        from (select LINE_ID,
                                     ILR_ID,
                                     TO_DATE(PAYMENT_TERM_DATE, 'mm/dd/yyyy') as TERM_START,
                                     ADD_MONTHS(TO_DATE(PAYMENT_TERM_DATE, 'mm/dd/yyyy'),
                                                NUMBER_OF_TERMS *
                                                DECODE(PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 4, 1)) as TERM_END
                                from LS_IMPORT_ILR_REV
                               where IMPORT_RUN_ID = A_RUN_ID))
               where TERM_START <> TERM_END
                 and TERM_END <> TO_DATE('07/04/1776', 'mm/dd/yyyy'));
				 
		--Loop through executory buckets to validate if theyre enabled if populated		
	l_msg := 'Validating Executory Buckets are enabled where populated';		
		FOR l_index in 1..10 
		loop
			l_sqls := 'update LS_IMPORT_ILR_REV ' ||
			'         set ERROR_MESSAGE = ERROR_MESSAGE || '' Executory bucket '||To_Char(l_index)||' is not enabled.'''||
			'       where not (E_BUCKET_'||To_Char(l_index)||' is null or E_BUCKET_'||To_Char(l_index)||' = 0)'||
			'         and IMPORT_RUN_ID = '||to_char(a_run_id)||' ' ||
			'         and (exists (select * ' ||
			'                        from LS_RENT_BUCKET_ADMIN ' ||
			'                       where RENT_TYPE = ''Executory''' ||
			'                         and BUCKET_NUMBER = '||To_Char(l_index)||' ' ||
			'                         and STATUS_CODE_ID = 0) or not exists' ||
			'              (select *' ||
			'                 from LS_RENT_BUCKET_ADMIN' ||
			'                where RENT_TYPE = ''Executory''' ||
			'                  and BUCKET_NUMBER = '||To_Char(l_index)||'))';	  
			
			execute immediate l_sqls;

		END LOOP;
		
		--Loop through contingent buckets to validate if theyre enabled if populated	
	l_msg := 'Validating Contingent Buckets are enabled where populated';		
		FOR l_index in 1..10 
		loop
			l_sqls := 'update LS_IMPORT_ILR_REV ' ||
			'         set ERROR_MESSAGE = ERROR_MESSAGE || '' Contingent bucket '||To_Char(l_index)||' is not enabled.'''||
			'       where not (C_BUCKET_'||To_Char(l_index)||' is null or C_BUCKET_'||To_Char(l_index)||' = 0 '||
			'					or c_'||To_Char(l_index)||'_incl_initial_measure = 0 or c_'||To_Char(l_index)||'_incl_initial_measure is null)'||
			'         and IMPORT_RUN_ID = '||to_char(a_run_id)||' ' ||
			'         and (exists (select * ' ||
			'                        from LS_RENT_BUCKET_ADMIN ' ||
			'                       where RENT_TYPE = ''Contingent''' ||
			'                         and BUCKET_NUMBER = '||To_Char(l_index)||' ' ||
			'                         and STATUS_CODE_ID = 0) or not exists' ||
			'              (select *' ||
			'                 from LS_RENT_BUCKET_ADMIN' ||
			'                where RENT_TYPE = ''Contingent''' ||
			'                  and BUCKET_NUMBER = '||To_Char(l_index)||'))';  
			
			execute immediate l_sqls;

		END LOOP;
		
	
      --Validate if Sublease Flag is no, there is no sublease
	l_msg := 'Validating Sublease attributes';		
      UPDATE LS_IMPORT_ILR_REV a
      SET    error_message = error_message
                             || ' Sublease populated when Sublease Flag set to "No".'
      WHERE  import_run_id = a_run_id AND
			 payment_Term_id = 1 AND
             sublease_flag = 0 AND
             sublease_id IS NOT NULL;

      --Validate intercompany Lease company is a lease company
	l_msg := 'Validating Intercompany Lease attributes';		
      UPDATE LS_IMPORT_ILR_REV a
      SET    error_message = error_message
                             || ' Intercompany Lease Company must be a Lease Company.'
      WHERE  import_run_id = a_run_id AND
			 payment_Term_id = 1 AND
             NOT EXISTS
                 ( SELECT 1
                   FROM   COMPANY_SETUP
                   WHERE  interco_company_id = company_id AND
                          is_lease_company = 1 ) AND
             interco_company_id IS NOT NULL;
			 
      --Validate variable escalation has no escalation percent
	l_msg := 'Validating Escalation not assigned where Escalation Percent provided';	
      UPDATE LS_IMPORT_ILR_REV a
      SET    error_message = error_message
                             || ' An Escalation Percent may not be assigned to an escalation which is a Variable Escalation.'
      WHERE  import_run_id = a_run_id
      AND escalation IS NOT NULL
      AND escalation_pct <> 0
      AND escalation_pct IS NOT NULL;

      --Validate escalation is not more frequent than payment
	l_msg := 'Validating Escalation Frequency against Payment Frequency';	
      UPDATE LS_IMPORT_ILR_REV a
      SET    error_message = error_message
                             || ' An escalation frequency may not be more frequent than the payment.'
      WHERE  import_run_id = a_run_id
      AND escalation_freq_id > payment_freq_id;

      --Validate escalation has a frequency assigned
	l_msg := 'Validating Escalation Frequency provided when Escalation or Escalation Percent provided';	
      UPDATE LS_IMPORT_ILR_REV a
      SET    error_message = error_message
                             || ' An escalation frequency is required if escalations are assigned.'
      WHERE  import_run_id = a_run_id
      AND escalation_freq_id IS NULL
      AND (escalation IS NOT NULL
        OR (escalation IS NULL AND escalation_pct IS NOT NULL AND escalation_pct <> 0) );

  
    RETURN 'OK';
  EXCEPTION
    WHEN OTHERS THEN
               l_msg := Substr( l_msg
                                || ': '
                                || SQLERRM, 1, 2000 );

               RETURN l_msg;
  END F_VALIDATE_ILR_REVISION;
  
END PKG_LEASE_IMPORT;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (18170, 0, 2018, 2, 1, 0, 0, 'C:\BitBucketRepos\Classic_PB\scripts\00_base\packages', 
    'PKG_LEASE_IMPORT.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
