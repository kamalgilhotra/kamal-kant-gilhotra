create or replace package PP_PROCESS_PKG is
  --||============================================================================
  --|| Application: PowerPlan
  --|| Object Name: pp_process_pkg
  --|| Description:
  --||============================================================================
  --|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
  --||============================================================================
  --|| Version  Date       Revised By          Reason for Change
  --|| -------- ---------- ------------------- -----------------------------------
  --|| 1.0      03/20/2013 Charlie Shilling    Original Version
  --|| 1.3      12/05/2013 Stephen Motter      maint 34370 - Concurrent Processing
  --|| 1.4      07/05/2014 Kyle Peterson       maint 38908 - change PP_SET_PROCESS to CLOB
  --|| 1.5      07/17/2014 Sunjin Cone         maint 39159 - new function
  --|| 1.6      11/04/2014 Andrew Scott        maint 41075 - don't pull session id from gv$session
  --||                                         use "userenv('sessionid')"
  --||
  --||
  --||	NOTE: Any time this package is modified, all of the unit test cases from the
  --||		following Confluence page should be verified. 
  --||		
  --||		"PP_PROCESS_PKG enhancements to support month-end concurrency"
  --||		https://powerme.pwrplan.com/display/DEV/PP_PROCESS_PKG+enhancements+to+support+month-end+concurrency
  --||
  --||
  --||============================================================================
  type STRING_ARRAY is table of varchar2(32767);

  function PP_SET_PROCESS(A_ARG CLOB, A_ARG2 CLOB) return varchar2;

  function PP_RELEASE_PROCESS(A_ARG CLOB) return varchar2;

  function PP_CHECK_PROCESS(A_ARG CLOB) return varchar2;

  function PP_CHECK_PROCESS_PREFIX(A_ARG CLOB) return varchar2;

  function PP_RESET_DISCONNECTED(A_ARG varchar2, A_CO_ID number) return varchar2;

end PP_PROCESS_PKG;
/
create or replace package body PP_PROCESS_PKG is
  -- =============================================================================
  --  Function PP_SET_PROCESS
  -- =============================================================================
  function PP_SET_PROCESS(A_ARG CLOB, A_ARG2 CLOB) return varchar2 as
	/*
	||============================================================================
	|| Application: PowerPlant
	|| Object Name: PP_SET_PROCESS
	|| Description:
	||============================================================================
	|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
	||============================================================================
	|| Version Date       Revised By       Reason for Change
	|| ------- ---------- ---------------- -----------------------------------------
	|| 1.1                                 Added a check for null argument
	|| 1.2     08/30/2013 Charlie Shilling maint 31807
	|| 1.3     12/05/2013 Stephen Motter   maint 34370 - Concurrent Processing
	||============================================================================
	*/

	PRAGMA AUTONOMOUS_TRANSACTION;

	AUDSID             varchar(60);
	PROCESS_COUNT      number(22, 0);
	CHECK_STRING       CLOB;
	PROCESS_STRING     CLOB;
	OLD_PROCESS_STRING CLOB;
	CHECK_STRING_ARR   string_array;
	PROCESS_STRING_ARR string_array;
	STATUS             varchar2(35);
	I                  number;
	J                  number;

	--utility function to break clobs into an array of strings, delimited by semi-colons
	function parse_clob_to_array (a_clob clob) return string_array as
		result_array 	string_array;
		local_clob		CLOB;
		idx 			number(22,0);
	begin
		result_array := string_array();
		local_clob := a_clob;
		idx := 1;

		--break up the check string into pieces
		while length(local_clob) > 0 loop
			if instr(local_clob, ';') > 0 then
				--get everything until the next semicolon
				result_array.extend(1);
				result_array(IDX) := trim(substr(local_clob, 1, instr(local_clob, ';') - 1));
				local_clob := substr(local_clob, instr(local_clob, ';') + 1);
			else
				--no more simicolons - use the rest of the string in the last elements
				result_array.extend(1);
				result_array(IDX) := trim(local_clob);
				local_clob := '';
			end if;
			IDX := IDX + 1;
		end loop;

		return result_array;
	end parse_clob_to_array;

	--utility function to build a clobs from an array of strings, delimited by semi-colons
	function string_array_to_clob (a_string_array string_array) return clob as
		result_clob		CLOB;
		idx			number(22,0);
		arr_count	number(22,0);
	begin
		result_clob := '';
		--loop over elements of the input and build a semicolon delimited clob
		arr_count := a_string_array.count;
		for idx in 1 .. arr_count loop
			result_clob := result_clob || a_string_array(idx);
			if idx <> arr_count then 
				result_clob := result_clob || ';';
			end if;
		end loop;

		return result_clob;
	end string_array_to_clob;

  begin
	--Clean up previous runs
	delete from pp_processes_running
	 where sid not in
		   (select audsid from GV$SESSION where STATUS <> 'KILLED');

	STATUS := 'OK';

	begin
		--get our session's current process string for later reference
		select trim(process_name_string)
		into OLD_PROCESS_STRING
		from pp_processes_running
		where sid in ( select userenv('sessionid') from dual );
	exception
		when NO_DATA_FOUND then
			OLD_PROCESS_STRING := ' ';
	end;

	--Special case to remove the process
	begin
		if A_ARG2 = ' ' then
			if OLD_PROCESS_STRING = trim(A_ARG) then
				delete from pp_processes_running
				where sid in ( select userenv('sessionid') from dual );
			else
				--Remove specific processes that were passed in by creating two arrays and using the EXCEPT set operator
				declare 
					OLD_STRING_ARR	 	string_array;
					NEW_STRING_ARR		string_array;
					NEW_PROCESS_STRING 	clob;
				begin 
					--convert old and new strings into arrays
					OLD_STRING_ARR := parse_clob_to_array(OLD_PROCESS_STRING);
					NEW_STRING_ARR := parse_clob_to_array(A_ARG);

					--remove elements of new_string_arr from old_string_arr using except operator
					NEW_STRING_ARR := (OLD_STRING_ARR multiset except NEW_STRING_ARR);
					
					if NEW_STRING_ARR.COUNT = 0 then 
						--nothing in the new string - delete record completely
						delete from pp_processes_running
						where sid in ( select userenv('sessionid') from dual );
					else 
						--new array still has processes - rebuild into clob and update table 
						NEW_PROCESS_STRING := trim(string_array_to_clob(NEW_STRING_ARR));

						update pp_processes_running
						set process_name_string = NEW_PROCESS_STRING
						where sid in ( select userenv('sessionid') from dual );
					end if;
				end;
			end if;
			commit;
			return 'OK';
		end if;
	exception
		when others then
			RAISE_APPLICATION_ERROR(-20041,
						'Error removing running process ! Msg = ' ||
						sqlerrm(sqlcode) || ' code=' ||
						TO_CHAR(sqlcode));
			STATUS := 'Error';
	end;

	--Lock the process temporarily while we check the table
	if STATUS = 'OK' then
		--check to see if we have an existing record
		select count(*)
		into PROCESS_COUNT
		from pp_processes_running
		where sid in ( select userenv('sessionid') from dual );

		if PROCESS_COUNT = 0 and length(trim(A_ARG)) > 0 then
			-- no existing record - insert new one
			insert into pp_processes_running (sid, process_name_string)
			select userenv('sessionid'), trim(A_ARG) from dual;
		elsif length(trim(A_ARG)) > 0 then
			--record exists - update it
			if length(trim(OLD_PROCESS_STRING)) > 0 then
				--there are existing locked processes for our session - make sure there are
				-- 	no duplicates when the two are combined.
				declare
					OLD_STRING_ARR	 	string_array;
					NEW_STRING_ARR		string_array;
					NEW_PROCESS_STRING 	clob;
				begin
					--convert old and new strings into arrays
					OLD_STRING_ARR := parse_clob_to_array(OLD_PROCESS_STRING);
					NEW_STRING_ARR := parse_clob_to_array(A_ARG);

					--Combine new and old, getting a distinct list
					NEW_STRING_ARR := (OLD_STRING_ARR multiset union distinct NEW_STRING_ARR);

					--convert back into string
					NEW_PROCESS_STRING := trim(string_array_to_clob(NEW_STRING_ARR));

					--update existing record with new string
					update pp_processes_running
					set process_name_string = NEW_PROCESS_STRING
					where sid in ( select userenv('sessionid') from dual );
				end;
			else
				--existing record, but no process string - simply set.
				update pp_processes_running
				set process_name_string = trim(A_ARG)
				where sid in ( select userenv('sessionid') from dual );
			end if;
		end if;

		commit;
	end if;

	--Parse check string array
	if STATUS = 'OK' then
		CHECK_STRING     := A_ARG2;
		CHECK_STRING_ARR := parse_clob_to_array(CHECK_STRING);
	end if;

	--Parse process strings into arrays
	if STATUS = 'OK' then
		PROCESS_STRING_ARR := string_array();
		select userenv('sessionid') into AUDSID from dual;

		--loop over every record in in pp_processes_running
		for process in (select process_name_string, sid
						from pp_processes_running)
		loop

			if process.sid = AUDSID then
				--if we're in the current session, use the saved OLD_PROCESS_STRING,
				--	which doesn't contain the process we're currently trying to lock
				PROCESS_STRING := OLD_PROCESS_STRING;
			else
				PROCESS_STRING := process.process_name_string;
			end if;

			declare
				temp_arr string_array;
			begin
				--parse the process string into an array and add it to the collection
				temp_arr := parse_clob_to_array(PROCESS_STRING);
				PROCESS_STRING_ARR := (PROCESS_STRING_ARR multiset union distinct TEMP_ARR);
			end;
		end loop;
	end if;

	--Check to make sure the process isn't locked
	if STATUS = 'OK' then
		i := CHECK_STRING_ARR.FIRST;
		--loop over elements of our check_string_arr and make sure the same process string is not
		--	in the process_string_arr, which are the currently locked processes
		while i is not null loop
			--don't run the inner loop if we don't have to
			if instr(CHECK_STRING_ARR(i), '%') > 0 then
				--wildcard in our checkstring - loop over each element and do a LIKE comparison instead
				--	of "member of" which is faster
				j := PROCESS_STRING_ARR.FIRST;
				while j is not null loop
					if PROCESS_STRING_ARR(j) like CHECK_STRING_ARR(i) then
						--conflicting process - change status and exit loop
						STATUS := 'Error';
						EXIT;
					end if;
					j := PROCESS_STRING_ARR.NEXT(j);
				end loop;
			else
				if CHECK_STRING_ARR(i) member of PROCESS_STRING_ARR then
					STATUS := 'Error';
				end if;
			end if;
			i := CHECK_STRING_ARR.NEXT(i);
		end loop;
	end if;

	--Remove the temporary process lock if there were errors
	if STATUS <> 'OK' then
		if length(trim(OLD_PROCESS_STRING)) = 0 then
			delete from pp_processes_running
			where sid in ( select userenv('sessionid') from dual );
		else
			update pp_processes_running
			set process_name_string = OLD_PROCESS_STRING
			where sid in ( select userenv('sessionid') from dual );
		end if;
	end if;

	commit;

	return STATUS;

  exception
	when others then
	  RAISE_APPLICATION_ERROR(-20041,
							  'Error setting process lock ! Msg = ' ||
							  sqlerrm(sqlcode) || ' code=' ||
							  TO_CHAR(sqlcode));
	  return 'Error';
  end;

  -- =============================================================================
  --  Function PP_RELEASE_PROCESS
  -- =============================================================================
  function PP_RELEASE_PROCESS(A_ARG clob) return varchar2 as

	/*
	||============================================================================
	|| Application: PowerPlant
	|| Object Name: PP_RELEASE_PROCESS
	|| Description:
	||============================================================================
	|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
	||============================================================================
	|| Version Date       Revised By      Reason for Change
	|| ------- ---------- --------------- ----------------------------------------
	|| 1.0     02/03/2013 Stephen Motter  Create
	||============================================================================
	*/

  begin
	return PP_SET_PROCESS(To_Clob(A_ARG), ' ');
  end PP_RELEASE_PROCESS;

  -- =============================================================================
  --  Function PP_CHECK_PROCESS
  -- =============================================================================
  function PP_CHECK_PROCESS(A_ARG clob) return varchar2 as

	/*
	||============================================================================
	|| Application: PowerPlant
	|| Object Name: PP_CHECK_PROCESS
	|| Description:
	||============================================================================
	|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
	||============================================================================
	|| Version Date       Revised By      Reason for Change
	|| ------- ---------- --------------- ----------------------------------------
	|| 1.0     03/13/2013 Alex Pivoshenko Create
	|| 1.1     08/29/2013 C. Shilling     maint 31807
	|| 1.3     12/05/2013 Stephen Motter  Functions consolidated
	||============================================================================
	*/

  begin
	return PP_SET_PROCESS(' ', A_ARG);
  end PP_CHECK_PROCESS;

  -- =============================================================================
  --  Function PP_CHECK_PROCESS_PREFIX
  -- =============================================================================
  function PP_CHECK_PROCESS_PREFIX(A_ARG clob) return varchar2 as

	/*
	||============================================================================
	|| Application: PowerPlant
	|| Object Name: PP_CHECK_PROCESS_PREFIX
	|| Description:
	||============================================================================
	|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
	||============================================================================
	|| Version  Date       Revised By      Reason for Change
	|| -------- ---------- --------------- ---------------------------------------
	|| 10.4.1.0 08/01/2013 Charlie Shilling Create
	|| 1.3      12/05/2013 Stephen Motter  Functions consolidated
	||============================================================================
	*/

  begin
	return PP_SET_PROCESS(' ', A_ARG || '%');
  end PP_CHECK_PROCESS_PREFIX;


  function PP_RESET_DISCONNECTED(A_ARG varchar2, A_CO_ID number) return varchar2 as

	 /*
	 ||============================================================================
	 || Application: PowerPlant
	 || Object Name: PP_RESET_DISCONNECTED
	 || Description: Remove data from given Table_Name for data associated with a disconnected session.
	 ||============================================================================
	 || Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
	 ||============================================================================
	 || Version Date       Revised By      Reason for Change
	 || ------- ---------- --------------- ----------------------------------------
	 || 1.5     07/17/2014 Sunjin Cone Create
	 ||============================================================================
	 */
	STATUS varchar2(50);

	begin
		if A_ARG = 'UNITIZE_WO_LIST' then
			--This "not in" logic will delete for sessions where the STATUS = Killed as well as sessions that are no longer in the GV$SESSION becaues of clean disconnects.
			delete from UNITIZE_WO_LIST
			where company_id = A_CO_ID
			and db_session_id not in
				  (select audsid from GV$SESSION where STATUS <> 'KILLED');
		end if;

		STATUS := 'OK';

		commit;

		return STATUS;

	exception
		when others then
		RAISE_APPLICATION_ERROR(-20041,
									'Error clearing out stranded data from prior process run ! Msg = ' ||
									sqlerrm(sqlcode) || ' code=' ||
									TO_CHAR(sqlcode));
		return 'Error';
	end PP_RESET_DISCONNECTED;

end PP_PROCESS_PKG;
/

--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (2390, 0, 2015, 1, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2015.1.0.0_PP_PROCESS_PKG.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
