CREATE OR REPLACE package pkg_lease_import_sl is

 /*
   ||==================================================================================
   || Application: PowerPlan
   || Object Name: PKG_LEASE_IMPORT_SL
   || Description: Includes special handling for importing via SnapLogic
   ||==================================================================================
   || Copyright (C) 2018 by PowerPlan Inc, Inc. All Rights Reserved.
   ||==================================================================================
   || Version    Date       Revised By     Reason for Change
   || --------   ---------- -------------- --------------------------------------------
   || 2017.4.0   03/11/2018 Chris Navalata Original Version
   || 2018.1     09/10/2018 Chris Navalta  Added wrappers for Validate, Import Invoice
   ||===================================================================================
   */
  G_PKG_VERSION varchar(35) := '2018.2.1.0';

  PROCEDURE P_VALIDATE_LESSORS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2);

  PROCEDURE P_IMPORT_LESSORS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2);

  PROCEDURE P_VALIDATE_MLAS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2);

  PROCEDURE P_IMPORT_MLAS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2);

  PROCEDURE P_VALIDATE_ILRS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2);

  PROCEDURE P_IMPORT_ILRS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2);

  PROCEDURE P_VALIDATE_ASSETS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2);

  PROCEDURE P_IMPORT_ASSETS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2);

  PROCEDURE P_VALIDATE_INVOICES(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2);

  PROCEDURE P_IMPORT_INVOICES(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2);
  
  PROCEDURE P_VALIDATE_ILR_REVISION(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2);

  PROCEDURE P_IMPORT_ILR_REVISION(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2);

end PKG_LEASE_IMPORT_SL;
/

CREATE OR REPLACE package body pkg_lease_import_sl is

PROCEDURE P_VALIDATE_LESSORS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2) is
  L_ERROR_MSG varchar2(4000);
BEGIN
   A_RESULT := PKG_LEASE_IMPORT.F_VALIDATE_LESSORS(A_RUN_ID);

   SELECT count(error_message) INTO l_error_msg from LS_IMPORT_LESSOR
   WHERE IMPORT_RUN_ID = A_RUN_ID
   AND error_message is not null;

/* if there was a validation error, flag it so it can be logged to pp_processes_messages by SL*/
   IF l_error_msg > 0 AND A_RESULT = 'OK' then
      A_RESULT := 'ERRORMSG';
   END IF;

END P_VALIDATE_LESSORS;

PROCEDURE P_IMPORT_LESSORS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2) is
BEGIN
A_RESULT := PKG_LEASE_IMPORT.F_IMPORT_LESSORS(A_RUN_ID);

END P_IMPORT_LESSORS;


PROCEDURE P_VALIDATE_MLAS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2) is
   L_ERROR_MSG varchar2(4000);
BEGIN
   A_RESULT := PKG_LEASE_IMPORT.F_VALIDATE_MLAS(A_RUN_ID);

   SELECT count(error_message) INTO l_error_msg from LS_IMPORT_LEASE
   WHERE IMPORT_RUN_ID = A_RUN_ID
   AND error_message is not null;

/* if there was a validation error, flag it so it can be logged to pp_processes_messages by SL*/
   IF l_error_msg > 0 AND A_RESULT = 'OK' then
      A_RESULT := 'ERRORMSG';
   END IF;

END P_VALIDATE_MLAS;

PROCEDURE P_IMPORT_MLAS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2) is
BEGIN
A_RESULT := PKG_LEASE_IMPORT.F_IMPORT_MLAS(A_RUN_ID);
END P_IMPORT_MLAS;


PROCEDURE P_VALIDATE_ILRS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2) is
   L_ERROR_MSG varchar2(4000);
BEGIN
   A_RESULT := PKG_LEASE_IMPORT.F_VALIDATE_ILRS(A_RUN_ID);

   SELECT count(error_message) INTO l_error_msg from LS_IMPORT_ILR
   WHERE IMPORT_RUN_ID = A_RUN_ID
   AND error_message is not null;

/* if there was a validation error, flag it so it can be logged to pp_processes_messages by SL*/
   IF l_error_msg > 0 AND A_RESULT = 'OK' then
      A_RESULT := 'ERRORMSG';
   END IF;

END P_VALIDATE_ILRS;

PROCEDURE P_IMPORT_ILRS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2) is
BEGIN
A_RESULT := PKG_LEASE_IMPORT.F_IMPORT_ILRS(A_RUN_ID);
END P_IMPORT_ILRS;


PROCEDURE P_VALIDATE_ASSETS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2) is
   L_ERROR_MSG varchar2(4000);
BEGIN
   A_RESULT := PKG_LEASE_IMPORT.F_VALIDATE_ASSETS(A_RUN_ID);

   SELECT count(error_message) INTO l_error_msg from LS_IMPORT_ASSET
   WHERE IMPORT_RUN_ID = A_RUN_ID
   AND error_message is not null;

/* if there was a validation error, flag it so it can be logged to pp_processes_messages by SL*/
   IF l_error_msg > 0 AND A_RESULT = 'OK' then
      A_RESULT := 'ERRORMSG';
   END IF;

END P_VALIDATE_ASSETS;

PROCEDURE P_IMPORT_ASSETS(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2) is
BEGIN
A_RESULT := PKG_LEASE_IMPORT.F_IMPORT_ASSETS(A_RUN_ID);
END P_IMPORT_ASSETS;

PROCEDURE P_VALIDATE_INVOICES(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2) is
   L_ERROR_MSG varchar2(4000);
BEGIN
   A_RESULT := PKG_LEASE_IMPORT.F_VALIDATE_INVOICES(A_RUN_ID);

   SELECT count(error_message) INTO l_error_msg from LS_IMPORT_INVOICE
   WHERE IMPORT_RUN_ID = A_RUN_ID
   AND error_message is not null;

/* if there was a validation error, flag it so it can be logged to pp_processes_messages by SL*/
   IF l_error_msg > 0 AND A_RESULT = 'OK' then
      A_RESULT := 'ERRORMSG';
   END IF;

END P_VALIDATE_INVOICES;

PROCEDURE P_IMPORT_INVOICES(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2) is
BEGIN
A_RESULT := PKG_LEASE_IMPORT.F_IMPORT_INVOICES(A_RUN_ID);
END P_IMPORT_INVOICES;

PROCEDURE P_VALIDATE_ILR_REVISION(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2) is
   L_ERROR_MSG varchar2(4000);
BEGIN
   A_RESULT := PKG_LEASE_IMPORT.F_VALIDATE_ILR_REVISION(A_RUN_ID);

   SELECT count(error_message) INTO l_error_msg from LS_IMPORT_ILR_REV
   WHERE IMPORT_RUN_ID = A_RUN_ID
   AND error_message is not null;

/* if there was a validation error, flag it so it can be logged to pp_processes_messages by SL*/
   IF l_error_msg > 0 AND A_RESULT = 'OK' then
      A_RESULT := 'ERRORMSG';
   END IF;

END P_VALIDATE_ILR_REVISION;

PROCEDURE P_IMPORT_ILR_REVISION(A_RUN_ID IN NUMBER, A_RESULT OUT VARCHAR2) is
BEGIN
A_RESULT := PKG_LEASE_IMPORT.F_IMPORT_ILR_REVISION(A_RUN_ID);
END P_IMPORT_ILR_REVISION;


end PKG_LEASE_IMPORT_SL;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (18187, 0, 2018, 2, 1, 0, 0, 'C:\BitBucketRepos\Classic_PB\scripts\00_base\packages', 
    'PKG_LEASE_IMPORT_SL.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
