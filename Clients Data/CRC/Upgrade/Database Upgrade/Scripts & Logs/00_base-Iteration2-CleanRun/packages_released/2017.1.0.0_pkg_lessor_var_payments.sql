CREATE OR REPLACE package pkg_lessor_var_payments is

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LESSOR_VAR_PAYMENTS
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version    Date       Revised By      Reason for Change
   || ---------- ---------- --------------  ----------------------------------------
   || 2017.1.0.0 10/25/2017 A. Hill         Original Verion (Copied from PKG_LEASE_VAR_PAYMENTS)
   ||============================================================================
   */

   function f_get_initial_pay_terms(a_ilr_id number, a_revision number) return lsr_ilr_op_sch_pay_term_tab pipelined;

  -- Function to calc the value of an index/rate formula component for a given variable name and month
  function F_CALC_VAR_PAYMENT_INITIAL_AMT(A_VARIABLE_PAYMENT_ID number,
                                          A_ILR_ID              number,
                                          A_REVISION            number) return number;

  -- Function to calculate an index/rate formula component value
  FUNCTION F_CALC_INDEX(A_COMPONENT_ID           number,
                        A_ILR_ID                 number,
                        A_REVISION               number,
                        A_INITIAL_AMT_FLAG       number,
                        A_MONTH                  date ) return number;

  -- Function to calculate a variable component
  function F_CALC_VAR_COMPONENT(A_COMPONENT_ID number,
--                                A_ILR_ID       number,
                                A_ASSET_ID     number,
                                A_MONTH        date) return number;

   -- Function to calculate the value of a historic component
   function F_CALC_HISTORIC(A_COMPONENT_ID          number,
                            A_ID                    number,
                            A_REVISION              number,
                            A_ASSET_LEVEL_FLAG      number,
                            A_SET_OF_BOOKS_ID       number,
                            A_MONTH	                date) return number;


   -- Function to calculate all variable payments for an ILR in appropriate order
   function F_CALC_ILR_VAR_PAYMENTS(A_ILR_ID     number,
                                    A_REVISION   number,
                                    A_MONTH      date) return varchar2;

   -- Function to calculate variable payments for all ILR/revision combos currently in the schedule build staging tables

   function F_CALC_ALL_VAR_PAYMENTS(A_MONTH   date) return varchar2;

   -- Function to calculate variable payments for each asset bucket
   function F_CALC_ASSET_BUCKET( A_ILR_ID number, A_ASSET_ID number, A_REVISION number, A_SET_OF_BOOKS_ID number, A_PAYMENT_TERM_ID number, A_BUCKET_NUMBER number, A_MONTH date, A_PROCESS_TYPE varchar2) return number;

   -- Function to rollup all buckets from the assets to ILR level.
   function F_ROLLUP_ASSETS_BUCKETS_TO_ILR(A_COMPANY_ID in number, A_MONTH in date, A_CALC_TYPE varchar2) return varchar2;

   -- Function to calculate variable payments for all assets of a company during month end.
   function F_CALC_ASSET_BUCKETS_MONTH_END(A_COMPANY_ID in number, A_MONTH in date, CALC_TYPE varchar2) return varchar2;

   -- Function to calculate paid amount for a given asset and month
   function F_CALC_ASSET_PAID_AMOUNT(A_ASSET_ID       number,
                                     A_REVISION       number,
                                     A_MONTH          date,
                                     A_BUCKET_NUMBER  number,
                                     A_SET_OF_BOOKS_ID number) return number;

   -- Function to update the ILR and asset schedule tables during calculation of variable payments
   function F_UPDATE_SCHEDULE_TABLES(A_ILR_ID            number,
                                     A_MONTH             date,
                                     A_REVISION          number) return varchar2;

   -- Function to return if a given month is a payment month for a given ILR and revision
   function F_IS_PAYMENT_MONTH(A_ILR_ID     number,
                               A_REVISION   number,
                               A_MONTH      date) return boolean;

   -- Function to return the first payment month for a given ILR and revision
   function F_GET_FIRST_PAYMENT_TERM_MONTH(A_ILR_ID     number,
                                           A_REVISION   number) return date;

	function F_IS_PAYMENT_MONTH_RTN_NUMBER(A_ILR_ID     number,
                               A_REVISION   number,
                               A_MONTH      date) return INTEGER;

  FUNCTION f_get_rate(a_ilr_id NUMBER, a_revision NUMBER, a_rate_type_id NUMBER) RETURN NUMBER;

END pkg_lessor_var_payments;
/

create or replace PACKAGE BODY pkg_lessor_var_payments AS
/*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LESSOR_VAR_PAYMENTS
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version    Date       Revised By      Reason for Change
   || ---------- ---------- --------------  ----------------------------------------
   || 2017.1.0.0 10/25/2017 A. Hill         Original Version (Copied from PKG_LEASE_VAR_PAYMENTS)
   ||============================================================================
   */

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

  -- Function and procedure implementations
  FUNCTION f_get_initial_pay_terms(a_ilr_id NUMBER, a_revision NUMBER) RETURN lsr_ilr_op_sch_pay_term_tab PIPELINED IS
  BEGIN
    -- The buckets should be in their own table, but until that happens we're stuck with this pivot/unpivot string matching mess
    for rec in (WITH buckets
                as (SELECT  ilr_id,
                            revision,
                            payment_term_id,
                            CASE substr(LOWER(bucket), 0, 1)
                              WHEN 'c' THEN 'contingent'
                              WHEN 'e' THEN 'executory'
                            END as bucket_type,
                            lower(bucket) as bucket,
                            bucket_amount
                    FROM lsr_ilr_payment_term
                    UNPIVOT INCLUDE NULLS (bucket_amount FOR bucket IN (c_bucket_1,
                                                                        c_bucket_2,
                                                                        c_bucket_3,
                                                                        c_bucket_4,
                                                                        c_bucket_5,
                                                                        c_bucket_6,
                                                                        c_bucket_7,
                                                                        c_bucket_8,
                                                                        c_bucket_9,
                                                                        c_bucket_10,
                                                                        e_bucket_1,
                                                                        e_bucket_2,
                                                                        e_bucket_3,
                                                                        e_bucket_4,
                                                                        e_bucket_5,
                                                                        e_bucket_6,
                                                                        e_bucket_7,
                                                                        e_bucket_8,
                                                                        e_bucket_9,
                                                                        e_bucket_10))),
                variable_payments
                AS (SELECT  var_payment.ilr_id,
                            var_payment.revision,
                            var_payment.payment_term_id,
                            var_payment.variable_payment_id,
                            pay_term.bucket,
                            pay_term.bucket_amount
                    FROM lsr_ilr_payment_term_var_pay var_payment
                    JOIN buckets pay_term
                      ON var_payment.ilr_id = pay_term.ilr_id
                      AND var_payment.revision = pay_term.revision
                      AND var_payment.payment_term_id = pay_term.payment_term_id
                      AND LOWER(substr(var_payment.receivable_type, 0, 1)) || '_bucket_' || bucket_number = lower(pay_term.bucket)
                    WHERE var_payment.incl_in_initial_measure = 1)
                SELECT lsr_ilr_op_sch_pay_term( freq.payment_month_freq,
                                                pay_term.payment_term_date,
                                                pay_term.number_of_terms,
                                                pay_term.payment_amount + SUM(pay_term.add_payment_amount),
                                                CAST(COLLECT(pay_term.contingent_bucket) AS lsr_bucket_amount_tab),
                                                CAST(COLLECT(pay_term.executory_bucket) AS lsr_bucket_amount_tab),
                                                lease.pre_payment_sw) AS payment_term
                FROM (SELECT  terms.payment_term_date,
                              terms.number_of_terms,
                              terms.payment_freq_id,
                              terms.payment_amount,
                              terms.ilr_id,
                              CASE
                                WHEN var_pay.ilr_id IS NULL
                                  THEN 0
                                WHEN var_pay.variable_payment_id IS NULL
                                  THEN SUM(var_pay.bucket_amount) OVER (PARTITION BY  var_pay.ilr_id,
                                                                                      var_pay.revision,
                                                                                      var_pay.payment_term_id)
                                ELSE pkg_lessor_var_payments.f_calc_var_payment_initial_amt(var_pay.variable_payment_id,
                                                                                            terms.ilr_id,
                                                                                            terms.revision)
                              END AS add_payment_amount,
                              CASE buckets.bucket_type
                                WHEN 'contingent'
                                  THEN lsr_bucket_amount(buckets.bucket,  CASE
                                                                            WHEN var_pay.ilr_id IS NULL
                                                                              THEN buckets.bucket_amount
                                                                            ELSE buckets.bucket_amount - var_pay.bucket_amount
                                                                          END)
                              END AS contingent_bucket,
                              CASE buckets.bucket_type
                                WHEN 'executory'
                                THEN lsr_bucket_amount( buckets.bucket,
                                                        CASE
                                                          WHEN var_pay.ilr_id IS NULL
                                                            THEN buckets.bucket_amount
                                                          ELSE buckets.bucket_amount - var_pay.bucket_amount
                                                        END)
                              END AS executory_bucket
                      FROM lsr_ilr_payment_term terms
                      JOIN buckets on terms.ilr_id = buckets.ilr_id
                                    AND terms.revision = buckets.revision
                                    and terms.payment_term_id = buckets.payment_term_id
                      LEFT JOIN variable_payments var_pay ON terms.ilr_id = var_pay.ilr_id
                                                            AND terms.revision = var_pay.revision
                                                            AND terms.payment_term_id = var_pay.payment_term_id
                                                            AND buckets.bucket = var_pay.bucket
                      WHERE terms.ilr_id = a_ilr_id
                      AND terms.revision = a_revision) pay_term
                JOIN ls_payment_freq freq ON pay_term.payment_freq_id = freq.payment_freq_id
                JOIN lsr_ilr ilr ON pay_term.ilr_id = ilr.ilr_id
                JOIN lsr_lease lease on ilr.lease_id = lease.lease_id
                GROUP BY  freq.payment_month_freq,
                          pay_term.payment_term_date,
                          pay_term.number_of_terms,
                          pay_term.payment_amount,
                          lease.pre_payment_sw)
    LOOP
      PIPE ROW(rec.payment_term);
    END LOOP;
  END f_get_initial_pay_terms;

  FUNCTION f_get_rate(a_ilr_id NUMBER, a_revision NUMBER, a_rate_type_id NUMBER) RETURN NUMBER IS
    l_msg varchar2(2000);
    l_result NUMBER;
  BEGIN
    SELECT rate INTO l_result
    FROM lsr_ilr_rates R
    JOIN lsr_ilr_rate_types rt ON R.rate_type_id = rt.rate_type_id
    WHERE r.ilr_id = a_ilr_id
    AND R.revision = a_revision
    AND R.rate_type_id = a_rate_type_id;

    return l_result;

  EXCEPTION
    WHEN OTHERS THEN
      L_MSG := SUBSTR('ERROR retrieving rate: ' || sqlerrm, 1, 2000);
         return L_MSG;
  END f_get_rate;


  function F_CALC_VAR_PAYMENT_INITIAL_AMT(A_VARIABLE_PAYMENT_ID number,
                                          A_ILR_ID              number,
                                          A_REVISION            number) return number is

           L_MSG varchar2(2000);
           L_SQLS varchar2(2000);
           L_FORMULA varchar2(4000);
           L_MONTH date;
           L_RESULT number(22,8);

  BEGIN

    SELECT PAYMENT_FORMULA_DB
      INTO l_formula
      FROM LSR_VARIABLE_PAYMENT
      WHERE VARIABLE_PAYMENT_ID = A_VARIABLE_PAYMENT_ID;

      --use the first payment term month when calculating the initial amount
      L_MONTH := F_GET_FIRST_PAYMENT_TERM_MONTH(A_ILR_ID, A_REVISION);

      L_MSG := '   Executing formula: ' || L_FORMULA;
      --PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      --This formula can only contain formula components  constants if included in initial amount
      --So a simple select will work
      L_SQLS := 'SELECT nvl(' || L_FORMULA || ',0)' ||
                ' FROM (
                       SELECT to_date('|| to_char(L_MONTH,'yyyymm') || ',''yyyymm'') as month from dual
                   ) a,
                   ( select 1 as initial_amt_flag from dual) b,
                   lsr_ilr_options lio
                   where lio.ilr_id = :ilr_id
                   and lio.revision = :revision';

      EXECUTE IMMEDIATE L_SQLS
       INTO L_RESULT
       USING A_ILR_ID, A_REVISION;

       L_MSG := '   Got the following initial amount result: ' || to_char(L_RESULT);
       --PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

       return L_RESULT;

  exception
    when others then
      L_MSG := SUBSTR('ERROR calculating variable payment initial amount: ' || sqlerrm, 1, 2000);
         return L_MSG;
  end F_CALC_VAR_PAYMENT_INITIAL_AMT;

  FUNCTION F_CALC_INDEX(A_COMPONENT_ID            number,
                        A_ILR_ID                  number,
                        A_REVISION                number,
                        A_INITIAL_AMT_FLAG        number,
                        A_MONTH                   date ) return number is

    L_COMPONENT_ID      number(22,0);
    L_SQLS              varchar2(2000);
    L_MSG               varchar2(2000);
    L_COMPONENT_AMOUNT  number(22,8);
    L_COMPONENT_TYPE    number(1,0);

  begin
    --check if we're calculating an initial amount and if given month is a payment month
    --if not calculating initial amount and not a payment month, return null to zero out the formula value
    --if calculating an initial amount, we want to return the component value regardless of if it's a payment month or not
    if A_INITIAL_AMT_FLAG = 0 and F_IS_PAYMENT_MONTH(A_ILR_ID, A_REVISION, A_MONTH) = false then
      return null;
    end if;

    L_COMPONENT_ID := TO_NUMBER(A_COMPONENT_ID);

    L_SQLS := 'SELECT amount
                FROM (
                    SELECT amount
                    FROM lsr_index_component_values
                    WHERE formula_component_id = :component_id
                    AND effective_date <= :open_month
                    ORDER BY effective_date DESC
                )
                WHERE ROWNUM = 1';

  BEGIN
      EXECUTE IMMEDIATE L_SQLS
        INTO L_COMPONENT_AMOUNT
        USING L_COMPONENT_ID, A_MONTH;
  EXCEPTION
    WHEN No_Data_Found THEN
      RETURN 0 ;
  END;

    L_MSG := '   Got the following component value: ' || to_char(L_COMPONENT_AMOUNT);
     --  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

    RETURN L_COMPONENT_AMOUNT;
  exception
    when others then
      L_MSG := SUBSTR('Error calculating formula index/rate component: ' || sqlerrm, 1, 2000);
         return L_MSG;
  end F_CALC_INDEX;

  function F_CALC_VAR_COMPONENT(A_COMPONENT_ID NUMBER,
                                A_ASSET_ID     number,
                                A_MONTH        date) return number is

   L_COMPONENT_ID               number(22,0);
   L_SQLS                       varchar2(2000);
   L_MSG                        varchar2(2000);
   L_COMPONENT_AMOUNT           number(22,8);
   L_COMPONENT_NUMBER_FORMAT    number(1,0);
   L_ARG_MONTH                  number(2,0);
   L_ARG_YEAR                   number(4,0);
   L_PAY_MONTH                  number(2,0);
   L_TEMP_MONTH                 number(2,0);
   L_TEMP_MONTH_CHAR            varchar2(35);
   L_CLOSEST_PAY_MONTH         number(2,0);
   L_LOAD_MONTH                varchar2(35);
   L_TEMP_MONTH_NUMBER         number(6,0);
   L_LOAD_MONTHS               varchar2(254);

   begin
    --get month number from A_MONTH and check if it's a payment month for any load month on the component calendar
    L_ARG_MONTH := to_number(to_char(A_MONTH,'mm'));
    L_ARG_YEAR := to_number(to_char(A_MONTH,'yyyy'));

    L_SQLS :=  'SELECT distinct PAYMENT_MONTH_NUMBER
                FROM lsr_var_component_calendar
                WHERE FORMULA_COMPONENT_ID = :component_id
                AND PAYMENT_MONTH_NUMBER = :month';

    BEGIN
          EXECUTE IMMEDIATE L_SQLS
            INTO L_PAY_MONTH
            USING A_COMPONENT_ID, L_ARG_MONTH;
      EXCEPTION
        --if the SQL returns no data, then the passed in month is not a payment month, return null to zero out the formula
        WHEN No_Data_Found THEN
          RETURN null ;
      END;

    SELECT NUMBER_FORMAT_ID
    INTO l_component_number_format
    FROM LSR_FORMULA_COMPONENT
    WHERE FORMULA_COMPONENT_ID = A_COMPONENT_ID;

    -- only sum the amounts if the number is not a percentage
    IF L_COMPONENT_NUMBER_FORMAT <> 3 THEN
      --since the calendar only has pure month numbers (2 digits), we need to relate those months to
      --the corresponding full month numbers (6 digits) related to the component value data in LSR_VARIABLE_COMPONENT_VALUES

      --start the string build
      L_LOAD_MONTHS := '(';

     --get the load_month_numbers from the calendar with this payment_month_number
     for month in (select load_month_number
                    from lsr_var_component_calendar
                    where formula_component_id = A_COMPONENT_ID
                    and payment_month_number = L_ARG_MONTH)
      loop
        --convert so that leading 0s are added
        L_TEMP_MONTH_CHAR := to_char(to_date(month.load_month_number,'mm'),'mm');

        --add the year, taking care to use previous year for any month > the passed in month
        if month.load_month_number > L_ARG_MONTH then
          L_TEMP_MONTH_NUMBER := to_number(to_char(L_ARG_YEAR - 1) || L_TEMP_MONTH_CHAR);
        else
          L_TEMP_MONTH_NUMBER := to_number(to_char(L_ARG_YEAR) || L_TEMP_MONTH_CHAR);
        end if;

        --add month number to the string containing all full month numbers
        L_LOAD_MONTHS := L_LOAD_MONTHS || to_char(L_TEMP_MONTH_NUMBER) || ',';

      end loop;

      --remove the last ',' and finish the string
      L_LOAD_MONTHS := SUBSTR(L_LOAD_MONTHS, 1, LENGTH(L_LOAD_MONTHS)-1);
      L_LOAD_MONTHS := L_LOAD_MONTHS || ')';

      l_sqls := 'select sum(amount) amount
                 from lsr_variable_component_values vc
                 where vc.formula_component_id = :component_id
                 and vc.asset_id = :asset_id
                 and vc.incurred_month_number in '|| L_LOAD_MONTHS || ' ' || '
                 group by vc.formula_component_id, vc.ilr_id, vc.asset_id';

      --retrieve the value
      BEGIN
          EXECUTE IMMEDIATE L_SQLS
            INTO L_COMPONENT_AMOUNT
            USING A_COMPONENT_ID, A_ASSET_ID;
      EXCEPTION
        WHEN No_Data_Found THEN
          RETURN 0 ;
      END;

    ELSE
      -- if it is a percentage, retrieve the latest
      -- this requires building a list of the incurred months for this particular payment month
      -- then selecting the most recently loaded value

       --start the string build
      L_LOAD_MONTHS := '(';

       --get the load_month_numbers from the calendar with this payment_month_number
       for month in (select load_month_number
                      from lsr_var_component_calendar
                      where formula_component_id = A_COMPONENT_ID
                      and payment_month_number = L_ARG_MONTH)
        loop
          --convert so that leading 0s are added
          L_TEMP_MONTH_CHAR := to_char(to_date(month.load_month_number,'mm'),'mm');

          --add the year, taking care to use previous year for any month > the passed in month
          if month.load_month_number > L_ARG_MONTH then
            L_TEMP_MONTH_NUMBER := to_number(to_char(L_ARG_YEAR - 1) || L_TEMP_MONTH_CHAR);
          else
            L_TEMP_MONTH_NUMBER := to_number(to_char(L_ARG_YEAR) || L_TEMP_MONTH_CHAR);
          end if;

          --add month number to the string containing all full month numbers
          L_LOAD_MONTHS := L_LOAD_MONTHS || to_char(L_TEMP_MONTH_NUMBER) || ',';

        end loop;

        --remove the last ',' and finish the string
        L_LOAD_MONTHS := SUBSTR(L_LOAD_MONTHS, 1, LENGTH(L_LOAD_MONTHS)-1);
        L_LOAD_MONTHS := L_LOAD_MONTHS || ')';

         --get the actual value from the list of load months
        L_SQLS := ' select amount from (
                      select amount
                      from lsr_variable_component_values
                      where formula_component_id = :component_id
                      and asset_id = :asset_id
                      and incurred_month_number in '|| L_LOAD_MONTHS || '
                      order by incurred_month_number desc
                     )
                     where ROWNUM = 1'
                    ;

        BEGIN
            EXECUTE IMMEDIATE L_SQLS
              INTO L_COMPONENT_AMOUNT
              USING A_COMPONENT_ID, A_ASSET_ID;
        EXCEPTION
          WHEN No_Data_Found THEN
            RETURN 0 ;
        END;

    END IF;

    RETURN L_COMPONENT_AMOUNT;

   exception
    when others then
      L_MSG := SUBSTR('Error calculating formula variable component: ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_CALC_VAR_COMPONENT;

   --Function to calculate historic component value
   --The A_ID argument is either ILR ID or asset ID
   --The formula with a historic component follows the associated VP for asset or ILR level
   --So this function must support calculating either way
   function F_CALC_HISTORIC(A_COMPONENT_ID          number,
                            A_ID                    number,
                            A_REVISION              number,
                            A_ASSET_LEVEL_FLAG      number,
                            A_SET_OF_BOOKS_ID       number,
                            A_MONTH                 date) return number is

   L_SQLS                   varchar2(5000);
   L_MSG                    varchar2(2000);
   L_COMPONENT_AMOUNT       number;
   L_ARG_MONTH              number(2,0);
   L_ARG_YEAR               number(4,0);
   L_COMPONENT_YEAR         number(4,0);
   L_COMPONENT_MONTH        number(2,0);
   L_COMPONENT_STR          varchar2(6);
   L_PAY_MONTH              number(2,0);
   L_ASSOC_BUCKET_NUMBER    number;
   L_ASSOC_BUCKET_STR       varchar2(2000);
   L_ILR_ID                 number;
   L_MONTHS_PER_TERM         number;
   L_ANNUAL                  number;
   L_SEMI_ANNUAL             number;
   L_QUARTERLY               number;
   L_MONTHLY                 number;
   L_ACTUAL                  number;
   L_NEXT_PAYMENT_MONTH      varchar2(35);
   l_term_component_pay_months VARCHAR2(2000);
   l_temp_amount               NUMBER;
   l_prepay_switch          number(1,0);

   TYPE T_PAYMENT_MONTHS is table of number index by varchar2(35);

   L_COMPONENT_CALC_MONTHS          t_payment_months;
   L_TERM_PAYMENT_MONTHS               t_payment_months;

   begin
     L_ARG_MONTH := to_number(to_char(A_MONTH,'mm'));
     L_ARG_YEAR := to_number(to_char(A_MONTH,'yyyy'));

     --determine if the given month is a payment month for this component
     l_sqls :=  'SELECT distinct PAYMENT_MONTH_NUMBER
                FROM LSR_HIST_COMPONENT_CALENDAR
                WHERE FORMULA_COMPONENT_ID = :component_id
                AND PAYMENT_MONTH_NUMBER = :month';

      BEGIN
          EXECUTE IMMEDIATE L_SQLS
            INTO L_PAY_MONTH
            USING A_COMPONENT_ID, L_ARG_MONTH;
      EXCEPTION
        --if the SQL returns no data, then the passed in month is not a payment month, return null to zero out the formula
        WHEN No_Data_Found THEN
          RETURN null ;
      END;

    --if this component is asset level, get the ILR ID
    --A_ID will be an asset ID in this case
    if A_ASSET_LEVEL_FLAG = 1 then
      select ilr_id
      INTO l_ilr_id
      FROM lsr_asset
      where lsr_asset_id = A_ID
      and revision = A_REVISION;
    else
      L_ILR_ID := A_ID;
    end if;
    
    SELECT pre_payment_sw INTO l_prepay_switch
    FROM lsr_lease
    where lease_id = (select distinct lease_id from lsr_ilr where ilr_id = l_ilr_id);

    --create a master list (array) of all payment months for the component
    --this will be based on the calc months combined with the timing column, so it can only span over 2 years at most
    --example: its 201203 and March is a payment month for Jan-Mar, with Jan timing as Previous Year and Feb-Mar as Current Year
    --the month numbers stored would be 201101,201202,201203
    FOR calc_month IN ( SELECT calc_month_number, TIMING
                        FROM lsr_hist_component_calendar
                        WHERE formula_component_id = a_component_id
                        and payment_month_number = L_ARG_MONTH)
    loop
      --extract correct year based on timing
      if calc_month.timing = 1 then
        L_COMPONENT_YEAR := L_ARG_YEAR;
      elsif calc_month.timing = 2 then
        L_COMPONENT_YEAR := L_ARG_YEAR - 1;
      end if;

      --create the month number string with leading zero on the month if needed
      L_COMPONENT_STR := to_char(L_COMPONENT_YEAR) || lpad(to_char(calc_month.calc_month_number), 2, '0');

      --add the full month number to the master list
      --the index is the month number string, which is all we care about
      --so the value associated is just a dummy- use the calc_month_number
      L_COMPONENT_CALC_MONTHS(L_COMPONENT_STR) := calc_month.calc_month_number;

    end loop;

     --set the variables for payment frequency types
     L_ANNUAL := 1;
     L_SEMI_ANNUAL := 2;
     L_QUARTERLY := 3;
     L_MONTHLY := 4;
     L_ACTUAL := 5;

    --initialize the amount
    L_COMPONENT_AMOUNT := 0;

    --loop over the payment terms
    for payment_term in (select payment_term_id,
                                payment_term_type_id,
                                payment_term_date,
                                payment_freq_id,
                                number_of_terms
                         FROM lsr_ilr_payment_term
                         WHERE ilr_id = l_ilr_id
                         AND revision = a_revision
                         order by payment_term_date)
    loop
      --reset the string to hold the months as well as the string to hold associated buckets to sum
      L_TERM_COMPONENT_PAY_MONTHS := '(';
      L_ASSOC_BUCKET_STR := '(';

      --get the buckets for the VP associated to this component in this payment term
      FOR bucket IN (SELECT bucket_number
                     FROM lsr_ilr_payment_term_var_pay ptvp
                     INNER JOIN lsr_historic_component_values hcv
                       on hcv.formula_component_id = A_COMPONENT_ID
                       and hcv.variable_payment_id = ptvp.variable_payment_id
                     where ptvp.ilr_id = L_ILR_ID
                     and ptvp.revision = A_REVISION
                     and payment_term_id = payment_term.payment_term_id)
       loop
         L_ASSOC_BUCKET_STR := L_ASSOC_BUCKET_STR || 'sum(contingent_paid' || to_char(bucket.bucket_number) || ')+';
       end loop;

       --if the VP is not associated to any buckets for this term, go to the next loop
       --the string will only have the leading paren if this is the case
       if LENGTH(L_ASSOC_BUCKET_STR) = 1 then
         continue;
       end if;

       --trim the final + from the bucket string and add the trailing paren
      L_ASSOC_BUCKET_STR := SUBSTR(L_ASSOC_BUCKET_STR, 1, LENGTH(L_ASSOC_BUCKET_STR)-1) || ')';

      --create a list of months in this payment term that are calculation months for the historic component
      --  i.e. if the component calcs jan-mar to pay in mar, then get the month numbers from this payment term for jan-mar
      --  if instead feb was the payment term start date, then this prevents double counting, as it would have been calced in a previous loop
      --  in that case, we'd only add feb and mar to the list since jan would have been included in the sum already

      --set the number of months per term for this payment term
       if payment_term.payment_freq_id = L_ANNUAL then
         L_MONTHS_PER_TERM := 12;
       elsif payment_term.payment_freq_id = L_SEMI_ANNUAL then
         L_MONTHS_PER_TERM := 6;
       elsif payment_term.payment_freq_id = L_QUARTERLY then
         L_MONTHS_PER_TERM := 3;
       elsif payment_term.payment_freq_id = L_MONTHLY or payment_term.payment_freq_id = L_ACTUAL then
         L_MONTHS_PER_TERM := 1;
       end if;

       --loop over the number of terms and check if payment months coincide with component payment months
       --if so, add those months to a string to be used in summing the component value
       for term in 1..payment_term.number_of_terms
         loop
           --add months to payment_term_date based on payment frequency and current term number
           --subtract 1 month from the start month because add_months appends to the end, which would throw everything off by 1 month
           IF l_prepay_switch = 0 THEN
              l_next_payment_month := to_char(add_months(add_months(payment_term.payment_term_date,-1), l_months_per_term*TERM),'yyyymm');
           ELSE
              l_next_payment_month := to_char(add_months(payment_term.payment_term_date, l_months_per_term * (term - 1)), 'yyyymm');
           END IF;

           --check if this payment month is in the component calc months
           if L_COMPONENT_CALC_MONTHS.EXISTS(L_NEXT_PAYMENT_MONTH) then
             L_TERM_COMPONENT_PAY_MONTHS := L_TERM_COMPONENT_PAY_MONTHS || L_NEXT_PAYMENT_MONTH || ',';
           end if;

       end loop;

      --if no months were added to the string, proceed to the next loop
      if LENGTH(L_TERM_COMPONENT_PAY_MONTHS) = 1 then
        continue;
      end if;

      --finish the string holding the months - remove the trailing , and add the final )
      L_TERM_COMPONENT_PAY_MONTHS := SUBSTR(L_TERM_COMPONENT_PAY_MONTHS, 1, LENGTH(L_TERM_COMPONENT_PAY_MONTHS)-1) || ')';

      --now sum the buckets over the months from this payment term which are also calc months for the component
      --if asset level, select from the asset schedule
      --otherwise, select from the ilr_schedule
      if A_ASSET_LEVEL_FLAG = 1 then
        l_sqls := 'select ' || l_assoc_bucket_str || ' ' ||
                  'from LSR_ASSET_SCHEDULE_TMP ' ||
                  'where lsr_asset_id = :a_asset_id ' ||
                  'and revision = :a_revision ' ||
                  'and set_of_books_id = :a_set_of_books_id ' ||
                  'and to_char(month,''yyyymm'') in ' || L_TERM_COMPONENT_PAY_MONTHS;

         execute immediate L_SQLS
         into L_TEMP_AMOUNT
         using A_ID, A_REVISION, A_SET_OF_BOOKS_ID;

       else
         l_sqls := 'select ' || l_assoc_bucket_str || ' ' ||
                  'from LSR_ILR_SCHEDULE ' ||
                  'where ilr_id = :a_ilr_id ' ||
                  'and revision = :a_revision ' ||
                  'and set_of_books_id = :a_set_of_books_id ' ||
                  'and to_char(month,''yyyymm'') in ' || L_TERM_COMPONENT_PAY_MONTHS;

         execute immediate L_SQLS
         into L_TEMP_AMOUNT
         using L_ILR_ID, A_REVISION, A_SET_OF_BOOKS_ID;

       end if;

      --add the sum to the running total
      L_COMPONENT_AMOUNT := L_COMPONENT_AMOUNT + L_TEMP_AMOUNT;

    end loop;

    --all done, just return the calculated amount
    return L_COMPONENT_AMOUNT;

   exception
     when others then
       return 'Error! ' + sqlerrm;
   end F_CALC_HISTORIC;

   --Function to calculate the variable payments for an ILR in appropriate run order
   function F_CALC_ILR_VAR_PAYMENTS(A_ILR_ID     number,
                                    A_REVISION   number,
                                    A_MONTH      date) return varchar2 is

   L_SQLS                       varchar2(2000);
   L_BUCKET_NAME                varchar2(35);
   L_MSG                        varchar2(2000);
   L_STATUS                     varchar2(2000);
   L_VALUE                      number(22,8);
   l_paid_amount                NUMBER(22,8);


   CURSOR c_assets IS
          select la.ilr_id, la.revision, las.month, las.set_of_books_id, las.lsr_asset_id, ipt.payment_term_id, ptvp.receivable_type,
         ptvp.bucket_number, ptvp.run_order, lvp.asset_level_formula, ipt.payment_term_date
         from lsr_ilr_payment_term ipt, lsr_ilr_payment_term_var_pay ptvp, v_lsr_pseudo_asset_schedule las, lsr_asset la, lsr_variable_payment lvp
         WHERE la.ilr_id = A_ILR_ID
             AND la.revision = A_REVISION
             AND ipt.ilr_id = la.ilr_id
             AND la.lsr_asset_id = las.lsr_asset_id
             AND lower(ptvp.receivable_type) = 'contingent'
             AND ptvp.ilr_id = la.ilr_id
             AND ptvp.revision = la.revision
			 AND la.revision = las.revision
			 AND la.revision = ipt.revision
             AND ptvp.incl_in_initial_measure = 0
             AND lvp.variable_payment_id = ptvp.variable_payment_id
             AND las.month >= ipt.payment_term_date
             AND las.month < add_months(ipt.payment_term_date, decode(payment_freq_id, 4, 1, 3, 3, 2, 6, 1, 12) * number_of_terms)
             AND ptvp.payment_term_id IN (
                SELECT payment_term_id
                FROM lsr_ilr_payment_term b
                WHERE b.ilr_id = la.ilr_id
                AND b.revision = las.revision
                AND las.month >= payment_term_date
                AND las.month < add_months(payment_term_date, decode(payment_freq_id, 4, 1, 3, 3, 2, 6, 1, 12) * number_of_terms)
                )
             order by la.ilr_id, la.revision, ptvp.run_order, ptvp.bucket_number, las.lsr_asset_id, las.month, ipt.payment_term_id, las.set_of_books_id;

   BEGIN

     PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
     L_MSG := '  Beginning variable payment calculation';
     PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

--     --roll up to the schedule tables before beginning calculation
--     L_STATUS := PKG_LEASE_SCHEDULE.F_SAVE_SCHEDULES(A_MONTH);
--     if L_STATUS <> 'OK' then
--       PKG_PP_LOG.P_WRITE_MESSAGE('Saving schedule tables before VP calc failed: ' || L_STATUS);
--       L_STATUS := 'Saving schedule tables before VP calc failed:'  || L_STATUS;
--       return L_STATUS;
--     end if;

     l_msg := ' Clearing temporary Lessor Asset Schedule Table (LSR_ASSET_SCHEDULE_TMP)';

     DELETE FROM lsr_asset_schedule_tmp;
     
     L_MSG := ' Populating temporary Lessor Asset Schedule Table (LSR_ASSET_SCHEDULE_TMP)';

     INSERT INTO lsr_asset_schedule_tmp(lsr_asset_id,
                                        revision,
                                        set_of_books_id,
                                        month,
                                        interest_income_received,
                                        interest_income_accrued,
                                        interest_rental_recvd_spread,
                                        beg_deferred_rev,
                                        deferred_rev_activity,
                                        end_deferred_rev,
                                        beg_receivable,
                                        end_receivable,
                                        beg_lt_receivable,
                                        end_lt_receivable,
                                        initial_direct_cost,
                                        executory_accrual1,
                                        executory_accrual2,
                                        executory_accrual3,
                                        executory_accrual4,
                                        executory_accrual5,
                                        executory_accrual6,
                                        executory_accrual7,
                                        executory_accrual8,
                                        executory_accrual9,
                                        executory_accrual10,
                                        executory_paid1,
                                        executory_paid2,
                                        executory_paid3,
                                        executory_paid4,
                                        executory_paid5,
                                        executory_paid6,
                                        executory_paid7,
                                        executory_paid8,
                                        executory_paid9,
                                        executory_paid10,
                                        contingent_accrual1,
                                        contingent_accrual2,
                                        contingent_accrual3,
                                        contingent_accrual4,
                                        contingent_accrual5,
                                        contingent_accrual6,
                                        contingent_accrual7,
                                        contingent_accrual8,
                                        contingent_accrual9,
                                        contingent_accrual10,
                                        contingent_paid1,
                                        contingent_paid2,
                                        contingent_paid3,
                                        contingent_paid4,
                                        contingent_paid5,
                                        contingent_paid6,
                                        contingent_paid7,
                                        contingent_paid8,
                                        contingent_paid9,
                                        contingent_paid10)
     SELECT sch.lsr_asset_id,
            sch.revision,
            sch.set_of_books_id,
            sch.month,
            sch.interest_income_received,
            sch.interest_income_accrued,
            sch.interest_rental_recvd_spread,
            sch.beg_deferred_rev,
            sch.deferred_rev_activity,
            sch.end_deferred_rev,
            sch.beg_receivable,
            sch.end_receivable,
            sch.beg_lt_receivable,
            sch.end_lt_receivable,
            sch.initial_direct_cost,
            sch.executory_accrual1,
            sch.executory_accrual2,
            sch.executory_accrual3,
            sch.executory_accrual4,
            sch.executory_accrual5,
            sch.executory_accrual6,
            sch.executory_accrual7,
            sch.executory_accrual8,
            sch.executory_accrual9,
            sch.executory_accrual10,
            sch.executory_paid1,
            sch.executory_paid2,
            sch.executory_paid3,
            sch.executory_paid4,
            sch.executory_paid5,
            sch.executory_paid6,
            sch.executory_paid7,
            sch.executory_paid8,
            sch.executory_paid9,
            sch.executory_paid10,
            sch.contingent_accrual1,
            sch.contingent_accrual2,
            sch.contingent_accrual3,
            sch.contingent_accrual4,
            sch.contingent_accrual5,
            sch.contingent_accrual6,
            sch.contingent_accrual7,
            sch.contingent_accrual8,
            sch.contingent_accrual9,
            sch.contingent_accrual10,
            sch.contingent_paid1,
            sch.contingent_paid2,
            sch.contingent_paid3,
            sch.contingent_paid4,
            sch.contingent_paid5,
            sch.contingent_paid6,
            sch.contingent_paid7,
            sch.contingent_paid8,
            sch.contingent_paid9,
            sch.contingent_paid10
     FROM v_lsr_pseudo_asset_schedule sch
     WHERE lsr_asset_id IN (SELECT lsr_asset_id
                            FROM lsr_asset
                            WHERE ilr_id = a_ilr_id
                            and revision = a_revision)
     and revision = a_revision;

      delete from lsr_ilr_payment_month_tmp;

      INSERT INTO lsr_ilr_payment_month_tmp
      SELECT MONTH
      FROM TABLE(pkg_lessor_schedule.f_get_payment_info_from_terms(pkg_lessor_schedule.f_get_payment_terms(a_ilr_id, a_revision)))
      where mod(iter, payment_month_frequency) = 0;

     --loop over every row in the cursor
     for asset in c_assets
       loop
         --call the function to calc the formula
         --check if the current month is a payment month, if not then don't calc anything
         --formulas with components will handle this correctly on their own,
         -- but formulas with only amounts and/or schedule fields will not, so we handle them here
         if F_IS_PAYMENT_MONTH(A_ILR_ID, A_REVISION, asset.month) then
            L_VALUE := F_CALC_ASSET_BUCKET(asset.ilr_id, asset.lsr_asset_id, asset.revision, asset.set_of_books_id,
                                           asset.payment_term_id, asset.bucket_number, asset.month,'SCHEDULE') ;
         else
           L_VALUE := 0;
         end if;

         --update the bucket accrual column
         L_BUCKET_NAME := 'CONTINGENT_ACCRUAL' || to_char(asset.bucket_number);

         L_SQLS := 'UPDATE LSR_ASSET_SCHEDULE_TMP
                   SET ' || L_BUCKET_NAME || ' = :value
                    WHERE lsr_asset_id = :asset_id
                    AND revision = :revision
                    AND set_of_books_id = :set_of_books_id
                    AND month = :month';

          EXECUTE IMMEDIATE l_sqls
            USING L_VALUE, asset.LSR_ASSET_ID, asset.REVISION, asset.SET_OF_BOOKS_ID, to_char(asset.MONTH);

         --update the bucket paid column
         --  if asset-level formula, update no matter what
         L_BUCKET_NAME := 'CONTINGENT_PAID' || to_char(asset.bucket_number);
         if asset.asset_level_formula = 1 then

           L_SQLS := 'UPDATE LSR_ASSET_SCHEDULE_TMP
                     SET ' || L_BUCKET_NAME || ' = :value
                      WHERE lsr_asset_id = :asset_id
                      AND revision = :revision
                      AND set_of_books_id = :set_of_books_id
                      AND month = :month';

           EXECUTE IMMEDIATE l_sqls
            USING L_VALUE, asset.LSR_ASSET_ID, asset.REVISION, asset.SET_OF_BOOKS_ID, to_char(asset.MONTH);

         elsif asset.asset_level_formula = 0 then
           --  if ILR-level formula, calc the paid amount and update
           --  for formulas w/ index/rate components, paid amount will be 0 in non payment month, and full amount in payment month (matches accrual)
           --  for formulas w/ schedule fields, we need to use the function so that payment follows the payment term frequency

           L_PAID_AMOUNT := F_CALC_ASSET_PAID_AMOUNT(asset.lsr_asset_id, asset.revision, asset.month, asset.bucket_number, asset.set_of_books_id);

           L_SQLS := 'UPDATE LSR_ASSET_SCHEDULE_TMP
                     SET ' || L_BUCKET_NAME || ' = :value
                      WHERE lsr_asset_id = :asset_id
                      AND revision = :revision
                      AND set_of_books_id = :set_of_books_id
                      AND month = :month';

           EXECUTE IMMEDIATE l_sqls
            USING L_PAID_AMOUNT, asset.LSR_ASSET_ID, asset.REVISION, asset.SET_OF_BOOKS_ID, to_char(asset.MONTH);
         else
           return 'Formula has no value denoting asset-level or ILR-level';
         end if;

         --call the update function to roll up the staging tables to the live tables
         --this ensures the next calculation uses the most up to date numbers
         L_STATUS := F_UPDATE_SCHEDULE_TABLES(A_ILR_ID, asset.month, A_REVISION);
         --L_STATUS := PKG_LEASE_SCHEDULE.F_SAVE_SCHEDULES(A_MONTH);
         if L_STATUS <> 'OK' then
           PKG_PP_LOG.P_WRITE_MESSAGE('Updating schedule tables during VP calc failed: ' || L_STATUS);
           L_STATUS := 'Updating schedule tables during VP calc failed: ' || L_STATUS;
           return L_STATUS;
         end if;

      end loop;

      return 'OK';

  exception
    when zero_divide then
       L_MSG := 'Variable payment calculation error: divide by zero. Ensure formula components used in division are non-zero';
       return L_MSG;
    when others then
      l_msg := substr('ERROR calculating variable payments: ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_CALC_ILR_VAR_PAYMENTS;

   -- Function to calculate variable payments for all staged ILRs
   function F_CALC_ALL_VAR_PAYMENTS(A_MONTH date) return varchar2 is
     L_SQLS                       varchar2(2000);
     L_MSG                        varchar2(2000);
     L_STATUS                     varchar2(2000);
     L_DEL_STATUS                 varchar2(2000);

     cursor STAGED_ILRS is
          SELECT DISTINCT ilr_id, revision
          FROM lsr_ilr_schedule
          WHERE MONTH = a_month;

   begin
     PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
     PKG_PP_LOG.P_WRITE_MESSAGE('Starting var payment calc for all staged ILRs');


     for STAGED_ILR in STAGED_ILRS
       loop
         L_MSG := ' Calculating variable payments for ILR with ID: ' || to_char(STAGED_ILR.ILR_ID);

         L_STATUS := F_CALC_ILR_VAR_PAYMENTS(STAGED_ILR.ILR_ID, STAGED_ILR.REVISION, A_MONTH);

         IF L_STATUS <> 'OK' THEN
           L_MSG := 'Calculation of variable payments failed for ILR ID: ' || to_char(STAGED_ILR.ILR_ID) || ' with message : ' || L_STATUS;

--           --clear the staging tables since the schedule build will end now
--           L_STATUS := PKG_LEASE_SCHEDULE.F_CLEAR_STAGING_TABLES;
--           if L_STATUS <> 'OK' then
--             PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
--           end if;

           --return the constructed error message
           return L_MSG;
         END IF;

       end loop;


     return 'OK';

   exception
    when others then
      L_MSG := SUBSTR('ERROR calculating variable payments: ' || sqlerrm, 1, 2000);
         return L_MSG;

   end F_CALC_ALL_VAR_PAYMENTS;


   --**************************************************************************
   --  F_CALC_ASSET_BUCKET.
   -- This function calculates the variable payments for the passed in arguments.
   -- If the variable payment type is variable component, lsr_asset_schedule is used
   -- else lsr_ilr_schedule is used for calculations
   --**************************************************************************

   function F_CALC_ASSET_BUCKET( A_ILR_ID number, A_ASSET_ID number, A_REVISION number, A_SET_OF_BOOKS_ID number, A_PAYMENT_TERM_ID number, A_BUCKET_NUMBER number, A_MONTH date, A_PROCESS_TYPE varchar2) return number is
   L_MSG varchar2(100);
   l_formula_db varchar2(4000);
   l_asset_level_formula number(1,0);
   l_amount number(22,2);
   l_sqls varchar2(8000);
   l_asset_fmv number; l_ilr_current_cost number; l_asset_npv number;

      begin
        SELECT lvp.payment_formula_db, asset_level_formula INTO l_formula_db, l_asset_level_formula
       from lsr_variable_payment lvp, lsr_ilr_payment_term_var_pay ptvp
       where ptvp.ilr_id = A_ILR_ID
          and ptvp.revision = A_REVISION
          AND ptvp.payment_term_id = a_payment_term_id
          and lower(ptvp.receivable_type) = 'contingent'
          and ptvp.bucket_number = A_BUCKET_NUMBER
          and ptvp.variable_payment_id = lvp.variable_payment_id;


     begin
       if l_asset_level_formula = 1 then
         --calculate the formula for the assets
         l_sqls := 'SELECT nvl(' || l_formula_db || ', 0)' ;
         --Note there's no need for a temp table on the sales/direct schedule table, as none of the fields are manipulated by var payments
         IF a_process_type = 'MONTH_END' THEN
           l_sqls := l_sqls || ' FROM LSR_ASSET_SCHEDULE_TMP a, V_LSR_PSEUDO_ASSET_SCH_SALES sales, LSR_ILR_OPTIONS LIO, LSR_ILR_AMOUNTS calc, (select 0 as initial_amt_flag from dual) b';
         ELSE
           L_SQLS := L_SQLS || ' FROM LSR_ASSET_SCHEDULE_TMP a, V_LSR_PSEUDO_ASSET_SCH_SALES sales, LSR_ILR_OPTIONS LIO, LSR_ILR_AMOUNTS calc, (select 0 as initial_amt_flag from dual) b';

         end if;

         L_SQLS := L_SQLS || '  WHERE a.LSR_ASSET_ID = :A_ASSET_ID
                       AND a.revision = :A_REVISION
                       AND a.MONTH = :A_MONTH
                       and a.SET_OF_BOOKS_ID = :A_SET_OF_BOOKS_ID
                       AND LIO.ILR_ID = :A_ILR_ID
                       AND LIO.REVISION = A.REVISION
                       AND a.LSR_ASSET_ID = sales.LSR_ASSET_ID (+)
                       AND a.REVISION = sales.REVISION (+)
                       AND a.SET_OF_BOOKS_ID = sales.SET_OF_BOOKS_ID (+)
                       AND a.MONTH = sales.MONTH (+)
                       AND LIO.ILR_ID = calc.ILR_ID
                       AND a.REVISION = calc.REVISION
                       AND a.SET_OF_BOOKS_ID = calc.SET_OF_BOOKS_ID';

            EXECUTE IMMEDIATE L_SQLS
            INTO L_AMOUNT
            USING A_ASSET_ID, A_REVISION, A_MONTH, A_SET_OF_BOOKS_ID, A_ILR_ID;

       else
         ---calculate the formula for the asset by first calculating the npv ratio and then
         -- evaluate the formula at ILR level and then apply tha to the variable payment
        SELECT fair_market_value,
                current_lease_cost,
                (fair_market_value/current_lease_cost)
        INTO  l_asset_fmv,
              l_ilr_current_cost,
              l_asset_npv
        FROM (SELECT  ilr_id,
                      revision,
                      lsr_asset_id,
                      fair_market_value,
                      SUM(fair_market_value) OVER (PARTITION BY ilr_id, revision) AS current_lease_cost
              FROM lsr_asset)
        WHERE ilr_id = a_ilr_id
        AND revision = a_revision
        AND lsr_asset_id = a_asset_id;


        l_sqls := 'SELECT ' || to_char(l_asset_npv) || '* (nvl(' || l_formula_db || ',0))';
        IF a_process_type = 'MONTH_END' THEN
           L_SQLS := L_SQLS || ' FROM LSR_ILR_SCHEDULE A, LSR_ILR_SCHEDULE_SALES_DIRECT sales, LSR_ILR_AMOUNTS calc, LSR_ILR_OPTIONS LIO, (select 0 as initial_amt_flag from dual) b';
         ELSE
           L_SQLS := L_SQLS || ' FROM LSR_ILR_SCHEDULE A, LSR_ILR_SCHEDULE_SALES_DIRECT sales, LSR_ILR_AMOUNTS calc, LSR_ILR_OPTIONS LIO, (select 0 as initial_amt_flag from dual) b';

         end if;


        L_SQLS := L_SQLS ||  ' WHERE LIO.ILR_ID = :A_ILR_ID
                       AND A.revision = :A_REVISION
                       AND A.MONTH = :A_MONTH
                       and A.SET_OF_BOOKS_ID = :A_SET_OF_BOOKS_ID
                       AND A.ILR_ID = LIO.ILR_ID
                       AND A.REVISION = LIO.REVISION
                       AND A.ILR_ID = sales.ILR_ID (+)
                       AND A.REVISION = sales.REVISION (+)
                       AND A.SET_OF_BOOKS_ID = sales.SET_OF_BOOKS_ID (+)
                       AND A.MONTH = SALES.MONTH(+)
                       AND A.ILR_ID = CALC.ILR_ID
                       AND A.REVISION = CALC.REVISION
                       AND A.SET_OF_BOOKS_ID = CALC.SET_OF_BOOKS_ID';

         EXECUTE IMMEDIATE L_SQLS
         INTO L_AMOUNT
         USING A_ILR_ID, A_REVISION, A_MONTH, A_SET_OF_BOOKS_ID;

       end if;


        exception when no_data_found then
         RETURN 0 ;
         --REMOVE THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         --WHEN zero_divide then return 0;

       end;


       return L_AMOUNT;

     end F_CALC_ASSET_BUCKET;


   --**************************************************************************
   --  F_CALC_ASSET_BUCKETS_MONTH_END.
   -- This function calculates accrual and paid contingent buckets for all assets
   -- for a given company id. The buckets are then rolled up to the ILRs
   --**************************************************************************

   function F_CALC_ASSET_BUCKETS_MONTH_END(A_COMPANY_ID in number, A_MONTH in date, CALC_TYPE varchar2) return varchar2 is
       CURSOR c_assets IS
         select lsr_ilr.ilr_id, lsr_asset.lsr_asset_id, lsr_ilr.current_revision, csob.set_of_books_id, ipt.payment_term_id, ptvp.receivable_type,
         ptvp.bucket_number,ptvp.run_order, ptvp.incl_in_initial_measure, ptvp.initial_var_payment_value, ipt.payment_term_date
         FROM lsr_ilr, company_set_of_books csob, lsr_asset, lsr_ilr_payment_term ipt, lsr_ilr_payment_term_var_pay ptvp,
		      lsr_ilr_approval lia
         WHERE lsr_ilr.company_id = a_company_id
             AND lsr_ilr.ilr_id = lsr_asset.ilr_id
             AND lsr_ilr.current_revision = lsr_asset.revision
             AND csob.company_id = lsr_ilr.company_id
             AND lsr_asset.revision = lsr_ilr.current_revision
             AND ipt.ilr_id = lsr_ilr.ilr_id
             AND ipt.revision = lsr_ilr.current_revision
             AND ptvp.ilr_id = lsr_ilr.ilr_id
             AND ptvp.revision = lsr_ilr.current_revision
             AND lsr_ilr.ilr_id = lia.ilr_id
             AND lsr_ilr.current_revision = lia.revision
             AND lia.approval_status_id in (3,6)
             AND lower(ptvp.receivable_type) = 'contingent'
             AND ptvp.variable_payment_id IS NOT NULL
             AND A_MONTH >= ipt.payment_term_date
             AND A_MONTH < add_months(ipt.payment_term_date, decode(payment_freq_id, 4, 1, 3, 3, 2, 6, 1, 12) * number_of_terms)
             AND ptvp.payment_term_id IN (
                SELECT payment_term_id
                FROM lsr_ilr_payment_term b
                WHERE b.ilr_id = lsr_ilr.ilr_id
                AND b.revision = lsr_ilr.current_revision
                AND A_MONTH >= payment_term_date
                AND A_MONTH < add_months(payment_term_date, decode(payment_freq_id, 4, 1, 3, 3, 2, 6, 1, 12) * number_of_terms)
                )
             order by ilr_id, lsr_asset_id, current_revision, payment_term_id, bucket_number, run_order;


       L_MSG varchar2(100);
       l_amount number(22,2); L_TEMP_AMOUNT  number(22,2); l_asset_npv NUMBER(22,2);
       l_sqls varchar2(4000); l_temp_col varchar2(100);

       begin
         PKG_PP_LOG.P_WRITE_MESSAGE(' Calculating Variable Payments for assets.');
         for asset in c_assets
         LOOP
		 	--pkg_pp_log.p_write_message(' asset.ilr_id: ' || asset.ilr_id || ' asset.asset_id: ' || asset.lsr_asset_id || ' asset.current_revision: ' || asset.current_revision || ' asset.set_of_books_id: ' || asset.set_of_books_id || ' asset.payment_term_id: ' || asset.payment_term_id || ' asset.bucket_number: ' || asset.bucket_number || ' asset.run_order: ' || asset.run_order);
		    -- If running accruals and payments more than once, reset the buckets to their initial values
		    l_sqls := 'update lsr_asset_schedule_tmp ';
           if lower(calc_type) = 'accruals' then
             l_temp_col:= 'contingent_accrual';
           else
             l_temp_col:= 'contingent_paid';
           end if;

           l_temp_col := l_temp_col || to_char(asset.bucket_number);

           l_sqls:= l_sqls || ' set ' || l_temp_col || ' = ' || to_char(nvl(asset.initial_var_payment_value,0)) ;
           l_sqls:= l_sqls || ' where lsr_asset_id = ' || to_char(asset.lsr_asset_id) ;
           l_sqls:= l_sqls || ' and revision = ' || to_char(asset.current_revision);
           l_sqls:= l_sqls || ' and set_of_books_id = ' || to_char(asset.set_of_books_id);
           l_sqls:= l_sqls || ' and month = ''' || to_char(A_MONTH, 'DD-MON-YYYY') || '''';

           begin
              execute immediate l_sqls;
           exception
              when no_data_found then
              CONTINUE;

           end;


            l_temp_amount := F_CALC_ASSET_BUCKET(asset.ilr_id, asset.lsr_asset_id, asset.current_revision, asset.set_of_books_id, asset.payment_term_id, asset.bucket_number, A_MONTH, 'MONTH_END' );

		   --calculate gain loss if this was included in the initial measure. The initial value for the ILR must be broken based on the asset NPV

           if asset.incl_in_initial_measure = 1 then

            select nvl(la.fair_market_value,1)/nvl(la.current_lease_cost,1)
            INTO l_asset_npv
            from (select  ilr_id,
                          revision,
                          lsr_asset_id,
                          fair_market_value,
                          SUM(fair_market_value) OVER (PARTITION BY ilr_id, revision) AS current_lease_cost
                  FROM lsr_asset la) la
            JOIN lsr_ilr ilr ON la.ilr_id = ilr.ilr_id AND la.revision = ilr.current_revision
            JOIN company_set_of_books csob on ilr.company_id = csob.company_id
            where la.ilr_id = asset.ilr_id
            AND la.revision = asset.current_revision
            AND csob.set_of_books_id = asset.set_of_books_id
            and la.lsr_asset_id = asset.lsr_asset_id;


			  l_amount := l_temp_amount - (asset.initial_var_payment_value * l_asset_npv);
           else
              l_amount :=  l_temp_amount;

           end if;


           l_sqls := 'update lsr_asset_schedule_tmp ';
           if lower(calc_type) = 'accruals' then
             l_temp_col:= 'contingent_accrual';
           else
             l_temp_col:= 'contingent_paid';

           end if;


           l_temp_col := l_temp_col || to_char(asset.bucket_number);

           --l_sqls:= 'set ' || l_temp_col || ' = ' || l_temp_col || ' + ' ||  to_char(l_amount) ;
           l_sqls:= l_sqls || ' set ' || l_temp_col || ' = ' || to_char(l_amount) ;
           l_sqls:= l_sqls || ' where lsr_asset_id = ' || to_char(asset.lsr_asset_id) ;
           l_sqls:= l_sqls || ' and revision = ' || to_char(asset.current_revision);
           l_sqls:= l_sqls || ' and set_of_books_id = ' || to_char(asset.set_of_books_id);
           l_sqls:= l_sqls || ' and month = ''' || to_char(A_MONTH, 'DD-MON-YYYY') || '''';

           begin
              execute immediate l_sqls;
           exception
              when no_data_found then
              CONTINUE;

           end;


         END LOOP;


         -- Roll up the asset buckets to ILR level
         L_MSG := F_ROLLUP_ASSETS_BUCKETS_TO_ILR(A_COMPANY_ID, A_MONTH, CALC_TYPE);
         if L_MSG <> 'OK' then
           return L_MSG;
         end if;


       return 'OK';
     exception
        when others then
         L_MSG := SUBSTR(sqlerrm, 1, 2000);
		 return L_MSG;

   end F_CALC_ASSET_BUCKETS_MONTH_END;


   --**************************************************************************
   --  F_ROLLUP_ASSETS_BUCKETS_TO_ILR.
   -- This function rolls up all bucket values from the assets to the ILR level.
   --
   --**************************************************************************

   function F_ROLLUP_ASSETS_BUCKETS_TO_ILR(A_COMPANY_ID in number, A_MONTH in date, A_CALC_TYPE varchar2) return varchar2 is
     begin
       IF LOWER(a_calc_type) = 'accruals' THEN
         update lsr_ilr_schedule LIS set (contingent_accrual1, contingent_accrual2,contingent_accrual3,contingent_accrual4,contingent_accrual5, contingent_accrual6, contingent_accrual7,contingent_accrual8,contingent_accrual9,contingent_accrual10)=
           (
             select contingent_accrual1, contingent_accrual2,contingent_accrual3,contingent_accrual4,contingent_accrual5, contingent_accrual6, contingent_accrual7, contingent_accrual8, contingent_accrual9, contingent_accrual10
             from
                 (
                   select la.ilr_id, la.revision, las.set_of_books_id, las.month, sum(contingent_accrual1) contingent_accrual1, sum(contingent_accrual2) contingent_accrual2, sum(contingent_accrual3) contingent_accrual3, sum(contingent_accrual4) contingent_accrual4,
                          SUM(contingent_accrual5) contingent_accrual5, SUM(contingent_accrual6) contingent_accrual6, SUM(contingent_accrual7) contingent_accrual7, SUM(contingent_accrual8) contingent_accrual8, SUM(contingent_accrual9) contingent_accrual9, SUM(contingent_accrual10) contingent_accrual10
                   from lsr_asset_schedule_tmp las, lsr_asset la, lsr_ilr ilr
                   WHERE ilr.ilr_id = la.ilr_id
                     AND  ilr.current_revision = la.revision
                     AND las.revision = ilr.current_revision
                     and la.lsr_asset_id = las.lsr_asset_id
                     and ilr.company_id = A_COMPANY_ID
                     AND las.MONTH = a_month
                   group by la.ilr_id, la.revision, las.set_of_books_id, las.month
                   order by 1, 2, 3, 4
                 ) DS
             where LIS.ilr_id = DS.ilr_id and LIS.revision = DS.revision and LIS.set_of_books_id = DS.set_of_books_id and LIS.month = DS.month
           )
         where (ilr_id, revision) in (select ilr_id, current_revision from lsr_ilr where company_id = A_COMPANY_ID )
         and month = A_MONTH
		 AND EXISTS (
		 	SELECT 1
			FROM lsr_asset_schedule_tmp las
			INNER JOIN lsr_asset la
				ON la.lsr_asset_id = las.lsr_asset_id
				AND la.revision = las.revision
			INNER JOIN lsr_ilr ilr
				ON ilr.ilr_id = la.ilr_id
				AND ilr.current_revision = la.revision
			WHERE lis.ilr_id = ilr.ilr_id
			AND lis.revision = ilr.current_revision
			AND lis.set_of_books_id = las.set_of_books_id
			AND lis.month = las.month
			AND ilr.company_id = A_COMPANY_ID
			AND las.MONTH = A_MONTH
		);

       ELSE
                  update lsr_ilr_schedule LIS set (contingent_paid1, contingent_paid2,contingent_paid3,contingent_paid4,contingent_paid5, contingent_paid6, contingent_paid7,contingent_paid8,contingent_paid9,contingent_paid10)=
           (
             select contingent_paid1, contingent_paid2,contingent_paid3,contingent_paid4,contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8, contingent_paid9, contingent_paid10
             from
                 (
                   select la.ilr_id, la.revision, las.set_of_books_id, las.month, sum(contingent_paid1) contingent_paid1, sum(contingent_paid2) contingent_paid2, sum(contingent_paid3) contingent_paid3, sum(contingent_paid4) contingent_paid4,
                          SUM(contingent_paid5) contingent_paid5, SUM(contingent_paid6) contingent_paid6, SUM(contingent_paid7) contingent_paid7, SUM(contingent_paid8) contingent_paid8, SUM(contingent_paid9) contingent_paid9, SUM(contingent_paid10) contingent_paid10
                   from lsr_asset_schedule_tmp las, lsr_asset la, lsr_ilr ilr
                   WHERE ilr.ilr_id = la.ilr_id
                     AND  ilr.current_revision = la.revision
                     AND las.revision = ilr.current_revision
                     AND la.lsr_asset_id = las.lsr_asset_id
                     and la.revision = las.revision
                     and ilr.company_id = A_COMPANY_ID
                     AND las.MONTH = a_month
                   group by la.ilr_id, la.revision, las.set_of_books_id, las.month
                   order by 1, 2, 3, 4
                 ) DS
             where LIS.ilr_id = DS.ilr_id and LIS.revision = DS.revision and LIS.set_of_books_id = DS.set_of_books_id and LIS.month = DS.month
           )
         where (ilr_id, revision) in (select ilr_id, current_revision from lsr_ilr where company_id = A_COMPANY_ID )
         and month = A_MONTH
		 AND EXISTS (
		 	SELECT 1
			FROM lsr_asset_schedule_tmp las
			INNER JOIN lsr_asset la
				ON la.lsr_asset_id = las.lsr_asset_id
				AND la.revision = las.revision
			INNER JOIN lsr_ilr ilr
				ON ilr.ilr_id = la.ilr_id
				AND ilr.current_revision = la.revision
			WHERE lis.ilr_id = ilr.ilr_id
			AND lis.revision = ilr.current_revision
			AND lis.set_of_books_id = las.set_of_books_id
			AND lis.month = las.month
			AND ilr.company_id = A_COMPANY_ID
			AND las.MONTH = A_MONTH
		);

       end if;

       return 'OK';
     exception when no_data_found then
       return 'OK';
       when others then
         return sqlerrm;

     end F_ROLLUP_ASSETS_BUCKETS_TO_ILR;


     function F_CALC_ASSET_PAID_AMOUNT(A_ASSET_ID       number,
                                       A_REVISION       number,
                                       A_MONTH          date,
                                       A_BUCKET_NUMBER  number,
                                       A_SET_OF_BOOKS_ID number) return number is

      L_SQLS                         varchar2(2000);
      L_BUCKET_NAME                  varchar2(35);
      l_amount                       NUMBER(22,8);
      L_ILR_ID                       number(22,0);

      begin
        --PKG_PP_LOG.P_WRITE_MESSAGE('   Starting calc for paid amount');

        L_BUCKET_NAME := 'CONTINGENT_ACCRUAL' || to_char(A_BUCKET_NUMBER);

        l_sqls := 'select amt from (
                    SELECT asset_id, revision, set_of_books_id, MONTH, payment_month, accrual, CASE WHEN MONTH = payment_month THEN Sum(accrual) OVER (PARTITION BY asset_id, revision, set_of_books_id, payment_month) ELSE 0 END as amt
                    FROM (
                        SELECT a.lsr_asset_id AS asset_id, a.revision AS revision, a.set_of_books_id AS set_of_books_id,
                            a.MONTH AS month, Min(payment_month.payment_month) AS payment_month, a.' || L_BUCKET_NAME || ' AS accrual
                        FROM lsr_asset_schedule_tmp a
                        INNER JOIN lsr_ilr_payment_month_tmp payment_month
                        ON payment_month.payment_month >= A.MONTH
                        WHERE a.lsr_asset_id = :asset_id
                        AND a.revision = :revision
                        AND a.set_of_books_id = :set_of_books_id
                        GROUP BY a.lsr_asset_id, a.revision, a.set_of_books_id, a.MONTH, a.' || L_BUCKET_NAME || '
                    )
                    ORDER BY month
                  )
                  where month = :month';

      EXECUTE IMMEDIATE L_SQLS
        INTO l_amount
        USING A_ASSET_ID, A_REVISION, A_SET_OF_BOOKS_ID, A_MONTH;

      return L_AMOUNT;

      exception
        when no_data_found then
        return 0;
        when others then
         return sqlerrm;

      end F_CALC_ASSET_PAID_AMOUNT;


   function F_UPDATE_SCHEDULE_TABLES(A_ILR_ID            number,
                                     A_MONTH             date,
                                     A_REVISION          number) return varchar2
   is
     L_STATUS                        varchar2(2000);

   BEGIN

     --update the ILR schedule table using the asset schedule
     update lsr_ilr_schedule LIS set (contingent_accrual1, contingent_accrual2,contingent_accrual3,contingent_accrual4,contingent_accrual5, contingent_accrual6, contingent_accrual7,contingent_accrual8,contingent_accrual9,contingent_accrual10,
                                         contingent_paid1, contingent_paid2,contingent_paid3,contingent_paid4,contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8, contingent_paid9, contingent_paid10)=
           (
             select coalesce(contingent_accrual1, 0) as contingent_accrual1,
                    coalesce(contingent_accrual2, 0) as contingent_accrual2,
                    coalesce(contingent_accrual3, 0) as contingent_accrual3,
                    coalesce(contingent_accrual4, 0) as contingent_accrual4,
                    coalesce(contingent_accrual5, 0) as contingent_accrual5,
                    coalesce(contingent_accrual6, 0) as contingent_accrual6,
                    coalesce(contingent_accrual7, 0) as contingent_accrual7,
                    coalesce(contingent_accrual8, 0) as contingent_accrual8,
                    coalesce(contingent_accrual9, 0) as contingent_accrual9,
                    coalesce(contingent_accrual10, 0) as contingent_accrual10,
                    coalesce(contingent_paid1, 0) as contingent_paid1,
                    coalesce(contingent_paid2, 0) as contingent_paid2,
                    coalesce(contingent_paid3, 0) as contingent_paid3,
                    coalesce(contingent_paid4, 0) as contingent_paid4,
                    coalesce(contingent_paid5, 0) as contingent_paid5,
                    coalesce(contingent_paid6, 0) as contingent_paid6,
                    coalesce(contingent_paid7, 0) as contingent_paid7,
                    COALESCE(contingent_paid8, 0) AS contingent_paid8,
                    coalesce(contingent_paid9, 0) as contingent_paid9,
                    coalesce(contingent_paid10, 0) as contingent_paid10
             from
                 (
                   select la.ilr_id, la.revision, las.set_of_books_id, las.month, sum(contingent_accrual1) contingent_accrual1, sum(contingent_accrual2) contingent_accrual2, sum(contingent_accrual3) contingent_accrual3, sum(contingent_accrual4) contingent_accrual4,
                          sum(contingent_accrual5) contingent_accrual5, sum(contingent_accrual6) contingent_accrual6, sum(contingent_accrual7) contingent_accrual7, sum(contingent_accrual8) contingent_accrual8, sum(contingent_accrual9) contingent_accrual9, sum(contingent_accrual10) contingent_accrual10,
                          sum(contingent_paid1) contingent_paid1, sum(contingent_paid2) contingent_paid2, sum(contingent_paid3) contingent_paid3, sum(contingent_paid4) contingent_paid4,
                          SUM(contingent_paid5) contingent_paid5, SUM(contingent_paid6) contingent_paid6, SUM(contingent_paid7) contingent_paid7, SUM(contingent_paid8) contingent_paid8, SUM(contingent_paid9) contingent_paid9, SUM(contingent_paid10) contingent_paid10
                   from lsr_asset_schedule_tmp las, lsr_asset la
                   WHERE las.revision = la.revision
                     and la.lsr_asset_id = las.lsr_asset_id
                     and la.ilr_id = A_ILR_ID
                     AND las.MONTH = a_month
                   group by la.ilr_id, la.revision, las.set_of_books_id, las.month
                   order by 1, 2, 3, 4
                 ) DS
             where LIS.ilr_id = DS.ilr_id and LIS.revision = DS.revision and LIS.set_of_books_id = DS.set_of_books_id and LIS.month = DS.month
           )
         where ilr_id = A_ILR_ID
         AND revision = a_revision
         AND MONTH = a_month;


         return 'OK';
      exception
        when others then
         return sqlerrm;
      end F_UPDATE_SCHEDULE_TABLES;

	function F_IS_PAYMENT_MONTH_RTN_NUMBER( A_ILR_ID     number,
                                          A_REVISION   number,
                                          A_MONTH      date) return integer
	is
    cnt number;
	begin
    
    SELECT COUNT(1) into cnt
    FROM TABLE(pkg_lessor_schedule.f_get_payment_info_from_terms(pkg_lessor_schedule.f_get_payment_terms(a_ilr_id, a_revision)))
    WHERE MOD(iter, payment_month_frequency) = 0
    and month = a_month;
  
    RETURN cnt;
    
	end F_IS_PAYMENT_MONTH_RTN_NUMBER;


  FUNCTION f_is_payment_month(a_ilr_id     NUMBER,
                              A_REVISION   number,
                              A_MONTH      date) return boolean
  IS
  BEGIN
    IF f_is_payment_month_rtn_number(a_ilr_id, a_revision, a_month) = 0
      THEN RETURN FALSE;
      ELSE RETURN TRUE; 
    END IF; 

  END f_is_payment_month;

     function F_GET_FIRST_PAYMENT_TERM_MONTH( A_ILR_ID     number,
                                              A_REVISION   number) return date is
     L_FIRST_PAYMENT_TERM_MONTH     date;

     begin
      
      SELECT min(payment_term_date) INTO l_first_payment_term_month
      FROM lsr_ilr_payment_term
      WHERE ilr_id = a_ilr_id
      AND revision = a_revision;

      return L_FIRST_PAYMENT_TERM_MONTH;

     end F_GET_FIRST_PAYMENT_TERM_MONTH;

END pkg_lessor_var_payments;
/

--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (4068, 0, 2017, 1, 0, 0, 0, 'C:\plasticwks\powerplant\sql\packages', '2017.1.0.0_pkg_lessor_var_payments.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 