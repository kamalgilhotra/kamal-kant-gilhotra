/*
||=================================================================================================
|| Application: PowerPlant
|| Object Name: PKG_LESSOR_COMMON
|| Description:
||=================================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||=================================================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- --------------    ----------------------------------------------------------
|| 2017.1.0.0 11/01/2017 C.Shilling		  Create package
||=================================================================================================
*/
CREATE OR REPLACE PACKAGE pkg_lessor_common AS
	-- function the return the process id for lessor calcs
	FUNCTION f_get_process_id(a_description pp_processes.description%TYPE)
		RETURN pp_processes.process_id%TYPE;


	TYPE ilr_schedule_line IS RECORD (
		ilr_id						lsr_ilr_schedule_sales_direct.ilr_id%TYPE,
		set_of_books_id				lsr_ilr_schedule_sales_direct.set_of_books_id%TYPE,
		rownumber					INTEGER,
		company_id					lsr_ilr.company_id%type,
		month 						lsr_ilr_schedule.month%type,
		principal_received			lsr_ilr_schedule_sales_direct.principal_received%TYPE,
		interest_income	lsr_ilr_schedule.interest_income_received%TYPE,
		executory1 			lsr_ilr_schedule.executory_paid1%TYPE,
		executory2 			lsr_ilr_schedule.executory_paid2%TYPE,
		executory3 			lsr_ilr_schedule.executory_paid3%TYPE,
		executory4 			lsr_ilr_schedule.executory_paid4%TYPE,
		executory5 			lsr_ilr_schedule.executory_paid5%TYPE,
		executory6 			lsr_ilr_schedule.executory_paid6%TYPE,
		executory7 			lsr_ilr_schedule.executory_paid7%TYPE,
		executory8 			lsr_ilr_schedule.executory_paid8%TYPE,
		executory9 			lsr_ilr_schedule.executory_paid9%TYPE,
		executory10			lsr_ilr_schedule.executory_paid10%TYPE,
		contingent1			lsr_ilr_schedule.contingent_paid1%TYPE,
		contingent2			lsr_ilr_schedule.contingent_paid2%TYPE,
		contingent3			lsr_ilr_schedule.contingent_paid3%TYPE,
		contingent4			lsr_ilr_schedule.contingent_paid4%TYPE,
		contingent5			lsr_ilr_schedule.contingent_paid5%TYPE,
		contingent6			lsr_ilr_schedule.contingent_paid6%TYPE,
		contingent7			lsr_ilr_schedule.contingent_paid7%TYPE,
		contingent8			lsr_ilr_schedule.contingent_paid8%TYPE,
		contingent9			lsr_ilr_schedule.contingent_paid9%TYPE,
		contingent10		lsr_ilr_schedule.contingent_paid10%TYPE
	);
	TYPE ilr_schedule_line_table IS TABLE OF ilr_schedule_line INDEX BY PLS_INTEGER;
END pkg_lessor_common;
/

CREATE OR REPLACE PACKAGE BODY pkg_lessor_common AS
	--**************************************************************
	--         functions
	--**************************************************************
	-- returns the process id for lease calc
	FUNCTION f_get_process_id(a_description pp_processes.description%TYPE)
		RETURN pp_processes.process_id%TYPE
	IS
		l_process_id pp_processes.process_id%TYPE;
	BEGIN
		SELECT process_id
		INTO l_process_id
		FROM pp_processes
		WHERE description = a_description;

		RETURN l_process_id;
	EXCEPTION
		WHEN No_Data_Found THEN
			Raise_Application_Error(-20000, 'The description passed to PKG_LESSOR_COMMON.F_GET_PROCESS_ID (''' || a_description || ''') was not found.');
	END f_get_process_id;
END pkg_lessor_common;
/
--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (3957, 0, 2017, 1, 0, 0, 0, 'C:\plasticwks\powerplant\sql\packages', '2017.1.0.0_PKG_LESSOR_COMMON.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 