/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_037099_sys_PKG_PP_ERROR.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/07/2014 Kyle Peterson  Original Version
||============================================================================
*/

create or replace package PKG_PP_ERROR is
    G_PKG_VERSION varchar(35) := '2018.1.0.0';
	
	procedure SET_MODULE_NAME(MODULE_NAME_IN in varchar2);

	-- Add the module name to the call stack, so that we know where
	-- any errors occurred.
	procedure SET_ERR_MSG(MSG_ID_IN in varchar2);

	-- As modules finish correctly the module name is dropped from
	-- the stack. When the last module completes correctly the stack is
	-- discarded.
	procedure REMOVE_MODULE_NAME;

	-- Called when we want to raise a specific error from the message table
	procedure RAISE_ERROR(ERROR_TYPE in varchar2, COMMENTS_IN in varchar2 default null);

	-- future enhancement
	procedure ADD_CONTEXT(CONTEXT_DOMAIN_IN varchar2 default null,
						  CONTEXT_ITEM_IN   varchar2,
						  CONTEXT_VALUE_IN  varchar2);
						  
	-- Turn on/off the "Added to Stack" and "Removed from Stack" logging					  
	procedure LOG_ALL(A_BOOL boolean);
	
end; -- error_pkg
/

create or replace package body PKG_PP_ERROR is

	type STACK is table of varchar2(100) index by binary_integer;

	type ERROR_CONTEXT_RECTYPE is record(
		
		CONTEXT_DOMAIN varchar2(30) := null,
		CONTEXT_ITEM   varchar2(25) := null,
		CONTEXT_VALUE  varchar2(250) := null);

	type ERROR_CONTEXT_TYPE is table of ERROR_CONTEXT_RECTYPE index by binary_integer;

	------------------------------------------------------------
	-- Private Package variables
	------------------------------------------------------------
	ERROR_CONTEXT       ERROR_CONTEXT_TYPE;
	EMPTY_ERROR_CONTEXT ERROR_CONTEXT_TYPE; -- to clear error_context
	MAX_CONTEXT_ID      pls_integer := 0; -- index for error_context
	CALL_STACK          STACK; -- Stack of calling modules
	EMPTY_STACK         STACK; -- to clear call_stack
	TOP_OF_STACK        pls_integer := 0; -- call_stack index
	STACK_IS_EMPTY      boolean := true;
	B_LOG_ALL				boolean := false;
	CALL_STACK_TEXT     varchar2(2000) := null;
	-- text representation of call_stack, created on first call to raise_error
	ERR_MSG_ID   varchar2(100) := null;
	ERR_MSG_TYPE varchar2(3) := null; 
	ERR_COMMENTS varchar2(2000) := null; -- passed in by raise_error
	G_LINE_NUMBER varchar2(100) := null; --Holds the original erroring line

	-----------------------------------------------------------
	--                  Private Interface                    --
	-----------------------------------------------------------

	--------------------------------------------------------------------------
	-- Clear the stack and release the memory associated with it.
	-- This is done when either
	-- a) the whole sequence of calls completed successfully
	-- b) an exception has been raised, and we have already dumped out the call stack
	procedure CLEAR_STACK is
	begin
		CALL_STACK     := EMPTY_STACK;
		TOP_OF_STACK   := 0;
		STACK_IS_EMPTY := true;
	end;

	--------------------------------------------------------------
	-- Future enhancement
	procedure CLEAR_CONTEXT is
	begin
		ERROR_CONTEXT  := EMPTY_ERROR_CONTEXT;
		MAX_CONTEXT_ID := 0;
	end;

	--------------------------------------------------------------
	-- Called by raise_error to create a log file describing the error
	procedure LOG_ERROR is
		LDOMAIN varchar2(25);
	begin
		CLEAR_CONTEXT;
		ADD_CONTEXT('ERROR', 'Error Log Time', TO_CHAR(sysdate, 'HH24:MI:SS'));
		ADD_CONTEXT('ERROR', 'Call Stack', CALL_STACK_TEXT);
		ADD_CONTEXT('ERROR', 'Comments', ERR_COMMENTS);
		ADD_CONTEXT('ERROR', 'Oracle Error', sqlerrm);
		
		PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: '||CALL_STACK_TEXT||' -- '||ERR_COMMENTS);

	exception
		when others then
			PKG_PP_LOG.P_WRITE_MESSAGE('Problem in error_pkg.log_error: ' || sqlerrm);
	end;

	-----------------------------------------------------------
	--                  Public  Interface                    --
	-----------------------------------------------------------

	--------------------------------------------------------------
	-- Future enhancement
	procedure ADD_CONTEXT(CONTEXT_DOMAIN_IN varchar2 default null,
						  CONTEXT_ITEM_IN   varchar2,
						  CONTEXT_VALUE_IN  varchar2) is
	begin
		ERROR_CONTEXT(MAX_CONTEXT_ID).CONTEXT_DOMAIN := SUBSTR(CONTEXT_DOMAIN_IN, 1, 30);
		ERROR_CONTEXT(MAX_CONTEXT_ID).CONTEXT_ITEM := SUBSTR(CONTEXT_ITEM_IN, 1, 25);
		ERROR_CONTEXT(MAX_CONTEXT_ID).CONTEXT_VALUE := SUBSTR(NVL(CONTEXT_VALUE_IN, 'NULL'), 1, 250);
		MAX_CONTEXT_ID := MAX_CONTEXT_ID + 1;
	end;
	
	--------------------------------------------------------------
	-- Turn on/off detailed logging
	procedure LOG_ALL(A_BOOL boolean) is
	begin
		B_LOG_ALL := A_BOOL;
	end;

	--------------------------------------------------------------
	-- Set the active error message for retrieval by the calling procedure
	procedure SET_ERR_MSG(MSG_ID_IN in varchar2) is
	begin
		-- validate the format of the message number
		if UPPER(SUBSTR(MSG_ID_IN, 1, 3)) not in ('DAT', 'APP') or
		   TO_NUMBER(SUBSTR(MSG_ID_IN, 4, 3)) not between 0 and 499 then
			-- invalid message type
			RAISE_APPLICATION_ERROR(-20000, 'Invalid message number');
		end if;
	
		-- only set the message id if it isn't already defined
		if ERR_MSG_ID is null then
			ERR_MSG_TYPE := UPPER(SUBSTR(MSG_ID_IN, 1, 3));
			ERR_MSG_ID   := MSG_ID_IN;
		end if;
	end;

	--------------------------------------------------------------
	-- Add the module name to the call stack, so that we know where
	-- any errors occurred.
	procedure SET_MODULE_NAME(MODULE_NAME_IN in varchar2) is
	begin

		if B_LOG_ALL then
			PKG_PP_LOG.P_WRITE_MESSAGE('Adding ' || MODULE_NAME_IN || ' to stack');
		end if;
	
		if STACK_IS_EMPTY then
			CALL_STACK(1) := SUBSTR(MODULE_NAME_IN, 1, 100);
			TOP_OF_STACK := 1;
			STACK_IS_EMPTY := false;
		else
			TOP_OF_STACK := TOP_OF_STACK + 1;
			CALL_STACK(TOP_OF_STACK) := SUBSTR(MODULE_NAME_IN, 1, 100);
		end if;
	
		-- clear out the msg nbr from any previous unsuccessful run
		ERR_MSG_ID      := null;
		ERR_COMMENTS    := null;
		CALL_STACK_TEXT := null;
	exception
		-- something really bad must happen to get here, probably some PL/SQL table error
		when others then
			PKG_PP_LOG.P_WRITE_MESSAGE('********************* STACK FAULT ************************');
			null;
	end;

	----------------------------------------------------------------
	-- As modules finish correctly the module name is dropped from
	-- the list. When the last module completes correctly the array is
	-- discarded.
	procedure REMOVE_MODULE_NAME is
		EMPTY_STACK exception;
	begin
		-- check if the user is trying to pop off an element that doesn't exist
		if STACK_IS_EMPTY then
			raise EMPTY_STACK;
		end if;
	
		if B_LOG_ALL then
			PKG_PP_LOG.P_WRITE_MESSAGE('removing ' || CALL_STACK(TOP_OF_STACK) || ' from stack....');
		end if;
		-- clear the contents of the stack element
		CALL_STACK(TOP_OF_STACK) := null;
		-- move the stack pointer down to the next element
		TOP_OF_STACK := TOP_OF_STACK - 1;
		-- if there are no more elements in the stack, set our boolean variable
		STACK_IS_EMPTY := (TOP_OF_STACK = 0);
	
		-- if the stack doesn't contain any elements, release the memory associated with it.
		if STACK_IS_EMPTY then
			CLEAR_STACK;
		end if;
	exception
		when EMPTY_STACK then
			PKG_PP_LOG.P_WRITE_MESSAGE('********************* STACK FAULT ************************');
			null;
		when others then
			RAISE_APPLICATION_ERROR(-20000,'Error in REMOVE_MODULE_NAME');
	end;

	--------------------------------------------------------------
	-- Return a text representation of the call stack
	function GET_CALL_STACK return varchar2 is
		L_CALL_STACK_TEXT varchar2(2000);
	begin
		if STACK_IS_EMPTY then
			L_CALL_STACK_TEXT := L_CALL_STACK_TEXT || '<empty>';
		else
			for STACK_INDEX in 1 .. TOP_OF_STACK
			loop
				if STACK_INDEX = 1 then
					L_CALL_STACK_TEXT := L_CALL_STACK_TEXT || CALL_STACK(STACK_INDEX);
				else
					L_CALL_STACK_TEXT := L_CALL_STACK_TEXT || ' => ' || CALL_STACK(STACK_INDEX);
				end if;
			end loop;
		end if;
	
		return(L_CALL_STACK_TEXT);
	exception
		when others then
			RAISE_APPLICATION_ERROR(-20000,'Error in GET_CALL_STACK');
	end;

	--------------------------------------------------------------
	-- Called when we determine that the required parameters aren't available
	-- The module names in the error stack are displayed with the module which has
	-- the error indicated
	procedure RAISE_ERROR(ERROR_TYPE in varchar2, COMMENTS_IN in varchar2 default null) is
		l_errm varchar2(4000);
	begin
	
		l_errm := sqlerrm;
		-- Some logic here based on Error Type if we so desire
		
	
		if CALL_STACK_TEXT is null then
			CALL_STACK_TEXT := SUBSTR(GET_CALL_STACK, 1, 2000);
			ERR_COMMENTS    := COMMENTS_IN || ': ' || l_errm;
			LOG_ERROR;
			G_LINE_NUMBER := regexp_substr(dbms_utility.format_error_backtrace,'line [0-9]+');
		end if;
	
		if upper(ERROR_TYPE) = 'ERROR' then
			CLEAR_STACK;
			RAISE_APPLICATION_ERROR(-20000,
									CALL_STACK_TEXT ||', '|| G_LINE_NUMBER||' -- ' || ERR_COMMENTS || ': ' ||l_errm);
			G_LINE_NUMBER := null;
		elsif upper(ERROR_TYPE) = 'WARNING' then
			--Do something cool here in the future
			REMOVE_MODULE_NAME;
		elsif upper(ERROR_TYPE) = 'INFO' then
			--Do something cool here in the future
			REMOVE_MODULE_NAME;
		else
			RAISE_ERROR('ERROR','Invalid Error type!: ' ||l_errm);
		end if;
		
	end;
end;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (11669, 0, 2018, 1, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2018.1.0.0_PKG_PP_ERROR.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
