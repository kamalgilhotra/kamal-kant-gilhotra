create or replace package pkg_lease_asset_post as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_ASSET_POST
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 2015.1   11/07/2014 Anand Rajashekar  PP-41098
   || 2015.2   10/12/2015 Andrew Scott   Maint-45003. Prevent error when xfering unmatched sobs.
   || 2016.1   03/11/2016 S. Byers       Maint-45567: Add company_set_of_books when checking for second book
   || 2017.1   07/14/2017 Sisouphanh     Maint-48238: MultCurr - Enhance Asset In Service to Translate Amounts
   ||                                                 into Company Currency
  || 2017.1   07/26/2017 Charlie Shilling  maint-48366: update retire function to work with multicurrency
  || 2017.1   07/28/2017 Anand R        maint-48364: update tranfer function to work with multicurrency
   || 2017.2   02/08/2018 Sarah Byers    maint-50400: Fix for back-dated leases to generate 3042 entries
   ||============================================================================
   */
  G_SEND_JES    boolean NOT NULL := true;
   G_PKG_VERSION varchar(35) := '2018.2.0.0';

   procedure P_SET_ILR_ID(A_ILR_ID number);

   procedure P_GETTAXES(A_ASSET_IDS in PKG_LEASE_CALC.NUM_ARRAY);

   -- Function to perform specific Leased Asset addition
   function F_ADD_ASSET(A_LS_ASSET_ID number) return varchar2;

   -- Function to perform specific Leased Asset retirements
   function F_RETIRE_ASSET(A_CPR_ASSET_ID        number,
                           A_ACCT_MONTH          date,
                           A_MONTH               date,
                           A_RETIREMENT_AMOUNT   number,
                           A_RETIREMENT_QUANTITY number,
                           A_GAIN_LOSS_AMOUNT    number,
                           A_DG_ID               number,
                           A_PT_ID               number,
                           A_MSG                 out varchar2) return number;

   -- Function to perform specific Leased Asset transfer from
   function F_TRANSFER_ASSET_FROM(A_CPR_ASSET_ID number,
                                  A_AMOUNT       number,
                                  A_QUANTITY     number,
                                  A_PT_ID        number,
                                  A_MSG          out varchar2) return number;

   -- Function to perform specific Leased Asset transfer to
   -- takes in the asset attributes to handle a transfer.
   -- out parameter for message.  Returns a number
   -- 1 for success, 0 for failure
   function F_TRANSFER_ASSET_TO(A_CPR_ASSET_ID       number,
                                A_AMOUNT             number,
                                A_QUANTITY           number,
                                A_COMPANY_ID         number,
                                A_BUS_SEGMENT_ID     number,
                                A_UTILITY_ACCOUNT_ID number,
                                A_SUB_ACCOUNT_ID     number,
                                A_FUNC_CLASS_ID      number,
                                A_PROPERTY_GROUP_ID  number,
                                A_RETIREMENT_UNIT_ID number,
                                A_ASSET_LOCATION_ID  number,
                                A_DG_ID              number,
                                A_PT_ID              number,
                               A_MSG                out varchar2)
    return number;

  -- Function to delete assets and all dependant tables
  function F_DELETE_ASSETS(A_ASSET_IDS in PKG_LEASE_CALC.NUM_ARRAY)
    return varchar2;

   function F_GET_ILR_ID return number;
end PKG_LEASE_ASSET_POST;
/
create or replace package body pkg_lease_asset_post as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_ASSET_POST
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 2015.1   11/07/2014 Anand Rajashekar  PP-41098
   || 2015.2   10/12/2015 Andrew Scott   Maint-45003. Prevent error when xfering unmatched sobs.
   || 2016.1   03/11/2016 S. Byers       Maint-45567: Add company_set_of_books when checking for second book
   || 2017.4   06/26/2018 David Conway   maint-51542: IS_OM determined at SOB instead of Lease Cap Type.
   || 2017.4   06/28/2018 David Conway   maint-51542: Corrected inverted IS_OM determination logic.
   ||============================================================================
   */

   L_ILR_ID number;

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   --**************************************************************************
   --                            SET_ILR_ID
   --**************************************************************************

   procedure P_SET_ILR_ID(A_ILR_ID number) is
   begin
      L_ILR_ID := A_ILR_ID;
   end P_SET_ILR_ID;

   --**************************************************************************
   --                            GETTAXES
   --**************************************************************************

procedure P_GETTAXES(A_ASSET_IDS in PKG_LEASE_CALC.NUM_ARRAY) is
      ENABLED_LOCAL_TAXES PKG_LEASE_CALC.NUM_ARRAY;
    counter             number;
   begin

      /* WMD We need to change the way this is working. Initiated assets are getting all tax types assigned to them */
    for I in 1 .. A_ASSET_IDS.COUNT
    loop

        /* This is ridiculous but we have to do this */
        insert into ls_asset_tax_map
        (ls_asset_id, tax_local_id, status_code_id)
        select A_ASSET_IDS(I), tl.tax_local_id, 0
        from ls_tax_local tl
         where not exists (select 1
                  from ls_asset_tax_map z
                 where z.ls_asset_id = A_ASSET_IDS(I)
                   and z.tax_local_id = tl.tax_local_id);

        select count(1)
        into counter
        from ls_asset_tax_map
        where ls_asset_id = A_ASSET_IDS(I)
          and status_code_id = 1;

        if counter = 0 then
          goto endloop;
        end if;

         --create an array of local taxes that are disabled under this asset
         select TAX_LOCAL_ID
        bulk collect
        into ENABLED_LOCAL_TAXES
         from LS_ASSET_TAX_MAP
         where STATUS_CODE_ID = 1
           and LS_ASSET_ID = A_ASSET_IDS(I);

         --delete old records from ls_asset_tax_map
         delete from LS_ASSET_TAX_MAP where LS_ASSET_ID = A_ASSET_IDS(I);

         --insert new records into ls_asset_tax_map
         insert into LS_ASSET_TAX_MAP
            (LS_ASSET_ID, TAX_LOCAL_ID, STATUS_CODE_ID)
        (select A_ASSET_IDS(I) LS_ASSET_ID,
                TL.TAX_LOCAL_ID TAX_LOCAL_ID,
                0 STATUS_CODE_ID
               from LS_ASSET A, LS_TAX_LOCAL TL
              where A_ASSET_IDS(I) = A.LS_ASSET_ID
                and A.TAX_SUMMARY_ID = TL.TAX_SUMMARY_ID);

         --the insert statement defaults all local taxes to enabled, so disable the ones that need to be
      for J in 1 .. ENABLED_LOCAL_TAXES.COUNT
      loop
            update LS_ASSET_TAX_MAP
               set STATUS_CODE_ID = 1
             where LS_ASSET_ID = A_ASSET_IDS(I)
               and TAX_LOCAL_ID = ENABLED_LOCAL_TAXES(J);
         end loop;

      <<endloop>>
      null;
      end loop;
   end P_GETTAXES;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
  function F_FOOT_DEPR(A_CPR_ASSET_ID IN NUMBER,
                       A_MONTH        IN DATE) return varchar2 is
  L_STATUS varchar2(5000);
  begin

  /*logic primarily from f_calc_cpr_depr_end_bals*/
   L_STATUS := 'Footing CPR depr end balances for current month';
  
  update cpr_depr
       set depr_reserve  = nvl(beg_reserve_month, 0) + nvl(retirements, 0) +
                           nvl(depr_exp_alloc_adjust, 0) +
                           nvl(depr_exp_adjust, 0) + nvl(salvage_dollars, 0) +
                           nvl(cost_of_removal, 0) +
                           nvl(other_credits_and_adjust, 0) +
                           nvl(gain_loss, 0) + nvl(reserve_trans_in, 0) +
                           nvl(reserve_trans_out, 0) +
                           nvl(curr_depr_expense, 0) +
                           nvl(reserve_adjustment, 0) +
                           nvl(impairment_asset_amount, 0) +
                           nvl(impairment_expense_amount, 0),
           asset_dollars = nvl(beg_asset_dollars, 0) +
                           nvl(net_adds_and_adjust, 0) + nvl(retirements, 0) +
                           nvl(transfers_in, 0) + nvl(transfers_out, 0) +
                           nvl(impairment_asset_amount, 0)
      where gl_posting_mo_yr = A_MONTH
      and asset_id = A_CPR_ASSET_ID;
	  
	L_STATUS := 'Updating CPR Depr YTD fields';
	
    update cpr_depr a set (ytd_depr_expense, ytd_depr_exp_adjust ) =
	(select decode(to_number(to_char(b.gl_posting_mo_yr,'MM')),12,0,
				nvl(b.ytd_depr_expense,0)) + nvl(a.curr_depr_expense,0), 
	decode(to_number(to_char(b.gl_posting_mo_yr,'MM')),12,0,
				nvl(b.ytd_depr_exp_adjust,0)) + nvl(a.depr_exp_adjust,0) + nvl(a.depr_exp_alloc_adjust,0)
	from cpr_depr b
	where a.asset_id = b.asset_id 
	and a.set_of_books_id = b.set_of_books_id
	and b.gl_posting_mo_yr = add_months(a.gl_posting_mo_yr,-1)
	
	)
	where gl_posting_mo_yr = A_MONTH
      and asset_id = A_CPR_ASSET_ID
	and exists (select 1 from cpr_depr z
					where a.asset_id = z.asset_id
					and a.set_of_books_id  = z.set_of_books_id
					and z.gl_posting_mo_yr = add_months(a.gl_posting_mo_yr,-1));
					
    update cpr_depr a 
	set ytd_depr_expense = nvl(curr_depr_expense,0)
	, ytd_depr_exp_adjust = nvl(depr_exp_adjust,0) + nvl(depr_exp_alloc_adjust,0)
	where gl_posting_mo_yr = A_MONTH
    and asset_id = A_CPR_ASSET_ID
	and not exists (select 1 from cpr_depr z
					where a.asset_id = z.asset_id
					and a.set_of_books_id  = z.set_of_books_id
					and z.gl_posting_mo_yr = add_months(a.gl_posting_mo_yr,-1))
	;				
	
	/*logic primarily from f_calc_cpr_depr_roll_fwd*/
	L_STATUS := 'Updating CPR Depr for Future Month';
	
	 update cpr_depr a 
		set (beg_reserve_month,beg_asset_dollars, 
				ytd_depr_expense, ytd_depr_exp_adjust,
				beg_reserve_year, remaining_life,
				impairment_asset_begin_balance
				) = 
					 (select nvl(b.depr_reserve,0), nvl(b.asset_dollars,0), 
							decode(to_number(to_char(b.gl_posting_mo_yr,'MM')),12,0,nvl(b.ytd_depr_expense,0)), 
							decode(to_number(to_char(b.gl_posting_mo_yr,'MM')),12,0,nvl(b.ytd_depr_exp_adjust,0)), 
					 		decode(to_number(to_char(b.gl_posting_mo_yr,'MM')),12,nvl(b.depr_reserve,0),nvl(b.beg_reserve_year,0)), 
							greatest(b.remaining_life - decode(nvl(d.init_life, -1),-1,
								decode(b.init_life,b.remaining_life,b.mid_period_conv,1),1), 0),
								b.impairment_asset_begin_balance + b.impairment_asset_activity_salv
							from  cpr_depr b, cpr_depr d
							where a.asset_id = b.asset_id 
							and a.set_of_books_id = b.set_of_books_id
							and b.gl_posting_mo_yr = add_months(a.gl_posting_mo_yr,-1)
							and b.asset_id = d.asset_id (+)
							and b.set_of_books_id = d.set_of_books_id (+)
							and add_months(b.gl_posting_mo_yr,-1) = d.gl_posting_Mo_yr (+)
					)
		where asset_id = A_CPR_ASSET_ID
		and a.gl_posting_mo_yr = add_months(a_month,1)
		and exists (select 1 from cpr_depr z
				where a.asset_id = z.asset_id
				and a.set_of_books_id  = z.set_of_books_id
				and z.gl_posting_mo_yr = add_months(a.gl_posting_mo_yr,-1))
		;
		
	    update cpr_depr a 
		set (prior_ytd_depr_expense,prior_ytd_depr_exp_adjust) = 
				 (select nvl(b.ytd_depr_expense,0), nvl(b.ytd_depr_exp_adjust,0)
				  from  cpr_depr b
						where a.asset_id = b.asset_id 
						and a.set_of_books_id = b.set_of_books_id
						and b.gl_posting_mo_yr = add_months(a.gl_posting_mo_yr,-12)
				)
		where asset_id = A_CPR_ASSET_ID
		and a.gl_posting_mo_yr = add_months(a_month,1)
		and exists (select 1 from cpr_depr z
				where a.asset_id = z.asset_id
				and a.set_of_books_id  = z.set_of_books_id
				and z.gl_posting_mo_yr = add_months(a.gl_posting_mo_yr,-12))
		;
			
 
    /*logic primarily from f_depr_ledger_roll_fwd, also in f_lam_cpr_close for all groups*/
	 L_STATUS := 'Updating balances on Depr Ledger for current month';

      UPDATE depr_ledger u
	      SET ( end_balance, end_reserve, reserve_bal_cor, reserve_bal_adjust,
		        reserve_bal_retirements, reserve_bal_tran_in, reserve_bal_tran_out,
		        reserve_bal_gain_loss, reserve_bal_other_credits, current_net_salvage_reserve,
		        cor_end_reserve, reserve_bal_salvage_exp, salvage_balance, reserve_bal_impairment
		        ) =
		            (SELECT   c.begin_balance + c.additions + c.retirements + c.transfers_in
					            + c.transfers_out + c.adjustments + c.impairment_asset_amount as end_balance, 
								c.begin_reserve + c.retirements + c.gain_loss + c.salvage_cash
					            + c.salvage_returns + c.reserve_credits + c.reserve_tran_in + c.reserve_tran_out
					            + c.reserve_adjustments + c.depr_exp_alloc_adjust + c.depr_exp_adjust + c.depreciation_expense
					            - nvl(c.current_net_salvage_amort,0) + decode(nvl(subledger_type_id,0), 0, 0, c.cost_of_removal)
					            + c.salvage_expense + c.salvage_exp_adjust + c.salvage_exp_alloc_adjust  + c.reserve_blending_transfer
					            + c.impairment_expense_amount + c.impairment_asset_amount as end_reserve
				            , nvl(p.reserve_bal_cor,0) + c.cost_of_removal as reserve_bal_cor
				            , nvl(p.reserve_bal_adjust,0) + c.reserve_adjustments + c.reserve_blending_adjustment + c.reserve_blending_transfer as reserve_bal_adjust
				            , nvl(p.reserve_bal_retirements,0) + c.retirements as reserve_bal_retirements
				            , nvl(p.reserve_bal_tran_in,0) + c.reserve_tran_in as reserve_bal_tran_in
				            , nvl(p.reserve_bal_tran_out,0) + c.reserve_tran_out as reserve_bal_tran_out
				            , nvl(p.reserve_bal_gain_loss,0) + c.gain_loss as reserve_bal_gain_loss
				            , nvl(p.reserve_bal_other_credits,0) + c.reserve_credits as reserve_bal_other_credits
				            , nvl(p.current_net_salvage_reserve,0) + nvl(c.current_net_salvage_amort,0) as current_net_salvage_reserve
				            , decode(nvl(subledger_type_id,0), 0, c.cor_beg_reserve + c.cor_expense + c.cor_exp_adjust + c.cost_of_removal
					            + c.cor_res_tran_in + c.cor_res_tran_out + c.cor_res_adjust + c.cor_exp_alloc_adjust  + c.cor_blending_transfer, 0) as cor_end_reserve
				            , nvl(p.reserve_bal_salvage_exp, 0) + c.salvage_expense + c.salvage_exp_adjust + c.salvage_exp_alloc_adjust as reserve_bal_salvage_exp
				            , nvl(p.salvage_balance, 0) + c.salvage_cash + c.salvage_returns as salvage_balance
				            , (nvl(p.reserve_bal_impairment, 0) + c.impairment_expense_amount + c.impairment_asset_amount) as reserve_bal_impairment
		            from  depr_ledger p, depr_ledger c, depr_group g
		            where p.depr_group_id (+) = c.depr_group_id
		            and   p.set_of_books_id (+) = c.set_of_books_id
		            and   c.depr_group_id = g.depr_group_id
		            and   to_char(p.gl_post_mo_yr (+), 'mm/yyyy') = to_char(add_months(c.gl_post_mo_yr,-1), 'mm/yyyy' )
		            and   u.depr_group_id = c.depr_group_id
		            and   u.set_of_books_id = c.set_of_books_id
		            and   u.gl_post_mo_yr = c.gl_post_mo_yr
		          )
	      WHERE u.gl_post_mo_yr = A_MONTH
	      AND	u.depr_group_id IN (select depr_group_id
										  from cpr_ledger
										 where asset_id = A_CPR_ASSET_ID
									);		 
	
	  L_STATUS := 'Update beginning balances on Depr Ledger for next month';

      UPDATE depr_ledger a
      SET (begin_reserve, begin_balance, impairment_reserve_beg, cor_beg_reserve, impairment_asset_begin_balance) =
	      (SELECT b.end_reserve, b.end_balance, impairment_reserve_end, cor_end_reserve, impairment_asset_begin_balance + impairment_asset_activity_salv
		      FROM  depr_ledger b
		      WHERE a.set_of_books_id = b.set_of_books_id and
				      a.depr_group_id   = b.depr_group_id   and
				      Add_Months(a.gl_post_mo_yr, -1) =b.gl_post_mo_yr
	      )
      WHERE a.gl_post_mo_yr = add_months(A_MONTH,1)
      AND a.depr_group_id IN (select depr_group_id
										  from cpr_ledger
										 where asset_id = A_CPR_ASSET_ID
                              );
	
			 
			 

    RETURN 'OK';

  EXCEPTION
    WHEN OTHERS THEN
    RETURN SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
  END F_FOOT_DEPR;

   -- Function to perform specific Leased Asset Adjustment
  function F_ADJUST_ASSET(A_LS_ASSET       in LS_ASSET%rowtype,
                          A_PRIOR_REVISION in number,
                          A_REVISION       in number,
                          A_SOB            in number,
                          A_OBL_ADJ        out number,
                          A_LT_OBL_ADJ     out number,
                          A_DEF_ADJ        out number,
                          A_LT_DEF_ADJ     out number,
                          A_PREPAID_ADJ    out number,
                          A_IDC_ADJ        out number,
                          A_INC_ADJ        out number,
                          A_UNACCRUED_ADJ  out number,
							            A_IMPAIRMENT_ADJ out number,
                          A_ASSET_ID       out number,
                          A_OM_TO_CAP      out boolean,
                          A_MONTH          in date := NULL) return varchar2 is
      L_MSG                     varchar2(2000);
      L_ASSET_SCHEDULE_MONTH    LS_ASSET_SCHEDULE%rowtype;
      L_ASSET_SCHEDULE_APPROVED LS_ASSET_SCHEDULE%rowtype;
      L_MONTH                   date;
      L_TRANSFER_CHECK          number;
      L_COUNT                   number;
      L_REMEASUREMENT           NUMBER;
      L_ORIG_IS_OM              NUMBER;
      L_REMEASURE_IS_OM         NUMBER;
      L_REMEASURE_DATE          DATE;
      LB_OM_TO_CAP              BOOLEAN := FALSE;
      L_OM_TO_CAP               NUMBER;
      L_NEW_FASB_CAP_TYPE       NUMBER;
   begin

    pkg_pp_log.p_write_message('ADJUSTING ASSET');

    A_OM_TO_CAP := LB_OM_TO_CAP;

    L_MSG := 'Getting the current open month for lessee';
    select greatest(min(LPC.GL_POSTING_MO_YR), min(LAS.MONTH))
        into L_MONTH
        from LS_PROCESS_CONTROL LPC, LS_ASSET_SCHEDULE LAS
       where LPC.LAM_CLOSED is null
         and LPC.COMPANY_ID = A_LS_ASSET.COMPANY_ID
     and LAS.LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID
     and LAS.REVISION = A_REVISION;

     /* WMD */
     IF A_MONTH IS NOT NULL THEN
      L_MONTH := A_MONTH;
     end if;

      L_MSG := 'Starting to retrieve leased asset schedule for the month';
      --get the asset_schedule for the first month
      select AA.*
        into L_ASSET_SCHEDULE_MONTH
        from LS_ASSET_SCHEDULE AA
       where AA.LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID
         and AA.REVISION = A_REVISION
         and AA.SET_OF_BOOKS_ID = A_SOB
         and aa."MONTH" = L_MONTH;

      L_MSG := 'Checking for remeasurement';
	  pkg_pp_log.p_write_message(l_msg);
      SELECT Count(1)
      INTO L_REMEASUREMENT
      FROM ls_ilr_options o
      WHERE o.ilr_id = A_LS_ASSET.ILR_ID
      AND o.revision = A_REVISION
      AND o.remeasurement_date IS NOT NULL;

      select count(1)
      into L_TRANSFER_CHECK
      from ls_asset_transfer_history
      where to_ls_asset_id = A_LS_ASSET.LS_ASSET_ID
        and month = L_MONTH;

      if L_TRANSFER_CHECK = 1 THEN
      L_MSG := 'Starting to retrieve leased asset schedule for the transferred revision';
      --get the asset_schedule for the first month
      select AA.*
        into L_ASSET_SCHEDULE_APPROVED
        from LS_ASSET_SCHEDULE AA
       where AA.LS_ASSET_ID =
             (select from_ls_asset_id
                from ls_asset_transfer_history
               where month = L_MONTH
                 and to_ls_asset_id = A_LS_ASSET.LS_ASSET_ID)
         and AA.REVISION =
             (select approved_revision
                from ls_asset
               where ls_asset_id =
                     (select from_ls_asset_id
                        from ls_asset_transfer_history
                       where month = L_MONTH
                         and to_ls_asset_id = A_LS_ASSET.LS_ASSET_ID))
         and AA.SET_OF_BOOKS_ID = A_SOB
         and aa."MONTH" = L_MONTH;
      ELSE

      L_MSG := 'Starting to retrieve approved leased asset schedule for first month';
      --get the asset_schedule for the first month
      select count(1)
      into l_count
      from LS_ASSET_SCHEDULE AA
       where AA.LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID
         and AA.REVISION = A_PRIOR_REVISION
         and AA.SET_OF_BOOKS_ID = A_SOB
         and aa."MONTH" = L_MONTH;

      if L_COUNT = 0 then
        PKG_PP_LOG.P_WRITE_MESSAGE('No row found for ' ||
                                   to_char(l_month, 'yyyymm') ||
                                   ' for this asset. No adjustment will be made');
          return 'OK';
      end if;

      select AA.*
        into L_ASSET_SCHEDULE_APPROVED
        from LS_ASSET_SCHEDULE AA
       where AA.LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID
         and AA.REVISION = A_PRIOR_REVISION
         and AA.SET_OF_BOOKS_ID = A_SOB
         and aa."MONTH" = L_MONTH;

    end if;

     IF L_REMEASUREMENT = 1 THEN
      --See if we need to convert Cap Type for IDC and Incentive
      -- check up front since the remeasurement date is needed for the other
      -- amounts for backdated remeasurements
      SELECT CASE
               WHEN orig_is_om.fasb_cap_type_id in (4, 5) THEN
                1
               ELSE
                0
             END AS ORIG_OM,
             CASE
               WHEN remeasure_is_om.fasb_cap_type_id in (4, 5) THEN
                1
               ELSE
                0
             END AS REMEASURE_OM,
             Trunc(remeasure_revision.remeasurement_date, 'month') remeasurement_date,
             remeasure_is_om.fasb_cap_type_id
        INTO L_ORIG_IS_OM, L_REMEASURE_IS_OM, L_REMEASURE_DATE, L_NEW_FASB_CAP_TYPE
        FROM ls_ilr_options           orig_revision,
             ls_ilr_options           remeasure_revision,
             ls_fasb_cap_type_sob_map orig_is_om,
             ls_fasb_cap_type_sob_map remeasure_is_om
       WHERE orig_revision.ilr_id = a_ls_asset.ilr_id
         AND orig_revision.revision = A_PRIOR_REVISION
         AND orig_revision.ilr_id = remeasure_revision.ilr_id
         AND remeasure_revision.revision = a_revision
         AND orig_is_om.set_of_books_id = a_sob
         AND orig_revision.lease_cap_type_id = orig_is_om.lease_cap_type_id
         AND remeasure_is_om.set_of_books_id = a_sob
         AND remeasure_revision.lease_cap_type_id =
             remeasure_is_om.lease_cap_type_id;
    
        L_MSG := 'Setting amounts for remeasurements';

      -- Short Term Obligation Adjustment
      if L_REMEASURE_DATE < L_MONTH then
        -- backdated remeasurement
      A_OBL_ADJ := case
                     when L_ASSET_SCHEDULE_MONTH.IS_OM = 1 then
                      0
                     else
                        (L_ASSET_SCHEDULE_MONTH.BEG_OBLIGATION - L_ASSET_SCHEDULE_MONTH.BEG_LT_OBLIGATION)
                        - (L_ASSET_SCHEDULE_APPROVED.BEG_OBLIGATION - L_ASSET_SCHEDULE_APPROVED.BEG_LT_OBLIGATION)                           
                     end;
      else
        A_OBL_ADJ := case
                       when L_ASSET_SCHEDULE_MONTH.IS_OM = 1 then
                        0
                       else
                      L_ASSET_SCHEDULE_MONTH.ST_OBLIGATION_REMEASUREMENT
                   end;
      end if;
    
      pkg_pp_log.p_write_message('Short term obligation amount: ' ||
                                 to_char(a_obl_adj));

      -- Long Term Obligation Adjustment
      if L_REMEASURE_DATE < L_MONTH then
        -- backdated remeasurement
      A_LT_OBL_ADJ := case
                        when L_ASSET_SCHEDULE_MONTH.IS_OM = 1 then
                         0
                        else
                           L_ASSET_SCHEDULE_MONTH.BEG_LT_OBLIGATION -
                           L_ASSET_SCHEDULE_APPROVED.BEG_LT_OBLIGATION
                        end;
      else
        A_LT_OBL_ADJ := case
                          when L_ASSET_SCHEDULE_MONTH.IS_OM = 1 then
                           0
                          else
                         L_ASSET_SCHEDULE_MONTH.LT_OBLIGATION_REMEASUREMENT
                      end;
      end if;
      pkg_pp_log.p_write_message('Long term obligation amount: ' ||
                                 to_char(a_lt_obl_adj));

      -- Unaccrued Interest Adjustment
      if L_REMEASURE_DATE < L_MONTH then
        -- backdated remeasurement
      A_UNACCRUED_ADJ := case
                           when L_ASSET_SCHEDULE_MONTH.IS_OM = 1 then
                            0
                           else
                              (L_ASSET_SCHEDULE_MONTH.BEG_LT_LIABILITY -
                              L_ASSET_SCHEDULE_APPROVED.BEG_LT_LIABILITY) -
                              (L_ASSET_SCHEDULE_MONTH.BEG_LT_OBLIGATION -
                              L_ASSET_SCHEDULE_APPROVED.BEG_LT_OBLIGATION)
                           end;
      else
        A_UNACCRUED_ADJ := case
                             when L_ASSET_SCHEDULE_MONTH.IS_OM = 1 then
                              0
                             else
                            L_ASSET_SCHEDULE_MONTH.UNACCRUED_INTEREST_REMEASURE
                         end;
      end if;
      pkg_pp_log.p_write_message('Unaccrued Interest amount: ' ||
                                 to_char(a_unaccrued_adj));

      -- Short Term Deferred Rent Adjustment
      A_DEF_ADJ := case
                     when L_ASSET_SCHEDULE_MONTH.IS_OM = 0 then
                      0
                     else
                       case when L_NEW_FASB_CAP_TYPE in (3, 6) then
                         0
                       else
                         L_ASSET_SCHEDULE_MONTH.END_ST_DEFERRED_RENT -
                         L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT
                       end
                   end;
      pkg_pp_log.p_write_message('Short term deferred rent amount: ' ||
                                 to_char(a_def_adj));

      -- Long Term Deferred Rent Adjustment
      A_LT_DEF_ADJ := case
                        when L_ASSET_SCHEDULE_MONTH.IS_OM = 0 then
                         0
                        else
                          case when L_NEW_FASB_CAP_TYPE in (3, 6) then
                            0
                          else
                            L_ASSET_SCHEDULE_MONTH.END_DEFERRED_RENT -
                            L_ASSET_SCHEDULE_MONTH.BEG_DEFERRED_RENT
                          end
                      end;
      pkg_pp_log.p_write_message('Long term deferred rent amount: ' ||
                                 to_char(a_lt_def_adj));

      -- Prepaid Rent Adjustment
      A_PREPAID_ADJ := case
                         when L_ASSET_SCHEDULE_MONTH.IS_OM = 0 then
                          0
                         else
                           case when L_NEW_FASB_CAP_TYPE in (3, 6) then
                             0
                           else
                             L_ASSET_SCHEDULE_MONTH.END_PREPAID_RENT -
                             L_ASSET_SCHEDULE_MONTH.BEG_PREPAID_RENT
                           end
                       end;
      pkg_pp_log.p_write_message('Prepaid rent amount: ' ||
                                 to_char(a_prepaid_adj));

		  L_MSG := 'Retrieving Remeasurement Amounts for Incentive and Initial Direct Cost';
		  IF L_ORIG_IS_OM = 1 AND L_REMEASURE_IS_OM = 0 and L_NEW_FASB_CAP_TYPE not in (3, 6) THEN
			  --Get IDC and Incentive Math amounts at remeasurement
			  L_MSG := 'Retrieving Remeasurement Amounts for Incentive and Initial Direct Cost';
			  pkg_pp_log.p_write_message(l_msg);
			  lb_om_to_cap := TRUE;
        A_OM_TO_CAP  := TRUE;
        select nvl(idc_math_amount, 0), nvl(incentive_math_amount, 0)
				  into a_idc_adj, a_inc_adj
				  from ls_asset_schedule s
				  WHERE s.ls_asset_id = a_ls_asset.ls_asset_id
				  AND s.revision = a_revision
				  AND s.MONTH = l_remeasure_date
				  AND S.set_of_books_id = a_sob;

        pkg_pp_log.p_write_message('Initial Direct Cost Remeasurement: ' ||
                                   to_char(a_idc_adj));
        pkg_pp_log.p_write_message('Incentive Remeasurement: ' ||
                                   to_char(a_inc_adj));
		  ELSE
				A_IDC_ADJ := 0;
				A_INC_ADJ := 0;
		  end if;

     ELSE
        L_MSG := 'Setting adjustment amounts';
     /* CJS 2/16/15 Changing ST obligation amount */
      A_OBL_ADJ := case
                     when L_ASSET_SCHEDULE_MONTH.IS_OM = 1 then
                      0
                     else
                      (L_ASSET_SCHEDULE_MONTH.BEG_OBLIGATION -
                      L_ASSET_SCHEDULE_MONTH.BEG_LT_OBLIGATION)
                   end - (case
                     when L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 then
                      0
                     else
                      L_ASSET_SCHEDULE_APPROVED.BEG_OBLIGATION -
                      L_ASSET_SCHEDULE_APPROVED.BEG_LT_OBLIGATION
                   end);

      pkg_pp_log.p_write_message('Short term obligation amount: ' ||
                                 to_char(a_obl_adj));

      A_LT_OBL_ADJ := case
                        when L_ASSET_SCHEDULE_MONTH.IS_OM = 1 then
                         0
                        else
                         L_ASSET_SCHEDULE_MONTH.BEG_LT_OBLIGATION
                      end - case
                        when L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 then
                         0
                        else
                         L_ASSET_SCHEDULE_APPROVED.BEG_LT_OBLIGATION
                      END;

      pkg_pp_log.p_write_message('Long term obligation amount: ' ||
                                 to_char(a_lt_obl_adj));

        -- go ahead and break out the use cases for both sides
        -- for Off Balance Sheet to Off Balance Sheet, just want the true difference between deferred rent
        -- for Off-BS to On-BS, we want to back out the deferred rent from the approved revision
        -- for On-BS to Off-BS, we want to true-up just the new revision deferred rent
        -- for On-BS to On-BS, we just want to pull whatever is there
        -- also to note, for On-BS, this is used for 3057/3058 trans types, for Off-BS, its used in 3053-3056 trans types
        -- don't journal deferred/prepaid rent if the new fasb cap type is 3 or 6 (FERC Depr)
        
      L_MSG := 'Check for transition from off to on';
      pkg_pp_log.p_write_message(l_msg);
      SELECT CASE
               WHEN orig_is_om.fasb_cap_type_id in (4, 5) AND
                    remeasure_is_om.fasb_cap_type_id NOT in (4, 5) THEN
                1
               ELSE
                0
            END,
            remeasure_is_om.fasb_cap_type_id
            INTO L_OM_TO_CAP, L_NEW_FASB_CAP_TYPE
        FROM ls_ilr_options           orig_revision,
             ls_ilr_options           remeasure_revision,
             ls_fasb_cap_type_sob_map orig_is_om,
             ls_fasb_cap_type_sob_map remeasure_is_om
		  WHERE orig_revision.ilr_id = a_ls_asset.ilr_id
		  AND orig_revision.revision = A_PRIOR_REVISION
		  AND orig_revision.ilr_id = remeasure_revision.ilr_id
		  AND remeasure_revision.revision = a_revision
          AND orig_is_om.set_of_books_id = a_sob
		  AND orig_revision.lease_cap_type_id = orig_is_om.lease_cap_type_id
          AND remeasure_is_om.set_of_books_id = a_sob
         AND remeasure_revision.lease_cap_type_id =
             remeasure_is_om.lease_cap_type_id;

      LB_OM_TO_CAP := CASE
                        WHEN L_OM_TO_CAP = 1 THEN
                         TRUE
                        ELSE
                         FALSE
                      END;
      A_OM_TO_CAP  := LB_OM_TO_CAP;
      
        
      A_DEF_ADJ := CASE
                          -- Off BS to Off BS, true difference in differed rent
                     WHEN L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 and
                          L_ASSET_SCHEDULE_MONTH.IS_OM = 1 THEN
                      L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT -
                      L_ASSET_SCHEDULE_APPROVED.BEG_ST_DEFERRED_RENT
                          -- Off BS to On BS, back out deferred rent from approved revision
                     WHEN L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 and
                          L_ASSET_SCHEDULE_MONTH.IS_OM = 0 and
                          L_NEW_FASB_CAP_TYPE not in (3,6) THEN
                            L_ASSET_SCHEDULE_APPROVED.BEG_ST_DEFERRED_RENT
                          -- On BS to Off BS, true-up just the new revision deferred rent
                     WHEN L_ASSET_SCHEDULE_APPROVED.IS_OM = 0 and
                          L_ASSET_SCHEDULE_MONTH.IS_OM = 1 THEN
                            L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT
                          -- On BS to On BS, just pull whatever is there
                          ELSE
                            case when L_NEW_FASB_CAP_TYPE not in (3,6) then
                                L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT -
                                L_ASSET_SCHEDULE_APPROVED.BEG_ST_DEFERRED_RENT
                              else
                                0
                            end
                        END;

      pkg_pp_log.p_write_message('Short term deferred rent amount: ' ||
                                 to_char(a_def_adj));

        A_LT_DEF_ADJ := case
                          -- Off BS to Off BS, true difference in differed rent
                        WHEN L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 AND
                             L_ASSET_SCHEDULE_MONTH.IS_OM = 1 THEN
                         (L_ASSET_SCHEDULE_MONTH.BEG_DEFERRED_RENT -
                         L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT) -
                         (L_ASSET_SCHEDULE_APPROVED.BEG_DEFERRED_RENT -
                         L_ASSET_SCHEDULE_APPROVED.BEG_ST_DEFERRED_RENT)
                          -- Off BS to On BS, back out deferred rent from approved revision
                        WHEN L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 AND
                             L_ASSET_SCHEDULE_MONTH.IS_OM = 0 and
                             L_NEW_FASB_CAP_TYPE not in (3,6) THEN
                         (L_ASSET_SCHEDULE_APPROVED.BEG_DEFERRED_RENT -
                         L_ASSET_SCHEDULE_APPROVED.BEG_ST_DEFERRED_RENT)
                          -- On BS to Off BS, true-up just the new revision deferred rent
                        WHEN L_ASSET_SCHEDULE_APPROVED.IS_OM = 0 AND
                             L_ASSET_SCHEDULE_MONTH.IS_OM = 1 THEN
                         (L_ASSET_SCHEDULE_MONTH.BEG_DEFERRED_RENT -
                         L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT)
                          -- On BS to On BS, just pull whatever is there
                          ELSE
                            case when L_NEW_FASB_CAP_TYPE not in (3,6) then
                               (L_ASSET_SCHEDULE_MONTH.BEG_DEFERRED_RENT -
                               L_ASSET_SCHEDULE_MONTH.BEG_ST_DEFERRED_RENT) -
                               (L_ASSET_SCHEDULE_APPROVED.BEG_DEFERRED_RENT -
                               L_ASSET_SCHEDULE_APPROVED.BEG_ST_DEFERRED_RENT)
                            else
                              0
                            end
                        END;

      pkg_pp_log.p_write_message('Long term deferred rent amount: ' ||
                                 to_char(a_lt_def_adj));

          A_PREPAID_ADJ := CASE
                            -- Off BS to Off BS, true difference in differed rent
                         WHEN L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 and
                              L_ASSET_SCHEDULE_MONTH.IS_OM = 1 THEN
                          L_ASSET_SCHEDULE_MONTH.BEG_PREPAID_RENT -
                          L_ASSET_SCHEDULE_APPROVED.BEG_PREPAID_RENT
                            -- Off BS to On BS, back out deferred rent from approved revision
                         WHEN L_ASSET_SCHEDULE_APPROVED.IS_OM = 1 and
                              L_ASSET_SCHEDULE_MONTH.IS_OM = 0 and
                              L_NEW_FASB_CAP_TYPE not in (3,6) THEN
                              L_ASSET_SCHEDULE_APPROVED.BEG_PREPAID_RENT
                            -- On BS to Off BS, true-up just the new revision deferred rent
                         WHEN L_ASSET_SCHEDULE_APPROVED.IS_OM = 0 and
                              L_ASSET_SCHEDULE_MONTH.IS_OM = 1 THEN
                              L_ASSET_SCHEDULE_MONTH.BEG_PREPAID_RENT
                            -- On BS to On BS, just pull whatever is there
                            ELSE
                              case when L_NEW_FASB_CAP_TYPE not in (3,6) then
                                L_ASSET_SCHEDULE_MONTH.BEG_PREPAID_RENT -
                                L_ASSET_SCHEDULE_APPROVED.BEG_PREPAID_RENT
                              else
                                0
                              end
                           END;

      pkg_pp_log.p_write_message('Prepaid rent amount: ' ||
                                 to_char(a_prepaid_adj));


	  if lb_om_to_cap and L_NEW_FASB_CAP_TYPE not in (3,6) then
				--For Transition/Forecast need to grab IDC and Incentive on transition
        select nvl(idc_math_amount, 0), nvl(incentive_math_amount, 0)
				  into a_idc_adj, a_inc_adj
				  from ls_asset_schedule s
				  WHERE s.ls_asset_id = a_ls_asset.ls_asset_id
				  AND s.revision = a_revision
				  AND s.MONTH = l_month
				  AND S.set_of_books_id = a_sob;

        pkg_pp_log.p_write_message('Initial Direct Cost Remeasurement: ' ||
                                   to_char(a_idc_adj));
        pkg_pp_log.p_write_message('Incentive Remeasurement: ' ||
                                   to_char(a_inc_adj));
		else
			a_idc_adj := 0;
			a_inc_adj := 0;
		end if;

     END IF;

	 --If we have Impairment Activity this month and it hasnt been flagged as a remeasurement then we need to adjust with Impairment JE CODE
    if NVL(L_ASSET_SCHEDULE_MONTH.IMPAIRMENT_ACTIVITY, 0) <> 0 THEN
		A_IMPAIRMENT_ADJ := L_ASSET_SCHEDULE_MONTH.IMPAIRMENT_ACTIVITY;
      pkg_pp_log.p_write_message('Impairment Activity: ' ||
                                 to_char(L_ASSET_SCHEDULE_MONTH.IMPAIRMENT_ACTIVITY));
	 else
		A_IMPAIRMENT_ADJ := 0;
	 END IF;

      L_MSG := 'Get the asset id';
      select A.ASSET_ID
        into A_ASSET_ID
        from (select ASSET_ID,
                     ROW_NUMBER() OVER(partition by LS_ASSET_ID order by EFFECTIVE_DATE desc) as THE_ROW
                from LS_CPR_ASSET_MAP
               where LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID) A
       where A.THE_ROW = 1;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_ADJUST_ASSET;

  function F_GET_DEPR_GROUP(A_LS_ASSET_ID   IN number,
                            A_LS_PEND_TRANS IN LS_PEND_TRANSACTION%rowtype,
                            A_GL_ACCT_ID    IN GL_ACCOUNT.GL_ACCOUNT_ID%type,
                            A_MONTH         IN date,
                            A_MAJOR_LOC     IN MAJOR_LOCATION.MAJOR_LOCATION_ID%type,
                            A_PROP_UNIT     IN PROPERTY_UNIT.PROPERTY_UNIT_ID%type,
                            A_LOC_TYPE      IN LOCATION_TYPE.LOCATION_TYPE_ID%type,
                            A_DG_ID         IN OUT DEPR_GROUP.DEPR_GROUP_ID%type)
    return varchar2 is
    L_DG_CC_IND varchar2(100);
    L_CC_ID     NUMBER;
    L_CC_VALUE  varchar2(100);
    L_SQLS      varchar2(2000);
    L_CHECK     number;
  begin
    --CJS 5/2/17 If this is an adjustment, we should not re-find depr group; if the control tree has changed, 
    --  it messes things up. only transfers/additions should trigger this
    --CJS Also need to change back to the original depr_group_id on ls_asset. 
    --  p_get_lease_depr changes it when saving schedule, but no good solution to prevent it
    if A_LS_PEND_TRANS.ACTIVITY_CODE = 11 THEN
      select DEPR_GROUP_ID
        into A_DG_ID
        from CPR_LEDGER CPR, LS_CPR_ASSET_MAP MAP
       where MAP.LS_ASSET_ID = A_LS_ASSET_ID
         and MAP.ASSET_ID = CPR.ASSET_ID;
    
      update LS_ASSET
         set DEPR_GROUP_ID = A_DG_ID
       where LS_ASSET_ID = A_LS_ASSET_ID;
    
    else
      L_DG_CC_IND := pkg_pp_system_control.f_pp_system_control('Lease Depr Group Class Code');
    
      if nvl(L_DG_CC_IND, 'none') = 'none' then
        L_CC_ID    := -1;
        L_CC_VALUE := 'NO CLASS CODE';
      else
        begin
          select CLASS_CODE_ID
            into L_CC_ID
            from CLASS_CODE
           where lower(trim(description)) =
                 pkg_pp_system_control.f_pp_system_control('DEPR GROUP CLASS CODE');
        
        exception
          when no_data_found then
            L_CC_ID := -1;
        end;
      
        begin
          L_SQLS := 'select value from ls_' || L_DG_CC_IND ||
                    '_class_code where class_code_id = ' || L_CC_ID;
          case
            when L_DG_CC_IND = 'mla' then
              L_SQLS := L_SQLS ||
                        ' and lease_id = (select lease_id from ls_ilr ilr, ls_asset la where ilr.ilr_id = la.ilr_id and la.ls_asset_id = ' ||
                        A_LS_ASSET_ID || ')';
            when L_DG_CC_IND = 'ilr' then
              L_SQLS := L_SQLS ||
                        ' and ilr_id = (select ilr_id from ls_asset where ls_asset_id = ' ||
                        A_LS_ASSET_ID || ')';
            when L_DG_CC_IND = 'asset' then
              L_SQLS := L_SQLS || ' and ls_asset_id = ' || A_LS_ASSET_ID;
          end case;
        
          EXECUTE IMMEDIATE L_SQLS
            into L_CC_VALUE;
        
        exception
          when no_data_found then
            L_CC_VALUE := 'NO CLASS CODE';
        end;
      end if;
    
      A_DG_ID := PP_DEPR_PKG.F_FIND_DEPR_GROUP(A_LS_PEND_TRANS.COMPANY_ID,
                                               A_GL_ACCT_ID,
                                               A_MAJOR_LOC,
                                               A_LS_PEND_TRANS.UTILITY_ACCOUNT_ID,
                                               A_LS_PEND_TRANS.BUS_SEGMENT_ID,
                                               A_LS_PEND_TRANS.SUB_ACCOUNT_ID,
                                               -100, --SUBLEDGER_TYPE_ID
                                               TO_NUMBER(TO_CHAR(NVL(A_LS_PEND_TRANS.IN_SERVICE_DATE,
                                                                     A_MONTH),
                                                                 'YYYY')),
                                               A_LS_PEND_TRANS.ASSET_LOCATION_ID,
                                               A_LOC_TYPE,
                                               A_LS_PEND_TRANS.RETIREMENT_UNIT_ID,
                                               A_PROP_UNIT,
                                               L_CC_ID, --A_CLASS_CODE_ID
                                               L_CC_VALUE); --A_CC_VALUE
      if A_DG_ID in (-1, -9) then
        -- we canot save... set error_message on ls_pend_transactions
        return 'ERROR: Finding Depreciation Group (' || TO_CHAR(NVL(A_LS_PEND_TRANS.COMPANY_ID,
                                                                    '')) || ' : ' || TO_CHAR(NVL(A_GL_ACCT_ID,
                                                                                                 '')) || ' : ' || TO_CHAR(NVL(A_MAJOR_LOC,
                                                                                                                              '')) || ' : ' || TO_CHAR(NVL(A_LS_PEND_TRANS.UTILITY_ACCOUNT_ID,
                                                                                                                                                           '')) || ' : ' || TO_CHAR(NVL(A_LS_PEND_TRANS.BUS_SEGMENT_ID,
                                                                                                                                                                                        '')) || ' : ' || TO_CHAR(NVL(A_LS_PEND_TRANS.SUB_ACCOUNT_ID,
                                                                                                                                                                                                                     '')) || ' : -100 : ' || TO_CHAR(NVL(TO_CHAR(NVL(A_LS_PEND_TRANS.IN_SERVICE_DATE,
                                                                                                                                                                                                                                                                     A_MONTH),
                                                                                                                                                                                                                                                                 'YYYY'),
                                                                                                                                                                                                                                                         '')) || ' : ' || TO_CHAR(NVL(A_LS_PEND_TRANS.ASSET_LOCATION_ID,
                                                                                                                                                                                                                                                                                      '')) || ' : ' || TO_CHAR(NVL(A_LOC_TYPE,
                                                                                                                                                                                                                                                                                                                   '')) || ' : ' || TO_CHAR(NVL(A_LS_PEND_TRANS.RETIREMENT_UNIT_ID,
                                                                                                                                                                                                                                                                                                                                                '')) || ' : ' || TO_CHAR(NVL(A_PROP_UNIT,
                                                                                                                                                                                                                                                                                                                                                                             ''));
      end if;
    
      -- check to make sure the depr group has subledger_type_id = -100
      L_CHECK := 0;
      select count(1)
        into L_CHECK
        from DEPR_GROUP
       where DEPR_GROUP_ID = A_DG_ID
         and SUBLEDGER_TYPE_ID = -100;
    
      if L_CHECK <> 1 then
        return 'ERROR: Depreciation Group is not a lease depreciation group.  Fix the depreciation group control tree: ' || TO_CHAR(A_DG_ID);
      end if;
    end if;
    
    return 'OK';
  end F_GET_DEPR_GROUP;

  procedure P_ADD_CPR_DEPR_UPDATES(A_ASSET_ID           IN number,
                                   A_INIT_LIFE          IN number,
                                   A_LS_PEND_TRANS      IN LS_PEND_TRANSACTION%rowtype,
                                   A_MONTH              IN date,
                                   A_ACCT_MONTH         IN date,
                                   A_REMEASUREMENT_DATE IN date,
                                   A_OM_TO_CAP         IN number,
                                   A_3033_COUNT         IN number,
								   A_SOB_ID    			IN number,
                                   A_MSG                IN OUT varchar2) is
    L_MSG       varchar2(2000);
  begin
    A_MSG := 'Performing CPR Depr Update Adds for Set of Books ID:' || A_SOB_ID;
    PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    
	A_MSG := 'Updating CPR Depr Init Life if before in service date';
    PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    -- Only update the life if the min open month aligns with the first month on the schedule
    -- since this update runs on remeasurement revisions and would update the first month's
    -- life values after the month is closed
    update cpr_depr
       set remaining_life = A_INIT_LIFE,
           init_life      = A_INIT_LIFE
     where asset_id = A_ASSET_ID
       and gl_posting_mo_yr <=
           (select min(payment_term_date)
              from ls_ilr_payment_term
             where A_LS_PEND_TRANS.ILR_ID = ILR_ID
               AND REVISION = A_LS_PEND_TRANS.REVISION)
       and gl_posting_mo_yr >= A_MONTH
	   and set_of_books_id = A_SOB_ID;
  
    PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG || ' Rows Updated: ' || sql%rowcount);
  

    A_MSG := 'OM to Cap Indicator: ' || To_Char(A_OM_TO_CAP) || ' For Set of Books ID:' || A_SOB_ID;
    PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
  
    --Remeasurement date = null and A_OM_TO_CAP = 1 === transition (or off-on adjustment)         
    --    transition: set init=remaining, no MPC (except subtract 1 for MPC <> an integer)
    --Remeasurement date = null and A_OM_TO_CAP = 0 === off-off adjustment or on-on adjustment
    --    init no change, remaining include MPC
    A_MSG := 'Updating CPR Depr Init Life for in service ILR non-remeasurement adjustment';
    PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    UPDATE cpr_depr
       SET remaining_life =
           (SELECT Count(month) - CASE
                     WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                          f.mid_period_conv NOT IN (0, 1) AND A_OM_TO_CAP = 0 THEN
                      mid_period_conv
                     WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                          f.mid_period_conv NOT IN (0, 1) AND A_OM_TO_CAP = 1 THEN
                      1
                     ELSE
                      0
                   end
              FROM ls_depr_forecast f
              JOIN ls_asset a ON a.ls_asset_id = f.ls_asset_id
              JOIN ls_ilr ilr ON a.ilr_id = ilr.ilr_id
              JOIN ls_ilr_options o ON o.ilr_id = a.ilr_id
                                   AND o.revision = f.revision
              JOIN ls_cpr_asset_map map ON map.ls_asset_id = a.ls_asset_id
             WHERE o.remeasurement_date IS NULL
               AND ilr.ilr_status_id = 2
               AND f.MONTH >= A_ACCT_MONTH
               AND o.ilr_id = A_LS_PEND_TRANS.ilr_id
               AND o.revision = A_LS_PEND_TRANS.revision
               AND depr_expense <> 0
               AND cpr_depr.set_of_books_id = f.set_of_books_id
			   AND cpr_depr.set_of_books_id = A_SOB_ID
               AND map.asset_id = A_ASSET_ID
               AND cpr_depr.asset_id = map.asset_id
               group by CASE
                     WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                             f.mid_period_conv NOT IN (0, 1) AND
                             A_OM_TO_CAP = 0 THEN
                      mid_period_conv
                     WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                             f.mid_period_conv NOT IN (0, 1) AND
                             A_OM_TO_CAP = 1 THEN
                      1
                     ELSE
                      0
                   end),
           init_life = case
                         when A_3033_COUNT > 0 then
                         -- Modified Retrospective Transitions should retain the init life determined by the asset
                          init_life
                         else
                          Decode(A_OM_TO_CAP,
                                 0,
                                 init_life,
                                 (SELECT Count(month) - CASE
                                           WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                                                f.mid_period_conv NOT IN (0, 1) AND
                                                A_OM_TO_CAP = 0 THEN
                                            f.mid_period_conv
                                           WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                                                f.mid_period_conv NOT IN (0, 1) AND
                                                A_OM_TO_CAP = 1 THEN
                                            1
                                           ELSE
                                            0
                                         end
                                    FROM ls_depr_forecast f
                                    JOIN ls_asset a ON a.ls_asset_id =
                                                       f.ls_asset_id
                                    JOIN ls_ilr ilr ON a.ilr_id = ilr.ilr_id
                                    JOIN ls_ilr_options o ON o.ilr_id =
                                                             a.ilr_id
                                                         AND o.revision =
                                                             f.revision
                                    JOIN ls_cpr_asset_map map ON map.ls_asset_id =
                                                                 a.ls_asset_id
                                   WHERE o.remeasurement_date IS NULL
                                     AND ilr.ilr_status_id = 2
                                     AND f.MONTH >= A_ACCT_MONTH
                                     AND o.ilr_id = A_LS_PEND_TRANS.ilr_id
                                     AND o.revision = A_LS_PEND_TRANS.revision
                                     AND depr_expense <> 0
                                     AND cpr_depr.set_of_books_id =
                                         f.set_of_books_id
								     AND cpr_depr.set_of_books_id = A_SOB_ID
                                     AND map.asset_id = A_ASSET_ID
                                     AND cpr_depr.asset_id = map.asset_id
                                     group by CASE
                                           WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                                                   f.mid_period_conv NOT IN
                                                   (0, 1) AND A_OM_TO_CAP = 0 THEN
                                            f.mid_period_conv
                                           WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                                                   f.mid_period_conv NOT IN
                                                   (0, 1) AND A_OM_TO_CAP = 1 THEN
                                            1
                                           ELSE
                                            0
                                         end))
                       end
     WHERE asset_id = A_ASSET_ID
       AND gl_posting_mo_yr = A_ACCT_MONTH
       AND EXISTS
     (SELECT 1
              FROM ls_depr_forecast f
              JOIN ls_asset a ON a.ls_asset_id = f.ls_asset_id
              JOIN ls_ilr ilr ON a.ilr_id = ilr.ilr_id
              JOIN ls_ilr_options o ON o.ilr_id = a.ilr_id
                                   AND o.revision = f.revision
              JOIN ls_cpr_asset_map map ON map.ls_asset_id = a.ls_asset_id
             WHERE o.remeasurement_date IS NULL
               AND ilr.ilr_status_id = 2
               AND f.MONTH >= A_ACCT_MONTH
               AND o.ilr_id = A_LS_PEND_TRANS.ilr_id
               AND o.revision = A_LS_PEND_TRANS.revision
               AND depr_expense <> 0
               AND cpr_depr.set_of_books_id = f.set_of_books_id
			   AND cpr_depr.set_of_books_id = A_SOB_ID
               AND map.asset_id = A_ASSET_ID
               AND cpr_depr.asset_id = map.asset_id);
  
    PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG || ' Rows Updated: ' || sql%rowcount);
  
    -- For remeasurements, update the cpr_depr remaining life to the new remaining depreciable life
    -- Determine the new remaining depreciable life by looking at months with depreciation in ls_depr_forecast
    -- Only update if the remeasurment date is before (backdated) or equal to the current month. 
    --   Remeasurement after the current month (future dated) is not valid.
    IF A_REMEASUREMENT_DATE <= A_ACCT_MONTH THEN
      A_MSG := 'Updating CPR Depr Init Life for remeasurement';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    
      --Remeasurement date = not null and A_OM_TO_CAP = 1 === off to on remeasurement       
      --        set init=remaining, no MPC (except subtract 1 for MPC <> an integer)
      --Remeasurement date = not null and A_OM_TO_CAP = 0 === off-off or on-on remeasurement
      --        init no change, remaining include MPC
      --Remeasurement date = not null and A_OM_TO_CAP = 0 and current cap type is cap = on to on remeasurement 
      -- init no change, remaining no change. Changes happen in following month end depr calc
      UPDATE cpr_depr
         SET remaining_life =
             (SELECT Count(month) - CASE
                       WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                            f.mid_period_conv NOT IN (0, 1) AND
                            A_OM_TO_CAP = 0 THEN
                        f.mid_period_conv
                       WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                            f.mid_period_conv NOT IN (0, 1) AND
                            A_OM_TO_CAP = 1 THEN
                        1
                       ELSE
                        0
                     end
                FROM ls_depr_forecast f
                JOIN ls_asset a ON a.ls_asset_id = f.ls_asset_id
                JOIN ls_ilr_options o ON o.ilr_id = a.ilr_id
                                     AND o.revision = f.revision
                JOIN ls_cpr_asset_map map ON map.ls_asset_id = a.ls_asset_id
               WHERE o.remeasurement_date IS NOT NULL
                 AND f.MONTH >= A_ACCT_MONTH
                 AND o.ilr_id = A_LS_PEND_TRANS.ilr_id
                 AND o.revision = A_LS_PEND_TRANS.revision
                 AND depr_expense <> 0
                 AND cpr_depr.set_of_books_id = f.set_of_books_id
				 AND cpr_depr.set_of_books_id = A_SOB_ID
                 AND map.asset_id = A_ASSET_ID
                 AND cpr_depr.asset_id = map.asset_id
               group by CASE
                       WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                               f.mid_period_conv NOT IN (0, 1) AND
                               A_OM_TO_CAP = 0 THEN
                        f.mid_period_conv
                       WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                               f.mid_period_conv NOT IN (0, 1) AND
                               A_OM_TO_CAP = 1 THEN
                        1
                       ELSE
                        0
                     end),
             init_life      = Decode(A_OM_TO_CAP,
                                     0,
                                     init_life,
                                     (SELECT Count(month) - CASE
                                               WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                                                    mid_period_conv NOT IN
                                                    (0, 1) AND A_OM_TO_CAP = 0 THEN
                                                f.mid_period_conv
                                               WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                                                    f.mid_period_conv NOT IN
                                                    (0, 1) AND A_OM_TO_CAP = 1 THEN
                                                1
                                               ELSE
                                                0
                                             end
                                        FROM ls_depr_forecast f
                                        JOIN ls_asset a ON a.ls_asset_id =
                                                           f.ls_asset_id
                                        JOIN ls_ilr_options o ON o.ilr_id =
                                                                 a.ilr_id
                                                             AND o.revision =
                                                                 f.revision
                                        JOIN ls_cpr_asset_map map ON map.ls_asset_id =
                                                                     a.ls_asset_id
                                       WHERE o.remeasurement_date IS NOT NULL
                                         AND f.MONTH >= A_ACCT_MONTH
                                         AND o.ilr_id = A_LS_PEND_TRANS.ilr_id
                                         AND o.revision =
                                             A_LS_PEND_TRANS.revision
                                         AND depr_expense <> 0
                                         AND cpr_depr.set_of_books_id =
                                             f.set_of_books_id
									     AND cpr_depr.set_of_books_id = A_SOB_ID
                                         AND map.asset_id = A_ASSET_ID
                                         AND cpr_depr.asset_id = map.asset_id
                                       group by CASE
                                               WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                                                    mid_period_conv NOT IN
                                                       (0, 1) AND
                                                       A_OM_TO_CAP = 0 THEN
                                                f.mid_period_conv
                                               WHEN Lower(Trim(f.mid_period_method)) = 'sl' AND
                                                    f.mid_period_conv NOT IN
                                                       (0, 1) AND
                                                       A_OM_TO_CAP = 1 THEN
                                                1
                                               ELSE
                                                0
                                             end))
       WHERE asset_id = A_ASSET_ID
         AND gl_posting_mo_yr = A_ACCT_MONTH
         AND EXISTS
       (SELECT 1
                FROM ls_depr_forecast f
                JOIN ls_asset a ON a.ls_asset_id = f.ls_asset_id
                JOIN ls_ilr_options o ON o.ilr_id = a.ilr_id
                                     AND o.revision = f.revision
                JOIN ls_cpr_asset_map map ON map.ls_asset_id = a.ls_asset_id
                JOIN ls_fasb_cap_type_sob_map fmap ON fmap.lease_cap_type_id =
                                                      o.lease_cap_type_id
                                                  AND fmap.set_of_books_id =
                                                      f.set_of_books_id
               WHERE o.remeasurement_date IS NOT NULL
                 AND f.MONTH >= A_ACCT_MONTH
                 AND o.ilr_id = A_LS_PEND_TRANS.ilr_id
                 AND o.revision = A_LS_PEND_TRANS.revision
                 AND depr_expense <> 0
                 AND cpr_depr.set_of_books_id = f.set_of_books_id
				 AND cpr_depr.set_of_books_id = A_SOB_ID
                 AND map.asset_id = A_ASSET_ID
                 AND cpr_depr.asset_id = map.asset_id
                 AND CASE
                       WHEN A_OM_TO_CAP = 0 AND fasb_cap_type_id IN (1, 2, 3, 6) AND
                            f.mid_period_conv = 0 --Filter out On to On Remeasurement with MPC = 0
                        THEN
                        0
                       ELSE
                        1
                     END = 1);
    
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG || ' Rows updated: ' ||
                                 sql%rowcount);
    
      --Save off Life for On to On Remeasurements for Depr Month End to pick it up
      A_MSG := 'Saving off Remaining and Init Life for On BS to On BS remeasurement with MPC = 0 for following Month End';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    
      --Clear out any pre-existing records that may be in here for this asset for whatever reason (remeasuring twice in the same month?)
      DELETE FROM ls_mpc_asset_remeasure_life
       WHERE asset_id = A_ASSET_ID
         AND gl_posting_mo_yr = Add_Months(A_ACCT_MONTH, 1)
         AND set_of_books_id = A_SOB_ID;
    
      -- Use A_ACCT_MONTH rather than the remeasurement date when the remeasurement_date + 1 is less than
      -- the accounting month
      INSERT INTO ls_mpc_asset_remeasure_life
        (asset_id, set_of_books_id, init_life, remaining_life,
         gl_posting_mo_yr)
      select asset_id,
             set_of_books_id,
             init_life,
             case when add_months(remeasurement_date, 1) < A_ACCT_MONTH then
               remaining_life + 1
             else
               remaining_life
             end remaining_life,
             case when add_months(remeasurement_date, 1) < A_ACCT_MONTH then
               A_ACCT_MONTH
             else
               add_months(remeasurement_date, 1)
             end gl_posting_mo_yr
        from (
          SELECT map.asset_id asset_id,
                 f.set_of_books_id set_of_books_id,
                 d.init_life init_life,
                 COUNT(1) remaining_life,
                 trunc(o.remeasurement_date,'month') remeasurement_date
          FROM ls_depr_forecast f
          JOIN ls_asset a ON a.ls_asset_id = f.ls_asset_id
		  AND f.set_of_books_id = A_SOB_ID
          JOIN ls_ilr_options o ON o.ilr_id = a.ilr_id
                               AND o.revision = f.revision
          JOIN ls_fasb_cap_type_sob_map fmap ON fmap.lease_cap_type_id =
                                                o.lease_cap_type_id
                                             AND fmap.set_of_books_id =
                                                f.set_of_books_id
											 AND fmap.set_of_books_id = A_SOB_ID
          JOIN ls_cpr_asset_map map ON map.ls_asset_id = a.ls_asset_id
            JOIN (select ls_asset_id,
                         revision,
                         set_of_books_id,
                       count(*) - 1 init_life
                  from ls_depr_forecast
                 group by ls_asset_id, revision, set_of_books_id) d ON d.ls_asset_id =
                                                                       map.ls_asset_id
                                                                   AND d.revision =
                                                                       o.revision
                                                                   AND d.set_of_books_id =
                                                                       f.set_of_books_id
																   AND d.set_of_books_id = A_SOB_ID
         WHERE o.remeasurement_date IS NOT NULL
           AND f.month >= add_months(A_ACCT_MONTH, 1)
           AND o.ilr_id = A_LS_PEND_TRANS.ilr_id
           AND o.revision = A_LS_PEND_TRANS.revision
           AND depr_expense <> 0
           AND map.asset_id = A_ASSET_ID
           AND fmap.fasb_cap_type_id IN (1, 2, 3, 6)
           AND A_OM_TO_CAP = 0
           GROUP BY map.asset_id,
                    f.set_of_books_id,
                    d.init_life,
                   trunc(o.remeasurement_date,'month'));
    
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG ||
                                 ' Rows saved off for Following Month End: ' ||
                                 sql%rowcount);
    
    END IF;
                
  end P_ADD_CPR_DEPR_UPDATES;
  
   -- Function to perform specific Leased Asset addition
   function F_ADD_ASSET(A_LS_ASSET_ID number) return varchar2 is
    L_MSG                    varchar2(2000);
    L_DG_ID                  DEPR_GROUP.DEPR_GROUP_ID%type;
    L_LS_ASSET               LS_ASSET%rowtype;
    L_LS_PEND_TRANS          LS_PEND_TRANSACTION%rowtype;
    L_ASSET_SCHEDULE         LS_ASSET_SCHEDULE%rowtype;
    L_MAJOR_LOC              MAJOR_LOCATION.MAJOR_LOCATION_ID%type;
    L_PROP_UNIT              PROPERTY_UNIT.PROPERTY_UNIT_ID%type;
    L_LOC_TYPE               LOCATION_TYPE.LOCATION_TYPE_ID%type;
    L_ASSET_ID               CPR_LEDGER.ASSET_ID%type;
    L_SECOND_COST            CPR_LEDGER.SECOND_FINANCIAL_COST%type;
    L_GL_ACCT_ID             GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_ST_ACCT_ID             GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_LT_ACCT_ID             GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_AP_ACCT_ID             GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_RES_ACCT_ID            GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_ST_DEF_ACCT_ID         GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_LT_DEF_ACCT_ID         GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_INTACC_ACCT_ID         GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_PREPAID_RENT_ACCT_ID   GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_ASSET_ACT_ID           number;
    L_ACT_CODE               CPR_ACTIVITY.ACTIVITY_CODE%type;
    L_FERC_ACT_CODE          CPR_ACTIVITY.FERC_ACTIVITY_CODE%type;
    L_ADJ                    number;
    L_RTN                    number;
    L_OBL_ADJ                number;
    L_LT_OBL_ADJ             number;
    L_DEF_ADJ                number;
    L_LT_DEF_ADJ             number;
    L_PREPAID_ADJ            NUMBER;
    L_RESERVE_ADJ            NUMBER;
    L_IDC_ADJ                number;
    L_INC_ADJ                number;
    L_IDC_MATH_AMOUNT        number;
    L_INCENTIVE_MATH_AMOUNT  NUMBER;
    L_INIT_LIFE              number;
    L_MONTH                  date;
    L_ACCT_MONTH             date;
    L_MID_PERIOD_METHOD      varchar2(50);
    L_MAX_MONTH              date;
    L_IS_OM                  number(1, 0);
    L_CAP_TYPE               NUMBER(22, 0);
    L_FASB_CHECK             NUMBER(22, 0);
    L_APPROVED_RATE          number;
    L_CURRENCY_FROM          number;
    L_CURRENCY_TO            number;
    l_earnings_count         NUMBER;
    l_earnings_amount        NUMBER;
    l_earnings_acct_id       gl_account.gl_account_id%TYPE;
    L_IDC_ACCT_ID            gl_account.gl_account_id%TYPE;
    L_INC_ACCT_ID            gl_account.gl_account_id%TYPE;
    L_INT_EXP_ACCT_ID        gl_account.gl_account_id%TYPE;
    L_BACKDATE_INC_AMT       NUMBER;
    L_REMEASUREMENT          number;
    L_REMEASUREMENT_DATE     DATE;
    L_IS_REMEASURE           NUMBER;
    L_PRE_PAYMENT_SW         number;
    LB_OM_TO_CAP             BOOLEAN;
    L_PARTIAL_TERMINATION    number(1) := 0;
	  l_partial_term_gain_loss number;
    L_REMEASUREMENT_TYPE     NUMBER(1) := 0;
    L_ADDITIONAL_ROU_ASSET   NUMBER;
    L_OM_TO_CAP              NUMBER;
    L_UNACCRUED_ADJ          number;
    L_3033_AMT               number;
    L_3033_COUNT             number := 0;
    L_APPROVED_REV_IS_OM     number;
    L_LEASE_ID               NUMBER;
    L_PAYMENT_SHIFT          NUMBER;
    L_IMP_ACCUMACC           gl_account.gl_account_id%TYPE;
    L_IMP_EXPACC             gl_account.gl_account_id%TYPE;
    L_IMP_REVACC             gl_account.gl_account_id%TYPE;
    L_IMPAIRMENT_ACTIVITY    number(22, 2);
    L_CURR_PRIN_PAID         NUMBER(22,2);
    L_APPR_PRIN_PAID         NUMBER(22,2);
    L_BACKDATE_INT_ACCRUED   ls_asset_schedule.interest_accrual%type;
    L_BACKDATE_INT_PAID      ls_asset_schedule.interest_paid%type;
    L_PRIOR_REVISION         NUMBER;
    L_NEW_REVISION           NUMBER;
	  L_IS_IMPAIRMENT          NUMBER := 0;
    L_ST_FX_GL               NUMBER(22,2);
    L_LT_FX_GL               NUMBER(22,2);
    L_ST_FX_GL_DIFF          NUMBER(22,2);
    L_LT_FX_GL_DIFF          NUMBER(22,2);
    L_FX_GAIN_LOSS_DR_ACCT_ID gl_account.gl_account_id%TYPE;
    L_FX_GAIN_LOSS_CR_ACCT_ID gl_account.gl_account_id%TYPE;
	L_CONTRACT_JES  varchar2(100);
	L_JE_METHOD_SOB_CHECK number;
   begin
      L_MSG := 'Starting to add leased asset';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      -- ls_asset
    select *
      into L_LS_ASSET
      from LS_ASSET
     where LS_ASSET_ID = A_LS_ASSET_ID;

      -- ls_pend_transaction
    select *
      into L_LS_PEND_TRANS
      from LS_PEND_TRANSACTION
     where LS_ASSET_ID = A_LS_ASSET_ID;

   -- Save off the prior approved revision and new revision
    L_PRIOR_REVISION := L_LS_ASSET.APPROVED_REVISION;
    L_NEW_REVISION := L_LS_PEND_TRANS.REVISION;
  
    L_MSG := 'Retrieving the current open month for lessee';
	PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
    select greatest(min(LPC.GL_POSTING_MO_YR), min(LAS.MONTH)),
           min(LPC.GL_POSTING_MO_YR)
        into L_MONTH, L_ACCT_MONTH
        from LS_PROCESS_CONTROL LPC, LS_ASSET_SCHEDULE LAS
       where LPC.LAM_CLOSED is null
         and LPC.COMPANY_ID = L_LS_ASSET.COMPANY_ID
     and LAS.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
     and LAS.REVISION = L_NEW_REVISION;

    /* WMD */
    if L_LS_PEND_TRANS.GL_POSTING_MO_YR is not null then
      L_ACCT_MONTH := L_LS_PEND_TRANS.GL_POSTING_MO_YR;
    end if;

    /* CJS 4/2/15 Checking to make sure asset schedule not already run out; Would never get picked up for retirement */
    L_MSG := 'Checking for greatest month on asset schedule';
    select max(LAS.MONTH)
      into L_MAX_MONTH
      from LS_ASSET_SCHEDULE LAS
    where LAS.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
      and LAS.REVISION = L_NEW_REVISION;

    if L_MAX_MONTH < L_ACCT_MONTH then
      return 'Error: Asset schedule expired before current open month. Cannot add an already retired asset.';
    end if;
	
	PKG_PP_LOG.P_WRITE_MESSAGE('Getting Contract Currency JE System Control');
	L_CONTRACT_JES:=lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_company('Lease: Contract Currency JEs', L_LS_ASSET.COMPANY_ID)));
	  
    
	if L_CONTRACT_JES IS NULL then
		   L_CONTRACT_JES := 'no';
	end if;
	  
    PKG_PP_LOG.P_WRITE_MESSAGE('Contract Currency JE System Control:' || L_CONTRACT_JES);
     
	if L_CONTRACT_JES = 'yes' then
		L_MSG := 'Checking JE Method Set of Books' ;
		PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
		L_JE_METHOD_SOB_CHECK := PKG_LEASE_COMMON.F_JE_METHOD_SOB_CHECK(L_MSG);
		
		if L_JE_METHOD_SOB_CHECK > 0 then
			  L_MSG:='You cannot book contract currency JEs with more than one set of books configured per JE Method';
			  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
			  return -1;
		end if;
	end if;

    l_init_life := PKG_LEASE_COMMON.F_GET_INIT_LIFE(A_LS_ASSET_ID, L_NEW_REVISION, L_LS_ASSET.ilr_id, L_LS_ASSET.economic_life)
                   .init_life;
  
      -- adding an asset if the count returns 0 (meaning it exists in the CPR) and there is an approved revision
      if L_LS_PEND_TRANS.ACTIVITY_CODE = 11 then
         L_ACT_CODE      := 'UADJ';
         L_ADJ           := 1;
         L_FERC_ACT_CODE := 3;

         -- this is an ADJUSTMENT
         -- figure out the adjustment amounts.
         L_MSG := F_ADJUST_ASSET(L_LS_ASSET,
                                 L_PRIOR_REVISION,
                                 L_NEW_REVISION,
                                 1,
                                 L_OBL_ADJ,
                                 L_LT_OBL_ADJ,
                                 L_DEF_ADJ,
                                 L_LT_DEF_ADJ,
                                 L_PREPAID_ADJ,
								                 L_IDC_ADJ,
								                 L_INC_ADJ,
                                 L_UNACCRUED_ADJ,
								                 L_IMPAIRMENT_ACTIVITY,
                                 L_ASSET_ID,
                                 LB_OM_TO_CAP,
                                 L_LS_PEND_TRANS.GL_POSTING_MO_YR);
         if L_MSG <> 'OK' then
            return L_MSG;
         end if;

         L_LS_PEND_TRANS.POSTING_QUANTITY := 0;
      else
         --Generate asset id
         --Hopefully use something marginally better than pwrplant1
		 pkg_pp_log.p_write_message('retrieve new asset id ');
         select PWRPLANT1.NEXTVAL into L_ASSET_ID from DUAL;

      L_ACT_CODE            := 'UADD';
      L_ADJ                 := 0;
      L_FERC_ACT_CODE       := 1;
      L_ASSET_ACT_ID        := 1;
      L_DEF_ADJ             := 0;
      L_LT_DEF_ADJ          := 0;
      L_PREPAID_ADJ         := 0;
      L_IDC_ADJ             := 0;
      L_INC_ADJ             := 0;
      L_IMPAIRMENT_ACTIVITY := 0;
	  end if;

      -- gl_account
      /* WMD */
    L_MSG := 'Retrieving GL Accounts';
	    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
    select IA.CAP_ASSET_ACCOUNT_ID,
           IA.ST_OBLIG_ACCOUNT_ID,
           IA.LT_OBLIG_ACCOUNT_ID,
           IA.AP_ACCOUNT_ID,
           IA.ST_DEFERRED_ACCOUNT_ID,
           IA.LT_DEFERRED_ACCOUNT_ID,
           IA.INT_ACCRUAL_ACCOUNT_ID,
           IA.PREPAID_RENT_ACCOUNT_ID,
           IA.INIT_DIRECT_COST_ACCOUNT_ID,
           IA.INCENTIVE_ACCOUNT_ID,
           IA.INT_EXPENSE_ACCOUNT_ID,
           IA.IMPAIR_EXPENSE_ACCOUNT_ID,
           IA.IMPAIR_REVERSAL_ACCOUNT_ID,
           IA.IMPAIR_ACCUM_AMORT_ACCOUNT_ID,
           IA.CURRENCY_GAIN_LOSS_DR_ACCT_ID,
           IA.CURRENCY_GAIN_LOSS_CR_ACCT_ID
      into L_GL_ACCT_ID,
           L_ST_ACCT_ID,
           L_LT_ACCT_ID,
           L_AP_ACCT_ID,
           L_ST_DEF_ACCT_ID,
           L_LT_DEF_ACCT_ID,
           L_INTACC_ACCT_ID,
           L_PREPAID_RENT_ACCT_ID,
           L_IDC_ACCT_ID,
           L_INC_ACCT_ID,
           L_INT_EXP_ACCT_ID,
           L_IMP_EXPACC,
           L_IMP_REVACC,
           L_IMP_ACCUMACC,
           L_FX_GAIN_LOSS_DR_ACCT_ID,
           L_FX_GAIN_LOSS_CR_ACCT_ID
        from LS_ILR_ACCOUNT IA, LS_ASSET LA

       where LA.ILR_ID = IA.ILR_ID
         and LA.LS_ASSET_ID = A_LS_ASSET_ID;

      -- major_location
      select MAJOR_LOCATION_ID
        into L_MAJOR_LOC
        from ASSET_LOCATION
       where ASSET_LOCATION_ID = L_LS_ASSET.ASSET_LOCATION_ID;

      -- location_type
      select LOCATION_TYPE_ID
        into L_LOC_TYPE
        from MAJOR_LOCATION
       where MAJOR_LOCATION_ID = L_MAJOR_LOC;

      -- property_unit
      select PROPERTY_UNIT_ID
        into L_PROP_UNIT
        from RETIREMENT_UNIT
       where RETIREMENT_UNIT_ID = L_LS_PEND_TRANS.RETIREMENT_UNIT_ID;

    L_MSG := 'Finding Depreciation Group';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
    L_MSG := F_GET_DEPR_GROUP(A_LS_ASSET_ID,
                              L_LS_PEND_TRANS,
                                                L_GL_ACCT_ID,
                              L_MONTH,
                                                L_MAJOR_LOC,
                                                L_PROP_UNIT,
                              L_LOC_TYPE,
                              L_DG_ID);

    if L_MSG <> 'OK' then
      return L_MSG;
        end if;

    -- Reserve Account
      SELECT RESERVE_ACCT_ID
      INTO L_RES_ACCT_ID
      FROM DEPR_GROUP
      WHERE DEPR_GROUP_ID = L_DG_ID;
      L_MSG          := 'Get the next activity id';
    L_ASSET_ACT_ID := PP_CPR_PKG.F_GET_ASSET_ACTIVITY_ID(L_ASSET_ID,
                                                         L_DG_ID);

    -- Mid Period Method                                                     
      select UPPER(trim(MID_PERIOD_METHOD))
      into L_MID_PERIOD_METHOD
      from DEPR_GROUP
      where DEPR_GROUP_ID = L_DG_ID;

	  --call to new function to extend BPO schedules out 1 extra month if mid period conv on the DG found is <> 1
    L_MSG := PKG_LEASE_SCHEDULE.F_BPO_SCHEDULE_EXTEND(L_LS_ASSET.LS_ASSET_ID,
                                                      L_LS_ASSET.ILR_ID,
                                                      L_NEW_REVISION);

       IF L_MSG <> 'OK' THEN
      PKG_PP_LOG.P_WRITE_MESSAGE('Error extending schedules for LS_ASSET_ID: ' ||
                                 L_LS_ASSET.LS_ASSET_ID || ' ILR_ID: ' ||
                                 L_LS_ASSET.ILR_ID || ' Revision: ' ||
                                 L_NEW_REVISION);
         ROLLBACK;
       END IF;

    L_MSG := 'Checking if operating or capital, pulling cap type';
    select ilro.lease_cap_type_id, Nvl(ilro.payment_shift, 0)
         into L_CAP_TYPE, L_PAYMENT_SHIFT
         from ls_ilr_options ilro
         where ilro.ilr_id = L_LS_PEND_TRANS.ILR_ID
          and ilro.revision = L_NEW_REVISION;

    for L_SOBS in (select PB.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                          PB.POSTING_AMOUNT  as AMOUNT
                       from LS_PEND_SET_OF_BOOKS PB
                    where PB.LS_PEND_TRANS_ID =
                          L_LS_PEND_TRANS.LS_PEND_TRANS_ID)
    loop
         --get the asset_schedule for the first month
         begin
            select A.*
            into L_ASSET_SCHEDULE
            from LS_ASSET_SCHEDULE A
            where A.LS_ASSET_ID = A_LS_ASSET_ID
            and A.REVISION = L_NEW_REVISION
            and A.SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID
            and A.MONTH = L_MONTH;

         exception
            when NO_DATA_FOUND then
               continue;
         end;

      --CJS 3/6/17 if it's SLE on a different SOB, need to account for it here
      L_MSG := 'Pulling FASB Cap Type';
	  begin
      SELECT FASB_CAP_TYPE_ID
      INTO L_FASB_CHECK
      FROM LS_FASB_CAP_TYPE_SOB_MAP
      WHERE LEASE_CAP_TYPE_ID = L_CAP_TYPE
      AND SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID;

      exception
         when NO_DATA_FOUND then
            continue;
      end;

      L_IS_OM := CASE
                   WHEN L_FASB_CHECK IN (4, 5) THEN
                    1
                   ELSE
                    0
                 END;

      -- Convert the depr expense to company currency for all mid period methods except for units of production
      L_MSG := PKG_LEASE_DEPR.F_CALC_COMPANY_EXPENSE(A_LS_ASSET_ID,
                                                       L_NEW_REVISION,
                                                       L_ASSET_ID,
                                                       L_SOBS.SET_OF_BOOKS_ID,
                                                       L_LS_PEND_TRANS.IN_SERVICE_DATE);
      
      if L_MSG <> 'OK' then
        return L_MSG;
      end if;

      if L_ADJ = 1 then
        -- this is an ADJUSTMENT
        -- figure out the adjustment amounts.
        L_MSG := F_ADJUST_ASSET(L_LS_ASSET,
                                L_PRIOR_REVISION,
                                L_NEW_REVISION,
                                L_SOBS.SET_OF_BOOKS_ID,
                                L_OBL_ADJ,
                                L_LT_OBL_ADJ,
                                L_DEF_ADJ,
                                L_LT_DEF_ADJ,
                                L_PREPAID_ADJ,
                                L_IDC_ADJ,
                                L_INC_ADJ,
                                L_UNACCRUED_ADJ,
                                L_IMPAIRMENT_ACTIVITY,
                                L_ASSET_ID,
                                LB_OM_TO_CAP,
                                L_LS_PEND_TRANS.GL_POSTING_MO_YR);
        if L_MSG <> 'OK' then
           return L_MSG;
        end if;

        select count(1)
        into l_is_remeasure
              FROM ls_ilr_options o
              WHERE o.ilr_id = L_LS_PEND_TRANS.ILR_ID
              AND o.revision = L_NEW_REVISION
              AND o.remeasurement_date IS NOT NULL;
			  
			  
		select nvl(o.is_impairment, 0)
		into l_is_impairment
		from ls_ilr_options o
		  WHERE o.ilr_id = L_LS_PEND_TRANS.ILR_ID
		  AND o.revision = L_NEW_REVISION;

        if l_is_remeasure > 0 then
            SELECT Decode(Trunc(o.remeasurement_date, 'month'),
                           L_ACCT_MONTH,
                           1,
                           0),
                   Trunc(remeasurement_date, 'month') remeasurement_date,
                   nvl(partial_termination, 0) partial_termination,
                   nvl(remeasurement_type, 0) remeasurement_type
            INTO L_REMEASUREMENT,
                 L_REMEASUREMENT_DATE,
                 L_PARTIAL_TERMINATION,
                 L_REMEASUREMENT_TYPE
          FROM ls_ilr_options o
          WHERE o.ilr_id = L_LS_PEND_TRANS.ILR_ID
          AND o.revision = L_NEW_REVISION
          AND o.remeasurement_date IS NOT NULL;
        end if;

       select is_om
         into L_APPROVED_REV_IS_OM
         from ls_asset_schedule s
         join ls_asset a on a.ls_asset_id = s.ls_asset_id
        where s.ls_asset_id = A_LS_ASSET_ID
          and s.revision = L_PRIOR_REVISION
          and s.set_of_books_Id = L_SOBS.SET_OF_BOOKS_ID
          and s.month = L_MONTH;

        IF L_REMEASUREMENT IS NULL THEN
          L_REMEASUREMENT := 0;
        END IF;

        IF L_REMEASUREMENT = 0 AND L_REMEASUREMENT_DATE IS NULL then
          L_ASSET_SCHEDULE.BEG_OBLIGATION := L_SOBS.AMOUNT;
              -- Don't zero out BEG_LT_OBLIGATION if we're moving from off to on bs
          if not (L_APPROVED_REV_IS_OM = 1 and L_ASSET_SCHEDULE.IS_OM = 0) THEN
            L_ASSET_SCHEDULE.BEG_LT_OBLIGATION := 0;
              end if;
            end if;
         end if;

--         CJS 3/6/17 Move this up above to be used in depr check
--         L_MSG:='Checking if operating or capital';
--         select lct.is_om
--         into L_IS_OM
--         from ls_lease_cap_type lct, ls_ilr_options ilro
--         where ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
--          and ilro.ilr_id = L_LS_PEND_TRANS.ILR_ID
--          and ilro.revision = L_LS_PEND_TRANS.REVISION;

          -- CJS set the approved revision here; was setting it too late such that wrong lease cap type is used for JE exclusions
          update LS_ASSET
         set APPROVED_REVISION  = L_NEW_REVISION,
             IN_SERVICE_DATE    = NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE,
                                      L_MONTH),
             LS_ASSET_STATUS_ID = 3,
             EXPECTED_LIFE      = L_INIT_LIFE
          where LS_ASSET_ID = A_LS_ASSET_ID;

         -- Process by set of books
         -- process JEs 3001, 3002, and 3003

          -- L_CURRENCY_FROM
      L_MSG := 'Retrieving L_CURRENCY_FROM';
          SELECT contract_currency_id, l.pre_payment_sw, l.lease_id
          INTO L_CURRENCY_FROM, L_PRE_PAYMENT_SW, L_LEASE_ID
          FROM ls_lease l, ls_ilr i
          WHERE l.lease_id = i.lease_id
            AND i.ilr_id = L_LS_PEND_TRANS.ILR_ID;

            -- L_CURRENCY_TO
      L_MSG := 'Retrieving L_CURRENCY_TO';
          SELECT currency_id
          INTO L_CURRENCY_TO
          FROM ls_ilr ilr, currency_schema cs
          WHERE ilr.company_id = cs.company_id
            AND currency_type_id = 1
            AND ilr_id = L_LS_PEND_TRANS.ILR_ID;

          -- L_APPROVED_RATE
      L_MSG := 'Retrieving L_APPROVED_RATE';
          SELECT in_service_exchange_rate
          INTO L_APPROVED_RATE
          FROM ls_ilr_options ilro
          WHERE ilr_id = L_LS_PEND_TRANS.ILR_ID
            AND revision = L_NEW_REVISION;

		--FYI 0 = CREDIT 1 = DEBIT

         if L_IS_OM <> 1 and G_SEND_JES then
			if l_is_remeasure = 1 and l_partial_termination = 1 then
				-- Remeasurement with Partial Termination
				L_MSG := 'Get the CPR asset id';
				PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				select A.ASSET_ID
					into L_ASSET_ID
				from (select ASSET_ID,
								ROW_NUMBER() OVER(partition by LS_ASSET_ID order by EFFECTIVE_DATE desc) as THE_ROW
					  from LS_CPR_ASSET_MAP
					  where LS_ASSET_ID = A_LS_ASSET_ID) A
				where A.THE_ROW = 1;
				L_MSG := 'Creating Journal (Leased Asset Retirement Credit)';
				PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
												   3026,
												   -1 * L_SOBS.AMOUNT, -- ROU_ASSET_REMEASUREMENT
												   L_ASSET_ACT_ID,
												   L_DG_ID,
												   L_LS_PEND_TRANS.WORK_ORDER_ID,
												   L_GL_ACCT_ID,
												   0,
												   L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
												   L_LS_PEND_TRANS.COMPANY_ID,
												   L_ACCT_MONTH,
												   0,
												   L_LS_PEND_TRANS.GL_JE_CODE,
												   L_SOBS.SET_OF_BOOKS_ID,
												   L_APPROVED_RATE,
												   L_CURRENCY_FROM,
												   L_CURRENCY_TO,
												   NULL, /* A_ORIG */
												   L_ASSET_ID, /* A_POSTING_ASSET_ID */
												   L_MSG);
				if L_RTN = -1 then
				   return L_MSG;
				end if;

				L_MSG := 'Creating Journal (Leased Asset ST Gain/Loss Debit)';
          if case when L_ADJ = 1 then L_OBL_ADJ else
           L_ASSET_SCHEDULE.BEG_OBLIGATION -
             NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0) end <> 0 then
            /* WMD */
				   PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				   L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													  3028,
                                                  -1 * L_OBL_ADJ,
													  L_ASSET_ACT_ID,
													  L_DG_ID,
													  L_LS_PEND_TRANS.WORK_ORDER_ID,
													  L_ST_ACCT_ID,
													  0,
													  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													  L_LS_PEND_TRANS.COMPANY_ID,
													  L_ACCT_MONTH,
													  1,
													  L_LS_PEND_TRANS.GL_JE_CODE,
													  L_SOBS.SET_OF_BOOKS_ID,
													  L_APPROVED_RATE,
													  L_CURRENCY_FROM,
													  L_CURRENCY_TO,
                            NULL, /* A_ORIG */
													  L_MSG);
				   if L_RTN = -1 then
					  return L_MSG;
				   end if;
				end if; -- short term JE

				L_MSG := 'Creating Journal (Leased Asset LT Gain/Loss Debit)';
          if case when L_ADJ = 1 then L_LT_OBL_ADJ else
           NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0) end <> 0 then
            /* WMD */
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				   L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													  3029,
                                                  -1 * L_LT_OBL_ADJ,
													  L_ASSET_ACT_ID,
													  L_DG_ID,
													  L_LS_PEND_TRANS.WORK_ORDER_ID,
													  L_LT_ACCT_ID,
													  0,
													  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													  L_LS_PEND_TRANS.COMPANY_ID,
													  L_ACCT_MONTH,
													  1,
													  L_LS_PEND_TRANS.GL_JE_CODE,
													  L_SOBS.SET_OF_BOOKS_ID,
													  L_APPROVED_RATE,
													  L_CURRENCY_FROM,
													  L_CURRENCY_TO,
                            NULL, /* A_ORIG */
													  L_MSG);
				   if L_RTN = -1 then
					  return L_MSG;
				   end if;
				end if;

				L_MSG := 'Creating Journal (Leased Asset Partial Retirement Gain/Loss Credit)';
				--Retrieve Partial Term Gain Loss
				select partial_term_gain_loss
				into l_partial_term_gain_loss
				from ls_asset_schedule
				where ls_asset_id = a_ls_asset_id
				and revision = L_NEW_REVISION
				and trunc(month, 'month') = l_remeasurement_date
				and set_of_books_id = l_sobs.set_of_books_id;

				PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
												   3027,
                                                -1 *
                                                L_PARTIAL_TERM_GAIN_LOSS,
												   L_ASSET_ACT_ID,
												   L_DG_ID,
												   L_LS_PEND_TRANS.WORK_ORDER_ID,
												   L_GL_ACCT_ID,
												   0,
												   L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
												   L_LS_PEND_TRANS.COMPANY_ID,
												   L_ACCT_MONTH,
												   0,
												   L_LS_PEND_TRANS.GL_JE_CODE,
												   L_SOBS.SET_OF_BOOKS_ID,
												   L_APPROVED_RATE,
												   L_CURRENCY_FROM,
												   L_CURRENCY_TO,
                           NULL, /* A_ORIG */
												   L_MSG);
				if L_RTN = -1 then
				   return L_MSG;
				end if;

			else
				--Addition with no Partial Termination
				if L_SOBS.AMOUNT <> 0 then --Dont post if 0 (ex Impairments which dont change Cap Cost)
					L_MSG := 'Creating Journal (Leased Asset Addition Debit)';
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
					L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													   3001,
													   L_SOBS.AMOUNT,
													   L_ASSET_ACT_ID,
													   L_DG_ID,
													   L_LS_PEND_TRANS.WORK_ORDER_ID,
													   L_GL_ACCT_ID,
													   0,
													   L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													   L_LS_PEND_TRANS.COMPANY_ID,
													   L_ACCT_MONTH,
													   1,
													   L_LS_PEND_TRANS.GL_JE_CODE,
													   L_SOBS.SET_OF_BOOKS_ID,
													   L_APPROVED_RATE,
													   L_CURRENCY_FROM,
													   L_CURRENCY_TO,
							   NULL, /* A_ORIG */
													   L_MSG);
					if L_RTN = -1 then
					   return L_MSG;
					end if;
				end if;
				/* CJS 2/16/15 Changing to use obligation adjustments */
				L_MSG := 'Creating Journal (Leased Asset Addition ST Obligation Credit)';
          if case when L_ADJ = 1 then L_OBL_ADJ else
           L_ASSET_SCHEDULE.BEG_OBLIGATION -
             NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0) end <> 0 then
            /* WMD */
				   PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				   L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													  3002,
													  case
								when L_ADJ = 1 then
								L_OBL_ADJ
							  else
                                                     L_ASSET_SCHEDULE.BEG_OBLIGATION -
                                                     NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0)
							  end, /* WMD */
													  L_ASSET_ACT_ID,
													  L_DG_ID,
													  L_LS_PEND_TRANS.WORK_ORDER_ID,
													  L_ST_ACCT_ID,
													  0,
													  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													  L_LS_PEND_TRANS.COMPANY_ID,
													  L_ACCT_MONTH,
													  0,
													  L_LS_PEND_TRANS.GL_JE_CODE,
													  L_SOBS.SET_OF_BOOKS_ID,
													  L_APPROVED_RATE,
													  L_CURRENCY_FROM,
													  L_CURRENCY_TO,
                            NULL, /* A_ORIG */
													  L_MSG);
				   if L_RTN = -1 then
					  return L_MSG;
				   end if;
				end if; -- short term JE

				L_MSG := 'Creating Journal (Leased Asset Addition LT Obligation Credit)';
          if case when L_ADJ = 1 then L_LT_OBL_ADJ else
           NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0) end <> 0 then
            /* WMD */
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				   L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													  3003,
													  case
								when L_ADJ = 1 then
								L_LT_OBL_ADJ
							  else
								NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0)
							  end, /* WMD */
													  L_ASSET_ACT_ID,
													  L_DG_ID,
													  L_LS_PEND_TRANS.WORK_ORDER_ID,
													  L_LT_ACCT_ID,
													  0,
													  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													  L_LS_PEND_TRANS.COMPANY_ID,
													  L_ACCT_MONTH,
													  0,
													  L_LS_PEND_TRANS.GL_JE_CODE,
													  L_SOBS.SET_OF_BOOKS_ID,
													  L_APPROVED_RATE,
													  L_CURRENCY_FROM,
													  L_CURRENCY_TO,
                            NULL, /* A_ORIG */
													  L_MSG);
				   if L_RTN = -1 then
					  return L_MSG;
				   end if;
				end if; -- long term JE

          if L_ADJ = 1 and NVL(L_IMPAIRMENT_ACTIVITY, 0) <> 0 and
             l_is_remeasure <> 1 then
            -- Begin Impairment
					IF L_IMPAIRMENT_ACTIVITY > 0 THEN 
						L_MSG := 'Creating Journal (Leased Asset Impairment Credit)';
						PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
						L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
													3080,
													L_IMPAIRMENT_ACTIVITY,
													L_ASSET_ACT_ID,
													L_DG_ID,
													L_LS_ASSET.WORK_ORDER_ID,
													L_IMP_ACCUMACC,
													0,
													L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													L_LS_PEND_TRANS.COMPANY_ID,
													L_ACCT_MONTH,
                                                    0, --credit
													L_LS_PEND_TRANS.GL_JE_CODE,
													L_SOBS.SET_OF_BOOKS_ID,
													L_APPROVED_RATE,
													L_CURRENCY_FROM,
													L_CURRENCY_TO,
													NULL,
													L_MSG);
						   if L_RTN = -1 then
							  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
							  return L_MSG;
			end if;
						   
						L_MSG := 'Creating Journal (Leased Asset Impairment Debit)';
						PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
						L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
													3082,
													L_IMPAIRMENT_ACTIVITY,
													L_ASSET_ACT_ID,
													L_DG_ID,
													L_LS_ASSET.WORK_ORDER_ID,
													L_IMP_EXPACC,
													0,
													L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													L_LS_PEND_TRANS.COMPANY_ID,
													L_ACCT_MONTH,
                                                    1, --debit
													L_LS_PEND_TRANS.GL_JE_CODE,
													L_SOBS.SET_OF_BOOKS_ID,
													L_APPROVED_RATE,
													L_CURRENCY_FROM,
													L_CURRENCY_TO,
													NULL,
													L_MSG);
						   if L_RTN = -1 then
							  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
							  return L_MSG;
						   end if;
            ELSE
              --has to be negative so its a reversal of an impairment
						L_MSG := 'Creating Journal (Leased Asset Impairment Reversal Credit)';
						PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
						L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
													3083,
													L_IMPAIRMENT_ACTIVITY,
													L_ASSET_ACT_ID,
													L_DG_ID,
													L_LS_ASSET.WORK_ORDER_ID,
													L_IMP_REVACC,
													0,
													L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													L_LS_PEND_TRANS.COMPANY_ID,
													L_ACCT_MONTH,
                                                    0, --credit
													L_LS_PEND_TRANS.GL_JE_CODE,
													L_SOBS.SET_OF_BOOKS_ID,
													L_APPROVED_RATE,
													L_CURRENCY_FROM,
													L_CURRENCY_TO,
													NULL,
													L_MSG);
						   if L_RTN = -1 then
							  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
							  return L_MSG;
						   end if;
						   
						L_MSG := 'Creating Journal (Leased Asset Impairment Reversal Debit)';
						PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
						L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
													3081,
													L_IMPAIRMENT_ACTIVITY,
													L_ASSET_ACT_ID,
													L_DG_ID,
													L_LS_ASSET.WORK_ORDER_ID,
													L_IMP_ACCUMACC,
													0,
													L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													L_LS_PEND_TRANS.COMPANY_ID,
													L_ACCT_MONTH,
                                                    1, --debit
													L_LS_PEND_TRANS.GL_JE_CODE,
													L_SOBS.SET_OF_BOOKS_ID,
													L_APPROVED_RATE,
													L_CURRENCY_FROM,
													L_CURRENCY_TO,
													NULL,
													L_MSG);
						   if L_RTN = -1 then
							  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
							  return L_MSG;
						   end if;
					END IF;	   
				end if; -- End Impairment

			end if;
			--End Addition 3001, 3002, 3003

      -- Unaccrued Interest Entries

      L_MSG := 'Creating Journal (Unaccrued Interest Debit)';
        if case when L_ADJ = 1 and L_IS_REMEASURE = 1 then L_UNACCRUED_ADJ else
         NVL(L_ASSET_SCHEDULE.BEG_LT_LIABILITY, 0) -
           NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0) end <> 0 and L_IS_IMPAIRMENT <> 1 then -- Dont Send for Impairments
          /* WMD */
        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
        L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                          3076,
                                                case
                                                  when L_ADJ = 1 and L_IS_REMEASURE = 1 then
                                                   L_UNACCRUED_ADJ
                                                  else
                                                   NVL(L_ASSET_SCHEDULE.BEG_LT_LIABILITY, 0) -
                                                   NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0)
                                                end,
                          L_ST_ACCT_ID,
                          L_DG_ID,
                          L_LS_PEND_TRANS.WORK_ORDER_ID,
                          L_ST_ACCT_ID,
                          0,
                          L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                          L_LS_PEND_TRANS.COMPANY_ID,
                          L_ACCT_MONTH,
                          1,
                          L_LS_PEND_TRANS.GL_JE_CODE,
                          L_SOBS.SET_OF_BOOKS_ID,
                          L_APPROVED_RATE,
                          L_CURRENCY_FROM,
                          L_CURRENCY_TO,
                          NULL, /* A_ORIG */
                          L_MSG);
         if L_RTN = -1 then
          return L_MSG;
         end if;
      end if;

      L_MSG := 'Creating Journal (Unaccrued Interest Credit)';
        if case when L_ADJ = 1 and L_IS_REMEASURE = 1 then L_UNACCRUED_ADJ else
         NVL(L_ASSET_SCHEDULE.BEG_LT_LIABILITY, 0) -
           NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0) end <> 0 and L_IS_IMPAIRMENT <> 1 then --Dont Send for Impairments
          /* WMD */
        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
        L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                          3077,
                                                case
                                                  when L_ADJ = 1 and L_IS_REMEASURE = 1 then
                                                   L_UNACCRUED_ADJ
                                                  else
                                                   NVL(L_ASSET_SCHEDULE.BEG_LT_LIABILITY, 0) -
                                                   NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0)
                                                end,
                          L_LT_ACCT_ID,
                          L_DG_ID,
                          L_LS_PEND_TRANS.WORK_ORDER_ID,
                          L_LT_ACCT_ID,
                          0,
                          L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                          L_LS_PEND_TRANS.COMPANY_ID,
                          L_ACCT_MONTH,
                          0,
                          L_LS_PEND_TRANS.GL_JE_CODE,
                          L_SOBS.SET_OF_BOOKS_ID,
                          L_APPROVED_RATE,
                          L_CURRENCY_FROM,
                          L_CURRENCY_TO,
                          NULL, /* A_ORIG */
                          L_MSG);
         if L_RTN = -1 then
          return L_MSG;
         end if;
      end if;

           /*Begin IDC and Incentive Logic for On Balance Sheet/ROU */
           -- If the FASB Cap Type is 3 or 6 (FERC) then treat the journal entries similarly
          -- to the Off B/S journals
          if L_FASB_CHECK in (3,6) then
            --Get Initial Direct Cost and Incentive Amounts at commencement so we can credit/debit out of them in accruals
            SELECT idc_math_amount, incentive_math_amount
              into l_idc_math_amount, l_incentive_math_amount
              FROM (SELECT ls_asset_id, revision, Min(month) month
                       FROM LS_ASSET_SCHEDULE a
                      WHERE ls_asset_id = A_LS_ASSET_ID
                        AND set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
                        AND revision = L_NEW_REVISION
                     GROUP BY ls_asset_id, revision) c,
                   LS_ASSET_SCHEDULE b
             WHERE c.ls_asset_id = b.ls_asset_id
               AND c.revision = b.revision
               AND c.month = b.month
               AND set_of_books_id = L_SOBS.SET_OF_BOOKS_ID;  
          
            --Book IDC if it exists
            L_MSG := 'Creating Journal (Initial Direct Cost Debit)';
            IF Nvl(l_idc_math_amount, 0) <> 0 AND L_ADJ <> 1 THEN
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                    3065,
                                                    L_IDC_MATH_AMOUNT,
                                                    L_ASSET_ACT_ID,
                                                    L_DG_ID,
                                                    L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                    L_IDC_ACCT_ID,
                                                    0,
                                                    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                    L_LS_PEND_TRANS.COMPANY_ID,
                                                    L_ACCT_MONTH,
                                                    1,
                                                    L_LS_PEND_TRANS.GL_JE_CODE,
                                                    L_SOBS.SET_OF_BOOKS_ID,
                                                    L_APPROVED_RATE,
                                                    L_CURRENCY_FROM,
                                                    L_CURRENCY_TO,
                                                    NULL, /* A_ORIG */
                                                    L_MSG);
              if L_RTN = -1 then
                return L_MSG;
              end if;

              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                    3022,
                                                    L_IDC_MATH_AMOUNT,
                                                    L_ASSET_ACT_ID,
                                                    L_DG_ID,
                                                    L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                    L_ST_ACCT_ID,
                                                    0,
                                                    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                    L_LS_PEND_TRANS.COMPANY_ID,
                                                    L_ACCT_MONTH,
                                                    0,
                                                    L_LS_PEND_TRANS.GL_JE_CODE,
                                                    L_SOBS.SET_OF_BOOKS_ID,
                                                    L_APPROVED_RATE,
                                                    L_CURRENCY_FROM,
                                                    L_CURRENCY_TO,
                                                    NULL, /* A_ORIG */
                                                    L_MSG);
              if L_RTN = -1 then
                return L_MSG;
              end if;
            END IF;

            -- Book Incentive if it exists
            L_MSG := 'Creating Journal (Incentive Credit)';
            IF Nvl(l_incentive_math_amount, 0) <> 0 AND L_ADJ <> 1 THEN
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                    3064,
                                                    l_INCENTIVE_MATH_AMOUNT,
                                                    L_ASSET_ACT_ID,
                                                    L_DG_ID,
                                                    L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                    L_INC_ACCT_ID,
                                                    0,
                                                    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                    L_LS_PEND_TRANS.COMPANY_ID,
                                                    L_ACCT_MONTH,
                                                    0,
                                                    L_LS_PEND_TRANS.GL_JE_CODE,
                                                    L_SOBS.SET_OF_BOOKS_ID,
                                                    L_APPROVED_RATE,
                                                    L_CURRENCY_FROM,
                                                    L_CURRENCY_TO,
                                                    NULL, /* A_ORIG */
                                                    L_MSG);
              if L_RTN = -1 then
                return L_MSG;
              end if;

              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                    3022,
                                                    L_INCENTIVE_MATH_AMOUNT,
                                                    L_ASSET_ACT_ID,
                                                    L_DG_ID,
                                                    L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                    L_ST_ACCT_ID,
                                                    0,
                                                    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                    L_LS_PEND_TRANS.COMPANY_ID,
                                                    L_ACCT_MONTH,
                                                    1,
                                                    L_LS_PEND_TRANS.GL_JE_CODE,
                                                    L_SOBS.SET_OF_BOOKS_ID,
                                                    L_APPROVED_RATE,
                                                    L_CURRENCY_FROM,
                                                    L_CURRENCY_TO,
                                                    NULL, /* A_ORIG */
                                                    L_MSG);
              if L_RTN = -1 then
                return L_MSG;
              end if;

              --Check to see if we need to backdate some accruals for incentives where l_acct_month > in-service
              -- Don't include accounting month's incentive since it will be caught in accrual process
              SELECT SUM(Nvl(incentive_amount, 0) -
                          Decode(trunc(a.month, 'month'),
                                 trunc(est_in_svc_date, 'month'),
                                 0,
                                 incentive_math_amount)) backdate_inc_amount
                INTO l_backdate_inc_amt
                FROM LS_ASSET_SCHEDULE a, LS_ILR_ASSET_MAP m, LS_ILR i
               WHERE a.ls_asset_id = A_LS_ASSET_ID
                 AND a.revision = L_NEW_REVISION
                 AND a.set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
                 AND a.month < L_ACCT_MONTH
                 AND a.ls_asset_id = m.ls_asset_id
                 AND a.revision = m.revision
                 AND i.ilr_id = m.ilr_id;

              if l_backdate_inc_amt <> 0 then
                L_MSG := 'Creating Journal (Catch-Up Incentive Accrual Debit)';
                PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

                L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                      3064,
                                                      l_backdate_inc_amt,
                                                      L_ASSET_ACT_ID,
                                                      L_DG_ID,
                                                      L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                      L_INC_ACCT_ID,
                                                      0,
                                                      L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                      L_LS_PEND_TRANS.COMPANY_ID,
                                                      L_ACCT_MONTH,
                                                      1,
                                                      L_LS_PEND_TRANS.GL_JE_CODE,
                                                      L_SOBS.SET_OF_BOOKS_ID,
                                                      L_APPROVED_RATE,
                                                      L_CURRENCY_FROM,
                                                      L_CURRENCY_TO,
                                                      NULL, /* A_ORIG */
                                                      L_MSG);
                if L_RTN = -1 then
                  return L_MSG;
                end if;

                L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                      3010,
                                                      l_backdate_inc_amt,
                                                      L_ASSET_ACT_ID,
                                                      L_DG_ID,
                                                      L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                      L_INT_EXP_ACCT_ID,
                                                      0,
                                                      L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                      L_LS_PEND_TRANS.COMPANY_ID,
                                                      L_ACCT_MONTH,
                                                      0,
                                                      L_LS_PEND_TRANS.GL_JE_CODE,
                                                      L_SOBS.SET_OF_BOOKS_ID,
                                                      L_APPROVED_RATE,
                                                      L_CURRENCY_FROM,
                                                      L_CURRENCY_TO,
                                                      NULL, /* A_ORIG */
                                                      L_MSG);
                if L_RTN = -1 then
                  return L_MSG;
                end if;
              end if;
            end if;
          else
             --Book IDC if it exists
             L_MSG := 'Creating Journal (Initial Direct Cost Credit)';
             if l_adj <> 1 then
              --Get Initial Direct Cost and Incentive Amounts at commencement so we can credit/debit out of them in accruals
              SELECT idc_math_amount, incentive_math_amount
                into l_idc_math_amount, l_incentive_math_amount
                FROM (SELECT ls_asset_id, revision, Min(month) month
                         FROM LS_ASSET_SCHEDULE a
                        WHERE ls_asset_id = A_LS_ASSET_ID
                          AND set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
                          AND revision = L_NEW_REVISION
                       GROUP BY ls_asset_id, revision) c,
                     LS_ASSET_SCHEDULE b
               WHERE c.ls_asset_id = b.ls_asset_id
                 AND c.revision = b.revision
                 AND c.month = b.month
                 AND set_of_books_id = L_SOBS.SET_OF_BOOKS_ID;
                 
              
                IF Nvl(l_idc_math_amount, 0) <> 0 THEN
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

                  L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                        3065,
                                                        L_IDC_MATH_AMOUNT,
                                                        L_ASSET_ACT_ID,
                                                        L_DG_ID,
                                                        L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                        L_IDC_ACCT_ID,
                                                        0,
                                                        L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                        L_LS_PEND_TRANS.COMPANY_ID,
                                                        L_ACCT_MONTH,
                                                        0,
                                                        L_LS_PEND_TRANS.GL_JE_CODE,
                                                        L_SOBS.SET_OF_BOOKS_ID,
                                                        L_APPROVED_RATE,
                                                        L_CURRENCY_FROM,
                                                        L_CURRENCY_TO,
                                                        NULL, /* A_ORIG */
                                                        L_MSG);
                  if L_RTN = -1 then
                    return L_MSG;
                  end if;
                END IF;

                --Book Incentive if it exists
                L_MSG := 'Creating Journal (Incentive Debit)';
                IF Nvl(l_incentive_math_amount, 0) <> 0 THEN
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

                  L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                        3064,
                                                        L_INCENTIVE_MATH_AMOUNT,
                                                        L_ASSET_ACT_ID,
                                                        L_DG_ID,
                                                        L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                        L_INC_ACCT_ID,
                                                        0,
                                                        L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                        L_LS_PEND_TRANS.COMPANY_ID,
                                                        L_ACCT_MONTH,
                                                        1,
                                                        L_LS_PEND_TRANS.GL_JE_CODE,
                                                        L_SOBS.SET_OF_BOOKS_ID,
                                                        L_APPROVED_RATE,
                                                        L_CURRENCY_FROM,
                                                        L_CURRENCY_TO,
                                                        NULL, /* A_ORIG */
                                                        L_MSG);
                  if L_RTN = -1 then
                    return L_MSG;
                   end if;
                END IF;
			        elsif l_adj = 1 THEN
                --Book IDC adjustment if this is a forecast/transition only
                IF L_IS_REMEASURE <> 1 AND Nvl(L_IDC_ADJ, 0) <> 0 and
                   LB_OM_TO_CAP THEN
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

                  L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                    3065,
                                    L_IDC_ADJ,
                                    L_ASSET_ACT_ID,
                                    L_DG_ID,
                                    L_LS_PEND_TRANS.WORK_ORDER_ID,
                                    L_IDC_ACCT_ID,
                                    0,
                                    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                    L_LS_PEND_TRANS.COMPANY_ID,
                                    L_ACCT_MONTH,
                                    0,
                                    L_LS_PEND_TRANS.GL_JE_CODE,
                                    L_SOBS.SET_OF_BOOKS_ID,
                                    L_APPROVED_RATE,
                                    L_CURRENCY_FROM,
                                    L_CURRENCY_TO,
                                    NULL, /* A_ORIG */
                                    L_MSG);
                   if L_RTN = -1 then
                    return L_MSG;
                   end if;
              END IF;

              --Book Incentive adjustment regardless of Remeasurement or Forecast/Transition
              L_MSG := 'Creating Journal (Incentive Debit)';
                IF Nvl(L_INC_ADJ, 0) <> 0 and LB_OM_TO_CAP THEN
                PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

                L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                  3064,
                                  L_INC_ADJ,
                                  L_ASSET_ACT_ID,
                                  L_DG_ID,
                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                  L_INC_ACCT_ID,
                                  0,
                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                  L_LS_PEND_TRANS.COMPANY_ID,
                                  L_ACCT_MONTH,
                                  1,
                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                  L_SOBS.SET_OF_BOOKS_ID,
                                  L_APPROVED_RATE,
                                  L_CURRENCY_FROM,
                                  L_CURRENCY_TO,
                                  NULL, /* A_ORIG */
                                  L_MSG);
                 if L_RTN = -1 then
                  return L_MSG;
                 end if;
              END IF;
         end if;
			end if;
			/*End IDC and Incentive Logic for On Balance Sheet/ROU */

            --Off Balance to On Balance Remeasurement Deferred and Prepaid Rent Reversals
        IF L_ADJ = 1 AND L_IS_REMEASURE = 1 AND LB_OM_TO_CAP AND
           L_ASSET_SCHEDULE.LIABILITY_REMEASUREMENT - L_INC_ADJ -
           L_SOBS.AMOUNT <> 0 THEN
          IF L_ASSET_SCHEDULE.LIABILITY_REMEASUREMENT - L_INC_ADJ -
             L_SOBS.AMOUNT > 0 THEN
                L_MSG := 'Creating Journal (Leased Asset Remeasurement Off Balance to On Balance Deferred Rent Reversal Debit)';
					      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				        L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													        3055,
                                                  L_ASSET_SCHEDULE.LIABILITY_REMEASUREMENT -
                                                  L_INC_ADJ - L_SOBS.AMOUNT,
													        L_ASSET_ACT_ID,
													        L_DG_ID,
													        L_LS_PEND_TRANS.WORK_ORDER_ID,
													        L_LT_DEF_ACCT_ID,
													        0,
													        L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													        L_LS_PEND_TRANS.COMPANY_ID,
													        L_ACCT_MONTH,
													        1,
													        L_LS_PEND_TRANS.GL_JE_CODE,
													        L_SOBS.SET_OF_BOOKS_ID,
													        L_APPROVED_RATE,
													        L_CURRENCY_FROM,
													        L_CURRENCY_TO,
													        NULL, /* A_ORIG */
													        L_MSG);
				        if L_RTN = -1 then
					        return L_MSG;
				        end if;
          ELSE
            --L_ASSET_SCHEDULE.LIABILITY_REMEASUREMENT - L_SOBS.AMOUNT < 0
                L_MSG := 'Creating Journal (Leased Asset Remeasurement Off Balance to On Balance Prepaid Rent Reversal Credit)';
                PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				        L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													        3061,
                                                  - (L_ASSET_SCHEDULE.LIABILITY_REMEASUREMENT +
                                                    L_IDC_ADJ - L_INC_ADJ -
                                                    L_SOBS.AMOUNT),
													        L_ASSET_ACT_ID,
													        L_DG_ID,
													        L_LS_PEND_TRANS.WORK_ORDER_ID,
													        L_PREPAID_RENT_ACCT_ID,
													        0,
													        L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													        L_LS_PEND_TRANS.COMPANY_ID,
													        L_ACCT_MONTH,
													        0,
													        L_LS_PEND_TRANS.GL_JE_CODE,
													        L_SOBS.SET_OF_BOOKS_ID,
													        L_APPROVED_RATE,
													        L_CURRENCY_FROM,
													        L_CURRENCY_TO,
													        NULL, /* A_ORIG */
													        L_MSG);
				        if L_RTN = -1 then
					        return L_MSG;
				        end if;
              END IF;
            END IF; --Off Balance to On Balance Remeasurement Deferred and Prepaid Rent Reversals

			/* Debit ST Deferred/Accrued Rent */
            L_MSG := 'Creating Journal (Leased Asset Addition ST Deferred/Accrued Rent Debit)';
            if L_ADJ = 1 and L_DEF_ADJ <> 0 then
				PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                  3057,
                                                  L_DEF_ADJ,
                                                  L_ASSET_ACT_ID,
                                                  L_DG_ID,
                                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                  L_ST_DEF_ACCT_ID,
                                                  0,
                                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                  L_LS_PEND_TRANS.COMPANY_ID,
                                                  L_ACCT_MONTH,
                                                  1,
                                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                                  L_SOBS.SET_OF_BOOKS_ID,
                                                  L_APPROVED_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  NULL, /* A_ORIG */
                                                  L_MSG);
               if L_RTN = -1 then
                  return L_MSG;
               end if;
            end if;

            /* Debit LT Deferred/Accrued Rent */
			L_MSG := 'Creating Journal (Leased Asset Addition LT Deferred/Accrued Rent Debit)';
            if L_ADJ = 1 and L_LT_DEF_ADJ <> 0 then
				PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                  3058,
                                                  L_LT_DEF_ADJ,
                                                  L_ASSET_ACT_ID,
                                                  L_DG_ID,
                                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                  L_LT_DEF_ACCT_ID,
                                                  0,
                                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                  L_LS_PEND_TRANS.COMPANY_ID,
                                                  L_ACCT_MONTH,
                                                  1,
                                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                                  L_SOBS.SET_OF_BOOKS_ID,
                                                  L_APPROVED_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  NULL, /* A_ORIG */
                                                  L_MSG);
               if L_RTN = -1 then
                  return L_MSG;
               end if;
            end if;

			      /* Credit Prepaid Rent */
            if L_ADJ = 1 and L_PREPAID_ADJ <> 0 then
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                  3063,
                                                  L_PREPAID_ADJ,
                                                  L_ASSET_ACT_ID,
                                                  L_DG_ID,
                                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                  L_PREPAID_RENT_ACCT_ID,
                                                  0,
                                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                  L_LS_PEND_TRANS.COMPANY_ID,
                                                  L_ACCT_MONTH,
                                                  0,
                                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                                  L_SOBS.SET_OF_BOOKS_ID,
                                                  L_APPROVED_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  NULL, /* A_ORIG */
                                                  L_MSG);
               if L_RTN = -1 then
                  return L_MSG;
               end if;
            end if;

            --If this asset has an entry in ls_asset_fcst_earnings, we need to post that to the retained earnings at Conversion trans type (3059) -  SCW
             if l_is_om = 0 then
          SELECT Count(1)
                into l_earnings_count
            FROM LS_ASSET_FCST_EARNINGS e
           WHERE e.ls_asset_id = a_ls_asset_id
             AND e.set_of_books_id = l_sobs.set_of_books_id
             and e.revision IN
                 (SELECT m.revision
                    FROM LS_FORECAST_ILR_MASTER m
                   WHERE m.in_service_revision = L_NEW_REVISION
                     AND m.ilr_id = l_ls_pend_trans.ilr_id);

              IF l_earnings_count > 0 THEN
              --Get the Retained Earnings Amount to post
                l_msg := 'Retrieving Retained Earnings amount at Transition/Conversion';
                PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

                  select sum(earnings_amount)
                  into l_earnings_amount
                  from ls_asset_fcst_earnings e
                  where e.ls_asset_id = a_ls_asset_id
                   and e.revision IN
                   (SELECT m.revision
                      FROM LS_FORECAST_ILR_MASTER m
                     WHERE m.in_service_revision = L_NEW_REVISION
                       AND m.ilr_id = l_ls_pend_trans.ilr_id)
                   and e.set_of_books_id = l_sobs.set_of_books_id;

                --Get Retained Earnings Account for the Forecast ILR it originates from
                l_msg := 'Retrieving Retained Earnings Account for Forecast Version';
                SELECT earnings_gl_acct_id
                into l_earnings_acct_id
              FROM ls_forecast_version v
             WHERE v.revision IN
                   (SELECT revision
                      FROM ls_forecast_ilr_master m
                     WHERE m.in_service_revision = L_NEW_REVISION
                       AND m.ilr_id = l_ls_pend_trans.ilr_id);

               if l_earnings_acct_id is null then
                L_MSG := 'No Earnings Account assigned to Forecast Revision the ILR originates from.';
                return L_MSG;
               end if;

              L_MSG := 'Creating Journal (Leased Asset Retained Earnings Debit)';
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                          3059,
                          l_earnings_amount,
                          L_ASSET_ACT_ID,
                          L_DG_ID,
                          L_LS_PEND_TRANS.WORK_ORDER_ID,
                          l_earnings_acct_id,
                          0,
                          L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                          L_LS_PEND_TRANS.COMPANY_ID,
                          L_ACCT_MONTH,
                          1,
                          L_LS_PEND_TRANS.GL_JE_CODE,
                          L_SOBS.SET_OF_BOOKS_ID,
                          L_APPROVED_RATE,
                          L_CURRENCY_FROM,
                          L_CURRENCY_TO,
                          NULL, /* A_ORIG */
                          L_MSG);

                if L_RTN = -1 then
                  return L_MSG;
                end if;

              END IF;
            end if;

            /* Hit depr if our adjustment doesn't balance */
        if L_ADJ = 1 and L_IS_REMEASURE <> 1 and L_FASB_CHECK not in (3, 6) then
          L_3033_AMT := L_SOBS.AMOUNT + L_DEF_ADJ + L_LT_DEF_ADJ -
                        L_LT_OBL_ADJ - L_OBL_ADJ - L_PREPAID_ADJ -
                        L_IDC_ADJ + L_INC_ADJ + NVL(l_earnings_amount, 0);
            else
              L_3033_AMT := 0;
            end if;

            L_MSG := 'Creating Journal (Leased Asset Addition Depreciation Credit)';
        if L_ADJ = 1 AND L_IS_REMEASURE <> 1 and L_3033_AMT <> 0 then
              L_3033_COUNT := L_3033_COUNT + 1;
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                  3033,
                                                  L_3033_AMT,
                                                  L_ASSET_ACT_ID,
                                                  L_DG_ID,
                                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                  L_RES_ACCT_ID,
                                                  0,
                                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                  L_LS_PEND_TRANS.COMPANY_ID,
                                                  L_ACCT_MONTH,
                                                  0,
                                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                                  L_SOBS.SET_OF_BOOKS_ID,
                                                  L_APPROVED_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  NULL, /* A_ORIG */
                                                  L_MSG);
               if L_RTN = -1 then
                  return L_MSG;
               end if;

               -- Make sure the reserve adjustment gets updated appropriately if there was a 3033 journal
          L_MSG := 'Updating CPR Depr table for reserve adjustment';
              update cpr_depr
                 set reserve_adjustment = reserve_adjustment + L_3033_AMT
               where gl_posting_mo_yr = L_ACCT_MONTH
             and asset_id = (select asset_id
                               from ls_cpr_asset_map
                              where ls_asset_id = A_LS_ASSET_ID)
                 and set_of_books_id = L_SOBS.SET_OF_BOOKS_ID;

          L_MSG := 'Updating depr ledger table for reserve adjustment';
              update depr_ledger
                 set reserve_adjustments = reserve_adjustments + L_3033_AMT
               where depr_group_id = L_DG_id
                 and gl_post_mo_yr = L_ACCT_MONTH
                 and set_of_books_id = L_SOBS.SET_OF_BOOKS_ID;
            end if; -- depr JE

        if L_ADJ = 0 AND
           L_SOBS.AMOUNT 
           + case when L_FASB_CHECK in (3,6) then 0 else 1 end * (L_DEF_ADJ + L_LT_DEF_ADJ - L_PREPAID_ADJ -
                                                                  L_IDC_MATH_AMOUNT + L_INCENTIVE_MATH_AMOUNT) 
           <> nvl(L_ASSET_SCHEDULE.BEG_OBLIGATION, 0) then
          L_MSG := 'Creating Journal (Leased Asset Addition Backdated Obligation Trueup)';
		  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
          L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                3042,
                                                L_SOBS.AMOUNT 
                                                + case when L_FASB_CHECK in (3,6) then 
                                                    0 
                                                  else 
                                                    1 
                                                  end * (L_DEF_ADJ + L_LT_DEF_ADJ - L_PREPAID_ADJ -
                                                         L_IDC_MATH_AMOUNT + L_INCENTIVE_MATH_AMOUNT)
                                                - nvl(L_ASSET_SCHEDULE.beg_obligation, 0),
                                                L_ASSET_ACT_ID,
                                                L_DG_ID,
                                                L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                L_ST_ACCT_ID,
                                                0,
                                                L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                L_LS_PEND_TRANS.COMPANY_ID,
                                                L_ACCT_MONTH,
                                                0,
                                                L_LS_PEND_TRANS.GL_JE_CODE,
                                                L_SOBS.SET_OF_BOOKS_ID,
                                                L_APPROVED_RATE,
                                                L_CURRENCY_FROM,
                                                L_CURRENCY_TO,
                                                NULL, /* A_ORIG */
                                                L_MSG);
         if L_RTN = -1 then
            return L_MSG;
         end if;

          L_RESERVE_ADJ := L_SOBS.AMOUNT + L_DEF_ADJ + L_LT_DEF_ADJ -
                           L_PREPAID_ADJ - L_IDC_MATH_AMOUNT +
                           L_INCENTIVE_MATH_AMOUNT -
                           nvl(L_ASSET_SCHEDULE.beg_obligation, 0);
         /* WMD adding principal catchup */
          if lower(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Lease Catch Up Principal',
                                                                          L_LS_PEND_TRANS.COMPANY_ID))) =
             'yes' then
          L_MSG := 'Creating Journal (Leased Asset Addition Backdated Principal Trueup Debit to Principal)';
		      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
          L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                            3018,
                                                  L_SOBS.AMOUNT + L_DEF_ADJ +
                                                  L_LT_DEF_ADJ -
                                                  L_PREPAID_ADJ -
                                                  L_IDC_MATH_AMOUNT +
                                                  L_INCENTIVE_MATH_AMOUNT -
                                                  nvl(L_ASSET_SCHEDULE.beg_obligation,
                                                      0),
                            L_ASSET_ACT_ID,
                            L_DG_ID,
                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                            L_ST_ACCT_ID,
                            0,
                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                            L_LS_PEND_TRANS.COMPANY_ID,
                            L_ACCT_MONTH,
                            1,
                            L_LS_PEND_TRANS.GL_JE_CODE,
                            L_SOBS.SET_OF_BOOKS_ID,
                            L_APPROVED_RATE,
                            L_CURRENCY_FROM,
                            L_CURRENCY_TO,
                            NULL, /* A_ORIG */
                            L_MSG);
         if L_RTN = -1 then
            return L_MSG;
         end if;

          L_MSG := 'Creating Journal (Leased Asset Addition Backdated Principal Trueup Debit to Principal)';
		  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
          L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                            3022,
                                                  L_SOBS.AMOUNT + L_DEF_ADJ +
                                                  L_LT_DEF_ADJ -
                                                  L_PREPAID_ADJ -
                                                  L_IDC_MATH_AMOUNT +
                                                  L_INCENTIVE_MATH_AMOUNT -
                                                  nvl(L_ASSET_SCHEDULE.beg_obligation,
                                                      0),
                            L_ASSET_ACT_ID,
                            L_DG_ID,
                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                            L_AP_ACCT_ID,
                            0,
                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                            L_LS_PEND_TRANS.COMPANY_ID,
                            L_ACCT_MONTH,
                            0,
                            L_LS_PEND_TRANS.GL_JE_CODE,
                            L_SOBS.SET_OF_BOOKS_ID,
                            L_APPROVED_RATE,
                            L_CURRENCY_FROM,
                            L_CURRENCY_TO,
                            NULL, /* A_ORIG */
                            L_MSG);
         if L_RTN = -1 then
            return L_MSG;
         end if;

         end if; -- Principal Catch Up
        elsif L_ADJ = 1 and L_IS_REMEASURE = 1 and L_REMEASUREMENT_DATE < L_ACCT_MONTH then
          -- Need to book catch up journals if this is a backdated remeasurement
          select sum(principal_paid)
            into L_CURR_PRIN_PAID
            from ls_asset_schedule
           where ls_asset_id = A_LS_ASSET_ID
             and revision = L_NEW_REVISION
             and set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
             and month between L_REMEASUREMENT_DATE and add_months(L_ACCT_MONTH,-1);
             
          select sum(principal_paid)
            into L_APPR_PRIN_PAID
            from ls_asset_schedule
           where ls_asset_id = A_LS_ASSET_ID
             and revision = L_PRIOR_REVISION
             and set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
             and month between L_REMEASUREMENT_DATE and add_months(L_ACCT_MONTH,-1); 
             
          if L_CURR_PRIN_PAID - L_APPR_PRIN_PAID <> 0 then
            L_MSG := 'Creating Journal (Leased Asset Addition Backdated Obligation Trueup)';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            
            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                  3042,
                                                  L_CURR_PRIN_PAID - L_APPR_PRIN_PAID,
                                                  L_ASSET_ACT_ID,
                                                  L_DG_ID,
                                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                  L_ST_ACCT_ID,
                                                  0,
                                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                  L_LS_PEND_TRANS.COMPANY_ID,
                                                  L_ACCT_MONTH,
                                                  0,
                                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                                  L_SOBS.SET_OF_BOOKS_ID,
                                                  L_APPROVED_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  NULL, /* A_ORIG */
                                                  L_MSG);
            if L_RTN = -1 then
              return L_MSG;
            end if;
          
            if lower(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Lease Catch Up Principal',
                                                                            L_LS_PEND_TRANS.COMPANY_ID))) =
               'yes' then
              L_MSG := 'Creating Journal (Leased Asset Addition Backdated Principal Trueup Debit to Principal)';
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                    3018,
                                                    L_CURR_PRIN_PAID - L_APPR_PRIN_PAID,
                                                    L_ASSET_ACT_ID,
                                                    L_DG_ID,
                                                    L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                    L_ST_ACCT_ID,
                                                    0,
                                                    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                    L_LS_PEND_TRANS.COMPANY_ID,
                                                    L_ACCT_MONTH,
                                                    1,
                                                    L_LS_PEND_TRANS.GL_JE_CODE,
                                                    L_SOBS.SET_OF_BOOKS_ID,
                                                    L_APPROVED_RATE,
                                                    L_CURRENCY_FROM,
                                                    L_CURRENCY_TO,
                                                    NULL, /* A_ORIG */
                                                    L_MSG);
              if L_RTN = -1 then
                return L_MSG;
              end if;
            
              L_MSG := 'Creating Journal (Leased Asset Addition Backdated Principal Trueup Debit to Principal)';
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
                                                    3022,
                                                    L_CURR_PRIN_PAID - L_APPR_PRIN_PAID,
                                                    L_ASSET_ACT_ID,
                                                    L_DG_ID,
                                                    L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                    L_AP_ACCT_ID,
                                                    0,
                                                    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                    L_LS_PEND_TRANS.COMPANY_ID,
                                                    L_ACCT_MONTH,
                                                    0,
                                                    L_LS_PEND_TRANS.GL_JE_CODE,
                                                    L_SOBS.SET_OF_BOOKS_ID,
                                                    L_APPROVED_RATE,
                                                    L_CURRENCY_FROM,
                                                    L_CURRENCY_TO,
                                                    NULL, /* A_ORIG */
                                                    L_MSG);
              if L_RTN = -1 then
                return L_MSG;
              end if;
            
            end if; -- Principal Catch Up
          end if;
      end if; -- Trueup

		--see if we need to handle backdated interest accruals
        if (L_ADJ = 0 OR (L_ADJ = 1 AND L_IS_REMEASURE = 1 AND L_REMEASUREMENT_DATE < L_ACCT_MONTH)) AND
           lower(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Disable Lease Interest True-up JEs',
                                                                        L_LS_PEND_TRANS.COMPANY_ID))) = 'no' then
          
				--get info from schedule for backdated months
          if L_ADJ = 0 then
            -- backdated in service
            SELECT Nvl(Sum(interest_accrual), 0),
                   Nvl(Sum(interest_paid), 0)
				INTO L_BACKDATE_INT_ACCRUED, L_BACKDATE_INT_PAID
				FROM ls_asset_schedule las
				WHERE las.ls_asset_id = A_LS_ASSET_ID
				AND las.revision = L_NEW_REVISION
				AND las.set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
				AND las.MONTH < L_ACCT_MONTH;
          elsif L_ADJ = 1 AND L_IS_REMEASURE = 1 AND L_REMEASUREMENT_DATE < L_ACCT_MONTH then
            -- backdated remeasurement
            select curr.int_accrual - appr.int_accrual, curr.int_paid - appr.int_paid
              into L_BACKDATE_INT_ACCRUED, L_BACKDATE_INT_PAID
              from (
                     select nvl(sum(interest_accrual), 0) int_accrual, nvl(sum(interest_paid), 0) int_paid
                       from ls_asset_schedule
                      where ls_asset_id = A_LS_ASSET_ID
                        and revision = L_NEW_REVISION
                        and set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
                        and month between L_REMEASUREMENT_DATE and add_months(L_ACCT_MONTH, -1)
                   ) curr,
                   (
                     select nvl(sum(interest_accrual), 0) int_accrual, nvl(sum(interest_paid), 0) int_paid
                       from ls_asset_schedule
                      where ls_asset_id = A_LS_ASSET_ID
                        and revision = L_PRIOR_REVISION
                        and set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
                        and month between L_REMEASUREMENT_DATE and add_months(L_ACCT_MONTH, -1)
                   ) appr;

          end if;
          
            PKG_PP_LOG.P_WRITE_MESSAGE('Backdated Interest Accrued: ' ||
                                       L_BACKDATE_INT_ACCRUED ||
                                       ' Backdated Interest Paid: ' ||
                                       L_BACKDATE_INT_PAID);

				IF L_BACKDATE_INT_ACCRUED <> 0 THEN
				L_MSG := 'Creating Journal (3010 - Lease Monthly Interest Debit for Backdated Interest Accrual Catchup)';
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													    3010,
													    L_BACKDATE_INT_ACCRUED,
													    L_ASSET_ACT_ID,
													    L_DG_ID,
													    L_LS_PEND_TRANS.WORK_ORDER_ID,
													    L_INT_EXP_ACCT_ID,
													    0,
													    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													    L_LS_PEND_TRANS.COMPANY_ID,
													    L_ACCT_MONTH,
													    1,
													    L_LS_PEND_TRANS.GL_JE_CODE,
													    L_SOBS.SET_OF_BOOKS_ID,
													    L_APPROVED_RATE,
													    L_CURRENCY_FROM,
													    L_CURRENCY_TO,
													    NULL, /* A_ORIG */
													    L_MSG);
				    if L_RTN = -1 then
					    return L_MSG;
				    end if;

					L_MSG := 'Creating Journal (3011 - Lease Monthly Interest Credit for Backdated Interest Accrual Catchup)';
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													    3011,
													    L_BACKDATE_INT_ACCRUED,
													    L_ASSET_ACT_ID,
													    L_DG_ID,
													    L_LS_PEND_TRANS.WORK_ORDER_ID,
													    L_INTACC_ACCT_ID,
													    0,
													    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													    L_LS_PEND_TRANS.COMPANY_ID,
													    L_ACCT_MONTH,
													    0,
													    L_LS_PEND_TRANS.GL_JE_CODE,
													    L_SOBS.SET_OF_BOOKS_ID,
													    L_APPROVED_RATE,
													    L_CURRENCY_FROM,
													    L_CURRENCY_TO,
													    NULL, /* A_ORIG */
													    L_MSG);
				    if L_RTN = -1 then
					    return L_MSG;
				    end if;
				END IF; -- JEs for interest accrual catchup

				IF L_BACKDATE_INT_PAID <> 0 THEN
					L_MSG := 'Creating Journal (3019 - Lease Payment Interest Debit for Backdated Interest Paid Catchup)';
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													    3019,
													    L_BACKDATE_INT_PAID,
													    L_ASSET_ACT_ID,
													    L_DG_ID,
													    L_LS_PEND_TRANS.WORK_ORDER_ID,
													    L_INT_EXP_ACCT_ID,
													    0,
													    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													    L_LS_PEND_TRANS.COMPANY_ID,
													    L_ACCT_MONTH,
													    1,
													    L_LS_PEND_TRANS.GL_JE_CODE,
													    L_SOBS.SET_OF_BOOKS_ID,
													    L_APPROVED_RATE,
													    L_CURRENCY_FROM,
													    L_CURRENCY_TO,
													    NULL, /* A_ORIG */
													    L_MSG);
				    if L_RTN = -1 then
					    return L_MSG;
				    end if;

					L_MSG := 'Creating Journal (3022 - Lease Payment Credit for Backdated Interest Paid Catchup)';
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													    3022,
													    L_BACKDATE_INT_PAID,
													    L_ASSET_ACT_ID,
													    L_DG_ID,
													    L_LS_PEND_TRANS.WORK_ORDER_ID,
													    L_INTACC_ACCT_ID,
													    0,
													    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													    L_LS_PEND_TRANS.COMPANY_ID,
													    L_ACCT_MONTH,
													    0,
													    L_LS_PEND_TRANS.GL_JE_CODE,
													    L_SOBS.SET_OF_BOOKS_ID,
													    L_APPROVED_RATE,
													    L_CURRENCY_FROM,
													    L_CURRENCY_TO,
													    NULL, /* A_ORIG */
													    L_MSG);
				    if L_RTN = -1 then
					    return L_MSG;
				    end if;
				END IF; -- JEs for Interest Paid catch up
		end if; -- Backdated interest system control.
		
		 -- Handle Backdated Currency Gain/Loss entries
     -- Don't handle remeasurements yet
     if L_CURRENCY_FROM <> L_CURRENCY_TO and L_ADJ = 0 and L_CONTRACT_JES = 'no' then
       -- Book ST/LT FX Gain/Loss Debit/Credit as sum of ST/LT GAIN/LOSS for months between the in service
       -- month and the current month 
       L_MSG := 'Gathering Backdated Currency Gain/Loss Amounts'; 
       select sum(st_currency_gain_loss), sum(lt_currency_gain_loss)
         into L_ST_FX_GL, L_LT_FX_GL
         from v_ls_asset_schedule_fx_vw
        where ls_asset_id = A_LS_ASSET_ID
          and revision = L_NEW_REVISION
          and set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
          and ls_cur_type = 2
          and month < L_ACCT_MONTH;
          
       if NVL(L_ST_FX_GL,0) <> 0 then
         -- ST Debit
         L_MSG := 'Creating Journal (3068 - Lease Currency Gain/Loss ST Debit)';
         L_RTN := PKG_LEASE_COMMON.F_BOOKJE(A_LS_ASSET_ID,
                                            3068,
                                            L_ST_FX_GL,
                                            L_ASSET_ACT_ID,
                                            L_DG_ID,
                                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                                            L_FX_GAIN_LOSS_DR_ACCT_ID,
                                            0,
                                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                            L_LS_PEND_TRANS.COMPANY_ID,
                                            L_ACCT_MONTH,
                                            1,
                                            L_LS_PEND_TRANS.GL_JE_CODE,
                                            L_SOBS.SET_OF_BOOKS_ID,
                                            L_MSG);
         
         if L_RTN = -1 then
           return L_MSG;
         end if;
         
         -- ST Credit
         L_MSG := 'Creating Journal (3073 - Lessee Currency Gain/Loss ST Credit)';
         L_RTN := PKG_LEASE_COMMON.F_BOOKJE(A_LS_ASSET_ID,
                                            3073,
                                            L_ST_FX_GL,
                                            L_ASSET_ACT_ID,
                                            L_DG_ID,
                                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                                            L_FX_GAIN_LOSS_CR_ACCT_ID,
                                            0,
                                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                            L_LS_PEND_TRANS.COMPANY_ID,
                                            L_ACCT_MONTH,
                                            0,
                                            L_LS_PEND_TRANS.GL_JE_CODE,
                                            L_SOBS.SET_OF_BOOKS_ID,
                                            L_MSG);
         
         if L_RTN = -1 then
           return L_MSG;
         end if;
       end if;
       
       if NVL(L_LT_FX_GL,0) <> 0 then
         -- LT Debit
         L_MSG := 'Creating Journal (3051 - Lease Currency Gain/Loss LT Debit)';
         L_RTN := PKG_LEASE_COMMON.F_BOOKJE(A_LS_ASSET_ID,
                                            3051,
                                            L_LT_FX_GL,
                                            L_ASSET_ACT_ID,
                                            L_DG_ID,
                                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                                            L_FX_GAIN_LOSS_DR_ACCT_ID,
                                            0,
                                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                            L_LS_PEND_TRANS.COMPANY_ID,
                                            L_ACCT_MONTH,
                                            1,
                                            L_LS_PEND_TRANS.GL_JE_CODE,
                                            L_SOBS.SET_OF_BOOKS_ID,
                                            L_MSG);
         
         if L_RTN = -1 then
           return L_MSG;
         end if;
         
         -- LT Credit
         L_MSG := 'Creating Journal (3052 - Lease Currency Gain/Loss LT Credit)';
         L_RTN := PKG_LEASE_COMMON.F_BOOKJE(A_LS_ASSET_ID,
                                            3052,
                                            L_LT_FX_GL,
                                            L_ASSET_ACT_ID,
                                            L_DG_ID,
                                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                                            L_FX_GAIN_LOSS_CR_ACCT_ID,
                                            0,
                                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                            L_LS_PEND_TRANS.COMPANY_ID,
                                            L_ACCT_MONTH,
                                            0,
                                            L_LS_PEND_TRANS.GL_JE_CODE,
                                            L_SOBS.SET_OF_BOOKS_ID,
                                            L_MSG);
         
         if L_RTN = -1 then
           return L_MSG;
         end if;
       end if;
     
       -- Book catch up entries
       if upper(trim(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Lease MC: Use Average Rates', 
                                                                       L_LS_PEND_TRANS.COMPANY_ID))) = 'YES' then
         -- Calculate the FX Gain/Loss Amounts for each month between the in service date on the ILR and the current month:
         --   LT = (in-service exchange rate - locked month exchange rate) * 
         --          (obligation_reclass - unaccrued_interest_reclass)
         --   ST = (in-service exchange rate - locked month actual exchange rate) * 
         --          (-obligation_reclass + unaccrued_interest_reclass) + 
         --        (in-service exchange rate - locked month average exchange rate) * 
         --          (-interest_accrual + interest_paid + principal_paid)
         select sum((L_APPROVED_RATE - r1.rate) * 
                      (las.obligation_reclass - las.unaccrued_interest_reclass)) lt_fx_gl_diff,
                sum((L_APPROVED_RATE - r1.rate) * 
                      (las.unaccrued_interest_reclass - las.obligation_reclass) +
                    (L_APPROVED_RATE - r4.rate) *
                      (las.interest_paid + las.principal_paid - las.interest_accrual)) st_fx_gl_diff
           into L_LT_FX_GL_DIFF, L_ST_FX_GL_DIFF
           from ls_asset_schedule las
           join ls_lease_calculated_date_rates r1 on r1.accounting_month = las.month
           join ls_lease_calculated_date_rates r4 on r4.company_id = r1.company_id
                                                 and r4.accounting_month = r1.accounting_month
                                                 and r4.contract_currency_id = r1.contract_currency_id
                                                 and r4.company_currency_id = r1.company_currency_id
          where las.ls_asset_id = A_LS_ASSET_ID
            and las.revision = L_NEW_REVISION
            and las.set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
            and las.month < L_ACCT_MONTH
            and r1.company_id = L_LS_PEND_TRANS.COMPANY_ID
            and r1.contract_currency_id = L_CURRENCY_FROM
            and r1.company_currency_id = L_CURRENCY_TO
            and r1.exchange_rate_type_id = 1
            and r4.exchange_rate_type_id = 4;
       else
         -- Calculate the FX Gain/Loss Amounts for each month between the in service date on the ILR and the current month:
         --   LT = (in-service exchange rate - locked month exchange rate) * 
         --          (obligation_reclass - unaccrued_interest_reclass)
         --   ST = (in-service exchange rate - locked month actual exchange rate) * 
         --          (-interest_accrual + interest_paid + principal_paid - obligation_reclass + unaccrued_interest_reclass)
         select sum((L_APPROVED_RATE - r.rate) * (las.obligation_reclass - las.unaccrued_interest_reclass)) lt_fx_gl_diff,
                sum((L_APPROVED_RATE - r.rate) * 
                      (las.interest_paid + las.principal_paid - las.interest_accrual 
                       - las.obligation_reclass + las.unaccrued_interest_reclass)) st_fx_gl_diff
           into L_LT_FX_GL_DIFF, L_ST_FX_GL_DIFF
           from ls_asset_schedule las
           join ls_lease_calculated_date_rates r on r.accounting_month = las.month
          where las.ls_asset_id = A_LS_ASSET_ID
            and las.revision = L_NEW_REVISION
            and las.set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
            and las.month < L_ACCT_MONTH
            and r.company_id = L_LS_PEND_TRANS.COMPANY_ID
            and r.contract_currency_id = L_CURRENCY_FROM
            and r.company_currency_id = L_CURRENCY_TO
            and r.exchange_rate_type_id = 1;
       end if;
       
       -- Process the journal entries for the catch up amounts
       if NVL(L_ST_FX_GL_DIFF,0) <> 0 then
         -- ST Debit
         L_MSG := 'Creating Journal (3068 - Lease Currency Gain/Loss ST Debit) for Backdated difference amount';
         L_RTN := PKG_LEASE_COMMON.F_BOOKJE(A_LS_ASSET_ID,
                                            3068,
                                            L_ST_FX_GL_DIFF,
                                            L_ASSET_ACT_ID,
                                            L_DG_ID,
                                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                                            L_FX_GAIN_LOSS_DR_ACCT_ID,
                                            0,
                                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                            L_LS_PEND_TRANS.COMPANY_ID,
                                            L_ACCT_MONTH,
                                            1,
                                            L_LS_PEND_TRANS.GL_JE_CODE,
                                            L_SOBS.SET_OF_BOOKS_ID,
                                            L_MSG);
         
         if L_RTN = -1 then
           return L_MSG;
         end if;
         
         -- ST Credit
         L_MSG := 'Creating Journal (3073 - Lease Currency Gain/Loss ST Credit) for Backdated difference amount';
         L_RTN := PKG_LEASE_COMMON.F_BOOKJE(A_LS_ASSET_ID,
                                            3073,
                                            L_ST_FX_GL_DIFF,
                                            L_ASSET_ACT_ID,
                                            L_DG_ID,
                                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                                            L_FX_GAIN_LOSS_CR_ACCT_ID,
                                            0,
                                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                            L_LS_PEND_TRANS.COMPANY_ID,
                                            L_ACCT_MONTH,
                                            0,
                                            L_LS_PEND_TRANS.GL_JE_CODE,
                                            L_SOBS.SET_OF_BOOKS_ID,
                                            L_MSG);
         
         if L_RTN = -1 then
           return L_MSG;
         end if;
       end if;
       
       if NVL(L_LT_FX_GL_DIFF,0) <> 0 then
         -- LT Debit
         L_MSG := 'Creating Journal (3051 - Lease Currency Gain/Loss LT Debit) for Backdated difference amount';
         L_RTN := PKG_LEASE_COMMON.F_BOOKJE(A_LS_ASSET_ID,
                                            3051,
                                            L_LT_FX_GL_DIFF,
                                            L_ASSET_ACT_ID,
                                            L_DG_ID,
                                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                                            L_FX_GAIN_LOSS_DR_ACCT_ID,
                                            0,
                                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                            L_LS_PEND_TRANS.COMPANY_ID,
                                            L_ACCT_MONTH,
                                            1,
                                            L_LS_PEND_TRANS.GL_JE_CODE,
                                            L_SOBS.SET_OF_BOOKS_ID,
                                            L_MSG);
         
         if L_RTN = -1 then
           return L_MSG;
         end if;
         
         -- LT Credit
         L_MSG := 'Creating Journal (3052 - Lease Currency Gain/Loss LT Credit) for Backdated difference amount';
         L_RTN := PKG_LEASE_COMMON.F_BOOKJE(A_LS_ASSET_ID,
                                            3052,
                                            L_LT_FX_GL_DIFF,
                                            L_ASSET_ACT_ID,
                                            L_DG_ID,
                                            L_LS_PEND_TRANS.WORK_ORDER_ID,
                                            L_FX_GAIN_LOSS_CR_ACCT_ID,
                                            0,
                                            L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                            L_LS_PEND_TRANS.COMPANY_ID,
                                            L_ACCT_MONTH,
                                            0,
                                            L_LS_PEND_TRANS.GL_JE_CODE,
                                            L_SOBS.SET_OF_BOOKS_ID,
                                            L_MSG);
         
         if L_RTN = -1 then
           return L_MSG;
         end if;
       end if;
     end if; -- if L_CURRENCY_FROM <> L_CURRENCY_TO and L_ADJ = 0 and L_CONTRACT_JES = 'no' then
     
     --For additions and adjustments, lets book deferred rent amounts (should never start with a balance on deferred rent though)
         elsif L_IS_OM = 1 and G_SEND_JES then
        if lower(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Generate Deferred Rent JEs',
                                                                   L_LS_PEND_TRANS.COMPANY_ID)) =
           'yes' then
				  L_MSG := 'Creating Journal (Leased Asset Addition ST Deferred Rent Credit)';
				  if L_ADJ = 1 and L_DEF_ADJ <> 0 then
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													    3053,
													    L_DEF_ADJ,
													    L_ASSET_ACT_ID,
													    L_DG_ID,
													    L_LS_PEND_TRANS.WORK_ORDER_ID,
													    L_ST_DEF_ACCT_ID,
													    0,
													    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													    L_LS_PEND_TRANS.COMPANY_ID,
													    L_ACCT_MONTH,
													    0,
													    L_LS_PEND_TRANS.GL_JE_CODE,
													    L_SOBS.SET_OF_BOOKS_ID,
													    L_APPROVED_RATE,
													    L_CURRENCY_FROM,
													    L_CURRENCY_TO,
													    NULL, /* A_ORIG */
													    L_MSG);
				    if L_RTN = -1 then
					    return L_MSG;
				    end if;
				  end if; -- short term Deferred JE

				  L_MSG := 'Creating Journal (Leased Asset Addition LT Deferred Rent Credit)';
				  if L_ADJ = 1 and L_LT_DEF_ADJ <> 0 then
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													    3054,
													    L_LT_DEF_ADJ,
													    L_ASSET_ACT_ID,
													    L_DG_ID,
													    L_LS_PEND_TRANS.WORK_ORDER_ID,
													    L_LT_DEF_ACCT_ID,
													    0,
													    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													    L_LS_PEND_TRANS.COMPANY_ID,
													    L_ACCT_MONTH,
													    0,
													    L_LS_PEND_TRANS.GL_JE_CODE,
													    L_SOBS.SET_OF_BOOKS_ID,
													    L_APPROVED_RATE,
													    L_CURRENCY_FROM,
													    L_CURRENCY_TO,
													    NULL, /* A_ORIG */
													    L_MSG);
				    if L_RTN = -1 then
					    return L_MSG;
				    end if;
				  end if; -- long term Deferred JE

				  L_MSG := 'Creating Journal (Leased Asset Addition Prepaid Rent Debit)';
				  if L_ADJ = 1 and L_PREPAID_ADJ <> 0 then
				    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													    3060,
													    L_PREPAID_ADJ,
													    L_ASSET_ACT_ID,
													    L_DG_ID,
													    L_LS_PEND_TRANS.WORK_ORDER_ID,
													    L_PREPAID_RENT_ACCT_ID,
													    0,
													    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													    L_LS_PEND_TRANS.COMPANY_ID,
													    L_ACCT_MONTH,
													    1,
													    L_LS_PEND_TRANS.GL_JE_CODE,
													    L_SOBS.SET_OF_BOOKS_ID,
													    L_APPROVED_RATE,
													    L_CURRENCY_FROM,
													    L_CURRENCY_TO,
													    NULL, /* A_ORIG */
													    L_MSG);
				    if L_RTN = -1 then
					    return L_MSG;
				    end if;
				  end if; -- Prepaid JE

				  L_MSG := 'Creating Journal (Leased Asset Addition Deferred Rent Debit)';
				  if L_ADJ = 1 and L_DEF_ADJ + L_LT_DEF_ADJ <> 0 then
					PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
				    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													    3055,
													    L_DEF_ADJ + L_LT_DEF_ADJ,
													    L_ASSET_ACT_ID,
													    L_DG_ID,
													    L_LS_PEND_TRANS.WORK_ORDER_ID,
													    L_INTACC_ACCT_ID,
													    0,
													    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													    L_LS_PEND_TRANS.COMPANY_ID,
													    L_ACCT_MONTH,
													    1,
													    L_LS_PEND_TRANS.GL_JE_CODE,
													    L_SOBS.SET_OF_BOOKS_ID,
													    L_APPROVED_RATE,
													    L_CURRENCY_FROM,
													    L_CURRENCY_TO,
													    NULL, /* A_ORIG */
													    L_MSG);
				    if L_RTN = -1 then
					    return L_MSG;
				    end if;
				  end if; -- Deferred Debit JE

				  L_MSG := 'Creating Journal (Leased Asset Addition Prepaid Rent Credit)';
				  if L_ADJ = 1 and L_PREPAID_ADJ <> 0 then
				    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													    3061,
													    L_PREPAID_ADJ,
													    L_ASSET_ACT_ID,
													    L_DG_ID,
													    L_LS_PEND_TRANS.WORK_ORDER_ID,
													    L_INTACC_ACCT_ID,
													    0,
													    L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													    L_LS_PEND_TRANS.COMPANY_ID,
													    L_ACCT_MONTH,
													    0,
													    L_LS_PEND_TRANS.GL_JE_CODE,
													    L_SOBS.SET_OF_BOOKS_ID,
													    L_APPROVED_RATE,
													    L_CURRENCY_FROM,
													    L_CURRENCY_TO,
													    NULL, /* A_ORIG */
													    L_MSG);
				    if L_RTN = -1 then
					    return L_MSG;
				    end if;
				  end if; -- Prepaid Credit JE

          if lower(PKG_PP_SYSTEM_CONTROL.F_PP_SYSTEM_CONTROL_COMPANY('Generate Catch-up Deferred Rent JEs',
                                                                     L_LS_PEND_TRANS.COMPANY_ID)) =
             'yes' then
					  L_MSG := 'Creating Journal (Leased Asset Addition Backdated ST Deferred Rent)';
					    if L_ADJ = 0 and L_ASSET_SCHEDULE.beg_st_deferred_rent <> 0 then
							PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
						    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													      3053,
													      L_ASSET_SCHEDULE.beg_st_deferred_rent,
													      L_ASSET_ACT_ID,
													      L_DG_ID,
													      L_LS_PEND_TRANS.WORK_ORDER_ID,
													      L_ST_DEF_ACCT_ID,
													      0,
													      L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													      L_LS_PEND_TRANS.COMPANY_ID,
													      L_ACCT_MONTH,
													      0,
													      L_LS_PEND_TRANS.GL_JE_CODE,
													      L_SOBS.SET_OF_BOOKS_ID,
													      L_APPROVED_RATE,
													      L_CURRENCY_FROM,
													      L_CURRENCY_TO,
													      NULL, /* A_ORIG */
													      L_MSG);
						    if L_RTN = -1 then
						      return L_MSG;
						    end if;
					    end if; -- short term Deferred JE

					    L_MSG := 'Creating Journal (Leased Asset Addition Backdated LT Deferred Rent)';
            if L_ADJ = 0 and L_ASSET_SCHEDULE.beg_deferred_rent -
               L_ASSET_SCHEDULE.beg_st_deferred_rent <> 0 then
							PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
						    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													      3054,
                                                    L_ASSET_SCHEDULE.beg_deferred_rent -
                                                    L_ASSET_SCHEDULE.beg_st_deferred_rent,
													      L_ASSET_ACT_ID,
													      L_DG_ID,
													      L_LS_PEND_TRANS.WORK_ORDER_ID,
													      L_LT_DEF_ACCT_ID,
													      0,
													      L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													      L_LS_PEND_TRANS.COMPANY_ID,
													      L_ACCT_MONTH,
													      0,
													      L_LS_PEND_TRANS.GL_JE_CODE,
													      L_SOBS.SET_OF_BOOKS_ID,
													      L_APPROVED_RATE,
													      L_CURRENCY_FROM,
													      L_CURRENCY_TO,
													      NULL, /* A_ORIG */
													      L_MSG);
						    if L_RTN = -1 then
						      return L_MSG;
						    end if;
					    end if; -- long term Deferred JE

					    L_MSG := 'Creating Journal (Leased Asset Addition Backdated Deferred Rent Debit)';
					    if L_ADJ = 0 and L_ASSET_SCHEDULE.beg_deferred_rent <> 0 then
							PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
						    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													      3056,
													      L_ASSET_SCHEDULE.beg_deferred_rent,
													      L_ASSET_ACT_ID,
													      L_DG_ID,
													      L_LS_PEND_TRANS.WORK_ORDER_ID,
													      L_INTACC_ACCT_ID,
													      0,
													      L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													      L_LS_PEND_TRANS.COMPANY_ID,
													      L_ACCT_MONTH,
													      1,
													      L_LS_PEND_TRANS.GL_JE_CODE,
													      L_SOBS.SET_OF_BOOKS_ID,
													      L_APPROVED_RATE,
													      L_CURRENCY_FROM,
													      L_CURRENCY_TO,
													      NULL, /* A_ORIG */
													      L_MSG);
						    if L_RTN = -1 then
						      return L_MSG;
						    end if;
					    end if; -- Deferred Rent Debit

              L_MSG := 'Creating Journal (Leased Asset Addition Backdated Prepaid Rent)';
					    if L_ADJ = 0 and L_ASSET_SCHEDULE.beg_prepaid_rent <> 0 then
						    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													      3060,
													      L_ASSET_SCHEDULE.beg_prepaid_rent,
													      L_ASSET_ACT_ID,
													      L_DG_ID,
													      L_LS_PEND_TRANS.WORK_ORDER_ID,
													      L_PREPAID_RENT_ACCT_ID,
													      0,
													      L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													      L_LS_PEND_TRANS.COMPANY_ID,
													      L_ACCT_MONTH,
													      1,
													      L_LS_PEND_TRANS.GL_JE_CODE,
													      L_SOBS.SET_OF_BOOKS_ID,
													      L_APPROVED_RATE,
													      L_CURRENCY_FROM,
													      L_CURRENCY_TO,
													      NULL, /* A_ORIG */
													      L_MSG);
						    if L_RTN = -1 then
						      return L_MSG;
						    end if;
					    end if; -- Prepaid JE

					    L_MSG := 'Creating Journal (Leased Asset Addition Backdated Prepaid Rent Credit)';
					    if L_ADJ = 0 and L_ASSET_SCHEDULE.beg_prepaid_rent <> 0 then
						    L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
													      3062,
													      L_ASSET_SCHEDULE.beg_prepaid_rent,
													      L_ASSET_ACT_ID,
													      L_DG_ID,
													      L_LS_PEND_TRANS.WORK_ORDER_ID,
													      L_INTACC_ACCT_ID,
													      0,
													      L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
													      L_LS_PEND_TRANS.COMPANY_ID,
													      L_ACCT_MONTH,
													      0,
													      L_LS_PEND_TRANS.GL_JE_CODE,
													      L_SOBS.SET_OF_BOOKS_ID,
													      L_APPROVED_RATE,
													      L_CURRENCY_FROM,
													      L_CURRENCY_TO,
													      NULL, /* A_ORIG */
													      L_MSG);
						    if L_RTN = -1 then
						      return L_MSG;
						    end if;
					    end if; -- Prepaid Rent Credit

				    end if; -- Deferred Rent Catch-up
			    end if; -- Generate Deferred Rent JEs

					/*Begin IDC and Incentive Logic for OM*/
          --Get Initial Direct Cost and Incentive Amounts at commencement so we can credit/debit out of them in accruals
        SELECT idc_math_amount, incentive_math_amount
          into l_idc_math_amount, l_incentive_math_amount
          FROM (SELECT ls_asset_id, revision, Min(month) month
                   FROM LS_ASSET_SCHEDULE a
                  WHERE ls_asset_id = A_LS_ASSET_ID
                    AND set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
                    AND revision = L_NEW_REVISION
                 GROUP BY ls_asset_id, revision) c,
               LS_ASSET_SCHEDULE b
         WHERE c.ls_asset_id = b.ls_asset_id
           AND c.revision = b.revision
           AND c.month = b.month
           AND set_of_books_id = L_SOBS.SET_OF_BOOKS_ID;

					--Book IDC if it exists
					L_MSG := 'Creating Journal (Initial Direct Cost Debit)';
        IF Nvl(l_idc_math_amount, 0) <> 0 AND L_ADJ <> 1 THEN
					  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

					  L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
														  3065,
														  L_IDC_MATH_AMOUNT,
														  L_ASSET_ACT_ID,
														  L_DG_ID,
														  L_LS_PEND_TRANS.WORK_ORDER_ID,
														  L_IDC_ACCT_ID,
														  0,
														  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
														  L_LS_PEND_TRANS.COMPANY_ID,
														  L_ACCT_MONTH,
														  1,
														  L_LS_PEND_TRANS.GL_JE_CODE,
														  L_SOBS.SET_OF_BOOKS_ID,
														  L_APPROVED_RATE,
														  L_CURRENCY_FROM,
														  L_CURRENCY_TO,
														  NULL, /* A_ORIG */
														  L_MSG);
					   if L_RTN = -1 then
						  return L_MSG;
					   end if;

					  L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
														  3022,
														  L_IDC_MATH_AMOUNT,
														  L_ASSET_ACT_ID,
														  L_DG_ID,
														  L_LS_PEND_TRANS.WORK_ORDER_ID,
														  L_ST_ACCT_ID,
														  0,
														  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
														  L_LS_PEND_TRANS.COMPANY_ID,
														  L_ACCT_MONTH,
														  0,
														  L_LS_PEND_TRANS.GL_JE_CODE,
														  L_SOBS.SET_OF_BOOKS_ID,
														  L_APPROVED_RATE,
														  L_CURRENCY_FROM,
														  L_CURRENCY_TO,
														  NULL, /* A_ORIG */
														  L_MSG);
					   if L_RTN = -1 then
						  return L_MSG;
					   end if;

					END IF;

					-- Book Incentive if it exists
					L_MSG := 'Creating Journal (Incentive Credit)';
        IF Nvl(l_incentive_math_amount, 0) <> 0 AND L_ADJ <> 1 THEN
					  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

					  L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
														  3064,
														  l_INCENTIVE_MATH_AMOUNT,
														  L_ASSET_ACT_ID,
														  L_DG_ID,
														  L_LS_PEND_TRANS.WORK_ORDER_ID,
														  L_INC_ACCT_ID,
														  0,
														  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
														  L_LS_PEND_TRANS.COMPANY_ID,
														  L_ACCT_MONTH,
														  0,
														  L_LS_PEND_TRANS.GL_JE_CODE,
														  L_SOBS.SET_OF_BOOKS_ID,
														  L_APPROVED_RATE,
														  L_CURRENCY_FROM,
														  L_CURRENCY_TO,
														  NULL, /* A_ORIG */
														  L_MSG);
					   if L_RTN = -1 then
						  return L_MSG;
					   end if;

					  L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
														  3022,
														  L_INCENTIVE_MATH_AMOUNT,
														  L_ASSET_ACT_ID,
														  L_DG_ID,
														  L_LS_PEND_TRANS.WORK_ORDER_ID,
														  L_ST_ACCT_ID,
														  0,
														  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
														  L_LS_PEND_TRANS.COMPANY_ID,
														  L_ACCT_MONTH,
														  1,
														  L_LS_PEND_TRANS.GL_JE_CODE,
														  L_SOBS.SET_OF_BOOKS_ID,
														  L_APPROVED_RATE,
														  L_CURRENCY_FROM,
														  L_CURRENCY_TO,
														  NULL, /* A_ORIG */
														  L_MSG);
					   if L_RTN = -1 then
						  return L_MSG;
					   end if;

					   --Check to see if we need to backdate some accruals for incentives where l_acct_month > in-service
					   -- Don't include accounting month's incentive since it will be caught in accrual process
          SELECT SUM(Nvl(incentive_amount, 0) -
                      Decode(trunc(a.month, 'month'),
                             trunc(est_in_svc_date, 'month'),
                             0,
                             incentive_math_amount)) backdate_inc_amount
							INTO l_backdate_inc_amt
            FROM LS_ASSET_SCHEDULE a, LS_ILR_ASSET_MAP m, LS_ILR i
           WHERE a.ls_asset_id = A_LS_ASSET_ID
             AND a.revision = L_NEW_REVISION
             AND a.set_of_books_id = L_SOBS.SET_OF_BOOKS_ID
             AND a.month < L_ACCT_MONTH
             AND a.ls_asset_id = m.ls_asset_id
             AND a.revision = m.revision
             AND i.ilr_id = m.ilr_id;

					   if l_backdate_inc_amt <> 0 then
						L_MSG := 'Creating Journal (Catch-Up Incentive Accrual Debit)';
						  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

						  L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
															  3064,
															  l_backdate_inc_amt,
															  L_ASSET_ACT_ID,
															  L_DG_ID,
															  L_LS_PEND_TRANS.WORK_ORDER_ID,
															  L_INC_ACCT_ID,
															  0,
															  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
															  L_LS_PEND_TRANS.COMPANY_ID,
															  L_ACCT_MONTH,
															  1,
															  L_LS_PEND_TRANS.GL_JE_CODE,
															  L_SOBS.SET_OF_BOOKS_ID,
															  L_APPROVED_RATE,
															  L_CURRENCY_FROM,
															  L_CURRENCY_TO,
															  NULL, /* A_ORIG */
															  L_MSG);
						   if L_RTN = -1 then
							  return L_MSG;
						   end if;

						  L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(A_LS_ASSET_ID,
															  3010,
															  l_backdate_inc_amt,
															  L_ASSET_ACT_ID,
															  L_DG_ID,
															  L_LS_PEND_TRANS.WORK_ORDER_ID,
															  L_INT_EXP_ACCT_ID,
															  0,
															  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
															  L_LS_PEND_TRANS.COMPANY_ID,
															  L_ACCT_MONTH,
															  0,
															  L_LS_PEND_TRANS.GL_JE_CODE,
															  L_SOBS.SET_OF_BOOKS_ID,
															  L_APPROVED_RATE,
															  L_CURRENCY_FROM,
															  L_CURRENCY_TO,
															  NULL, /* A_ORIG */
															  L_MSG);
						   if L_RTN = -1 then
							  return L_MSG;
						   end if;

					   end if;

					end if;
					/*End IDC and Incentive Logic for OM */
         end if; -- end if is_om 


      end loop; -- END Process by set of books
	  
	   
       -- verify JE's balance
				L_RTN := PKG_LEASE_COMMON.F_BALANCE_JE(A_LS_ASSET_ID,
                        L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                        L_LS_PEND_TRANS.COMPANY_ID,
		                L_ACCT_MONTH,
                        L_LS_PEND_TRANS.GL_JE_CODE,
                        L_MSG);

        if L_RTN = -1 then
           return L_MSG;
        end if;
		
	  
	  	-- verify MC AUDIT in balance
				L_RTN := PKG_LEASE_COMMON.F_BALANCE_MC_AUDIT(A_LS_ASSET_ID,
                        L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                        L_LS_PEND_TRANS.COMPANY_ID,
					    L_ACCT_MONTH,
                        L_LS_PEND_TRANS.GL_JE_CODE,
                        L_MSG);

        if L_RTN = -1 then
           return L_MSG;
        end if;

      L_SECOND_COST := 0;

    select count(*)
      into L_SECOND_COST
        from SET_OF_BOOKS S
       where S.SECOND_SET_OF_BOOKS_IND = 1
       and S.SET_OF_BOOKS_ID in
           (select set_of_books_id
              from company_set_of_books
             where nvl(include_indicator, 0) = 1
            and company_id = L_LS_PEND_TRANS.COMPANY_ID);

      if L_SECOND_COST > 0 then
         L_MSG := 'Retrieving second cost';
         BEGIN
           select POSTING_AMOUNT
             into L_SECOND_COST
             from SET_OF_BOOKS S, LS_PEND_SET_OF_BOOKS PB
            where S.SECOND_SET_OF_BOOKS_IND = 1
              and PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID
              and S.SET_OF_BOOKS_ID = PB.SET_OF_BOOKS_ID;
         EXCEPTION
           WHEN no_data_found then
             L_MSG := 'ERROR: Second Cost Book not found in LS_PEND_SET_OF_BOOKS.';
             return L_MSG;
         END;
      end if;

      -- if it's on pend transaction, take it from there
      L_MSG := 'Adding asset to CPR Ledger';
      merge into CPR_LEDGER A
      using (select L_ASSET_ID as ASSET_ID,
                    L_LS_PEND_TRANS.PROPERTY_GROUP_ID as PROPERTY_GROUP_ID,
                    L_DG_ID as DEPR_GROUP_ID,
                    L_LS_PEND_TRANS.RETIREMENT_UNIT_ID as RETIREMENT_UNIT_ID,
                    L_LS_PEND_TRANS.BUS_SEGMENT_ID as BUS_SEGMENT_ID,
                    L_LS_PEND_TRANS.COMPANY_ID as COMPANY_ID,
                    L_LS_PEND_TRANS.FUNC_CLASS_ID as FUNC_CLASS_ID,
                    L_LS_PEND_TRANS.UTILITY_ACCOUNT_ID as UTILITY_ACCOUNT_ID,
                    L_GL_ACCT_ID as GL_ACCOUNT_ID,
                    L_LS_PEND_TRANS.ASSET_LOCATION_ID as ASSET_LOCATION_ID,
                    L_LS_PEND_TRANS.SUB_ACCOUNT_ID as SUB_ACCOUNT_ID,
                    WOC.WORK_ORDER_NUMBER as WORK_ORDER_NUMBER,
                    NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE, L_MONTH) as IN_SERVICE_DATE,
                    L_LS_PEND_TRANS.POSTING_QUANTITY as POSTING_QUANTITY,
                    L_LS_PEND_TRANS.POSTING_AMOUNT * L_APPROVED_RATE as POSTING_AMOUNT,
                    L_LS_ASSET.DESCRIPTION as DESCRIPTION,
                    L_LS_ASSET.LONG_DESCRIPTION as LONG_DESCRIPTION,
                    L_LS_PEND_TRANS.SERIAL_NUMBER as SERIAL_NUMBER,
                    case
                       when L_SECOND_COST > 0 then
                        L_LS_PEND_TRANS.POSTING_AMOUNT * L_APPROVED_RATE
                       else
                        0
                    end as SECOND_COST
               from WORK_ORDER_CONTROL WOC
              where WOC.WORK_ORDER_ID = L_LS_PEND_TRANS.WORK_ORDER_ID) B
      on (A.ASSET_ID = B.ASSET_ID)
      when matched then
         update
         set A.ACCUM_QUANTITY        = A.ACCUM_QUANTITY + B.POSTING_QUANTITY,
             A.ACCUM_COST            = A.ACCUM_COST + B.POSTING_AMOUNT,
             A.SECOND_FINANCIAL_COST = A.SECOND_FINANCIAL_COST +
                                       B.SECOND_COST,
                /* WMD Update in service dates if the start date */
             A.ENG_IN_SERVICE_YEAR = B.IN_SERVICE_DATE,
             A.IN_SERVICE_YEAR     = B.IN_SERVICE_DATE
      when not matched then
         insert
        (ASSET_ID, PROPERTY_GROUP_ID, DEPR_GROUP_ID, BOOKS_SCHEMA_ID,
         RETIREMENT_UNIT_ID, BUS_SEGMENT_ID, COMPANY_ID, FUNC_CLASS_ID,
         UTILITY_ACCOUNT_ID, GL_ACCOUNT_ID, ASSET_LOCATION_ID,
         SUB_ACCOUNT_ID, WORK_ORDER_NUMBER, LEDGER_STATUS, IN_SERVICE_YEAR,
         ACCUM_QUANTITY, ACCUM_COST, SUBLEDGER_INDICATOR, DESCRIPTION,
         LONG_DESCRIPTION, ENG_IN_SERVICE_YEAR, SERIAL_NUMBER, ACCUM_COST_2,
         SECOND_FINANCIAL_COST)
         values
        (B.ASSET_ID, B.PROPERTY_GROUP_ID, B.DEPR_GROUP_ID, 1,
         B.RETIREMENT_UNIT_ID, B.BUS_SEGMENT_ID, B.COMPANY_ID,
         B.FUNC_CLASS_ID, B.UTILITY_ACCOUNT_ID, B.GL_ACCOUNT_ID,
         B.ASSET_LOCATION_ID, B.SUB_ACCOUNT_ID, B.WORK_ORDER_NUMBER, 1,
         B.IN_SERVICE_DATE, B.POSTING_QUANTITY, B.POSTING_AMOUNT, -100,
         B.DESCRIPTION, B.LONG_DESCRIPTION, B.IN_SERVICE_DATE,
         B.SERIAL_NUMBER, 0, B.SECOND_COST);

      L_MSG := 'Adding CPR Activity';
      insert into CPR_ACTIVITY
      (ASSET_ID, ASSET_ACTIVITY_ID, GL_POSTING_MO_YR, CPR_POSTING_MO_YR,
       WORK_ORDER_NUMBER, GL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION,
       ACTIVITY_CODE, ACTIVITY_STATUS, ACTIVITY_QUANTITY, ACTIVITY_COST,
       FERC_ACTIVITY_CODE, MONTH_NUMBER, ACTIVITY_COST_2,
          ACT_SECOND_FINANCIAL_COST)
      select L_ASSET_ID,
             L_ASSET_ACT_ID,
             L_ACCT_MONTH,
             --gl_posting_mo_yr
             L_ACCT_MONTH,
             --cpr_posting_mo_yr
             WOC.WORK_ORDER_NUMBER,
             L_LS_PEND_TRANS.GL_JE_CODE,
             L_LS_ASSET.DESCRIPTION,
             L_LS_ASSET.LONG_DESCRIPTION,
             L_ACT_CODE,
             --Activity Code
             L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
             --activity_status (this is the pend_trans_id)
                L_LS_PEND_TRANS.POSTING_QUANTITY,
                L_LS_PEND_TRANS.POSTING_AMOUNT * L_APPROVED_RATE,
             L_FERC_ACT_CODE,
             --ferc_activity_code (addition)
             TO_NUMBER(TO_CHAR(L_ACCT_MONTH, 'yyyymm')),
             --month_number
                0,
                case
                   when L_SECOND_COST > 0 then
                    L_LS_PEND_TRANS.POSTING_AMOUNT * L_APPROVED_RATE
                   else
                    0
                end
           from WORK_ORDER_CONTROL WOC
          where WOC.WORK_ORDER_ID = L_LS_PEND_TRANS.WORK_ORDER_ID;

      L_MSG := 'Adding to activity basis table';
      insert into CPR_ACT_BASIS
      (ASSET_ID, ASSET_ACTIVITY_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4,
       BASIS_5, BASIS_6, BASIS_7, BASIS_8, BASIS_9, BASIS_10, BASIS_11,
       BASIS_12, BASIS_13, BASIS_14, BASIS_15, BASIS_16, BASIS_17, BASIS_18,
       BASIS_19, BASIS_20, BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25,
       BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32,
       BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39,
       BASIS_40, BASIS_41, BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46,
       BASIS_47, BASIS_48, BASIS_49, BASIS_50, BASIS_51, BASIS_52, BASIS_53,
       BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60,
       BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67,
       BASIS_68, BASIS_69, BASIS_70)
      select L_ASSET_ID,
             L_ASSET_ACT_ID,
                BASIS_1 * L_APPROVED_RATE AS BASIS_1,
                BASIS_2 * L_APPROVED_RATE AS BASIS_2,
                BASIS_3 * L_APPROVED_RATE AS BASIS_3,
                BASIS_4 * L_APPROVED_RATE AS BASIS_4,
                BASIS_5 * L_APPROVED_RATE AS BASIS_5,
                BASIS_6 * L_APPROVED_RATE AS BASIS_6,
                BASIS_7 * L_APPROVED_RATE AS BASIS_7,
                BASIS_8 * L_APPROVED_RATE AS BASIS_8,
                BASIS_9 * L_APPROVED_RATE AS BASIS_9,
                BASIS_10 * L_APPROVED_RATE AS BASIS_10,
                BASIS_11 * L_APPROVED_RATE AS BASIS_11,
                BASIS_12 * L_APPROVED_RATE AS BASIS_12,
                BASIS_13 * L_APPROVED_RATE AS BASIS_13,
                BASIS_14 * L_APPROVED_RATE AS BASIS_14,
                BASIS_15 * L_APPROVED_RATE AS BASIS_15,
                BASIS_16 * L_APPROVED_RATE AS BASIS_16,
                BASIS_17 * L_APPROVED_RATE AS BASIS_17,
                BASIS_18 * L_APPROVED_RATE AS BASIS_18,
                BASIS_19 * L_APPROVED_RATE AS BASIS_19,
                BASIS_20 * L_APPROVED_RATE AS BASIS_20,
                BASIS_21 * L_APPROVED_RATE AS BASIS_21,
                BASIS_22 * L_APPROVED_RATE AS BASIS_22,
                BASIS_23 * L_APPROVED_RATE AS BASIS_23,
                BASIS_24 * L_APPROVED_RATE AS BASIS_24,
                BASIS_25 * L_APPROVED_RATE AS BASIS_25,
                BASIS_26 * L_APPROVED_RATE AS BASIS_26,
                BASIS_27 * L_APPROVED_RATE AS BASIS_27,
                BASIS_28 * L_APPROVED_RATE AS BASIS_28,
                BASIS_29 * L_APPROVED_RATE AS BASIS_29,
                BASIS_30 * L_APPROVED_RATE AS BASIS_30,
                BASIS_31 * L_APPROVED_RATE AS BASIS_31,
                BASIS_32 * L_APPROVED_RATE AS BASIS_32,
                BASIS_33 * L_APPROVED_RATE AS BASIS_33,
                BASIS_34 * L_APPROVED_RATE AS BASIS_34,
                BASIS_35 * L_APPROVED_RATE AS BASIS_35,
                BASIS_36 * L_APPROVED_RATE AS BASIS_36,
                BASIS_37 * L_APPROVED_RATE AS BASIS_37,
                BASIS_38 * L_APPROVED_RATE AS BASIS_38,
                BASIS_39 * L_APPROVED_RATE AS BASIS_39,
                BASIS_40 * L_APPROVED_RATE AS BASIS_40,
                BASIS_41 * L_APPROVED_RATE AS BASIS_41,
                BASIS_42 * L_APPROVED_RATE AS BASIS_42,
                BASIS_43 * L_APPROVED_RATE AS BASIS_43,
                BASIS_44 * L_APPROVED_RATE AS BASIS_44,
                BASIS_45 * L_APPROVED_RATE AS BASIS_45,
                BASIS_46 * L_APPROVED_RATE AS BASIS_46,
                BASIS_47 * L_APPROVED_RATE AS BASIS_47,
                BASIS_48 * L_APPROVED_RATE AS BASIS_48,
                BASIS_49 * L_APPROVED_RATE AS BASIS_49,
                BASIS_50 * L_APPROVED_RATE AS BASIS_50,
                BASIS_51 * L_APPROVED_RATE AS BASIS_51,
                BASIS_52 * L_APPROVED_RATE AS BASIS_52,
                BASIS_53 * L_APPROVED_RATE AS BASIS_53,
                BASIS_54 * L_APPROVED_RATE AS BASIS_54,
                BASIS_55 * L_APPROVED_RATE AS BASIS_55,
                BASIS_56 * L_APPROVED_RATE AS BASIS_56,
                BASIS_57 * L_APPROVED_RATE AS BASIS_57,
                BASIS_58 * L_APPROVED_RATE AS BASIS_58,
                BASIS_59 * L_APPROVED_RATE AS BASIS_59,
                BASIS_60 * L_APPROVED_RATE AS BASIS_60,
                BASIS_61 * L_APPROVED_RATE AS BASIS_61,
                BASIS_62 * L_APPROVED_RATE AS BASIS_62,
                BASIS_63 * L_APPROVED_RATE AS BASIS_63,
                BASIS_64 * L_APPROVED_RATE AS BASIS_64,
                BASIS_65 * L_APPROVED_RATE AS BASIS_65,
                BASIS_66 * L_APPROVED_RATE AS BASIS_66,
                BASIS_67 * L_APPROVED_RATE AS BASIS_67,
                BASIS_68 * L_APPROVED_RATE AS BASIS_68,
                BASIS_69 * L_APPROVED_RATE AS BASIS_69,
                BASIS_70 * L_APPROVED_RATE AS BASIS_70
           from LS_PEND_BASIS
          where LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID;

      L_MSG := 'Adding to ledger basis table';
      merge into CPR_LDG_BASIS A
      using (select L_ASSET_ID as ASSET_ID,
                BASIS_1 * L_APPROVED_RATE AS BASIS_1,
                BASIS_2 * L_APPROVED_RATE AS BASIS_2,
                BASIS_3 * L_APPROVED_RATE AS BASIS_3,
                BASIS_4 * L_APPROVED_RATE AS BASIS_4,
                BASIS_5 * L_APPROVED_RATE AS BASIS_5,
                BASIS_6 * L_APPROVED_RATE AS BASIS_6,
                BASIS_7 * L_APPROVED_RATE AS BASIS_7,
                BASIS_8 * L_APPROVED_RATE AS BASIS_8,
                BASIS_9 * L_APPROVED_RATE AS BASIS_9,
                BASIS_10 * L_APPROVED_RATE AS BASIS_10,
                BASIS_11 * L_APPROVED_RATE AS BASIS_11,
                BASIS_12 * L_APPROVED_RATE AS BASIS_12,
                BASIS_13 * L_APPROVED_RATE AS BASIS_13,
                BASIS_14 * L_APPROVED_RATE AS BASIS_14,
                BASIS_15 * L_APPROVED_RATE AS BASIS_15,
                BASIS_16 * L_APPROVED_RATE AS BASIS_16,
                BASIS_17 * L_APPROVED_RATE AS BASIS_17,
                BASIS_18 * L_APPROVED_RATE AS BASIS_18,
                BASIS_19 * L_APPROVED_RATE AS BASIS_19,
                BASIS_20 * L_APPROVED_RATE AS BASIS_20,
                BASIS_21 * L_APPROVED_RATE AS BASIS_21,
                BASIS_22 * L_APPROVED_RATE AS BASIS_22,
                BASIS_23 * L_APPROVED_RATE AS BASIS_23,
                BASIS_24 * L_APPROVED_RATE AS BASIS_24,
                BASIS_25 * L_APPROVED_RATE AS BASIS_25,
                BASIS_26 * L_APPROVED_RATE AS BASIS_26,
                BASIS_27 * L_APPROVED_RATE AS BASIS_27,
                BASIS_28 * L_APPROVED_RATE AS BASIS_28,
                BASIS_29 * L_APPROVED_RATE AS BASIS_29,
                BASIS_30 * L_APPROVED_RATE AS BASIS_30,
                BASIS_31 * L_APPROVED_RATE AS BASIS_31,
                BASIS_32 * L_APPROVED_RATE AS BASIS_32,
                BASIS_33 * L_APPROVED_RATE AS BASIS_33,
                BASIS_34 * L_APPROVED_RATE AS BASIS_34,
                BASIS_35 * L_APPROVED_RATE AS BASIS_35,
                BASIS_36 * L_APPROVED_RATE AS BASIS_36,
                BASIS_37 * L_APPROVED_RATE AS BASIS_37,
                BASIS_38 * L_APPROVED_RATE AS BASIS_38,
                BASIS_39 * L_APPROVED_RATE AS BASIS_39,
                BASIS_40 * L_APPROVED_RATE AS BASIS_40,
                BASIS_41 * L_APPROVED_RATE AS BASIS_41,
                BASIS_42 * L_APPROVED_RATE AS BASIS_42,
                BASIS_43 * L_APPROVED_RATE AS BASIS_43,
                BASIS_44 * L_APPROVED_RATE AS BASIS_44,
                BASIS_45 * L_APPROVED_RATE AS BASIS_45,
                BASIS_46 * L_APPROVED_RATE AS BASIS_46,
                BASIS_47 * L_APPROVED_RATE AS BASIS_47,
                BASIS_48 * L_APPROVED_RATE AS BASIS_48,
                BASIS_49 * L_APPROVED_RATE AS BASIS_49,
                BASIS_50 * L_APPROVED_RATE AS BASIS_50,
                BASIS_51 * L_APPROVED_RATE AS BASIS_51,
                BASIS_52 * L_APPROVED_RATE AS BASIS_52,
                BASIS_53 * L_APPROVED_RATE AS BASIS_53,
                BASIS_54 * L_APPROVED_RATE AS BASIS_54,
                BASIS_55 * L_APPROVED_RATE AS BASIS_55,
                BASIS_56 * L_APPROVED_RATE AS BASIS_56,
                BASIS_57 * L_APPROVED_RATE AS BASIS_57,
                BASIS_58 * L_APPROVED_RATE AS BASIS_58,
                BASIS_59 * L_APPROVED_RATE AS BASIS_59,
                BASIS_60 * L_APPROVED_RATE AS BASIS_60,
                BASIS_61 * L_APPROVED_RATE AS BASIS_61,
                BASIS_62 * L_APPROVED_RATE AS BASIS_62,
                BASIS_63 * L_APPROVED_RATE AS BASIS_63,
                BASIS_64 * L_APPROVED_RATE AS BASIS_64,
                BASIS_65 * L_APPROVED_RATE AS BASIS_65,
                BASIS_66 * L_APPROVED_RATE AS BASIS_66,
                BASIS_67 * L_APPROVED_RATE AS BASIS_67,
                BASIS_68 * L_APPROVED_RATE AS BASIS_68,
                BASIS_69 * L_APPROVED_RATE AS BASIS_69,
                BASIS_70 * L_APPROVED_RATE AS BASIS_70
               from LS_PEND_BASIS
              where LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID) B
      on (A.ASSET_ID = B.ASSET_ID)
      when matched then
         update
         set A.BASIS_1  = A.BASIS_1 + B.BASIS_1,
             A.BASIS_2  = A.BASIS_2 + B.BASIS_2,
             A.BASIS_3  = A.BASIS_3 + B.BASIS_3,
             A.BASIS_4  = A.BASIS_4 + B.BASIS_4,
             A.BASIS_5  = A.BASIS_5 + B.BASIS_5,
             A.BASIS_6  = A.BASIS_6 + B.BASIS_6,
             A.BASIS_7  = A.BASIS_7 + B.BASIS_7,
             A.BASIS_8  = A.BASIS_8 + B.BASIS_8,
             A.BASIS_9  = A.BASIS_9 + B.BASIS_9,
             A.BASIS_10 = A.BASIS_10 + B.BASIS_10,
             A.BASIS_11 = A.BASIS_11 + B.BASIS_11,
             A.BASIS_12 = A.BASIS_12 + B.BASIS_12,
             A.BASIS_13 = A.BASIS_13 + B.BASIS_13,
             A.BASIS_14 = A.BASIS_14 + B.BASIS_14,
             A.BASIS_15 = A.BASIS_15 + B.BASIS_15,
             A.BASIS_16 = A.BASIS_16 + B.BASIS_16,
             A.BASIS_17 = A.BASIS_17 + B.BASIS_17,
             A.BASIS_18 = A.BASIS_18 + B.BASIS_18,
             A.BASIS_19 = A.BASIS_19 + B.BASIS_19,
             A.BASIS_20 = A.BASIS_20 + B.BASIS_20,
             A.BASIS_21 = A.BASIS_21 + B.BASIS_21,
             A.BASIS_22 = A.BASIS_22 + B.BASIS_22,
             A.BASIS_23 = A.BASIS_23 + B.BASIS_23,
             A.BASIS_24 = A.BASIS_24 + B.BASIS_24,
             A.BASIS_25 = A.BASIS_25 + B.BASIS_25,
             A.BASIS_26 = A.BASIS_26 + B.BASIS_26,
             A.BASIS_27 = A.BASIS_27 + B.BASIS_27,
             A.BASIS_28 = A.BASIS_28 + B.BASIS_28,
             A.BASIS_29 = A.BASIS_29 + B.BASIS_29,
             A.BASIS_30 = A.BASIS_30 + B.BASIS_30,
             A.BASIS_31 = A.BASIS_31 + B.BASIS_31,
             A.BASIS_32 = A.BASIS_32 + B.BASIS_32,
             A.BASIS_33 = A.BASIS_33 + B.BASIS_33,
             A.BASIS_34 = A.BASIS_34 + B.BASIS_34,
             A.BASIS_35 = A.BASIS_35 + B.BASIS_35,
             A.BASIS_36 = A.BASIS_36 + B.BASIS_36,
             A.BASIS_37 = A.BASIS_37 + B.BASIS_37,
             A.BASIS_38 = A.BASIS_38 + B.BASIS_38,
             A.BASIS_39 = A.BASIS_39 + B.BASIS_39,
             A.BASIS_40 = A.BASIS_40 + B.BASIS_40,
             A.BASIS_41 = A.BASIS_41 + B.BASIS_41,
             A.BASIS_42 = A.BASIS_42 + B.BASIS_42,
             A.BASIS_43 = A.BASIS_43 + B.BASIS_43,
             A.BASIS_44 = A.BASIS_44 + B.BASIS_44,
             A.BASIS_45 = A.BASIS_45 + B.BASIS_45,
             A.BASIS_46 = A.BASIS_46 + B.BASIS_46,
             A.BASIS_47 = A.BASIS_47 + B.BASIS_47,
             A.BASIS_48 = A.BASIS_48 + B.BASIS_48,
             A.BASIS_49 = A.BASIS_49 + B.BASIS_49,
             A.BASIS_50 = A.BASIS_50 + B.BASIS_50,
             A.BASIS_51 = A.BASIS_51 + B.BASIS_51,
             A.BASIS_52 = A.BASIS_52 + B.BASIS_52,
             A.BASIS_53 = A.BASIS_53 + B.BASIS_53,
             A.BASIS_54 = A.BASIS_54 + B.BASIS_54,
             A.BASIS_55 = A.BASIS_55 + B.BASIS_55,
             A.BASIS_56 = A.BASIS_56 + B.BASIS_56,
             A.BASIS_57 = A.BASIS_57 + B.BASIS_57,
             A.BASIS_58 = A.BASIS_58 + B.BASIS_58,
             A.BASIS_59 = A.BASIS_59 + B.BASIS_59,
             A.BASIS_60 = A.BASIS_60 + B.BASIS_60,
             A.BASIS_61 = A.BASIS_61 + B.BASIS_61,
             A.BASIS_62 = A.BASIS_62 + B.BASIS_62,
             A.BASIS_63 = A.BASIS_63 + B.BASIS_63,
             A.BASIS_64 = A.BASIS_64 + B.BASIS_64,
             A.BASIS_65 = A.BASIS_65 + B.BASIS_65,
             A.BASIS_66 = A.BASIS_66 + B.BASIS_66,
             A.BASIS_67 = A.BASIS_67 + B.BASIS_67,
             A.BASIS_68 = A.BASIS_68 + B.BASIS_68,
             A.BASIS_69 = A.BASIS_69 + B.BASIS_69,
             A.BASIS_70 = A.BASIS_70 + B.BASIS_70
      when not matched then
         insert
        (ASSET_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6,
         BASIS_7, BASIS_8, BASIS_9, BASIS_10, BASIS_11, BASIS_12, BASIS_13,
         BASIS_14, BASIS_15, BASIS_16, BASIS_17, BASIS_18, BASIS_19,
         BASIS_20, BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25,
         BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31,
         BASIS_32, BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37,
         BASIS_38, BASIS_39, BASIS_40, BASIS_41, BASIS_42, BASIS_43,
         BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49,
         BASIS_50, BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55,
         BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60, BASIS_61,
         BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67,
         BASIS_68, BASIS_69, BASIS_70)
         values
        (B.ASSET_ID, B.BASIS_1, B.BASIS_2, B.BASIS_3, B.BASIS_4, B.BASIS_5,
         B.BASIS_6, B.BASIS_7, B.BASIS_8, B.BASIS_9, B.BASIS_10, B.BASIS_11,
         B.BASIS_12, B.BASIS_13, B.BASIS_14, B.BASIS_15, B.BASIS_16,
         B.BASIS_17, B.BASIS_18, B.BASIS_19, B.BASIS_20, B.BASIS_21,
         B.BASIS_22, B.BASIS_23, B.BASIS_24, B.BASIS_25, B.BASIS_26,
         B.BASIS_27, B.BASIS_28, B.BASIS_29, B.BASIS_30, B.BASIS_31,
         B.BASIS_32, B.BASIS_33, B.BASIS_34, B.BASIS_35, B.BASIS_36,
         B.BASIS_37, B.BASIS_38, B.BASIS_39, B.BASIS_40, B.BASIS_41,
         B.BASIS_42, B.BASIS_43, B.BASIS_44, B.BASIS_45, B.BASIS_46,
         B.BASIS_47, B.BASIS_48, B.BASIS_49, B.BASIS_50, B.BASIS_51,
         B.BASIS_52, B.BASIS_53, B.BASIS_54, B.BASIS_55, B.BASIS_56,
         B.BASIS_57, B.BASIS_58, B.BASIS_59, B.BASIS_60, B.BASIS_61,
         B.BASIS_62, B.BASIS_63, B.BASIS_64, B.BASIS_65, B.BASIS_66,
         B.BASIS_67, B.BASIS_68, B.BASIS_69, B.BASIS_70);

      L_MSG := 'Loading cpr_depr';
      merge into CPR_DEPR A
    using (select L_ASSET_ID as ASSET_ID,
                  PB.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                    l_init_life as EXPECTED_LIFE,
                  l_init_life +
                   months_between(NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE, L_MONTH),
                                  L_ACCT_MONTH) +
                   Decode(Sign(months_between(NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE,
                                                  L_MONTH),
                                              L_ACCT_MONTH)),
                          -1,
                                  (1 - DG.MID_PERIOD_CONV),
                                  0) AS REMAINING_LIFE,
                    PB.POSTING_AMOUNT * L_APPROVED_RATE as AMOUNT,
                    DMR.RATE / 12 as MONTH_RATE,
                    L_LS_PEND_TRANS.COMPANY_ID as COMPANY_ID,
                    decode(MAP.FASB_CAP_TYPE_ID, 1, 'SL', 2, 'SLE', 3, 'FERC', 6, 'FERC', DG.MID_PERIOD_METHOD) as MID_PERIOD_METHOD,
                    DG.MID_PERIOD_CONV as MID_PERIOD_CONV,
                    L_DG_ID as DEPR_GROUP_ID,
                    DMR.DEPR_METHOD_ID as DEPR_METHOD_ID,
                    nvl(DG.TRUE_UP_CPR_DEPR,2) as TRUE_UP_CPR_DEPR,
                    L_MONTH as GL_POSTING_MO_YR,
                  0 reserve_adjustment,
                  case
                    when (L_LS_ASSET.ESTIMATED_RESIDUAL = 0 and lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual',
                                                                                                                             L_LS_PEND_TRANS.COMPANY_ID))) =
                         'yes') /* WMD */
                     then
                     L_LS_ASSET.GUARANTEED_RESIDUAL_AMOUNT
                    else
                     round(L_LS_ASSET.ESTIMATED_RESIDUAL * L_LS_ASSET.FMV, 2)
                  end * L_APPROVED_RATE as estimated_salvage,
                  nvl((select nvl(f.total_depr, 0)
                        from (select l.set_of_books_id,
                                     l.month,
                                     sum(l.depr_exp_comp_curr) over(partition by l.set_of_books_id order by l.month) - l.depr_exp_comp_curr total_depr
                           from ls_depr_forecast l
                                join ls_asset a on a.ls_asset_id =
                                                   l.ls_asset_id
                           join ls_ilr i on i.ilr_id = a.ilr_id
                                             and i.current_revision =
                                                 l.revision
                          where l.ls_asset_id = A_LS_ASSET_ID
                                 and trunc(l.month, 'fmmonth') <=
                                     trunc(L_ACCT_MONTH, 'fmmonth')) f
                       where f.set_of_books_id = pb.set_of_books_id
                         and trunc(f.month, 'fmmonth') =
                             trunc(L_ACCT_MONTH, 'fmmonth')),
                      0) total_depr,
                  nvl((select nvl(curr.depr_expense,0) - nvl(appr.depr_expense,0)
                         from (
                                select set_of_books_id, sum(depr_exp_comp_curr) depr_expense
                                  from ls_depr_forecast 
                                 where ls_asset_id = A_LS_ASSET_ID
                                   and revision = L_NEW_REVISION
                                   and month between L_REMEASUREMENT_DATE and ADD_MONTHS(L_ACCT_MONTH,-1)
                                 group by set_of_books_id
                              ) curr,
                              (
                                select set_of_books_id, sum(depr_exp_comp_curr) depr_expense
                                  from ls_depr_forecast 
                                 where ls_asset_id = A_LS_ASSET_ID
                                   and revision = L_PRIOR_REVISION
                                   and month between L_REMEASUREMENT_DATE and ADD_MONTHS(L_ACCT_MONTH,-1)
                                 group by set_of_books_id
                              ) appr
                        where pb.set_of_books_id = curr.set_of_books_id
                          and pb.set_of_books_id = appr.set_of_books_id
                      ),
                      0) remeasure_depr 
             from DEPR_GROUP DG,
                  LS_PEND_SET_OF_BOOKS PB,
                  LS_FASB_CAP_TYPE_SOB_MAP MAP,
                  (select DDD.DEPR_METHOD_ID,
                          DDD.RATE,
                          DDD.SET_OF_BOOKS_ID,
                            DDD.EXPECTED_AVERAGE_LIFE,
                            ROW_NUMBER() OVER(partition by DDD.DEPR_METHOD_ID, DDD.SET_OF_BOOKS_ID order by DDD.EFFECTIVE_DATE desc) as THE_ROW
                       from DEPR_METHOD_RATES DDD) DMR
              where DG.DEPR_GROUP_ID = L_DG_ID
                and DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
                and PB.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
                and PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID
                and MAP.SET_OF_BOOKS_ID = PB.SET_OF_BOOKS_ID
                and MAP.LEASE_CAP_TYPE_ID = L_CAP_TYPE
                and DMR.THE_ROW = 1) B
      on (A.ASSET_ID = B.ASSET_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.GL_POSTING_MO_YR = B.GL_POSTING_MO_YR and A.DEPR_GROUP_ID = B.DEPR_GROUP_ID)
      when matched then
         update
         set NET_ADDS_AND_ADJUST   = NET_ADDS_AND_ADJUST + B.AMOUNT,
             ASSET_DOLLARS         = ASSET_DOLLARS + B.AMOUNT,
             ESTIMATED_SALVAGE     = case
                                       when (asset_dollars + b.amount) = 0 then
                                        0
                                       else
                                        B.ESTIMATED_SALVAGE / (asset_dollars + b.amount)
                                     END,
             RESERVE_ADJUSTMENT    = case
                                       when L_ADJ = 1 and L_IS_REMEASURE <> 1 and
                                            A.GL_POSTING_MO_YR = L_ACCT_MONTH and L_3033_COUNT > 0 then
                               A.RESERVE_ADJUSTMENT
                             else
                               B.RESERVE_ADJUSTMENT
                                     end,
             DEPR_EXP_ALLOC_ADJUST = A.DEPR_EXP_ALLOC_ADJUST +
                                     case when nvl(B.TRUE_UP_CPR_DEPR,2) <> 0 and upper(B.MID_PERIOD_METHOD) <> 'UOPR' then
                                       case when L_IS_REMEASURE = 1 then
                                         B.REMEASURE_DEPR
                                       else
                                         B.TOTAL_DEPR - A.BEG_RESERVE_MONTH -
                                         (A.RETIREMENTS + A.DEPR_EXP_ADJUST + A.SALVAGE_DOLLARS +
                                          A.COST_OF_REMOVAL + A.OTHER_CREDITS_AND_ADJUST + A.GAIN_LOSS +
                                          B.RESERVE_ADJUSTMENT)
                                       end  
                                     else
                                         0  
                             end
      when not matched then
      insert (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, INIT_LIFE, REMAINING_LIFE, ESTIMATED_SALVAGE, 
              BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, 
              BEG_RESERVE_MONTH, SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN, 
              RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS, DEPRECIATION_BASE, 
              CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE, YTD_DEPR_EXP_ADJUST, 
              PRIOR_YTD_DEPR_EXPENSE, PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB, MONTH_RATE, COMPANY_ID, 
              MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR, SALVAGE_EXPENSE, 
              SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, 
              IMPAIRMENT_ASSET_ACTIVITY_SALV, IMPAIRMENT_ASSET_BEGIN_BALANCE, DEPR_EXP_ALLOC_ADJUST) 
      values (B.ASSET_ID, B.SET_OF_BOOKS_ID, B.GL_POSTING_MO_YR, B.EXPECTED_LIFE, B.REMAINING_LIFE,
             case
               when b.amount = 0 then
                0
                       else
                B.ESTIMATED_SALVAGE / b.amount
             end, 0, B.AMOUNT, 0, 0, 0, B.AMOUNT, 0, 0, b.reserve_adjustment, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
             0, 0, 0, 0, 0, B.MONTH_RATE, B.COMPANY_ID, B.MID_PERIOD_METHOD, B.MID_PERIOD_CONV, B.DEPR_GROUP_ID, 
             B.DEPR_METHOD_ID, nvl(B.TRUE_UP_CPR_DEPR,2), 0, 0, 0, 0, 0, 0, 0,
             case
               when nvl(B.TRUE_UP_CPR_DEPR,2) <> 0 and upper(B.MID_PERIOD_METHOD) <> 'UOPR' then
                 B.TOTAL_DEPR - B.RESERVE_ADJUSTMENT
               else
                 0
             end);

      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG || ' Rows Updated: ' || sql%rowcount);

	
    /*OM to Cap indicator could be different for different set of books, so need to do this by SOB*/
    L_MSG := 'Looping though CPR Depr Updates';
	
    for L_SOBS in (select PB.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID, PB.POSTING_AMOUNT as AMOUNT
                       from LS_PEND_SET_OF_BOOKS PB
                      where PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID)
    loop
	  
	    
		if L_LS_PEND_TRANS.ACTIVITY_CODE = 11 then
		  
			SELECT CASE WHEN orig_is_om.fasb_cap_type_id in (4,5)
				AND remeasure_is_om.fasb_cap_type_id NOT in (4,5)
				THEN 1
				ELSE 0
				END
				INTO L_OM_TO_CAP
			  FROM ls_ilr_options orig_revision, ls_ilr_options remeasure_revision, ls_fasb_cap_type_sob_map orig_is_om, ls_fasb_cap_type_sob_map remeasure_is_om
			  WHERE orig_revision.ilr_id = L_LS_PEND_TRANS.ILR_ID
			  AND orig_revision.revision = L_PRIOR_REVISION
			  AND orig_revision.ilr_id = remeasure_revision.ilr_id
			  AND remeasure_revision.revision = L_NEW_REVISION
			  AND orig_is_om.set_of_books_id = L_SOBS.set_of_books_id
			  AND orig_revision.lease_cap_type_id = orig_is_om.lease_cap_type_id
			  AND remeasure_is_om.set_of_books_id = L_SOBS.set_of_books_id
			  AND remeasure_revision.lease_cap_type_id = remeasure_is_om.lease_cap_type_id;
		else
		  L_OM_TO_CAP := 0;
		end if;
	 
		-- Process updates to cpr_depr
		P_ADD_CPR_DEPR_UPDATES(L_ASSET_ID,
							   L_INIT_LIFE,
							   L_LS_PEND_TRANS,
							   L_MONTH,
							   L_ACCT_MONTH,
							   L_REMEASUREMENT_DATE,
							   L_OM_TO_CAP,
							   L_3033_COUNT,
							   L_SOBS.SET_OF_BOOKS_ID,
							   L_MSG);

	end loop;
	
    -- only do the below on initial adds
    if L_ADJ = 0 then
      L_MSG := 'Adding class codes';
      insert into CLASS_CODE_CPR_LEDGER
        (CLASS_CODE_ID, ASSET_ID, value)
        select CLASS_CODE_ID, L_ASSET_ID, value
          from LS_PEND_CLASS_CODE
         where LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID;

      --* Link the LS_ASSET to the CPR
      L_MSG := 'Connecting leased asset to CPR';
      insert into LS_CPR_ASSET_MAP
        (LS_ASSET_ID, ASSET_ID, EFFECTIVE_DATE)
      values
        (L_LS_PEND_TRANS.LS_ASSET_ID, L_ASSET_ID, L_MONTH);

      L_MSG := 'Connecting activity and depr group';
      insert into CPR_ACT_DEPR_GROUP
        (ASSET_ID, GL_POSTING_MO_YR, DEPR_GROUP_ID)
      values
        (L_ASSET_ID, L_MONTH, L_DG_ID);
    end if; -- not adjustment

      L_MSG := 'Updating Account Summary';
      merge into ACCOUNT_SUMMARY A
    using (select L_LS_PEND_TRANS.COMPANY_ID         as COMPANY,
                  PB.SET_OF_BOOKS_ID                 as SET_OF_BOOKS,
                  L_GL_ACCT_ID                       as ACCOUNT,
                  L_LS_PEND_TRANS.BUS_SEGMENT_ID     as BUSINESS_SEGMENT,
                  L_LS_PEND_TRANS.SUB_ACCOUNT_ID     as SUB_ACCOUNT,
                    L_LS_PEND_TRANS.UTILITY_ACCOUNT_ID as UTILITY_ACCOUNT,
                  L_MAJOR_LOC                        as MAJOR_LOCATION,
                  L_MONTH                            as GL_POSTING_MO_YR,
                  --GL_POSTING_MO_YR
                    (1 - L_ADJ) * PB.POSTING_AMOUNT * L_APPROVED_RATE as ADDITION,
                    L_ADJ * PB.POSTING_AMOUNT * L_APPROVED_RATE as ADJUSTMENTS
               from LS_PEND_SET_OF_BOOKS PB
              where PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID) B
      on (A.COMPANY_ID = B.COMPANY and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS and A.GL_ACCOUNT_ID = B.ACCOUNT and A.BUS_SEGMENT_ID = B.BUSINESS_SEGMENT and A.SUB_ACCOUNT_ID = B.SUB_ACCOUNT and A.UTILITY_ACCOUNT_ID = B.UTILITY_ACCOUNT and A.MAJOR_LOCATION_ID = B.MAJOR_LOCATION and A.GL_POSTING_MO_YR = B.GL_POSTING_MO_YR)
      when matched then
         update
         set A.ENDING_BALANCE = A.ENDING_BALANCE + B.ADDITION +
                                B.ADJUSTMENTS,
             A.ADDITIONS      = A.ADDITIONS + B.ADDITION,
             A.ADJUSTMENTS    = A.ADJUSTMENTS + B.ADJUSTMENTS
      when not matched then
         insert
        (SET_OF_BOOKS_ID, COMPANY_ID, GL_ACCOUNT_ID, BUS_SEGMENT_ID,
         SUB_ACCOUNT_ID, UTILITY_ACCOUNT_ID, MAJOR_LOCATION_ID,
         GL_POSTING_MO_YR, BEGINNING_BALANCE, ACCT_SUMM_STATUS, RETIREMENTS,
         ADDITIONS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS, ENDING_BALANCE)
         values
        (B.SET_OF_BOOKS, B.COMPANY, B.ACCOUNT, B.BUSINESS_SEGMENT,
         B.SUB_ACCOUNT, B.UTILITY_ACCOUNT, B.MAJOR_LOCATION,
         B.GL_POSTING_MO_YR, 0, 1, 0, B.ADDITION, 0, 0, B.ADJUSTMENTS,
         B.ADDITION + B.ADJUSTMENTS);

      L_MSG := 'Updating Depr Ledger';
      merge into DEPR_LEDGER A
    using (select DG2.DEPR_GROUP_ID  as DEPR_GROUP,
                    PB.SET_OF_BOOKS_ID as SET_OF_BOOKS,
                  L_MONTH            as GL_POSTING_MO_YR,
                  --GL_POSTING_MO_YR
                    (1 - L_ADJ) * PB.POSTING_AMOUNT * L_APPROVED_RATE as ADDITION,
                    L_ADJ * PB.POSTING_AMOUNT * L_APPROVED_RATE as ADJUSTMENTS
               from LS_PEND_SET_OF_BOOKS PB,
                  (select DG.DEPR_GROUP_ID,
                          DG.COMPANY_ID,
                          DMR.SET_OF_BOOKS_ID,
                            ROW_NUMBER() OVER(partition by DMR.SET_OF_BOOKS_ID, DMR.DEPR_METHOD_ID order by DMR.EFFECTIVE_DATE desc) as THE_ROW
                       from DEPR_METHOD_RATES DMR, DEPR_GROUP DG
                      where DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
                        and DG.DEPR_GROUP_ID = L_DG_ID) DG2
              where DG2.THE_ROW = 1
                and PB.SET_OF_BOOKS_ID = DG2.SET_OF_BOOKS_ID
                and PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID) B
      on (A.DEPR_GROUP_ID = B.DEPR_GROUP and A.GL_POST_MO_YR = B.GL_POSTING_MO_YR and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS)
      when matched then
         update
            set A.END_BALANCE = A.END_BALANCE + B.ADDITION + B.ADJUSTMENTS,
             A.ADDITIONS   = A.ADDITIONS + B.ADDITION,
                A.ADJUSTMENTS = A.ADJUSTMENTS + B.ADJUSTMENTS;

--      -- set the approved revision
--      update LS_ASSET
--         set APPROVED_REVISION = L_LS_PEND_TRANS.REVISION,
--             IN_SERVICE_DATE = NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE, L_MONTH), LS_ASSET_STATUS_ID = 3,
--          EXPECTED_LIFE = L_INIT_LIFE
--       where LS_ASSET_ID = A_LS_ASSET_ID;

    /* WMD Update Property Tax Date and Cost if they are null */
    L_MSG := 'Update Property Tax Date if null';
    update LS_ASSET
    set property_tax_date = in_service_date
    where property_tax_date is null
      and ls_asset_id = A_LS_ASSET_ID;

    L_MSG := 'Update Property Tax Amount if null';
    update ls_asset a
    set property_tax_amount =
    (select sum(cc.amount)
     from ls_component_charge cc, ls_component lc
     where cc.component_id = lc.component_id
      and cc.interim_interest_start_date is not null
               and nvl(lc.prop_tax_exempt, 0) <> 1
      and a.ls_asset_id = lc.ls_asset_id)
    WHERE property_tax_amount is null
      and ls_asset_id = A_LS_ASSET_ID; /* WMD we need to make sure that null is all they want */

      /* WMD Mark Components Posted */
    L_MSG := 'Marking components posted to GL';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      update ls_component_charge cc
       set posted_to_gl = 1
      where interim_interest_start_date is not null
        and posted_to_gl <> 1
       and component_id in
           (select lc.component_id
          from ls_asset la, ls_component lc
          where la.ls_asset_id = L_LS_PEND_TRANS.LS_ASSET_ID
            and lc.ls_asset_id = la.ls_asset_id
            and cc.component_id = lc.component_id);

      --For non-adjustments, arrears, negative payment shift, backdated in service ILRs need to create the missing payment lines/headers
    IF L_ADJ = 0 AND L_PRE_PAYMENT_SW = 0 AND L_PAYMENT_SHIFT < 0 AND
       NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE, L_MONTH) < L_ACCT_MONTH THEN
        L_MSG := 'Creating missing payment lines and headers';
		    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      for L_MISSING_PAY_MONTHS in (SELECT To_Date(Add_Months(Add_Months(L_ACCT_MONTH,
                                                                         L_PAYMENT_SHIFT),
                                                              LEVEL - 1)) month
                        FROM dual
                                   CONNECT BY LEVEL <
                                              Months_Between(L_ACCT_MONTH,
                                                             Add_Months(L_ACCT_MONTH,
                                                                        L_PAYMENT_SHIFT)) + 1
                                    ORDER BY MONTH ASC)
      LOOP
        L_MSG := PKG_LEASE_CALC.F_PAYMENT_CALC(L_LEASE_ID,
                                               L_LS_ASSET.COMPANY_ID,
                                               L_MISSING_PAY_MONTHS.month);

          if L_MSG <> 'OK' then
					  return L_MSG;
				  end if;
        END LOOP;
      END IF;
	  
	  
	  --Populate acct_month_approved and revision_app_order on ls_ilr_approval
	  L_MSG := 'Update LS ILR Approval Revision Order and Approval Accounting Month';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
	  
	  UPDATE ls_ilr_approval lia
	  set acct_month_approved = L_ACCT_MONTH,
	  revision_app_order = (select NVL(max(nvl(revision_app_order,0)),0) + 1 from ls_ilr_approval a where a.ilr_id = L_LS_PEND_TRANS.ILR_ID and a.revision <> L_NEW_REVISION )
	  where ilr_id = L_LS_PEND_TRANS.ILR_ID
	  and revision = L_NEW_REVISION;

      /* CJS 2/16/15 Need to readdress; ends log prematurely during transfers
      PKG_PP_LOG.P_END_LOG(); */
      L_MSG := 'OK';
      return L_MSG;
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_ADD_ASSET;

   -- Function to perform specific Leased Asset retirement
   -- return 1 on success, 0 on failure.  a_msg is an out param for messaging
   function F_RETIRE_ASSET(A_CPR_ASSET_ID        number,
                           A_ACCT_MONTH          date,
                           A_MONTH               date,
                           A_RETIREMENT_AMOUNT   number,
                           A_RETIREMENT_QUANTITY number,
                           A_GAIN_LOSS_AMOUNT    number,
                           A_DG_ID               number,
                           A_PT_ID               number,
                           A_MSG                 out varchar2) return number is
    L_LS_ASSET            LS_ASSET%rowtype;
    L_LS_ASSET_SCH        LS_ASSET_SCHEDULE%rowtype;
    L_GL_ACCT_ID          GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_ST_ACCT_ID          GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_LT_ACCT_ID          GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_PO_DR_ACCT_ID       GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_INTACC_ACCT_ID      GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_IMP_ACCUM_ACCT_ID   GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_RESERVE_ACCT_ID     GL_ACCOUNT.GL_ACCOUNT_ID%type;
    L_AMT_OBL             number(22, 2);
    L_AMT_OBL_LT          number(22, 2);
    L_PO_GL_AMT_OBL       number(22, 2);
    L_UNACCRUED_INT_AMT   number(22, 2);
    L_PCT                 number(22, 12);
    L_RTN                 number;
    L_GL_JE_CODE          varchar2(35);
    L_ILR_ID              number;
    L_STATUS              varchar2(4000);
    L_SQLS                varchar2(2000);
    rtn                   varchar2(5000);
    L_SCH_MONTH           date;
    L_ASSET_IDS           PKG_LEASE_CALC.NUM_ARRAY;
    L_PEND_TRANS_COUNTER  NUMBER;
    L_CURRENCY_FROM       pend_transaction.currency_from%type;
    L_CURRENCY_TO         pend_transaction.currency_to%type;
    L_EXCHANGE_RATE       pend_transaction.exchange_rate%type;
    L_RETIREMENT_QUANTITY number;
    L_CONTRACT_JES  varchar2(100);
    L_JE_METHOD_SOB_CHECK number;
    L_APPROVED_REVISION number;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
    A_MSG := 'Starting to retire the leased asset for month: ' ||
             TO_CHAR(A_MONTH, 'yyyymm');
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Accounting Month: ' || TO_CHAR(A_ACCT_MONTH, 'yyyymm');
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Quantity: ' || TO_CHAR(A_RETIREMENT_QUANTITY);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Amount: ' || TO_CHAR(A_RETIREMENT_AMOUNT);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Gain / Loss: ' || TO_CHAR(A_GAIN_LOSS_AMOUNT);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Associated Pending Transaction: ' || TO_CHAR(A_PT_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      --Get our LS_ASSET
      A_MSG := 'Get the leased asset record';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      select A.*
        into L_LS_ASSET
        from LS_ASSET A, LS_CPR_ASSET_MAP C
       where C.ASSET_ID = A_CPR_ASSET_ID
         and C.LS_ASSET_ID = A.LS_ASSET_ID;

    A_MSG := 'Checking to see if depreciation has been calculated for the month';
      select count(1)
      into l_rtn
      from ls_process_control
      where company_id = L_LS_ASSET.COMPANY_ID
        and gl_posting_mo_yr = A_MONTH
        and depr_approved is not null;

      if l_rtn <> 1 then
      A_MSG := 'You cannot retire this asset yet because depreciation has not been calculated and approved for the out of service month.';
      return - 1;
      end if;

      /* WMD */
    A_MSG := 'Getting GL Accounts';
    select IA.CAP_ASSET_ACCOUNT_ID,
           IA.ST_OBLIG_ACCOUNT_ID,
           IA.LT_OBLIG_ACCOUNT_ID,
           IA.PO_DEBIT_ACCOUNT_ID,
           IA.INT_ACCRUAL_ACCOUNT_ID
      into L_GL_ACCT_ID,
           L_ST_ACCT_ID,
           L_LT_ACCT_ID,
           L_PO_DR_ACCT_ID,
           L_INTACC_ACCT_ID
        from LS_ILR_ACCOUNT IA, LS_ASSET LA
       where LA.ILR_ID = IA.ILR_ID
         and LA.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID;

      if L_LS_ASSET.QUANTITY = 0 then
         L_RETIREMENT_QUANTITY := 0;
       else
         L_RETIREMENT_QUANTITY := A_RETIREMENT_QUANTITY;
       end if;

      -- is this asset fully retired?
      A_MSG := 'Determine Percentage for Asset Retirement';
      if L_LS_ASSET.QUANTITY + L_RETIREMENT_QUANTITY = 0 then
         -- fully retired
         -- set the status to say retired
         L_LS_ASSET.LS_ASSET_STATUS_ID := 4;
      L_PCT                         := 1;
      else
        L_PCT := -1 * L_RETIREMENT_QUANTITY / L_LS_ASSET.QUANTITY;
      end if;

      A_MSG := 'Updating LS ASSET';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      -- update the capital cost and the quantity based on the retirement amount
      update LS_ASSET
       set QUANTITY           = QUANTITY + L_RETIREMENT_QUANTITY,
           LS_ASSET_STATUS_ID = L_LS_ASSET.LS_ASSET_STATUS_ID,
           RETIREMENT_DATE    = A_MONTH
       where LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID;

		  /* WMD */
		  /* CJS 4/21/17 Post isn't using external values for category, update here */
      A_MSG := 'Updating Asset ID on GL Transaction to LS Asset ID';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      update gl_transaction
       set asset_id     = L_LS_ASSET.LS_ASSET_ID,
           source       = 'LESSEE',
           description  = 'TRANS TYPE: ' || trans_type,
           gl_status_id = to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status',
                                                                                               L_LS_ASSET.COMPANY_ID)),
                                        1))
     where month = A_ACCT_MONTH
         and asset_id = A_CPR_ASSET_ID
         and originator = 'POST'
         and trans_type in (3025, 3026, 3030, 3031);

      PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount || ' rows updated');

      select NVL(E.GL_JE_CODE, 'LAMRETIRE')
        into L_GL_JE_CODE
        from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
       where E.JE_ID = G.JE_ID
         and G.PROCESS_ID = 'LAM RETIREMENTS';

	  
	  PKG_PP_LOG.P_WRITE_MESSAGE('Getting Contract Currency JE System Control');
	  L_CONTRACT_JES:=lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_company('Lease: Contract Currency JEs', L_LS_ASSET.COMPANY_ID)));
	  
    
	  if L_CONTRACT_JES IS NULL then
		   L_CONTRACT_JES := 'no';
	  end if;
	  
     PKG_PP_LOG.P_WRITE_MESSAGE('Contract Currency JE System Control:' || L_CONTRACT_JES);
     
	 if L_CONTRACT_JES = 'yes' then
		A_MSG := 'Checking JE Method Set of Books' ;
		PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
		L_JE_METHOD_SOB_CHECK := PKG_LEASE_COMMON.F_JE_METHOD_SOB_CHECK(A_MSG);
		
		if L_JE_METHOD_SOB_CHECK > 0 then
			  A_MSG:='You cannot book contract currency JEs with more than one set of books configured per JE Method';
			  PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
			  return -1;
		end if;
	 end if;
	 
	 A_MSG := 'Updating POST GL Transactions to Contract Currency: for pend_trans/asset_id: ' || nvl(TO_CHAR(A_PT_ID),'null') || '/' || nvl(TO_CHAR(L_LS_ASSET.LS_ASSET_ID),'null');

	 if  L_CONTRACT_JES = 'yes' then
	  select approved_revision into L_APPROVED_REVISION from ls_asset where ls_asset_id = L_LS_ASSET.LS_ASSET_ID;
      PKG_PP_LOG.P_WRITE_MESSAGE('Approved_revision: ' || L_APPROVED_REVISION );
   

      A_MSG := 'Updating POST GL Transactions to Contract Currency: for pend_trans/company/month/GL JE code: ' || nvl(TO_CHAR(A_PT_ID),'null') || '/' || nvl(TO_CHAR(L_LS_ASSET.COMPANY_ID),'null') ||
      '/' || nvl(TO_CHAR(A_ACCT_MONTH),'null') ;
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
	  -- Convert Post JEs to Contract Currency
			L_RTN := PKG_LEASE_COMMON.F_MC_POST_CONTRACT_CURR(A_PT_ID,
					L_LS_ASSET.COMPANY_ID,
					A_ACCT_MONTH,
					A_MSG);

        if L_RTN = -1 then
           PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           return -1;
        end if;
	 end if; 
	  
      -- normal asset retirement transactions are handled by POST
      -- ie 3025 and 3026.
      -- in this function only handle the lease specific entries (termination_penalty_amount: 3036 and 3037)


		  -- get exchange rate information'
		  SELECT currency_from, currency_to, exchange_rate
		  INTO L_CURRENCY_FROM, L_CURRENCY_TO, L_EXCHANGE_RATE
		  FROM pend_transaction
		  WHERE pend_trans_id = A_PT_ID;


      -- loop over the set of books eligible for this asset
      A_MSG := 'Looping over set of books';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      for L_SOB in (select CS.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID, FASB.BOOK_SUMMARY_ID as BS_ID, 
                    CSC.CURRENCY_ID AS COMPANY_CURRENCY_ID, NVL(WAR.NET_WEIGHTED_AVG_RATE,0) as NET_WEIGHTED_AVG_RATE
                      from COMPANY_SET_OF_BOOKS CS,
                           SET_OF_BOOKS         S,
                           LS_ILR_OPTIONS       ILR,
                           LS_LEASE_CAP_TYPE    LCT,
                           LS_FASB_CAP_TYPE_SOB_MAP FASB,
                           CURRENCY_SCHEMA CSC,
                           LS_ILR_WEIGHTED_AVG_RATES WAR
                     where CS.COMPANY_ID = L_LS_ASSET.COMPANY_ID
                       and CS.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                       and ILR.ILR_ID = L_LS_ASSET.ILR_ID
                       and ILR.REVISION = L_LS_ASSET.APPROVED_REVISION
                       and LCT.LS_LEASE_CAP_TYPE_ID = ILR.LEASE_CAP_TYPE_ID
                       and LCT.LS_LEASE_CAP_TYPE_ID = FASB.LEASE_CAP_TYPE_ID
                       and FASB.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                       and CSC.COMPANY_ID = L_LS_ASSET.COMPANY_ID
                       and WAR.ILR_ID = ILR.ILR_ID
                       and WAR.REVISION = ILR.REVISION
                       and WAR.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID)
      loop
         A_MSG := '   Set of Books: ' || TO_CHAR(L_SOB.SET_OF_BOOKS_ID);
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         -- get the current month's row for the schedule for the current revision
         A_MSG := 'Get the current month schedule for leased asset id: ' ||
                  TO_CHAR(L_LS_ASSET.LS_ASSET_ID);
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

         select max(s.month)
               into L_SCH_MONTH
               from LS_ASSET_SCHEDULE S
              where S.MONTH <= A_MONTH
                and S.REVISION = L_LS_ASSET.APPROVED_REVISION
                and S.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
                and S.SET_OF_BOOKS_ID = L_SOB.SET_OF_BOOKS_ID;

         select S.*
           into L_LS_ASSET_SCH
           from LS_ASSET_SCHEDULE S
          where S.MONTH = L_SCH_MONTH
            and S.REVISION = L_LS_ASSET.APPROVED_REVISION
            and S.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
            and S.SET_OF_BOOKS_ID = L_SOB.SET_OF_BOOKS_ID;

         -- is there a termination penalty
         -- CJS 4/20/15 Only book actual termination amount
         A_MSG := A_MSG || '   Original Termination Amount: ' ||
                  TO_CHAR(NVL(L_LS_ASSET.TERMINATION_PENALTY_AMOUNT, 0));
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         A_MSG := A_MSG || '   Actual Termination Amount: ' ||
                  TO_CHAR(NVL(L_LS_ASSET.ACTUAL_TERMINATION_AMOUNT, 0));
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      if NVL(L_LS_ASSET.ACTUAL_TERMINATION_AMOUNT, 0) /*- L_LS_ASSET.TERMINATION_PENALTY_AMOUNT*/
         <> 0 then
            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                               3036,
                                               L_LS_ASSET.ACTUAL_TERMINATION_AMOUNT /*- L_LS_ASSET.TERMINATION_PENALTY_AMOUNT*/,
                                               0,
                                               A_DG_ID,
                                               L_LS_ASSET.WORK_ORDER_ID,
                                               0,
                                               1,
                                               A_PT_ID,
                                               L_LS_ASSET.COMPANY_ID,
                                               A_ACCT_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               L_SOB.SET_OF_BOOKS_ID,
											   L_EXCHANGE_RATE,
											   L_CURRENCY_FROM,
											   L_CURRENCY_TO,
                                               A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;

            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                               3037,
                                               L_LS_ASSET.ACTUAL_TERMINATION_AMOUNT /*- L_LS_ASSET.TERMINATION_PENALTY_AMOUNT*/,
                                               0,
                                               A_DG_ID,
                                               L_LS_ASSET.WORK_ORDER_ID,
                                               0,
                                               1,
                                               A_PT_ID,
                                               L_LS_ASSET.COMPANY_ID,
                                               A_ACCT_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_SOB.SET_OF_BOOKS_ID,
											   L_EXCHANGE_RATE,
											   L_CURRENCY_FROM,
											   L_CURRENCY_TO,
                                               A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;
         end if; -- termination penalty

         -- Purchase Option Amount
         A_MSG := A_MSG || '   Purchase Option Amount: ' ||
                  TO_CHAR(NVL(L_LS_ASSET.ACTUAL_PURCHASE_AMOUNT, 0));
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         if NVL(L_LS_ASSET.ACTUAL_PURCHASE_AMOUNT, 0) <> 0 then
            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3069,
                                                  L_LS_ASSET.ACTUAL_PURCHASE_AMOUNT,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  L_PO_DR_ACCT_ID,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  L_EXCHANGE_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;

            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3070,
                                                  L_LS_ASSET.ACTUAL_PURCHASE_AMOUNT,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  0,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  L_EXCHANGE_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;

         if L_CONTRACT_JES = 'no' then 
            -- Create the PO Gain/Loss JE's
            --Trans Type: 3071 - Lease PO Gain/Loss (Asset) Debit A_GAIN_LOSS_AMOUNT is in company contract from POST
            L_RTN := PKG_LEASE_COMMON.F_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3071,
                                                  -A_GAIN_LOSS_AMOUNT,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  0,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;

            --Trans Type: 3072 - Lease PO Gain/Loss (Asset) Credit A_GAIN_LOSS_AMOUNT is in company contract from POST
            L_RTN := PKG_LEASE_COMMON.F_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3072,
                                                  -A_GAIN_LOSS_AMOUNT,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  L_PO_DR_ACCT_ID,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;

         ELSE  /*Book Contract Currency JEs*/
                        -- Create the PO Gain/Loss JE's
            --Trans Type: 3071 - Lease PO Gain/Loss (Asset) Debit A_GAIN_LOSS_AMOUNT is in company contract from POST
            if L_SOB.NET_WEIGHTED_AVG_RATE = 0 then
			      A_MSG := 'ERROR: Aborting because net weighted avg rate for contract currency conversion = 0'
				  || '. For pend_trans/asset_id: ' || nvl(TO_CHAR(A_PT_ID),'null') || '/' || nvl(TO_CHAR(L_LS_ASSET.LS_ASSET_ID),'null');
		          return -1;
			end if;
			
			L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3071,
                                                  -(A_GAIN_LOSS_AMOUNT/L_SOB.NET_WEIGHTED_AVG_RATE),
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  0,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  L_SOB.NET_WEIGHTED_AVG_RATE,
                                                  L_LS_ASSET.CONTRACT_CURRENCY_ID,
                                                  L_SOB.COMPANY_CURRENCY_ID,
                                                  A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;

            --Trans Type: 3072 - Lease PO Gain/Loss (Asset) Credit A_GAIN_LOSS_AMOUNT is in company contract from POST
            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3072,
                                                  -(A_GAIN_LOSS_AMOUNT/L_SOB.NET_WEIGHTED_AVG_RATE),
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  L_PO_DR_ACCT_ID,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  L_SOB.NET_WEIGHTED_AVG_RATE,
                                                  L_LS_ASSET.CONTRACT_CURRENCY_ID,
                                                  L_SOB.COMPANY_CURRENCY_ID,
                                                  A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;
         END IF; 

            --3074 - Lease PO Gain/Loss (Liability) Debit
            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3074,
                                                  L_LS_ASSET_SCH.END_LIABILITY,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  0,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  L_EXCHANGE_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;

            --3075 - Lease PO Gain/Loss (Liability) Credit
            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3075,
                                                  L_LS_ASSET_SCH.END_LIABILITY,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  L_PO_DR_ACCT_ID,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  L_EXCHANGE_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;
         end if; -- Purchase Option

		 if NVL(L_LS_ASSET_SCH.END_ACCUM_IMPAIR, 0) <> 0 then
            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                               3081,
                                               L_LS_ASSET_SCH.END_ACCUM_IMPAIR,
                                               0,
                                               A_DG_ID,
                                               L_LS_ASSET.WORK_ORDER_ID,
                                               L_IMP_ACCUM_ACCT_ID,
                                               1,
                                               A_PT_ID,
                                               L_LS_ASSET.COMPANY_ID,
                                               A_ACCT_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               L_SOB.SET_OF_BOOKS_ID,
											   L_EXCHANGE_RATE,
											   L_CURRENCY_FROM,
											   L_CURRENCY_TO,
                                               A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;
			
			--Get Depr Group Reserve ACCT_DISTRIB
			select RESERVE_ACCT_ID
			into L_RESERVE_ACCT_ID
			from depr_group
			where depr_group_id = a_dg_id;

            L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                               3084,
                                               L_LS_ASSET_SCH.END_ACCUM_IMPAIR,
                                               0,
                                               A_DG_ID,
                                               L_LS_ASSET.WORK_ORDER_ID,
                                               L_RESERVE_ACCT_ID,
                                               1,
                                               A_PT_ID,
                                               L_LS_ASSET.COMPANY_ID,
                                               A_ACCT_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_SOB.SET_OF_BOOKS_ID,
											   L_EXCHANGE_RATE,
											   L_CURRENCY_FROM,
											   L_CURRENCY_TO,
                                               A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;
         end if; -- Reverse Impairment

         -- check to see if the ilr is capital for this set of books
         L_SQLS := 'select to_char(basis_' || TO_CHAR(L_SOB.BS_ID) ||
                   '_indicator) from set_of_books s where set_of_books_id = ' ||
                   TO_CHAR(L_SOB.SET_OF_BOOKS_ID);

         A_MSG := '   Set of Books (Cap or Operating): ' || L_SQLS;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

         L_RTN := TO_NUMBER(PP_MISC_PKG.DYNAMIC_SELECT(L_SQLS));
         PKG_PP_LOG.P_WRITE_MESSAGE('      RETURN: ' || TO_CHAR(L_RTN));

         -- plus additional g/l for the st / lt obligation clearing (3027 G/L CR, 3028 ST DR, 2029 LT DR)
         if L_RTN = 1 then
            A_MSG := '     CAPITAL and Early Retirement: ' || L_SQLS;
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            -- this is an early retirement
            -- so calc additional G/L by clearing out the obligations
        L_AMT_OBL := L_PCT * (L_LS_ASSET_SCH.END_OBLIGATION -
                     NVL(L_LS_ASSET_SCH.END_LT_OBLIGATION, 0));

            -- If retiring in current open month, need to subtract the obligation reclass
            if A_ACCT_MONTH = A_MONTH then
          L_AMT_OBL := L_AMT_OBL -
                       NVL(L_LS_ASSET_SCH.OBLIGATION_RECLASS, 0);
            end if;

            if L_AMT_OBL <> 0 then
               -- book the G/L/ credit
               L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3027,
                                                  L_AMT_OBL,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  0,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
											   L_EXCHANGE_RATE,
											   L_CURRENCY_FROM,
											   L_CURRENCY_TO,
                                                  A_MSG);
               if L_RTN = -1 then

                  return 0;
               end if;

               -- book the ST CLEAR DR
               L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3028,
                                                  L_AMT_OBL,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  L_ST_ACCT_ID,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
											   L_EXCHANGE_RATE,
											   L_CURRENCY_FROM,
											   L_CURRENCY_TO,
                                                  A_MSG);
               if L_RTN = -1 then

                  return 0;
               end if;
            end if; -- end clearing out short term obligation

        L_AMT_OBL := L_PCT * L_LS_ASSET_SCH.END_LT_OBLIGATION;

            -- If retiring in current open month, need to add the obligation reclass to long term
            if A_ACCT_MONTH = A_MONTH then
          L_AMT_OBL_LT := L_AMT_OBL +
                          NVL(L_LS_ASSET_SCH.OBLIGATION_RECLASS, 0);
            else
              L_AMT_OBL_LT := L_AMT_OBL;
            end if;

            if NVL(L_AMT_OBL_LT, 0) <> 0 then
               -- book the G/L/ credit
               L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3027,
                                                  L_AMT_OBL_LT,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  0,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  L_EXCHANGE_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  A_MSG);
               if L_RTN = -1 then

                  return 0;
               end if;

               -- book the LT CLEAR DR
               L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3029,
                                                  L_AMT_OBL_LT,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  L_LT_ACCT_ID,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  L_EXCHANGE_RATE,
                                                  L_CURRENCY_FROM,
                                                  L_CURRENCY_TO,
                                                  A_MSG);
               if L_RTN = -1 then

                  return 0;
               end if;
            end if; -- end clearing out long term obligation

            -- Unaccrued Interest JE's: END_LT_OBLIGATION - END_LT_LIABILITY
        L_UNACCRUED_INT_AMT := NVL(L_LS_ASSET_SCH.END_LT_OBLIGATION, 0) -
                               NVL(L_LS_ASSET_SCH.END_LT_LIABILITY, 0);

            -- If retiring in current open month, need to add the unaccrued interest reclass
            if A_ACCT_MONTH = A_MONTH then
          L_UNACCRUED_INT_AMT := L_UNACCRUED_INT_AMT +
                                 NVL(L_LS_ASSET_SCH.UNACCRUED_INTEREST_RECLASS,
                                     0);
            end if;

            A_MSG := 'Creating Journal (Unaccrued Interest Debit)';
        if L_UNACCRUED_INT_AMT <> 0 then
          /* WMD */
              PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                3076,
                                L_UNACCRUED_INT_AMT,
                                L_ST_ACCT_ID,
                                A_DG_ID,
                                L_LS_ASSET.WORK_ORDER_ID,
                                L_ST_ACCT_ID,
                                0,
                                A_PT_ID,
                                L_LS_ASSET.COMPANY_ID,
                                A_ACCT_MONTH,
                                1,
                                L_GL_JE_CODE,
                                L_SOB.SET_OF_BOOKS_ID,
                                L_EXCHANGE_RATE,
                                L_CURRENCY_FROM,
                                L_CURRENCY_TO,
                                A_MSG);
               if L_RTN = -1 then
                return A_MSG;
               end if;
            end if;

            A_MSG := 'Creating Journal (Unaccrued Interest Credit)';
        if L_UNACCRUED_INT_AMT <> 0 then
          /* WMD */
              PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                3077,
                                L_UNACCRUED_INT_AMT,
                                L_LT_ACCT_ID,
                                A_DG_ID,
                                L_LS_ASSET.WORK_ORDER_ID,
                                L_LT_ACCT_ID,
                                0,
                                A_PT_ID,
                                L_LS_ASSET.COMPANY_ID,
                                A_ACCT_MONTH,
                                0,
                                L_GL_JE_CODE,
                                L_SOB.SET_OF_BOOKS_ID,
                                L_EXCHANGE_RATE,
                                L_CURRENCY_FROM,
                                L_CURRENCY_TO,
                                A_MSG);
               if L_RTN = -1 then
                return A_MSG;
               end if;
            end if;

            -- Accrued Interest JE's: END_LIABILITY - END_OBLIGATION
            A_MSG := 'Creating Journal (Accrued Interest Debit)';
        if NVL(L_LS_ASSET_SCH.END_LIABILITY, 0) -
           NVL(L_LS_ASSET_SCH.END_OBLIGATION, 0) <> 0 then
          /* WMD */
              PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                3078,
                                                NVL(L_LS_ASSET_SCH.END_LIABILITY,
                                                    0) - NVL(L_LS_ASSET_SCH.END_OBLIGATION,
                                                             0),
                                L_INTACC_ACCT_ID,
                                A_DG_ID,
                                L_LS_ASSET.WORK_ORDER_ID,
                                L_INTACC_ACCT_ID,
                                0,
                                A_PT_ID,
                                L_LS_ASSET.COMPANY_ID,
                                A_ACCT_MONTH,
                                1,
                                L_GL_JE_CODE,
                                L_SOB.SET_OF_BOOKS_ID,
                                L_EXCHANGE_RATE,
                                L_CURRENCY_FROM,
                                L_CURRENCY_TO,
                                A_MSG);
               if L_RTN = -1 then
                return A_MSG;
               end if;
            end if;

            A_MSG := 'Creating Journal (Accrued Interest Gain/Loss Credit)';
        if NVL(L_LS_ASSET_SCH.END_LIABILITY, 0) -
           NVL(L_LS_ASSET_SCH.END_OBLIGATION, 0) <> 0 then
          /* WMD */
              PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
              L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                3027,
                                                NVL(L_LS_ASSET_SCH.END_LIABILITY,
                                                    0) - NVL(L_LS_ASSET_SCH.END_OBLIGATION,
                                                             0),
                                0,
                                A_DG_ID,
                                L_LS_ASSET.WORK_ORDER_ID,
                                0,
                                0,
                                A_PT_ID,
                                L_LS_ASSET.COMPANY_ID,
                                A_ACCT_MONTH,
                                0,
                                L_GL_JE_CODE,
                                L_SOB.SET_OF_BOOKS_ID,
                                L_EXCHANGE_RATE,
                                L_CURRENCY_FROM,
                                L_CURRENCY_TO,
                                A_MSG);
               if L_RTN = -1 then
                return A_MSG;
               end if;
            end if;

         end if; -- end if capital and early retirement (to calc additional gain loss)
      end loop; -- loop over set of books

      if L_LS_ASSET.LS_ASSET_STATUS_ID = 4 then
        if L_LS_ASSET.ACTUAL_RESIDUAL_AMOUNT <> 0 then
          A_MSG := 'Processing actual residual amount';
          PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
          L_ASSET_IDS(1) := L_LS_ASSET.LS_ASSET_ID;

            A_MSG := PKG_LEASE_CALC.F_PROCESS_RESIDUAL(A_LS_ASSET_IDS => L_ASSET_IDS,
                                                       A_END_LOG      => 0);
          if A_MSG <> 'OK' then
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            return 0;
          end if;
        end if;
      end if;

     /* WMD 20160809 DO NOT DO THE PROCESSING BELOW IF WE STILL HAVE RETIREMENT TRANSACTIONS PENDING */
     A_MSG := 'Checking for other pending transactions associated with this ILR';
     SELECT COUNT(1)
     INTO L_PEND_TRANS_COUNTER
     FROM PEND_TRANSACTION
     WHERE LDG_ASSET_ID in
           (SELECT ASSET_ID
                           FROM LS_CPR_ASSET_MAP
             WHERE LS_ASSET_ID IN
                   (SELECT LS_ASSET_ID
                      FROM LS_ASSET
                     WHERE ILR_ID = L_LS_ASSET.ILR_ID
                       AND LS_ASSET_ID <> L_LS_ASSET.LS_ASSET_ID))
      and trim(activity_code) = 'URGL';

    IF L_PEND_TRANS_COUNTER <> 0 THEN
      A_MSG := 'Other pending transactions exist for this ILR... Waiting until all are posted to send for approval';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    ELSE
      -- Handle ILR Status changes if all assets under the ILR are retired
      L_RTN := 0;
      select count(1)
        into L_RTN
        from LS_ASSET
       where ILR_ID = L_LS_ASSET.ILR_ID
         and LS_ASSET_STATUS_ID <> 4;

      if L_RTN = 0 then
         A_MSG := 'Retiring the ILR';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

         update LS_ILR
           set ILR_STATUS_ID     = 3,
             funding_status_id = 5
         where ILR_ID = L_LS_ASSET.ILR_ID;
      end if;

  END IF; /* END WMD CHANGES 20160809 */

      --POST.exe needs to be checking for a return value of 1.

  /* CJS Adding call to retirement revision function; 0 is retirement, 1 is transfer */
    if L_PCT = 1 then
      --only create retirement revision if asset is fully retiring
      A_MSG := 'Creating retirement revision';
    PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      L_RTN := PKG_LEASE_ILR.F_RETIREREVISION(L_LS_ASSET.ILR_ID,
                                              L_LS_ASSET.LS_ASSET_ID,
                                              A_MONTH,
                                              0,
                                              A_ACCT_MONTH);
    if L_RTN <> 1 then
       PKG_PP_LOG.P_WRITE_MESSAGE(L_RTN);
       PKG_PP_LOG.P_END_LOG();
       return 0;
    end if;
      A_MSG := 'Clear out Tax Rows for Future Months';
    delete from ls_monthly_tax
    where ls_asset_id = L_LS_ASSET.LS_ASSET_ID
         and gl_posting_mo_yr > A_MONTH;
         
    delete from ls_asset_schedule_tax
     where ls_asset_id = L_LS_ASSET.LS_ASSET_ID
       and revision = L_LS_ASSET.APPROVED_REVISION
       and gl_posting_mo_yr > A_MONTH;

   if L_LS_ASSET_SCH.END_LT_OBLIGATION > 0 then
   update ls_asset
           set early_term_end_lt_obligation = nvl(L_LS_ASSET_SCH.END_LT_OBLIGATION,
                                                  0)
   where ls_asset_id = L_LS_ASSET.LS_ASSET_ID;
   end if;
  end if;

    A_MSG := F_FOOT_DEPR(A_CPR_ASSET_ID, A_ACCT_MONTH);

  IF A_MSG <> 'OK' THEN
      RETURN - 1;
  END IF;

  PKG_PP_LOG.P_END_LOG();

      return 1;
   exception
      when others then
         A_MSG := SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || A_MSG);
     PKG_PP_LOG.P_END_LOG();
         return 0;
   end F_RETIRE_ASSET;

   -- Function to perform specific Leased Asset transfer from
   function F_TRANSFER_ASSET_FROM(A_CPR_ASSET_ID number,
                                  A_AMOUNT       number,
                                  A_QUANTITY     number,
                                  A_PT_ID        number,
                                  A_MSG          out varchar2) return number is
      L_LS_ASSET_ID number;
      OLD_QUANTITY  number;
      L_REVISION    number;
      L_ILR_ID      number;
      L_STATUS      varchar2(4000);
      L_RTN         number;

   begin
      /*      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());

            A_MSG := 'Starting to transfer asset from';
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            A_MSG := '   Quantity: ' || TO_CHAR(A_QUANTITY);
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            A_MSG := '   Amount: ' || TO_CHAR(A_AMOUNT);
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            A_MSG := '   CPR Asset ID: ' || TO_CHAR(A_CPR_ASSET_ID);
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            A_MSG := '   Associated Pending Transaction: ' || TO_CHAR(A_PT_ID);
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

          -- NO MAJOR PROCESSING HERE...
          -- all processing happens on the TO side

            A_MSG := 'DONE';
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);


            --Handle rounding... somehow...
      */
      A_MSG := 'DONE';
      return 1;
   exception
      when others then
         A_MSG := SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || A_MSG);

      return - 1;
   end F_TRANSFER_ASSET_FROM;

   -- Function to perform specific Leased Asset transfer to
   function F_TRANSFER_ASSET_TO(A_CPR_ASSET_ID       number,
                                A_AMOUNT             number,
                                A_QUANTITY           number,
                                A_COMPANY_ID         number,
                                A_BUS_SEGMENT_ID     number,
                                A_UTILITY_ACCOUNT_ID number,
                                A_SUB_ACCOUNT_ID     number,
                                A_FUNC_CLASS_ID      number,
                                A_PROPERTY_GROUP_ID  number,
                                A_RETIREMENT_UNIT_ID number,
                                A_ASSET_LOCATION_ID  number,
                                A_DG_ID              number,
                                A_PT_ID              number,
                               A_MSG                out varchar2)
    return number is
      L_LS_ASSET_ID_FROM      number;
      L_LS_ASSET_ID_TO        number;
      L_REVISION_FROM         number;
      L_REVISION_TO           number;
      L_CURRENT_REVISION_FROM number;
      L_TO_ILR_ID             number;
      L_FROM_ILR_ID           number;
      L_WO_ID                 number;
      L_STATUS                varchar2(4000);
      L_RTN                   number;
      L_PCT                   number(22, 8);
      L_PEND_TRANS_TO         PEND_TRANSACTION%rowtype;
      L_PEND_TRANS_FROM       PEND_TRANSACTION%rowtype;
      L_MONTH                 date;
      COMPANY_FIELD varchar2(35);
      V_COUNTER number;
      V_COLUMN_COUNT number;
      SQLS varchar2(20000);
      L_IN_SVC_DATE date;
      L_GL_JE_CODE VARCHAR2(36);
      L_IS_OM NUMBER;
	  L_CONTRACT_JES_TO  varchar2(100);
	  L_CONTRACT_JES_FROM  varchar2(100);
	  L_JE_METHOD_SOB_CHECK number;
	  
   begin
      --G_SEND_JES:=FALSE;
      --Update the lessee tables to the new CPR attributes
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_MSG := 'Starting to transfer asset to';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Quantity: ' || TO_CHAR(A_QUANTITY);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Amount: ' || TO_CHAR(A_AMOUNT);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   CPR Asset ID: ' || TO_CHAR(A_CPR_ASSET_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   TO Company ID: ' || TO_CHAR(A_COMPANY_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Associated Pending Transaction: ' || TO_CHAR(A_PT_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      -- the ldg_asset_id is the to side transaction
      A_MSG := 'GET to pending transaction';
    select *
      into L_PEND_TRANS_TO
      from PEND_TRANSACTION
     where LDG_ASSET_ID = A_PT_ID;

      -- the pend_trans_id is the from side transaction
      A_MSG := 'GET from pending transaction';
    select *
      into L_PEND_TRANS_FROM
      from PEND_TRANSACTION
     where PEND_TRANS_ID = A_PT_ID;

    L_MONTH := L_PEND_TRANS_FROM.GL_POSTING_MO_YR;
      -- THE FROM SIDE
      A_MSG := 'GET from leased asset';
      select LS_ASSET_ID
        into L_LS_ASSET_ID_FROM
        from LS_CPR_ASSET_MAP
       where ASSET_ID = L_PEND_TRANS_FROM.LDG_ASSET_ID;

      A_MSG := 'GET percent transferred';

      select A_QUANTITY / F.QUANTITY, F.ILR_ID, F.APPROVED_REVISION
        into L_PCT, L_FROM_ILR_ID, L_CURRENT_REVISION_FROM
        from LS_ASSET F
       where F.LS_ASSET_ID = L_LS_ASSET_ID_FROM;

      A_MSG := 'FROM Leased Asset: ' || TO_CHAR(L_LS_ASSET_ID_FROM);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := 'PERCENT Transferred: ' || TO_CHAR(L_PCT);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      /* WMD we need in svc date so we don't lose component rows */
    A_MSG := 'Getting in service date from ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      select trunc(EST_IN_SVC_DATE, 'month')
      into L_IN_SVC_DATE
      from ls_ilr ilr, ls_asset la
      where la.ls_asset_id = L_LS_ASSET_ID_FROM
        and la.ilr_id = ilr.ilr_id;

      --CJS 4/17/15 Adding check for partial transfer on components
    if L_PCT <> 1 then
        A_MSG := 'Checking for partial transfer on asset with components';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      L_RTN := 0;

      select count(1)
      into L_RTN
      from LS_COMPONENT
      where LS_ASSET_ID = L_LS_ASSET_ID_FROM;

      if L_RTN > 0 then
        A_MSG := 'Cannot do a partial transfer with components. Delete transactions and modify transfer.';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        return - 1;
      end if;
    end if;

      A_MSG            := 'COPY from asset';
    L_LS_ASSET_ID_TO := PKG_LEASE_ILR.F_COPYASSET(L_LS_ASSET_ID_FROM,
                                                  L_PCT,
                                                  A_QUANTITY);
      if L_LS_ASSET_ID_TO = -1 then
      return - 1;
      end if;

      A_MSG := 'TO Leased Asset: ' || TO_CHAR(L_LS_ASSET_ID_TO);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      A_MSG := 'LOAD LS_CPR_ASSET_MAP';
      insert into LS_CPR_ASSET_MAP
         (LS_ASSET_ID, ASSET_ID, EFFECTIVE_DATE)
      values
         (L_LS_ASSET_ID_TO, A_CPR_ASSET_ID, L_PEND_TRANS_TO.GL_POSTING_MO_YR);

    /* JKim 03282017 - insert the class codes from the pend transaction where filled out */
    A_MSG := 'Inserting in class codes for To asset';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

    delete from LS_ASSET_CLASS_CODE where LS_ASSET_ID = L_LS_ASSET_ID_TO;

      INSERT INTO ls_asset_class_code
      (class_code_id, ls_asset_id, Value)
      SELECT class_code_id, L_LS_ASSET_ID_TO, Value
        FROM class_code_pending_Trans
        WHERE pend_trans_id = L_PEND_TRANS_TO.pend_trans_id
      UNION
      SELECT class_code_id, L_LS_ASSET_ID_TO, Value
        FROM ls_asset_class_code
        WHERE ls_Asset_id = L_LS_ASSET_ID_FROM
        AND class_code_id NOT IN
             (SELECT class_code_id
                FROM class_code_pending_Trans
            WHERE pend_trans_id = L_PEND_TRANS_TO.pend_trans_id);

    /* end JKim 03282017 */

		 /* CJS 3/10/15 CALC STG check; converted assets don't have anything and need a new revision first */
	   A_MSG := 'Checking for previous CALC staging records';
	   L_RTN := 0;

     select count(1)
     into L_RTN
     from LS_ILR_ASSET_SCHEDULE_CALC_STG
     where LS_ASSET_ID = L_LS_ASSET_ID_FROM;

     if L_RTN is null or L_RTN = 0 then
        /* SET NEW ASSET TO STATUS = 4 so we don't split amounts */
        update ls_asset
        set ls_asset_status_id = 4
        where ls_asset_id = L_LS_ASSET_ID_TO;

      A_MSG := 'Creating new revision for asset because no records exist in ls_ilr_asset_schedule_calc_stg';
        PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        L_REVISION_FROM := PKG_LEASE_ILR.F_NEWREVISION(L_FROM_ILR_ID);
        PKG_PP_LOG.P_WRITE_MESSAGE('New Revision = ' || L_REVISION_FROM);
      if L_REVISION_FROM = -1 then
        A_MSG := 'Error creating new revision for asset with no records in ls_ilr_asset_schedule_calc_stg';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        return - 1;
      end if;
      L_STATUS := PKG_LEASE_SCHEDULE.F_PROCESS_ILR(L_FROM_ILR_ID,
                                                   L_REVISION_FROM);
      if L_STATUS <> 'OK' then
        A_MSG := 'Error in F_PROCESS_ILR: ' || L_STATUS;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        return - 1;
      end if;

      L_STATUS := PKG_LEASE_SCHEDULE.F_PROCESS_ASSETS(L_IN_SVC_DATE);
      if L_STATUS <> 'OK' then
        A_MSG := 'Error in F_PROCESS_ASSETS: ' || L_STATUS;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        return - 1;
      end if;
      L_STATUS := PKG_LEASE_SCHEDULE.F_SAVE_SCHEDULES(L_IN_SVC_DATE);
      if L_STATUS <> 'OK' then
        A_MSG := 'Error in F_SAVE_SCHEDULES: ' || L_STATUS;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        return - 1;
      end if;

      L_STATUS := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(L_FROM_ILR_ID,
                                                         L_REVISION_FROM,
                                                         A_MSG,
														 TRUE,
														 FALSE,
														 FALSE);
      if L_STATUS <> 1 then
        A_MSG := 'Error in F_APPROVE_ILR_NO_COMMIT: ' || A_MSG;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        return - 1;
      end if;

      update LS_ASSET
      set APPROVED_REVISION = L_REVISION_FROM
      where LS_ASSET_ID = L_LS_ASSET_ID_FROM;
	  
	  --Populate acct_month_approved and revision_app_order on ls_ilr_approval for transfers, since f_add_asset isn't called
	  A_MSG := 'Update LS ILR Approval Revision Order and Approval Accounting Month for FROM ILR';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    
	  UPDATE ls_ilr_approval lia
	    set acct_month_approved = L_MONTH,
	  revision_app_order = (select NVL(max(nvl(revision_app_order,0)),0) + 1 from ls_ilr_approval a where a.ilr_id = L_FROM_ILR_ID and revision <> L_REVISION_FROM)
	  where ilr_id = L_FROM_ILR_ID
	  and revision = L_REVISION_FROM;

--      select  F.APPROVED_REVISION
--        into  L_CURRENT_REVISION_FROM
--        from LS_ASSET F
--       where F.LS_ASSET_ID = L_LS_ASSET_ID_FROM;

      L_CURRENT_REVISION_FROM := L_REVISION_FROM;

      PKG_PP_LOG.P_WRITE_MESSAGE('New from revision for converted asset = ' ||
                                 L_CURRENT_REVISION_FROM);
       /* SET NEW ASSET TO STATUS = 1 again */
        update ls_asset
        set ls_asset_status_id = 1
        where ls_asset_id = L_LS_ASSET_ID_TO;
     end if;

      -- create the new from revision
      L_REVISION_FROM := PKG_LEASE_ILR.F_NEWREVISION(L_FROM_ILR_ID);
      if L_REVISION_FROM = -1 then
      return - 1;
      end if;

      PKG_PP_LOG.P_WRITE_MESSAGE('From revision = ' || L_REVISION_FROM);
	  
	  --Populate acct_month_approved and revision_app_order on ls_ilr_approval for transfers, since f_add_asset isn't called
	  A_MSG := 'Update LS ILR Approval Revision Order and Approval Accounting Month for new ILR FROM Revision';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    
	  UPDATE ls_ilr_approval lia
	    set acct_month_approved = L_MONTH,
	  revision_app_order = (select NVL(max(nvl(revision_app_order,0)),0) + 1 from ls_ilr_approval a where a.ilr_id = L_FROM_ILR_ID and revision <> L_REVISION_FROM)
	  where ilr_id = L_FROM_ILR_ID
	  and revision = L_REVISION_FROM;  


      --  if from company <> to company
      --  CREATE NEW ILR
      --  Set the new assets ilr
      --  Associate new asset to ILR / revision
      if L_PEND_TRANS_TO.COMPANY_ID <> L_PEND_TRANS_FROM.COMPANY_ID then
         L_TO_ILR_ID := PKG_LEASE_ILR.F_COPYILR(L_FROM_ILR_ID, L_PCT);
         if L_TO_ILR_ID = -1 then
        return - 1;
         end if;
         L_REVISION_TO := 1;

      A_MSG := 'Setting GL Account on LS_ILR_ACCOUNT';
         update ls_ilr_account
         set cap_asset_account_id = L_PEND_TRANS_TO.GL_ACCOUNT_ID
         where ilr_id = L_TO_ILR_ID;
         --
         A_MSG := 'SET company on new ILR';
         update LS_ILR
         set COMPANY_ID = A_COMPANY_ID
         where ILR_ID = L_TO_ILR_ID;

         -- update from ILR Payment TERMS if company not the same
         A_MSG := 'UPDATE from ILRs payment terms';
         update LS_ILR_PAYMENT_TERM LA
            set (EST_EXECUTORY_COST,
                  PAID_AMOUNT,
                  CONTINGENT_AMOUNT,
                  C_BUCKET_1,
                  C_BUCKET_2,
                  C_BUCKET_3,
                  C_BUCKET_4,
                  C_BUCKET_5,
                  C_BUCKET_6,
                  C_BUCKET_7,
                  C_BUCKET_8,
                  C_BUCKET_9,
                  C_BUCKET_10,
                  E_BUCKET_1,
                  E_BUCKET_2,
                  E_BUCKET_3,
                  E_BUCKET_4,
                  E_BUCKET_5,
                  E_BUCKET_6,
                  E_BUCKET_7,
                  E_BUCKET_8,
                  E_BUCKET_9,
                  E_BUCKET_10) =
                 (select LA.EST_EXECUTORY_COST - LT.EST_EXECUTORY_COST,
                         LA.PAID_AMOUNT - LT.PAID_AMOUNT,
                         LA.CONTINGENT_AMOUNT - LT.CONTINGENT_AMOUNT,
                         LA.C_BUCKET_1 - LT.C_BUCKET_1,
                         LA.C_BUCKET_2 - LT.C_BUCKET_2,
                         LA.C_BUCKET_3 - LT.C_BUCKET_3,
                         LA.C_BUCKET_4 - LT.C_BUCKET_4,
                         LA.C_BUCKET_5 - LT.C_BUCKET_5,
                         LA.C_BUCKET_6 - LT.C_BUCKET_6,
                         LA.C_BUCKET_7 - LT.C_BUCKET_7,
                         LA.C_BUCKET_8 - LT.C_BUCKET_8,
                         LA.C_BUCKET_9 - LT.C_BUCKET_9,
                         LA.C_BUCKET_10 - LT.C_BUCKET_10,
                         LA.E_BUCKET_1 - LT.E_BUCKET_1,
                         LA.E_BUCKET_2 - LT.E_BUCKET_2,
                         LA.E_BUCKET_3 - LT.E_BUCKET_3,
                         LA.E_BUCKET_4 - LT.E_BUCKET_4,
                         LA.E_BUCKET_5 - LT.E_BUCKET_5,
                         LA.E_BUCKET_6 - LT.E_BUCKET_6,
                         LA.E_BUCKET_7 - LT.E_BUCKET_7,
                         LA.E_BUCKET_8 - LT.E_BUCKET_8,
                         LA.E_BUCKET_9 - LT.E_BUCKET_9,
                         LA.E_BUCKET_10 - LT.E_BUCKET_10
                    from LS_ILR_PAYMENT_TERM LT
                   where LT.ILR_ID = L_TO_ILR_ID
                     and LT.REVISION = 1
					and LT.PAYMENT_TERM_ID = LA.PAYMENT_TERM_ID)
          where LA.ILR_ID = L_FROM_ILR_ID
            and LA.REVISION = L_REVISION_FROM;

         A_MSG := 'UPDATE from ILRs amounts by set of book';
         update LS_ILR_AMOUNTS_SET_OF_BOOKS LA
            set (NET_PRESENT_VALUE, CAPITAL_COST, CURRENT_LEASE_COST) =
                 (select LA.NET_PRESENT_VALUE - LT.NET_PRESENT_VALUE,
                         LA.CAPITAL_COST - LT.CAPITAL_COST,
                         LA.CURRENT_LEASE_COST - LT.CURRENT_LEASE_COST
                    from LS_ILR_AMOUNTS_SET_OF_BOOKS LT
                   where LT.ILR_ID = L_TO_ILR_ID
                     and LT.REVISION = 1
                     and LA.SET_OF_BOOKS_ID = LT.SET_OF_BOOKS_ID)
          where LA.ILR_ID = L_FROM_ILR_ID
            and LA.REVISION = L_REVISION_FROM;

         A_MSG := 'UPDATE from ILRs options';
         update LS_ILR_OPTIONS LA
            set (PURCHASE_OPTION_AMT, TERMINATION_AMT) =
                 (select LA.PURCHASE_OPTION_AMT - LT.PURCHASE_OPTION_AMT,
                         LA.TERMINATION_AMT - LT.TERMINATION_AMT
                    from LS_ILR_OPTIONS LT
                   where LT.ILR_ID = L_TO_ILR_ID
                     and LT.REVISION = 1)
          where LA.ILR_ID = L_FROM_ILR_ID
            and LA.REVISION = L_REVISION_FROM;
			
	     --Populate acct_month_approved and revision_app_order on ls_ilr_approval for transfers, since f_add_asset isn't called
	     A_MSG := 'Update LS ILR Approval Revision Order and Approval Accounting Month for new ILR TO Revision';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    
	     UPDATE ls_ilr_approval lia
	       set acct_month_approved = L_MONTH,
	     revision_app_order = (select NVL(max(nvl(revision_app_order,0)),0) + 1 from ls_ilr_approval a where a.ilr_id = L_TO_ILR_ID and revision <> L_REVISION_TO)
	     where ilr_id = L_TO_ILR_ID
	     and revision = L_REVISION_TO;     
            
      else
         L_TO_ILR_ID   := L_FROM_ILR_ID;
         L_REVISION_TO := L_REVISION_FROM;
      end if;

      A_MSG := 'ASSOCIATE new asset to the ILR / Revision';
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, LS_ASSET_ID, REVISION)
      values
         (L_TO_ILR_ID, L_LS_ASSET_ID_TO, L_REVISION_TO);

      A_MSG := 'UPDATE asset ILR';
      update LS_ASSET
       set ILR_ID            = L_TO_ILR_ID,
           APPROVED_REVISION = L_REVISION_TO
       where LS_ASSET_ID = L_LS_ASSET_ID_TO;

      -- UPDATE FROM ASSET's values
    /* CJS 3/3/15 Add NVL to values */
      A_MSG := 'UPDATE from assets values';
      update LS_ASSET LA
       set (TERMINATION_PENALTY_AMOUNT,
               GUARANTEED_RESIDUAL_AMOUNT,
               ACTUAL_RESIDUAL_AMOUNT,
               QUANTITY) =
           (select nvl(LA.TERMINATION_PENALTY_AMOUNT, 0) -
                    nvl(LT.TERMINATION_PENALTY_AMOUNT, 0),
                   nvl(LA.GUARANTEED_RESIDUAL_AMOUNT, 0) -
                    nvl(LT.GUARANTEED_RESIDUAL_AMOUNT, 0),
                   nvl(LA.ACTUAL_RESIDUAL_AMOUNT, 0) -
                    nvl(LT.ACTUAL_RESIDUAL_AMOUNT, 0),
                   nvl(LA.QUANTITY, 0) - nvl(LT.QUANTITY, 0)
                 from LS_ASSET LT
                where LT.LS_ASSET_ID = L_LS_ASSET_ID_TO)
       where LA.LS_ASSET_ID = L_LS_ASSET_ID_FROM;

     /* CJS 4/21/15 Set retirement date */
    L_MONTH := L_PEND_TRANS_FROM.gl_posting_mo_yr;

      A_MSG := 'Retire fully transferred asset';
      update LS_ASSET LA
       set LS_ASSET_STATUS_ID = 4,
           RETIREMENT_DATE    = L_MONTH
       where LA.LS_ASSET_ID = L_LS_ASSET_ID_FROM
       and nvl(LA.QUANTITY, 0) = 0;

    A_MSG := 'Retire ILR if no assets associated any more';
      update LS_ILR ILR
      set ilr_status_id = 3
      where ilr_id = L_FROM_ILR_ID
       and not exists (select 1
              from ls_asset
             where ilr_id = ilr.ilr_id
               and ls_asset_status_id <= 3);
      A_MSG := 'GET work order id';
      L_RTN := 0;
      select count(1)
        into L_RTN
        from WORK_ORDER_CONTROL
       where COMPANY_ID = A_COMPANY_ID
         and FUNDING_WO_INDICATOR = 0
       and WORK_ORDER_NUMBER =
           replace(L_PEND_TRANS_FROM.MISC_DESCRIPTION, 'wo:', '');

      if L_RTN = 1 then
         A_MSG := '   TO work order number: ' ||
                  replace(L_PEND_TRANS_FROM.MISC_DESCRIPTION, 'wo:', '');
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

         select WORK_ORDER_ID
           into L_WO_ID
           from WORK_ORDER_CONTROL
          where COMPANY_ID = A_COMPANY_ID
            and FUNDING_WO_INDICATOR = 0
         and WORK_ORDER_NUMBER =
             replace(L_PEND_TRANS_FROM.MISC_DESCRIPTION, 'wo:', '');
      else
      A_MSG := '   TO work order number: ' ||
               L_PEND_TRANS_TO.WORK_ORDER_NUMBER;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

         select WORK_ORDER_ID
           into L_WO_ID
           from WORK_ORDER_CONTROL
          where COMPANY_ID = A_COMPANY_ID
            and FUNDING_WO_INDICATOR = 0
            and WORK_ORDER_NUMBER = L_PEND_TRANS_TO.WORK_ORDER_NUMBER;
      end if;

      --
      A_MSG := 'SET the cpr attributes on the NEW Asset';
      update LS_ASSET
       set UTILITY_ACCOUNT_ID    = A_UTILITY_ACCOUNT_ID,
           COMPANY_ID            = A_COMPANY_ID,
           BUS_SEGMENT_ID        = A_BUS_SEGMENT_ID,
           RETIREMENT_UNIT_ID    = A_RETIREMENT_UNIT_ID,
           SUB_ACCOUNT_ID        = A_SUB_ACCOUNT_ID,
           PROPERTY_GROUP_ID     = A_PROPERTY_GROUP_ID,
           ASSET_LOCATION_ID     = A_ASSET_LOCATION_ID,
           FUNC_CLASS_ID         = A_FUNC_CLASS_ID,
           WORK_ORDER_ID         = L_WO_ID,
           ILR_ID                = L_TO_ILR_ID,
           TAX_ASSET_LOCATION_ID = A_ASSET_LOCATION_ID
       where LS_ASSET_ID = L_LS_ASSET_ID_TO;

      -- call function to build the schedules for the new asset and the old asset
      PKG_PP_LOG.P_WRITE_MESSAGE('Processing asset transer');
      A_MSG := PKG_LEASE_SCHEDULE.F_PROCESS_ASSET_TRF(L_LS_ASSET_ID_FROM,
                                                      L_LS_ASSET_ID_TO,
                                                      L_CURRENT_REVISION_FROM,
                                                      L_REVISION_FROM,
                                                      L_PCT,
                                                      L_FROM_ILR_ID,
                                                      L_TO_ILR_ID,
                                                      L_IN_SVC_DATE);
      if A_MSG <> 'OK' then
      return - 1;
      end if;

      A_MSG := 'Sending the new schedule and asset adjustments for approval';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      -- the "to" asset
    L_RTN := PKG_LEASE_CALC.F_SEND_ILR_NO_COMMIT(L_TO_ILR_ID,
                                                 L_REVISION_TO,
                                                 L_STATUS,
												 FALSE);
      if L_RTN <> 1 then
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

      return - 1;
      end if;

      --Rebuild the ILR and asset schedules. I guess this works? It's done in the retirements above
      --Not sure how you can just approve it before sending it
      A_MSG := 'Approving the new schedule and asset adjustments';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
    L_RTN := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(L_TO_ILR_ID,
                                                    L_REVISION_TO,
                                                    L_STATUS,
                                                    TRUE,
                                                    true,
													FALSE);
      if L_RTN <> 1 then
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

      return - 1;
      end if;

      -- only send the from if the comapnys are different and we created a new to side
      if L_PEND_TRANS_TO.COMPANY_ID <> L_PEND_TRANS_FROM.COMPANY_ID then
         A_MSG := '(FROM) Sending the new schedule and asset adjustments for approval';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      L_RTN := PKG_LEASE_CALC.F_SEND_ILR_NO_COMMIT(L_FROM_ILR_ID,
                                                   L_REVISION_FROM,
                                                   L_STATUS,
												   FALSE);
         if L_RTN <> 1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

        return - 1;
         end if;

         --Rebuild the ILR and asset schedules. I guess this works? It's done in the retirements above
         --Not sure how you can just approve it before sending it
         A_MSG := '(FROM) Approving the new schedule and asset adjustments';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      L_RTN := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(L_FROM_ILR_ID,
                                                      L_REVISION_FROM,
                                                      L_STATUS,
                                                      true,
                                                      true,
													  FALSE);
         if L_RTN <> 1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
        return - 1;
         end if;
      end if;

      --Get taxes on the transferred Assets
      L_STATUS := PKG_LEASE_ILR.F_GETTAXES(L_TO_ILR_ID);
      if L_STATUS <> 'OK' then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      return - 1;
         end if;

  /* WMD */
    A_MSG := 'Adding to and from assets to transfer history table';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      insert into ls_asset_transfer_history
      (from_ls_asset_id, to_ls_asset_id, month, from_pend_trans_id,
       to_pend_trans_id)
      values
      (L_LS_ASSET_ID_FROM, L_LS_ASSET_ID_TO, L_MONTH,
       L_PEND_TRANS_FROM.pend_trans_id, L_PEND_TRANS_TO.pend_trans_id);

	 -- CJS 4/21/17 Update gl je code to get external value; Post is not
    A_MSG := 'Updating GL transaction to have LS Asset ID on To Record';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      update gl_transaction
       set asset_id     = L_LS_ASSET_ID_TO,
           source       = 'LESSEE',
           description  = 'TRANS TYPE: ' || trans_type,
           gl_status_id = to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status',
                                                                                               L_PEND_TRANS_TO.COMPANY_ID)),
                                        1))
     where month = L_PEND_TRANS_TO.GL_POSTING_MO_YR
       and pend_trans_id in
           (L_PEND_TRANS_FROM.PEND_TRANS_ID, L_PEND_TRANS_TO.PEND_TRANS_ID)
       and trans_type in (3034, 3044, 3048)
       and ORIGINATOR = 'POST';

    A_MSG := 'Updating GL transaction to have LS Asset ID on From Record';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      update gl_transaction
       set asset_id     = L_LS_ASSET_ID_FROM,
           source       = 'LESSEE',
           description  = 'TRANS TYPE: ' || trans_type,
           gl_status_id = to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status',
                                                                                               L_PEND_TRANS_FROM.COMPANY_ID)),
                                        1))
     where month = L_PEND_TRANS_FROM.GL_POSTING_MO_YR
       and pend_trans_id in
           (L_PEND_TRANS_FROM.PEND_TRANS_ID, L_PEND_TRANS_TO.PEND_TRANS_ID)
        and trans_type in (3035, 3043, 3047)
        and ORIGINATOR='POST';

    /* CJS 4/21/17 Add trim, Change to LAM TRANSFERS; It was 'LAM RETIREMENTS'... C'mon Will. */
      A_MSG:='Getting transfer GL JE Code';
      select NVL(E.GL_JE_CODE, 'LAMTRANSFER')
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
      and G.PROCESS_ID = 'LAM TRANSFERS';
	  
	  PKG_PP_LOG.P_WRITE_MESSAGE('Getting Contract Currency JE System Control');
	  L_CONTRACT_JES_TO:=lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_company('Lease: Contract Currency JEs', L_PEND_TRANS_TO.COMPANY_ID)));
	  L_CONTRACT_JES_FROM:=lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_company('Lease: Contract Currency JEs', L_PEND_TRANS_FROM.COMPANY_ID)));
    
	  if L_CONTRACT_JES_TO IS NULL then
		   L_CONTRACT_JES_TO := 'no';
	  end if;
	  
	  if L_CONTRACT_JES_FROM IS NULL then
		   L_CONTRACT_JES_FROM := 'no';
	  end if;
	  
	 if L_CONTRACT_JES_TO <> L_CONTRACT_JES_FROM then
		  A_MSG:='You cannot transfer between companies with different system control settings for the Contract Currency JE Creation control';
		  PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
		  return -1;
	 end if;
	 
     PKG_PP_LOG.P_WRITE_MESSAGE('Contract Currency JE System Control:' || L_CONTRACT_JES_FROM);
     
	 if L_CONTRACT_JES_FROM = 'yes' then
		A_MSG := 'Checking JE Method Set of Books' ;
		L_JE_METHOD_SOB_CHECK := PKG_LEASE_COMMON.F_JE_METHOD_SOB_CHECK(A_MSG);
		
		if L_JE_METHOD_SOB_CHECK > 0 then
			  A_MSG:='You cannot book contract currency JEs with more than one set of books configured per JE Method';
			  PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
			  return -1;
		end if;
	 end if;
	 
	 if  L_CONTRACT_JES_FROM = 'yes' then
	 
	   -- convert gl transactions for both pend trans ids and the from company
	   
	 	 A_MSG := 'Updating POST GL Transactions to Contract Currency for Transfer: for pend_trans: ' || nvl(TO_CHAR(L_PEND_TRANS_FROM.PEND_TRANS_ID),'null');

	  -- Convert Post JEs to Contract Currency
			L_RTN := PKG_LEASE_COMMON.F_MC_POST_CONTRACT_CURR(
					L_PEND_TRANS_FROM.PEND_TRANS_ID,
					L_PEND_TRANS_FROM.COMPANY_ID,
					L_PEND_TRANS_FROM.GL_POSTING_MO_YR,
					A_MSG);

        if L_RTN = -1 then
		   PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
           return A_MSG;
        end if;
		
	    A_MSG := 'Updating POST GL Transactions to Contract Currency for Transfer: for pend_trans: ' || nvl(TO_CHAR(L_PEND_TRANS_TO.PEND_TRANS_ID),'null');

	   -- Convert Post JEs to Contract Currency
			L_RTN := PKG_LEASE_COMMON.F_MC_POST_CONTRACT_CURR(
					L_PEND_TRANS_TO.PEND_TRANS_ID, 
					L_PEND_TRANS_FROM.COMPANY_ID,
					L_PEND_TRANS_FROM.GL_POSTING_MO_YR,
					A_MSG);
					
		-- If the from and to companies aren't the same, need to call the post contract conversion function for both pend trans ids and both companies
		
		if L_PEND_TRANS_TO.COMPANY_ID <> L_PEND_TRANS_FROM.COMPANY_ID then
			   A_MSG := 'Updating POST GL Transactions to Contract Currency for Transfer: for pend_trans: ' || nvl(TO_CHAR(L_PEND_TRANS_FROM.PEND_TRANS_ID),'null');

			  -- Convert Post JEs to Contract Currency
					L_RTN := PKG_LEASE_COMMON.F_MC_POST_CONTRACT_CURR(
							L_PEND_TRANS_FROM.PEND_TRANS_ID,
							L_PEND_TRANS_TO.COMPANY_ID,
							L_PEND_TRANS_FROM.GL_POSTING_MO_YR,
							A_MSG);

				if L_RTN = -1 then
				   return A_MSG;
				end if;
				
				A_MSG := 'Updating POST GL Transactions to Contract Currency for Transfer: for pend_trans: ' || nvl(TO_CHAR(L_PEND_TRANS_TO.PEND_TRANS_ID),'null');

			 -- Convert Post JEs to Contract Currency
					L_RTN := PKG_LEASE_COMMON.F_MC_POST_CONTRACT_CURR(
							L_PEND_TRANS_TO.PEND_TRANS_ID, 
							L_PEND_TRANS_TO.COMPANY_ID,
							L_PEND_TRANS_FROM.GL_POSTING_MO_YR,
							A_MSG);
		end if;

        if L_RTN = -1 then
           return A_MSG;
        end if;
	 end if;
		

      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');

    A_MSG := 'Clearing Depr Method SLE table for transferred asset';
      delete from depr_method_sle
     where asset_id = (select asset_id
                         from ls_cpr_asset_map
                        where ls_asset_id = L_LS_ASSET_ID_FROM)
        and GL_POSTING_MO_YR >= L_MONTH;

   PKG_LEASE_DEPR.P_FCST_LEASE(L_TO_ILR_ID, L_REVISION_TO);


      DELETE FROM DEPR_METHOD_SLE
     WHERE ASSET_ID = (select asset_id
                         from ls_cpr_asset_map
                        where ls_asset_id = L_LS_ASSET_ID_TO)
      and GL_POSTING_MO_YR < L_MONTH;

    A_MSG := 'Inserting depreciation to date on asset in prior month so we dont end up with any crazy adjustments';
      insert into depr_method_sle
      (asset_id, set_of_books_id, gl_posting_mo_yr, depr_expense)
      select (select asset_id
                 from ls_cpr_asset_map
               where ls_asset_id = L_LS_ASSET_ID_TO),
             cprd.set_of_books_id,
             add_months(L_MONTH, -1),
             cprd.beg_reserve_month
      from cpr_depr cprd
       where asset_id =
             (select asset_id
                from ls_cpr_asset_map
               where ls_asset_id = L_LS_ASSET_ID_FROM)
        and gl_posting_mo_yr = L_MONTH;
    A_MSG := 'Updating end reserve on depreciation projection table';
      update ls_depr_forecast
       set end_reserve = end_reserve +
                         nvl(L_PEND_TRANS_FROM.adjusted_reserve, 0)
      where ls_asset_id = L_LS_ASSET_ID_FROM
        and revision = L_REVISION_FROM
        and month = L_PEND_TRANS_FROM.GL_POSTING_MO_YR;

    /* CJS 2/27/2015 Calling RETIREREVISION for from asset if completely transferred; 0 is retirement, 1 is transfer */

      if L_PCT = 1 then
      A_MSG := 'Calling Retire Revision for fully transferred asset';
      L_RTN := PKG_LEASE_ILR.F_RETIREREVISION(L_FROM_ILR_ID,
                                              L_LS_ASSET_ID_FROM,
                                              L_MONTH,
                                              1,
                                              L_MONTH);
      if L_RTN <> 1 then
            PKG_PP_LOG.P_WRITE_MESSAGE('Error in RETIREREVISION for Transfer');
        return - 1;
         end if;
    end if;

    /* WMD MAINT-44042 Clear out obligations */

    A_MSG := 'Getting asset schedule details on Transfer From asset';
    for OBLIG_FROM in (select las.*
                        from ls_asset_schedule las
                        where las.ls_asset_id = L_LS_ASSET_ID_FROM
                          and revision = L_CURRENT_REVISION_FROM
                          and nvl(las.is_om, 0) = 0 /* 06/28/2018 DJC use IS_OM from schedule */
                          and month = L_MONTH)
    loop
      if nvl(OBLIG_FROM.BEG_OBLIGATION, 0) -
         nvl(OBLIG_FROM.BEG_LT_OBLIGATION, 0) > 0 then
        L_STATUS := 'Processing short term obligation TRANSFER FROM debit on ls_asset_id = ' ||
                    L_LS_ASSET_ID_FROM;
      /* Clear ST Obligation on from asset */
        L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_FROM,
                                                3002,
                                              L_PCT *
                                              (nvl(OBLIG_FROM.BEG_OBLIGATION,
                                                   0) - nvl(OBLIG_FROM.BEG_LT_OBLIGATION,
                                                             0)),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                -1,
                                                L_PEND_TRANS_FROM.COMPANY_ID,
                                                L_MONTH,
                                                1, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_FROM.SET_OF_BOOKS_ID,
                                              nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,
                                                  0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    IF L_PEND_TRANS_FROM.COMPANY_ID <> L_PEND_TRANS_TO.COMPANY_ID or
           (pkg_pp_system_control.F_PP_SYSTEM_CONTROL('POST INTERCOMPANY BUS SEG TRF GL') =
           'yes' and L_PEND_TRANS_FROM.BUS_SEGMENT_ID <>
           L_PEND_TRANS_TO.BUS_SEGMENT_ID) THEN
          L_STATUS := 'Processing Intercompany TRANSFER FROM credit on ls_asset_id = ' ||
                      L_LS_ASSET_ID_FROM;
          L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_FROM,
                                                3050,
                                                   L_PCT * (nvl(OBLIG_FROM.BEG_OBLIGATION,
                                                                0) -
                                                   nvl(OBLIG_FROM.BEG_LT_OBLIGATION,
                                                                0)),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                L_PEND_TRANS_FROM.PEND_TRANS_ID,
                                                L_PEND_TRANS_FROM.COMPANY_ID,
                                                L_MONTH,
                                                0, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_FROM.SET_OF_BOOKS_ID,
                                                   nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,
                                                       0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    end if; /* 3050 entry */

    end if; /* ST OBLIGATION */

      if nvl(OBLIG_FROM.BEG_LT_OBLIGATION, 0) > 0 then
        L_STATUS := 'Processing Long Term Obligation Transfer From debit on ls_asset_id = ' ||
                    L_LS_ASSET_ID_FROM;
      /* Clear LT Obligation on from asset */
        L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_FROM,
                                                3003,
                                              L_PCT * nvl(OBLIG_FROM.BEG_LT_OBLIGATION,
                                                          0),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                -1,
                                                L_PEND_TRANS_FROM.COMPANY_ID,
                                                L_MONTH,
                                                1, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_FROM.SET_OF_BOOKS_ID,
                                              nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,
                                                  0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

        L_STATUS := 'Processing Intercompany TRANSFER To credit on ls_asset_id = ' ||
                    L_LS_ASSET_ID_FROM;
    IF L_PEND_TRANS_FROM.COMPANY_ID <> L_PEND_TRANS_TO.COMPANY_ID or
           (pkg_pp_system_control.F_PP_SYSTEM_CONTROL('POST INTERCOMPANY BUS SEG TRF GL') =
           'yes' and L_PEND_TRANS_FROM.BUS_SEGMENT_ID <>
           L_PEND_TRANS_TO.BUS_SEGMENT_ID) THEN
          L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_FROM,
                                                3050,
                                                L_PCT * nvl(OBLIG_FROM.BEG_LT_OBLIGATION,
                                                            0),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                L_PEND_TRANS_FROM.PEND_TRANS_ID,
                                                L_PEND_TRANS_FROM.COMPANY_ID,
                                                L_MONTH,
                                                0, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_FROM.SET_OF_BOOKS_ID,
                                                nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,
                                                    0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    end if; /* 3050 entry */

    end if; /* LT OBLIGATION */

    end loop;

    for OBLIG_TO in (select las.*
                        from ls_asset_schedule las, ls_asset la
                        where las.ls_asset_id = L_LS_ASSET_ID_TO
              and la.ls_asset_id = las.ls_asset_id
              and la.approved_revision = las.revision
                        and nvl(las.is_om, 0) = 0 /* 06/28/2018 DJC */
                        and month = L_MONTH)
    loop
      if nvl(OBLIG_TO.BEG_OBLIGATION, 0) -
         nvl(OBLIG_TO.BEG_LT_OBLIGATION, 0) > 0 then
      /* Clear ST Obligation on TO asset */
    IF L_PEND_TRANS_FROM.COMPANY_ID <> L_PEND_TRANS_TO.COMPANY_ID or
           (pkg_pp_system_control.F_PP_SYSTEM_CONTROL('POST INTERCOMPANY BUS SEG TRF GL') =
           'yes' and L_PEND_TRANS_FROM.BUS_SEGMENT_ID <>
           L_PEND_TRANS_TO.BUS_SEGMENT_ID) THEN
          L_STATUS := 'Processing Intercompany TRANSFER TO ST debit on ls_asset_id = ' ||
                      L_LS_ASSET_ID_TO;
          L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_TO,
                                                3049,
                                                   (nvl(OBLIG_TO.BEG_OBLIGATION,
                                                        0) -
                                                   nvl(OBLIG_TO.BEG_LT_OBLIGATION,
                                                        0)),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                L_PEND_TRANS_FROM.PEND_TRANS_ID, /* CJS 4/24/17 Change to FROM, follow all others */
                                                L_PEND_TRANS_TO.COMPANY_ID,
                                                L_MONTH,
                                                1, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_TO.SET_OF_BOOKS_ID,
                                                   nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,
                                                       0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    end if; /* 3049 entry */

        L_STATUS := 'Processing ST TRANSFER TO ST CREDIT on ls_asset_id = ' ||
                    L_LS_ASSET_ID_TO;
        L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_TO,
                                                3002,
                                                 (nvl(OBLIG_TO.BEG_OBLIGATION,
                                                      0) -
                                                 nvl(OBLIG_TO.BEG_LT_OBLIGATION,
                                                      0)),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                -1,
                                                L_PEND_TRANS_TO.COMPANY_ID,
                                                L_MONTH,
                                                0, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_TO.SET_OF_BOOKS_ID,
                                                 nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,
                                                     0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    end if; /* ST OBLIGATION */

      if nvl(OBLIG_TO.BEG_LT_OBLIGATION, 0) > 0 then
        L_STATUS := 'Processing Intercompany TRANSFER TO LT debit on ls_asset_id = ' ||
                    L_LS_ASSET_ID_TO;
      /* Clear LT Obligation on TO asset */
    IF L_PEND_TRANS_FROM.COMPANY_ID <> L_PEND_TRANS_TO.COMPANY_ID or
           (pkg_pp_system_control.F_PP_SYSTEM_CONTROL('POST INTERCOMPANY BUS SEG TRF GL') =
           'yes' and L_PEND_TRANS_FROM.BUS_SEGMENT_ID <>
           L_PEND_TRANS_TO.BUS_SEGMENT_ID) THEN
          L_RTN := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_TO,
                                                3049,
                                                nvl(OBLIG_TO.BEG_LT_OBLIGATION,
                                                    0),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                L_PEND_TRANS_FROM.PEND_TRANS_ID, /* CJS 4/24/17 Change to FROM, follow all others */
                                                L_PEND_TRANS_TO.COMPANY_ID,
                                                L_MONTH,
                                                1, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_TO.SET_OF_BOOKS_ID,
                                                nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,
                                                    0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    end if; /* 3049 entry */

        L_STATUS := 'Processing ST TRANSFER TO LT CREDIT on ls_asset_id = ' ||
                    L_LS_ASSET_ID_TO;
        L_RTN    := PKG_LEASE_COMMON.F_MC_BOOKJE(L_LS_ASSET_ID_TO,
                                                3003,
                                                 nvl(OBLIG_TO.BEG_LT_OBLIGATION,
                                                     0),
                                                0,
                                                A_DG_ID,
                                                L_WO_ID,
                                                0,
                                                0,
                                                -1,
                                                L_PEND_TRANS_TO.COMPANY_ID,
                                                L_MONTH,
                                                0, /* DR/CR */
                                                L_GL_JE_CODE,
                                                OBLIG_TO.SET_OF_BOOKS_ID,
                                                 nvl(L_PEND_TRANS_FROM.EXCHANGE_RATE,
                                                     0),
                                                L_PEND_TRANS_FROM.CURRENCY_FROM,
                                                L_PEND_TRANS_FROM.CURRENCY_TO,
                                                A_MSG);

    if L_RTN = -1 then
      PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || L_STATUS || A_MSG);
      return 0;
    end if;

    end if; /* LT OBLIGATION */

    end loop;

    A_MSG := 'Deleting tax rows for transfer from asset';
    delete from ls_monthly_tax
    where ls_asset_id = L_LS_ASSET_ID_FROM
      and gl_posting_mo_yr >= L_MONTH;
      
    delete from ls_asset_schedule_tax
     where ls_asset_id = L_LS_ASSET_ID_FROM
       and revision = L_REVISION_FROM
       and gl_posting_mo_yr >= L_MONTH;
       
    G_SEND_JES := TRUE;

    /* WMD Set company column on deriver control to new company */
    A_MSG := 'Checking for company column from CR system control table';
    select count(1)
    into v_counter
    from cr_system_control
     where upper(trim(control_name)) = 'COMPANY FIELD';

    if v_counter = 1 then
      A_MSG := 'Getting company column from CR system control table';
      select control_value
      into company_field
      from cr_system_control
       where upper(trim(control_name)) = 'COMPANY FIELD';

      A_MSG := 'Checking to see if company column is on CR Deriver Control';
      select count(1)
      into v_column_count
      from all_tab_columns
      where table_name = 'CR_DERIVER_CONTROL'
        and column_name = upper(trim(company_field));

      if v_column_count = 1 then
        A_MSG := 'Dynamic update of company on CR deriver control';
        sqls  := 'update cr_deriver_control crd
              set ' || company_field || ' =
              (select gl_company_no
                from company_setup
                where company_id = ' ||
                 to_char(L_PEND_TRANS_TO.COMPANY_ID) || ')
              where type=''Lessee''
                and substr(crd.string, 0, instr(crd.string, '':'') -1) = ' ||
                 to_char(L_LS_ASSET_ID_TO);
        A_MSG := sqls;
        execute immediate sqls;
      end if;
    end if;

    IF L_PCT = 1 THEN

      A_MSG := 'Deleting prior months from transfer to asset';
      delete from ls_asset_schedule
      where LS_ASSET_ID = L_LS_ASSET_ID_TO
        and month < L_MONTH;

      A_MSG := 'Setting beginning capital cost to zero on transfer to asset';
      update ls_asset_schedule
      set beg_capital_cost = 0
      where LS_ASSET_ID = L_LS_ASSET_ID_TO
        and month = L_MONTH
        and revision = L_REVISION_TO;

      A_MSG := PKG_LEASE_SCHEDULE.F_SYNC_ILR_SCHEDULE(L_TO_ILR_ID,
                                                      L_REVISION_TO);

      IF A_MSG <> 'OK' then
        PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        PKG_PP_LOG.P_END_LOG();
        return - 1;
      end if;
    update ls_ilr a
      set ilr_status_id = 3
      where ilr_id = L_FROM_ILR_ID
         and not exists (select 1
                from ls_asset
               where ilr_id = a.ilr_id
                 and ls_asset_status_id < 4);

    END IF;

    A_MSG := 'Delete out the empty prior month cpr depr rows so true up will work';
    delete from cpr_depr
     where asset_id = (select asset_id
                         from ls_cpr_asset_map
                        where ls_asset_id = L_LS_ASSET_ID_TO)
      and gl_posting_mo_yr < L_MONTH;
	  
      PKG_PP_LOG.P_END_LOG();

      return 1;
   exception
      when others then
         A_MSG := SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || A_MSG);

         PKG_PP_LOG.P_END_LOG();
      return - 1;
   end F_TRANSFER_ASSET_TO;

   --**************************************************************************
   --                            F_DELETE_ASSETS
   --**************************************************************************
  function F_DELETE_ASSETS(A_ASSET_IDS in PKG_LEASE_CALC.NUM_ARRAY)
    return varchar2 is
    L_STATUS  varchar2(4000);
    ASSET_IDS T_NUM_ARRAY;
  begin
    PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_ASSET_POST.F_DELETE_ASSETS');
    PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
    PKG_PP_LOG.P_WRITE_MESSAGE('Preparing to delete ' || A_ASSET_IDS.COUNT ||
                               ' assets.');

    ASSET_IDS := T_NUM_ARRAY();
    for I in 1 .. A_ASSET_IDS.count
    loop
      PKG_PP_LOG.P_WRITE_MESSAGE('-- Asset ID: ' ||
                                 to_char(A_ASSET_IDS(I)));
      ASSET_IDS.extend;
      ASSET_IDS(I) := A_ASSET_IDS(I);
    end loop;

    L_STATUS := 'Deleting class codes';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from LS_ASSET_CLASS_CODE
    where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));

    /* WMD Needed to Change Table to LS_COMPONENT FROM LS_ASSET_COMPONENT */
    L_STATUS := 'Deleting asset components';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from LS_COMPONENT
    where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));

    L_STATUS := 'Deleting documents';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from LS_ASSET_DOCUMENT
    where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));

    L_STATUS := 'Deleting tax maps';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from LS_ASSET_TAX_MAP
    where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));

    L_STATUS := 'Deleting local taxes';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from LS_ASSET_LOCAL_TAX
    where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));

    L_STATUS := 'Deleting leased assets';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from LS_ASSET
    where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));

    L_STATUS := 'Deleting JE Allocation Rows';
    PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
    delete from cr_deriver_control
    where string like (select column_value || ':%' from table(ASSET_IDS));
    PKG_PP_LOG.P_WRITE_MESSAGE('Success');
    PKG_PP_ERROR.REMOVE_MODULE_NAME;
    return 'OK';

  exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR', 'Failed Operation');
  end F_DELETE_ASSETS;

   --**************************************************************************
   --                            GET_ILR_ID
   --**************************************************************************

   function F_GET_ILR_ID return number is

   begin
      return L_ILR_ID;
   end F_GET_ILR_ID;

--**************************************************************************
--                            Initialize Package
--**************************************************************************

begin
   L_ILR_ID := 0;

end PKG_LEASE_ASSET_POST;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (16251, 0, 2018, 2, 0, 0, 0, 'c:\plasticwks\powerplant\sql\packages', 
    'PKG_LEASE_ASSET_POST.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
