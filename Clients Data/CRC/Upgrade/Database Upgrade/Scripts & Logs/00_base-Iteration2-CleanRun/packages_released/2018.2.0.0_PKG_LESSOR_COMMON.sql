/*
||=================================================================================================
|| Application: PowerPlant
|| Object Name: PKG_LESSOR_COMMON
|| Description:
||=================================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||=================================================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- --------------    ----------------------------------------------------------
|| 2017.1.0.0 11/01/2017 C.Shilling	   Create package
|| 2017.3.0.0 01/02/2018 Anand R	   PP-50272 Add Lessor version of F_BOOKJE and F_MC_BOOKJE
||=================================================================================================
*/
CREATE OR REPLACE PACKAGE pkg_lessor_common AS
	G_PKG_VERSION varchar(35) := '2018.2.0.0';
	procedure p_startLog (a_process_description varchar2);
	
	-- function the return the process id for lessor calcs
	FUNCTION f_get_process_id(a_description pp_processes.description%TYPE)
		RETURN pp_processes.process_id%TYPE;
	
	--overlaoaded function with A_ORIG for the ORIGINATOR column on GL_TRANSACTION
   function F_BOOKJE(A_LSR_ILR_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_JE_METHOD_ID    in number,
                     A_AMOUNT_TYPE     in number,
                     A_REVERSAL_CONVENTION in number,
                     A_ORIG          in varchar2,
                     A_MSG           out varchar2) return number;

   -- Function to perform booking of LESSEE JEs (Multicurrencies)
   function F_BOOKJE(A_LSR_ILR_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
                        A_JE_METHOD_ID    in number,
                        A_AMOUNT_TYPE     in number,
                        A_REVERSAL_CONVENTION in number,
                        A_MSG           out varchar2) return number;


   -- Function to perform booking of LESSOR JEs (Multicurrencies)
   function F_MC_BOOKJE(A_LSR_ILR_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
                        A_JE_METHOD_ID    in number,
                        A_AMOUNT_TYPE     in number,
                        A_REVERSAL_CONVENTION in number,
                        A_EXCHANGE_RATE in number,
                        A_CURRENCY_FROM in number,
                        A_CURRENCY_TO   in number,
                        A_MSG           out varchar2) return number;

   --overlaoaded function with A_ORIG for the ORIGINATOR column on GL_TRANSACTION (Multicurrencies)
   function F_MC_BOOKJE(A_LSR_ILR_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
                        A_JE_METHOD_ID    in number,
                        A_AMOUNT_TYPE     in number,
                        A_REVERSAL_CONVENTION in number,
                        A_EXCHANGE_RATE in number,
                        A_CURRENCY_FROM in number,
                        A_CURRENCY_TO   in number,
                        A_ORIG          in varchar2,
                        A_MSG           out varchar2) return number;
		
	TYPE ilr_schedule_line IS RECORD (
		ilr_id				  lsr_ilr_schedule_sales_direct.ilr_id%TYPE,
		set_of_books_id		  lsr_ilr_schedule_sales_direct.set_of_books_id%TYPE,
		rownumber			  INTEGER,
		company_id			  lsr_ilr.company_id%type,
		month 				  lsr_ilr_schedule.month%type,
		principal_received	  lsr_ilr_schedule_sales_direct.principal_received%TYPE,
		interest_income	      lsr_ilr_schedule.interest_income_received%TYPE,
		interest_unguaranteed lsr_ilr_schedule_sales_direct.interest_unguaranteed_residual %TYPE,
		recognized_profit     lsr_ilr_schedule_direct_fin.recognized_profit %TYPE,
		initial_direct_cost   lsr_ilr_schedule.initial_direct_cost %TYPE,
		executory1 			  lsr_ilr_schedule.executory_paid1%TYPE,
		executory2 			  lsr_ilr_schedule.executory_paid2%TYPE,
		executory3 			  lsr_ilr_schedule.executory_paid3%TYPE,
		executory4 			  lsr_ilr_schedule.executory_paid4%TYPE,
		executory5 			  lsr_ilr_schedule.executory_paid5%TYPE,
		executory6 			  lsr_ilr_schedule.executory_paid6%TYPE,
		executory7 			  lsr_ilr_schedule.executory_paid7%TYPE,
		executory8 			  lsr_ilr_schedule.executory_paid8%TYPE,
		executory9 			  lsr_ilr_schedule.executory_paid9%TYPE,
		executory10			  lsr_ilr_schedule.executory_paid10%TYPE,
		contingent1			  lsr_ilr_schedule.contingent_paid1%TYPE,
		contingent2			  lsr_ilr_schedule.contingent_paid2%TYPE,
		contingent3			  lsr_ilr_schedule.contingent_paid3%TYPE,
		contingent4			  lsr_ilr_schedule.contingent_paid4%TYPE,
		contingent5			  lsr_ilr_schedule.contingent_paid5%TYPE,
		contingent6			  lsr_ilr_schedule.contingent_paid6%TYPE,
		contingent7			  lsr_ilr_schedule.contingent_paid7%TYPE,
		contingent8			  lsr_ilr_schedule.contingent_paid8%TYPE,
		contingent9			  lsr_ilr_schedule.contingent_paid9%TYPE,
		contingent10		  lsr_ilr_schedule.contingent_paid10%TYPE,
		accrued_rent		  lsr_ilr_schedule.accrued_rent%TYPE,
		deferred_rent		  lsr_ilr_schedule.deferred_rent%TYPE
	);
	TYPE ilr_schedule_line_table IS TABLE OF ilr_schedule_line INDEX BY PLS_INTEGER;
END pkg_lessor_common;
/

CREATE OR REPLACE PACKAGE BODY pkg_lessor_common AS
	--**************************************************************
	--         functions
	--**************************************************************
	-- returns the process id for lease calc
	FUNCTION f_get_process_id(a_description pp_processes.description%TYPE)
		RETURN pp_processes.process_id%TYPE
	IS
		l_process_id pp_processes.process_id%TYPE;
	BEGIN
		SELECT process_id
		INTO l_process_id
		FROM pp_processes
		WHERE description = a_description;

		RETURN l_process_id;
	EXCEPTION
		WHEN No_Data_Found THEN
			Raise_Application_Error(-20000, 'The description passed to PKG_LESSOR_COMMON.F_GET_PROCESS_ID (''' || a_description || ''') was not found.');
	END f_get_process_id;
	
    --Wrapper for the overloaded version
   function F_BOOKJE(A_LSR_ILR_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_JE_METHOD_ID    in number,
                     A_AMOUNT_TYPE     in number,
                     A_REVERSAL_CONVENTION in number,
                     A_MSG           out varchar2) return number is

   begin

      return F_BOOKJE(A_LSR_ILR_ID, A_TRANS_TYPE, A_AMT, A_GL_ACCOUNT_ID, A_GAIN_LOSS, 
                      A_COMPANY_ID, A_MONTH, A_DR_CR, A_GL_JC, A_SOB_ID, A_JE_METHOD_ID, A_AMOUNT_TYPE,
                       A_REVERSAL_CONVENTION, '', A_MSG);
   exception
      when others then
         return -1;
   end F_BOOKJE;
   
   --**************************************************************************
   --                            F_BOOKJE
   --**************************************************************************
   function F_BOOKJE(A_LSR_ILR_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_JE_METHOD_ID    in number,
                     A_AMOUNT_TYPE     in number,
                     A_REVERSAL_CONVENTION in number,
                     A_ORIG          in varchar2,
                     A_MSG           out varchar2) return number is
      L_GL_ACCOUNT varchar2(2000);

   begin

      A_MSG       := 'Starting JE Creation for trans_type: ' || TO_CHAR(A_TRANS_TYPE);

         L_GL_ACCOUNT := PP_GL_TRANSACTION(A_TRANS_TYPE,
                                            '',
                                            '',
                                            '',
                                            '',
                                            A_GL_ACCOUNT_ID,
                                            A_GAIN_LOSS,
                                            '',
                                            A_JE_METHOD_ID,
                                            A_SOB_ID,
                                            A_AMOUNT_TYPE,
                                            A_REVERSAL_CONVENTION,
                                            A_LSR_ILR_ID);

         if LOWER(L_GL_ACCOUNT) like '%error%' then
            A_MSG := L_GL_ACCOUNT;
            return - 1;
         end if;


         A_MSG := 'Inserting into gl_transaction: ' || TO_CHAR(A_TRANS_TYPE);
         insert into GL_TRANSACTION
            (GL_TRANS_ID, "MONTH", COMPANY_NUMBER, GL_ACCOUNT, DEBIT_CREDIT_INDICATOR, AMOUNT,
             GL_JE_CODE, GL_STATUS_ID, DESCRIPTION, source, ORIGINATOR, COMMENTS, PEND_TRANS_ID,
             ASSET_ID, AMOUNT_TYPE, JE_METHOD_ID, TAX_ORIG_MONTH_NUMBER, TRANS_TYPE)
            select PWRPLANT1.NEXTVAL,
                   A_MONTH,
                   C.GL_COMPANY_NO,
                   L_GL_ACCOUNT,
                   A_DR_CR,
                   A_AMT * NVL(A_REVERSAL_CONVENTION,1),
                   A_GL_JC,
                   to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', A_COMPANY_ID)), 1)),
                   'TRANS TYPE: ' || TO_CHAR(A_TRANS_TYPE),
                   'LESSOR',
                   A_ORIG,
                   '',
                   '',
                   '',
                   A_AMOUNT_TYPE,
                   A_JE_METHOD_ID,
                   null,
				           A_TRANS_TYPE
              from COMPANY_SETUP C
             where C.COMPANY_ID = A_COMPANY_ID;

      return 1;
   exception
      when others then
		 a_msg:=SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         return -1;
   end F_BOOKJE;
	
   --Wrapper for the overloaded version (Multicurrencies)
   function F_MC_BOOKJE(A_LSR_ILR_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
                        A_JE_METHOD_ID    in number,
                        A_AMOUNT_TYPE     in number,
                        A_REVERSAL_CONVENTION in number,
                        A_EXCHANGE_RATE in number,
                        A_CURRENCY_FROM in number,
                        A_CURRENCY_TO   in number,
                        A_MSG           out varchar2) return number is

   begin

      return F_MC_BOOKJE(A_LSR_ILR_ID, A_TRANS_TYPE, A_AMT, A_GL_ACCOUNT_ID, A_GAIN_LOSS, 
                      A_COMPANY_ID, A_MONTH, A_DR_CR, A_GL_JC, A_SOB_ID, A_JE_METHOD_ID, A_AMOUNT_TYPE,
            A_REVERSAL_CONVENTION, A_EXCHANGE_RATE, A_CURRENCY_FROM, A_CURRENCY_TO, '', A_MSG);
   exception
      when others then
         return -1;
   end F_MC_BOOKJE;
   
   --**************************************************************************
   --                            F_MC_BOOKJE (Multicurrencies)
   --**************************************************************************
   function F_MC_BOOKJE(A_LSR_ILR_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_JE_METHOD_ID    in number,
                     A_AMOUNT_TYPE     in number,
                     A_REVERSAL_CONVENTION in number,
                     A_EXCHANGE_RATE in number,
                     A_CURRENCY_FROM in number,
                     A_CURRENCY_TO   in number,
                     A_ORIG          in varchar2,
                     A_MSG           out varchar2) return number is
      L_GL_ACCOUNT varchar2(2000);
      L_GL_TRANS_ID number;
	  L_CONTRACT_JES  varchar2(100);
	  L_JE_METHOD_SOB_CHECK number;

   begin

   	  A_MSG       := 'Getting Contract Currency JE System Control';
	  L_CONTRACT_JES:=lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_company('Lease: Contract Currency JEs', A_COMPANY_ID)));
	  
	  if L_CONTRACT_JES IS NULL then
		L_CONTRACT_JES := 'no';
	  end if;
	  
	  if L_CONTRACT_JES = 'yes' then
		A_MSG := 'Checking JE Method Set of Books' ;
		L_JE_METHOD_SOB_CHECK := PKG_LEASE_COMMON.F_JE_METHOD_SOB_CHECK(A_MSG);
		
		if L_JE_METHOD_SOB_CHECK > 0 then
		      A_MSG := 'You cannot book contract currency JEs with more than one set of books configured per JE Method';
			  return -1;
		end if;
	  end if;

   
      A_MSG       := 'Starting JE Creation for trans_type: ' || TO_CHAR(A_TRANS_TYPE);

         L_GL_ACCOUNT := PP_GL_TRANSACTION(A_TRANS_TYPE,
                                            '',
                                            '',
                                            '',
                                            '',
                                            A_GL_ACCOUNT_ID,
                                            A_GAIN_LOSS,
                                            '',
                                            A_JE_METHOD_ID,
                                            A_SOB_ID,
                                            A_AMOUNT_TYPE,
                                            A_REVERSAL_CONVENTION,
                                            A_LSR_ILR_ID);

         if LOWER(L_GL_ACCOUNT) like '%error%' then
            A_MSG := L_GL_ACCOUNT;
            return - 1;
         end if;

      select PWRPLANT1.NEXTVAL into L_GL_TRANS_ID from dual;

         A_MSG := 'Inserting into gl_transaction: ' || TO_CHAR(A_TRANS_TYPE);
         insert into GL_TRANSACTION
            (GL_TRANS_ID, "MONTH", COMPANY_NUMBER, GL_ACCOUNT, DEBIT_CREDIT_INDICATOR, AMOUNT,
             GL_JE_CODE, GL_STATUS_ID, DESCRIPTION, source, ORIGINATOR, COMMENTS, PEND_TRANS_ID,
             ASSET_ID, AMOUNT_TYPE, JE_METHOD_ID, TAX_ORIG_MONTH_NUMBER, TRANS_TYPE)
            select L_GL_TRANS_ID,
                   A_MONTH,
                   C.GL_COMPANY_NO,
                   L_GL_ACCOUNT,
                   A_DR_CR,
                   A_AMT * decode(L_CONTRACT_JES, 'yes', 1, A_EXCHANGE_RATE) * NVL(A_REVERSAL_CONVENTION,1),
                   A_GL_JC,
                   to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', A_COMPANY_ID)), 1)),
                   'TRANS TYPE: ' || TO_CHAR(A_TRANS_TYPE) || '; ILR ID: ' || TO_CHAR(A_LSR_ILR_ID),
                   'LESSOR',
                   A_ORIG,
                   '',
                   '',
                   '',
                   A_AMOUNT_TYPE,
                   A_JE_METHOD_ID,
                   null,
                   A_TRANS_TYPE
              from COMPANY_SETUP C
             where C.COMPANY_ID = A_COMPANY_ID;

         A_MSG := 'Inserting into ls_mc_gl_transaction_audit: ' || TO_CHAR(A_TRANS_TYPE);
     insert into LS_MC_GL_TRANSACTION_AUDIT
        (COMPANY_ID, GL_POSTING_MO_YR, GL_TRANS_ID, TRANS_TYPE, GL_JE_CODE,
      FROM_CURRENCY, TO_CURRENCY, EXCHANGE_RATE, CALCULATED_AMOUNT, TRANSLATED_AMOUNT, ILR_ID, LS_CURRENCY_TYPE_ID)
         select A_COMPANY_ID,
              A_MONTH,
          L_GL_TRANS_ID,
          A_TRANS_TYPE,
          A_GL_JC,
          A_CURRENCY_FROM,
          A_CURRENCY_TO,
          A_EXCHANGE_RATE,
          A_AMT * NVL(A_REVERSAL_CONVENTION,1),
          A_AMT * decode(L_CONTRACT_JES, 'yes', 1, A_EXCHANGE_RATE) * NVL(A_REVERSAL_CONVENTION,1),
		  A_LSR_ILR_ID,
		  decode(L_CONTRACT_JES, 'yes', 1, 2)
      from DUAL;

      return 1;
   exception
      when others then
     a_msg:=SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         return -1;
   end F_MC_BOOKJE;
 
   procedure p_startLog (a_process_description varchar2) is 
   begin
		--Start log for passed in process
		pkg_pp_log.p_start_log(pkg_lessor_common.f_get_Process_id(a_process_description));
	
   end p_startLog;
   
END pkg_lessor_common;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (16263, 0, 2018, 2, 0, 0, 0, 'c:\plasticwks\powerplant\sql\packages', 
    'PKG_LESSOR_COMMON.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
