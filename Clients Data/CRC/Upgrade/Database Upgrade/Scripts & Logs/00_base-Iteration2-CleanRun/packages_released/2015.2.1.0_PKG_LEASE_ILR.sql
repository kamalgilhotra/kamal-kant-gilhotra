/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039519_lease_PKG_LEASE_ILR.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 06/25/2014 Kyle Peterson
|| 10.4.3.0 07/07/2014 Charlie Shilling
|| 10.4.3.0 07/24/2014 Charlie Shilling Use "where revision < 9999" in f_newrevision
||============================================================================
*/

create or replace package PKG_LEASE_ILR as

   type CR_DERIVER_TYPE is table of CR_DERIVER_CONTROL%rowtype;

   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_NEWREVISION(A_ILR_ID number) return number;

   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the passed in revision information as the starting point, and copy everything, including the schedule
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *      number: in: a_revision = the revision to copy
   *   @@RETURN
   *      number: new revision number for success
   *            -1 for failure
   */
   function F_COPYREVISION(A_ILR_ID   number,
                           A_REVISION number,
                           A_TO_REVISION number:=null) return number;

   /*
   *   @@DESCRIPTION
   *      This function converts forecasts into new "real" revisions for all in-service ILR's
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      none
   *   @@RETURN
   *      number: 1 for success
   *            -1 for failure
   */
   function f_RETIREREVISION(A_ILR_ID      number,
                             A_LS_ASSET_ID number,
                             A_MONTH       date,
							 A_FLAG        number) return number;
   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in asset/ILR being retired
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a retired revision
   *      number: in: a_ls_asset_id = the ls_asset_id to create a retired revision
   *      date: in: a_month = the retirement month to use
   *   @@RETURN
   *      number: 1 for success
   *            -1 for failure
   */
   function F_CONVERTFORECASTS return number;
   
   /*
   *   @@DESCRIPTION
   *      This function converts forecasts into new "real" revisions for the passed in array of ILR's
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      none
   *   @@RETURN
   *      number: 1 for success
   *            -1 for failure
   */
   function F_CONVERTFORECASTS(A_ILR_IDS in PKG_LEASE_CALC.NUM_ARRAY) return number;

   /*
   *   @@DESCRIPTION
   *      This function copies an ILR into a new ILR
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new ilr
   *    number: in: a_pct = the percent to transfer
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_COPYILR(A_ILR_ID number,
                      A_PCT    number) return number;

   /*
   *   @@DESCRIPTION
   *      This function copies an asset into another leased asset
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *    number: in: a_ls_asset_id = the asset to copy
   *    number: in: a_pct. Percent to copy
   *    number: in: a_qty.  The quantity to copy
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_COPYASSET(A_LS_ASSET_ID number,
                        A_PCT         number,
                        A_QTY         number) return number;

   function F_GETTAXES(A_ILR_ID number) return varchar2;
   
   procedure P_CALC_II(A_ILR_ID number, A_REVISION number);

end PKG_LEASE_ILR;
/


create or replace package body PKG_LEASE_ILR as
   --**************************************************************************
   --                            VARIABLES
   --**************************************************************************   
   --**************************************************************************
   --                            FORWARD DECLARATIONS
   --**************************************************************************

   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *   @@RETURN
   *      number: the revision added for success
   *            -1 for failure
   */
   function F_NEWREVISION(A_ILR_ID number) return number is

      L_STATUS           varchar2(2000);
      L_REVISION         number;
      L_CURRENT_REVISION number;

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW Revision for: ' || TO_CHAR(A_ILR_ID));

      L_STATUS := 'Get current revision';
      select CURRENT_REVISION into L_CURRENT_REVISION from LS_ILR where ILR_ID = A_ILR_ID;
      PKG_PP_LOG.P_WRITE_MESSAGE('   COPY Revision: ' || TO_CHAR(L_CURRENT_REVISION));

      L_STATUS := 'Get new revision';
      select nvl(max(REVISION),0) + 1 into L_REVISION from LS_ILR_APPROVAL where ILR_ID = A_ILR_ID and REVISION < 9999;
      PKG_PP_LOG.P_WRITE_MESSAGE('   NEW Revision: ' || TO_CHAR(L_REVISION));

      L_STATUS := 'LOAD ilr_approval';
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select A.ILR_ID, L_REVISION, A.APPROVAL_TYPE_ID, 1
           from LS_ILR_APPROVAL A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_options';
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT
          --, payment_shift
          )
         select A.ILR_ID,
                L_REVISION,
                A.PURCHASE_OPTION_TYPE_ID,
                A.PURCHASE_OPTION_AMT,
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                A.TERMINATION_AMT
         --, payment_shift
           from LS_ILR_OPTIONS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_amounts';
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select A.ILR_ID,
                L_REVISION,
                SET_OF_BOOKS_ID,
                NET_PRESENT_VALUE,
                INTERNAL_RATE_RETURN,
                CAPITAL_COST,
                CURRENT_LEASE_COST,
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_payment_term';
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10)
         select A.ILR_ID,
                L_REVISION,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10
           from LS_ILR_PAYMENT_TERM A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_asset_map';
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, REVISION, LS_ASSET_ID)
         select A.ILR_ID, L_REVISION, A.LS_ASSET_ID
           from LS_ILR_ASSET_MAP A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;


      return L_REVISION;

   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
   end F_NEWREVISION;

   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the passed in revision information as the starting point, and copy everything, including the schedule
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *      number: in: a_revision = the revision to copy
   *   @@RETURN
   *      number: new revision number for success
   *            -1 for failure
   */
   function F_COPYREVISION(A_ILR_ID   number,
                           A_REVISION number,
                           A_TO_REVISION number:=null) return number is

      L_STATUS   varchar2(2000);
      L_REVISION number;
      RTN        number;
      IS_AUTO    varchar2(254);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW Revision for: ' || TO_CHAR(A_ILR_ID) ||
                                 ' copied from revision: ' || TO_CHAR(A_REVISION));

      L_STATUS := 'Get new revision';
      if A_TO_REVISION is null then
        select nvl(max(REVISION),0) + 1
          into L_REVISION
          from LS_ILR_APPROVAL
         where ILR_ID = A_ILR_ID
           and REVISION < 9999;
        PKG_PP_LOG.P_WRITE_MESSAGE('   NEW Revision: ' || TO_CHAR(L_REVISION));
       else
         L_REVISION := A_TO_REVISION;
       end if;

      L_STATUS := 'LOAD ilr_approval';
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select A.ILR_ID, L_REVISION, A.APPROVAL_TYPE_ID, 1
           from LS_ILR_APPROVAL A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION;

      L_STATUS := 'LOAD ilr_options';
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT
          --, payment_shift
          )
         select A.ILR_ID,
                L_REVISION,
                A.PURCHASE_OPTION_TYPE_ID,
                A.PURCHASE_OPTION_AMT,
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                A.TERMINATION_AMT
         --, payment_shift
           from LS_ILR_OPTIONS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION;

      L_STATUS := 'LOAD ilr_amounts';
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select A.ILR_ID,
                L_REVISION,
                SET_OF_BOOKS_ID,
                NET_PRESENT_VALUE,
                INTERNAL_RATE_RETURN,
                CAPITAL_COST,
                CURRENT_LEASE_COST,
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION;

      L_STATUS := 'LOAD ilr_payment_term';
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10)
         select A.ILR_ID,
                L_REVISION,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10
           from LS_ILR_PAYMENT_TERM A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION;

      L_STATUS := 'LOAD ilr_asset_map';
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, REVISION, LS_ASSET_ID)
         select A.ILR_ID, L_REVISION, A.LS_ASSET_ID
           from LS_ILR_ASSET_MAP A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION;

      L_STATUS := 'LOAD ilr_schedule';
      insert into LS_ILR_SCHEDULE
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
          EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
          EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
          EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
          EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
          CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
          CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          IS_OM, CURRENT_LEASE_COST)
         select ILR_ID,
                L_REVISION,
                SET_OF_BOOKS_ID,
                month,
                RESIDUAL_AMOUNT,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                INTEREST_ACCRUAL,
                PRINCIPAL_ACCRUAL,
                INTEREST_PAID,
                PRINCIPAL_PAID,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                IS_OM,
				CURRENT_LEASE_COST
           from LS_ILR_SCHEDULE
          where ILR_ID = A_ILR_ID
            and REVISION = A_REVISION;

      L_STATUS := 'LOAD asset_schedule';
      insert into LS_ASSET_SCHEDULE
         (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
          EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
          EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
          EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
          EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
          CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
          CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          IS_OM, CURRENT_LEASE_COST)
         select LS_ASSET_ID,
                L_REVISION,
                SET_OF_BOOKS_ID,
                month,
                RESIDUAL_AMOUNT,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                INTEREST_ACCRUAL,
                PRINCIPAL_ACCRUAL,
                INTEREST_PAID,
                PRINCIPAL_PAID,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                IS_OM,
                CURRENT_LEASE_COST
           from LS_ASSET_SCHEDULE
          where LS_ASSET_ID in (select LS_ASSET_ID
                                  from LS_ILR_ASSET_MAP
                                 where ILR_ID = A_ILR_ID
                                   and REVISION = A_REVISION)
            and REVISION = A_REVISION;


      return L_REVISION;

   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
   end F_COPYREVISION;

	/*
	*   @@DESCRIPTION
	*      This function converts forecasts into new "real" revisions for all in-service ILR's
	*      NO COMMITS or ROLLBACK in this function
	*   @@PARAMS
	*      none
	*   @@RETURN
	*      number: 1 for success
	*            -1 for failure
	*/
	function F_CONVERTFORECASTS return number is

		L_STATUS varchar2(2000);
		L_IDS PKG_LEASE_CALC.NUM_ARRAY;

	begin
		return F_CONVERTFORECASTS(L_IDS);

	exception
		when others then
			L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
			PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

			return -1;
	end F_CONVERTFORECASTS;

   /*
   *   @@DESCRIPTION
   *      This function converts forecasts into new "real" revisions for the passed in array of ILR's
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      none
   *   @@RETURN
   *      number: 1 for success
   *            -1 for failure
   */
   function F_CONVERTFORECASTS(A_ILR_IDS in PKG_LEASE_CALC.NUM_ARRAY) return number is

      L_STATUS varchar2(2000);
      L_RTN    number;
	  L_ID_COUNT number;
	  L_IDS T_NUM_ARRAY;

   begin
	  PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_ILR.F_CONVERTFORECASTS');
	   
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('Converting forecast revisions');

	  L_STATUS := 'Initializing ILR Array';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
	  L_ID_COUNT := A_ILR_IDS.count;
	  if L_ID_COUNT = 0 then
		PKG_PP_LOG.P_WRITE_MESSAGE('-- Converting forecasts for all in-service ILR''s');
	  elsif L_ID_COUNT = 1 then
		PKG_PP_LOG.P_WRITE_MESSAGE('-- Converting forecasts for 1 ILR');
	  else
		PKG_PP_LOG.P_WRITE_MESSAGE('-- Converting forecasts for '||to_char(L_ID_COUNT)||' ILR''s');
	  end if;
	  
	  L_IDS := T_NUM_ARRAY();
	  for I in 1..A_ILR_IDS.count
	  loop
		L_IDS.extend;
		L_IDS(I) := A_ILR_IDS(I);
	  end loop;

      L_STATUS := 'Loading ILR approvals';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select A.ILR_ID,
                (select max(REVISION) + 1
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                A.APPROVAL_TYPE_ID,
                1
           from LS_ILR_APPROVAL A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading ILR options';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT
          --, payment_shift
          )
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                A.PURCHASE_OPTION_TYPE_ID,
                A.PURCHASE_OPTION_AMT,
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                A.TERMINATION_AMT
         --, payment_shift
           from LS_ILR_OPTIONS A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading ILR amounts';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                SET_OF_BOOKS_ID,
                NET_PRESENT_VALUE,
                INTERNAL_RATE_RETURN,
                CAPITAL_COST,
                CURRENT_LEASE_COST,
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading payment terms';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10)
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10
           from LS_ILR_PAYMENT_TERM A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading ilr_asset_map';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, REVISION, LS_ASSET_ID)
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                A.LS_ASSET_ID
           from LS_ILR_ASSET_MAP A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading ILR schedule';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_SCHEDULE
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
          EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
          EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
          EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
          EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
          CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
          CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          IS_OM, CURRENT_LEASE_COST)
         select SCH.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                SET_OF_BOOKS_ID,
                month,
                RESIDUAL_AMOUNT,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                INTEREST_ACCRUAL,
                PRINCIPAL_ACCRUAL,
                INTEREST_PAID,
                PRINCIPAL_PAID,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                IS_OM,
				CURRENT_LEASE_COST
           from LS_ILR_SCHEDULE SCH, LS_ILR ILR
          where SCH.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and REVISION = 9999;

      L_STATUS := 'Loading asset schedule';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ASSET_SCHEDULE
         (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
          EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
          EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
          EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
          EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
          CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
          CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          IS_OM, CURRENT_LEASE_COST)
         select LS_ASSET_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                SET_OF_BOOKS_ID,
                month,
                RESIDUAL_AMOUNT,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                INTEREST_ACCRUAL,
                PRINCIPAL_ACCRUAL,
                INTEREST_PAID,
                PRINCIPAL_PAID,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                IS_OM,
				CURRENT_LEASE_COST
           from LS_ASSET_SCHEDULE, LS_ILR ILR
          where LS_ASSET_SCHEDULE.LS_ASSET_ID in
                (select LS_ASSET_ID
                   from LS_ILR_ASSET_MAP
                  where ILR_ID = ILR.ILR_ID
                    and REVISION = (select max(REVISION)
                                      from LS_ILR_APPROVAL
                                     where ILR_ID = ILR.ILR_ID
                                       and REVISION < 9999))
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and REVISION = 9999;

	  PKG_PP_ERROR.REMOVE_MODULE_NAME;
      return 1;

   exception
      when others then
		 PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end F_CONVERTFORECASTS;

   /*
   *   @@DESCRIPTION
   *      This function copies an ILR into a new ILR
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new ilr
   *    number: in: a_pct = the percent to transfer
   *   @@RETURN
   *      number: the ls_ilr_id for success
   *            -1 for failure
   */
   function F_COPYILR(A_ILR_ID number,
                      A_PCT    number) return number is

      L_STATUS           varchar2(2000);
      L_REVISION         number;
      L_CURRENT_REVISION number;
      L_ILR_ID           number;

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW ILR from: ' || TO_CHAR(A_ILR_ID));

      L_STATUS := 'Get current revision';
      select CURRENT_REVISION into L_CURRENT_REVISION from LS_ILR where ILR_ID = A_ILR_ID;
      PKG_PP_LOG.P_WRITE_MESSAGE('   Revision: ' || TO_CHAR(L_CURRENT_REVISION));

      select LS_ILR_SEQ.NEXTVAL into L_ILR_ID from DUAL;
      PKG_PP_LOG.P_WRITE_MESSAGE('TO ILR: ' || TO_CHAR(L_ILR_ID));

      L_STATUS   := 'Get new revision';
      L_REVISION := 1;

      L_STATUS := 'LOAD ilr';
      insert into LS_ILR
         (ILR_ID, ILR_NUMBER, LEASE_ID, COMPANY_ID, EST_IN_SVC_DATE,
          EXTERNAL_ILR, ILR_STATUS_ID, ILR_GROUP_ID, NOTES,
          CURRENT_REVISION, WORKFLOW_TYPE_ID, RATE_GROUP_ID, FUNDING_STATUS_ID /* WMD */ )
         select L_ILR_ID,
		 /* CJS 3/3/15 Fixed TRF% functionality*/

                substr(replace(regexp_replace(A.ILR_NUMBER,'TRF(.)'),'() ') ||
                ' (TRF' ||
						  (select 1 + count(1)
               from LS_ILR LL



               where LL.ILR_NUMBER like '(TRF%) ' || replace(regexp_replace(A.ILR_NUMBER,'TRF(.)'),'() ')) || ') '
               , 0, 35),
                A.LEASE_ID,
                A.COMPANY_ID,
                A.EST_IN_SVC_DATE,
                A.EXTERNAL_ILR,
                1,
                A.ILR_GROUP_ID,
                A.NOTES,
                L_REVISION,
                A.WORKFLOW_TYPE_ID,
                A.RATE_GROUP_ID,
                A.FUNDING_STATUS_ID
           from LS_ILR A
          where A.ILR_ID = A_ILR_ID;

      L_STATUS := 'LOAD class codes';
      insert into LS_ILR_CLASS_CODE
         (CLASS_CODE_ID, ILR_ID, "VALUE")
         select A.CLASS_CODE_ID, L_ILR_ID, a."VALUE"
           from LS_ILR_CLASS_CODE A
          where A.ILR_ID = A_ILR_ID;


	  L_STATUS := 'LOAD ilr_account';
      insert into LS_ILR_ACCOUNT
         (ILR_ID, INT_ACCRUAL_ACCOUNT_ID, INT_EXPENSE_ACCOUNT_ID, EXEC_ACCRUAL_ACCOUNT_ID,
          EXEC_EXPENSE_ACCOUNT_ID, CONT_ACCRUAL_ACCOUNT_ID, CONT_EXPENSE_ACCOUNT_ID,
          CAP_ASSET_ACCOUNT_ID, ST_OBLIG_ACCOUNT_ID, LT_OBLIG_ACCOUNT_ID, AP_ACCOUNT_ID, RES_DEBIT_ACCOUNT_ID, RES_CREDIT_ACCOUNT_ID)
         select L_ILR_ID,
                A.INT_ACCRUAL_ACCOUNT_ID,
                A.INT_EXPENSE_ACCOUNT_ID,
                A.EXEC_ACCRUAL_ACCOUNT_ID,
                A.EXEC_EXPENSE_ACCOUNT_ID,
                A.CONT_ACCRUAL_ACCOUNT_ID,
                A.CONT_EXPENSE_ACCOUNT_ID,
                A.CAP_ASSET_ACCOUNT_ID,
                A.ST_OBLIG_ACCOUNT_ID,
                A.LT_OBLIG_ACCOUNT_ID,
                A.AP_ACCOUNT_ID,
                A.RES_DEBIT_ACCOUNT_ID,
                A.RES_CREDIT_ACCOUNT_ID
           from LS_ILR_ACCOUNT A
          where A.ILR_ID = A_ILR_ID;

      L_STATUS := 'LOAD ilr_approval';
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select L_ILR_ID, L_REVISION, A.APPROVAL_TYPE_ID, 1
           from LS_ILR_APPROVAL A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_options';
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT
          --, payment_shift
          )
         select L_ILR_ID,
                L_REVISION,
                A.PURCHASE_OPTION_TYPE_ID,
                ROUND(A_PCT * A.PURCHASE_OPTION_AMT, 2),
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                ROUND(A_PCT * A.TERMINATION_AMT, 2)
         --, payment_shift
           from LS_ILR_OPTIONS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_amounts';
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select L_ILR_ID,
                L_REVISION,
                A.SET_OF_BOOKS_ID,
                ROUND(A_PCT * NET_PRESENT_VALUE, 2),
                INTERNAL_RATE_RETURN,
                ROUND(A_PCT * CAPITAL_COST, 2),
                ROUND(A_PCT * CURRENT_LEASE_COST, 2),
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_payment_term';
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10)
         select L_ILR_ID,
                L_REVISION,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                ROUND(A_PCT * EST_EXECUTORY_COST, 2),
                ROUND(A_PCT * PAID_AMOUNT, 2),
                ROUND(A_PCT * CONTINGENT_AMOUNT, 2),
                CURRENCY_TYPE_ID,
                ROUND(A_PCT * C_BUCKET_1, 2),
                ROUND(A_PCT * C_BUCKET_2, 2),
                ROUND(A_PCT * C_BUCKET_3, 2),
                ROUND(A_PCT * C_BUCKET_4, 2),
                ROUND(A_PCT * C_BUCKET_5, 2),
                ROUND(A_PCT * C_BUCKET_6, 2),
                ROUND(A_PCT * C_BUCKET_7, 2),
                ROUND(A_PCT * C_BUCKET_8, 2),
                ROUND(A_PCT * C_BUCKET_9, 2),
                ROUND(A_PCT * C_BUCKET_10, 2),
                ROUND(A_PCT * E_BUCKET_1, 2),
                ROUND(A_PCT * E_BUCKET_2, 2),
                ROUND(A_PCT * E_BUCKET_3, 2),
                ROUND(A_PCT * E_BUCKET_4, 2),
                ROUND(A_PCT * E_BUCKET_5, 2),
                ROUND(A_PCT * E_BUCKET_6, 2),
                ROUND(A_PCT * E_BUCKET_7, 2),
                ROUND(A_PCT * E_BUCKET_8, 2),
                ROUND(A_PCT * E_BUCKET_9, 2),
                ROUND(A_PCT * E_BUCKET_10, 2)
           from LS_ILR_PAYMENT_TERM A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;


      return L_ILR_ID;

   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
   end F_COPYILR;

   /*
   *   @@DESCRIPTION
   *      This function copies an asset into another leased asset
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ls_asset_id = the asset to copy
   *    number: in: a_pct. Percent to copy
   *    number: in: a_qty.  The quantity to copy
   *   @@RETURN
   *      number: The ls_asset_id added for success
   *            -1 for failure
   */
   function F_COPYASSET(A_LS_ASSET_ID number,
                        A_PCT         number,
                        A_QTY         number) return number is

      L_STATUS     varchar2(2000);
      L_ASSET_ID   number;
      TEMP_DERIVER CR_DERIVER_TYPE;
      L_SQLS       varchar2(2000);
      L_FIELDS     varchar2(2000);
      L_COUNT      number;

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW Asset from: ' || TO_CHAR(A_LS_ASSET_ID));

      select LS_ASSET_SEQ.NEXTVAL into L_ASSET_ID from DUAL;
      PKG_PP_LOG.P_WRITE_MESSAGE('TO Asset: ' || TO_CHAR(L_ASSET_ID));

/* WMD */
      L_STATUS := 'LOAD asset';
      insert into LS_ASSET
         (LS_ASSET_ID, LEASED_ASSET_NUMBER, LS_ASSET_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION,
          /* WMD REMOVING UNNEEDED COLUMNS */
--          CURRENT_LEASE_DATE, INTERIM_INTEREST_BEGIN_DATE, GL_POSTING_BEGIN_DATE,
          TERMINATION_PENALTY_AMOUNT, sale_proceed_amount, EXPECTED_LIFE, ECONOMIC_LIFE, FMV, PROPERTY_GROUP_ID,
          UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID, RETIREMENT_UNIT_ID, WORK_ORDER_ID,
          FUNC_CLASS_ID, ASSET_LOCATION_ID, IN_SERVICE_DATE, SERIAL_NUMBER, ACTUAL_RESIDUAL_AMOUNT,
          GUARANTEED_RESIDUAL_AMOUNT, NOTES, COMPANY_ID, QUANTITY, IS_EARLY_RET, TAX_ASSET_LOCATION_ID, DEPARTMENT_ID,
		      property_tax_date, property_tax_amount, used_yn_sw, tax_summary_id, estimated_residual /*, capitalized_cost */)  /* WMD */
         select L_ASSET_ID,
		 /* CJS 3/3/15 Fixed TRF% functionality*/
               substr(replace(regexp_replace(a.leased_asset_number,'TRF(.)'),'() ') || ' (TRF' ||
                (select 1 + count(1)
                   from LS_ASSET LL
                  where LL.LEASED_ASSET_NUMBER like '(TRF%) ' ||
				        replace(regexp_replace(a.leased_asset_number,'TRF(.)'),'() ')) || ') '
                ,0, 35) ,
                1,
                substr('(TRF) ' || replace(DESCRIPTION, '(TRF) ', ''), 0 , 35),
                substr('(TRF) ' || replace(LONG_DESCRIPTION, '(TRF) ', ''), 0, 254),
                /* WMD REMOVING */
--                CURRENT_LEASE_DATE,
--                INTERIM_INTEREST_BEGIN_DATE,
--                GL_POSTING_BEGIN_DATE,
                ROUND(A_PCT * TERMINATION_PENALTY_AMOUNT, 2),
                sale_proceed_amount,
                EXPECTED_LIFE,
                ECONOMIC_LIFE,
                ROUND(A_PCT * FMV, 2),
                PROPERTY_GROUP_ID,
                UTILITY_ACCOUNT_ID,
                BUS_SEGMENT_ID,
                SUB_ACCOUNT_ID,
                RETIREMENT_UNIT_ID,
                WORK_ORDER_ID,
                FUNC_CLASS_ID,
                ASSET_LOCATION_ID,
                IN_SERVICE_DATE,
                SERIAL_NUMBER,
                ROUND(A_PCT * ACTUAL_RESIDUAL_AMOUNT, 2),
                ROUND(A_PCT * GUARANTEED_RESIDUAL_AMOUNT, 2),
                NOTES,
                COMPANY_ID,
                A_QTY,
                0,
                TAX_ASSET_LOCATION_ID,
                DEPARTMENT_ID, property_tax_date, property_tax_amount,
                used_yn_sw,
				tax_summary_id,/*, ROUND(A_PCT * CAPITALIZED_COST, 2) */ /* WMD */
        estimated_residual





           from LS_ASSET A
          where A.LS_ASSET_ID = A_LS_ASSET_ID;

      L_STATUS := 'LOAD class codes';

      insert into LS_ASSET_CLASS_CODE
         (CLASS_CODE_ID, LS_ASSET_ID, "VALUE")
         select A.CLASS_CODE_ID, L_ASSET_ID, a."VALUE"
           from LS_ASSET_CLASS_CODE A
          where A.LS_ASSET_ID = A_LS_ASSET_ID;

	  L_STATUS := 'LOAD cr deriver control';
      L_FIELDS := 'SCO_BILLING_TYPE_ID, VALIDATION_MESSAGE, DESCRIPTION, PERCENT, TYPE,  LS_JE_TRANS_TYPE, ';

      SELECT COUNT(1)
      INTO L_COUNT
      FROM ALL_TAB_COLUMNS
      WHERE TABLE_NAME = 'CR_DERIVER_CONTROL'
        AND COLUMN_NAME = 'TAX_LOCAL_ID';

      IF L_COUNT > 0 THEN
        L_FIELDS:= L_FIELDS || 'TAX_LOCAL_ID, ';
      END IF;



      for COL in (select ELEMENT_COLUMN from CR_ELEMENTS)
      loop
         L_FIELDS := L_FIELDS || COL.ELEMENT_COLUMN || ', ';
      end loop;
      L_FIELDS := RTRIM(L_FIELDS, ', ');
      L_SQLS   := 'insert into CR_DERIVER_CONTROL (STRING, ID, ' || L_FIELDS || ')';
      L_SQLS   := L_SQLS || ' select ''' || TO_CHAR(L_ASSET_ID) || '''|| '':'' || ls_je_trans_type

                   , costrepository.nextval, ' || L_FIELDS;
      L_SQLS   := L_SQLS ||
                  ' from CR_DERIVER_CONTROL where lower(type) in (''lessee'', ''lessee offset'')';
      L_SQLS   := L_SQLS || ' and substr(string, 0, instr(string, '':'') -1) = ''' || TO_CHAR(A_LS_ASSET_ID) || '''';

	  /* WMD */
      L_STATUS := L_STATUS || '. SQL: ' || L_SQLS;
      execute immediate L_SQLS;
	  
	  /* CJS 2/16/15 Looping over components to get charges correct */
	  /* CJS 2/13/15 Changing to pull right ls_asset_id */
	  /* WMD 12/15 adding manufacturer and model */ 
	  FOR i in (select component_id from ls_component where ls_asset_id = A_LS_ASSET_ID)
	  loop
      L_STATUS:='Copying Components';
      insert into ls_component
      (COMPONENT_ID, LS_COMP_STATUS_ID, COMPANY_ID, DATE_RECEIVED, DESCRIPTION, LONG_DESCRIPTION,
       SERIAL_NUMBER, PO_NUMBER, AMOUNT, LS_ASSET_ID, INTERIM_INTEREST, USER_ID, TIME_STAMP, PROP_TAX_EXEMPT, manufacturer, model)
      select
       ls_component_seq.nextval, LS_COMP_STATUS_ID, COMPANY_ID, DATE_RECEIVED, DESCRIPTION, LONG_DESCRIPTION,
       SERIAL_NUMBER, PO_NUMBER, AMOUNT, L_ASSET_ID, INTERIM_INTEREST, USER_ID, TIME_STAMP, PROP_TAX_EXEMPT, manufacturer, model
      from ls_component
      where ls_asset_id = A_LS_ASSET_ID
	  and component_id = i.component_id;
	  /* CJS 2/13/15 Adding component charge */
	  L_STATUS:='Copying Component Charges';
	  insert into ls_component_charge
	  (ID, COMPONENT_ID, INTERIM_INTEREST_START_DATE, INVOICE_DATE, INVOICE_NUMBER, AMOUNT)
	  select
	  (select nvl(max(ls_component_charge.id), 0) from ls_component_charge) + rownum,
	  ls_component_seq.currval as COMPONENT_ID,
	  CC.INTERIM_INTEREST_START_DATE,
	  CC.INVOICE_DATE,
	  CC.INVOICE_NUMBER,
	  CC.AMOUNT
	  from ls_component_charge cc
	  where cc.component_id = i.component_id;
	  end loop;

	  L_STATUS:='Updating Asset FMV';
	  update LS_ASSET
	  set FMV = (
	     select nvl(sum(CC.AMOUNT), 0)
		 from LS_COMPONENT_CHARGE CC, LS_COMPONENT LC
		 where CC.COMPONENT_ID = LC.COMPONENT_ID
		 and LC.LS_ASSET_ID = A_LS_ASSET_ID
		 and interim_interest_start_date is not null)
	  where LS_ASSET_ID = L_ASSET_ID
	  	and exists
          (select 1 from ls_asset a, ls_ilr i, ls_lease l, ls_lease_group g
            where A_LS_ASSET_ID = a.ls_asset_id
              and a.ilr_id = i.ilr_id
              and i.lease_id = l.lease_id
              and l.lease_group_id = g.lease_group_id
              and g.require_components = 1);
	  
      PKG_PP_LOG.P_WRITE_MESSAGE('Ending F_COPYASSET');
      return L_ASSET_ID;
   exception
      when others then
        PKG_PP_LOG.P_WRITE_MESSAGE('ERROR!');
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         return -1;
   end F_COPYASSET;

   function F_GETTAXES(A_ILR_ID number)
                     return varchar2 is
      L_STATUS varchar2(400);
      L_ASSETS PKG_LEASE_CALC.NUM_ARRAY;
		
   begin

      L_STATUS := 'Getting taxes for ILR_ID: '||to_char(A_ILR_ID);
      
      --create an array of assets under this ILR that do not already have a tax summary
      select LS_ASSET_ID
      bulk collect into L_ASSETS
      from LS_ASSET
      where ILR_ID = A_ILR_ID;

      --map the assets under this ILR to local taxes
      PKG_LEASE_ASSET_POST.P_GETTAXES(L_ASSETS);

      return 'OK';
   exception
      when others then
         return L_STATUS;
   end F_GETTAXES;
   
procedure P_CALC_II(A_ILR_ID number, A_REVISION in number) is
	II_AMOUNT number;
	type COMPONENT_TABLE is table of LS_COMPONENT%rowtype;
	L_COMPONENT_TABLE COMPONENT_TABLE;
	DAYS_IN_YEAR number;
	CUT_OFF_DAY number;
	IN_SVC_DATE date;
	i number;
	II_DATE date;
	II_MONTH date;
	DAYS_IN_MONTH_SW number;
	type COMPONENT_MONTHLY_II is table of LS_COMPONENT_MONTHLY_II_STG%rowtype;
	L_REC LS_COMPONENT_MONTHLY_II_STG%rowtype;
	L_COMPONENT_MONTHLY_II COMPONENT_MONTHLY_II := COMPONENT_MONTHLY_II();
	L_PROC_ID number;
   begin
    PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_ILR.P_CALC_II');

	select PROCESS_ID
	into L_PROC_ID
	from PP_PROCESSES
	where lower(DESCRIPTION) = 'lessee calculations'
	;

	PKG_PP_LOG.P_START_LOG(L_PROC_ID);
	/* CJS 4/27/15 Move before checking for anything to calculate; if in service date changed, rows might not get deleted */
		--clear out old records
	delete from LS_COMPONENT_MONTHLY_II_STG
	where COMPONENT_ID in (select LC.COMPONENT_ID
		from LS_COMPONENT LC
		join LS_ASSET LA on LC.LS_ASSET_ID = LA.LS_ASSET_ID
		join LS_ILR LI on LI.ILR_ID = LA.ILR_ID
		where LI.ILR_ID = A_ILR_ID);

  delete from ls_calc_ii_stg
  where component_id in (select component_id from ls_component where ls_asset_id in (select ls_asset_id from ls_asset where ilr_id = A_ILR_ID));

  insert into ls_calc_ii_stg
  (id, month, component_id, interim_interest_start_date, current_lease_cost, est_in_svc_date, cut_off_day, ii_rate, days_in_month_basis, days_in_year_basis)
  With all_months(month) as
  (select min(trunc(interim_interest_start_date,'month')) month
  from ls_component_charge cc, ls_component lc, ls_asset la
  where cc.component_id = lc.component_id
    and lc.ls_asset_id = la.ls_asset_id
    and la.ilr_id = A_ILR_ID
  UNION ALL
  select add_months(all_months.month,1) month
    from dual
      inner join all_months on months_between(
      (select trunc(est_in_svc_date, 'month') from ls_ilr where ilr_id = A_ILR_ID)
      , month) > 0)
  select
    cc.id,
    all_months.month,
    lc.component_id,
    cc.interim_interest_start_date,
    cc.amount,
    trunc(ilr.est_in_svc_date, 'month'),
    ll.cut_off_day,
    lir.rate,
    decode(ll.days_in_month_sw, 1, 30, extract(day from last_day(all_months.month))),
    ll.days_in_year
  from ls_component_charge cc, ls_component lc, ls_asset la, ls_ilr ilr, ls_lease ll, ls_lease_interim_rates lir, all_months
  where cc.component_id = lc.component_id
    and lc.ls_asset_id = la.ls_asset_id
    and la.ilr_id = ilr.ilr_id
    and ilr.lease_id = ll.lease_id
    and ll.lease_id = lir.lease_id
    and cc.interim_interest_start_date is not null
    and ilr.ilr_id = A_ILR_ID
    and all_months.month between trunc(cc.interim_interest_start_date,'month') and trunc(ilr.est_in_svc_date, 'month')
    and lir.month =
			  (
				  select max(l2.month)
				  from ls_lease_interim_rates l2
				  where l2.lease_id = lir.lease_id
				  and l2.month <= last_day(all_months.month)
			);

  /* Set interim interest month */
  update ls_calc_ii_stg
  set days_of_interest = days_in_month_basis - extract(day from interim_interest_start_date) + 1,
      roll_forward_amount = case when cut_off_day <=extract(day from interim_interest_start_date) then
      ii_rate * (1/days_in_year_basis) * nvl(days_in_month_basis - extract(day from interim_interest_start_date) + 1,0) * nvl(current_lease_cost,0) else 0 end,
      amount = case when cut_off_day <=extract(day from interim_interest_start_date) then 0 else
      ii_rate * (1/days_in_year_basis) * nvl(days_in_month_basis - extract(day from interim_interest_start_date) + 1,0) * nvl(current_lease_cost,0) end
  where trunc(interim_interest_start_date,'month') = month;


  /* Set other months other than in service */
  update ls_calc_ii_stg
  set days_of_interest = days_in_month_basis
  where est_in_svc_date > month
    and trunc(interim_interest_start_date,'month') <> month;

  /* Set Balances On Interim Row */
  update ls_calc_ii_stg
  set amount =  ii_rate * (1/days_in_year_basis) * nvl(days_of_interest,0) * nvl(current_lease_cost,0)
  where trunc(interim_interest_start_date,'month') <> month;



  /* Roll forward as necessary */
  merge into ls_calc_ii_stg a
  using(
  select id, add_months(month, 1) month, roll_forward_amount
  from ls_calc_ii_stg
  where nvl(roll_forward_amount,0) <> 0) b
  on (a.id = b.id and a.month = b.month)
  when matched then update set a.amount = nvl(a.amount,0) + nvl(b.roll_forward_amount,0);



  insert into LS_COMPONENT_MONTHLY_II_STG
  (component_id, month, amount)
  select
  component_id, month, sum(nvl(amount,0))
  from ls_calc_ii_stg
  group by component_id, month
  having sum(amount) <> 0;

	-- update ls_component
	update LS_COMPONENT LC
	set LC.INTERIM_INTEREST =
	(
		select nvl(sum(AA.AMOUNT),0)
		from LS_COMPONENT_MONTHLY_II_STG AA
		where AA.COMPONENT_ID = LC.COMPONENT_ID
	)
	where exists
	(
		select 1
		from LS_ASSET LA
		where LA.ILR_ID = A_ILR_ID
		and LC.LS_ASSET_ID = LA.LS_ASSET_ID
	);

  /* WMD we need to reset the interest in the in-service month so that we don't double it up when we re-run calcs */
merge into ls_calc_ii_stg a using (
select z.id, lc.ls_asset_id, z.month, ilrpt.paid_amount
from ls_asset la, ls_component lc, ls_calc_ii_stg z, ls_ilr ilr,
     (select *
      from ls_ilr_payment_term
      where ilr_id = A_ILR_ID
        and revision = A_REVISION
        and (ilr_id, payment_term_id) in (
                                          select ilr.ilr_id,  min(ilrpt.payment_term_id)
                                          from ls_ilr ilr, ls_ilr_payment_term ilrpt
                                          where ilr.ilr_id = ilrpt.ilr_id
                                            and ilr.est_in_svc_date >=ilrpt.payment_term_date
                                            and ilrpt.revision = A_REVISION
                                          group by ilr.ilr_id)) ILRPT
where z.component_id = lc.component_id
  and lc.ls_asset_id = la.ls_asset_id
  and la.ilr_id = ilrpt.ilr_id
  and la.ilr_id = A_ILR_ID
  and la.ilr_id = ilr.ilr_id
  and z.month >=trunc(ilr.est_in_svc_date,'month')) b
on (a.id = b.id and a.month = b.month)
when matched then update set payment_for_month = b.paid_amount;

merge into ls_calc_ii_stg a using (
select z.id, lc.ls_asset_id, z.month, las.principal_accrual
from ls_asset la, ls_component lc, ls_calc_ii_stg z, ls_ilr ilr,
     ls_asset_schedule las
where z.component_id = lc.component_id
  and lc.ls_asset_id = la.ls_asset_id
  and la.ilr_id = A_ILR_ID
  and la.ilr_id = ilr.ilr_id
  and z.month = las.month
  and las.revision = A_REVISION
  and las.ls_asset_id = la.ls_asset_id
  and z.month = z.est_in_svc_date
  ) b
on (a.id = b.id and a.month = b.month)
when matched then update set principal_for_month = b.principal_accrual;

merge into ls_asset_schedule a using(
select distinct lc.ls_asset_id, z.month, A_REVISION as revision, nvl(principal_for_month,0) principal_for_month,
        nvl(payment_for_month,0) * la.ratio payment_for_month
from ls_calc_ii_stg z, ls_component lc, ls_asset a,
     (select ls_asset_id, ratio_to_report(fmv) over(partition by ilr_id) ratio
      from ls_asset
      where ilr_id = A_ILR_ID
        and ls_asset_status_id = 3) la
where nvl(z.payment_for_month,0) <> 0
  and z.component_id = lc.component_id
  and lc.ls_asset_id = a.ls_asset_id
  and z.month >= (select min(gl_posting_mo_yr) from ls_process_control where company_id = a.company_id and lam_closed is null)
  and lc.ls_asset_id = la.ls_asset_id) b
on (a.ls_asset_id = b.ls_asset_id and a.month = b.month and a.revision = b.revision)
when matched then update set a.interest_accrual = nvl(b.payment_for_month,0) - nvl(b.principal_for_month,0),
                             a.interest_paid = nvl(b.payment_for_month,0) - nvl(b.principal_for_month,0);

	/* CJS 2/16/15 Updating schedules with amounts */
	update LS_ASSET_SCHEDULE LAS
	set (LAS.INTEREST_ACCRUAL, LAS.INTEREST_PAID) = (
	       select nvl(case when las.month >= trunc(ilr.est_in_svc_date,'month') then las.interest_accrual else 0 end + sum(cm.AMOUNT),0),
                nvl(case when las.month >= trunc(ilr.est_in_svc_date,'month') then las.interest_paid else 0 end + sum(cm.amount),0)
	       from ls_component_monthly_ii_stg cm, ls_component c, ls_asset la, ls_ilr ilr
	       where cm.component_id = c.component_id
	       and cm.month = las.month
	       and c.ls_asset_id = las.ls_asset_id
         and c.ls_asset_id = la.ls_asset_id
         and la.ilr_id = ilr.ilr_id
         and cm.month >= (select min(gl_posting_mo_yr) from ls_process_control where company_id = la.company_id and lam_closed is null)
         and la.ilr_id = A_ILR_ID
         group by ilr.est_in_svc_date)
	where las.ls_asset_id in (Select ls_asset_id from ls_asset where ilr_id = A_ILR_ID)
    and exists(
	   select 1
	   from ls_asset la, ls_component c, ls_component_monthly_ii_stg cm
	   where la.ilr_id = A_ILR_ID
	   and la.ls_asset_id = las.ls_asset_id
	   and la.ls_asset_id = c.ls_asset_id
	   and c.component_id = cm.component_id
     and cm.month>=(select min(gl_posting_mo_yr) from ls_process_control where company_id = la.company_id and lam_closed is null)
	   and cm.month = las.month
	   and las.revision = A_REVISION);

	--Updating ILR schedule
	update LS_ILR_SCHEDULE LIS
	set (LIS.INTEREST_ACCRUAL, LIS.INTEREST_PAID) = (
	       select nvl(sum(INTEREST_ACCRUAL),0), nvl(sum(INTEREST_ACCRUAL),0)
	       from ls_asset_schedule las, ls_asset la
	       where las.ls_asset_id = la.ls_asset_id
		   and las.month = lis.month
		   and las.revision = A_REVISION
		   and las.set_of_books_id = lis.set_of_books_id
		   and la.ilr_id = A_ILR_ID)
	where LIS.ILR_ID = A_ILR_ID
    and LIS.REVISION = A_REVISION
    and exists(
	   select 1
	   from ls_asset la, ls_component c, ls_component_monthly_ii_stg cm, ls_ilr ilr
	   where ilr.ilr_id = A_ILR_ID
	   and la.ilr_id = ilr.ilr_id
	   and la.ilr_id = lis.ilr_id
	   and la.ls_asset_id = c.ls_asset_id
	   and c.component_id = cm.component_id
	   and cm.month = lis.month
	   and lis.revision = A_REVISION);

  delete from ls_calc_ii_stg_arc
  where (id, month) in (select id, month from ls_calc_ii_stg);

  insert into ls_calc_ii_stg_arc
  select * from ls_calc_ii_stg;

  delete from ls_calc_ii_stg;

	PKG_PP_ERROR.REMOVE_MODULE_NAME;
   exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_CALC_II;
/* CJS Adding retirement revision function */
/*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in asset/ILR being retired/transferred
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a retired revision
   *      number: in: a_ls_asset_id = the ls_asset_id to create a retired revision
   *      date:   in: a_month = the retirement/transfer month to use
   *      number: in: a_flag = flag indicating transfer or retirement; 0 is retirement, 1 is transfer)
   *   @@RETURN
   *      number: 1 for success
   *             -1 for failure
   */
   function F_RETIREREVISION(A_ILR_ID number, A_LS_ASSET_ID number, A_MONTH date, A_FLAG number) return number is
      L_STATUS           varchar2(2000);
      L_REVISION         number;
      L_CURRENT_REVISION number;
	  L_PREV_REVISION    number;
	  L_RTN				 number;
	  L_SQLS			 varchar2(2000);
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('RETIREMENT Revision for: ' || TO_CHAR(A_ILR_ID));
	  L_STATUS := 'Get current revision';
      select CURRENT_REVISION into L_CURRENT_REVISION from LS_ILR where ILR_ID = A_ILR_ID;
      PKG_PP_LOG.P_WRITE_MESSAGE('   Current Revision: ' || TO_CHAR(L_CURRENT_REVISION));

	  L_STATUS := 'Get previous revision';
	  select max(REVISION)
	  into L_PREV_REVISION
	  from LS_ILR_APPROVAL
	  where ILR_ID = A_ILR_ID
	  and REVISION <> L_CURRENT_REVISION
	  and (APPROVAL_STATUS_ID = 3 or (APPROVAL_TYPE_ID = (select WORKFLOW_TYPE_ID
                                                               from WORKFLOW_TYPE
                                                               where lower(description) = 'auto approve')
                        and APPROVAL_STATUS_ID  = 1));

	  L_RTN:=0;
	  select count(1)
	  into L_RTN
	  from lS_ASSET
	  where ILR_ID = A_ILR_ID
	  and LS_ASSET_STATUS_ID <> 4;

	if L_RTN > 0 then /* Other assets associated with ILR besides retired asset exist */
		if A_FLAG = 0 then /* Retirement */
			L_STATUS:= 'Adding retired revision to current revision for ILR with other assets attached';
			PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
			/* Transfer is included in this portion; not possible in other scenario */
			
      if l_prev_revision is not null then
      --Clear out any previous rows to prevent PK errors
      delete from ls_asset_schedule where ls_asset_id = A_LS_ASSET_ID and revision = L_CURRENT_REVISION and month <= A_MONTH;
      
      PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount || ' rows deleted from asset schedule for revision ' || L_CURRENT_REVISION);
      insert into LS_ASSET_SCHEDULE(LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
			  BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
			  END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
			  EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
			  EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
			  EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
			  EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
			  EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
			  CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
			  CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
			  CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
			  CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
			  IS_OM)
			select
			  LS_ASSET_ID, L_CURRENT_REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
			  BEG_CAPITAL_COST, decode(month, A_MONTH, 0, END_CAPITAL_COST), BEG_OBLIGATION, decode(month, A_MONTH, 0, END_OBLIGATION),
			  BEG_LT_OBLIGATION, decode(month, A_MONTH, 0, END_LT_OBLIGATION),
			  INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL,
			  INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3,
			  EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
			  EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
			  EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
			  EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
			  CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
			  CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
			  CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
			  CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
			  IS_OM
			from LS_ASSET_SCHEDULE
			where LS_ASSET_ID = A_LS_ASSET_ID
			and REVISION = L_PREV_REVISION
			and MONTH <= A_MONTH;
			
      end if;
			L_STATUS := 'Update asset with current revision';
			update LS_ASSET
			set APPROVED_REVISION = L_CURRENT_REVISION
			where LS_ASSET_ID = A_LS_ASSET_ID;

			/* Leaving asset disassociated from ilr current revision for now (LS_ILR_ASSET_MAP) */
			
		else /* Transfer; A_FLAG = 1 */
		    
			L_STATUS := 'Clip transfer_from asset schedule';
			
			delete from LS_ASSET_SCHEDULE
			where LS_ASSET_ID = A_LS_ASSET_ID
			and REVISION = L_PREV_REVISION
			and month > A_MONTH;

			L_STATUS := 'Update ending amounts on transfer_from asset schedule';

			L_SQLS := 'update LS_ASSET_SCHEDULE set END_CAPITAL_COST = 0, END_OBLIGATION = 0, END_LT_OBLIGATION = 0';
			L_SQLS := L_SQLS||', INTEREST_ACCRUAL = 0, INTEREST_PAID = 0, PRINCIPAL_ACCRUAL = 0, PRINCIPAL_PAID = 0';

			for i in (select rent_type, bucket_number from ls_rent_bucket_admin where status_code_id = 1 order by 1,2)
			loop
			   L_SQLS := L_SQLS || ', '||i.rent_type||'_ACCRUAL'||i.bucket_number||' = 0, '||i.rent_type||'_PAID'||i.bucket_number||' = 0';
			end loop;

			L_SQLS := L_SQLS || ' where ls_asset_id = '||A_LS_ASSET_ID||' and revision = '||L_PREV_REVISION;
			L_SQLS := L_SQLS || ' and month = to_date('||to_char(A_MONTH,'yyyymm')||',''yyyymm'')';

			execute immediate L_SQLS;
		end if;
	else /* No assets associated with ILR other than retired/transferred asset; L_RTN = 0 */
		if A_FLAG = 0 then /* retired asset revision */
		  L_STATUS := 'Get new revision';
		  select nvl(max(REVISION),0) + 1 into L_REVISION from LS_ILR_APPROVAL where ILR_ID = A_ILR_ID and REVISION < 9999;
		  PKG_PP_LOG.P_WRITE_MESSAGE('   RETIRE Revision: ' || TO_CHAR(L_REVISION));

		  L_STATUS := 'LOAD ilr_approval';
		  insert into LS_ILR_APPROVAL
			 (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
			 select A.ILR_ID, L_REVISION, A.APPROVAL_TYPE_ID, 3
			   from LS_ILR_APPROVAL A
			  where A.ILR_ID = A_ILR_ID
				and A.REVISION = L_CURRENT_REVISION;

		  L_STATUS := 'LOAD ilr_options';
		  insert into LS_ILR_OPTIONS
			 (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
			  CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
			  LEASE_CAP_TYPE_ID, TERMINATION_AMT
			  --, payment_shift
			  )
			 select A.ILR_ID,
					L_REVISION,
					A.PURCHASE_OPTION_TYPE_ID,
					A.PURCHASE_OPTION_AMT,
					A.RENEWAL_OPTION_TYPE_ID,
					A.CANCELABLE_TYPE_ID,
					A.ITC_SW,
					A.PARTIAL_RETIRE_SW,
					A.SUBLET_SW,
					A.MUNI_BO_SW,
					A.INCEPTION_AIR,
					A.LEASE_CAP_TYPE_ID,
					A.TERMINATION_AMT
			 --, payment_shift
			   from LS_ILR_OPTIONS A
			  where A.ILR_ID = A_ILR_ID
				and A.REVISION = L_CURRENT_REVISION;

		  L_STATUS := 'LOAD ilr_amounts';
		  insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
			 (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
			  CURRENT_LEASE_COST, IS_OM)
			 select A.ILR_ID,
					L_REVISION,
					SET_OF_BOOKS_ID,
					NET_PRESENT_VALUE,
					INTERNAL_RATE_RETURN,
					CAPITAL_COST,
					CURRENT_LEASE_COST,
					IS_OM
			   from LS_ILR_AMOUNTS_SET_OF_BOOKS A
			  where A.ILR_ID = A_ILR_ID
				and A.REVISION = L_CURRENT_REVISION;

		  L_STATUS := 'LOAD ilr_payment_term';
		  insert into LS_ILR_PAYMENT_TERM
			 (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
			  PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
			  CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
			  C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
			  E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10)
			 select A.ILR_ID,
					L_REVISION,
					PAYMENT_TERM_ID,
					PAYMENT_TERM_TYPE_ID,
					PAYMENT_TERM_DATE,
					PAYMENT_FREQ_ID,
					NUMBER_OF_TERMS,
					EST_EXECUTORY_COST,
					PAID_AMOUNT,
					CONTINGENT_AMOUNT,
					CURRENCY_TYPE_ID,
					C_BUCKET_1,
					C_BUCKET_2,
					C_BUCKET_3,
					C_BUCKET_4,
					C_BUCKET_5,
					C_BUCKET_6,
					C_BUCKET_7,
					C_BUCKET_8,
					C_BUCKET_9,
					C_BUCKET_10,
					E_BUCKET_1,
					E_BUCKET_2,
					E_BUCKET_3,
					E_BUCKET_4,
					E_BUCKET_5,
					E_BUCKET_6,
					E_BUCKET_7,
					E_BUCKET_8,
					E_BUCKET_9,
					E_BUCKET_10
			   from LS_ILR_PAYMENT_TERM A
			  where A.ILR_ID = A_ILR_ID
				and A.REVISION = L_CURRENT_REVISION;

		  L_STATUS := 'LOAD ilr_asset_map';
		  insert into LS_ILR_ASSET_MAP
			 (ILR_ID, REVISION, LS_ASSET_ID)
			 select A_ILR_ID, L_REVISION, A_LS_ASSET_ID
			   from dual;

			L_STATUS := 'LOAD asset schedule for retire revision';
		insert into LS_ASSET_SCHEDULE(LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
			  BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
			  END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
			  EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
			  EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
			  EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
			  EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
			  EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
			  CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
			  CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
			  CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
			  CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
			  IS_OM, CURRENT_LEASE_COST)
			select LS_ASSET_ID, L_REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
			  BEG_CAPITAL_COST, decode(month, A_MONTH, 0, END_CAPITAL_COST), BEG_OBLIGATION,
			  decode(month, A_MONTH, 0, END_OBLIGATION), BEG_LT_OBLIGATION,
			  decode(month, A_MONTH, 0, END_LT_OBLIGATION), INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
			  EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
			  EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
			  EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
			  EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
			  EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
			  CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
			  CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
			  CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
			  CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
			  IS_OM, CURRENT_LEASE_COST
			from LS_ASSET_SCHEDULE
			where LS_ASSET_ID = A_LS_ASSET_ID
			and REVISION = L_CURRENT_REVISION
			and MONTH <= A_MONTH;

			L_STATUS := 'Update asset with retire revision';
			update LS_ASSET
			set APPROVED_REVISION = L_REVISION
			where LS_ASSET_ID = A_LS_ASSET_ID;

			L_STATUS := 'LOAD ilr_schedule';
			insert into LS_ILR_SCHEDULE
			 (ILR_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
			  BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
			  END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
			  EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
			  EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
			  EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
			  EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
			  EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
			  CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
			  CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
			  CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
			  CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
			  IS_OM, CURRENT_LEASE_COST)
			select
			  ILR_ID, L_REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
			  BEG_CAPITAL_COST, decode(month, A_MONTH, 0, END_CAPITAL_COST), BEG_OBLIGATION,
				decode(month, A_MONTH, 0, END_OBLIGATION), BEG_LT_OBLIGATION,
			  decode(month, A_MONTH, 0, END_LT_OBLIGATION), INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
			  EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
			  EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
			  EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
			  EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
			  EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
			  CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
			  CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
			  CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
			  CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
			  IS_OM, CURRENT_LEASE_COST
			from LS_ILR_SCHEDULE
			where ILR_ID = A_ILR_ID
			and REVISION = L_CURRENT_REVISION
			and MONTH <= A_MONTH;

			L_STATUS:='Updating ILR current revision';
			update LS_ILR
			set CURRENT_REVISION = L_REVISION
			where ILR_ID = A_ILR_ID;
		else /* A_FLAG = 1. Intercompany transfer could leave asset/ILR by itself also if a full transfer.
				 Need to clip previous schedule if nothing there for current revision, otherwise leave as is */
			L_STATUS:= 'Checking for transfer_from schedule related to current revision';
			select count(1)
			into L_RTN
			from LS_ILR_SCHEDULE
			where ILR_ID = A_ILR_ID
			and REVISION = L_CURRENT_REVISION;

			if L_RTN = 0 then

				L_STATUS:= 'Clipping TRANSFER_FROM ILR/Asset schedule for revision: '||L_PREV_REVISION;
				PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

				L_STATUS:= 'Clipping transfer_from asset schedule';
				delete from LS_ASSET_SCHEDULE
				where LS_ASSET_ID = A_LS_ASSET_ID
				and REVISION = L_PREV_REVISION
				and month > A_MONTH;

				L_STATUS:= 'Clipping transfer_from ILR schedule';
				delete from LS_ILR_SCHEDULE
				where ILR_ID = A_ILR_ID
				and REVISION = L_PREV_REVISION
				and month > A_MONTH;

				L_STATUS := 'Update ending amounts on transfer_from asset schedule';

				L_SQLS := 'update LS_ASSET_SCHEDULE set END_CAPITAL_COST = 0, END_OBLIGATION = 0, END_LT_OBLIGATION = 0, CURRENT_LEASE_COST = 0';
				L_SQLS := L_SQLS||', INTEREST_ACCRUAL = 0, INTEREST_PAID = 0, PRINCIPAL_ACCRUAL = 0, PRINCIPAL_PAID = 0';

				for i in (select rent_type, bucket_number from ls_rent_bucket_admin where status_code_id = 1 order by 1,2)
				loop
				   L_SQLS := L_SQLS || ', '||i.rent_type||'_ACCRUAL'||i.bucket_number||' = 0, '||i.rent_type||'_PAID'||i.bucket_number||' = 0';
				end loop;

				L_SQLS := L_SQLS || ' where ls_asset_id = '||A_LS_ASSET_ID||' and revision = '||L_PREV_REVISION;
				L_SQLS := L_SQLS || ' and month = to_date('||to_char(A_MONTH,'yyyymm')||',''yyyymm'')';

				execute immediate L_SQLS;

				L_STATUS := 'Update ending amounts on transfer_from ILR schedule';

				L_SQLS := 'update LS_ILR_SCHEDULE set END_CAPITAL_COST = 0, END_OBLIGATION = 0, END_LT_OBLIGATION = 0, CURRENT_LEASE_COST = 0';
				L_SQLS := L_SQLS||', INTEREST_ACCRUAL = 0, INTEREST_PAID = 0, PRINCIPAL_ACCRUAL = 0, PRINCIPAL_PAID = 0';

				for i in (select rent_type, bucket_number from ls_rent_bucket_admin where status_code_id = 1 order by 1,2)
				loop
				   L_SQLS := L_SQLS || ', '||i.rent_type||'_ACCRUAL'||i.bucket_number||' = 0, '||i.rent_type||'_PAID'||i.bucket_number||' = 0';
				end loop;

				L_SQLS := L_SQLS || ' where ilr_id = '||A_ILR_ID||' and revision = '||L_PREV_REVISION;
				L_SQLS := L_SQLS || ' and month = to_date('||to_char(A_MONTH,'yyyymm')||',''yyyymm'')';

				execute immediate L_SQLS;
			end if;
		end if;
	end if;

			L_STATUS:='Finished loading retirement revision';
			PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

      return 1;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         return -1;
   end F_RETIREREVISION;
--**************************************************************************
--                            Initialize Package
--**************************************************************************

end PKG_LEASE_ILR;
/



--************************** 
-- Log the run of the script 
--************************** 

insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (3014, 0, 2015, 2, 1, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2015.2.1.0_PKG_LEASE_ILR.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
