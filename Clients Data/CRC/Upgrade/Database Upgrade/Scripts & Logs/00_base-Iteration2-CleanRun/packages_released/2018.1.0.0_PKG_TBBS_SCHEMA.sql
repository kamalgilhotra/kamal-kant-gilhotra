/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   PKG_TBBS_SCHEMA.sql
||========================================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Revised By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2016.1.0.0 09/2/2016  Jared Watkins        Harvest package into base
||========================================================================================
*/
CREATE OR REPLACE PACKAGE pkg_tbbs_schema AS
    G_PKG_VERSION varchar(35) := '2018.1.0.0';
	function f_add_like_schema(source_id NUMBER, new_description varchar2) return number;
END pkg_tbbs_schema;
/

CREATE OR REPLACE PACKAGE BODY pkg_tbbs_schema AS
--**************************************************************************
--       VARIABLES
--**************************************************************************
  g_process_id pp_processes.process_id%TYPE;
  G_MSG VARCHAR2(254);
--**************************************************************************
--       PROCEDURES
--**************************************************************************
  PROCEDURE p_setprocess IS
    BEGIN
    g_msg := 'Unable to find the Process ID for "TBBS - PROCESS SCHEMATA" in pp_processes';
    SELECT process_id
    INTO g_process_id
    FROM pp_processes
    WHERE UPPER(DESCRIPTION) = 'TBBS - MANAGE SCHEMATA';

    exception
    WHEN others THEN
      raise_application_error(-20000, g_msg || ': ' || sqlerrm);
  END P_SETPROCESS;

--**************************************************************************
--       FUNCTIONS
--**************************************************************************
  FUNCTION f_add_like_schema(source_id NUMBER, new_description VARCHAR2) RETURN NUMBER AS
  BEGIN
    PKG_TBBS_SCHEMA.P_SETPROCESS;
    pkg_pp_error.set_module_name('PKG_TBBS_ACCOUNT.F_ADD_LIKE_SCHEMA');

    G_MSG := 'Inserting new schema into tbbs_schema';

    INSERT INTO tbbs_schema(schema_id, description)
    SELECT tbbs_schema_seq.nextval, substr(new_description, 0, 50)
    FROM dual;

    G_MSG := 'Inserting book assignments into tbbs_book_assign';
    INSERT INTO tbbs_book_assign(schema_id, line_item_id, book_account_id)
    SELECT tbbs_schema_seq.currval, line_item_id, book_account_id
    FROM tbbs_book_assign
    WHERE schema_id = source_id;

    G_MSG := 'Inserting tax assignments into tbbs_tax_assign';
    INSERT INTO tbbs_tax_assign(schema_id, line_item_id, m_item_id)
    SELECT tbbs_schema_seq.currval, line_item_id, m_item_id
    FROM tbbs_tax_assign
    WHERE schema_id = source_id;

    G_MSG := 'Inserting unused book accounts into tbbs_unused_book_account';
    INSERT INTO tbbs_unused_book_account(schema_id, book_account_id)
    SELECT tbbs_schema_seq.currval, book_account_id
    FROM tbbs_unused_book_account
    WHERE schema_id = source_id;

    G_MSG := 'Inserting unused M-Items into tbbs_unused_m_item';
    INSERT INTO tbbs_unused_m_item(schema_id, m_item_id)
    SELECT tbbs_schema_seq.currval, m_item_id
    FROM tbbs_unused_m_item
    WHERE schema_id = source_id;


	  PKG_PP_ERROR.REMOVE_MODULE_NAME;

    RETURN tbbs_schema_seq.currval;

    exception
    WHEN others THEN
      raise_application_error(-20000, g_msg || ': ' || sqlerrm);

  END f_add_like_schema;
END pkg_tbbs_schema;
/

--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (11682, 0, 2018, 1, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2018.1.0.0_PKG_TBBS_SCHEMA.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
