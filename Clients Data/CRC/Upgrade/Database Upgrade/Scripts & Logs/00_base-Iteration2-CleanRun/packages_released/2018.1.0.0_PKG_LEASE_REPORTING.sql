create or replace package PKG_LEASE_REPORTING is
  /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_REPORTING
   || Description: Used to return multi-currency schedule results for 
   ||              disclosure reporting purposes.
   ||============================================================================
   || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version    Date       Revised By     Reason for Change
   || ---------- ---------- -------------- --------------------------------------
   || 2017.4.0.2 08/24/2018 SBYERS         Initial version
   ||============================================================================
   */
  G_PKG_VERSION varchar(35) := '2018.1.0.0';
  
  type t_month_range_rec is record (
    start_date date,
    end_date   date
  );
  
  type t_month_range_table is table of t_month_range_rec index by pls_integer;
  
  function f_get_month_range(A_START_DATE date,
                             A_END_DATE date,
                             A_MONTH_MOD integer)
  return t_month_range_table;
  
  procedure p_load_ls_curr_fx_temp(A_START_DATE date,
                                   A_END_DATE date);
  
  procedure p_get_ilr_schedule(A_REPORT_ID number,
                               A_LS_CUR_TYPE number,
                               A_SET_OF_BOOKS_ID number,
                               A_START_DATE date,
                               A_END_DATE date,
                               A_GET_DEPR_DATA number);
  
  procedure p_get_future_payments(A_LS_CUR_TYPE number,
                                  A_SET_OF_BOOKS_ID number,
                                  A_START_DATE date,
                                  A_END_DATE date,
								  A_RATE_START_DATE date);
								  
  procedure p_get_npv(A_LS_CUR_TYPE number,
                      A_SET_OF_BOOKS_ID number,
                      A_START_DATE date,
                      A_END_DATE date);
                                            
  procedure p_get_future_liabilities(A_LS_CUR_TYPE number,
                                     A_SET_OF_BOOKS_ID number,
                                     A_START_DATE date,
                                     A_END_DATE date);                             

end PKG_LEASE_REPORTING;
/
create or replace package body PKG_LEASE_REPORTING is

  function f_get_month_range(A_START_DATE date,
                             A_END_DATE date,
                             A_MONTH_MOD integer)
  return t_month_range_table
  is
    L_MONTH_RANGE_TAB t_month_range_table;
  begin
    select add_months(start_date, (level - 1) * A_MONTH_MOD) start_date, 
           case when add_months(start_date, level * A_MONTH_MOD - 1) > end_date then
             end_date
           else
             add_months(start_date, level * A_MONTH_MOD - 1)
           end end_date
      bulk collect
      into l_month_range_tab
      from (select A_START_DATE start_date, A_END_DATE end_date from dual)
     where end_date > start_date
    connect by level <= ceil(months_between(add_months(end_date,1), start_date)/A_MONTH_MOD);
    
    RETURN L_MONTH_RANGE_TAB;
  end f_get_month_range;
  
  procedure p_load_ls_curr_fx_temp(A_START_DATE date,
                                   A_END_DATE date) is
    CURR_FIRST date;
    CURR_LAST date;
  begin
    -- Get the dates that are missing from the table
    select min(dates), max(dates)
        into CURR_FIRST, CURR_LAST
        from (
          select add_months(start_date, level - 1) dates
            from (select A_START_DATE start_date, nvl(A_END_DATE,to_date(300001,'YYYYMM')) end_date from dual)
          connect by level <= months_between(end_date, start_date) + 1
          minus
          select month from ls_curr_fx_temp);
      
    -- Load the table
   insert into ls_curr_fx_temp (
      currency_from, currency_to, month, act_month_rate, avg_month_rate, act_now_rate)
    with contract_curr as (
         select /*+ MATERIALIZE */ distinct contract_currency_id from ls_lease
		  union
		    SELECT DISTINCT d.currency_id
			 FROM ls_lease a, ls_lease_company b, company c, currency_schema d
			 WHERE a.lease_id = b.lease_id
			 AND b.company_id = c.company_id
			 AND d.company_id = c.company_id
			 AND d.currency_type_id = 1
         )
    select crdd1.currency_from, crdd1.currency_to, trunc(crdd1.exchange_date,'fmmonth') month, 
           crdd1.rate act_month_rate, nvl(crdd4.rate, crdd1.rate) avg_month_rate, cr_now.rate act_now_rate
      from (
             select currency_from, currency_to, rate, exchange_date, exchange_rate_type_id
               from currency_rate_default_dense
              where trunc(exchange_date,'fmmonth') between trunc(CURR_FIRST,'fmmonth') and trunc(CURR_LAST,'fmmonth')
                and exchange_rate_type_id = 1
                and currency_from in (select contract_currency_id from contract_curr)
           ) crdd1
      join (
             select currency_from, currency_to, rate, exchange_date, exchange_rate_type_id
               from currency_rate_default_dense d
              where trunc(exchange_date, 'fmmonth') <= trunc(sysdate, 'fmmonth')
                and exchange_rate_type_id = 1
                and (currency_from, currency_to, exchange_date) in (
                    select currency_from, currency_to, max(exchange_date) exchange_date
                      from currency_rate_default_dense d
                     where trunc(exchange_date, 'fmmonth') <= trunc(sysdate, 'fmmonth')
                       and exchange_rate_type_id = 1
                       and currency_from in (select contract_currency_id from contract_curr)
                     group by currency_from, currency_to)
           ) cr_now on cr_now.exchange_rate_type_id = crdd1.exchange_rate_type_id
                 and cr_now.currency_from = crdd1.currency_from
                 and cr_now.currency_to = crdd1.currency_to
      left outer join (
             select currency_from, currency_to, rate, exchange_date, exchange_rate_type_id
               from currency_rate_default_dense
              where trunc(exchange_date,'fmmonth') between trunc(CURR_FIRST,'fmmonth') and trunc(CURR_LAST,'fmmonth')
                and exchange_rate_type_id = 4
                and currency_from in (select contract_currency_id from contract_curr)
           ) crdd4 on trunc(crdd4.exchange_date,'fmmonth') = trunc(crdd1.exchange_date, 'fmmonth')
                  and crdd4.currency_from = crdd1.currency_from
                  and crdd4.currency_to = crdd1.currency_to
    where not exists (
        select 1 from ls_curr_fx_temp a
         where a.currency_from = crdd1.currency_from
           and a.currency_to = crdd1.currency_to
           and a.month = trunc(crdd1.exchange_date,'fmmonth'));
  end p_load_ls_curr_fx_temp;
  
  procedure p_load_ls_ilr_war_fx_temp is
    WAR_COUNT number(22,0);
  begin
    -- Get the ILR weighted average rates
    select count(*) into WAR_COUNT from ls_ilr_war_fx_temp;
      
    if WAR_COUNT = 0 then
      insert into ls_ilr_war_fx_temp (
        ilr_id, revision, set_of_books_id, gross_weighted_avg_rate, net_weighted_avg_rate, 
        in_service_exchange_rate, effective_month)
      with weighted_avg_rates as (
             select /*+ MATERIALIZE */ * from ls_ilr_weighted_avg_rates
           ),
           ilr as (
             select /*+ MATERIALIZE */ ilr_id, current_revision, est_in_svc_date
               from ls_ilr
              where current_revision > 0
           ),
           ilr_options as (
             select /*+ MATERIALIZE */ o.ilr_id, o.revision, o.in_service_exchange_rate, 
                    trunc(nvl(o.remeasurement_date, ilr.est_in_svc_date), 'fmmonth') effective_month
               from ls_ilr_options o
               join ilr on o.ilr_id = ilr.ilr_id
              where o.revision > 0
           )
      select war.ilr_id, 
             war.revision, 
             war.set_of_books_id,
             war.gross_weighted_avg_rate, 
             war.net_weighted_avg_rate, 
             lio.in_service_exchange_rate,
             lio.effective_month
        from weighted_avg_rates war
        join ilr_options lio ON lio.ilr_id = war.ilr_id AND lio.revision = war.revision
      union
      select lio.ilr_id,
             lio.revision,
             sob.set_of_books_id,
             lio.in_service_exchange_rate,
             lio.in_service_exchange_rate,
             lio.in_service_exchange_rate,
             lio.effective_month
        from ilr_options lio 
       cross join set_of_books sob
       where (lio.ilr_id, lio.revision) not in (select ilr_id, revision from weighted_avg_rates);
    end if;
  end p_load_ls_ilr_war_fx_temp;
  
  procedure p_get_ilr_schedule(A_REPORT_ID number,
                               A_LS_CUR_TYPE number,
                               A_SET_OF_BOOKS_ID number,
                               A_START_DATE date,
                               A_END_DATE date,
                               A_GET_DEPR_DATA number) is
   
    SCHED_FIRST date;
    SCHED_LAST date;
    
    DEPR_FIRST date;
    DEPR_LAST date;
    
  begin
    if a_ls_cur_type = 2 then
      -- Get the ILR weighted average rates
      p_load_ls_ilr_war_fx_temp;
      
      -- Get the rates for the months being queried
      p_load_ls_curr_fx_temp(A_START_DATE, A_END_DATE);
       
      -- load the schedule table 
      select min(month), max(month)
        into SCHED_FIRST, SCHED_LAST
        from (
          select A_LS_CUR_TYPE ls_cur_type, A_SET_OF_BOOKS_ID set_of_books_id, add_months(start_date, level - 1) month
            from (select A_START_DATE start_date, nvl(A_END_DATE,to_date(300001,'YYYYMM')) end_date from dual)
          connect by level <= months_between(end_date, start_date) + 1
          minus
          select ls_cur_type, set_of_books_id, month from ls_ilr_schedule_fx_temp);
      
      insert into ls_ilr_schedule_fx_temp (
        ls_cur_type, ilr_id, revision, set_of_books_id, month, company_id, lease_id, is_om, iso_code, currency_display_symbol,
        residual_amount, term_penalty, bpo_price, beg_capital_cost, end_capital_cost,
        beg_obligation, end_obligation, beg_lt_obligation, end_lt_obligation,
        beg_liability, end_liability, beg_lt_liability, end_lt_liability,
        interest_accrual, interest_paid, principal_paid,
        executory_paid1, executory_paid2, executory_paid3, executory_paid4, executory_paid5,
        executory_paid6, executory_paid7, executory_paid8, executory_paid9, executory_paid10,
        contingent_paid1, contingent_paid2, contingent_paid3, contingent_paid4, contingent_paid5,
        contingent_paid6, contingent_paid7, contingent_paid8, contingent_paid9, contingent_paid10,
        executory_adjust, contingent_adjust, beg_net_wavg_rate, curr_net_wavg_rate, remeasurement_date,
        currency_id)
      with lease_co as (
             select /*+ MATERIALIZE */ c.company_id, c.description, cs.currency_id
               from company c
               join currency_schema cs on c.company_id = cs.company_id
              where c.is_lease_company = 1
                and cs.currency_type_id = 1
           ),
           sc as (
              select /*+ MATERIALIZE */ * 
                from pp_system_control_company
               where trim(lower(control_name)) = 'lease mc: use average rates'
           ),
           calc_rate as(
             select /*+ MATERIALIZE */ cr1.company_id, cr1.contract_currency_id, cr1.company_currency_id, 
                    cr1.accounting_month, cr1.rate act_rate, nvl(cr4.rate, cr1.rate) avg_rate
              from ls_lease_calculated_date_rates cr1
              left outer join (
                   select company_id, contract_currency_id, company_currency_id, accounting_month, rate
                     from ls_lease_calculated_date_rates
                    where trunc(accounting_month,'fmmonth') between add_months(trunc(SCHED_FIRST, 'fmmonth'), -1)
                                                                and trunc(SCHED_LAST)
                      and exchange_rate_type_id = 4
                              ) cr4 on cr1.company_id = cr4.company_id
                                   and cr1.contract_currency_id = cr4.contract_currency_id
                                   and cr1.company_currency_id = cr4.company_currency_id
                                   and cr1.accounting_month = cr4.accounting_month
             where trunc(cr1.accounting_month,'fmmonth') between add_months(trunc(SCHED_FIRST,'fmmonth'), -1)
                                                             and trunc(SCHED_LAST, 'fmmonth')
               and cr1.exchange_rate_type_id = 1
           )
      select A_LS_CUR_TYPE, s.ilr_id, s.revision, s.set_of_books_id, s.month, i.company_id, i.lease_id, 
             s.is_om, cur.iso_code, cur.currency_display_symbol,
             s.residual_amount * nvl(calc_rate.act_rate, cr.act_month_rate), 
             s.term_penalty * nvl(calc_rate.act_rate, cr.act_month_rate),  
             s.bpo_price * nvl(calc_rate.act_rate, cr.act_month_rate), 
             s.beg_capital_cost * nvl(war2.gross_weighted_avg_rate, cr.act_now_rate), 
             s.end_capital_cost * nvl(case when s.month < nvl(o.remeasurement_date, s.month) 
                                           then war2.gross_weighted_avg_rate
                                           else war.gross_weighted_avg_rate
                                      end, cr.act_now_rate), 
             s.beg_obligation * nvl(prev_calc_rate.act_rate, cr.act_month_rate), 
             s.end_obligation * nvl(calc_rate.act_rate, cr.act_month_rate),  
             s.beg_lt_obligation * nvl(prev_calc_rate.act_rate, cr.act_month_rate), 
             s.end_lt_obligation * nvl(calc_rate.act_rate, cr.act_month_rate),  
             s.beg_liability * nvl(prev_calc_rate.act_rate, cr.act_month_rate), 
             s.end_liability * nvl(calc_rate.act_rate, cr.act_month_rate),  
             s.beg_lt_liability * nvl(prev_calc_rate.act_rate, cr.act_month_rate), 
             s.end_lt_liability * nvl(calc_rate.act_rate, cr.act_month_rate),  
             s.interest_accrual * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.interest_paid * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.principal_paid * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.executory_paid1 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.executory_paid2 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.executory_paid3 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.executory_paid4 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.executory_paid5 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.executory_paid6 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.executory_paid7 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.executory_paid8 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.executory_paid9 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.executory_paid10 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.contingent_paid1 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.contingent_paid2 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.contingent_paid3 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.contingent_paid4 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.contingent_paid5 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.contingent_paid6 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.contingent_paid7 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.contingent_paid8 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.contingent_paid9 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.contingent_paid10 * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.executory_adjust * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             s.contingent_adjust * decode(avg_rate_cv.control_value, 'yes', nvl(calc_rate.avg_rate, cr.avg_month_rate), nvl(calc_rate.act_rate, cr.act_month_rate)), 
             nvl(war2.net_weighted_avg_rate, cr.act_now_rate) beg_net_wavg_rate,
             nvl(case when s.month < nvl(o.remeasurement_date, s.month) 
                      then war2.net_weighted_avg_rate
                      else war.net_weighted_avg_rate
                 end, cr.act_now_rate) curr_net_wavg_rate,
             o.remeasurement_date,
             c.currency_id
        from ls_ilr_schedule s
        inner join ls_ilr i on i.ilr_id = s.ilr_id and i.current_revision = s.revision
        inner join ls_ilr_options o on o.ilr_id = s.ilr_id and o.revision = s.revision
        inner join lease_co c on c.company_id = i.company_id
        inner join ls_lease l on l.lease_id = i.lease_id
        inner join (
          select sc.company_id, lower(trim(sc.control_value)) control_value
             from sc
            where sc.company_id <> -1
          union all
          select c.company_id, lower(trim(sc.control_value)) control_value
            from sc
           cross join (select company_id from lease_co
                       minus
                       select company_id from sc b) c
           where sc.company_id = -1
                   ) avg_rate_cv on avg_rate_cv.company_id = i.company_id
        inner join currency cur on cur.currency_id = c.currency_id
        inner join ls_curr_fx_temp cr on cr.currency_from = l.contract_currency_id
                                     and cr.currency_to = cur.currency_id
                                     and cr.month = trunc(s.month, 'fmmonth')
        inner join ls_ilr_war_fx_temp war on war.ilr_id = s.ilr_id
                                         and war.revision = s.revision
                                         and war.set_of_books_id = s.set_of_books_id
        inner join ls_ilr_war_fx_temp war2 on war2.ilr_id = s.ilr_id
                                          and war2.revision = case when s.month <= nvl(o.remeasurement_date, s.month)
                                                                   then case when s.revision - 1 <= 0 then 1 else s.revision - 1 end
                                                                   else s.revision
                                                                   end
                                          and war2.set_of_books_id = s.set_of_books_id
                                          and s.month >= war2.effective_month
        left outer join calc_rate on calc_rate.company_id = i.company_id
                                 and calc_rate.contract_currency_id = l.contract_currency_id
                                 and calc_rate.accounting_month = s.month
                                 and calc_rate.company_currency_id = cur.currency_id 
        left outer join calc_rate prev_calc_rate on prev_calc_rate.company_id = i.company_id
                                 and prev_calc_rate.contract_currency_id = l.contract_currency_id
                                 and add_months(prev_calc_rate.accounting_month, 1) = s.month
                                 and prev_calc_rate.company_currency_id = cur.currency_id
       where s.set_of_books_id = A_SET_OF_BOOKS_ID
         and s.month between SCHED_FIRST and SCHED_LAST
         and s.revision > 0
         and not exists (
            select 1 from ls_ilr_schedule_fx_temp a
             where a.ls_cur_type = A_LS_CUR_TYPE
               and a.ilr_id = s.ilr_id
               and a.revision = s.revision
               and a.set_of_books_id = s.set_of_books_id
               and a.month = s.month);
    else
      -- Load the schedule table
      select min(month), max(month)
        into SCHED_FIRST, SCHED_LAST
        from (
          select A_LS_CUR_TYPE ls_cur_type, A_SET_OF_BOOKS_ID set_of_books_id, add_months(start_date, level - 1) month
            from (select A_START_DATE start_date, nvl(A_END_DATE,to_date(300001,'YYYYMM')) end_date from dual)
          connect by level <= months_between(end_date, start_date) + 1
          minus
          select ls_cur_type, set_of_books_id, month from ls_ilr_schedule_fx_temp);
          
      insert into ls_ilr_schedule_fx_temp 
      select A_LS_CUR_TYPE, s.ilr_id, s.revision, s.set_of_books_id, s.month, i.company_id, i.lease_id, 
             s.is_om, cur.iso_code, cur.currency_display_symbol, 
             s.residual_amount, s.term_penalty, s.bpo_price, s.beg_capital_cost, s.end_capital_cost, 
             s.beg_obligation, s.end_obligation, s.beg_lt_obligation, s.end_lt_obligation, 
             s.beg_liability, s.end_liability, s.beg_lt_liability, s.end_lt_liability, 
             s.beg_net_rou_asset, s.end_net_rou_asset, 
             s.interest_accrual, s.principal_accrual, s.interest_paid, s.principal_paid, 
             s.executory_paid1, s.executory_paid2, s.executory_paid3, s.executory_paid4, s.executory_paid5, 
             s.executory_paid6, s.executory_paid7, s.executory_paid8, s.executory_paid9, s.executory_paid10, 
             s.contingent_paid1, s.contingent_paid2, s.contingent_paid3, s.contingent_paid4, s.contingent_paid5, 
             s.contingent_paid6, s.contingent_paid7, s.contingent_paid8, s.contingent_paid9, s.contingent_paid10, 
             s.executory_accrual1, s.executory_accrual2, s.executory_accrual3, s.executory_accrual4, s.executory_accrual5, 
             s.executory_accrual6, s.executory_accrual7, s.executory_accrual8, s.executory_accrual9, s.executory_accrual10, 
             s.contingent_accrual1, s.contingent_accrual2, s.contingent_accrual3, s.contingent_accrual4, s.contingent_accrual5, 
             s.contingent_accrual6, s.contingent_accrual7, s.contingent_accrual8, s.contingent_accrual9, s.contingent_accrual10, 
             0 depr_expense, 0 begin_reserve, 0 end_reserve,
             s.current_lease_cost, s.beg_deferred_rent, s.deferred_rent, s.end_deferred_rent, 
             s.beg_st_deferred_rent, s.end_st_deferred_rent, 
             s.principal_remeasurement, s.liability_remeasurement, s.initial_direct_cost, s.incentive_amount, 
             s.beg_prepaid_rent, s.prepay_amortization, s.prepaid_rent, s.end_prepaid_rent, 
             s.idc_math_amount, s.incentive_math_amount, s.rou_asset_remeasurement, s.partial_term_gain_loss, 
             s.additional_rou_asset, s.executory_adjust, s.contingent_adjust, s.beg_arrears_accrual, 
             s.arrears_accrual, s.end_arrears_accrual, s.remaining_principal, 1, 1, null,
             l.contract_currency_id
        from ls_ilr_schedule s
        join ls_ilr i on i.ilr_id = s.ilr_id and i.current_revision = s.revision
        join ls_lease l on l.lease_id = i.lease_id
        join currency cur on cur.currency_id = l.contract_currency_id
       where s.month between SCHED_FIRST and SCHED_LAST
         and s.set_of_books_id = A_SET_OF_BOOKS_ID
         and s.revision > 0;
    
    end if;
    
    if A_GET_DEPR_DATA = 1 then
      -- Load the depr table; always load in contract currency since the schedule fx temp table has the
      -- net weighted average rates to use for depr for each month
      select min(month), max(month)
        into DEPR_FIRST, DEPR_LAST
        from (
          select A_SET_OF_BOOKS_ID set_of_books_id, add_months(start_date, level - 1) month
            from (select A_START_DATE start_date, nvl(A_END_DATE,to_date(300001,'YYYYMM')) end_date from dual)
          connect by level <= months_between(end_date, start_date) + 1
          minus
          select set_of_books_id, month from ls_ilr_depr_fx_temp);
          
      insert into ls_ilr_depr_fx_temp (
        ilr_id, revision, set_of_books_id, month, depr_expense, begin_reserve, end_reserve)
      select a.ilr_id, d.revision, d.set_of_books_id, d.month, 
             sum(d.depr_expense), sum(d.begin_reserve), sum(d.end_reserve)
        from ls_depr_forecast d
        join ls_asset a on a.ls_asset_id = d.ls_asset_id
       where d.revision > 0
         and d.set_of_books_id = A_SET_OF_BOOKS_ID
         and d.month between DEPR_FIRST and DEPR_LAST
         and a.ilr_id is not null
       group by a.ilr_id, d.revision, d.set_of_books_id, d.month;
    end if;
         
    RETURN;
  end p_get_ilr_schedule;
  
  procedure p_load_calc_rate_temp(A_START_DATE date) is
  begin
    -- Load for the start date to get the now rates
    p_load_ls_curr_fx_temp(A_START_DATE, A_START_DATE);
    
    -- Clear out the tables  
    delete from ls_avg_rate_cv_temp;
    delete from ls_lease_calc_rates_temp;
      
    -- Load the average rate control value temp table
    insert into ls_avg_rate_cv_temp
    with lease_co as (
                 select /*+ MATERIALIZE */ c.company_id, c.description, cs.currency_id
                   from company c
                   join currency_schema cs on c.company_id = cs.company_id
                  where cs.currency_type_id = 1
               ),
               sc as (
                  select /*+ MATERIALIZE */ * 
                    from pp_system_control_company
                   where trim(lower(control_name)) = 'lease mc: use average rates'
               )
    select cv.company_id, cv.control_value, lease_co.currency_id
      from (
            select sc.company_id, lower(trim(sc.control_value)) control_value
               from sc
              where sc.company_id <> -1
            union all
            select c.company_id, lower(trim(sc.control_value)) control_value
              from sc
             cross join (select company_id from lease_co
                         minus
                         select company_id from sc b) c
             where sc.company_id = -1
           ) cv
      join lease_co on lease_co.company_id = cv.company_id;
        
    -- Load the calculated date rates temp table
    insert into ls_lease_calc_rates_temp
    select cr1.company_id, cr1.contract_currency_id, cr1.company_currency_id, 
                cr1.accounting_month, cr1.rate act_rate, nvl(cr4.rate, cr1.rate) avg_rate
          from ls_lease_calculated_date_rates cr1
          left outer join (
               select company_id, contract_currency_id, company_currency_id, accounting_month, rate
                 from ls_lease_calculated_date_rates
                where trunc(accounting_month,'fmmonth') >= add_months(trunc(A_START_DATE, 'fmmonth'), -1)
                  and exchange_rate_type_id = 4
                          ) cr4 on cr1.company_id = cr4.company_id
                               and cr1.contract_currency_id = cr4.contract_currency_id
                               and cr1.company_currency_id = cr4.company_currency_id
                               and cr1.accounting_month = cr4.accounting_month
         where trunc(cr1.accounting_month,'fmmonth') >= add_months(trunc(A_START_DATE, 'fmmonth'), -1)
           and cr1.exchange_rate_type_id = 1;
  end p_load_calc_rate_temp;
  
  procedure p_get_future_payments(A_LS_CUR_TYPE number,
                                  A_SET_OF_BOOKS_ID number,
                                  A_START_DATE date,
                                  A_END_DATE date,
								  A_RATE_START_DATE date) is
  begin
    delete from ls_ilr_future_pay_mon_fx_temp
     where ls_cur_type = A_LS_CUR_TYPE
       and set_of_books_id = A_SET_OF_BOOKS_ID;
     
    delete from ls_ilr_future_payments_fx_temp
     where ls_cur_type = A_LS_CUR_TYPE
       and set_of_books_id = A_SET_OF_BOOKS_ID;
     
    if a_ls_cur_type = 2 then
      -- Get the rates for only the start month to get the "now" rate
      -- since that will be used when there are no calculated/locked rates available
      p_load_calc_rate_temp(A_RATE_START_DATE);
       
      -- Load the amounts that have calculated rates      
      insert into ls_ilr_future_pay_mon_fx_temp
      select A_LS_CUR_TYPE ls_cur_type, s.ilr_id, s.revision,       
             s.set_of_books_id, s.month, i.company_id, i.lease_id,        
             cur.currency_id,  cur.iso_code, cur.currency_display_symbol,
             (s.interest_paid + s.principal_paid) * decode(avg_rate_cv.control_value, 'yes', calc_rate.avg_rate, calc_rate.act_rate)
        from ls_ilr_schedule s        
        inner join ls_ilr i on i.ilr_id = s.ilr_id and i.current_revision = s.revision     
        inner join ls_lease l on l.lease_id = i.lease_id        
        inner join ls_avg_rate_cv_temp avg_rate_cv on avg_rate_cv.company_id = i.company_id  
        inner join currency cur on cur.currency_id = avg_rate_cv.currency_id   
        inner join ls_lease_calc_rates_temp calc_rate on l.contract_currency_id = calc_rate.contract_currency_id    
                                                     and cur.currency_id = calc_rate.company_currency_id                     
                                                     and i.company_id = calc_rate.company_id                      
                                                     and s.month = calc_rate.accounting_month   
       where s.set_of_books_id = A_SET_OF_BOOKS_iD 
         and s.month > A_START_DATE
         and s.revision > 0
         and (s.interest_paid + s.principal_paid) <> 0;
   
      -- Load the amounts that do not have calculated rates      
      insert into ls_ilr_future_pay_mon_fx_temp
      select A_LS_CUR_TYPE ls_cur_type, s.ilr_id, s.revision,       
             s.set_of_books_id, s.month, i.company_id, i.lease_id,        
             cur.currency_id,  cur.iso_code, cur.currency_display_symbol,
             (s.interest_paid + s.principal_paid) * cr.act_now_rate
        from ls_ilr_schedule s        
        inner join ls_ilr i on i.ilr_id = s.ilr_id and i.current_revision = s.revision     
        inner join ls_lease l on l.lease_id = i.lease_id        
        inner join ls_avg_rate_cv_temp avg_rate_cv on avg_rate_cv.company_id = i.company_id  
        inner join currency cur on cur.currency_id = avg_rate_cv.currency_id     
        inner join ls_curr_fx_temp cr on cr.currency_from = l.contract_currency_id       
                         and cr.currency_to = cur.currency_id                
                         and cr.month = trunc(A_RATE_START_DATE, 'fmmonth')  
       where s.set_of_books_id = A_SET_OF_BOOKS_iD
         and s.month > A_START_DATE
         and s.revision > 0 
         and (s.interest_paid + s.principal_paid) <> 0
         and not exists (
             select 1 from ls_lease_calc_rates_temp t
              where t.company_id = i.company_id
                and t.accounting_month = s.month) ;      

      -- Summarize the payments by ilr, revision, set of books
      insert into ls_ilr_future_payments_fx_temp
      select ls_cur_type, ilr_id, revision, set_of_books_id, company_id, lease_id, 
             iso_code, currency_display_symbol, currency_id, sum(total_payment)
        from ls_ilr_future_pay_mon_fx_temp
       where ls_cur_type = A_LS_CUR_TYPE
         and set_of_books_id = A_SET_OF_BOOKS_ID
       group by ls_cur_type, ilr_id, revision, set_of_books_id, company_id, lease_id, 
                iso_code, currency_display_symbol, currency_id;
    else
      -- Load the remaining payments
      insert into ls_ilr_future_pay_mon_fx_temp
      select A_LS_CUR_TYPE ls_cur_type, s.ilr_id, s.revision,       
             s.set_of_books_id, s.month, i.company_id, i.lease_id,        
             cur.currency_id,  cur.iso_code, cur.currency_display_symbol,
             (s.interest_paid + s.principal_paid)
        from ls_ilr_schedule s        
        inner join ls_ilr i on i.ilr_id = s.ilr_id and i.current_revision = s.revision     
        inner join ls_lease l on l.lease_id = i.lease_id 
        inner join currency cur on cur.currency_id = l.contract_currency_id
       where s.set_of_books_id = A_SET_OF_BOOKS_iD
         and s.month > A_START_DATE
         and s.revision > 0 
         and (s.interest_paid + s.principal_paid) <> 0;      

      -- Summarize the payments by ilr, revision, set of books
      insert into ls_ilr_future_payments_fx_temp
      select ls_cur_type, ilr_id, revision, set_of_books_id, company_id, lease_id, 
             iso_code, currency_display_symbol, currency_id, sum(total_payment)
        from ls_ilr_future_pay_mon_fx_temp
       where ls_cur_type = A_LS_CUR_TYPE
         and set_of_books_id = A_SET_OF_BOOKS_ID
       group by ls_cur_type, ilr_id, revision, set_of_books_id, company_id, lease_id, 
                iso_code, currency_display_symbol, currency_id;
             
    end if;
    
  end p_get_future_payments;
  
  
  procedure p_get_npv(A_LS_CUR_TYPE number,
                      A_SET_OF_BOOKS_ID number,
                      A_START_DATE date,
                      A_END_DATE date) is
  begin
    -- Clear out the NPV tables
    delete from ls_ilr_npv_fx_temp
     where ls_cur_type = A_LS_CUR_TYPE;
     
    delete from ls_ilr_npv_rate;
    
    -- Load the rate table
    insert into ls_ilr_npv_rate (
      ilr_id, revision, company_id, set_of_books_id, lease_id, pre_payment_sw, contract_currency_id,
      start_month, months_remaining, year_mod, eff_rate)
    select t.ilr_id, t.revision, t.company_id, A_SET_OF_BOOKS_ID, rate.lease_id, rate.pre_payment_sw, rate.contract_currency_id,
           t.start_month, 
           t.months_remaining,
           months_between(t.start_month, trunc(A_START_DATE,'month')) as year_mod,
           rate.eff_rate
      from (
              select ilr_id, revision, company_id,
                     greatest(min_payment_term_date, A_START_DATE) start_month,
                     add_months(min_payment_term_date, terms) last_payment_month,
                     months_between(add_months(min_payment_term_date, terms),
                                    greatest(min_payment_term_date, A_START_DATE)) months_remaining
                from (
                  select p.ilr_id, p.revision, i.company_id,
                         sum(p.number_of_terms * decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1)) terms,
                         min(p.payment_term_date) min_payment_term_date, min(p.payment_term_id) min_payment_term_id
                    from ls_ilr_payment_term p
                    join ls_ilr i on i.ilr_id = p.ilr_id and i.current_revision = p.revision
                   where i.ilr_status_id in (2, 3, 6)
                   group by p.ilr_id, p.revision, i.company_id)
           ) t
      join (
            select r.ilr_id, r.revision, l.lease_id, l.pre_payment_sw, l.contract_currency_id,
                   case when r.irr_used = 1 then
                     power((1 + nvl(r.rate,0)), (1/(l.days_in_year / 30))) - 1
                   else
                     case when t.rate_convention_id = 2 then
                       power((1 + nvl(r.rate,0)), (1/(l.days_in_year/30))) - 1
                     else
                        nvl(r.rate,0) / (l.days_in_year/30)
                     end
                   end eff_rate
              from (
               select o.ilr_id, o.revision, i.lease_id, 
                      decode(irr.internal_rate_return, 0, o.inception_air*.01, irr.internal_rate_return*.01) as rate, 
                      irr.internal_rate_return, o.lease_cap_type_id,
                      decode(irr.internal_rate_return, 0, 0, 1) irr_used  
                 from ls_ilr_amounts_set_of_books irr
                 join ls_ilr i on irr.ilr_id = i.ilr_id and irr.revision = i.current_revision
                 join ls_ilr_options o on irr.ilr_id = o.ilr_id and irr.revision = o.revision
                where irr.internal_rate_return < 200
                  and irr.set_of_books_id = A_SET_OF_BOOKS_ID
                   ) r
              join ls_lease_cap_type t on t.ls_lease_cap_type_id = r.lease_cap_type_id
              join ls_lease l on r.lease_id = l.lease_id
           ) rate on rate.ilr_id = t.ilr_id
                 and rate.revision = t.revision;
                 
    -- Get the future payments for the same period
    p_get_future_payments(A_LS_CUR_TYPE, A_SET_OF_BOOKS_ID, add_months(A_START_DATE,-1), A_END_DATE, A_START_DATE);
     
    if a_ls_cur_type = 2 then
      -- Necessary rates are loaded in p_get_future_payments, so just load the NPV table
      insert into ls_ilr_npv_fx_temp (
        ls_cur_type, ilr_id, revision, set_of_books_id, month, company_id, lease_id, 
        iso_code, currency_display_symbol, currency_id,
        start_month, months_remaining, calc_month, year_mod, in_year,
        future_payment, disc_payment, residual_amount, term_penalty, bpo_price
      )
      select A_LS_CUR_TYPE, s.ilr_id, s.revision, s.set_of_books_id, s.month, s.company_id, s.lease_id,
             s.iso_code, s.currency_display_symbol, s.currency_id,
             t.start_month, t.months_remaining,
             months_between(s.month, t.start_month) as calc_month, 
             t.year_mod as year_mod,
             floor((t.year_mod + months_between(s.month, t.start_month))/12) as in_year,
             s.total_payment as future_payment,
             round(s.total_payment / power(1 + t.eff_rate, months_between(s.month, t.start_month) + 1 - t.pre_payment_sw), 2) disc_payment,
             a.residual_amount, a.term_penalty, a.bpo_price
        from ls_ilr_future_pay_mon_fx_temp s
        join ls_ilr_npv_rate t on s.ilr_id = t.ilr_id 
                              and s.revision = t.revision
                              and s.month >= t.start_month
        join (
              select o.ilr_id,
                     sum(case when a.estimated_residual = 0 
                               and lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Amort to Guaranteed Residual', a.COMPANY_ID))) = 'yes' then
                            NVL(a.GUARANTEED_RESIDUAL_AMOUNT, 0)
                          else
                            Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', a.COMPANY_ID))), 'no', 0, 
                                   round(a.ESTIMATED_RESIDUAL * a.FMV, 2))
                          end
                          * nvl(calc_rate.act_rate, cr.act_now_rate)) residual_amount,
                     nvl(o.purchase_option_amt,0) * nvl(calc_rate.act_rate, cr.act_now_rate) bpo_price,
                     nvl(o.termination_amt, 0) * nvl(calc_rate.act_rate, cr.act_now_rate) term_penalty 
                from ls_asset a
                join ls_ilr i on i.ilr_id = a.ilr_id
                join ls_ilr_options o on o.ilr_id = i.ilr_id and o.revision = i.current_revision
                join ls_avg_rate_cv_temp avg_rate_cv on avg_rate_cv.company_id = i.company_id  
                join currency cur on cur.currency_id = avg_rate_cv.currency_id 
                join ls_ilr_npv_rate t on o.ilr_id = t.ilr_id 
                                      and o.revision = t.revision
                join ls_curr_fx_temp cr on cr.currency_from = a.contract_currency_id       
                                       and cr.currency_to = cur.currency_id                
                                       and cr.month = trunc(A_START_DATE, 'fmmonth')
                left outer join ls_lease_calc_rates_temp calc_rate on a.contract_currency_id = calc_rate.contract_currency_id    
                                                       and cur.currency_id = calc_rate.company_currency_id                     
                                                       and i.company_id = calc_rate.company_id                      
                                                       and calc_rate.accounting_month = add_months(t.start_month, t.months_remaining)
               where i.ilr_status_id in (2, 3, 6)	
               group by o.ilr_id, 
                        nvl(o.purchase_option_amt,0) * nvl(calc_rate.act_rate, cr.act_now_rate), 
                        nvl(o.termination_amt, 0) * nvl(calc_rate.act_rate, cr.act_now_rate)
             ) a on s.ilr_id = a.ilr_id
       where s.ls_cur_type = A_LS_CUR_TYPE
         and s.set_of_books_id = A_SET_OF_BOOKS_ID
         and s.month >= A_START_DATE;
    else
      -- load the amount table
      insert into ls_ilr_npv_fx_temp (
        ls_cur_type, ilr_id, revision, set_of_books_id, month, company_id, lease_id, 
        iso_code, currency_display_symbol, currency_id,
        start_month, months_remaining, calc_month, year_mod, in_year,
        future_payment, disc_payment, residual_amount, term_penalty, bpo_price
      )
      select A_LS_CUR_TYPE, s.ilr_id, s.revision, s.set_of_books_id, s.month, s.company_id, s.lease_id,
             s.iso_code, s.currency_display_symbol, s.currency_id,
             t.start_month, t.months_remaining,
             months_between(s.month, t.start_month) as calc_month, 
             t.year_mod as year_mod,
             floor((t.year_mod + months_between(s.month, t.start_month))/12) as in_year,
             s.total_payment as future_payment,
             round(s.total_payment / power(1 + t.eff_rate, months_between(s.month, t.start_month) + 1 - t.pre_payment_sw), 2) disc_payment,
             a.residual_amount, a.term_penalty, a.bpo_price
        from ls_ilr_future_pay_mon_fx_temp s
        join ls_ilr_npv_rate t on s.ilr_id = t.ilr_id 
                              and s.revision = t.revision
                              and s.month >= t.start_month
        join (
              select o.ilr_id,
                     sum(case when a.estimated_residual = 0 
                               and lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Amort to Guaranteed Residual', a.COMPANY_ID))) = 'yes' then
                            NVL(a.GUARANTEED_RESIDUAL_AMOUNT, 0)
                          else
                            Decode(lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', a.COMPANY_ID))), 'no', 0, 
                                   round(a.ESTIMATED_RESIDUAL * a.FMV, 2))
                          end) residual_amount,
                     nvl(o.purchase_option_amt,0) bpo_price,
                     nvl(o.termination_amt, 0) term_penalty 
                from ls_asset a
                join ls_ilr i on i.ilr_id = a.ilr_id
                join ls_ilr_options o on o.ilr_id = i.ilr_id and o.revision = i.current_revision
               where i.ilr_status_id in (2, 3, 6)
               group by o.ilr_id, nvl(o.purchase_option_amt,0), nvl(o.termination_amt, 0)
             ) a on s.ilr_id = a.ilr_id
       where s.ls_cur_type = A_LS_CUR_TYPE
         and s.set_of_books_id = A_SET_OF_BOOKS_ID
         and s.month >= A_START_DATE;
    end if;    
  end p_get_npv;
  
  procedure p_get_future_liabilities(A_LS_CUR_TYPE number,
                                     A_SET_OF_BOOKS_ID number,
                                     A_START_DATE date,
                                     A_END_DATE date) is
  begin
    delete from ls_ilr_future_liab_fx_temp
     where ls_cur_type = A_LS_CUR_TYPE
       and set_of_books_id = A_SET_OF_BOOKS_ID;
     
    if a_ls_cur_type = 2 then
      -- Get the rates for only the start month to get the "now" rate
      -- since that will be used when there are no calculated/locked rates available
      p_load_calc_rate_temp(A_START_DATE);
       
      -- Load the amounts that have calculated rates      
      insert into ls_ilr_future_liab_fx_temp
      select A_LS_CUR_TYPE ls_cur_type, s.ilr_id, s.revision,       
             s.set_of_books_id, t.company_id, t.lease_id,        
             cur.currency_id,  cur.iso_code, cur.currency_display_symbol,
             s.end_liability * calc_rate.act_rate, t.months_remaining
        from ls_ilr_schedule s 
        join (
              select ilr_id, revision, company_id, lease_id,
                     greatest(min_payment_term_date, A_START_DATE) start_month,
                     add_months(min_payment_term_date, terms) last_payment_month,
                     months_between(add_months(min_payment_term_date, terms),
                                    greatest(min_payment_term_date, add_months(A_START_DATE,1))) months_remaining
                from (
                  select p.ilr_id, p.revision, i.company_id, i.lease_id,
                         sum(p.number_of_terms * decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1)) terms,
                         min(p.payment_term_date) min_payment_term_date, min(p.payment_term_id) min_payment_term_id
                    from ls_ilr_payment_term p
                    join ls_ilr i on i.ilr_id = p.ilr_id and i.current_revision = p.revision
                   where i.ilr_status_id in (2, 3, 6)
                   group by p.ilr_id, p.revision, i.company_id, i.lease_id)
           ) t on t.ilr_id = s.ilr_id
              and t.revision = s.revision
              and t.start_month = s.month     
        inner join ls_lease l on l.lease_id = t.lease_id        
        inner join ls_avg_rate_cv_temp avg_rate_cv on avg_rate_cv.company_id = t.company_id  
        inner join currency cur on cur.currency_id = avg_rate_cv.currency_id   
        inner join ls_lease_calc_rates_temp calc_rate on l.contract_currency_id = calc_rate.contract_currency_id    
                                                     and cur.currency_id = calc_rate.company_currency_id                     
                                                     and t.company_id = calc_rate.company_id                      
                                                     and s.month = calc_rate.accounting_month   
       where s.set_of_books_id = A_SET_OF_BOOKS_iD 
         and s.month >= A_START_DATE
         and s.revision > 0
         and t.months_remaining >= 0
         and s.end_liability <> 0;
   
      -- Load the amounts that do not have calculated rates      
      insert into ls_ilr_future_liab_fx_temp
      select A_LS_CUR_TYPE ls_cur_type, s.ilr_id, s.revision,       
             s.set_of_books_id, t.company_id, t.lease_id,        
             cur.currency_id,  cur.iso_code, cur.currency_display_symbol,
             s.end_liability * cr.act_now_rate, t.months_remaining
        from ls_ilr_schedule s  
        join (
              select ilr_id, revision, company_id, lease_id,
                     greatest(min_payment_term_date, A_START_DATE) start_month,
                     add_months(min_payment_term_date, terms) last_payment_month,
                     months_between(add_months(min_payment_term_date, terms),
                                    greatest(min_payment_term_date, add_months(A_START_DATE,1))) months_remaining
                from (
                  select p.ilr_id, p.revision, i.company_id, i.lease_id,
                         sum(p.number_of_terms * decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1)) terms,
                         min(p.payment_term_date) min_payment_term_date, min(p.payment_term_id) min_payment_term_id
                    from ls_ilr_payment_term p
                    join ls_ilr i on i.ilr_id = p.ilr_id and i.current_revision = p.revision
                   where i.ilr_status_id in (2, 3, 6)
                   group by p.ilr_id, p.revision, i.company_id, i.lease_id)
           ) t on t.ilr_id = s.ilr_id
              and t.revision = s.revision
              and t.start_month = s.month     
        inner join ls_lease l on l.lease_id = t.lease_id        
        inner join ls_avg_rate_cv_temp avg_rate_cv on avg_rate_cv.company_id = t.company_id  
        inner join currency cur on cur.currency_id = avg_rate_cv.currency_id     
        inner join ls_curr_fx_temp cr on cr.currency_from = l.contract_currency_id       
                         and cr.currency_to = cur.currency_id                
                         and cr.month = trunc(A_START_DATE, 'fmmonth')  
       where s.set_of_books_id = A_SET_OF_BOOKS_iD
         and s.month >= A_START_DATE
         and s.revision > 0 
         and t.months_remaining >= 0
         and s.end_liability <> 0
         and not exists (
             select 1 from ls_lease_calc_rates_temp t
              where t.company_id = t.company_id
                and t.accounting_month = s.month) ;      
    else
      -- Load the end liability      
      insert into ls_ilr_future_liab_fx_temp
      select A_LS_CUR_TYPE ls_cur_type, s.ilr_id, s.revision,       
             s.set_of_books_id, t.company_id, t.lease_id,        
             cur.currency_id, cur.iso_code, cur.currency_display_symbol,
             s.end_liability, t.months_remaining
        from ls_ilr_schedule s 
        join (
              select ilr_id, revision, company_id, lease_id,
                     greatest(min_payment_term_date, A_START_DATE) start_month,
                     add_months(min_payment_term_date, terms) last_payment_month,
                     months_between(add_months(min_payment_term_date, terms),
                                    greatest(min_payment_term_date, add_months(A_START_DATE,1))) months_remaining
                from (
                  select p.ilr_id, p.revision, i.company_id, i.lease_id,
                         sum(p.number_of_terms * decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1)) terms,
                         min(p.payment_term_date) min_payment_term_date, min(p.payment_term_id) min_payment_term_id
                    from ls_ilr_payment_term p
                    join ls_ilr i on i.ilr_id = p.ilr_id and i.current_revision = p.revision
                   where i.ilr_status_id in (2, 3, 6)
                   group by p.ilr_id, p.revision, i.company_id, i.lease_id)
           ) t on t.ilr_id = s.ilr_id
              and t.revision = s.revision
              and t.start_month = s.month     
        inner join ls_lease l on l.lease_id = t.lease_id 
        inner join currency cur on cur.currency_id = l.contract_currency_id
       where s.set_of_books_id = A_SET_OF_BOOKS_iD 
         and s.month >= A_START_DATE
         and s.revision > 0
         and t.months_remaining >= 0
         and s.end_liability <> 0;
             
    end if;
  end p_get_future_liabilities;
  
end PKG_LEASE_REPORTING;
/
--************************** 
-- Log the run of the script 
--************************** 
insert into PP_SCHEMA_CHANGE_LOG 
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) 
values 
   (12123, 0, 2018, 1, 0, 0, 0, 'C:\PlasticWks\PowerPlant\sql\packages', '2018.1.0.0_PKG_LEASE_REPORTING.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME')); 
commit; 
