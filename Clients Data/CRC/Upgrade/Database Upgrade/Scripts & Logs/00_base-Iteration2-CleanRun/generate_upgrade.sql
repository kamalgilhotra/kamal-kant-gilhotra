SET DEFINE OFF
SET SERVEROUTPUT ON FORMAT WRAPPED
SET LINESIZE 256
SET FEEDBACK OFF

PROMPT SET ECHO ON
PROMPT SET TIME ON

PROMPT /*
PROMPT ***** NOTE: Run before update *****
PROMPT Log in as SYS and run the GRANT script.
PROMPT
PROMPT SPOOL &&PP_SCRIPT_PATH.upgrade_utilities/v10_sys_grants.log
PROMPT @&&PP_SCRIPT_PATH.upgrade_utilities/v10_sys_grants.sql
PROMPT SPOOL OFF
PROMPT
PROMPT Log back in as PWPRLANT to run this script.
PROMPT */
PROMPT
PROMPT SET SQLPROMPT '_CONNECT_IDENTIFIER> '
PROMPT
PROMPT /* Set PP_SCRIPT_PATH = to the path of the update scripts. */
PROMPT /* If you are running sqlplus from the script directory    */
PROMPT /* then there is no need to set PP_SCRIPT_PATH.            */
PROMPT /* Example: DEFINE PP_SCRIPT_PATH='C:\temp\scripts\'       */
PROMPT DEFINE PP_SCRIPT_PATH=''
PROMPT HOST MKDIR &&PP_SCRIPT_PATH.PACKAGES_RELEASED\LOGS
PROMPT HOST MKDIR &&PP_SCRIPT_PATH.MAINT_SCRIPTS\LOGS
PROMPT HOST MKDIR &&PP_SCRIPT_PATH.UPGRADE_UTILITIES\LOGS
PROMPT
PROMPT /*
PROMPT ***** NOTE: The following statement will exit SQLPlus if any ORA- errors occur.
PROMPT *****       If SQLPlus exists then review last Log file, resolve issues and
PROMPT *****       continue running the rest of the scripts after the error location.
PROMPT *****
PROMPT *****       Certain sripts may turn on and off the SQLEROR so look below for
PROMPT *****       comments about specific scripts.
PROMPT */
PROMPT WHENEVER SQLERROR exit failure rollback
PROMPT
PROMPT /*
PROMPT ||============================================================================||
PROMPT || Application: PowerPlant                                                    ||
PROMPT || Object Name: generate_upgrade.sql                   ||
PROMPT || Description:                                                               ||
PROMPT ||============================================================================||
PROMPT || Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.     ||
PROMPT ||============================================================================||
PROMPT || Version    Date       Revised By     Reason for Change                     ||
PROMPT || ---------- ---------- -------------- --------------------------------------||
PROMPT || 2018.2.0.0  3/15/2019 build script         2018.2.0.0 Release                                                              ||
PROMPT ||============================================================================||
PROMPT || NOTES: Check log files for: ORA-                                           ||
PROMPT ||                             SP2-                                           ||
PROMPT ||                             Enter value for                                ||
PROMPT ||                             PPC-ERR>                                       ||
PROMPT ||                             maximum size                                   ||
PROMPT ||                             Warning:                                       ||
PROMPT ||============================================================================||
PROMPT */
PROMPT
PROMPT
PROMPT /***********************************************************/
PROMPT /********************* Auto Generated **********************/
PROMPT /***********************************************************/
PROMPT
PROMPT /***********************************************************/
PROMPT /*        PLEASE REVIEW OUTPUT BEFORE RUNNING              */
PROMPT /***********************************************************/

declare
   PPCMSG        varchar2(10) := 'PPC-MSG> ';
   PPCERR        varchar2(10) := 'PPC-ERR> ';
   PPCSQL        varchar2(10) := 'PPC-SQL> ';
   PPCORA        varchar2(10) := 'PPC-ORA' || '-> ';
   PPCCOMMENT    varchar2(10) := '--';
   PPCMINVERSION varchar2(10) := '10320';

   L_PP_VERSION    number;
   L_MAJOR_VERSION number;
   L_MINOR_VERSION number;
   L_POINT_VERSION number;
   L_PATCH_VERSION number;
   L_LAST_VERSION  number := 0;
   L_LAST_MODULE   varchar2(30);

   L_PROP_TAX_VERSION    number;
   L_PROP_TAX_COMMENT    varchar2(60);
   L_PROP_TAX_BEING_USED boolean := true;

   L_PROV_VERSION number;
   L_PROV_COMMENT varchar2(60);

   L_PLANT_COMMENT varchar2(2);
   L_COUNT         number;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   --******************************************************************************
   -- Get the Plant version
   --******************************************************************************
   begin
      select SUBSTR(REGEXP_REPLACE(PP_VERSION, '[^0-9]', ''), 1, 4) ||
             NVL(SUBSTR(REGEXP_REPLACE(PP_PATCH, '[^0-9]', ''),
                        LENGTH(REGEXP_REPLACE(PP_PATCH, '[^0-9]', '')),
                        1),
                 0) PP_VERSION
        into L_PP_VERSION
        from PP_VERSION;

      DBMS_OUTPUT.PUT_LINE(PPCCOMMENT || 'POWERPLAN STARTING VERSION: ' || L_PP_VERSION);
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE(PPCERR || 'PowerPlan version not found');
         L_PP_VERSION := 0;
      when others then
         DBMS_OUTPUT.PUT_LINE(PPCERR || 'ERROR GETTING POWERPLAN VERSION. ');
         raise;
   end;


   --******************************************************************************
   -- Get Property Tax Version
   --******************************************************************************
   begin
      select count(*)
        into L_COUNT
        from ALL_TAB_COLUMNS
       where TABLE_NAME = 'PP_VERSION'
         and COLUMN_NAME = 'PROP_TAX_VERSION'
         and OWNER = 'PWRPLANT';

      if L_COUNT = 1 then
         execute immediate 'select REGEXP_REPLACE(PROP_TAX_VERSION, ''[^0-9]'') from PP_VERSION'
            into L_PROP_TAX_VERSION;

         if L_PROP_TAX_VERSION is null then
            -- Check PTC_SCRIPT_LOG to see if 10.3.2 scripts were run.
            -- Verify that PTC_SCRIPT_LOG table exists.
            select count(*)
              into L_COUNT
              from ALL_TABLES
             where TABLE_NAME = 'PTC_SCRIPT_LOG'
               and OWNER = 'PWRPLANT';

            if L_COUNT = 1 then
               execute immediate 'select count(*) from PTC_SCRIPT_LOG where REGEXP_REPLACE(VERSION, ''[^0-9]'') = ''10320'''
                  into L_COUNT;
               if L_COUNT > 0 then
                  L_PROP_TAX_VERSION := 10320;
               else
                  L_PROP_TAX_VERSION := 0;
               end if;
            else
               L_PROP_TAX_VERSION := 0;
            end if;

         else
            --Add a zero for the Patch Version if it doesn't EXIST.
            if L_PROP_TAX_VERSION < 10000 then
               L_PROP_TAX_VERSION := L_PROP_TAX_VERSION * 10;
            end if;
         end if;

      else
         L_PROP_TAX_VERSION := 0;
      end if;

      if L_PROP_TAX_VERSION = 0 or L_PROP_TAX_VERSION is null then
         DBMS_OUTPUT.PUT_LINE(PPCCOMMENT ||
                              'Property Tax Starting Version could not be determined, skipping Property Tax update.');
         L_PROP_TAX_VERSION := 0;
      else
         DBMS_OUTPUT.PUT_LINE(PPCCOMMENT || 'PROPERTY TAX STARTING VERSION: ' ||
                              L_PROP_TAX_VERSION);
      end if;
   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE(PPCERR || 'Property Tax version not found');
         L_PROP_TAX_VERSION := 0;
      when others then
         DBMS_OUTPUT.PUT_LINE(PPCERR || 'Error getting Property Tax version.');
         raise;
   end;
   --******************************************************************************
   -- Get Provison Version
   --******************************************************************************
   begin
      select count(*)
        into L_COUNT
        from ALL_TABLES
       where TABLE_NAME = 'TAX_ACCRUAL_SYSTEM_CONTROL'
         and OWNER = 'PWRPLANT';

      if L_COUNT = 1 then
         execute immediate 'select REGEXP_REPLACE(CONTROL_VALUE, ''[^0-9]'') from TAX_ACCRUAL_SYSTEM_CONTROL
                             where LOWER(trim(CONTROL_NAME)) like LOWER(''Provision DB Version'')
                               and ROWNUM = 1'
            into L_PROV_VERSION;
      else
         select REGEXP_REPLACE(CONTROL_VALUE, '[^0-9]')
           into L_PROV_VERSION
           from PP_SYSTEM_CONTROL_COMPANY
          where LOWER(trim(CONTROL_NAME)) like LOWER('Provision DB Version')
            and ROWNUM = 1;
      end if;

      --Add a zero for the Patch Version if it doesn't exist.
      if L_PROV_VERSION is null then
         L_PROV_VERSION := 0;
      else
         if L_PROV_VERSION < 10000 then
            L_PROV_VERSION := L_PROV_VERSION * 10;
         end if;
      end if;

      if L_PROV_VERSION = 0 or L_PROV_VERSION is null then
         DBMS_OUTPUT.PUT_LINE(PPCCOMMENT ||
                              'Provision Starting Version could not be determined, skipping Provision update.');
         L_PROV_VERSION := 0;
      else

         DBMS_OUTPUT.PUT_LINE(PPCCOMMENT || 'PROVISION STARTING VERSION: ' || L_PROV_VERSION);
      end if;

   exception
      when NO_DATA_FOUND then
         DBMS_OUTPUT.PUT_LINE(PPCERR || 'Provision version not found');
         L_PROV_VERSION := 0;
      when others then
         DBMS_OUTPUT.PUT_LINE(PPCERR || 'Error getting Provision version.');
         raise;
   end;

   --******************************************************************************
   -- Check if Property Tax is being used.  10.4.1.0 Rebuilds it if it is not
   -- being used.
   --******************************************************************************

   -- Check to see if the PROP_TAX_YEAR table exists.
   select count(*)
     into L_COUNT
     from ALL_TABLES
    where OWNER = 'PWRPLANT'
      and TABLE_NAME = 'PROPERTY_TAX_YEAR';

   if L_COUNT > 0 then
      execute immediate 'select count(*) from PROPERTY_TAX_YEAR'
         into L_COUNT;
      if L_COUNT > 0 then
         L_PROP_TAX_BEING_USED := true;
      -- If Property Tax version is 10410 or higher then we have already rebuilt it.
      elsif L_PROP_TAX_VERSION < 10410 then
         L_PROP_TAX_BEING_USED := false;
      else
         L_PROP_TAX_BEING_USED := true;
      end if;
   else
      L_PROP_TAX_BEING_USED := false;
   end if;

   --******************************************************************************
   -- Validate starting versions
   --******************************************************************************
   -- Plant
   if L_PP_VERSION < PPCMINVERSION then
      DBMS_OUTPUT.PUT_LINE(PPCERR || 'PowerPlant version is: ' || L_PP_VERSION);
      DBMS_OUTPUT.PUT_LINE(PPCERR || 'Version must be 10.3.2.0 or higher to run this upgrade.');
      RAISE_APPLICATION_ERROR(-20000, 'PowerPlant version is not correct.');
   end if;

   -- Property Tax
   if L_PROP_TAX_VERSION < PPCMINVERSION then
      DBMS_OUTPUT.PUT_LINE(PPCERR || 'Property Tax version is: ' || L_PROP_TAX_VERSION);
      DBMS_OUTPUT.PUT_LINE(PPCERR || 'Version must be 10.3.2.0 or higher to run this upgrade.');
      DBMS_OUTPUT.PUT_LINE(PPCERR || 'Property Tax scripts will be commented out.');
      if L_PROP_TAX_BEING_USED then
         L_PROP_TAX_COMMENT := '-- Previous Version Needed --';
      else
         L_PROP_TAX_COMMENT := '-- Proptax will be rebuilt in V10.4.1.0 --';
      end if;
   end if;

   if not L_PROP_TAX_BEING_USED then
      DBMS_OUTPUT.PUT_LINE(PPCERR ||
                           'Property Tax is not being used and will be rebuilt in 10.4.1.0.');
      L_PROP_TAX_COMMENT := '-- Proptax will be rebuilt in V10.4.1.0 --';
   end if;

   -- Provision Tax
   if L_PROV_VERSION < PPCMINVERSION then
      DBMS_OUTPUT.PUT_LINE(PPCERR || 'Provision version is: ' || L_PROV_VERSION);
      DBMS_OUTPUT.PUT_LINE(PPCERR || 'Version must be 10.3.2.0 or higher to run this upgrade.');
      DBMS_OUTPUT.PUT_LINE(PPCERR || 'Provision scripts will be commented out.');
      L_PROV_COMMENT := '-- Previous Version Needed --';
   end if;


   --******************************************************************************
   -- Check to make sure PEND_TRANSACTION doesn't have existing records.
   -- If it have records this will throw an ORA- error and stop the upgrade.
   --******************************************************************************
   DBMS_OUTPUT.NEW_LINE;
   DBMS_OUTPUT.PUT_LINE('-- Check to make sure PEND_TRANSACTION doesn''t have existing records.');
   DBMS_OUTPUT.PUT_LINE('SPOOL &&PP_SCRIPT_PATH.UPGRADE_UTILITIES\LOGS\PEND_TRANSACTION_table_data_check.log');
   DBMS_OUTPUT.PUT_LINE('@&&PP_SCRIPT_PATH.UPGRADE_UTILITIES\PEND_TRANSACTION_table_data_check.sql');
   DBMS_OUTPUT.PUT_LINE('SPOOL OFF');
   DBMS_OUTPUT.NEW_LINE;

   --******************************************************************************
   -- Starting with 10.4.1.0 going forward Property Tax and Provision scripts will not be divided out and will be run in
   -- BASE_DATE_APPLIED order mixed in with the other scripts.
   --******************************************************************************

   for CSR_SCRIPTS in (with DS1 as (
            select DECODE(UPPER(TRIM(T1.released_module)),
                                    'PACKAGE','SPOOL &&PP_SCRIPT_PATH.packages_released\LOGS\', 
                                    'SPOOL &&PP_SCRIPT_PATH.maint_scripts\LOGS\') ||
                              SUBSTR(T1.RELEASED_SCRIPT_NAME, 1, LENGTH(T1.RELEASED_SCRIPT_NAME) - 3) ||
                              'log' LINE1,
                              DECODE(UPPER(TRIM(T1.released_module)),
                                    'PACKAGE','@&&PP_SCRIPT_PATH.packages_released\', 
                                    '@&&PP_SCRIPT_PATH.maint_scripts\') || T1.RELEASED_SCRIPT_NAME LINE2,
                              'SPOOL OFF' LINE3,
                              T1.BASE_MAJOR_VERSION || T1.BASE_MINOR_VERSION ||
                              T1.BASE_POINT_VERSION || T1.BASE_PATCH_VERSION FULL_VERSION,
                              T1.BASE_SCRIPT_NAME,
                              T1.RELEASED_SCRIPT_NAME,
                              case
                                 when T1.BASE_MAJOR_VERSION || T1.BASE_MINOR_VERSION ||
                                      T1.BASE_POINT_VERSION || T1.BASE_PATCH_VERSION >= 10410 then
                                  1
                                 else
                                  DECODE(T1.RELEASED_MODULE, 'PROV', 3, 'PROPTAX', 2, 1)
                              end MODULE_ORDER,
                              T1.BASE_DATE_APPLIED,
                              T1.RELEASED_NOT_NEEDED,
                              DECODE(T1.RELEASED_MODULE,
                                     'PROV',
                                     'PROV',
                                     'PROPTAX',
                                     'PROPTAX',
                                     'PLANT') RELEASED_MODULE,
                              T1.BASE_MAJOR_VERSION,
                              T1.BASE_MINOR_VERSION,
                              T1.BASE_POINT_VERSION,
                              T1.BASE_PATCH_VERSION,
                              DECODE(T2.SCRIPT_NAME, null, 0, 1) BEEN_RUN,
                              DECODE(T2.SCRIPT_NAME, null, '', '-- This has already been run --') BEEN_RUN_COMMENT,
                              T1.RELEASED_VERSION,
                              T1.SKIPPED_INSERT_STATEMENT,
                              T1.BASE_ID
                         from PP_SCHEMA_UPGRADE T1, PP_SCHEMA_CHANGE_LOG T2
                        where T1.BASE_ID = T2.ID(+)
                          and T1.BASE_SCRIPT_REVISION = T2.SCRIPT_REVISION(+)
                          and RELEASED_NOT_NEEDED <> 1
                          and to_number(BASE_MAJOR_VERSION || BASE_MINOR_VERSION || BASE_POINT_VERSION || BASE_PATCH_VERSION) <= 2017400
                        order by T1.BASE_MAJOR_VERSION || '.' || T1.BASE_MINOR_VERSION || '.' ||
                                 T1.BASE_POINT_VERSION || '.' || T1.BASE_PATCH_VERSION,
                                 MODULE_ORDER,
                                 T1.BASE_DATE_APPLIED ),
DS2 as (
         select DECODE(UPPER(TRIM(T1.released_module)),
                                    'PACKAGE','SPOOL &&PP_SCRIPT_PATH.packages_released\LOGS\', 
                                    'SPOOL &&PP_SCRIPT_PATH.maint_scripts\LOGS\') ||
                              SUBSTR(T1.RELEASED_SCRIPT_NAME, 1, LENGTH(T1.RELEASED_SCRIPT_NAME) - 3) ||
                              'log' LINE1,
                              DECODE(UPPER(TRIM(T1.released_module)),
                                    'PACKAGE','@&&PP_SCRIPT_PATH.packages_released\', 
                                    '@&&PP_SCRIPT_PATH.maint_scripts\') || T1.RELEASED_SCRIPT_NAME LINE2,
                              'SPOOL OFF' LINE3,
                              T1.BASE_MAJOR_VERSION || T1.BASE_MINOR_VERSION ||
                              T1.BASE_POINT_VERSION || T1.BASE_PATCH_VERSION FULL_VERSION,
                              T1.BASE_SCRIPT_NAME,
                              T1.RELEASED_SCRIPT_NAME,
                              case
                                 when T1.BASE_MAJOR_VERSION || T1.BASE_MINOR_VERSION ||
                                      T1.BASE_POINT_VERSION || T1.BASE_PATCH_VERSION >= 10410 then
                                  1
                                 else
                                  DECODE(T1.RELEASED_MODULE, 'PROV', 3, 'PROPTAX', 2, 1)
                              end MODULE_ORDER,
                              T1.BASE_DATE_APPLIED,
                              T1.RELEASED_NOT_NEEDED,
                              DECODE(T1.RELEASED_MODULE,
                                     'PROV',
                                     'PROV',
                                     'PROPTAX',
                                     'PROPTAX',
                                     'PLANT') RELEASED_MODULE,
                              T1.BASE_MAJOR_VERSION,
                              T1.BASE_MINOR_VERSION,
                              T1.BASE_POINT_VERSION,
                              T1.BASE_PATCH_VERSION,
                              DECODE(T2.SCRIPT_NAME, null, 0, 1) BEEN_RUN,
                              DECODE(T2.SCRIPT_NAME, null, '', '-- This has already been run --') BEEN_RUN_COMMENT,
                              T1.RELEASED_VERSION,
                              T1.SKIPPED_INSERT_STATEMENT,
                              T1.BASE_ID
                         from PP_SCHEMA_UPGRADE T1, PP_SCHEMA_CHANGE_LOG T2
                        where T1.BASE_ID = T2.ID(+)
                          and T1.BASE_SCRIPT_REVISION = T2.SCRIPT_REVISION(+)
                          and RELEASED_NOT_NEEDED <> 1
						  and to_number(BASE_MAJOR_VERSION || BASE_MINOR_VERSION || BASE_POINT_VERSION || BASE_PATCH_VERSION) >= 2017401
                        order by T1.BASE_DATE_APPLIED )
select * from DS1
union all
select * from DS2)
   loop
      -- Write version comment between versions
      if CSR_SCRIPTS.FULL_VERSION <> L_LAST_VERSION then
         DBMS_OUTPUT.NEW_LINE;
         DBMS_OUTPUT.PUT_LINE(PPCCOMMENT || ' ** ' || CSR_SCRIPTS.BASE_MAJOR_VERSION || '.' ||
                              CSR_SCRIPTS.BASE_MINOR_VERSION || '.' ||
                              CSR_SCRIPTS.BASE_POINT_VERSION || '.' ||
                              CSR_SCRIPTS.BASE_PATCH_VERSION || ' **');
         L_LAST_VERSION := CSR_SCRIPTS.FULL_VERSION;
         L_LAST_MODULE  := ' ';
      end if;

      -- For version < 10.4.1.0 write comment identifing the start of Property Tax and Provision
      -- This will alway be 10410.  Starting with 1041 scripts are not broken out into different sections
      if CSR_SCRIPTS.RELEASED_MODULE <> L_LAST_MODULE and CSR_SCRIPTS.FULL_VERSION < 10410 then
         case
            when CSR_SCRIPTS.RELEASED_MODULE = 'PROPTAX' then
               DBMS_OUTPUT.NEW_LINE;
               DBMS_OUTPUT.PUT_LINE(PPCCOMMENT || ' ** PROPERTY TAX');
            when CSR_SCRIPTS.RELEASED_MODULE = 'PROV' then
               DBMS_OUTPUT.NEW_LINE;
               DBMS_OUTPUT.PUT_LINE(PPCCOMMENT || ' ** PROVISION');
            else
               DBMS_OUTPUT.PUT_LINE(PPCCOMMENT || ' ** PLANT');
         end case;
         L_LAST_MODULE := CSR_SCRIPTS.RELEASED_MODULE;
      end if;

      case
         when CSR_SCRIPTS.RELEASED_MODULE = 'PROPTAX' then
            if CSR_SCRIPTS.BEEN_RUN = 1 and CSR_SCRIPTS.RELEASED_VERSION = CSR_SCRIPTS.FULL_VERSION then
               if not L_PROP_TAX_BEING_USED and CSR_SCRIPTS.FULL_VERSION >= 10410 then
                  L_PROP_TAX_COMMENT := '';
               end if;
               DBMS_OUTPUT.PUT_LINE(L_PROP_TAX_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE1);
               DBMS_OUTPUT.PUT_LINE(L_PROP_TAX_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE2);
               DBMS_OUTPUT.PUT_LINE(L_PROP_TAX_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE3);
            elsif CSR_SCRIPTS.BEEN_RUN = 0 then
               -- If Property Tax is being used then do not run rebuild scripts and insert into PP_SCHEMA_CHANGE_LOG that the
               -- scripts have been skipped.
               if L_PROP_TAX_BEING_USED and CSR_SCRIPTS.FULL_VERSION >= 10410 and
                  (CSR_SCRIPTS.BASE_ID = 537 or CSR_SCRIPTS.BASE_ID = 538 or
                  CSR_SCRIPTS.BASE_ID = 539 or CSR_SCRIPTS.BASE_ID = 540 or
                  CSR_SCRIPTS.BASE_ID = 541) then
                  DBMS_OUTPUT.PUT_LINE('-- Property Tax is being used so skip rebuild.');
                  DBMS_OUTPUT.PUT_LINE(CSR_SCRIPTS.SKIPPED_INSERT_STATEMENT);
                  DBMS_OUTPUT.PUT_LINE('-- Do not rebuild - Property Tax is being used --' ||
                                       CSR_SCRIPTS.BEEN_RUN_COMMENT || CSR_SCRIPTS.LINE1);
                  DBMS_OUTPUT.PUT_LINE('-- Do not rebuild - Property Tax is being used --' ||
                                       CSR_SCRIPTS.BEEN_RUN_COMMENT || CSR_SCRIPTS.LINE2);
                  DBMS_OUTPUT.PUT_LINE('-- Do not rebuild - Property Tax is being used --' ||
                                       CSR_SCRIPTS.BEEN_RUN_COMMENT || CSR_SCRIPTS.LINE3);

               else
                  if not L_PROP_TAX_BEING_USED and CSR_SCRIPTS.FULL_VERSION >= 10410 then
                     L_PROP_TAX_COMMENT := '';
                  end if;
                  DBMS_OUTPUT.PUT_LINE(L_PROP_TAX_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                       CSR_SCRIPTS.LINE1);
                  DBMS_OUTPUT.PUT_LINE(L_PROP_TAX_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                       CSR_SCRIPTS.LINE2);
                  DBMS_OUTPUT.PUT_LINE(L_PROP_TAX_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                       CSR_SCRIPTS.LINE3);
               end if;
            end if;
         when CSR_SCRIPTS.RELEASED_MODULE = 'PROV' then
            if CSR_SCRIPTS.BEEN_RUN = 1 and CSR_SCRIPTS.RELEASED_VERSION = CSR_SCRIPTS.FULL_VERSION then
               DBMS_OUTPUT.PUT_LINE(L_PROV_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE1);
               DBMS_OUTPUT.PUT_LINE(L_PROV_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE2);
               DBMS_OUTPUT.PUT_LINE(L_PROV_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE3);
            elsif CSR_SCRIPTS.BEEN_RUN = 0 then
               DBMS_OUTPUT.PUT_LINE(L_PROV_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE1);
               DBMS_OUTPUT.PUT_LINE(L_PROV_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE2);
               DBMS_OUTPUT.PUT_LINE(L_PROV_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE3);

            end if;
         else
            -- Plant scripts
            if CSR_SCRIPTS.BEEN_RUN = 1 and CSR_SCRIPTS.RELEASED_VERSION = CSR_SCRIPTS.FULL_VERSION then
               DBMS_OUTPUT.PUT_LINE(L_PLANT_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE1);
               DBMS_OUTPUT.PUT_LINE(L_PLANT_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE2);
               DBMS_OUTPUT.PUT_LINE(L_PLANT_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE3);
            elsif CSR_SCRIPTS.BEEN_RUN = 0 then
               DBMS_OUTPUT.PUT_LINE(L_PLANT_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE1);
               DBMS_OUTPUT.PUT_LINE(L_PLANT_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE2);
               DBMS_OUTPUT.PUT_LINE(L_PLANT_COMMENT || CSR_SCRIPTS.BEEN_RUN_COMMENT ||
                                    CSR_SCRIPTS.LINE3);
            end if;
      end case;
   end loop;

end;
/

PROMPT
PROMPT
PROMPT -- Load Exclude List for DBAdmin database compare
PROMPT SPOOL &&PP_SCRIPT_PATH.UPGRADE_UTILITIES\LOGS\load_exclude_list.log
PROMPT @&&PP_SCRIPT_PATH.UPGRADE_UTILITIES\load_exclude_list.sql
PROMPT SPOOL OFF
PROMPT

PROMPT --logging in the file
PROMPT @&&PP_SCRIPT_PATH.UPGRADE_UTILITIES\create_grants.sql
PROMPT @&&PP_SCRIPT_PATH.UPGRADE_UTILITIES\create_pwrplant_role_rdonly_grants.sql
PROMPT @&&PP_SCRIPT_PATH.UPGRADE_UTILITIES\create_pwrplant_role_select_grants.sql
PROMPT --logging in the file
PROMPT @&&PP_SCRIPT_PATH.UPGRADE_UTILITIES\create_synonyms.sql
PROMPT --logging in the file
PROMPT @&&PP_SCRIPT_PATH.UPGRADE_UTILITIES\create_timestamp_triggers.sql
PROMPT
PROMPT EXECUTE DBMS_UTILITY.COMPILE_SCHEMA('PWRPLANT', FALSE);;
PROMPT
PROMPT --execute DBMS_STATS.GATHER_SCHEMA_STATS('PWRPLANT');;
PROMPT
PROMPT /* ** Things to check after the upgrade
PROMPT
PROMPT -- Verify that all scripts ran.
PROMPT select T1.RELEASED_SCRIPT_NAME SCRIPT_NAME,
PROMPT        T1.BASE_MAJOR_VERSION || T1.BASE_MINOR_VERSION || T1.BASE_POINT_VERSION || T1.BASE_PATCH_VERSION SCRIPT_VERSION,
PROMPT        DECODE(T1.RELEASED_NOT_NEEDED, 1, 'TRUE', 'FALSE') RELEASED_NOT_NEEDED,
PROMPT        DECODE(T1.RELEASED_MODULE, 'PROV', 'PROV', 'PROPTAX', 'PROPTAX', 'PLANT') RELEASED_MODULE,
PROMPT        --       DECODE(T2.SCRIPT_NAME, null, 0, 1) BEEN_RUN,
PROMPT        T1.RELEASED_VERSION
PROMPT   from PP_SCHEMA_UPGRADE T1, PP_SCHEMA_CHANGE_LOG T2
PROMPT where T1.BASE_ID = T2.ID(+)
PROMPT    and T1.BASE_SCRIPT_REVISION = T2.SCRIPT_REVISION(+)
PROMPT    and RELEASED_NOT_NEEDED <> 1
PROMPT    and DECODE(T2.SCRIPT_NAME, null, 0, 1) <> 1 -- Script has been run
PROMPT order by T1.BASE_MAJOR_VERSION || '.' || T1.BASE_MINOR_VERSION || '.' || T1.BASE_POINT_VERSION || '.' ||
PROMPT           T1.BASE_PATCH_VERSION,
PROMPT           T1.BASE_DATE_APPLIED;;
PROMPT
PROMPT -- Verify all constraints are enabled.
PROMPT select T1.TABLE_NAME,
PROMPT        T1.CONSTRAINT_NAME,
PROMPT        T1.COLUMN_NAMES,
PROMPT        R1.REFERENCED_TABLE,
PROMPT        R1.REFERENCED_CONSTRAINT_NAME,
PROMPT        R1.REFERENCED_COLUMN_NAMES,
PROMPT        'alter table ' || T1.TABLE_NAME || ' enable constraint ' || T1.CONSTRAINT_NAME || ';' "Enable Constraint SQL",
PROMPT        'select distinct ' || T1.COLUMN_NAMES || ' from ' || T1.TABLE_NAME || ' where (' || T1.COLUMN_NAMES ||
PROMPT        ') not in (select ' || R1.REFERENCED_COLUMN_NAMES || ' from ' || R1.REFERENCED_TABLE || ');' "Bad Data SQL"
PROMPT   from (select UCC.TABLE_NAME REFERENCED_TABLE,
PROMPT                UCC.CONSTRAINT_NAME REFERENCED_CONSTRAINT_NAME,
PROMPT                LISTAGG(UCC.COLUMN_NAME, ',') WITHIN group(order by UCC.POSITION) as REFERENCED_COLUMN_NAMES
PROMPT           from USER_CONSTRAINTS UC, USER_CONS_COLUMNS UCC
PROMPT          where UCC.TABLE_NAME <> 'PT_TEMP_SCENARIO_FORMULA'
PROMPT            and UC.CONSTRAINT_NAME = UCC.CONSTRAINT_NAME
PROMPT            and UC.TABLE_NAME = UCC.TABLE_NAME
PROMPT            and UC.CONSTRAINT_NAME in (select R_CONSTRAINT_NAME from USER_CONSTRAINTS where STATUS <> 'ENABLED')
PROMPT          group by UCC.TABLE_NAME, UCC.CONSTRAINT_NAME) R1,
PROMPT        (select UCC.TABLE_NAME,
PROMPT                UC.R_CONSTRAINT_NAME,
PROMPT                UCC.CONSTRAINT_NAME,
PROMPT                LISTAGG(UCC.COLUMN_NAME, ',') WITHIN group(order by UCC.POSITION) as COLUMN_NAMES
PROMPT           from USER_CONSTRAINTS UC, USER_CONS_COLUMNS UCC
PROMPT          where UCC.TABLE_NAME <> 'PT_TEMP_SCENARIO_FORMULA'
PROMPT            and UC.CONSTRAINT_NAME = UCC.CONSTRAINT_NAME
PROMPT            and UC.TABLE_NAME = UCC.TABLE_NAME
PROMPT            and UC.STATUS <> 'ENABLED'
PROMPT          group by UCC.TABLE_NAME, UCC.CONSTRAINT_NAME, UC.R_CONSTRAINT_NAME) T1
PROMPT where R1.REFERENCED_CONSTRAINT_NAME = T1.R_CONSTRAINT_NAME;;
PROMPT
PROMPT
PROMPT -- Verify all objects are Valid.
PROMPT column object_name format A30
PROMPT
PROMPT select OBJECT_NAME, OBJECT_TYPE, STATUS
PROMPT   from USER_OBJECTS
PROMPT where STATUS <> 'VALID'
PROMPT order by OBJECT_TYPE, OBJECT_NAME;;
PROMPT
PROMPT -- Verify all Triggers are Enabled.
PROMPT select TRIGGER_NAME, TABLE_NAME, STATUS from USER_TRIGGERS where STATUS <> 'ENABLED' order by TRIGGER_NAME;;
PROMPT
PROMPT -- Review sequences for:
PROMPT -- INCREMENT_BY > 1
PROMPT -- CACHE_SIZE too large
PROMPT -- LAST_NUM too large - max for PB code is 2 billion
PROMPT select SEQUENCE_NAME, MIN_VALUE, INCREMENT_BY, CACHE_SIZE, LAST_NUMBER from USER_SEQUENCES order by SEQUENCE_NAME;;
PROMPT
PROMPT */
PROMPT
PROMPT /*
PROMPT Log the run of the script
PROMPT */
PROMPT insert into PP_UPDATE_FLEX
PROMPT             (COL_ID,
PROMPT              TYPE_NAME,
PROMPT              FLEXVCHAR1)
PROMPT       select NVL(MAX(COL_ID), 0) + 1,
PROMPT              'UPDATE_SCRIPTS',
PROMPT              'run_auto_generated_upgrade.sql'
PROMPT         from PP_UPDATE_FLEX;;
PROMPT commit;;

SET DEFINE ON
SET FEEDBACK ON
