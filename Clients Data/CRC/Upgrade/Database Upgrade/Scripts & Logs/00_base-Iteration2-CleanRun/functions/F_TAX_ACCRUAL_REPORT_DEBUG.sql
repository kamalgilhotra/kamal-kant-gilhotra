
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_TAX_ACCRUAL_REPORT_DEBUG" 
(
	/* a_retrieve = 1 means we are storing data from temp tables */
	/* a_retrieve = 2 means we are populating temp tables with data from non-temp tables */
	a_retrieve number
)
return number is

PRAGMA AUTONOMOUS_TRANSACTION;

begin

if a_retrieve > 2 or a_retrieve < 1 then
	return -1;
end if;

if a_retrieve = 1 then

	delete from tax_accrual_rep_criteria_debug;
	insert into tax_accrual_rep_criteria_debug
	select * from tax_accrual_rep_criteria_tmp;

	delete from tax_accrual_m_item_debug;
	insert into tax_accrual_m_item_debug
	select * from tax_accrual_m_item_tmp;

	delete from tax_accrual_account_debug;
	insert into tax_accrual_account_debug
	select * from tax_accrual_account_tmp;

	delete from tax_accrual_rep_tree_debug;
	insert into tax_accrual_rep_tree_debug
	select * from tax_accrual_rep_tree_temp;

	delete from tax_accrual_eff_rate_debug;
	insert into tax_accrual_eff_rate_debug
	select * from tax_accrual_eff_rate_tmp2;

	delete from tax_accrual_rep_je_type_debug;
	insert into tax_accrual_rep_je_type_debug
	select * from tax_accrual_rep_je_type_temp;

	delete from tax_accrual_pt_trc_debug;
	insert into tax_accrual_pt_trc_debug
	select * from tax_accrual_pt_trc_tmp;

	delete from tax_accrual_pt_tc_rollup_debug;
	insert into tax_accrual_pt_tc_rollup_debug
	select * from tax_accrual_pt_tc_rollup_tmp;

	delete from tax_accrual_beg_bal_load_debug;
	insert into tax_accrual_beg_bal_load_debug
	select * from tax_accrual_beg_bal_load;

	delete from tax_accrual_eff_rate_debug1;
	insert into tax_accrual_eff_rate_debug1
	select * from tax_accrual_eff_rate_tmp;

	delete from tax_accrual_assign_ns_debug;
	insert into tax_accrual_assign_ns_debug
	select * from tax_accrual_assign_ns_tmp;

	delete from tax_accrual_subledger_debug;
	insert into tax_accrual_subledger_debug
	select * from tax_accrual_subledger_tmp;

	delete from tax_accrual_cr_struct2_debug;
	insert into tax_accrual_cr_struct2_debug
	select * from tax_accrual_cr_structures2_tmp;

	delete from tax_accrual_cr_struct_debug;
	insert into tax_accrual_cr_struct_debug
	select * from tax_accrual_cr_structures_tmp;

	delete from tax_accrual_entity_ded_debug;
	insert into tax_accrual_entity_ded_debug
	select * from tax_accrual_entity_deduct_tmp;

	delete from tax_accrual_rep_months_debug;
	insert into tax_accrual_rep_months_debug
	select * from tax_accrual_rep_months;

end if;

if a_retrieve = 2 then

	delete from tax_accrual_rep_criteria_tmp;
	insert into tax_accrual_rep_criteria_tmp
	select * from tax_accrual_rep_criteria_debug;

	delete from tax_accrual_m_item_tmp;
	insert into tax_accrual_m_item_tmp
	select * from tax_accrual_m_item_debug;

	delete from tax_accrual_account_tmp;
	insert into tax_accrual_account_tmp
	select * from tax_accrual_account_debug;

	delete from tax_accrual_rep_tree_temp;
	insert into tax_accrual_rep_tree_temp
	select * from tax_accrual_rep_tree_debug;

	delete from tax_accrual_eff_rate_tmp2;
	insert into tax_accrual_eff_rate_tmp2
	select * from tax_accrual_eff_rate_debug;

	delete from tax_accrual_rep_je_type_temp;
	insert into tax_accrual_rep_je_type_temp
	select * from tax_accrual_rep_je_type_debug;

	delete from tax_accrual_pt_trc_tmp;
	insert into tax_accrual_pt_trc_tmp
	select * from tax_accrual_pt_trc_debug;

	delete from tax_accrual_pt_tc_rollup_tmp;
	insert into tax_accrual_pt_tc_rollup_tmp
	select * from tax_accrual_pt_tc_rollup_debug;

	delete from tax_accrual_beg_bal_load;
	insert into tax_accrual_beg_bal_load
	select * from tax_accrual_beg_bal_load_debug;

	delete from tax_accrual_eff_rate_tmp;
	insert into tax_accrual_eff_rate_tmp
	select * from tax_accrual_eff_rate_debug1;

	delete from tax_accrual_assign_ns_tmp;
	insert into tax_accrual_assign_ns_tmp
	select * from tax_accrual_assign_ns_debug;

	delete from tax_accrual_subledger_tmp;
	insert into tax_accrual_subledger_tmp
	select * from tax_accrual_subledger_debug;

	delete from tax_accrual_cr_structures2_tmp;
	insert into tax_accrual_cr_structures2_tmp
	select * from tax_accrual_cr_struct2_debug;

	delete from tax_accrual_cr_structures_tmp;
	insert into tax_accrual_cr_structures_tmp
	select * from tax_accrual_cr_struct_debug;

	delete from tax_accrual_entity_deduct_tmp;
	insert into tax_accrual_entity_deduct_tmp
	select * from tax_accrual_entity_ded_debug;

	delete from tax_accrual_rep_months;
	insert into tax_accrual_rep_months
	select * from tax_accrual_rep_months_debug;

end if;

commit;

return 1;

end;
 
 
 
 
 
/