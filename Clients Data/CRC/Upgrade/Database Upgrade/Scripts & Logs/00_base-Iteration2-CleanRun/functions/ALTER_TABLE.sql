
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."ALTER_TABLE" (TABLE_NAME in varchar2,
                                       ALTER_TYPE in varchar2,
                                       CONST      varchar2) return number as
   /* Vers 1.3 6/22/05 */
   /* Vers 1.3.7 10/10/2012 constraint spelling incorrect and foreign key format was wrong */
   /* Vers 1.3.8 6/20/2014 The SQL statement to add foreign keys is misspelled */
   SQL_STMT varchar2(4000);
begin
   if ALTER_TYPE = 'drop constraint' then
      SQL_STMT := 'alter table ' || TABLE_NAME || ' drop constraint ' || CONST;
   elsif ALTER_TYPE = 'add constraint' then
      SQL_STMT := 'alter table ' || TABLE_NAME || ' add foreign key ( ' || CONST;
   elsif ALTER_TYPE = 'add primary key' then
      SQL_STMT := 'alter table ' || TABLE_NAME || ' add ( primary key  ' || CONST || ')';
   else
      return -1;
   end if;
   execute immediate SQL_STMT;
   return 0;
if alter_type = 'drop constraint' then
   sql_stmt  := 'alter table ' || table_name || ' drop constraint ' || const;
elsif alter_type = 'add constraint' then
   sql_stmt  := 'alter table ' || table_name || ' add foreign key ( ' || const;
elsif alter_type = 'add primary key' then
   sql_stmt  := 'alter table ' || table_name || ' add ( primary key  ' || const || ')';
else
   return -1;
end if;
execute immediate sql_stmt;
return 0;
if alter_type = 'drop constraint' then
   sql_stmt  := 'alter table ' || table_name || ' drop constraint ' || const;
elsif alter_type = 'add constraint' then
   sql_stmt  := 'alter table ' || table_name || ' add foreign key ( ' || const || ')';
elsif alter_type = 'add primary key' then
   sql_stmt  := 'alter table ' || table_name || ' add ( primary key  ' || const || ')';
else
   return -1;
end if;
execute immediate sql_stmt;
return 0;
exception
   when others then
      RAISE_APPLICATION_ERROR(-20000,
                              'Failed Alter Table ' || TABLE_NAME || ' Type ' || ALTER_TYPE || 'Constraint ' || CONST ||
                              ' Error: ' || sqlerrm);
      return -1;

end;
 
 
/