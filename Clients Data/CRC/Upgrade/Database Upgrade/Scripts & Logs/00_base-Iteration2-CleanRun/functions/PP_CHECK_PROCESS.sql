
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."PP_CHECK_PROCESS" (A_ARG varchar2) return varchar2 as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PP_CHECK_PROCESS
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By      Reason for Change
   || ------- ---------- --------------- ----------------------------------------
   || 1.0     03/13/2013 Alex Pivoshenko Create
   || 1.1     08/29/2013 C Shilling      maint_31807 - Remove code - call package instead
   ||============================================================================
   */
begin
   return PP_PROCESS_PKG.PP_CHECK_PROCESS(A_ARG);
end PP_CHECK_PROCESS;
 
 
/