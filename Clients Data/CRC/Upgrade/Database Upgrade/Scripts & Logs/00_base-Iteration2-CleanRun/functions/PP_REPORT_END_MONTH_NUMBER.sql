
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."PP_REPORT_END_MONTH_NUMBER" return number is
   END_MONTH number;
begin
   select TO_NUMBER(TO_CHAR(END_MONTH, 'yyyymm'))
     into END_MONTH
     from REPORT_TIME
    where SESSION_ID = USERENV('sessionid')
      and LOWER(USER_ID) = LOWER(user);

   return(END_MONTH);
end;
 
 
 
 
 
 
/