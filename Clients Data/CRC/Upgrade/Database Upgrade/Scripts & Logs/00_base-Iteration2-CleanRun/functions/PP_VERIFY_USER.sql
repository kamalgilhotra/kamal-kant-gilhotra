
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."PP_VERIFY_USER" (a_verify_user   VARCHAR2,
                                          a_verify_passwd VARCHAR2,
                                          a_dn            VARCHAR2,
                                          a_server        VARCHAR2)
   RETURN VARCHAR2

 IS
   num          NUMBER(22,
                       0);
   code         INT;
   retval       PLS_INTEGER;
   my_session   dbms_ldap.session;
   my_message   dbms_ldap.message;
   CLASS        VARCHAR2(100);
   my_attrs     dbms_ldap.string_collection;
   my_dn        VARCHAR2(256);
   sqls         VARCHAR2(256);
   userid       VARCHAR2(254);
   pw_raw       RAW(2049);
   pw           VARCHAR(1024);
   key_raw      RAW(128);
   data_in_raw  RAW(2049);
   data_out_raw RAW(2049);
   dn           VARCHAR2(2048);
   ldap_path    VARCHAR2(2048);
   server       VARCHAR2(1048);
   server_dn    VARCHAR2(2048);
   port         NUMBER(22,
                       0);
   token        VARCHAR2(100) := 'PPCSSOKEYPPC';
   id           VARCHAR2(254);
   pos          INT;
   /*********************** Initail Version 1.0 ***************************************/
   --
   -- Vers 1.1    added support to retrieve dn from table
   /***********************************************************************************/
   CURSOR sso_user_cur IS
      SELECT NAME,
             password,
             dn,
             server,
             server_dn,
             port
        FROM pp_ldap_user;
BEGIN
   IF a_dn = ''
   THEN
      SELECT adsi_path
        INTO my_dn
        FROM pp_security_users
       WHERE upper(users) = upper(a_verify_user)
         AND rownum = 1;

      IF my_dn IS NULL
         OR my_dn = ''
      THEN
         RETURN 'failed';
      END IF;
   ELSE
      my_dn := a_dn;
   END IF;
   BEGIN
      port       := 389;
      my_session := dbms_ldap.init(a_server,
                                   port);
   EXCEPTION
      WHEN OTHERS THEN
         code := SQLCODE;
         raise_application_error(-20000,
                                 'Failed Connect to  ' || server ||
                                 ' Port:' || to_char(port) || ' Code=' ||
                                 to_char(code) || ' Msg: ' || SQLERRM);
         RETURN 'failed';
   END;
   /* check the user's password */
   BEGIN
      --retval := DBMS_LDAP.unbind_s(my_session);
      retval := dbms_ldap.simple_bind_s(my_session,
                                        my_dn,
                                        a_verify_passwd);

   EXCEPTION
      WHEN OTHERS THEN
         code := SQLCODE;
         raise_application_error(-20000,
                                 'Password Invalid for ' || a_verify_user ||
                                 ' Code=' || to_char(code) || ' Msg: ' ||
                                 SQLERRM);
         RETURN 'failed';
   END;
   retval := dbms_ldap.unbind_s(my_session);

   RETURN 'success';

END;
 
 
 
 
 
 
/