
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."SYS_DOES_FUNCTION_EXIST" (funcName varchar2)
  RETURN NUMBER is
  proc_exists number := 0;

begin
  select count(*)
    into proc_exists
    from ALL_PROCEDURES
   where Object_Name = upper(funcName);

  return proc_exists;

end;
 
 
/