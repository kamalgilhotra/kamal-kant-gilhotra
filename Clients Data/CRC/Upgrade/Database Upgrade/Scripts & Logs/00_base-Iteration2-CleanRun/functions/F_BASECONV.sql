
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_BASECONV" (VALUEIN in varchar2, -- incoming value to convert
                                      RADFROM in number, -- source base
                                      RADOUT  in number -- target base
                                      ) return varchar2 -- outgoing value in target base
 is
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: F_BASECONV
   || Description:
   ||============================================================================
   || Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1       10/25/2010
   ||============================================================================
   */

   VALIN          varchar2(1000);
   SIGN           char;
   LENIN          number;
   BASE10VALUE    number;
   DIGITPOOL      varchar2(50) := '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
   DIGITHOLD      varchar(1);
   HIGHPOWER      number;
   CURRVALUE      number;
   CURRDIGIT      number;
   RESULTINGVALUE varchar(2000);

   function GETDIGIT10(INDIGIT in varchar2,
                       RADIN   in number) return number is

      BAD_DIGIT exception;
      pragma exception_init(BAD_DIGIT, -6502);

   begin
      if INDIGIT = '0' then
         return 0;
      end if;
      DIGITHOLD := UPPER(INDIGIT);
      for I in 1 .. RADIN - 1
      loop
         if DIGITHOLD = SUBSTR(DIGITPOOL, I, 1) then
            return I;
         end if;
      end loop;
      RAISE_APPLICATION_ERROR(-20000,
                              'Illegal digit, "' || INDIGIT || '" for base "' || RADIN || '"');
   end;

begin
   VALIN := VALUEIN;

   if SUBSTR(VALIN, 1, 1) = '-' then
      SIGN  := '-';
      VALIN := SUBSTR(VALIN, 2);
   else
      SIGN := null;
   end if;

   LENIN       := LENGTH(NVL(VALIN, '0'));
   BASE10VALUE := 0;

   for I in 1 .. LENIN
   loop
      BASE10VALUE := BASE10VALUE +
                     GETDIGIT10(SUBSTR(VALIN, I, 1), RADFROM) * POWER(RADFROM, LENIN - I);
   end loop;

   for I in 1 .. 1000
   loop
      if POWER(RADOUT, I) > BASE10VALUE then
         HIGHPOWER := I - 1;
         exit;
      end if;
   end loop;

   CURRVALUE      := BASE10VALUE;
   RESULTINGVALUE := null;

   for I in 0 .. HIGHPOWER
   loop
      CURRDIGIT := FLOOR(CURRVALUE / POWER(RADOUT, HIGHPOWER - I));
      CURRVALUE := CURRVALUE - (CURRDIGIT * POWER(RADOUT, HIGHPOWER - I));
      if CURRDIGIT = 0 then
         RESULTINGVALUE := RESULTINGVALUE || '0';
      else
         RESULTINGVALUE := RESULTINGVALUE || SUBSTR(DIGITPOOL, CURRDIGIT, 1);
      end if;
   end loop;

   return SIGN || RESULTINGVALUE;
end;
 
 
 
 
/