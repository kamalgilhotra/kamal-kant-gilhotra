
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_CLIENT_REIMB_DETAIL_UPDATE" (A_BILLING_GROUP_ID number,
                                             A_PRE_UPDATE       number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_DETAIL_UPDATE
   --Note that A_PRE_UPDATE is a boolean in the PB environment but a long here
   return 1;
end;
 
 
/