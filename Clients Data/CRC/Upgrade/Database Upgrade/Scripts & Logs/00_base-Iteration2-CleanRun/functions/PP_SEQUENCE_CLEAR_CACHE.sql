
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."PP_SEQUENCE_CLEAR_CACHE" (A_SEQUENCE varchar2) return number as
   type NUMBERS is table of number;

   INC  number;
   CS   number;
   RAC  varchar2(5);
   BURN NUMBERS;

begin
   select INCREMENT_BY, CACHE_SIZE
     into INC, CS
     from ALL_SEQUENCES
    where SEQUENCE_NAME = UPPER(trim(A_SEQUENCE));
   select "VALUE" into RAC from V$PARAMETER where "NAME" = 'cluster_database';

   if UPPER(RAC) = 'FALSE' then
      /* On non-RAC databases, no need to clear the cache as only 1 cache is being used */
      return 0;
   else
      /* RAC database -- could potentially have multiple caches -- purge the current instance to make sure we have
      fresh values */

      if INC <> 1 then
         /* Expect sequences with increment_by = 1 only. */
         return - 1;
      else
         /* If they are using a cache, burn that many numbers */
         if CS > 0 then
            select CRDETAIL.NEXTVAL bulk collect into BURN from DUAL connect by level <= CS;
            return BURN(CS);
         else
            /* No need to clear */
            return 0;
         end if;
      end if;
   end if;
end;
 
 
/