
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."PP_USER_DESCR" (A_USER varchar2) return varchar2 is

   /*
   ||============================================================================
   || Application: PowerPlant
   || Function: PP_USER_DESCR
   || Description: Return the best description for the user
   ||============================================================================
   || Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.2.1.2 12/03/2009
   ||============================================================================
   */

   V_RTN       varchar2(254);
   PP_ERR_CODE varchar2(254);
   V_FN        varchar2(254);
   V_LN        varchar2(254);
   V_DESC      varchar2(254);

begin
   PP_ERR_CODE := '001';

   select NVL(FIRST_NAME, 'Z'), NVL(LAST_NAME, 'Z'), NVL(DESCRIPTION, 'Z')
     into V_FN, V_LN, V_DESC
     from PP_SECURITY_USERS
    where USERS = trim(LOWER(A_USER));

   if V_FN <> 'Z' and V_LN <> 'Z' then
      return V_LN || ', ' || V_FN;
   end if;

   if V_DESC <> 'Z' then
      return V_DESC;
   end if;

   /* if it gets to here return original users */
   return A_USER;

exception
   when NO_DATA_FOUND then
      /* this catches no_data_found */
      return('Users Not Found');
   when others then
      /* this catches all SQL errors */
      return('ERROR SQL: ' || PP_ERR_CODE || ':' || sqlerrm);
end;
 
 
 
 
 
 
/