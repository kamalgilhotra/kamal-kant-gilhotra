
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_CLIENT_REIMB_REV_CALC_CUSTOM" (A_BILLING_GROUP_ID number,
                                               A_FINAL_REVIEW     number,
                                               A_MONTH_NUMBER     number,
                                               A_BILL_SEQ_ID      number,
                                               A_MAN_REFUND       number) return varchar2 is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_REV_CALC_CUSTOM
   --Note that A_FINAL_REVIEW and A_MAN_REFUND are booleans in PB environment but are converted to longs before this function is called
   return 'OK';
end;
 
 
/