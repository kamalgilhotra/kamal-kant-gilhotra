
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."DROP_TAX_PARTITION" (A_TABLE varchar2, VERS number) return number as
   /* version 1.07 7-23-10 */
   SQLS       varchar2(2000);
   MSG        varchar2(10000);
   OWNER_NAME varchar2(40);
   STATUS     integer;
   TABLENAME  varchar2(40);
   CONSTNAME  varchar2(40);
   CODE       integer;

   cursor CONST_CUR is
      select C.TABLE_NAME, C.CONSTRAINT_NAME
        from ALL_CONSTRAINTS C
       where C.TABLE_NAME in (select C2.TABLE_NAME
                                from ALL_CONSTRAINTS C, ALL_CONSTRAINTS C2
                               where C.CONSTRAINT_NAME = C2.R_CONSTRAINT_NAME
                                 and C.TABLE_NAME = 'TAX_RECORD_CONTROL');

begin
   /* Tables that reference the Archive Tables */
   OWNER_NAME := 'PWRPLANT';

   -- Disable all constraints on children tables
   open CONST_CUR;
   loop
      fetch CONST_CUR
         into TABLENAME, CONSTNAME;
      if (CONST_CUR%notfound) then
         exit;
      end if;
      SQLS := 'alter table ' || TABLENAME || ' disable constraint ' || CONSTNAME;
      begin
         execute immediate SQLS;
      exception
         when others then
            CODE := 0;
      end;
   end loop;
   close CONST_CUR;

   SQLS := 'alter table ' || OWNER_NAME || '.' || A_TABLE || ' drop partition V' ||
           TO_CHAR(VERS + 1) || ' update global indexes'; -- update global indexes';
   execute immediate SQLS;

   -- Disable all constraints on children tables
   open CONST_CUR;
   loop
      fetch CONST_CUR
         into TABLENAME, CONSTNAME;
      if (CONST_CUR%notfound) then
         exit;
      end if;
      SQLS := 'alter table ' || TABLENAME || ' enable constraint ' || CONSTNAME;
      begin
         execute immediate SQLS;
      exception
         when others then
            CODE := 0;
      end;
   end loop;
   close CONST_CUR;

   CODE := 0;
   return 0;
exception
   when others then
      STATUS := sqlcode;
      CODE   := STATUS;
      return STATUS;
end DROP_TAX_PARTITION;
 
 
 
 
 
 
/