
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_CLIENT_REIMB_DETAIL_UPDATE_2" (A_BILLING_GROUP_ID PKG_PP_COMMON.NUM_TABTYPE,
                                               A_SOURCE_ID        PKG_PP_COMMON.NUM_TABTYPE,
                                               A_ID               PKG_PP_COMMON.NUM_TABTYPE,
                                               A_AMT_TYPE_ID      PKG_PP_COMMON.NUM_TABTYPE)
   return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_DETAIL_CUSTOM_UPDATE
   return 1;
end;
 
 
/