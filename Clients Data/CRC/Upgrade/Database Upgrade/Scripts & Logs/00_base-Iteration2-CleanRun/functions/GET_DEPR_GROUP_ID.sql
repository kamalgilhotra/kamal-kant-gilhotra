
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."GET_DEPR_GROUP_ID" (UTIL_ACCT     number,
                                             SUB_ACCT      number,
                                             GL_ACCT       number,
                                             ASSET_LOC     number,
                                             COMPANY       number,
                                             BUS_SEGMENT   number,
                                             SUBLEDGER_IND number,
                                             IN_SVC_YR     number) return number is
/*
||============================================================================
|| Application: PowerPlant
|| Function Name: get_depr_group_id
|| Description: Find the depr_group_id for the parameters passed in.
||              Use Depr Group Control. Similar to PB function f_find_depr_group
|| Returns: Depr Group ID if found
||          -1 Not found
||          -2 Found Inactive depr group
||
|| Example: get_depr_group_id(utility_account_id, sub_account_id, unitized_gl_account,
||          asset_location_id, company_id, bus_segment_id, subledger_type_id,
||          to_number(to_char(in_service_date,'YYYY'))) depr_group_id
||
|| SU ARCS EXAMPLE:
||     select distinct WORK_ORDER_ID, 'Unable to find valid Depr Group (' || DEPR_GROUP_ID || ')' VALUE1
||       from (select C.WORK_ORDER_ID,
||                    GET_DEPR_GROUP_ID(E.UTILITY_ACCOUNT_ID,
||                                      E.SUB_ACCOUNT_ID,
||                                      A.UNITIZED_GL_ACCOUNT,
||                                      AL.ASSET_LOCATION_ID,
||                                      C.COMPANY_ID,
||                                      E.BUS_SEGMENT_ID,
||                                      P.SUBLEDGER_TYPE_ID,
||                                      TO_NUMBER(TO_CHAR(IN_SERVICE_DATE, 'YYYY'))) DEPR_GROUP_ID
||               from WORK_ORDER_CONTROL C,
||                    WORK_ORDER_ACCOUNT A,
||                    WO_ESTIMATE E,
||                    ASSET_LOCATION AL,
||                    (select EI.WORK_ORDER_ID, max(EI.REVISION) MAX_REV
||                       from WO_ESTIMATE EI
||                      group by WORK_ORDER_ID) MV,
||                    RETIREMENT_UNIT R,
||                    PROPERTY_UNIT P,
||                    WO_ARC_TEMP_RUN ARC
||              where ARC.WORK_ORDER_ID = C.WORK_ORDER_ID
||                and C.WORK_ORDER_ID = A.WORK_ORDER_ID
||                and C.WORK_ORDER_ID = E.WORK_ORDER_ID
||                and E.WORK_ORDER_ID = MV.WORK_ORDER_ID
||                and E.REVISION = MV.MAX_REV
||                and NVL(E.ASSET_LOCATION_ID, C.ASSET_LOCATION_ID) = AL.ASSET_LOCATION_ID
||                and E.RETIREMENT_UNIT_ID = R.RETIREMENT_UNIT_ID
||                and R.PROPERTY_UNIT_ID = P.PROPERTY_UNIT_ID)
||      where DEPR_GROUP_ID < 0;
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------  ---------- -------------- ----------------------------------------
|| V10.1.2  01/05/2009
||============================================================================
*/

   MY_DEPR_GROUP_ID number;
   MY_MAJ_LOC       number;
   MY_LOC_TYPE      number;
   MY_CV            varchar2(35);
   MY_RTN           number;

begin
   select MAJOR_LOCATION_ID
     into MY_MAJ_LOC
     from ASSET_LOCATION
    where ASSET_LOCATION_ID = ASSET_LOC;
   select LOCATION_TYPE_ID
     into MY_LOC_TYPE
     from MAJOR_LOCATION
    where MAJOR_LOCATION_ID = MY_MAJ_LOC;
   select NVL(UPPER(trim(CONTROL_VALUE)), 'YES')
     into MY_CV
     from PP_SYSTEM_CONTROL
    where LOWER(CONTROL_NAME) = 'post depr';
   if MY_CV = 'YES' then
      select DEPR_GROUP_ID
        into MY_DEPR_GROUP_ID
        from (select "DEPR_GROUP_CONTROL"."DEPR_GROUP_ID" DEPR_GROUP_ID
                from "DEPR_GROUP_CONTROL", "DEPR_GROUP"
               where DEPR_GROUP.COMPANY_ID = COMPANY
                 and DEPR_GROUP_CONTROL.BUS_SEGMENT_ID = BUS_SEGMENT
                 and DEPR_GROUP_CONTROL.UTILITY_ACCOUNT_ID = UTIL_ACCT
                 and NVL(DEPR_GROUP_CONTROL.GL_ACCOUNT_ID, GL_ACCT) = GL_ACCT
                 and NVL(DEPR_GROUP_CONTROL.MAJOR_LOCATION_ID, MY_MAJ_LOC) = MY_MAJ_LOC
                 and NVL(DEPR_GROUP_CONTROL.SUB_ACCOUNT_ID, SUB_ACCT) = SUB_ACCT
                 and NVL(TO_CHAR(DEPR_GROUP_CONTROL.BOOK_VINTAGE, 'YYYY'), IN_SVC_YR) = IN_SVC_YR
                 and NVL(DEPR_GROUP_CONTROL.SUBLEDGER_TYPE_ID, SUBLEDGER_IND) = SUBLEDGER_IND
                 and NVL(DEPR_GROUP_CONTROL.ASSET_LOCATION_ID, ASSET_LOC) = ASSET_LOC
                 and NVL(DEPR_GROUP_CONTROL.LOCATION_TYPE_ID, MY_LOC_TYPE) = MY_LOC_TYPE
                 and DEPR_GROUP_CONTROL.DEPR_GROUP_ID = DEPR_GROUP.DEPR_GROUP_ID
                 and DEPR_GROUP_CONTROL.BUS_SEGMENT_ID = DEPR_GROUP.BUS_SEGMENT_ID
               order by DEPR_GROUP.COMPANY_ID,
                        DEPR_GROUP_CONTROL.BUS_SEGMENT_ID,
                        DEPR_GROUP_CONTROL.UTILITY_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.SUB_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.GL_ACCOUNT_ID,
                        DEPR_GROUP_CONTROL.MAJOR_LOCATION_ID,
                        DEPR_GROUP_CONTROL.ASSET_LOCATION_ID,
                        DEPR_GROUP_CONTROL.LOCATION_TYPE_ID,
                        TO_CHAR("DEPR_GROUP_CONTROL"."BOOK_VINTAGE", 'YYYY'),
                        DEPR_GROUP_CONTROL.SUBLEDGER_TYPE_ID)
       where ROWNUM = 1;
      select NVL(STATUS_ID, 1) into MY_RTN from DEPR_GROUP where DEPR_GROUP_ID = MY_DEPR_GROUP_ID;
   end if;
   if MY_RTN = 1 then
      return MY_DEPR_GROUP_ID;
   else
      return - 2;
   end if;
exception
   when NO_DATA_FOUND then
      return - 1;
end;
 
 
 
 
 
 
/