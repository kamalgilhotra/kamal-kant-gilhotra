
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."SYS_DOES_TABLE_EXIST" (tableName  VARCHAR2,
                                                         tableOwner VARCHAR2)
  RETURN NUMBER IS
  table_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*)
    INTO table_exists
    FROM ALL_TABLES
   WHERE TABLE_NAME = upper(tableName)
     AND OWNER = upper(tableOwner);

  RETURN table_exists;
END;
 
 
/