
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."PPCPRODUCT" (INPUT number) return number
/*
||============================================================================
|| Application: PowerPlant
|| Function Name: PPCPRODUCT
|| Description:
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------  ---------- -------------- ----------------------------------------
|| V10.2.0  09/25/2008 Lee Quinn      Point Release
||============================================================================
*/

   parallel_enable
   aggregate using T_PRODUCT;
 
 
 
 
 
 
/