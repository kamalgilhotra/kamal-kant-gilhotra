
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_WEM_NPV_REMAIN" (A_WORK_ORDER_ID number,
                                            A_DOCUMENT_ID   number,
                                            A_TAB_INDICATOR number) return number is
   NPV number;
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: WEM_NPV_REMAIN
   || Description: Ignores Sunk Costs/Benefits in NPV Calculation
   ||============================================================================
   || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     3/31/2011  Craig Cormack   Copied f_wem_npv, ignore sunk costs
   ||
   ||============================================================================
   */
   type AMOUNT_TABLE is table of number(22, 2) index by binary_integer;
   V_AMOUNT_TAB AMOUNT_TABLE;

   V_DISC_RATE      number := 0;
   V_NPV            number := 0;
   V_INDEX          number;
   V_ERROR_LOCATION varchar2(128);
   J                number;

begin

   --Get the discount rate (latest effective date by company)
   select DISCOUNT_RATE
     into V_DISC_RATE
     from DISCOUNT_RATE DR
    where COMPANY_ID =
          (select COMPANY_ID
             from WORK_ORDER_CONTROL
            where WORK_ORDER_ID = A_WORK_ORDER_ID
           union
           select COMPANY_ID from BUDGET where BUDGET_ID = A_WORK_ORDER_ID)
      and EFFECTIVE_DATE = (select max(EFFECTIVE_DATE)
                              from DISCOUNT_RATE A
                             where A.COMPANY_ID = DR.COMPANY_ID
                               and EFFECTIVE_DATE <= sysdate);

   if V_DISC_RATE is null then
      V_DISC_RATE := 0;
   end if;

   --Initialize and fill array of amounts
   V_ERROR_LOCATION := 'Initialize and fill array of amounts';
   V_INDEX          := 0;
   for CSR_AMOUNT in (select CASH.TOTAL_BENEFIT -
                             sum(DECODE(WEM.EXPENDITURE_TYPE_ID, 1, WEM.TOTAL, 2, WEM.TOTAL, 0)) -
                             sum(DECODE(WEM.EXPENDITURE_TYPE_ID, 3, WEM.TOTAL, 0)) AMOUNT
                        from WORK_ORDER_CASHFLOW CASH,
                             (select WEM.WORK_ORDER_ID,
                                     WEM.REVISION,
                                     WEM.YEAR,
                                     WEM.EXPENDITURE_TYPE_ID,
                                     WEM.TOTAL
                                from WO_EST_MONTHLY_FY WEM, ESTIMATE_CHARGE_TYPE ECT
                               where WEM.WORK_ORDER_ID = A_WORK_ORDER_ID
                                 and WEM.REVISION =
                                     (
                                      /* BAT 02/02/2011 update */
                                      select DECODE(WOD.EST_REVISION,
                                                     0,
                                                     NVL((select max(BVFP.REVISION)
                                                           from BUDGET_VERSION_FUND_PROJ BVFP,
                                                                BUDGET_VERSION_VIEW      BVV
                                                          where BVV.COMPANY_ID = WOC.COMPANY_ID
                                                            and BVFP.BUDGET_VERSION_ID =
                                                                BVV.FORECAST_VERSION
                                                            and BVFP.WORK_ORDER_ID = WOC.WORK_ORDER_ID
                                                            and BVFP.ACTIVE = 1),
                                                         (select max(A.REVISION)
                                                            from WORK_ORDER_APPROVAL A
                                                           where A.WORK_ORDER_ID = WOC.WORK_ORDER_ID)),
                                                     WOD.EST_REVISION) WOD_REV
                                        from WO_DOCUMENTATION WOD, WORK_ORDER_CONTROL WOC
                                       where WOD.WORK_ORDER_ID = WOC.WORK_ORDER_ID
                                         and WOD.WORK_ORDER_ID = A_WORK_ORDER_ID
                                         and WOD.DOCUMENT_ID = A_DOCUMENT_ID
                                         and WOD.TAB_INDICATOR = A_TAB_INDICATOR)
                                 and WEM.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
                                 and ECT.PROCESSING_TYPE_ID not in (1, 5)
                              union all
                              select WEM.BUDGET_ID           WORK_ORDER_ID,
                                     WEM.BUDGET_VERSION_ID   REVISION,
                                     WEM.YEAR,
                                     WEM.EXPENDITURE_TYPE_ID,
                                     WEM.TOTAL
                                from BUDGET_MONTHLY_DATA_FY WEM, ESTIMATE_CHARGE_TYPE ECT
                               where WEM.BUDGET_ID = A_WORK_ORDER_ID
                                 and WEM.BUDGET_VERSION_ID =
                                     (
                                      /* dkb 12/21/2010 update */
                                      select DECODE(WOD.EST_REVISION,
                                                     0,
                                                     NVL((select max(A.BUDGET_VERSION_ID)
                                                           from BUDGET_AMOUNTS A, BUDGET_VERSION_VIEW BVV
                                                          where BVV.COMPANY_ID = B.COMPANY_ID
                                                            and A.BUDGET_VERSION_ID =
                                                                BVV.FORECAST_VERSION
                                                            and A.BUDGET_ID = B.BUDGET_ID),
                                                         (select max(A.BUDGET_VERSION_ID)
                                                            from BUDGET_AMOUNTS A
                                                           where A.BUDGET_ID = B.BUDGET_ID)),
                                                     WOD.EST_REVISION)
                                        from WO_DOCUMENTATION WOD, BUDGET B
                                       where WOD.WORK_ORDER_ID = B.BUDGET_ID
                                         and WOD.WORK_ORDER_ID = A_WORK_ORDER_ID
                                         and WOD.DOCUMENT_ID = A_DOCUMENT_ID
                                         and WOD.TAB_INDICATOR = A_TAB_INDICATOR)
                                 and WEM.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
                                 and ECT.PROCESSING_TYPE_ID not in (1, 5)) WEM
                       where CASH.WORK_ORDER_ID = A_WORK_ORDER_ID
                         and CASH.DOCUMENT_ID = A_DOCUMENT_ID
                         and CASH.WORK_ORDER_ID = WEM.WORK_ORDER_ID(+)
                         and CASH.YEAR = WEM.YEAR(+)
                         and CASH.YEAR >=
                             (select BVV.CURRENT_YEAR
                                from BUDGET_VERSION_VIEW BVV
                               where BVV.COMPANY_ID =
                                     (select WO.COMPANY_ID
                                        from WORK_ORDER_CONTROL WO
                                       where WO.WORK_ORDER_ID = A_WORK_ORDER_ID))
                       group by CASH.YEAR, CASH.TOTAL_BENEFIT
                       order by CASH.YEAR)
   loop
      V_INDEX := V_INDEX + 1;
      V_AMOUNT_TAB(V_INDEX) := CSR_AMOUNT.AMOUNT;
   end loop;

   for J in 1 .. V_AMOUNT_TAB.COUNT()
   loop
      /*DBMS_OUTPUT.PUT_LINE(round(V_NPV,2));*/
      V_NPV := V_NPV + V_AMOUNT_TAB(J) / POWER((1 + V_DISC_RATE), (J - 1));
   end loop;

   return(V_NPV);

exception
   when others then
      /*RAISE_APPLICATION_ERROR(-20001,
      'WEM_IRR Function Error Loaction: ' || V_ERROR_LOCATION ||
      ' - Error Message: ' || sqlerrm);*/
      return(-3.14159265358);
end F_WEM_NPV_REMAIN;
 
 
 
 
/