
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_BV_TEMP_WORK_ORDER" (REPORT_ID number) return varchar2 is
   /*******************************************************************************************
   //   Function    :   f_bv_temp_work_order
   //
   //   Purpose     :   Populate temp_work_order for reporting based on budget version arguments
   //
   //   Arguments   :   NUMBER report_id  - Identifier of the report being run
   //
   //   Return Codes:   STRING 'OK'       - success
   //                          'ERROR...' - failure
   //
   //   DATE        NAME   REVISION      CHANGES
   //   ----------  ----   -----------   -----------------------------------------------
   //   09-23-2011  CDM    Version 1.0   Initial Version
   //
   //   PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
   *******************************************************************************************/

   NEG_1_EXISTS number;
   NEG_8_EXISTS number;
   NEG_9_EXISTS number;
   BV_EXISTS    number;
   MSG          varchar2(254);
   RET_STR      varchar2(35);

begin

   MSG := 'Deleting from temp_work_order';
   delete from TEMP_WORK_ORDER;

   MSG := 'Checking for -1 records in temp_budget_version';
   select count(*) into NEG_1_EXISTS from TEMP_BUDGET_VERSION where BUDGET_VERSION_ID = -1;

   MSG := 'Checking for -8 records in temp_budget_version';
   select count(*) into NEG_8_EXISTS from TEMP_BUDGET_VERSION where BUDGET_VERSION_ID = -8;

   MSG := 'Checking for -9 records in temp_budget_version';
   select count(*) into NEG_9_EXISTS from TEMP_BUDGET_VERSION where BUDGET_VERSION_ID = -9;

   MSG := 'Checking for budget_version_ids in temp_budget_version';
   select count(*)
     into BV_EXISTS
     from TEMP_BUDGET_VERSION
    where BUDGET_VERSION_ID not in (-1, -8, -9);

   if ((NEG_8_EXISTS > 0 or NEG_9_EXISTS > 0) and BV_EXISTS = 0) or
      (BV_EXISTS > 0 and NEG_8_EXISTS = 0 and NEG_9_EXISTS = 0) then
      MSG := 'Inserting into temp_work_order for -8 and -9';
      insert into TEMP_WORK_ORDER
         (USER_ID, SESSION_ID, BATCH_REPORT_ID, WORK_ORDER_ID)
         select distinct user, USERENV('sessionid'), 0, WORK_ORDER_ID
           from BUDGET_VERSION_FUND_PROJ_SV
          where BUDGET_VERSION_ID in (select BUDGET_VERSION_ID from TEMP_BUDGET_VERSION)
            and ACTIVE = 1;
      RET_STR := ' ' || TO_CHAR(sql%rowcount) || ' rows (-8,-9)';
   elsif (BV_EXISTS > 0 and (NEG_8_EXISTS > 0 or NEG_9_EXISTS > 0)) then
      MSG := 'Inserting into temp_work_order for bv_ids';
      insert into TEMP_WORK_ORDER
         (USER_ID, SESSION_ID, BATCH_REPORT_ID, WORK_ORDER_ID)
         select distinct user, USERENV('sessionid'), 0, WORK_ORDER_ID
           from BUDGET_VERSION_FUND_PROJ_SV
          where BUDGET_VERSION_ID in
                (select BUDGET_VERSION_ID
                   from TEMP_BUDGET_VERSION
                  where BUDGET_VERSION_ID not in (-1, -8, -9))
            and ACTIVE = 1;
      RET_STR := ' ' || TO_CHAR(sql%rowcount) || ' rows (bv)';
   elsif NEG_1_EXISTS > 0 then
      MSG := 'Inserting into temp_work_order for -1';
      insert into TEMP_WORK_ORDER
         (USER_ID, SESSION_ID, BATCH_REPORT_ID, WORK_ORDER_ID)
         select distinct user, USERENV('sessionid'), 0, WORK_ORDER_ID
           from WORK_ORDER_CONTROL_SV
          where FUNDING_WO_INDICATOR = 1;
      RET_STR := ' ' || TO_CHAR(sql%rowcount) || ' rows (-1)';
   end if;

   return 'OK' || RET_STR;

exception
   when others then
      rollback;
      return 'ERROR ' || MSG || ': ' || sqlerrm;

end F_BV_TEMP_WORK_ORDER;
 
 
/