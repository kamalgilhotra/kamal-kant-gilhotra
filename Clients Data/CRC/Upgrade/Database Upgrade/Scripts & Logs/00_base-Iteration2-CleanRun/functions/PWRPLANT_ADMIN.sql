
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."PWRPLANT_ADMIN" (A_CMD             in varchar2,
                                          A_USER            in varchar2,
                                          A_PASSWD          in out varchar2,
                                          A_TABLESPACE      in varchar2 := '',
                                          A_TEMP_TABLESPACE in varchar2 := '',
                                          A_USER_ROLE       in varchar2 := '',
                                          A_DEV_ROLE        in varchar2 := '') return number as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PWRPLANT_ADMIN
   || Description:
   ||============================================================================
   || Copyright (C) 2010 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.1     06-07-2005 Roger Roach    Added Change Password Option Last Update
   || 1.2     06-14-2005 Roger Roach    Remove the privilege check
   || 1.3     09-08-2005 Roger Roach    added the privilege check
   || 1.4     09-17-2005 Roger Roach    change the size the raw from 2049 to 256
   || 1.5     01-19-2006 Roger Roach    added the alter user profile,expire and unlock
   || 1.6     03-01-2006 Roger Roach    added quotes for password  rkr
   || 1.7     05-31-2006 Roger Roach    prevent pwrplant, sys and system from being drop
   || 1.8     08-17-2006 Roger Roach    add upper to drop user
   || 1.9     11-07-2006 Roger Roach    add replace to changepassword
   || 2.0     02-20-2007 Roger Roach    add check for nothing being selected
   || 2.1     02-20-2007 Roger Roach    add upper to profile and unlock
   || 2.2     03-05-2007 Roger Roach    add check osuser and login ID
   || 2.3     03-05-2007 Roger Roach    add grant proxy to user
   || 2.4     03-08-2007 Roger Roach    add revoke proxy to user
   || 2.5     03-16-2007 Roger Roach    add AD verification
   || 2.6     03-20-2007 Roger Roach    add distinguished name
   || 2.7     03-29-2007 Roger Roach    add argument to pp_verify_user
   || 2.8     02-29-2008 Roger Roach    add check for proxy id
   || 2.9     03-19-2009 Roger Roach    remove quotes from user's ID
   || 3.0     06-10-2009 Roger Roach    support for connecting from a web page
   || 3.0.1   06-22-2009 Roger Roach    special code for Amerem
   || 3.1.1   10-12-2009 Roger Roach    remove question marks from user's ID
   || 3.1.2   10-16-2009 Roger Roach    remove periods from user's ID
   || 3.1.3   10-28-2009 Roger Roach    added trim around password change
   || 3.1.4   01-04-2010 Roger Roach    changed pp_verify_user to use db ID instead of OS ID
   || 3.1.5   07-01-2010 Lee Quinn      Removed domain name from OS User
   || 3.1.6   08-26-2010 Lee Quinn      Quoted the password on the Add User to handle special charcters.
   || 3.1.7   10-07-2013 Lee Quinn      Removed code that strips periods, apostrophes, question marks from user names.
   || 3.1.8   04-02-2014 Lee Quinn      Added new command getrolesp so Interfaces can log on as a proxy
	|| 3.1.9	  12-08-2015 Sarah Byers    Add code to strip characters from user names back using a system control.
   ||============================================================================
   */

   CODE         int;
   KEY_RAW      raw(128);
   DATA_IN_RAW  raw(2049);
   DATA_OUT_RAW raw(2049);
	POS          number(22, 0);
   SQLS         varchar2(200);
	STR1         varchar2(200);
   STR2         varchar2(200);
   OSUSERID     varchar2(100);
   PROXY_USER   varchar2(100);
   PRIV_FLAG    number(22, 0);
	STRIP_CHAR	 varchar2(254);

begin
   CODE := -1;

   select count(1)
     into PRIV_FLAG
     from DBA_ROLE_PRIVS
    where GRANTED_ROLE = 'PWRPLANT_ROLE_ADMIN'
      and GRANTEE = user;

   if A_CMD = 'drop' and PRIV_FLAG > 0 then
      if UPPER(A_USER) = 'SYS' or UPPER(A_USER) = 'SYSTEM' or UPPER(A_USER) = 'PWRPLANT' then
         RAISE_APPLICATION_ERROR(-20021, 'Error can not drop user ' || A_USER || '.');
         return - 20021;
      end if;
      SQLS := 'drop user "' || UPPER(A_USER) || '" cascade';
      execute immediate SQLS;
      CODE := 0;
   elsif A_CMD = 'profile' and PRIV_FLAG > 0 then
      SQLS := 'alter user "' || UPPER(A_USER) || '" profile ' || A_PASSWD;
      execute immediate SQLS;
      CODE := 0;
   elsif A_CMD = 'unlock' and PRIV_FLAG > 0 then
      SQLS := 'alter user "' || UPPER(A_USER) || '" account unlock';
      execute immediate SQLS;
      CODE := 0;
   elsif A_CMD = 'add' and PRIV_FLAG > 0 then
      SQLS := 'create user "' || UPPER(A_USER) || '" identified by "' || A_PASSWD ||
              '" default tablespace ' || A_TABLESPACE || ' temporary tablespace ' ||
              A_TEMP_TABLESPACE || ' quota unlimited on ' || A_TABLESPACE;
      execute immediate SQLS;
      SQLS := 'grant ' || A_USER_ROLE || ' to "' || UPPER(A_USER) || '"';
      execute immediate SQLS;
      SQLS := 'grant ' || A_DEV_ROLE || ' to "' || UPPER(A_USER) || '"';
      execute immediate SQLS;
      SQLS := 'alter user "' || UPPER(A_USER) || '" default role ' || A_USER_ROLE;
      execute immediate SQLS;
      CODE := 0;
   elsif A_CMD = 'getrole' then

		OSUSERID := NVL(SYS_CONTEXT('USERENV', 'OS_USER'), 'NONE');

		-- remove character defined in system control from user's ID, single space is the default value which indicates nothing should be removed
		select substr(trim(control_value), 1, 1)
		  into STRIP_CHAR
		  from pp_system_control
		 where upper(trim(control_name)) = 'PWRPLANT_ADMIN: REMOVE CHAR'
			and company_id = -1;

		if STRIP_CHAR <> ' ' then
			if INSTR(OSUSERID, STRIP_CHAR) > 0 then
				while INSTR(OSUSERID, STRIP_CHAR) > 0
				loop
					POS      := INSTR(OSUSERID, STRIP_CHAR);
					STR1     := SUBSTR(OSUSERID, 1, POS - 1);
					STR2     := SUBSTR(OSUSERID, POS + 1);
					OSUSERID := STR1 || STR2;
				end loop;
			end if;
		end if;

      -- remove domain name from user's ID
      if INSTR(OSUSERID, '\') > 0 then
         OSUSERID := SUBSTR(OSUSERID, INSTR(OSUSERID, '\') + 1, LENGTH(OSUSERID));
      end if;

      PROXY_USER := NVL(SYS_CONTEXT('USERENV', 'PROXY_USER'), 'NONE');

      if A_TABLESPACE <> ' ' and PROXY_USER <> 'NONE' then
         if PP_VERIFY_USER(user, A_TABLESPACE, A_TEMP_TABLESPACE, A_USER_ROLE) <> 'success' then
            RAISE_APPLICATION_ERROR(-20022,
                                    'Error user ID and Password Failed ! user ID = ' || user ||
                                    ' OS ID = ' || OSUSERID);
            return -1;
         end if;
      else
         -- if no password is specified the OS IDs must match
         if UPPER(OSUSERID) <> UPPER(user) and PROXY_USER <> 'NONE' then
            RAISE_APPLICATION_ERROR(-20022,
                                    'Error user ID and OS ID do not match ! user ID = ' || user ||
                                    ' OS ID = ' || OSUSERID);
            return -1;
         end if;
      end if;
      KEY_RAW := UTL_RAW.CAST_TO_RAW(RPAD(A_USER, 24));
      select DATA into DATA_IN_RAW from PP_SECURITY_DATA where ID = 1;
      DBMS_OBFUSCATION_TOOLKIT.DESDECRYPT(INPUT          => DATA_IN_RAW,
                                          KEY            => KEY_RAW,
                                          DECRYPTED_DATA => DATA_OUT_RAW);
      A_PASSWD := UTL_RAW.CAST_TO_VARCHAR2(DATA_OUT_RAW);

      CODE := 0;
   elsif A_CMD = 'getrolesp' then
      KEY_RAW := UTL_RAW.CAST_TO_RAW(RPAD(A_USER, 24));
      select DATA into DATA_IN_RAW from PP_SECURITY_DATA where ID = 1;
      DBMS_OBFUSCATION_TOOLKIT.DESDECRYPT(INPUT          => DATA_IN_RAW,
                                          KEY            => KEY_RAW,
                                          DECRYPTED_DATA => DATA_OUT_RAW);
      A_PASSWD := UTL_RAW.CAST_TO_VARCHAR2(DATA_OUT_RAW);

      CODE := 0;

   elsif A_CMD = 'setrole' and PRIV_FLAG > 0 then
      KEY_RAW     := UTL_RAW.CAST_TO_RAW(RPAD(A_USER, 24));
      DATA_IN_RAW := UTL_RAW.CAST_TO_RAW(RPAD(A_PASSWD, 48));
      DBMS_OBFUSCATION_TOOLKIT.DESENCRYPT(INPUT          => DATA_IN_RAW,
                                          KEY            => KEY_RAW,
                                          ENCRYPTED_DATA => DATA_OUT_RAW);
      update PP_SECURITY_DATA set DATA = DATA_OUT_RAW where ID = 1;
      commit;
      CODE := 0;
   elsif A_CMD = 'changepasswd' and PRIV_FLAG > 0 then
      SQLS := 'alter user "' || UPPER(A_USER) || '" identified by  "' || trim(A_PASSWD) ||
              '" replace dumbpassword';
      --  version 3.0.1
      --SQLS := 'alter user "' || UPPER(A_USER) || '" IDENTIFIED GLOBALLY AS ''cn='|| UPPER(A_USER) ||',ou=Internal,ou=People,dc=ameren,dc=com'' ';
      execute immediate SQLS;
      CODE := 0;
   elsif A_CMD = 'expire' and PRIV_FLAG > 0 then
      SQLS := 'alter user "' || UPPER(A_USER) || '" PASSWORD EXPIRE';
      execute immediate SQLS;
      CODE := 0;
   elsif A_CMD = 'proxy' and PRIV_FLAG > 0 then
      SQLS := 'alter user  "' || UPPER(A_USER) || '" grant connect through "' || UPPER(A_PASSWD) || '"';
      execute immediate SQLS;
      CODE := 0;
   elsif A_CMD = 'revoke proxy' and PRIV_FLAG > 0 then
      SQLS := 'alter user  "' || UPPER(A_USER) || '" revoke connect through "' || UPPER(A_PASSWD) || '"';
      execute immediate SQLS;
      CODE := 0;
   end if;

   return CODE;

exception
   when others then
      CODE := sqlcode;
      RAISE_APPLICATION_ERROR(-20021,
                              'Error PWRPLANT_ADMIN ! Msg = ' || sqlerrm(CODE) || ' code=' ||
                              TO_CHAR(CODE));
      return CODE;
end PWRPLANT_ADMIN;
 
/