
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_TAX_REPAIRS_DEBUG" (
                                               -- a_retrieve = 1 means we are storing data from temp tables
                                               -- a_retrieve = 2 means we are populating temp tables with data from non-temp tables
                                               A_RETRIEVE number) return number is

   pragma autonomous_transaction;

begin

   if A_RETRIEVE > 2 or A_RETRIEVE < 1 then
      return -1;
   end if;

   if A_RETRIEVE = 1 then

      delete from REPAIR_WORK_ORDER_DEBUG;
      insert into REPAIR_WORK_ORDER_DEBUG
         select * from REPAIR_WORK_ORDER_TEMP;

      delete from REPAIR_WORK_ORDER_ORIG_DEBUG;
      insert into REPAIR_WORK_ORDER_ORIG_DEBUG
         select * from REPAIR_WORK_ORDER_TEMP_ORIG;

      delete from REPAIR_AMOUNT_ALLOCATE_DEBUG;
      insert into REPAIR_AMOUNT_ALLOCATE_DEBUG
         select * from REPAIR_AMOUNT_ALLOCATE;

      delete from REPAIR_BLANKET_PROC_DEBUG;
      insert into REPAIR_BLANKET_PROC_DEBUG
         select * from REPAIR_BLANKET_PROCESSING;

      delete from REPAIR_BLANKET_RESULTS_DEBUG;
      insert into REPAIR_BLANKET_RESULTS_DEBUG
         select * from REPAIR_BLANKET_RESULTS;

      delete from REPAIR_CALC_PRA_REVERSE_DEBUG;
      insert into REPAIR_CALC_PRA_REVERSE_DEBUG
         select * from REPAIR_CALC_PRA_REVERSE;

   end if;

   if A_RETRIEVE = 2 then

      delete from REPAIR_WORK_ORDER_TEMP;
      insert into REPAIR_WORK_ORDER_TEMP
         select * from REPAIR_WORK_ORDER_DEBUG;

      delete from REPAIR_WORK_ORDER_TEMP_ORIG;
      insert into REPAIR_WORK_ORDER_TEMP_ORIG
         select * from REPAIR_WORK_ORDER_ORIG_DEBUG;

      delete from REPAIR_AMOUNT_ALLOCATE;
      insert into REPAIR_AMOUNT_ALLOCATE
         select * from REPAIR_AMOUNT_ALLOCATE_DEBUG;

      delete from REPAIR_BLANKET_PROCESSING;
      insert into REPAIR_BLANKET_PROCESSING
         select * from REPAIR_BLANKET_PROC_DEBUG;

      delete from REPAIR_BLANKET_RESULTS;
      insert into REPAIR_BLANKET_RESULTS
         select * from REPAIR_BLANKET_RESULTS_DEBUG;

      delete from REPAIR_CALC_PRA_REVERSE;
      insert into REPAIR_CALC_PRA_REVERSE
         select * from REPAIR_CALC_PRA_REVERSE_DEBUG;

   end if;

   commit;

   return 1;
end;
 
 
/