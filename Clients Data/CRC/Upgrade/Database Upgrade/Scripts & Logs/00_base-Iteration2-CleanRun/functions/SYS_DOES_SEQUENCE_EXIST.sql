
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."SYS_DOES_SEQUENCE_EXIST" (sequenceName  varchar2,
                                                            sequenceOwner varchar2)
  RETURN NUMBER is
  seq_count number := 0;

begin
	select count(*) into seq_count
	from all_objects where object_type = 'SEQUENCE'
	and Owner = upper(sequenceOwner)
	and Object_Name = upper(sequenceName);

	return seq_count;

end;
 
 
/