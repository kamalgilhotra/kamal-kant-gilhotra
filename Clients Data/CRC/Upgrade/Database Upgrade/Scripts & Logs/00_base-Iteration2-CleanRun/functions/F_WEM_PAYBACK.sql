
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_WEM_PAYBACK" (A_WORK_ORDER_ID number,
                                         A_DOCUMENT_ID   number,
                                         A_TAB_INDICATOR number) return number is

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: f_wem_payback
   || Description: Returns the first year where cumulative benefits exceed cumulative costs
   ||============================================================================
   || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By      Reason for Change
   || ------- ---------- --------------  ----------------------------------------
   || 1.0    3/21/2011  Craig Cormack    Initial Version
   ||============================================================================
   */

   V_ERROR_LOCATION varchar2(250);
   V_YEAR           number;

begin

   V_ERROR_LOCATION := 'Select Year.';

   select NVL(min(year), 0) year
     into V_YEAR
     from (select WORK_ORDER_ID,
                  DOCUMENT_ID,
                  year,
                  COSTS,
                  BENEFITS,
                  sum(COSTS) OVER(order by year) CUM_COST,
                  sum(BENEFITS) OVER(order by year) CUM_BEN
             from (select WORK_ORDER_ID, DOCUMENT_ID, year, sum(COSTS) COSTS, sum(BENEFITS) BENEFITS
                     from (select WOD.WORK_ORDER_ID WORK_ORDER_ID,
                                  WOD.DOCUMENT_ID DOCUMENT_ID,
                                  WEM.YEAR year,
                                  sum(WEM.TOTAL) COSTS,
                                  0 BENEFITS
                             from WO_DOCUMENTATION     WOD,
                                  WO_EST_MONTHLY_FY    WEM,
                                  WORK_ORDER_CONTROL   WOC,
                                  ESTIMATE_CHARGE_TYPE ECT
                            where WOD.WORK_ORDER_ID = A_WORK_ORDER_ID
                              and WOD.DOCUMENT_ID = A_DOCUMENT_ID
                              and WOD.WORK_ORDER_ID = WEM.WORK_ORDER_ID
                              and WEM.REVISION =
                                  DECODE(WOD.EST_REVISION,
                                         0,
                                         NVL((select max(BVFP.REVISION)
                                               from BUDGET_VERSION_FUND_PROJ BVFP,
                                                    BUDGET_VERSION_VIEW      BVV
                                              where BVV.COMPANY_ID = WOC.COMPANY_ID
                                                and BVFP.BUDGET_VERSION_ID = BVV.FORECAST_VERSION
                                                and BVFP.WORK_ORDER_ID = WOC.WORK_ORDER_ID
                                                and BVFP.ACTIVE = 1),
                                             (select max(A.REVISION)
                                                from WORK_ORDER_APPROVAL A
                                               where A.WORK_ORDER_ID = WOC.WORK_ORDER_ID)),
                                         WOD.EST_REVISION)
                              and WOD.TAB_INDICATOR = A_TAB_INDICATOR
                              and WOD.WORK_ORDER_ID = WOC.WORK_ORDER_ID
                              and WEM.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
                              and ECT.PROCESSING_TYPE_ID not in (1, 5)
                            group by WOD.WORK_ORDER_ID, WOD.DOCUMENT_ID, WEM.YEAR
                           union
                           select WOD.WORK_ORDER_ID WORK_ORDER_ID,
                                  WOD.DOCUMENT_ID DOCUMENT_ID,
                                  CASH.YEAR year,
                                  0 COSTS,
                                  sum(CASH.TOTAL_BENEFIT) BENEFITS
                             from WO_DOCUMENTATION    WOD,
                                  WORK_ORDER_CASHFLOW CASH,
                                  WORK_ORDER_CONTROL  WOC
                            where WOD.WORK_ORDER_ID = A_WORK_ORDER_ID
                              and WOD.DOCUMENT_ID = A_DOCUMENT_ID
                              and WOD.WORK_ORDER_ID = CASH.WORK_ORDER_ID
                              and WOD.DOCUMENT_ID = CASH.DOCUMENT_ID
                              and WOD.TAB_INDICATOR = A_TAB_INDICATOR
                              and WOD.WORK_ORDER_ID = WOC.WORK_ORDER_ID
                            group by WOD.WORK_ORDER_ID, WOD.DOCUMENT_ID, CASH.YEAR
                           union all
                           select WOD.WORK_ORDER_ID WORK_ORDER_ID,
                                  WOD.DOCUMENT_ID DOCUMENT_ID,
                                  WEM.YEAR year,
                                  sum(WEM.TOTAL) COSTS,
                                  0 BENEFITS
                             from WO_DOCUMENTATION       WOD,
                                  BUDGET_MONTHLY_DATA_FY WEM,
                                  BUDGET                 WOC,
                                  ESTIMATE_CHARGE_TYPE   ECT
                            where WOD.WORK_ORDER_ID = A_WORK_ORDER_ID
                              and WOD.DOCUMENT_ID = A_DOCUMENT_ID
                              and WOD.WORK_ORDER_ID = WEM.BUDGET_ID
                              and WEM.BUDGET_VERSION_ID =
                                  DECODE(WOD.EST_REVISION,
                                         0,
                                         NVL((select max(A.BUDGET_VERSION_ID)
                                               from BUDGET_AMOUNTS A, BUDGET_VERSION_VIEW BVV
                                              where BVV.COMPANY_ID = WOC.COMPANY_ID
                                                and A.BUDGET_VERSION_ID = BVV.FORECAST_VERSION
                                                and A.BUDGET_ID = WOC.BUDGET_ID),
                                             (select max(A.BUDGET_VERSION_ID)
                                                from BUDGET_AMOUNTS A
                                               where A.BUDGET_ID = WOC.BUDGET_ID)),
                                         WOD.EST_REVISION)
                              and WOD.TAB_INDICATOR = A_TAB_INDICATOR
                              and WOD.WORK_ORDER_ID = WOC.BUDGET_ID
                              and WEM.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
                              and ECT.PROCESSING_TYPE_ID not in (1, 5)
                            group by WOD.WORK_ORDER_ID, WOD.DOCUMENT_ID, WEM.YEAR
                           union
                           select WOD.WORK_ORDER_ID WORK_ORDER_ID,
                                  WOD.DOCUMENT_ID DOCUMENT_ID,
                                  CASH.YEAR year,
                                  0 COSTS,
                                  sum(CASH.TOTAL_BENEFIT) BENEFITS
                             from WO_DOCUMENTATION WOD, WORK_ORDER_CASHFLOW CASH, BUDGET WOC
                            where WOD.WORK_ORDER_ID = A_WORK_ORDER_ID
                              and WOD.DOCUMENT_ID = A_DOCUMENT_ID
                              and WOD.WORK_ORDER_ID = CASH.WORK_ORDER_ID
                              and WOD.DOCUMENT_ID = CASH.DOCUMENT_ID
                              and WOD.TAB_INDICATOR = A_TAB_INDICATOR
                              and WOD.WORK_ORDER_ID = WOC.BUDGET_ID
                            group by WOD.WORK_ORDER_ID, WOD.DOCUMENT_ID, CASH.YEAR)
                    group by WORK_ORDER_ID, DOCUMENT_ID, year
                    order by year)
            where COSTS <> 0
               or BENEFITS <> 0)
    where CUM_COST <= CUM_BEN;

   return V_YEAR;

exception
   when NO_DATA_FOUND then
      RAISE_APPLICATION_ERROR(-20001,
                              'F_WEM_PAYBACK Function Error Loaction: No Data Found' ||
                              V_ERROR_LOCATION || ' - Error Message: ' || sqlerrm);
   when others then
      RAISE_APPLICATION_ERROR(-20002,
                              'F_WEM_PAYBACK Function Error Loaction: ' || V_ERROR_LOCATION ||
                              ' - Error Message: ' || sqlerrm);
end F_WEM_PAYBACK;
 
 
 
 
/