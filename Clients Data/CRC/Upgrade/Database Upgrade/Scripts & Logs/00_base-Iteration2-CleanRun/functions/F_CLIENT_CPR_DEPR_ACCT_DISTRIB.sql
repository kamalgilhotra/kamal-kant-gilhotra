
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_CLIENT_CPR_DEPR_ACCT_DISTRIB" (A_COMPANY_ID number,
                                               A_MONTH      date) return number is
-- *********************************************
-- In order to utilize an extension function.
-- The following table MUST be populated
-- pp_client_extensions:
--    id = A unique number
--    function_name = the name of the oracle function being implemented
--    is_active = 1 (Can be inactivated later by setting to 0)
-- *********************************************

begin
   --maps to PB function F_CPR_DEPR_ACCT_DISTRIB
   --RETURN 0 IF SUCCESS, -1 OTHERWISE
   return 0;
end F_CLIENT_CPR_DEPR_ACCT_DISTRIB;
 
 
/