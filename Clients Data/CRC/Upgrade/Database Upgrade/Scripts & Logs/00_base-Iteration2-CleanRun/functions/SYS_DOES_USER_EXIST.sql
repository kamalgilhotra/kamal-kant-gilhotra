
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."SYS_DOES_USER_EXIST" (aUserName varchar2)
  RETURN NUMBER is
  user_exists number := 0;

begin
  select count(*)
    into user_exists
    from ALL_USERS
   where UserName = upper(aUserName);

  return user_exists;

end;
 
 
/