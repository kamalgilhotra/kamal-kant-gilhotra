
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."CURR_BV" (a_bv_id NUMBER) RETURN NUMBER IS
   curr_id NUMBER(22,
                  8);
   counter NUMBER(22,
                  0);
BEGIN

   SELECT COUNT(*)
     INTO counter
     FROM budget_version
    WHERE budget_version_id = a_bv_id;

   IF counter = 0
   THEN
      RETURN 0;
   ELSE
      SELECT reporting_currency_id
        INTO curr_id
        FROM budget_version
       WHERE budget_version_id = a_bv_id;

      RETURN curr_id;
   END IF;
END;
 
 
 
 
 
 
/