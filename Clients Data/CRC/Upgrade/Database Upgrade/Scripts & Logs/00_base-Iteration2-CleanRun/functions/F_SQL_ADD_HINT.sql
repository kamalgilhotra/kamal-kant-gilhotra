
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_SQL_ADD_HINT" (A_SQL     varchar2,
                                          A_KEYWORD varchar2) return varchar2 is
   LS_SQL      varchar2(32767);
   LL_HINT_POS number;
   LB_ADD_HINT boolean;
   type HINT_LIST is table of PP_DATAWINDOW_HINTS.HINT%type index by binary_integer;
   LS_HINT HINT_LIST;
   type SELECT_NUMBER_LIST is table of PP_DATAWINDOW_HINTS.SELECT_NUMBER%type index by binary_integer;
   LL_SELECT_NUMBER SELECT_NUMBER_LIST;
begin
   --sww - don't upper because that will interfer with any static values built into the SQL that need to be a certain case
   LS_SQL := A_SQL;

   select HINT, SELECT_NUMBER bulk collect
     into LS_HINT, LL_SELECT_NUMBER
     from PP_DATAWINDOW_HINTS
    where LOWER(DATAWINDOW) = LOWER(A_KEYWORD)
    order by SELECT_NUMBER;

   for II in 1 .. LS_HINT.COUNT
   loop
      LL_HINT_POS := 1;
      LB_ADD_HINT := true;

      if INSTR(LS_SQL, '.select') > 0 then
         LL_SELECT_NUMBER(II) := LL_SELECT_NUMBER(II) + 1;
      end if; -- the sql has datawindow.select in front of it

      -- Check for Select number = 0 - This represents a hint that will be added to an
      -- UPDATE, INSERT, or DELETE statement; check for spaces or tabs after the keyword; can't just check for keyword as it may be part of table or column name
      if LL_SELECT_NUMBER(II) = 0 then
         if INSTR(replace(UPPER(LS_SQL), CHR(9), ' '), 'UPDATE ', 1) > 0 then
            LL_HINT_POS := INSTR(replace(UPPER(LS_SQL), CHR(9), ' '), 'UPDATE ', 1);
            LL_HINT_POS := LL_HINT_POS + 6;
         elsif INSTR(replace(UPPER(LS_SQL), CHR(9), ' '), 'INSERT ', 1) > 0 then
            LL_HINT_POS := INSTR(replace(UPPER(LS_SQL), CHR(9), ' '), 'INSERT ', 1);
            LL_HINT_POS := LL_HINT_POS + 6;
         elsif INSTR(replace(UPPER(LS_SQL), CHR(9), ' '), 'DELETE ', 1) > 0 then
            LL_HINT_POS := INSTR(replace(UPPER(LS_SQL), CHR(9), ' '), 'DELETE ', 1);
            LL_HINT_POS := LL_HINT_POS + 6;
         else
            LB_ADD_HINT := false;
         end if;
      else
         --loop until we reach the correct select
         --check for space or tabs after the SELECT; can't just check for SELECT by itself as it may be part of table or column name
         for JJ in 1 .. LL_SELECT_NUMBER(II)
         loop
            LL_HINT_POS := INSTR(replace(UPPER(LS_SQL), CHR(9), ' '), 'SELECT ', LL_HINT_POS);
            if LL_HINT_POS = 0 then
               LB_ADD_HINT := false;
               exit;
            end if;
            LL_HINT_POS := LL_HINT_POS + 6;
         end loop;
      end if;

      if LB_ADD_HINT then
         --add the hint
         LS_SQL := SUBSTR(LS_SQL, 1, LL_HINT_POS) || ' ' || LS_HINT(II) || ' ' ||
                   SUBSTR(LS_SQL, LL_HINT_POS + 1);
      end if;

   end loop;

   return(LS_SQL);
end F_SQL_ADD_HINT;

 
 
/