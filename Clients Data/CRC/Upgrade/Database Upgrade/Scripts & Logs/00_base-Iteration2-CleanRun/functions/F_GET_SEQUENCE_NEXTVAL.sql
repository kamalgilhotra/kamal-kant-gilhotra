
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_GET_SEQUENCE_NEXTVAL" (sequence_owner VARCHAR2, sequence_name VARCHAR2) RETURN NUMBER AS
  val NUMBER;
BEGIN
  EXECUTE IMMEDIATE 'SELECT ' || sequence_owner  || '.' || sequence_name || '.nextval from dual' INTO val;
  return val;
END;
/