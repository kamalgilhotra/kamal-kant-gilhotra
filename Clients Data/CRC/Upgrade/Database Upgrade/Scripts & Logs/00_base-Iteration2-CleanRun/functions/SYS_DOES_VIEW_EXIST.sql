
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."SYS_DOES_VIEW_EXIST" (viewName  varchar2,
                                                        viewOwner varchar2)
  RETURN NUMBER is
  view_exists number := 0;

begin
  select count(*)
    into view_exists
    from ALL_VIEWS
   where VIEW_NAME = upper(viewName)
     and OWNER = upper(viewOwner);

  return view_exists;
end;
 
 
/