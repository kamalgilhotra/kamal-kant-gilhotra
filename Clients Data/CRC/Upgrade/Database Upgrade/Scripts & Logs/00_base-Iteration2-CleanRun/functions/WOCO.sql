
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."WOCO" (wo_number VARCHAR2, co_id NUMBER)
   RETURN NUMBER IS
   wo_id NUMBER;
BEGIN
   SELECT work_order_id
     INTO wo_id
     FROM work_order_control
    WHERE work_order_number = wo_number
      AND company_id = co_id;
   RETURN(wo_id);
END;
 
 
 
 
 
 
/