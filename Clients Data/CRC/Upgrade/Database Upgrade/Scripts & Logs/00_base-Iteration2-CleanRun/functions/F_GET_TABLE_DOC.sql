
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_GET_TABLE_DOC" (filter in varchar2) return CLOB is
  Result CLOB;

  CURSOR emp_cursor IS
  SELECT WIKI
  FROM (
   SELECT TABLE_NAME, -1 column_id, 'h2. '||table_name||chr(13)||chr(10)||nvl(replace(replace(replace(replace(COMMENTS,'[','('),']',')'),'{','('),'}',')'),'Null table description')||chr(13)||chr(10)||
                                    '||Column Name||Constraints||Data Type||Nullable||Description||'||CHR(13)||CHR(10) WIKI
        FROM all_tab_comments
        WHERE owner = 'PWRPLANT'
        and table_type = 'TABLE'
        and REGEXP_LIKE(table_name, filter)
    UNION ALL
    SELECT
        All_tab_columns.TABLE_NAME, ALL_TAB_COLUMNS.COLUMN_ID,
        '|'||
        All_col_comments.column_name
        ||'|'||
          (select
          NVL(LISTAGG(case all_constraints.constraint_Type
                 when 'P' then
                     '(pk)'
                 when 'R' then
                      (select '[(fk)|Table Doc '||substr(temp.table_name, 1,1)||'#'||temp.table_name||']'
                      from all_constraints temp where temp.constraint_name = all_constraints.r_constraint_name)
                 else
                   ' '
            end, ' ') within group(order by all_constraints.constraint_Type), ' ')
          from All_cons_columns, all_constraints
          where All_cons_columns.table_name = All_col_comments.table_name
            and All_cons_columns.column_name = All_col_comments.column_name
            and All_cons_columns.constraint_name not like 'SYS_%'
            and (all_constraints.constraint_type = 'P' or all_constraints.constraint_type = 'R')
            and all_constraints.constraint_name = all_cons_columns.constraint_name
            and all_constraints.table_name = all_cons_columns.table_name)
          ||'|'||
          CASE
            WHEN All_tab_columns.data_type IN ('NUMBER','FLOAT') THEN
                All_tab_columns.data_type||'('||All_tab_columns.data_length||','||All_tab_columns.data_scale||')'
            WHEN All_tab_columns.data_type IN ('VARCHAR2','VARCHAR','CHAR') THEN
                All_tab_columns.data_type||'('||All_tab_columns.data_length||')'
            ELSE
              All_tab_columns.data_type
          END
          ||'|'||
          decode(All_tab_columns.nullable,'Y', 'NULL', 'N', 'NOT NULL')
          ||'|'||
          NVL(replace(replace(replace(replace(All_col_comments.comments,'[','('),']',')'),'{','('),'}',')'),' ')||'|'||CHR(13)||CHR(10) WIKI
    from All_col_comments, All_tab_columns, All_tab_comments
    where all_tab_columns.owner = 'PWRPLANT'
      and REGEXP_LIKE(All_col_comments.table_name, filter)
      and All_tab_columns.table_name = All_col_comments.table_name
      and All_col_comments.column_name = All_tab_columns.column_name
      and all_col_comments.table_name = all_tab_comments.table_name
      and all_tab_comments.table_type = 'TABLE'
  )
  order by TABLE_NAME, COLUMN_ID;


 TYPE employeeArrayType IS TABLE OF CLOB;
 employeeArray employeeArrayType;

begin
  result := to_clob('');
  OPEN emp_cursor;
  LOOP
    FETCH emp_cursor BULK COLLECT INTO employeeArray;
    EXIT WHEN emp_cursor%NOTFOUND;
  END LOOP;
  CLOSE emp_cursor;

  for i in 1..employeeArray.count loop
     result := result||employeeArray(i);
  end loop;

  return(Result);
end F_GET_TABLE_DOC;

 
 
/