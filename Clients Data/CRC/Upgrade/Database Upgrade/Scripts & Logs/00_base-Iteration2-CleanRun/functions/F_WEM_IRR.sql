
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_WEM_IRR" (A_WORK_ORDER_ID number,
                                     A_DOCUMENT_ID   number,
                                     A_TAB_INDICATOR number) return number is
   IRR number;
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: WEM_IRR
   || Description:
   ||============================================================================
   || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By      Reason for Change
   || ------- ---------- --------------  -----------------------------------------
   || 1.0     12/04/2009 Chris Mardis    Create
   ||         12/21/2010 David Brumbeloe Update to handle where justifications do
   ||                                    not track revisions
   ||============================================================================
   */
   type AMOUNT_TABLE is table of number(22, 2) index by binary_integer;
   V_AMOUNT_TAB AMOUNT_TABLE;

   V_EST_TOTAL      number := 0;
   V_INEG           number := 0;
   V_IPOS           number := 0;
   V_IRR            number;
   V_NPV            number;
   V_INDEX          number;
   X                number;
   I                number;
   L                number;
   V_LAST_WAS_NEG   boolean := false;
   V_LAST_WAS_POS   boolean := false;
   V_ERROR_LOCATION varchar2(128);

begin
   V_ERROR_LOCATION := 'First select';
   select sum(DECODE(SIGN(AMOUNT), -1, 1, 0)) INEG,
          sum(DECODE(SIGN(AMOUNT), 1, 1, 0)) IPOS,
          sum(AMOUNT) TOTAL
     into V_INEG, V_IPOS, V_EST_TOTAL
     from (select CASH.TOTAL_BENEFIT -
                  sum(DECODE(WEM.EXPENDITURE_TYPE_ID, 1, WEM.TOTAL, 2, WEM.TOTAL, 0)) -
                  sum(DECODE(WEM.EXPENDITURE_TYPE_ID, 3, WEM.TOTAL, 0)) AMOUNT
             from WORK_ORDER_CASHFLOW CASH,
                  (select WEM.WORK_ORDER_ID,
                          WEM.REVISION,
                          WEM.YEAR,
                          WEM.EXPENDITURE_TYPE_ID,
                          WEM.TOTAL
                     from WO_EST_MONTHLY_FY WEM, ESTIMATE_CHARGE_TYPE ECT
                    where WEM.WORK_ORDER_ID = A_WORK_ORDER_ID
                      and WEM.REVISION =
                          (
                           /* BAT 02/02/2011 update */
                           select DECODE(WOD.EST_REVISION,
                                          0,
                                          NVL((select max(BVFP.REVISION)
                                                from BUDGET_VERSION_FUND_PROJ BVFP,
                                                     BUDGET_VERSION_VIEW      BVV
                                               where BVV.COMPANY_ID = WOC.COMPANY_ID
                                                 and BVFP.BUDGET_VERSION_ID = BVV.FORECAST_VERSION
                                                 and BVFP.WORK_ORDER_ID = WOC.WORK_ORDER_ID
                                                 and BVFP.ACTIVE = 1),
                                              (select max(A.REVISION)
                                                 from WORK_ORDER_APPROVAL A
                                                where A.WORK_ORDER_ID = WOC.WORK_ORDER_ID)),
                                          WOD.EST_REVISION) WOD_REV
                             from WO_DOCUMENTATION WOD, WORK_ORDER_CONTROL WOC
                            where WOD.WORK_ORDER_ID = WOC.WORK_ORDER_ID
                              and WOD.WORK_ORDER_ID = A_WORK_ORDER_ID
                              and WOD.DOCUMENT_ID = A_DOCUMENT_ID
                              and WOD.TAB_INDICATOR = A_TAB_INDICATOR)
                      and WEM.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
                      and ECT.PROCESSING_TYPE_ID not in (1, 5)
                   union all
                   select WEM.BUDGET_ID           WORK_ORDER_ID,
                          WEM.BUDGET_VERSION_ID   REVISION,
                          WEM.YEAR,
                          WEM.EXPENDITURE_TYPE_ID,
                          WEM.TOTAL
                     from BUDGET_MONTHLY_DATA_FY WEM, ESTIMATE_CHARGE_TYPE ECT
                    where WEM.BUDGET_ID = A_WORK_ORDER_ID
                      and WEM.BUDGET_VERSION_ID =
                          (
                           /* dkb 12/21/2010 update */
                           select DECODE(WOD.EST_REVISION,
                                          0,
                                          NVL((select max(A.BUDGET_VERSION_ID)
                                                from BUDGET_AMOUNTS A, BUDGET_VERSION_VIEW BVV
                                               where BVV.COMPANY_ID = B.COMPANY_ID
                                                 and A.BUDGET_VERSION_ID = BVV.FORECAST_VERSION
                                                 and A.BUDGET_ID = B.BUDGET_ID),
                                              (select max(A.BUDGET_VERSION_ID)
                                                 from BUDGET_AMOUNTS A
                                                where A.BUDGET_ID = B.BUDGET_ID)),
                                          WOD.EST_REVISION)
                             from WO_DOCUMENTATION WOD, BUDGET B
                            where WOD.WORK_ORDER_ID = B.BUDGET_ID
                              and WOD.WORK_ORDER_ID = A_WORK_ORDER_ID
                              and WOD.DOCUMENT_ID = A_DOCUMENT_ID
                              and WOD.TAB_INDICATOR = A_TAB_INDICATOR)
                      and WEM.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
                      and ECT.PROCESSING_TYPE_ID not in (1, 5)) WEM
            where CASH.WORK_ORDER_ID = A_WORK_ORDER_ID
              and CASH.DOCUMENT_ID = A_DOCUMENT_ID
              and CASH.WORK_ORDER_ID = WEM.WORK_ORDER_ID(+)
              and CASH.YEAR = WEM.YEAR(+)
            group by CASH.YEAR, CASH.TOTAL_BENEFIT
            order by CASH.YEAR);

   if V_EST_TOTAL is null then
      raise NO_DATA_FOUND;
   else
      if V_INEG = 0 or V_IPOS = 0 then
         V_IRR := 0;
         return(V_IRR);
      end if;

      --Initialize and fill array of amounts
      V_ERROR_LOCATION := 'Initialize and fill array of amounts';
      V_INDEX          := 0;
      for CSR_AMOUNT in (select CASH.TOTAL_BENEFIT -
                                sum(DECODE(WEM.EXPENDITURE_TYPE_ID, 1, WEM.TOTAL, 2, WEM.TOTAL, 0)) -
                                sum(DECODE(WEM.EXPENDITURE_TYPE_ID, 3, WEM.TOTAL, 0)) AMOUNT
                           from WORK_ORDER_CASHFLOW CASH,
                                (select WEM.WORK_ORDER_ID,
                                        WEM.REVISION,
                                        WEM.YEAR,
                                        WEM.EXPENDITURE_TYPE_ID,
                                        WEM.TOTAL
                                   from WO_EST_MONTHLY_FY WEM, ESTIMATE_CHARGE_TYPE ECT
                                  where WEM.WORK_ORDER_ID = A_WORK_ORDER_ID
                                    and WEM.REVISION =
                                        (
                                         /* BAT 02/02/2011 update */
                                         select DECODE(WOD.EST_REVISION,
                                                        0,
                                                        NVL((select max(BVFP.REVISION)
                                                              from BUDGET_VERSION_FUND_PROJ BVFP,
                                                                   BUDGET_VERSION_VIEW      BVV
                                                             where BVV.COMPANY_ID = WOC.COMPANY_ID
                                                               and BVFP.BUDGET_VERSION_ID =
                                                                   BVV.FORECAST_VERSION
                                                               and BVFP.WORK_ORDER_ID = WOC.WORK_ORDER_ID
                                                               and BVFP.ACTIVE = 1),
                                                            (select max(A.REVISION)
                                                               from WORK_ORDER_APPROVAL A
                                                              where A.WORK_ORDER_ID = WOC.WORK_ORDER_ID)),
                                                        WOD.EST_REVISION) WOD_REV
                                           from WO_DOCUMENTATION WOD, WORK_ORDER_CONTROL WOC
                                          where WOD.WORK_ORDER_ID = WOC.WORK_ORDER_ID
                                            and WOD.WORK_ORDER_ID = A_WORK_ORDER_ID
                                            and WOD.DOCUMENT_ID = A_DOCUMENT_ID
                                            and WOD.TAB_INDICATOR = A_TAB_INDICATOR)
                                    and WEM.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
                                    and ECT.PROCESSING_TYPE_ID not in (1, 5)
                                 union all
                                 select WEM.BUDGET_ID           WORK_ORDER_ID,
                                        WEM.BUDGET_VERSION_ID   REVISION,
                                        WEM.YEAR,
                                        WEM.EXPENDITURE_TYPE_ID,
                                        WEM.TOTAL
                                   from BUDGET_MONTHLY_DATA_FY WEM, ESTIMATE_CHARGE_TYPE ECT
                                  where WEM.BUDGET_ID = A_WORK_ORDER_ID
                                    and WEM.BUDGET_VERSION_ID =
                                        (
                                         /* dkb 12/21/2010 update */
                                         select DECODE(WOD.EST_REVISION,
                                                        0,
                                                        NVL((select max(A.BUDGET_VERSION_ID)
                                                              from BUDGET_AMOUNTS      A,
                                                                   BUDGET_VERSION_VIEW BVV
                                                             where BVV.COMPANY_ID = B.COMPANY_ID
                                                               and A.BUDGET_VERSION_ID =
                                                                   BVV.FORECAST_VERSION
                                                               and A.BUDGET_ID = B.BUDGET_ID),
                                                            (select max(A.BUDGET_VERSION_ID)
                                                               from BUDGET_AMOUNTS A
                                                              where A.BUDGET_ID = B.BUDGET_ID)),
                                                        WOD.EST_REVISION)
                                           from WO_DOCUMENTATION WOD, BUDGET B
                                          where WOD.WORK_ORDER_ID = B.BUDGET_ID
                                            and WOD.WORK_ORDER_ID = A_WORK_ORDER_ID
                                            and WOD.DOCUMENT_ID = A_DOCUMENT_ID
                                            and WOD.TAB_INDICATOR = A_TAB_INDICATOR)
                                    and WEM.EST_CHG_TYPE_ID = ECT.EST_CHG_TYPE_ID
                                    and ECT.PROCESSING_TYPE_ID not in (1, 5)) WEM
                          where CASH.WORK_ORDER_ID = A_WORK_ORDER_ID
                            and CASH.DOCUMENT_ID = A_DOCUMENT_ID
                            and CASH.WORK_ORDER_ID = WEM.WORK_ORDER_ID(+)
                            and CASH.YEAR = WEM.YEAR(+)
                          group by CASH.YEAR, CASH.TOTAL_BENEFIT
                          order by CASH.YEAR)
      loop
         V_INDEX := V_INDEX + 1;
         V_AMOUNT_TAB(V_INDEX) := CSR_AMOUNT.AMOUNT;
      end loop;

      --Calculate IRR greater than 0
      if V_EST_TOTAL > 0 then
         V_ERROR_LOCATION := 'Calculate IRR greater than 0';
         V_IRR            := 0.15;
         X                := 0.15;
         for I in 1 .. 1000
         loop
            V_NPV := 0;
            for J in 1 .. V_AMOUNT_TAB.COUNT()
            loop
               V_NPV := V_NPV + V_AMOUNT_TAB(J) / POWER((1 + V_IRR), (J - 1));
            end loop;
            /* DBMS_OUTPUT.PUT_LINE(ROUND(V_IRR,4) || ' - ' || ROUND(X,8) || ' - ' || ROUND(V_NPV,4)); */

            if V_NPV >= 0 and V_NPV < 0.01 then
               return V_IRR;
            elsif V_NPV > 0 then
               if V_LAST_WAS_NEG = false then
                  V_IRR := V_IRR + X;
               else
                  X     := X / 2;
                  V_IRR := V_IRR + X;
                  /* X     := X - 0.0000001; */
               end if;
               V_LAST_WAS_NEG := false;
            elsif V_NPV < 0 then
               if V_LAST_WAS_NEG = true then
                  V_IRR := V_IRR - X;
               else
                  X     := X / 2;
                  V_IRR := V_IRR - X;
               end if;
               V_LAST_WAS_NEG := true;
            end if;

         end loop;
         --Calculate IRR less than or equal to 0
      else
         V_ERROR_LOCATION := 'Calculate IRR less than or equal to 0';
         V_IRR            := 0.15;
         X                := 0.15;
         for I in 1 .. 1000
         loop
            V_NPV := 0;
            for J in 1 .. V_AMOUNT_TAB.COUNT()
            loop
               V_NPV := V_NPV + V_AMOUNT_TAB(J) / POWER((1 + V_IRR), (J - 1));
            end loop;
            /* DBMS_OUTPUT.PUT_LINE(ROUND(V_IRR,4) || ' - ' || ROUND(X,8) || ' - ' || ROUND(V_NPV,4)); */

            if V_NPV <= 0 and V_NPV > -0.01 then
               return V_IRR;
            elsif V_NPV < 0 then
               if V_LAST_WAS_POS = false then
                  V_IRR := V_IRR - X;
               else
                  X     := X / 2;
                  V_IRR := V_IRR - X;
                  /* X     := X + 0.0000001; */
               end if;
               V_LAST_WAS_POS := false;
            elsif V_NPV > 0 then
               if V_LAST_WAS_POS = true then
                  V_IRR := V_IRR + X;
               else
                  X     := X / 2;
                  V_IRR := V_IRR + X;
               end if;
               V_LAST_WAS_POS := true;
            end if;
            L := I;
         end loop;
      end if;
   end if;

   --We didn't find a valid IRR
   V_IRR := 0;
   return(V_IRR);

exception
   when others then
      /* RAISE_APPLICATION_ERROR(-20001,
      'WEM_IRR Function Error Loaction: ' || V_ERROR_LOCATION ||
      ' - Error Message: ' || sqlerrm); */
      return(-3.14159265358);
end F_WEM_IRR;
 
 
 
 
/