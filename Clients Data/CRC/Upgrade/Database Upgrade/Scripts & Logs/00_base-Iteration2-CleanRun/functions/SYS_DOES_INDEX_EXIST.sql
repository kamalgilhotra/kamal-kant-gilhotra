
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."SYS_DOES_INDEX_EXIST" (indexName  varchar2,
                                                         indexOwner varchar2)
  RETURN NUMBER is
  index_exists number := 0;

begin

  select count(*)
    into index_exists
    from ALL_INDEXES
   where INDEX_NAME = upper(indexName)
     and owner = upper(indexOwner);

  return index_exists;

end;
 
 
/