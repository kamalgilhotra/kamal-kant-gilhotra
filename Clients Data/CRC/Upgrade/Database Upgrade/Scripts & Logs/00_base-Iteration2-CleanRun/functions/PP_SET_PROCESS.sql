
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."PP_SET_PROCESS" (A_ARG varchar2, A_ARG2 varchar2) return varchar2 as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PP_SET_PROCESS
   || Description:
   ||============================================================================
   || Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.1                               Added a check for null argument
   || 1.1   08/29/2013 C Shilling       maint_31807 - Remove code - call package instead
   ||============================================================================
   */
begin
   return PP_PROCESS_PKG.PP_SET_PROCESS(A_ARG, A_ARG2);
end;
 
 
/