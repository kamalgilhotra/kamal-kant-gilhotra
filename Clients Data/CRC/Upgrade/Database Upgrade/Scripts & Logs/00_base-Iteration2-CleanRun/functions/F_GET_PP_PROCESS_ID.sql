
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_GET_PP_PROCESS_ID" (a_process_description pp_processes.DESCRIPTION%TYPE) RETURN pp_processes.process_id%TYPE
RESULT_CACHE RELIES_ON (pp_processes)
IS
  /*****************************************************************************
  * Function: f_get_pp_process_id
  * 
  * PARAMETERS: The description of the process in pp_processes
  * RETURNS: The process_id of the process
  ******************************************************************************/
  l_id pp_processes.process_id%type;
BEGIN
  SELECT process_id INTO l_id
  FROM pp_processes
  WHERE UPPER(TRIM(DESCRIPTION)) = UPPER(TRIM(a_process_description));

  IF l_id IS NULL THEN
    raise_application_error(-20000, 'Invalid process description passed to f_get_pp_process_id' || chr(10) || f_get_call_stack);
  END IF;

  return l_id;
end;
/