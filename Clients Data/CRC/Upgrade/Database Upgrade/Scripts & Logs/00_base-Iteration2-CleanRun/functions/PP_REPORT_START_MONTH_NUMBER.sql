
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."PP_REPORT_START_MONTH_NUMBER" return number is
   START_MONTH number;
begin
   select TO_NUMBER(TO_CHAR(START_MONTH, 'yyyymm'))
     into START_MONTH
     from REPORT_TIME
    where SESSION_ID = USERENV('sessionid')
      and LOWER(USER_ID) = LOWER(user);

   return(START_MONTH);
end;
 
 
 
 
 
 
/