
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_CLIENT_REIMB_BILL_AUDIT" (A_BILL_NUMBER_ID number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to F_REIMB_BILL_AUDIT
   return 1;
end;
 
 
/