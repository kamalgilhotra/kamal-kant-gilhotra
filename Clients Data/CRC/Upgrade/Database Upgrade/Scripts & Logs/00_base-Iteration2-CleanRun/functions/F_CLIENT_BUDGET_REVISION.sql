
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_CLIENT_BUDGET_REVISION" (A_FUNCTION varchar2, A_CALL number, A_WO_FP varchar2, A_CALL_VAR_NUM DBMS_SQL.Number_Table, A_CALL_VAR_STR DBMS_SQL.Varchar2_Table, A_CST_RTN number) return number is

begin

  return 1;

exception
  when others then rollback;

end F_CLIENT_BUDGET_REVISION;

 
 
/