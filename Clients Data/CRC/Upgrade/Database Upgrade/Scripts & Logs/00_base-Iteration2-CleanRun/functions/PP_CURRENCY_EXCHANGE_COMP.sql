
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."PP_CURRENCY_EXCHANGE_COMP" (from_comp    NUMBER,
                                                     to_curr      VARCHAR2,
                                                     month_number NUMBER,
                                                     rate_type    NUMBER)
   RETURN NUMBER IS
   exchange_rate NUMBER(22,
                        8);

BEGIN

   SELECT round(a.rate / b.rate,
                8)
     INTO exchange_rate
     FROM currency_rate   a,
          currency_rate   b,
          currency        aa,
          currency        bb,
          currency_schema cs
    WHERE cs.company_id = from_comp
      AND cs.currency_id = a.currency_from
      AND cs.currency_type_id = 1
      AND bb.description = to_curr
      AND a.currency_from = aa.currency_id
      AND b.currency_from = bb.currency_id
      AND a.currency_to = b.currency_to
      AND to_number(to_char(a.exchange_date,
                            'yyyymm')) = month_number
      AND to_number(to_char(b.exchange_date,
                            'yyyymm')) = month_number
      AND a.exchange_rate_type_id = rate_type
      AND b.exchange_rate_type_id = rate_type;
   RETURN(exchange_rate);
END;
 
 
 
 
 
 
/