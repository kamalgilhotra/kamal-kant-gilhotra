
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."CR_COMBOS_BDG_GET_RULE_ID" (A_ACT_RULE_ID number) return number is
   L_BDG_RULE_ID number;
begin

   L_BDG_RULE_ID := 0;
   select RULE_ID
     into L_BDG_RULE_ID
     from CR_VALIDATION_RULES_BDG
    where ACTUALS_RULE_ID = A_ACT_RULE_ID;

   return L_BDG_RULE_ID;

exception
   when NO_DATA_FOUND then
      return -2;
   when others then
      return('ERROR SQL: ' || sqlerrm);
end;
 
 
 
 
 
 
/