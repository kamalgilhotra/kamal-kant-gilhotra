
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."PP_REPORT_END_MONTH" return date is
   END_MONTH date;
begin
   select END_MONTH
     into END_MONTH
     from REPORT_TIME
    where SESSION_ID = USERENV('sessionid')
      and LOWER(USER_ID) = LOWER(user);

   return(END_MONTH);
end;
 
 
 
 
 
 
/