
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."CURR_CO" (a_co_id NUMBER) RETURN NUMBER IS
   curr_id NUMBER(22,
                  8);
   counter NUMBER(22,
                  0);
BEGIN

   SELECT COUNT(*)
     INTO counter
     FROM currency_schema
    WHERE company_id = a_co_id;

   IF counter = 0
   THEN
      RETURN 0;
   ELSE
      SELECT currency_id
        INTO curr_id
        FROM currency_schema
       WHERE company_id = a_co_id;

      RETURN curr_id;
   END IF;
END;
 
 
 
 
 
 
/