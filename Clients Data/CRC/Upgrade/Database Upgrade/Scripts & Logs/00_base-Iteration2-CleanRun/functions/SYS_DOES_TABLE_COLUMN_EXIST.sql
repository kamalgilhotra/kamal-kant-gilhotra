
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."SYS_DOES_TABLE_COLUMN_EXIST" (tableColumn VARCHAR2,
                                                                tableName   VARCHAR2,
                                                                tableOwner  VARCHAR2)
  RETURN NUMBER IS
  table_column_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*)
    INTO table_column_exists
    FROM ALL_TAB_COLUMNS
   WHERE COLUMN_NAME = upper(tableColumn)
     AND TABLE_NAME = upper(tableName)
     AND OWNER = upper(tableOwner);

  RETURN table_column_exists;
END;
 
 
/