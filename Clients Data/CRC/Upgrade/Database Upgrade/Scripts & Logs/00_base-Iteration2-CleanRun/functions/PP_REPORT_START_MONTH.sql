
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."PP_REPORT_START_MONTH" return date is
   START_MONTH date;
begin
   select START_MONTH
     into START_MONTH
     from REPORT_TIME
    where SESSION_ID = USERENV('sessionid')
      and LOWER(USER_ID) = LOWER(user);

   return(START_MONTH);
end;
 
 
 
 
 
 
/