
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."IS_DATE" (char_in VARCHAR2, format_in VARCHAR2)
   RETURN NUMBER IS
   n DATE;
BEGIN
   n := to_date(char_in,
                format_in);
   RETURN 1;
EXCEPTION
   WHEN OTHERS THEN
      RETURN - 1;
END;
 
 
 
 
 
 
/