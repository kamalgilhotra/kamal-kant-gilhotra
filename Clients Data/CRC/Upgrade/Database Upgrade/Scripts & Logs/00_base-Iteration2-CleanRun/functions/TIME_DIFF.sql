
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."TIME_DIFF" (DATE_1 in date,
                                     DATE_2 in date) return number is

   /*
   ||============================================================================
   || Application: PowerPlan
   || Object Name: TIME_DIFF
   || Description:
   ||============================================================================
   || Copyright (C) 2012 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- -----------------------------------------
   || 10.3.6.0 10/29/2012 Joseph King    New
   ||============================================================================
   */

   NDATE_1   number;
   NDATE_2   number;
   NSECOND_1 number(5, 0);
   NSECOND_2 number(5, 0);

begin
   -- Get Julian date number from first date (DATE_1)
   NDATE_1 := TO_NUMBER(TO_CHAR(DATE_1, 'J'));

   -- Get Julian date number from second date (DATE_2)
   NDATE_2 := TO_NUMBER(TO_CHAR(DATE_2, 'J'));

   -- Get seconds since midnight from first date (DATE_1)
   NSECOND_1 := TO_NUMBER(TO_CHAR(DATE_1, 'SSSSS'));

   -- Get seconds since midnight from second date (DATE_2)
   NSECOND_2 := TO_NUMBER(TO_CHAR(DATE_2, 'SSSSS'));

   return(((NDATE_2 - NDATE_1) * 86400) + (NSECOND_2 - NSECOND_1));
end TIME_DIFF;
 
 
/