
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_GET_CALL_STACK" RETURN VARCHAR2 IS
  /*****************************************************************************
  * Function: f_get_call_stack
  * PURPOSE: Returns the call stack at the current time
  * NOTE: This uses a different implementation for versions of Oracle starting with 12,
  *       in order to provide more information (esp. function names inside packages,
  *                                               instead of line numbers
  *
  * RETURNS: Call stack
  ******************************************************************************/
  call_stack VARCHAR2(32767);
  l_depth pls_integer;
BEGIN
  $IF dbms_db_version.VERSION > 11 $THEN
    --https://oracle-base.com/articles/12c/utl-call-stack-12cr1#call-stack
    l_depth := utl_call_stack.dynamic_depth;
    call_stack := '***** Call Stack *****' || CHR(10);
    call_stack := call_stack || 'Depth     Lexical   Line      Owner     Edition   Name' || CHR(10);
    call_stack := call_stack || '.         Depth     Number' || CHR(10);
    call_stack := call_stack || '--------- --------- --------- --------- --------- --------------------';

    FOR I IN 1 .. l_depth LOOP
      call_stack := call_stack || CHR(10);
      call_stack := call_stack || rpad(I, 10) ||
                                  RPAD(UTL_CALL_STACK.lexical_depth(i), 10) ||
                                  RPAD(TO_CHAR(UTL_CALL_STACK.unit_line(i),'99'), 10) ||
                                  RPAD(NVL(UTL_CALL_STACK.owner(i),' '), 10) ||
                                  RPAD(NVL(UTL_CALL_STACK.current_edition(i),' '), 10) ||
                                  UTL_CALL_STACK.concatenate_subprogram(UTL_CALL_STACK.subprogram(i));
    END LOOP;
  $ELSE
    call_stack := dbms_utility.format_call_stack;
  $END
  RETURN call_stack;
END;
/