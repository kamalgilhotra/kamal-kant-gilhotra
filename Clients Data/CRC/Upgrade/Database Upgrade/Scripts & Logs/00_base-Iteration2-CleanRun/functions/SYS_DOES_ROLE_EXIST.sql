
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."SYS_DOES_ROLE_EXIST" (roleName varchar2)
  RETURN NUMBER is
  role_exists number := 0;

begin
  select count(*)
    into role_exists
    from DBA_ROLES
   where Role = upper(roleName);

  return role_exists;

end;
 
 
/