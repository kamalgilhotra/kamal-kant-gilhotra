
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_CLIENT_PRE_APPROVE_DEPR_CO" (A_COMPANY_ID number,
                                             A_MONTH      date) return varchar2 is
-- *********************************************
-- In order to utilize an extension function.
-- The following table MUST be populated
-- pp_client_extensions:
--    id = A unique number
--    function_name = the name of the oracle function being implemented
--    is_active = 1 (Can be inactivated later by setting to 0)
-- *********************************************
begin
   --maps to PB function F_PRE_APPROVE_DEPR_CO_CUSTOM
   return '';
end F_CLIENT_PRE_APPROVE_DEPR_CO;
 
 
/