
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."GET_PEND_SOB_GL_REVERSAL" (P_LDG_ASSET_ID      PEND_TRANSACTION.LDG_ASSET_ID%type,
                                                    P_SET_OF_BOOKS_ID   SET_OF_BOOKS.SET_OF_BOOKS_ID%type,
                                                    P_ACTIVITY_CODE     PEND_TRANSACTION.ACTIVITY_CODE%type,
                                                    P_WORK_ORDER_NUMBER PEND_TRANSACTION.WORK_ORDER_NUMBER%type,
                                                    P_COMPANY_ID        PEND_TRANSACTION.COMPANY_ID%type)
   return PEND_TRANSACTION.GAIN_LOSS_REVERSAL%type is

   V_ACTIVITY_CODE      PEND_TRANSACTION.ACTIVITY_CODE%type;
   V_GAIN_LOSS_REVERSAL PEND_TRANSACTION.GAIN_LOSS_REVERSAL%type;
   V_WORK_ORDER_ID      WORK_ORDER_CONTROL.WORK_ORDER_ID%type;
   V_SUBLEDGER_INDICATOR CPR_LEDGER.SUBLEDGER_INDICATOR%type;

begin
   V_ACTIVITY_CODE := trim(UPPER(P_ACTIVITY_CODE));

   if V_ACTIVITY_CODE <> 'URGL' and V_ACTIVITY_CODE <> 'SAGL' then
      return 0;
   end if;

   begin
      select WORK_ORDER_ID
        into V_WORK_ORDER_ID
        from WORK_ORDER_CONTROL
       where WORK_ORDER_NUMBER = P_WORK_ORDER_NUMBER
         and COMPANY_ID = P_COMPANY_ID
         and FUNDING_WO_INDICATOR = 0;

      select NVL(-SUM(ACCR_GAIN_LOSS), 0)
        into V_GAIN_LOSS_REVERSAL
        from WO_ACCRUED_GAIN_LOSS
       where ASSET_ID = P_LDG_ASSET_ID
         and SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
         and WORK_ORDER_ID = V_WORK_ORDER_ID;
   exception
      when NO_DATA_FOUND then
         return 0;
   end;

   return V_GAIN_LOSS_REVERSAL;
end;
 
 
/