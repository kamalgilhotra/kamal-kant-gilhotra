
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."SYS_DOES_TRIGGER_EXIST" (objectName  VARCHAR2,
                                                          objectOwner VARCHAR2)
  RETURN NUMBER IS
  object_exists NUMBER := 0;
BEGIN
  --This Does Not Confirm Object Type.  Just that the name exists.
  SELECT COUNT(*)
    INTO object_exists
    FROM ALL_TRIGGERS
   WHERE TRIGGER_NAME = upper(objectName)
     AND OWNER = upper(objectOwner);

  RETURN object_exists;
END;
 
/