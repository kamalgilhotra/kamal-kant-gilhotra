
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."DELETE_TABLE" (TABLE_NAME_STR varchar,
                                        OWNER_NAME     varchar,
                                        WHERE_CLAUSE   varchar,
                                        ERR_MSG        out varchar) return integer as
/*
||============================================================================
|| Application: PowerPlant
|| Function Name: DELETE_TABLE
|| Description:
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------  ---------- -------------- ----------------------------------------
||  1.1     01/01/2009                Changed code to enable constraints after
||                                    an error in deleting rows.
||============================================================================
*/

REF_TABLES_CONS varchar(2048);
TABLE_NAME_CONS varchar(2048);

cursor TAB_CUR is
   select LOWER(C.CONSTRAINT_NAME), LOWER(C2.CONSTRAINT_NAME), C2.TABLE_NAME
     from ALL_CONSTRAINTS C, ALL_CONSTRAINTS C2
    where C.CONSTRAINT_NAME = C2.R_CONSTRAINT_NAME
      and C.OWNER = OWNER_NAME
      and C.TABLE_NAME = UPPER(TABLE_NAME_STR)
      and C.CONSTRAINT_TYPE = 'P';

COUNT2       int;
COL_COUNT    int;
I            int;
J            int;
NUM          int;
REF_TABLE    varchar(2048);
CODE         int;
CONS_NAME    varchar(2048);
TO_CONS_NAME varchar(2048);
COLUMN_STR   varchar(2048);

SQLS         varchar(2048);
USER_CURSOR  integer;

type COL_LIST is table of varchar(40) index by binary_integer;
type TABLE_STRUCT_REC is record(name varchar2(40),
                                CONS varchar2(40));
type TABLE_LIST_S is table of TABLE_STRUCT_REC index by binary_integer;
TABLE_LIST TABLE_LIST_S;
REF_TABLES TABLE_LIST_S;

begin

   select count(*)
     into COUNT2
     from ALL_TABLES
    where OWNER = UPPER(OWNER_NAME)
      and TABLE_NAME = UPPER(TABLE_NAME);

   COUNT2 := 0;
   open TAB_CUR;

   loop
      fetch TAB_CUR
         into CONS_NAME, TO_CONS_NAME, REF_TABLE;
      if (TAB_CUR%notfound) then
         exit;
      end if;
      COUNT2 := COUNT2 + 1;
      REF_TABLES(COUNT2).CONS := TO_CONS_NAME;
      REF_TABLES(COUNT2).name := REF_TABLE;
      TABLE_LIST(COUNT2).CONS := CONS_NAME;
   end loop;

   close TAB_CUR;

   for I in 1 .. COUNT2
   loop
      SQLS        := 'alter table ' || OWNER_NAME || '.' || REF_TABLES(I)
                    .name || ' disable constraint ' || REF_TABLES(I).CONS;
      USER_CURSOR := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE(USER_CURSOR, SQLS, DBMS_SQL.V7);
      NUM := DBMS_SQL.execute(USER_CURSOR);
      DBMS_SQL.CLOSE_CURSOR(USER_CURSOR);
   end loop;

   SQLS        := 'delete from ' || OWNER_NAME || '.' || TABLE_NAME_STR || ' where ' ||
                  WHERE_CLAUSE;
   USER_CURSOR := DBMS_SQL.OPEN_CURSOR;
   DBMS_SQL.PARSE(USER_CURSOR, SQLS, DBMS_SQL.V7);
   NUM  := DBMS_SQL.execute(USER_CURSOR);
   CODE := sqlcode;
   DBMS_SQL.CLOSE_CURSOR(USER_CURSOR);

   for I in 1 .. COUNT2
   loop
      SQLS        := 'alter table ' || OWNER_NAME || '.' || REF_TABLES(I).name ||
                     ' enable constraint ' || REF_TABLES(I).CONS;
      USER_CURSOR := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE(USER_CURSOR, SQLS, DBMS_SQL.V7);
      NUM := DBMS_SQL.execute(USER_CURSOR);
      DBMS_SQL.CLOSE_CURSOR(USER_CURSOR);
   end loop;

   if (CODE <> 0) then
      -- code := sqlcode;
      ERR_MSG := 'code = ' || TO_CHAR(CODE) || ' ' || SQLS;
      return - 1;
   end if;

   return 0;
exception
   --  Enter the code to handle exception conditions
   --  following this line
   when others then
      CODE := sqlcode;
      return CODE;
end;
 
 
 
 
 
 
/