
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."ADD_DATE_FY" (a_mo_yr DATE) RETURN DATE IS
   fy_mo_yr DATE;

BEGIN
   SELECT to_date(to_number(fiscal_year || lpad(fiscal_month,
                                                2,
                                                '0')),
                  'yyyymm')
     INTO fy_mo_yr
     FROM pp_calendar
    WHERE MONTH = a_mo_yr;

   RETURN fy_mo_yr;

EXCEPTION
   WHEN no_data_found THEN
      RETURN a_mo_yr;
END;
 
 
 
 
 
 
/