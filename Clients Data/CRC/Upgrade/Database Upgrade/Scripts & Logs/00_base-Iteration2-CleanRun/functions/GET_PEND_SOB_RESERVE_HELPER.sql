
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."GET_PEND_SOB_RESERVE_HELPER" (P_PEND_TRANS_ID      PEND_TRANSACTION.PEND_TRANS_ID%type,
														P_LDG_ASSET_ID       number,
														P_SET_OF_BOOKS_ID    number,
														P_GL_POSTING_MO_YR   date,
														P_SOB_ACTIVITY_CODE  varchar2,
														P_SOB_POSTING_AMOUNT number,
														P_POSTING_QUANTITY   PEND_TRANSACTION.POSTING_QUANTITY%type,
														P_COMPANY_ID		 PEND_TRANSACTION.COMPANY_ID%type)
	return number is

	V_ASSET_ID      		CPR_LEDGER.ASSET_ID%type;
	V_ACTIVITY_CODE 		PEND_TRANSACTION.ACTIVITY_CODE%type;
	V_FROM_COMPANY_ID		PEND_TRANSACTION.COMPANY_ID%type;
	V_FROM_POSTING_AMOUNT	PEND_TRANSACTION.POSTING_AMOUNT%type;
	V_FROM_RESERVE			PEND_TRANSACTION.RESERVE%type;
	V_COUNT					integer;

begin

	V_ACTIVITY_CODE := trim(UPPER(P_SOB_ACTIVITY_CODE));

	-- Our reserve will always be 0 except when processing a gain_loss or transfer
	if V_ACTIVITY_CODE <> 'URGL' and V_ACTIVITY_CODE <> 'SAGL' and V_ACTIVITY_CODE <> 'UTRF' and V_ACTIVITY_CODE <> 'UTRT' then
		return 0;
	end if;

	-- get asset id
	if V_ACTIVITY_CODE = 'URGL' or V_ACTIVITY_CODE = 'SAGL' or V_ACTIVITY_CODE = 'UTRF' then
		V_ASSET_ID := P_LDG_ASSET_ID;
	else
		-- UTRTs

		--get necessary info from linked UTRF
		begin
			select UTRF.LDG_ASSET_ID, UTRF.COMPANY_ID, UTRF.POSTING_AMOUNT, UTRF.RESERVE
			into V_ASSET_ID, V_FROM_COMPANY_ID, V_FROM_POSTING_AMOUNT, V_FROM_RESERVE
			from PEND_TRANSACTION UTRT, PEND_TRANSACTION UTRF
			where UTRF.PEND_TRANS_ID = UTRT.LDG_ASSET_ID
			and UTRT.PEND_TRANS_ID = P_PEND_TRANS_ID;
		exception
			when NO_DATA_FOUND then
				RAISE_APPLICATION_ERROR(-20005,
										'Could not find ldg_asset_id on UTRF ' ||
										'side of transfer for UTRT. UTRT pend_trans_id: ' ||
										P_PEND_TRANS_ID);
		end;

		--if this is an intercompany transfer, make sure the SOB exists on the FROM side.
		if P_COMPANY_ID <> V_FROM_COMPANY_ID then
			select count(1)
			into V_COUNT
			from COMPANY_SET_OF_BOOKS CSOB
			where CSOB.COMPANY_ID = V_FROM_COMPANY_ID
			and CSOB.SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID;

			if V_COUNT = 0 then
				--the FROM company is missing this set of books. Calc reserve based ratio.
				if V_FROM_POSTING_AMOUNT = 0 then
					return 0;
				else
					return V_FROM_RESERVE * (P_SOB_POSTING_AMOUNT / V_FROM_POSTING_AMOUNT);
				end if;
			end if;
		end if;
	end if;

	return GET_PEND_SOB_RESERVE(V_ASSET_ID,
								P_SET_OF_BOOKS_ID,
								P_GL_POSTING_MO_YR,
								P_SOB_ACTIVITY_CODE,
								P_SOB_POSTING_AMOUNT,
								P_POSTING_QUANTITY);
end;
 
/