
  CREATE OR REPLACE EDITIONABLE FUNCTION "PWRPLANT"."F_BV_TEMP_BUDGET" (REPORT_ID number) return varchar2 is
   /*******************************************************************************************
   //   Function    :   f_bv_temp_budget
   //
   //   Purpose     :   Populate temp_budget for reporting based on budget version arguments
   //
   //   Arguments   :   NUMBER report_id  - Identifier of the report being run
   //
   //   Return Codes:   STRING 'OK'       - success
   //                          'ERROR...' - failure
   //
   //   DATE        NAME   REVISION      CHANGES
   //   ----------  ----   -----------   -----------------------------------------------
   //   09-23-2011  CDM    Version 1.0   Initial Version
   //
   //   PROPRIETARY INFORMATION OF   POWERPLAN CONSULTANTS, INC. , ALL RIGHTS RESERVED
   *******************************************************************************************/

   NEG_1_EXISTS number;
   BV_EXISTS    number;
   MSG          varchar2(254);

begin

   MSG := 'Deleting from temp_budget';
   delete from TEMP_BUDGET;

   MSG := 'Checking for -1 records in temp_budget_version';
   select count(*) into NEG_1_EXISTS from TEMP_BUDGET_VERSION where BUDGET_VERSION_ID = -1;

   MSG := 'Checking for budget_version_ids in temp_budget_version';
   select count(*)
     into BV_EXISTS
     from TEMP_BUDGET_VERSION
    where BUDGET_VERSION_ID not in (-1, -8, -9);

   if BV_EXISTS > 0 then
      MSG := 'Inserting into temp_budget for bv_ids';
      insert into TEMP_BUDGET
         (USER_ID, SESSION_ID, BATCH_REPORT_ID, BUDGET_ID, BUDGET_VERSION_ID)
         select distinct user, USERENV('sessionid'), 0, BUDGET_ID, 0
           from BUDGET_AMOUNTS_SV
          where BUDGET_VERSION_ID in
                (select BUDGET_VERSION_ID
                   from TEMP_BUDGET_VERSION
                  where BUDGET_VERSION_ID not in (-1, -8, -9));
   elsif NEG_1_EXISTS > 0 then
      MSG := 'Inserting into temp_budget for -1';
      insert into TEMP_BUDGET
         (USER_ID, SESSION_ID, BATCH_REPORT_ID, BUDGET_ID, BUDGET_VERSION_ID)
         select distinct user, USERENV('sessionid'), 0, BUDGET_ID, 0 from BUDGET_SV;
   end if;

   return 'OK';

exception
   when others then
      rollback;
      return 'ERROR ' || MSG || ': ' || sqlerrm;

end F_BV_TEMP_BUDGET;
 
 
/