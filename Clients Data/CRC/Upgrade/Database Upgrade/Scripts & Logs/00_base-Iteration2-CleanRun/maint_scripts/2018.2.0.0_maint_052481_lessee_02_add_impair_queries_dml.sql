/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_52481_lessee_02_add_impair_queries_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.1 01/15/2019 Chris Trigg    Updating reports to include impairment information
||============================================================================
*/
Set Define off
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Forecast Lease Asset Schedule By MLA';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'select * from LS_FCST_ASSET_SCHED_BY_MLA_VW ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT  l_id, 'ls_forecast_version', 1, 0, 1, null, 'Forecast Version', 400, 'description', 'ls_forecast_version', 'VARCHAR2', 0, 'description', 1, 2, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_number', 2, 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number', 1, 2, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'conversion_date', 3, 0, 1, null, 'Conversion Date', 400, 'conversion_date', 'ls_forecast_version', 'VARCHAR2', 0, 'conversion_date', null, null, 'conversion_date', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ls_asset_id', 4, 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0, 'ls_asset_id', null, null, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'leased_asset_number', 5, 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset', 'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_id', 6, 0, 1, null, 'Company Id', 300, 'description', '(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_description', 7, 0, 1, null, 'Company Description', 300, 'description', '(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_id', 8, 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_number', 9, 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null, null, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_id', 10, 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_cap_type', 11, 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'location', 12, 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0, 'long_description', null, null, 'long_description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'revision', 13, 0, 1, null, 'Revision', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'set_of_books_id', 14, 0, 1, null, 'Set Of Books Id', 300, 'description', 'set_of_books', 'NUMBER', 0, 'set_of_books_id', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'monthnum', 15, 0, 1, null, 'Monthnum', 300, 'month_number', '(select month_number from pp_calendar)', 'VARCHAR2', 0, 'month_number', null, null, 'the_sort', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_capital_cost', 16, 1, 0, null, 'Beg Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_capital_cost', 17, 1, 0, null, 'End Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_obligation', 18, 1, 0, null, 'Beg Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_obligation', 19, 1, 0, null, 'End Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_obligation', 20, 1, 0, null, 'Beg Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_obligation', 21, 1, 0, null, 'End Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_accrual', 22, 1, 0, null, 'Interest Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_accrual', 23, 1, 0, null, 'Principal Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_paid', 24, 1, 0, null, 'Interest Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_paid', 25, 1, 0, null, 'Principal Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual1', 26, 1, 0, null, 'Executory Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual2', 27, 1, 0, null, 'Executory Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual3', 28, 1, 0, null, 'Executory Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual4', 29, 1, 0, null, 'Executory Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual5', 30, 1, 0, null, 'Executory Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual6', 31, 1, 0, null, 'Executory Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual7', 32, 1, 0, null, 'Executory Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual8', 33, 1, 0, null, 'Executory Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual9', 34, 1, 0, null, 'Executory Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual10', 35, 1, 0, null, 'Executory Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid1', 36, 1, 0, null, 'Executory Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid2', 37, 1, 0, null, 'Executory Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid3', 38, 1, 0, null, 'Executory Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid4', 39, 1, 0, null, 'Executory Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid5', 40, 1, 0, null, 'Executory Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid6', 41, 1, 0, null, 'Executory Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid7', 42, 1, 0, null, 'Executory Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid8', 43, 1, 0, null, 'Executory Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid9', 44, 1, 0, null, 'Executory Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid10', 45, 1, 0, null, 'Executory Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual1', 46, 1, 0, null, 'Contingent Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual2', 47, 1, 0, null, 'Contingent Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual3', 48, 1, 0, null, 'Contingent Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual4', 49, 1, 0, null, 'Contingent Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual5', 50, 1, 0, null, 'Contingent Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual6', 51, 1, 0, null, 'Contingent Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual7', 52, 1, 0, null, 'Contingent Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual8', 53, 1, 0, null, 'Contingent Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual9', 54, 1, 0, null, 'Contingent Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual10', 55, 1, 0, null, 'Contingent Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid1', 56, 1, 0, null, 'Contingent Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid2', 57, 1, 0, null, 'Contingent Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid3', 58, 1, 0, null, 'Contingent Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid4', 59, 1, 0, null, 'Contingent Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid5', 60, 1, 0, null, 'Contingent Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid6', 61, 1, 0, null, 'Contingent Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid7', 62, 1, 0, null, 'Contingent Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid8', 63, 1, 0, null, 'Contingent Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid9', 64, 1, 0, null, 'Contingent Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid10', 65, 1, 0, null, 'Contingent Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'is_om', 66, 0, 1, null, 'Is Om', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'current_lease_cost', 67, 1, 0, null, 'Current Lease Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'residual_amount', 68, 1, 0, null, 'Residual Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'term_penalty', 69, 1, 0, null, 'Term Penalty', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'bpo_price', 70, 1, 0, null, 'Bpo Price', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'currency_type', 71, 0, 1, null, 'Currency Type', 300, 'description', 'ls_lease_currency_type', 'VARCHAR2', 0, 'description', 1, 2, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'currency_display_symbol', 72, 0, 0, null, 'Currency Symbol', 300, '', '', 'VARCHAR2', 0, '', null, null, 'currency_display_symbol', 1, 1 FROM dual UNION ALL 
SELECT  l_id, 'iso_code', 73, 0, 1, null, 'Currency', 300, 'description', 'currency', 'VARCHAR2', 0, 'iso_code', null, null, 'iso_code', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_deferred_rent', 74, 1, 1, null, 'Beg Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'deferred_rent', 75, 1, 1, null, 'Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_deferred_rent', 76, 1, 1, null, 'End Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_liability', 77, 1, 1, null, 'Beg Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_liability', 78, 1, 1, null, 'End Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_liability', 79, 1, 1, null, 'Beg LT Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_liability', 80, 1, 1, null, 'End LT Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_remeasurement', 81, 1, 1, null, 'Principal Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'liability_remeasurement', 82, 1, 1, null, 'Liability Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_net_rou_asset', 83, 1, 1, null, 'Beg Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'rou_asset_remeasurement', 84, 1, 1, null, 'ROU Asset Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_net_rou_asset', 85, 1, 1, null, 'End Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'initial_direct_cost', 86, 1, 1, null, 'Initial Direct Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'incentive_amount', 87, 1, 1, null, 'Incentive Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_prepaid_rent', 88, 1, 1, null, 'Beg Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepay_amortization', 89, 1, 1, null, 'Prepay Amortization', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepaid_rent', 90, 1, 1, null, 'Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_prepaid_rent', 91, 1, 1, null, 'End Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_arrears_accrual', 92, 1, 1, null, 'Beg Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'arrears_accrual', 93, 1, 1, null, 'Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_arrears_accrual', 94, 1, 1, null, 'End Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_adjust', 95, 1, 1, null, 'Executory Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_adjust', 96, 1, 1, null, 'Contingent Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'begin_reserve', 97, 1, 1, null, 'Begin Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_expense', 98, 1, 1, null, 'Depr Expense', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_exp_alloc_adjust', 99, 1, 1, null, 'Depr Expense Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_reserve', 100, 1, 1, null, 'End Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'begin_accum_impair', 101, 1, 1, null, 'Begin Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'impairment_activity', 102, 1, 1, null, 'Impairment Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_accum_impair', 103, 1, 1, null, 'End Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual
;

END;

/

DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Forecast Lease Asset Schedule by ILR';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'select * from LS_FCST_ASSET_SCHED_BY_ILR_VW ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT  l_id, 'FORECAST_VERSION', 1, 0, 1, null, 'Forecast Version', 300, 'description', 'ls_forecast_version', 'VARCHAR2', 0, 'description', 1, 2, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'ILR_NUMBER', 2, 0, 1, null, 'ILR Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', 1, 2, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONVERSION_DATE', 3, 0, 1, null, 'Conversion Date', 300, 'conversion_date', 'ls_forecast_version', 'DATE', 0, 'conversion_date', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'LS_ASSET_ID', 4, 0, 1, null, 'LS Asset ID', 300, 'ls_asset_id', 'ls_asset', 'NUMBER', 0, 'ls_asset_id', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'LEASED_ASSET_NUMBER', 5, 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset', 'VARCHAR2', 0, 'leased_asset_number', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'COMPANY_ID', 6, 0, 1, null, 'Company Id', 300, 'company_id', '(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'COMPANY_DESCRIPTION', 7, 0, 1, null, 'Company Description', 300, 'description', '(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'ILR_ID', 8, 0, 1, null, 'ILR Id', 300, 'ilr_id', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'LEASE_ID', 9, 0, 1, null, 'Lease Id', 300, 'lease_id', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'LEASE_NUMBER', 10, 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'LEASE_CAP_TYPE', 11, 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0, 'description', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'LOCATION', 12, 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0, 'long_description', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'REVISION', 13, 0, 1, null, 'Revision', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'SET_OF_BOOKS', 14, 0, 1, null, 'Set Of Books', 300, 'description', 'set_of_books', 'VARCHAR2', 0, 'description', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'MONTHNUM', 15, 0, 1, null, 'Month Number', 300, 'month', 'ls_lease_asset_schedule', 'DATE', 0, 'month', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'BEG_CAPITAL_COST', 16, 0, 1, null, 'Beginning Capital Cost', 300, '', '', 'NUMBER', 1, '', null, null, 'the_sort', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'END_CAPITAL_COST', 17, 0, 1, null, 'Ending Capital Cost', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'BEG_OBLIGATION', 18, 0, 1, null, 'Beg Obligation', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'END_OBLIGATION', 19, 0, 1, null, 'End Obligation', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'BEG_LT_OBLIGATION', 20, 0, 1, null, 'Beginning LT Obligation', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'END_LT_OBLIGATION', 21, 0, 1, null, 'Ending LT Obligation', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'INTEREST_ACCRUAL', 22, 0, 1, null, 'Interest Accrual', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'PRINCIPAL_ACCRUAL', 23, 0, 1, null, 'Principal Accrual', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'INTEREST_PAID', 24, 0, 1, null, 'Interest Paid', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'PRINCIPAL_PAID', 25, 0, 1, null, 'Principal Paid', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL1', 26, 0, 1, null, 'Executory Accrual1', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL2', 27, 0, 1, null, 'Executory Accrual2', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL3', 28, 0, 1, null, 'Executory Accrual3', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL4', 29, 0, 1, null, 'Executory Accrual4', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL5', 30, 0, 1, null, 'Executory Accrual5', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL6', 31, 0, 1, null, 'Executory Accrual6', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL7', 32, 0, 1, null, 'Executory Accrual7', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL8', 33, 0, 1, null, 'Executory Accrual8', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL9', 34, 0, 1, null, 'Executory Accrual9', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL10', 35, 0, 1, null, 'Executory Accrual10', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID1', 36, 0, 1, null, 'Executory Paid1', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID2', 37, 0, 1, null, 'Executory Paid2', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID3', 38, 0, 1, null, 'Executory Paid3', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID4', 39, 0, 1, null, 'Executory Paid4', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID5', 40, 0, 1, null, 'Executory Paid5', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID6', 41, 0, 1, null, 'Executory Paid6', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID7', 42, 0, 1, null, 'Executory Paid7', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID8', 43, 0, 1, null, 'Executory Paid8', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID9', 44, 0, 1, null, 'Executory Paid9', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID10', 45, 0, 1, null, 'Executory Paid10', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL1', 46, 0, 1, null, 'Contingent Accrual1', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL2', 47, 0, 1, null, 'Contingent Accrual2', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL3', 48, 0, 1, null, 'Contingent Accrual3', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL4', 49, 0, 1, null, 'Contingent Accrual4', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL5', 50, 0, 1, null, 'Contingent Accrual5', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL6', 51, 0, 1, null, 'Contingent Accrual6', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL7', 52, 0, 1, null, 'Contingent Accrual7', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL8', 53, 0, 1, null, 'Contingent Accrual8', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL9', 54, 0, 1, null, 'Contingent Accrual9', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL10', 55, 0, 1, null, 'Contingent Accrual10', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID1', 56, 0, 1, null, 'Contingent Paid1', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID2', 57, 0, 1, null, 'Contingent Paid2', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID3', 58, 0, 1, null, 'Contingent Paid3', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID4', 59, 0, 1, null, 'Contingent Paid4', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID5', 60, 0, 1, null, 'Contingent Paid5', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID6', 61, 0, 1, null, 'Contingent Paid6', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID7', 62, 0, 1, null, 'Contingent Paid7', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID8', 63, 0, 1, null, 'Contingent Paid8', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID9', 64, 0, 1, null, 'Contingent Paid9', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID10', 65, 0, 1, null, 'Contingent Paid10', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'IS_OM', 66, 0, 1, null, 'is O&M', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CURRENT_LEASE_COST', 67, 0, 1, null, 'Current Lease Cost', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'RESIDUAL_AMOUNT', 68, 0, 1, null, 'Residual Amount', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'TERM_PENALTY', 69, 0, 1, null, 'Term Penalty', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'BPO_PRICE', 70, 0, 1, null, 'BPO Price', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'currency_type', 71, 0, 1, null, 'Currency Type', 300, 'description', 'ls_lease_currency_type', 'VARCHAR2', 0, 'description', 1, 2, 'description', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'currency_display_symbol', 72, 0, 0, null, 'Currency Symbol', 300, '', '', 'VARCHAR2', 0, '', null, null, 'currency_display_symbol', 1, 1 FROM dual UNION ALL
SELECT  l_id, 'iso_code', 73, 0, 1, null, 'Currency', 300, 'description', 'currency', 'VARCHAR2', 0, 'iso_code', null, null, 'iso_code', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'beg_deferred_rent', 74, 1, 1, null, 'Beg Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'deferred_rent', 75, 1, 1, null, 'Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_deferred_rent', 76, 1, 1, null, 'End Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'beg_liability', 77, 1, 1, null, 'Beg Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_liability', 78, 1, 1, null, 'End Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'beg_lt_liability', 79, 1, 1, null, 'Beg LT Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_lt_liability', 80, 1, 1, null, 'End LT Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'principal_remeasurement', 81, 1, 1, null, 'Principal Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'liability_remeasurement', 82, 1, 1, null, 'Liability Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'beg_net_rou_asset', 83, 1, 1, null, 'Beg Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'rou_asset_remeasurement', 84, 1, 1, null, 'ROU Asset Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_net_rou_asset', 85, 1, 1, null, 'End Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'initial_direct_cost', 86, 1, 1, null, 'Initial Direct Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'incentive_amount', 87, 1, 1, null, 'Incentive Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'beg_prepaid_rent', 88, 1, 1, null, 'Beg Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'prepay_amortization', 89, 1, 1, null, 'Prepay Amortization', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'prepaid_rent', 90, 1, 1, null, 'Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_prepaid_rent', 91, 1, 1, null, 'End Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'beg_arrears_accrual', 92, 1, 1, null, 'Beg Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'arrears_accrual', 93, 1, 1, null, 'Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_arrears_accrual', 94, 1, 1, null, 'End Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'executory_adjust', 95, 1, 1, null, 'Executory Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'contingent_adjust', 96, 1, 1, null, 'Contingent Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'begin_reserve', 97, 1, 1, null, 'Begin Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'depr_expense', 98, 1, 1, null, 'Depr Expense', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'depr_exp_alloc_adjust', 99, 1, 1, null, 'Depr Expense Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_reserve', 100, 1, 1, null, 'End Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'begin_accum_impair', 101, 1, 1, null, 'Begin Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'impairment_activity', 102, 1, 1, null, 'Impairment Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_accum_impair', 103, 1, 1, null, 'End Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual
;

END;

/


DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Forecast Lease Asset Schedule by Leased Asset';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'select * from LS_FCST_ASSET_SCHED_BY_AST_VW ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT  l_id, 'forecast_revision', 1, 0, 1, null, 'Forecast Version', 300, 'description', 'ls_forecast_version', 'VARCHAR2', 0, 'description', 1, 2, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'leased_asset_number', 2, 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset', 'VARCHAR2', 0, 'leased_asset_number', 1, 2, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'conversion_date', 3, 0, 1, null, 'Conversion Date', 300, 'conversion_date', 'ls_forecast_version', 'VARCHAR2', 0, 'description', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ls_asset_id', 4, 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0, 'ls_asset_id', null, null, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_id', 5, 0, 1, null, 'Company Id', 300, 'description', '(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_description', 6, 0, 1, null, 'Company Description', 300, 'description', '(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_id', 7, 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_number', 8, 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null, null, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_id', 9, 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_number', 10, 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number', null, null, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_cap_type', 11, 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'location', 12, 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0, 'long_description', null, null, 'long_description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'set_of_books_id', 13, 0, 1, null, 'Set Of Books Id', 300, 'description', 'set_of_books', 'NUMBER', 0, 'set_of_books_id', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'monthnum', 14, 0, 1, null, 'Monthnum', 300, 'month_number', '(select month_number from pp_calendar)', 'VARCHAR2', 0, 'month_number', null, null, 'the_sort', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_capital_cost', 15, 1, 1, null, 'Beg Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_capital_cost', 16, 1, 1, null, 'End Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_obligation', 17, 1, 1, null, 'Beg Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_obligation', 18, 1, 1, null, 'End Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_obligation', 19, 1, 1, null, 'Beg Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_obligation', 20, 1, 1, null, 'End Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_accrual', 21, 1, 1, null, 'Interest Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_accrual', 22, 1, 1, null, 'Principal Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_paid', 23, 1, 1, null, 'Interest Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_paid', 24, 1, 1, null, 'Principal Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual1', 25, 1, 1, null, 'Executory Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual2', 26, 1, 1, null, 'Executory Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual3', 27, 1, 1, null, 'Executory Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual4', 28, 1, 1, null, 'Executory Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual5', 29, 1, 1, null, 'Executory Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual6', 30, 1, 1, null, 'Executory Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual7', 31, 1, 1, null, 'Executory Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual8', 32, 1, 1, null, 'Executory Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual9', 33, 1, 1, null, 'Executory Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual10', 34, 1, 1, null, 'Executory Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid1', 35, 1, 1, null, 'Executory Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid2', 36, 1, 1, null, 'Executory Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid3', 37, 1, 1, null, 'Executory Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid4', 38, 1, 1, null, 'Executory Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid5', 39, 1, 1, null, 'Executory Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid6', 40, 1, 1, null, 'Executory Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid7', 41, 1, 1, null, 'Executory Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid8', 42, 1, 1, null, 'Executory Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid9', 43, 1, 1, null, 'Executory Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid10', 44, 1, 1, null, 'Executory Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual1', 45, 1, 1, null, 'Contingent Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual2', 46, 1, 1, null, 'Contingent Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual3', 47, 1, 1, null, 'Contingent Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual4', 48, 1, 1, null, 'Contingent Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual5', 49, 1, 1, null, 'Contingent Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual6', 50, 1, 1, null, 'Contingent Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual7', 51, 1, 1, null, 'Contingent Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual8', 52, 1, 1, null, 'Contingent Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual9', 53, 1, 1, null, 'Contingent Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual10', 54, 1, 1, null, 'Contingent Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid1', 55, 1, 1, null, 'Contingent Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid2', 56, 1, 1, null, 'Contingent Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid3', 57, 1, 1, null, 'Contingent Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid4', 58, 1, 1, null, 'Contingent Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid5', 59, 1, 1, null, 'Contingent Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid6', 60, 1, 1, null, 'Contingent Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid7', 61, 1, 1, null, 'Contingent Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid8', 62, 1, 1, null, 'Contingent Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid9', 63, 1, 1, null, 'Contingent Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid10', 64, 1, 1, null, 'Contingent Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'is_om', 65, 0, 1, null, 'Is Om', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'current_lease_cost', 66, 1, 1, null, 'Current Lease Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'residual_amount', 67, 1, 1, null, 'Residual Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'term_penalty', 68, 1, 1, null, 'Term Penalty', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'bpo_price', 69, 1, 1, null, 'Bpo Price', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'currency_type', 70, 0, 1, null, 'Currency Type', 300, 'description', 'ls_lease_currency_type', 'VARCHAR2', 0, 'description', 1, 2, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'currency_display_symbol', 71, 0, 0, null, 'Currency Symbol', 300, '', '', 'VARCHAR2', 0, '', null, null, 'currency_display_symbol', 1, 1 FROM dual UNION ALL 
SELECT  l_id, 'iso_code', 72, 0, 1, null, 'Currency', 300, 'description', 'currency', 'VARCHAR2', 0, 'iso_code', null, null, 'iso_code', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_deferred_rent', 73, 1, 1, null, 'Beg Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'deferred_rent', 74, 1, 1, null, 'Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_deferred_rent', 75, 1, 1, null, 'End Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_liability', 76, 1, 1, null, 'Beg Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_liability', 77, 1, 1, null, 'End Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_liability', 78, 1, 1, null, 'Beg LT Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_liability', 79, 1, 1, null, 'End LT Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_remeasurement', 80, 1, 1, null, 'Principal Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'liability_remeasurement', 81, 1, 1, null, 'Liability Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_net_rou_asset', 82, 1, 1, null, 'Beg Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'rou_asset_remeasurement', 83, 1, 1, null, 'ROU Asset Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_net_rou_asset', 84, 1, 1, null, 'End Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'initial_direct_cost', 85, 1, 1, null, 'Initial Direct Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'incentive_amount', 86, 1, 1, null, 'Incentive Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_prepaid_rent', 87, 1, 1, null, 'Beg Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepay_amortization', 88, 1, 1, null, 'Prepay Amortization', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepaid_rent', 89, 1, 1, null, 'Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_prepaid_rent', 90, 1, 1, null, 'End Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_arrears_accrual', 91, 1, 1, null, 'Beg Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'arrears_accrual', 92, 1, 1, null, 'Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_arrears_accrual', 93, 1, 1, null, 'End Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_adjust', 94, 1, 1, null, 'Executory Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_adjust', 95, 1, 1, null, 'Contingent Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'begin_reserve', 96, 1, 1, null, 'Begin Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_expense', 97, 1, 1, null, 'Depr Expense', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_exp_alloc_adjust', 98, 1, 1, null, 'Depr Expense Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_reserve', 99, 1, 1, null, 'End Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'begin_accum_impair', 100, 1, 1, null, 'Begin Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'impairment_activity', 101, 1, 1, null, 'Impairment Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_accum_impair', 102, 1, 1, null, 'End Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual;

END;

/

DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Asset Schedule By MLA';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'SELECT la.ls_asset_id,la.leased_asset_number,co.company_id,co.description AS company_description,
     ilr.ilr_id,ilr.ilr_number,ll.lease_id,ll.lease_number,lct.description AS lease_cap_type,al.long_description AS location,
     las.REVISION,las.SET_OF_BOOKS_ID,TO_CHAR(las.MONTH, ''yyyymm'') AS monthnum,lcurt.DESCRIPTION AS currency_type,
     las.iso_code currency,las.currency_display_symbol,las.BEG_CAPITAL_COST,las.END_CAPITAL_COST,las.BEG_OBLIGATION,
     las.END_OBLIGATION,las.BEG_LT_OBLIGATION,las.END_LT_OBLIGATION,las.INTEREST_ACCRUAL,las.PRINCIPAL_ACCRUAL,
     las.INTEREST_PAID,las.PRINCIPAL_PAID,las.EXECUTORY_ACCRUAL1,las.EXECUTORY_ACCRUAL2,las.EXECUTORY_ACCRUAL3,
     las.EXECUTORY_ACCRUAL4,las.EXECUTORY_ACCRUAL5,las.EXECUTORY_ACCRUAL6,las.EXECUTORY_ACCRUAL7,las.EXECUTORY_ACCRUAL8,
     las.EXECUTORY_ACCRUAL9,las.EXECUTORY_ACCRUAL10,las.EXECUTORY_PAID1,las.EXECUTORY_PAID2,las.EXECUTORY_PAID3,
     las.EXECUTORY_PAID4,las.EXECUTORY_PAID5,las.EXECUTORY_PAID6,las.EXECUTORY_PAID7,las.EXECUTORY_PAID8,
     las.EXECUTORY_PAID9,las.EXECUTORY_PAID10,las.CONTINGENT_ACCRUAL1,las.CONTINGENT_ACCRUAL2,las.CONTINGENT_ACCRUAL3,
     las.CONTINGENT_ACCRUAL4,las.CONTINGENT_ACCRUAL5,las.CONTINGENT_ACCRUAL6,las.CONTINGENT_ACCRUAL7,
     las.CONTINGENT_ACCRUAL8,las.CONTINGENT_ACCRUAL9,las.CONTINGENT_ACCRUAL10,las.CONTINGENT_PAID1,
     las.CONTINGENT_PAID2,las.CONTINGENT_PAID3,las.CONTINGENT_PAID4,las.CONTINGENT_PAID5,las.CONTINGENT_PAID6,
     las.CONTINGENT_PAID7,las.CONTINGENT_PAID8,las.CONTINGENT_PAID9,las.CONTINGENT_PAID10,las.IS_OM,
     las.CURRENT_LEASE_COST,las.RESIDUAL_AMOUNT,las.TERM_PENALTY,las.BPO_PRICE,las.BEG_DEFERRED_RENT,
     las.DEFERRED_RENT,las.END_DEFERRED_RENT,las.BEG_LIABILITY,las.END_LIABILITY,las.BEG_LT_LIABILITY, las.END_LT_LIABILITY,
     las.principal_remeasurement, las.liability_remeasurement, las.beg_net_rou_asset, las.rou_asset_remeasurement, las.end_net_rou_asset, 
     las.initial_direct_cost, las.incentive_amount, las.beg_prepaid_rent, las.prepay_amortization, las.prepaid_rent, las.end_prepaid_rent, 
     las.beg_arrears_accrual, las.arrears_accrual, las.end_arrears_accrual,las.executory_adjust, las.contingent_adjust, 
     ldf.begin_reserve, ldf.depr_expense, ldf.depr_exp_alloc_adjust, ldf.end_reserve, las.begin_accum_impair, las.impairment_activity, las.end_accum_impair
    FROM ls_asset la,ls_ilr ilr,ls_lease ll,company co,asset_location al,
      ls_lease_cap_type lct,ls_ilr_options ilro,v_ls_asset_schedule_fx_vw las,ls_lease_currency_type lcurt, ls_depr_forecast ldf
    WHERE la.ilr_id             = ilr.ilr_id
    AND ilr.lease_id            = ll.lease_id
    AND ilr.ilr_id              = ilro.ilr_id
    AND ilr.current_revision    = ilro.revision
    AND ilro.lease_cap_type_id  = lct.ls_lease_cap_type_id
    AND la.asset_location_id    = al.asset_location_id
    AND la.company_id           = co.company_id
    AND las.ls_asset_id         = la.ls_asset_id
    AND las.ls_cur_type         = lcurt.ls_currency_type_id
    AND las.revision            = la.approved_revision
    and ldf.ls_asset_id (+) = las.ls_asset_id and ldf.revision (+) = las.revision
    and ldf.set_of_books_id (+) = las.set_of_books_id and ldf.month (+) = las.month 
    AND upper(ll.lease_number) IN 
      (SELECT upper(filter_value)
      FROM pp_any_required_filter
      WHERE upper(trim(column_name)) = ''LEASE NUMBER''
      )
    AND UPPER(TRIM(lcurt.DESCRIPTION)) IN
      (SELECT UPPER(filter_value)
      FROM pp_any_required_filter
      WHERE Upper(Trim(column_name)) = ''CURRENCY TYPE'') ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT  l_id, 'currency', 1, 0, 0, null, 'Currency', 300, '', '', 'VARCHAR2', 0, '', null, null, '', 0, 1 FROM dual UNION ALL 
SELECT  l_id, 'currency_display_symbol', 2, 0, 0, null, 'Currency Symbol', 300, '', '', 'VARCHAR2', 0, '', null, null, '', 0, 1 FROM dual UNION ALL 
SELECT  l_id, 'ls_asset_id', 3, 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0, 'leased_asset_id', null, null, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'leased_asset_number', 4, 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset', 'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_id', 5, 0, 1, null, 'Company Id', 300, 'description', '(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_description', 6, 0, 1, null, 'Company Description', 300, 'description', '(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_id', 7, 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_number', 8, 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null, null, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_id', 9, 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_number', 10, 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number', 1, 2, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_cap_type', 11, 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'location', 12, 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0, 'long_description', null, null, 'long_description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'revision', 13, 0, 1, null, 'Revision', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'set_of_books_id', 14, 0, 1, null, 'Set Of Books Id', 300, 'description', 'set_of_books', 'NUMBER', 0, 'set_of_books_id', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'monthnum', 15, 0, 1, null, 'Monthnum', 300, 'month_number', '(select month_number from pp_calendar)', 'VARCHAR2', 0, 'month_number', null, null, 'the_sort', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_capital_cost', 16, 1, 1, null, 'Beg Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_capital_cost', 17, 1, 1, null, 'End Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_obligation', 18, 1, 1, null, 'Beg Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_obligation', 19, 1, 1, null, 'End Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_obligation', 20, 1, 1, null, 'Beg Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_obligation', 21, 1, 1, null, 'End Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_accrual', 22, 1, 1, null, 'Interest Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_accrual', 23, 1, 1, null, 'Principal Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_paid', 24, 1, 1, null, 'Interest Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_paid', 25, 1, 1, null, 'Principal Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual1', 26, 1, 1, null, 'Executory Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual2', 27, 1, 1, null, 'Executory Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual3', 28, 1, 1, null, 'Executory Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual4', 29, 1, 1, null, 'Executory Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual5', 30, 1, 1, null, 'Executory Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual6', 31, 1, 1, null, 'Executory Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual7', 32, 1, 1, null, 'Executory Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual8', 33, 1, 1, null, 'Executory Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual9', 34, 1, 1, null, 'Executory Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual10', 35, 1, 1, null, 'Executory Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid1', 36, 1, 1, null, 'Executory Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid2', 37, 1, 1, null, 'Executory Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid3', 38, 1, 1, null, 'Executory Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid4', 39, 1, 1, null, 'Executory Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid5', 40, 1, 1, null, 'Executory Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid6', 41, 1, 1, null, 'Executory Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid7', 42, 1, 1, null, 'Executory Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid8', 43, 1, 1, null, 'Executory Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid9', 44, 1, 1, null, 'Executory Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid10', 45, 1, 1, null, 'Executory Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual1', 46, 1, 1, null, 'Contingent Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual2', 47, 1, 1, null, 'Contingent Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual3', 48, 1, 1, null, 'Contingent Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual4', 49, 1, 1, null, 'Contingent Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual5', 50, 1, 1, null, 'Contingent Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual6', 51, 1, 1, null, 'Contingent Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual7', 52, 1, 1, null, 'Contingent Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual8', 53, 1, 1, null, 'Contingent Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual9', 54, 1, 1, null, 'Contingent Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual10', 55, 1, 1, null, 'Contingent Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid1', 56, 1, 1, null, 'Contingent Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid2', 57, 1, 1, null, 'Contingent Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid3', 58, 1, 1, null, 'Contingent Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid4', 59, 1, 1, null, 'Contingent Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid5', 60, 1, 1, null, 'Contingent Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid6', 61, 1, 1, null, 'Contingent Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid7', 62, 1, 1, null, 'Contingent Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid8', 63, 1, 1, null, 'Contingent Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid9', 64, 1, 1, null, 'Contingent Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid10', 65, 1, 1, null, 'Contingent Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'is_om', 66, 0, 1, null, 'Is Om', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'current_lease_cost', 67, 1, 1, null, 'Current Lease Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'residual_amount', 68, 1, 1, null, 'Residual Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'term_penalty', 69, 1, 1, null, 'Term Penalty', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'bpo_price', 70, 1, 1, null, 'Bpo Price', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'currency_type', 71, 0, 1, null, 'Currency Type', 300, 'description', 'ls_lease_currency_type', 'VARCHAR2', 0, 'description', 1, 2, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_deferred_rent', 72, 1, 1, null, 'Beg Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'deferred_rent', 73, 1, 1, null, 'Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_deferred_rent', 74, 1, 1, null, 'End Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_liability', 75, 1, 1, null, 'Beg Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_liability', 76, 1, 1, null, 'Beg Lt Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_liability', 77, 1, 1, null, 'End Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_liability', 78, 1, 1, null, 'End Lt Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_remeasurement', 79, 1, 1, null, 'Principal Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'liability_remeasurement', 80, 1, 1, null, 'Liability Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_net_rou_asset', 81, 1, 1, null, 'Beg Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'rou_asset_remeasurement', 82, 1, 1, null, 'ROU Asset Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_net_rou_asset', 83, 1, 1, null, 'End Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'initial_direct_cost', 84, 1, 1, null, 'Initial Direct Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'incentive_amount', 85, 1, 1, null, 'Incentive Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_prepaid_rent', 86, 1, 1, null, 'Beg Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepay_amortization', 87, 1, 1, null, 'Prepay Amortization', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepaid_rent', 88, 1, 1, null, 'Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_prepaid_rent', 89, 1, 1, null, 'End Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_arrears_accrual', 90, 1, 1, null, 'Beg Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'arrears_accrual', 91, 1, 1, null, 'Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_arrears_accrual', 92, 1, 1, null, 'End Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_adjust', 93, 1, 1, null, 'Executory Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_adjust', 94, 1, 1, null, 'Contingent Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'begin_reserve', 95, 1, 1, null, 'Begin Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_expense', 96, 1, 1, null, 'Depr Expense', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_exp_alloc_adjust', 97, 1, 1, null, 'Depr Expense Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_reserve', 98, 1, 1, null, 'End Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'begin_accum_impair', 99, 1, 1, null, 'Begin Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'impairment_activity', 100, 1, 1, null, 'Impairment Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_accum_impair', 101, 1, 1, null, 'End Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual
;

END;

/


DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Asset Schedule by ILR';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'select la.ls_asset_id, la.leased_asset_number, co.company_id, co.description as company_description, ilr.ilr_id, ilr.ilr_number,
     ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location, las.REVISION, las.SET_OF_BOOKS_ID,
     to_char(las.MONTH, ''yyyymm'') as monthnum, las.BEG_CAPITAL_COST, las.END_CAPITAL_COST, las.BEG_OBLIGATION, las.END_OBLIGATION,
     las.BEG_LT_OBLIGATION, las.END_LT_OBLIGATION, las.INTEREST_ACCRUAL, las.PRINCIPAL_ACCRUAL, las.INTEREST_PAID, las.PRINCIPAL_PAID,
     las.EXECUTORY_ACCRUAL1, las.EXECUTORY_ACCRUAL2, las.EXECUTORY_ACCRUAL3, las.EXECUTORY_ACCRUAL4, las.EXECUTORY_ACCRUAL5, las.EXECUTORY_ACCRUAL6,
     las.EXECUTORY_ACCRUAL7, las.EXECUTORY_ACCRUAL8, las.EXECUTORY_ACCRUAL9, las.EXECUTORY_ACCRUAL10, las.EXECUTORY_PAID1, las.EXECUTORY_PAID2,
     las.EXECUTORY_PAID3, las.EXECUTORY_PAID4, las.EXECUTORY_PAID5, las.EXECUTORY_PAID6, las.EXECUTORY_PAID7, las.EXECUTORY_PAID8, las.EXECUTORY_PAID9,
     las.EXECUTORY_PAID10, las.CONTINGENT_ACCRUAL1, las.CONTINGENT_ACCRUAL2, las.CONTINGENT_ACCRUAL3, las.CONTINGENT_ACCRUAL4, las.CONTINGENT_ACCRUAL5,
     las.CONTINGENT_ACCRUAL6, las.CONTINGENT_ACCRUAL7, las.CONTINGENT_ACCRUAL8, las.CONTINGENT_ACCRUAL9, las.CONTINGENT_ACCRUAL10, las.CONTINGENT_PAID1,
     las.CONTINGENT_PAID2, las.CONTINGENT_PAID3, las.CONTINGENT_PAID4, las.CONTINGENT_PAID5, las.CONTINGENT_PAID6, las.CONTINGENT_PAID7, las.CONTINGENT_PAID8,
     las.CONTINGENT_PAID9, las.CONTINGENT_PAID10,las.IS_OM, las.CURRENT_LEASE_COST, las.RESIDUAL_AMOUNT, las.TERM_PENALTY, las.BPO_PRICE,
     las.BEG_DEFERRED_RENT, las.DEFERRED_RENT, las.END_DEFERRED_RENT, las.BEG_LIABILITY, las.END_LIABILITY, las.BEG_LT_LIABILITY, las.END_LT_LIABILITY,
     las.principal_remeasurement, las.liability_remeasurement, las.beg_net_rou_asset, las.rou_asset_remeasurement, las.end_net_rou_asset, 
     las.initial_direct_cost, las.incentive_amount, las.beg_prepaid_rent, las.prepay_amortization, las.prepaid_rent, las.end_prepaid_rent, 
     las.beg_arrears_accrual, las.arrears_accrual, las.end_arrears_accrual,las.executory_adjust, las.contingent_adjust, 
     ldf.begin_reserve, ldf.depr_expense, ldf.depr_exp_alloc_adjust, ldf.end_reserve, las.begin_accum_impair, las.impairment_activity, las.end_accum_impair
     from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_lease_cap_type lct, ls_ilr_options ilro, ls_asset_schedule las, ls_depr_forecast ldf
     where la.ilr_id = ilr.ilr_id and ilr.lease_id = ll.lease_id and ilr.ilr_id = ilro.ilr_id and ilr.current_revision = ilro.revision
     and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id and la.asset_location_id = al.asset_location_id and la.company_id = co.company_id
     and las.ls_asset_id = la.ls_asset_id and las.revision = la.approved_revision
     and ldf.ls_asset_id (+) = las.ls_asset_id and ldf.revision (+) = las.revision
     and ldf.set_of_books_id (+) = las.set_of_books_id and ldf.month (+) = las.month
     and upper(ilr.ilr_number) in (select upper(filter_value) from pp_any_required_filter where upper(trim(column_name)) = ''ILR NUMBER'')  ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT  l_id, 'ls_asset_id', 1, 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0, 'ls_asset_id', null, null, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'leased_asset_number', 2, 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset', 'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_id', 3, 0, 1, null, 'Company Id', 300, 'description', '(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_description', 4, 0, 1, null, 'Company Description', 300, 'description', '(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_id', 5, 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_number', 6, 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', 1, 2, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_id', 7, 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_number', 8, 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number', null, null, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_cap_type', 9, 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'location', 10, 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0, 'long_description', null, null, 'long_description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'revision', 11, 0, 1, null, 'Revision', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'set_of_books_id', 12, 0, 1, null, 'Set Of Books Id', 300, 'description', 'set_of_books', 'NUMBER', 0, 'set_of_books_id', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'monthnum', 13, 0, 1, null, 'Monthnum', 300, 'month_number', '(select month_number from pp_calendar)', 'VARCHAR2', 0, 'month_number', null, null, 'the_sort', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_capital_cost', 14, 1, 1, null, 'Beg Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_capital_cost', 15, 1, 1, null, 'End Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_obligation', 16, 1, 1, null, 'Beg Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_obligation', 17, 1, 1, null, 'End Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_obligation', 18, 1, 1, null, 'Beg Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_obligation', 19, 1, 1, null, 'End Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_accrual', 20, 1, 1, null, 'Interest Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_accrual', 21, 1, 1, null, 'Principal Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_paid', 22, 1, 1, null, 'Interest Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_paid', 23, 1, 1, null, 'Principal Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual1', 24, 1, 1, null, 'Executory Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual2', 25, 1, 1, null, 'Executory Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual3', 26, 1, 1, null, 'Executory Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual4', 27, 1, 1, null, 'Executory Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual5', 28, 1, 1, null, 'Executory Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual6', 29, 1, 1, null, 'Executory Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual7', 30, 1, 1, null, 'Executory Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual8', 31, 1, 1, null, 'Executory Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual9', 32, 1, 1, null, 'Executory Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual10', 33, 1, 1, null, 'Executory Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid1', 34, 1, 1, null, 'Executory Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid2', 35, 1, 1, null, 'Executory Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid3', 36, 1, 1, null, 'Executory Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid4', 37, 1, 1, null, 'Executory Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid5', 38, 1, 1, null, 'Executory Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid6', 39, 1, 1, null, 'Executory Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid7', 40, 1, 1, null, 'Executory Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid8', 41, 1, 1, null, 'Executory Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid9', 42, 1, 1, null, 'Executory Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid10', 43, 1, 1, null, 'Executory Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual1', 44, 1, 1, null, 'Contingent Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual2', 45, 1, 1, null, 'Contingent Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual3', 46, 1, 1, null, 'Contingent Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual4', 47, 1, 1, null, 'Contingent Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual5', 48, 1, 1, null, 'Contingent Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual6', 49, 1, 1, null, 'Contingent Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual7', 50, 1, 1, null, 'Contingent Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual8', 51, 1, 1, null, 'Contingent Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual9', 52, 1, 1, null, 'Contingent Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual10', 53, 1, 1, null, 'Contingent Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid1', 54, 1, 1, null, 'Contingent Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid2', 55, 1, 1, null, 'Contingent Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid3', 56, 1, 1, null, 'Contingent Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid4', 57, 1, 1, null, 'Contingent Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid5', 58, 1, 1, null, 'Contingent Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid6', 59, 1, 1, null, 'Contingent Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid7', 60, 1, 1, null, 'Contingent Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid8', 61, 1, 1, null, 'Contingent Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid9', 62, 1, 1, null, 'Contingent Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid10', 63, 1, 1, null, 'Contingent Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'is_om', 64, 0, 1, null, 'Is Om', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'current_lease_cost', 65, 1, 1, null, 'Current Lease Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'residual_amount', 66, 1, 1, null, 'Residual Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'term_penalty', 67, 1, 1, null, 'Term Penalty', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'bpo_price', 68, 1, 1, null, 'Bpo Price', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_deferred_rent', 69, 1, 1, null, 'Beg Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'deferred_rent', 70, 1, 1, null, 'Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_deferred_rent', 71, 1, 1, null, 'End Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_liability', 72, 1, 1, null, 'Beg Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_liability', 73, 1, 1, null, 'Beg Lt Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_liability', 74, 1, 1, null, 'End Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_liability', 75, 1, 1, null, 'End Lt Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_remeasurement', 76, 1, 1, null, 'Principal Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'liability_remeasurement', 77, 1, 1, null, 'Liability Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_net_rou_asset', 78, 1, 1, null, 'Beg Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'rou_asset_remeasurement', 79, 1, 1, null, 'ROU Asset Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_net_rou_asset', 80, 1, 1, null, 'End Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'initial_direct_cost', 81, 1, 1, null, 'Initial Direct Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'incentive_amount', 82, 1, 1, null, 'Incentive Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_prepaid_rent', 83, 1, 1, null, 'Beg Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepay_amortization', 84, 1, 1, null, 'Prepay Amortization', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepaid_rent', 85, 1, 1, null, 'Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_prepaid_rent', 86, 1, 1, null, 'End Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_arrears_accrual', 87, 1, 1, null, 'Beg Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'arrears_accrual', 88, 1, 1, null, 'Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_arrears_accrual', 89, 1, 1, null, 'End Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_adjust', 90, 1, 1, null, 'Executory Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_adjust', 91, 1, 1, null, 'Contingent Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'begin_reserve', 92, 1, 1, null, 'Begin Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_expense', 93, 1, 1, null, 'Depr Expense', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_exp_alloc_adjust', 94, 1, 1, null, 'Depr Expense Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_reserve', 95, 1, 1, null, 'End Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'begin_accum_impair', 96, 1, 1, null, 'Begin Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'impairment_activity', 97, 1, 1, null, 'Impairment Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_accum_impair', 98, 1, 1, null, 'End Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual 
;

END;

/


DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Asset Schedule by Leased Asset';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'select la.ls_asset_id, la.leased_asset_number, co.company_id, co.description as company_description, ilr.ilr_id, ilr.ilr_number,
     ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location, las.REVISION, las.SET_OF_BOOKS_ID,
     to_char(las.MONTH, ''yyyymm'') as monthnum, las.BEG_CAPITAL_COST, las.END_CAPITAL_COST, las.BEG_OBLIGATION, las.END_OBLIGATION,
     las.BEG_LT_OBLIGATION, las.END_LT_OBLIGATION, las.INTEREST_ACCRUAL, las.PRINCIPAL_ACCRUAL, las.INTEREST_PAID, las.PRINCIPAL_PAID,
     las.EXECUTORY_ACCRUAL1, las.EXECUTORY_ACCRUAL2, las.EXECUTORY_ACCRUAL3, las.EXECUTORY_ACCRUAL4, las.EXECUTORY_ACCRUAL5, las.EXECUTORY_ACCRUAL6,
     las.EXECUTORY_ACCRUAL7, las.EXECUTORY_ACCRUAL8, las.EXECUTORY_ACCRUAL9, las.EXECUTORY_ACCRUAL10, las.EXECUTORY_PAID1, las.EXECUTORY_PAID2,
     las.EXECUTORY_PAID3, las.EXECUTORY_PAID4, las.EXECUTORY_PAID5, las.EXECUTORY_PAID6, las.EXECUTORY_PAID7, las.EXECUTORY_PAID8, las.EXECUTORY_PAID9,
     las.EXECUTORY_PAID10, las.CONTINGENT_ACCRUAL1, las.CONTINGENT_ACCRUAL2, las.CONTINGENT_ACCRUAL3, las.CONTINGENT_ACCRUAL4, las.CONTINGENT_ACCRUAL5,
     las.CONTINGENT_ACCRUAL6, las.CONTINGENT_ACCRUAL7, las.CONTINGENT_ACCRUAL8, las.CONTINGENT_ACCRUAL9, las.CONTINGENT_ACCRUAL10, las.CONTINGENT_PAID1,
     las.CONTINGENT_PAID2, las.CONTINGENT_PAID3, las.CONTINGENT_PAID4, las.CONTINGENT_PAID5, las.CONTINGENT_PAID6, las.CONTINGENT_PAID7,
     las.CONTINGENT_PAID8, las.CONTINGENT_PAID9, las.CONTINGENT_PAID10, las.IS_OM, las.CURRENT_LEASE_COST, las.RESIDUAL_AMOUNT, las.TERM_PENALTY,
     las.BPO_PRICE, las.BEG_DEFERRED_RENT, las.DEFERRED_RENT, las.END_DEFERRED_RENT, las.BEG_LIABILITY, las.END_LIABILITY, las.BEG_LT_LIABILITY,
     las.END_LT_LIABILITY, las.principal_remeasurement, las.liability_remeasurement, las.beg_net_rou_asset, las.rou_asset_remeasurement, las.end_net_rou_asset,
	 las.begin_accum_impair, las.impairment_activity, las.end_accum_impair, las.initial_direct_cost, las.incentive_amount, las.beg_prepaid_rent, 
	 las.prepay_amortization, las.prepaid_rent, las.end_prepaid_rent, las.beg_arrears_accrual, las.arrears_accrual, las.end_arrears_accrual,
	 las.executory_adjust, las.contingent_adjust, ldf.begin_reserve, ldf.depr_expense, ldf.depr_exp_alloc_adjust, ldf.end_reserve
     from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al,
     ls_lease_cap_type lct, ls_ilr_options ilro,ls_asset_schedule las, ls_depr_forecast ldf 
     where la.ilr_id = ilr.ilr_id and ilr.lease_id = ll.lease_id
     and ilr.ilr_id = ilro.ilr_id and ilr.current_revision = ilro.revision and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
     and la.asset_location_id = al.asset_location_id and la.company_id = co.company_id
     and las.ls_asset_id = la.ls_asset_id and las.revision = la.approved_revision  
     and ldf.ls_asset_id (+) = las.ls_asset_id and ldf.revision (+) = las.revision
     and ldf.set_of_books_id (+) = las.set_of_books_id and ldf.month (+) = las.month
     and upper(la.leased_asset_number) in (select upper(filter_value) from pp_any_required_filter where upper(trim(column_name)) = ''LEASED ASSET NUMBER'')  ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT  l_id, 'ls_asset_id', 1, 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0, 'ls_asset_id', null, null, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'leased_asset_number', 2, 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset', 'VARCHAR2', 0, 'leased_asset_number', 1, 2, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_id', 3, 0, 1, null, 'Company Id', 300, 'description', '(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_description', 4, 0, 1, null, 'Company Description', 300, 'description', '(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_id', 5, 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_number', 6, 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null, null, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_id', 7, 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_number', 8, 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number', null, null, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_cap_type', 9, 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'location', 10, 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0, 'long_description', null, null, 'long_description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'revision', 11, 0, 1, null, 'Revision', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'set_of_books_id', 12, 0, 1, null, 'Set Of Books Id', 300, 'description', 'set_of_books', 'NUMBER', 0, 'set_of_books_id', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'monthnum', 13, 0, 1, null, 'Monthnum', 300, 'month_number', '(select month_number from pp_calendar)', 'VARCHAR2', 0, 'month_number', null, null, 'the_sort', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_capital_cost', 14, 1, 1, null, 'Beg Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_capital_cost', 15, 1, 1, null, 'End Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_obligation', 16, 1, 1, null, 'Beg Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_obligation', 17, 1, 1, null, 'End Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_obligation', 18, 1, 1, null, 'Beg Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_obligation', 19, 1, 1, null, 'End Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_accrual', 20, 1, 1, null, 'Interest Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_accrual', 21, 1, 1, null, 'Principal Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_paid', 22, 1, 1, null, 'Interest Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_paid', 23, 1, 1, null, 'Principal Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual1', 24, 1, 1, null, 'Executory Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual2', 25, 1, 1, null, 'Executory Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual3', 26, 1, 1, null, 'Executory Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual4', 27, 1, 1, null, 'Executory Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual5', 28, 1, 1, null, 'Executory Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual6', 29, 1, 1, null, 'Executory Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual7', 30, 1, 1, null, 'Executory Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual8', 31, 1, 1, null, 'Executory Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual9', 32, 1, 1, null, 'Executory Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual10', 33, 1, 1, null, 'Executory Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid1', 34, 1, 1, null, 'Executory Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid2', 35, 1, 1, null, 'Executory Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid3', 36, 1, 1, null, 'Executory Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid4', 37, 1, 1, null, 'Executory Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid5', 38, 1, 1, null, 'Executory Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid6', 39, 1, 1, null, 'Executory Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid7', 40, 1, 1, null, 'Executory Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid8', 41, 1, 1, null, 'Executory Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid9', 42, 1, 1, null, 'Executory Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid10', 43, 1, 1, null, 'Executory Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual1', 45, 1, 1, null, 'Contingent Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual2', 46, 1, 1, null, 'Contingent Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual3', 47, 1, 1, null, 'Contingent Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual4', 48, 1, 1, null, 'Contingent Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual5', 49, 1, 1, null, 'Contingent Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual6', 50, 1, 1, null, 'Contingent Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual7', 51, 1, 1, null, 'Contingent Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual8', 52, 1, 1, null, 'Contingent Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual9', 53, 1, 1, null, 'Contingent Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual10', 54, 1, 1, null, 'Contingent Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid1', 55, 1, 1, null, 'Contingent Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid2', 56, 1, 1, null, 'Contingent Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid3', 57, 1, 1, null, 'Contingent Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid4', 58, 1, 1, null, 'Contingent Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid5', 59, 1, 1, null, 'Contingent Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid6', 60, 1, 1, null, 'Contingent Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid7', 61, 1, 1, null, 'Contingent Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid8', 62, 1, 1, null, 'Contingent Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid9', 63, 1, 1, null, 'Contingent Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid10', 64, 1, 1, null, 'Contingent Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'is_om', 66, 0, 1, null, 'Is Om', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'current_lease_cost', 67, 1, 1, null, 'Current Lease Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'residual_amount', 68, 1, 1, null, 'Residual Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'term_penalty', 69, 1, 1, null, 'Term Penalty', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'bpo_price', 70, 1, 1, null, 'Bpo Price', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_deferred_rent', 71, 1, 1, null, 'Beg Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'deferred_rent', 72, 1, 1, null, 'Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_deferred_rent', 73, 1, 1, null, 'End Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_liability', 74, 1, 1, null, 'Beg Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_liability', 75, 1, 1, null, 'End Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_liability', 76, 1, 1, null, 'Beg Lt Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_liability', 77, 1, 1, null, 'End Lt Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_remeasurement', 78, 1, 1, null, 'Principal Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'liability_remeasurement', 79, 1, 1, null, 'Liability Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_net_rou_asset', 80, 1, 1, null, 'Beg Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'begin_accum_impair', 81, 1, 1, null, 'Begin Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'impairment_activity', 82, 1, 1, null, 'Impairment Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_accum_impair', 83, 1, 1, null, 'End Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'rou_asset_remeasurement', 84, 1, 1, null, 'ROU Asset Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_net_rou_asset', 85, 1, 1, null, 'End Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'initial_direct_cost', 86, 1, 1, null, 'Initial Direct Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'incentive_amount', 87, 1, 1, null, 'Incentive Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_prepaid_rent', 88, 1, 1, null, 'Beg Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepay_amortization', 89, 1, 1, null, 'Prepay Amortization', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepaid_rent', 90, 1, 1, null, 'Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_prepaid_rent', 91, 1, 1, null, 'End Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_arrears_accrual', 92, 1, 1, null, 'Beg Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'arrears_accrual', 93, 1, 1, null, 'Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_arrears_accrual', 94, 1, 1, null, 'End Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'begin_reserve', 95, 1, 1, null, 'Begin Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_expense', 96, 1, 1, null, 'Depr Expense', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_exp_alloc_adjust', 97, 1, 1, null, 'Depr Expense Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_reserve', 98, 1, 1, null, 'End Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_adjust', 99, 1, 1, null, 'Executory Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_adjust', 100, 1, 1, null, 'Contingent Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual 
;

END;

/


DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Forecast Lease Depreciation';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := '
	  SELECT fct.description as forecast_version, la.leased_asset_number, ilr.ilr_number, mla.lease_number, cs.description as company, sob.description as sob,
	  to_number(to_char(ldf.MONTH,''yyyymm'')) as month, ldf.depr_expense, sch.impairment_activity, ldf.begin_reserve, ldf.end_reserve, sch.end_capital_cost,
	  (sch.end_capital_cost-ldf.end_reserve) as ROU
	  FROM ls_lease mla,
		  ls_ilr ilr,
		  ls_asset la,
		  ls_depr_forecast ldf,
		  company cs,
		  set_of_books sob,
		  ls_asset_schedule sch,
		  ls_forecast_version fct
	  WHERE la.ilr_id = ilr.ilr_id
	  AND   ilr.lease_id = mla.lease_id
	  AND   fct.revision = ldf.revision
	  AND   abs(fct.set_of_books_id) = abs(sob.set_of_books_id)
	  AND   la.ls_asset_id = ldf.ls_asset_id
	  AND   la.company_id = cs.company_id
	  and   ldf.set_of_books_id = sob.set_of_books_id
	  AND   la.ls_asset_status_id = 3
	  and   sch.ls_asset_id = la.ls_asset_id
	  and   sch.revision = fct.revision
	  and   sch.month = ldf.month
	  and   sch.set_of_books_id = sob.set_of_books_id
	  ORDER BY la.leased_asset_number, ldf.MONTH, sob.description';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT l_id,'forecast_version',1,0,1,NULL,'Forecast Version',300,'description','ls_forecast_version','VARCHAR2',0,'description',1,2,'description',0,0 FROM dual UNION ALL
SELECT l_id,'sob',2,0,1,NULL,'Set Of Books',300,'description','(select * from set_of_books where set_of_books_id in (select set_of_books_id from ls_fasb_cap_type_sob_map))','VARCHAR2',0,'description',0,0,'description',0,0 FROM dual UNION ALL
SELECT l_id,'company',3,0,1,NULL,'Company',300,'description','(select * from company where is_lease_company = 1)','VARCHAR2',0,'description',0,0,'description',0,0 FROM dual UNION ALL
SELECT l_id,'lease_number',4,0,1,NULL,'Lease Number',300,'lease_number','ls_lease','VARCHAR2',0,'lease_number',0,0,'lease_number',0,0 FROM dual UNION ALL
SELECT l_id,'ilr_number',5,0,1,NULL,'ILR Number',300,'ilr_number','ls_ilr','VARCHAR2',0,'ilr_number',0,0,'ilr_number',0,0 FROM dual UNION ALL
SELECT l_id,'leased_asset_number',6,0,1,NULL,'Leased Asset Number',300,'leased_asset_number','ls_asset','VARCHAR2',0,'leased_asset_number',0,0,'leased_asset_number',0,0 FROM dual UNION ALL
SELECT l_id,'month',7,0,1,NULL,'Month',300,'month_number','pp_calendar','number',0,'month_number',0,0,'month_number',0,0 FROM dual UNION ALL
SELECT l_id,'depr_expense',8,1,1,NULL,'Depr Expense',300,NULL,NULL,'NUMBER',0,NULL,0,0,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'impairment_activity',9,1,1,NULL,'Impairment Amount',300,NULL,NULL,'NUMBER',0,NULL,0,0,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'begin_reserve',10,1,1,NULL,'Begin Reserve',300,NULL,NULL,'NUMBER',0,NULL,0,0,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_reserve',11,1,1,NULL,'End Reserve',300,NULL,NULL,'NUMBER',0,NULL,0,0,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_capital_cost',12,1,1,NULL,'End Capital Cost',300,NULL,NULL,'NUMBER',0,NULL,0,0,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'rou',13,1,1,NULL,'ROU',300,NULL,NULL,'NUMBER',0,NULL,0,0,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'currency_type',14,0,1,NULL,'Currency Type',300,'description','ls_lease_currency_type','VARCHAR2',0,'description',1,2,'description',0,0 FROM dual UNION ALL
SELECT l_id,'currency_display_symbol',15,0,0,NULL,'Currency Symbol',300,NULL,NULL,'VARCHAR2',0,NULL,NULL,NULL,'currency_display_symbol',1,1 FROM dual UNION ALL
SELECT l_id,'iso_code',16,0,1,NULL,'Currency',300,'description','currency','VARCHAR2',0,'iso_code',NULL,NULL,'iso_code',0,0 FROM dual;

END;

/


DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Balance Sheet Detail by Asset (Company Currency)';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := 1;
  
  l_query_sql := 'select la.ls_asset_id, la.leased_asset_number, to_char(las.month,''yyyymm'') as monthnum, la.description as asset_description,
       la.company_id, co.description as company_description, ilr.ilr_id,
       ilr.ilr_number, ilr.external_ilr, ll.lease_id, ll.lease_number, lct.description as lease_cap_type,
       al.long_description as location, al.state_id as state, gl.external_account_code as account,
       ua.utility_account_id, ua.description as utility_account_description, fc.description as func_class_description,
       las.currency_display_symbol,
       las.iso_code AS currency,
       las.beg_capital_cost, las.end_capital_cost, las.beg_obligation, las.end_obligation,
       las.beg_lt_obligation, las.end_lt_obligation,
       case when las.month > depr_calc.month then las.begin_reserve else cprd.beg_reserve_month end as beg_reserve,
       case when las.month > depr_calc.month then las.end_reserve else cprd.depr_reserve end - nvl(cprd.retirements,0) as end_reserve,
	   las.begin_accum_impair, las.end_accum_impair, nvl(cprd. net_adds_and_adjust,0) as additions, nvl(cprd.retirements,0) + nvl(gain_loss,0) as retirements, 
	   nvl(cprd.transfers_in,0) as transfers_in, nvl(cprd.transfers_out,0) as transfers_out, nvl(cprd.reserve_trans_in,0) as reserve_trans_in, nvl(cprd.reserve_trans_out,0) as reserve_trans_out, 
	   (las.end_capital_cost - las.end_obligation - case when las.month > depr_calc.month then las.end_reserve else cprd.depr_reserve end ) as ferc_gaap_difference,
       las.set_of_books_id, las.revision
from ls_asset la, ls_cpr_asset_map map, ls_ilr ilr, ls_lease ll, company co,
     cpr_ledger cpr, utility_account ua, business_segment bs, func_class fc, gl_account gl,
     v_ls_asset_schedule_fx_vw las, asset_location al,
     ls_ilr_options ilro, ls_lease_cap_type lct,
     (select m.ls_asset_id, c.*
      from cpr_depr c, ls_cpr_asset_map m, cpr_ledger l
      where c.asset_id = m.asset_id
        and m.asset_id = l.asset_id
        and to_char(c.gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(column_name)= ''MONTHNUM'')
        and l.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')) cprd,
     (select company_id, max(gl_posting_mo_yr) month
      from ls_process_control where depr_calc is not null
         and company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
      group by company_id) depr_calc
where la.ls_asset_id = map.ls_asset_id
  and map.asset_id = cpr.asset_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilro.revision = ilr.current_revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and las.is_om = 0
  and cpr.gl_account_id = gl.gl_account_id
  and la.utility_account_id = ua.utility_account_id
  and la.bus_segment_id = ua.bus_segment_id
  and ua.func_class_id = fc.func_class_id
  and la.bus_segment_id = bs.bus_segment_id
  and co.company_id = la.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and depr_calc.company_id = co.company_id
  and las.month = cprd.gl_posting_mo_yr (+)
  and las.ls_asset_id = cprd.ls_asset_id (+)
  and las.set_of_books_id = cprd.set_of_books_id (+)
  and las.ls_cur_type = 2
  and la.asset_location_id = al.asset_location_id
  and to_char(las.month, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(column_name)= ''MONTHNUM'')
  and co.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT l_id,'ls_asset_id',1,0,1,NULL,'Ls Asset Id',300,'leased_asset_number','ls_asset','NUMBER',0,'ls_asset_id',NULL,NULL,'leased_asset_number',0,0 FROM dual UNION ALL
SELECT l_id,'leased_asset_number',2,0,1,NULL,'Leased Asset Number',300,'leased_asset_number','ls_asset','VARCHAR2',0,'leased_asset_number',NULL,NULL,'leased_asset_number',0,0 FROM dual UNION ALL
SELECT l_id,'monthnum',3,0,1,NULL,'Monthnum',300,'month_number','(select month_number from pp_calendar)','VARCHAR2',0,'month_number',1,2,'the_sort',0,0 FROM dual UNION ALL
SELECT l_id,'asset_description',4,0,1,NULL,'Asset Description',300,'description','ls_asset','VARCHAR2',0,'description',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'company_id',5,0,1,NULL,'Company Id',300,'description','(select * from company where is_lease_company = 1)','NUMBER',0,'company_id',1,2,'description',0,0 FROM dual UNION ALL
SELECT l_id,'company_description',6,0,1,NULL,'Company Description',300,'description','(select * from company where is_lease_company = 1)','VARCHAR2',0,'description',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'ilr_id',7,0,1,NULL,'Ilr Id',300,'ilr_number','ls_ilr','NUMBER',0,'ilr_id',NULL,NULL,'ilr_number',0,0 FROM dual UNION ALL
SELECT l_id,'ilr_number',8,0,1,NULL,'Ilr Number',300,'ilr_number','ls_ilr','VARCHAR2',0,'ilr_number',NULL,NULL,'ilr_number',0,0 FROM dual UNION ALL
SELECT l_id,'external_ilr',9,0,1,NULL,'External Ilr',300,'external_ilr','ls_ilr','VARCHAR2',0,'external_ilr',NULL,NULL,'external_ilr',0,0 FROM dual UNION ALL
SELECT l_id,'lease_id',10,0,1,NULL,'Lease Id',300,'lease_number','ls_lease','NUMBER',0,'lease_id',NULL,NULL,'lease_number',0,0 FROM dual UNION ALL
SELECT l_id,'lease_number',11,0,1,NULL,'Lease Number',300,'lease_number','ls_lease','VARCHAR2',0,'lease_number',NULL,NULL,'lease_number',0,0 FROM dual UNION ALL
SELECT l_id,'lease_cap_type',12,0,1,NULL,'Lease Cap Type',300,'description','ls_lease_cap_type','VARCHAR2',0,'description',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'location',13,0,1,NULL,'Location',300,'long_description','asset_location','VARCHAR2',0,'long_description',NULL,NULL,'long_description',0,0 FROM dual UNION ALL
SELECT l_id,'state',14,0,1,NULL,'State',300,'state_id','state','VARCHAR2',0,'state_id',NULL,NULL,'state_id',0,0 FROM dual UNION ALL 
SELECT l_id,'account',15,0,1,NULL,'Account',300,'description','ls_dist_gl_account','VARCHAR2',0,'external_account_code',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'utility_account_id',16,0,1,NULL,'Utility Account Id',300,'description','utility_account','NUMBER',0,'utility_account_id',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'utility_account_description',17,0,1,NULL,'Utility Account Description',300,'description','utility_account','VARCHAR2',0,'description',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'func_class_description',18,0,1,NULL,'Func Class Description',300,'description','func_class','VARCHAR2',0,'description',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'currency_display_symbol',19,0,0,NULL,'Currency Symbol',300,NULL,NULL,'VARCHAR2',0,NULL,NULL,NULL,NULL,1,1 FROM dual UNION ALL
SELECT l_id,'currency',20,0,0,NULL,'Currency',300,NULL,NULL,'VARCHAR2',0,NULL,NULL,NULL,NULL,0,1 FROM dual UNION ALL
SELECT l_id,'beg_capital_cost',21,1,1,NULL,'Beg Capital Cost',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_capital_cost',22,1,1,NULL,'End Capital Cost',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'beg_obligation',23,1,1,NULL,'Beg Obligation',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_obligation',24,1,1,NULL,'End Obligation',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'beg_lt_obligation',25,1,1,NULL,'Beg Lt Obligation',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_lt_obligation',26,1,1,NULL,'End Lt Obligation',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'beg_reserve',27,1,1,NULL,'Beg Reserve',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_reserve',28,1,1,NULL,'End Reserve',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'begin_accum_impair',29,1,1,NULL,'Begin Accum Impairment',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_accum_impair',30,1,1,NULL,'End Accum Impairment',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'additions',31,1,1,NULL,'Additions',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'retirements',32,1,1,NULL,'Retirements',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'transfers_in',33,1,1,NULL,'Transfers In',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'transfers_out',34,1,1,NULL,'Transfers Out',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'reserve_trans_in',35,1,1,NULL,'Reserve Trans In',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'reserve_trans_out',36,1,1,NULL,'Reserve Trans Out',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'ferc_gaap_difference',	37,1,1,NULL,'Ferc Gaap Difference',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual;


END;

/



DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Balance Sheet Span by Asset (Company Currency)';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := 1;

  l_query_sql := 'with balance_sheet_detail as (
select la.ls_asset_id, la.leased_asset_number, to_char(las.month,''yyyymm'') as monthnum, la.description as asset_description,
       la.company_id, co.description as company_description, ilr.ilr_id,
       ilr.ilr_number, ilr.external_ilr, ll.lease_id, ll.lease_number, lct.description as lease_cap_type,
       al.long_description as location, al.state_id as state, gl.external_account_code as account,
       ua.utility_account_id, ua.description as utility_account_description, fc.description as func_class_description,
       las.currency_display_symbol,
       las.iso_code AS currency,
       las.beg_capital_cost, las.end_capital_cost, las.beg_obligation, las.end_obligation,
       las.beg_lt_obligation, las.end_lt_obligation,
       case when las.month > depr_calc.month then las.begin_reserve else cprd.beg_reserve_month end as beg_reserve,
       case when las.month > depr_calc.month then las.end_reserve else cprd.depr_reserve end as end_reserve, las.begin_accum_impair, las.end_accum_impair,
       nvl(cprd. net_adds_and_adjust,0) as additions, nvl(cprd.retirements,0) + nvl(gain_loss,0) as retirements, nvl(cprd.transfers_in,0) as transfers_in,
       nvl(cprd.transfers_out,0) as transfers_out, nvl(cprd.reserve_trans_in,0) as reserve_trans_in, nvl(cprd.reserve_trans_out,0) as reserve_trans_out,
       (las.end_capital_cost - las.end_obligation - case when las.month > depr_calc.month then las.end_reserve else cprd.depr_reserve end) as ferc_gaap_difference
from ls_asset la, ls_cpr_asset_map map, ls_ilr ilr, ls_lease ll, company co,
     cpr_ledger cpr, utility_account ua, business_segment bs, func_class fc, gl_account gl,
     v_ls_asset_schedule_fx_vw las, asset_location al,
     ls_ilr_options ilro, ls_lease_cap_type lct,
     (select m.ls_asset_id, c.*
      from cpr_depr c, ls_cpr_asset_map m, cpr_ledger l
      where c.asset_id = m.asset_id
        and m.asset_id = l.asset_id
        and to_char(c.gl_posting_mo_yr, ''yyyymm'') >= (select filter_value from pp_any_required_filter where upper(column_name)= ''START MONTHNUM'')
        and to_char(c.gl_posting_mo_yr, ''yyyymm'') <= (select filter_value from pp_any_required_filter where upper(column_name)= ''END MONTHNUM'')
        and l.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')) cprd,
     (select company_id, max(gl_posting_mo_yr) month
      from ls_process_control where depr_calc is not null
        and company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
      group by company_id) depr_calc
where la.ls_asset_id = map.ls_asset_id
  and map.asset_id = cpr.asset_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilro.revision = ilr.current_revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and las.is_om = 0
  and cpr.gl_account_id = gl.gl_account_id
  and la.utility_account_id = ua.utility_account_id
  and la.bus_segment_id = ua.bus_segment_id
  and ua.func_class_id = fc.func_class_id
  and la.bus_segment_id = bs.bus_segment_id
  and co.company_id = la.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and depr_calc.company_id = co.company_id (+)
  and las.month = cprd.gl_posting_mo_yr (+)
  and las.ls_asset_id = cprd.ls_asset_id (+)
  and las.set_of_books_id = cprd.set_of_books_id (+)
  and las.ls_cur_type = 2
  and la.asset_location_id = al.asset_location_id
  and to_char(las.month, ''yyyymm'') >= (select filter_value from pp_any_required_filter where upper(column_name)= ''START MONTHNUM'')
  and to_char(las.month, ''yyyymm'') <= (select filter_value from pp_any_required_filter where upper(column_name)= ''END MONTHNUM'')
  and co.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')) select (select filter_value from pp_any_required_filter where upper(column_name)= ''START MONTHNUM'') as start_monthnum,
       (select filter_value from pp_any_required_filter where upper(column_name)= ''END MONTHNUM'') as end_monthnum,
       fm.LS_ASSET_ID, LEASED_ASSET_NUMBER, ASSET_DESCRIPTION, COMPANY_ID, COMPANY_DESCRIPTION, ILR_ID, ILR_NUMBER,
       EXTERNAL_ILR, LEASE_ID, LEASE_NUMBER, LEASE_CAP_TYPE, LOCATION, STATE, ACCOUNT, UTILITY_ACCOUNT_ID, UTILITY_ACCOUNT_DESCRIPTION,
       FUNC_CLASS_DESCRIPTION,
       currency_display_symbol,
       currency,
       nvl(BEG_CAPITAL_COST,0) as BEG_CAPITAL_COST, nvl(END_CAPITAL_COST,0) as end_capital_cost, nvl(BEG_OBLIGATION,0) as beg_obligation,
       nvl(END_OBLIGATION,0) as end_obligation, nvl(BEG_LT_OBLIGATION,0) as beg_lt_obligation, nvl(END_LT_OBLIGATION,0) as end_lt_obligation,
       nvl(BEG_RESERVE,0) as beg_reserve, nvl(END_RESERVE,0) as end_reserve,
       sum(additions) as additions, sum(RETIREMENTS) as retirements, sum(TRANSFERS_IN) as transfers_in, sum(TRANSFERS_OUT) as transfers_out,
       sum(RESERVE_TRANS_IN) as reserve_trans_in, sum(RESERVE_TRANS_OUT) as reserve_trans_out, sum(FERC_GAAP_DIFFERENCE) as ferc_gaap_difference
from
(select monthnum, ls_asset_id, beg_capital_cost, beg_obligation, beg_lt_obligation, beg_reserve
 from balance_sheet_detail
 where monthnum = (select filter_value from pp_any_required_filter where upper(column_name)= ''START MONTHNUM'')) fm, /* first month */
(select monthnum, ls_asset_id, end_capital_cost, end_obligation, end_lt_obligation, end_reserve
 from balance_sheet_detail
 where monthnum = (select filter_value from pp_any_required_filter where upper(column_name)= ''END MONTHNUM'')) lm, /* last month */
(select LS_ASSET_ID, LEASED_ASSET_NUMBER, MONTHNUM, ASSET_DESCRIPTION, COMPANY_ID, COMPANY_DESCRIPTION, ILR_ID, ILR_NUMBER,
        EXTERNAL_ILR, LEASE_ID, LEASE_NUMBER, LEASE_CAP_TYPE, LOCATION, STATE, ACCOUNT, UTILITY_ACCOUNT_ID, UTILITY_ACCOUNT_DESCRIPTION,
        FUNC_CLASS_DESCRIPTION,
        currency_display_symbol,
        currency,
        ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, RESERVE_TRANS_IN, RESERVE_TRANS_OUT, FERC_GAAP_DIFFERENCE
 from balance_sheet_detail) am /* all months */
where 1=1
  and am.ls_asset_id = fm.ls_asset_id (+)
  and am.ls_asset_id = lm.ls_asset_id (+)
group by fm.monthnum, lm.monthnum, fm.LS_ASSET_ID, LEASED_ASSET_NUMBER, ASSET_DESCRIPTION, COMPANY_ID, COMPANY_DESCRIPTION, ILR_ID, ILR_NUMBER,
       EXTERNAL_ILR, LEASE_ID, LEASE_NUMBER, LEASE_CAP_TYPE, LOCATION, STATE, ACCOUNT, UTILITY_ACCOUNT_ID, UTILITY_ACCOUNT_DESCRIPTION,
       FUNC_CLASS_DESCRIPTION,
       currency_display_symbol,
       currency,
       BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, BEG_RESERVE,
       END_RESERVE
order by 1        ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT l_id,'start_monthnum',1,0,1,NULL,'Start Monthnum',300,NULL,NULL,'VARCHAR2',0,NULL,1,1,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_monthnum',2,0,1,NULL,'End Monthnum',300,NULL,NULL,'VARCHAR2',0,NULL,1,1,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'ls_asset_id',3,0,1,NULL,'Ls Asset Id',300,'leased_asset_number','ls_asset','NUMBER',0,'ls_asset_id',NULL,NULL,'leased_asset_number',0,0 FROM dual UNION ALL
SELECT l_id,'leased_asset_number',4,0,1,NULL,'Leased Asset Number',300,'leased_asset_number','ls_asset','VARCHAR2',0,'leased_asset_number',NULL,NULL,'leased_asset_number',0,0 FROM dual UNION ALL
SELECT l_id,'asset_description',5,0,1,NULL,'Asset Description',300,'description','ls_asset','VARCHAR2',0,'description',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'company_id',6,0,1,NULL,'Company Id',300,'description','(select * from company where is_lease_company = 1)','NUMBER',0,'company_id',1,2,'description',0,0 FROM dual UNION ALL
SELECT l_id,'company_description',7,0,1,NULL,'Company Description',300,'description','(select * from company where is_lease_company = 1)','VARCHAR2',0,'description',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'ilr_id',8,0,1,NULL,'Ilr Id',300,'ilr_number','ls_ilr','NUMBER',0,'ilr_id',NULL,NULL,'ilr_number',0,0 FROM dual UNION ALL
SELECT l_id,'ilr_number',9,0,1,NULL,'Ilr Number',300,'ilr_number','ls_ilr','VARCHAR2',0,'ilr_number',NULL,NULL,'ilr_number',0,0 FROM dual UNION ALL
SELECT l_id,'external_ilr',10,0,1,NULL,'External Ilr',300,'external_ilr','ls_ilr','VARCHAR2',0,'external_ilr',NULL,NULL,'external_ilr',0,0 FROM dual UNION ALL
SELECT l_id,'lease_id',11,0,1,NULL,'Lease Id',300,'lease_number','ls_lease','NUMBER',0,'lease_id',NULL,NULL,'lease_number',0,0 FROM dual UNION ALL
SELECT l_id,'lease_number',12,0,1,NULL,'Lease Number',300,'lease_number','ls_lease','VARCHAR2',0,'lease_number',NULL,NULL,'lease_number',0,0 FROM dual UNION ALL
SELECT l_id,'lease_cap_type',13,0,1,NULL,'Lease Cap Type',300,'description','ls_lease_cap_type','VARCHAR2',0,'description',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'location',14,0,1,NULL,'Location',300,'long_description','asset_location','VARCHAR2',0,'long_description',NULL,NULL,'long_description',0,0 FROM dual UNION ALL
SELECT l_id,'state',15,	0,1,NULL,'State',300,'state_id','state','VARCHAR2',0,'state_id',NULL,NULL,'state_id',0,0 FROM dual UNION ALL
SELECT l_id,'account',16,0,1,NULL,'Account',300,'description','ls_dist_gl_account','VARCHAR2',0,'external_account_code',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'utility_account_id',17,0,1,NULL,'Utility Account Id',300,'description','utility_account','NUMBER',0,'utility_account_id',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'utility_account_description',18,0,1,NULL,'Utility Account Description',300,'description','utility_account','VARCHAR2',0,'description',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'func_class_description',19,0,1,NULL,'Func Class Description',300,'description','func_class','VARCHAR2',0,'description',NULL,NULL,'description',0,0 FROM dual UNION ALL
SELECT l_id,'currency_display_symbol',20,0,0,NULL,'Currency Symbol',300,NULL,NULL,'VARCHAR2',0,NULL,NULL,NULL,NULL,1,1 FROM dual UNION ALL
SELECT l_id,'currency',21,0,0,NULL,'Currency',300,NULL,NULL,'VARCHAR2',0,NULL,NULL,NULL,NULL,0,1 FROM dual UNION ALL
SELECT l_id,'beg_capital_cost',22,1,1,NULL,'Beg Capital Cost',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_capital_cost',23,1,1,NULL,'End Capital Cost',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'beg_obligation',24,1,1,NULL,'Beg Obligation',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_obligation',25,1,1,NULL,'End Obligation',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'beg_lt_obligation',26,1,1,NULL,'Beg Lt Obligation',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_lt_obligation',27,1,1,NULL,'End Lt Obligation',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'beg_reserve',28,1,1,NULL,'Beg Reserve',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_reserve',29,1,1,NULL,'End Reserve',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'begin_accum_impair',30,1,1,NULL,'Begin Accum Impairment',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_accum_impair',31,1,1,NULL,'End Accum Impairment',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'additions',32,1,1,NULL,'Additions',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'retirements',33,1,1,NULL,'Retirements',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'transfers_in',34,1,1,NULL,'Transfers In',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'transfers_out',35,1,1,NULL,'Transfers Out',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'reserve_trans_in',36,1,1,NULL,'Reserve Trans In',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'reserve_trans_out',37,1,1,NULL,'Reserve Trans Out',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'ferc_gaap_difference',38,1,1,NULL,'Ferc Gaap Difference',300,NULL,NULL,'NUMBER',0,NULL,NULL,NULL,NULL,0,0 FROM dual;


END;
/

set define on


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14083, 0, 2018, 2, 0, 0, 52481, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052481_lessee_02_add_impair_queries_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;