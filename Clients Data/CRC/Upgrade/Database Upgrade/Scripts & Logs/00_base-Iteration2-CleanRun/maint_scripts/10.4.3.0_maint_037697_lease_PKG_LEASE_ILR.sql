/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037697_lease_PKG_LEASE_ILR.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 04/14/2014 Ryan Oliveria	Adding support to convert forecasts for selected ILR's
||============================================================================
*/

create or replace package PKG_LEASE_ILR as

   type CR_DERIVER_TYPE is table of CR_DERIVER_CONTROL%rowtype;

   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_NEWREVISION(A_ILR_ID number) return number;

   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the passed in revision information as the starting point, and copy everything, including the schedule
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *      number: in: a_revision = the revision to copy
   *   @@RETURN
   *      number: new revision number for success
   *            -1 for failure
   */
   function F_COPYREVISION(A_ILR_ID   number,
                           A_REVISION number) return number;

   /*
   *   @@DESCRIPTION
   *      This function converts forecasts into new "real" revisions for all in-service ILR's
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      none
   *   @@RETURN
   *      number: 1 for success
   *            -1 for failure
   */
   function F_CONVERTFORECASTS return number;

   /*
   *   @@DESCRIPTION
   *      This function converts forecasts into new "real" revisions for the passed in array of ILR's
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      none
   *   @@RETURN
   *      number: 1 for success
   *            -1 for failure
   */
   function F_CONVERTFORECASTS(A_ILR_IDS in PKG_LEASE_CALC.NUM_ARRAY) return number;

   /*
   *   @@DESCRIPTION
   *      This function copies an ILR into a new ILR
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new ilr
   *    number: in: a_pct = the percent to transfer
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_COPYILR(A_ILR_ID number,
                      A_PCT    number) return number;

   /*
   *   @@DESCRIPTION
   *      This function copies an asset into another leased asset
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *    number: in: a_ls_asset_id = the asset to copy
   *    number: in: a_pct. Percent to copy
   *    number: in: a_qty.  The quantity to copy
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_COPYASSET(A_LS_ASSET_ID number,
                        A_PCT         number,
                        A_QTY         number) return number;

   function F_GETTAXES(A_ILR_ID number) return varchar2;

end PKG_LEASE_ILR;
/


create or replace package body PKG_LEASE_ILR as
   --**************************************************************************
   --                            VARIABLES
   --**************************************************************************

   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *   @@RETURN
   *      number: the revision added for success
   *            -1 for failure
   */
   function F_NEWREVISION(A_ILR_ID number) return number is

      L_STATUS           varchar2(2000);
      L_REVISION         number;
      L_CURRENT_REVISION number;

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW Revision for: ' || TO_CHAR(A_ILR_ID));

      L_STATUS := 'Get current revision';
      select CURRENT_REVISION into L_CURRENT_REVISION from LS_ILR where ILR_ID = A_ILR_ID;
      PKG_PP_LOG.P_WRITE_MESSAGE('   COPY Revision: ' || TO_CHAR(L_CURRENT_REVISION));

      L_STATUS := 'Get new revision';
      select max(REVISION) + 1 into L_REVISION from LS_ILR_APPROVAL where ILR_ID = A_ILR_ID;
      PKG_PP_LOG.P_WRITE_MESSAGE('   NEW Revision: ' || TO_CHAR(L_REVISION));

      L_STATUS := 'LOAD ilr_approval';
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select A.ILR_ID, L_REVISION, A.APPROVAL_TYPE_ID, 1
           from LS_ILR_APPROVAL A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_options';
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT
          --, payment_shift
          )
         select A.ILR_ID,
                L_REVISION,
                A.PURCHASE_OPTION_TYPE_ID,
                A.PURCHASE_OPTION_AMT,
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                A.TERMINATION_AMT
         --, payment_shift
           from LS_ILR_OPTIONS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_amounts';
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select A.ILR_ID,
                L_REVISION,
                SET_OF_BOOKS_ID,
                NET_PRESENT_VALUE,
                INTERNAL_RATE_RETURN,
                CAPITAL_COST,
                CURRENT_LEASE_COST,
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_payment_term';
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10)
         select A.ILR_ID,
                L_REVISION,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10
           from LS_ILR_PAYMENT_TERM A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_asset_map';
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, REVISION, LS_ASSET_ID)
         select A.ILR_ID, L_REVISION, A.LS_ASSET_ID
           from LS_ILR_ASSET_MAP A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;


      return L_REVISION;

   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
   end F_NEWREVISION;

   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the passed in revision information as the starting point, and copy everything, including the schedule
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *      number: in: a_revision = the revision to copy
   *   @@RETURN
   *      number: new revision number for success
   *            -1 for failure
   */
   function F_COPYREVISION(A_ILR_ID   number,
                           A_REVISION number) return number is

      L_STATUS   varchar2(2000);
      L_REVISION number;
      RTN        number;
      IS_AUTO    varchar2(254);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW Revision for: ' || TO_CHAR(A_ILR_ID) ||
                                 ' copied from revision: ' || TO_CHAR(A_REVISION));

      L_STATUS := 'Get new revision';
      select max(REVISION) + 1
        into L_REVISION
        from LS_ILR_APPROVAL
       where ILR_ID = A_ILR_ID
         and REVISION < 9999;
      PKG_PP_LOG.P_WRITE_MESSAGE('   NEW Revision: ' || TO_CHAR(L_REVISION));

      L_STATUS := 'LOAD ilr_approval';
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select A.ILR_ID, L_REVISION, A.APPROVAL_TYPE_ID, 1
           from LS_ILR_APPROVAL A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION;

      L_STATUS := 'LOAD ilr_options';
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT
          --, payment_shift
          )
         select A.ILR_ID,
                L_REVISION,
                A.PURCHASE_OPTION_TYPE_ID,
                A.PURCHASE_OPTION_AMT,
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                A.TERMINATION_AMT
         --, payment_shift
           from LS_ILR_OPTIONS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION;

      L_STATUS := 'LOAD ilr_amounts';
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select A.ILR_ID,
                L_REVISION,
                SET_OF_BOOKS_ID,
                NET_PRESENT_VALUE,
                INTERNAL_RATE_RETURN,
                CAPITAL_COST,
                CURRENT_LEASE_COST,
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION;

      L_STATUS := 'LOAD ilr_payment_term';
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10)
         select A.ILR_ID,
                L_REVISION,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10
           from LS_ILR_PAYMENT_TERM A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION;

      L_STATUS := 'LOAD ilr_asset_map';
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, REVISION, LS_ASSET_ID)
         select A.ILR_ID, L_REVISION, A.LS_ASSET_ID
           from LS_ILR_ASSET_MAP A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = A_REVISION;

      L_STATUS := 'LOAD ilr_schedule';
      insert into LS_ILR_SCHEDULE
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
          EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
          EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
          EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
          EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
          CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
          CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          IS_OM)
         select ILR_ID,
                L_REVISION,
                SET_OF_BOOKS_ID,
                month,
                RESIDUAL_AMOUNT,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                INTEREST_ACCRUAL,
                PRINCIPAL_ACCRUAL,
                INTEREST_PAID,
                PRINCIPAL_PAID,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                IS_OM
           from LS_ILR_SCHEDULE
          where ILR_ID = A_ILR_ID
            and REVISION = A_REVISION;

      L_STATUS := 'LOAD asset_schedule';
      insert into LS_ASSET_SCHEDULE
         (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
          EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
          EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
          EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
          EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
          CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
          CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          IS_OM)
         select LS_ASSET_ID,
                L_REVISION,
                SET_OF_BOOKS_ID,
                month,
                RESIDUAL_AMOUNT,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                INTEREST_ACCRUAL,
                PRINCIPAL_ACCRUAL,
                INTEREST_PAID,
                PRINCIPAL_PAID,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                IS_OM
           from LS_ASSET_SCHEDULE
          where LS_ASSET_ID in (select LS_ASSET_ID
                                  from LS_ILR_ASSET_MAP
                                 where ILR_ID = A_ILR_ID
                                   and REVISION = A_REVISION)
            and REVISION = A_REVISION;


      return L_REVISION;

   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
   end F_COPYREVISION;

	/*
	*   @@DESCRIPTION
	*      This function converts forecasts into new "real" revisions for all in-service ILR's
	*      NO COMMITS or ROLLBACK in this function
	*   @@PARAMS
	*      none
	*   @@RETURN
	*      number: 1 for success
	*            -1 for failure
	*/
	function F_CONVERTFORECASTS return number is

		L_STATUS varchar2(2000);
		L_IDS PKG_LEASE_CALC.NUM_ARRAY;

	begin
		return F_CONVERTFORECASTS(L_IDS);

	exception
		when others then
			L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
			PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

			return -1;
	end F_CONVERTFORECASTS;

   /*
   *   @@DESCRIPTION
   *      This function converts forecasts into new "real" revisions for the passed in array of ILR's
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      none
   *   @@RETURN
   *      number: 1 for success
   *            -1 for failure
   */
   function F_CONVERTFORECASTS(A_ILR_IDS in PKG_LEASE_CALC.NUM_ARRAY) return number is

      L_STATUS varchar2(2000);
      L_RTN    number;
	  L_ID_COUNT number;
	  L_IDS T_NUM_ARRAY;

   begin
	  PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_ILR.F_CONVERTFORECASTS');

      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('Converting forecast revisions');

	  L_STATUS := 'Initializing ILR Array';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
	  L_ID_COUNT := A_ILR_IDS.count;
	  if L_ID_COUNT = 0 then
		PKG_PP_LOG.P_WRITE_MESSAGE('-- Converting forecasts for all in-service ILR''s');
	  elsif L_ID_COUNT = 1 then
		PKG_PP_LOG.P_WRITE_MESSAGE('-- Converting forecasts for 1 ILR');
	  else
		PKG_PP_LOG.P_WRITE_MESSAGE('-- Converting forecasts for '||to_char(L_ID_COUNT)||' ILR''s');
	  end if;

	  L_IDS := T_NUM_ARRAY();
	  for I in 1..A_ILR_IDS.count
	  loop
		L_IDS.extend;
		L_IDS(I) := A_ILR_IDS(I);
	  end loop;

      L_STATUS := 'Loading ILR approvals';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select A.ILR_ID,
                (select max(REVISION) + 1
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                A.APPROVAL_TYPE_ID,
                1
           from LS_ILR_APPROVAL A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading ILR options';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT
          --, payment_shift
          )
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                A.PURCHASE_OPTION_TYPE_ID,
                A.PURCHASE_OPTION_AMT,
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                A.TERMINATION_AMT
         --, payment_shift
           from LS_ILR_OPTIONS A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading ILR amounts';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                SET_OF_BOOKS_ID,
                NET_PRESENT_VALUE,
                INTERNAL_RATE_RETURN,
                CAPITAL_COST,
                CURRENT_LEASE_COST,
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading payment terms';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10)
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10
           from LS_ILR_PAYMENT_TERM A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading ilr_asset_map';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, REVISION, LS_ASSET_ID)
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                A.LS_ASSET_ID
           from LS_ILR_ASSET_MAP A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading ILR schedule';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_SCHEDULE
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
          EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
          EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
          EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
          EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
          CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
          CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          IS_OM)
         select SCH.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                SET_OF_BOOKS_ID,
                month,
                RESIDUAL_AMOUNT,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                INTEREST_ACCRUAL,
                PRINCIPAL_ACCRUAL,
                INTEREST_PAID,
                PRINCIPAL_PAID,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                IS_OM
           from LS_ILR_SCHEDULE SCH, LS_ILR ILR
          where SCH.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and REVISION = 9999;

      L_STATUS := 'Loading asset schedule';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ASSET_SCHEDULE
         (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4,
          EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8,
          EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3,
          EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8,
          EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2,
          CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6,
          CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          IS_OM)
         select LS_ASSET_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                SET_OF_BOOKS_ID,
                month,
                RESIDUAL_AMOUNT,
                TERM_PENALTY,
                BPO_PRICE,
                BEG_CAPITAL_COST,
                END_CAPITAL_COST,
                BEG_OBLIGATION,
                END_OBLIGATION,
                BEG_LT_OBLIGATION,
                END_LT_OBLIGATION,
                INTEREST_ACCRUAL,
                PRINCIPAL_ACCRUAL,
                INTEREST_PAID,
                PRINCIPAL_PAID,
                EXECUTORY_ACCRUAL1,
                EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3,
                EXECUTORY_ACCRUAL4,
                EXECUTORY_ACCRUAL5,
                EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7,
                EXECUTORY_ACCRUAL8,
                EXECUTORY_ACCRUAL9,
                EXECUTORY_ACCRUAL10,
                EXECUTORY_PAID1,
                EXECUTORY_PAID2,
                EXECUTORY_PAID3,
                EXECUTORY_PAID4,
                EXECUTORY_PAID5,
                EXECUTORY_PAID6,
                EXECUTORY_PAID7,
                EXECUTORY_PAID8,
                EXECUTORY_PAID9,
                EXECUTORY_PAID10,
                CONTINGENT_ACCRUAL1,
                CONTINGENT_ACCRUAL2,
                CONTINGENT_ACCRUAL3,
                CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5,
                CONTINGENT_ACCRUAL6,
                CONTINGENT_ACCRUAL7,
                CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9,
                CONTINGENT_ACCRUAL10,
                CONTINGENT_PAID1,
                CONTINGENT_PAID2,
                CONTINGENT_PAID3,
                CONTINGENT_PAID4,
                CONTINGENT_PAID5,
                CONTINGENT_PAID6,
                CONTINGENT_PAID7,
                CONTINGENT_PAID8,
                CONTINGENT_PAID9,
                CONTINGENT_PAID10,
                IS_OM
           from LS_ASSET_SCHEDULE, LS_ILR ILR
          where LS_ASSET_SCHEDULE.LS_ASSET_ID in
                (select LS_ASSET_ID
                   from LS_ILR_ASSET_MAP
                  where ILR_ID = ILR.ILR_ID
                    and REVISION = (select max(REVISION)
                                      from LS_ILR_APPROVAL
                                     where ILR_ID = ILR.ILR_ID
                                       and REVISION < 9999))
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and REVISION = 9999;

	  PKG_PP_LOG.P_END_LOG;
	  PKG_PP_ERROR.REMOVE_MODULE_NAME;
      return 1;

   exception
      when others then
		 PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end F_CONVERTFORECASTS;

   /*
   *   @@DESCRIPTION
   *      This function copies an ILR into a new ILR
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new ilr
   *    number: in: a_pct = the percent to transfer
   *   @@RETURN
   *      number: the ls_ilr_id for success
   *            -1 for failure
   */
   function F_COPYILR(A_ILR_ID number,
                      A_PCT    number) return number is

      L_STATUS           varchar2(2000);
      L_REVISION         number;
      L_CURRENT_REVISION number;
      L_ILR_ID           number;

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW ILR from: ' || TO_CHAR(A_ILR_ID));

      L_STATUS := 'Get current revision';
      select CURRENT_REVISION into L_CURRENT_REVISION from LS_ILR where ILR_ID = A_ILR_ID;
      PKG_PP_LOG.P_WRITE_MESSAGE('   Revision: ' || TO_CHAR(L_CURRENT_REVISION));

      select LS_ILR_SEQ.NEXTVAL into L_ILR_ID from DUAL;
      PKG_PP_LOG.P_WRITE_MESSAGE('TO ILR: ' || TO_CHAR(L_ILR_ID));

      L_STATUS   := 'Get new revision';
      L_REVISION := 1;

      L_STATUS := 'LOAD ilr';
      insert into LS_ILR
         (ILR_ID, ILR_NUMBER, LEASE_ID, COMPANY_ID, EST_IN_SVC_DATE, FINAL_EOT_DATE,
          INTERIM_INTEREST_BEGIN_DATE, EXTERNAL_ILR, ILR_STATUS_ID, ILR_GROUP_ID, NOTES,
          CURRENT_REVISION, WORKFLOW_TYPE_ID)
         select L_ILR_ID,
                '(TRF' || (select 1 + count(1)
                             from LS_ILR LL
                            where LL.ILR_NUMBER like '(TRF%) ' || A.ILR_NUMBER) || ') ' ||
                A.ILR_NUMBER,
                A.LEASE_ID,
                A.COMPANY_ID,
                A.EST_IN_SVC_DATE,
                A.FINAL_EOT_DATE,
                A.INTERIM_INTEREST_BEGIN_DATE,
                A.EXTERNAL_ILR,
                1,
                A.ILR_GROUP_ID,
                A.NOTES,
                L_REVISION,
                A.WORKFLOW_TYPE_ID
           from LS_ILR A
          where A.ILR_ID = A_ILR_ID;

      L_STATUS := 'LOAD class codes';
      insert into LS_ILR_CLASS_CODE
         (CLASS_CODE_ID, ILR_ID, "VALUE")
         select A.CLASS_CODE_ID, L_ILR_ID, a."VALUE"
           from LS_ILR_CLASS_CODE A
          where A.ILR_ID = A_ILR_ID;

      L_STATUS := 'LOAD ilr_account';
      insert into LS_ILR_ACCOUNT
         (ILR_ID, INT_ACCRUAL_ACCOUNT_ID, INT_EXPENSE_ACCOUNT_ID, EXEC_ACCRUAL_ACCOUNT_ID,
          EXEC_EXPENSE_ACCOUNT_ID, CONT_ACCRUAL_ACCOUNT_ID, CONT_EXPENSE_ACCOUNT_ID)
         select L_ILR_ID,
                A.INT_ACCRUAL_ACCOUNT_ID,
                A.INT_EXPENSE_ACCOUNT_ID,
                A.EXEC_ACCRUAL_ACCOUNT_ID,
                A.EXEC_EXPENSE_ACCOUNT_ID,
                A.CONT_ACCRUAL_ACCOUNT_ID,
                A.CONT_EXPENSE_ACCOUNT_ID
           from LS_ILR_ACCOUNT A
          where A.ILR_ID = A_ILR_ID;

      L_STATUS := 'LOAD ilr_approval';
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select L_ILR_ID, L_REVISION, A.APPROVAL_TYPE_ID, 1
           from LS_ILR_APPROVAL A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_options';
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT
          --, payment_shift
          )
         select L_ILR_ID,
                L_REVISION,
                A.PURCHASE_OPTION_TYPE_ID,
                ROUND(A_PCT * A.PURCHASE_OPTION_AMT, 2),
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                ROUND(A_PCT * A.TERMINATION_AMT, 2)
         --, payment_shift
           from LS_ILR_OPTIONS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_amounts';
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select L_ILR_ID,
                L_REVISION,
                A.SET_OF_BOOKS_ID,
                ROUND(A_PCT * NET_PRESENT_VALUE, 2),
                INTERNAL_RATE_RETURN,
                ROUND(A_PCT * CAPITAL_COST, 2),
                ROUND(A_PCT * CURRENT_LEASE_COST, 2),
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_payment_term';
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10)
         select L_ILR_ID,
                L_REVISION,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                ROUND(A_PCT * EST_EXECUTORY_COST, 2),
                ROUND(A_PCT * PAID_AMOUNT, 2),
                ROUND(A_PCT * CONTINGENT_AMOUNT, 2),
                CURRENCY_TYPE_ID,
                ROUND(A_PCT * C_BUCKET_1, 2),
                ROUND(A_PCT * C_BUCKET_2, 2),
                ROUND(A_PCT * C_BUCKET_3, 2),
                ROUND(A_PCT * C_BUCKET_4, 2),
                ROUND(A_PCT * C_BUCKET_5, 2),
                ROUND(A_PCT * C_BUCKET_6, 2),
                ROUND(A_PCT * C_BUCKET_7, 2),
                ROUND(A_PCT * C_BUCKET_8, 2),
                ROUND(A_PCT * C_BUCKET_9, 2),
                ROUND(A_PCT * C_BUCKET_10, 2),
                ROUND(A_PCT * E_BUCKET_1, 2),
                ROUND(A_PCT * E_BUCKET_2, 2),
                ROUND(A_PCT * E_BUCKET_3, 2),
                ROUND(A_PCT * E_BUCKET_4, 2),
                ROUND(A_PCT * E_BUCKET_5, 2),
                ROUND(A_PCT * E_BUCKET_6, 2),
                ROUND(A_PCT * E_BUCKET_7, 2),
                ROUND(A_PCT * E_BUCKET_8, 2),
                ROUND(A_PCT * E_BUCKET_9, 2),
                ROUND(A_PCT * E_BUCKET_10, 2)
           from LS_ILR_PAYMENT_TERM A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;


      return L_ILR_ID;

   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
   end F_COPYILR;

   /*
   *   @@DESCRIPTION
   *      This function copies an asset into another leased asset
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ls_asset_id = the asset to copy
   *    number: in: a_pct. Percent to copy
   *    number: in: a_qty.  The quantity to copy
   *   @@RETURN
   *      number: The ls_asset_id added for success
   *            -1 for failure
   */
   function F_COPYASSET(A_LS_ASSET_ID number,
                        A_PCT         number,
                        A_QTY         number) return number is

      L_STATUS     varchar2(2000);
      L_ASSET_ID   number;
      TEMP_DERIVER CR_DERIVER_TYPE;
      L_SQLS       varchar2(2000);
      L_FIELDS     varchar2(2000);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW Asset from: ' || TO_CHAR(A_LS_ASSET_ID));

      select LS_ASSET_SEQ.NEXTVAL into L_ASSET_ID from DUAL;
      PKG_PP_LOG.P_WRITE_MESSAGE('TO Asset: ' || TO_CHAR(L_ASSET_ID));

      L_STATUS := 'LOAD asset';
      insert into LS_ASSET
         (LS_ASSET_ID, LEASED_ASSET_NUMBER, LS_ASSET_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION,
          CURRENT_LEASE_DATE, INTERIM_INTEREST_BEGIN_DATE, GL_POSTING_BEGIN_DATE,
          TERMINATION_PENALTY_AMOUNT, EXPECTED_LIFE, ECONOMIC_LIFE, FMV, PROPERTY_GROUP_ID,
          UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID, RETIREMENT_UNIT_ID, WORK_ORDER_ID,
          FUNC_CLASS_ID, ASSET_LOCATION_ID, IN_SERVICE_DATE, SERIAL_NUMBER, ACTUAL_RESIDUAL_AMOUNT,
          GUARANTEED_RESIDUAL_AMOUNT, NOTES, COMPANY_ID, QUANTITY, IS_EARLY_RET, TAX_ASSET_LOCATION_ID, DEPARTMENT_ID)
         select L_ASSET_ID,
                '(TRF' ||
                (select 1 + count(1)
                   from LS_ASSET LL
                  where LL.LEASED_ASSET_NUMBER like '(TRF%) ' || A.LEASED_ASSET_NUMBER) || ') ' ||
                A.LEASED_ASSET_NUMBER,
                1,
                '(TRF) ' || DESCRIPTION,
                '(TRF) ' || LONG_DESCRIPTION,
                CURRENT_LEASE_DATE,
                INTERIM_INTEREST_BEGIN_DATE,
                GL_POSTING_BEGIN_DATE,
                ROUND(A_PCT * TERMINATION_PENALTY_AMOUNT, 2),
                EXPECTED_LIFE,
                ECONOMIC_LIFE,
                ROUND(A_PCT * FMV, 2),
                PROPERTY_GROUP_ID,
                UTILITY_ACCOUNT_ID,
                BUS_SEGMENT_ID,
                SUB_ACCOUNT_ID,
                RETIREMENT_UNIT_ID,
                WORK_ORDER_ID,
                FUNC_CLASS_ID,
                ASSET_LOCATION_ID,
                IN_SERVICE_DATE,
                SERIAL_NUMBER,
                ROUND(A_PCT * ACTUAL_RESIDUAL_AMOUNT, 2),
                ROUND(A_PCT * GUARANTEED_RESIDUAL_AMOUNT, 2),
                NOTES,
                COMPANY_ID,
                A_QTY,
                0,
                TAX_ASSET_LOCATION_ID,
                DEPARTMENT_ID
           from LS_ASSET A
          where A.LS_ASSET_ID = A_LS_ASSET_ID;

      L_STATUS := 'LOAD class codes';

      insert into LS_ASSET_CLASS_CODE
         (CLASS_CODE_ID, LS_ASSET_ID, "VALUE")
         select A.CLASS_CODE_ID, L_ASSET_ID, a."VALUE"
           from LS_ASSET_CLASS_CODE A
          where A.LS_ASSET_ID = A_LS_ASSET_ID;

      L_STATUS := 'LOAD cr deriver control';
      L_FIELDS := 'SCO_BILLING_TYPE_ID, VALIDATION_MESSAGE, DESCRIPTION, PERCENT, TYPE, ';
      for COL in (select ELEMENT_COLUMN from CR_ELEMENTS)
      loop
         L_FIELDS := L_FIELDS || COL.ELEMENT_COLUMN || ', ';
      end loop;
      L_FIELDS := SUBSTR(L_FIELDS, 1, LENGTH(L_FIELDS) - 2);
      L_SQLS   := 'insert into CR_DERIVER_CONTROL (STRING, ID, ' || L_FIELDS || ')';
      L_SQLS   := L_SQLS || ' select ''' || TO_CHAR(L_ASSET_ID) ||
                  ''', (select max(ID) from CR_DERIVER_CONTROL)+rownum, ' || L_FIELDS;
      L_SQLS   := L_SQLS ||
                  ' from CR_DERIVER_CONTROL where lower(type) in (''lessee'', ''lessee offset'')';
      L_SQLS   := L_SQLS || ' and string = ''' || TO_CHAR(A_LS_ASSET_ID) || '''';

      L_STATUS := L_STATUS || '. SQL: ' || L_SQLS;
      execute immediate L_SQLS;
      PKG_PP_LOG.P_WRITE_MESSAGE('Ending F_COPYASSET');

      return L_ASSET_ID;

   exception
      when others then
        PKG_PP_LOG.P_WRITE_MESSAGE('ERROR!');
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         return -1;
   end F_COPYASSET;

   function F_GETTAXES(A_ILR_ID number)
                     return varchar2 is
      L_STATUS varchar2(400);

   begin

      L_STATUS := 'Getting taxes for ILR_ID: '||to_char(A_ILR_ID);
      insert into LS_ASSET_TAX_MAP
      (LS_ASSET_ID, TAX_LOCAL_ID, STATUS_CODE_ID)
      (select LA.LS_ASSET_ID LS_ASSET_ID, TL.TAX_LOCAL_ID TAX_LOCAL_ID, 1 STATUS_CODE_ID
        from LS_ASSET           LA,
            LS_TAX_LOCAL       TL,
            LS_ILR,
            LS_LEASE,
            LS_LEASE_OPTIONS,
            LS_TAX_STATE_RATES,
            ASSET_LOCATION
       where LA.ILR_ID = LS_ILR.ILR_ID
         and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID
         and LS_LEASE.LEASE_ID = LS_LEASE_OPTIONS.LEASE_ID
         and LS_LEASE.CURRENT_REVISION = LS_LEASE_OPTIONS.REVISION
         and LS_LEASE_OPTIONS.TAX_SUMMARY_ID = TL.TAX_SUMMARY_ID
         and TL.TAX_LOCAL_ID = LS_TAX_STATE_RATES.TAX_LOCAL_ID
         and LS_TAX_STATE_RATES.STATE_ID = ASSET_LOCATION.STATE_ID
         and LA.TAX_ASSET_LOCATION_ID = ASSET_LOCATION.ASSET_LOCATION_ID
         and LA.ILR_ID = A_ILR_ID
      union
      select LA.LS_ASSET_ID LS_ASSET_ID, TL.TAX_LOCAL_ID TAX_LOCAL_ID, 1 STATUS_CODE_ID
        from LS_ASSET              LA,
            LS_TAX_LOCAL          TL,
            LS_ILR,
            LS_LEASE,
            LS_LEASE_OPTIONS,
            LS_TAX_DISTRICT_RATES,
            ASSET_LOCATION
       where LA.ILR_ID = LS_ILR.ILR_ID
         and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID
         and LA.ILR_ID = A_ILR_ID
         and LS_LEASE.LEASE_ID = LS_LEASE_OPTIONS.LEASE_ID
         and LS_LEASE.CURRENT_REVISION = LS_LEASE_OPTIONS.REVISION
         and LS_LEASE_OPTIONS.TAX_SUMMARY_ID = TL.TAX_SUMMARY_ID
         and TL.TAX_LOCAL_ID = LS_TAX_DISTRICT_RATES.TAX_LOCAL_ID
         and LS_TAX_DISTRICT_RATES.LS_TAX_DISTRICT_ID = ASSET_LOCATION.LS_TAX_DISTRICT_ID
         and LA.TAX_ASSET_LOCATION_ID = ASSET_LOCATION.ASSET_LOCATION_ID
      minus --subtract out the assets that are already in the mapping table
      select LS_ASSET_ID, TAX_LOCAL_ID, 1
        from LS_ASSET_TAX_MAP);


      return 'OK';
   exception
      when others then
         return L_STATUS;
   end F_GETTAXES;

--**************************************************************************
--                            Initialize Package
--**************************************************************************

end PKG_LEASE_ILR;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1113, 0, 10, 4, 3, 0, 37697, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037697_lease_PKG_LEASE_ILR.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
