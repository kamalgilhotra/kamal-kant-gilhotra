SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035534_pwrtax_transfer_window.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- ------------------------------------
|| 10.4.2.0 01/18/2014 Andrew Scott        maint-35448  MLP gui changes. Specifically:
||                                         1. transfer window uses a temp table version
||                                            of tax_transfer_control
||                                         2. creating a sequence to use for the
||                                            pkey on tax_transfer_control (no longer max + 1)
||============================================================================
*/

----drop new table, but don't error if it does not already exist
begin
   execute immediate 'drop table TAX_TEMP_TRANSFER_CONTROL';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_TEMP_TRANSFER_CONTROL does not exist or drop table priv not granted.');
end;
/

create global temporary table TAX_TEMP_TRANSFER_CONTROL
(
 TAX_TRANSFER_ID         number(22,0) default 0 not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18) default null,
 TAX_YEAR                number(24,2) default 0 not null,
 FROM_TRID               number(22,0) default 0 not null,
 TO_TRID                 number(22,0) default 0 not null,
 BOOK_AMOUNT             number(22,2),
 STATUS_ID               number(2,0) default 0,
 STATUS_COMMENT          varchar2(255),
 TO_TRID_OFFSET          number(22,0),
 TRANSFER_TYPE           varchar2(35),
 PACKAGE_ID              number(22,0),
 TO_TRID_DEF_GAIN        number(22,0),
 PROCEEDS                number(22,2),
 CREATE_TO_TRID          number(22,0),
 VERSION_ID              number(22,0),
 CALC_DEFERRED_GAIN      number(22,0),
 DEFERRED_GAIN_AMOUNT    number(22,2),
 COMPANY_ID              number(22,0),
 TAX_CLASS_ID            number(22,0),
 DEFERRED_TAX_SCHEMA_ID  number(22,0),
 TAX_DEPR_SCHEMA_ID_FROM number(22,0),
 TAX_DEPR_SCHEMA_ID_TO   number(22,0),
 DEF_GAIN_FROM_TAX_CLASS number(22,0),
 DEF_GAIN_TO_TAX_CLASS   number(22,0),
 DEF_GAIN_VINTAGE_ID     number(22,0),
 NOTES                   varchar2(2000)
) on commit PRESERVE ROWS;

create unique index PK_TEMP_TAX_TRANSFER_CONTROL on TAX_TEMP_TRANSFER_CONTROL(TAX_TRANSFER_ID);
create unique index TEMP_TAX_XFER_CTRL_UDX1_FTRID on TAX_TEMP_TRANSFER_CONTROL(FROM_TRID);

create index TAX_TEMP_XFER_CTRL_COMBO_IDX on TAX_TEMP_TRANSFER_CONTROL(FROM_TRID,
                                                                       TAX_YEAR,
                                                                       VERSION_ID,
                                                                       COMPANY_ID);

begin
   DBMS_STATS.SET_TABLE_STATS('PWRPLANT',
                              'TAX_TEMP_TRANSFER_CONTROL',
                              '',
                              null,
                              null,
                              1,
                              1,
                              36,
                              null);
end;
/


comment on table  TAX_TEMP_TRANSFER_CONTROL is '(T) [09] The Tax Temp Transfer Control is used for population and computation of transfer data during manual transfers.  As a final step, data from this table is populated into Tax Transfer Control for future reprocessing.';
comment on column TAX_TEMP_TRANSFER_CONTROL.TAX_TRANSFER_ID is 'system-assigned identifier of a particular tax transfer.';
comment on column TAX_TEMP_TRANSFER_CONTROL.TIME_STAMP is 'standard system-assigned time stamp used for audit purposes.';
comment on column TAX_TEMP_TRANSFER_CONTROL.USER_ID is 'standard system-assigned user id used for audit purposes.';
comment on column TAX_TEMP_TRANSFER_CONTROL.TAX_YEAR is 'calendar tax year of the transfer.  (the fraction is a sequential number for dealing with short tax years.)';
comment on column TAX_TEMP_TRANSFER_CONTROL.FROM_TRID is 'transfer from''s tax_record_id.';
comment on column TAX_TEMP_TRANSFER_CONTROL.TO_TRID is 'transfer to''s tax_record_id.';
comment on column TAX_TEMP_TRANSFER_CONTROL.BOOK_AMOUNT is 'book amount of the transfer.';
comment on column TAX_TEMP_TRANSFER_CONTROL.STATUS_ID is 'identifier of a particular status.';
comment on column TAX_TEMP_TRANSFER_CONTROL.STATUS_COMMENT is 'if the status is Error, this comment explains the reason for failure.';
comment on column TAX_TEMP_TRANSFER_CONTROL.TO_TRID_OFFSET is 'optional column holding tax record id of tax record made for offset calculation.';
comment on column TAX_TEMP_TRANSFER_CONTROL.TRANSFER_TYPE is 'system-assigned identifier to indicate which type of transfer.  possible values are "sec351","interco", or "transfer".';
comment on column TAX_TEMP_TRANSFER_CONTROL.PACKAGE_ID is 'system-assigned identifier for which package transfer is assigned.';
comment on column TAX_TEMP_TRANSFER_CONTROL.TO_TRID_DEF_GAIN is 'optional column holding tax record id of tax record made for deferred gain calculation.';
comment on column TAX_TEMP_TRANSFER_CONTROL.PROCEEDS is 'amount of salvage associated with this transfer.';
comment on column TAX_TEMP_TRANSFER_CONTROL.CREATE_TO_TRID is 'yes/no field designating whether a new tax record id needs to be made for the to transfer.';
comment on column TAX_TEMP_TRANSFER_CONTROL.VERSION_ID is 'system-assigned identifier of tax version that this transfer belongs to.';
comment on column TAX_TEMP_TRANSFER_CONTROL.CALC_DEFERRED_GAIN is 'yes/no field designating whether a deferred gain calculation needs to be performed.';
comment on column TAX_TEMP_TRANSFER_CONTROL.DEFERRED_GAIN_AMOUNT is 'optional column holding the amount of deferred gain calculated if a deferred gain calculation is being performed.';
comment on column TAX_TEMP_TRANSFER_CONTROL.COMPANY_ID is 'company the activity is being transferred to, in the system.';
comment on column TAX_TEMP_TRANSFER_CONTROL.TAX_CLASS_ID is 'tax class the activity is being transferred to, in the system.';
comment on column TAX_TEMP_TRANSFER_CONTROL.DEFERRED_TAX_SCHEMA_ID is 'deferred tax schema the activity is being transferred to, in the system.';
comment on column TAX_TEMP_TRANSFER_CONTROL.TAX_DEPR_SCHEMA_ID_FROM is 'the system-assigned tax depr schema of the ''from'' activity.';
comment on column TAX_TEMP_TRANSFER_CONTROL.TAX_DEPR_SCHEMA_ID_TO is 'the system-assigned tax depr schema of the ''to'' activity.';
comment on column TAX_TEMP_TRANSFER_CONTROL.DEF_GAIN_FROM_TAX_CLASS is 'tax class to be set up dynamically with an intercompany deferred gain (from company).';
comment on column TAX_TEMP_TRANSFER_CONTROL.DEF_GAIN_TO_TAX_CLASS is 'tax class to be set up dynamically with an intercompany deferred gain (to company).';
comment on column TAX_TEMP_TRANSFER_CONTROL.DEF_GAIN_VINTAGE_ID is 'vintage used for the calculated deferred gain.';
comment on column TAX_TEMP_TRANSFER_CONTROL.NOTES is 'User input note describing the nature of the transfer.';


----Add new columns needed for Temp table to the original table.  But keep in PLSQL block to prevent error
begin
   execute immediate 'alter table TAX_TRANSFER_CONTROL add DEFERRED_TAX_SCHEMA_ID number(22,0)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_TRANSFER_CONTROL already has DEFERRED_TAX_SCHEMA_ID or table alter not granted.');
end;
/

begin
   execute immediate 'alter table TAX_TRANSFER_CONTROL add NOTES varchar2(2000)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_TRANSFER_CONTROL already has NOTES or table alter not granted.');
end;
/

begin
   execute immediate 'alter table TAX_TRANSFER_CONTROL add DEF_GAIN_VINTAGE_ID number(22, 0)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_TRANSFER_CONTROL already has DEF_GAIN_VINTAGE_ID or table alter not granted.');
end;
/

comment on column TAX_TRANSFER_CONTROL.DEFERRED_TAX_SCHEMA_ID is 'deferred tax schema the activity is being transferred to, in the system.';
comment on column TAX_TRANSFER_CONTROL.NOTES is 'User input note describing the nature of the transfer.';
comment on column TAX_TRANSFER_CONTROL.DEF_GAIN_VINTAGE_ID is 'vintage used for the calculated deferred gain.';


----create a new sequence for the tax_transfer_id.  Make re-runnable.
declare
   CUR_MAX_TTC_ID number;
   CUR_MAX_TDT_ID number;
   SEQ_START_NO   number;
begin

   -- grab the max tax transfer id on tax_transfer_control
   select max(TAX_TRANSFER_ID) into CUR_MAX_TTC_ID from TAX_TRANSFER_CONTROL;

   if CUR_MAX_TTC_ID is null then
      CUR_MAX_TTC_ID := 0;
   end if;

   DBMS_OUTPUT.PUT_LINE('Max tax_transfer_id on tax_transfer_control : ' || CUR_MAX_TTC_ID);

   -- grab the max tax transfer id on tax_depreciation_transfer
   select max(TAX_TRANSFER_ID) into CUR_MAX_TDT_ID from TAX_DEPRECIATION_TRANSFER;

   if CUR_MAX_TDT_ID is null then
      CUR_MAX_TDT_ID := 0;
   end if;

   DBMS_OUTPUT.PUT_LINE('Max tax_transfer_id on tax_depreciation_transfer : ' || CUR_MAX_TDT_ID);

   ----which one is greater?  Use that number + 1 as the starting number for the new sequence
   if CUR_MAX_TTC_ID > CUR_MAX_TDT_ID then
      SEQ_START_NO := CUR_MAX_TTC_ID + 1;
   else
      SEQ_START_NO := CUR_MAX_TDT_ID + 1;
   end if;

   DBMS_OUTPUT.PUT_LINE('New sequence to start with : ' || SEQ_START_NO);

   ---- drop the existing sequence if it exists
   begin
      execute immediate 'drop public synonym TAX_TRANSFER_SEQ';
      DBMS_OUTPUT.PUT_LINE('Synonym TAX_TRANSFER_SEQ dropped.');
   exception
      when others then
         null; --Ignore exception if public synonym can not be dropped.
   end;

   begin
      execute immediate 'drop sequence TAX_TRANSFER_SEQ';
      DBMS_OUTPUT.PUT_LINE('Sequence TAX_TRANSFER_SEQ dropped.');
   exception
      when others then
         null; --Ignore exception if sequence can not be dropped.
   end;

   ---- create the new sequence
   execute immediate 'create sequence TAX_TRANSFER_SEQ start with ' || SEQ_START_NO;
   DBMS_OUTPUT.PUT_LINE('Sequence TAX_TRANSFER_SEQ created, starting with ' || SEQ_START_NO);

end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (924, 0, 10, 4, 2, 0, 35534, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035534_pwrtax_transfer_window.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;