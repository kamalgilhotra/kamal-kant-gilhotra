/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011206_tax_repairs7.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   04/16/2013 Alex P.         Point Release
||============================================================================
*/

alter table WORK_ORDER_TAX_STATUS drop constraint FK_WOTS_REPAIR_LOC_ROLLUP_ID;

delete from CLASS_CODE_VALUES
 where CLASS_CODE_ID in (select CLASS_CODE_ID from CLASS_CODE where DESCRIPTION = 'Tax Expensing');

-- Insert Tax Expensing class code, if it doesn't exist yet
insert into CLASS_CODE
   (CLASS_CODE_ID, DESCRIPTION, CWIP_INDICATOR, TAX_INDICATOR, CPR_INDICATOR, BUDGET_INDICATOR,
    LONG_DESCRIPTION, CLASS_CODE_TYPE, CLASS_CODE_CATEGORY)
   select distinct A.MAX_ID, 'Tax Expensing', 0, 3, 0, 0, 'Tax Expensing', 'Standard', 'Class Code'
     from CLASS_CODE, (select max(CLASS_CODE_ID) + 1 MAX_ID from CLASS_CODE) A
    where not exists (select ('x') from CLASS_CODE B where B.DESCRIPTION = 'Tax Expensing');

-- Change report numbers
update PP_REPORTS
   set REPORT_NUMBER = replace(REPORT_NUMBER, 'REPRS - 12', 'RPR - 0')
 where REPORT_NUMBER like 'REPRS - 12%';

update PP_REPORTS
   set REPORT_NUMBER = replace(REPORT_NUMBER, 'SITUS - 00', 'GEN - 1')
 where REPORT_NUMBER like 'SITUS - 00%';

update PP_REPORTS
   set REPORT_NUMBER = replace(REPORT_NUMBER, 'NETWK - 00', 'UOP - 2')
 where REPORT_NUMBER like 'NETWK - 00%';

update PP_REPORTS
   set REPORT_NUMBER = replace(REPORT_NUMBER, 'WMIS - 00', 'UOPWT - 3')
 where REPORT_NUMBER like 'WMIS - 00%';

update PP_REPORTS
   set REPORT_NUMBER = replace(REPORT_NUMBER, 'RBLKT - 00', 'ALLOC - 4')
 where REPORT_NUMBER like 'RBLKT - 00%';

update PP_REPORTS
   set PP_REPORT_ENVIR_ID = 7
 where REPORT_NUMBER like 'REP-00%'
   and ((SUBSTR(REPORT_NUMBER, -2, 2) between '01' and '18') or
       (SUBSTR(REPORT_NUMBER, -2, 2) = '24'));

update PP_REPORTS
   set REPORT_NUMBER = replace(REPORT_NUMBER, 'REP-00', 'RPR - 0')
 where REPORT_NUMBER like 'REP-00%';

update PP_REPORTS
   set DESCRIPTION = SUBSTR(replace(DESCRIPTION, 'De Min', 'Pretest'), 1, 35),
       LONG_DESCRIPTION = replace(LONG_DESCRIPTION, 'De Minimis Test', 'Pre-Test')
 where DESCRIPTION like '%De Min';

update PP_REPORTS
   set REPORT_NUMBER = REPORT_NUMBER || '0', SPECIAL_NOTE = null
 where REPORT_NUMBER like 'RPR - 0%'
    or REPORT_NUMBER like 'GEN - 1%'
    or REPORT_NUMBER like 'UOP - 2%'
    or REPORT_NUMBER like 'UOPWT - 3%'
    or REPORT_NUMBER like 'ALLOC - 4%';

update PPBASE_MENU_ITEMS
   set LABEL = 'General Method'
 where MODULE = 'REPAIRS'
   and LABEL = 'Pre-unitized';

update PPBASE_MENU_ITEMS
   set LABEL = 'Unit of Property Method'
 where MODULE = 'REPAIRS'
   and LABEL = 'Post-unitized';

update PPBASE_MENU_ITEMS set LABEL = replace(LABEL, '(Pre)', '(Gen)') where MODULE = 'REPAIRS';

update PPBASE_MENU_ITEMS set LABEL = replace(LABEL, '(Post)', '(UOP)') where MODULE = 'REPAIRS';

--update PP_DATAWINDOW_HINTS
--   set SELECT_NUMBER = 0, HINT = ' ', REASON = null
-- where DATAWINDOW = 'TAXREP UPDATE INSERT_ACTIVITY_ID';

update PP_REPORTS
   set SPECIAL_NOTE = 'REPAIRS'
 where REPORT_NUMBER like 'RPR - 00%'
   and (SUBSTR(REPORT_NUMBER, -2, 2) between '10' and '70')
   and PP_REPORT_ENVIR_ID <> 7;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'PWRPLANT',
          TO_TIMESTAMP('22-03-2013 16:41:30', 'DD-MM-YYYY HH24:MI:SS'),
          'Project Management Tax Repairs',
          'Project Management Tax Repairs grouped by Company',
          null,
          'dw_rpr_rpt_cwip_repairs',
          'REPAIRS',
          null,
          null,
          'RPR - 0080',
          null,
          null,
          null,
          13,
          100,
          43,
          34,
          1,
          3,
          null,
          null,
          TO_TIMESTAMP('22-03-2013 16:41:30', 'DD-MM-YYYY HH24:MI:SS'),
          null,
          null,
          0
     from PP_REPORTS;

-- Move Threshold Estimate under Reporting
update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 3, PARENT_IDENTIFIER = 'menu_wksp_reporting'
 where LABEL = 'Threshold Estimate'
   and IDENTIFIER = 'menu_wksp_threshold_estimate'
   and MODULE = 'REPAIRS';

-- Move Import under Config
update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 7, PARENT_IDENTIFIER = 'menu_wksp_config', MENU_LEVEL = 2
 where LABEL = 'Import'
   and IDENTIFIER = 'menu_wksp_import'
   and MODULE = 'REPAIRS';

-- Shift up menu items at the top menu level
update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER - 1
 where PARENT_IDENTIFIER = 'repairs_top'
   and ITEM_ORDER > 4
   and MODULE = 'REPAIRS';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (349, 0, 10, 4, 0, 0, 11206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011206_tax_repairs7.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;