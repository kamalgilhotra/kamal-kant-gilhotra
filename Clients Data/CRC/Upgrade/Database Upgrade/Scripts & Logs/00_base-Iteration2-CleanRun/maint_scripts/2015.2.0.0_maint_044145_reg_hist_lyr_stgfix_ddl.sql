/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044145_reg_hist_lyr_stgfix_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/17/2015 Andrew Scott   previously added inc process id to depr
||                                      stg tables.  but not defaulting to 0 breaks
||                                      the calc in a lot of cases.
||============================================================================
*/

ALTER TABLE DEPR_CALC_STG MODIFY INCREMENTAL_PROCESS_ID DEFAULT 0;
ALTER TABLE DEPR_CALC_STG MODIFY INCREMENTAL_PROCESS_ID NOT NULL;

ALTER TABLE CPR_DEPR_CALC_STG MODIFY INCREMENTAL_PROCESS_ID DEFAULT 0;
ALTER TABLE CPR_DEPR_CALC_STG MODIFY INCREMENTAL_PROCESS_ID NOT NULL;

ALTER TABLE DEPR_LEDGER_BLENDING_STG MODIFY INCREMENTAL_PROCESS_ID DEFAULT 0;
ALTER TABLE DEPR_LEDGER_BLENDING_STG MODIFY INCREMENTAL_PROCESS_ID NOT NULL;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2602, 0, 2015, 2, 0, 0, 044145, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044145_reg_hist_lyr_stgfix_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;