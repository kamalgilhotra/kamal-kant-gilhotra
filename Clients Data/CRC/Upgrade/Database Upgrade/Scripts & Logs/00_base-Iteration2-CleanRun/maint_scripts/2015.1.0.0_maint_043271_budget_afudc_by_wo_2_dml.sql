/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043271_budget_afudc_by_wo_2_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 3/15/2015  Chris Mardis     Ability to calculate budget afudc by work order
||============================================================================
*/

update BUDGET_AFUDC_CALC_CLOSING
set wo_work_order_id = -99
;

update BUDGET_AFUDC_CALC_BEG_BAL
set wo_work_order_id = -99
;

update BUDGET_AFUDC_CALC
set wo_work_order_id = -99
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2381, 0, 2015, 1, 0, 0, 43271, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043271_budget_afudc_by_wo_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;