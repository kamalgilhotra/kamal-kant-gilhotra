/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042728_pcm_old_sys_controls_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1 	  02/16/2015 Ryan Oliveria		 Delete unused system controls
|| 2015.1 	  03/24/2015 Ryan Oliveria		 PCM backout
||==========================================================================================
*/

/* These deletes should not be run
delete from PP_SYSTEM_CONTROL_COMPANY where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense User Object'));
delete from PP_SYSTEM_CONTROL_COMPANY where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test FPs - Always'));
delete from PP_SYSTEM_CONTROL_COMPANY where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test FP - New Rev'));
delete from PP_SYSTEM_CONTROL_COMPANY where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test FP - Monthly Ests'));
delete from PP_SYSTEM_CONTROL_COMPANY where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test FP - Monthly Fcst'));
delete from PP_SYSTEM_CONTROL_COMPANY where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test FP - Unit Ests'));
delete from PP_SYSTEM_CONTROL_COMPANY where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense User Object - WO'));
delete from PP_SYSTEM_CONTROL_COMPANY where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test WOs - Always Run'));
delete from PP_SYSTEM_CONTROL_COMPANY where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test WO - New Rev'));
delete from PP_SYSTEM_CONTROL_COMPANY where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test WO - Monthly Ests'));
delete from PP_SYSTEM_CONTROL_COMPANY where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test WO - Unit Ests'));
delete from PP_SYSTEM_CONTROL_COMPANY where lower(trim(CONTROL_NAME)) = lower(trim('Display Related Work Order tab'));
*/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2304, 0, 2015, 1, 0, 0, 042728, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042728_pcm_old_sys_controls_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;