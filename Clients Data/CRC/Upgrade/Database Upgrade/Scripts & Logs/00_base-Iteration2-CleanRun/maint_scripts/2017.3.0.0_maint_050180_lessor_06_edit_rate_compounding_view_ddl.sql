/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050180_lessor_06_edit_rate_compounding_view_ddl.sql
|| Description:	Create view to display ILR rates compounded by frequencies
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/20/2018 Andrew Hill    Edit view (add annaul compounded rate)
||============================================================================
*/

CREATE OR REPLACE VIEW v_lsr_ilr_rates_compounded AS
SELECT  lsr_ilr_rates.ilr_id,
        lsr_ilr_rates.revision,
        lsr_ilr_rates.rate_type_id,
        lsr_ilr_rate_types.DESCRIPTION AS rate_type_description,
        lsr_ilr_rates.rate AS rate_implicit,
        (POWER(1 + lsr_ilr_rates.rate, 3) - 1)/3 AS quarterly_compounded_rate, 
        (POWER(1 + lsr_ilr_rates.rate, 6) - 1)/6 AS  semiannual_compounded_rate,
        (POWER(1 + lsr_ilr_rates.rate, 12) - 1)/12 AS  annual_compounded_rate,
        POWER(1 + lsr_ilr_rates.rate, 12) - 1 AS annual_discount_rate
FROM lsr_ilr_rates
JOIN lsr_ilr_rate_types ON lsr_ilr_rates.rate_type_id = lsr_ilr_rate_types.rate_type_id;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4228, 0, 2017, 3, 0, 0, 50180, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050180_lessor_06_edit_rate_compounding_view_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;