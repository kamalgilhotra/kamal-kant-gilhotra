/*
||============================================================================================
|| Application: PowerPlant
|| File Name:   maint_047752_sys_add_any_query_hidden_field_flags_ddl.sql
||============================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================================
|| Version     Date       Revised By     Reason for Change
|| --------    ---------- -------------- -----------------------------------------------------
|| 2017.1.0.0  04/27/2017 David H        PP-47752 Enhance base queries to hide a field from the Filter Criteria and Results
||============================================================================================
*/

ALTER TABLE pp_any_query_criteria_fields
    ADD (hide_from_results NUMBER(1,0) DEFAULT 0 NOT NULL,
         hide_from_filters NUMBER(1,0) DEFAULT 0 NOT NULL);
    
COMMENT ON COLUMN pp_any_query_criteria_fields.hide_from_results IS 'This flag controls whether the specified field is hidden from the results grid. By default, fields are included in the results.';
COMMENT ON COLUMN pp_any_query_criteria_fields.hide_from_filters IS 'This flag controls whether the specified field is hidden from the query filters. By default, columns are included in the filters.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3463, 0, 2017, 1, 0, 0, 47752, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047752_sys_add_any_query_hidden_field_flags_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;