/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045763_jobserver_add_missing_schedule_security_keys_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2016.1.0.0 06/17/2016 Jared Watkins  Recreate Schedule security keys deleted previously
||============================================================================
*/

--add notification and input jobs to allowed Schedule jobs
INSERT INTO pp_web_security_perm_control
(objects, module_id, pseudo_component, SECTION, name)
VALUES
('JobService.Workflow.Schedule.Notification', 9, 'Schedule', 'Add/Clone', 'Notifications')
;

INSERT INTO pp_web_security_perm_control
(objects, module_id, pseudo_component, SECTION, name)
VALUES
('JobService.Workflow.Reschedule.Notification', 9, 'Schedule', 'Edit', 'Notifications')
;

INSERT INTO pp_web_security_perm_control
(objects, module_id, pseudo_component, SECTION, name)
VALUES
('JobService.Workflow.Delete.Notification', 9, 'Schedule', 'Delete', 'Notifications')
;

INSERT INTO pp_web_security_perm_control
(objects, module_id, pseudo_component, SECTION, name)
VALUES
('JobService.Workflow.Schedule.Input', 9, 'Schedule', 'Add/Clone', 'Input Jobs')
;

INSERT INTO pp_web_security_perm_control
(objects, module_id, pseudo_component, SECTION, name)
VALUES
('JobService.Workflow.Reschedule.Input', 9, 'Schedule', 'Edit', 'Input Jobs')
;

INSERT INTO pp_web_security_perm_control
(objects, module_id, pseudo_component, SECTION, name)
VALUES
('JobService.Workflow.Delete.Input', 9, 'Schedule', 'Delete', 'Input Jobs')
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3223, 0, 2016, 1, 0, 0, 045763, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045763_jobserver_add_missing_schedule_security_keys_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;