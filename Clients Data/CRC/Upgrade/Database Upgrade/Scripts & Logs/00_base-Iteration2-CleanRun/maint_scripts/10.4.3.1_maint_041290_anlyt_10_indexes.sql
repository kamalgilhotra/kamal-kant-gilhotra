/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041290_anlyt_10_indexes.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
||  1.0     09/06/2013 Scott Moody      Initial
|| 10.4.3.1 11/17/2014 Chad Theilman    Asset Analytics - Indexes
||============================================================================
*/

declare 
  doesIndexExist number := 0;
  
  begin
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_LOCATION_STATE','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_LOCATION_STATE ON PWRPLANT.PA_LOCATIONS (STATE)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_LOCATION_COUNTY','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_LOCATION_COUNTY ON PWRPLANT.PA_LOCATIONS (COUNTY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_COMP','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_LDGR_COMP ON PWRPLANT.PA_CPR_LEDGER(COMPANY_ID)';
		end;
	end if;
	
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_LOC','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_LDGR_LOC ON PWRPLANT.PA_CPR_LEDGER(ASSET_LOCATION_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_ACCT','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_LDGR_ACCT ON PWRPLANT.PA_CPR_LEDGER(ACCT_KEY)';
		end;
	end if;
	
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_PROP','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_LDGR_PROP ON PWRPLANT.PA_CPR_LEDGER(PROP_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_DEPR','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_LDGR_DEPR ON PWRPLANT.PA_CPR_LEDGER(DEPR_GROUP_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_VY','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_LDGR_VY ON PWRPLANT.PA_CPR_LEDGER(VINTAGE_YEAR)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_EF','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_LDGR_EF ON PWRPLANT.PA_CPR_LEDGER(EXCLUDE_FLAG)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_LDGR_LOCN','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_LDGR_LOCN ON PWRPLANT.PA_CPR_LEDGER(LOCN_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_VINTSPR_COMP','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_VINTSPR_COMP ON PWRPLANT.PA_VINTAGE_SPREADS(COMPANY_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_VINTSPR_ACCT','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_VINTSPR_ACCT ON PWRPLANT.PA_VINTAGE_SPREADS(ACCT_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_VINTSPR_LOCN','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_VINTSPR_LOCN ON PWRPLANT.PA_VINTAGE_SPREADS(LOCN_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_VINTSPR_DEPR','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_VINTSPR_DEPR ON PWRPLANT.PA_VINTAGE_SPREADS(DEPR_GROUP_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_VINTSPR_VY','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_VINTSPR_VY ON PWRPLANT.PA_VINTAGE_SPREADS(VINTAGE_YEAR)';
		end;	
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_COMP','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACT_COMP ON PWRPLANT.PA_CPR_ACTIVITY(COMPANY_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_LOC','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACT_LOC ON PWRPLANT.PA_CPR_ACTIVITY(ASSET_LOCATION_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_LOCN','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACT_LOCN ON PWRPLANT.PA_CPR_ACTIVITY(LOCN_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_ACCT','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACT_ACCT ON PWRPLANT.PA_CPR_ACTIVITY(ACCT_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_PROP','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACT_PROP ON PWRPLANT.PA_CPR_ACTIVITY(PROP_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_DEPR','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACT_DEPR ON PWRPLANT.PA_CPR_ACTIVITY(DEPR_GROUP_ID)';
		end;
	end if;
	
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_GL_POST','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACT_GL_POST ON PWRPLANT.PA_CPR_ACTIVITY(GL_POSTING_DATE)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_FERC','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACT_FERC ON PWRPLANT.PA_CPR_ACTIVITY(FERC_ACTIVITY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACT_EF','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACT_EF ON PWRPLANT.PA_CPR_ACTIVITY(EXCLUDE_FLAG)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACTSUM_COMP','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACTSUM_COMP ON PWRPLANT.PA_CPR_ACTSUM(COMPANY_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACTSUM_LOCN','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACTSUM_LOCN ON PWRPLANT.PA_CPR_ACTSUM(LOCN_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACTSUM_ACCT','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACTSUM_ACCT ON PWRPLANT.PA_CPR_ACTSUM(ACCT_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACTSUM_DEPR','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACTSUM_DEPR ON PWRPLANT.PA_CPR_ACTSUM(DEPR_GROUP_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACTSUM_GL_POST','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACTSUM_GL_POST ON PWRPLANT.PA_CPR_ACTSUM(GL_POSTING_YEAR)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_CPR_ACTSUM_FERC','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_CPR_ACTSUM_FERC ON PWRPLANT.PA_CPR_ACTSUM(FERC_ACTIVITY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_AVGAGES_COMP','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_AVGAGES_COMP ON PWRPLANT.PA_AVERAGE_AGES(COMPANY_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_AVGAGES_LOC','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_AVGAGES_LOC ON PWRPLANT.PA_AVERAGE_AGES(LOCN_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_AVGAGES_ACCT','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_AVGAGES_ACCT ON PWRPLANT.PA_AVERAGE_AGES(ACCT_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_AVGAGES_DEPR','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_AVGAGES_DEPR ON PWRPLANT.PA_AVERAGE_AGES(DEPR_GROUP_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_AVGAGES_ASOF','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_AVGAGES_ASOF ON PWRPLANT.PA_AVERAGE_AGES(PROCESS_YEAR)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_CPRBAL_COMP','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_CPRBAL_COMP ON PWRPLANT.PA_CPR_BALANCES(COMPANY_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_CPRBAL_LOC','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_CPRBAL_LOC ON PWRPLANT.PA_CPR_BALANCES(LOCN_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_CPRBAL_ACCT','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_CPRBAL_ACCT ON PWRPLANT.PA_CPR_BALANCES(ACCT_KEY)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_CPRBAL_DEPR','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_CPRBAL_DEPR ON PWRPLANT.PA_CPR_BALANCES(DEPR_GROUP_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_CPRBAL_ASOF','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_CPRBAL_ASOF ON PWRPLANT.PA_CPR_BALANCES(PROCESS_YEAR)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_RETWO_WONUM','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_RETWO_WONUM ON PWRPLANT.PA_RETIRE_WO(WO_NUMBER)';  
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_RETWO_COMP','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_RETWO_COMP ON PWRPLANT.PA_RETIRE_WO(COMPANY_ID)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_RETWO_BSEG','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_RETWO_BSEG ON PWRPLANT.PA_RETIRE_WO(BUS_SEGMENT)';
		end;
	end if;
		
	doesIndexExist := PWRPLANT.SYS_DOES_INDEX_EXIST('IDX_PA_RETWO_STAT','PWRPLANT');
	
	if doesIndexExist = 0 then
		begin
			execute immediate 'CREATE INDEX PWRPLANT.IDX_PA_RETWO_STAT ON PWRPLANT.PA_RETIRE_WO(WO_STATUS)';
		end;
	end if;
  end;
/




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2019, 0, 10, 4, 3, 1, 041290, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.1_maint_041290_anlyt_10_indexes.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;