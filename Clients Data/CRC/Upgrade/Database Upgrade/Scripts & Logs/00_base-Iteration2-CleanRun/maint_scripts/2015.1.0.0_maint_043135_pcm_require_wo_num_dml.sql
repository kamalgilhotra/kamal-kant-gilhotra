/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043135_pcm_require_wo_num_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/10/2015 Ryan Oliveria    Require WO Num
||============================================================================
*/

insert into PP_REQUIRED_TABLE_COLUMN
	(ID, TABLE_NAME, COLUMN_NAME, OBJECTPATH, DESCRIPTION, REQUIRED_COLUMN_EXPRESSION)
values
	(1021, 'work_order_control', 'work_order_number', 'uo_pcm_maint_info.dw_detail', 'Work Order Number', null);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2371, 0, 2015, 1, 0, 0, 043135, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043135_pcm_require_wo_num_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;