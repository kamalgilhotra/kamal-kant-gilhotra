/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030609_lease_sequences.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/10/2013 K. Peterson       Point release
||============================================================================
*/

create sequence LS_LEASE_SEQ
   minvalue 1
   start with 1
   increment by 1
   cache 20;

create sequence LS_ILR_SEQ
   minvalue 1
   start with 1
   increment by 1
   cache 20;

create sequence LS_ASSET_SEQ
   minvalue 1
   start with 1
   increment by 1
   cache 20;

create sequence LS_LESSOR_SEQ
   minvalue 1
   start with 1
   increment by 1
   cache 20;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (461, 0, 10, 4, 1, 0, 30609, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030609_lease_sequences.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
