/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029296_basegui.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.0.0 02/14/2013 Shaun Black    Point Release
||============================================================================
*/

--
-- Add a column to store the parent table's primary key column.  This is necessary when the PK column name isn't the same as the column name
-- on pp_import_column.
--
alter table PP_IMPORT_COLUMN add PARENT_TABLE_PK_COLUMN varchar2(35);

--
-- Populate the new column for tables where it is different than the import column.
--
update PP_IMPORT_COLUMN set PARENT_TABLE_PK_COLUMN = 'description' where PARENT_TABLE = 'activity_code';
update PP_IMPORT_COLUMN set PARENT_TABLE_PK_COLUMN = 'minor_location_id' where PARENT_TABLE = 'minor_location' and COLUMN_NAME = 'minor_location_id2';
update PP_IMPORT_COLUMN set PARENT_TABLE_PK_COLUMN = 'type_code_id' where PARENT_TABLE = 'prop_tax_type_code' and COLUMN_NAME = 'parent_type_code_id';
update PP_IMPORT_COLUMN set PARENT_TABLE_PK_COLUMN = 'parcel_type_id' where PARENT_TABLE = 'pt_parcel_type' and COLUMN_NAME = 'parcel_type_id_map';
update PP_IMPORT_COLUMN set PARENT_TABLE_PK_COLUMN = 'status_code_id' where PARENT_TABLE = 'status_code' and COLUMN_NAME = 'status';
update PP_IMPORT_COLUMN set PARENT_TABLE_PK_COLUMN = 'subledger_type_id' where PARENT_TABLE = 'subledger_control' and COLUMN_NAME = 'subledger_indicator';
update PP_IMPORT_COLUMN set PARENT_TABLE_PK_COLUMN = 'tax_status_id' where PARENT_TABLE = 'wo_tax_status' and COLUMN_NAME <> 'tax_status_id';
update PP_IMPORT_COLUMN set PARENT_TABLE_PK_COLUMN = 'yes_no_id' where PARENT_TABLE = 'yes_no';

--
-- Remove the parent table from any columns that are not being translated.
--
update PP_IMPORT_COLUMN
   set PARENT_TABLE = null
 where IMPORT_COLUMN_NAME is null
   and PARENT_TABLE is not null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (303, 0, 10, 4, 0, 0, 29296, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029296_basegui.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;