/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050970_lessor_04_reset_lessor_mc_amounts_view_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/25/2018  Alex Healey    Alter Lessor view v_lsr_ilr_mc_calc_amounts to its previous version
||============================================================================
*/
CREATE OR REPLACE VIEW v_lsr_ilr_mc_calc_amounts (
  ilr_id,
  revision,
  set_of_books_id,
  npv_lease_payments,
  npv_guaranteed_residual,
  npv_unguaranteed_residual,
  selling_profit_loss,
  beginning_lease_receivable,
  beginning_net_investment,
  cost_of_goods_sold,
  rate,
  ls_cur_type,
  currency_id,
  currency_display_symbol,
  iso_code
) AS
WITH cur AS (
  SELECT ls_currency_type_id AS ls_cur_type,
    currency_id,
    currency_display_symbol,
    iso_code,
    CASE ls_currency_type_id
      WHEN 1 THEN 1
      ELSE NULL
    END AS contract_approval_rate
  FROM currency
  CROSS JOIN ls_lease_currency_type
)
SELECT
lia.ilr_id,
lia.revision,
lia.set_of_books_id,
lia.npv_lease_payments * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS npv_lease_payments,
lia.npv_guaranteed_residual * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS npv_guaranteed_residual,
lia.npv_unguaranteed_residual * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS npv_unguaranteed_residual,
lia.selling_profit_loss * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS selling_profit_loss,
lia.beginning_lease_receivable * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS beginning_lease_receivable,
lia.beginning_net_investment * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS beginning_net_investment,
lia.cost_of_goods_sold * Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) AS cost_of_goods_sold,
Nvl(cur.contract_approval_rate, lio.in_service_exchange_rate) rate,
cur.ls_cur_type,
cur.currency_id,
cur.currency_display_symbol,
cur.iso_code
FROM lsr_ilr_amounts lia
	INNER JOIN lsr_ilr ilr ON (lia.ilr_id = ilr.ilr_id)
	INNER JOIN lsr_lease lease ON (ilr.lease_id = lease.lease_id)
	INNER JOIN lsr_ilr_options lio ON (lia.ilr_id = lio.ilr_id AND lia.revision = lio.revision)
	INNER JOIN currency_schema cs
    ON ilr.company_id = cs.company_id
	INNER JOIN cur
    ON cur.currency_id =
      CASE cur.ls_cur_type
        WHEN 1 THEN lease.contract_currency_id
        WHEN 2 THEN cs.currency_id
        ELSE NULL
      END
WHERE cs.currency_type_id = 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (10805, 0, 2018, 1, 0, 0, 50970, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_050970_lessor_04_reset_lessor_mc_amounts_view_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;