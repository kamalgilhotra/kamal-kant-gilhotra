/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030302_lease_mla_documents.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/28/2013 Kyle Peterson  Point release
||============================================================================
*/

alter table LS_LEASE add LEASE_GROUP_ID number(22,0);

/* This already exists
create table LS_LEASE_DOCUMENT
(
 LEASE_ID      number(22,0) not null,
 DOCUMENT_DATA blob,
 DOCUMENT_ID   number(22,0) not null,
 NOTES         varchar2(2000),
 DESCRIPTION   varchar2(35),
 FILE_NAME     varchar2(100) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 FILESIZE      number(22,0),
 FILE_LINK     varchar2(2000)
);

alter table LS_LEASE_DOCUMENT
   add constraint PK_LS_LEASE_DOCUMENT
       primary key (LEASE_ID, DOCUMENT_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_LEASE_DOCUMENT
   add constraint LS_LEASE_DOCUMENT_FK1
       foreign key (LEASE_ID)
       references LS_LEASE (LEASE_ID);
*/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (422, 0, 10, 4, 1, 0, 30302, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030302_lease_mla_documents.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
