/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045352_sys_drop_unused_packages_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 01/05/2017 Anand R		  Drop packages post_pkg and pp_datastore
||============================================================================
*/

drop package POST_PKG;

drop package PP_DATASTORE;

/* $$$ KKG, 20190710 - commented the synonym drop statements, as both of the below synonyms do not exist (start)
drop public synonym POST_PKG;

drop public synonym PP_DATASTORE;
$$$ KKG, 20190710, (end) */

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3353, 0, 2017, 1, 0, 0, 045352, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_045352_sys_drop_unused_packages_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;