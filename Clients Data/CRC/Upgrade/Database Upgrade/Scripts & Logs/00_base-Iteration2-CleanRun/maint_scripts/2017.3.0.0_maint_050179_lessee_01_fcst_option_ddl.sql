/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050179_lessee_01_fcst_option_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/06/2018 Shane "C" Ward	Add new column for Earnings Acct to Lessee Forecasting (Modified Retrospective)
||										Also add way to link Forecast ILR's around to in-service ones
||============================================================================
*/

ALTER TABLE ls_forecast_version ADD earnings_gl_acct_id NUMBER(22,0) NULL;

ALTER TABLE ls_forecast_version
  ADD CONSTRAINT ls_forecast_version_acct_fk FOREIGN KEY (
    earnings_gl_acct_id
  ) REFERENCES gl_account (
    gl_account_id
  );

CREATE TABLE LS_ASSET_FCST_EARNINGS
  (
     ls_asset_id     NUMBER(22, 0),
     revision        NUMBER(22, 0),
     set_of_books_id NUMBER(22, 0),
     month           DATE,
     earnings_amount NUMBER(22, 2),
     user_id         VARCHAR2(18) NULL,
     time_stamp      DATE NULL
  );

COMMENT ON COLUMN ls_asset_fcst_earnings.ls_asset_id IS 'System-assigned identifier of a particular leased asset.
The internal leased asset id within PowerPlan.';
COMMENT ON COLUMN ls_asset_fcst_earnings.revision IS 'The revision.';
COMMENT ON COLUMN ls_asset_fcst_earnings.set_of_books_id IS 'The internal set of books id within PowerPlant.';
COMMENT ON COLUMN ls_asset_fcst_earnings.month IS 'The month being processed.';
COMMENT ON COLUMN ls_asset_fcst_earnings.earnings_amount IS 'The earnings amount of the Asset at Conversion.';
COMMENT ON COLUMN ls_asset_fcst_earnings.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_asset_fcst_earnings.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

alter table ls_forecast_ilr_master add source_revision number(22,0);
alter table ls_forecast_ilr_master add in_service_revision number(22,0);

COMMENT on column ls_forecast_ilr_master.source_revision is 'Current revision of the ILR that the forecast is based off. To be used for reference in transition.';
COMMENT on column ls_forecast_ilr_master.in_service_revision is 'Revision number of the ILR that the forecast version puts into-service. To be used for reference in transition.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4164, 0, 2017, 3, 0, 0, 50179, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050179_lessee_01_fcst_option_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;