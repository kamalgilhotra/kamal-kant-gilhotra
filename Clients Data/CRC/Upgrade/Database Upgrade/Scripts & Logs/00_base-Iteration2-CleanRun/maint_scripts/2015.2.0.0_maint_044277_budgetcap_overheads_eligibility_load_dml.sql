/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044277_budgetcap_overheads_eligibility_load_dml.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By            Reason for Change
|| -------- ---------- -------------------   ----------------------------------------------
|| 2015.2   07/23/2015 Ryan Oliveria 		 Convert existing loading overheads eligibility
||========================================================================================
*/

insert into wo_fp_clear_over_bdg (work_order_id, clearing_id, include_exclude)
select woc.work_order_id, oh.clearing_id, 1 include_exclude
from clearing_wo_control_bdg oh, budget b, work_order_control woc
where oh.clearing_indicator = 2
and (oh.fp = 1 or oh.wo = 1)
--Budget Summary Eligibility
and (
	b.budget_summary_id in (
		select budget_summary_id
		from budget_summary_clear_dflt_bdg
		where clearing_id = oh.clearing_id )
	or b.budget_id in (
		select budget_id
		from wo_clear_over_bdg
		where clearing_id = oh.clearing_id
		and include_exclude = 1)
	or oh.target_type = 0 --If it's by org, then it doesn't have to pass sum eligibility
)
--Budget Organization Eligibility
and (
	b.budget_organization_id in (
		select budget_organization_id
		from budget_org_clear_dflt_bdg
		where clearing_id = oh.clearing_id )
	or b.budget_id in (
		select budget_id
		from wo_clear_over_bdg
		where clearing_id = oh.clearing_id
		and include_exclude = 1)
	or oh.target_type = 1 --It it's by sum, then it doesn't have to pass org eligibility
)
--Included Budget Exceptions
and b.budget_id not in (
	select budget_id
	from wo_clear_over_bdg
	where clearing_id = oh.clearing_id
	and include_exclude = 0)
and woc.budget_id = b.budget_id
--Only convert if WO/FP types have not been setup for this Overhead
and not exists (
	select 1
	from wo_type_clear_dflt_bdg
	where clearing_id = oh.clearing_id
)
and not exists (
	select 1
	from wo_fp_clear_over_bdg
	where clearing_id = oh.clearing_id
);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2714, 0, 2015, 2, 0, 0, 044277, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044277_budgetcap_overheads_eligibility_load_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;