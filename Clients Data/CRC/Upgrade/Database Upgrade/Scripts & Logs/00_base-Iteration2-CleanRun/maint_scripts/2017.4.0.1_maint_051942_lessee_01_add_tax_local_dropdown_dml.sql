/*
||=============================================================================
|| Application: PowerPlan
|| File Name:   maint_051942_lessee_01_add_tax_local_dropdown_dml.sql
||=============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.1  08/02/2018 Crystal Yura      Add Tax Local to Lease Deriver Dropdown Config Table
||============================================================================
*/

INSERT INTO LS_CR_DERIVER_DROPDOWN_CONFIG
  (COLUMN_NAME, DDDW_NAME, DISPLAY_COLUMN, DATA_COLUMN, SORT_ORDER)
  SELECT 'tax_local_id',
         'dddw_ls_tax_local',
         'description',
         'tax_local_id',
         3
    FROM DUAL
   WHERE NOT EXISTS (SELECT 1 FROM LS_CR_DERIVER_DROPDOWN_CONFIG WHERE LOWER(COLUMN_NAME) = 'tax_local_id');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8943, 0, 2017, 4, 0, 1, 51942, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.1_maint_051942_lessee_01_add_tax_local_dropdown_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;		   