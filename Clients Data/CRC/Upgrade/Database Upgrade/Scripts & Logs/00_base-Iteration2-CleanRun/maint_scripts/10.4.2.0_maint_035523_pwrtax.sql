/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035523_pwrtax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/10/2014 Julia Breuer   Point Release
||============================================================================
*/

--
-- Update the PowerTax menu to have a new Reporting section.  Remove the old Reporting subsections.
--
insert into pwrplant.ppbase_menu_items ( module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn ) values ( 'powertax', 'reporting', sysdate, user, 1, 5, 'Reporting', '', '', '', 1 );
insert into pwrplant.ppbase_menu_items ( module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn ) values ( 'powertax', 'reporting_reports', sysdate, user, 2, 1, 'Reports', '', 'reporting', '', 1 );
insert into pwrplant.ppbase_menu_items ( module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn ) values ( 'powertax', 'reporting_query', sysdate, user, 2, 2, 'Any Query', '', 'reporting', '', 0 );
insert into pwrplant.ppbase_menu_items ( module, menu_identifier, time_stamp, user_id, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn ) values ( 'powertax', 'reporting_verify', sysdate, user, 2, 3, 'Verify', '', 'reporting', '', 0 );

update pwrplant.ppbase_menu_items set parent_menu_identifier = 'depreciation' where module = 'powertax' and menu_identifier = 'depr_review_asset_grid';
update pwrplant.ppbase_menu_items set parent_menu_identifier = 'deferred_taxes' where module = 'powertax' and menu_identifier = 'deferred_review_tax_grid';

delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'depr_review_reports';
delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'depr_review_query';
delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'depr_review_verify';
delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'depr_review';
delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'deferred_review_reports';
delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'deferred_review_query';
delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'deferred_review_verify';
delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'deferred_review';
delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'forecast_review_reports';
delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'forecast_review_query';
delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'forecast_review_verify';
delete from pwrplant.ppbase_menu_items where module = 'powertax' and menu_identifier = 'forecast_review';

update pwrplant.ppbase_menu_items set menu_level = 2 where module = 'powertax' and menu_identifier = 'depr_review_asset_grid';
update pwrplant.ppbase_menu_items set menu_level = 2 where module = 'powertax' and menu_identifier = 'deferred_review_tax_grid';
update pwrplant.ppbase_menu_items set item_order = 1 where module = 'powertax' and menu_identifier = 'forecast_process';
update pwrplant.ppbase_menu_items set item_order = 2 where module = 'powertax' and menu_identifier = 'forecast_import';
update pwrplant.ppbase_menu_items set item_order = 3 where module = 'powertax' and menu_identifier = 'forecast_export';

--
-- Set up the Reports workspace.
--
insert into pwrplant.ppbase_workspace ( module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp ) values ( 'powertax', 'reporting_reports', sysdate, user, 'Reports', 'uo_tax_rpt_wksp_reports', 'Reports' );
update pwrplant.ppbase_menu_items set workspace_identifier = 'reporting_reports' where module = 'powertax' and menu_identifier = 'reporting_reports';

--
-- Set up the Asset Review workspace.
--
insert into pwrplant.ppbase_workspace ( module, workspace_identifier, time_stamp, user_id, label, workspace_uo_name, minihelp ) values ( 'powertax', 'depr_review_asset_grid', sysdate, user, 'Asset Grid', 'uo_tax_depr_asset_wksp_review', 'Asset Grid' );
update pwrplant.ppbase_menu_items set workspace_identifier = 'depr_review_asset_grid' where module = 'powertax' and menu_identifier = 'depr_review_asset_grid';

--
-- Update PowerTax report types.
--
update pwrplant.pp_report_type set description = 'PowerTax - Depreciation', sort_order = 1 where report_type_id = 29;
update pwrplant.pp_report_type set description = 'PowerTax - Forecast', sort_order = 4 where report_type_id = 30;
insert into pwrplant.pp_report_type ( report_type_id, time_stamp, user_id, description, sort_order ) values ( 129, sysdate, user, 'PowerTax - Deferred Tax', 3 );
insert into pwrplant.pp_report_type ( report_type_id, time_stamp, user_id, description, sort_order ) values ( 130, sysdate, user, 'PowerTax - Depreciation Overlay', 2 );

--
-- Create PowerTax time options.
--
insert into pwrplant.pp_reports_time_option ( pp_report_time_option_id, time_stamp, user_id, description, parameter_uo_name, dwname1, label1, keycolumn1, dwname2, label2 ) values ( 50, sysdate, user, 'PowerTax - Single Tax  Year', 'uo_ppbase_report_parms_dddw_one', 'dddw_tax_year_by_version', 'Tax Year', '', '', '' );
insert into pwrplant.pp_reports_time_option ( pp_report_time_option_id, time_stamp, user_id, description, parameter_uo_name, dwname1, label1, keycolumn1, dwname2, label2 ) values ( 51, sysdate, user, 'PowerTax - Tax Year Compare', 'uo_ppbase_report_parms_dddw_two', 'dddw_tax_year_by_version', 'Current Tax Year', '', 'dddw_tax_year_by_version', 'Prior Tax Year' );
insert into pwrplant.pp_reports_time_option ( pp_report_time_option_id, time_stamp, user_id, description, parameter_uo_name, dwname1, label1, keycolumn1, dwname2, label2 ) values ( 52, sysdate, user, 'PowerTax - Multi Tax Year', 'uo_ppbase_report_parms_multigrid', 'dw_tax_year_by_version_filter', 'Tax Years to Compare', 'tax_year', '', '' );

--
-- Create PowerTax filters.
--
insert into pwrplant.pp_reports_filter ( pp_report_filter_id, time_stamp, user_id, description, table_name, filter_uo_name ) values ( 50, sysdate, user, 'PowerTax - Depreciation', 'tax_record_control', 'uo_ppbase_tab_filter_dynamic' );
insert into pwrplant.pp_reports_filter ( pp_report_filter_id, time_stamp, user_id, description, table_name, filter_uo_name ) values ( 51, sysdate, user, 'PowerTax - Forecast', 'tax_record_control', 'uo_ppbase_tab_filter_dynamic' );
insert into pwrplant.pp_reports_filter ( pp_report_filter_id, time_stamp, user_id, description, table_name, filter_uo_name ) values ( 52, sysdate, user, 'PowerTax - Deferred Tax', 'tax_record_control', 'uo_ppbase_tab_filter_dynamic' );
insert into pwrplant.pp_reports_filter ( pp_report_filter_id, time_stamp, user_id, description, table_name, filter_uo_name ) values ( 53, sysdate, user, 'PowerTax - Depreciation Overlay', 'tax_record_control', 'uo_tax_selecttabs_rpts_rollup' );
insert into pwrplant.pp_reports_filter ( pp_report_filter_id, time_stamp, user_id, description, table_name, filter_uo_name ) values ( 54, sysdate, user, 'PowerTax - Asset Grid', 'tax_record_control', 'uo_ppbase_tab_filter_dynamic' );

insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (101, 'Company', 'dw', 'tax_record_control.company_id', '', 'dw_tax_company_by_version_filter', 'company_id', 'description', 'N', 0, 0, '', '', user, sysdate, 0, 0, 0 );
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (102, 'Tax Class', 'dw', 'tax_record_control.tax_class_id', '', 'dw_tax_class_by_version_filter', 'tax_class_id', 'description', 'N', 0, 0, '', '', user, sysdate, 0, 0, 0 );
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (103, 'Vintage', 'dw', 'tax_record_control.vintage_id', '', 'dw_tax_vintage_by_version_filter', 'vintage_id', 'description', 'N', 0, 0, '', '', user, sysdate, 0, 0, 0 );
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (104, 'Tax Book', 'dw', 'tax_depreciation.tax_book_id', '', 'dw_tax_book_filter', 'tax_book_id', 'description', 'N', 0, 0, '', '', user, sysdate, 0, 0, 0 );
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (105, 'Tax Book', 'dw', 'tax_book_view.x', '', 'dw_tax_book_filter', 'tax_book_id', 'description', 'N', 0, 0, '', '', user, sysdate, 0, 0, 0 );
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (106, 'Normalization Schema', 'dw', 'deferred_income_tax.normalization_id', '', 'dw_tax_normalization_schema_filter', 'normalization_id', 'description', 'N', 0, 0, '', '', user, sysdate, 0, 0, 0 );
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (107, 'Tax Year', 'dw', 'tax_depreciation.tax_year', '', 'dw_tax_year_by_version_filter', 'tax_year', 'tax_year', 'N', 1, 0, '', '', user, sysdate, 0, 0, 0 );
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (108, 'Tax Record ID', 'dw', 'tax_record_control.tax_record_id', '', 'dw_tax_record_by_version_filter', 'tax_record_id', 'tax_record_id', 'N', 0, 0, '', '', user, sysdate, 0, 1, 0 );
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (109, 'In Service Month', 'dw', 'to_char( tax_record_control.in_service_month, ''mm'' )', '', 'dw_tax_month_filter', 'month_id', 'month_name', 'C', 0, 0, '', '', user, sysdate, 0, 0, 0 );
insert into pwrplant.pp_dynamic_filter ( filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required, single_select_only, valid_operators, data, user_id, time_stamp, exclude_from_where, not_retrieve_immediate, invisible ) values (110, 'Tax Rollups', 'dw', 'tax_class.tax_class_id in (select distinct tax_class_id from tax_class_rollups where tax_rollup_detail_id', '[selected_values])', 'dw_tax_rollup_tree', 'tax_rollup_detail_id', 'tax_rollup_detail_description', 'N', 0, 0, '', '', user, sysdate, 0, 0, 0 );

insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 50, 101, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 50, 102, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 50, 103, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 50, 104, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 50, 110, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 53, 101, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 53, 102, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 53, 103, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 53, 105, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 53, 110, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 52, 101, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 52, 102, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 52, 103, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 52, 106, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 52, 110, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 51, 101, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 51, 102, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 51, 103, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 51, 110, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 54, 101, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 54, 102, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 54, 103, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 54, 104, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 54, 107, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 54, 108, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 54, 109, user, sysdate );
insert into pwrplant.pp_dynamic_filter_mapping ( pp_report_filter_id, filter_id, user_id, time_stamp ) values ( 54, 110, user, sysdate );

--
-- Create new versions of the PowerTax reports that can be run from the base GUI reporting workspace.
-- Leave the subsystem field blank so that they don't show up in the old PowerTax reporting window.
--
insert into pwrplant.pp_reports ( report_id, time_stamp, user_id, description, long_description, subsystem, datawindow, special_note, report_type, time_option, report_number, input_window, filter_option, status, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, user_comment, last_approved_date, pp_report_number, old_report_number, dynamic_dw ) values (  400001, sysdate, user, 'PowerTax Detail', 'PowerTax Detail.  Includes all header and balance information.  Grouped by tax record and tax book.', '', 'dw_tax_rpt_record_detail', '', ' ', '', 'PwrTax - 6', '', '', '', 1, 29, 50, 50, 1, 3, '', '', null, '', '', 0 );
insert into pwrplant.pp_reports ( report_id, time_stamp, user_id, description, long_description, subsystem, datawindow, special_note, report_type, time_option, report_number, input_window, filter_option, status, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, user_comment, last_approved_date, pp_report_number, old_report_number, dynamic_dw ) values (  403005, sysdate, user, 'Depreciation Summary Report 7', 'PowerTax Depreciation Summary Report 7.  Detail by vintage.  Grouped by Company Consolidation and Rollup.', '', 'dw_tax_rpt_depr_sum_rep7', '', '', '', 'PwrTax - 17', '', '', '', 1, 130, 50, 53, 1, 3, '', '', null, '', '', 0 );

--
-- Delete unused PowerTax reports.
--
delete from pwrplant.pp_reports where report_id = -550;
delete from pwrplant.pp_reports where report_id = -257;
delete from pwrplant.pp_reports where report_id = -256;
delete from pwrplant.pp_reports where report_id = -249;
delete from pwrplant.pp_reports where report_id = -248;
delete from pwrplant.pp_reports where report_id = -231;
delete from pwrplant.pp_reports where report_id = -216;
delete from pwrplant.pp_reports where report_id = -165;
delete from pwrplant.pp_reports where report_id = -133;
delete from pwrplant.pp_reports where report_id = -132;
delete from pwrplant.pp_reports where report_id = -120;
delete from pwrplant.pp_reports where report_id = -117;
delete from pwrplant.pp_reports where report_id = -116;
delete from pwrplant.pp_reports where report_id = -112;
delete from pwrplant.pp_reports where report_id = -73;
delete from pwrplant.pp_reports where report_id = -72;
delete from pwrplant.pp_reports where report_id = -65;
delete from pwrplant.pp_reports where report_id = -64;
delete from pwrplant.pp_reports where report_id = -54;
delete from pwrplant.pp_reports where report_id = -52;
delete from pwrplant.pp_reports where report_id = -51;
delete from pwrplant.pp_reports where report_id = -50;
delete from pwrplant.pp_reports where report_id = -36;
delete from pwrplant.pp_reports where report_id = -35;
delete from pwrplant.pp_reports where report_id = -30;
delete from pwrplant.pp_reports where report_id = -27;
delete from pwrplant.pp_reports where report_id = -26;
delete from pwrplant.pp_reports where report_id = -22;
delete from pwrplant.pp_reports where report_id = -17;
delete from pwrplant.pp_reports where report_id = -16;
delete from pwrplant.pp_reports where report_id = -12;
delete from pwrplant.pp_reports where report_id = -8;
delete from pwrplant.pp_reports where report_id = -4;
delete from pwrplant.pp_reports where report_id = 2;
delete from pwrplant.pp_reports where report_id = 3;
delete from pwrplant.pp_reports where report_id = 28;
delete from pwrplant.pp_reports where report_id = 29;
delete from pwrplant.pp_reports where report_id = 40;
delete from pwrplant.pp_reports where report_id = 41;
delete from pwrplant.pp_reports where report_id = 42;
delete from pwrplant.pp_reports where report_id = 71;
delete from pwrplant.pp_reports where report_id = 130;
delete from pwrplant.pp_reports where report_id = 251;
delete from pwrplant.pp_reports where report_id = 252;
delete from pwrplant.pp_reports where report_id = 253;
delete from pwrplant.pp_reports where report_id = 254;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (893, 0, 10, 4, 2, 0, 35523, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035523_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;