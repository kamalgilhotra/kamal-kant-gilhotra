/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_033536_depr_dg_comment_change.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.2   12/02/2013 Charlie Shilling add some mid period methods to the column comment
||============================================================================
*/

comment on column DEPR_GROUP.MID_PERIOD_METHOD is 'The mid-period method defines the depreciable balance methodology.  For depreciation done at the group level, activity impacting the depreciable base can be recognized several ways:
''Monthly'' means a ''half'' month convention is recognized in the current month only.  The beginning point for the depreciable base is the prior month''s ending balance.
''QTR'' means a ''half'' quarter convention is recognized in the current month only. The beginning point for the depreciable base is the prior begin balance at the start of the quarter.
''Yearly'' means that a ''half'' year convention is recognized in the current month only.  The beginning point for the depreciable base is the beginning balance at the start of the year.
''End of Life'' means that depreciation is calculated on a net basis over the remaining months, using the end date given in the Depr Method Rates table.
''Rate w/ End of Life'' means that depreciation is calculated on the net basis using the input rate.  The End of Life value is used for rate recalculation and reserve allocation.
''Unit of Production'' means that the ratio of monthly units of production to remaining units of production will be applied to a net depreciable balance.  Minimum and maximum annual depreciation constraints can also be applied.
''Curve'' means that a ''half'' year convention is recognized over the year based on estimated net additions.  The beginning point for the depreciable base is the balance at the start of the year.
''Curve (no true-up)'' is the same as ''Curve'' (except that there is no substitute of actual net additions for estimate net additions in December).
''Sum of Months Digits'' - [For group depreciation, this only is valid if an end of life is specified.]  The method uses the Net Balance x (Remaining life (in months)/ Sum of remaining life), i.e., (2 x Net Balance / (RM + 1).
''Sinking yearly fund'' - An accelerated depreciation method where more depreciation is taken in the earlier years.
For individual asset depreciation and depreciable sub-ledgers, the methods are:
''Straight-line'' - Straight line (remaining life), e.g. (1/RL * Net Balance).
''SL to EOL'' - ''Straight-line to End of Life''
''Schedule'' - An input schedule of rates by age (1st year, 2nd year, etc.) is entered in Depr Method Schedule and is then applied against the gross balance.
''Sum of Months Digits'' - Remaining sum of the months (remaining life), e.g. (2 x Net Balance / (RM + 1)
''DB 1.5'' - 1.50 declining balance switching to straight line.
''DB 2.0'' - Double declining balance switching to straight line.
''Rate'' - Individually Depreciates Assets using a set rate per month
''UOP Rate'' - Assets depreciated using a set rate per unit of production
''Custom'' - Custom depreciation function ''f_custom_calc'' must be provided.
All sub-ledger methods use a remaining life and a net balance, except for Schedule.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (777, 0, 10, 4, 1, 2, 33536, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_033536_depr_dg_comment_change.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;