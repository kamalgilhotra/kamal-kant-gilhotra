/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029599_pwrtax.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/04/2014 Julia Breuer
||============================================================================
*/

--
-- Setup the Additions Interface in the PowerTax menu.
--
insert into PWRPLANT.PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'depr_input_book_additions', sysdate, user, 'Additions',
    'uo_tax_depr_act_wksp_intfc_adds', 'Additions');

update PWRPLANT.PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'depr_input_book_additions'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'depr_input_book_additions';

--
-- Add a column to Tax Add Audit Trail for monthly additions.
--
alter table TAX_ADD_AUDIT_TRAIL add IN_SERVICE_MONTH date;

comment on column TAX_ADD_AUDIT_TRAIL.IN_SERVICE_MONTH is 'In-service month of the tax record (optional).';

--
-- Add a column to help track kickouts from the Additions Interface.
--
alter table TAX_BOOK_TRANS_ADDS add ERROR_MESSAGE varchar2(4000);

comment on column TAX_BOOK_TRANS_ADDS.ERROR_MESSAGE is 'Error message detailing why the record was not processed into PowerTax.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1009, 0, 10, 4, 2, 0, 29599, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_029599_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;