/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048667_lessor_01_workflow_subsystem_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/06/2017 Shane "C" Ward  Update field1 sql to point to lsr
||============================================================================
*/

UPDATE workflow_subsystem SET field1_sql = 'select ilr_number from lsr_ilr where ilr_id = <<id_field1>>'
WHERE subsystem = 'lessor_ilr_approval';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3884, 0, 2017, 1, 0, 0, 48667, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048667_lessor_01_workflow_subsystem_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;