/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032948_pwrtax_timeslice_1.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/31/2014 Andrew Scott        Update time slice id on deferred tables
||                                         to be 1 whereever it is null
||============================================================================
*/

update DEFERRED_INCOME_TAX_TRANSFER set TIME_SLICE_ID = 1 where TIME_SLICE_ID is null;

update DEFERRED_INCOME_TAX set TIME_SLICE_ID = 1 where TIME_SLICE_ID is null;

update ARC_DEFERRED_INCOME_TAX_TRANSF set TIME_SLICE_ID = 1 where TIME_SLICE_ID is null;

update ARC_DEFERRED_INCOME_TAX set TIME_SLICE_ID = 1 where TIME_SLICE_ID is null;

alter table DEFERRED_INCOME_TAX_TRANSFER modify TIME_SLICE_ID default 1;

alter table DEFERRED_INCOME_TAX modify TIME_SLICE_ID default 1;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (935, 0, 10, 4, 2, 0, 32948, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_032948_pwrtax_timeslice_1.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;