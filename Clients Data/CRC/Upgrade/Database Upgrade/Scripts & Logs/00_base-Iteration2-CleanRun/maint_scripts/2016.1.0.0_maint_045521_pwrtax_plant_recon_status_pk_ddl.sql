/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045521_pwrtax_plant_recon_status_pk_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| --------    ---------- -------------- ----------------------------------------
|| 2016.1.0.0  03/30/2016 David Haupt    Changed primary key on tax_plant_recon_form_status
||============================================================================
*/

alter table tax_plant_recon_form_status
	drop constraint tax_plant_recon_form_status_pk;
	
ALTER TABLE tax_plant_recon_form_status
  ADD CONSTRAINT tax_plant_recon_form_status_pk PRIMARY KEY (
    tax_pr_form_id,
	tax_pr_section_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3128, 0, 2016, 1, 0, 0, 45521, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045521_pwrtax_plant_recon_status_pk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
