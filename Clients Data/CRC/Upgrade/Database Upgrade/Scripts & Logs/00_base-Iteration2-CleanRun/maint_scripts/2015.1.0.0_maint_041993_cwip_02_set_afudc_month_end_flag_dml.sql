/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041993_cwip_02_set_afudc_month_end_flag_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/05/2015 Charlie Shilling flip wo_month_end_flag for the OH/AFUDC/WIP Comp calc so the logs will show up in the WO logs window
||============================================================================
*/
UPDATE pp_processes
SET wo_month_end_flag = 1
WHERE executable_file = 'ssp_wo_calc_oh_afudc_wip.exe';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2252, 0, 2015, 1, 0, 0, 041993, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041993_cwip_02_set_afudc_month_end_flag_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;