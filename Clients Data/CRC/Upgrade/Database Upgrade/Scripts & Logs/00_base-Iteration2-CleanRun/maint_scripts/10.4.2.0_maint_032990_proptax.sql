/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032990_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/10/2013 Julia Breuer   Point Release
||============================================================================
*/

--
-- Add an indicator to PT Parcel Assessment to flag parcels that should use tiered taxable value rates.
--
alter table PT_PARCEL_ASSESSMENT add USE_TIERED_RATES_YN number(22,0);

comment on column PT_PARCEL_ASSESSMENT.USE_TIERED_RATES_YN is 'Indicator for whether to use tiered taxable value rates for the given parcel, assessment group, and case (Yes = 1, No = 0 or null).';

--
-- Add the new column to the import tool.
--
alter table PT_IMPORT_PRCL_ASMT add USE_TIERED_RATES_XLATE varchar2(254);
alter table PT_IMPORT_PRCL_ASMT add USE_TIERED_RATES_YN number(22,0);
alter table PT_IMPORT_PRCL_ASMT_ARCHIVE add USE_TIERED_RATES_XLATE varchar2(254);
alter table PT_IMPORT_PRCL_ASMT_ARCHIVE add USE_TIERED_RATES_YN number(22,0);

insert into PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN ) values ( 11, 'use_tiered_rates_yn', sysdate, user, 'Use Tiered Rates', 'use_tiered_rates_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null, 'yes_no_id' );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 11, 'use_tiered_rates_yn', 77, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 11, 'use_tiered_rates_yn', 78, sysdate, user );

comment on column PT_IMPORT_PRCL_ASMT.USE_TIERED_RATES_XLATE is 'Value from the import file translated to obtain the use_tiered_rates_yn.';
comment on column PT_IMPORT_PRCL_ASMT.USE_TIERED_RATES_YN is 'Yes/No value indicating if tiered taxable value rates should be used for the given parcel, assessment group, and case.';
comment on column PT_IMPORT_PRCL_ASMT_ARCHIVE.USE_TIERED_RATES_XLATE is 'Value from the import file translated to obtain the use_tiered_rates_yn.';
comment on column PT_IMPORT_PRCL_ASMT_ARCHIVE.USE_TIERED_RATES_YN is 'Yes/No value indicating if tiered taxable value rates should be used for the given parcel, assessment group, and case.';

--
-- Add a system option for displaying the new column.
--
insert into PPBASE_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Assessments - Entry - Display Use Tiered Rates in Grid', sysdate, user, 'Specifies whether or not the use tiered rates field is displayed in the assessment entry grids.', 0, 'No', null, 1 );
insert into PPBASE_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Use Tiered Rates in Grid', 'No', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Use Tiered Rates in Grid', 'Yes', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Use Tiered Rates in Grid', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Use Tiered Rates in Grid', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Use Tiered Rates in Grid', 'parcel_search', sysdate, user );

--
-- Add a temp column to PT Parcel Assessment to store the equalization factor used - it will help with calculations.
--
alter table PT_PARCEL_ASSESSMENT add TEMP_EQ_FACTOR number(22,8);

comment on column PT_PARCEL_ASSESSMENT.TEMP_EQ_FACTOR is 'Temporary column used in processing.';

--
-- Add columns to the Taxable Value Rates tables to store the threshold and rate to be used under the threshold.
--
alter table PT_TAXABLE_VALUE_RATE add THRESHOLD_AMOUNT number(22,2);
alter table PT_TAXABLE_VALUE_RATE add RATE_UNDER_THRESHOLD number(22,8);
alter table PT_TAXABLE_VALUE_RATE_COUNTY add THRESHOLD_AMOUNT number(22,2);
alter table PT_TAXABLE_VALUE_RATE_COUNTY add RATE_UNDER_THRESHOLD number(22,8);

comment on column PT_TAXABLE_VALUE_RATE.THRESHOLD_AMOUNT is 'Assessment amount below which the rate_under_threshold applies when calculating the taxable value.  Above this amount, the taxable_value_rate applies.  If blank, the rate_under_threshold is ignored.';
comment on column PT_TAXABLE_VALUE_RATE.RATE_UNDER_THRESHOLD is 'Rate used on the portion of the assessment below the threshold_amount.';
comment on column PT_TAXABLE_VALUE_RATE_COUNTY.THRESHOLD_AMOUNT is 'Assessment amount below which the rate_under_threshold applies when calculating the taxable value.  Above this amount, the taxable_value_rate applies.  If blank, the rate_under_threshold is ignored.';
comment on column PT_TAXABLE_VALUE_RATE_COUNTY.RATE_UNDER_THRESHOLD is 'Rate used on the portion of the assessment below the threshold_amount.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (782, 0, 10, 4, 2, 0, 32990, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_032990_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
