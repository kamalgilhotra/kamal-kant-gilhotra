/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_048330_lease_04_deferred_rent_import_col_dml.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2017.1.0.0 06/26/2017 Jared Schwantz     	Adding import columns for deferred rent
||========================================================================================
*/

insert into pp_import_column(import_type_id, column_name, description, import_column_name, is_required, processing_order,
    column_type, parent_table, is_on_table, parent_table_pk_column)
select 252, 'st_deferred_account_id', 'ST Deferred Rent Account', 'st_deferred_account_xlate', 0, 2, 'number(22,0)',
    'gl_account', 1, 'gl_account_id'
from dual
where not exists(
    select 1
    from pp_import_column
    where import_type_id = 252
    and column_name = 'st_deferred_account_id')
;

insert into pp_import_column(import_type_id, column_name, description, import_column_name, is_required, processing_order,
    column_type, parent_table, is_on_table, parent_table_pk_column)
select 252, 'lt_deferred_account_id', 'LT Deferred Rent Account', 'lt_deferred_account_xlate', 0, 2, 'number(22,0)',
    'gl_account', 1, 'gl_account_id'
from dual
where not exists(
    select 1
    from pp_import_column
    where import_type_id = 252
    and column_name = 'lt_deferred_account_id')
;

insert into pp_import_column_lookup(import_type_id, column_name, import_lookup_id)
select 252, 'st_deferred_account_id', import_lookup_id
from pp_import_column_lookup
where import_type_id = 252
and column_name = 'ap_account_id'
union all
select 252, 'lt_deferred_account_id', import_lookup_id
from pp_import_column_lookup
where import_type_id = 252
and column_name = 'ap_account_id'
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3543, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_04_deferred_rent_import_col_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;