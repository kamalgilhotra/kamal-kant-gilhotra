SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030261_taxrpr_wo_bal.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   07/25/2013 Alex P.         Point Release
||============================================================================
*/

declare
   TABLE_EXISTS exception;
   pragma exception_init(TABLE_EXISTS, -955);

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
   execute immediate 'create table TREPAIRS_WO_CWIP
                      (
                       COMPANY_ID           number(22,0),
                       WORK_ORDER_NUMBER    varchar2(35),
                       FUNDING_WO_INDICATOR number(22,0),
                       WORK_ORDER_ID        number(22,0),
                       TIME_STAMP           date,
                       USER_ID              varchar2(18),
                       BOOK_SUMMARY_ID      number(22,0),
                       CLOSED_MONTH_NUMBER  number(22,0),
                       CWIP_106_BEG         number(22,2),
                       CWIP_106_ACT         number(22,2),
                       CWIP_106_END         number(22,2),
                       CWIP_101_BEG         number(22,2),
                       CWIP_101_ACT         number(22,2),
                       CWIP_101_END         number(22,2)
                      )';
   DBMS_OUTPUT.PUT_LINE('TREPAIRS_WO_CWIP table created.');

   execute immediate 'alter table TREPAIRS_WO_CWIP
                         add constraint PK_TREPAIRS_WO_CWIP
                             primary key (WORK_ORDER_ID, BOOK_SUMMARY_ID, CLOSED_MONTH_NUMBER)
                             using index tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE('TREPAIRS_WO_CWIP table altered.');

   execute immediate 'create table TREPAIRS_WO_CPR
                      (
                       COMPANY_ID          number(22,0),
                       WORK_ORDER_NUMBER   varchar2(35),
                       WORK_ORDER_ID       number(22,0),
                       TIME_STAMP          date,
                       USER_ID             varchar2(18),
                       BOOK_SUMMARY_ID     number(22,0),
                       CLOSED_MONTH_NUMBER number(22,0),
                       CPR_106_ACT         number(22,2),
                       CPR_101_ACT         number(22,2)
                      )';
   DBMS_OUTPUT.PUT_LINE('TREPAIRS_WO_CPR table created.');

   execute immediate 'alter table TREPAIRS_WO_CPR
                         add constraint PK_TREPAIRS_WO_CPR
                             primary key (COMPANY_ID, WORK_ORDER_NUMBER, BOOK_SUMMARY_ID, CLOSED_MONTH_NUMBER)
                             using index tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE('TREPAIRS_WO_CPR table altered.');

   execute immediate 'create table TREPAIRS_WO_COMPARE
                      (
                       COMPANY_ID          number(22,0),
                       WORK_ORDER_NUMBER   varchar2(35),
                       WORK_ORDER_ID       number(22,0),
                       TIME_STAMP          date,
                       USER_ID             varchar2(18),
                       BOOK_SUMMARY_ID     number(22,0),
                       CLOSED_MONTH_NUMBER number(22,0),
                       CPR_106_ACT         number(22,2),
                       CWIP_106_ACT        number(22,2),
                       DIFF_106_ACT        number(22,2),
                       CPR_101_ACT         number(22,2),
                       CWIP_101_ACT        number(22,2),
                       DIFF_101_ACT        number(22,2),
                       NEW106_CHARGE_ID    number(22,0),
                       NEW101_CHARGE_ID    number(22,0),
                       BATCH_ID            number(22,0),
                       CURR_MONTH_NUMBER   number(22,0)
                      )';
   DBMS_OUTPUT.PUT_LINE('TREPAIRS_WO_COMPARE table created.');

   execute immediate 'alter table TREPAIRS_WO_COMPARE
                         add constraint PK_TREPAIRS_WO_COMPARE
                             primary key (COMPANY_ID, WORK_ORDER_NUMBER, BOOK_SUMMARY_ID, CLOSED_MONTH_NUMBER, BATCH_ID)
                             using index tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE('TREPAIRS_WO_COMPARE table altered.');

   execute immediate 'create table TREPAIR_FUNDING_PROJ_CHARGES
                      (
                       CHARGE_ID                number(22,0) not null,
                       CHARGE_AUDIT_ID          number(22,0),
                       COST_ELEMENT_ID          number(22,0),
                       CHARGE_TYPE_ID           number(22,0),
                       EXPENDITURE_TYPE_ID      number(22,0),
                       WORK_ORDER_ID            number(22,0),
                       CHARGE_MO_YR             date,
                       DESCRIPTION              varchar2(35),
                       QUANTITY                 number(22,4),
                       AMOUNT                   number(22,2),
                       DEPARTMENT_ID            number(22,0),
                       GL_ACCOUNT_ID            number(22,0),
                       COMPANY_ID               number(22,0),
                       MONTH_NUMBER             number(22,0),
                       TAX_ORIG_MONTH_NUMBER    number(22,0),
                       CLOSED_MONTH_NUMBER      number(22,0),
                       UNIT_CLOSED_MONTH_NUMBER number(22,0),
                       NOTES                    varchar2(2000),
                       VENDOR_INFORMATION       varchar2(254),
                       REFERENCE_NUMBER         varchar2(254),
                       JOURNAL_CODE             varchar2(35),
                       EXTERNAL_GL_ACCOUNT      varchar2(254),
                       STATUS                   number(22,0),
                       NON_UNITIZED_STATUS      number(22,0)
                      )';
   DBMS_OUTPUT.PUT_LINE('TREPAIR_FUNDING_PROJ_CHARGES table created.');

   execute immediate 'alter table TREPAIR_FUNDING_PROJ_CHARGES
                         add constraint PK_TREPAIR_FUNDING_PROJ_CHG
                             primary key (charge_id)
                             using index tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE('TREPAIR_FUNDING_PROJ_CHARGES table altered.');

   execute immediate 'insert into PP_PROCESSES (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION)
                      select max(PROCESS_ID) + 1, ''WO Bal Tax Repairs'', ''Work Order Balancing for Tax Repairs charge types''
                        from PP_PROCESSES';

exception
   when TABLE_EXISTS then
      DBMS_OUTPUT.PUT_LINE('The script was already run in this database. Skipping.');
end;
/


create or replace view CWIP_CHARGE_SOB_VIEW
   (CHARGE_ID, CHARGE_TYPE_ID, JOB_TASK_ID, EXPENDITURE_TYPE_ID, WORK_ORDER_ID, TIME_STAMP, USER_ID, STCK_KEEP_UNIT_ID,
    RETIREMENT_UNIT_ID, CHARGE_MO_YR, DESCRIPTION, QUANTITY, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, AMOUNT, SUB_ACCOUNT_ID,
    HOURS, PAYMENT_DATE, NOTES, ID_NUMBER, UNITS, LOADING_AMOUNT, REFERENCE_NUMBER, VENDOR_INFORMATION, DEPARTMENT_ID,
    CHARGE_AUDIT_ID, JOURNAL_CODE, COST_ELEMENT_ID, EXTERNAL_GL_ACCOUNT, GL_ACCOUNT_ID, STATUS, COMPANY_ID, MONTH_NUMBER,
    NON_UNITIZED_STATUS, EXCLUDE_FROM_OVERHEADS, CLOSED_MONTH_NUMBER, ORIGINAL_CURRENCY, ORIGINAL_AMOUNT, ASSET_LOCATION_ID,
    SERIAL_NUMBER, UNIT_CLOSED_MONTH_NUMBER, PO_NUMBER, TAX_ORIG_MONTH_NUMBER)
as
 select CWIP_CHARGE.CHARGE_ID,
        CWIP_CHARGE.CHARGE_TYPE_ID,
        CWIP_CHARGE.JOB_TASK_ID,
        CWIP_CHARGE.EXPENDITURE_TYPE_ID,
        CWIP_CHARGE.WORK_ORDER_ID,
        CWIP_CHARGE.TIME_STAMP,
        CWIP_CHARGE.USER_ID,
        CWIP_CHARGE.STCK_KEEP_UNIT_ID,
        CWIP_CHARGE.RETIREMENT_UNIT_ID,
        CWIP_CHARGE.CHARGE_MO_YR,
        CWIP_CHARGE.DESCRIPTION,
        CWIP_CHARGE.QUANTITY,
        CWIP_CHARGE.UTILITY_ACCOUNT_ID,
        CWIP_CHARGE.BUS_SEGMENT_ID,
        CWIP_CHARGE.AMOUNT,
        CWIP_CHARGE.SUB_ACCOUNT_ID,
        CWIP_CHARGE.HOURS,
        CWIP_CHARGE.PAYMENT_DATE,
        CWIP_CHARGE.NOTES,
        CWIP_CHARGE.ID_NUMBER,
        CWIP_CHARGE.UNITS,
        CWIP_CHARGE.LOADING_AMOUNT,
        CWIP_CHARGE.REFERENCE_NUMBER,
        CWIP_CHARGE.VENDOR_INFORMATION,
        CWIP_CHARGE.DEPARTMENT_ID,
        CWIP_CHARGE.CHARGE_AUDIT_ID,
        CWIP_CHARGE.JOURNAL_CODE,
        CWIP_CHARGE.COST_ELEMENT_ID,
        CWIP_CHARGE.EXTERNAL_GL_ACCOUNT,
        CWIP_CHARGE.GL_ACCOUNT_ID,
        CWIP_CHARGE.STATUS,
        CWIP_CHARGE.COMPANY_ID,
        CWIP_CHARGE.MONTH_NUMBER,
        CWIP_CHARGE.NON_UNITIZED_STATUS,
        CWIP_CHARGE.EXCLUDE_FROM_OVERHEADS,
        CWIP_CHARGE.CLOSED_MONTH_NUMBER,
        CWIP_CHARGE.ORIGINAL_CURRENCY,
        CWIP_CHARGE.ORIGINAL_AMOUNT,
        CWIP_CHARGE.ASSET_LOCATION_ID,
        CWIP_CHARGE.SERIAL_NUMBER,
        CWIP_CHARGE.UNIT_CLOSED_MONTH_NUMBER,
        CWIP_CHARGE.PO_NUMBER,
        CWIP_CHARGE.TAX_ORIG_MONTH_NUMBER
   from CWIP_CHARGE, TEMP_SET_OF_BOOKS
  where CWIP_CHARGE.CHARGE_TYPE_ID = TEMP_SET_OF_BOOKS.CHARGE_TYPE_ID;

delete PP_REPORTS
 where DATAWINDOW in ('dw_trepairs_wo_incurred_yr_report', 'dw_trepairs_co_incurred_yr_report');

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
select
   max(report_id) + 1, 'PWRPLANT', TO_DATE('25-07-2013 15:33:56', 'dd-mm-yyyy hh24:mi:ss'),
    'Repairs: WO Incurred by Tax Year',
    'Tax Repairs activity details for work orders by tax year (tax_orig_month_number)', null,
    'dw_rpr_rpt_wo_incurred_yr', null, null, null, 'RPR - 0301', null, null, null, 6, 100, 2, 34, 1,
    1, null, null, null, 'RPR - 0301', null, 0
from pp_reports;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
select
   max(report_id) + 1, 'PWRPLANT', TO_DATE('25-07-2013 15:33:57', 'dd-mm-yyyy hh24:mi:ss'),
    'Repairs: Incurred by Tax Year',
    'Tax Repairs activity summary for companies by tax year (tax_orig_month_number)', null,
    'dw_rpr_rpt_co_incurred_yr', null, null, null, 'RPR - 0300', null, null, null, 6, 100, 2, 34, 1,
    1, null, null, null, 'RPR - 0300', null, 0
from pp_reports;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (486, 0, 10, 4, 1, 0, 30261, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030261_taxrpr_wo_bal.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;