 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_041904_pcm_menu_items_dml.sql
|| Description: Change Maintain/Details to Details/Header
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	01/06/2014 Ryan Oliveria  New Module
||============================================================================
*/


update PPBASE_MENU_ITEMS
   set LABEL = 'Details'
 where MODULE = 'pcm'
   and MENU_IDENTIFIER in ('fp_maintain', 'wo_maintain', 'jt_maintain', 'config_maintain', 'prog_maintain');

update PPBASE_MENU_ITEMS
   set LABEL = 'Information'
 where MODULE = 'pcm'
   and MENU_IDENTIFIER in ('fp_maint_details', 'wo_maint_details', 'jt_maint_details', 'prog_maint_details');




update PPBASE_WORKSPACE
   set MINIHELP = 'Config Details (wo types, etc.)'
 where MODULE = 'pcm'
   and WORKSPACE_IDENTIFIER = 'config_maintain';

update PPBASE_WORKSPACE
   set LABEL = replace(LABEL, 'Maintain', 'Details'),
   	   MINIHELP = replace(MINIHELP, 'Maintain', 'Details')
 where MODULE = 'pcm'
   and WORKSPACE_IDENTIFIER in ('prog_maint_info', 'fp_maint_info', 'wo_maint_info', 'jt_maint_info', 'config_maintain');

update PPBASE_WORKSPACE
   set LABEL = 'Information',
   	   MINIHELP = 'Information'
 where MODULE = 'pcm'
   and WORKSPACE_IDENTIFIER in ('wo_maint_details', 'prog_maint_details', 'fp_maint_details', 'jt_maint_details');



update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = ''
 where MODULE = 'pcm'
   and MENU_IDENTIFIER in ('fp_maintain', 'wo_maintain');

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_pcm_maint_wksp_info'
 where MODULE = 'pcm'
   and WORKSPACE_IDENTIFIER in ('fp_maint_details', 'wo_maint_details');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2149, 0, 2015, 1, 0, 0, 041904, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041904_pcm_menu_items_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;