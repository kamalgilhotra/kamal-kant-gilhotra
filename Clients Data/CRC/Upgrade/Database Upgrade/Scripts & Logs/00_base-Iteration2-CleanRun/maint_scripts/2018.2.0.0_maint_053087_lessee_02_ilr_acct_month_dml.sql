/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053087_lessee_02_ilr_acct_month_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.1.0.2 02/11/2019 C. Yura        backfill new ls_ilr_approval fields
||============================================================================
*/

update ls_ilr_approval set acct_month_approved = null;

-- Populate bulk of revisions using ls_pend_transaction_arc/cpr_activity link, using distinct for multiple assets per ILR.  Should get accurate date for Adds
merge into ls_ilr_approval LIA
using (select distinct ilr_app.ilr_id ilr_id , ilr_app.revision revision , cpr_act.gl_posting_mo_yr gl_posting_mo_yr
        from ls_ilr_approval ilr_app, ls_pend_transaction_arc pend_arc, ls_cpr_asset_map map, cpr_activity cpr_act
        where ilr_app.approval_date is not null
        and acct_month_approved is null
        and ilr_app.ilr_id = pend_arc.ilr_id
		and ilr_app.revision > 0
        and ilr_app.revision = pend_arc.revision
        and map.ls_asset_id = pend_arc.ls_asset_id
        and cpr_act.asset_id = map.asset_id
        and cpr_act.activity_status = pend_arc.ls_pend_trans_id) ls_dates
     on (ls_dates.ilr_id = lia.ilr_id and ls_dates.revision = lia.revision)
    when matched then
      update set lia.acct_month_approved = ls_dates.gl_posting_mo_yr;

-- Now try to populate remaining scenarios in the best way possible, defaulting to a catch all using ls_process_control dates and approval date timestamps
-- Populate LEASERETIRE revisions using cpr_activity gl posting month.  This will work for retirements done with newer version, where LEASERETIRE is populated and approval date is populated
merge into ls_ilr_approval LIA
using (
     select distinct ilr_app.ilr_id ilr_id, ilr_app.revision revision, cpr_act.gl_posting_mo_yr gl_posting_mo_yr
      From ls_ilr_approval ilr_app, ls_ilr_asset_map ilr_map, ls_cpr_asset_map asset_map, cpr_activity cpr_act
      where  ilr_app.approval_date is not null 
      and  nvl(ilr_app.revision_desc,'null') = 'LEASERETIRE'
      and ilr_map.ilr_id = ilr_app.ilr_id
	  and ilr_app.revision > 0
	  and ilr_app.acct_month_approved is null
      and ilr_map.revision = (select max(revision) from ls_ilr_approval a where a.ilr_id = ilr_app.ilr_id and a.approval_status_id = 3 and a.revision < ilr_app.revision)
      and ilr_map.ls_asset_id = asset_map.ls_asset_id
      and asset_map.asset_id = cpr_act.asset_id
    and trim(cpr_act.activity_code) = 'URGL'
    and ilr_app.ilr_id not in (select ilr_app2.ilr_id from ls_ilr_approval ilr_app2 where nvl(ilr_app2.revision_desc,'null') = 'LEASERETIRE' group by ilr_app2.ilr_id having count(*) > 1  )) ls_dates
  on (ls_dates.ilr_id = lia.ilr_id and ls_dates.revision = lia.revision)
   when matched then
      update set lia.acct_month_approved = ls_dates.gl_posting_mo_yr;
	  

-- Older retirement revisions didn't populate approval date but had approval status = 3. For these, use this to update retired ILRs max revision with the max URGL date in cpr activity	  
merge into ls_ilr_approval LIA
using (  
    select distinct ilr_app.ilr_id, ilr_app.revision, cpr_act.gl_posting_mo_yr 
    From ls_ilr_approval ilr_app, ls_ilr ilr, (select ilr_id, max(revision) max_rev from ls_ilr_approval group by ilr_id) max_rev, ls_ilr_asset_map ilr_map,
   (select a.asset_id, max(gl_posting_mo_yr) gl_posting_mo_yr from cpr_activity a, cpr_ledger b 
    where a.asset_id = b.asset_id and b.subledger_indicator = -100 and trim(a.activity_code) = 'URGL' group by a.asset_id) cpr_act, ls_cpr_asset_map asset_map
    where ilr.ilr_id = ilr_app.ilr_id
    and ilr.ilr_id = max_rev.ilr_id 
    and ilr.ilr_id = ilr_map.ilr_id
    and ilr_app.revision = ilr_map.revision
	and ilr_app.revision > 0
    and ilr.ilr_status_id = 3 --Retired
    and ilr_app.approval_status_id = 3 --Approved
    and ilr_app.approval_date is null
    and ilr_app.acct_month_approved is null
    and ilr_app.revision = max_rev.max_rev
    and asset_map.ls_asset_id = ilr_map.ls_asset_id
    and cpr_act.asset_id = asset_map.asset_id) ls_dates
  on (ls_dates.ilr_id = lia.ilr_id and ls_dates.revision = lia.revision)
   when matched then
      update set lia.acct_month_approved = ls_dates.gl_posting_mo_yr;

-- Another attempt to populate those without an approval date, but approval_status_id = 3 and revision isn't the max for some reason
merge into ls_ilr_approval LIA
using (   
    select ilr_app.ilr_id ilr_id, ilr_app.revision revision, max(cpr_act.gl_posting_mo_yr) gl_posting_mo_yr
      From ls_ilr_approval ilr_app, ls_ilr_asset_map ilr_map, ls_cpr_asset_map asset_map, cpr_activity cpr_act, ls_ilr ilr
      where  ilr_app.approval_date is null
      and ilr_app.acct_month_approved is null
      and ilr_app.approval_status_id = 3 
      and ilr_app.ilr_id = ilr.ilr_id
      and ilr_status_id = 3
	  and ilr_app.revision > 0
      and ilr_map.ilr_id = ilr_app.ilr_id
      and ilr_map.revision = ilr_app.revision
      and ilr_map.ls_asset_id = asset_map.ls_asset_id
      and asset_map.asset_id = cpr_act.asset_id
      and trim(cpr_act.activity_code) = 'URGL'
      group by ilr_app.ilr_id, ilr_app.revision) ls_dates
  on (ls_dates.ilr_id = lia.ilr_id and ls_dates.revision = lia.revision)
   when matched then
      update set lia.acct_month_approved = ls_dates.gl_posting_mo_yr;
    
-- Default catch all for everything left, using approval date and timestamps on ls_process_control*/	  
merge into ls_ilr_approval LIA
using (   
    select distinct ilr_app.ilr_id, ilr_app.revision, ilr_app.approval_date, lpc.gl_posting_mo_yr
    from ls_ilr_approval ilr_app, ls_ilr ilr, ls_process_control lpc
    where ilr_app.ilr_id = ilr.ilr_id
    and ilr.ilr_status_id <> 6
    and ilr.company_id = lpc.company_id
    and ilr_app.approval_date is not null
    and ilr_app.acct_month_approved is null
	and ilr_app.revision > 0
    and lpc.lam_closed > ilr_app.approval_date
    and lpc.lam_closed = (select min(lpc2.lam_closed) from ls_process_control lpc2 where lpc2.lam_closed > ilr_app.approval_date and lpc2.company_id = lpc.company_id)  ) ls_dates
  on (ls_dates.ilr_id = lia.ilr_id and ls_dates.revision = lia.revision)
   when matched then
      update set lia.acct_month_approved = ls_dates.gl_posting_mo_yr;	

declare
   L_COUNT number;
begin
   -- See if there are any records left unpopulated
   select count(*) into L_COUNT  
   From ls_ilr_approval, ls_ilr 
   where ls_ilr_approval.ilr_id = ls_ilr.ilr_id 
   and (approval_status_id = 3 or approval_date is not null) 
   and acct_month_approved is null
   and (ls_ilr_approval.ilr_id, ls_ilr_approval.revision) not in (select ilr_id, current_revision from ls_ilr where ilr_status_id  = 6);
   
   if L_COUNT > 0 then
      RAISE_APPLICATION_ERROR(-20000,
                              'Above statements could not populate ls_ilr_approval.acct_month_approved for all records with status = 3 or approval_date is not null where revision is not Pending In Service status.  
                               Will have to manually determine how to backfill outliers');
   end if;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15005, 0, 2018, 2, 0, 0, 53087, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053087_lessee_02_ilr_acct_month_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
	  