/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041290_anlyt_08_tables.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.1 11/17/2014 Chad Theilman    Asset Analytics - Tables
||============================================================================
*/

declare 
  doesTableExist number := 0;
  
  begin
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_AUDIT_HISTORY','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_AUDIT_HISTORY (
        TABLENAME VARCHAR2(50) NOT NULL,
        USERID VARCHAR2(50),
        PROCESSDATE TIMESTAMP NOT NULL,
        STEPNBR NUMBER NOT NULL ENABLE, 
        STEPNAME VARCHAR2(100) NOT NULL
      )
      NOCOMPRESS NOLOGGING
      TABLESPACE PWRPLANT ';
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CONTROL_PROCESS','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_CONTROL_PROCESS (
          TABLENAME VARCHAR2(50) NOT NULL ENABLE,
          PROCESSYEAR NUMBER(22) NOT NULL,
          USERID VARCHAR2(50),
          PROCESSDATE TIMESTAMP NOT NULL,
          CONSTRAINT PK_PA_CONTROL_PROCESS PRIMARY KEY (TABLENAME, PROCESSYEAR)
          )
          NOCOMPRESS NOLOGGING
          TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CONTROL_GROUPS','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_CONTROL_GROUPS (
        GROUP_NAME VARCHAR2(50) NOT NULL ENABLE,
        GROUPBY_SHOW CHAR(1) DEFAULT ''1'',
        FILTERBY_SHOW CHAR(1) DEFAULT ''1'',
        CONSTRAINT PK_PA_CONTROL_GROUPS PRIMARY KEY (GROUP_NAME)
        )
        NOCOMPRESS NOLOGGING
        TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CONTROL_LOCATIONS','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_CONTROL_LOCATIONS (
        GROUP_NAME VARCHAR2(50) NOT NULL ENABLE,
        GROUPBY_SHOW CHAR(1) DEFAULT ''1'',
        FILTERBY_SHOW CHAR(1) DEFAULT ''1'',
        CONSTRAINT PK_PA_CONTROL_LOCATIONS PRIMARY KEY (GROUP_NAME)
        )
        NOCOMPRESS NOLOGGING
        TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_EXCLUDE_ASSETS','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_EXCLUDE_ASSETS (
          ASSET_ID NUMBER(22) NOT NULL ENABLE,
          USER_ID VARCHAR2(35) NOT NULL,  
          USER_COMMENT VARCHAR(2000),
          CONSTRAINT PK_EXCLUDE_ASSETS PRIMARY KEY (ASSET_ID) 
          )
          NOCOMPRESS NOLOGGING
          TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_EXCLUDE_ACTIVITY','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_EXCLUDE_ACTIVITY (
        ASSET_ID NUMBER(22) NOT NULL ENABLE,
        ACTIVITY_ID NUMBER(22) NOT NULL,
        USER_ID VARCHAR2(35) NOT NULL,  
        USER_COMMENT VARCHAR(2000),
        CONSTRAINT PK_EXCLUDE_ACTIVITY PRIMARY KEY (ASSET_ID, ACTIVITY_ID) 
        )
        NOCOMPRESS NOLOGGING
        TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_EXCLUDE_ACCOUNTS','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_EXCLUDE_ACCOUNTS (
          ACCT_KEY NUMBER(22) NOT NULL ENABLE,
          USER_ID VARCHAR2(35) NOT NULL,  
          USER_COMMENT VARCHAR(2000),
          CONSTRAINT PK_EXCLUDE_ACCOUNTS PRIMARY KEY (ACCT_KEY) 
          )
          NOCOMPRESS NOLOGGING
          TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_EXCLUDE_LOCATIONS','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_EXCLUDE_LOCATIONS (
        ASSET_LOCATION_ID NUMBER(22) NOT NULL ENABLE,
        USER_ID VARCHAR2(35) NOT NULL,  
        USER_COMMENT VARCHAR(2000),
        CONSTRAINT PK_EXCLUDE_LOCATIONS PRIMARY KEY (ASSET_LOCATION_ID) 
        )
        NOCOMPRESS NOLOGGING
        TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_EXCLUDE_DEPR_GROUP','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_EXCLUDE_DEPR_GROUP (
          DEPR_GROUP_ID NUMBER(22) NOT NULL ENABLE,
          USER_ID VARCHAR2(35) NOT NULL,  
          USER_COMMENT VARCHAR(2000),
          CONSTRAINT PK_EXCLUDE_DEPR_GROUP PRIMARY KEY (DEPR_GROUP_ID) 
          )
          NOCOMPRESS NOLOGGING
          TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_EXCLUDE_PROPERTY','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_EXCLUDE_PROPERTY (
        PROP_KEY NUMBER(22) NOT NULL ENABLE,
        USER_ID VARCHAR2(35) NOT NULL,  
        USER_COMMENT VARCHAR(2000),
        CONSTRAINT PK_EXCLUDE_PROPERTY PRIMARY KEY (PROP_KEY) 
        )
        NOCOMPRESS NOLOGGING
        TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_EXCLUDE_COMPANIES','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_EXCLUDE_COMPANIES (
          COMPANY_ID NUMBER(22) NOT NULL ENABLE,
          USER_ID VARCHAR2(35 BYTE) NOT NULL, 
          USER_COMMENT VARCHAR2(2000 BYTE), 
          CONSTRAINT PK_EXCLUDE_COMPANIES PRIMARY KEY (COMPANY_ID)
          )
          NOCOMPRESS NOLOGGING
          TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_ACCOUNTS','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_ACCOUNTS (
        ACCT_KEY NUMBER(22) NOT NULL ENABLE, --GENERATED FIELD
        COMPANY_ID NUMBER(22) NOT NULL,
        UTIL_ACCT_ID NUMBER(22) NOT NULL,
        BUS_SEGMENT_ID NUMBER(22) NOT NULL,
        UTIL_ACCT VARCHAR2(35),
        BUS_SEGMENT VARCHAR2(35),
        FERC_PLT_ACCT VARCHAR2(35),
        FUNC_CLASS VARCHAR2(35),
        HW_TABLE_LINE_ID NUMBER(22),
        EXPECTED_LIFE NUMBER(22,2),  --source from utility_account
        MORT_CURVE_ID NUMBER(22),  --source from utility_account
        MORT_CURVE_DESC VARCHAR(35),  --source from mortality_curve
        EXCLUDE_FLAG CHAR(1) DEFAULT ''N'',
        CONSTRAINT PK_PA_ACCOUNTS PRIMARY KEY (ACCT_KEY)
        )
        NOCOMPRESS NOLOGGING
        TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_DEPR_GROUP','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_DEPR_GROUP (
            DEPR_GROUP_ID NUMBER(22) NOT NULL ENABLE,
            COMPANY_ID NUMBER(22),
            DEPR_GROUP VARCHAR2(35),
            DEPR_METHOD VARCHAR2(254),
            COR_TREATMENT VARCHAR2(5),
            SALVAGE_TREATMENT VARCHAR2(5),
            STATUS_ID NUMBER(22),
            CURR_RATE NUMBER(22,8),
            CURR_NET_SALV_PCT NUMBER(22,8),
            CURR_EXP_AVG_LIFE NUMBER(22,8),
            CURR_MORT_CURVE_ID NUMBER(22),
            CURR_MORT_CURVE_DESC VARCHAR(35),
            CURR_COR_RATE NUMBER(22,8),
            EXCLUDE_FLAG CHAR(1) DEFAULT ''N'',
            CONSTRAINT PK_PA_DEPR_GROUP PRIMARY KEY (DEPR_GROUP_ID)
            )
            NOCOMPRESS NOLOGGING
            TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_LOCATIONS','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_LOCATIONS (
            ASSET_LOCATION_ID NUMBER(22) NOT NULL ENABLE,
            STATE VARCHAR2(18),
			STATE_CODE VARCHAR2(5),
            COUNTY VARCHAR2(35),
            ZIP_CODE NUMBER(22),
            TOWN VARCHAR2(35),
            HW_REGION VARCHAR2(35),
            MAJOR_LOCATION VARCHAR2(35),
            DIVISION VARCHAR2(35),
            LOCATION_TYPE VARCHAR2(35),
            MINOR_LOCATION VARCHAR2(35),
            TAX_LOCATION VARCHAR2(35),
            CHARGE_LOCATION VARCHAR2(35),
            HW_REGION_ID NUMBER(22),
            EXCLUDE_FLAG CHAR(1) DEFAULT ''N'',
            CONSTRAINT PK_LOCATIONS PRIMARY KEY (ASSET_LOCATION_ID)
            )
            NOCOMPRESS NOLOGGING
            TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_LOCATION_AGG','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_LOCATION_AGG (
            LOCN_KEY NUMBER(22) NOT NULL ENABLE,
            STATE VARCHAR2(18),
            --COUNTY VARCHAR2(35),
            MAJOR_LOCATION VARCHAR2(35),
            --CUSTOM_LOCATION VARCHAR2(35),
            CONSTRAINT PK_LOCATION_AGG PRIMARY KEY (LOCN_KEY)
            )
            NOCOMPRESS NOLOGGING
            TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_PROPERTY','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_PROPERTY (
            PROP_KEY NUMBER(22) NOT NULL ENABLE, --GENERATED FIELD
            PROP_GROUP_ID NUMBER(22) NOT NULL,
            RETIRE_UNIT_ID NUMBER(22) NOT NULL,
            PROP_GROUP VARCHAR2(35),
            RETIRE_UNIT VARCHAR2(35),
            PROP_UNIT VARCHAR2(35),
            EXPECTED_LIFE NUMBER(22,2),
            DEFAULT_LIFE NUMBER(22),
            MORT_CURVE_ID NUMBER(22),
            MORT_CURVE_DESC VARCHAR(35),
            ASSET_ACCT_METHOD VARCHAR2(35),
            RETIRE_METHOD VARCHAR2(35),
            EXCLUDE_FLAG CHAR(1) DEFAULT ''N'',  
            CONSTRAINT PK_PROPERTY PRIMARY KEY (PROP_KEY)
            )
            NOCOMPRESS NOLOGGING
            TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CPR_LEDGER','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_CPR_LEDGER (
            ASSET_ID NUMBER(22) NOT NULL ENABLE,
            COMPANY_ID NUMBER(22) NOT NULL,
            ASSET_LOCATION_ID NUMBER(22) NOT NULL,
            ACCT_KEY NUMBER(22) NOT NULL,
            PROP_KEY NUMBER(22) NOT NULL,
            DEPR_GROUP_ID NUMBER(22) NOT NULL,
            VINTAGE DATE NOT NULL,
            VINTAGE_YEAR INT NOT NULL,
            WO_NBR VARCHAR2(35),
            CURR_AGE NUMBER(5,1),
            CURR_QTY NUMBER(22,2),
            CURR_COST NUMBER(22,2),
            CURR_ADJ NUMBER(22,2),
            AGE_W_QTY NUMBER(22,6),
            AGE_W_CST NUMBER(22,6),
            AGE_W_ADJ NUMBER(22,6),
            MORT_CURVE_ID NUMBER(22),  --populate based on mortality curve control / field used to calculate forecast
            EXPECTED_LIFE NUMBER(22,2),  --populate based on mortality curve control / field used to calculate forecast
            MORT_CURVE_DESC VARCHAR(35),
            REMAIN_LIFE NUMBER(22,2),
            MAX_LIFE NUMBER(22,2),
            VRATE NUMBER(22,8),
            CRATE NUMBER(22,8),
            COMPANY_CURVE_ID NUMBER(22), --populate based on company_account_curve
            COMPANY_EXPECTED_LIFE NUMBER(22), --populate based on company_account_curve
            COMPANY_CURVE_DESC VARCHAR(35),
            ANNUAL_BEGIN_QTY NUMBER(22,2),
            ANNUAL_BEGIN_COST NUMBER(22,2),
            PROJ_RETIRE_QTY NUMBER(22,8),
            PROJ_RETIRE_COST NUMBER(22,8),
            MORT_PCT NUMBER(22,5),
            EXCLUDE_FLAG CHAR(1) DEFAULT ''N'',
            LOCN_KEY NUMBER(22),
            CONSTRAINT PK_PA_CPR_LEDGER PRIMARY KEY (ASSET_ID)
            )
            NOCOMPRESS NOLOGGING
            TABLESPACE PWRPLANT' ;
    
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CPR_LEDGER_TMP','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_CPR_LEDGER_TMP (
            ASSET_ID NUMBER(22) NOT NULL ENABLE,
            PROCESS_YEAR INT NOT NULL,
            CURR_AGE NUMBER(5,1),
            CURR_QTY NUMBER(22,2),
            CURR_COST NUMBER(22,2),
            CURR_ADJ NUMBER(22,2),
            AGE_W_QTY NUMBER(22,6),
            AGE_W_CST NUMBER(22,6),
            AGE_W_ADJ NUMBER(22,6),
            MORT_PCT NUMBER(22,5),
            PROJ_RETIRE_QTY NUMBER(22,8),
            PROJ_RETIRE_COST NUMBER(22,8),
            BEGIN_QTY NUMBER(22,2),
            BEGIN_COST NUMBER(22,2),
            RETIRE_QTY NUMBER(22,2),
            RETIRE_COST NUMBER(22,2),
            ADDITION_QTY NUMBER(22,2),
            ADDITION_COST NUMBER(22,2),
            TRANSFER_QTY NUMBER(22,2),
            TRANSFER_COST NUMBER(22,2),
            ADJUSTMENT_QTY NUMBER(22,2),
            ADJUSTMENT_COST NUMBER(22,2),
            END_QTY NUMBER(22,2),
            END_COST NUMBER(22,2),
            CONSTRAINT PK_CPR_LEDGER_TMP PRIMARY KEY (ASSET_ID)
            )
            NOCOMPRESS NOLOGGING
            TABLESPACE PWRPLANT' ;
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_VINTAGE_SPREADS','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_VINTAGE_SPREADS (
            VIN_SPR_KEY NUMBER(22) NOT NULL,
            COMPANY_ID NUMBER(22) NOT NULL,
            ACCT_KEY NUMBER(22) NOT NULL,
            LOCN_KEY NUMBER(22) NOT NULL,
            PROP_KEY NUMBER(22) NOT NULL, -- Added in 
            DEPR_GROUP_ID NUMBER(22) NOT NULL,
            VINTAGE_YEAR INT NOT NULL,
            EXPECTED_LIFE NUMBER(22,2), 
            MORT_CURVE_DESC VARCHAR(35),
            MAX_LIFE NUMBER(22,2),
            CURR_AGE NUMBER(5,1),
            CURR_QTY NUMBER(22,2),
            CURR_COST NUMBER(22,2),
            CURR_ADJ NUMBER(22,2),
            AGE_W_QTY NUMBER(22,2),
            AGE_W_CST NUMBER(22,2),
            AGE_W_ADJ NUMBER(22,2),
            W_QTY NUMBER(22,2),
            W_COST NUMBER(22,2),
            W_ADJ NUMBER(22,2),
            RECORD_COUNT NUMBER(22,6),
            UNTZ_QTY NUMBER(22,2),
            UNTZ_COST NUMBER(22,2),
            UNTZ_ADJ NUMBER(22,2),
            CONSTRAINT PK_VINTAGE_SPREADS PRIMARY KEY (VIN_SPR_KEY)
            )
            NOCOMPRESS NOLOGGING
            TABLESPACE PWRPLANT' ;
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CPR_ACTIVITY','PWRPLANT');
  
  if doesTableExist = 0 then
    begin
      execute immediate 'CREATE TABLE PWRPLANT.PA_CPR_ACTIVITY (
          ASSET_ID NUMBER(22) NOT NULL ENABLE,
          ACTIVITY_ID NUMBER(22) NOT NULL,
          COMPANY_ID NUMBER(22) NOT NULL,
          ASSET_LOCATION_ID NUMBER(22) NOT NULL,
          LOCN_KEY NUMBER(22) NOT NULL,
          ACCT_KEY NUMBER(22) NOT NULL,
          PROP_KEY NUMBER(22) NOT NULL,
          DEPR_GROUP_ID NUMBER(22) NOT NULL,
          GL_POSTING_DATE DATE,
          OUT_OF_SERVICE_DATE DATE,
          CPR_POSTING_DATE DATE,
          WO_NBR VARCHAR2(35),
          ACTIVITY_STATUS NUMBER(22),
          ACTIVITY_CODE CHAR(8),
          FERC_ACTIVITY VARCHAR2(35),
          ACTIVITY_QTY NUMBER(22,2),
          ACTIVITY_COST NUMBER(22,2),
          EXCLUDE_FLAG CHAR(1) DEFAULT ''N'',
          CONSTRAINT PK_PA_CPR_ACTIVITY PRIMARY KEY (ASSET_ID, ACTIVITY_ID)
          )
          NOCOMPRESS NOLOGGING
          TABLESPACE PWRPLANT' ;
  
    end;
  end if;
    
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CPR_ACTSUM','PWRPLANT');
  
  if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_CPR_ACTSUM (
              COMPANY_ID NUMBER(22) NOT NULL,
              LOCN_KEY NUMBER(22) NOT NULL,
              ACCT_KEY NUMBER(22) NOT NULL,
              PROP_KEY NUMBER(22) NOT NULL,
              DEPR_GROUP_ID NUMBER(22) NOT NULL,
              VINTAGE_YEAR NUMBER(22),
              GL_POSTING_YEAR NUMBER(22),
              ACTIVITY_CODE CHAR(8) NOT NULL,
              FERC_ACTIVITY VARCHAR2(35) NOT NULL,
              ACTIVITY_QTY NUMBER(22,2),
              ACTIVITY_COST NUMBER(22,2)
              )
              NOCOMPRESS NOLOGGING
              TABLESPACE PWRPLANT' ;
      end;
  end if;
      
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_AVERAGE_AGES','PWRPLANT');
  
  if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_AVERAGE_AGES (
              AVG_AGE_KEY NUMBER(22) NOT NULL ENABLE,
              COMPANY_ID NUMBER(22) NOT NULL,
              ACCT_KEY NUMBER(22) NOT NULL,
              PROP_KEY NUMBER(22) NOT NULL,  
              DEPR_GROUP_ID NUMBER(22) NOT NULL,
              LOCN_KEY NUMBER(22) NOT NULL,
              PROCESS_YEAR INT NOT NULL,
              CURR_QTY NUMBER(22,2),
              CURR_COST NUMBER(22,2),
              CURR_ADJ NUMBER(22,2),
              AGE_W_QTY NUMBER(22,2),
              AGE_W_CST NUMBER(22,2),
              AGE_W_ADJ NUMBER(22,2),
              W_QTY NUMBER(22,2),
              W_COST NUMBER(22,2),
              W_ADJ NUMBER(22,2),
              RETIRE_QTY NUMBER(22,2),
              RETIRE_COST NUMBER(22,2),  
              ADDITION_QTY NUMBER(22,2),
              ADDITION_COST NUMBER(22,2),
              CONSTRAINT PK_PA_AVERAGE_AGES PRIMARY KEY (AVG_AGE_KEY)
              )
              NOCOMPRESS NOLOGGING
              TABLESPACE PWRPLANT' ;
        
      execute immediate 'alter table PWRPLANT.pa_average_ages add (FLAG_ACP2_COST NCHAR(1) DEFAULT ''N'')';
      execute immediate 'alter table PWRPLANT.pa_average_ages add (FLAG_ACM2_COST NCHAR(1) DEFAULT ''N'')';
      execute immediate 'alter table PWRPLANT.pa_average_ages add (FLAG_PCP1_COST NCHAR(1) DEFAULT ''N'')';
      execute immediate 'alter table PWRPLANT.pa_average_ages add (FLAG_PCM1_COST NCHAR(1) DEFAULT ''N'')';
      execute immediate 'alter table PWRPLANT.pa_average_ages add (FLAG_ACP2_QTY NCHAR(1) DEFAULT ''N'')';
      execute immediate 'alter table PWRPLANT.pa_average_ages add (FLAG_ACM2_QTY NCHAR(1) DEFAULT ''N'')';
      execute immediate 'alter table PWRPLANT.pa_average_ages add (FLAG_PCP1_QTY NCHAR(1) DEFAULT ''N'')';
      execute immediate 'alter table PWRPLANT.pa_average_ages add (FLAG_PCM1_QTY NCHAR(1) DEFAULT ''N'')';
      
      end;
  end if;
      
  doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_CPR_BALANCES','PWRPLANT');
  if doesTableExist = 0 then
        begin
          execute immediate 'CREATE TABLE PWRPLANT.PA_CPR_BALANCES (
                CPR_BALANCES_KEY NUMBER(22) NOT NULL ENABLE,
                COMPANY_ID NUMBER(22) NOT NULL,
                ACCT_KEY NUMBER(22) NOT NULL,
                DEPR_GROUP_ID NUMBER(22) NOT NULL,
                LOCN_KEY NUMBER(22) NOT NULL,
                PROP_KEY NUMBER(22) NOT NULL,
                VINTAGE_YEAR INT NOT NULL,
                PROCESS_YEAR INT NOT NULL,
                PROJ_RETIRE_QTY NUMBER(22,8),
                PROJ_RETIRE_COST NUMBER(22,8),
                ANNUAL_BEGIN_QTY NUMBER(22,2),
                ANNUAL_BEGIN_COST NUMBER(22,2),
                RETIRE_QTY NUMBER(22,2),
                RETIRE_COST NUMBER(22,2),
                ADDITION_QTY NUMBER(22,2),
                ADDITION_COST NUMBER(22,2),
                TRANSFER_QTY NUMBER(22,2),
                TRANSFER_COST NUMBER(22,2),
                ADJUSTMENT_QTY NUMBER(22,2),
                ADJUSTMENT_COST NUMBER(22,2),
                ANNUAL_END_QTY NUMBER(22,2),
                ANNUAL_END_COST NUMBER(22,2),
                COST_OF_REMOVAL NUMBER(22,2),
                SALVAGE_CASH NUMBER(22,2),
                SALVAGE_RETURNS NUMBER(22,2),
                FCST_COR_PROJ NUMBER(22,5),
                FCST_COR_ACTUAL NUMBER(22,5),
                CONSTRAINT PK_PA_CPR_BALANCES PRIMARY KEY (CPR_BALANCES_KEY)
                )
                NOCOMPRESS NOLOGGING
                TABLESPACE PWRPLANT' ;
          
        end;
    end if;
        
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_RETIRE_WO','PWRPLANT');
  
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_RETIRE_WO (
              WO_ID NUMBER(22) NOT NULL,
              CHARGE_YEAR NUMBER(22) NOT NULL,
              WO_NUMBER VARCHAR2(35) NOT NULL,
              WO_DESC VARCHAR2(35),
              WO_STATUS VARCHAR2(35),
              BUS_SEGMENT VARCHAR2(35) NOT NULL,
              COMPANY_ID NUMBER(22) NOT NULL,
              IN_SERVICE_DATE DATE,
              CLOSE_DATE DATE,
              SUSPENDED_DATE DATE,
              ADDITION_QTY NUMBER(22,4),
              ADDITION_COST NUMBER(22,2),
              RETIRE_QTY NUMBER(22,4),
              RETIRE_COST NUMBER(22,2),
              RECORDED_COR NUMBER(22,2),
              RECORDED_SALVAGE NUMBER(22,2),
              RETIRE_FLAG CHAR(1) DEFAULT ''N'',
              COR_FLAG CHAR(1) DEFAULT ''N'',
              SALVAGE_FLAG CHAR(1) DEFAULT ''N'',
              LAST_CHARGE_YEAR NUMBER(22),
              FUNDING_WO_IND NUMBER(22), 
              CLOSING_OPTION VARCHAR2(35),
              RDESC_FLAG CHAR(1) DEFAULT ''N'',
              CONSTRAINT PK_PA_RETIRE_WO PRIMARY KEY(WO_ID, CHARGE_YEAR)
              )
              NOCOMPRESS NOLOGGING
              TABLESPACE PWRPLANT' ;
      
      end;
      
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_DEPR_BALANCES','PWRPLANT');
  
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_DEPR_BALANCES (
              --SET_OF_BOOKS_ID NUMBER(22,0) NOT NULL, 
              DEPR_GROUP_ID NUMBER(22,0) NOT NULL,
              GL_POST_DATE DATE NOT NULL, 
              DEPR_LEDGER_STATUS NUMBER(22,0) NOT NULL, 
              BEGIN_RESERVE NUMBER(22,2) DEFAULT 0, 
              END_RESERVE NUMBER(22,2) DEFAULT 0, 
              BEGIN_BALANCE NUMBER(22,2) DEFAULT 0, 
              ADDITIONS NUMBER(22,2) DEFAULT 0, 
              RETIREMENTS NUMBER(22,2) DEFAULT 0, 
              TRANSFERS_IN NUMBER(22,2) DEFAULT 0, 
              TRANSFERS_OUT NUMBER(22,2) DEFAULT 0, 
              ADJUSTMENTS NUMBER(22,2) DEFAULT 0, 
              END_BALANCE NUMBER(22,2) DEFAULT 0, 
              DEPRECIATION_EXPENSE NUMBER(22,2) DEFAULT 0, 
              DEPR_EXP_ADJUST NUMBER(22,2) DEFAULT 0, 
              DEPR_EXP_ALLOC_ADJUST NUMBER(22,2) DEFAULT 0, 
              COST_OF_REMOVAL NUMBER(22,2) DEFAULT 0, 
              SALVAGE_RETURNS NUMBER(22,2) DEFAULT 0, 
              SALVAGE_CASH NUMBER(22,2) DEFAULT 0, 
              RESERVE_CREDITS NUMBER(22,2) DEFAULT 0, 
              RESERVE_ADJUSTMENTS NUMBER(22,2) DEFAULT 0, 
              RESERVE_TRAN_IN NUMBER(22,2) DEFAULT 0, 
              RESERVE_TRAN_OUT NUMBER(22,2) DEFAULT 0, 
              GAIN_LOSS NUMBER(22,2) DEFAULT 0, 
              CURRENT_NET_SALVAGE_AMORT NUMBER(22,2) DEFAULT 0, 
              COR_BEG_RESERVE NUMBER(22,2) DEFAULT 0, 
              COR_EXPENSE NUMBER(22,2) DEFAULT 0, 
              COR_EXP_ADJUST NUMBER(22,2) DEFAULT 0, 
              COR_EXP_ALLOC_ADJUST NUMBER(22,2) DEFAULT 0, 
              COR_RES_TRAN_IN NUMBER(22,2) DEFAULT 0, 
              COR_RES_TRAN_OUT NUMBER(22,2) DEFAULT 0, 
              COR_RES_ADJUST NUMBER(22,2) DEFAULT 0, 
              COR_END_RESERVE NUMBER(22,2) DEFAULT 0, 
              SALVAGE_EXPENSE NUMBER(22,2) DEFAULT 0, 
              SALVAGE_EXP_ADJUST NUMBER(22,2) DEFAULT 0, 
              SALVAGE_EXP_ALLOC_ADJUST NUMBER(22,2) DEFAULT 0, 
              RESERVE_BLENDING_ADJUSTMENT NUMBER(22,2) DEFAULT 0, 
              RESERVE_BLENDING_TRANSFER NUMBER(22,2) DEFAULT 0, 
              BUS_SEGMENT_ID NUMBER(22,0),
              COMPANY_ID NUMBER(22,0),
              FUNC_CLASS_ID NUMBER(22,0),
              UTIL_ACCT_ID NUMBER(22,0),
              ACCT_KEY NUMBER(22,0),
              BEGIN_THEO_RESERVE NUMBER(22,2),
              END_THEO_RESERVE NUMBER(22,2),
              CONSTRAINT PK_DEPR_BALANCES PRIMARY KEY (DEPR_GROUP_ID, GL_POST_DATE)
              )
              NOCOMPRESS NOLOGGING
              TABLESPACE PWRPLANT' ;
      
      end;
      
    end if;
      
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_DEPR_BALANCES_YR','PWRPLANT');
  
    if doesTableExist = 0 then
      begin
        execute immediate 'CREATE TABLE PWRPLANT.PA_DEPR_BALANCES_YR (
              --SET_OF_BOOKS_ID NUMBER(22,0) NOT NULL, 
              DEPR_GROUP_ID NUMBER(22,0) NOT NULL,
              GL_POST_YEAR NUMBER(22,0) NOT NULL, 
              DEPR_LEDGER_STATUS NUMBER(22,0) NOT NULL, 
              BEGIN_RESERVE NUMBER(22,2) DEFAULT 0, 
              END_RESERVE NUMBER(22,2) DEFAULT 0, 
              BEGIN_BALANCE NUMBER(22,2) DEFAULT 0, 
              ADDITIONS NUMBER(22,2) DEFAULT 0, 
              RETIREMENTS NUMBER(22,2) DEFAULT 0, 
              TRANSFERS NUMBER(22,2) DEFAULT 0, 
              ADJUSTMENTS NUMBER(22,2) DEFAULT 0, 
              END_BALANCE NUMBER(22,2) DEFAULT 0, 
              DEPRECIATION_EXPENSE NUMBER(22,2) DEFAULT 0, 
              COST_OF_REMOVAL NUMBER(22,2) DEFAULT 0, 
              SALVAGE_RETURNS NUMBER(22,2) DEFAULT 0, 
              SALVAGE_CASH NUMBER(22,2) DEFAULT 0, 
              RESERVE_CREDITS NUMBER(22,2) DEFAULT 0, 
              RESERVE_ADJUSTMENTS NUMBER(22,2) DEFAULT 0, 
              RESERVE_TRANSFERS NUMBER(22,2) DEFAULT 0, 
              GAIN_LOSS NUMBER(22,2) DEFAULT 0, 
              CURRENT_NET_SALVAGE_AMORT NUMBER(22,2) DEFAULT 0, 
              COR_BEG_RESERVE NUMBER(22,2) DEFAULT 0, 
              COR_EXPENSE NUMBER(22,2) DEFAULT 0, 
              COR_RES_TRANSFERS NUMBER(22,2) DEFAULT 0, 
              COR_RES_ADJUST NUMBER(22,2) DEFAULT 0, 
              COR_END_RESERVE NUMBER(22,2) DEFAULT 0, 
              SALVAGE_EXPENSE NUMBER(22,2) DEFAULT 0, 
              RESERVE_BLENDING_ADJUSTMENT NUMBER(22,2) DEFAULT 0, 
              RESERVE_BLENDING_TRANSFER NUMBER(22,2) DEFAULT 0, 
              BUS_SEGMENT_ID NUMBER(22,0),
              COMPANY_ID NUMBER(22,0),
              FUNC_CLASS_ID NUMBER(22,0),
              UTIL_ACCT_ID NUMBER(22,0),
              ACCT_KEY NUMBER(22,0),
              BEGIN_THEO_RESERVE NUMBER(22,2),
              END_THEO_RESERVE NUMBER(22,2),
              CONSTRAINT PK_DEPR_BALANCES_YR PRIMARY KEY (DEPR_GROUP_ID, GL_POST_YEAR)
              )
              NOCOMPRESS NOLOGGING
              TABLESPACE PWRPLANT' ;
                  
      end;
      
    end if;
                  
    doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PA_FILTER_LOOKUP_CACHE','PWRPLANT');
  
    if doesTableExist = 0 then
      begin
        --ADDED FOR LOOKUP CACHING 2013-08-19  
        execute immediate 'CREATE TABLE PWRPLANT.PA_FILTER_LOOKUP_CACHE (  
          PA_LOOKUP_NAME NVARCHAR2(50) NOT NULL ENABLE, 
          PA_KEY NVARCHAR2(100) NOT NULL ENABLE, 
          PA_VALUE NVARCHAR2(100) NOT NULL ENABLE, 
          COMPANY_ID number(22,0) NOT NULL ENABLE,
          CONSTRAINT PA_PK_FILTER_LOOKUP_CACHE PRIMARY KEY (PA_LOOKUP_NAME, PA_KEY,COMPANY_ID) 
          )
          NOCOMPRESS NOLOGGING
          TABLESPACE PWRPLANT' ;
      
      end;
      
    end if;
    
    
  end;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2021, 0, 10, 4, 3, 1, 041290, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.1_maint_041290_anlyt_08_tables.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;