
/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048888_lessor_04_ilr_import_renewal_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/18/2017 Shane "C" Ward		Set up Lessor ILR Import Tables
||============================================================================
*/

INSERT INTO PP_IMPORT_TYPE
            (import_type_id,
             description,
             long_description,
             import_table_name,
             archive_table_name,
             allow_updates_on_add,
             delegate_object_name,
             archive_additional_columns)
VALUES      (515,
             'Add: Lessor ILR Renewal Options',
             'Lessor ILR Renewal Options',
             'lsr_import_ilr_renewal',
             'lsr_import_ilr_renewal_archive',
             1,
             'nvo_lsr_logic_import',
             'ilr_renewal_id, ilr_renewal_option_id');
			 
insert into pp_import_type_subsystem (import_type_id, import_subsystem_id) values (515, 14);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 515,
             'ilr_id',
             'ILR ID',
             'ilr_id_xlate',
             1,
             1,
             'number(22,0)',
             'lsr_ilr',
             1,
             'ilr_id');
			 
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 515,
             'revision',
             'Revision',
             NULL,
             0,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);
			 
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 515,
             'ilr_renewal_prob_id',
             'Probability of Exercising Renewal',
             'ilr_renewal_prob_id_xlate',
             0,
             1,
             'number(22,0)',
             'ls_ilr_renewal_probability',
             1,
             'ilr_renewal_probability_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 515,
             'renewal_notice',
             'Renewal Notice (in Months)',
             NULL,
             0,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 515,
             'renewal_frequency_id',
             'Renewal Frequency',
             'renewal_frequency_id_xlate',
             0,
             1,
             'number(22,0)',
             'ls_payment_freq',
             1,
             'payment_freq_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 515,
             'renewal_number_of_terms',
             'Renewal Number of Terms',
             NULL,
             0,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 515,
             'renewal_amount_per_term',
             'Renewal Amount Per Term',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

			 
INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      ( 1086,
             'Renewal Probability Description',
             'Renewal Probability',
             'ilr_renewal_probability_id',
             '( select b.ilr_renewal_probability_id from ls_ilr_renewal_probability b where upper(trim( <importfield> )) = upper( trim( b.description ) ) )',
             0,
             'ls_ilr_renewal_probability',
             'description');
			 
INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      (1090,
             'Lessor ILR Number',
             'Lessor ILR Number',
             'ilr_id',
             '( select b.ilr_id from lsr_ilr b where upper( nvl(trim( <importfield> ), trim(b.ilr_number)) ) = upper( trim( b.ilr_number ) ) )',
             0,
             'lsr_ilr',
             'ilr_number' ); 


INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 515,
             'ilr_id',
             1090);
			 
			 

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 515,
             'renewal_frequency_id',
             1047);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 515,
             'renewal_frequency_id',
             1064);

			 
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 515,
             'ilr_renewal_prob_id',
             1086);
			 
--Default Template
INSERT INTO pp_import_template 
(import_template_id,
             import_type_id,
             description,
             long_description,
             do_update_with_add,
             is_autocreate_template)
VALUES (
	( SELECT Max(import_template_id) + 1
               FROM   PP_IMPORT_TEMPLATE ),
	515,
	'Add: Lessor ILR Renewal Options',
	'Default Lessor ILR Renewal Options Template',
	1,
	0);
	


INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 515 AND
                      description = 'Add: Lessor ILR Renewal Options' ),
             1,
             515,
             'ilr_id',
             1090);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 515 AND
                      description = 'Add: Lessor ILR Renewal Options' ),
             2,
             515,
             'revision',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 515 AND
                      description = 'Add: Lessor ILR Renewal Options' ),
             3,
             515,
             'ilr_renewal_prob_id',
             1086);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 515 AND
                      description = 'Add: Lessor ILR Renewal Options' ),
             4,
             515,
             'renewal_notice',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 515 AND
                      description = 'Add: Lessor ILR Renewal Options' ),
             5,
             515,
             'renewal_frequency_id',
             1047);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 515 AND
                      description = 'Add: Lessor ILR Renewal Options' ),
             6,
             515,
             'renewal_number_of_terms',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 515 AND
                      description = 'Add: Lessor ILR Renewal Options' ),
             7,
             515,
             'renewal_amount_per_term',
             NULL); 

			 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3801, 0, 2017, 1, 0, 0, 48888, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048888_lessor_04_ilr_import_renewal_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;		