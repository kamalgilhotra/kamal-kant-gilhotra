/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008037_matlrec.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/31/2011 Zack Spratling Point Release
||============================================================================
*/

insert into CR_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, LONG_DESCRIPTION)
values
   (266, 'CR Matlrec GL Journal Category',
    'The GL Journal Category that will be used for any material reconciliation journals. The default is null, which will cause the CR Manual GL Journal Category control value to be used.');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (27, 0, 10, 3, 3, 0, 8037, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008037_matlrec.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
