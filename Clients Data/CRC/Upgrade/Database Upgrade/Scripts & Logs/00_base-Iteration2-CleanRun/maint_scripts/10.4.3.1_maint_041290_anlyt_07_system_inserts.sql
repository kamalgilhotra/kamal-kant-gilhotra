/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041290_anlyt_07_system_inserts.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.1 11/17/2014 Chad Theilman    Asset Analytics - System Data
||============================================================================
*/

declare
    begin
	
	delete from PWRPLANT.PA_CHART;
    
    INSERT INTO PWRPLANT.PA_CHART values (1000,'PR_UnderCost','Bar2D','SingleSeries','Under Retired by Cost','ferc_plt_acct',1000,1000,null,null,null,null,'$',null,null,null,'Summary Account','Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1001,'PR_OverCost','Bar2D','SingleSeries','Over Retired by Cost','ferc_plt_acct',1001,1001,null,null,null,null,'$',null,null,null,'Summary Account','Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1003,'PR_UnderQty','Bar2D','SingleSeries','Under Retired by Quantity','ferc_plt_acct',1047,1003,null,null,null,null,null,null,null,null,'Summary Account','Quantity');
	INSERT INTO PWRPLANT.PA_CHART values (1004,'PR_OverQty','Bar2D','SingleSeries','Over Retired by Quantity','ferc_plt_acct',1048,1004,null,null,null,null,null,null,null,null,'Summary Account','Quantity');
	INSERT INTO PWRPLANT.PA_CHART values (1006,'SD_UnitzCombo','MSCombiDY2D','MultiSeries','Survivor Distribution - Unitized','VINTAGE_INTERVAL',1003,1025,null,null,null,null,'$',null,null,null,'Vintage Interval',null);
	INSERT INTO PWRPLANT.PA_CHART values (1007,'SD_TotalCombo','MSCombiDY2D','MultiSeries','Survivor Distribution - Total','VINTAGE_INTERVAL',1005,1026,null,null,null,null,'$',null,null,null,'Vintage Interval',null);
	INSERT INTO PWRPLANT.PA_CHART values (1008,'SD_CostPerUnit','Column2D','SingleSeries','Unitized Cost per Unit','VINTAGE_INTERVAL',1007,1025,null,null,null,null,'$',null,null,null,'Vintage Interval','Cost per Unit');
	INSERT INTO PWRPLANT.PA_CHART values (1009,'LA_AgeDist_Cost','ScrollStackedColumn2D','MultiSeries','Asset Distribution by Age and Cost','AGE',1008,1035,5,50,null,null,'$',null,null,null,'Age',null);
	INSERT INTO PWRPLANT.PA_CHART values (1010,'LA_PastExp_Cost','Column2D','SingleSeries','Assets Past Expected Life by Cost','YEARS_PAST_EXPECTED',1011,1036,null,null,null,null,'$',null,null,null,'Years Past Expected Life','Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1011,'LA_PastMax_Cost','Column2D','SingleSeries','Assets Past Max Life by Cost','YEARS_PAST_MAX',1012,1037,null,null,null,null,'$',null,null,null,'Years Past Maximum Life','Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1012,'LA_AgeDist_Qty','ScrollStackedColumn2D','MultiSeries','Asset Distribution by Age and Quantity','AGE',1013,1035,5,50,null,null,null,null,null,null,'Age',null);
	INSERT INTO PWRPLANT.PA_CHART values (1013,'LA_PastExp_Qty','Column2D','SingleSeries','Assets Past Expected Life by Quantity','YEARS_PAST_EXPECTED',1016,1036,null,null,null,null,null,null,null,null,'Years Past Expected Life','Quantity');
	INSERT INTO PWRPLANT.PA_CHART values (1014,'LA_PastMax_Qty','Column2D','SingleSeries','Assets Past Max Life by Quantity','YEARS_PAST_MAX',1017,1037,null,null,null,null,null,null,null,null,'Years Past Maximum Life','Quantity');
	INSERT INTO PWRPLANT.PA_CHART values (1015,'LA_AgeDist_Esc','ScrollStackedColumn2D','MultiSeries','Asset Distribution by Age and Escalated Cost','AGE',1018,1035,5,50,null,null,'$',null,null,null,'Age',null);
	INSERT INTO PWRPLANT.PA_CHART values (1016,'LA_PastExp_Esc','Column2D','SingleSeries','Assets Past Expected Life by Escalated','YEARS_PAST_EXPECTED',1021,1036,null,null,null,null,'$',null,null,null,'Years Past Expected Life','Escalated Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1017,'LA_PastMax_Esc','Column2D','SingleSeries','Assets Past Max Life by Escalated Cost','YEARS_PAST_MAX',1022,1037,null,null,null,null,'$',null,null,null,'Years Past Maximum Life','Escalated Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1018,'LE_ZeroQtyCosts','StackedColumn2D','MultiSeries','Percent (%) of Costs Assigned to Assets with Zero Quantity','COMPANY',1023,1048,null,null,null,null,null,'%',null,null,'Company',null);
	INSERT INTO PWRPLANT.PA_CHART values (1019,'LE_NegQtyPosCost','Bar2D','SingleSeries','Count of Asset Records with (-) Qty, (+) Costs','COMPANY',1025,1049,null,null,null,null,null,null,null,null,'Company','# of Records');
	INSERT INTO PWRPLANT.PA_CHART values (1020,'LE_FutureAssets','MSCombiDY2D','MultiSeries','Future Assets (Cost and Quantity)','VINTAGE_YEAR',1027,1050,null,null,null,null,'$',null,null,null,'Vintage Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1021,'LE_PosQtyNegCosts','Bar2D','SingleSeries','Count of Asset Records with (+) Qty, (-) Costs','COMPANY',1025,1051,null,null,null,null,null,null,null,null,'Company','# of Records');
	INSERT INTO PWRPLANT.PA_CHART values (1022,'LE_ZeroCostQtys','StackedColumn2D','MultiSeries','Percent (%) of Quantities Assigned to Assets with Zero Cost','COMPANY',1092,1052,null,null,null,null,null,'%',null,null,'Company',null);
	INSERT INTO PWRPLANT.PA_CHART values (1023,'RP_AnnualRetirements','MSCombiDY2D','MultiSeries','Annual Retirements (Inverted)','PROCESS_YEAR',1028,1071,null,null,null,null,'$',null,null,null,'Process Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1025,'RP_PositiveRetirements_Cost','Column2D','SingleSeries','Positive Retirement Costs','VINTAGE_INTERVAL',1031,1081,null,null,null,null,'$',null,null,null,'Vintage Interval','Retirement Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1027,'RP_PositiveRetirements_Qty','Column2D','SingleSeries','Positive Retirement Quantities','VINTAGE_INTERVAL',1032,1091,null,null,null,null,null,null,null,null,'Vintage Interval','Retirement Quantity');
	INSERT INTO PWRPLANT.PA_CHART values (1028,'AP_AnnualAdditions','MSCombiDY2D','MultiSeries','Annual Additions','PROCESS_YEAR',1033,1096,null,null,null,null,'$',null,null,null,'Process Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1030,'AP_NegativeAddsCost','Column2D','SingleSeries','Negative Addition Costs (Inverted)','VINTAGE_INTERVAL',1036,1106,null,null,null,null,'$',null,null,null,'Vintage Interval','Addition Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1032,'AP_NegativeAddsQty','Column2D','SingleSeries','Negative Addition Quantities (Inverted)','VINTAGE_INTERVAL',1037,1116,null,null,null,null,null,null,null,null,'Vintage Interval','Addition Quantity');
	INSERT INTO PWRPLANT.PA_CHART values (1033,'UC_AverageCosts','MSCombiDY2D','MultiSeries','Average Unit Costs - Handy Whitman Rates','VINTAGE_INTERVAL',1038,1151,null,null,null,null,'$',null,null,null,'Vintage Interval',null);
	INSERT INTO PWRPLANT.PA_CHART values (1034,'RA_EndReserveTrend','MSLine','MultiSeries','Annual Trend in Life Reserve (Actual and Theoretical)','GL_POSTING_YEAR',1040,1122,null,null,null,null,'$',null,null,null,'Posting Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1036,'RA_AnnualChange','Column2D','SingleSeries','Annual Change in Life Reserve','GL_POSTING_YEAR',1043,1124,null,null,null,null,'$',null,null,null,'Posting Year','Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1037,'CR_RemovalPerRetirement','MSCombiDY2D','MultiSeries','Cost of Removal vs. Retirement Quantity (Inverted)','PROCESS_YEAR',1044,1137,null,null,null,null,'$',null,null,null,'Process Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1038,'CR_RemovalPerAddition','MSCombiDY2D','MultiSeries','Cost of Removal vs. Addition Quantity (Inverted COR)','PROCESS_YEAR',1044,1142,null,null,null,null,'$',null,null,null,'Process Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1039,'PR_AnnualCost','MSColumn2D','MultiSeries','Actual vs. Projected Retirement Costs (Inverted)','PROCESS_YEAR',1078,1147,null,null,null,null,'$',null,null,null,'Process Year','Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1043,'DL_AnnualEndBalance','Line','SingleSeries','Ending Balances ($)','GL_POSTING_YEAR',1055,1166,null,null,null,null,'$',null,null,null,'Posting Year','Total Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1044,'DL_AnnualRetires','Line','SingleSeries','Retirements (Inverted $)','GL_POSTING_YEAR',1056,1166,null,null,null,null,'$',null,null,null,'Posting Year','Retirement Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1045,'DL_AnnualAdditions','Line','SingleSeries','Additions ($)','GL_POSTING_YEAR',1057,1166,null,null,null,null,'$',null,null,null,'Posting Year','Addition Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1046,'DL_AnnualCOR','Line','SingleSeries','Cost of Removal (Inverted $)','GL_POSTING_YEAR',1058,1166,null,null,null,null,'$',null,null,null,'Posting Year','COR $');
	INSERT INTO PWRPLANT.PA_CHART values (1047,'DL_AnnualCorVsRetires','MSLine','MultiSeries','Cost of Removal vs. Retirements (Inverted $)','GL_POSTING_YEAR',1058,1166,null,null,null,null,'$',null,'$',null,'Posting Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1048,'DL_AnnualCorVsAdds','MSLine','MultiSeries','Cost of Removal vs. Additions (Inverted COR $)','GL_POSTING_YEAR',1058,1166,null,null,null,null,'$',null,'$',null,'Posting Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1049,'DL_CorBeginReserve','Line','SingleSeries','COR / Begin COR Reserve (%)','GL_POSTING_YEAR',1059,1170,null,null,null,null,null,'%',null,null,'Posting Year','% of Begin Reserve');
	INSERT INTO PWRPLANT.PA_CHART values (1050,'DL_CORExpCost','Line','SingleSeries','COR Expense Added to Reserve','GL_POSTING_YEAR',1087,1174,null,null,null,null,'$',null,null,null,'Posting Year','Total Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1051,'DL_CorReserveChange','Column2D','SingleSeries','Annual Change in COR Reserve','GL_POSTING_YEAR',1060,1178,null,null,null,null,'$',null,null,null,'Posting Year','Total Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1052,'DL_RetireReserveRatio','Line','SingleSeries','Retirements / Begin Life Reserve (%)','GL_POSTING_YEAR',1061,1182,null,null,null,null,null,'%',null,null,'Posting Year','% of Begin Reserve');
	INSERT INTO PWRPLANT.PA_CHART values (1053,'DL_DeprExpCost','Line','SingleSeries','Depreciation Expense Added to Reserve','GL_POSTING_YEAR',1088,1186,null,null,null,null,'$',null,null,null,'Posting Year','Total Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1054,'DL_LifeReserveChange','Column2D','SingleSeries','Annual Change in Life Reserve','GL_POSTING_YEAR',1062,1190,null,null,null,null,'$',null,null,null,'Posting Year','Total Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1055,'AA_WeightedAges','MSLine','MultiSeries','Average Age Trend','PROCESS_YEAR',1063,1194,null,null,null,null,null,null,null,null,'Process Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1056,'AA_Additions','MSCombiDY2D','MultiSeries','Addition Activity','PROCESS_YEAR',1067,1199,null,null,null,null,'$',null,null,null,'Process Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1057,'AA_Retirements','MSCombiDY2D','MultiSeries','Retirement Activity (Inverted)','PROCESS_YEAR',1069,1204,null,null,null,null,'$',null,null,null,'Process Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1058,'AA_AnnualChange','MSLine','MultiSeries','Large Annual Age Changes','PROCESS_YEAR',1063,1209,null,null,null,null,null,null,null,null,'Process Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1059,'AA_PeriodChange','MSLine','MultiSeries','Large Period Age Changes','PROCESS_YEAR',1063,1212,null,null,null,null,null,null,null,null,'Process Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1060,'RE_ZCost_Qty','Column2D','SingleSeries','Retirement Quantities with Zero Cost (Inverted)','GL_POSTING_YEAR',1070,1215,null,null,null,null,null,null,null,null,'Posting Year','Net Quantity');
	INSERT INTO PWRPLANT.PA_CHART values (1061,'RE_ZQty_Cost','Column2D','SingleSeries','Retirement Costs with Zero Quantity (Inverted)','GL_POSTING_YEAR',1071,1218,null,null,null,null,'$',null,null,null,'Posting Year','Net Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1062,'AP_VintageAdditions','MSCombiDY2D','MultiSeries','Additions by Vintage','VINTAGE_INTERVAL',1072,1223,null,null,null,null,'$',null,null,null,'Vintage Interval',null);
	INSERT INTO PWRPLANT.PA_CHART values (1063,'RP_VintageRetirements','MSCombiDY2D','MultiSeries','Retirements by Vintage (Inverted)','VINTAGE_INTERVAL',1081,1224,null,null,null,null,'$',null,null,null,'Vintage Interval',null);
	INSERT INTO PWRPLANT.PA_CHART values (1064,'PR_VintageCostCurve','MSLine','MultiSeries','Retirement Cost Curve (Inverted)','VINTAGE_INTERVAL',1076,1225,null,null,null,null,'$',null,'$',null,'Vintage Interval','Cost');
	INSERT INTO PWRPLANT.PA_CHART values (1066,'PR_VintageQtyCurve','MSLine','MultiSeries','Retirement Quantity Curve (Inverted)','VINTAGE_INTERVAL',1077,1226,null,null,null,null,null,null,null,null,'Vintage Interval',null);
	INSERT INTO PWRPLANT.PA_CHART values (1067,'PR_AnnualQty','MSColumn2D','MultiSeries','Actual vs. Projected Retirement Quantities (Inverted)','PROCESS_YEAR',1079,1228,null,null,null,null,null,null,null,null,'Process Year',null);
	INSERT INTO PWRPLANT.PA_CHART values (1068,'RA_PctOverUnder','Bar2D','SingleSeries','Percent Over - Under Reserved (Life Reserve)','GL_POSTING_YEAR',1084,1229,null,null,null,null,null,'%',null,null,'Posting Year','% Over/Under Reserved');
	INSERT INTO PWRPLANT.PA_CHART values (1069,'CR_CORRetireMissing','Column2D','SingleSeries','COR ($) without Retirement Cost or Quantity (Inverted)','VINTAGE_INTERVAL',1085,1230,null,null,null,null,'$',null,null,null,'Vintage Interval','COR $');
	INSERT INTO PWRPLANT.PA_CHART values (1070,'CR_CORperUnit','Column2D','SingleSeries','Cost of Removal ($) per Unit Retired','PROCESS_YEAR',1086,1137,null,null,null,null,'$',null,null,null,'Process Year','Cost per Unit');
	INSERT INTO PWRPLANT.PA_CHART values (1071,'SV_SalvagePerRetirement','MSCombiDY2D','MultiSeries','Salvage Cash vs. Retirement Quantity (Inverted Quantity)','PROCESS_YEAR',1089,1279,null,null,null,null,'$',null,null,null,'Process Year','Salvage Cash');
	INSERT INTO PWRPLANT.PA_CHART values (1072,'SV_SalvageperUnit','Column2D','SingleSeries','Salvage ($) per Unit Retired','PROCESS_YEAR',1090,1279,null,null,null,null,'$',null,null,null,'Process Year','Salvage per Unit');
	
    

    delete from PWRPLANT.PA_CHART_SERIES;
    

	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1000,'SD_Unitz_Cost',null,null,null,null,'006699','Unitized Cost',1006,1003,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1001,'SD_Unitz_Qty','Line','S',null,5,'FF9933','Unitized Quantity',1006,1004,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1002,'SD_Total_Cost',null,null,null,null,'006699','Total Cost',1007,1005,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1003,'SD_Total_Qty','Line','S',null,5,'FF9933','Total Quantity',1007,1006,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1004,'LA_TotalCost_BEL','Bar',null,0,0,'66CC66','Before Expected Life',1009,1008,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1005,'LA_TotalCost_PEL','Bar',null,0,0,'FFCC33','Past Expected Life',1009,1009,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1006,'LA_TotalCost_PML','Bar',null,0,0,'990000','Past Max Life',1009,1010,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1007,'LA_TotalQty_BEL','Bar',null,0,0,'66CC66','Before Expected Life',1012,1013,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1008,'LA_TotalQty_PEL','Bar',null,0,0,'FFCC33','Past Expected Life',1012,1014,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1009,'LA_TotalQty_PML','Bar',null,0,0,'990000','Past Max Life',1012,1015,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1010,'LA_TotalEsc_BEL','Bar',null,0,0,'66CC66','Before Expected Life',1015,1018,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1011,'LA_TotalEsc_PEL','Bar',null,0,0,'FFCC33','Past Expected Life',1015,1019,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1012,'LA_TotalEsc_PML','Bar',null,0,0,'990000','Past Max Life',1015,1020,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1013,'LE_ZeroQtyCosts','Bar',null,0,0,'990000','Zero Qty Cost %',1018,1023,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1014,'LE_NonZeroQtyCosts','Bar',null,0,0,'006699','Non Zero Qty Cost %',1018,1024,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1015,'LE_TotalCost','Bar',null,null,null,'006699','Total Cost',1020,1027,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1016,'LE_TotalQty','Line','S',1,5,'FF9933','Total Quantity',1020,1026,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1017,'RP_RetireCost','Bar',null,null,null,'006699','Retirement Cost',1023,1028,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1018,'RP_RetireQty','Line','S',1,5,'FF9933','Retirement Quantity',1023,1029,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1019,'AP_AdditionCost','Bar',null,null,null,'006699','Addition Cost',1028,1033,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1020,'AP_AdditionQty','Line','S',1,5,'FF9933','Addition Quantity',1028,1034,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1021,'SC_AvgServiceCost','Bar',null,null,null,'006699','Average Cost',1033,1038,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1022,'SC_HandyWhitman','Line','S',1,2,'FF9933','Handy Whitman Rate',1033,1039,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1023,'RA_LifeReserveTrend','Line',null,1,2,'006699','Ending Life Reserve',1034,1040,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1024,'RA_TheoReserve','Line','S',1,2,'FF9933','Ending Theo Reserve',1034,1041,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1025,'CR_RemovalCost','Bar',null,null,null,'006699','Cost of Removal',1037,1044,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1026,'CR_RetireQty','Line','S',1,2,'FF9933','Retirement Quantity',1037,1045,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1027,'CR_RemovalCost','Bar',null,null,null,'006699','Cost of Removal',1038,1044,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1028,'CR_AdditionQty','Line','S',1,2,'FF9933','Addition Quantity',1038,1046,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1029,'PR_RetireCost','Bar',null,null,null,'006699','Retirement Cost',1039,1050,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1030,'PR_ProjectedCost','Bar','S',null,-1,'FF9933','Projected Retirement Cost',1039,1051,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1031,'DL_CostOfRemoval','Line',null,null,null,'006699','Cost Of Removal',1047,1058,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1032,'DL_Retirements','Line','S',1,null,'FF9933','Retirements',1047,1056,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1033,'DL_CostOfRemoval','Line',null,null,null,'006699','Cost of Removal',1048,1058,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1034,'DL_Additions','Line','S',1,null,'FF9933','Additions',1048,1057,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1035,'AA_CostWeightedAge','Line',null,2,3,'006699','Cost Weighted Age',1055,1063,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1036,'AA_QuantityWeightedAge','Line','S',2,3,'FF9933','Qty Weighted Age',1055,1064,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1037,'AA_EscCostWeightedAge','Line','S',2,3,'66CC66','Escalated Cost Weighted Age',1055,1065,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1038,'AA_AdditionCost','Bar',null,null,null,'006699','Addition Cost',1056,1067,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1039,'AA_AdditionQty','Line','S',2,5,'FF9933','Addition Quantity',1056,1066,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1040,'AA_RetireCost','Bar',null,null,null,'006699','Retirement Cost',1057,1069,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1041,'AA_RetireQty','Line','S',2,5,'FF9933','Retirement Quantity',1057,1068,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1042,'AA_LCC_CostWeightedAge','Line',null,2,5,'006699','Cost Weighted Age',1058,1063,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1043,'AA_LCC_QtyWeightedAge','Line','S',2,5,'FF9933','Qty Weighted Age',1058,1064,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1044,'AA_LCC_EscWeightedAge','Line','S',2,5,'66CC66','Escalated Cost Weighted Age',1058,1065,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1045,'AA_LPC_CostWeightedAge','Line',null,2,5,'006699','Cost Weighted Age',1059,1063,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1046,'AA_LPC_QtyWeightedAge','Line','S',2,5,'FF9933','Qty Weighted Age',1059,1064,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1047,'AA_LPC_EscWeightedAge','Line','S',2,5,'66CC66','Escalated Cost Weighted Age',1059,1065,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1048,'AP_VintageAdds_Cost','Bar',null,0,0,'006699','Addition Cost',1062,1072,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1049,'AP_VintageAdds_Qty','Line','S',null,5,'FF9933','Addition Quantity',1062,1073,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1050,'RP_VintageRets_Cost','Bar',null,null,null,'006699','Retirement Cost',1063,1074,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1051,'RP_VintageRets_Qty','Line','S',null,5,'FF9933','Retirement Quantity',1063,1075,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1052,'PR_VintageCostCurve','Line',null,null,null,'006699','Retirement Cost',1064,1076,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1053,'PR_VintageCostCurveProj','Line','S',1,null,'FF9933','Projected Retirement Cost',1064,1080,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1054,'PR_VintageQtyCurve','Line',null,null,null,'006699','Retirement Quantity',1066,1077,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1055,'PR_VintageQtyCurve_Proj','Line','S',1,null,'FF9933','Projected Retirement Quantity',1066,1081,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1056,'PR_AnnualCost','Bar',null,0,0,'006699','Retirement Cost',1065,1078,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1057,'PR_AnnualCost_Proj','Bar',null,0,0,'FF9933','Projected Retirement Cost',1065,1082,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1058,'PR_AnnualQty','Bar',null,0,0,'006699','Retirement Quantity',1067,1079,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1059,'PR_AnnualQty_Proj','Bar',null,0,0,'FF9933','Projected Retirement Quantity',1067,1083,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1060,'SV_SalvageCost','Bar',null,null,null,'006699','Salvage Cash',1071,1089,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1061,'SV_RetireQty','Line','S',1,1,'FF9933','Retirement Quantity',1071,1091,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1062,'LE_ZeroCostQty','Bar',null,0,0,'990000','Zero Cost Qty %',1022,1092,null);
	INSERT INTO PWRPLANT.PA_CHART_SERIES values (1063,'LE_NonZeroCostQty','Bar',null,0,0,'006699','Non Zero Cost Qty%',1022,1093,null);
	

    delete from PWRPLANT.PA_COLUMN_FORMAT;
   

    -- Inserts for PA_COLUMN_FORMAT
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (1,'CANCELLED','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (2,'COMPLETED','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (3,'IN_SERVICE','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (4,'OPEN','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (5,'POSTED_TO_CPR','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (6,'UNITIZED','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (7,'PROJRETIREQTY','{0:N}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (8,'RETIRECOST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (9,'PROJRETIRECOST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (10,'TOTALQUANTITY','{0:N}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (11,'TOTALCOST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (12,'UNITIZED_ORIGINAL_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (14,'UNITIZED_ESCALATED_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (15,'ORIGINAL_COST_PER_UNIT','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (16,'ESCALATED_COST_PER_UNIT','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (17,'ZERO_QTY_PCT','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (18,'NONZERO_QTY_PCT','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (19,'RETIRE_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (20,'PROJ_RETIRE_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (21,'DELTA','{0:N}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (22,'UNITIZED_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (23,'SCALED_DIFF','{0:N}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (24,'RETIRE_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (25,'PROJ_RETIRE_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (27,'UNITIZED_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (28,'COST_PER_UNIT','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (29,'TOTAL_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (30,'TOTAL_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (31,'TOTAL_QUANTITY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (32,'TOTAL_ESCALATED_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (33,'ZERO_QTY_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (34,'RECORDCOUNT','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (35,'TOTALRECORDS','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (36,'TOTAL_COSTS','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (37,'CURR_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (38,'CURR_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (39,'ACTIVITY_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (40,'ACTIVITY_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (41,'SCALED_VALUE','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (42,'ADDITION_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (43,'ADDITION_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (44,'AVG_SERVICE_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (45,'AVG_HW_RATE','{0:N2}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (46,'END_LIFE_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (47,'END_THEO_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (48,'END_RATIO','{0:N2}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (49,'BEGIN_LIFE_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (50,'ANNUAL_CHANGE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (51,'CURR_END_LIFE_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (52,'CURR_END_THEO_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (53,'END_BALANCE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (54,'BEGIN_BALANCE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (55,'PCT_DELTA','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (56,'CURR_END_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (57,'GL_POSTING_YEAR','{0:####}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (58,'GL_POSTING_DATE','{0:yyyy-MM-dd}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (59,'VINTAGE_YEAR','{0:####}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (60,'PROCESS_YEAR','{0:####}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (61,'COR','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (62,'CORPERUNIT','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (63,'SCALEDVALUE','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (64,'QTY_DELTA','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (65,'QTY_SCALED_DIFF','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (68,'COR_FLAG','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (69,'COR_PER_UNIT','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (70,'PCT_COR_RETIRE_COST','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (71,'PCT_COR_ADDITION_COST','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (72,'AVG_END_BALANCE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (73,'ADDITIONS','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (74,'RETIREMENTS','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (75,'COST_OF_REMOVAL','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (76,'BEGIN_COR_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (77,'PCT_INCREASE_COR_RESERVE','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (78,'COR_EXPENSE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (79,'END_COR_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (80,'AVG_BEGIN_COR_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (81,'PCT_CHANGE_COR_RESERVE','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (82,'AVG_END_COR_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (83,'BEG_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (84,'PCT_BEGIN_RESERVE','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (85,'AVG_BEG_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (86,'DEPR_EXPENSE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (87,'END_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (88,'BEGIN_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (89,'PCT_CHANGE_LIFE_RESERVE','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (90,'AVG_BEGIN_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (91,'AVG_END_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (92,'RETIREMENT_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (93,'RECORD_COUNT','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (94,'POS_RETIREMENT_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (95,'NEG_RETIREMENT_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (96,'NET_RETIREMENT_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (97,'POS_RETIREMENT_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (98,'NEG_RETIREMENT_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (99,'NET_RETIREMENT_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (100,'ZERO_COST_PCT','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (101,'NONZERO_COST_PCT','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (102,'COST_DELTA','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (103,'ZERO_COST_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (104,'PCT_RESERVED','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (105,'ANNUAL_CHANGE_COR_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (106,'ANNUAL_CHANGE_LIFE_RESERVE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (107,'RETIREMENT_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (108,'SALVAGE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (109,'SALVAGE_PER_UNIT','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (110,'PCT_SALVAGE_RETIRE_COST','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (111,'PCT_COR_BEGIN_RESERVE','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (112,'PCT_COR_EXPENSE_BEGIN_RESERVE','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (113,'PCT_DEPR_EXPENSE_BEGIN_RESERVE','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (114,'DEPRECIATION_EXPENSE','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (115,'PCT_RETIRE_BEGIN_RESERVE','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (116,'PCT_OVER_UNDER_RESERVED','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (117,'PCT_DEPR_BEGIN_RESERVE','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (118,'COST_PER_UNIT_ADDED','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (119,'COST_PER_UNIT_RETIRED','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (120,'POSITIVE_RETIRE_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (121,'ASSOCIATED_RETIRE_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (122,'POSITIVE_RETIRE_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (123,'ASSOCIATED_RETIRE_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (124,'NEGATIVE_ADDITION_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (125,'ASSOCIATED_ADDITION_QTY','{0:N0}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (126,'RESERVE_DELTA','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (127,'COR_PER_UNIT_ADDED','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (128,'COR_PER_UNIT_RETIRED','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (129,'AVG_UNIT_COST','{0:$#,##0.00}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (130,'PCT_COR_EXPENSE_BEGIN_RESERVE','{0:P}');
	INSERT INTO PWRPLANT.PA_COLUMN_FORMAT values (131,'PCT_SALV_RETIRE_COST','{0:P}');
	
    

    delete from PWRPLANT.PA_CONFIG;
    

    -- Inserts for PA_CONFIG (MCM - MADE NULL THE 'SET_OF_BOOKS', 'END_YEAR', 'LEDGER_DATE')
    insert into PWRPLANT.PA_CONFIG values (1,'DEFAULT_VINTAGEINTERVAL','5');
    insert into PWRPLANT.PA_CONFIG values (2,'REQUIRED_FIELDS','VINTAGEINTERVAL,GROUPBY');
    insert into PWRPLANT.PA_CONFIG values (3,'SET_OF_BOOKS','');
    insert into PWRPLANT.PA_CONFIG values (4,'LEDGER_DATE','');
    insert into PWRPLANT.PA_CONFIG values (5,'STARTYEAR','2008');
    insert into PWRPLANT.PA_CONFIG values (6,'ENDYEAR','');
     

    delete from PWRPLANT.PA_DASHBOARD;
    

    -- Inserts for PA_DASHBOARD
    INSERT INTO PWRPLANT.PA_DASHBOARD values (1000,1000,'Projected Retirements by Cost','Missed retirements have significant tax and regulatory implications, as well as increase the average age of assets on the books and decrease future depreciation rates.  Significant under-retirements can signify an inappropriate curve and/or expected life and should be investigated.  Similarly, over-retirements can also be problematic in that they may indicate that asset costs are not being recovered appropriately relative to their use.  Both scenarios should be monitored closely as they will show up in depreciation studies and rate cases.','The Projected Retirements by Cost dashboard is used to identify areas where retirements are potentially being missed and/or over-recorded by providing actual versus projected retirement comparisons based on cost distribution. The projections are calculated based on the vintage distribution of asset quantities, assigned mortality curves and the expected lives of surviving assets. The values show whether an asset group is performing according to expectations for the assigned curve and life, with prominent under- and over-retired accounts highlighted for analysis.  Variance from projections are highlighted for analysis, with red indicating (-) variance (under-retirements) and yellow indicating (+) variance (over-retirements).  Additionally, total yearly projections are plotted against actual retirement costs, as well as vintage cost projections against vintage cost actuals.','Dashboard/SummaryDashboard/?DashboardId=1000','Y','Projected Retirements by Cost Dashboard','ProjectedRetirementsCost');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1001,1001,'Projected Retirements by Quantity','Missed retirements have significant tax and regulatory implications, as well as increase the average age of assets on the books and decrease future depreciation rates.  Significant under-retirements can signify an inappropriate curve and/or expected life and should be investigated.  Similarly, over-retirements can also be problematic in that they may indicate that asset costs are not being recovered appropriately relative to their use.  Both scenarios should be monitored closely as they will show up in depreciation studies and rate cases.','The Projected Retirements by Quantity dashboard is used to identify areas where retirements are potentially being missed and/or over-recorded by providing actual versus projected retirement comparisons based on quantity distribution. The projections are calculated based on the vintage distribution of asset quantities, assigned mortality curves and the expected lives of surviving assets. The values show whether an asset group is performing according to expectations for the assigned curve and life, with prominent under- and over-retired accounts highlighted for analysis.  Variance from projections are highlighted for analysis, with red indicating (-) variance (under-retirements) and yellow indicating (+) variance (over-retirements).  Additionally, total yearly projections are plotted against actual retirement quantities, as well as vintage quantity projections against vintage quantity actuals. ','Dashboard/SummaryDashboard/?DashboardId=1001','Y','Projected Retirements by Quantity Dashboard','ProjectedRetirementsQty');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1002,1002,'Survivor Distribution','This dashboard allows for an analysis of vintage composition, helping to identify cost and/or quantity distortions.  Inaccurate vintage compositions can distort retirement data (for curve retired assets) as well as projected retirement data.  Large swings in cost per unit can pinpoint a cost or quantity distortion.  Non-unitized costs (total cost - unitized cost) should typically not exist more than one year after their in-service date, particularly with regards to mass assets, and their existence can indicate a unitization issue in particular accounts.','The Survivor Distribution dashboard visually displays the surviving asset balances (in-service assets) based on cost and quantity. The charts provided for this dashboard include both unitized (101 accounts) and non-unitized (106 accounts) balance information, allowing for unitized asset balances to be separately analyzed from total asset balances (unitized + non-unitized).  Additionally, the unitized cost per unit by vintage is displayed.  This dashboard is used to identify asset vintage distortions which can create problems when processing mass retirements.','Dashboard/SummaryDashboard/?DashboardId=1002','Y','Survivor Distribution Dashboard','SurvivorDistribution');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1003,1003,'Life Analysis by Cost','This dashboard is used to identify assets which are potential ‘ghosts’ within the system, meaning they have been physically retired but have not been recorded as retired on the books.  ‘Ghosts’ can be identified in the system by looking for assets that have fallen off their mortality curves (theoretically past their maximum life expectancy).  Assets with spikes in cost towards the tail-end of their expected lives indicate the same potential issue – assets displaying these conditions are potential missed retirements and should be dealt with appropriately.','The Life Analysis by Cost dashboard provides a cost-based breakdown of survivor (in-service) asset balances based on their relative position to their assigned expected lives.  Assets are assigned mortality curves and expected lives, and this dashboard illustrates situations in which assets are trending towards, are past and are significantly outliving their expected lives.  The groupings shown are assets Before Expected Life (BEL), Past Expected Life (PEL) and Past Max Life (PML). Assets that are past their expected lives (colored yellow) or maximum lives (colored red) are highlighted for analysis. ','Dashboard/SummaryDashboard/?DashboardId=1003','Y','Life Analysis by Cost Dashboard','LifeAnalysisCost');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1004,1004,'Life Analysis by Quantity','This dashboard is used to identify assets which are potential ‘ghosts’ within the system, meaning they have been physically retired but have not been recorded as retired on the books.  ‘Ghosts’ can be identified in the system by looking for assets that have fallen off their mortality curves (theoretically past their maximum life expectancy).  Assets with spikes in quantity towards the tail-end of their expected lives indicate the same potential issue – assets displaying these conditions are potential missed retirements and should be dealt with appropriately.','The Life Analysis by Quantity dashboard provides a quantity-based breakdown of survivor (in-service) asset balances based on their relative position to their assigned expected lives.  Assets are assigned mortality curves and expected lives, and this dashboard illustrates situations in which assets are trending towards, are past and are significantly outliving their expected lives.  The groupings shown are assets Before Expected Life (BEL), Past Expected Life (PEL) and Past Max Life (PML). Assets that are past their expected lives (colored yellow) or maximum lives (colored red) are highlighted for analysis.','Dashboard/SummaryDashboard/?DashboardId=1004','Y','Life Analysis by Quantity Dashboard','LifeAnalysisQty');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1005,1005,'Life Analysis by Escalated Cost','This dashboard is used to identify assets which are potential ‘ghosts’ within the system, meaning they have been physically retired but have not been recorded as retired on the books.  ‘Ghosts’ can be identified in the system by looking for assets that have fallen off their mortality curves (theoretically past their maximum life expectancy).  Assets with spikes in escalated cost towards the tail-end of their expected lives indicate the same potential issue – assets displaying these conditions are potential missed retirements and should be dealt with appropriately.','The Life Analysis by Escalated Cost dashboard provides a escalated cost-based breakdown of survivor (in-service) asset balances based on their relative position to their assigned expected lives.  Assets are assigned mortality curves and expected lives, and this dashboard illustrates situations in which assets are trending towards, are past and/or are significantly outliving their expected lives.  The groupings shown are assets Before Expected Life (BEL), Past Expected Life (PEL) and Past Max Life (PML). Assets that are past their expected lives (colored yellow) or maximum lives (colored red) are highlighted for analysis.','Dashboard/SummaryDashboard/?DashboardId=1005','Y','Life Analysis by Escalated Cost Dashboard','LifeAnalysisEsc');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1006,1006,'CPR Ledger Anomalies','Any assets with vintages in the future are anomalous records and should be adjusted.  Similarly, there are no conditions where quantity is negative and cost is positive.  While negative quantity and cost conditions can exist, as can positive quantities with negative costs, these conditions are atypical and should be investigated for accuracy.','The CPR Ledger Anomalies dashboard highlights unusual, irregular or error conditions in the CPR Ledger that necessitate investigation.  These issues include costs assigned to assets with zero quantities, quantities assigned to assets with zero costs, opposite signed cost and quantity records (cost is positive and quantity is negative, and vice versa) and assets with vintages in the future.','Dashboard/SummaryDashboard/?DashboardId=1006','Y','CPR Ledger Anomalies','LedgerAnomalies');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1007,1007,'Retirement Patterns by Cost','Large swings in annual retirement costs should be investigated, particularly in vintage accounts.  Significantly greater retirements in a given vintage in any particular year can indicate a vintage distortion that should be remediated; similarly, any retirement data that is net positive for a particular year/vintage is an anomalous condition that should be investigated (retirements costs are negative).  While unretirement activity occurs, significant unretired costs should be validated for accuracy.','The Retirement Patterns by Cost dashboard provides information about annual retirement costs.  The visuals in this dashboard provide a look at the pattern of retirement activity on a yearly basis and can be used to identify potential missed retirements within the system by comparing yearly trends in certain accounts.  The visuals illustrate annual retirement activity by cost, the distribution of retirement costs by vintage, as well as the gross $ amount of unretirement activity by vintage. ','Dashboard/SummaryDashboard/?DashboardId=1007','Y','Retirement Patterns by Cost Dashboard','RetirementPatternsCost');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1008,1008,'Retirement Patterns by Quantity','Large swings in annual retirement quantities should be investigated, particularly in vintage accounts.  Significantly greater retirements in a given vintage in any particular year can indicate a vintage distortion that should be remediated; similarly, any retirement data that is net positive for a particular year/vintage is an anomalous condition that should be investigated (retirements quantities are negative).  While unretirement activity occurs, significant unretired quantities should be validated for accuracy.','The Retirement Patterns by Quantity dashboard provides information about annual retirement quantity.  The visuals in this dashboard provide a look at the pattern of retirement activity on a yearly basis and can be used to identify potential missed retirements within the system by comparing yearly trends in certain accounts.  The visuals illustrate annual retirement activity by quantity, the distribution of retirement quantities by vintage, as well as the gross quantity amount of unretirement activity by vintage. ','Dashboard/SummaryDashboard/?DashboardId=1008','Y','Retirement Patterns by Quantity Dashboard','RetirementPatternsQty');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1009,1009,'Addition Patterns by Cost','Large swings in annual addition costs should be investigated, particularly in vintage accounts.  Significantly greater additions in a given vintage in any particular year can indicate a vintage distortion that should be remediated.  Any addition data that is net negative for a particular year/ vintage is an anomalous condition that should be investigated (addition costs are positive).','The Addition Patterns by Cost dashboard provides information about annual addition costs.  The visuals in this dashboard provide a look at the pattern of addition activity on a yearly basis and can be compared against retirement activity to identify trends.  The dashboard is used in conjunction with the Retirement Patterns dashboards to show time periods where you would expect to see increased retirement activity due to increased overall addition activity levels (and vice-versa).  The visuals illustrate annual addition activity by cost, the distribution of addition costs by vintage, as well additions with negative costs (anomalous conditions).  ','Dashboard/SummaryDashboard/?DashboardId=1009','Y','Addition Patterns by Cost Dashboard','AdditionPatternsCost');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1010,1010,'Addition Patterns by Quantity','Large swings in annual addition quantities should be investigated, particularly in vintage accounts.  Significantly greater additions in a given vintage in any particular year can indicate a vintage distortion that should be remediated.  Any addition data that is net negative for a particular year/ vintage is an anomalous condition that should be investigated (addition quantities are positive).','The Addition Patterns by Quantity dashboard provides information about annual addition quantities.  The visuals in this dashboard provide a look at the pattern of addition activity on a yearly basis and can be compared against retirement activity to identify trends.  The dashboard is used in conjunction with the Retirement Patterns dashboards to show time periods where you would expect to see increased retirement activity due to increased overall addition activity levels (and vice-versa).  The visuals illustrate annual addition activity by quantity, the distribution of addition quantities by vintage, as well additions with negative quantities (anomalous conditions). ','Dashboard/SummaryDashboard/?DashboardId=1010','Y','Addition Patterns by Quantity Dashboard','AdditionPatternsQty');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1011,1011,'Reserve Adequacy','If the actual reserve is small relative to the theoretical reserve, this indicates an under-reserved condition where sufficient funds are not being collected to replace the asset at the end of its expected life. If the actual reserve is large relative to the theoretical reserve, this indicates an over-reserved condition where more depreciation has been collected on assets than previously estimated (indicating missed retirements/ghost assets). When more depreciation has been collected on assets than previously estimated (actual reserves > theoretical reserves) it means that the average age of assets is increasing due to natural/technological factors or, more significantly, missed retirements. Significant over-reserved conditions can lead to ratepayer refunds or credits; as such, they should be investigated thoroughly. As a best practice, actual reserves should be trending toward their associated theoretical reserves.','The Reserve Adequacy dashboard highlights reserve imbalances between the Theoretical and Actual Life Reserves and is used to understand where you have under- or over-reserved situations that are significant enough to warrant additional scrutiny in a rate case (the Theoretical Life Reserve is calculated based on the remaining life calculations from the depreciation module, and is a useful tool for gauging retirement activity). Reserve imbalances are highlighted by plotting the annual trend of the Life Reserve against the Theoretical Life Reserve. Additionally, the magnitude of the difference between the Actual and Theoretical Life Reserves is shown as a percent value and the annual change in the Life Reserve is plotted as a line graph for trend analysis.  ','Dashboard/SummaryDashboard/?DashboardId=1011','Y','Reserve Adequacy Dashboard','ReserveAdequacyDashboard');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1012,1012,'Cost of Removal per Unit','Allocating the appropriate COR to retired assets has beneficial tax implications, and failing to do so can have negative regulatory implications.  Plotting COR $ relative to retirement and addition $ allows for a trend analysis of COR data, highlighting potentially misallocated costs in particular years and accounts.  No COR should ever be taken without an associated retirement.  While retirements can occur without COR, specific accounts should be investigated where COR rates are applied but no dollars are accumulated.','The Cost of Removal per Unit dashboard provides information about how cost of removal (COR) per unit is trending over time.  This dashboard is used to determine areas where COR and/or retirements are not being processed.  The dashboard compares the yearly COR to retirement and addition $.  Additionally, bar charts display potentially anomalous conditions in which COR has been taken without an associated retirement, or a retirement has been recorded without any associated COR.   ','Dashboard/SummaryDashboard/?DashboardId=1012','Y','Cost of Removal Per Unit','CostOfRemovalPerUnit');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1013,1013,'Average Unit Costs','Average unit costs should have a positive correlation with the inflation rates, and thus should share a general slope. If there is significant diversion, quantity and/or cost distortions may have occurred and appropriate accounts should be investigated.','The Average Unit Costs dashboard provides a comparison of the average asset cost per unit compared with the Handy Whitman inflation index (standardized rate of inflation). This dashboard is used to highlight areas where unusual activity has created an average asset cost per unit that does not align with the general rate of inflation. The dashboard trends average unit costs by GL posting year relative to the Handy Whitman rate. Hover over the Handy Whitman rates (orange line) to see the rate for a given year (it does not share the same Y-axis scale as the unit costs, which are in dollars). ','Dashboard/SummaryDashboard/?DashboardId=1013','Y','Average Unit Costs','AverageUnitCosts');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1014,1014,'Depreciation Ledger Trends','Additions and retirements should generally trend together, particularly in mass accounts, unless significant infrastructure expansions have occurred – significant divergences should be investigated for accuracy. Validate addition and retirement costs against ending balances. Plotting COR $ relative to retirement $ allows for a trend analysis of COR data, highlighting potentially misallocated costs in particular years and accounts.','The Depreciation Ledger Trends dashboard provides annual trend information for various components of the depreciation ledger:  yearly ending asset balance, additions, retirements and cost of removal (COR).  Additionally, COR $ are plotted against both addition and retirement $. ','Dashboard/SummaryDashboard/?DashboardId=1014','Y','Depr Ledger Trends','DeprLedgerTrends');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1015,1015,'COR Reserve Trends','Utilize this dashboard to investigate and validate fluctuations in the COR Reserve. Once an issue has been identified, validate the fluctuations (or oppositely identify the problem’s root cause) by investigating the annual COR expense added to the reserve and annual COR $ per Beginning COR Reserve.','The COR Reserve Trends dashboard highlights cost of removal (COR) trends from the depreciation ledger, particularly relating to the COR Reserve. Annual COR expense added the reserve, the annual change in the reserve balance and the ratio of COR/Begin COR Reserve are trended over GL posting years. ','Dashboard/SummaryDashboard/?DashboardId=1015','Y','COR Reserve Trends','CORReserveTrends');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1016,1016,'Life Reserve Trends','Utilize this dashboard to investigate and validate fluctuations in the Life Reserve.  Once an issue has been identified, validate the fluctuations (or oppositely identify the problem’s root cause) by investigating the annual depreciation added to the reserve and annual retirement $ per beginning Life Reserve.','The Life Reserve Trends dashboard highlights retirement trends from the depreciation ledger, particularly relating to the Life Reserve.  Annual asset depreciation added the reserve, the annual change in the reserve and the ratio of Retirements/Begin Life Reserve are trended over GL posting years. ','Dashboard/SummaryDashboard/?DashboardId=1016','Y','Life Reserve Trends','LifeReserveTrends');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1017,1017,'Average Age Trends','Original cost weighted age, escalated cost weighted age and quantity weighted age are all calculated to provide the fullest picture of asset age composition. Plotting these average ages, and the associated retirement and addition data, allows for analysis of age fluctuations that can occur from year to year. Large fluctuations in annual average ages can signify significant retirement and/or addition activity, which should be investigated for accuracy. Small changes in annual or periodic ages represent a more static condition where additions and/or retirements offset the natural year-by-year increase in the age of assets. ','The Age Trends dashboard highlights how asset ages are changing over time. Age trends are complemented with retirement and addition activity data to give insight into the reasons for the changing ages. Additionally, accounts whose ages have changed by more than two (2) years (both positive and negative) in a given year are highlighted as large annual changes; accounts whose ages have changed by more than one (1) year over a span of multiple years are highlighted as large period changes. ','Dashboard/SummaryDashboard/?DashboardId=1017','Y','Average Age Trends','AverageAgeTrends');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1018,1018,'Retirement Activity Anomalies','Conditions where there are costs with zero associated quantity and quantities with zero associated cost are potential anomalies in the data and should be investigated for accuracy.  While circumstances can occur where quantity exists with zero associated cost, as well as negative quantity and cost conditions, these occurrences are atypical and should be validated for accuracy.','The Retirement Activity Anomalies dashboard highlights anomalies in the CPR Activity table revolving around retirements. Conditions where there are non-zero quantities with zero costs and where there are non-zero costs with zero quantities are highlighted for investigation. This dashboard is used to identify potential errors within the CPR Activity table.','Dashboard/SummaryDashboard/?DashboardId=1018','Y','Retirement Activity Anomalies','RetirementAnomalies');
	INSERT INTO PWRPLANT.PA_DASHBOARD values (1019,1019,'Salvage per Unit','Allocating the appropriate salvage to retired assets has beneficial cash implications.  Plotting salvage $ relative to retirement quantity allows for a trend analysis of salvage data, highlighting potentially misallocated costs in particular years and accounts.  No salvage should ever be taken without an associated retirement.  While retirements can occur without salvage, specific accounts should be investigated where salvage rates are applied but no dollars are accumulated.','The Salvage per Unit dashboard provides information about how salvage per unit is trending over time.  This dashboard is used to determine areas where salvage and/or retirements are not being processed.  The dashboard compares the yearly salvage to retirement $, as well as plots salvage $ per unit of retirement.    ','Dashboard/SummaryDashboard/?DashboardId=1019','Y','Salvage per Unit','SalvagePerUnit');
	    

    delete from PWRPLANT.PA_DASHBOARD_CHARTS;
    

    -- Inserts for PWRPLANT.PA_DASHBOARD_CHARTS
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1,1,1000,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (2,1,1010,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (3,1,1036,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (4,1,1040,3);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (5,2,1006,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (6,2,1009,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (7,2,1018,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (8,2,1034,3);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (9,2,1039,4);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (10,3,1002,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (11,3,1024,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (12,4,1006,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (13,6,1000,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (14,5,1018,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (15,7,1034,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (16,8,1055,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (17,9,1061,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1000,1000,1000,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1001,1000,1001,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1002,1000,1039,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1003,1001,1003,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1004,1001,1004,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1005,1001,1067,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1006,1002,1006,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1007,1002,1007,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1008,1002,1008,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1009,1003,1009,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1010,1003,1010,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1011,1003,1011,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1012,1004,1012,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1013,1004,1013,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1014,1004,1014,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1015,1005,1015,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1016,1005,1016,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1017,1005,1017,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1018,1006,1018,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1019,1006,1019,3);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1020,1006,1020,4);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1021,1006,1021,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1022,1006,1022,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1023,1007,1023,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1024,1007,1063,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1025,1007,1025,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1026,1008,1023,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1027,1008,1063,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1028,1008,1027,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1029,1009,1028,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1030,1009,1062,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1031,1009,1030,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1032,1010,1028,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1033,1010,1062,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1034,1010,1032,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1035,1011,1034,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1036,1011,1068,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1037,1011,1036,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1038,1012,1037,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1039,1012,1038,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1041,1013,1033,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1044,1012,1070,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1045,1014,1043,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1046,1014,1045,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1047,1014,1044,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1048,1014,1046,3);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1049,1014,1047,4);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1050,1014,1048,5);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1051,1015,1049,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1052,1015,1050,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1053,1015,1051,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1054,1016,1052,2);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1055,1016,1053,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1056,1016,1054,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1057,1017,1055,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1060,1017,1058,3);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1061,1017,1059,4);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1062,1018,1060,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1063,1018,1061,1);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1064,1000,1064,3);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1065,1001,1066,3);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1066,1012,1069,3);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1067,1019,1071,0);
	INSERT INTO PWRPLANT.PA_DASHBOARD_CHARTS values (1068,1019,1072,1);
	   

    delete from PWRPLANT.PA_COLUMNGROUP;
    

    -- Inserts for PA_COLUMNGROUP
    INSERT INTO PWRPLANT.PA_COLUMNGROUP values (1,'CPR_BALANCES','BUS_SEGMENT,FUNC_CLASS,FERC_PLT_ACCT,UTIL_ACCT,DEPR_GROUP,STATE,MAJOR_LOCATION','FUNC_CLASS');
	INSERT INTO PWRPLANT.PA_COLUMNGROUP values (2,'VINTAGE_SPREADS','BUS_SEGMENT,FUNC_CLASS,FERC_PLT_ACCT,UTIL_ACCT,DEPR_GROUP,STATE,MAJOR_LOCATION','FUNC_CLASS');
	

    delete from PWRPLANT.PA_DATAPOINT;
    

    -- Inserts for PWRPLANT.PA_DATAPOINT
    INSERT INTO PWRPLANT.PA_DATAPOINT values (1000,'ferc_plt_acct','delta','FF0000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1001,'ferc_plt_acct','delta','FFCC33',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1002,'HeatMapLabel','scaled_diff','FF0000','vintage_year','process_year',1,'Under','Over','FF0000');
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1003,'VINTAGE_INTERVAL','UNITIZED_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1004,'VINTAGE_INTERVAL','UNITIZED_QTY','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1005,'VINTAGE_INTERVAL','TOTAL_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1006,'VINTAGE_INTERVAL','TOTAL_QTY','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1007,'VINTAGE_INTERVAL','COST_PER_UNIT','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1008,'AGE','BEL_Total_Cost','66CC66',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1009,'AGE','PEL_Total_Cost','FFCC33',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1010,'AGE','PML_Total_Cost','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1011,'YEARS_PAST_EXPECTED','TOTAL_COST','FFCC33',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1012,'YEARS_PAST_MAX','TOTAL_COST','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1013,'AGE','BEL_Total_Quantity','66CC66',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1014,'AGE','PEL_Total_Quantity','FFCC33',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1015,'AGE','PML_Total_Quantity','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1016,'YEARS_PAST_EXPECTED','TOTAL_QUANTITY','FFCC33',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1017,'YEARS_PAST_MAX','TOTAL_QUANTITY','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1018,'AGE','BEL_Total_Escalated_Cost','66CC66',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1019,'AGE','PEL_Total_Escalated_Cost','FFCC33',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1020,'AGE','PML_Total_Escalated_Cost','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1021,'YEARS_PAST_EXPECTED','TOTAL_ESCALATED_COST','FFCC33',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1022,'YEARS_PAST_MAX','TOTAL_ESCALATED_COST','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1023,'COMPANY','ZERO_QTY_PCT','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1024,'COMPANY','NONZERO_QTY_PCT','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1025,'COMPANY','RECORDCOUNT','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1026,'VINTAGE_YEAR','TOTAL_QUANTITY','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1027,'VINTAGE_YEAR','TOTAL_COST','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1028,'PROCESS_YEAR','RETIRE_COST','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1029,'PROCESS_YEAR','RETIRE_QTY','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1030,'HeatMapLabel','SCALEDVALUE','FF0000','VINTAGE_YEAR','PROCESS_YEAR',1,'Low','High','FF0000');
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1031,'VINTAGE_INTERVAL','RETIRE_COST','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1032,'VINTAGE_INTERVAL','RETIRE_QTY','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1033,'PROCESS_YEAR','ADDITION_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1034,'PROCESS_YEAR','ADDITION_QTY','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1035,'PROCESS_YEAR','SCALEDVALUE','66CC66','VINTAGE_YEAR','PROCESS_YEAR',1,'Low','High','FFFFFF');
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1036,'VINTAGE_INTERVAL','ADDITION_COST','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1037,'VINTAGE_INTERVAL','ADDITION_QTY','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1038,'VINTAGE_INTERVAL','AVG_UNIT_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1039,'VINTAGE_INTERVAL','AVG_HW_RATE','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1040,'GL_POSTING_YEAR','END_LIFE_RESERVE','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1041,'GL_POSTING_YEAR','END_THEO_RESERVE','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1042,'HeatMapLabel','END_RATIO','FF342D','GL_POSTING_YEAR','COMPANY',1,'Over Reserved','Under Reserved','FF342D');
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1043,'GL_POSTING_YEAR','ANNUAL_CHANGE','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1044,'PROCESS_YEAR','COR','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1045,'PROCESS_YEAR','RETIRE_QTY','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1046,'PROCESS_YEAR','ADDITION_QTY','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1047,'ferc_plt_acct','qty_delta','FF0000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1048,'ferc_plt_acct','qty_delta','FFCC33',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1049,'HeatMapLabel','qty_scaled_diff','FF0000','vintage_year','process_year',1,'Under','Over','FF0000');
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1050,'PROCESS_YEAR','RETIRE_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1051,'PROCESS_YEAR','PROJ_RETIRE_COST','FFCC33',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1052,'LIFE_BAND','CURR_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1053,'LIFE_BAND','CURR_QTY','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1054,'HeatMapLabel','COR_FLAG','FF0000','VINTAGE_YEAR','PROCESS_YEAR',1,'No COR','No Retirement','FF0000');
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1055,'GL_POSTING_YEAR','END_BALANCE','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1056,'GL_POSTING_YEAR','RETIREMENTS','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1057,'GL_POSTING_YEAR','ADDITIONS','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1058,'GL_POSTING_YEAR','COST_OF_REMOVAL','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1059,'GL_POSTING_YEAR','PCT_COR_BEGIN_RESERVE','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1060,'GL_POSTING_YEAR','ANNUAL_CHANGE_COR_RESERVE','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1061,'GL_POSTING_YEAR','PCT_RETIRE_BEGIN_RESERVE','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1062,'GL_POSTING_YEAR','ANNUAL_CHANGE_LIFE_RESERVE','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1063,'PROCESS_YEAR','COST_WEIGHTED_AGE','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1064,'PROCESS_YEAR','QUANTITY_WEIGHTED_AGE','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1065,'PROCESS_YEAR','ESCALATED_COST_WEIGHTED_AGE','66CC66',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1066,'PROCESS_YEAR','ADDITION_QTY','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1067,'PROCESS_YEAR','ADDITION_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1068,'PROCESS_YEAR','RETIRE_QTY','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1069,'PROCESS_YEAR','RETIRE_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1070,'GL_POSTING_YEAR','NET_RETIREMENT_QTY','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1071,'GL_POSTING_YEAR','NET_RETIREMENT_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1072,'VINTAGE_INTERVAL','ADDITION_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1073,'VINTAGE_INTERVAL','ADDITION_QTY','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1074,'VINTAGE_INTERVAL','RETIRE_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1075,'VINTAGE_INTERVAL','RETIRE_QTY','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1076,'VINTAGE_INTERVAL','RETIRE_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1077,'VINTAGE_INTERVAL','RETIRE_QTY','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1078,'PROCESS_YEAR','RETIRE_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1079,'PROCESS_YEAR','RETIRE_QTY','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1080,'VINTAGE_INTERVAL','PROJ_RETIRE_COST','FF9933',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1081,'VINTAGE_INTERVAL','PROJ_RETIRE_QTY','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1082,'PROCESS_YEAR','PROJ_RETIRE_COST','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1083,'PROCESS_YEAR','PROJ_RETIRE_QTY','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1084,'GL_POSTING_YEAR','PCT_OVER_UNDER_RESERVED','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1085,'VINTAGE_INTERVAL','COR','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1086,'PROCESS_YEAR','COR_PER_UNIT','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1087,'GL_POSTING_YEAR','COR_EXPENSE','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1088,'GL_POSTING_YEAR','DEPR_EXPENSE','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1089,'PROCESS_YEAR','SALVAGE','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1090,'PROCESS_YEAR','SALVAGE_PER_UNIT','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1091,'PROCESS_YEAR','RETIRE_QTY','006699',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1092,'COMPANY','ZERO_COST_PCT','990000',null,null,null,null,null,null);
	INSERT INTO PWRPLANT.PA_DATAPOINT values (1093,'COMPANY','NONZERO_COST_PCT','006699',null,null,null,null,null,null);
	    

    delete from PWRPLANT.PA_DATASOURCE;
    

    -- Inserts for PWRPLANT.PA_DATASOURCE
    INSERT INTO PWRPLANT.PA_DATASOURCE values (1000,'PR_UnderCost_Chart','ferc_plt_acct, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRPLANT.TEMP_PA_CPR_BALANCES','sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)) < 0','ferc_plt_acct','delta','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,10,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1001,'PR_OverCost_Chart','ferc_plt_acct, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRPLANT.TEMP_PA_CPR_BALANCES','sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)) > 0','ferc_plt_acct','delta desc','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,10,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1002,'PR_CostHeatMap_Chart','process_year, vintage_year, round(sum(nvl(retire_cost,0)),2) retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta, case when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) = 0 then 0 when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) < 0 then 100 when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) > 0 then -100 when (sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1) > 1 then 100 when (sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1) < -1 then -100 else round(((sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1))*100,0) end scaled_diff','PWRPLANT.TEMP_PA_CPR_BALANCES','proj_retire_cost is not null 
	','process_year, vintage_year','1, 2','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1003,'PR_UnderQty_Chart','ferc_plt_acct, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES','sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)) < 0','ferc_plt_acct','qty_delta','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,10,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1004,'PR_OverQty_Chart','ferc_plt_acct, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES','sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)) > 0','ferc_plt_acct','qty_delta desc','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,10,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1006,'PR_UnderCost_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1007,'PR_UnderCost_Locn','state, major_location,  round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'state, major_location','state, major_location','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1008,'PR_UnderCost_Comp','company,  round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'company','company','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1009,'PR_OverCost_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1010,'PR_OverCost_Locn','state, major_location,  round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'state, major_location','state, major_location','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1011,'PR_OverCost_Comp','company, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'company','company','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1015,'PR_UnderQty_Acct','bus_segment, func_class, ferc_plt_acct, util_acct,round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1016,'PR_UnderQty_Locn','state, major_location, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'state, major_location','state, major_location','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1017,'PR_UnderQty_Comp','company, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'company','company','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1018,'PR_OverQty_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1019,'PR_OverQty_Locn','state, major_location, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'state, major_location','state, major_location','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1020,'PR_OverQty_Comp','company, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'company','company','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1025,'SD_UnitzCombo_Chart','VINTAGE_INTERVAL, ROUND(SUM(UNTZ_COST),2) UNITIZED_COST, ROUND(SUM(UNTZ_QTY),2) UNITIZED_QTY, CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END COST_PER_UNIT','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'VINTAGE_INTERVAL','1','(CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1026,'SD_TotalCombo_Chart','VINTAGE_INTERVAL, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END COST_PER_UNIT','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'VINTAGE_INTERVAL','1','(CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1027,'SD_UnitzCombo_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, ROUND(SUM(UNTZ_COST),2) UNITIZED_COST, ROUND(SUM(UNTZ_QTY),2) UNITIZED_QTY, CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END COST_PER_UNIT','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','(CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1028,'SD_UnitzCombo_Locn','state, major_location, ROUND(SUM(UNTZ_COST),2) UNITIZED_COST, ROUND(SUM(UNTZ_QTY),2) UNITIZED_QTY, CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END COST_PER_UNIT','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'state, major_location','state, major_location','(CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1029,'SD_UnitzCombo_Comp','company, ROUND(SUM(UNTZ_COST),2) UNITIZED_COST, ROUND(SUM(UNTZ_QTY),2) UNITIZED_QTY, CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END COST_PER_UNIT','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'company','company','(CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1030,'SD_TotalCombo_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END COST_PER_UNIT','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','(CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1031,'SD_TotalCombo_Locn','state, major_location, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END COST_PER_UNIT','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'state, major_location','state, major_location','(CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1032,'SD_TotalCombo_Comp','company, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END COST_PER_UNIT','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'company','company','(CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1033,'SD_Unitz_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR,
	ROUND(SUM(UNTZ_COST),2) UNITIZED_COST,
	ROUND(SUM(UNTZ_QTY),2) UNITIZED_QTY,
	CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END UNITIZED_COST_PER_UNIT,
	ROUND(SUM(CURR_COST),2) TOTAL_COST,
	ROUND(SUM(CURR_QTY),2) TOTAL_QTY, 
	CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END TOTAL_COST_PER_UNIT,
	AGE, AGE_GROUP, EXPECTED_LIFE, MAX_LIFE, MORT_CURVE_DESC','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, AGE_GROUP, EXPECTED_LIFE, MAX_LIFE, MORT_CURVE_DESC','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, AGE_GROUP, EXPECTED_LIFE, MAX_LIFE, MORT_CURVE_DESC','(CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1034,'SD_Total_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR,
	ROUND(SUM(CURR_COST),2) TOTAL_COST,
	ROUND(SUM(CURR_QTY),2) TOTAL_QTY,
	CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END TOTAL_COST_PER_UNIT,
	ROUND(SUM(UNTZ_COST),2) UNITIZED_COST,
	ROUND(SUM(UNTZ_QTY),2) UNITIZED_QTY,
	CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END UNITIZED_COST_PER_UNIT,
	AGE, AGE_GROUP, EXPECTED_LIFE, MAX_LIFE, MORT_CURVE_DESC','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, AGE_GROUP, EXPECTED_LIFE, MAX_LIFE, MORT_CURVE_DESC','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, AGE_GROUP, EXPECTED_LIFE, MAX_LIFE, MORT_CURVE_DESC','(CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1035,'LA_AssetAgeDist_Chart','AGE, 
	AGE_GROUP,
	ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'AGE, AGE_GROUP','1','RETIRE_UNIT_ID > 5 AND AGE IS NOT NULL AND EXPECTED_LIFE IS NOT NULL',null,null,'AgeGroupDataTableLoader',2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1036,'LA_YearsPastExpected_Chart','YEARS_PAST_EXPECTED, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'YEARS_PAST_EXPECTED','1','RETIRE_UNIT_ID > 5 and YEARS_PAST_EXPECTED >= 0 AND YEARS_PAST_MAX <=0 
	',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1037,'LA_YearsPastMax_Chart','YEARS_PAST_MAX, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'YEARS_PAST_MAX','1','RETIRE_UNIT_ID > 5 and YEARS_PAST_MAX >=0',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1038,'LA_AssetAgeDist_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, 

	ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','RETIRE_UNIT_ID > 5 AND AGE IS NOT NULL AND EXPECTED_LIFE IS NOT NULL',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1039,'LA_AssetAgeDist_Locn','state, major_location, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'state, major_location','state, major_location','RETIRE_UNIT_ID > 5 AND AGE IS NOT NULL AND EXPECTED_LIFE IS NOT NULL',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1040,'LA_AssetAgeDist_Comp','company,

	 ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'company','company','RETIRE_UNIT_ID > 5 AND AGE IS NOT NULL AND EXPECTED_LIFE IS NOT NULL',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1041,'LA_YearsPastExp_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','RETIRE_UNIT_ID > 5 and YEARS_PAST_EXPECTED >= 0 AND YEARS_PAST_MAX <=0 
	',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1042,'LA_YearsPastExp_Locn','state, major_location, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'state, major_location','state, major_location','RETIRE_UNIT_ID > 5 and YEARS_PAST_EXPECTED >= 0 AND YEARS_PAST_MAX <=0 
	',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1043,'LA_YearsPastExp_Comp','company, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'company','company','RETIRE_UNIT_ID > 5 and YEARS_PAST_EXPECTED >= 0 AND YEARS_PAST_MAX <=0 ',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1044,'LA_YearsPastMax_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','RETIRE_UNIT_ID > 5 and YEARS_PAST_MAX >=0',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1045,'LA_YearsPastMax_Locn','state, major_location, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'state, major_location','state, major_location','RETIRE_UNIT_ID > 5 and YEARS_PAST_MAX >=0',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1046,'LA_YearsPastMax_Comp','company, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'company','company','RETIRE_UNIT_ID > 5 and YEARS_PAST_MAX >=0',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1047,'LA_Age_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
	AGE_GROUP, EXPECTED_LIFE, YEARS_PAST_EXPECTED, MAX_LIFE, YEARS_PAST_MAX, MORT_CURVE_DESC, 
	ROUND(SUM(CURR_COST),2) TOTAL_COST, 
	ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, 
	ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
	AGE_GROUP, EXPECTED_LIFE, YEARS_PAST_EXPECTED, MAX_LIFE, YEARS_PAST_MAX, MORT_CURVE_DESC','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
	AGE_GROUP, EXPECTED_LIFE, YEARS_PAST_EXPECTED, MAX_LIFE, YEARS_PAST_MAX, MORT_CURVE_DESC','RETIRE_UNIT_ID > 5 AND AGE IS NOT NULL AND EXPECTED_LIFE IS NOT NULL',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1048,'LE_ZeroQtyCosts_Chart','COMPANY, sum(curr_cost) TOTAL_COSTS, nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end),0) ZERO_QTY_COSTS, round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4)*100 ZERO_QTY_PCT, (1 - round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4))*100 NONZERO_QTY_PCT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'COMPANY','1','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1049,'LE_NegQtyPosCost_Chart','COMPANY, SUM(case when curr_qty < 0 AND curr_cost > 0 then 1 else 0 end) as RECORDCOUNT','PWRPLANT.TEMP_PA_CPR_LEDGER','SUM(case when curr_qty < 0 AND curr_cost > 0 then 1 else 0 end) <> 0','COMPANY','RECORDCOUNT DESC','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1050,'LE_FutureAssets_Chart','VINTAGE_YEAR, COUNT(*) RECORD_COUNT, SUM(CURR_QTY) TOTAL_QUANTITY, SUM(CURR_COST) TOTAL_COST','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'VINTAGE_YEAR','1','RETIRE_UNIT_ID > 5 and VINTAGE_YEAR > EXTRACT(YEAR FROM SYSDATE) ',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1051,'LE_PosQtyNegCost_Chart','COMPANY, SUM(case when curr_qty > 0 AND curr_cost < 0 then 1 else 0 end) as RECORDCOUNT','PWRPLANT.TEMP_PA_CPR_LEDGER','SUM(case when curr_qty > 0 AND curr_cost < 0 then 1 else 0 end) <> 0','COMPANY','RECORDCOUNT DESC','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1052,'LE_ZeroCostQty_Chart','COMPANY, sum(curr_qty) TOTAL_QUANTITY, nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end),0) ZERO_COST_QTY, round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4)*100 ZERO_COST_PCT, (1 - round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4))*100 NONZERO_COST_PCT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'COMPANY','1','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1053,'LE_ZeroQtyCosts_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, sum(curr_cost) TOTAL_COST, nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end),0) ZERO_QTY_COST, round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4) ZERO_QTY_PCT, (1-round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4)) NONZERO_QTY_PCT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1054,'LE_ZeroQtyCosts_Locn','state, major_location, sum(curr_cost) TOTAL_COST, nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end),0) ZERO_QTY_COST, round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4) ZERO_QTY_PCT, (1-round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4)) NONZERO_QTY_PCT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'state, major_location','state, major_location','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1055,'LE_ZeroQtyCosts_Comp','company, sum(curr_cost) TOTAL_COST, nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end),0) ZERO_QTY_COST, round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4) ZERO_QTY_PCT, (1-round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4)) NONZERO_QTY_PCT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'company','company','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1056,'LE_ZeroQtyCosts_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR,
	sum(curr_cost) TOTAL_COST, 
	nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end),0) ZERO_QTY_COST, 
	round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4) ZERO_QTY_PCT, 
	(1-round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),4)) NONZERO_QTY_PCT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1057,'LE_NegQtyPosCost_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, sum(curr_cost) TOTAL_COST, sum(curr_qty) TOTAL_QUANTITY, COUNT(*) AS RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','RETIRE_UNIT_ID > 5 and CURR_QTY < 0 AND CURR_COST > 0',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1058,'LE_NegQtyPosCost_Locn','state, major_location, sum(curr_cost) TOTAL_COST, sum(curr_qty) TOTAL_QUANTITY, COUNT(*) AS RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'state, major_location','state, major_location','RETIRE_UNIT_ID > 5 and CURR_QTY < 0 AND CURR_COST > 0',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1059,'LE_NegQtyPosCost_Comp','company, sum(curr_cost) TOTAL_COST, sum(curr_qty) TOTAL_QUANTITY, COUNT(*) AS RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'company','company','RETIRE_UNIT_ID > 5 and CURR_QTY < 0 AND CURR_COST > 0',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1060,'LE_NegQtyPosCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, 
	sum(curr_cost) total_cost, 
	sum(curr_qty) total_qty, 
	sum(curr_adj) total_escalated_cost, 
	COUNT(*) AS RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT,  STATE, MAJOR_LOCATION, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 and CURR_QTY < 0 AND CURR_COST > 0',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1061,'LE_PosQtyNegCost_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, sum(curr_cost) TOTAL_COST, sum(curr_qty) TOTAL_QUANTITY, COUNT(*) AS RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','RETIRE_UNIT_ID > 5 and CURR_QTY > 0 AND CURR_COST < 0',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1062,'LE_PosQtyNegCost_Locn','state, major_location, sum(curr_cost) TOTAL_COST, sum(curr_qty) TOTAL_QUANTITY, COUNT(*) AS RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'state, major_location','state, major_location','RETIRE_UNIT_ID > 5 and CURR_QTY > 0 AND CURR_COST < 0',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1063,'LE_PosQtyNegCost_Comp','company, sum(curr_cost) TOTAL_COST, sum(curr_qty) TOTAL_QUANTITY, COUNT(*) AS RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'company','company','RETIRE_UNIT_ID > 5 and CURR_QTY > 0 AND CURR_COST < 0',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1064,'LE_PosQtyNegCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, 
	sum(curr_cost) total_cost, 
	sum(curr_qty) total_qty, 
	sum(curr_adj) total_escalated_cost, 
	COUNT(*) AS RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 and CURR_QTY > 0 AND CURR_COST < 0',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1065,'LE_FutureAssets_Comp','COMPANY, SUM(CURR_COST) TOTAL_COST, SUM(CURR_QTY) TOTAL_QUANTITY, COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 and VINTAGE_YEAR > EXTRACT(YEAR FROM SYSDATE) ',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1066,'LE_FutureAssets_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, CURR_AGE,
	SUM(CURR_COST) TOTAL_COST, 
	SUM(CURR_QTY) TOTAL_QUANTITY, 
	COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, CURR_AGE','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, CURR_AGE','RETIRE_UNIT_ID > 5 and VINTAGE_YEAR > EXTRACT(YEAR FROM SYSDATE) ',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1067,'LE_ZeroCostQty_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, sum(curr_qty) TOTAL_QUANTITY, nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end),0) ZERO_COST_QTY, round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4) ZERO_COST_PCT, (1-round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4)) NONZERO_COST_PCT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1068,'LE_ZeroCostQty_Locn','state, major_location, sum(curr_qty) TOTAL_QUANTITY, nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end),0) ZERO_COST_QTY, round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4) ZERO_COST_PCT, (1-round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4)) NONZERO_COST_PCT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'state, major_location','state, major_location','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1069,'LE_ZeroCostQty_Comp','company, sum(curr_qty) TOTAL_QUANTITY, nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end),0) ZERO_COST_QTY, round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4) ZERO_COST_PCT, (1-round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4)) NONZERO_COST_PCT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'company','company','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1070,'LE_ZeroCostQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR,
	sum(curr_qty) TOTAL_QTY, 
	nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end),0) ZERO_COST_QTY, 
	round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4) ZERO_COST_PCT, 
	(1-round(nvl(sum(case when curr_cost = 0 and curr_qty <> 0 then curr_qty end) / sum (curr_qty),0),4)) NONZERO_COST_PCT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1071,'RP_AnnualRetirements_Chart','PROCESS_YEAR, (SUM(RETIRE_COST)*-1) RETIRE_COST, (SUM(RETIRE_QTY)*-1) RETIRE_QTY','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'PROCESS_YEAR','1','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1072,'RP_AnnualRetire_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY,  CASE WHEN SUM(RETIRE_QTY) = 0 THEN 0 ELSE ROUND(SUM(RETIRE_COST)/SUM(RETIRE_QTY),2) END COST_PER_UNIT_RETIRED','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1073,'RP_AnnualRetire_Locn','STATE, MAJOR_LOCATION, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY,  CASE WHEN SUM(RETIRE_QTY) = 0 THEN 0 ELSE ROUND(SUM(RETIRE_COST)/SUM(RETIRE_QTY),2) END COST_PER_UNIT_RETIRED','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1074,'RP_AnnualRetire_Comp','COMPANY, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY,  CASE WHEN SUM(RETIRE_QTY) = 0 THEN 0 ELSE ROUND(SUM(RETIRE_COST)/SUM(RETIRE_QTY),2) END COST_PER_UNIT_RETIRED','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1075,'RP_AnnualRetire_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	SUM(RETIRE_COST) RETIRE_COST,
	SUM(RETIRE_QTY) RETIRE_QTY,
	CASE WHEN SUM(RETIRE_QTY) = 0 THEN 0 ELSE ROUND(SUM(RETIRE_COST)/SUM(RETIRE_QTY),2) END COST_PER_UNIT_RETIRED, 
	SUM(ANNUAL_BEGIN_COST) ANNUAL_BEGIN_COST,
	SUM(ANNUAL_BEGIN_QTY) ANNUAL_BEGIN_QTY','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1081,'RP_UnRetireActivity_Chart','VINTAGE_INTERVAL, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1','RETIRE_UNIT_ID > 5 and RETIRE_COST > 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1082,'RP_UnRetireActivity_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, COMPANY, SUM(RETIRE_COST) POSITIVE_RETIRE_COST, SUM(RETIRE_QTY) ASSOCIATED_RETIRE_QTY, COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 and RETIRE_COST > 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1083,'RP_UnRetireActivity_Locn','STATE, MAJOR_LOCATION, COMPANY, SUM(RETIRE_COST) POSITIVE_RETIRE_COST, SUM(RETIRE_QTY) ASSOCIATED_RETIRE_QTY, COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 and RETIRE_COST > 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1084,'RP_UnRetireActivity_Comp','COMPANY, SUM(RETIRE_COST) POSITIVE_RETIRE_COST, SUM(RETIRE_QTY) ASSOCIATED_RETIRE_QTY, COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 and RETIRE_COST > 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1085,'RP_UnRetireActivity_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	SUM(RETIRE_COST) POSITIVE_RETIRE_COST, SUM(RETIRE_QTY) ASSOCIATED_RETIRE_QTY, COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 and RETIRE_COST > 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1091,'RP_PositiveRetsQty_Chart','VINTAGE_INTERVAL, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1','RETIRE_UNIT_ID > 5 and RETIRE_QTY > 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1092,'RP_PositiveRetsQty_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(RETIRE_QTY) POSITIVE_RETIRE_QTY, SUM(RETIRE_COST) ASSOCIATED_RETIRE_COST,  COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 and RETIRE_QTY > 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1093,'RP_PositiveRetsQty_Locn','STATE, MAJOR_LOCATION, SUM(RETIRE_QTY) POSITIVE_RETIRE_QTY, SUM(RETIRE_COST) ASSOCIATED_RETIRE_COST,  COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 and RETIRE_QTY > 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1094,'RP_PositiveRetsQty_Comp','COMPANY, SUM(RETIRE_QTY) POSITIVE_RETIRE_QTY, SUM(RETIRE_COST) ASSOCIATED_RETIRE_COST,  COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 and RETIRE_QTY > 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1095,'RP_PositiveRetsQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	SUM(RETIRE_QTY) POSITIVE_RETIRE_QTY, SUM(RETIRE_COST) ASSOCIATED_RETIRE_COST,  COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 and RETIRE_QTY > 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1096,'AP_AnnualAdds_Chart','PROCESS_YEAR, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'PROCESS_YEAR','1','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1097,'AP_AnnualAdds_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, CASE WHEN SUM(ADDITION_QTY) = 0 THEN 0 ELSE ROUND(SUM(ADDITION_COST)/SUM(ADDITION_QTY),2) END COST_PER_UNIT_ADDED','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1098,'AP_AnnualAdds_Locn','STATE, MAJOR_LOCATION, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, CASE WHEN SUM(ADDITION_QTY) = 0 THEN 0 ELSE ROUND(SUM(ADDITION_COST)/SUM(ADDITION_QTY),2) END COST_PER_UNIT_ADDED','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1099,'AP_AnnualAdds_Comp','COMPANY, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, CASE WHEN SUM(ADDITION_QTY) = 0 THEN 0 ELSE ROUND(SUM(ADDITION_COST)/SUM(ADDITION_QTY),2) END COST_PER_UNIT_ADDED','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1100,'AP_AnnualAdds_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	SUM(ADDITION_COST) ADDITION_COST,
	SUM(ADDITION_QTY) ADDITION_QTY, 
	CASE WHEN SUM(ADDITION_QTY) = 0 THEN 0 ELSE ROUND(SUM(ADDITION_COST)/SUM(ADDITION_QTY),2) END COST_PER_UNIT_ADDED,
	SUM(ANNUAL_BEGIN_COST) ANNUAL_BEGIN_COST,
	SUM(ANNUAL_BEGIN_QTY) ANNUAL_BEGIN_QTY','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1106,'AP_NegativeAddsCost_Chart','VINTAGE_INTERVAL, SUM(ADDITION_COST*-1) ADDITION_COST, SUM(ADDITION_QTY*-1) ADDITION_QTY','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1','RETIRE_UNIT_ID > 5 and ADDITION_COST < 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1107,'AP_NegativeAddsCost_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(ADDITION_COST) NEGATIVE_ADDITION_COST, SUM(ADDITION_QTY) ASSOCIATED_ADDITION_QTY,  COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 and ADDITION_COST < 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1108,'AP_NegativeAddsCost_Locn','STATE, MAJOR_LOCATION, SUM(ADDITION_COST) NEGATIVE_ADDITION_COST, SUM(ADDITION_QTY) ASSOCIATED_ADDITION_QTY,  COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 and ADDITION_COST < 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1109,'AP_NegativeAddsCost_Comp','COMPANY, SUM(ADDITION_COST) NEGATIVE_ADDITION_COST, SUM(ADDITION_QTY) ASSOCIATED_ADDITION_QTY,  COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 and ADDITION_COST < 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1110,'AP_NegativeAddsCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	SUM(ADDITION_COST) NEGATIVE_ADDITION_COST, SUM(ADDITION_QTY) ASSOCIATED_ADDITION_QTY,  COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 and ADDITION_COST < 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1116,'AP_NegativeAddsQty_Chart','VINTAGE_INTERVAL, SUM(ADDITION_COST*-1) ADDITION_COST, SUM(ADDITION_QTY*-1) ADDITION_QTY','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1','RETIRE_UNIT_ID > 5 and ADDITION_QTY < 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1117,'AP_NegativeAddsQty_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(ADDITION_QTY) NEGATIVE_ADDITION_QTY, SUM(ADDITION_COST) ASSOCIATED_ADDITION_COST,  COUNT(*) RECORD_COUNT
	','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 and ADDITION_QTY < 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1118,'AP_NegativeAddsQty_Locn','STATE, MAJOR_LOCATION, SUM(ADDITION_QTY) NEGATIVE_ADDITION_QTY, SUM(ADDITION_COST) ASSOCIATED_ADDITION_COST,  COUNT(*) RECORD_COUNT
	','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 and ADDITION_QTY < 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1119,'AP_NegativeAddsQty_Comp','COMPANY, SUM(ADDITION_QTY) NEGATIVE_ADDITION_QTY, SUM(ADDITION_COST) ASSOCIATED_ADDITION_COST,  COUNT(*) RECORD_COUNT
	','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 and ADDITION_QTY < 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1120,'AP_NegativeAddsQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	SUM(ADDITION_QTY) NEGATIVE_ADDITION_QTY, SUM(ADDITION_COST) ASSOCIATED_ADDITION_COST,  COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 and ADDITION_QTY < 0',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1122,'RA_EndReserveTrend_Chart','GL_POSTING_YEAR, ROUND(SUM(END_RESERVE),2) END_LIFE_RESERVE, ROUND(SUM(END_THEO_RESERVE),2) END_THEO_RESERVE, ROUND(SUM(END_THEO_RESERVE),2) - ROUND(SUM(END_RESERVE),2) RESERVE_DELTA','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1124,'RA_AnnualChange_Chart','GL_POSTING_YEAR, SUM(BEGIN_RESERVE) BEGIN_LIFE_RESERVE, SUM(END_RESERVE) END_LIFE_RESERVE, SUM(END_RESERVE) - SUM(BEGIN_RESERVE) ANNUAL_CHANGE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1125,'RA_EndReserveTrend_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(END_RESERVE),2) END_LIFE_RESERVE, ROUND(SUM(END_THEO_RESERVE),2) END_THEO_RESERVE, ROUND(SUM(END_RESERVE),2) - ROUND(SUM(END_THEO_RESERVE),2) RESERVE_DELTA','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1127,'RA_EndReserveTrend_Comp','COMPANY, ROUND(SUM(END_RESERVE),2) END_LIFE_RESERVE, ROUND(SUM(END_THEO_RESERVE),2) END_THEO_RESERVE, ROUND(SUM(END_RESERVE),2) - ROUND(SUM(END_THEO_RESERVE),2) RESERVE_DELTA','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1128,'RA_EndReserveTrend_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, 
	ROUND(SUM(END_RESERVE),2) END_LIFE_RESERVE, 
	ROUND(SUM(END_THEO_RESERVE),2) END_THEO_RESERVE, 
	ROUND(SUM(END_RESERVE),2) - ROUND(SUM(END_THEO_RESERVE),2) RESERVE_DELTA, 
	case when sum(end_theo_reserve) = 0 then null else ROUND(((SUM(END_RESERVE) - SUM(END_THEO_RESERVE))/SUM(END_THEO_RESERVE)),4) end PCT_OVER_UNDER_RESERVED,
	ROUND(SUM(BEGIN_RESERVE),2) BEGIN_LIFE_RESERVE,
	ROUND(SUM(BEGIN_THEO_RESERVE),2) BEGIN_THEO_RESERVE, 
	ROUND(SUM(BEGIN_BALANCE),2) BEGIN_BALANCE, 
	ROUND(SUM(DEPRECIATION_EXPENSE),2) DEPR_EXPENSE, 
	ROUND(SUM(RETIREMENTS),2) RETIREMENTS, 
	ROUND(SUM(END_BALANCE),2) END_BALANCE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1133,'RA_AnnualChange_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(BEGIN_RESERVE) BEGIN_LIFE_RESERVE, SUM(END_RESERVE) END_LIFE_RESERVE, SUM(END_RESERVE) - SUM(BEGIN_RESERVE) ANNUAL_CHANGE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1135,'RA_AnnualChange_Comp','COMPANY, SUM(BEGIN_RESERVE) BEGIN_LIFE_RESERVE, SUM(END_RESERVE) END_LIFE_RESERVE, SUM(END_RESERVE) - SUM(BEGIN_RESERVE) ANNUAL_CHANGE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1136,'RA_AnnualChange_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, 
	SUM(BEGIN_RESERVE) BEGIN_LIFE_RESERVE, 
	SUM(END_RESERVE) END_LIFE_RESERVE, 
	SUM(END_RESERVE) - SUM(BEGIN_RESERVE) ANNUAL_CHANGE, 
	ROUND(SUM(DEPRECIATION_EXPENSE),2) DEPR_EXPENSE, 
	ROUND(SUM(RETIREMENTS),2) RETIREMENTS','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1137,'CR_RemovalPerRetirement_Chart','process_year, sum(cost_of_removal*-1) cor, sum(retire_qty*-1) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(cost_of_removal)/sum(retire_qty) end,2) cor_per_unit','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'process_year','1','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1138,'CR_RemovalPerRetirement_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, sum(cost_of_removal) cor, sum(retire_qty) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(cost_of_removal)/sum(retire_qty) end,2) cor_per_unit_retired','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1139,'CR_RemovalPerRetirement_Locn','STATE, MAJOR_LOCATION, sum(cost_of_removal) cor, sum(retire_qty) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(cost_of_removal)/sum(retire_qty) end,2) cor_per_unit_retired','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1140,'CR_RemovalPerRetirement_Comp','COMPANY, sum(cost_of_removal) cor, sum(retire_qty) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(cost_of_removal)/sum(retire_qty) end,2) cor_per_unit_retired','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1141,'CR_RemovalPerRetirement_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, 
	sum(cost_of_removal) cor, 
	sum(retire_qty) retire_qty, 
	round(case when sum(retire_qty) = 0 then null else sum(cost_of_removal)/sum(retire_qty) end,2) cor_per_unit_retired,
	sum(retire_cost) retire_cost, 
	CASE WHEN SUM(NVL(RETIRE_COST,0)) = 0 THEN NULL ELSE ROUND((SUM(COST_OF_REMOVAL)/SUM(RETIRE_COST)),4) END PCT_COR_RETIRE_COST','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1142,'CR_RemovalPerAddition_Chart','process_year, sum(cost_of_removal*-1) cor, sum(addition_qty) addition_qty, round(case when sum(addition_qty) = 0 then null else sum(cost_of_removal*-1)/sum(addition_qty) end,2) cor_per_unit, sum(addition_cost) addition_cost, case when sum(addition_cost) = 0 then null else round(sum(cost_of_removal*-1)/sum(addition_cost) * 100,2) end pct_cor_addition_cost','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'process_year','1','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1143,'CR_RemovalPerAddition_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, sum(cost_of_removal) cor, sum(addition_qty) addition_qty, round(case when sum(addition_qty) = 0 then null else sum(cost_of_removal)/sum(addition_qty) end,2) cor_per_unit_added','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1144,'CR_RemovalPerAddition_Locn','STATE, MAJOR_LOCATION, sum(cost_of_removal) cor, sum(addition_qty) addition_qty, round(case when sum(addition_qty) = 0 then null else sum(cost_of_removal)/sum(addition_qty) end,2) cor_per_unit_added','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1145,'CR_RemovalPerAddition_Comp','COMPANY, sum(cost_of_removal) cor, sum(addition_qty) addition_qty, round(case when sum(addition_qty) = 0 then null else sum(cost_of_removal)/sum(addition_qty) end,2) cor_per_unit_added','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1146,'CR_RemovalPerAddition_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, 
	sum(cost_of_removal) cor, 
	sum(addition_qty) addition_qty, 
	round(case when sum(addition_qty) = 0 then null else sum(cost_of_removal)/sum(addition_qty) end,2) cor_per_unit_added,
	sum(ADDITION_cost) ADDITION_cost, 
	CASE WHEN SUM(NVL(RETIRE_COST,0)) = 0 THEN NULL ELSE ROUND((SUM(COST_OF_REMOVAL)/SUM(RETIRE_COST)),4) END PCT_COR_ADDITION_COST','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1147,'PR_AnnualCost_Chart','process_year, round(sum(nvl(retire_cost*-1,0)),2) retire_cost, round(sum(proj_retire_cost),2) as proj_retire_cost, round(sum(nvl(proj_retire_cost,0)),2) - sum(retire_cost*-1) delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'process_year','1','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1148,'PR_AnnualCost_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, round(sum(nvl((retire_cost),0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl((retire_cost),0)),2) cost_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1149,'PR_AnnualCost_Locn','STATE, MAJOR_LOCATION, round(sum(nvl((retire_cost),0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl((retire_cost),0)),2) cost_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1150,'PR_AnnualCost_Comp','COMPANY, round(sum(nvl((retire_cost),0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl((retire_cost),0)),2) cost_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1151,'UC_AvgServiceCost_Chart','VINTAGE_INTERVAL, SUM(CURR_COST) TOTAL_COST, SUM(CURR_QTY) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END AS AVG_UNIT_COST, ROUND(NVL(AVG(HWRATE),0),2) AS AVG_HW_RATE
	','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'VINTAGE_INTERVAL','1','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1152,'UC_AvgServiceCost_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(CURR_COST) TOTAL_COST, SUM(CURR_QTY) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END AS AVG_UNIT_COST','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1153,'UC_AvgServiceCost_Locn','STATE, MAJOR_LOCATION, SUM(CURR_COST) TOTAL_COST, SUM(CURR_QTY) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END AS AVG_UNIT_COST','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1154,'UC_AvgServiceCost_Comp','COMPANY, SUM(CURR_COST) TOTAL_COST, SUM(CURR_QTY) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END AS AVG_UNIT_COST','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1155,'UC_AvgServiceCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, 
	SUM(CURR_COST) TOTAL_COST, 
	SUM(CURR_QTY) TOTAL_QTY, 
	CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END AS AVG_UNIT_COST,
	ROUND(NVL(AVG(HWRATE),0),2) AS AVG_HW_RATE','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL)',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1166,'DL_AnnualDeprLedger_Chart','gl_posting_year, sum(end_balance) end_balance, sum(additions) additions, sum(retirements*-1) retirements, sum(cost_of_removal*-1) cost_of_removal','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'gl_posting_year','1','(BEGIN_BALANCE IS NOT NULL OR END_BALANCE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1167,'DL_AnnualDeprLedger_Comp','company, sum(end_balance) end_balance, sum(additions) additions, sum(retirements) retirements, sum(cost_of_removal) cost_of_removal','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'company','company','(BEGIN_BALANCE IS NOT NULL OR END_BALANCE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1168,'DL_AnnualDeprLedger_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, sum(end_balance) end_balance, sum(additions) additions, sum(retirements) retirements, sum(cost_of_removal) cost_of_removal','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(BEGIN_BALANCE IS NOT NULL OR END_BALANCE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1169,'DL_AnnualDeprLedger_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, 
	sum(begin_balance) begin_balance, 
	sum(additions) additions, 
	sum(retirements) retirements, 
	sum(cost_of_removal) cost_of_removal, 
	sum(salvage_cash) salvage, 
	sum(transfers) transfers, 
	sum(adjustments) adjustments, 
	sum(depreciation_expense) depr_expense, 
	sum(end_balance) end_balance','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','(BEGIN_BALANCE IS NOT NULL OR END_BALANCE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1170,'DL_CorBeginReserve_Chart','GL_POSTING_YEAR, ROUND(SUM(COST_OF_REMOVAL),2) COST_OF_REMOVAL, ROUND(SUM(COR_BEG_RESERVE),2) BEGIN_COR_RESERVE, CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN NULL ELSE ROUND(100*((SUM(COST_OF_REMOVAL)*-1)/SUM(COR_BEG_RESERVE)),2) END PCT_COR_BEGIN_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1','(COR_BEG_RESERVE IS NOT NULL OR COR_END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1171,'DL_CorBeginReserve_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(COR_BEG_RESERVE),2) BEGIN_COR_RESERVE, ROUND(SUM(COST_OF_REMOVAL),2) COST_OF_REMOVAL, CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN NULL ELSE ROUND(((SUM(COST_OF_REMOVAL)*-1)/SUM(COR_BEG_RESERVE)),4) END PCT_COR_BEGIN_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(COR_BEG_RESERVE IS NOT NULL OR COR_END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1172,'DL_CorBeginReserve_Comp','COMPANY, ROUND(SUM(COR_BEG_RESERVE),2) BEGIN_COR_RESERVE, ROUND(SUM(COST_OF_REMOVAL),2) COST_OF_REMOVAL, CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN NULL ELSE ROUND(((SUM(COST_OF_REMOVAL)*-1)/SUM(COR_BEG_RESERVE)),4) END PCT_COR_BEGIN_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY','(COR_BEG_RESERVE IS NOT NULL OR COR_END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1173,'DL_CorBeginReserve_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, 
	ROUND(SUM(COR_BEG_RESERVE),2) BEGIN_COR_RESERVE,
	ROUND(SUM(COST_OF_REMOVAL),2) COST_OF_REMOVAL, 
	CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN NULL ELSE ROUND(((SUM(COST_OF_REMOVAL)*-1)/SUM(COR_BEG_RESERVE)),4) END PCT_COR_BEGIN_RESERVE,
	ROUND(SUM(COR_END_RESERVE),2) END_COR_RESERVE,
	ROUND(SUM(COR_EXPENSE),2) COR_EXPENSE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','(COR_BEG_RESERVE IS NOT NULL OR COR_END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1174,'DL_CorAddedReserve_Chart','GL_POSTING_YEAR, ROUND(SUM(COR_EXPENSE),2) COR_EXPENSE, ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2) BEGIN_COR_RESERVE, CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN 0 ELSE ROUND((SUM(COR_EXPENSE)/SUM(COR_BEG_RESERVE)),4) END PCT_COR_EXPENSE_BEGIN_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1','(COR_BEG_RESERVE IS NOT NULL OR COR_END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1175,'DL_CorAddedReserve_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(COR_BEG_RESERVE),2) BEGIN_COR_RESERVE, ROUND(SUM(COR_EXPENSE),2) COR_EXPENSE, CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN NULL ELSE ROUND(((SUM(COR_EXPENSE))/SUM(COR_BEG_RESERVE)),4) END PCT_COR_EXPENSE_BEGIN_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(COR_BEG_RESERVE IS NOT NULL OR COR_END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1176,'DL_CorAddedReserve_Comp','COMPANY, ROUND(SUM(COR_BEG_RESERVE),2) BEGIN_COR_RESERVE, ROUND(SUM(COR_EXPENSE),2) COR_EXPENSE, CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN NULL ELSE ROUND(((SUM(COR_EXPENSE))/SUM(COR_BEG_RESERVE)),4) END PCT_COR_EXPENSE_BEGIN_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY','(COR_BEG_RESERVE IS NOT NULL OR COR_END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1177,'DL_CorAddedReserve_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, 
	ROUND(SUM(COR_BEG_RESERVE),2) BEGIN_COR_RESERVE, 
	ROUND(SUM(COR_EXPENSE),2) COR_EXPENSE, 
	CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN NULL ELSE ROUND(((SUM(COR_EXPENSE))/SUM(COR_BEG_RESERVE)),4) END PCT_COR_EXPENSE_BEGIN_RESERVE, 
	ROUND(SUM(COST_OF_REMOVAL),2) COST_OF_REMOVAL, 
	ROUND(SUM(COR_END_RESERVE),2) END_COR_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP,  GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP,  GL_POSTING_YEAR','(COR_BEG_RESERVE IS NOT NULL OR COR_END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1178,'DL_CorReserveChange_Chart','GL_POSTING_YEAR, ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2) BEGIN_COR_RESERVE, ROUND(SUM(NVL(COR_END_RESERVE,0)),2) END_COR_RESERVE, (ROUND(SUM(NVL(COR_END_RESERVE,0)),2)) - (ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2))  ANNUAL_CHANGE_COR_RESERVE, ROUND((CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN 0 ELSE (SUM(COR_END_RESERVE)-SUM(COR_BEG_RESERVE))/SUM(COR_BEG_RESERVE) END),4) PCT_CHANGE_COR_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1','(COR_BEG_RESERVE IS NOT NULL OR COR_END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1179,'DL_CorReserveChange_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(COR_BEG_RESERVE),2) BEGIN_COR_RESERVE, (ROUND(SUM(NVL(COR_END_RESERVE,0)),2)) - (ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2))  ANNUAL_CHANGE_COR_RESERVE, ROUND((CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN 0 ELSE (SUM(COR_END_RESERVE)-SUM(COR_BEG_RESERVE))/SUM(COR_BEG_RESERVE) END),4) PCT_CHANGE_COR_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(COR_BEG_RESERVE IS NOT NULL OR COR_END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1180,'DL_CorReserveChange_Comp','COMPANY,  ROUND(SUM(COR_BEG_RESERVE),2) BEGIN_COR_RESERVE, (ROUND(SUM(NVL(COR_END_RESERVE,0)),2)) - (ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2))  ANNUAL_CHANGE_COR_RESERVE, ROUND((CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN 0 ELSE (SUM(COR_END_RESERVE)-SUM(COR_BEG_RESERVE))/SUM(COR_BEG_RESERVE) END),4) PCT_CHANGE_COR_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY','(COR_BEG_RESERVE IS NOT NULL OR COR_END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1181,'DL_CorReserveChange_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, 
	ROUND(SUM(COR_BEG_RESERVE),2) BEGIN_COR_RESERVE, 
	ROUND(SUM(COR_END_RESERVE),2) END_COR_RESERVE, 
	(ROUND(SUM(NVL(COR_END_RESERVE,0)),2)) - (ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2))  ANNUAL_CHANGE_COR_RESERVE, 
	ROUND((CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN NULL ELSE (SUM(COR_END_RESERVE)-SUM(COR_BEG_RESERVE))/SUM(COR_BEG_RESERVE) END),4) PCT_CHANGE_COR_RESERVE,
	SUM(COR_EXPENSE) COR_EXPENSE, 
	SUM(COST_OF_REMOVAL) COST_OF_REMOVAL','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','(COR_BEG_RESERVE IS NOT NULL OR COR_END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1182,'DL_RetireRatio_Chart','GL_POSTING_YEAR, ROUND(SUM(RETIREMENTS*-1),2) RETIREMENTS, ROUND(SUM(BEGIN_RESERVE),2) BEGIN_LIFE_RESERVE, CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN NULL ELSE ROUND(100*((SUM(RETIREMENTS)*-1)/SUM(BEGIN_RESERVE)),2) END PCT_RETIRE_BEGIN_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1183,'DL_RetireRatio_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(BEGIN_RESERVE),2) BEGIN_LIFE_RESERVE, ROUND(SUM(RETIREMENTS),2) RETIREMENTS, CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN NULL ELSE ROUND(((SUM(RETIREMENTS)*-1)/SUM(BEGIN_RESERVE)),4) END PCT_RETIRE_BEGIN_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1184,'DL_RetireRatio_Comp','COMPANY, ROUND(SUM(BEGIN_RESERVE),2) BEGIN_LIFE_RESERVE, ROUND(SUM(RETIREMENTS),2) RETIREMENTS, CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN NULL ELSE ROUND(((SUM(RETIREMENTS)*-1)/SUM(BEGIN_RESERVE)),4) END PCT_RETIRE_BEGIN_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1185,'DL_RetireRatio_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, 
	ROUND(SUM(BEGIN_RESERVE),2) BEGIN_LIFE_RESERVE, 
	ROUND(SUM(RETIREMENTS),2) RETIREMENTS, 
	CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN NULL ELSE ROUND(((SUM(RETIREMENTS)*-1)/SUM(BEGIN_RESERVE)),4) END PCT_RETIRE_BEGIN_RESERVE,
	ROUND(SUM(END_RESERVE),2) END_LIFE_RESERVE, 
	ROUND(SUM(DEPRECIATION_EXPENSE),2) DEPR_EXPENSE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1186,'DL_DeprExpRatio_Chart','GL_POSTING_YEAR, SUM(BEGIN_RESERVE) BEG_RESERVE, SUM(END_RESERVE) END_RESERVE, SUM(DEPRECIATION_EXPENSE) DEPR_EXPENSE
	 , ROUND((CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE SUM(DEPRECIATION_EXPENSE)/SUM(BEGIN_RESERVE) END),4) PCT_DEPR_BEGIN_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1187,'DL_DeprExpRatio_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(BEGIN_RESERVE),2) BEGIN_LIFE_RESERVE, ROUND(SUM(DEPRECIATION_EXPENSE),2) DEPR_EXPENSE, CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN NULL ELSE ROUND(((SUM(DEPRECIATION_EXPENSE))/SUM(BEGIN_RESERVE)),4) END PCT_DEPR_BEGIN_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1188,'DL_DeprExpRatio_Comp','COMPANY, ROUND(SUM(BEGIN_RESERVE),2) BEGIN_LIFE_RESERVE, ROUND(SUM(DEPRECIATION_EXPENSE),2) DEPR_EXPENSE, CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN NULL ELSE ROUND(((SUM(DEPRECIATION_EXPENSE))/SUM(BEGIN_RESERVE)),4) END PCT_DEPR_BEGIN_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1189,'DL_DeprExpRatio_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, 
	ROUND(SUM(BEGIN_RESERVE),2) BEGIN_LIFE_RESERVE, 
	ROUND(SUM(DEPRECIATION_EXPENSE),2) DEPR_EXPENSE, 
	CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN NULL ELSE ROUND(((SUM(DEPRECIATION_EXPENSE))/SUM(BEGIN_RESERVE)),4) END PCT_DEPR_BEGIN_RESERVE, 
	ROUND(SUM(RETIREMENTS),2) RETIREMENTS, 
	ROUND(SUM(COR_END_RESERVE),2) END_LIFE_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1190,'DL_LifeReserveChange_Chart','GL_POSTING_YEAR, ROUND(SUM(NVL(BEGIN_RESERVE,0)),2) BEGIN_RESERVE, ROUND(SUM(NVL(END_RESERVE,0)),2) END_RESERVE, (ROUND(SUM(NVL(END_RESERVE,0)),2)) - (ROUND(SUM(NVL(BEGIN_RESERVE,0)),2))  ANNUAL_CHANGE_LIFE_RESERVE, ROUND((CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE (SUM(END_RESERVE)-SUM(BEGIN_RESERVE))/SUM(BEGIN_RESERVE) END),4) PCT_CHANGE_LIFE_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1191,'DL_LifeReserveChange_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(NVL(BEGIN_RESERVE,0)),2) BEGIN_RESERVE, (ROUND(SUM(NVL(END_RESERVE,0)),2)) - (ROUND(SUM(NVL(BEGIN_RESERVE,0)),2))  ANNUAL_CHANGE_LIFE_RESERVE, ROUND((CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE (SUM(END_RESERVE)-SUM(BEGIN_RESERVE))/SUM(BEGIN_RESERVE) END),4) PCT_CHANGE_LIFE_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1192,'DL_LifeReserveChange_Comp','COMPANY, ROUND(SUM(NVL(BEGIN_RESERVE,0)),2) BEGIN_RESERVE, (ROUND(SUM(NVL(END_RESERVE,0)),2)) - (ROUND(SUM(NVL(BEGIN_RESERVE,0)),2))  ANNUAL_CHANGE_LIFE_RESERVE, ROUND((CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE (SUM(END_RESERVE)-SUM(BEGIN_RESERVE))/SUM(BEGIN_RESERVE) END),4) PCT_CHANGE_LIFE_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1193,'DL_LifeReserveChange_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, 
	ROUND(SUM(NVL(BEGIN_RESERVE,0)),2) BEGIN_LIFE_RESERVE, 
	(ROUND(SUM(NVL(END_RESERVE,0)),2)) - (ROUND(SUM(NVL(BEGIN_RESERVE,0)),2))  ANNUAL_CHANGE_LIFE_RESERVE, 
	ROUND((CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN NULL ELSE (SUM(END_RESERVE)-SUM(BEGIN_RESERVE))/SUM(BEGIN_RESERVE) END),4) PCT_CHANGE_LIFE_RESERVE, 
	SUM(DEPRECIATION_EXPENSE) DEPR_EXPENSE, 
	SUM(RETIREMENTS) RETIREMENTS','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1194,'AA_WeightedAges_Chart','PROCESS_YEAR, ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	END),2) COST_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	END),2) QUANTITY_WEIGHTED_AGE ','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'PROCESS_YEAR','1','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL OR CURR_ADJ IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1195,'AA_WeightedAges_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	END),2) COST_WEIGHTED_AGE, ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE  ','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL OR CURR_ADJ IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1196,'AA_WeightedAges_Locn','STATE, MAJOR_LOCATION, ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	END),2) COST_WEIGHTED_AGE, ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE ','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL OR CURR_ADJ IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1197,'AA_WeightedAges_Comp','COMPANY, ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	END),2) COST_WEIGHTED_AGE, ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE ','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL OR CURR_ADJ IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1198,'AA_WeightedAges_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR,
	ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	  END),2) COST_WEIGHTED_AGE,
	ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	  END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE, 
	SUM(ADDITION_QTY) ADDITION_QTY, 
	SUM(ADDITION_COST) ADDITION_COST,
	SUM(RETIRE_QTY) RETIRE_QTY, 
	SUM(RETIRE_COST) RETIRE_COST','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR','RETIRE_UNIT_ID > 5 AND (CURR_COST IS NOT NULL OR CURR_QTY IS NOT NULL OR CURR_ADJ IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1199,'AA_Additions_Chart','PROCESS_YEAR, SUM(ADDITION_QTY) ADDITION_QTY, SUM(ADDITION_COST) ADDITION_COST','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'PROCESS_YEAR','1','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1200,'AA_Additions_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1201,'AA_Additions_Locn','STATE, MAJOR_LOCATION, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1202,'AA_Additions_Comp','COMPANY, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1203,'AA_Additions_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR,
	ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	  END),2) COST_WEIGHTED_AGE,
	ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	  END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE, 
	SUM(ADDITION_QTY) ADDITION_QTY, 
	SUM(ADDITION_COST) ADDITION_COST,
	SUM(RETIRE_QTY) RETIRE_QTY, 
	SUM(RETIRE_COST) RETIRE_COST','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1204,'AA_Retirements_Chart','PROCESS_YEAR, SUM(RETIRE_QTY*-1) RETIRE_QTY, SUM(RETIRE_COST*-1) RETIRE_COST','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'PROCESS_YEAR','1','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1205,'AA_Retirements_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1206,'AA_Retirements_Locn','STATE, MAJOR_LOCATION, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1207,'AA_Retirements_Comp','COMPANY, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1208,'AA_Retirements_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR,
	ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	  END),2) COST_WEIGHTED_AGE,
	ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	  END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE, 
	SUM(ADDITION_QTY) ADDITION_QTY, 
	SUM(ADDITION_COST) ADDITION_COST,
	SUM(RETIRE_QTY) RETIRE_QTY, 
	SUM(RETIRE_COST) RETIRE_COST','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1209,'AA_AnnualChange_Chart','PROCESS_YEAR, ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	END),2) COST_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	END),2) QUANTITY_WEIGHTED_AGE ','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'PROCESS_YEAR','1','RETIRE_UNIT_ID > 5 and (FLAG_ACP2_COST = ''Y'' OR FLAG_ACM2_COST = ''Y'' OR FLAG_ACP2_QTY = ''Y'' OR FLAG_ACM2_QTY = ''Y'') ',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1210,'AA_AnnualChange_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	END),2) COST_WEIGHTED_AGE, ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 and (FLAG_ACP2_COST = ''Y'' OR FLAG_ACM2_COST = ''Y'' OR FLAG_ACP2_QTY = ''Y'' OR FLAG_ACM2_QTY = ''Y'') ',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1211,'AA_AnnualChange_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR,
	ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	  END),2) COST_WEIGHTED_AGE,
	ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	  END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE, 
	SUM(ADDITION_QTY) ADDITION_QTY, 
	SUM(ADDITION_COST) ADDITION_COST,
	SUM(RETIRE_QTY) RETIRE_QTY, 
	SUM(RETIRE_COST) RETIRE_COST','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR','RETIRE_UNIT_ID > 5 and (FLAG_ACP2_COST = ''Y'' OR FLAG_ACM2_COST = ''Y'' OR FLAG_ACP2_QTY = ''Y'' OR FLAG_ACM2_QTY = ''Y'') ',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1212,'AA_PeriodChange_Chart','PROCESS_YEAR, ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	END),2) COST_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	END),2) QUANTITY_WEIGHTED_AGE ','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'PROCESS_YEAR','1','RETIRE_UNIT_ID > 5 and (FLAG_PCP1_COST = ''Y'' OR FLAG_PCM1_COST = ''Y'' OR FLAG_PCP1_QTY = ''Y'' OR FLAG_PCM1_QTY = ''Y'') ',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1213,'AA_PeriodChange_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	END),2) COST_WEIGHTED_AGE, ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 and (FLAG_PCP1_COST = ''Y'' OR FLAG_PCM1_COST = ''Y'' OR FLAG_PCP1_QTY = ''Y'' OR FLAG_PCM1_QTY = ''Y'') ',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1214,'AA_PeriodChange_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR,
	ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	  END),2) COST_WEIGHTED_AGE,
	ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	  END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE, 
	SUM(ADDITION_QTY) ADDITION_QTY, 
	SUM(ADDITION_COST) ADDITION_COST,
	SUM(RETIRE_QTY) RETIRE_QTY, 
	SUM(RETIRE_COST) RETIRE_COST','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR','RETIRE_UNIT_ID > 5 and (FLAG_PCP1_COST = ''Y'' OR FLAG_PCM1_COST = ''Y'' OR FLAG_PCP1_QTY = ''Y'' OR FLAG_PCM1_QTY = ''Y'') ',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1215,'RE_ZCost_Qty_Chart','GL_POSTING_YEAR, ROUND(SUM(ACTIVITY_COST),2) RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_QTY > 0 THEN ACTIVITY_QTY ELSE 0 END),2) POS_RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_QTY < 0 THEN ACTIVITY_QTY ELSE 0 END),2) NEG_RETIREMENT_QTY, ROUND(SUM(ACTIVITY_QTY),2)*-1 NET_RETIREMENT_QTY, COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_ACTSUM',null,'GL_POSTING_YEAR','1','RETIRE_UNIT_ID > 5 AND FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_COST = 0 AND (ACTIVITY_QTY <> 0 and ACTIVITY_QTY IS NOT NULL)',null,null,null,4);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1216,'RE_ZCost_Qty_Comp','COMPANY, ROUND(SUM(CASE WHEN ACTIVITY_QTY > 0 THEN ACTIVITY_QTY ELSE 0 END),2) POS_RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_QTY < 0 THEN ACTIVITY_QTY ELSE 0 END),2) NEG_RETIREMENT_QTY, COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_ACTSUM',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_COST = 0 AND (ACTIVITY_QTY <> 0 and ACTIVITY_QTY IS NOT NULL)',null,null,null,4);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1217,'RE_ZCost_Qty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, GL_POSTING_YEAR, VINTAGE_YEAR,
	ROUND(SUM(ACTIVITY_COST),2) RETIREMENT_COST, 
	ROUND(SUM(CASE WHEN ACTIVITY_QTY > 0 THEN ACTIVITY_QTY ELSE 0 END),2) POS_RETIREMENT_QTY, 
	ROUND(SUM(CASE WHEN ACTIVITY_QTY < 0 THEN ACTIVITY_QTY ELSE 0 END),2) NEG_RETIREMENT_QTY, 
	ROUND(SUM(ACTIVITY_QTY),2) NET_RETIREMENT_QTY, 
	COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_ACTSUM',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, GL_POSTING_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, GL_POSTING_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_COST = 0 AND (ACTIVITY_QTY <> 0 and ACTIVITY_QTY IS NOT NULL)',null,null,null,4);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1218,'RE_ZQty_Cost_Chart','GL_POSTING_YEAR, ROUND(SUM(ACTIVITY_QTY),2) RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_COST > 0 THEN ACTIVITY_COST ELSE 0 END),2) POS_RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_COST < 0 THEN ACTIVITY_COST ELSE 0 END),2) NEG_RETIREMENT_COST, ROUND(SUM(ACTIVITY_COST),2)*-1 NET_RETIREMENT_COST, COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_ACTSUM',null,'GL_POSTING_YEAR','1','RETIRE_UNIT_ID > 5 AND FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_QTY = 0 AND (ACTIVITY_COST <> 0 and ACTIVITY_COST IS NOT NULL)',null,null,null,4);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1219,'RE_ZQty_Cost_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(CASE WHEN ACTIVITY_COST > 0 THEN ACTIVITY_COST ELSE 0 END),2) POS_RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_COST < 0 THEN ACTIVITY_COST ELSE 0 END),2) NEG_RETIREMENT_COST,  COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_ACTSUM',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_QTY = 0 AND (ACTIVITY_COST <> 0 and ACTIVITY_COST IS NOT NULL)',null,null,null,4);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1220,'RE_ZQty_Cost_Locn','STATE, MAJOR_LOCATION, ROUND(SUM(CASE WHEN ACTIVITY_COST > 0 THEN ACTIVITY_COST ELSE 0 END),2) POS_RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_COST < 0 THEN ACTIVITY_COST ELSE 0 END),2) NEG_RETIREMENT_COST,  COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_ACTSUM',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_QTY = 0 AND (ACTIVITY_COST <> 0 and ACTIVITY_COST IS NOT NULL)',null,null,null,4);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1221,'RE_ZQty_Cost_Comp','COMPANY, ROUND(SUM(CASE WHEN ACTIVITY_COST > 0 THEN ACTIVITY_COST ELSE 0 END),2) POS_RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_COST < 0 THEN ACTIVITY_COST ELSE 0 END),2) NEG_RETIREMENT_COST,  COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_ACTSUM',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_QTY = 0 AND (ACTIVITY_COST <> 0 and ACTIVITY_COST IS NOT NULL)',null,null,null,4);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1222,'RE_ZQty_Cost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, GL_POSTING_YEAR, VINTAGE_YEAR,
	ROUND(SUM(ACTIVITY_QTY),2) RETIREMENT_QTY, 
	ROUND(SUM(CASE WHEN ACTIVITY_COST > 0 THEN ACTIVITY_COST ELSE 0 END),2) POS_RETIREMENT_COST, 
	ROUND(SUM(CASE WHEN ACTIVITY_COST < 0 THEN ACTIVITY_COST ELSE 0 END),2) NEG_RETIREMENT_COST,  
	ROUND(SUM(ACTIVITY_COST),2) NET_RETIREMENT_COST, 
	COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_ACTSUM',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, GL_POSTING_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, GL_POSTING_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_QTY = 0 AND (ACTIVITY_COST <> 0 and ACTIVITY_COST IS NOT NULL)',null,null,null,4);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1223,'AP_VintageAdds_Chart','VINTAGE_INTERVAL, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1224,'RP_VintageRets_Chart','VINTAGE_INTERVAL, (SUM(RETIRE_COST)*-1) RETIRE_COST, (SUM(RETIRE_QTY)*-1) RETIRE_QTY','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1225,'PR_VintageCost_Chart','VINTAGE_INTERVAL, round(sum(nvl((retire_cost*-1),0)),2) as retire_cost, round(sum(proj_retire_cost),2) as proj_retire_cost, round(sum(proj_retire_cost) - sum(nvl((retire_cost*-1),0)),2) delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1226,'PR_VintageQty_Chart','VINTAGE_INTERVAL, round(sum(nvl((retire_qty*-1),0)),2) as retire_qty, round(sum(proj_retire_qty),2) as proj_retire_qty, round(sum(proj_retire_qty) - sum(nvl((retire_qty*-1),0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1228,'PR_AnnualQty_Chart','process_year, round(sum(nvl(retire_qty*-1,0)),2) as retire_qty, round(sum(proj_retire_qty),2) as proj_retire_qty,  round(sum(proj_retire_qty) - sum(nvl(retire_qty*-1,0)),2) qty_delta
	','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'process_year','1','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1229,'RA_PctOverUnder_Chart','GL_POSTING_YEAR, SUM(END_RESERVE) END_RESERVE, SUM(END_THEO_RESERVE) END_THEO_RESERVE, case when sum(end_theo_reserve) = 0 then NULL else ROUND(((SUM(END_RESERVE) - SUM(END_THEO_RESERVE))/SUM(END_THEO_RESERVE))*100,2) end PCT_OVER_UNDER_RESERVED','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL) AND (BEGIN_THEO_RESERVE IS NOT NULL OR END_THEO_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1230,'CR_CorRetireMissing_Chart','vintage_interval, sum(nvl(cost_of_removal*-1,0)) cor, sum(nvl(retire_qty*-1,0)) retire_qty, sum(nvl(retire_cost*-1,0)) retire_cost','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'vintage_interval','1,2','RETIRE_UNIT_ID > 5 and (retire_qty = 0 or retire_cost = 0)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1231,'RE_ZCost_Qty_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(CASE WHEN ACTIVITY_QTY > 0 THEN ACTIVITY_QTY ELSE 0 END),2) POS_RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_QTY < 0 THEN ACTIVITY_QTY ELSE 0 END),2) NEG_RETIREMENT_QTY, COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_ACTSUM',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_COST = 0 AND (ACTIVITY_QTY <> 0 and ACTIVITY_QTY IS NOT NULL)',null,null,null,4);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1232,'RE_ZCost_Qty_Locn','STATE, MAJOR_LOCATION, ROUND(SUM(CASE WHEN ACTIVITY_QTY > 0 THEN ACTIVITY_QTY ELSE 0 END),2) POS_RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_QTY < 0 THEN ACTIVITY_QTY ELSE 0 END),2) NEG_RETIREMENT_QTY, COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_ACTSUM',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_COST = 0 AND (ACTIVITY_QTY <> 0 and ACTIVITY_QTY IS NOT NULL)',null,null,null,4);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1233,'AA_AnnualChange_Comp','COMPANY, ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	END),2) COST_WEIGHTED_AGE, ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 and (FLAG_ACP2_COST = ''Y'' OR FLAG_ACM2_COST = ''Y'' OR FLAG_ACP2_QTY = ''Y'' OR FLAG_ACM2_QTY = ''Y'') ',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1234,'AA_AnnualChange_Locn','STATE, MAJOR_LOCATION, ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	END),2) COST_WEIGHTED_AGE, ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 and (FLAG_ACP2_COST = ''Y'' OR FLAG_ACM2_COST = ''Y'' OR FLAG_ACP2_QTY = ''Y'' OR FLAG_ACM2_QTY = ''Y'') ',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1235,'AA_PeriodChange_Locn','STATE, MAJOR_LOCATION, ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	END),2) COST_WEIGHTED_AGE, ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 and (FLAG_PCP1_COST = ''Y'' OR FLAG_PCM1_COST = ''Y'' OR FLAG_PCP1_QTY = ''Y'' OR FLAG_PCM1_QTY = ''Y'') ',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1236,'AA_PeriodChange_Comp','COMPANY, ROUND((CASE SUM(CURR_COST)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_CST)/SUM(W_COST)
	END),2) COST_WEIGHTED_AGE, ROUND((CASE SUM(CURR_QTY)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
	END),2) QUANTITY_WEIGHTED_AGE, 
	ROUND((CASE SUM(CURR_ADJ)
	  WHEN 0 THEN 0 
	  ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
	END),2) ESCALATED_COST_WEIGHTED_AGE','PWRPLANT.TEMP_PA_AVERAGE_AGES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 and (FLAG_PCP1_COST = ''Y'' OR FLAG_PCM1_COST = ''Y'' OR FLAG_PCP1_QTY = ''Y'' OR FLAG_PCM1_QTY = ''Y'') ',null,null,null,1);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1237,'LE_FutureAssets_Locn','STATE, MAJOR_LOCATION,  SUM(CURR_COST) TOTAL_COST, SUM(CURR_QTY) TOTAL_QUANTITY, COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 and VINTAGE_YEAR > EXTRACT(YEAR FROM SYSDATE) ',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1238,'LE_FutureAssets_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT,  SUM(CURR_COST) TOTAL_COST, SUM(CURR_QTY) TOTAL_QUANTITY, SUM(CURR_ADJ), COUNT(*) RECORD_COUNT','PWRPLANT.TEMP_PA_CPR_LEDGER',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 and VINTAGE_YEAR > EXTRACT(YEAR FROM SYSDATE) ',null,null,null,6);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1239,'PR_AnnualQty_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1240,'PR_AnnualQty_Comp','COMPANY, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1241,'PR_AnnualQty_Locn','STATE, MAJOR_LOCATION, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1242,'RA_PctOverUnder_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(END_RESERVE) END_LIFE_RESERVE, SUM(END_THEO_RESERVE) END_THEO_RESERVE, case when sum(end_theo_reserve) = 0 then null else ROUND(((SUM(END_RESERVE) - SUM(END_THEO_RESERVE))/SUM(END_THEO_RESERVE)),4) end PCT_OVER_UNDER_RESERVED','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL) AND (BEGIN_THEO_RESERVE IS NOT NULL OR END_THEO_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1243,'RA_PctOverUnder_Comp','COMPANY, SUM(END_RESERVE) END_LIFE_RESERVE, SUM(END_THEO_RESERVE) END_THEO_RESERVE, case when sum(end_theo_reserve) = 0 then null else ROUND(((SUM(END_RESERVE) - SUM(END_THEO_RESERVE))/SUM(END_THEO_RESERVE)),4) end PCT_OVER_UNDER_RESERVED','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL) AND (BEGIN_THEO_RESERVE IS NOT NULL OR END_THEO_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1244,'AP_VintageAdds_Comp','COMPANY, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, CASE WHEN SUM(ADDITION_QTY) = 0 THEN 0 ELSE ROUND(SUM(ADDITION_COST)/SUM(ADDITION_QTY),2) END COST_PER_UNIT_ADDED','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1245,'AP_VintageAdds_Locn','STATE, MAJOR_LOCATION, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, CASE WHEN SUM(ADDITION_QTY) = 0 THEN 0 ELSE ROUND(SUM(ADDITION_COST)/SUM(ADDITION_QTY),2) END COST_PER_UNIT_ADDED','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1246,'AP_VintageAdds_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, CASE WHEN SUM(ADDITION_QTY) = 0 THEN 0 ELSE ROUND(SUM(ADDITION_COST)/SUM(ADDITION_QTY),2) END COST_PER_UNIT_ADDED','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1247,'RP_VintageRets_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY,  CASE WHEN SUM(RETIRE_QTY) = 0 THEN 0 ELSE ROUND(SUM(RETIRE_COST)/SUM(RETIRE_QTY),2) END COST_PER_UNIT_RETIRED','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1248,'RP_VintageRets_Locn','STATE, MAJOR_LOCATION, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY,  CASE WHEN SUM(RETIRE_QTY) = 0 THEN 0 ELSE ROUND(SUM(RETIRE_COST)/SUM(RETIRE_QTY),2) END COST_PER_UNIT_RETIRED','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1249,'RP_VintageRets_Comp','COMPANY, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY,  CASE WHEN SUM(RETIRE_QTY) = 0 THEN 0 ELSE ROUND(SUM(RETIRE_COST)/SUM(RETIRE_QTY),2) END COST_PER_UNIT_RETIRED','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1250,'PR_VintageCost_Comp','COMPANY, round(sum(nvl((retire_cost),0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl((retire_cost),0)),2) cost_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1251,'PR_VintageCost_Locn','STATE, MAJOR_LOCATION, round(sum(nvl((retire_cost),0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl((retire_cost),0)),2) cost_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1252,'PR_VintageCost_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT,round(sum(nvl((retire_cost),0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl((retire_cost),0)),2) cost_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1253,'PR_VintageQty_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT,  round(sum(nvl((retire_qty),0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl((retire_qty),0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1254,'PR_VintageQty_Comp','COMPANY, round(sum(nvl((retire_qty),0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl((retire_qty),0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1255,'PR_VintageQty_Locn','STATE, MAJOR_LOCATION, round(sum(nvl((retire_qty),0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl((retire_qty),0)),2) qty_delta','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1256,'CR_CorRetireMissing_Comp','COMPANY, sum(nvl(cost_of_removal,0)) cor,  sum(nvl(retire_cost,0)) retire_cost, sum(nvl(retire_qty,0)) retire_qty','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'company','company','RETIRE_UNIT_ID > 5 AND (nvl(RETIRE_QTY,0) = 0 OR nvl(RETIRE_COST,0) = 0)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1257,'CR_CorRetireMissing_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, sum(nvl(cost_of_removal,0)) cor,  sum(nvl(retire_cost,0)) retire_cost, sum(nvl(retire_qty,0)) retire_qty','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND (nvl(RETIRE_QTY,0) = 0 OR nvl(RETIRE_COST,0) = 0)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1258,'CR_CorRetireMissing_Locn','STATE, MAJOR_LOCATION, sum(nvl(cost_of_removal,0)) cor,  sum(nvl(retire_cost,0)) retire_cost, sum(nvl(retire_qty,0)) retire_qty','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND (nvl(RETIRE_QTY,0) = 0 OR nvl(RETIRE_COST,0) = 0)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1259,'CR_CorRetireMissing_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	sum(nvl(cost_of_removal,0)) cor,
	sum(nvl(retire_cost,0)) retire_cost,
	sum(nvl(retire_qty,0)) retire_qty, 
	sum(nvl(addition_cost,0)) addition_cost,
	sum(nvl(addition_qty,0)) addition_qty','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND (nvl(RETIRE_QTY,0) = 0 OR nvl(RETIRE_COST,0) = 0)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1260,'PR_VintageQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	round(sum(nvl(retire_qty,0)),2) as retire_qty,
	round(sum(proj_retire_qty*-1),2) as proj_retire_qty,
	round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta, 
	round(sum(nvl(retire_cost,0)),2) as retire_cost,
	round(sum(proj_retire_cost*-1),2) as proj_retire_cost,
	round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta,
	round(sum(annual_begin_cost),2) annual_begin_cost,
	round(sum(annual_begin_qty),2) annual_begin_qty','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1261,'PR_VintageCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	round(sum(nvl(retire_cost,0)),2) as retire_cost,
	round(sum(proj_retire_cost*-1),2) as proj_retire_cost,
	round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta,
	round(sum(nvl(retire_qty,0)),2) as retire_qty,
	round(sum(proj_retire_qty*-1),2) as proj_retire_qty,
	round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta, 
	round(sum(annual_begin_cost),2) annual_begin_cost,
	round(sum(annual_begin_qty),2) annual_begin_qty','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1262,'RP_VintageRets_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	SUM(RETIRE_COST) RETIRE_COST,
	SUM(RETIRE_QTY) RETIRE_QTY,
	CASE WHEN SUM(RETIRE_QTY) = 0 THEN 0 ELSE ROUND(SUM(RETIRE_COST)/SUM(RETIRE_QTY),2) END COST_PER_UNIT_RETIRED, 
	SUM(ANNUAL_BEGIN_COST) ANNUAL_BEGIN_COST,
	SUM(ANNUAL_BEGIN_QTY) ANNUAL_BEGIN_QTY','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1263,'AP_VintageAdds_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	SUM(ADDITION_COST) ADDITION_COST,
	SUM(ADDITION_QTY) ADDITION_QTY, 
	CASE WHEN SUM(ADDITION_QTY) = 0 THEN 0 ELSE ROUND(SUM(ADDITION_COST)/SUM(ADDITION_QTY),2) END COST_PER_UNIT_ADDED,
	SUM(ANNUAL_BEGIN_COST) ANNUAL_BEGIN_COST,
	SUM(ANNUAL_BEGIN_QTY) ANNUAL_BEGIN_QTY','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND (ADDITION_COST IS NOT NULL OR ADDITION_QTY IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1264,'RA_PctOverUnder_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, 
	SUM(END_RESERVE) END_LIFE_RESERVE, 
	SUM(END_THEO_RESERVE) END_THEO_RESERVE, 
	case when sum(end_theo_reserve) = 0 then null else ROUND(((SUM(END_RESERVE) - SUM(END_THEO_RESERVE))/SUM(END_THEO_RESERVE)),4) end PCT_OVER_UNDER_RESERVED, 
	SUM(BEGIN_RESERVE) BEGIN_LIFE_RESERVE, 
	SUM(BEGIN_THEO_RESERVE) BEGIN_THEO_RESERVE','PWRPLANT.TEMP_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','(BEGIN_RESERVE IS NOT NULL OR END_RESERVE IS NOT NULL) AND (BEGIN_THEO_RESERVE IS NOT NULL OR END_THEO_RESERVE IS NOT NULL)',null,null,null,8);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1265,'PR_AnnualQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	round(sum(nvl(retire_qty,0)),2) as retire_qty,
	round(sum(proj_retire_qty*-1),2) as proj_retire_qty,
	round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta, 
	round(sum(nvl(retire_cost,0)),2) as retire_cost,
	round(sum(proj_retire_cost*-1),2) as proj_retire_cost,
	round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta,
	round(sum(annual_begin_cost),2) annual_begin_cost,
	round(sum(annual_begin_qty),2) annual_begin_qty','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1266,'PR_AnnualCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	round(sum(nvl(retire_cost,0)),2) as retire_cost,
	round(sum(proj_retire_cost*-1),2) as proj_retire_cost,
	round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta,
	round(sum(nvl(retire_qty,0)),2) as retire_qty,
	round(sum(proj_retire_qty*-1),2) as proj_retire_qty,
	round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta, 
	round(sum(annual_begin_cost),2) annual_begin_cost,
	round(sum(annual_begin_qty),2) annual_begin_qty','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1267,'PR_UnderCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	round(sum(nvl(retire_cost,0)),2) as retire_cost,
	round(sum(proj_retire_cost*-1),2) as proj_retire_cost,
	round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta,
	round(sum(nvl(retire_qty,0)),2) as retire_qty,
	round(sum(proj_retire_qty*-1),2) as proj_retire_qty,
	round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta, 
	round(sum(annual_begin_cost),2) annual_begin_cost,
	round(sum(annual_begin_qty),2) annual_begin_qty','PWRPLANT.TEMP_PA_CPR_BALANCES','sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)) < 0','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1268,'PR_OverCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	round(sum(nvl(retire_cost,0)),2) as retire_cost,
	round(sum(proj_retire_cost*-1),2) as proj_retire_cost,
	round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta,
	round(sum(nvl(retire_qty,0)),2) as retire_qty,
	round(sum(proj_retire_qty*-1),2) as proj_retire_qty,
	round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta, 
	round(sum(annual_begin_cost),2) annual_begin_cost,
	round(sum(annual_begin_qty),2) annual_begin_qty','PWRPLANT.TEMP_PA_CPR_BALANCES','sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)) > 0','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1269,'PR_UnderQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	round(sum(nvl(retire_qty,0)),2) as retire_qty,
	round(sum(proj_retire_qty*-1),2) as proj_retire_qty,
	round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta, 
	round(sum(nvl(retire_cost,0)),2) as retire_cost,
	round(sum(proj_retire_cost*-1),2) as proj_retire_cost,
	round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta,
	round(sum(annual_begin_cost),2) annual_begin_cost,
	round(sum(annual_begin_qty),2) annual_begin_qty','PWRPLANT.TEMP_PA_CPR_BALANCES','sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)) < 0','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1270,'PR_OverQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,
	round(sum(nvl(retire_qty,0)),2) as retire_qty,
	round(sum(proj_retire_qty*-1),2) as proj_retire_qty,
	round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta, 
	round(sum(nvl(retire_cost,0)),2) as retire_cost,
	round(sum(proj_retire_cost*-1),2) as proj_retire_cost,
	round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) cost_delta,
	round(sum(annual_begin_cost),2) annual_begin_cost,
	round(sum(annual_begin_qty),2) annual_begin_qty','PWRPLANT.TEMP_PA_CPR_BALANCES','sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)) > 0','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND PROJ_RETIRE_COST IS NOT NULL',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1271,'RP_AnnualRetire_Detail_ACT','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRPLANT.TEMP_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 and UPPER(FERC_ACTIVITY) = ''RETIREMENT''',null,null,null,5);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1272,'RP_PositiveRetsCost_Detail_ACT','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRPLANT.TEMP_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 and UPPER(FERC_ACTIVITY) = ''RETIREMENT'' and ACTIVITY_COST > 0',null,null,null,5);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1273,'RP_PositiveRetsQty_Detail_ACT','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRPLANT.TEMP_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 and UPPER(FERC_ACTIVITY) = ''RETIREMENT'' and ACTIVITY_QTY > 0',null,null,null,5);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1274,'AP_AnnualAdds_Detail_ACT','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRPLANT.TEMP_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 and UPPER(FERC_ACTIVITY) = ''ADDITION''',null,null,null,5);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1275,'AP_NegativeAddsCost_Detail_ACT','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRPLANT.TEMP_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 and UPPER(FERC_ACTIVITY) = ''ADDITION'' and ACTIVITY_COST < 0',null,null,null,5);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1276,'AP_NegativeAddsQty_Detail_ACT','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRPLANT.TEMP_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 and UPPER(FERC_ACTIVITY) = ''ADDITION'' and ACTIVITY_QTY < 0',null,null,null,5);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1277,'LA_PastMax_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
	AGE_GROUP, EXPECTED_LIFE, YEARS_PAST_EXPECTED, MAX_LIFE, YEARS_PAST_MAX, MORT_CURVE_DESC, 
	ROUND(SUM(CURR_COST),2) TOTAL_COST, 
	ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, 
	ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
	AGE_GROUP, EXPECTED_LIFE, YEARS_PAST_EXPECTED, MAX_LIFE, YEARS_PAST_MAX, MORT_CURVE_DESC','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
	AGE_GROUP, EXPECTED_LIFE, YEARS_PAST_EXPECTED, MAX_LIFE, YEARS_PAST_MAX, MORT_CURVE_DESC','RETIRE_UNIT_ID > 5 and (YEARS_PAST_MAX >=0 AND YEARS_PAST_MAX IS NOT NULL)',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1278,'LA_PastExpected_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
	AGE_GROUP, EXPECTED_LIFE, YEARS_PAST_EXPECTED, MAX_LIFE, YEARS_PAST_MAX, MORT_CURVE_DESC, 
	ROUND(SUM(CURR_COST),2) TOTAL_COST, 
	ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, 
	ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRPLANT.TEMP_PA_VINTAGE_SPREADS',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
	AGE_GROUP, EXPECTED_LIFE, YEARS_PAST_EXPECTED, MAX_LIFE, YEARS_PAST_MAX, MORT_CURVE_DESC','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
	AGE_GROUP, EXPECTED_LIFE, YEARS_PAST_EXPECTED, MAX_LIFE, YEARS_PAST_MAX, MORT_CURVE_DESC','RETIRE_UNIT_ID > 5 and YEARS_PAST_EXPECTED > 0 AND YEARS_PAST_MAX <=0',null,null,null,2);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1279,'SV_SalvagePerRetirement_Chart','process_year, sum(salvage_cash) salvage, sum(retire_qty*-1) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(salvage_cash)/sum(retire_qty*-1) end,2) salvage_per_unit, sum(retire_cost*-1) retire_cost, case when sum(retire_cost) = 0 then null else round(sum(salvage_cash)/sum(retire_cost*-1) * 100,2) end pct_salvage_retire_cost','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'process_year','1','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL OR SALVAGE_CASH IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1280,'SV_SalvagePerRetirement_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, sum(salvage_cash) salvage, nvl(sum(retire_qty),0) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(salvage_cash)/sum(retire_qty*-1) end,2) salvage_per_unit','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL OR SALVAGE_CASH IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1281,'SV_SalvagePerRetirement_Comp','COMPANY, sum(salvage_cash) salvage, nvl(sum(retire_qty),0) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(salvage_cash)/sum(retire_qty*-1) end,2) salvage_per_unit','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL OR SALVAGE_CASH IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1282,'SV_SalvagePerRetirement_Locn','STATE, MAJOR_LOCATION, sum(salvage_cash) salvage, nvl(sum(retire_qty),0) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(salvage_cash)/sum(retire_qty*-1) end,2) salvage_per_unit','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL OR SALVAGE_CASH IS NOT NULL)',null,null,null,3);
	INSERT INTO PWRPLANT.PA_DATASOURCE values (1283,'SV_SalvagePerRetirement_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, 
	sum(salvage_cash) salvage, 
	nvl(sum(retire_qty),0) retire_qty, 
	round(case when sum(retire_qty) = 0 then null else sum(salvage_cash)/sum(retire_qty*-1) end,2) salvage_per_unit,
	sum(retire_cost) retire_cost, 
	CASE WHEN SUM(NVL(RETIRE_COST,0)) = 0 THEN NULL ELSE ROUND((SUM(SALVAGE_CASH)/SUM(RETIRE_COST)),4) END PCT_SALV_RETIRE_COST','PWRPLANT.TEMP_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, RETIRE_UNIT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_UNIT_ID > 5 AND (RETIRE_COST IS NOT NULL OR RETIRE_QTY IS NOT NULL OR COST_OF_REMOVAL IS NOT NULL OR SALVAGE_CASH IS NOT NULL)',null,null,null,3);
	

    delete from PWRPLANT.PA_EXPLORATION;
    

    -- Inserts for PWRPLANT.PA_EXPLORATION
    INSERT INTO PWRPLANT.PA_EXPLORATION values (1000,1000,1008,1267,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1001,1001,1011,1268,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1003,1003,1017,1269,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1004,1004,1020,1270,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1006,1006,1029,1033,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1007,1007,1032,1034,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1008,1008,1029,1033,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1009,1009,1040,1047,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1010,1010,1043,1278,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1011,1011,1046,1277,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1012,1012,1040,1047,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1013,1013,1043,1278,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1014,1014,1046,1277,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1015,1015,1040,1047,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1016,1016,1043,1278,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1017,1017,1046,1277,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1018,1018,1055,1056,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1019,1019,1059,1060,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1020,1020,1065,1066,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1021,1021,1063,1064,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1022,1022,1069,1070,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1023,1023,1074,1075,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1025,1025,1084,1085,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1027,1027,1094,1095,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1028,1028,1099,1100,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1030,1030,1109,1110,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1032,1032,1119,1120,2);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1033,1033,1154,1155,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1034,1034,1127,1128,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1036,1036,1135,1136,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1037,1037,1140,1141,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1038,1038,1145,1146,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1039,1039,1150,1266,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1043,1043,1167,1169,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1044,1044,1167,1169,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1045,1045,1167,1169,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1046,1046,1167,1169,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1047,1047,1167,1169,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1048,1048,1167,1169,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1049,1049,1172,1173,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1050,1050,1176,1177,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1051,1051,1180,1181,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1052,1052,1184,1185,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1053,1053,1188,1189,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1054,1054,1192,1193,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1055,1055,1197,1198,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1056,1056,1202,1203,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1057,1057,1207,1208,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1058,1058,1233,1211,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1059,1059,1236,1214,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1060,1060,1216,1217,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1061,1061,1221,1222,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1062,1062,1244,1263,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1063,1063,1249,1262,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1064,1064,1250,1261,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1066,1066,1254,1260,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1067,1067,1240,1265,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1068,1068,1243,1264,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1069,1069,1256,1259,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1070,1070,1140,1141,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1071,1071,1281,1283,1);
	INSERT INTO PWRPLANT.PA_EXPLORATION values (1072,1072,1281,1283,1);
	
    

    delete from PWRPLANT.PA_EXPLORE_DATASOURCE;
    

    -- Inserts for PWRPLANT.PA_EXPLORE_DATASOURCE
    INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (1,1000,1,'Account',1006);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (2,1000,2,'Location',1007);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (3,1000,3,'Company',1008);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (4,1001,1,'Account',1009);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (5,1001,2,'Location',1010);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (6,1001,3,'Company',1011);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (7,1002,1,'Account',1012);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (8,1002,2,'Location',1013);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (9,1002,3,'Company',1014);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (10,1003,1,'Account',1015);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (11,1003,2,'Location',1016);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (12,1003,3,'Company',1017);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (13,1004,1,'Account',1018);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (14,1004,2,'Location',1019);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (15,1004,3,'Company',1020);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (16,1005,1,'Account',1021);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (17,1005,2,'Location',1022);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (18,1005,3,'Company',1023);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (19,1006,1,'Account',1027);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (20,1006,2,'Location',1028);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (21,1006,3,'Company',1029);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (22,1007,1,'Account',1030);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (23,1007,2,'Location',1031);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (24,1007,3,'Company',1032);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (25,1009,1,'Account',1038);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (26,1009,2,'Location',1039);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (27,1009,3,'Company',1040);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (28,1010,1,'Account',1041);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (29,1010,2,'Location',1042);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (30,1010,3,'Company',1043);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (31,1011,1,'Account',1044);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (32,1011,2,'Location',1045);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (33,1011,3,'Company',1046);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (34,1012,1,'Account',1038);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (35,1012,2,'Location',1039);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (36,1012,3,'Company',1040);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (37,1013,1,'Account',1041);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (38,1013,2,'Location',1042);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (39,1013,3,'Company',1043);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (40,1014,1,'Account',1044);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (41,1014,2,'Location',1045);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (42,1014,3,'Company',1046);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (43,1015,1,'Account',1038);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (44,1015,2,'Location',1039);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (45,1015,3,'Company',1040);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (46,1016,1,'Account',1041);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (47,1016,2,'Location',1042);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (48,1016,3,'Company',1043);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (49,1017,1,'Account',1044);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (50,1017,2,'Location',1045);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (51,1017,3,'Company',1046);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (52,1018,1,'Account',1053);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (53,1018,2,'Location',1054);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (54,1018,3,'Company',1055);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (55,1019,1,'Account',1057);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (56,1019,2,'Location',1058);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (57,1019,3,'Company',1059);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (60,1020,3,'Company',1065);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (61,1021,1,'Account',1061);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (62,1021,2,'Location',1062);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (63,1021,3,'Company',1063);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (64,1022,1,'Account',1067);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (65,1022,2,'Location',1068);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (66,1022,3,'Company',1069);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (67,1023,1,'Account',1072);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (68,1023,2,'Location',1073);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (69,1023,3,'Company',1074);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (70,1024,1,'Account',1077);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (71,1024,2,'Location',1078);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (72,1024,3,'Company',1079);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (73,1025,1,'Account',1082);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (74,1025,2,'Location',1083);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (75,1025,3,'Company',1084);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (76,1026,1,'Account',1087);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (77,1026,2,'Location',1088);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (78,1026,3,'Company',1089);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (79,1027,1,'Account',1092);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (80,1027,2,'Location',1093);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (81,1027,3,'Company',1094);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (82,1028,1,'Account',1097);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (83,1028,2,'Location',1098);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (84,1028,3,'Company',1099);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (85,1029,1,'Account',1102);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (86,1029,2,'Location',1103);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (87,1029,3,'Company',1104);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (88,1030,1,'Account',1107);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (89,1030,2,'Location',1108);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (90,1030,3,'Company',1109);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (91,1031,1,'Account',1112);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (92,1031,2,'Location',1113);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (93,1031,3,'Company',1114);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (94,1034,1,'Account',1125);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (95,1034,3,'Company',1127);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (96,1036,1,'Account',1133);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (97,1036,3,'Company',1135);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (98,1035,1,'Account',1129);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (99,1035,3,'Company',1131);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (100,1037,1,'Account',1138);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (101,1037,2,'Location',1139);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (102,1037,3,'Company',1140);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (103,1039,1,'Account',1148);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (104,1039,2,'Location',1149);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (105,1039,3,'Company',1150);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (106,1033,1,'Account',1152);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (107,1033,2,'Location',1153);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (108,1033,3,'Company',1154);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (109,1032,1,'Account',1117);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (110,1032,2,'Location',1118);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (111,1032,3,'Company',1119);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (112,1040,1,'Account',1157);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (113,1040,2,'Location',1158);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (114,1040,3,'Company',1159);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (115,1041,1,'Account',1157);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (116,1041,2,'Location',1158);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (117,1041,3,'Company',1159);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (118,1038,1,'Account',1143);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (119,1038,2,'Location',1144);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (120,1038,3,'Company',1145);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (121,1042,1,'Account',1162);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (122,1042,2,'Location',1163);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (123,1042,3,'Company',1164);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (124,1043,1,'Account',1168);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (125,1043,3,'Company',1167);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (126,1045,1,'Account',1168);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (127,1045,3,'Company',1167);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (128,1044,1,'Account',1168);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (129,1044,3,'Company',1167);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (130,1046,1,'Account',1168);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (131,1046,3,'Company',1167);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (132,1049,1,'Account',1171);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (133,1049,3,'Company',1172);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (134,1050,1,'Account',1175);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (135,1050,3,'Company',1176);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (136,1051,1,'Account',1179);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (137,1051,3,'Company',1180);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (138,1052,1,'Account',1183);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (139,1052,3,'Company',1184);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (140,1047,1,'Account',1168);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (141,1047,3,'Company',1167);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (142,1048,1,'Account',1168);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (143,1048,3,'Company',1167);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (144,1053,1,'Account',1187);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (145,1053,3,'Company',1188);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (146,1054,1,'Account',1191);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (147,1054,3,'Company',1192);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (148,1055,1,'Account',1195);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (149,1055,2,'Location',1196);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (150,1055,3,'Company',1197);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (151,1056,1,'Account',1200);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (152,1056,2,'Location',1201);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (153,1056,3,'Company',1202);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (154,1057,1,'Account',1205);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (155,1057,2,'Location',1206);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (156,1057,3,'Company',1207);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (157,1058,1,'Account',1210);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (158,1059,1,'Account',1213);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (159,1060,3,'Company',1216);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (160,1061,1,'Account',1219);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (161,1061,2,'Location',1220);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (162,1061,3,'Company',1221);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (163,1060,1,'Account',1231);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (164,1060,2,'Location',1232);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (165,1058,2,'Location',1234);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (166,1058,3,'Company',1233);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (167,1059,2,'Location',1235);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (168,1059,3,'Company',1236);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (169,1020,2,'Location',1237);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (170,1020,1,'Account',1238);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (171,1067,1,'Account',1239);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (172,1067,2,'Location',1241);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (173,1067,3,'Company',1240);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (174,1070,1,'Account',1138);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (175,1070,2,'Location',1139);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (176,1070,3,'Company',1140);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (177,1068,1,'Account',1242);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (178,1068,3,'Company',1243);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (179,1008,1,'Account',1027);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (180,1008,2,'Location',1028);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (181,1008,3,'Company',1029);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (182,1062,1,'Account',1246);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (183,1062,2,'Location',1245);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (184,1062,3,'Company',1244);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (185,1063,1,'Account',1247);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (186,1063,2,'Location',1248);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (187,1063,3,'Company',1249);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (188,1064,1,'Account',1252);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (189,1064,2,'Location',1251);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (190,1064,3,'Company',1250);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (191,1066,1,'Account',1253);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (192,1066,2,'Location',1255);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (193,1066,3,'Company',1254);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (194,1069,1,'Account',1257);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (195,1069,2,'Location',1258);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (196,1069,3,'Company',1256);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (197,1071,1,'Account',1280);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (198,1071,2,'Location',1282);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (199,1071,3,'Company',1281);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (200,1072,1,'Account',1280);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (201,1072,2,'Location',1282);
	INSERT INTO PWRPLANT.PA_EXPLORE_DATASOURCE values (202,1072,3,'Company',1281);
	
    

    delete from PWRPLANT.PA_EXPLORE_GRIDFILTERS;
    

    -- Inserts for PA_EXPLORE_GRIDFILTERS
	INSERT INTO PWRPLANT.PA_EXPLORE_GRIDFILTERS values (1,2);
	INSERT INTO PWRPLANT.PA_EXPLORE_GRIDFILTERS values (1,7);
	INSERT INTO PWRPLANT.PA_EXPLORE_GRIDFILTERS values (1,8);
	INSERT INTO PWRPLANT.PA_EXPLORE_GRIDFILTERS values (1,13);
	INSERT INTO PWRPLANT.PA_EXPLORE_GRIDFILTERS values (2,9);
	INSERT INTO PWRPLANT.PA_EXPLORE_GRIDFILTERS values (2,12);
	INSERT INTO PWRPLANT.PA_EXPLORE_GRIDFILTERS values (3,3);
	INSERT INTO PWRPLANT.PA_EXPLORE_GRIDFILTERS values (4,4);
	
    

    delete from PWRPLANT.PA_CASE_STATUSES;
   

    -- Inserts for PA_CASE_STATUSES
	INSERT INTO PWRPLANT.PA_CASE_STATUSES values (1,'Submitted',1);
	INSERT INTO PWRPLANT.PA_CASE_STATUSES values (2,'In Progress',2);
	INSERT INTO PWRPLANT.PA_CASE_STATUSES values (3,'Completed',3);
	

    delete from PWRPLANT.PA_FILTER2;
    

    -- Inserts for PA_FILTER2
	INSERT INTO PWRPLANT.PA_FILTER2 values (1,'ActivityCode','ACTIVITY_CODE','ActivityCode','Activity Code','select distinct activity_code from PWRPLANT.v_pa_cpr_actsum order by 1','ACTIVITY_CODE','ACTIVITY_CODE','MULTISELECT','IN',0,1,'Misc',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (2,'BusinessSegment','BUS_SEGMENT','BusinessSegment','Business Segment','select distinct Bus_Segment from PWRPLANT.V_PA_ACCOUNTS t order by 1','BUS_SEGMENT','BUS_SEGMENT','MULTISELECT','IN',0,1,'Account',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (3,'Company','COMPANY','Company','Company','select distinct Description from PWRPLANT.V_PA_COMPANY order by 1','Description','Description','MULTISELECT','IN',0,1,'Company',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (4,'DeprGroup','DEPR_GROUP','DeprGroup','Depr Group','select distinct depr_group from PWRPLANT.v_pa_depr_group order by 1','DEPR_GROUP','DEPR_GROUP','TEXT','LIKE',0,1,'Depreciation',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (5,'DeprMethod','DEPR_METHOD','DeprMethod','Depr Method','select distinct depr_method from PWRPLANT.v_pa_cpr_actsum order by 1','DEPR_METHOD','DEPR_METHOD','TEXT','LIKE',0,1,'Depreciation',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (6,'FercActivity','FERC_ACTIVITY','FercActivity','FERC Activity','select distinct ferc_activity from PWRPLANT.v_pa_cpr_actsum order by 1','FERC_ACTIVITY','FERC_ACTIVITY','MULTISELECT','IN',0,1,'Misc',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (7,'SummaryAccount','FERC_PLT_ACCT','SummaryAccount','Summary Account','select distinct FERC_PLT_ACCT from PWRPLANT.V_PA_ACCOUNTS t order by 1','FERC_PLT_ACCT','FERC_PLT_ACCT','MULTISELECT','IN',0,1,'Account',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (8,'FunctionalClass','FUNC_CLASS','FunctionalClass','Functional Class','select distinct FUNC_CLASS from PWRPLANT.V_PA_ACCOUNTS t order by 1','FUNC_CLASS','FUNC_CLASS','MULTISELECT','IN',0,1,'Account',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (9,'MajorLocation','MAJOR_LOCATION','MajorLocation','Major Location','select distinct major_location from PWRPLANT.v_pa_locations order by 1','MAJOR_LOCATION','MAJOR_LOCATION','MULTISELECT','IN',0,1,'Location',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (10,'PostingYears','GL_POSTING_YEAR','PostingYears','Posting Years','select distinct gl_posting_year from PWRPLANT.v_pa_cpr_actsum where GL_POSTING_YEAR is not null order by 1','GL_POSTING_YEAR','GL_POSTING_YEAR','SLIDER','BETWEEN',0,0,'Time',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (11,'ProcessYears','PROCESS_YEAR','ProcessYears','Process Years','select distinct PROCESS_YEAR from PWRPLANT.V_PA_AVERAGE_AGES order by 1','PROCESS_YEAR','PROCESS_YEAR','SLIDER','BETWEEN',0,0,'Time',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (12,'State','STATE','State','State','select distinct STATE from PWRPLANT.V_PA_LOCATIONS order by 1','State','STATE','MULTISELECT','IN',0,1,'Location',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (13,'DetailAccount','UTIL_ACCT','DetailAccount','Detail Account','select distinct UTIL_ACCT from PWRPLANT.V_PA_ACCOUNTS t order by 1','UTIL_ACCT','UTIL_ACCT','MULTISELECT','IN',0,1,'Account',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (14,'VintageYear','VINTAGE_YEAR','VintageYear','Vintage Years','select distinct VINTAGE_YEAR from PWRPLANT.V_PA_VINTAGE_SPREADS t order by 1','VINTAGE_YEAR','VINTAGE_YEAR','SLIDER','BETWEEN',0,0,'Time',0,null,1,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (15,'VintageInterval','VINTAGE_INTERVAL','VintageInterval','Vintage Interval','select 1 Interval from dual union select 2 Interval from dual union select 3 Interval from dual union select 5 Interval from dual union select 10 Interval from dual union select 20 Interval from dual','Interval','Interval','DROPDOWNLIST','PARAM_REPLACE',1,0,'Time',1,'(VINTAGE_YEAR / [X] ) * [X]',1,'FLOOR(VINTAGE_YEAR/{VintageInterval})*{VintageInterval}','5');
	INSERT INTO PWRPLANT.PA_FILTER2 values (16,'Age','Age','Age','Age','select 1 from dual','Age','Age','MULTISELECT','IN',0,0,'Time',0,null,0,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (17,'YearsPastExpected','YEARS_PAST_EXPECTED','YearsPastExpected','Years Past Expected','select 1 from dual','YearsPastExpected','YearsPastExpected','MULTISELECT','IN',0,0,'Time',0,null,0,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (18,'YearsPastMax','YEARS_PAST_MAX','YearsPastMax','Years Past Max','select 1 from dual','YearsPastMax','YearsPastMax','MULTISELECT','IN',0,0,'Time',0,null,0,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (19,'LifeBand','LIFE_BAND','LifeBand','Life Band','select 1 from dual','LifeBand','LifeBand','MULTISELECT','IN',0,0,'Time',0,null,0,null,null);
	INSERT INTO PWRPLANT.PA_FILTER2 values (20,'PostingDate','POSTING_YRMO','PostingDate','Posting Date','select distinct posting_yrmo from PWRPLANT.V_PA_DEPR_BALANCES','POSTING_YRMO','POSTING_YRMO','MULTISELECT','IN',0,1,'Time',0,null,1,null,null);
	    

    delete from PWRPLANT.PA_FILTERGROUP;
    

    -- Inserts for PA_FILTERGROUP
	INSERT INTO PWRPLANT.PA_FILTERGROUP values (1,'AverageAge');
	INSERT INTO PWRPLANT.PA_FILTERGROUP values (2,'VintageSpread');
	INSERT INTO PWRPLANT.PA_FILTERGROUP values (3,'CprBalances');
	INSERT INTO PWRPLANT.PA_FILTERGROUP values (4,'CprActSummary');
	INSERT INTO PWRPLANT.PA_FILTERGROUP values (5,'CprActivity');
	INSERT INTO PWRPLANT.PA_FILTERGROUP values (6,'CprLedger');
	INSERT INTO PWRPLANT.PA_FILTERGROUP values (7,'DeprBalances');
	INSERT INTO PWRPLANT.PA_FILTERGROUP values (8,'DeprBalancesYr');
	

    delete from PWRPLANT.PA_FILTERGROUP_FILTERS;
    

    -- Inserts for PWRPLANT.PA_FILTERGROUP_FILTERS
   Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (1,3,2,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (2,3,3,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (3,3,4,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (4,3,7,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (5,3,8,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (6,3,9,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (7,3,11,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (8,3,12,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (9,3,13,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (10,3,14,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (11,2,2,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (12,2,3,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (13,2,4,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (14,2,7,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (15,2,8,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (16,2,9,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (17,2,12,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (18,2,13,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (19,2,14,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (20,2,15,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (21,6,2,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (22,6,3,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (23,6,4,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (25,6,7,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (26,6,8,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (27,6,9,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (28,6,12,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (29,6,13,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (30,6,14,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (31,6,15,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (32,7,2,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (33,7,3,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (34,7,4,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (35,7,7,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (36,7,8,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (37,7,10,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (38,7,13,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (39,8,2,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (40,8,3,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (41,8,4,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (42,8,7,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (43,8,8,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (44,8,10,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (45,8,13,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (46,5,2,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (47,5,3,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (48,5,4,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (50,5,7,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (51,5,8,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (52,5,9,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (53,5,10,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (54,5,12,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (55,5,13,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (66,1,3,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (67,1,2,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (68,1,8,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (69,1,7,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (70,1,13,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (71,1,4,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (72,1,12,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (73,1,9,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (74,1,11,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (75,4,3,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (76,4,2,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (77,4,8,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (78,4,7,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (79,4,13,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (80,4,12,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (81,4,9,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (82,4,4,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (83,4,10,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (84,3,15,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (85,7,5,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (86,8,5,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (91,4,14,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (92,4,15,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (93,2,16,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (94,2,17,null);
	Insert into PWRPLANT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (95,2,18,null);
	 

    delete from PWRPLANT.PA_CASE_PRIORITIES;
    

    -- Inserts for PA_CASE_PRIORITIES
	INSERT INTO PWRPLANT.PA_CASE_PRIORITIES values ('High',3,3);
	INSERT INTO PWRPLANT.PA_CASE_PRIORITIES values ('Low',1,1);
	INSERT INTO PWRPLANT.PA_CASE_PRIORITIES values ('Medium',2,2);
	

 

    delete from PWRPLANT.PA_HEATMAP_COLOR;
	

    
    
	
	end;
	/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2022, 0, 10, 4, 3, 1, 41290, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.1_maint_041290_anlyt_07_system_inserts.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;