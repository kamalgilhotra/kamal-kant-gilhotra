/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043403_depr_comments_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1     03/25/2015 Andrew Scott     activity 3 comments were not correct.
||                                        not merging in with the patch fix, just
||                                        fixing table doc for 2015.1
||============================================================================
*/

comment on column CPR_DEPR_CALC_STG.activity_3
  is 'The reserve activity.';
comment on column CPR_DEPR_CALC_STG_ARC.activity_3
  is 'The reserve activity.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2449, 0, 2015, 1, 0, 0, 43403, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043403_depr_comments_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;