/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051971_sys_get_pp_translation_fnc_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 07/12/2018 TechProdMgmt	Fix function to handle nonconsecutive id's 
||============================================================================
*/

create or replace function GET_PP_TRANSLATION(RULE_NAME in VARCHAR2, S_LABELS in VARCHAR2,S_VALUES in VARCHAR2) return VARCHAR2 is

  num_elements NUMBER;
  num_keys NUMBER;
  num_values NUMBER;
  a_rule_id NUMBER;
  s_rule_mapping VARCHAR2(4000);
  s_value  VARCHAR2(4000);
  s_key VARCHAR2(128);
  s_key_value VARCHAR2(128);
  b_match boolean;
  s_translation varchar2(256);
  type mapping_array is TABLE OF VARCHAR2(256);
  rule_array mapping_array;
  data_array mapping_array;

  CURSOR c_mapping(ruleid IN NUMBER)
  IS
  SELECT map_id, input_value, output_value
  FROM pp_translate_mapping
  WHERE rule_id = ruleid
  ORDER BY map_id;


begin
  s_translation := 'Translation not found for provided inputs';

  data_array := mapping_array();

  BEGIN
    SELECT rule_id INTO a_rule_id
    FROM pp_translate_rule
    WHERE UPPER(description) = TRIM(UPPER(RULE_NAME));
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      s_translation := 'Translation Rule not found: ' || RULE_Name;
      RETURN (s_translation);
  END;

  num_keys := regexp_count(S_LABELS,',') + 1;
  num_values := regexp_count(S_VALUES,',') + 1;
  if num_keys <> num_values then
    RETURN ('Number of labels and values must match.');
  end if;


  for i in 1..num_keys loop
    s_key := TRIM(regexp_substr(S_LABELS,'[^,]+',1,i));
    s_value := TRIM(regexp_substr(S_VALUES,'[^,]+',1,i));
    s_key_value := UPPER(s_key) || ':' || UPPER(s_value);
    data_array.extend;
    data_array(data_array.count):= s_key_value;
   end loop;

   FOR mapping_rec in c_mapping(a_rule_id) LOOP
    rule_array := mapping_array();

    s_rule_mapping := mapping_rec.input_value;

    /*strip off {}, " */
    s_rule_mapping := replace(s_rule_mapping,'{');
    s_rule_mapping := replace(s_rule_mapping,'}');
    s_rule_mapping := replace(s_rule_mapping,'"');

    num_elements := regexp_count(s_rule_mapping,',') + 1;

    /* loop through the elements in this mapping and see if it is a match */
    for j in 1..num_elements loop
      s_value := regexp_substr(s_rule_mapping,'[^,]+',1,j);
      rule_array.extend;
      rule_array(rule_array.count):= UPPER(s_value);
    end loop;

    b_match:= data_array IN (rule_array);
    /* if it matches, get the translation and exit the loop */
    if b_match then
      s_translation := mapping_rec.output_value;
      exit;
    end if;
  end LOOP;

  return(s_translation);

end GET_PP_TRANSLATION;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8007, 0, 2017, 4, 0, 0, 51971, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051971_sys_get_pp_translation_fnc_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;