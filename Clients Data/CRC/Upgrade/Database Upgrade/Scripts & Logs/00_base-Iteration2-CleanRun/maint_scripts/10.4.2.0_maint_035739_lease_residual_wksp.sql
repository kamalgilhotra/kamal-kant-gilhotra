/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035739_lease_residual_wksp.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/10/2014 Ryan Oliveria  New actual residual workspace
||============================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'control_residual', 'Residual Processing', 'uo_ls_mecntr_wksp_residual', null);

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'control_residual', 2, 4, 'Residual Processing', null, 'menu_wksp_control', 'control_residual', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (952, 0, 10, 4, 2, 0, 35739, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035739_lease_residual_wksp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;