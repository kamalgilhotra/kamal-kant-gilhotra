/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050365_lessee_01_update_fx_views_filter_currency_type_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.2.0.0 03/05/2018 Andrew Hill   	  Filter FX Views on Currency Type
||============================================================================
*/

CREATE OR REPLACE VIEW V_LS_INVOICE_FX_VW AS
  WITH
cur AS (
  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
    contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
  FROM currency contract_cur
  UNION
  SELECT 2, company_cur.currency_id,
    company_cur.currency_display_symbol, company_cur.iso_code, 0
  FROM currency company_cur
)
SELECT
 invoice_id,
 vendor_id,
 invoice_status,
 invoice_number,
 inv.ls_asset_Id,
 inv.ilr_id,
 inv.ilr_Id tax_local_id,
 gl_posting_Mo_yr,
 invoice_amount * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_amount,
 invoice_interest * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_interest,
 invoice_executory * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_executory,
 invoice_contingent * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_contingent,
 lease.lease_id,
 lease.lease_number,
 inv.company_id,
 cur.ls_cur_type ls_cur_type,
 cr.exchange_date,
 lease.contract_currency_id,
 cur.currency_id display_currency_id,
 cs.currency_id company_currency_id,
 decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) rate,
 cur.iso_code,
 cur.currency_display_symbol
 FROM ls_invoice inv
 INNER JOIN currency_schema cs
    ON inv.company_id = cs.company_id
 INNER JOIN ls_lease lease
    ON inv.lease_id = lease.lease_id
 INNER JOIN cur
    ON (cur.ls_cur_type = 1 AND
       cur.currency_id = lease.contract_currency_id)
    OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
 LEFT OUTER JOIN ls_lease_calculated_date_rates cr
    ON lease.contract_currency_id = cr.contract_currency_id
   AND cur.currency_id = cr.company_currency_id
   AND inv.company_id = cr.company_id
   AND inv.gl_posting_mo_yr = cr.accounting_month
 WHERE Nvl(cr.exchange_rate_type_id, 1) = 1
 AND cs.currency_type_id = 1;
/
 
 
  CREATE OR REPLACE VIEW V_LS_INVOICE_LINE_FX as
  WITH cur
       AS (SELECT 1                                    ls_cur_type,
                  contract_cur.currency_id             AS currency_id,
                  contract_cur.currency_display_symbol currency_display_symbol,
                  contract_cur.iso_code                iso_code,
                  1                                    historic_rate
           FROM   currency contract_cur
           UNION
           SELECT 2,
                  company_cur.currency_id,
                  company_cur.currency_display_symbol,
                  company_cur.iso_code,
                  0
           FROM   currency company_cur)
  SELECT lil.invoice_id,
         lil.invoice_line_number,
         lil.payment_type_id,
         lil.ls_asset_id,
         lil.gl_posting_mo_yr,
         lil.amount * Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                                          1)  amount,
         lil.description,
         lil.notes,
         lease.contract_currency_id,
         cs.currency_id                       company_currency_id,
         cur.ls_cur_type                      AS ls_cur_type,
         Nvl(cr.exchange_date, '01-Jan-1900') exchange_date,
         Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                             1)               rate,
         cur.iso_code,
         cur.currency_display_symbol
  FROM   ls_invoice_line lil
         inner join ls_invoice li
                 ON ( li.invoice_id = lil.invoice_id )
         inner join ls_lease lease
                 ON li.lease_id = lease.lease_id
         inner join currency_schema cs
                 ON li.company_id = cs.company_id
         inner join cur
                 ON ( ( cur.ls_cur_type = 1
                        AND cur.currency_id = lease.contract_currency_id )
                       OR ( cur.ls_cur_type = 2
                            AND cur.currency_id = cs.currency_id ) )
         left outer join ls_lease_calculated_date_rates cr
                      ON ( cr.company_id = li.company_id
                           AND cr.contract_currency_id =
                               lease.contract_currency_id
                           AND cr.company_currency_id = cs.currency_id
                           AND cr.accounting_month = lil.gl_posting_mo_yr )
  WHERE  Nvl(cr.exchange_rate_type_id, 1) = 1
  AND cs.currency_type_id = 1;
/
  
  CREATE OR REPLACE VIEW V_LS_MONTHLY_ACCRUAL_STG_FX AS
WITH cur
     AS (SELECT 1                                    ls_cur_type,
                contract_cur.currency_id             AS currency_id,
                contract_cur.currency_display_symbol currency_display_symbol,
                contract_cur.iso_code                iso_code
         FROM   currency contract_cur
         UNION
         SELECT 2,
                company_cur.currency_id,
                company_cur.currency_display_symbol,
                company_cur.iso_code
         FROM   currency company_cur)
SELECT lmas.accrual_id,
       lmas.accrual_type_id,
       lmas.ls_asset_id,
       lmas.amount * Decode(ls_cur_type, 2, rate,
                                         1) amount,
       lmas.gl_posting_mo_yr,
       lmas.set_of_books_id,
       la.contract_currency_id,
       cs.currency_id                       company_currency_id,
       cur.ls_cur_type                      AS ls_cur_type,
       cr.exchange_date,
       Decode(ls_cur_type, 2, rate,
                           1)               rate,
       cur.iso_code,
       cur.currency_display_symbol
FROM   ls_monthly_accrual_stg lmas
       inner join ls_asset la
               ON lmas.ls_asset_id = la.ls_asset_id
       inner join currency_schema cs
               ON la.company_id = cs.company_id
       inner join cur
               ON ( ( cur.ls_cur_type = 1
                      AND cur.currency_id = la.contract_currency_id )
                     OR ( cur.ls_cur_type = 2
                          AND cur.currency_id = cs.currency_id ) )
       inner join ls_lease_calculated_date_rates cr
               ON ( cr.company_id = la.company_id
                    AND cr.contract_currency_id = la.contract_currency_id
                    AND cr.company_currency_id = cs.currency_id
                    AND cr.accounting_month = lmas.gl_posting_mo_yr )
WHERE  cr.exchange_rate_type_id = 1
AND cs.currency_type_id = 1;
/

CREATE OR REPLACE VIEW V_LS_MONTHLY_DEPR_STG_FX AS
WITH cur
     AS (SELECT 1                                    ls_cur_type,
                contract_cur.currency_id             AS currency_id,
                contract_cur.currency_display_symbol currency_display_symbol,
                contract_cur.iso_code                iso_code
         FROM   currency contract_cur
         UNION
         SELECT 2,
                company_cur.currency_id,
                company_cur.currency_display_symbol,
                company_cur.iso_code
         FROM   currency company_cur)
SELECT lmds.ls_asset_id,
       lmds.depreciation_expense * Decode(ls_cur_type, 2, rate,
                                                       1) depreciation_expense
       ,
       lmds.gl_posting_mo_yr,
       lmds.set_of_books_id,
       la.contract_currency_id,
       cs.currency_id                                     company_currency_id,
       cur.ls_cur_type                                    AS ls_cur_type,
       cr.exchange_date,
       Decode(ls_cur_type, 2, rate,
                           1)                             rate,
       cur.iso_code,
       cur.currency_display_symbol
FROM   ls_monthly_depr_stg lmds
       inner join ls_asset la
               ON lmds.ls_asset_id = la.ls_asset_id
       inner join currency_schema cs
               ON la.company_id = cs.company_id
       inner join cur
               ON ( ( cur.ls_cur_type = 1
                      AND cur.currency_id = la.contract_currency_id )
                     OR ( cur.ls_cur_type = 2
                          AND cur.currency_id = cs.currency_id ) )
       inner join ls_lease_calculated_date_rates cr
               ON ( cr.company_id = la.company_id
                    AND cr.contract_currency_id = la.contract_currency_id
                    AND cr.company_currency_id = cs.currency_id
                    AND cr.accounting_month = lmds.gl_posting_mo_yr )
WHERE  cr.exchange_rate_type_id = 1
AND cs.currency_type_id = 1;
/

CREATE OR REPLACE VIEW V_LS_PAYMENT_HDR_FX AS
WITH cur
     AS (SELECT 1                                    ls_cur_type,
                contract_cur.currency_id             AS currency_id,
                contract_cur.currency_display_symbol currency_display_symbol,
                contract_cur.iso_code                iso_code,
                1                                    historic_rate
         FROM   currency contract_cur
         UNION
         SELECT 2,
                company_cur.currency_id,
                company_cur.currency_display_symbol,
                company_cur.iso_code,
                0
         FROM   currency company_cur)
SELECT lph.payment_id,
       lph.lease_id,
       lph.vendor_id,
       lph.company_id,
       lph.amount                           original_amount,
       lph.amount * Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                                        1)  amount,
       lph.description,
       lph.gl_posting_mo_yr,
       lph.payment_status_id,
       lph.ap_status_id,
       lph.ls_asset_id,
       lph.ilr_id,
       lease.contract_currency_id,
       cs.currency_id                       company_currency_id,
       cur.ls_cur_type                      AS ls_cur_type,
       Nvl(cr.exchange_date, '01-Jan-1900') exchange_date,
       Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                           1)               rate,
       cur.iso_code,
       cur.currency_display_symbol
FROM   ls_payment_hdr lph
       inner join ls_lease lease
               ON lph.lease_id = lease.lease_id
       inner join currency_schema cs
               ON lph.company_id = cs.company_id
       inner join cur
               ON ( ( cur.ls_cur_type = 1
                      AND cur.currency_id = lease.contract_currency_id )
                     OR ( cur.ls_cur_type = 2
                          AND cur.currency_id = cs.currency_id ) )
       left outer join ls_lease_calculated_date_rates cr
                    ON ( cr.company_id = lph.company_id
                         AND cr.contract_currency_id =
                             lease.contract_currency_id
                         AND cr.company_currency_id = cs.currency_id
                         AND cr.accounting_month = lph.gl_posting_mo_yr )
WHERE  Nvl(cr.exchange_rate_type_id, 1) = 1
AND cs.currency_type_id = 1;
/

CREATE OR REPLACE VIEW V_LS_PAYMENT_LINE_FX AS
WITH cur
     AS (SELECT 1                                    ls_cur_type,
                contract_cur.currency_id             AS currency_id,
                contract_cur.currency_display_symbol currency_display_symbol,
                contract_cur.iso_code                iso_code,
                1                                    historic_rate
         FROM   currency contract_cur
         UNION
         SELECT 2,
                company_cur.currency_id,
                company_cur.currency_display_symbol,
                company_cur.iso_code,
                0
         FROM   currency company_cur)
SELECT lpl.payment_id,
       lpl.payment_line_number,
       lpl.payment_type_id,
       lpl.ls_asset_id,
       lpl.amount * Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                                        1)                      amount,
       lpl.gl_posting_mo_yr,
       lpl.description,
       lpl.set_of_books_id,
       Round(lpl.adjustment_amount * Decode(ls_cur_type, 2,
                                     Nvl(rate, historic_rate),
                                                         1), 2)
       adjustment_amount,
       lease.contract_currency_id,
       cs.currency_id
       company_currency_id,
       cur.ls_cur_type                                          AS ls_cur_type
       ,
       Nvl(cr.exchange_date, '01-Jan-1900')
       exchange_date,
       Decode(ls_cur_type, 2, Nvl(rate, historic_rate),
                           1)                                   rate,
       cur.iso_code,
       cur.currency_display_symbol
FROM   ls_payment_line lpl
       inner join ls_payment_hdr lph
               ON lph.payment_id = lpl.payment_id
       inner join ls_lease lease
               ON lph.lease_id = lease.lease_id
       inner join currency_schema cs
               ON lph.company_id = cs.company_id
       inner join cur
               ON ( ( cur.ls_cur_type = 1
                      AND cur.currency_id = lease.contract_currency_id )
                     OR ( cur.ls_cur_type = 2
                          AND cur.currency_id = cs.currency_id ) )
       left outer join ls_lease_calculated_date_rates cr
                    ON ( cr.company_id = lph.company_id
                         AND cr.contract_currency_id =
                             lease.contract_currency_id
                         AND cr.company_currency_id = cs.currency_id
                         AND cr.accounting_month = lph.gl_posting_mo_yr )
WHERE  Nvl(cr.exchange_rate_type_id, 1) = 1
AND cs.currency_type_id = 1;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4162, 0, 2017, 2, 0, 0, 50365, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.2.0.0_maint_050365_lessee_01_update_fx_views_filter_currency_type_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;