/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045186_taxrpr_esc_rets_debug_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   11/05/2015 A Scott        Escalate Retirement Dollars for pretests
||                                    =>add column to debug tables (related to 44611)
||============================================================================
*/

alter table REPAIR_WORK_ORDER_DEBUG add (HW_RETS_BASE_YEAR number(22,0));
alter table REPAIR_WORK_ORDER_ORIG_DEBUG add (HW_RETS_BASE_YEAR number(22,0));

comment on column REPAIR_WORK_ORDER_DEBUG.HW_RETS_BASE_YEAR IS 'Retirement Year for a given tax unit of property/repair location combination. The retirement year is calculated as a weighted average of the retirement vintages found in cpr_ledger.eng_in_service_year.';
comment on column REPAIR_WORK_ORDER_ORIG_DEBUG.HW_RETS_BASE_YEAR IS 'Retirement Year for a given tax unit of property/repair location combination. The retirement year is calculated as a weighted average of the retirement vintages found in cpr_ledger.eng_in_service_year.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2965, 0, 2015, 2, 0, 0, 45186, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045186_taxrpr_esc_rets_debug_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
