/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036257_projects_just_default_rev.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/05/2014 Chris Mardis   Point Release
||============================================================================
*/

create or replace view BUDGET_VERSION_VIEW as
select COMPANY_ID,
       WORKING_VERSION,
       FORECAST_VERSION,
       PREVIOUS_FORECAST_VERSION,
       BUDGET_VERSION,
       NVL(REVISED_BUDGET, BUDGET_VERSION) REVISED_BUDGET,
       CURRENT_YEAR,
       ACTUALS_MONTH,
       CURRENT_VERSION
  from (select CS.COMPANY_ID COMPANY_ID,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) WORKING_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 2
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 2
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) FORECAST_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 3
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 3
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) PREVIOUS_FORECAST_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 4
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 4
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) BUDGET_VERSION,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 5
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 5
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) REVISED_BUDGET,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.CURRENT_YEAR)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.CURRENT_YEAR)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) CURRENT_YEAR,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.ACTUALS_MONTH)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.ACTUALS_MONTH)
                         from BUDGET_VERSION BV, BUDGET_VERSION_TYPE BVT
                        where BV.BUDGET_VERSION_TYPE_ID = BVT.BUDGET_VERSION_TYPE_ID
                          and BVT.BUDGET_VERSION_STATUS_ID = 1
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) ACTUALS_MONTH,
               DECODE(UPPER(SC.CONTROL_VALUE),
                      'SUMMARY',
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV
                        where BV.CURRENT_VERSION = 1
                          and BV.COMPANY_ID in
                              (select S.COMPANY_ID
                                 from COMPANY_SETUP S
                                where S.COMPANY_SUMMARY_ID = CS.COMPANY_SUMMARY_ID)),
                      (select max(BV.BUDGET_VERSION_ID)
                         from BUDGET_VERSION BV
                        where BV.CURRENT_VERSION = 1
                          and DECODE(UPPER(SC.CONTROL_VALUE), 'YES', BV.COMPANY_ID, SC.COMPANY_ID) =
                              SC.COMPANY_ID)) CURRENT_VERSION
          from COMPANY_SETUP CS, PP_SYSTEM_CONTROL_COMPANIES SC
         where SC.COMPANY_ID = CS.COMPANY_ID
           and UPPER(SC.CONTROL_NAME) = 'BV CURRENT VERSION BY COMPANY');


create or replace view WO_EST_INITIAL_REVISION as
select woc.work_order_id,
   case when woc.funding_wo_indicator = 1 then
      nvl(
         (  select max(bvfp.revision)
            from budget_version_fund_proj bvfp, budget_version_view bvv
            where woc.company_id = bvv.company_id
            and woc.work_order_id = bvfp.work_order_id
            and bvfp.active = 1
            and bvfp.budget_version_id = bvv.working_version
         ),
         nvl(
            (  select max(bvfp.revision)
               from budget_version_fund_proj bvfp, budget_version_view bvv
               where woc.company_id = bvv.company_id
               and woc.work_order_id = bvfp.work_order_id
               and bvfp.active = 1
               and bvfp.budget_version_id = bvv.current_version
            ),
            nvl(
               (  select max(woa.revision)
                  from work_order_approval woa
                  where woa.work_order_id = woc.work_order_id
               ),
               0)))
   else
      (  select max(woa.revision)
         from work_order_approval woa
         where woa.work_order_id = woc.work_order_id
      )
   end
   revision
from work_order_control woc;


create or replace view BI_EST_INITIAL_VERSION as
select b.budget_id,
   nvl(
      (  select max(ba.budget_version_id)
         from budget_amounts ba, budget_version_view bvv
         where b.company_id = bvv.company_id
         and b.budget_id = ba.budget_id
         and ba.budget_version_id = bvv.working_version
      ),
      nvl(
         (  select max(ba.budget_version_id)
            from budget_amounts ba, budget_version_view bvv
            where b.company_id = bvv.company_id
            and b.budget_id = ba.budget_id
            and ba.budget_version_id = bvv.current_version
         ),
         nvl(
            (  select max(ba.budget_version_id)
               from budget_amounts ba
               where ba.budget_id = b.budget_id
            ),
            0)))
   budget_version_id
from budget b;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (942, 0, 10, 4, 2, 0, 36257, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036257_projects_just_default_rev.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;