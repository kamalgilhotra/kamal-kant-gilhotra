/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_005806_01_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/10/2011 Blake Andrews  Point Release
||============================================================================
*/

alter table TAX_ACCRUAL_M_TYPE
   drop column POST_APPORT_IND;

alter table TAX_ACCRUAL_M_TYPE
   add M_TYPE_TREATMENT_ID number(22) default 0;

update TAX_ACCRUAL_M_TYPE set M_TYPE_TREATMENT_ID = 1 where M_TYPE_ID = 1;

update TAX_ACCRUAL_M_TYPE set M_TYPE_TREATMENT_ID = 2 where M_TYPE_ID = 10;

update TAX_ACCRUAL_M_TYPE set M_TYPE_TREATMENT_ID = 3 where M_TYPE_ID >= 200;

update TAX_ACCRUAL_M_TYPE set M_TYPE_TREATMENT_ID = 4 where M_TYPE_ID = 80;

update TAX_ACCRUAL_M_TYPE set M_TYPE_TREATMENT_ID = 5 where M_TYPE_ID = 20;

update TAX_ACCRUAL_REP_SPECIAL_NOTE set REP_ROLLUP_GROUP_ID = 12 where SPECIAL_NOTE_ID = 71;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (48, 0, 10, 3, 3, 0, 5806, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_005806_01_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
