/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008550_sys_PP_MISC_PKG.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.3.4.0 02/16/2012 Joseph King    Point Release
||============================================================================
*/

-- ** The following grant needs to be run as the SYS user.
-- ** This has been added to V10_sys_grants.sql
--grant select on SYS.GV_$SESSION to PWRPLANT;

create or replace package PP_MISC_PKG as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PP_MISC_PKG
   || Description: Miscellaneous functions and procedures for PowerPlant application.
   ||============================================================================
   || Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- -----------------------------------------
   || 10.      03/23/2011 David Liss     Created Package
   || 10.3.4.0 02/16/2012 Joseph King    Added PP_COUNT_AUDSID function
   ||============================================================================
   */
   function DYNAMIC_SELECT(SQLS varchar2) return varchar2;
   function PP_COUNT_AUDSID(V_AUDSID in number) return number;
end PP_MISC_PKG;
/

create or replace package body PP_MISC_PKG as
   function DYNAMIC_SELECT(SQLS varchar2) return varchar2 is
      /*
      ||============================================================================
      || Application: PowerPlant
      || Object Name: DYNAMIC_SELECT
      || Description: Executes a select statement and only succeeds if returns exactly one record.
      || Used in cases when creating a datastore in powerscript to return one record could
      || affect performance. Make sure you check sqlca.sqlcode when calling this function.
      ||============================================================================
      || Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
      ||============================================================================
      || Version Date       Revised By     Reason for Change
      || ------- ---------- -------------- -----------------------------------------
      || 1.0     03/23/2011 David Liss   Create
      || 1.1     01/11/2012 David Liss   Need to Raise Application Error to catch all errors
      ||============================================================================
      */

      RECORD_VAL varchar2(4000);

   begin
      -- If this doesn't return 1 record this will fail.
      execute immediate SQLS
         into RECORD_VAL;

      return RECORD_VAL;

   exception
      when others then
         -- this catches all SQL errors, including no_data_found
         RAISE_APPLICATION_ERROR(-20001, 'ERROR executing SQL Block: ', true);

   end;

   function PP_COUNT_AUDSID(V_AUDSID in number) return number is
      NUMVAL number;

      /*
      ||============================================================================
      || Application: PowerPlant
      || Object Name: PP_COUNT_AUDSID
      || Description: Return numerical count of sessions matching v_audsid application
      || uses this function to decide on resetting interface if == 0, user is able to
      || reset interface.
      ||============================================================================
      || Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
      ||============================================================================
      || Version  Date       Revised By     Reason for Change
      || -------- ---------- -------------- ----------------------------------------
      || 10.3.4.0 02/16/2012 Joseph King    Needed to access GV$SESSION for RAC databases
      ||                                    to search all database instances.  The function
      ||                                    is used to keep from having to give select on
      ||                                    GV$SESSION to PWRPLANT_ROLE_DEV.
      ||============================================================================
      */

   begin
      if V_AUDSID < 1 then
         return 0;
      end if;

      select count(*) into NUMVAL from GV$SESSION where AUDSID = V_AUDSID;

      return NUMVAL;

   exception
      when others then
         return -1;
   end;

end PP_MISC_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (95, 0, 10, 3, 4, 0, 8550, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_008550_sys_PP_MISC_PKG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
