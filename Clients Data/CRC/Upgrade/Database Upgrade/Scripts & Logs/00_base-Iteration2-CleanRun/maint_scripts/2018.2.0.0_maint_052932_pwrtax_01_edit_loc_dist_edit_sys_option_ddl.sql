/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052932_pwrtax_01_edit_loc_dist_edit_sys_option_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 02/05/2019 Shane "C" Ward   New System Option for controlling whether Distinction and Location are editable on Tax Depr Interface Additions
||============================================================================
*/

INSERT INTO ppbase_system_options
  (system_option_id, long_description, system_only, pp_default_value,
   option_value, is_base_option, allow_company_override)
VALUES
  ('Allow Tax Dist/Loc Remap',
   'Whether or not to allow users to re-map additions interfaced in to different Tax Locations or Distinctions',
   0, 'No', null, 1, 0);

INSERT INTO ppbase_system_options_module (system_option_id, MODULE)
VALUES ('Allow Tax Dist/Loc Remap', 'powertax');

INSERT INTO ppbase_system_options_values (system_option_id, option_value)
VALUES ('Allow Tax Dist/Loc Remap', 'Yes');

INSERT INTO ppbase_system_options_values (system_option_id, option_value)
VALUES ('Allow Tax Dist/Loc Remap', 'No');

INSERT INTO ppbase_system_options_wksp (system_option_id, workspace_identifier)
VALUES ('Allow Tax Dist/Loc Remap', 'depr_input_book_additions');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15682, 0, 2018, 2, 0, 0, 52932, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052932_pwrtax_01_edit_loc_dist_edit_sys_option_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;