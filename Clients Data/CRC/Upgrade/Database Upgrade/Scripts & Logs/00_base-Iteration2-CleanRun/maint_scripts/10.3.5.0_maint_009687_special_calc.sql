/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009687_special_calc.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   03/30/2012 Brandon Beck   Point Release
||============================================================================
*/

alter table REIMB_BILL_DOLLAR_TYPE
   add SPECIAL_CALC1_BASE number(1,0) default 0;

alter table REIMB_REFUND_TYPE
   add SPECIAL_CALC_ID number(22,0);

create table REIMB_REFUND_SPECIAL_CALC
(
 SPECIAL_CALC_ID      number(22,0) not null,
 SPECIAL_CALC_NAME    varchar2(254),
 SPECIAL_CALC_FORMULA varchar2(4000),
 SPECIAL_CALC_RESULT  varchar2(35),
 USER_ID              varchar2(18),
 TIME_STAMP           date
);

alter table REIMB_REFUND_SPECIAL_CALC
   add constraint REF_SPEC_CALC_PK
       primary key (SPECIAL_CALC_ID)
       using index tablespace PWRPLANT_IDX;

create table REIMB_REFUND_SPECIAL_CALC_ARGS
(
 ID              number(22,0) not null,
 SPECIAL_CALC_ID number(22,0),
 ARG_NAME        varchar2(35),
 ARG_RESULT      varchar2(4000),
 USER_ID         varchar2(18),
 TIME_STAMP      date
);

alter table REIMB_REFUND_SPECIAL_CALC_ARGS
   add constraint REF_SPEC_ARG_PK
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (126, 0, 10, 3, 5, 0, 9687, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009687_special_calc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
