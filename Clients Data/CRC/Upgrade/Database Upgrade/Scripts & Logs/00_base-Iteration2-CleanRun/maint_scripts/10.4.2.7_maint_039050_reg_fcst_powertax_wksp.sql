/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_039050_reg_fcst_powertax_wksp.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.2.7 07/17/2014 Ryan Oliveria       Forecast Powertax Integration
||========================================================================================
*/

--Menu Item
update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 8
 where MODULE = 'REG'
   and MENU_IDENTIFIER = 'FORE_VERSION';

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_fcst_tax_int', 'PowerTax', 'uo_reg_fcst_tax_int', 'PowerTax Integration');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'FORE_POWERTAX', 3, 7, 'PowerTax', 'PowerTax Integration', 'FORE_INTEGRATION', 'uo_reg_fcst_tax_int', 1);


--Add nullable column
alter table REG_TAX_FAMILY_MEMBER_MAP add FORECAST_VERSION_ID number(22,0);
alter table REG_TAX_COMP_MEMBER_MAP   add FORECAST_VERSION_ID number(22,0);
alter table REG_TA_SPLIT_RULES        add FORECAST_VERSION_ID number(22,0);
alter table REG_TA_SPLIT_PROV_AMOUNT  add FORECAST_VERSION_ID number(22,0);
alter table REG_TAX_STAGE             add FORECAST_VERSION_ID number(22,0);


--Foreign Keys
alter table REG_TAX_FAMILY_MEMBER_MAP
   add constraint R_REG_TAX_FAMILY_MEMBER_MAP6
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_TAX_COMP_MEMBER_MAP
   add constraint R_REG_TAX_COMP_MEMBER_MAP4
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_TA_SPLIT_RULES
   add constraint R_REG_TA_SPLIT_RULES4
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_TA_SPLIT_PROV_AMOUNT
   add constraint R_REG_TA_SPLIT_PROV_AMOUNT4
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_TAX_STAGE
   add constraint R_REG_TAX_STAGE8
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);


--Default to zero
update REG_TAX_FAMILY_MEMBER_MAP set FORECAST_VERSION_ID = 0;
update REG_TAX_COMP_MEMBER_MAP   set FORECAST_VERSION_ID = 0;
update REG_TA_SPLIT_RULES        set FORECAST_VERSION_ID = 0;
update REG_TA_SPLIT_PROV_AMOUNT  set FORECAST_VERSION_ID = 0;
update REG_TAX_STAGE             set FORECAST_VERSION_ID = 0;


--Make column not nullable
alter table REG_TAX_FAMILY_MEMBER_MAP modify FORECAST_VERSION_ID not null;
alter table REG_TAX_COMP_MEMBER_MAP   modify FORECAST_VERSION_ID not null;
alter table REG_TA_SPLIT_RULES        modify FORECAST_VERSION_ID not null;
alter table REG_TA_SPLIT_PROV_AMOUNT  modify FORECAST_VERSION_ID not null;
alter table REG_TAX_STAGE             modify FORECAST_VERSION_ID not null;


--Primary Keys
alter table REG_TAX_COMP_MEMBER_MAP drop primary key drop index;

alter table REG_TAX_COMP_MEMBER_MAP
   add constraint PK_REG_TAX_COMP_MEMBER_MAP
       primary key (HISTORIC_VERSION_ID, FORECAST_VERSION_ID, REG_COMPONENT_ID, REG_FAMILY_ID, REG_MEMBER_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_TA_SPLIT_PROV_AMOUNT drop primary key drop index;

alter table REG_TA_SPLIT_PROV_AMOUNT
   add constraint PK_REG_TA_SPLIT_PROV_AMOUNT
       primary key (HISTORIC_VERSION_ID, FORECAST_VERSION_ID, REG_ACCT_ID, GL_MONTH)
       using index tablespace PWRPLANT_IDX;


--Comments
comment on column REG_TAX_FAMILY_MEMBER_MAP.FORECAST_VERSION_ID is 'System assigned identifier of a forecast version';
comment on column REG_TAX_COMP_MEMBER_MAP.FORECAST_VERSION_ID is 'System assigned identifier of a forecast version';
comment on column REG_TA_SPLIT_RULES.FORECAST_VERSION_ID is 'System assigned identifier of a forecast version';
comment on column REG_TA_SPLIT_PROV_AMOUNT.FORECAST_VERSION_ID is 'System assigned identifier of a forecast version';
comment on column REG_TAX_STAGE.FORECAST_VERSION_ID is 'System assigned identifier of a forecast version';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1288, 0, 10, 4, 2, 7, 39050, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039050_reg_fcst_powertax_wksp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;