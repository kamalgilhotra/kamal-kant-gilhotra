/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047461_lease_add_historical_lookback_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- --------------    ----------------------------------------
|| 2017.1.0.0 05/01/2017 Johnny Sisouphanh Add Historic Lookback Payment Calendar
|| 2017.1.0.0 05/04/2017 Johnny Sisouphanh Add Historic Component Values Table
||=================================================================================
*/

--create the ls_formula_component_cal_hist
--this tables holds the calendar values for a historical component
create table ls_formula_component_cal_hist (
  formula_component_id  number(22) not null,
  calc_month_number     number(2)  not null,
  timing                number(2)  not null,  
  payment_month_number  number(2)  not null,
  user_id				varchar2(18),
  time_stamp			date
);

alter table ls_formula_component_cal_hist
  add constraint ls_formula_comp_cal_hist_pk
  primary key (formula_component_id, calc_month_number)
  using index tablespace pwrplant_idx;

alter table ls_formula_component_cal_hist 
  add constraint ls_formula_comp_cal_hist_fk
  foreign key (formula_component_id)
  references ls_formula_component (formula_component_id);
  
comment on table ls_formula_component_cal_hist is '(F) [06] The LS Formula Component Calendar Historical Lookback table associates a calculated month to each payment month for a given historical component.';
comment on column ls_formula_component_cal_hist.formula_component_id is 'A reference to the formula component for which the load month and payment month will be stored.';
comment on column ls_formula_component_cal_hist.calc_month_number is 'The month number to store values for the load month.';
comment on column ls_formula_component_cal_hist.timing is 'The timing of the calculated month number. 1 - Current Year, 2 - Previous Year, 3 - Payment Month.'; 
comment on column ls_formula_component_cal_hist.payment_month_number is 'The month number to store values for the payment month'; 
comment on column ls_formula_component_cal_hist.user_id    is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_formula_component_cal_hist.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--create the ls_historical_component_values table
--this table holds the variable payment for the Historical Lookback Component 
create table ls_historical_component_values (
  formula_component_id   number(22) not null,
  variable_payment_id    number(22) not null,
  user_id				 varchar2(18),
  time_stamp			 date
);

alter table ls_historical_component_values 
  add constraint ls_historical_comp_valuespk
  primary key (formula_component_id, variable_payment_id)
  using index tablespace pwrplant_idx;
  
alter table ls_historical_component_values
  add constraint ls_historical_comp_valuesfk
  foreign key (formula_component_id)
  references ls_formula_component (formula_component_id);
  
comment on table ls_historical_component_values is '(S) [06] The LS Historic Component Values table contains relates a Variable Payment ID to the Historical Component.';
comment on column ls_historical_component_values.formula_component_id is 'A reference to the formula component for which this Historic Lookback is being entered';
comment on column ls_historical_component_values.variable_payment_id IS 'System assigned identifier for a variable payment';
comment on column ls_historical_component_values.user_id    is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_historical_component_values.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3482, 0, 2017, 1, 0, 0, 47461, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047461_lease_add_historical_lookback_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
