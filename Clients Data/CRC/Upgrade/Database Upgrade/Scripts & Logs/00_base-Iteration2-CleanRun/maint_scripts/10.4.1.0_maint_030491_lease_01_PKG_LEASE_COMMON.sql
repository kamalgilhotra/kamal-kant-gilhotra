/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030491_lease_01_PKG_LEASE_COMMON.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/13/2013 Brandon Beck   Point release
||============================================================================
*/

create or replace package PKG_LEASE_COMMON as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_COMMON
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   ||============================================================================
   */

   -- function the return the process id for lessee calcs
   function F_GETPROCESS return number;

   -- Function to perform booking of LESSEE JEs
   function F_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_MSG           out varchar2) return number;
end PKG_LEASE_COMMON;
/

create or replace package body PKG_LEASE_COMMON as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_COMMON
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck          Original Version
   ||============================================================================
   */

   --**************************************************************
   --         TYPE DECLARATIONS
   --**************************************************************

   --**************************************************************
   --         VARIABLES
   --**************************************************************
   L_PROCESS_ID number;

   --**************************************************************
   --         PROCEDURES
   --**************************************************************
   procedure P_SETPROCESS is
   begin
      select PROCESS_ID
        into L_PROCESS_ID
        from PP_PROCESSES
       where UPPER(DESCRIPTION) = 'LESSEE CALCULATION';
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         L_PROCESS_ID := -1;
   end P_SETPROCESS;

   --**************************************************************
   --         FUNCTIONS
   --**************************************************************
   -- Returns the process id for lease calc
   function F_GETPROCESS return number is

   begin
      return L_PROCESS_ID;
   end F_GETPROCESS;

   --**************************************************************************
   --                            F_BOOKJE
   --**************************************************************************
   function F_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_MSG           out varchar2) return number is
      L_GL_ACCOUNT varchar2(2000);
      L_PROCESSED  number;
   begin
      A_MSG       := 'Starting JE Creation for trans_type: ' || TO_CHAR(A_TRANS_TYPE);
      L_PROCESSED := 0;
      for L_METHOD_REC in (select M.JE_METHOD_ID        as JE_METHOD_ID,
                                  S.SET_OF_BOOKS_ID     as SET_OF_BOOKS_ID,
                                  M.AMOUNT_TYPE         as AMOUNT_TYPE,
                                  S.REVERSAL_CONVENTION as REVERSAL_CONVENTION
                             from JE_METHOD              M,
                                  JE_METHOD_SET_OF_BOOKS S,
                                  COMPANY_SET_OF_BOOKS   C,
                                  JE_METHOD_TRANS_TYPE   T
                            where M.JE_METHOD_ID = S.JE_METHOD_ID
                              and C.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                              and C.INCLUDE_INDICATOR = 1
                              and C.COMPANY_ID = A_COMPANY_ID
                              and M.JE_METHOD_ID = T.JE_METHOD_ID
                              and T.TRANS_TYPE = A_TRANS_TYPE
                              and C.SET_OF_BOOKS_ID = A_SOB_ID)
      loop
         L_PROCESSED  := 1;
         L_GL_ACCOUNT := PP_GL_TRANSACTION2(A_TRANS_TYPE,
                                            A_LS_ASSET_ID,
                                            A_ASSET_ACT_ID,
                                            A_DG_ID,
                                            A_WO_ID,
                                            A_GL_ACCOUNT_ID,
                                            A_GAIN_LOSS,
                                            A_PEND_TRANS_ID,
                                            L_METHOD_REC.JE_METHOD_ID,
                                            L_METHOD_REC.SET_OF_BOOKS_ID,
                                            L_METHOD_REC.AMOUNT_TYPE,
                                            L_METHOD_REC.REVERSAL_CONVENTION);

         if LOWER(L_GL_ACCOUNT) like '%error%' then
            A_MSG := L_GL_ACCOUNT;
            return - 1;
         end if;

         A_MSG := 'Inserting into gl_transaction: ' || TO_CHAR(A_TRANS_TYPE);
         insert into GL_TRANSACTION
            (GL_TRANS_ID, "MONTH", COMPANY_NUMBER, GL_ACCOUNT, DEBIT_CREDIT_INDICATOR, AMOUNT,
             GL_JE_CODE, GL_STATUS_ID, DESCRIPTION, source, ORIGINATOR, COMMENTS, PEND_TRANS_ID,
             ASSET_ID, AMOUNT_TYPE, JE_METHOD_ID, TAX_ORIG_MONTH_NUMBER)
            select PWRPLANT1.NEXTVAL,
                   A_MONTH,
                   C.GL_COMPANY_NO,
                   L_GL_ACCOUNT,
                   A_DR_CR,
                   A_AMT,
                   A_GL_JC,
                   1,
                   'TRANS TYPE: ' || TO_CHAR(A_TRANS_TYPE),
                   'LESSEE',
                   '',
                   '',
                   A_PEND_TRANS_ID,
                   A_LS_ASSET_ID,
                   L_METHOD_REC.AMOUNT_TYPE,
                   L_METHOD_REC.JE_METHOD_ID,
                   null
              from COMPANY_SETUP C
             where C.COMPANY_ID = A_COMPANY_ID;
      end loop;

      return 1;
   exception
      when others then
         return -1;
   end F_BOOKJE;

begin
   -- initialize the package
   P_SETPROCESS;
end PKG_LEASE_COMMON;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (569, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_lease_01_PKG_LEASE_COMMON.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
