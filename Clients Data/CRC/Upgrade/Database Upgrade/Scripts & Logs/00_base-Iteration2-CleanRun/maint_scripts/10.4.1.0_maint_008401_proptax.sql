/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008401_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/19/2012 Julia Breuer   Point Release
||============================================================================
*/

--
-- Create the tables needed to store valuation variables, values, and formulas.
--

--
-- pt_val_timeframe - This table will store the different timeframes for which values may be stored.
--
create table PWRPLANT.PT_VAL_TIMEFRAME
(
 VALUE_TIMEFRAME_ID number(22,0)  not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 DESCRIPTION        varchar2(35)  not null,
 LONG_DESCRIPTION   varchar2(254)
);

alter table PWRPLANT.PT_VAL_TIMEFRAME
   add constraint PT_VAL_TIMEFRAME_PK
       primary key ( VALUE_TIMEFRAME_ID )
       using index tablespace PWRPLANT_IDX;

insert into PWRPLANT.PT_VAL_TIMEFRAME (VALUE_TIMEFRAME_ID, DESCRIPTION, LONG_DESCRIPTION) values (1, 'Cumulative', 'Cumulative');
insert into PWRPLANT.PT_VAL_TIMEFRAME (VALUE_TIMEFRAME_ID, DESCRIPTION, LONG_DESCRIPTION) values (2, 'Annual', 'Annual');
insert into PWRPLANT.PT_VAL_TIMEFRAME (VALUE_TIMEFRAME_ID, DESCRIPTION, LONG_DESCRIPTION) values (3, 'Month', 'Month');

--
-- pt_val_variable_source - This table will store the different sources for variables.
--
create table PWRPLANT.PT_VAL_VARIABLE_SOURCE
(
 VARIABLE_SOURCE_ID number(22,0)  not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 DESCRIPTION        varchar2(35)  not null,
 LONG_DESCRIPTION   varchar2(254)
);

alter table PWRPLANT.PT_VAL_VARIABLE_SOURCE
   add constraint PT_VAL_VAR_SOURCE_PK
       primary key ( VARIABLE_SOURCE_ID )
       using index tablespace PWRPLANT_IDX;

insert into PWRPLANT.PT_VAL_VARIABLE_SOURCE (VARIABLE_SOURCE_ID, DESCRIPTION, LONG_DESCRIPTION) values (1, 'Query', 'Query');
insert into PWRPLANT.PT_VAL_VARIABLE_SOURCE (VARIABLE_SOURCE_ID, DESCRIPTION, LONG_DESCRIPTION) values (2, 'Formula', 'Formula');
insert into PWRPLANT.PT_VAL_VARIABLE_SOURCE (VARIABLE_SOURCE_ID, DESCRIPTION, LONG_DESCRIPTION) values (3, 'Static Value', 'Static Value');

--
-- pt_val_variable - This table will store the different variables.
--
create table PWRPLANT.PT_VAL_VARIABLE
(
 VARIABLE_ID       number(22,0)   not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 DESCRIPTION       varchar2(35)   not null,
 LONG_DESCRIPTION  varchar2(254),
 SHORT_DESCRIPTION varchar2(22)   not null,
 ROUND_RESULT_YN   number(22,0)   not null,
 ROUND_TO_DECIMALS number(22,0)
);

alter table PWRPLANT.PT_VAL_VARIABLE
   add constraint PT_VAL_VARIABLE_PK
       primary key ( VARIABLE_ID )
       using index tablespace PWRPLANT_IDX;

--
-- pt_val_template - This table will store the templates.
--
create table PWRPLANT.PT_VAL_TEMPLATE
(
 VALUE_TEMPLATE_ID      number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 DESCRIPTION            varchar2(35)   not null,
 LONG_DESCRIPTION       varchar2(254),
 PROP_TAX_COMPANY_YN    number(22,0) default 0 not null,
 COMPANY_YN             number(22,0) default 0 not null,
 STATE_YN               number(22,0) default 0 not null,
 COUNTY_YN              number(22,0) default 0 not null,
 TAX_DISTRICT_YN        number(22,0) default 0 not null,
 PARCEL_YN              number(22,0) default 0 not null,
 RESPONSIBLE_ENTITY_YN  number(22,0) default 0 not null,
 RESPONSIBILITY_TYPE_YN number(22,0) default 0 not null,
 GEOGRAPHY_TYPE_YN      number(22,0) default 0 not null,
 FORECAST_YN            number(22,0) default 0 not null,
 FORECAST_YEARS         number(22,0)
);

alter table PWRPLANT.PT_VAL_TEMPLATE
   add constraint PT_VAL_TEMPLATE_PK
       primary key ( VALUE_TEMPLATE_ID )
       using index tablespace PWRPLANT_IDX;

--
-- pt_val_template_equation - This table will store the variables for each template.
--
create TABLE PWRPLANT.PT_VAL_TEMPLATE_EQUATION
(
 VALUE_TEMPLATE_ID       number(22,0)   not null,
 VALUE_TIMEFRAME_ID      number(22,0)   not null,
 VARIABLE_ID             number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 SEQUENCE_NUMBER         number(22,0)   not null,
 VARIABLE_SOURCE_ID      number(22,0)   not null,
 QUERY_TABLE             varchar2(35),
 QUERY_COLUMN            varchar2(35),
 QUERY_FILTER            varchar2(254),
 QUERY_TIMEFRAME_ID      number(22,0),
 STATIC_VALUE            number(22,8),
 PROP_TAX_COMPANY_YN     number(22,0) default 0 not null,
 COMPANY_YN              number(22,0) default 0 not null,
 STATE_YN                number(22,0) default 0 not null,
 COUNTY_YN               number(22,0) default 0 not null,
 TAX_DISTRICT_YN         number(22,0) default 0 not null,
 PARCEL_YN               number(22,0) default 0 not null,
 RESPONSIBLE_ENTITY_YN   number(22,0) default 0 not null,
 RESPONSIBILITY_TYPE_YN  number(22,0) default 0 not null,
 GEOGRAPHY_TYPE_YN       number(22,0) default 0 not null,
 FORECAST_YN             number(22,0) default 0 not null,
 FORECAST_YEARS          number(22,0)
);

alter table PWRPLANT.PT_VAL_TEMPLATE_EQUATION
   add constraint PT_VAL_TEMP_EQN_PK
       primary key ( VALUE_TEMPLATE_ID, VARIABLE_ID, VALUE_TIMEFRAME_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_VAL_TEMPLATE_EQUATION
   add constraint PT_VAL_TEMP_EQN_TEMP_FK
       foreign key ( VALUE_TEMPLATE_ID )
       references PWRPLANT.PT_VAL_TEMPLATE;

alter table PWRPLANT.PT_VAL_TEMPLATE_EQUATION
   add constraint PT_VAL_TEMP_EQN_VAR_FK
       foreign key ( VARIABLE_ID )
       references PWRPLANT.PT_VAL_VARIABLE;

alter table PWRPLANT.PT_VAL_TEMPLATE_EQUATION
   add constraint PT_VAL_TEMP_EQN_TF_FK
       foreign key ( VALUE_TIMEFRAME_ID )
       references PWRPLANT.PT_VAL_TIMEFRAME;

alter table PWRPLANT.PT_VAL_TEMPLATE_EQUATION
   add constraint PT_VAL_TEMP_EQN_SRC_FK
       foreign key ( VARIABLE_SOURCE_ID )
       references PWRPLANT.PT_VAL_VARIABLE_SOURCE;

alter table PWRPLANT.PT_VAL_TEMPLATE_EQUATION
   add constraint PT_VAL_TEMP_EQN_QTF_FK
       foreign key ( QUERY_TIMEFRAME_ID )
       references PWRPLANT.PT_VAL_TIMEFRAME ( VALUE_TIMEFRAME_ID );

--
-- pt_val_template_eq_formula - This table will store the formula for variables using a formula.
--
create table PWRPLANT.PT_VAL_TEMPLATE_EQ_FORMULA
(
 VALUE_TEMPLATE_ID   number(22,0)  not null,
 VARIABLE_ID         number(22,0)  not null,
 VALUE_TIMEFRAME_ID  number(22,0)  not null,
 SEQUENCE_NUMBER     number(22,0)  not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 IS_VARIABLE_YN      number(22,0)  not null,
 FORMULA_VARIABLE_ID number(22,0),
 OPERATOR            varchar2(35)
);

alter table PWRPLANT.PT_VAL_TEMPLATE_EQ_FORMULA
   add constraint PT_VAL_TEMP_EQ_FORM_PK
       primary key ( VALUE_TEMPLATE_ID, VARIABLE_ID, VALUE_TIMEFRAME_ID, SEQUENCE_NUMBER )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_VAL_TEMPLATE_EQ_FORMULA
   add constraint PT_VAL_TEMP_EQ_FORM_TEMP_FK
       foreign key ( VALUE_TEMPLATE_ID )
       references PWRPLANT.PT_VAL_TEMPLATE;

alter table PWRPLANT.PT_VAL_TEMPLATE_EQ_FORMULA
   add constraint PT_VAL_TEMP_EQ_FORM_VAR_FK
       foreign key ( VARIABLE_ID )
       references PWRPLANT.PT_VAL_VARIABLE;

alter table PWRPLANT.PT_VAL_TEMPLATE_EQ_FORMULA
   add constraint PT_VAL_TEMP_EQ_FORM_TF_FK
       foreign key ( VALUE_TIMEFRAME_ID )
       references PWRPLANT.PT_VAL_TIMEFRAME;

alter table PWRPLANT.PT_VAL_TEMPLATE_EQ_FORMULA
   add constraint PT_VAL_TEMP_EQ_FORM_FVAR_FK
       foreign key ( FORMULA_VARIABLE_ID )
       references PWRPLANT.PT_VAL_VARIABLE ( VARIABLE_ID );

alter table PWRPLANT.PT_VAL_TEMPLATE_EQ_FORMULA
   add constraint PT_VAL_TEMP_EQ_FORM_TEQ_FK
       foreign key ( VALUE_TEMPLATE_ID, VARIABLE_ID, VALUE_TIMEFRAME_ID )
       references PWRPLANT.PT_VAL_TEMPLATE_EQUATION ( VALUE_TEMPLATE_ID, VARIABLE_ID, VALUE_TIMEFRAME_ID );

--
-- pt_val_scenario - This table will store the different scenarios that are run (like a case or tax year).
--
create table PWRPLANT.PT_VAL_SCENARIO
(
 SCENARIO_ID      number(22,0)   not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(35)   not null,
 LONG_DESCRIPTION varchar2(254),
 TAX_YEAR         number(22,0)   not null
);

alter table PWRPLANT.PT_VAL_SCENARIO
   add constraint PT_VAL_SCENARIO_PK
       primary key ( SCENARIO_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_VAL_SCENARIO
   add constraint PT_VAL_SCEN_TY_FK
       foreign key ( TAX_YEAR )
       references PWRPLANT.PROPERTY_TAX_YEAR;

--
-- pt_val_scenario_run - This table will store the scenario/template runs.
--
create table PWRPLANT.PT_VAL_SCENARIO_RUN
(
 SCENARIO_ID       number(22,0)   not null,
 VALUE_TEMPLATE_ID number(22,0)   not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 MONTH_NUMBER      number(22,0),
 TAX_YEAR          number(22,0),
 PROMOTED_CASE_ID  number(22,0),
 FILTER_STRING     varchar2(4000)
);

alter table PWRPLANT.PT_VAL_SCENARIO_RUN
   add constraint PT_VAL_SCN_RUN_PK
       primary key ( SCENARIO_ID, VALUE_TEMPLATE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_VAL_SCENARIO_RUN
   add constraint PT_VAL_SCN_RUN_SCN_FK
       foreign key ( sCENARIO_ID )
       references PWRPLANT.PT_VAL_SCENARIO;

alter table PWRPLANT.PT_VAL_SCENARIO_RUN
   add constraint PT_VAL_SCN_RUN_TEMP_FK
       foreign key ( VALUE_TEMPLATE_ID )
       references PWRPLANT.PT_VAL_TEMPLATE;

alter table PWRPLANT.PT_VAL_SCENARIO_RUN
   add constraint PT_VAL_SCN_RUN_TY_FK
       foreign key ( TAX_YEAR )
       references PWRPLANT.PROPERTY_TAX_YEAR;

alter table PWRPLANT.PT_VAL_SCENARIO_RUN
   add constraint PT_VAL_SCN_RUN_CASE_FK
       foreign key ( PROMOTED_CASE_ID )
       references PWRPLANT.PT_CASE;

--
-- pt_val_scenario_result - This table will store the actual values for each variable calculated by a scenario run.  Each row will be for one specific primary key,
--                         such as one parcel/responsible entity combination.
--
create table PWRPLANT.PT_VAL_SCENARIO_RESULT
(
 SCENARIO_ID            number(22,0)   not null,
 VALUE_TEMPLATE_ID      number(22,0)   not null,
 VARIABLE_ID            number(22,0)   not null,
 VALUE_TIMEFRAME_ID     number(22,0)   not null,
 PK_STRING              varchar2(2000) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 VALUE                  number(22,12)  not null,
 PROP_TAX_COMPANY_ID    number(22,0),
 COMPANY_ID             number(22,0),
 STATE_ID               char(18),
 COUNTY_ID              char(18),
 TAX_DISTRICT_ID        number(22,0),
 PARCEL_ID              number(22,0),
 RESPONSIBLE_ENTITY_ID  number(22,0),
 RESPONSIBILITY_TYPE_ID number(22,0),
 GEOGRAPHY_TYPE_ID      number(22,0),
 FORECAST_YEAR          number(22,0)
);

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_PK
       primary key ( SCENARIO_ID, VALUE_TEMPLATE_ID, VARIABLE_ID, VALUE_TIMEFRAME_ID, PK_STRING )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_SCN_FK
       foreign key ( SCENARIO_ID )
       references PWRPLANT.PT_VAL_SCENARIO;

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_TEMP_FK
       foreign key ( VALUE_TEMPLATE_ID )
       references PWRPLANT.PT_VAL_TEMPLATE;

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_VAR_FK
       foreign key ( VARIABLE_ID )
       references PWRPLANT.PT_VAL_VARIABLE;

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_TF_FK
       foreign key ( VALUE_TIMEFRAME_ID )
       references PWRPLANT.PT_VAL_TIMEFRAME;

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_PTCO_FK
       foreign key ( PROP_TAX_COMPANY_ID )
       references PWRPLANT.PT_COMPANY;

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_CO_FK
       foreign key ( COMPANY_ID )
       references PWRPLANT.COMPANY_SETUP;

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_ST_FK
       foreign key ( STATE_ID )
       references PWRPLANT.STATE;

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_CTY_FK
       foreign key ( STATE_ID, COUNTY_ID )
       references PWRPLANT.COUNTY ( STATE_ID, COUNTY_ID );

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_TD_FK
       foreign key ( TAX_DISTRICT_ID )
       references PWRPLANT.PROP_TAX_DISTRICT;

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_PR_FK
       foreign key ( PARCEL_ID )
       references PWRPLANT.PT_PARCEL;

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_RE_FK
       foreign key ( RESPONSIBLE_ENTITY_ID )
       references PWRPLANT.PT_PARCEL_RESPONSIBLE_ENTITY;

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_RT_FK
       foreign key ( RESPONSIBILITY_TYPE_ID )
       references PWRPLANT.PT_PARCEL_RESPONSIBILITY_TYPE;

alter table PWRPLANT.PT_VAL_SCENARIO_RESULT
   add constraint PT_VAL_SCN_RESULT_GT_FK
       foreign key ( GEOGRAPHY_TYPE_ID )
       references PWRPLANT.PT_PARCEL_GEOGRAPHY_TYPE;

create unique index PWRPLANT.PT_VAL_SCEN_RESULT_UNQ_NDX
   on PWRPLANT.PT_VAL_SCENARIO_RESULT ( SCENARIO_ID, VALUE_TEMPLATE_ID, VARIABLE_ID, VALUE_TIMEFRAME_ID,
                                        PROP_TAX_COMPANY_ID, COMPANY_ID, STATE_ID, COUNTY_ID, TAX_DISTRICT_ID,
                                        PARCEL_ID, RESPONSIBLE_ENTITY_ID, RESPONSIBILITY_TYPE_ID,
                                        GEOGRAPHY_TYPE_ID, FORECAST_YEAR )
      tablespace PWRPLANT_IDX;

--
-- pt_value_vault - This table will store values loaded into PowerPlant.  It may then be queried during a scenario run, similar to how pt_ledger_tax_year can be queried.
--
create table PWRPLANT.PT_VALUE_VAULT
(
 VARIABLE_ID            number(22,0)   not null,
 VALUE_TIMEFRAME_ID     number(22,0)   not null,
 MONTH_NUMBER           number(22,0)   not null,
 PK_STRING              varchar2(2000) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 VALUE                  number(22,12)  not null,
 PROP_TAX_COMPANY_ID    number(22,0),
 COMPANY_ID             number(22,0),
 STATE_ID               char(18),
 COUNTY_ID              char(18),
 TAX_DISTRICT_ID        number(22,0),
 PARCEL_ID              number(22,0),
 RESPONSIBLE_ENTITY_ID  number(22,0),
 RESPONSIBILITY_TYPE_ID number(22,0),
 GEOGRAPHY_TYPE_ID      number(22,0)
);

alter table PWRPLANT.PT_VALUE_VAULT
   add constraint PT_VAL_VAULT_PK
       primary key ( VARIABLE_ID, VALUE_TIMEFRAME_ID, MONTH_NUMBER, PK_STRING )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_VALUE_VAULT
   add constraint PT_VAL_VAULT_VAR_FK
       foreign key ( VARIABLE_ID )
       references PWRPLANT.PT_VAL_VARIABLE;

alter table PWRPLANT.PT_VALUE_VAULT
   add constraint PT_VAL_VAULT_TF_FK
       foreign key ( VALUE_TIMEFRAME_ID )
       references PWRPLANT.PT_VAL_TIMEFRAME;

alter table PWRPLANT.PT_VALUE_VAULT
   add constraint PT_VAL_VAULT_PTCO_FK
       foreign key ( PROP_TAX_COMPANY_ID )
       references PWRPLANT.PT_COMPANY;

alter table PWRPLANT.PT_VALUE_VAULT
   add constraint PT_VAL_VAULT_CO_FK
       foreign key ( COMPANY_ID )
       references PWRPLANT.COMPANY_SETUP;

alter table PWRPLANT.PT_VALUE_VAULT
   add constraint PT_VAL_VAULT_ST_FK
       foreign key ( STATE_ID )
       references PWRPLANT.STATE;

alter table PWRPLANT.PT_VALUE_VAULT
   add constraint PT_VAL_VAULT_CTY_FK
       foreign key ( STATE_ID, COUNTY_ID )
       references PWRPLANT.COUNTY ( STATE_ID, COUNTY_ID );

alter table PWRPLANT.PT_VALUE_VAULT
   add constraint PT_VAL_VAULT_TD_FK
       foreign key ( TAX_DISTRICT_ID )
       references PWRPLANT.PROP_TAX_DISTRICT;

alter table PWRPLANT.PT_VALUE_VAULT
   add constraint PT_VAL_VAULT_PR_FK
       foreign key ( PARCEL_ID )
       references PWRPLANT.PT_PARCEL;

alter table PWRPLANT.PT_VALUE_VAULT
   add constraint PT_VAL_VAULT_RE_FK
       foreign key ( RESPONSIBLE_ENTITY_ID )
       references PWRPLANT.PT_PARCEL_RESPONSIBLE_ENTITY;

alter table PWRPLANT.PT_VALUE_VAULT
   add constraint PT_VAL_VAULT_RT_FK
       foreign key ( RESPONSIBILITY_TYPE_ID )
       references PWRPLANT.PT_PARCEL_RESPONSIBILITY_TYPE;

alter table PWRPLANT.PT_VALUE_VAULT
   add constraint PT_VAL_VAULT_GT_FK
       foreign key ( GEOGRAPHY_TYPE_ID )
       references PWRPLANT.PT_PARCEL_GEOGRAPHY_TYPE;

create unique index PWRPLANT.PT_VALUE_VAULT_UNIQUE_NDX
   on PWRPLANT.PT_VALUE_VAULT ( VARIABLE_ID, VALUE_TIMEFRAME_ID, MONTH_NUMBER, PROP_TAX_COMPANY_ID, COMPANY_ID,
                                STATE_ID, COUNTY_ID, TAX_DISTRICT_ID, PARCEL_ID, RESPONSIBLE_ENTITY_ID,
                                RESPONSIBILITY_TYPE_ID, GEOGRAPHY_TYPE_ID)
      tablespace PWRPLANT_IDX;

--
-- Create import tables for loading the pt_value_vault table.
--
-- pt_import_value_vault
create table PWRPLANT.PT_IMPORT_VALUE_VAULT
(
 IMPORT_RUN_ID             number(22,0)   not null,
 LINE_ID                   number(22,0)   not null,
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 VARIABLE_XLATE            varchar2(254),
 VARIABLE_ID               number(22,0),
 VALUE_TIMEFRAME_XLATE     varchar2(254),
 VALUE_TIMEFRAME_ID        number(22,0),
 MONTH_NUMBER              varchar2(35),
 PROP_TAX_COMPANY_XLATE    varchar2(254),
 PROP_TAX_COMPANY_ID       number(22,0),
 PARCEL_TYPE_XLATE         varchar2(254),
 PARCEL_TYPE_ID            number(22,0),
 STATE_XLATE               varchar2(254),
 STATE_ID                  char(18),
 COUNTY_XLATE              varchar2(254),
 COUNTY_ID                 char(18),
 TAX_DISTRICT_XLATE        varchar2(254),
 TAX_DISTRICT_ID           number(22,0),
 ASSESSOR_XLATE            varchar2(254),
 ASSESSOR_ID               number(22,0),
 PARCEL_XLATE              varchar2(254),
 PARCEL_ID                 number(22,0),
 COMPANY_XLATE             varchar2(254),
 COMPANY_ID                number(22,0),
 RESPONSIBLE_ENTITY_XLATE  varchar2(254),
 RESPONSIBLE_ENTITY_ID     number(22,0),
 RESPONSIBILITY_TYPE_XLATE varchar2(254),
 RESPONSIBILITY_TYPE_ID    number(22,0),
 GEOGRAPHY_TYPE_XLATE      varchar2(254),
 GEOGRAPHY_TYPE_ID         number(22,0),
 VALUE                     varchar2(35),
 PK_STRING                 varchar2(2000),
 ERROR_MESSAGE             varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_VALUE_VAULT
   add constraint PT_IMPORT_VAL_VAULT_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_VALUE_VAULT
   add constraint PT_IMPORT_VAL_VAULT_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PP_IMPORT_RUN;

-- pt_import_value_vault_archive
create table PWRPLANT.PT_IMPORT_VALUE_VAULT_ARCHIVE
(
 IMPORT_RUN_ID             number(22,0)   not null,
 LINE_ID                   number(22,0)   not null,
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 VARIABLE_XLATE            varchar2(254),
 VARIABLE_ID               number(22,0),
 VALUE_TIMEFRAME_XLATE     varchar2(254),
 VALUE_TIMEFRAME_ID        number(22,0),
 MONTH_NUMBER              varchar2(35),
 PROP_TAX_COMPANY_XLATE    varchar2(254),
 PROP_TAX_COMPANY_ID       number(22,0),
 PARCEL_TYPE_XLATE         varchar2(254),
 PARCEL_TYPE_ID            number(22,0),
 STATE_XLATE               varchar2(254),
 STATE_ID                  char(18),
 COUNTY_XLATE              varchar2(254),
 COUNTY_ID                 char(18),
 TAX_DISTRICT_XLATE        varchar2(254),
 TAX_DISTRICT_ID           number(22,0),
 ASSESSOR_XLATE            varchar2(254),
 ASSESSOR_ID               number(22,0),
 PARCEL_XLATE              varchar2(254),
 PARCEL_ID                 number(22,0),
 COMPANY_XLATE             varchar2(254),
 COMPANY_ID                number(22,0),
 RESPONSIBLE_ENTITY_XLATE  varchar2(254),
 RESPONSIBLE_ENTITY_ID     number(22,0),
 RESPONSIBILITY_TYPE_XLATE varchar2(254),
 RESPONSIBILITY_TYPE_ID    number(22,0),
 GEOGRAPHY_TYPE_XLATE      varchar2(254),
 GEOGRAPHY_TYPE_ID         number(22,0),
 VALUE                     varchar2(35),
 PK_STRING                 varchar2(2000)
);

alter table PWRPLANT.PT_IMPORT_VALUE_VAULT_ARCHIVE
   add constraint PT_IMPORT_VAL_VAULT_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_VALUE_VAULT_ARCHIVE
   add constraint PT_IMPORT_VAL_VAULT_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PP_IMPORT_RUN;

-- Insert rows needed for importing
insert into pwrplant.pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description ) values ( 17, sysdate, user, 'Add : Variable Values', 'Import Variable Values', 'pt_import_value_vault', 'pt_import_value_vault_archive', null, 0, 'uo_ptc_logic_import', 'pk_string', 'Variable Value' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 17, 1, sysdate, user );

insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'prop_tax_company_id', sysdate, user, 'Prop Tax Company', 'prop_tax_company_xlate', 0, 1, 'number(22,0)', 'pt_company', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'company_id', sysdate, user, 'Company', 'company_xlate', 0, 1, 'number(22,0)', 'company', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'parcel_type_id', sysdate, user, 'Parcel Type', 'parcel_type_xlate', 0, 1, 'number(22,0)', 'pt_parcel_type', 0, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'state_id', sysdate, user, 'State', 'state_xlate', 0, 1, 'char(18)', 'state', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'county_id', sysdate, user, 'County', 'county_xlate', 0, 2, 'char(18)', 'county', 1, 'state_id', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'tax_district_id', sysdate, user, 'Tax District', 'tax_district_xlate', 0, 3, 'number(22,0)', 'prop_tax_district', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'assessor_id', sysdate, user, 'Assessor', 'assessor_xlate', 0, 1, 'number(22,0)', 'pt_assessor', 0, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'parcel_id', sysdate, user, 'Parcel', 'parcel_xlate', 0, 4, 'number(22,0)', 'pt_parcel', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'responsible_entity_id', sysdate, user, 'Responsible Entity', 'responsible_entity_xlate', 0, 1, 'number(22,0)', 'pt_parcel_responsible_entity', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'responsibility_type_id', sysdate, user, 'Responsibility Type', 'responsibility_type_xlate', 0, 1, 'number(22,0)', 'pt_parcel_responsibility_type', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'variable_id', sysdate, user, 'Variable', 'variable_xlate', 1, 1, 'number(22,0)', 'pt_val_variable', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'value_timeframe_id', sysdate, user, 'Value Timeframe', 'value_timeframe_xlate', 1, 1, 'number(22,0)', 'pt_val_timeframe', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'value', sysdate, user, 'Value', '', 1, 1, 'number(22,8)', '', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'month_number', sysdate, user, 'Month Number', '', 1, 1, 'number(22,0)', '', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 17, 'geography_type_id', sysdate, user, 'Geography Type', 'geography_type_xlate', 0, 1, 'number(22,0)', 'pt_parcel_geography_type', 1, '', null );

insert into PWRPLANT.PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 122, sysdate, user, 'PT Value Variable.Description', 'The passed in value corresponds to the PT Value Variable: Description field.  Translate to the Variable ID using the Description column on the PT Value Variable table.', 'variable_id', '( select v.variable_id from pt_val_variable v where upper( trim( <importfield> ) ) = upper( trim( v.description ) ) )', 0, 'pt_val_variable', 'description', '' );
insert into PWRPLANT.PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 123, sysdate, user, 'PT Value Variable.Long Description', 'The passed in value corresponds to the PT Value Variable: Long Description field.  Translate to the Variable ID using the Long Description column on the PT Value Variable table.', 'variable_id', '( select v.variable_id from pt_val_variable v where upper( trim( <importfield> ) ) = upper( trim( v.long_description ) ) )', 0, 'pt_val_variable', 'long_description', '' );
insert into PWRPLANT.PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 124, sysdate, user, 'PT Value Timeframe.Description', 'The passed in value corresponds to the PT Value Timeframe: Description field.  Translate to the Value Timeframe ID using the Description column on the PT Value Timeframe table.', 'value_timeframe_id', '( select vt.value_timeframe_id from pt_val_timeframe vt where upper( trim( <importfield> ) ) = upper( trim( vt.description ) ) )', 0, 'pt_val_timeframe', 'description', '' );
insert into PWRPLANT.PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 125, sysdate, user, 'PT Value Timeframe.Long Description', 'The passed in value corresponds to the PT Value Timeframe: Long Description field.  Translate to the Value Timeframe ID using the Long Description column on the PT Value Timeframe table.', 'value_timeframe_id', '( select vt.value_timeframe_id from pt_val_timeframe vt where upper( trim( <importfield> ) ) = upper( trim( vt.long_description ) ) )', 0, 'pt_val_timeframe', 'long_description', '' );

insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'assessor_id', 67, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'assessor_id', 68, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'assessor_id', 69, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'company_id', 19, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'company_id', 20, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'company_id', 21, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'county_id', 43, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'county_id', 61, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'county_id', 62, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'county_id', 63, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'county_id', 64, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'geography_type_id', 126, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'geography_type_id', 127, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 9, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 10, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 11, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 12, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 13, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 14, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 15, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 16, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 17, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 18, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 70, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 71, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 101, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 102, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 103, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_id', 104, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_type_id', 72, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'parcel_type_id', 73, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'prop_tax_company_id', 3, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'prop_tax_company_id', 4, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'prop_tax_company_id', 5, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'prop_tax_company_id', 6, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'responsibility_type_id', 120, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'responsibility_type_id', 121, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'responsible_entity_id', 118, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'responsible_entity_id', 119, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'responsible_entity_id', 128, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'responsible_entity_id', 129, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'tax_district_id', 79, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'tax_district_id', 80, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'tax_district_id', 81, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'tax_district_id', 82, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'tax_district_id', 83, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'tax_district_id', 84, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'tax_district_id', 85, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'tax_district_id', 86, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'tax_district_id', 87, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'tax_district_id', 105, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'tax_district_id', 106, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'tax_district_id', 107, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'value_timeframe_id', 124, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'value_timeframe_id', 125, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'variable_id', 122, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 17, 'variable_id', 123, sysdate, user );

-- Create a system option for the valuation center menu
insert into PWRPLANT.PTC_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Data Center - Initial Menu State', sysdate, user, 'Whether the side menu is initially expanded or collapsed when opening the Data Center.  If the side menu is expanded the descriptions for each menu item are displayed.  If the side menu is collapsed only the pictures are displayed and the workspace opens in a larger space.', 0, 'Expanded', null, 1 );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_CENTERS ( SYSTEM_OPTION_ID, CENTER_NAME, TIME_STAMP, USER_ID ) values ( 'Data Center - Initial Menu State', 'valuation', sysdate, user );

insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Data Center - Initial Menu State', 'Expanded', sysdate, user );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Data Center - Initial Menu State', 'Collapsed', sysdate, user );

--
-- pt_temp_scenario_formula - This temp table will store the variables used in a formula during a run in the Valuation Center.
--
create global temporary table PWRPLANT.PT_TEMP_SCENARIO_FORMULA
(
 PROP_TAX_COMPANY_ID    number(22,0),
 COMPANY_ID             number(22,0),
 STATE_ID               char(18),
 COUNTY_ID              char(18),
 TAX_DISTRICT_ID        number(22,0),
 PARCEL_ID              number(22,0),
 RESPONSIBLE_ENTITY_ID  number(22,0),
 RESPONSIBILITY_TYPE_ID number(22,0),
 GEOGRAPHY_TYPE_ID      number(22,0),
 FORECAST_YEAR          number(22,0),
 FINAL_VALUE            number(22,8),
 VARIABLE_ID1           number(22,0),
 VARIABLE_VALUE1        number(22,8) default 0 not null,
 VARIABLE_ID2           number(22,0),
 VARIABLE_VALUE2        number(22,8) default 0 not null,
 VARIABLE_ID3           number(22,0),
 VARIABLE_VALUE3        number(22,8) default 0 not null,
 VARIABLE_ID4           number(22,0),
 VARIABLE_VALUE4        number(22,8) default 0 not null,
 VARIABLE_ID5           number(22,0),
 VARIABLE_VALUE5        number(22,8) default 0 not null,
 VARIABLE_ID6           number(22,0),
 VARIABLE_VALUE6        number(22,8) default 0 not null,
 VARIABLE_ID7           number(22,0),
 VARIABLE_VALUE7        number(22,8) default 0 not null,
 VARIABLE_ID8           number(22,0),
 VARIABLE_VALUE8        number(22,8) default 0 not null,
 VARIABLE_ID9           number(22,0),
 VARIABLE_VALUE9        number(22,8) default 0 not null,
 VARIABLE_ID10          number(22,0),
 VARIABLE_VALUE10       number(22,8) default 0 not null,
 VARIABLE_ID11          number(22,0),
 VARIABLE_VALUE11       number(22,8) default 0 not null,
 VARIABLE_ID12          number(22,0),
 VARIABLE_VALUE12       number(22,8) default 0 not null,
 VARIABLE_ID13          number(22,0),
 VARIABLE_VALUE13       number(22,8) default 0 not null,
 VARIABLE_ID14          number(22,0),
 VARIABLE_VALUE14       number(22,8) default 0 not null,
 VARIABLE_ID15          number(22,0),
 VARIABLE_VALUE15       number(22,8) default 0 not null,
 VARIABLE_ID16          number(22,0),
 VARIABLE_VALUE16       number(22,8) default 0 not null,
 VARIABLE_ID17          number(22,0),
 VARIABLE_VALUE17       number(22,8) default 0 not null,
 VARIABLE_ID18          number(22,0),
 VARIABLE_VALUE18       number(22,8) default 0 not null,
 VARIABLE_ID19          number(22,0),
 VARIABLE_VALUE19       number(22,8) default 0 not null,
 VARIABLE_ID20          number(22,0),
 VARIABLE_VALUE20       number(22,8) default 0 not null,
 VARIABLE_ID21          number(22,0),
 VARIABLE_VALUE21       number(22,8) default 0 not null,
 VARIABLE_ID22          number(22,0),
 VARIABLE_VALUE22       number(22,8) default 0 not null,
 VARIABLE_ID23          number(22,0),
 VARIABLE_VALUE23       number(22,8) default 0 not null,
 VARIABLE_ID24          number(22,0),
 VARIABLE_VALUE24       number(22,8) default 0 not null,
 VARIABLE_ID25          number(22,0),
 VARIABLE_VALUE25       number(22,8) default 0 not null,
 VARIABLE_ID26          number(22,0),
 VARIABLE_VALUE26       number(22,8) default 0 not null,
 VARIABLE_ID27          number(22,0),
 VARIABLE_VALUE27       number(22,8) default 0 not null,
 VARIABLE_ID28          number(22,0),
 VARIABLE_VALUE28       number(22,8) default 0 not null,
 VARIABLE_ID29          number(22,0),
 VARIABLE_VALUE29       number(22,8) default 0 not null,
 VARIABLE_ID30          number(22,0),
 VARIABLE_VALUE30       number(22,8) default 0 not null,
 VARIABLE_ID31          number(22,0),
 VARIABLE_VALUE31       number(22,8) default 0 not null,
 VARIABLE_ID32          number(22,0),
 VARIABLE_VALUE32       number(22,8) default 0 not null,
 VARIABLE_ID33          number(22,0),
 VARIABLE_VALUE33       number(22,8) default 0 not null,
 VARIABLE_ID34          number(22,0),
 VARIABLE_VALUE34       number(22,8) default 0 not null,
 VARIABLE_ID35          number(22,0),
 VARIABLE_VALUE35       number(22,8) default 0 not null,
 VARIABLE_ID36          number(22,0),
 VARIABLE_VALUE36       number(22,8) default 0 not null,
 VARIABLE_ID37          number(22,0),
 VARIABLE_VALUE37       number(22,8) default 0 not null,
 VARIABLE_ID38          number(22,0),
 VARIABLE_VALUE38       number(22,8) default 0 not null,
 VARIABLE_ID39          number(22,0),
 VARIABLE_VALUE39       number(22,8) default 0 not null,
 VARIABLE_ID40          number(22,0),
 VARIABLE_VALUE40       number(22,8) default 0 not null,
 VARIABLE_ID41          number(22,0),
 VARIABLE_VALUE41       number(22,8) default 0 not null,
 VARIABLE_ID42          number(22,0),
 VARIABLE_VALUE42       number(22,8) default 0 not null,
 VARIABLE_ID43          number(22,0),
 VARIABLE_VALUE43       number(22,8) default 0 not null,
 VARIABLE_ID44          number(22,0),
 VARIABLE_VALUE44       number(22,8) default 0 not null,
 VARIABLE_ID45          number(22,0),
 VARIABLE_VALUE45       number(22,8) default 0 not null,
 VARIABLE_ID46          number(22,0),
 VARIABLE_VALUE46       number(22,8) default 0 not null,
 VARIABLE_ID47          number(22,0),
 VARIABLE_VALUE47       number(22,8) default 0 not null,
 VARIABLE_ID48          number(22,0),
 VARIABLE_VALUE48       number(22,8) default 0 not null,
 VARIABLE_ID49          number(22,0),
 VARIABLE_VALUE49       number(22,8) default 0 not null,
 VARIABLE_ID50          number(22,0),
 VARIABLE_VALUE50       number(22,8) default 0 not null,
 VARIABLE_ID51          number(22,0),
 VARIABLE_VALUE51       number(22,8) default 0 not null,
 VARIABLE_ID52          number(22,0),
 VARIABLE_VALUE52       number(22,8) default 0 not null,
 VARIABLE_ID53          number(22,0),
 VARIABLE_VALUE53       number(22,8) default 0 not null,
 VARIABLE_ID54          number(22,0),
 VARIABLE_VALUE54       number(22,8) default 0 not null,
 VARIABLE_ID55          number(22,0),
 VARIABLE_VALUE55       number(22,8) default 0 not null,
 VARIABLE_ID56          number(22,0),
 VARIABLE_VALUE56       number(22,8) default 0 not null,
 VARIABLE_ID57          number(22,0),
 VARIABLE_VALUE57       number(22,8) default 0 not null,
 VARIABLE_ID58          number(22,0),
 VARIABLE_VALUE58       number(22,8) default 0 not null,
 VARIABLE_ID59          number(22,0),
 VARIABLE_VALUE59       number(22,8) default 0 not null,
 VARIABLE_ID60          number(22,0),
 VARIABLE_VALUE60       number(22,8) default 0 not null,
 VARIABLE_ID61          number(22,0),
 VARIABLE_VALUE61       number(22,8) default 0 not null,
 VARIABLE_ID62          number(22,0),
 VARIABLE_VALUE62       number(22,8) default 0 not null,
 VARIABLE_ID63          number(22,0),
 VARIABLE_VALUE63       number(22,8) default 0 not null,
 VARIABLE_ID64          number(22,0),
 VARIABLE_VALUE64       number(22,8) default 0 not null,
 VARIABLE_ID65          number(22,0),
 VARIABLE_VALUE65       number(22,8) default 0 not null,
 VARIABLE_ID66          number(22,0),
 VARIABLE_VALUE66       number(22,8) default 0 not null,
 VARIABLE_ID67          number(22,0),
 VARIABLE_VALUE67       number(22,8) default 0 not null,
 VARIABLE_ID68          number(22,0),
 VARIABLE_VALUE68       number(22,8) default 0 not null,
 VARIABLE_ID69          number(22,0),
 VARIABLE_VALUE69       number(22,8) default 0 not null,
 VARIABLE_ID70          number(22,0),
 VARIABLE_VALUE70       number(22,8) default 0 not null,
 VARIABLE_ID71          number(22,0),
 VARIABLE_VALUE71       number(22,8) default 0 not null,
 VARIABLE_ID72          number(22,0),
 VARIABLE_VALUE72       number(22,8) default 0 not null,
 VARIABLE_ID73          number(22,0),
 VARIABLE_VALUE73       number(22,8) default 0 not null,
 VARIABLE_ID74          number(22,0),
 VARIABLE_VALUE74       number(22,8) default 0 not null,
 VARIABLE_ID75          number(22,0),
 VARIABLE_VALUE75       number(22,8) default 0 not null,
 VARIABLE_ID76          number(22,0),
 VARIABLE_VALUE76       number(22,8) default 0 not null,
 VARIABLE_ID77          number(22,0),
 VARIABLE_VALUE77       number(22,8) default 0 not null,
 VARIABLE_ID78          number(22,0),
 VARIABLE_VALUE78       number(22,8) default 0 not null,
 VARIABLE_ID79          number(22,0),
 VARIABLE_VALUE79       number(22,8) default 0 not null,
 VARIABLE_ID80          number(22,0),
 VARIABLE_VALUE80       number(22,8) default 0 not null,
 VARIABLE_ID81          number(22,0),
 VARIABLE_VALUE81       number(22,8) default 0 not null,
 VARIABLE_ID82          number(22,0),
 VARIABLE_VALUE82       number(22,8) default 0 not null,
 VARIABLE_ID83          number(22,0),
 VARIABLE_VALUE83       number(22,8) default 0 not null,
 VARIABLE_ID84          number(22,0),
 VARIABLE_VALUE84       number(22,8) default 0 not null,
 VARIABLE_ID85          number(22,0),
 VARIABLE_VALUE85       number(22,8) default 0 not null,
 VARIABLE_ID86          number(22,0),
 VARIABLE_VALUE86       number(22,8) default 0 not null,
 VARIABLE_ID87          number(22,0),
 VARIABLE_VALUE87       number(22,8) default 0 not null,
 VARIABLE_ID88          number(22,0),
 VARIABLE_VALUE88       number(22,8) default 0 not null,
 VARIABLE_ID89          number(22,0),
 VARIABLE_VALUE89       number(22,8) default 0 not null,
 VARIABLE_ID90          number(22,0),
 VARIABLE_VALUE90       number(22,8) default 0 not null,
 VARIABLE_ID91          number(22,0),
 VARIABLE_VALUE91       number(22,8) default 0 not null,
 VARIABLE_ID92          number(22,0),
 VARIABLE_VALUE92       number(22,8) default 0 not null,
 VARIABLE_ID93          number(22,0),
 VARIABLE_VALUE93       number(22,8) default 0 not null,
 VARIABLE_ID94          number(22,0),
 VARIABLE_VALUE94       number(22,8) default 0 not null,
 VARIABLE_ID95          number(22,0),
 VARIABLE_VALUE95       number(22,8) default 0 not null,
 VARIABLE_ID96          number(22,0),
 VARIABLE_VALUE96       number(22,8) default 0 not null,
 VARIABLE_ID97          number(22,0),
 VARIABLE_VALUE97       number(22,8) default 0 not null,
 VARIABLE_ID98          number(22,0),
 VARIABLE_VALUE98       number(22,8) default 0 not null,
 VARIABLE_ID99          number(22,0),
 VARIABLE_VALUE99       number(22,8) default 0 not null,
 VARIABLE_ID100         number(22,0),
 VARIABLE_VALUE100      number(22,8) default 0 not null,
 VARIABLE_ID101         number(22,0),
 VARIABLE_VALUE101      number(22,8) default 0 not null,
 VARIABLE_ID102         number(22,0),
 VARIABLE_VALUE102      number(22,8) default 0 not null,
 VARIABLE_ID103         number(22,0),
 VARIABLE_VALUE103      number(22,8) default 0 not null,
 VARIABLE_ID104         number(22,0),
 VARIABLE_VALUE104      number(22,8) default 0 not null,
 VARIABLE_ID105         number(22,0),
 VARIABLE_VALUE105      number(22,8) default 0 not null,
 VARIABLE_ID106         number(22,0),
 VARIABLE_VALUE106      number(22,8) default 0 not null,
 VARIABLE_ID107         number(22,0),
 VARIABLE_VALUE107      number(22,8) default 0 not null,
 VARIABLE_ID108         number(22,0),
 VARIABLE_VALUE108      number(22,8) default 0 not null,
 VARIABLE_ID109         number(22,0),
 VARIABLE_VALUE109      number(22,8) default 0 not null,
 VARIABLE_ID110         number(22,0),
 VARIABLE_VALUE110      number(22,8) default 0 not null,
 VARIABLE_ID111         number(22,0),
 VARIABLE_VALUE111      number(22,8) default 0 not null,
 VARIABLE_ID112         number(22,0),
 VARIABLE_VALUE112      number(22,8) default 0 not null,
 VARIABLE_ID113         number(22,0),
 VARIABLE_VALUE113      number(22,8) default 0 not null,
 VARIABLE_ID114         number(22,0),
 VARIABLE_VALUE114      number(22,8) default 0 not null,
 VARIABLE_ID115         number(22,0),
 VARIABLE_VALUE115      number(22,8) default 0 not null,
 VARIABLE_ID116         number(22,0),
 VARIABLE_VALUE116      number(22,8) default 0 not null,
 VARIABLE_ID117         number(22,0),
 VARIABLE_VALUE117      number(22,8) default 0 not null,
 VARIABLE_ID118         number(22,0),
 VARIABLE_VALUE118      number(22,8) default 0 not null,
 VARIABLE_ID119         number(22,0),
 VARIABLE_VALUE119      number(22,8) default 0 not null,
 VARIABLE_ID120         number(22,0),
 VARIABLE_VALUE120      number(22,8) default 0 not null,
 VARIABLE_ID121         number(22,0),
 VARIABLE_VALUE121      number(22,8) default 0 not null,
 VARIABLE_ID122         number(22,0),
 VARIABLE_VALUE122      number(22,8) default 0 not null,
 VARIABLE_ID123         number(22,0),
 VARIABLE_VALUE123      number(22,8) default 0 not null,
 VARIABLE_ID124         number(22,0),
 VARIABLE_VALUE124      number(22,8) default 0 not null,
 VARIABLE_ID125         number(22,0),
 VARIABLE_VALUE125      number(22,8) default 0 not null,
 VARIABLE_ID126         number(22,0),
 VARIABLE_VALUE126      number(22,8) default 0 not null,
 VARIABLE_ID127         number(22,0),
 VARIABLE_VALUE127      number(22,8) default 0 not null,
 VARIABLE_ID128         number(22,0),
 VARIABLE_VALUE128      number(22,8) default 0 not null,
 VARIABLE_ID129         number(22,0),
 VARIABLE_VALUE129      number(22,8) default 0 not null,
 VARIABLE_ID130         number(22,0),
 VARIABLE_VALUE130      number(22,8) default 0 not null,
 VARIABLE_ID131         number(22,0),
 VARIABLE_VALUE131      number(22,8) default 0 not null,
 VARIABLE_ID132         number(22,0),
 VARIABLE_VALUE132      number(22,8) default 0 not null,
 VARIABLE_ID133         number(22,0),
 VARIABLE_VALUE133      number(22,8) default 0 not null,
 VARIABLE_ID134         number(22,0),
 VARIABLE_VALUE134      number(22,8) default 0 not null,
 VARIABLE_ID135         number(22,0),
 VARIABLE_VALUE135      number(22,8) default 0 not null,
 VARIABLE_ID136         number(22,0),
 VARIABLE_VALUE136      number(22,8) default 0 not null,
 VARIABLE_ID137         number(22,0),
 VARIABLE_VALUE137      number(22,8) default 0 not null,
 VARIABLE_ID138         number(22,0),
 VARIABLE_VALUE138      number(22,8) default 0 not null,
 VARIABLE_ID139         number(22,0),
 VARIABLE_VALUE139      number(22,8) default 0 not null,
 VARIABLE_ID140         number(22,0),
 VARIABLE_VALUE140      number(22,8) default 0 not null,
 VARIABLE_ID141         number(22,0),
 VARIABLE_VALUE141      number(22,8) default 0 not null,
 VARIABLE_ID142         number(22,0),
 VARIABLE_VALUE142      number(22,8) default 0 not null,
 VARIABLE_ID143         number(22,0),
 VARIABLE_VALUE143      number(22,8) default 0 not null,
 VARIABLE_ID144         number(22,0),
 VARIABLE_VALUE144      number(22,8) default 0 not null,
 VARIABLE_ID145         number(22,0),
 VARIABLE_VALUE145      number(22,8) default 0 not null,
 VARIABLE_ID146         number(22,0),
 VARIABLE_VALUE146      number(22,8) default 0 not null,
 VARIABLE_ID147         number(22,0),
 VARIABLE_VALUE147      number(22,8) default 0 not null,
 VARIABLE_ID148         number(22,0),
 VARIABLE_VALUE148      number(22,8) default 0 not null,
 VARIABLE_ID149         number(22,0),
 VARIABLE_VALUE149      number(22,8) default 0 not null,
 VARIABLE_ID150         number(22,0),
 VARIABLE_VALUE150      number(22,8) default 0 not null,
 VARIABLE_ID151         number(22,0),
 VARIABLE_VALUE151      number(22,8) default 0 not null,
 VARIABLE_ID152         number(22,0),
 VARIABLE_VALUE152      number(22,8) default 0 not null,
 VARIABLE_ID153         number(22,0),
 VARIABLE_VALUE153      number(22,8) default 0 not null,
 VARIABLE_ID154         number(22,0),
 VARIABLE_VALUE154      number(22,8) default 0 not null,
 VARIABLE_ID155         number(22,0),
 VARIABLE_VALUE155      number(22,8) default 0 not null,
 VARIABLE_ID156         number(22,0),
 VARIABLE_VALUE156      number(22,8) default 0 not null,
 VARIABLE_ID157         number(22,0),
 VARIABLE_VALUE157      number(22,8) default 0 not null,
 VARIABLE_ID158         number(22,0),
 VARIABLE_VALUE158      number(22,8) default 0 not null,
 VARIABLE_ID159         number(22,0),
 VARIABLE_VALUE159      number(22,8) default 0 not null,
 VARIABLE_ID160         number(22,0),
 VARIABLE_VALUE160      number(22,8) default 0 not null,
 VARIABLE_ID161         number(22,0),
 VARIABLE_VALUE161      number(22,8) default 0 not null,
 VARIABLE_ID162         number(22,0),
 VARIABLE_VALUE162      number(22,8) default 0 not null,
 VARIABLE_ID163         number(22,0),
 VARIABLE_VALUE163      number(22,8) default 0 not null,
 VARIABLE_ID164         number(22,0),
 VARIABLE_VALUE164      number(22,8) default 0 not null,
 VARIABLE_ID165         number(22,0),
 VARIABLE_VALUE165      number(22,8) default 0 not null,
 VARIABLE_ID166         number(22,0),
 VARIABLE_VALUE166      number(22,8) default 0 not null,
 VARIABLE_ID167         number(22,0),
 VARIABLE_VALUE167      number(22,8) default 0 not null,
 VARIABLE_ID168         number(22,0),
 VARIABLE_VALUE168      number(22,8) default 0 not null,
 VARIABLE_ID169         number(22,0),
 VARIABLE_VALUE169      number(22,8) default 0 not null,
 VARIABLE_ID170         number(22,0),
 VARIABLE_VALUE170      number(22,8) default 0 not null,
 VARIABLE_ID171         number(22,0),
 VARIABLE_VALUE171      number(22,8) default 0 not null,
 VARIABLE_ID172         number(22,0),
 VARIABLE_VALUE172      number(22,8) default 0 not null,
 VARIABLE_ID173         number(22,0),
 VARIABLE_VALUE173      number(22,8) default 0 not null,
 VARIABLE_ID174         number(22,0),
 VARIABLE_VALUE174      number(22,8) default 0 not null,
 VARIABLE_ID175         number(22,0),
 VARIABLE_VALUE175      number(22,8) default 0 not null,
 VARIABLE_ID176         number(22,0),
 VARIABLE_VALUE176      number(22,8) default 0 not null,
 VARIABLE_ID177         number(22,0),
 VARIABLE_VALUE177      number(22,8) default 0 not null,
 VARIABLE_ID178         number(22,0),
 VARIABLE_VALUE178      number(22,8) default 0 not null,
 VARIABLE_ID179         number(22,0),
 VARIABLE_VALUE179      number(22,8) default 0 not null,
 VARIABLE_ID180         number(22,0),
 VARIABLE_VALUE180      number(22,8) default 0 not null,
 VARIABLE_ID181         number(22,0),
 VARIABLE_VALUE181      number(22,8) default 0 not null,
 VARIABLE_ID182         number(22,0),
 VARIABLE_VALUE182      number(22,8) default 0 not null,
 VARIABLE_ID183         number(22,0),
 VARIABLE_VALUE183      number(22,8) default 0 not null,
 VARIABLE_ID184         number(22,0),
 VARIABLE_VALUE184      number(22,8) default 0 not null,
 VARIABLE_ID185         number(22,0),
 VARIABLE_VALUE185      number(22,8) default 0 not null,
 VARIABLE_ID186         number(22,0),
 VARIABLE_VALUE186      number(22,8) default 0 not null,
 VARIABLE_ID187         number(22,0),
 VARIABLE_VALUE187      number(22,8) default 0 not null,
 VARIABLE_ID188         number(22,0),
 VARIABLE_VALUE188      number(22,8) default 0 not null,
 VARIABLE_ID189         number(22,0),
 VARIABLE_VALUE189      number(22,8) default 0 not null,
 VARIABLE_ID190         number(22,0),
 VARIABLE_VALUE190      number(22,8) default 0 not null,
 VARIABLE_ID191         number(22,0),
 VARIABLE_VALUE191      number(22,8) default 0 not null,
 VARIABLE_ID192         number(22,0),
 VARIABLE_VALUE192      number(22,8) default 0 not null,
 VARIABLE_ID193         number(22,0),
 VARIABLE_VALUE193      number(22,8) default 0 not null,
 VARIABLE_ID194         number(22,0),
 VARIABLE_VALUE194      number(22,8) default 0 not null,
 VARIABLE_ID195         number(22,0),
 VARIABLE_VALUE195      number(22,8) default 0 not null,
 VARIABLE_ID196         number(22,0),
 VARIABLE_VALUE196      number(22,8) default 0 not null,
 VARIABLE_ID197         number(22,0),
 VARIABLE_VALUE197      number(22,8) default 0 not null,
 VARIABLE_ID198         number(22,0),
 VARIABLE_VALUE198      number(22,8) default 0 not null,
 VARIABLE_ID199         number(22,0),
 VARIABLE_VALUE199      number(22,8) default 0 not null,
 VARIABLE_ID200         number(22,0),
 VARIABLE_VALUE200      number(22,8) default 0 not null,
 VARIABLE_ID201         number(22,0),
 VARIABLE_VALUE201      number(22,8) default 0 not null,
 VARIABLE_ID202         number(22,0),
 VARIABLE_VALUE202      number(22,8) default 0 not null,
 VARIABLE_ID203         number(22,0),
 VARIABLE_VALUE203      number(22,8) default 0 not null,
 VARIABLE_ID204         number(22,0),
 VARIABLE_VALUE204      number(22,8) default 0 not null,
 VARIABLE_ID205         number(22,0),
 VARIABLE_VALUE205      number(22,8) default 0 not null,
 VARIABLE_ID206         number(22,0),
 VARIABLE_VALUE206      number(22,8) default 0 not null,
 VARIABLE_ID207         number(22,0),
 VARIABLE_VALUE207      number(22,8) default 0 not null,
 VARIABLE_ID208         number(22,0),
 VARIABLE_VALUE208      number(22,8) default 0 not null,
 VARIABLE_ID209         number(22,0),
 VARIABLE_VALUE209      number(22,8) default 0 not null,
 VARIABLE_ID210         number(22,0),
 VARIABLE_VALUE210      number(22,8) default 0 not null,
 VARIABLE_ID211         number(22,0),
 VARIABLE_VALUE211      number(22,8) default 0 not null,
 VARIABLE_ID212         number(22,0),
 VARIABLE_VALUE212      number(22,8) default 0 not null,
 VARIABLE_ID213         number(22,0),
 VARIABLE_VALUE213      number(22,8) default 0 not null,
 VARIABLE_ID214         number(22,0),
 VARIABLE_VALUE214      number(22,8) default 0 not null,
 VARIABLE_ID215         number(22,0),
 VARIABLE_VALUE215      number(22,8) default 0 not null,
 VARIABLE_ID216         number(22,0),
 VARIABLE_VALUE216      number(22,8) default 0 not null,
 VARIABLE_ID217         number(22,0),
 VARIABLE_VALUE217      number(22,8) default 0 not null,
 VARIABLE_ID218         number(22,0),
 VARIABLE_VALUE218      number(22,8) default 0 not null,
 VARIABLE_ID219         number(22,0),
 VARIABLE_VALUE219      number(22,8) default 0 not null,
 VARIABLE_ID220         number(22,0),
 VARIABLE_VALUE220      number(22,8) default 0 not null,
 VARIABLE_ID221         number(22,0),
 VARIABLE_VALUE221      number(22,8) default 0 not null,
 VARIABLE_ID222         number(22,0),
 VARIABLE_VALUE222      number(22,8) default 0 not null,
 VARIABLE_ID223         number(22,0),
 VARIABLE_VALUE223      number(22,8) default 0 not null,
 VARIABLE_ID224         number(22,0),
 VARIABLE_VALUE224      number(22,8) default 0 not null,
 VARIABLE_ID225         number(22,0),
 VARIABLE_VALUE225      number(22,8) default 0 not null,
 VARIABLE_ID226         number(22,0),
 VARIABLE_VALUE226      number(22,8) default 0 not null,
 VARIABLE_ID227         number(22,0),
 VARIABLE_VALUE227      number(22,8) default 0 not null,
 VARIABLE_ID228         number(22,0),
 VARIABLE_VALUE228      number(22,8) default 0 not null,
 VARIABLE_ID229         number(22,0),
 VARIABLE_VALUE229      number(22,8) default 0 not null,
 VARIABLE_ID230         number(22,0),
 VARIABLE_VALUE230      number(22,8) default 0 not null,
 VARIABLE_ID231         number(22,0),
 VARIABLE_VALUE231      number(22,8) default 0 not null,
 VARIABLE_ID232         number(22,0),
 VARIABLE_VALUE232      number(22,8) default 0 not null,
 VARIABLE_ID233         number(22,0),
 VARIABLE_VALUE233      number(22,8) default 0 not null,
 VARIABLE_ID234         number(22,0),
 VARIABLE_VALUE234      number(22,8) default 0 not null,
 VARIABLE_ID235         number(22,0),
 VARIABLE_VALUE235      number(22,8) default 0 not null,
 VARIABLE_ID236         number(22,0),
 VARIABLE_VALUE236      number(22,8) default 0 not null,
 VARIABLE_ID237         number(22,0),
 VARIABLE_VALUE237      number(22,8) default 0 not null,
 VARIABLE_ID238         number(22,0),
 VARIABLE_VALUE238      number(22,8) default 0 not null,
 VARIABLE_ID239         number(22,0),
 VARIABLE_VALUE239      number(22,8) default 0 not null,
 VARIABLE_ID240         number(22,0),
 VARIABLE_VALUE240      number(22,8) default 0 not null,
 VARIABLE_ID241         number(22,0),
 VARIABLE_VALUE241      number(22,8) default 0 not null,
 VARIABLE_ID242         number(22,0),
 VARIABLE_VALUE242      number(22,8) default 0 not null,
 VARIABLE_ID243         number(22,0),
 VARIABLE_VALUE243      number(22,8) default 0 not null,
 VARIABLE_ID244         number(22,0),
 VARIABLE_VALUE244      number(22,8) default 0 not null,
 VARIABLE_ID245         number(22,0),
 VARIABLE_VALUE245      number(22,8) default 0 not null,
 VARIABLE_ID246         number(22,0),
 VARIABLE_VALUE246      number(22,8) default 0 not null,
 VARIABLE_ID247         number(22,0),
 VARIABLE_VALUE247      number(22,8) default 0 not null,
 VARIABLE_ID248         number(22,0),
 VARIABLE_VALUE248      number(22,8) default 0 not null,
 VARIABLE_ID249         number(22,0),
 VARIABLE_VALUE249      number(22,8) default 0 not null,
 VARIABLE_ID250         number(22,0),
 VARIABLE_VALUE250      number(22,8) default 0 not null,
 VARIABLE_ID251         number(22,0),
 VARIABLE_VALUE251      number(22,8) default 0 not null,
 VARIABLE_ID252         number(22,0),
 VARIABLE_VALUE252      number(22,8) default 0 not null,
 VARIABLE_ID253         number(22,0),
 VARIABLE_VALUE253      number(22,8) default 0 not null,
 VARIABLE_ID254         number(22,0),
 VARIABLE_VALUE254      number(22,8) default 0 not null,
 VARIABLE_ID255         number(22,0),
 VARIABLE_VALUE255      number(22,8) default 0 not null,
 VARIABLE_ID256         number(22,0),
 VARIABLE_VALUE256      number(22,8) default 0 not null,
 VARIABLE_ID257         number(22,0),
 VARIABLE_VALUE257      number(22,8) default 0 not null,
 VARIABLE_ID258         number(22,0),
 VARIABLE_VALUE258      number(22,8) default 0 not null,
 VARIABLE_ID259         number(22,0),
 VARIABLE_VALUE259      number(22,8) default 0 not null,
 VARIABLE_ID260         number(22,0),
 VARIABLE_VALUE260      number(22,8) default 0 not null,
 VARIABLE_ID261         number(22,0),
 VARIABLE_VALUE261      number(22,8) default 0 not null,
 VARIABLE_ID262         number(22,0),
 VARIABLE_VALUE262      number(22,8) default 0 not null,
 VARIABLE_ID263         number(22,0),
 VARIABLE_VALUE263      number(22,8) default 0 not null,
 VARIABLE_ID264         number(22,0),
 VARIABLE_VALUE264      number(22,8) default 0 not null,
 VARIABLE_ID265         number(22,0),
 VARIABLE_VALUE265      number(22,8) default 0 not null,
 VARIABLE_ID266         number(22,0),
 VARIABLE_VALUE266      number(22,8) default 0 not null,
 VARIABLE_ID267         number(22,0),
 VARIABLE_VALUE267      number(22,8) default 0 not null,
 VARIABLE_ID268         number(22,0),
 VARIABLE_VALUE268      number(22,8) default 0 not null,
 VARIABLE_ID269         number(22,0),
 VARIABLE_VALUE269      number(22,8) default 0 not null,
 VARIABLE_ID270         number(22,0),
 VARIABLE_VALUE270      number(22,8) default 0 not null,
 VARIABLE_ID271         number(22,0),
 VARIABLE_VALUE271      number(22,8) default 0 not null,
 VARIABLE_ID272         number(22,0),
 VARIABLE_VALUE272      number(22,8) default 0 not null,
 VARIABLE_ID273         number(22,0),
 VARIABLE_VALUE273      number(22,8) default 0 not null,
 VARIABLE_ID274         number(22,0),
 VARIABLE_VALUE274      number(22,8) default 0 not null,
 VARIABLE_ID275         number(22,0),
 VARIABLE_VALUE275      number(22,8) default 0 not null,
 VARIABLE_ID276         number(22,0),
 VARIABLE_VALUE276      number(22,8) default 0 not null,
 VARIABLE_ID277         number(22,0),
 VARIABLE_VALUE277      number(22,8) default 0 not null,
 VARIABLE_ID278         number(22,0),
 VARIABLE_VALUE278      number(22,8) default 0 not null,
 VARIABLE_ID279         number(22,0),
 VARIABLE_VALUE279      number(22,8) default 0 not null,
 VARIABLE_ID280         number(22,0),
 VARIABLE_VALUE280      number(22,8) default 0 not null,
 VARIABLE_ID281         number(22,0),
 VARIABLE_VALUE281      number(22,8) default 0 not null,
 VARIABLE_ID282         number(22,0),
 VARIABLE_VALUE282      number(22,8) default 0 not null,
 VARIABLE_ID283         number(22,0),
 VARIABLE_VALUE283      number(22,8) default 0 not null,
 VARIABLE_ID284         number(22,0),
 VARIABLE_VALUE284      number(22,8) default 0 not null,
 VARIABLE_ID285         number(22,0),
 VARIABLE_VALUE285      number(22,8) default 0 not null,
 VARIABLE_ID286         number(22,0),
 VARIABLE_VALUE286      number(22,8) default 0 not null,
 VARIABLE_ID287         number(22,0),
 VARIABLE_VALUE287      number(22,8) default 0 not null,
 VARIABLE_ID288         number(22,0),
 VARIABLE_VALUE288      number(22,8) default 0 not null,
 VARIABLE_ID289         number(22,0),
 VARIABLE_VALUE289      number(22,8) default 0 not null,
 VARIABLE_ID290         number(22,0),
 VARIABLE_VALUE290      number(22,8) default 0 not null,
 VARIABLE_ID291         number(22,0),
 VARIABLE_VALUE291      number(22,8) default 0 not null,
 VARIABLE_ID292         number(22,0),
 VARIABLE_VALUE292      number(22,8) default 0 not null,
 VARIABLE_ID293         number(22,0),
 VARIABLE_VALUE293      number(22,8) default 0 not null,
 VARIABLE_ID294         number(22,0),
 VARIABLE_VALUE294      number(22,8) default 0 not null,
 VARIABLE_ID295         number(22,0),
 VARIABLE_VALUE295      number(22,8) default 0 not null,
 VARIABLE_ID296         number(22,0),
 VARIABLE_VALUE296      number(22,8) default 0 not null,
 VARIABLE_ID297         number(22,0),
 VARIABLE_VALUE297      number(22,8) default 0 not null,
 VARIABLE_ID298         number(22,0),
 VARIABLE_VALUE298      number(22,8) default 0 not null,
 VARIABLE_ID299         number(22,0),
 VARIABLE_VALUE299      number(22,8) default 0 not null,
 VARIABLE_ID300         number(22,0),
 VARIABLE_VALUE300      number(22,8) default 0 not null,
 VARIABLE_ID301         number(22,0),
 VARIABLE_VALUE301      number(22,8) default 0 not null,
 VARIABLE_ID302         number(22,0),
 VARIABLE_VALUE302      number(22,8) default 0 not null,
 VARIABLE_ID303         number(22,0),
 VARIABLE_VALUE303      number(22,8) default 0 not null,
 VARIABLE_ID304         number(22,0),
 VARIABLE_VALUE304      number(22,8) default 0 not null,
 VARIABLE_ID305         number(22,0),
 VARIABLE_VALUE305      number(22,8) default 0 not null,
 VARIABLE_ID306         number(22,0),
 VARIABLE_VALUE306      number(22,8) default 0 not null,
 VARIABLE_ID307         number(22,0),
 VARIABLE_VALUE307      number(22,8) default 0 not null,
 VARIABLE_ID308         number(22,0),
 VARIABLE_VALUE308      number(22,8) default 0 not null,
 VARIABLE_ID309         number(22,0),
 VARIABLE_VALUE309      number(22,8) default 0 not null,
 VARIABLE_ID310         number(22,0),
 VARIABLE_VALUE310      number(22,8) default 0 not null,
 VARIABLE_ID311         number(22,0),
 VARIABLE_VALUE311      number(22,8) default 0 not null,
 VARIABLE_ID312         number(22,0),
 VARIABLE_VALUE312      number(22,8) default 0 not null,
 VARIABLE_ID313         number(22,0),
 VARIABLE_VALUE313      number(22,8) default 0 not null,
 VARIABLE_ID314         number(22,0),
 VARIABLE_VALUE314      number(22,8) default 0 not null,
 VARIABLE_ID315         number(22,0),
 VARIABLE_VALUE315      number(22,8) default 0 not null,
 VARIABLE_ID316         number(22,0),
 VARIABLE_VALUE316      number(22,8) default 0 not null,
 VARIABLE_ID317         number(22,0),
 VARIABLE_VALUE317      number(22,8) default 0 not null,
 VARIABLE_ID318         number(22,0),
 VARIABLE_VALUE318      number(22,8) default 0 not null,
 VARIABLE_ID319         number(22,0),
 VARIABLE_VALUE319      number(22,8) default 0 not null,
 VARIABLE_ID320         number(22,0),
 VARIABLE_VALUE320      number(22,8) default 0 not null,
 VARIABLE_ID321         number(22,0),
 VARIABLE_VALUE321      number(22,8) default 0 not null,
 VARIABLE_ID322         number(22,0),
 VARIABLE_VALUE322      number(22,8) default 0 not null,
 VARIABLE_ID323         number(22,0),
 VARIABLE_VALUE323      number(22,8) default 0 not null,
 VARIABLE_ID324         number(22,0),
 VARIABLE_VALUE324      number(22,8) default 0 not null,
 VARIABLE_ID325         number(22,0),
 VARIABLE_VALUE325      number(22,8) default 0 not null,
 VARIABLE_ID326         number(22,0),
 VARIABLE_VALUE326      number(22,8) default 0 not null,
 VARIABLE_ID327         number(22,0),
 VARIABLE_VALUE327      number(22,8) default 0 not null,
 VARIABLE_ID328         number(22,0),
 VARIABLE_VALUE328      number(22,8) default 0 not null,
 VARIABLE_ID329         number(22,0),
 VARIABLE_VALUE329      number(22,8) default 0 not null,
 VARIABLE_ID330         number(22,0),
 VARIABLE_VALUE330      number(22,8) default 0 not null,
 VARIABLE_ID331         number(22,0),
 VARIABLE_VALUE331      number(22,8) default 0 not null,
 VARIABLE_ID332         number(22,0),
 VARIABLE_VALUE332      number(22,8) default 0 not null,
 VARIABLE_ID333         number(22,0),
 VARIABLE_VALUE333      number(22,8) default 0 not null,
 VARIABLE_ID334         number(22,0),
 VARIABLE_VALUE334      number(22,8) default 0 not null,
 VARIABLE_ID335         number(22,0),
 VARIABLE_VALUE335      number(22,8) default 0 not null,
 VARIABLE_ID336         number(22,0),
 VARIABLE_VALUE336      number(22,8) default 0 not null,
 VARIABLE_ID337         number(22,0),
 VARIABLE_VALUE337      number(22,8) default 0 not null,
 VARIABLE_ID338         number(22,0),
 VARIABLE_VALUE338      number(22,8) default 0 not null,
 VARIABLE_ID339         number(22,0),
 VARIABLE_VALUE339      number(22,8) default 0 not null,
 VARIABLE_ID340         number(22,0),
 VARIABLE_VALUE340      number(22,8) default 0 not null,
 VARIABLE_ID341         number(22,0),
 VARIABLE_VALUE341      number(22,8) default 0 not null,
 VARIABLE_ID342         number(22,0),
 VARIABLE_VALUE342      number(22,8) default 0 not null,
 VARIABLE_ID343         number(22,0),
 VARIABLE_VALUE343      number(22,8) default 0 not null,
 VARIABLE_ID344         number(22,0),
 VARIABLE_VALUE344      number(22,8) default 0 not null,
 VARIABLE_ID345         number(22,0),
 VARIABLE_VALUE345      number(22,8) default 0 not null,
 VARIABLE_ID346         number(22,0),
 VARIABLE_VALUE346      number(22,8) default 0 not null,
 VARIABLE_ID347         number(22,0),
 VARIABLE_VALUE347      number(22,8) default 0 not null,
 VARIABLE_ID348         number(22,0),
 VARIABLE_VALUE348      number(22,8) default 0 not null,
 VARIABLE_ID349         number(22,0),
 VARIABLE_VALUE349      number(22,8) default 0 not null,
 VARIABLE_ID350         number(22,0),
 VARIABLE_VALUE350      number(22,8) default 0 not null,
 VARIABLE_ID351         number(22,0),
 VARIABLE_VALUE351      number(22,8) default 0 not null,
 VARIABLE_ID352         number(22,0),
 VARIABLE_VALUE352      number(22,8) default 0 not null,
 VARIABLE_ID353         number(22,0),
 VARIABLE_VALUE353      number(22,8) default 0 not null,
 VARIABLE_ID354         number(22,0),
 VARIABLE_VALUE354      number(22,8) default 0 not null,
 VARIABLE_ID355         number(22,0),
 VARIABLE_VALUE355      number(22,8) default 0 not null,
 VARIABLE_ID356         number(22,0),
 VARIABLE_VALUE356      number(22,8) default 0 not null,
 VARIABLE_ID357         number(22,0),
 VARIABLE_VALUE357      number(22,8) default 0 not null,
 VARIABLE_ID358         number(22,0),
 VARIABLE_VALUE358      number(22,8) default 0 not null,
 VARIABLE_ID359         number(22,0),
 VARIABLE_VALUE359      number(22,8) default 0 not null,
 VARIABLE_ID360         number(22,0),
 VARIABLE_VALUE360      number(22,8) default 0 not null,
 VARIABLE_ID361         number(22,0),
 VARIABLE_VALUE361      number(22,8) default 0 not null,
 VARIABLE_ID362         number(22,0),
 VARIABLE_VALUE362      number(22,8) default 0 not null,
 VARIABLE_ID363         number(22,0),
 VARIABLE_VALUE363      number(22,8) default 0 not null,
 VARIABLE_ID364         number(22,0),
 VARIABLE_VALUE364      number(22,8) default 0 not null,
 VARIABLE_ID365         number(22,0),
 VARIABLE_VALUE365      number(22,8) default 0 not null,
 VARIABLE_ID366         number(22,0),
 VARIABLE_VALUE366      number(22,8) default 0 not null,
 VARIABLE_ID367         number(22,0),
 VARIABLE_VALUE367      number(22,8) default 0 not null,
 VARIABLE_ID368         number(22,0),
 VARIABLE_VALUE368      number(22,8) default 0 not null,
 VARIABLE_ID369         number(22,0),
 VARIABLE_VALUE369      number(22,8) default 0 not null,
 VARIABLE_ID370         number(22,0),
 VARIABLE_VALUE370      number(22,8) default 0 not null,
 VARIABLE_ID371         number(22,0),
 VARIABLE_VALUE371      number(22,8) default 0 not null,
 VARIABLE_ID372         number(22,0),
 VARIABLE_VALUE372      number(22,8) default 0 not null,
 VARIABLE_ID373         number(22,0),
 VARIABLE_VALUE373      number(22,8) default 0 not null,
 VARIABLE_ID374         number(22,0),
 VARIABLE_VALUE374      number(22,8) default 0 not null,
 VARIABLE_ID375         number(22,0),
 VARIABLE_VALUE375      number(22,8) default 0 not null,
 VARIABLE_ID376         number(22,0),
 VARIABLE_VALUE376      number(22,8) default 0 not null,
 VARIABLE_ID377         number(22,0),
 VARIABLE_VALUE377      number(22,8) default 0 not null,
 VARIABLE_ID378         number(22,0),
 VARIABLE_VALUE378      number(22,8) default 0 not null,
 VARIABLE_ID379         number(22,0),
 VARIABLE_VALUE379      number(22,8) default 0 not null,
 VARIABLE_ID380         number(22,0),
 VARIABLE_VALUE380      number(22,8) default 0 not null,
 VARIABLE_ID381         number(22,0),
 VARIABLE_VALUE381      number(22,8) default 0 not null,
 VARIABLE_ID382         number(22,0),
 VARIABLE_VALUE382      number(22,8) default 0 not null,
 VARIABLE_ID383         number(22,0),
 VARIABLE_VALUE383      number(22,8) default 0 not null,
 VARIABLE_ID384         number(22,0),
 VARIABLE_VALUE384      number(22,8) default 0 not null,
 VARIABLE_ID385         number(22,0),
 VARIABLE_VALUE385      number(22,8) default 0 not null,
 VARIABLE_ID386         number(22,0),
 VARIABLE_VALUE386      number(22,8) default 0 not null,
 VARIABLE_ID387         number(22,0),
 VARIABLE_VALUE387      number(22,8) default 0 not null,
 VARIABLE_ID388         number(22,0),
 VARIABLE_VALUE388      number(22,8) default 0 not null,
 VARIABLE_ID389         number(22,0),
 VARIABLE_VALUE389      number(22,8) default 0 not null,
 VARIABLE_ID390         number(22,0),
 VARIABLE_VALUE390      number(22,8) default 0 not null,
 VARIABLE_ID391         number(22,0),
 VARIABLE_VALUE391      number(22,8) default 0 not null,
 VARIABLE_ID392         number(22,0),
 VARIABLE_VALUE392      number(22,8) default 0 not null,
 VARIABLE_ID393         number(22,0),
 VARIABLE_VALUE393      number(22,8) default 0 not null,
 VARIABLE_ID394         number(22,0),
 VARIABLE_VALUE394      number(22,8) default 0 not null,
 VARIABLE_ID395         number(22,0),
 VARIABLE_VALUE395      number(22,8) default 0 not null,
 VARIABLE_ID396         number(22,0),
 VARIABLE_VALUE396      number(22,8) default 0 not null,
 VARIABLE_ID397         number(22,0),
 VARIABLE_VALUE397      number(22,8) default 0 not null,
 VARIABLE_ID398         number(22,0),
 VARIABLE_VALUE398      number(22,8) default 0 not null,
 VARIABLE_ID399         number(22,0),
 VARIABLE_VALUE399      number(22,8) default 0 not null,
 VARIABLE_ID400         number(22,0),
 VARIABLE_VALUE400      number(22,8) default 0 not null
) on commit preserve rows;

create unique index PWRPLANT.PT_TEMP_SCEN_FORM_UNQ_NDX
   on PWRPLANT.PT_TEMP_SCENARIO_FORMULA ( PROP_TAX_COMPANY_ID, COMPANY_ID, STATE_ID, COUNTY_ID,
                                          TAX_DISTRICT_ID, PARCEL_ID, RESPONSIBLE_ENTITY_ID,
                                          RESPONSIBILITY_TYPE_ID, GEOGRAPHY_TYPE_ID, FORECAST_YEAR );

--
-- Create a new data for reporting.
--
insert into PWRPLANT.PP_REPORTS_TIME_OPTION (PP_REPORT_TIME_OPTION_ID, DESCRIPTION) values (36, 'PT - Single Scenario');
insert into PWRPLANT.PP_REPORTS_TIME_OPTION (PP_REPORT_TIME_OPTION_ID, DESCRIPTION) values (37, 'PT - Scenario Compare');

insert into PWRPLANT.PP_REPORTS_FILTER (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME) values (22, 'Property Tax - Scenario', 'pt_val_scenario_result');

-- Create system options for the default review display in the search grids
insert into PWRPLANT.PTC_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Data Center - Search Scenario - Default Results Display', sysdate, user, 'The default selection for the display of results - by Variable or by Attributes.  This specifies the inital selection when the workspace is opened.', 0, 'Variable', null, 1 );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_CENTERS ( SYSTEM_OPTION_ID, CENTER_NAME, TIME_STAMP, USER_ID ) values ( 'Data Center - Search Scenario - Default Results Display', 'valuation', sysdate, user );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Data Center - Search Scenario - Default Results Display', 'Variable', sysdate, user );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Data Center - Search Scenario - Default Results Display', 'Attributes', sysdate, user );

insert into PWRPLANT.PTC_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Data Center - Search Vault - Default Results Display', sysdate, user, 'The default selection for the display of results - by Variable or by Attributes.  This specifies the inital selection when the workspace is opened.', 0, 'Variable', null, 1 );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_CENTERS ( SYSTEM_OPTION_ID, CENTER_NAME, TIME_STAMP, USER_ID ) values ( 'Data Center - Search Vault - Default Results Display', 'valuation', sysdate, user );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Data Center - Search Vault - Default Results Display', 'Variable', sysdate, user );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Data Center - Search Vault - Default Results Display', 'Attributes', sysdate, user );

--
-- Create a table to store factors by geography type.
--
create table PWRPLANT.PT_PARCEL_GEO_TYPE_FACTORS
(
 TAX_YEAR          number(22,0)   not null,
 GEOGRAPHY_TYPE_ID number(22,0)   not null,
 AGE               number(22,0)   not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 PERCENT           number(22,8)
);

alter table PWRPLANT.PT_PARCEL_GEO_TYPE_FACTORS
   add constraint PT_PRCL_GEO_TYPE_FCTRS_PK
       primary key ( TAX_YEAR, GEOGRAPHY_TYPE_ID, AGE )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_PARCEL_GEO_TYPE_FACTORS
   add constraint PT_PRCL_GEO_TYPE_FCTRS_TY_FK
       foreign key ( TAX_YEAR )
       references PWRPLANT.PROPERTY_TAX_YEAR;

alter table PWRPLANT.PT_PARCEL_GEO_TYPE_FACTORS
   add constraint PT_PRCL_GEO_TYPE_FCTRS_GT_FK
       foreign key ( GEOGRAPHY_TYPE_ID )
       references PWRPLANT.PT_PARCEL_GEOGRAPHY_TYPE;

--
-- Add an import for pt_parcel_geo_type_factors.
--

-- pt_import_prcl_geo
create table PWRPLANT.PT_IMPORT_PRCL_GEOFCTR
(
 IMPORT_RUN_ID        number(22,0)   not null,
 LINE_ID              number(22,0)   not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 TAX_YEAR_XLATE       varchar2(254),
 TAX_YEAR             number(22,0),
 GEOGRAPHY_TYPE_XLATE varchar2(254),
 GEOGRAPHY_TYPE_ID    number(22,0),
 AGE                  varchar2(35),
 PERCENT              varchar2(35),
 ERROR_MESSAGE        varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_PRCL_GEOFCTR
   add constraint PT_IMPORT_PR_GFCTR_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_PRCL_GEOFCTR
   add constraint PT_IMPORT_PR_GFCTR_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PP_IMPORT_RUN;

-- pt_import_prcl_geofctr_archive
create table PWRPLANT.PT_IMPORT_PRCL_GEOFCTR_ARCHIVE
(
 IMPORT_RUN_ID        number(22,0)   not null,
 LINE_ID              number(22,0)   not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 TAX_YEAR_XLATE       varchar2(254),
 TAX_YEAR             number(22,0),
 GEOGRAPHY_TYPE_XLATE varchar2(254),
 GEOGRAPHY_TYPE_ID    number(22,0),
 AGE                  varchar2(35),
 PERCENT              varchar2(35),
 ERROR_MESSAGE        varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_PRCL_GEOFCTR_ARCHIVE
   add constraint PT_IMPORT_PR_GFCTR_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_PRCL_GEOFCTR_ARCHIVE
   add constraint PT_IMPORT_PR_GFCTR_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PP_IMPORT_RUN;

-- Add the import type rows
insert into PWRPLANT.PP_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION ) values ( 19, sysdate, user, 'Add/Update : Geography Type Factors', 'Import Parcel Geography Type Factors', 'pt_import_prcl_geofctr', 'pt_import_prcl_geofctr_archive', null, 0, 'uo_ptc_logic_import', '', 'Parcel Geography Type Factor' );

insert into pwrplant.pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 19, 1, sysdate, user );

insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 19, 'tax_year', sysdate, user, 'Tax Year', 'tax_year_xlate', 1, 1, 'number(22,0)', 'property_tax_year', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 19, 'geography_type_id', sysdate, user, 'Geography Type', 'geography_type_xlate', 1, 1, 'number(22,0)', 'pt_parcel_geography_type', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 19, 'age', sysdate, user, 'Age', '', 1, 1, 'number(22,0)', '', 1, '', null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, AUTOCREATE_IMPORT_TYPE_ID ) values ( 19, 'percent', sysdate, user, 'Percent', '', 1, 1, 'number(22,8)', '', 1, '', null );

insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 19, 'tax_year', 35, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 19, 'tax_year', 36, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 19, 'tax_year', 37, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 19, 'tax_year', 38, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 19, 'geography_type_id', 126, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 19, 'geography_type_id', 127, sysdate, user );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (232, 0, 10, 4, 1, 0, 8401, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_008401_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
