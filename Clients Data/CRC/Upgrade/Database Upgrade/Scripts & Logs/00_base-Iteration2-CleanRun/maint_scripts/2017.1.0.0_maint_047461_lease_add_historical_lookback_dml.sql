/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047461_lease_add_historical_lookback_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- --------------    ----------------------------------------
|| 2017.1.0.0 05/01/2017 Johnny Sisouphanh Add Historic Lookback to Lease
||=================================================================================
*/

insert into ls_component_type(ls_component_type_id, description)
values(3, 'Historical');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3481, 0, 2017, 1, 0, 0, 47461, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047461_lease_add_historical_lookback_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;