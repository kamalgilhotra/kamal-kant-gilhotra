/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_006619_cr_budget.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/07/2011 Brandon TubandtPoint Release
||============================================================================
*/

create table CR_TO_BDG_TO_CR_FCST_AMT
(
 id number(22,0),
 description varchar2(35),
 amount_fields varchar2(200)
);

alter table cr_to_bdg_to_cr_fcst_amt
   add constraint CR_TO_B_TO_CR_FCST_AMT_PK
       primary key (id)
       using index tablespace PWRPLANT_IDX;

alter table CR_TO_BUDGET_TO_CR_CONTROL
   add (FCST_DEPR_AMT_TYPE varchar2(2000),
        ADDL_SQL           varchar2(4000));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (46, 0, 10, 3, 3, 0, 6619, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_006619_cr_budget.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
