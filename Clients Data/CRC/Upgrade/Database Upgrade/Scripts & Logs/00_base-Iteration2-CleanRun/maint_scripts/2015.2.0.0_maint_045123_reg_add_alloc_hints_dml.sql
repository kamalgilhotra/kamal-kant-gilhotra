/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045123_reg_add_alloc_hints_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/27/2015 Sarah Byers    Add hints for allocations process
||============================================================================
*/

insert into pp_datawindow_hints (
	DATAWINDOW, SELECT_NUMBER, HINT, REASON, CHANGE_BY, CHANGE_DATE)
values (
	'REG_ALLOC_RESULT INSERT FOR NON-DYNAMIC ALLOCATORS', 0, '/*+ NO_CPU_COSTING */', null, 'pwrplant', to_date('20-10-2015 11:06:51', 'dd-mm-yyyy hh24:mi:ss'));

insert into pp_datawindow_hints (
	DATAWINDOW, SELECT_NUMBER, HINT, REASON, CHANGE_BY, CHANGE_DATE)
values (
	'REG_ALLOC_RESULT INSERT FOR DYNAMIC ALLOCATORS', 0, '/*+ NO_CPU_COSTING */', null, 'pwrplant', to_date('20-10-2015 11:06:51', 'dd-mm-yyyy hh24:mi:ss'));
 
insert into pp_datawindow_hints (
	DATAWINDOW, SELECT_NUMBER, HINT, REASON, CHANGE_BY, CHANGE_DATE)
values (
	'UPDATE REG_CASE_ADJUST_ALLOC_RESULT PERCENT EXCEPT DISALLOWED ADJ', 0, '/*+ NO_CPU_COSTING */', null, 'pwrplant', to_date('20-10-2015 11:07:49', 'dd-mm-yyyy hh24:mi:ss'));
 
insert into pp_datawindow_hints (
	DATAWINDOW, SELECT_NUMBER, HINT, REASON, CHANGE_BY, CHANGE_DATE)
values (
	'REG_CASE_ADJUST_ALLOC_RESULT PERCENT FOR DISALLOWED ADJ DYNAMIC', 0, '/*+ NO_CPU_COSTING */', null, 'pwrplant', to_date('20-10-2015 11:08:25', 'dd-mm-yyyy hh24:mi:ss'));

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2941, 0, 2015, 2, 0, 0, 45123, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045123_reg_add_alloc_hints_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
