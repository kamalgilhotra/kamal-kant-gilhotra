/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038731_lease_PKG_LEASE_ASSET_POST.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 04/14/2014 Ryan Oliveria	Added F_DELETE_ASSETS
||============================================================================
*/

create or replace package PKG_LEASE_ASSET_POST as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_ASSET_POST
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   ||============================================================================
   */
   G_SEND_JES boolean NOT NULL := true;

   procedure P_SET_ILR_ID(A_ILR_ID number);

   -- Function to perform specific Leased Asset addition
   function F_ADD_ASSET(A_LS_ASSET_ID number) return varchar2;

   -- Function to perform specific Leased Asset retirements
   function F_RETIRE_ASSET(A_CPR_ASSET_ID        number,
                           A_ACCT_MONTH          date,
                           A_MONTH               date,
                           A_RETIREMENT_AMOUNT   number,
                           A_RETIREMENT_QUANTITY number,
                           A_GAIN_LOSS_AMOUNT    number,
                           A_DG_ID               number,
                           A_PT_ID               number,
                           A_MSG                 out varchar2) return number;

   -- Function to perform specific Leased Asset transfer from
   function F_TRANSFER_ASSET_FROM(A_CPR_ASSET_ID number,
                                  A_AMOUNT       number,
                                  A_QUANTITY     number,
                                  A_PT_ID        number,
                                  A_MSG          out varchar2) return number;

   -- Function to perform specific Leased Asset transfer to
   -- takes in the asset attributes to handle a transfer.
   -- out parameter for message.  Returns a number
   -- 1 for success, 0 for failure
   function F_TRANSFER_ASSET_TO(A_CPR_ASSET_ID       number,
                                A_AMOUNT             number,
                                A_QUANTITY           number,
                                A_COMPANY_ID         number,
                                A_BUS_SEGMENT_ID     number,
                                A_UTILITY_ACCOUNT_ID number,
                                A_SUB_ACCOUNT_ID     number,
                                A_FUNC_CLASS_ID      number,
                                A_PROPERTY_GROUP_ID  number,
                                A_RETIREMENT_UNIT_ID number,
                                A_ASSET_LOCATION_ID  number,
                                A_DG_ID              number,
                                A_PT_ID              number,
                                A_MSG                out varchar2) return number;
	
	-- Function to delete assets and all dependant tables
	function F_DELETE_ASSETS(A_ASSET_IDS in PKG_LEASE_CALC.NUM_ARRAY) return varchar2;

   function F_GET_ILR_ID return number;
end PKG_LEASE_ASSET_POST;
/


create or replace package body PKG_LEASE_ASSET_POST as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_ASSET_POST
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   ||============================================================================
   */

   L_ILR_ID number;

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   procedure P_DEPR_SLE( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_LIFE number, A_MSG out varchar2)
   is
      L_MONTHLY_OBL number(22,2);
      L_TOTAL_OBL number(22,2);
      L_TOTAL_CHECK number(22, 2);
      L_NUM_MONTHS number(22,0);
      L_ROUND number(22,2);
    L_MAX_MONTH  LS_ASSET_SCHEDULE."MONTH"%type;
    L_CUR_MONTH  LS_ASSET_SCHEDULE."MONTH"%type;
    L_START_MONTH  LS_ASSET_SCHEDULE."MONTH"%type;
    L_MONTHLOOP number;
   begin
      A_MSG := 'DELETING from depr_method_sle for current month through end of lease';
      delete from DEPR_METHOD_SLE
      where ASSET_ID = A_ASSET_ID
      and SET_OF_BOOKS_ID = A_SOB_ID
      ;

      a_msg := 'GET number of months';
      select min("MONTH"), add_months( min("MONTH"), A_LIFE), count(1), sum(PRINCIPAL_PAID) + sum(INTEREST_PAID)
      into L_START_MONTH, L_MAX_MONTH, L_NUM_MONTHS, L_TOTAL_OBL
      from LS_ASSET_SCHEDULE
      where LS_ASSET_ID = A_LS_ASSET_ID
      and REVISION = A_REVISION
      and SET_OF_BOOKS_ID = A_SOB_ID;

      if A_LIFE > 0 then
         L_MONTHLY_OBL := round( L_TOTAL_OBL / A_LIFE, 2);
         L_TOTAL_CHECK := A_LIFE * L_MONTHLY_OBL;
         L_ROUND := L_TOTAL_OBL - L_TOTAL_CHECK;

         A_MSG := 'INSERTING depreciation expense for SLE Method';
         insert into DEPR_METHOD_SLE
         (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, DEPR_EXPENSE)
         select A_ASSET_ID, A_SOB_ID, LAS."MONTH",
               case when LAS."MONTH" <> L_MAX_MONTH then L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL
               else L_MONTHLY_OBL - LAS.INTEREST_ACCRUAL + L_ROUND
               end
         from LS_ASSET_SCHEDULE LAS
         where LAS.LS_ASSET_ID = A_LS_ASSET_ID
         and LAS.REVISION = A_REVISION
         and LAS.SET_OF_BOOKS_ID = A_SOB_ID
         ;

       -- add extra months if A_LIFE > L_NUM_MONTHS.  This is the portion after the lease term
       -- and when the asset is "an owned asset"
       if A_LIFE > L_NUM_MONTHS then
        for L_MONTHLOOP in L_NUM_MONTHS .. (A_LIFE - 1) loop
         L_CUR_MONTH := add_months( L_START_MONTH, L_MONTHLOOP );

         insert into DEPR_METHOD_SLE
         (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, DEPR_EXPENSE)
         values (A_ASSET_ID, A_SOB_ID, L_CUR_MONTH,
           CASE WHEN L_MONTHLOOP <> (A_LIFE - 1) then L_MONTHLY_OBL else L_MONTHLY_OBL + L_ROUND end);
        end loop;
       end if;

      end if;

      A_MSG := 'OK';
   end P_DEPR_SLE;

   procedure P_DEPR_FERC( A_LS_ASSET_ID in number, A_REVISION in number,
                  A_ASSET_ID in number, A_SOB_ID in number,
                  A_MONTH date, A_MSG out varchar2)
   is
   begin
      A_MSG := 'DELETING from depr_method_sle for current month through end of lease';
      delete from DEPR_METHOD_SLE
      where ASSET_ID = A_ASSET_ID
      and SET_OF_BOOKS_ID = A_SOB_ID;

    A_MSG := 'INSERTING depreciation expense for FERC Method';
    insert into DEPR_METHOD_SLE
      (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, DEPR_EXPENSE)
      select A_ASSET_ID, A_SOB_ID, LAS."MONTH", LAS.PRINCIPAL_ACCRUAL
       from LS_ASSET_SCHEDULE LAS
      where LAS.LS_ASSET_ID = A_LS_ASSET_ID
        and LAS.REVISION = A_REVISION
        and LAS.SET_OF_BOOKS_ID = A_SOB_ID;


      A_MSG := 'OK';
   end P_DEPR_FERC;

   --**************************************************************************
   --                            SET_ILR_ID
   --**************************************************************************

   procedure P_SET_ILR_ID(A_ILR_ID number) is
   begin
      L_ILR_ID := A_ILR_ID;
   end P_SET_ILR_ID;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   -- Function to perform specific Leased Asset Adjustment
   function F_ADJUST_ASSET(A_LS_ASSET   in LS_ASSET%rowtype,
                           A_REVISION   in number,
                           A_SOB        in number,
                           A_OBL_ADJ    out number,
                           A_LT_OBL_ADJ out number,
                           A_ASSET_ID   out number) return varchar2 is
      L_MSG                     varchar2(2000);
      L_ASSET_SCHEDULE_MONTH    LS_ASSET_SCHEDULE%rowtype;
      L_ASSET_SCHEDULE_APPROVED LS_ASSET_SCHEDULE%rowtype;
      L_MONTH                   date;
   begin
      
	  L_MSG := 'Getting the current open month for lessee';
      select greatest(min(LPC.GL_POSTING_MO_YR),min(LAS.MONTH))
        into L_MONTH
        from LS_PROCESS_CONTROL LPC, LS_ASSET_SCHEDULE LAS
       where LPC.LAM_CLOSED is null
         and LPC.COMPANY_ID = A_LS_ASSET.COMPANY_ID
		 and LAS.LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID
		 and LAS.REVISION = A_REVISION;

      L_MSG := 'Starting to retrieve leased asset schedule for the month';
      --get the asset_schedule for the first month
      select AA.*
        into L_ASSET_SCHEDULE_MONTH
        from LS_ASSET_SCHEDULE AA
       where AA.LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID
         and AA.REVISION = A_REVISION
         and AA.SET_OF_BOOKS_ID = A_SOB
         and aa."MONTH" = L_MONTH;

      L_MSG := 'Starting to retrieve leased asset schedule for the approved revision';
      --get the asset_schedule for the first month
      select AA.*
        into L_ASSET_SCHEDULE_APPROVED
        from LS_ASSET_SCHEDULE AA
       where AA.LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID
         and AA.REVISION = A_LS_ASSET.APPROVED_REVISION
         and AA.SET_OF_BOOKS_ID = A_SOB
         and aa."MONTH" = L_MONTH;

      A_OBL_ADJ    := L_ASSET_SCHEDULE_MONTH.BEG_OBLIGATION -
                      L_ASSET_SCHEDULE_APPROVED.BEG_OBLIGATION;
      A_LT_OBL_ADJ := L_ASSET_SCHEDULE_MONTH.BEG_LT_OBLIGATION -
                      L_ASSET_SCHEDULE_APPROVED.BEG_LT_OBLIGATION;

      L_MSG := 'Get the asset id';
      select A.ASSET_ID
        into A_ASSET_ID
        from (select ASSET_ID,
                     ROW_NUMBER() OVER(partition by LS_ASSET_ID order by EFFECTIVE_DATE desc) as THE_ROW
                from LS_CPR_ASSET_MAP
               where LS_ASSET_ID = A_LS_ASSET.LS_ASSET_ID) A
       where A.THE_ROW = 1;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_ADJUST_ASSET;


   function F_GET_INIT_LIFE
   (
    A_LS_ASSET_ID LS_ASSET.LS_ASSET_ID%type,
    A_REVISION number,
    A_ILR_ID LS_ILR.ILR_ID%type,
    A_ECONOMIC_LIFE number
   ) return number
   is
    L_INIT_LIFE    number;
    L_OWNER_TRF    number;
   begin
     -- get the initial life (all set of books have the same remaining life
    select the_count
    into L_INIT_LIFE
    from
    (
      select count(1) the_count, set_of_books_id
      from ls_asset_schedule
      where ls_asset_id = a_ls_asset_id
      and revision = A_REVISION
      group by set_of_books_id
    )
    where rownum = 1
    ;

    select case when purchase_option_type_id = 1 then 0 else 1 end
    into L_OWNER_TRF
    from ls_ilr_options
    where ilr_id = a_ilr_id
    and revision = a_revision
    ;

    -- if there is an ownership transfer, the init life (depreciable life)
    -- should be based on the economic life of the asset and not the lease term length
    if L_OWNER_TRF = 1 then
      L_INIT_LIFE := A_ECONOMIC_LIFE;
    end if;

    return L_INIT_LIFE;
   end F_GET_INIT_LIFE;

   -- Function to perform specific Leased Asset addition
   function F_ADD_ASSET(A_LS_ASSET_ID number) return varchar2 is

      L_MSG            varchar2(2000);
      L_DG_ID          DEPR_GROUP.DEPR_GROUP_ID%type;
      L_LS_ASSET       LS_ASSET%rowtype;
      L_LS_PEND_TRANS  LS_PEND_TRANSACTION%rowtype;
      L_ASSET_SCHEDULE LS_ASSET_SCHEDULE%rowtype;
      L_MAJOR_LOC      MAJOR_LOCATION.MAJOR_LOCATION_ID%type;
      L_PROP_UNIT      PROPERTY_UNIT.PROPERTY_UNIT_ID%type;
      L_LOC_TYPE       LOCATION_TYPE.LOCATION_TYPE_ID%type;
      L_ASSET_ID       CPR_LEDGER.ASSET_ID%type;
      L_SECOND_COST    CPR_LEDGER.SECOND_FINANCIAL_COST%type;
      L_GL_ACCT_ID     GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_ST_ACCT_ID    GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_LT_ACCT_ID    GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_ASSET_ACT_ID   number;
      L_ACT_CODE       CPR_ACTIVITY.ACTIVITY_CODE%type;
      L_FERC_ACT_CODE  CPR_ACTIVITY.FERC_ACTIVITY_CODE%type;
      L_CHECK          number;
      L_ADJ            number;
      L_RTN            number;
      L_OBL_ADJ        number;
      L_LT_OBL_ADJ     number;
    L_INIT_LIFE    number;
      L_MONTH          date;
      L_MID_PERIOD_METHOD varchar2(50);
   begin
      L_MSG := 'Starting to add leased asset';

      -- ls_asset
      select * into L_LS_ASSET from LS_ASSET where LS_ASSET_ID = A_LS_ASSET_ID;

      -- ls_pend_transaction
      select * into L_LS_PEND_TRANS from LS_PEND_TRANSACTION where LS_ASSET_ID = A_LS_ASSET_ID;

	  L_MSG := 'Getting the current open month for lessee';
      select greatest(min(LPC.GL_POSTING_MO_YR),min(LAS.MONTH))
        into L_MONTH
        from LS_PROCESS_CONTROL LPC, LS_ASSET_SCHEDULE LAS
       where LPC.LAM_CLOSED is null
         and LPC.COMPANY_ID = L_LS_ASSET.COMPANY_ID
		 and LAS.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
		 and LAS.REVISION = L_LS_PEND_TRANS.REVISION;

    l_init_life := F_GET_INIT_LIFE(A_LS_ASSET_ID, L_LS_PEND_TRANS.revision,
                    L_LS_ASSET.ilr_id, L_LS_ASSET.economic_life);

      -- adding an asset if the count returns 0 (meaning it exists in the CPR) and there is an approved revision
      if L_LS_PEND_TRANS.ACTIVITY_CODE = 11 then
         L_ACT_CODE      := 'UADJ';
         L_ADJ           := 1;
         L_FERC_ACT_CODE := 3;

         -- this is an ADJUSTMENT
         -- figure out the adjustment amounts.
         L_MSG := F_ADJUST_ASSET(L_LS_ASSET,
                                 L_LS_PEND_TRANS.REVISION,
                                 1,
                                 L_OBL_ADJ,
                                 L_LT_OBL_ADJ,
                                 L_ASSET_ID);
         if L_MSG <> 'OK' then
            return L_MSG;
         end if;

         L_LS_PEND_TRANS.POSTING_QUANTITY := 0;
      else
         --Generate asset id
         --Hopefully use something marginally better than pwrplant1
         select PWRPLANT1.NEXTVAL into L_ASSET_ID from DUAL;

         L_ACT_CODE      := 'UADD';
         L_ADJ           := 0;
         L_FERC_ACT_CODE := 1;
         L_ASSET_ACT_ID  := 1;
      end if;

      -- gl_account
      select IA.CAP_ASSET_ACCOUNT_ID, IA.ST_OBLIG_ACCOUNT_ID, IA.LT_OBLIG_ACCOUNT_ID
        into L_GL_ACCT_ID, L_ST_ACCT_ID, L_LT_ACCT_ID
        from LS_ILR_ACCOUNT IA, LS_ASSET LA
       where LA.ILR_ID = IA.ILR_ID
         and LA.LS_ASSET_ID = A_LS_ASSET_ID;

      -- major_location
      select MAJOR_LOCATION_ID
        into L_MAJOR_LOC
        from ASSET_LOCATION
       where ASSET_LOCATION_ID = L_LS_ASSET.ASSET_LOCATION_ID;

      -- location_type
      select LOCATION_TYPE_ID
        into L_LOC_TYPE
        from MAJOR_LOCATION
       where MAJOR_LOCATION_ID = L_MAJOR_LOC;

      -- property_unit
      select PROPERTY_UNIT_ID
        into L_PROP_UNIT
        from RETIREMENT_UNIT
       where RETIREMENT_UNIT_ID = L_LS_PEND_TRANS.RETIREMENT_UNIT_ID;

      L_MSG   := 'Finding depr group';
      L_DG_ID := PP_DEPR_PKG.F_FIND_DEPR_GROUP(L_LS_PEND_TRANS.COMPANY_ID,
                                               L_GL_ACCT_ID,
                                               L_MAJOR_LOC,
                                               L_LS_PEND_TRANS.UTILITY_ACCOUNT_ID,
                                               L_LS_PEND_TRANS.BUS_SEGMENT_ID,
                                               L_LS_PEND_TRANS.SUB_ACCOUNT_ID,
                                               -100, --SUBLEDGER_TYPE_ID
                                               TO_NUMBER(TO_CHAR(NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE,
                                                                     L_MONTH),
                                                                 'YYYY')),
                                               L_LS_PEND_TRANS.ASSET_LOCATION_ID,
                                               L_LOC_TYPE,
                                               L_LS_PEND_TRANS.RETIREMENT_UNIT_ID,
                                               L_PROP_UNIT,
                                               -1, --A_CLASS_CODE_ID
                                               'NO CLASS CODE'); --A_CC_VALUE
      if L_DG_ID in (-1, -9) then
         -- we canot save... set error_message on ls_pend_transactions
         return 'ERROR: Finding Depreciation Group (' || TO_CHAR(NVL(L_LS_PEND_TRANS.COMPANY_ID,
                                                                     '')) || ' : ' || TO_CHAR(NVL(L_GL_ACCT_ID,
                                                                                                  '')) || ' : ' || TO_CHAR(NVL(L_MAJOR_LOC,
                                                                                                                               '')) || ' : ' || TO_CHAR(NVL(L_LS_PEND_TRANS.UTILITY_ACCOUNT_ID,
                                                                                                                                                            '')) || ' : ' || TO_CHAR(NVL(L_LS_PEND_TRANS.BUS_SEGMENT_ID,
                                                                                                                                                                                         '')) || ' : ' || TO_CHAR(NVL(L_LS_PEND_TRANS.SUB_ACCOUNT_ID,
                                                                                                                                                                                                                      '')) || ' : -100 : ' || TO_CHAR(NVL(TO_CHAR(NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE,
                                                                                                                                                                                                                                                                      L_MONTH),
                                                                                                                                                                                                                                                                  'YYYY'),
                                                                                                                                                                                                                                                          '')) || ' : ' || TO_CHAR(NVL(L_LS_PEND_TRANS.ASSET_LOCATION_ID,
                                                                                                                                                                                                                                                                                       '')) || ' : ' || TO_CHAR(NVL(L_LOC_TYPE,
                                                                                                                                                                                                                                                                                                                    '')) || ' : ' || TO_CHAR(NVL(L_LS_PEND_TRANS.RETIREMENT_UNIT_ID,
                                                                                                                                                                                                                                                                                                                                                 '')) || ' : ' || TO_CHAR(NVL(L_PROP_UNIT,
                                                                                                                                                                                                                                                                                                                                                                              ''));
      end if;

      -- check to make sure the depr group has subledger_type_id = -100
      L_CHECK := 0;
      select count(1)
        into L_CHECK
        from DEPR_GROUP
       where DEPR_GROUP_ID = L_DG_ID
         and SUBLEDGER_TYPE_ID = -100;

      if L_CHECK <> 1 then
         return 'ERROR: Depreciation Group is not a lease depreciation group.  Fix the depreciation group control tree: ' || TO_CHAR(L_DG_ID);
      end if;

      L_MSG          := 'Get the next activity id';
      L_ASSET_ACT_ID := PP_CPR_PKG.F_GET_ASSET_ACTIVITY_ID(L_ASSET_ID, L_DG_ID);

      select UPPER(trim(MID_PERIOD_METHOD))
      into L_MID_PERIOD_METHOD
      from DEPR_GROUP
      where DEPR_GROUP_ID = L_DG_ID;

      for L_SOBS in (select PB.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID, PB.POSTING_AMOUNT as AMOUNT
                       from LS_PEND_SET_OF_BOOKS PB
                      where PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID)
      loop
         --get the asset_schedule for the first month
         select AA.*
           into L_ASSET_SCHEDULE
           from LS_ASSET_SCHEDULE AA,
                (select A.*,
                        ROW_NUMBER() OVER(partition by A.LS_ASSET_ID, A.SET_OF_BOOKS_ID, A.REVISION order by month) as THE_ROW
                   from LS_ASSET_SCHEDULE A
                  where A.LS_ASSET_ID = A_LS_ASSET_ID
                    and A.REVISION = L_LS_PEND_TRANS.REVISION
                    and A.SET_OF_BOOKS_ID = L_SOBS.SET_OF_BOOKS_ID) B
          where B.THE_ROW = 1
            and AA.LS_ASSET_ID = B.LS_ASSET_ID
            and AA.REVISION = B.REVISION
            and AA.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID
            and aa."MONTH" = b."MONTH";

      if L_MID_PERIOD_METHOD = 'SLE' then
      -- load SLE table for depreciation
         P_DEPR_SLE( A_LS_ASSET_ID, L_LS_PEND_TRANS.REVISION, L_ASSET_ID, L_SOBS.SET_OF_BOOKS_ID,
          L_MONTH, l_init_life, L_MSG );
         if L_MSG <> 'OK' then
            return L_MSG;
      end if;
      elsif L_MID_PERIOD_METHOD = 'FERC' then
         P_DEPR_FERC( A_LS_ASSET_ID, L_LS_PEND_TRANS.REVISION, L_ASSET_ID, L_SOBS.SET_OF_BOOKS_ID,
          L_MONTH, L_MSG );
         if L_MSG <> 'OK' then
            return L_MSG;
         end if;
      end if;

         if L_ADJ = 1 then
            -- this is an ADJUSTMENT
            -- figure out the adjustment amounts.
            L_MSG := F_ADJUST_ASSET(L_LS_ASSET,
                                    L_LS_PEND_TRANS.REVISION,
                                    L_SOBS.SET_OF_BOOKS_ID,
                                    L_OBL_ADJ,
                                    L_LT_OBL_ADJ,
                                    L_ASSET_ID);
            if L_MSG <> 'OK' then
               return L_MSG;
            end if;

--            L_ASSET_SCHEDULE.BEG_OBLIGATION    := L_OBL_ADJ;
--            L_ASSET_SCHEDULE.BEG_LT_OBLIGATION := L_LT_OBL_ADJ;
            L_ASSET_SCHEDULE.BEG_OBLIGATION     := L_SOBS.AMOUNT;
            L_ASSET_SCHEDULE.BEG_LT_OBLIGATION  := 0;
         end if;

         -- Process by set of books
         -- process JEs 3001, 3002, and 3003
         L_MSG := 'Creating Journal (Leased Asset Addition Debit)';
         if L_SOBS.AMOUNT <> 0 and G_SEND_JES then
            L_RTN := PKG_LEASE_COMMON.F_BOOKJE(A_LS_ASSET_ID,
                                               3001,
                                               L_SOBS.AMOUNT,
                                               L_ASSET_ACT_ID,
                                               L_DG_ID,
                                               L_LS_PEND_TRANS.WORK_ORDER_ID,
                                               L_GL_ACCT_ID,
                                               0,
                                               L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                               L_LS_PEND_TRANS.COMPANY_ID,
                                               L_MONTH,
                                               1,
                                               L_LS_PEND_TRANS.GL_JE_CODE,
                                               L_SOBS.SET_OF_BOOKS_ID,
                                               L_MSG);
            if L_RTN = -1 then
               return L_MSG;
            end if;

            L_MSG := 'Creating Journal (Leased Asset Addition ST Obligation Credit)';
            if L_ASSET_SCHEDULE.BEG_OBLIGATION - NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0) <> 0 then
               L_RTN := PKG_LEASE_COMMON.F_BOOKJE(A_LS_ASSET_ID,
                                                  3002,
                                                  L_ASSET_SCHEDULE.BEG_OBLIGATION -
                                                  NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0),
                                                  L_ASSET_ACT_ID,
                                                  L_DG_ID,
                                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                  L_ST_ACCT_ID,
                                                  0,
                                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                  L_LS_PEND_TRANS.COMPANY_ID,
                                                  L_MONTH,
                                                  0,
                                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                                  L_SOBS.SET_OF_BOOKS_ID,
                                                  L_MSG);
               if L_RTN = -1 then
                  return L_MSG;
               end if;
            end if; -- short term JE

            L_MSG := 'Creating Journal (Leased Asset Addition LT Obligation Credit)';
            if NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0) <> 0 then
               L_RTN := PKG_LEASE_COMMON.F_BOOKJE(A_LS_ASSET_ID,
                                                  3003,
                                                  NVL(L_ASSET_SCHEDULE.BEG_LT_OBLIGATION, 0),
                                                  L_ASSET_ACT_ID,
                                                  L_DG_ID,
                                                  L_LS_PEND_TRANS.WORK_ORDER_ID,
                                                  L_LT_ACCT_ID,
                                                  0,
                                                  L_LS_PEND_TRANS.LS_PEND_TRANS_ID,
                                                  L_LS_PEND_TRANS.COMPANY_ID,
                                                  L_MONTH,
                                                  0,
                                                  L_LS_PEND_TRANS.GL_JE_CODE,
                                                  L_SOBS.SET_OF_BOOKS_ID,
                                                  L_MSG);
               if L_RTN = -1 then
                  return L_MSG;
               end if;
            end if; -- long term JE
         end if; -- process JEs for this SOB
      end loop; -- END Process by set of books

      L_SECOND_COST := 0;

      select count(*) into L_SECOND_COST from SET_OF_BOOKS S where S.SECOND_SET_OF_BOOKS_IND = 1;

      if L_SECOND_COST > 0 then
         L_MSG := 'Retrieving second cost';
         select POSTING_AMOUNT
           into L_SECOND_COST
           from SET_OF_BOOKS S, LS_PEND_SET_OF_BOOKS PB
          where S.SECOND_SET_OF_BOOKS_IND = 1
            and PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID
            and S.SET_OF_BOOKS_ID = PB.SET_OF_BOOKS_ID;
      end if;

      -- if it's on pend transaction, take it from there
      L_MSG := 'Adding asset to CPR Ledger';
      merge into CPR_LEDGER A
      using (select L_ASSET_ID as ASSET_ID,
                    L_LS_PEND_TRANS.PROPERTY_GROUP_ID as PROPERTY_GROUP_ID,
                    L_DG_ID as DEPR_GROUP_ID,
                    L_LS_PEND_TRANS.RETIREMENT_UNIT_ID as RETIREMENT_UNIT_ID,
                    L_LS_PEND_TRANS.BUS_SEGMENT_ID as BUS_SEGMENT_ID,
                    L_LS_PEND_TRANS.COMPANY_ID as COMPANY_ID,
                    L_LS_PEND_TRANS.FUNC_CLASS_ID as FUNC_CLASS_ID,
                    L_LS_PEND_TRANS.UTILITY_ACCOUNT_ID as UTILITY_ACCOUNT_ID,
                    L_GL_ACCT_ID as GL_ACCOUNT_ID,
                    L_LS_PEND_TRANS.ASSET_LOCATION_ID as ASSET_LOCATION_ID,
                    L_LS_PEND_TRANS.SUB_ACCOUNT_ID as SUB_ACCOUNT_ID,
                    WOC.WORK_ORDER_NUMBER as WORK_ORDER_NUMBER,
                    NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE, L_MONTH) as IN_SERVICE_DATE,
                    L_LS_PEND_TRANS.POSTING_QUANTITY as POSTING_QUANTITY,
                    L_LS_PEND_TRANS.POSTING_AMOUNT as POSTING_AMOUNT,
                    L_LS_ASSET.DESCRIPTION as DESCRIPTION,
                    L_LS_ASSET.LONG_DESCRIPTION as LONG_DESCRIPTION,
                    L_LS_PEND_TRANS.SERIAL_NUMBER as SERIAL_NUMBER,
                    case
                       when L_SECOND_COST > 0 then
                        L_LS_PEND_TRANS.POSTING_AMOUNT
                       else
                        0
                    end as SECOND_COST
               from WORK_ORDER_CONTROL WOC
              where WOC.WORK_ORDER_ID = L_LS_PEND_TRANS.WORK_ORDER_ID) B
      on (A.ASSET_ID = B.ASSET_ID)
      when matched then
         update
            set A.ACCUM_QUANTITY = A.ACCUM_QUANTITY + B.POSTING_QUANTITY,
                A.ACCUM_COST = A.ACCUM_COST + B.POSTING_AMOUNT,
                A.SECOND_FINANCIAL_COST = A.SECOND_FINANCIAL_COST + B.SECOND_COST
      when not matched then
         insert
            (ASSET_ID, PROPERTY_GROUP_ID, DEPR_GROUP_ID, BOOKS_SCHEMA_ID, RETIREMENT_UNIT_ID,
             BUS_SEGMENT_ID, COMPANY_ID, FUNC_CLASS_ID, UTILITY_ACCOUNT_ID, GL_ACCOUNT_ID,
             ASSET_LOCATION_ID, SUB_ACCOUNT_ID, WORK_ORDER_NUMBER, LEDGER_STATUS, IN_SERVICE_YEAR,
             ACCUM_QUANTITY, ACCUM_COST, SUBLEDGER_INDICATOR, DESCRIPTION, LONG_DESCRIPTION,
             ENG_IN_SERVICE_YEAR, SERIAL_NUMBER, ACCUM_COST_2, SECOND_FINANCIAL_COST)
         values
            (B.ASSET_ID, B.PROPERTY_GROUP_ID, B.DEPR_GROUP_ID, 1, B.RETIREMENT_UNIT_ID,
             B.BUS_SEGMENT_ID, B.COMPANY_ID, B.FUNC_CLASS_ID, B.UTILITY_ACCOUNT_ID, B.GL_ACCOUNT_ID,
             B.ASSET_LOCATION_ID, B.SUB_ACCOUNT_ID, B.WORK_ORDER_NUMBER, 1, B.IN_SERVICE_DATE,
             B.POSTING_QUANTITY, B.POSTING_AMOUNT, -100, B.DESCRIPTION, B.LONG_DESCRIPTION,
             B.IN_SERVICE_DATE, B.SERIAL_NUMBER, 0, B.SECOND_COST);

      L_MSG := 'Adding CPR Activity';
      insert into CPR_ACTIVITY
         (ASSET_ID, ASSET_ACTIVITY_ID, GL_POSTING_MO_YR, CPR_POSTING_MO_YR, WORK_ORDER_NUMBER,
          GL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION, ACTIVITY_CODE, ACTIVITY_STATUS,
          ACTIVITY_QUANTITY, ACTIVITY_COST, FERC_ACTIVITY_CODE, MONTH_NUMBER, ACTIVITY_COST_2,
          ACT_SECOND_FINANCIAL_COST)
         select L_ASSET_ID,
                L_ASSET_ACT_ID,
                L_MONTH, --gl_posting_mo_yr
                L_MONTH, --cpr_posting_mo_yr
                WOC.WORK_ORDER_NUMBER,
                L_LS_PEND_TRANS.GL_JE_CODE,
                L_LS_ASSET.DESCRIPTION,
                L_LS_ASSET.LONG_DESCRIPTION,
                L_ACT_CODE, --Activity Code
                L_LS_PEND_TRANS.LS_PEND_TRANS_ID, --activity_status (this is the pend_trans_id)
                L_LS_PEND_TRANS.POSTING_QUANTITY,
                L_LS_PEND_TRANS.POSTING_AMOUNT,
                L_FERC_ACT_CODE, --ferc_activity_code (addition)
                TO_NUMBER(TO_CHAR(L_MONTH, 'yyyymm')), --month_number
                0,
                case
                   when L_SECOND_COST > 0 then
                    L_LS_PEND_TRANS.POSTING_AMOUNT
                   else
                    0
                end
           from WORK_ORDER_CONTROL WOC
          where WOC.WORK_ORDER_ID = L_LS_PEND_TRANS.WORK_ORDER_ID;

      L_MSG := 'Adding to activity basis table';
      insert into CPR_ACT_BASIS
         (ASSET_ID, ASSET_ACTIVITY_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6,
          BASIS_7, BASIS_8, BASIS_9, BASIS_10, BASIS_11, BASIS_12, BASIS_13, BASIS_14, BASIS_15,
          BASIS_16, BASIS_17, BASIS_18, BASIS_19, BASIS_20, BASIS_21, BASIS_22, BASIS_23, BASIS_24,
          BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32, BASIS_33,
          BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41, BASIS_42,
          BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50, BASIS_51,
          BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60,
          BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68, BASIS_69,
          BASIS_70)
         select L_ASSET_ID,
                L_ASSET_ACT_ID,
                BASIS_1,
                BASIS_2,
                BASIS_3,
                BASIS_4,
                BASIS_5,
                BASIS_6,
                BASIS_7,
                BASIS_8,
                BASIS_9,
                BASIS_10,
                BASIS_11,
                BASIS_12,
                BASIS_13,
                BASIS_14,
                BASIS_15,
                BASIS_16,
                BASIS_17,
                BASIS_18,
                BASIS_19,
                BASIS_20,
                BASIS_21,
                BASIS_22,
                BASIS_23,
                BASIS_24,
                BASIS_25,
                BASIS_26,
                BASIS_27,
                BASIS_28,
                BASIS_29,
                BASIS_30,
                BASIS_31,
                BASIS_32,
                BASIS_33,
                BASIS_34,
                BASIS_35,
                BASIS_36,
                BASIS_37,
                BASIS_38,
                BASIS_39,
                BASIS_40,
                BASIS_41,
                BASIS_42,
                BASIS_43,
                BASIS_44,
                BASIS_45,
                BASIS_46,
                BASIS_47,
                BASIS_48,
                BASIS_49,
                BASIS_50,
                BASIS_51,
                BASIS_52,
                BASIS_53,
                BASIS_54,
                BASIS_55,
                BASIS_56,
                BASIS_57,
                BASIS_58,
                BASIS_59,
                BASIS_60,
                BASIS_61,
                BASIS_62,
                BASIS_63,
                BASIS_64,
                BASIS_65,
                BASIS_66,
                BASIS_67,
                BASIS_68,
                BASIS_69,
                BASIS_70
           from LS_PEND_BASIS
          where LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID;

      L_MSG := 'Adding to ledger basis table';
      merge into CPR_LDG_BASIS A
      using (select L_ASSET_ID as ASSET_ID,
                    BASIS_1,
                    BASIS_2,
                    BASIS_3,
                    BASIS_4,
                    BASIS_5,
                    BASIS_6,
                    BASIS_7,
                    BASIS_8,
                    BASIS_9,
                    BASIS_10,
                    BASIS_11,
                    BASIS_12,
                    BASIS_13,
                    BASIS_14,
                    BASIS_15,
                    BASIS_16,
                    BASIS_17,
                    BASIS_18,
                    BASIS_19,
                    BASIS_20,
                    BASIS_21,
                    BASIS_22,
                    BASIS_23,
                    BASIS_24,
                    BASIS_25,
                    BASIS_26,
                    BASIS_27,
                    BASIS_28,
                    BASIS_29,
                    BASIS_30,
                    BASIS_31,
                    BASIS_32,
                    BASIS_33,
                    BASIS_34,
                    BASIS_35,
                    BASIS_36,
                    BASIS_37,
                    BASIS_38,
                    BASIS_39,
                    BASIS_40,
                    BASIS_41,
                    BASIS_42,
                    BASIS_43,
                    BASIS_44,
                    BASIS_45,
                    BASIS_46,
                    BASIS_47,
                    BASIS_48,
                    BASIS_49,
                    BASIS_50,
                    BASIS_51,
                    BASIS_52,
                    BASIS_53,
                    BASIS_54,
                    BASIS_55,
                    BASIS_56,
                    BASIS_57,
                    BASIS_58,
                    BASIS_59,
                    BASIS_60,
                    BASIS_61,
                    BASIS_62,
                    BASIS_63,
                    BASIS_64,
                    BASIS_65,
                    BASIS_66,
                    BASIS_67,
                    BASIS_68,
                    BASIS_69,
                    BASIS_70
               from LS_PEND_BASIS
              where LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID) B
      on (A.ASSET_ID = B.ASSET_ID)
      when matched then
         update
            set A.BASIS_1 = A.BASIS_1 + B.BASIS_1, A.BASIS_2 = A.BASIS_2 + B.BASIS_2,
                A.BASIS_3 = A.BASIS_3 + B.BASIS_3, A.BASIS_4 = A.BASIS_4 + B.BASIS_4,
                A.BASIS_5 = A.BASIS_5 + B.BASIS_5, A.BASIS_6 = A.BASIS_6 + B.BASIS_6,
                A.BASIS_7 = A.BASIS_7 + B.BASIS_7, A.BASIS_8 = A.BASIS_8 + B.BASIS_8,
                A.BASIS_9 = A.BASIS_9 + B.BASIS_9, A.BASIS_10 = A.BASIS_10 + B.BASIS_10,
                A.BASIS_11 = A.BASIS_11 + B.BASIS_11, A.BASIS_12 = A.BASIS_12 + B.BASIS_12,
                A.BASIS_13 = A.BASIS_13 + B.BASIS_13, A.BASIS_14 = A.BASIS_14 + B.BASIS_14,
                A.BASIS_15 = A.BASIS_15 + B.BASIS_15, A.BASIS_16 = A.BASIS_16 + B.BASIS_16,
                A.BASIS_17 = A.BASIS_17 + B.BASIS_17, A.BASIS_18 = A.BASIS_18 + B.BASIS_18,
                A.BASIS_19 = A.BASIS_19 + B.BASIS_19, A.BASIS_20 = A.BASIS_20 + B.BASIS_20,
                A.BASIS_21 = A.BASIS_21 + B.BASIS_21, A.BASIS_22 = A.BASIS_22 + B.BASIS_22,
                A.BASIS_23 = A.BASIS_23 + B.BASIS_23, A.BASIS_24 = A.BASIS_24 + B.BASIS_24,
                A.BASIS_25 = A.BASIS_25 + B.BASIS_25, A.BASIS_26 = A.BASIS_26 + B.BASIS_26,
                A.BASIS_27 = A.BASIS_27 + B.BASIS_27, A.BASIS_28 = A.BASIS_28 + B.BASIS_28,
                A.BASIS_29 = A.BASIS_29 + B.BASIS_29, A.BASIS_30 = A.BASIS_30 + B.BASIS_30,
                A.BASIS_31 = A.BASIS_31 + B.BASIS_31, A.BASIS_32 = A.BASIS_32 + B.BASIS_32,
                A.BASIS_33 = A.BASIS_33 + B.BASIS_33, A.BASIS_34 = A.BASIS_34 + B.BASIS_34,
                A.BASIS_35 = A.BASIS_35 + B.BASIS_35, A.BASIS_36 = A.BASIS_36 + B.BASIS_36,
                A.BASIS_37 = A.BASIS_37 + B.BASIS_37, A.BASIS_38 = A.BASIS_38 + B.BASIS_38,
                A.BASIS_39 = A.BASIS_39 + B.BASIS_39, A.BASIS_40 = A.BASIS_40 + B.BASIS_40,
                A.BASIS_41 = A.BASIS_41 + B.BASIS_41, A.BASIS_42 = A.BASIS_42 + B.BASIS_42,
                A.BASIS_43 = A.BASIS_43 + B.BASIS_43, A.BASIS_44 = A.BASIS_44 + B.BASIS_44,
                A.BASIS_45 = A.BASIS_45 + B.BASIS_45, A.BASIS_46 = A.BASIS_46 + B.BASIS_46,
                A.BASIS_47 = A.BASIS_47 + B.BASIS_47, A.BASIS_48 = A.BASIS_48 + B.BASIS_48,
                A.BASIS_49 = A.BASIS_49 + B.BASIS_49, A.BASIS_50 = A.BASIS_50 + B.BASIS_50,
                A.BASIS_51 = A.BASIS_51 + B.BASIS_51, A.BASIS_52 = A.BASIS_52 + B.BASIS_52,
                A.BASIS_53 = A.BASIS_53 + B.BASIS_53, A.BASIS_54 = A.BASIS_54 + B.BASIS_54,
                A.BASIS_55 = A.BASIS_55 + B.BASIS_55, A.BASIS_56 = A.BASIS_56 + B.BASIS_56,
                A.BASIS_57 = A.BASIS_57 + B.BASIS_57, A.BASIS_58 = A.BASIS_58 + B.BASIS_58,
                A.BASIS_59 = A.BASIS_59 + B.BASIS_59, A.BASIS_60 = A.BASIS_60 + B.BASIS_60,
                A.BASIS_61 = A.BASIS_61 + B.BASIS_61, A.BASIS_62 = A.BASIS_62 + B.BASIS_62,
                A.BASIS_63 = A.BASIS_63 + B.BASIS_63, A.BASIS_64 = A.BASIS_64 + B.BASIS_64,
                A.BASIS_65 = A.BASIS_65 + B.BASIS_65, A.BASIS_66 = A.BASIS_66 + B.BASIS_66,
                A.BASIS_67 = A.BASIS_67 + B.BASIS_67, A.BASIS_68 = A.BASIS_68 + B.BASIS_68,
                A.BASIS_69 = A.BASIS_69 + B.BASIS_69, A.BASIS_70 = A.BASIS_70 + B.BASIS_70
      when not matched then
         insert
            (ASSET_ID, BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6, BASIS_7, BASIS_8,
             BASIS_9, BASIS_10, BASIS_11, BASIS_12, BASIS_13, BASIS_14, BASIS_15, BASIS_16, BASIS_17,
             BASIS_18, BASIS_19, BASIS_20, BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25,
             BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32, BASIS_33,
             BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41,
             BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49,
             BASIS_50, BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57,
             BASIS_58, BASIS_59, BASIS_60, BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65,
             BASIS_66, BASIS_67, BASIS_68, BASIS_69, BASIS_70)
         values
            (B.ASSET_ID, B.BASIS_1, B.BASIS_2, B.BASIS_3, B.BASIS_4, B.BASIS_5, B.BASIS_6,
             B.BASIS_7, B.BASIS_8, B.BASIS_9, B.BASIS_10, B.BASIS_11, B.BASIS_12, B.BASIS_13,
             B.BASIS_14, B.BASIS_15, B.BASIS_16, B.BASIS_17, B.BASIS_18, B.BASIS_19, B.BASIS_20,
             B.BASIS_21, B.BASIS_22, B.BASIS_23, B.BASIS_24, B.BASIS_25, B.BASIS_26, B.BASIS_27,
             B.BASIS_28, B.BASIS_29, B.BASIS_30, B.BASIS_31, B.BASIS_32, B.BASIS_33, B.BASIS_34,
             B.BASIS_35, B.BASIS_36, B.BASIS_37, B.BASIS_38, B.BASIS_39, B.BASIS_40, B.BASIS_41,
             B.BASIS_42, B.BASIS_43, B.BASIS_44, B.BASIS_45, B.BASIS_46, B.BASIS_47, B.BASIS_48,
             B.BASIS_49, B.BASIS_50, B.BASIS_51, B.BASIS_52, B.BASIS_53, B.BASIS_54, B.BASIS_55,
             B.BASIS_56, B.BASIS_57, B.BASIS_58, B.BASIS_59, B.BASIS_60, B.BASIS_61, B.BASIS_62,
             B.BASIS_63, B.BASIS_64, B.BASIS_65, B.BASIS_66, B.BASIS_67, B.BASIS_68, B.BASIS_69,
             B.BASIS_70);

      L_MSG := 'Loading cpr_depr';
      merge into CPR_DEPR A
      using (select L_ASSET_ID as ASSET_ID,
                    PB.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID,
                    l_init_life as EXPECTED_LIFE,
                    PB.POSTING_AMOUNT as AMOUNT,
                    DMR.RATE / 12 as MONTH_RATE,
                    L_LS_PEND_TRANS.COMPANY_ID as COMPANY_ID,
                    DG.MID_PERIOD_METHOD as MID_PERIOD_METHOD,
                    DG.MID_PERIOD_CONV as MID_PERIOD_CONV,
                    L_DG_ID as DEPR_GROUP_ID,
                    DMR.DEPR_METHOD_ID as DEPR_METHOD_ID,
                    DG.TRUE_UP_CPR_DEPR as TRUE_UP_CPR_DEPR,
                    L_MONTH as GL_POSTING_MO_YR,
					case when L_LS_ASSET.ESTIMATED_RESIDUAL = 0 
						then L_LS_ASSET.GUARANTEED_RESIDUAL_AMOUNT
						else round(L_LS_ASSET.ESTIMATED_RESIDUAL * L_LS_ASSET.FMV, 2) end as estimated_salvage
               from DEPR_GROUP DG,
                    LS_PEND_SET_OF_BOOKS PB,
                    (select DDD.DEPR_METHOD_ID,
                            DDD.RATE,
                            DDD.SET_OF_BOOKS_ID,
                            DDD.EXPECTED_AVERAGE_LIFE,
                            ROW_NUMBER() OVER(partition by DDD.DEPR_METHOD_ID, DDD.SET_OF_BOOKS_ID order by DDD.EFFECTIVE_DATE desc) as THE_ROW
                       from DEPR_METHOD_RATES DDD) DMR
              where DG.DEPR_GROUP_ID = L_DG_ID
                and DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
                and PB.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
                and PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID
                and DMR.THE_ROW = 1) B
      on (A.ASSET_ID = B.ASSET_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.GL_POSTING_MO_YR = B.GL_POSTING_MO_YR and A.DEPR_GROUP_ID = B.DEPR_GROUP_ID)
      when matched then
         update
            set NET_ADDS_AND_ADJUST = NET_ADDS_AND_ADJUST + B.AMOUNT,
                ASSET_DOLLARS = ASSET_DOLLARS + B.AMOUNT,
				ESTIMATED_SALVAGE = case when (asset_dollars + b.amount) = 0 then 0 
										else B.ESTIMATED_SALVAGE / (asset_dollars + b.amount) end
      when not matched then
         insert
            (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, INIT_LIFE, REMAINING_LIFE,
             ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS, TRANSFERS_IN,
             TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH, SALVAGE_DOLLARS, RESERVE_ADJUSTMENT,
             COST_OF_REMOVAL, RESERVE_TRANS_IN, RESERVE_TRANS_OUT, DEPR_EXP_ADJUST,
             OTHER_CREDITS_AND_ADJUST, GAIN_LOSS, DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE,
             BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE, YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE,
             PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB, MONTH_RATE, COMPANY_ID, MID_PERIOD_METHOD,
             MID_PERIOD_CONV, DEPR_GROUP_ID, DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR,
             SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT,
             IMPAIRMENT_EXPENSE_AMOUNT, IMPAIRMENT_ASSET_ACTIVITY_SALV,
             IMPAIRMENT_ASSET_BEGIN_BALANCE)
         values
            (B.ASSET_ID, B.SET_OF_BOOKS_ID, B.GL_POSTING_MO_YR, B.EXPECTED_LIFE, B.EXPECTED_LIFE,
			 case when b.amount = 0 then 0 else B.ESTIMATED_SALVAGE / b.amount end, 
			 0, B.AMOUNT, 0, 0, 0, B.AMOUNT, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
			 0, 0, 0, 0, 0, 0, 0, 0,
             B.MONTH_RATE, B.COMPANY_ID, B.MID_PERIOD_METHOD, B.MID_PERIOD_CONV, B.DEPR_GROUP_ID, 0,
             B.DEPR_METHOD_ID, B.TRUE_UP_CPR_DEPR, 0, 0, 0, 0, 0, 0, 0);

      L_MSG := 'Updating Account Summary';
      merge into ACCOUNT_SUMMARY A
      using (select L_LS_PEND_TRANS.COMPANY_ID as COMPANY,
                    PB.SET_OF_BOOKS_ID as SET_OF_BOOKS,
                    L_GL_ACCT_ID as ACCOUNT,
                    L_LS_PEND_TRANS.BUS_SEGMENT_ID as BUSINESS_SEGMENT,
                    L_LS_PEND_TRANS.SUB_ACCOUNT_ID as SUB_ACCOUNT,
                    L_LS_PEND_TRANS.UTILITY_ACCOUNT_ID as UTILITY_ACCOUNT,
                    L_MAJOR_LOC as MAJOR_LOCATION,
                    L_MONTH as GL_POSTING_MO_YR, --GL_POSTING_MO_YR
                    (1 - L_ADJ) * PB.POSTING_AMOUNT as ADDITION,
                    L_ADJ * PB.POSTING_AMOUNT as ADJUSTMENTS
               from LS_PEND_SET_OF_BOOKS PB
              where PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID) B
      on (A.COMPANY_ID = B.COMPANY and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS and A.GL_ACCOUNT_ID = B.ACCOUNT and A.BUS_SEGMENT_ID = B.BUSINESS_SEGMENT and A.SUB_ACCOUNT_ID = B.SUB_ACCOUNT and A.UTILITY_ACCOUNT_ID = B.UTILITY_ACCOUNT and A.MAJOR_LOCATION_ID = B.MAJOR_LOCATION and A.GL_POSTING_MO_YR = B.GL_POSTING_MO_YR)
      when matched then
         update
            set A.ENDING_BALANCE = A.ENDING_BALANCE + B.ADDITION + B.ADJUSTMENTS,
                A.ADDITIONS = A.ADDITIONS + B.ADDITION,
                A.ADJUSTMENTS = A.ADJUSTMENTS + B.ADJUSTMENTS
      when not matched then
         insert
            (SET_OF_BOOKS_ID, COMPANY_ID, GL_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID,
             UTILITY_ACCOUNT_ID, MAJOR_LOCATION_ID, GL_POSTING_MO_YR, BEGINNING_BALANCE,
             ACCT_SUMM_STATUS, RETIREMENTS, ADDITIONS, TRANSFERS_IN, TRANSFERS_OUT, ADJUSTMENTS,
             ENDING_BALANCE)
         values
            (B.SET_OF_BOOKS, B.COMPANY, B.ACCOUNT, B.BUSINESS_SEGMENT, B.SUB_ACCOUNT,
             B.UTILITY_ACCOUNT, B.MAJOR_LOCATION, B.GL_POSTING_MO_YR, 0, 1, 0, B.ADDITION,
             0, 0, B.ADJUSTMENTS, B.ADDITION + B.ADJUSTMENTS);

      L_MSG := 'Updating Depr Ledger';
      merge into DEPR_LEDGER A
      using (select DG2.DEPR_GROUP_ID as DEPR_GROUP,
                    PB.SET_OF_BOOKS_ID as SET_OF_BOOKS,
                    L_MONTH as GL_POSTING_MO_YR, --GL_POSTING_MO_YR
                    (1 - L_ADJ) * PB.POSTING_AMOUNT as ADDITION,
                    L_ADJ * PB.POSTING_AMOUNT as ADJUSTMENTS
               from LS_PEND_SET_OF_BOOKS PB,
                    (select DG.DEPR_GROUP_ID,
                            DG.COMPANY_ID,
                            DMR.SET_OF_BOOKS_ID,
                            ROW_NUMBER() OVER(partition by DMR.SET_OF_BOOKS_ID, DMR.DEPR_METHOD_ID order by DMR.EFFECTIVE_DATE desc) as THE_ROW
                       from DEPR_METHOD_RATES DMR, DEPR_GROUP DG
                      where DMR.DEPR_METHOD_ID = DG.DEPR_METHOD_ID
                        and DG.DEPR_GROUP_ID = L_DG_ID) DG2
              where DG2.THE_ROW = 1
                and PB.SET_OF_BOOKS_ID = DG2.SET_OF_BOOKS_ID
                and PB.LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID) B
      on (A.DEPR_GROUP_ID = B.DEPR_GROUP and A.GL_POST_MO_YR = B.GL_POSTING_MO_YR and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS)
      when matched then
         update
            set A.END_BALANCE = A.END_BALANCE + B.ADDITION + B.ADJUSTMENTS,
                A.ADDITIONS = A.ADDITIONS + B.ADDITION,
                A.ADJUSTMENTS = A.ADJUSTMENTS + B.ADJUSTMENTS;

      -- set the approved revision
      update LS_ASSET
         set APPROVED_REVISION = L_LS_PEND_TRANS.REVISION,
             IN_SERVICE_DATE = NVL(L_LS_PEND_TRANS.IN_SERVICE_DATE, L_MONTH), LS_ASSET_STATUS_ID = 3,
          EXPECTED_LIFE = L_INIT_LIFE
       where LS_ASSET_ID = A_LS_ASSET_ID;

      -- only do the below on initial adds
      if L_ADJ = 0 then
         L_MSG := 'Adding class codes';
         insert into CLASS_CODE_CPR_LEDGER
            (CLASS_CODE_ID, ASSET_ID, value)
            select CLASS_CODE_ID, L_ASSET_ID, value
              from LS_PEND_CLASS_CODE
             where LS_PEND_TRANS_ID = L_LS_PEND_TRANS.LS_PEND_TRANS_ID;

         --* Link the LS_ASSET to the CPR
         L_MSG := 'Connecting leased asset to CPR';
         insert into LS_CPR_ASSET_MAP
            (LS_ASSET_ID, ASSET_ID, EFFECTIVE_DATE)
         values
            (L_LS_PEND_TRANS.LS_ASSET_ID, L_ASSET_ID, L_MONTH);

         L_MSG := 'Connecting activity and depr group';
         insert into CPR_ACT_DEPR_GROUP
            (ASSET_ID, GL_POSTING_MO_YR, DEPR_GROUP_ID)
         values
            (L_ASSET_ID, L_MONTH, L_DG_ID);
      end if; -- adjustment

      L_MSG := 'OK';
      return L_MSG;
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_ADD_ASSET;

   -- Function to perform specific Leased Asset retirement
   -- return 1 on success, 0 on failure.  a_msg is an out param for messaging
   function F_RETIRE_ASSET(A_CPR_ASSET_ID        number,
                           A_ACCT_MONTH          date,
                           A_MONTH               date,
                           A_RETIREMENT_AMOUNT   number,
                           A_RETIREMENT_QUANTITY number,
                           A_GAIN_LOSS_AMOUNT    number,
                           A_DG_ID               number,
                           A_PT_ID               number,
                           A_MSG                 out varchar2) return number is
      L_LS_ASSET     LS_ASSET%rowtype;
      L_LS_ASSET_SCH LS_ASSET_SCHEDULE%rowtype;
      L_GL_ACCT_ID     GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_ST_ACCT_ID    GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_LT_ACCT_ID    GL_ACCOUNT.GL_ACCOUNT_ID%type;
      L_AMT_OBL      number(22, 2);
      L_PCT          number(22, 12);
      L_RTN          number;
      L_GL_JE_CODE   varchar2(35);
      L_ILR_ID       number;
      L_STATUS       varchar2(4000);
      L_SQLS         varchar2(2000);
	  L_SCH_MONTH	date;
    L_ASSET_IDS   PKG_LEASE_CALC.NUM_ARRAY;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_MSG := 'Starting to retire the leased asset for month: ' || TO_CHAR(A_MONTH, 'yyyymm');
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Accounting Month: ' || TO_CHAR(A_ACCT_MONTH, 'yyyymm');
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Quantity: ' || TO_CHAR(A_RETIREMENT_QUANTITY);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Amount: ' || TO_CHAR(A_RETIREMENT_AMOUNT);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Gain / Loss: ' || TO_CHAR(A_GAIN_LOSS_AMOUNT);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Associated Pending Transaction: ' || TO_CHAR(A_PT_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      --Get our LS_ASSET
      A_MSG := 'Get the leased asset record';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      select A.*
        into L_LS_ASSET
        from LS_ASSET A, LS_CPR_ASSET_MAP C
       where C.ASSET_ID = A_CPR_ASSET_ID
         and C.LS_ASSET_ID = A.LS_ASSET_ID;

      select IA.CAP_ASSET_ACCOUNT_ID, IA.ST_OBLIG_ACCOUNT_ID, IA.LT_OBLIG_ACCOUNT_ID
        into L_GL_ACCT_ID, L_ST_ACCT_ID, L_LT_ACCT_ID
        from LS_ILR_ACCOUNT IA, LS_ASSET LA
       where LA.ILR_ID = IA.ILR_ID
         and LA.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID;

      -- is this asset fully retired?
      if L_LS_ASSET.QUANTITY + A_RETIREMENT_QUANTITY = 0 then
         -- fully retired
         -- set the status to say retired
         L_LS_ASSET.LS_ASSET_STATUS_ID := 4;
      end if;

      L_PCT := -1 * A_RETIREMENT_QUANTITY / L_LS_ASSET.QUANTITY;

      A_MSG := 'Updating LS ASSET';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      -- update the capital cost and the quantity based on the retirement amount
      update LS_ASSET
         set CAPITALIZED_COST = CAPITALIZED_COST + L_LS_ASSET.CAPITALIZED_COST,
             QUANTITY = QUANTITY + A_RETIREMENT_QUANTITY,
             LS_ASSET_STATUS_ID = L_LS_ASSET.LS_ASSET_STATUS_ID, RETIREMENT_DATE = A_MONTH
       where LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID;

      -- normal asset retirement transactions are handled by POST
      -- ie 3025 and 3026.
      -- in this function only handle the lease specific entries (termination_penalty_amount: 3036 and 3037)

      select NVL(E.EXTERNAL_JE_CODE, E.GL_JE_CODE)
        into L_GL_JE_CODE
        from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
       where E.JE_ID = G.JE_ID
         and G.PROCESS_ID = 'LAM RETIREMENTS';

      -- loop over the set of books eligible for this asset
      A_MSG := 'Looping over set of books';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      for L_SOB in (select CS.SET_OF_BOOKS_ID as SET_OF_BOOKS_ID, LCT.BOOK_SUMMARY_ID as BS_ID
                      from COMPANY_SET_OF_BOOKS CS,
                           SET_OF_BOOKS         S,
                           LS_ILR_OPTIONS       ILR,
                           LS_LEASE_CAP_TYPE    LCT
                     where CS.COMPANY_ID = L_LS_ASSET.COMPANY_ID
                       and CS.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                       and ILR.ILR_ID = L_LS_ASSET.ILR_ID
                       and ILR.REVISION = L_LS_ASSET.APPROVED_REVISION
                       and LCT.LS_LEASE_CAP_TYPE_ID = ILR.LEASE_CAP_TYPE_ID)
      loop
         A_MSG := '   Set of Books: ' || TO_CHAR(L_SOB.SET_OF_BOOKS_ID);
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         -- get the current month's row for the schedule for the current revision
         A_MSG := 'Get the current month schedule for leased asset id: ' ||
                  TO_CHAR(L_LS_ASSET.LS_ASSET_ID);
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
		 
		 select max(s.month)
           into L_SCH_MONTH
           from LS_ASSET_SCHEDULE S
          where S.MONTH <= A_MONTH
            and S.REVISION = L_LS_ASSET.APPROVED_REVISION
            and S.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
            and S.SET_OF_BOOKS_ID = L_SOB.SET_OF_BOOKS_ID;
		 
         select S.*
           into L_LS_ASSET_SCH
           from LS_ASSET_SCHEDULE S
          where S.MONTH = L_SCH_MONTH
            and S.REVISION = L_LS_ASSET.APPROVED_REVISION
            and S.LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
            and S.SET_OF_BOOKS_ID = L_SOB.SET_OF_BOOKS_ID;

         -- is there a termination penalty

         A_MSG := A_MSG || '   Original Termination Amount: ' ||
                  TO_CHAR(L_LS_ASSET.TERMINATION_PENALTY_AMOUNT);
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         A_MSG := A_MSG || '   Actual Termination Amount: ' ||
                  TO_CHAR(L_LS_ASSET.ACTUAL_TERMINATION_AMOUNT);
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         if L_LS_ASSET.ACTUAL_TERMINATION_AMOUNT - L_LS_ASSET.TERMINATION_PENALTY_AMOUNT <> 0 then
            L_RTN := PKG_LEASE_COMMON.F_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                               3036,
                                               L_LS_ASSET.ACTUAL_TERMINATION_AMOUNT - L_LS_ASSET.TERMINATION_PENALTY_AMOUNT,
                                               0,
                                               A_DG_ID,
                                               L_LS_ASSET.WORK_ORDER_ID,
                                               0,
                                               1,
                                               A_PT_ID,
                                               L_LS_ASSET.COMPANY_ID,
                                               A_ACCT_MONTH,
                                               1,
                                               L_GL_JE_CODE,
                                               L_SOB.SET_OF_BOOKS_ID,
                                               A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;

            L_RTN := PKG_LEASE_COMMON.F_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                               3037,
                                               L_LS_ASSET.ACTUAL_TERMINATION_AMOUNT - L_LS_ASSET.TERMINATION_PENALTY_AMOUNT,
                                               0,
                                               A_DG_ID,
                                               L_LS_ASSET.WORK_ORDER_ID,
                                               0,
                                               1,
                                               A_PT_ID,
                                               L_LS_ASSET.COMPANY_ID,
                                               A_ACCT_MONTH,
                                               0,
                                               L_GL_JE_CODE,
                                               L_SOB.SET_OF_BOOKS_ID,
                                               A_MSG);
            if L_RTN = -1 then

               return 0;
            end if;
         end if; -- termination penalty

         -- check to see if the ilr is capital for this set of books
         L_SQLS := 'select to_char(basis_' || TO_CHAR(L_SOB.BS_ID) ||
                   '_indicator) from set_of_books s where set_of_books_id = ' ||
                   TO_CHAR(L_SOB.SET_OF_BOOKS_ID);

         A_MSG := '   Set of Books (Cap or Operating): ' || L_SQLS;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

         L_RTN := TO_NUMBER(PP_MISC_PKG.DYNAMIC_SELECT(L_SQLS));
         PKG_PP_LOG.P_WRITE_MESSAGE('      RETURN: ' || TO_CHAR(L_RTN));

         -- plus additional g/l for the st / lt obligation clearing (3027 G/L CR, 3028 ST DR, 2029 LT DR)
         if L_RTN = 1 and L_LS_ASSET.IS_EARLY_RET = 1 then
            A_MSG := '     CAPITAL and Early Retirement: ' || L_SQLS;
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            -- this is an early retirement
            -- so calc additional G/L by clearing out the obligations
            L_AMT_OBL := L_PCT *
                         (L_LS_ASSET_SCH.BEG_OBLIGATION - NVL(L_LS_ASSET_SCH.BEG_LT_OBLIGATION, 0));

            if L_AMT_OBL <> 0 then
               -- book the G/L/ credit
               L_RTN := PKG_LEASE_COMMON.F_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3027,
                                                  L_AMT_OBL,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  0,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  A_MSG);
               if L_RTN = -1 then

                  return 0;
               end if;

               -- book the ST CLEAR DR
               L_RTN := PKG_LEASE_COMMON.F_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3028,
                                                  L_AMT_OBL,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  L_ST_ACCT_ID,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  A_MSG);
               if L_RTN = -1 then

                  return 0;
               end if;
            end if; -- end clearing out short term obligation

            L_AMT_OBL := L_PCT * L_LS_ASSET_SCH.BEG_LT_OBLIGATION;

            if NVL(L_AMT_OBL, 0) <> 0 then
               -- book the G/L/ credit
               L_RTN := PKG_LEASE_COMMON.F_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3027,
                                                  L_AMT_OBL,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  0,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  0,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  A_MSG);
               if L_RTN = -1 then

                  return 0;
               end if;

               -- book the LT CLEAR DR
               L_RTN := PKG_LEASE_COMMON.F_BOOKJE(L_LS_ASSET.LS_ASSET_ID,
                                                  3029,
                                                  L_AMT_OBL,
                                                  0,
                                                  A_DG_ID,
                                                  L_LS_ASSET.WORK_ORDER_ID,
                                                  L_LT_ACCT_ID,
                                                  1,
                                                  A_PT_ID,
                                                  L_LS_ASSET.COMPANY_ID,
                                                  A_ACCT_MONTH,
                                                  1,
                                                  L_GL_JE_CODE,
                                                  L_SOB.SET_OF_BOOKS_ID,
                                                  A_MSG);
               if L_RTN = -1 then

                  return 0;
               end if;
            end if; -- end clearing out long term obligation
         end if; -- end if capital and early retirement (to calc additional gain loss)
      end loop; -- loop over set of books

      if L_LS_ASSET.LS_ASSET_STATUS_ID = 4 then
         A_MSG := 'Fully retired, REMOVING asset from association to ILR';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         delete from LS_ILR_ASSET_MAP
          where LS_ASSET_ID = L_LS_ASSET.LS_ASSET_ID
            and REVISION =
                (select ILR.CURRENT_REVISION from LS_ILR ILR where ILR_ID = L_LS_ASSET.ILR_ID);

    if L_LS_ASSET.ACTUAL_RESIDUAL_AMOUNT <> 0 then
      A_MSG := 'Processing actual residual amount';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      L_ASSET_IDS(1) := L_LS_ASSET.LS_ASSET_ID;

      A_MSG := PKG_LEASE_CALC.F_PROCESS_RESIDUAL(A_LS_ASSET_IDS => L_ASSET_IDS, A_END_LOG => 0);
      if A_MSG <> 'OK' then
        PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
        return 0;
      end if;
    end if;

      end if;

      -- Handle ILR Status changes if all assets under the ILR are retired
      L_RTN := 0;
      select count(1)
        into L_RTN
        from LS_ASSET
       where ILR_ID = L_LS_ASSET.ILR_ID
         and LS_ASSET_STATUS_ID <> 4;

      if L_RTN = 0 then
         A_MSG := 'Retiring the ILR';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         update LS_ILR set ILR_STATUS_ID = 3 where ILR_ID = L_LS_ASSET.ILR_ID;
      else
         L_RTN := 0;
         -- this is the current pending revision for the ilr
         A_MSG := 'Finding the pending revision for the ILR';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
		 
		 select count(1)
		 into l_rtn
		 from LS_ILR_APPROVAL L
          where L.ILR_ID = L_LS_ASSET.ILR_ID
            and L.APPROVAL_STATUS_ID = 2;
			
		if l_rtn = 1 then 
			 select L.REVISION
			   into L_RTN
			   from LS_ILR_APPROVAL L
			  where L.ILR_ID = L_LS_ASSET.ILR_ID
				and L.APPROVAL_STATUS_ID = 2;

			 A_MSG := 'Approving ILR';
			 PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
			 L_RTN := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(L_LS_ASSET.ILR_ID, L_RTN, L_STATUS);
			 if L_RTN <> 1 then

				return 0;
			 end if;
		end if;
      end if;
      --POST.exe needs to be checking for a return value of 1.

      return 1;
   exception
      when others then
         A_MSG := SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || A_MSG);

         return 0;
   end F_RETIRE_ASSET;

   -- Function to perform specific Leased Asset transfer from
   function F_TRANSFER_ASSET_FROM(A_CPR_ASSET_ID number,
                                  A_AMOUNT       number,
                                  A_QUANTITY     number,
                                  A_PT_ID        number,
                                  A_MSG          out varchar2) return number is
      L_LS_ASSET_ID number;
      OLD_QUANTITY  number;
      L_REVISION    number;
      L_ILR_ID      number;
      L_STATUS      varchar2(4000);
      L_RTN         number;

   begin
      /*      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());

            A_MSG := 'Starting to transfer asset from';
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            A_MSG := '   Quantity: ' || TO_CHAR(A_QUANTITY);
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            A_MSG := '   Amount: ' || TO_CHAR(A_AMOUNT);
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            A_MSG := '   CPR Asset ID: ' || TO_CHAR(A_CPR_ASSET_ID);
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
            A_MSG := '   Associated Pending Transaction: ' || TO_CHAR(A_PT_ID);
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

          -- NO MAJOR PROCESSING HERE...
          -- all processing happens on the TO side

            A_MSG := 'DONE';
            PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);


            --Handle rounding... somehow...
      */
      A_MSG := 'DONE';
      return 1;
   exception
      when others then
         A_MSG := SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || A_MSG);

         return -1;
   end F_TRANSFER_ASSET_FROM;

   -- Function to perform specific Leased Asset transfer to
   function F_TRANSFER_ASSET_TO(A_CPR_ASSET_ID       number,
                                A_AMOUNT             number,
                                A_QUANTITY           number,
                                A_COMPANY_ID         number,
                                A_BUS_SEGMENT_ID     number,
                                A_UTILITY_ACCOUNT_ID number,
                                A_SUB_ACCOUNT_ID     number,
                                A_FUNC_CLASS_ID      number,
                                A_PROPERTY_GROUP_ID  number,
                                A_RETIREMENT_UNIT_ID number,
                                A_ASSET_LOCATION_ID  number,
                                A_DG_ID              number,
                                A_PT_ID              number,
                                A_MSG                out varchar2) return number is
      L_LS_ASSET_ID_FROM      number;
      L_LS_ASSET_ID_TO        number;
      L_REVISION_FROM         number;
      L_REVISION_TO           number;
      L_CURRENT_REVISION_FROM number;
      L_TO_ILR_ID             number;
      L_FROM_ILR_ID           number;
      L_WO_ID                 number;
      L_STATUS                varchar2(4000);
      L_RTN                   number;
      L_PCT                   number(22, 8);
      L_PEND_TRANS_TO         PEND_TRANSACTION%rowtype;
      L_PEND_TRANS_FROM       PEND_TRANSACTION%rowtype;
      L_MONTH                 date;
   begin
      --Update the lessee tables to the new CPR attributes
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      A_MSG := 'Starting to transfer asset to';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Quantity: ' || TO_CHAR(A_QUANTITY);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Amount: ' || TO_CHAR(A_AMOUNT);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   CPR Asset ID: ' || TO_CHAR(A_CPR_ASSET_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   TO Company ID: ' || TO_CHAR(A_COMPANY_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := '   Associated Pending Transaction: ' || TO_CHAR(A_PT_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      -- the ldg_asset_id is the to side transaction
      A_MSG := 'GET to pending transaction';
      select * into L_PEND_TRANS_TO from PEND_TRANSACTION where LDG_ASSET_ID = A_PT_ID;

      -- the pend_trans_id is the from side transaction
      A_MSG := 'GET from pending transaction';
      select * into L_PEND_TRANS_FROM from PEND_TRANSACTION where PEND_TRANS_ID = A_PT_ID;

      -- THE FROM SIDE
      A_MSG := 'GET from leased asset';
      select LS_ASSET_ID
        into L_LS_ASSET_ID_FROM
        from LS_CPR_ASSET_MAP
       where ASSET_ID = L_PEND_TRANS_FROM.LDG_ASSET_ID;

      A_MSG := 'GET percent transferred';

      select A_QUANTITY / F.QUANTITY, F.ILR_ID, F.APPROVED_REVISION
        into L_PCT, L_FROM_ILR_ID, L_CURRENT_REVISION_FROM
        from LS_ASSET F
       where F.LS_ASSET_ID = L_LS_ASSET_ID_FROM;

      A_MSG := 'FROM Leased Asset: ' || TO_CHAR(L_LS_ASSET_ID_FROM);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      A_MSG := 'PERCENT Transferred: ' || TO_CHAR(L_PCT);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      A_MSG            := 'COPY from asset';
      L_LS_ASSET_ID_TO := PKG_LEASE_ILR.F_COPYASSET(L_LS_ASSET_ID_FROM, L_PCT, A_QUANTITY);
      if L_LS_ASSET_ID_TO = -1 then
         return -1;
      end if;

      A_MSG := 'TO Leased Asset: ' || TO_CHAR(L_LS_ASSET_ID_TO);
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

      A_MSG := 'LOAD LS_CPR_ASSET_MAP';
      insert into LS_CPR_ASSET_MAP
         (LS_ASSET_ID, ASSET_ID, EFFECTIVE_DATE)
      values
         (L_LS_ASSET_ID_TO, A_CPR_ASSET_ID, L_PEND_TRANS_TO.GL_POSTING_MO_YR);

      -- create the new from revision
      L_REVISION_FROM := PKG_LEASE_ILR.F_NEWREVISION(L_FROM_ILR_ID);
      if L_REVISION_FROM = -1 then
         return -1;
      end if;

      --  if from company <> to company
      --  CREATE NEW ILR
      --  Set the new assets ilr
      --  Associate new asset to ILR / revision
      if L_PEND_TRANS_TO.COMPANY_ID <> L_PEND_TRANS_FROM.COMPANY_ID then
         L_TO_ILR_ID := PKG_LEASE_ILR.F_COPYILR(L_FROM_ILR_ID, L_PCT);
         if L_TO_ILR_ID = -1 then
            return -1;
         end if;
         L_REVISION_TO := 1;

         --
         A_MSG := 'SET company on new ILR';
         update LS_ILR set COMPANY_ID = A_COMPANY_ID where ILR_ID = L_TO_ILR_ID;

         -- update from ILR Payment TERMS if company not the same
         A_MSG := 'UPDATE from ILRs payment terms';
         update LS_ILR_PAYMENT_TERM LA
            set (EST_EXECUTORY_COST,
                  PAID_AMOUNT,
                  CONTINGENT_AMOUNT,
                  C_BUCKET_1,
                  C_BUCKET_2,
                  C_BUCKET_3,
                  C_BUCKET_4,
                  C_BUCKET_5,
                  C_BUCKET_6,
                  C_BUCKET_7,
                  C_BUCKET_8,
                  C_BUCKET_9,
                  C_BUCKET_10,
                  E_BUCKET_1,
                  E_BUCKET_2,
                  E_BUCKET_3,
                  E_BUCKET_4,
                  E_BUCKET_5,
                  E_BUCKET_6,
                  E_BUCKET_7,
                  E_BUCKET_8,
                  E_BUCKET_9,
                  E_BUCKET_10) =
                 (select LA.EST_EXECUTORY_COST - LT.EST_EXECUTORY_COST,
                         LA.PAID_AMOUNT - LT.PAID_AMOUNT,
                         LA.CONTINGENT_AMOUNT - LT.CONTINGENT_AMOUNT,
                         LA.C_BUCKET_1 - LT.C_BUCKET_1,
                         LA.C_BUCKET_2 - LT.C_BUCKET_2,
                         LA.C_BUCKET_3 - LT.C_BUCKET_3,
                         LA.C_BUCKET_4 - LT.C_BUCKET_4,
                         LA.C_BUCKET_5 - LT.C_BUCKET_5,
                         LA.C_BUCKET_6 - LT.C_BUCKET_6,
                         LA.C_BUCKET_7 - LT.C_BUCKET_7,
                         LA.C_BUCKET_8 - LT.C_BUCKET_8,
                         LA.C_BUCKET_9 - LT.C_BUCKET_9,
                         LA.C_BUCKET_10 - LT.C_BUCKET_10,
                         LA.E_BUCKET_1 - LT.E_BUCKET_1,
                         LA.E_BUCKET_2 - LT.E_BUCKET_2,
                         LA.E_BUCKET_3 - LT.E_BUCKET_3,
                         LA.E_BUCKET_4 - LT.E_BUCKET_4,
                         LA.E_BUCKET_5 - LT.E_BUCKET_5,
                         LA.E_BUCKET_6 - LT.E_BUCKET_6,
                         LA.E_BUCKET_7 - LT.E_BUCKET_7,
                         LA.E_BUCKET_8 - LT.E_BUCKET_8,
                         LA.E_BUCKET_9 - LT.E_BUCKET_9,
                         LA.E_BUCKET_10 - LT.E_BUCKET_10
                    from LS_ILR_PAYMENT_TERM LT
                   where LT.ILR_ID = L_TO_ILR_ID
                     and LT.REVISION = 1)
          where LA.ILR_ID = L_FROM_ILR_ID
            and LA.REVISION = L_REVISION_FROM;

         A_MSG := 'UPDATE from ILRs amounts by set of book';
         update LS_ILR_AMOUNTS_SET_OF_BOOKS LA
            set (NET_PRESENT_VALUE, CAPITAL_COST, CURRENT_LEASE_COST) =
                 (select LA.NET_PRESENT_VALUE - LT.NET_PRESENT_VALUE,
                         LA.CAPITAL_COST - LT.CAPITAL_COST,
                         LA.CURRENT_LEASE_COST - LT.CURRENT_LEASE_COST
                    from LS_ILR_AMOUNTS_SET_OF_BOOKS LT
                   where LT.ILR_ID = L_TO_ILR_ID
                     and LT.REVISION = 1
                     and LA.SET_OF_BOOKS_ID = LT.SET_OF_BOOKS_ID)
          where LA.ILR_ID = L_FROM_ILR_ID
            and LA.REVISION = L_REVISION_FROM;

         A_MSG := 'UPDATE from ILRs options';
         update LS_ILR_OPTIONS LA
            set (PURCHASE_OPTION_AMT, TERMINATION_AMT) =
                 (select LA.PURCHASE_OPTION_AMT - LT.PURCHASE_OPTION_AMT,
                         LA.TERMINATION_AMT - LT.TERMINATION_AMT
                    from LS_ILR_OPTIONS LT
                   where LT.ILR_ID = L_TO_ILR_ID
                     and LT.REVISION = 1)
          where LA.ILR_ID = L_FROM_ILR_ID
            and LA.REVISION = L_REVISION_FROM;
      else
         L_TO_ILR_ID   := L_FROM_ILR_ID;
         L_REVISION_TO := L_REVISION_FROM;
      end if;

      A_MSG := 'ASSOCIATE new asset to the ILR / Revision';
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, LS_ASSET_ID, REVISION)
      values
         (L_TO_ILR_ID, L_LS_ASSET_ID_TO, L_REVISION_TO);

      A_MSG := 'UPDATE asset ILR';
      update LS_ASSET
         set ILR_ID = L_TO_ILR_ID, APPROVED_REVISION = L_REVISION_TO
       where LS_ASSET_ID = L_LS_ASSET_ID_TO;

      -- UPDATE FROM ASSET's values
      A_MSG := 'UPDATE from assets values';
      update LS_ASSET LA
         set (FMV,
               TERMINATION_PENALTY_AMOUNT,
               GUARANTEED_RESIDUAL_AMOUNT,
               ACTUAL_RESIDUAL_AMOUNT,
               QUANTITY) =
              (select LA.FMV - LT.FMV,
                      LA.TERMINATION_PENALTY_AMOUNT - LT.TERMINATION_PENALTY_AMOUNT,
                      LA.GUARANTEED_RESIDUAL_AMOUNT - LT.GUARANTEED_RESIDUAL_AMOUNT,
                      LA.ACTUAL_RESIDUAL_AMOUNT - LT.ACTUAL_RESIDUAL_AMOUNT,
                      LA.QUANTITY - LT.QUANTITY
                 from LS_ASSET LT
                where LT.LS_ASSET_ID = L_LS_ASSET_ID_TO)
       where LA.LS_ASSET_ID = L_LS_ASSET_ID_FROM;

      A_MSG := 'Retire fully transferred asset';
      update LS_ASSET LA
         set LS_ASSET_STATUS_ID = 4
       where LA.LS_ASSET_ID = L_LS_ASSET_ID_FROM
         and LA.FMV = 0
         and LA.QUANTITY = 0;

      A_MSG := 'GET work order id';
      L_RTN := 0;
      select count(1)
        into L_RTN
        from WORK_ORDER_CONTROL
       where COMPANY_ID = A_COMPANY_ID
         and FUNDING_WO_INDICATOR = 0
         and WORK_ORDER_NUMBER = replace(L_PEND_TRANS_FROM.MISC_DESCRIPTION, 'wo:', '');

      if L_RTN = 1 then
         A_MSG := '   TO work order number: ' ||
                  replace(L_PEND_TRANS_FROM.MISC_DESCRIPTION, 'wo:', '');
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

         select WORK_ORDER_ID
           into L_WO_ID
           from WORK_ORDER_CONTROL
          where COMPANY_ID = A_COMPANY_ID
            and FUNDING_WO_INDICATOR = 0
            and WORK_ORDER_NUMBER = replace(L_PEND_TRANS_FROM.MISC_DESCRIPTION, 'wo:', '');
      else
         A_MSG := '   TO work order number: ' || L_PEND_TRANS_TO.WORK_ORDER_NUMBER;
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);

         select WORK_ORDER_ID
           into L_WO_ID
           from WORK_ORDER_CONTROL
          where COMPANY_ID = A_COMPANY_ID
            and FUNDING_WO_INDICATOR = 0
            and WORK_ORDER_NUMBER = L_PEND_TRANS_TO.WORK_ORDER_NUMBER;
      end if;

      --
      A_MSG := 'SET the cpr attributes on the NEW Asset';
      update LS_ASSET
         set UTILITY_ACCOUNT_ID = A_UTILITY_ACCOUNT_ID, COMPANY_ID = A_COMPANY_ID,
             BUS_SEGMENT_ID = A_BUS_SEGMENT_ID, RETIREMENT_UNIT_ID = A_RETIREMENT_UNIT_ID,
             SUB_ACCOUNT_ID = A_SUB_ACCOUNT_ID, PROPERTY_GROUP_ID = A_PROPERTY_GROUP_ID,
             ASSET_LOCATION_ID = A_ASSET_LOCATION_ID, FUNC_CLASS_ID = A_FUNC_CLASS_ID,
             WORK_ORDER_ID = L_WO_ID, ILR_ID = L_TO_ILR_ID, TAX_ASSET_LOCATION_ID = A_ASSET_LOCATION_ID
       where LS_ASSET_ID = L_LS_ASSET_ID_TO;

      -- call function to build the schedules for the new asset and the old asset
      A_MSG := PKG_LEASE_SCHEDULE.F_PROCESS_ASSET_TRF(L_LS_ASSET_ID_FROM,
                                                      L_LS_ASSET_ID_TO,
                                                      L_CURRENT_REVISION_FROM,
                                                      L_REVISION_FROM,
                                                      L_PCT,
                                                      L_FROM_ILR_ID,
                                                      L_TO_ILR_ID);
      if A_MSG <> 'OK' then
         return -1;
      end if;

      A_MSG := 'Sending the new schedule and asset adjustments for approval';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      -- the "to" asset
      L_RTN := PKG_LEASE_CALC.F_SEND_ILR_NO_COMMIT(L_TO_ILR_ID, L_REVISION_TO, L_STATUS);
      if L_RTN <> 1 then
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
      end if;

      --Rebuild the ILR and asset schedules. I guess this works? It's done in the retirements above
      --Not sure how you can just approve it before sending it
      A_MSG := 'Approving the new schedule and asset adjustments';
      PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
      L_RTN := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(L_TO_ILR_ID, L_REVISION_TO, L_STATUS);
      if L_RTN <> 1 then
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
      end if;

      -- only send the from if the comapnys are different and we created a new to side
      if L_PEND_TRANS_TO.COMPANY_ID <> L_PEND_TRANS_FROM.COMPANY_ID then
         A_MSG := '(FROM) Sending the new schedule and asset adjustments for approval';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         L_RTN := PKG_LEASE_CALC.F_SEND_ILR_NO_COMMIT(L_FROM_ILR_ID, L_REVISION_FROM, L_STATUS);
         if L_RTN <> 1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

            return -1;
         end if;

         --Rebuild the ILR and asset schedules. I guess this works? It's done in the retirements above
         --Not sure how you can just approve it before sending it
         A_MSG := '(FROM) Approving the new schedule and asset adjustments';
         PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG);
         L_RTN := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(L_FROM_ILR_ID, L_REVISION_FROM, L_STATUS);
         if L_RTN <> 1 then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

            return -1;
         end if;
      end if;


      --Get taxes on the transferred Assets
      L_STATUS := PKG_LEASE_ILR.F_GETTAXES(L_TO_ILR_ID);
      if L_STATUS <> 'OK' then
            PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
            return -1;
         end if;

      PKG_PP_LOG.P_WRITE_MESSAGE('DONE');

      return 1;
   exception
      when others then
         A_MSG := SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE('ERROR: ' || A_MSG);

         return -1;
   end F_TRANSFER_ASSET_TO;
   
   --**************************************************************************
   --                            F_DELETE_ASSETS
   --**************************************************************************
	function F_DELETE_ASSETS(A_ASSET_IDS in PKG_LEASE_CALC.NUM_ARRAY) return varchar2 is
		L_STATUS varchar2(4000);
		ASSET_IDS T_NUM_ARRAY;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_ASSET_POST.F_DELETE_ASSETS');
		PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
		PKG_PP_LOG.P_WRITE_MESSAGE('Preparing to delete ' || A_ASSET_IDS.COUNT || ' assets.');

		ASSET_IDS := T_NUM_ARRAY();
		for I in 1..A_ASSET_IDS.count
		loop
			PKG_PP_LOG.P_WRITE_MESSAGE('-- Asset ID: ' || to_char(A_ASSET_IDS(I)));
			ASSET_IDS.extend;
			ASSET_IDS(I) := A_ASSET_IDS(I);
		end loop;
		
		L_STATUS := 'Deleting class codes';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		delete from LS_ASSET_CLASS_CODE
		where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));
		
		L_STATUS := 'Deleting asset components';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		delete from LS_ASSET_COMPONENT
		where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));
		
		L_STATUS := 'Deleting documents';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		delete from LS_ASSET_DOCUMENT
		where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));
		
		L_STATUS := 'Deleting tax maps';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		delete from LS_ASSET_TAX_MAP
		where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));
		
		L_STATUS := 'Deleting local taxes';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		delete from LS_ASSET_LOCAL_TAX
		where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));
		
		L_STATUS := 'Deleting leased assets';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
		delete from LS_ASSET
		where LS_ASSET_ID in (select COLUMN_VALUE from table(ASSET_IDS));
		
		PKG_PP_LOG.P_WRITE_MESSAGE('Success');
		PKG_PP_LOG.P_END_LOG;
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
		return 'OK';
		
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end F_DELETE_ASSETS;

   --**************************************************************************
   --                            GET_ILR_ID
   --**************************************************************************

   function F_GET_ILR_ID return number is

   begin
      return L_ILR_ID;
   end F_GET_ILR_ID;

--**************************************************************************
--                            Initialize Package
--**************************************************************************

begin
   L_ILR_ID := 0;

end PKG_LEASE_ASSET_POST;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1226, 0, 10, 4, 3, 0, 38731, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038731_lease_PKG_LEASE_ASSET_POST.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
