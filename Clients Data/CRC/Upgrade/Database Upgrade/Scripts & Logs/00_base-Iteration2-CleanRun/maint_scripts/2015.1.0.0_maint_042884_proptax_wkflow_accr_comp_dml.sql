--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042884_proptax_wkflow_accr_comp_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     03/05/2015 B Borm, A Scott  Update the subsystem sql for accrual approvals, to join on 
--||                                        prop_tax_company_id to company view, not company_id.
--||============================================================================
--*/

update pwrplant.workflow_subsystem
set   workflow_type_filter = 'and workflow_type_id = (select distinct ap.approval_type_id from pt_accrual_pending ap, company co where ap.prop_tax_company_id = co.prop_tax_company_id and co.prop_tax_company_id = <<id_field1>> and to_char( ap.month, ''mm/dd/yyyy'' ) = ''<<id_field2>>'')',
      approve_sql = 'update pt_accrual_pending set accrual_status_id = 2, final_approval_date = sysdate, final_approver = user where accrual_status_id = 1 and to_char( month, ''mm/dd/yyyy'' ) = ''<<id_field2>>'' and prop_tax_company_id in (select prop_tax_company_id from company where prop_tax_company_id = <<id_field1>>)',
      reject_sql = 'update pt_accrual_pending set accrual_status_id = 3, rejected_date = sysdate, rejector = user where to_char( month, ''mm/dd/yyyy'' ) = ''<<id_field2>>'' and prop_tax_company_id in (select prop_tax_company_id from company where prop_tax_company_id = <<id_field1>>)'
where subsystem = 'proptax_accrual_approval';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2348, 0, 2015, 1, 0, 0, 42884, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042884_proptax_wkflow_accr_comp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;