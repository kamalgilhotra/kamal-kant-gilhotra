/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030748_lease_menu_items.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/13/2013 Ryan Oliveria  Point Release
||============================================================================
*/

delete from PPBASE_MENU_ITEMS where MODULE = 'LESSEE';

delete from PPBASE_WORKSPACE
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER = 'LESSEE_SEARCH';

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'lessee_search', sysdate, user, 'Global Search', 'uo_ls_search_multi_wksp', null);


--* Menu Level 1
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_initiate', sysdate, 'PWRPLANT', 1, 1, 'Initiate', null, null, null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_details', sysdate, 'PWRPLANT', 1, 2, 'Details', null, null, null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_approval', sysdate, 'PWRPLANT', 1, 3, 'Approvals', null, null, null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_control', sysdate, 'PWRPLANT', 1, 4, 'Monthly Control', null, null, null,
    1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_reports', sysdate, 'PWRPLANT', 1, 5, 'Reports', null, null, null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_admin', sysdate, 'PWRPLANT', 1, 6, 'Admin', null, null, null, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'menu_wksp_import', sysdate, 'PWRPLANT', 1, 7, 'Import Tools', null, null, null, 1);

--* Menu level 2
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'admin_lessors', sysdate, 'PWRPLANT', 2, 2, 'Lessors', null, 'menu_wksp_admin',
    'admin_lessors', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'admin_vendors', sysdate, 'PWRPLANT', 2, 3, 'Vendors', null, 'menu_wksp_admin',
    'admin_vendors', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'admin_taxes', sysdate, 'PWRPLANT', 2, 4, 'Taxes', null, 'menu_wksp_admin',
    'admin_taxes', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'admin_system_controls', sysdate, 'PWRPLANT', 2, 5, 'System Controls', null,
    'menu_wksp_admin', 'admin_system_controls', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'admin_lease_groups', sysdate, 'PWRPLANT', 2, 1, 'Lease Groups', null,
    'menu_wksp_admin', 'admin_lease_groups', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'approval_mla', sysdate, 'PWRPLANT', 2, 1, 'MLA', null, 'menu_wksp_approval',
    'approval_mla', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'approval_assets', sysdate, 'PWRPLANT', 2, 2, 'Assets and Schedules', null,
    'menu_wksp_approval', 'approval_assets', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'approval_trans_ret', sysdate, 'PWRPLANT', 2, 3, 'Transfers and Retirements', null,
    'menu_wksp_approval', 'approval_trans_ret', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'approval_payments', sysdate, 'PWRPLANT', 2, 4, 'Payments', null,
    'menu_wksp_approval', 'approval_payments', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'control_payments', sysdate, 'PWRPLANT', 2, 1, 'Payments', null, 'menu_wksp_control',
    'control_payments', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'control_invoices', sysdate, 'PWRPLANT', 2, 2, 'Invoices', null, 'menu_wksp_control',
    'control_invoices', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'control_monthend', sysdate, 'PWRPLANT', 2, 3, 'Month End', null, 'menu_wksp_control',
    'control_monthend', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'search_mla', sysdate, 'PWRPLANT', 2, 1, 'MLA', null, 'menu_wksp_details',
    'search_mla', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'search_asset', sysdate, 'PWRPLANT', 2, 2, 'Asset', null, 'menu_wksp_details',
    'search_asset', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'search_ilr', sysdate, 'PWRPLANT', 2, 3, 'ILR', null, 'menu_wksp_details',
    'search_ilr', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'search_schedules', sysdate, 'PWRPLANT', 2, 4, 'Schedules', null, 'menu_wksp_details',
    'search_schedules', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'lessee_search', sysdate, 'PWRPLANT', 2, 5, 'Global Search', null,
    'menu_wksp_details', 'lessee_search', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'import_mla', sysdate, 'PWRPLANT', 2, 1, 'MLA', null, 'menu_wksp_import',
    'import_mla', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'import_assets', sysdate, 'PWRPLANT', 2, 2, 'Assets', null, 'menu_wksp_import',
    'import_assets', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'import_lessors', sysdate, 'PWRPLANT', 2, 3, 'Lessors', null, 'menu_wksp_import',
    'import_lessors', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'import_invoices', sysdate, 'PWRPLANT', 2, 4, 'Invoices', null, 'menu_wksp_import',
    'import_invoices', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'initiate_mla', sysdate, 'PWRPLANT', 2, 1, 'MLA', null, 'menu_wksp_initiate',
    'initiate_mla', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'initiate_asset', sysdate, 'PWRPLANT', 2, 2, 'Asset', null, 'menu_wksp_initiate',
    'initiate_asset', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'initiate_ilr', sysdate, 'PWRPLANT', 2, 3, 'ILR', null, 'menu_wksp_initiate', null, 1);

--* Menu level 3
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'initiate_ilr_new', sysdate, 'PWRPLANT', 3, 1, 'New', null, 'initiate_ilr',
    'initiate_ilr_new', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'initiate_ilr_from_assets', sysdate, 'PWRPLANT', 3, 2, 'From Assets', null,
    'initiate_ilr', 'initiate_ilr_from_assets', 1);

--Workspace labels
update PPBASE_WORKSPACE
   set LABEL = 'Global Search'
 where LABEL = 'GLOBAL Search'
   and MODULE = 'LESSEE';

update PPBASE_WORKSPACE
   set LABEL = 'From Assets'
 where LABEL = 'from Assets'
   and MODULE = 'LESSEE';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (474, 0, 10, 4, 1, 0, 30748, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030748_lease_menu_items.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
