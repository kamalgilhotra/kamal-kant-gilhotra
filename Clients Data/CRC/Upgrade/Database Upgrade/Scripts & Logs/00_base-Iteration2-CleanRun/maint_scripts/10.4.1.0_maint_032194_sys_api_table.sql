/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032194_sys_api_table.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.1.0 09/09/2013 Stephen Motter      Point Release
||============================================================================
*/

alter table PP_PROCESSES_KICKOUT_COLUMNS add COLUMN_ORDER number(22,0);

insert into POWERPLANT_TABLES
   (TABLE_NAME, PP_TABLE_TYPE_ID, DESCRIPTION, SUBSYSTEM_DISPLAY)
values
   ('pp_processes_kickout_control', 'spl*', 'Kickout Control', 'w_api_errors_control');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (596, 0, 10, 4, 1, 0, 32194, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032194_sys_api_table.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;