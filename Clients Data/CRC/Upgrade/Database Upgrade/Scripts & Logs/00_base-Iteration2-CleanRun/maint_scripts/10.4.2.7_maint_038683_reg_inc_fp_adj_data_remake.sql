/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038683_reg_inc_fp_adj_data_remake.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/01/2014 Kyle Peterson
||============================================================================
*/

drop table INCREMENTAL_FP_ADJ_DATA;

create sequence INC_DATA_SEQ;

--INCREMENTAL_FP_ADJ_DATA
create table INCREMENTAL_FP_ADJ_DATA
(
 INC_DATA_ID          number(22,0) not null,
 LABEL_ID             number(22,0) not null,
 ITEM_ID              number(22,0) not null,
 WORK_ORDER_ID        number(22,0) not null,
 REVISION             number(22,0) not null,
 BUDGET_VERSION_ID    number(22,0) not null,
 FCST_DEPR_VERSION_ID number(22,0) not null,
 VERSION_ID           number(22,0) not null,
 MONTH_YEAR           number(22,0) not null,
 AMOUNT               number(22,2) not null,
 FCST_DEPR_GROUP_ID   number(22,0),
 SET_OF_BOOKS_ID      number(22,0),
 EXPENDITURE_TYPE_ID  number(22,0),
 EST_CHG_TYPE_ID      number(22,0),
 WIP_COMPUTATION_ID   number(22,0),
 WIP_ACCOUNT_ID       number(22,0),
 TAX_CLASS_ID         number(22,0),
 NORMALIZATION_ID     number(22,0),
 JURISDICTION_ID      number(22,0),
 USER_ID              varchar2(18),
 TIMESTAMP            date
);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint PK_INCREMENTAL_FP_ADJ_DATA
       primary key(INC_DATA_ID)
       using index tablespace PWRPLANT_IDX;

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA1
       foreign key(LABEL_ID, ITEM_ID)
       references INCREMENTAL_FP_ADJ_ITEM(LABEL_ID, ITEM_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA2
       foreign key(WORK_ORDER_ID, REVISION)
       references WORK_ORDER_APPROVAL(WORK_ORDER_ID, REVISION);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA3
       foreign key(BUDGET_VERSION_ID)
       references BUDGET_VERSION(BUDGET_VERSION_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA4
       foreign key(FCST_DEPR_VERSION_ID)
       references FCST_DEPR_VERSION(FCST_DEPR_VERSION_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA5
       foreign key(FCST_DEPR_GROUP_ID)
       references FCST_DEPR_GROUP(FCST_DEPR_GROUP_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA6
       foreign key(SET_OF_BOOKS_ID)
       references SET_OF_BOOKS(SET_OF_BOOKS_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA7
       foreign key(EXPENDITURE_TYPE_ID)
       references EXPENDITURE_TYPE(EXPENDITURE_TYPE_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA8
       foreign key(EST_CHG_TYPE_ID)
       references ESTIMATE_CHARGE_TYPE(EST_CHG_TYPE_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA9
       foreign key(TAX_CLASS_ID)
       references TAX_CLASS(TAX_CLASS_ID);


alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA10
      foreign key(NORMALIZATION_ID)
      references NORMALIZATION_SCHEMA(NORMALIZATION_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA11
       foreign key(JURISDICTION_ID)
       references JURISDICTION(JURISDICTION_ID);

alter table INCREMENTAL_FP_ADJ_DATA
   add constraint FK_INCREMENTAL_FP_ADJ_DATA12
       foreign key(VERSION_ID)
       references VERSION(VERSION_ID);

comment on table INCREMENTAL_FP_ADJ_DATA is 'Stores incremental forecast data by label, item, funding project, and revision.';
comment on column INCREMENTAL_FP_ADJ_DATA.INC_DATA_ID is 'System assigned identifier used for uniqueness in this data.';
comment on column INCREMENTAL_FP_ADJ_DATA.LABEL_ID is 'System assigned identifier for the incremental data label.';
comment on column INCREMENTAL_FP_ADJ_DATA.ITEM_ID is 'System assigned identifier for the incremental data item.';
comment on column INCREMENTAL_FP_ADJ_DATA.WORK_ORDER_ID is 'System assigned identifier for the funding project.';
comment on column INCREMENTAL_FP_ADJ_DATA.REVISION is 'System assigned identifier for the funding project revision.';
comment on column INCREMENTAL_FP_ADJ_DATA.BUDGET_VERSION_ID is 'System assigned identifier for the capital budget version.';
comment on column INCREMENTAL_FP_ADJ_DATA.FCST_DEPR_VERSION_ID is 'System assigned identifier for the depreciation forecast version.';
comment on column INCREMENTAL_FP_ADJ_DATA.VERSION_ID is 'System assigned identifier for the PowerTax Case.';
comment on column INCREMENTAL_FP_ADJ_DATA.MONTH_YEAR is 'Month in YYYYMM format.';
comment on column INCREMENTAL_FP_ADJ_DATA.AMOUNT is 'Monthly incremental financial amount in dollars.';
comment on column INCREMENTAL_FP_ADJ_DATA.FCST_DEPR_GROUP_ID is 'System assigned identifier for the forecast depreciation group for labels/items that involve depreciation.';
comment on column INCREMENTAL_FP_ADJ_DATA.SET_OF_BOOKS_ID is 'System assigned identifier for the Set of Books for labels/items that involve depreciation.';
comment on column INCREMENTAL_FP_ADJ_DATA.EXPENDITURE_TYPE_ID is 'System assigned identifier for the expenditure type for the funding project.';
comment on column INCREMENTAL_FP_ADJ_DATA.EST_CHG_TYPE_ID is 'System assigned identifier for the estimate charge type for the funding project.';
comment on column INCREMENTAL_FP_ADJ_DATA.WIP_COMPUTATION_ID is 'System assigned identifier for the WIP computation for labels/items that involve WIP Computations.';
comment on column INCREMENTAL_FP_ADJ_DATA.WIP_ACCOUNT_ID is 'System assigned identifier for the GL account other than CWIP for labels/items that involve WIP Computations.';
comment on column INCREMENTAL_FP_ADJ_DATA.TAX_CLASS_ID is 'System assigned identifier for the tax class for labels/items that involve deferred taxes.';
comment on column INCREMENTAL_FP_ADJ_DATA.NORMALIZATION_ID is 'System assigned identifier for the normalization schema for labels/items that involve deferred taxes.';
comment on column INCREMENTAL_FP_ADJ_DATA.JURISDICTION_ID is 'System assigned identifier for the jurisdiction for labels/items that involve deferred taxes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1326, 0, 10, 4, 2, 7, 38683, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038683_reg_inc_fp_adj_data_remake.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;