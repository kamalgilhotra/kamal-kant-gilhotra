/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_036864_aro_approve_ssp.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 11/11/2014 Sarah Byers      SSP Release JE
||============================================================================
*/

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION, EXECUTABLE_FILE, VERSION, ALLOW_CONCURRENT, SYSTEM_LOCK_ENABLED,
    SYSTEM_LOCK, ASYNC_EMAIL_ON_COMPLETE)
   select (select max(PROCESS_ID) + 1 from PP_PROCESSES), 'Release Journal Entries', 'Release Journal Entries', 'ssp_release_je.exe', '2015.1.0.0', 0, 0, 0, 0
     from DUAL
    where not exists (select 1
                        from PP_PROCESSES
                       where EXECUTABLE_FILE = 'ssp_release_je.exe'
                         and VERSION = '2015.1.0.0');



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2033, 0, 2015, 1, 0, 0, 040972, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040972_cpr_ssp_release_je.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;