--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042917_taxrpr_ltd_prfmc4_20151_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1   03/10/2015 A Scott            Improve LTD performance
--||                                        Exact same as 10.4.3.4 script. Made
--||                                        re-runnable because 10.4.3.4 may be 
--||                                        released after 2015.1.  So this ensures
--||                                        the fix goes in regardless of which code
--||                                        is used by the client.
--||============================================================================
--*/

delete from PP_DATAWINDOW_HINTS
where DATAWINDOW in ('TAXREP cpr setup eligible insert repair wo temp orig','TAXREP blanket setup eligible insert repair wo temp orig');

insert into PP_DATAWINDOW_HINTS (DATAWINDOW, SELECT_NUMBER, HINT)
values ('TAXREP cpr setup eligible insert repair wo temp orig', 1, '/*+ USE_HASH( cl, ru, uapu ) */');
insert into PP_DATAWINDOW_HINTS (DATAWINDOW, SELECT_NUMBER, HINT)
values ('TAXREP blanket setup eligible insert repair wo temp orig', 1, '/*+ USE_HASH( cl, ru, uapu ) */');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2368, 0, 2015, 1, 0, 0, 42917, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042917_taxrpr_ltd_prfmc4_20151_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;