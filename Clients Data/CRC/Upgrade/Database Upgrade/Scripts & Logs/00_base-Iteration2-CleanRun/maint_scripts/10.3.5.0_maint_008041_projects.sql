/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008041_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   02/21/2012 Chris Mardis   Point Release
||============================================================================
*/

create table WO_STATUS_TRAIL
(
 SEQUENCE_KEY        number(22,0) not null,
 WORK_ORDER_ID       number(22,0) not null,
 WO_STATUS_ID        number(22,0),
 IN_SERVICE_DATE     date,
 OUT_OF_SERVICE_DATE date,
 COMPLETION_DATE     date,
 MODIFIED_USER_ID    varchar2(18),
 MODIFIED_TIMESTAMP  date,
 PROGRAM             varchar2(60),
 PROCESS             varchar2(60),
 ACTIVE_WINDOW       varchar2(60),
 WINDOW_TITLE        varchar2(254)
);

alter table WO_STATUS_TRAIL
   add constraint PK_WO_STATUS_TRAIL
       PRIMARY KEY (SEQUENCE_KEY)
       using index tablespace PWRPLANT_IDX;


CREATE OR REPLACE TRIGGER TRIG_WO_AUDIT_TRAIL AFTER
 INSERT  OR  UPDATE  OR  DELETE  OF COMPLETION_DATE, IN_SERVICE_DATE, OUT_OF_SERVICE_DATE, WO_STATUS_ID
 ON WORK_ORDER_CONTROL FOR EACH ROW
DECLARE
  prog varchar2(60);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF UPDATING OR INSERTING THEN
   IF :old.COMPLETION_DATE <> :new.COMPLETION_DATE OR
     (:old.COMPLETION_DATE is null AND :new.COMPLETION_DATE is not null) OR
     (:new.COMPLETION_DATE is null AND :old.COMPLETION_DATE is not null) THEN


      INSERT INTO WO_STATUS_TRAIL
        (SEQUENCE_KEY, WORK_ORDER_ID, WO_STATUS_ID, IN_SERVICE_DATE, OUT_OF_SERVICE_DATE, COMPLETION_DATE, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, PROGRAM, PROCESS, ACTIVE_WINDOW, WINDOW_TITLE)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, :new.WORK_ORDER_ID, null, null, null, :new.COMPLETION_DATE, USER, SYSDATE, prog, trans, window, windowtitle);
   END IF;
   IF :old.IN_SERVICE_DATE <> :new.IN_SERVICE_DATE OR
     (:old.IN_SERVICE_DATE is null AND :new.IN_SERVICE_DATE is not null) OR
     (:new.IN_SERVICE_DATE is null AND :old.IN_SERVICE_DATE is not null) THEN


      INSERT INTO WO_STATUS_TRAIL
        (SEQUENCE_KEY, WORK_ORDER_ID, WO_STATUS_ID, IN_SERVICE_DATE, OUT_OF_SERVICE_DATE, COMPLETION_DATE, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, PROGRAM, PROCESS, ACTIVE_WINDOW, WINDOW_TITLE)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, :new.WORK_ORDER_ID, null, :new.IN_SERVICE_DATE, null, null, USER, SYSDATE, prog, trans, window, windowtitle);

   END IF;
   IF :old.OUT_OF_SERVICE_DATE <> :new.OUT_OF_SERVICE_DATE OR
     (:old.OUT_OF_SERVICE_DATE is null AND :new.OUT_OF_SERVICE_DATE is not null) OR
     (:new.OUT_OF_SERVICE_DATE is null AND :old.OUT_OF_SERVICE_DATE is not null) THEN


      INSERT INTO WO_STATUS_TRAIL
        (SEQUENCE_KEY, WORK_ORDER_ID, WO_STATUS_ID, IN_SERVICE_DATE, OUT_OF_SERVICE_DATE, COMPLETION_DATE, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, PROGRAM, PROCESS, ACTIVE_WINDOW, WINDOW_TITLE)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, :new.WORK_ORDER_ID, null, null, :new.OUT_OF_SERVICE_DATE, null, USER, SYSDATE, prog, trans, window, windowtitle);
   END IF;
   IF :old.WO_STATUS_ID <> :new.WO_STATUS_ID OR
     (:old.WO_STATUS_ID is null AND :new.WO_STATUS_ID is not null) OR
     (:new.WO_STATUS_ID is null AND :old.WO_STATUS_ID is not null) THEN


      INSERT INTO WO_STATUS_TRAIL
        (SEQUENCE_KEY, WORK_ORDER_ID, WO_STATUS_ID, IN_SERVICE_DATE, OUT_OF_SERVICE_DATE, COMPLETION_DATE, MODIFIED_USER_ID, MODIFIED_TIMESTAMP, PROGRAM, PROCESS, ACTIVE_WINDOW, WINDOW_TITLE)
        VALUES
        (PP_TABLE_AUDIT_SEQ.NEXTVAL, :new.WORK_ORDER_ID, :new.WO_STATUS_ID, null, null, null, USER, SYSDATE, prog, trans, window, windowtitle);
   END IF;
END IF;

END;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (167, 0, 10, 3, 5, 0, 8041, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_008041_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
