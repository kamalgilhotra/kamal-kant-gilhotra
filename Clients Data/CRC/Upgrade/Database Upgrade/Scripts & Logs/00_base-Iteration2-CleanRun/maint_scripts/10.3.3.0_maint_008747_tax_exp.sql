/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008747_tax_exp.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/28/2011 Elhadj Bah     Point Release
||============================================================================
*/

create table REPAIR_BLANKET_PROCESSING
(
 WORK_ORDER_ID              number(22,0),
 WORK_ORDER_NUMBER          varchar2(35),
 COMPANY_ID                 number(22,0),
 BLANKET_METHOD             varchar2(35),
 REPAIR_LOCATION_ID         number(22,0),
 REPAIR_UNIT_CODE_ID        number(22,0),
 CLOSING_OPTION_ID          number(22,0),
 REPLACEMENT_QTY            number(22,2),
 WO_UOP_QTY                 number(22,0),
 WO_UOP_RETIRE_QTY          number(22,0),
 WO_UOP_COST                number(22,2),
 WO_UOP_RETIRE_COST         number(22,2),
 WO_TOTAL_PERIOD_COST       number(22,2),
 EXPENSE_PERCENTAGE         number(22,2),
 NUMBER_OF_CIRCUITS         number(22,0),
 PRORATA_TEST_QTY           number(22,2),
 PRORATA_THRESHOLD          number(22,2),
 PRORATA_PASS_FAIL          varchar2(35),
 PROP_TOTAL_UOP_QTY         number(22,2),
 PROP_TOTAL_CIRCUIT_PERCENT number(22,2),
 PROP_QTY_PER_CIRCUIT       number(22,2),
 PROP_THRESHOLD             number(22,2),
 PROP_PASS_FAIL             varchar2(35),
 COST_UOP_REPLACEMENT_COST  number(22,2),
 COST_UOP_TEST_QTY          number(22,0),
 COST_THRESHOLD             number(22,2),
 COST_PASS_FAIL             varchar2(35),
 ALL_COST_REPLACEMENT_COST  number(22,2),
 ALL_COST_THRESHOLD         number(22,2),
 ALL_COST_PASS_FAIL         varchar2(35)
);

alter table REPAIR_BLANKET_PROCESSING
   add constraint PK_REPAIR_BLANKET_PROCESSING
       primary key (WORK_ORDER_ID, BLANKET_METHOD, REPAIR_LOCATION_ID, REPAIR_UNIT_CODE_ID)
       using index tablespace PWRPLANT_IDX;

insert into REPAIR_LOCATION
   (REPAIR_LOCATION_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (0, 'Default Location', 'Do not delete - Default system location');

insert into REPAIR_METHOD
   (REPAIR_METHOD_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (0, 'Default Method', 'Do not delete - Default System Method');

alter table REPAIR_WORK_ORDER_TEMP_ORIG
   add (TOTAL_PERIOD_ADDS_EXPENSE number(22,2),
        TOTAL_PERIOD_RETS_EXPENSE number(22,2));

alter table WO_TAX_EXPENSE_TEST
   add TOLERANCE_PERCENT number(22,2);

-- ** EKB: Insert these into work_order_tax_status (New column for repair_loc_rollup_id). If not null, then repair_location_id is null.

-- ** This column should only be populated for work orders where the tax expense test id on work order account has a processing type of 'Blanket'
alter table WORK_ORDER_TAX_STATUS
   add REPAIR_LOC_ROLLUP_ID number(22,0);

alter table WORK_ORDER_TAX_STATUS
   add constraint FK_WOTS_REPAIR_LOC_ROLLUP_ID
       foreign key (REPAIR_LOC_ROLLUP_ID)
       references REPAIR_LOC_ROLLUP;

-- ** Need to run this alter table statement temporarilty in this database. Zach's code should have taken care of this.
--alter table WORK_ORDER_ACCOUNT
--   add TAX_EXPENSE_TEST_ID number(22,0);

alter table WORK_ORDER_ACCOUNT
   add PER_SE_CAPITAL_PCT number(22,2);

alter table REPAIR_THRESHOLDS
   add UNIT_REPLACE_COST number(22,2);

create table REPAIR_BLANKET_RESULTS
(
 WORK_ORDER_ID             number(22,0),
 WORK_ORDER_NUMBER         varchar2(35),
 COMPANY_ID                number(22,0),
 REPAIR_UNIT_CODE_ID       number(22,0),
 ADD_QTY                   number(22,0),
 ADD_COST                  number(22,2),
 RETIRE_QTY                number(22,0),
 RETIRE_COST               number(22,2),
 BLANKET_METHOD            varchar2(35),
 EXPENSE_PCT               number(22,2),
 PER_SE_CAPITAL_PCT        number(22,2),
 CURR_ADDS_EXPENSE_AMOUNT  number(22,2),
 CURR_RETS_EXPENSE_AMOUNT  number(22,2),
 TOTAL_PERIOD_ADDS_EXPENSE number(22,2),
 TOTAL_PERIOD_RETS_EXPENSE number(22,2),
 ADDS_TAX_EXPENSE          number(22,2),
 RETS_TAX_EXPENSE          number(22,2)
);

alter table REPAIR_BLANKET_RESULTS
   add constraint PK_REPAIR_BLANKET_RESULTS
       primary key (WORK_ORDER_ID, REPAIR_UNIT_CODE_ID, BLANKET_METHOD)
       using index tablespace PWRPLANT_IDX;

-- ** Do we need a reporting table for this one (I think so, include the batch_id in the reporting table, not in the processing table).
create table REPAIR_BLKT_RESULTS_REPORTING
(
 BATCH_ID                  number(22,0),
 WORK_ORDER_ID             number(22,0),
 WORK_ORDER_NUMBER         varchar2(35),
 COMPANY_ID                number(22,0),
 REPAIR_UNIT_CODE_ID       number(22,0),
 ADD_QTY                   number(22,0),
 ADD_COST                  number(22,2),
 RETIRE_QTY                number(22,0),
 RETIRE_COST               number(22,2),
 BLANKET_METHOD            varchar2(35),
 EXPENSE_PCT               number(22,2),
 PER_SE_CAPITAL_PCT        number(22,2),
 CURR_ADDS_EXPENSE_AMOUNT  number(22,2),
 CURR_RETS_EXPENSE_AMOUNT  number(22,2),
 TOTAL_PERIOD_ADDS_EXPENSE number(22,2),
 TOTAL_PERIOD_RETS_EXPENSE number(22,2),
 ADDS_TAX_EXPENSE          number(22,2),
 RETS_TAX_EXPENSE          number(22,2)
);

ALTER TABLE REPAIR_BLKT_RESULTS_REPORTING
   add constraint PK_REPAIR_BLKT_RESULTS_REPORT
       primary key (BATCH_ID, WORK_ORDER_ID, REPAIR_UNIT_CODE_ID, BLANKET_METHOD)
       using index tablespace PWRPLANT_IDX;

-- ** Create a table to hold the per_se_capital_percentages
create table REPAIR_PER_SE_CAPITAL_PCT
(
 WORK_ORDER_ID       number(22,0),
 REPAIR_UNIT_CODE_ID number(22,0),
 PER_SE_CAPITAL_PCT  number(22,2),
 NOTES               varchar2(255)
);

alter table repair_per_se_capital_pct
   add constraint FK_RPCP_RUC
       foreign key (REPAIR_UNIT_CODE_ID)
       references REPAIR_UNIT_CODE;

alter table REPAIR_PER_SE_CAPITAL_PCT
   add constraint PK_REPAIR_PER_SE_CAPITAL_PCT
       primary key (WORK_ORDER_ID, REPAIR_UNIT_CODE_ID);

-- ** Recreateing the tables below
-- ** Maint 8747 -- Add the following columns to check for add to retire ratios and see if we need to eliminate certain work orders based on that
--alter table repair_work_order_segments add( add_retire_tolerance_pct number(22,2),add_retire_ratio number(22,2));
--alter table repair_wo_seg_reporting add( add_retire_tolerance_pct number(22,2),add_retire_ratio number(22,2));

--alter table WO_TAX_EXPENSE_TEST
--   add (ADD_RETIRE_TOLERANCE_PCT   number(22,2),
--        TAX_STATUS_OVER_TOLERANCE  number(22,0),
--        BLANKET_PROCESSING_OPTION  number(22,0),
--        BLANKET_METHOD             varchar2(35),
--        EXPENSE_PERCENT            number(22,2));

--create table repair_per_se_capital_pct
--(
-- WORK_ORDER_ID       number(22,0),
-- REPAIR_UNIT_CODE_ID number(22,0),
-- PER_SE_CAPITAL_PCT  number(22,2),
-- NOTES               varchar2(255)
--);

--alter table REPAIR_PER_SE_CAPITAL_PCT
--   add constraint FK_RPCP_RUC
--       foreign key (REPAIR_UNIT_CODE_ID)
--       references REPAIR_UNIT_CODE;

--alter table REPAIR_PER_SE_CAPITAL_PCT
--   add constraint PK_REPAIR_PER_SE_CAPITAL_PCT
--       primary key (WORK_ORDER_ID, REPAIR_UNIT_CODE_ID)
--       using index tablespace PWRPLANT_IDX;

--alter table WORK_ORDER_TAX_STATUS
--   add REPAIR_LOC_ROLLUP_ID number(22,0);

-- ** Recreating the tables below. Keeping the sql here for reference
-- ** Maint 8747 -- Add the following columns to check for add to retire ratios and see if we need to eliminate certain work orders based on that
-- ** These have  npt been run in Lbudget2
--alter table repair_work_order_segments add( add_retire_tolerance_pct number(22,2),add_retire_ratio number(22,2));
--alter table repair_wo_seg_reporting add( add_retire_tolerance_pct number(22,2),add_retire_ratio number(22,2));

-- ** Maint 8747 Set up justification information to retrieve datawindow allowing clients to enter per_se_capital_percentages
update WO_DOC_JUSTIFICATION_RULES
   set SORT_ORDER = -1
 where LOWER(DATAWINDOW_NAME) = 'dw_wo_doc_just_tax_stat';

insert into WO_DOC_JUSTIFICATION_RULES
   (JUSTIFICATION_ID, DESCRIPTION, CATEGORY, SORT_ORDER, ALLOW_EDIT, INSTRUCTIONS, FOR_ALL_WO_TYPES,
    DATAWINDOW_NAME, DATAWINDOW_UPDATE, DATAWINDOW_SELECT_ROW, DROPDOWN, TAB_INDICATOR,
    WORK_FLOW_FLAG, DATAWINDOW_DEFAULT_ROWS)
values
   ((select NVL(max(JUSTIFICATION_ID), 0) + 1 from WO_DOC_JUSTIFICATION_RULES),
    'Per Se Capital Percentages',
    'Tax Status',
    0, --sort_order
    1, --allow_edit
    'Enter the Per Se Capital Percentages for the tax units of property when applicable. This is almost exclusively for blankets.',
    1, -- for_all_wo_types
    'dw_repair_per_se_capital_pct',
    1, --datawindow_update
    1, -- datawindow_select_row
    0, -- dropdown
    10, -- tab_indicator
    0, -- work_flow_flag
    4);

alter table REPAIR_WORK_ORDER_TEMP
   add (TOTAL_PERIOD_ADDS_EXPENSE number(22,2),
        TOTAL_PERIOD_RETS_EXPENSE number(22,2));

-- ** Just to be on the safe side, let's recreate the following tables
create table REPAIR_WO_SEG_BKUP as select * from REPAIR_WORK_ORDER_SEGMENTS;
create table REPAIR_WO_SEG_REP_BKUP as select * from REPAIR_WO_SEG_REPORTING;
create table REPAIR_AMOUNT_ALLOC_BKUP as select * from REPAIR_AMOUNT_ALLOCATE;
create table REPAIR_AMOUNT_ALLOC_REP_BKUP as select * from REPAIR_AMT_ALLOC_REPORTING;

drop table REPAIR_WORK_ORDER_SEGMENTS;
drop table REPAIR_WO_SEG_REPORTING;
drop table REPAIR_AMOUNT_ALLOCATE;
drop table REPAIR_AMT_ALLOC_REPORTING;

create table REPAIR_WORK_ORDER_SEGMENTS
(
 REPAIR_SCHEMA_ID              number(22,0) not null,
 COMPANY_ID                    number(22,0) not null,
 WORK_ORDER_NUMBER             varchar2(35) not null,
 REPAIR_UNIT_CODE_ID           number(22,0) not null,
 REPAIR_LOCATION_ID            number(22,0) not null,
 REPAIR_METHOD_ID              number(22,0) not null,
 ADD_QUANTITY                  number(22,0),
 RETIRE_QUANTITY               number(22,0),
 ADD_COST                      number(22,2),
 RETIRE_COST                   number(22,2),
 MIN_VINTAGE                   number(22,0),
 MAX_VINTAGE                   number(22,0),
 PRE81_QUANTITY                number(22,0),
 POST80_QUANTITY               number(22,0),
 MY_CONDITION                  varchar2(35),
 MY_EVALUATE                   number(1,0),
 MY_USE_VINTAGE                number(1,0),
 MY_ADD_TEST                   number(1,0),
 MY_RETIRE_TEST                number(1,0),
 MY_FACTOR                     number(22,8),
 MY_QTY_FACTOR                 number(22,8),
 MY_USE_RATIO                  number(1,0),
 MY_USE_REPLACE                number(1,0),
 MY_QTY_OR_DOLLARS             number(1,0),
 MY_ADD_AND_RETIRE_TEST        number(1,0),
 MY_PRE81_RETIREMENT_RATIO     number(22,8),
 MY_TEST_QUANTITY              number(22,2),
 MY_TEST_QUANTITY2             number(22,2),
 MY_ADD_THRESHOLD              number(22,0),
 MY_RETIRE_THRESHOLD           number(22,0),
 MY_UNIT_CODE_RATIO            number(22,8),
 MY_REPAIR_ELIGIBLE_AMOUNT     number(22,2),
 USER_ID                       varchar2(18),
 TIME_STAMP                    date,
 WORK_REQUEST                  varchar2(35) not null,
 CPI_ADD_COST                  number(22,2),
 RETIREMENT_UNIT_ID            number(22,0) default 0 not null,
 MY_CPI_ELIGIBLE_AMOUNT        number(22,2),
 MY_EXPENSE_REPLACE            number(22,0),
 MY_RETIREMENT_ELIGIBLE_AMOUNT number(22,2),
 REPAIR_QTY_INCLUDE            number(22,2) default 1 not null,
 RELATION_ID                   number(22,0),
 RELATION_ADD_QTY              number(22,2),
 RELATION_RETIRE_QTY           number(22,2),
 TE_AGGREGATION_ID             number(22,0),
 ADD_RETIRE_TOLERANCE_PCT      number(22,2),
 ADD_RETIRE_RATIO              number(22,2),
 TOTAL_PERIOD_ADDS_EXPENSE     number(22,2),
 TOTAL_PERIOD_RETS_EXPENSE     number(22,2),
 ADDS_TAX_EXPENSE              number(22,2),
 RETS_TAX_EXPENSE              number(22,2)
);

alter table REPAIR_WORK_ORDER_SEGMENTS
   add constraint PK_REPAIR_WORK_ORDER_SEGMENTS
       primary key (REPAIR_SCHEMA_ID, COMPANY_ID, WORK_ORDER_NUMBER, WORK_REQUEST,
                    REPAIR_UNIT_CODE_ID, REPAIR_LOCATION_ID, REPAIR_METHOD_ID,
                    RETIREMENT_UNIT_ID, REPAIR_QTY_INCLUDE)
       using index tablespace PWRPLANT_IDX;

alter table REPAIR_WORK_ORDER_SEGMENTS
   add constraint FK_TEAGGREGATE
       foreign key (TE_AGGREGATION_ID)
       references TE_AGGREGATION;

create table REPAIR_WO_SEG_REPORTING
(
 BATCH_ID                      NUMBER(22,0),
 REPAIR_SCHEMA_ID              NUMBER(22,0) NOT NULL,
 COMPANY_ID                    NUMBER(22,0) NOT NULL,
 WORK_ORDER_NUMBER             VARCHAR2(35) NOT NULL,
 REPAIR_UNIT_CODE_ID           NUMBER(22,0) NOT NULL,
 REPAIR_LOCATION_ID            NUMBER(22,0) NOT NULL,
 REPAIR_METHOD_ID              NUMBER(22,0) NOT NULL,
 ADD_QUANTITY                  NUMBER(22,0),
 RETIRE_QUANTITY               NUMBER(22,0),
 ADD_COST                      NUMBER(22,2),
 RETIRE_COST                   NUMBER(22,2),
 MIN_VINTAGE                   NUMBER(22,0),
 MAX_VINTAGE                   NUMBER(22,0),
 PRE81_QUANTITY                NUMBER(22,0),
 POST80_QUANTITY               NUMBER(22,0),
 MY_CONDITION                  VARCHAR2(35),
 MY_EVALUATE                   NUMBER(1,0),
 MY_USE_VINTAGE                NUMBER(1,0),
 MY_ADD_TEST                   NUMBER(1,0),
 MY_RETIRE_TEST                NUMBER(1,0),
 MY_FACTOR                     NUMBER(22,8),
 MY_QTY_FACTOR                 NUMBER(22,8),
 MY_USE_RATIO                  NUMBER(1,0),
 MY_USE_REPLACE                NUMBER(1,0),
 MY_QTY_OR_DOLLARS             NUMBER(1,0),
 MY_ADD_AND_RETIRE_TEST        NUMBER(1,0),
 MY_PRE81_RETIREMENT_RATIO     NUMBER(22,8),
 MY_TEST_QUANTITY              NUMBER(22,2),
 MY_TEST_QUANTITY2             NUMBER(22,2),
 MY_ADD_THRESHOLD              NUMBER(22,0),
 MY_RETIRE_THRESHOLD           NUMBER(22,0),
 MY_UNIT_CODE_RATIO            NUMBER(22,8),
 MY_REPAIR_ELIGIBLE_AMOUNT     NUMBER(22,2),
 USER_ID                       VARCHAR2(18),
 TIME_STAMP                    DATE,
 WORK_REQUEST                  VARCHAR2(35) NOT NULL,
 CPI_ADD_COST                  NUMBER(22,0),
 RETIREMENT_UNIT_ID            NUMBER(22,0) DEFAULT 0 NOT NULL,
 MY_CPI_ELIGIBLE_AMOUNT        NUMBER(22,0),
 MY_RETIREMENT_ELIGIBLE_AMOUNT NUMBER(22,2),
 MY_EXPENSE_REPLACE            NUMBER(22,0),
 REPAIR_QTY_INCLUDE            NUMBER(22,2) DEFAULT 1 NOT NULL,
 RELATION_ID                   NUMBER(22,0),
 RELATION_ADD_QTY              NUMBER(22,2),
 RELATION_RETIRE_QTY           NUMBER(22,2),
 TE_AGGREGATION_ID             NUMBER(22,0),
 ADD_RETIRE_TOLERANCE_PCT      NUMBER(22,2),
 ADD_RETIRE_RATIO              NUMBER(22,2),
 TOTAL_PERIOD_ADDS_EXPENSE     NUMBER(22,2),
 TOTAL_PERIOD_RETS_EXPENSE     NUMBER(22,2),
 ADDS_TAX_EXPENSE              NUMBER(22,2),
 RETS_TAX_EXPENSE              NUMBER(22,2)
);

alter table REPAIR_WO_SEG_REPORTING
   add constraint PK_REPAIR_WO_SEG_REPORTING
       primary key (BATCH_ID, REPAIR_SCHEMA_ID, COMPANY_ID, WORK_ORDER_NUMBER,
                    WORK_REQUEST, REPAIR_UNIT_CODE_ID, REPAIR_LOCATION_ID,
                    REPAIR_METHOD_ID, RETIREMENT_UNIT_ID, REPAIR_QTY_INCLUDE)
       using index tablespace PWRPLANT_IDX;

alter table REPAIR_WO_SEG_REPORTING
   add constraint FK_TEAGGREGATE_ID
       foreign key (TE_AGGREGATION_ID)
       references TE_AGGREGATION;

create table REPAIR_AMOUNT_ALLOCATE
(
 ASSET_ID                      number(22,0) not null,
 ASSET_ACTIVITY_ID             number(22,0) not null,
 COMPANY_ID                    number(22,0),
 WORK_ORDER_NUMBER             varchar2(35),
 REPAIR_UNIT_CODE_ID           number(22,0),
 REPAIR_LOCATION_ID            number(22,0),
 REPAIR_METHOD_ID              number(22,0),
 BOOK_SUMMARY_ID               number(22,0),
 ACTIVITY_COST                 number(22,2),
 ACTIVITY_COST_RATIO           number(22,8),
 MY_REPAIR_ELIGIBLE_AMOUNT     number(22,2),
 REPAIR_AMOUNT_SUB             number(22,2),
 USER_ID                       varchar2(18),
 TIME_STAMP                    date,
 WORK_REQUEST                  varchar2(35) not null,
 MY_RETIREMENT_ELIGIBLE_AMOUNT number(22,2),
 FERC_ACTIVITY_CODE            number(22,0)
);

alter table REPAIR_AMOUNT_ALLOCATE
   add constraint PK_REPAIR_AMOUNT_ALLOCATE
       primary key (ASSET_ID, ASSET_ACTIVITY_ID, WORK_REQUEST);

create table REPAIR_AMT_ALLOC_REPORTING
(
 BATCH_ID                      number(22,0),
 ASSET_ID                      number(22,0),
 ASSET_ACTIVITY_ID             number(22,0),
 COMPANY_ID                    number(22,0),
 WORK_ORDER_NUMBER             varchar2(35),
 REPAIR_UNIT_CODE_ID           number(22,0),
 REPAIR_LOCATION_ID            number(22,0),
 REPAIR_METHOD_ID              number(22,0),
 BOOK_SUMMARY_ID               number(22,0),
 ACTIVITY_COST                 number(22,2),
 ACTIVITY_COST_RATIO           number(22,8),
 MY_REPAIR_ELIGIBLE_AMOUNT     number(22,2),
 REPAIR_AMOUNT_SUB             number(22,2),
 USER_ID                       varchar2(18),
 TIME_STAMP                    date,
 WORK_REQUEST                  varchar2(35),
 MY_RETIREMENT_ELIGIBLE_AMOUNT number(22,2),
 FERC_ACTIVITY_CODE            number(22,0)
);

alter table REPAIR_AMT_ALLOC_REPORTING
   add constraint PK_REPAIR_AMT_ALLOC_REPORTING
       primary key (BATCH_ID, ASSET_ID, ASSET_ACTIVITY_ID, WORK_REQUEST)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (60, 0, 10, 3, 3, 0, 8747, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008747_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
