/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043707_cwip_failed_unitization_month_end_option_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 4/23/2015  Sarah Byers      Add month end option for Late Failed Unitization
||============================================================================
*/

-- new entry in pp_month_end_options for the Auto Unitization Late Months Job Server Default Value
-- Previous Months to Process default to 1
INSERT INTO pp_month_end_options (company_id, process_id, option_id, description, long_description, option_value, user_id, time_stamp)
SELECT -1 AS company_id,
 process_id,
 1 AS option_id,
 'Previous Months to Process',
 'Late Failed Unitization Previous Months to Process',
 '1',
 Sys_Context('USERENV','SESSION_USER'),
 SYSDATE
FROM pp_processes
WHERE executable_file = 'ssp_wo_auto_nonunitize.exe failed';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2521, 0, 2015, 1, 0, 0, 043707, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043707_cwip_failed_unitization_month_end_option_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;