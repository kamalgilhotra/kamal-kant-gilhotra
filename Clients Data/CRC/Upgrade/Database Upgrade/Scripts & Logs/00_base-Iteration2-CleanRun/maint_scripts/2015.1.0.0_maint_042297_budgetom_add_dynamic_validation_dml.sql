/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042297_budgetom_add_dynamic_validation_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 01/30/2015 Anand R          Add dynamic validation types for O&M
||============================================================================
*/

----insert validation types

insert into wo_validation_type
(wo_validation_type_id, description, long_description, function, find_company, col1, col2, col3, col4, col5, col6)
values
(1501, 'Dept Budget Entry', 'Dynamic validation used by Deparmental Budgeting Entry functionality.',
'w_cr_budget_entry_alt','select -1 from dual',
'call_location', 'i_selection_screen_dept_where_no_yr', 'cr_budget_version_id', 'i_component_id', 'row_id', 'i_template_id');

insert into wo_validation_type
(wo_validation_type_id, description, long_description, function, find_company, col1, col2, col3, col4, col5)
values
(1502, 'Dept Budget Entry Alt Labor', 'Dynamic validation used by Deparmental Budgeting Labor Entry and Labor Spreads functionality.', 
'w_cr_budget_entry_alt_labor','select -1 from dual',
'call_location', 'i_selection_screen_dept_where_no_yr', 'cr_budget_version_id', 'row_id', 'i_template_id');

insert into wo_validation_type
(wo_validation_type_id, description, long_description, function, find_company, col1, col2)
values
(1503, 'Dept Budget HR Security', 'Dynamic validation used by Deparmental Budgeting HR Security.', 
'w_cr_budget_labor_vfd','select -1 from dual',
'call_location', 'row_id');

insert into wo_validation_type
(wo_validation_type_id, description, long_description, function, find_company, col1, col2, col3, col4)
values
(1504, 'Dept Budget Labor Bal Rpt', 'Dynamic validation used by Deparmental Budgeting Labor Balancing Report.', 
'w_cr_budget_labor_report','select -1 from dual',
'call_location', 'i_selection_screen_dept_where_no_yr', 'cr_budget_version_id', 'i_template_id');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2238, 0, 2015, 1, 0, 0, 42297, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042297_budgetom_add_dynamic_validation_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;