/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035555_aro_trans_set_id.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/09/2014 Ryan Oliveria
||============================================================================
*/

alter table ARO add DEPR_TRANS_SET_ID number (22,0);

alter table ARO
   add constraint FK_ARO_DEPR_TRANS_SET
       foreign key (DEPR_TRANS_SET_ID)
       references DEPR_TRANS_SET (DEPR_TRANS_SET_ID);

comment on column ARO.DEPR_TRANS_SET_ID is 'A reference to the Depr Trans Set table.  Used for determining depr group allocation on regulated ARO''s.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (855, 0, 10, 4, 2, 0, 35555, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035555_aro_trans_set_id.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;