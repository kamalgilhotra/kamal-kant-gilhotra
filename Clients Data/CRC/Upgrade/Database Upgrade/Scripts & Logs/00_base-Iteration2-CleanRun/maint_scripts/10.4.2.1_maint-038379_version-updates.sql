/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint-038379_version-updates.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.1 05/30/2014 Lee Quinn
||============================================================================
*/

update PP_VERSION
   set PP_VERSION = 'Version 10.4.2.0',
       PP_PATCH   = 'Patch: 10.4.2.1',
       POWERTAX_VERSION = '10.4.2.1', --PowerTax version changed for the patch
       DATE_INSTALLED = sysdate
where PP_VERSION_ID = 1;

/*
update PP_PROCESSES
   set VERSION = '10.4.2.0'
 where trim(LOWER(EXECUTABLE_FILE)) in ('automatedreview.exe',
                                        'batreporter.exe',
                                        'budget_allocations.exe',
                                        'cpm.exe',
                                        'cr_allocations.exe',
                                        'cr_balances.exe',
                                        'cr_balances_bdg.exe',
                                        'cr_batch_derivation.exe',
                                        'cr_batch_derivation_bdg.exe',
                                        'cr_budget_entry_calc_all.exe',
                                        'cr_build_combos.exe',
                                        'cr_build_deriver.exe',
                                        'cr_delete_budget_allocations.exe',
                                        'cr_derivation_trueup.exe', --Next version cr_sap_trueup.exe will be renamed to cr_derivation_trueup.exe
                                        'cr_financial_reports_build.exe',
                                        'cr_flatten_structures.exe',
                                        'cr_man_jrnl_reversals.exe',
                                        'cr_posting.exe',
                                        'cr_posting_bdg.exe',
                                        'cr_sap_trueup.exe',
                                        'cr_sum.exe',
                                        'cr_sum_ab.exe',
                                        'cr_to_commitments.exe',
                                        'cwip_charge.exe',
                                        'populate_matlrec.exe',
                                        'ppmetricbatch.exe',
                                        'pptocr.exe',
                                        'ppverify.exe',
                                        'pp_archive.exe',
                                        'pp_integration.exe',
                                        'ssp_budget.exe',
                                        'ssp_depr_calc.exe');
*/

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
          select '10.4.2.1', 'automatedreview.exe',               '10.4.2.0' from dual
union all select '10.4.2.1', 'batreporter.exe',                   '10.4.2.0' from dual
union all select '10.4.2.1', 'budget_allocations.exe',            '10.4.2.0' from dual
union all select '10.4.2.1', 'cpm.exe',                           '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_allocations.exe',                '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_balances.exe',                   '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_balances_bdg.exe',               '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_batch_derivation.exe',           '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_batch_derivation_bdg.exe',       '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_budget_entry_calc_all.exe',      '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_build_combos.exe',               '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_build_deriver.exe',              '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_delete_budget_allocations.exe',  '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_derivation_trueup.exe',          '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_financial_reports_build.exe',    '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_flatten_structures.exe',         '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_man_jrnl_reversals.exe',         '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_posting.exe',                    '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_posting_bdg.exe',                '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_sap_trueup.exe',                 '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_sum.exe',                        '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_sum_ab.exe',                     '10.4.2.0' from dual
union all select '10.4.2.1', 'cr_to_commitments.exe',             '10.4.2.0' from dual
union all select '10.4.2.1', 'cwip_charge.exe',                   '10.4.2.0' from dual
union all select '10.4.2.1', 'populate_matlrec.exe',              '10.4.2.0' from dual
union all select '10.4.2.1', 'ppmetricbatch.exe',                 '10.4.2.0' from dual
union all select '10.4.2.1', 'pptocr.exe',                        '10.4.2.0' from dual
union all select '10.4.2.1', 'ppverify.exe',                      '10.4.2.0' from dual
union all select '10.4.2.1', 'pp_archive.exe',                    '10.4.2.0' from dual
union all select '10.4.2.1', 'pp_integration.exe',                '10.4.2.0' from dual
union all select '10.4.2.1', 'ssp_budget.exe',                    '10.4.2.0' from dual
union all select '10.4.2.1', 'ssp_depr_calc.exe',                 '10.4.2.0' from dual
minus select PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION from PP_VERSION_EXES;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1192, 0, 10, 4, 2, 1, 38379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.1_maint-038379_version-updates.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
