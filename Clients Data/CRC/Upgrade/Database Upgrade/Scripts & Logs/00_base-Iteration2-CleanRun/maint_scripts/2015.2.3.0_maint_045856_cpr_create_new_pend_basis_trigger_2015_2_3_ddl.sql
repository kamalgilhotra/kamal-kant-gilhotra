/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045856_cpr_create_new_pend_basis_trigger_2015_2_3_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.3.0 07/12/2016 Charlie Shilling prevent retirements from removing dollars from the retirement reversal basis bucket for tax repairs.
||============================================================================
*/
CREATE OR REPLACE TRIGGER pend_basis_rpr_zero_ret_rev
	BEFORE INSERT OR UPDATE ON pend_basis
	FOR EACH ROW
DECLARE
	l_activity_code 	pend_transaction.activity_code%TYPE;
	l_ret_rev_basis		book_summary.book_summary_id%TYPE;
	l_ret_rev_found		BOOLEAN := FALSE;
BEGIN
	SELECT Trim(pt.activity_code)
	INTO l_activity_code
	FROM pend_transaction pt
	WHERE pt.pend_trans_id = :new.pend_trans_id;

	--if this is a retirement transaction, make sure that the "Retirement Reversal" basis bucket is not retired from
	IF l_activity_code IN ('MRET','URET','URGL','SALE','SAGL') THEN
		BEGIN
			SELECT book_summary_id
			INTO l_ret_rev_basis
			FROM book_summary
			WHERE Lower(summary_name) = 'retirement reversal';

			l_ret_rev_found := TRUE;
		EXCEPTION
			WHEN No_Data_Found THEN
				--this is OK
				l_ret_rev_found := FALSE ;
			WHEN Too_Many_Rows THEN
				--this is not so OK.
				Raise_Application_Error(-20000, 'There were multiple book summaries found with the summary name "Retirement Reversal"');
		END;

		--only do special logic if we found a retirement reversal basis bucket
		IF l_ret_rev_found THEN
			--we know which basis bucket the retirement reversal is - make sure it is not being updated.
			--This case statement is not ideal, but since we don't know which basis bucket is the retirement reversals bucket
			--	and we can't do sql on the ":new" psuedovariable I thin this is the best work around.
			--We did include an error message in the event that the book_summary_id is an invalid number since, as of
			--	the writing of this trigger, we only have 70 basis buckets.
			CASE l_ret_rev_basis
				WHEN 1 THEN :new.basis_1 := 0;
				WHEN 2 THEN :new.basis_2 := 0;
				WHEN 3 THEN :new.basis_3 := 0;
				WHEN 4 THEN :new.basis_4 := 0;
				WHEN 5 THEN :new.basis_5 := 0;
				WHEN 6 THEN :new.basis_6 := 0;
				WHEN 7 THEN :new.basis_7 := 0;
				WHEN 8 THEN :new.basis_8 := 0;
				WHEN 9 THEN :new.basis_9 := 0;
				WHEN 10 THEN :new.basis_10 := 0;
				WHEN 11 THEN :new.basis_11 := 0;
				WHEN 12 THEN :new.basis_12 := 0;
				WHEN 13 THEN :new.basis_13 := 0;
				WHEN 14 THEN :new.basis_14 := 0;
				WHEN 15 THEN :new.basis_15 := 0;
				WHEN 16 THEN :new.basis_16 := 0;
				WHEN 17 THEN :new.basis_17 := 0;
				WHEN 18 THEN :new.basis_18 := 0;
				WHEN 19 THEN :new.basis_19 := 0;
				WHEN 20 THEN :new.basis_20 := 0;
				WHEN 21 THEN :new.basis_21 := 0;
				WHEN 22 THEN :new.basis_22 := 0;
				WHEN 23 THEN :new.basis_23 := 0;
				WHEN 24 THEN :new.basis_24 := 0;
				WHEN 25 THEN :new.basis_25 := 0;
				WHEN 26 THEN :new.basis_26 := 0;
				WHEN 27 THEN :new.basis_27 := 0;
				WHEN 28 THEN :new.basis_28 := 0;
				WHEN 29 THEN :new.basis_29 := 0;
				WHEN 30 THEN :new.basis_30 := 0;
				WHEN 31 THEN :new.basis_31 := 0;
				WHEN 32 THEN :new.basis_32 := 0;
				WHEN 33 THEN :new.basis_33 := 0;
				WHEN 34 THEN :new.basis_34 := 0;
				WHEN 35 THEN :new.basis_35 := 0;
				WHEN 36 THEN :new.basis_36 := 0;
				WHEN 37 THEN :new.basis_37 := 0;
				WHEN 38 THEN :new.basis_38 := 0;
				WHEN 39 THEN :new.basis_39 := 0;
				WHEN 40 THEN :new.basis_40 := 0;
				WHEN 41 THEN :new.basis_41 := 0;
				WHEN 42 THEN :new.basis_42 := 0;
				WHEN 43 THEN :new.basis_43 := 0;
				WHEN 44 THEN :new.basis_44 := 0;
				WHEN 45 THEN :new.basis_45 := 0;
				WHEN 46 THEN :new.basis_46 := 0;
				WHEN 47 THEN :new.basis_47 := 0;
				WHEN 48 THEN :new.basis_48 := 0;
				WHEN 49 THEN :new.basis_49 := 0;
				WHEN 50 THEN :new.basis_50 := 0;
				WHEN 51 THEN :new.basis_51 := 0;
				WHEN 52 THEN :new.basis_52 := 0;
				WHEN 53 THEN :new.basis_53 := 0;
				WHEN 54 THEN :new.basis_54 := 0;
				WHEN 55 THEN :new.basis_55 := 0;
				WHEN 56 THEN :new.basis_56 := 0;
				WHEN 57 THEN :new.basis_57 := 0;
				WHEN 58 THEN :new.basis_58 := 0;
				WHEN 59 THEN :new.basis_59 := 0;
				WHEN 60 THEN :new.basis_60 := 0;
				WHEN 61 THEN :new.basis_61 := 0;
				WHEN 62 THEN :new.basis_62 := 0;
				WHEN 63 THEN :new.basis_63 := 0;
				WHEN 64 THEN :new.basis_64 := 0;
				WHEN 65 THEN :new.basis_65 := 0;
				WHEN 66 THEN :new.basis_66 := 0;
				WHEN 67 THEN :new.basis_67 := 0;
				WHEN 68 THEN :new.basis_68 := 0;
				WHEN 69 THEN :new.basis_69 := 0;
				WHEN 70 THEN :new.basis_70 := 0;
				ELSE Raise_Application_Error(-20001, 'Invalid book_summary_id.');
			END CASE;
		END IF;
	END IF;
END ;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3269, 0, 2015, 2, 3, 0, 045856, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.3.0_maint_045856_cpr_create_new_pend_basis_trigger_2015_2_3_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;