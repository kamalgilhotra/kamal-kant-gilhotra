/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037617_reg_tax_add_interest_expense.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.5 04/14/2014 Sarah Byers
||============================================================================
*/

-- Add interest expense
alter table REG_CASE_CALCULATION add INTEREST_EXPENSE number(22,0);

comment on column REG_CASE_CALCULATION.INTEREST_EXPENSE is 'Amount of Interest Expense in dollars.';
comment on column REG_CASE_CALCULATION.WACC is 'Case weighted average cost of capital (percent).';
comment on column REG_CASE_CALCULATION.RETURN_ON_RATE_BASE is 'Rate Base times weighted average cost of capital in dollars.';
comment on column REG_CASE_CALCULATION.EQUITY_RETURN is 'Return on Rate Base less Associated Interest (Interest Expense) in dollars.';
comment on column REG_CASE_CALCULATION.GROSSUP_RATE is 'Gross Up Rate used in Equity Return Method Tax Calculation based on state and federal tax rates (percent).';
comment on column REG_CASE_CALCULATION.RETURN_BEFORE_TAX is 'Equity Return times Gross Up Rate in dollars.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1131, 0, 10, 4, 2, 5, 37617, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.5_maint_037617_reg_tax_add_interest_expense.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
