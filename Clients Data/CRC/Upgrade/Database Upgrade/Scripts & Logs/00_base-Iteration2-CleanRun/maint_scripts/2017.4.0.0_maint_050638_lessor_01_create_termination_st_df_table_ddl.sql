/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050638_lessor_01_create_termination_st_df_table_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/04/2018 Sisouphanh       Create new table to store termination info for Sales Type and Direct Finance
||============================================================================
*/

CREATE TABLE lsr_ilr_termination_st_df (
  ilr_id                        NUMBER(22,0)   NOT NULL,
  termination_date              DATE           NOT NULL,
  current_lease_receivable      NUMBER(22,2)   NOT NULL,
  future_payment		NUMBER(22,2)   NOT NULL,  
  net_lease_receivable          NUMBER(22,2)   NOT NULL,
  unguaranteed_residual		NUMBER(22,2)   NOT NULL,
  net_investment		NUMBER(22,0)   NULL,
  purchase_option_amount	NUMBER(22,2)   NOT NULL,  
  termination_amount		NUMBER(22,2)   NOT NULL,
  other_future_payments		NUMBER(22,2)   NOT NULL,  
  termination_source            VARCHAR2(35)   NOT NULL,  
  comments                      VARCHAR2(2000) NOT NULL,
  user_id                       VARCHAR2(18)   NULL,
  time_stamp                    DATE           NULL
);

ALTER TABLE lsr_ilr_termination_st_df
  ADD CONSTRAINT lsr_ilr_termination_st_df_pk PRIMARY KEY (
    ilr_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx;

ALTER TABLE lsr_ilr_termination_st_df
  ADD CONSTRAINT lsr_ilr_termination_st_df_fk FOREIGN KEY (
    ilr_id
  ) REFERENCES lsr_ilr (
    ilr_id
  )
;

COMMENT ON TABLE lsr_ilr_termination_st_df IS 'Table to store termination information for Sales Type and Direct Finance type ILR';

COMMENT ON COLUMN lsr_ilr_termination_st_df.ilr_id IS 'System generated id of the ILR';
COMMENT ON COLUMN lsr_ilr_termination_st_df.termination_date IS 'Date when the ILR will be terminated';
COMMENT ON COLUMN lsr_ilr_termination_st_df.current_lease_receivable IS 'Current Receivable Balance based on the termination date';
COMMENT ON COLUMN lsr_ilr_termination_st_df.future_payment IS 'Amount calculated by adding Purchase Option, Termination, and Other Future Payments/Cost';
COMMENT ON COLUMN lsr_ilr_termination_st_df.net_lease_receivable IS 'Current Receivable Balance based on Termination date less Future Payment';
COMMENT ON COLUMN lsr_ilr_termination_st_df.unguaranteed_residual IS 'Current Unguaranteed Residual Balance based on the termination date';
COMMENT ON COLUMN lsr_ilr_termination_st_df.net_investment IS 'Net Investment calculated by adding Net lease Receivable and Unguaranteed Residual';
COMMENT ON COLUMN lsr_ilr_termination_st_df.purchase_option_amount IS 'Purchase Option amount defaulted from ILR Details';
COMMENT ON COLUMN lsr_ilr_termination_st_df.termination_amount IS 'Termination amount defaulted from ILR Details';
COMMENT ON COLUMN lsr_ilr_termination_st_df.other_future_payments IS 'Termination amount defaulted from ILR Details';
COMMENT ON COLUMN lsr_ilr_termination_op.termination_source IS 'Stores where the termination originated - Early or Auto Termination';
COMMENT ON COLUMN lsr_ilr_termination_st_df.comments IS 'Comments for the ILR termination';
COMMENT ON COLUMN lsr_ilr_termination_st_df.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_termination_st_df.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6386, 0, 2017, 4, 0, 0, 50638, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050638_lessor_01_create_termination_st_df_table_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;