/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042727_pcm_create_dynval_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/03/2015 Alex P.          Add a dynamic validation on the create screen.
||============================================================================
*/

INSERT INTO wo_validation_type (wo_validation_type_id, description, long_description, FUNCTION, find_company, col1, col2, col3, col4, col5, col6, col7, hard_edit)
SELECT 90, 'WO/FP Initiation', 'WO/FP Initiation', 'w_wo_entry', 'select to_number(<arg6>) from dual', 
'funding_wo_indicator', 'work_order_number', 'funding_wo_id', 'work_order_type_id', 'budget_id', 'company_id', 'title', 1
FROM dual
WHERE NOT EXISTS (SELECT 1 FROM wo_validation_type WHERE wo_validation_type_id = 90);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2336, 0, 2015, 1, 0, 0, 042727, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042727_pcm_create_dynval_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;