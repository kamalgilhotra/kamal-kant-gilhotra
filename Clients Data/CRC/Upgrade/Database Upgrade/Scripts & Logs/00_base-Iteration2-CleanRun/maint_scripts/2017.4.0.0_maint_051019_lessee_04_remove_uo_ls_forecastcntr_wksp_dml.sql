/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051019_lessee_04_remove_uo_ls_forecastcntr_wksp_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/07/2018 Josh Sandler     Remove deprecated UO
||============================================================================
*/

DELETE FROM ppbase_workspace
WHERE workspace_uo_name = 'uo_ls_forecastcntr_wksp';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5188, 0, 2017, 4, 0, 0, 51019, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051019_lessee_04_remove_uo_ls_forecastcntr_wksp_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;