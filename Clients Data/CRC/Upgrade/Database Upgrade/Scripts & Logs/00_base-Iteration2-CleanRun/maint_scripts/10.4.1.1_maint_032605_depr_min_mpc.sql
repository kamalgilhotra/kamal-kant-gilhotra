/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032605_depr_min_mpc.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/14/2013 Ryan Oliveria
||============================================================================
*/

alter table CPR_DEPR_CALC_STG add MIN_MPC number(22,8);

comment on column CPR_DEPR_CALC_STG.MIN_MPC is 'The minimum mid period convention.  Used to calculate remaining life.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (688, 0, 10, 4, 1, 1, 32605, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032605_depr_min_mpc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
