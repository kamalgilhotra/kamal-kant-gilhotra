/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048369_lease_bal_sheet_detail_query_add_multicurrency_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 07/27/2017 Josh Sandler     Update Lease Balance Sheet Detail by Asset query for Multicurrency in Company currency
||============================================================================
*/

UPDATE pp_any_query_criteria_fields
SET column_order = column_order + 2
WHERE id = (SELECT id FROM pp_any_query_criteria WHERE description = 'Lease Balance Sheet Detail by Asset')
AND column_order >= 19;

INSERT INTO pp_any_query_criteria_fields
  (id, detail_field, column_order, amount_field, include_in_select_criteria, column_header, column_width, column_type, quantity_field, hide_from_results, hide_from_filters)
VALUES
  ((SELECT id FROM pp_any_query_criteria WHERE description = 'Lease Balance Sheet Detail by Asset'), 'currency_display_symbol', 19, 0, 0, 'Currency Symbol', 300, 'VARCHAR2', 0, 1, 1)
;

INSERT INTO pp_any_query_criteria_fields
  (id, detail_field, column_order, amount_field, include_in_select_criteria, column_header, column_width, column_type, quantity_field, hide_from_results, hide_from_filters)
VALUES
  ((SELECT id FROM pp_any_query_criteria WHERE description = 'Lease Balance Sheet Detail by Asset'), 'currency', 20, 0, 0, 'Currency', 300, 'VARCHAR2', 0, 0, 1)
;

UPDATE pp_any_query_criteria
SET description = 'Lease Balance Sheet Detail by Asset (Company Currency)',
SQL = 'select la.ls_asset_id, la.leased_asset_number, to_char(las.month,''yyyymm'') as monthnum, la.description as asset_description,
       la.company_id, co.description as company_description, ilr.ilr_id,
       ilr.ilr_number, ilr.external_ilr, ll.lease_id, ll.lease_number, lct.description as lease_cap_type,
       al.long_description as location, al.state_id as state, gl.external_account_code as account,
       ua.utility_account_id, ua.description as utility_account_description, fc.description as func_class_description,
       las.currency_display_symbol,
       las.iso_code AS currency,
       las.beg_capital_cost, las.end_capital_cost, las.beg_obligation, las.end_obligation,
       las.beg_lt_obligation, las.end_lt_obligation,
       case when las.month > depr_calc.month then las.begin_reserve else cprd.beg_reserve_month end as beg_reserve,
       case when las.month > depr_calc.month then las.end_reserve else cprd.depr_reserve end - nvl(cprd.retirements,0) as end_reserve,
       nvl(cprd. net_adds_and_adjust,0) as additions, nvl(cprd.retirements,0) + nvl(gain_loss,0) as retirements, nvl(cprd.transfers_in,0) as transfers_in,
       nvl(cprd.transfers_out,0) as transfers_out, nvl(cprd.reserve_trans_in,0) as reserve_trans_in, nvl(cprd.reserve_trans_out,0) as reserve_trans_out,
       (las.end_capital_cost - las.end_obligation - case when las.month > depr_calc.month then las.end_reserve else cprd.depr_reserve end ) as ferc_gaap_difference
       ,las.set_of_books_id,
       las.revision
from ls_asset la, ls_cpr_asset_map map, ls_ilr ilr, ls_lease ll, company co,
     cpr_ledger cpr, utility_account ua, business_segment bs, func_class fc, gl_account gl,
     v_ls_asset_schedule_fx_vw las, asset_location al,
     ls_ilr_options ilro, ls_lease_cap_type lct,
     (select m.ls_asset_id, c.*
      from cpr_depr c, ls_cpr_asset_map m, cpr_ledger l
      where c.asset_id = m.asset_id
        and m.asset_id = l.asset_id
        and to_char(c.gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(column_name)= ''MONTHNUM'')
        and l.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')) cprd,
     (select company_id, max(gl_posting_mo_yr) month
      from ls_process_control where depr_calc is not null
         and company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
      group by company_id) depr_calc
where la.ls_asset_id = map.ls_asset_id
  and map.asset_id = cpr.asset_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilro.revision = ilr.current_revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and lct.is_om = 0
  and cpr.gl_account_id = gl.gl_account_id
  and la.utility_account_id = ua.utility_account_id
  and la.bus_segment_id = ua.bus_segment_id
  and ua.func_class_id = fc.func_class_id
  and la.bus_segment_id = bs.bus_segment_id
  and co.company_id = la.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and depr_calc.company_id = co.company_id
  and las.month = cprd.gl_posting_mo_yr (+)
  and las.ls_asset_id = cprd.ls_asset_id (+)
  and las.set_of_books_id = cprd.set_of_books_id (+)
  and las.ls_cur_type = 2
  and la.asset_location_id = al.asset_location_id
  and to_char(las.month, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(column_name)= ''MONTHNUM'')
  and co.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')'
WHERE id = (SELECT id FROM pp_any_query_criteria WHERE description = 'Lease Balance Sheet Detail by Asset');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3612, 0, 2017, 1, 0, 0, 48369, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048369_lease_bal_sheet_detail_query_add_multicurrency_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;