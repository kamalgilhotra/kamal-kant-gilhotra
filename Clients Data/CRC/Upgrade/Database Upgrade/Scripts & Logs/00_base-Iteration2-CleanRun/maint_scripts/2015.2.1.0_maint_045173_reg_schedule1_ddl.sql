/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045173_reg_schedule1_ddl.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 11/03/2015 Shane "C" Ward  New Schedule Export DDL
||============================================================================
*/

CREATE TABLE reg_schedule (
  main_form            VARCHAR2(75)  NOT NULL,
  schedule_id          NUMBER(22,0)  NOT NULL,
  schedule_description VARCHAR2(254) NOT NULL,
  user_id              VARCHAR2(18)  NULL,
  time_stamp           DATE          NULL
)
;

ALTER TABLE reg_schedule
  ADD CONSTRAINT pk_reg_schedule PRIMARY KEY (
    main_form,
    schedule_id
  )
  using index tablespace PWRPLANT_IDX
;

ALTER TABLE reg_schedule
  ADD CONSTRAINT reg_schedule_unique UNIQUE (
  schedule_id)
  ;

comment on table reg_schedule is 'Main definition of Schedules;Reports for FERC Reporting';
comment on column reg_schedule.schedule_id is 'System assigned Identifier of Schedule';
comment on column reg_schedule.schedule_description is 'Name of Schedule';
comment on column reg_schedule.schedule_id is 'System assigned Identifier of Schedule';
comment on column reg_schedule.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';


CREATE TABLE reg_schedule_case_group (
  case_group_id NUMBER(22,0)   NOT NULL,
  description   VARCHAR2(35)   NOT NULL,
  notes         VARCHAR2(2000) NULL,
  user_id              VARCHAR2(18)  NULL,
  time_stamp           DATE          NULL
)
;

ALTER TABLE reg_schedule_case_group
  ADD CONSTRAINT pk_reg_schedule_case_group PRIMARY KEY (
    case_group_id
  )
using index tablespace PWRPLANT_IDX
;

comment on table reg_schedule_case_group is 'Main definition of Case Groups which hold cases and case years to be used with Schedule Reporting';
comment on column reg_schedule_case_group.case_group_id is 'System assigned identifier of Case Group';
comment on column reg_schedule_case_group.description is 'Name of Case Group';
comment on column reg_schedule_case_group.notes is 'Notes of Case Group';
comment on column reg_schedule_case_group.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule_case_group.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

CREATE TABLE reg_schedule_col_format (
  col_format_id NUMBER(22,0) NOT NULL,
  description   VARCHAR2(30) NOT NULL,
  format_mask   VARCHAR2(10) NULL,
  user_id              VARCHAR2(18)  NULL,
  time_stamp           DATE          NULL
)
;

ALTER TABLE reg_schedule_col_format
  ADD CONSTRAINT pk_reg_schedule_col_format PRIMARY KEY (
    col_format_id
  )
using index tablespace PWRPLANT_IDX
;

comment on table reg_schedule_col_format is 'Definition table of column formatting types to be applied to columns within a Schedule';
comment on column reg_schedule_col_format.col_format_id is 'System assigned identifier of column format';
comment on column reg_schedule_col_format.description is 'Name of Format Mask ex Currency, Decimal, Percent';
comment on column reg_schedule_col_format.format_mask is 'Actual excel mask to be applied to exported schedule';
comment on column reg_schedule_col_format.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule_col_format.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

CREATE TABLE reg_schedule_sql (
  schedule_id      NUMBER(22,0)   NOT NULL,
  page_title       VARCHAR2(60)   NULL,
  page_notes       VARCHAR2(2000) NULL,
  sheet_name       VARCHAR2(20)   NULL,
  page_footer      VARCHAR2(500)  NULL,
  schedule_type_id NUMBER(22,0)   NULL,
  any_query_id     NUMBER(22,0)   NULL,
  time_stamp       DATE           NULL,
  user_id          VARCHAR2(18)   NULL
)
;

ALTER TABLE reg_schedule_sql
  ADD CONSTRAINT pk_reg_schedule_sql PRIMARY KEY (
    schedule_id
  )
using index tablespace PWRPLANT_IDX
;

alter table reg_schedule_sql
	add constraint fk_reg_schedule_sql1 foreign key 
	(schedule_id) 
		references reg_schedule
	(schedule_id);

comment on table reg_schedule_sql is 'Detailed definition traits of a Schedule';
comment on column reg_schedule_sql.schedule_id is 'System assigned identifier of Schedule';
comment on column reg_schedule_sql.page_title is 'Page Title of Schedule to be shown on first row of schedule';
comment on column reg_schedule_sql.page_notes is 'Page Notes of Schedule shown on line below page_title';
comment on column reg_schedule_sql.sheet_name is 'Sheet Name of Schedule, applied to actual sheet name in Exported Excel';
comment on column reg_schedule_sql.page_footer is 'Page Footer of Schedule shown on last line of spreadsheet below broken out footers on reg_schedule_header';
comment on column reg_schedule_sql.schedule_type_id is 'Schedule Type of Schedule (ex. 1=Any Query leverages saved any queries, 2=standard leverages the reg schedule configuration)';
comment on column reg_schedule_sql.any_query_id is 'Saved CR Query ID to be used when schedule type is "Any Query"';
comment on column reg_schedule_sql.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule_sql.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

CREATE TABLE reg_schedule_line_type (
  line_type_id NUMBER(22,0) NOT NULL,
  description  VARCHAR2(35) NOT NULL,
  user_id              VARCHAR2(18)  NULL,
  time_stamp           DATE          NULL
)
;

ALTER TABLE reg_schedule_line_type
  ADD CONSTRAINT pk_reg_schedule_line_type PRIMARY KEY (
    line_type_id
  )
using index tablespace PWRPLANT_IDX
;

comment on table reg_schedule_line_type is 'Definition holding master list of Line Types for Schedules (ex. Formula, Reference, Value)';
comment on column reg_schedule_line_type.line_type_id is 'System assigned identifier of Line Type';
comment on column reg_schedule_line_type.description is 'Description of Line Type';
comment on column reg_schedule_line_type.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule_line_type.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

CREATE TABLE reg_schedule_line_number (
  schedule_id        NUMBER(22,0)   NOT NULL,
  line_id            NUMBER(22,0)   NOT NULL,
  line_number        VARCHAR2(10)   NULL,
  description        VARCHAR2(254)  NULL,
  line_type_id       NUMBER(22,0)   NULL,
  line_filter_string VARCHAR2(2000) NULL,
  line_order         NUMBER(22,0)   NULL,
  line_formula       VARCHAR2(254)  NULL,
  line_value1        VARCHAR2(35)   NULL,
  line_value2        VARCHAR2(35)   NULL,
  line_value3        VARCHAR2(35)   NULL,
  line_value4        VARCHAR2(35)   NULL,
  user_id              VARCHAR2(18)  NULL,
  time_stamp           DATE          NULL
)
;

ALTER TABLE reg_schedule_line_number
  ADD CONSTRAINT pk_reg_schedule_line_number PRIMARY KEY (
    schedule_id,
    line_id
  )
using index tablespace PWRPLANT_IDX
;

alter table reg_schedule_line_number
	add constraint fk_reg_schedule_line_number1 foreign key 
	(schedule_id) 
		references reg_schedule
	(schedule_id);

alter table reg_schedule_line_number
	add constraint fk_reg_schedule_line_number2 foreign key 
	(line_type_id) 
		references reg_schedule_line_type
	(line_type_id);
	
comment on table reg_schedule_line_number is 'Definition table of all lines by schedule.';
comment on column reg_schedule_line_number.schedule_Id is 'System assigned identifier of Schedule';
comment on column reg_schedule_line_number.line_Id is 'System assigned identifier of Line within Schedule';
comment on column reg_schedule_line_number.line_number is 'Line number of line item';
comment on column reg_schedule_line_number.description is 'Line Number description that shows on Schedule';
comment on column reg_schedule_line_number.line_type_id is 'System assigned identifier of Line Type (ex. Reference, formula, value)';
comment on column reg_schedule_line_number.line_filter_string is 'Filter string applied to line records to exclude results';
comment on column reg_schedule_line_number.line_order is 'Order of line in schedule';
comment on column reg_schedule_line_number.line_formula is 'Houses specific formula for line (FOR ALL COLUMNS) that does not apply to entire column, can be used for summing and other excel functionality';
comment on column reg_schedule_line_number.line_value1 is 'Value to be placed in column 1 before results columns';
comment on column reg_schedule_line_number.line_value2 is 'Value to be placed in column 2 before results columns';
comment on column reg_schedule_line_number.line_value3 is 'Value to be placed in column 3 before results columns';
comment on column reg_schedule_line_number.line_value4 is 'Value to be placed in column 4 before results columns';
comment on column reg_schedule_line_number.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule_line_number.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';


CREATE TABLE reg_schedule_column_tag (
  column_tag_id NUMBER(22,0) NOT NULL,
  description   VARCHAR2(35) NOT NULL,
  user_id              VARCHAR2(18)  NULL,
  time_stamp           DATE          NULL
)
;

ALTER TABLE reg_schedule_column_tag
  ADD CONSTRAINT pk_reg_schedule_column_tag PRIMARY KEY (
    column_tag_id
  )
using index tablespace PWRPLANT_IDX
;

comment on table reg_schedule_column_tag is 'Definition table holding all column tags that are used with columns. Customizeable by client with no code changes';
comment on column reg_schedule_column_tag.column_tag_id is 'System assigned identifier of column tag.';
comment on column reg_schedule_column_tag.description is 'Name of column tag shown in application.';
comment on column reg_schedule_column_tag.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule_column_tag.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

CREATE TABLE reg_schedule_col_type (
  col_type_id NUMBER(22,0) NOT NULL,
  description VARCHAR2(35) NOT NULL,
  user_id              VARCHAR2(18)  NULL,
  time_stamp           DATE          NULL
)
;

ALTER TABLE reg_schedule_col_type
  ADD CONSTRAINT pk_reg_schedule_col_type PRIMARY KEY (
    col_type_id
  )
using index tablespace PWRPLANT_IDX
;

comment on table reg_schedule_col_type is 'Definition table that holds all valid column types (ex. Reference, Formula, Values)';
comment on column reg_schedule_col_type.col_type_id is 'System assigned identifier of column type.';
comment on column reg_schedule_col_type.description is 'Name of column type shown in application.';
comment on column reg_schedule_col_type.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule_col_type.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';


CREATE TABLE reg_schedule_column (
  schedule_id       NUMBER(22,0)   NOT NULL,
  col_number        NUMBER(22,0)   NOT NULL,
  description       VARCHAR2(500)  NOT NULL,
  col_sql           VARCHAR2(2000) NULL,
  col_type_id       NUMBER(22,2)   NULL,
  col_filter_string VARCHAR2(2000) NULL,
  col_format_id     NUMBER(22,0)   NULL,
  column_tag_id     NUMBER(22,0)   NULL,
  user_id              VARCHAR2(18)  NULL,
  time_stamp           DATE          NULL
)
;

ALTER TABLE reg_schedule_column
  ADD CONSTRAINT pk_reg_schedule_column PRIMARY KEY (
    schedule_id,
    col_number
  )
using index tablespace PWRPLANT_IDX
;

alter table reg_schedule_column
	add constraint fk_reg_schedule_column1 foreign key 
	(schedule_id) 
		references reg_schedule
	(schedule_id);
	
alter table reg_schedule_column
	add constraint fk_reg_schedule_column2 foreign key 
	(column_tag_id) 
		references reg_schedule_column_tag
	(column_tag_id);
	
alter table reg_schedule_column
	add constraint fk_reg_schedule_column3 foreign key 
	(col_type_id) 
		references reg_schedule_col_type
	(col_type_id);
	
alter table reg_schedule_column
	add constraint fk_reg_schedule_column4 foreign key 
	(col_format_id) 
		references reg_schedule_col_format
	(col_format_id);

comment on table reg_schedule_column is 'Table holding all column definitions by schedule.';
comment on column reg_schedule_column.schedule_id is 'ID of schedule column is used with.';
comment on column reg_schedule_column.col_number is 'Identifier of column and also column order.';
comment on column reg_schedule_column.description is 'Name of column that is used in header of data.';
comment on column reg_schedule_column.col_sql is 'SQL that defines results of column.';
comment on column reg_schedule_column.col_type_id is 'ID of column type assigned to column (ex. Reference, Formula)';
comment on column reg_schedule_column.col_filter_string is 'Filter string to be applied to results of sql.';
comment on column reg_schedule_column.col_format_id is 'ID of column format to be applied to data within column.';
comment on column reg_schedule_column.column_tag_id is 'ID of column tag that tells code which case and case year end to apply to the column sql.';
comment on column reg_schedule_column.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule_column.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';


CREATE TABLE reg_schedule_line_col_formula (
  schedule_id   NUMBER(22,0)   NOT NULL,
  line_id       NUMBER(22,0)   NOT NULL,
  col_number    NUMBER(22,0)   NOT NULL,
  line_formula  VARCHAR2(2000) NULL,
  formula_notes VARCHAR2(254)  NULL,
  user_id       VARCHAR2(18)   NULL,
  time_stamp    DATE           NULL
)
;

ALTER TABLE reg_schedule_line_col_formula
  ADD PRIMARY KEY (
    schedule_id,
    line_id,
    col_number
  )
using index tablespace PWRPLANT_IDX
;

alter table reg_schedule_line_col_formula
	add constraint fk_reg_schedule_line_col_form1 foreign key 
	(schedule_id, line_id) 
		references reg_schedule_line_number
	(schedule_id, line_id);
	
alter table reg_schedule_line_col_formula
	add constraint fk_reg_schedule_line_col_form2 foreign key 
	(schedule_id, col_number) 
		references reg_schedule_column
	(schedule_id, col_number);
	
comment on table reg_schedule_line_col_formula is 'Holds definition of column-line specific formulas (Ex. sum in line 30 only for column 2)';
comment on column reg_schedule_line_col_formula.schedule_id is 'ID of schedule the line-column formula is to be applied to.)';
comment on column reg_schedule_line_col_formula.line_id is 'ID of line item to apply the column specific formula to.';
comment on column reg_schedule_line_col_formula.col_number is 'ID of column to apply the line specific formula to.';
comment on column reg_schedule_line_col_formula.line_formula is 'Formula for line-column combination';
comment on column reg_schedule_line_col_formula.formula_notes is 'Notes to be applied to formula cell for reference.';
comment on column reg_schedule_line_col_formula.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule_line_col_formula.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';


CREATE TABLE reg_schedule_line_acct (
  schedule_id NUMBER(22,0) NOT NULL,
  reg_acct_id NUMBER(22,0) NOT NULL,
  line_id     NUMBER(22,2) NOT NULL,
  sign        NUMBER(2,0)  NULL,
  user_id     VARCHAR2(18) NULL,
  time_stamp  DATE         NULL
)
;

ALTER TABLE reg_schedule_line_acct
  ADD CONSTRAINT pk_reg_schedule_line_acct PRIMARY KEY (
    schedule_id,
    reg_acct_id,
    line_id
  )
using index tablespace PWRPLANT_IDX
;

ALTER TABLE reg_schedule_line_acct
  ADD CONSTRAINT r_reg_sched_line_acct_1 FOREIGN KEY (
    schedule_id,
    line_id
  ) REFERENCES reg_schedule_line_number (
    schedule_id,
    line_id
  )
;

ALTER TABLE reg_schedule_line_acct
  ADD CONSTRAINT r_reg_sched_line_acct_2 FOREIGN KEY (
    reg_acct_id
  ) REFERENCES reg_acct_master (
    reg_acct_id
  )
;

comment on table reg_schedule_line_acct is 'Table holds assignment of Reg Accounts to Line Items per schedule.';
comment on column reg_schedule_line_acct.schedule_id is 'ID of schedule the assignment is used in.)'; 
comment on column reg_schedule_line_acct.line_id is 'ID of line item having account assigned to.';
comment on column reg_schedule_line_acct.reg_acct_id is 'ID of Regulatory Account assigned to Line Item.';
comment on column reg_schedule_line_acct.sign is 'Sign to be applied to amount of account (not line item).';
comment on column reg_schedule_line_acct.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule_line_acct.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';




CREATE TABLE reg_schedule_header (
  schedule_id        NUMBER(22,0)  NOT NULL,
  desc_header_left   VARCHAR2(254) NULL,
  desc_header_center VARCHAR2(254) NULL,
  desc_header_right  VARCHAR2(254) NULL,
  desc_footer_left   VARCHAR2(254) NULL,
  desc_footer_center VARCHAR2(254) NULL,
  desc_footer_right  VARCHAR2(254) NULL,
  user_id            VARCHAR2(18)  NULL,
  time_stamp         DATE          NULL
)
;

alter table reg_schedule_header
	add constraint pk_reg_schedule_header primary key 
	(schedule_id)
	using index tablespace PWRPLANT_IDX
;
	
alter table reg_schedule_header
	add constraint fk_reg_schedule_header1 foreign key 
	(schedule_id) 
		references reg_schedule
	(schedule_id);
	
comment on table reg_schedule_header is 'Header and Footer information for Schedule Header and Footer';
comment on column reg_schedule_header.schedule_id is 'ID of schedule the header is used for.'; 
comment on column reg_schedule_header.desc_header_left is 'Header information placed in top left of headers section';
comment on column reg_schedule_header.desc_header_center is 'Header information placed in top center of headers section';
comment on column reg_schedule_header.desc_header_right is 'Header information placed in top right of headers section';
comment on column reg_schedule_header.desc_footer_left is 'Footer information placed in bottom left of footers section';
comment on column reg_schedule_header.desc_footer_center is 'Footer information placed in bottom center of footers section';
comment on column reg_schedule_header.desc_footer_right is 'Footer information placed in bottom right of footers section';
comment on column reg_schedule_header.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule_header.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';


CREATE TABLE reg_schedule_case_group_assign (
  case_group_id NUMBER(22,0) NOT NULL,
  reg_case_id   NUMBER(22,0) NULL,
  column_tag_id NUMBER(22,0) NOT NULL,
  case_year     NUMBER(22,0) NULL,
  user_id              VARCHAR2(18)  NULL,
  time_stamp           DATE          NULL
)
;

comment on table reg_schedule_case_group_assign is 'Table holding case assignment to Groups to be used with Schedules.';
comment on column reg_schedule_case_group_assign.case_group_id is 'System assigned identifier of Case Group.';
comment on column reg_schedule_case_group_assign.reg_case_id is 'System assigned identifier of Reg Case';
comment on column reg_schedule_case_group_assign.column_tag_id is 'System assigned identifier of column tag, to be used to tell code where to use case based on column-column tag assignment';
comment on column reg_schedule_case_group_assign.case_year is 'Case Year of Case to be used with column tag.';
comment on column reg_schedule_case_group_assign.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_schedule_case_group_assign.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

ALTER TABLE reg_schedule_case_group_assign
  ADD CONSTRAINT pk_reg_sched_case_group_assign PRIMARY KEY (
    case_group_id,
    column_tag_id
  )
using index tablespace PWRPLANT_IDX
;

alter table reg_schedule_case_group_assign
	add constraint fk_reg_schedule_case_grp_ass1 foreign key 
	(case_group_id) 
		references reg_schedule_case_group
	(case_group_id);
	
alter table reg_schedule_case_group_assign
	add constraint fk_reg_schedule_case_grp_ass2 foreign key 
	(reg_case_id) 
		references reg_case
	(reg_case_id);
	
alter table reg_schedule_case_group_assign
	add constraint fk_reg_schedule_case_grp_ass3 foreign key 
	(column_tag_id) 
		references reg_schedule_column_tag
	(column_tag_id);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2957, 0, 2015, 2, 1, 0, 45173, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.1.0_maint_045173_reg_schedule1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;