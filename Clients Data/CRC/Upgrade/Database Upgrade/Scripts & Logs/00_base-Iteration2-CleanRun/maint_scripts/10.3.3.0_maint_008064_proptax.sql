/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008064_proptax.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/01/2011 Julia Breuer   Point Release
||============================================================================
*/

insert into PTC_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, IS_BASE_OPTION)
values
   ('Parcel Center - Copy Assessments - Copy Authority-District Relationships Default',
    'When copying assessments to another case, whether the copy authority-district relationships option is selected by default or not.',
    0, 'No', 1);

insert into PTC_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Parcel Center - Copy Assessments - Copy Authority-District Relationships Default', 'No');

insert into PTC_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Parcel Center - Copy Assessments - Copy Authority-District Relationships Default', 'Yes');

-- 01052012
--
-- Update pp_version for the new proptax version.
--
update PP_VERSION set PROP_TAX_VERSION = 'v10.3.3.0';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (36, 0, 10, 3, 3, 0, 8064, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008064_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
