 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_042305_om_cr_budget_sum_values_map_ddl.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 01/23/2014   Anand R        Modify table column type from number to varchar
 ||============================================================================
 */ 

 
alter table CR_BUDGET_SUM_VALUES_MAP modify ORIGINAL_VALUE varchar2(75) ;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2218, 0, 2015, 1, 0, 0, 42305, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042305_om_cr_budget_sum_values_map_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;