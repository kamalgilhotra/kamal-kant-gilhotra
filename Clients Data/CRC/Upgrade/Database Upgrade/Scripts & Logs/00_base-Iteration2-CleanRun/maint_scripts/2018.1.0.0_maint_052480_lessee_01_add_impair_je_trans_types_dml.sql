/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052480_lessee_01_add_impair_je_trans_types_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2018.1.0.0  11/09/2018 K Powers      Add new trans types for Impairments
||============================================================================
*/


DECLARE

 new_id number;

BEGIN

 select max(JE_ID) + 1 into new_id from STANDARD_JOURNAL_ENTRIES;

 -- Set up Impairment Journal Entries
 insert into STANDARD_JOURNAL_ENTRIES 
 (JE_ID,GL_JE_CODE,EXTERNAL_JE_CODE,DESCRIPTION,LONG_DESCRIPTION)
 values
 (new_id,'LEASEIMPAIR','LEASEIMPAIR','LAM Impairment','Lease Asset Impairments');

 insert into gl_je_control 
 (process_id,je_id,dr_table,dr_column,cr_table,cr_column)
 values
 ('LEASEIMPAIR',new_id,'NONE','NONE','NONE','NONE');

END;
/

-- Insert Journal Types for Impairments
insert into je_trans_type (trans_type, description)
select 3080, '3080 - Impairment Accum Amort Credit'
  from dual
 where not exists (select 1 from je_trans_type where trans_type = 3080);

insert into je_method_trans_type (je_method_id, trans_type)
select 1, trans_type from je_trans_type
 where trans_type = 3080
   and not exists (select 1 from je_method_trans_type where trans_type = 3080 and je_method_id = 1);

insert into je_trans_type (trans_type, description)
select 3081, '3081 - Impairment Accum Amort Debit'
  from dual
 where not exists (select 1 from je_trans_type where trans_type = 3081);

insert into je_method_trans_type (je_method_id, trans_type)
select 1, trans_type from je_trans_type
 where trans_type = 3081
   and not exists (select 1 from je_method_trans_type where trans_type = 3081 and je_method_id = 1);
   
insert into je_trans_type (trans_type, description)
select 3082, '3082 - Impairment Debit'
  from dual
 where not exists (select 1 from je_trans_type where trans_type = 3082);

insert into je_method_trans_type (je_method_id, trans_type)
select 1, trans_type from je_trans_type
 where trans_type = 3082
   and not exists (select 1 from je_method_trans_type where trans_type = 3082 and je_method_id = 1);
   
insert into je_trans_type (trans_type, description)
select 3083, '3083 - Impairment Reversal Credit'
  from dual
 where not exists (select 1 from je_trans_type where trans_type = 3083);

insert into je_method_trans_type (je_method_id, trans_type)
select 1, trans_type from je_trans_type
 where trans_type = 3083
   and not exists (select 1 from je_method_trans_type where trans_type = 3083 and je_method_id = 1);
   
-- Insert Journal Types for Impairment Retirements
insert into je_trans_type (trans_type, description)
select 3084, '3084 - Impairment Accum Amor Clearing'
  from dual
 where not exists (select 1 from je_trans_type where trans_type = 3084);
 
insert into je_method_trans_type (je_method_id, trans_type)
select 1, trans_type from je_trans_type
 where trans_type = 3084
   and not exists (select 1 from je_method_trans_type where trans_type = 3084 and je_method_id = 1);
   

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(12043, 0, 2018, 1, 0, 0, 52480, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052480_lessee_01_add_impair_je_trans_types_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 
