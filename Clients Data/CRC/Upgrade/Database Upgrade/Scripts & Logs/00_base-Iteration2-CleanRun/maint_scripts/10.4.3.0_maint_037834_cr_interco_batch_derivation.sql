/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037834_cr_interco_batch_derivation.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 05/12/2014 Kyle Peterson
||============================================================================
*/

insert into CR_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, LONG_DESCRIPTION)
values
   (270, 'Cross Charge Company column name', 'The name of the element used for intercompany transactions');

alter table CR_DERIVER_ADDITIONAL_FIELDS add DERIVE_VALUE number(22,0);
comment on column CR_DERIVER_ADDITIONAL_FIELDS.DERIVE_VALUE is '1 indicates that the value in the field on cr_deriver_control should be included as a derived field, 0 indicates that it is informational only.  0 is the default';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1156, 0, 10, 4, 3, 0, 37834, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037834_cr_interco_batch_derivation.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;