/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040976_cpr_ssp_close_pp_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 12/16/2014 Sarah Byers      SSP: Close PowerPlant
||============================================================================
*/

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION, EXECUTABLE_FILE, VERSION, ALLOW_CONCURRENT, SYSTEM_LOCK_ENABLED,
    SYSTEM_LOCK, ASYNC_EMAIL_ON_COMPLETE)
   select (select max(PROCESS_ID) + 1 from PP_PROCESSES), 'Close PowerPlant', 'Close PowerPlant', 'ssp_close_powerplant.exe', '2015.1.0.0', 0, 0, 0, 0
     from DUAL
    where not exists (select 1
                        from PP_PROCESSES
                       where EXECUTABLE_FILE = 'ssp_close_powerplant.exe'
                         and VERSION = '2015.1.0.0');

insert into wo_validation_type (
	wo_validation_type_id, description, long_description, "FUNCTION", find_company, col1, col2, col3, hard_edit)
values (
	1008, 'CPR - Close PowerPlant', 'CPR Control - Close PowerPlant', 'various', 'select company_id from company_setup where company_id = <arg2>', 'month', 'company_id', 'dw_allo_depr_reserve', 1);
	
	

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2121, 0, 2015, 1, 0, 0, 040976, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040976_cpr_ssp_close_pp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;