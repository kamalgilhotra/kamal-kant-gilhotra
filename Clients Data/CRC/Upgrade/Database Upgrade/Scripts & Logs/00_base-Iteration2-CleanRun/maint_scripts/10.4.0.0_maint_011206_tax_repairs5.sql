/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011206_tax_repairs5.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   02/11/2013 Blake Andrews
||============================================================================
*/

update ASSET_LOCATION
   set REPAIR_LOCATION_ID = null
 where REPAIR_LOCATION_ID in
       (select REPAIR_LOCATION_ID from REPAIR_LOCATION where DESCRIPTION = 'Default Location');

update WORK_ORDER_TAX_STATUS
   set REPAIR_LOCATION_ID = null
 where REPAIR_LOCATION_ID in
       (select REPAIR_LOCATION_ID from REPAIR_LOCATION where DESCRIPTION = 'Default Location');

update WORK_ORDER_TAX_REPAIRS
   set REPAIR_LOCATION_ID = null
 where REPAIR_LOCATION_ID in
       (select REPAIR_LOCATION_ID from REPAIR_LOCATION where DESCRIPTION = 'Default Location');

delete from REPAIR_THRESHOLDS
 where REPAIR_LOCATION_ID in
       (select REPAIR_LOCATION_ID from REPAIR_LOCATION where DESCRIPTION = 'Default Location');

delete from REPAIR_LOC_ROLLUP_MAPPING
 where REPAIR_LOCATION_ID in
       (select REPAIR_LOCATION_ID from REPAIR_LOCATION where DESCRIPTION = 'Default Location');

delete from REPAIR_LOCATION where DESCRIPTION = 'Default Location';

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = 'yes'
 where CONTROL_NAME in ('Tax Expense User Object',
                        'Tax Expense User Object - WO',
                        'Tax Expense Log Messages',
                        'Tax Exp - Ovh to Udgrd Rplcmt Check',
                        'TAX ADD KEEP WORK ORDER');

update PP_SYSTEM_CONTROL_COMPANY
   set DESCRIPTION = 'dw_yes_no;1'
 where CONTROL_NAME = 'TAX ADD KEEP WORK ORDER';

update CLASS_CODE set CWIP_INDICATOR = 0, CPR_INDICATOR = 0 where DESCRIPTION = 'Tax Expensing';

create table WORK_ORDER_CLASS_CODE_DELBKUP as
   select *
     from WORK_ORDER_CLASS_CODE
    where CLASS_CODE_ID in (select CLASS_CODE_ID from CLASS_CODE where DESCRIPTION = 'Tax Expensing');

delete from WORK_ORDER_CLASS_CODE
 where CLASS_CODE_ID in (select CLASS_CODE_ID from CLASS_CODE where DESCRIPTION = 'Tax Expensing');

update PPBASE_MENU_ITEMS set ITEM_ORDER = 4 where IDENTIFIER = 'menu_wksp_tax_exp_tests';
update PPBASE_MENU_ITEMS set ITEM_ORDER = 5 where IDENTIFIER = 'menu_wksp_work_order';
update PPBASE_MENU_ITEMS set ITEM_ORDER = 6 where IDENTIFIER = 'menu_wksp_asset';
update PPBASE_MENU_ITEMS set ITEM_ORDER = 7 where IDENTIFIER = 'menu_wksp_threshold_estimate';

--
-- Add an expense flag to the tax status table.
--
alter table WO_TAX_STATUS add EXPENSE_YN number(22,0) default 0;

update WO_TAX_STATUS set EXPENSE_YN = 1 where REPAIR_SCHEMA_ID is not null;
update WO_TAX_STATUS set EXPENSE_YN = 0 where REPAIR_SCHEMA_ID is null;

alter table WO_TAX_STATUS modify EXPENSE_YN not null;

--
-- Update the Tax Status import type and templates to have expense_yn instead of repair_schema_id.
--
insert into PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 108, 'expense_yn', sysdate, user, 'Expense Flag', 'expense_yn_xlate', 1, 1, 'number(22,0)', 'yes_no', '', 1, null );

insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 108, 'expense_yn', 77, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 108, 'expense_yn', 78, sysdate, user );

update PP_IMPORT_TEMPLATE_FIELDS
   set COLUMN_NAME = 'expense_yn', IMPORT_LOOKUP_ID = 77
 where COLUMN_NAME = 'repair_schema_id'
   and IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PWRPLANT.PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 108);

delete from PP_IMPORT_COLUMN_LOOKUP where IMPORT_TYPE_ID = 108 and COLUMN_NAME = 'repair_schema_id';
delete from PP_IMPORT_COLUMN        where IMPORT_TYPE_ID = 108 and COLUMN_NAME = 'repair_schema_id';

alter table RPR_IMPORT_TAX_STATUS     add EXPENSE_YN_XLATE varchar2(254);
alter table RPR_IMPORT_TAX_STATUS     add EXPENSE_YN       number(22,0);
alter table RPR_IMPORT_TAX_STATUS_arc add EXPENSE_YN_XLATE varchar2(254);
alter table RPR_IMPORT_TAX_STATUS_arc add EXPENSE_YN       number(22,0);

alter table RPR_IMPORT_TAX_STATUS     drop column REPAIR_SCHEMA_XLATE;
alter table RPR_IMPORT_TAX_STATUS     drop column REPAIR_SCHEMA_ID;
alter table RPR_IMPORT_TAX_STATUS_ARC drop column REPAIR_SCHEMA_XLATE;
alter table RPR_IMPORT_TAX_STATUS_ARC drop column REPAIR_SCHEMA_ID;

--
-- Remove unused columns from repair_thresholds.
--
alter table REPAIR_THRESHOLDS drop column UNIT_CODE_RATIO;
alter table REPAIR_THRESHOLDS drop column RETIRE_TEST;

update PP_IMPORT_TEMPLATE_FIELDS FLDS
   set FIELD_ID = FIELD_ID + 100
 where IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 101)
   and exists (select 'x'
          from PP_IMPORT_TEMPLATE_FIELDS INFLDS
         where INFLDS.IMPORT_TEMPLATE_ID = FLDS.IMPORT_TEMPLATE_ID
           and COLUMN_NAME = 'retire_test')
   and FIELD_ID > (select FIELD_ID
                     from PP_IMPORT_TEMPLATE_FIELDS INFLDS
                    where INFLDS.IMPORT_TEMPLATE_ID = FLDS.IMPORT_TEMPLATE_ID
                      and COLUMN_NAME = 'retire_test');

delete from PP_IMPORT_TEMPLATE_FIELDS
 where COLUMN_NAME = 'retire_test'
   and IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 101);

update PP_IMPORT_TEMPLATE_FIELDS FLDS
   set FIELD_ID = FIELD_ID - 101
 where IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 101)
   and FIELD_ID > 100;

update PP_IMPORT_TEMPLATE_FIELDS FLDS
   set FIELD_ID = FIELD_ID + 100
 where IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 101)
   and exists (select 'x'
          from PP_IMPORT_TEMPLATE_FIELDS INFLDS
         where INFLDS.IMPORT_TEMPLATE_ID = FLDS.IMPORT_TEMPLATE_ID
           and COLUMN_NAME = 'unit_code_ratio')
   and FIELD_ID > (select FIELD_ID
                     from PP_IMPORT_TEMPLATE_FIELDS INFLDS
                    where INFLDS.IMPORT_TEMPLATE_ID = FLDS.IMPORT_TEMPLATE_ID
                      and COLUMN_NAME = 'unit_code_ratio');

delete from PP_IMPORT_TEMPLATE_FIELDS
 where COLUMN_NAME = 'unit_code_ratio'
   and IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 101);

update PP_IMPORT_TEMPLATE_FIELDS FLDS
   set FIELD_ID = FIELD_ID - 101
 where IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 101)
   and FIELD_ID > 100;

delete from PP_IMPORT_COLUMN
 where IMPORT_TYPE_ID = 101
   and COLUMN_NAME in ('retire_test', 'unit_code_ratio');

alter table RPR_IMPORT_THRESHOLD     drop column RETIRE_TEST;
alter table RPR_IMPORT_THRESHOLD     drop column UNIT_CODE_RATIO;
alter table RPR_IMPORT_THRESHOLD_ARC drop column RETIRE_TEST;
alter table RPR_IMPORT_THRESHOLD_ARC drop column UNIT_CODE_RATIO;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'PWRPLANT',
          TO_TIMESTAMP('22-03-2013 16:41:30', 'DD-MM-YYYY HH24:MI:SS'),
          'In-Service WO Tax Repairs Details',
          'Tax Repairs for In-Service Work Orders grouped by Company',
          null,
          'dw_rpr_rpt_inservice_details',
          'REPAIRS',
          null,
          null,
          'REPRS - 1207',
          null,
          null,
          null,
          13,
          100,
          43,
          34,
          1,
          3,
          null,
          null,
          TO_TIMESTAMP('22-03-2013 16:41:30', 'DD-MM-YYYY HH24:MI:SS'),
          null,
          null,
          0
     from PP_REPORTS;

update WO_DELETE
   set SORT_ORDER = 5150
 where ID = 53
   and TABLE_NAME = 'WORK_ORDER_TAX_REPAIRS';

-- Attempt to delete duplicate records before creating a unique index. This may fail due to foreign key(s), if data is in use and then needs to be cleaned up manually.
delete from REPAIR_LOCATION RL
 where (RL.REPAIR_LOCATION_ID, RL.DESCRIPTION) <>
       (select min(REPAIR_LOCATION_ID), REPAIR_LOCATION.DESCRIPTION
          from REPAIR_LOCATION,
               (select DESCRIPTION, count(1)
                  from REPAIR_LOCATION
                 group by DESCRIPTION
                having count(1) > 1) DUPL_VIEW
         where REPAIR_LOCATION.DESCRIPTION = DUPL_VIEW.DESCRIPTION
         group by REPAIR_LOCATION.DESCRIPTION);

-- If duplicate tests are used, reassign the tests with the same description to the one with a min(test_id)
update WORK_ORDER_TYPE WOT
   set WOT.TAX_EXPENSE_TEST_ID =
        (select distinct WO_TAX_EXPENSE_TEST.TAX_EXPENSE_TEST_ID
           from WO_TAX_EXPENSE_TEST,
                WO_TAX_EXPENSE_TEST WTET_WOT,
                (select DESCRIPTION, count(1)
                   from WO_TAX_EXPENSE_TEST
                  group by DESCRIPTION
                 having count(1) > 1) DUPL_VIEW
          where WO_TAX_EXPENSE_TEST.DESCRIPTION = DUPL_VIEW.DESCRIPTION
            and WTET_WOT.DESCRIPTION = DUPL_VIEW.DESCRIPTION
            and WOT.TAX_EXPENSE_TEST_ID = WTET_WOT.TAX_EXPENSE_TEST_ID
            and WO_TAX_EXPENSE_TEST.TAX_EXPENSE_TEST_ID =
                (select min(WTET_INNER.TAX_EXPENSE_TEST_ID)
                   from WO_TAX_EXPENSE_TEST WTET_INNER
                  where WTET_INNER.DESCRIPTION = WO_TAX_EXPENSE_TEST.DESCRIPTION))
 where WOT.TAX_EXPENSE_TEST_ID in
       (select distinct WO_TAX_EXPENSE_TEST.TAX_EXPENSE_TEST_ID
          from WO_TAX_EXPENSE_TEST,
               WO_TAX_EXPENSE_TEST WTET_WOT,
               (select DESCRIPTION, count(1)
                  from WO_TAX_EXPENSE_TEST
                 group by DESCRIPTION
                having count(1) > 1) DUPL_VIEW
         where WO_TAX_EXPENSE_TEST.DESCRIPTION = DUPL_VIEW.DESCRIPTION
           and WTET_WOT.DESCRIPTION = DUPL_VIEW.DESCRIPTION
           and WOT.TAX_EXPENSE_TEST_ID = WTET_WOT.TAX_EXPENSE_TEST_ID
           and WO_TAX_EXPENSE_TEST.TAX_EXPENSE_TEST_ID <>
               (select min(WTET_INNER.TAX_EXPENSE_TEST_ID)
                  from WO_TAX_EXPENSE_TEST WTET_INNER
                 where WTET_INNER.DESCRIPTION = WO_TAX_EXPENSE_TEST.DESCRIPTION));

-- Attempt to delete duplicate records before creating a unique index. This may fail due to foreign key(s), if data is in use and then needs to be cleaned up manually.
delete from WO_TAX_EXPENSE_TEST WTET
 where (WTET.TAX_EXPENSE_TEST_ID, WTET.DESCRIPTION) in
       (select TAX_EXPENSE_TEST_ID, WO_TAX_EXPENSE_TEST.DESCRIPTION
          from WO_TAX_EXPENSE_TEST,
               (select DESCRIPTION, count(1)
                  from WO_TAX_EXPENSE_TEST
                 group by DESCRIPTION
                having count(1) > 1) DUPL_VIEW
         where WO_TAX_EXPENSE_TEST.DESCRIPTION = DUPL_VIEW.DESCRIPTION
           and TAX_EXPENSE_TEST_ID <>
               (select min(WTET_INNER.TAX_EXPENSE_TEST_ID)
                  from WO_TAX_EXPENSE_TEST WTET_INNER
                 where WTET_INNER.DESCRIPTION = WO_TAX_EXPENSE_TEST.DESCRIPTION));

-- Attempt to delete duplicate records before creating a unique index. This may fail due to foreign key(s), if data is in use and then needs to be cleaned up manually.
delete from WO_TAX_STATUS WTS
 where (WTS.TAX_STATUS_ID, WTS.DESCRIPTION) <>
       (select min(TAX_STATUS_ID), WO_TAX_STATUS.DESCRIPTION
          from WO_TAX_STATUS,
               (select DESCRIPTION, count(1)
                  from WO_TAX_STATUS
                 group by DESCRIPTION
                having count(1) > 1) DUPL_VIEW
         where WO_TAX_STATUS.DESCRIPTION = DUPL_VIEW.DESCRIPTION
         group by WO_TAX_STATUS.DESCRIPTION);

create unique index IDX_RL_DESCRIPTION
   on REPAIR_LOCATION (DESCRIPTION)
      tablespace PWRPLANT_IDX;

create unique index IDX_WTET_DESCRIPTION
   on WO_TAX_EXPENSE_TEST (DESCRIPTION)
      tablespace PWRPLANT_IDX;

create unique index IDX_WTS_DESCRIPTION
   on WO_TAX_STATUS (DESCRIPTION)
      tablespace PWRPLANT_IDX;

update PP_REPORTS set PP_REPORT_FILTER_ID = 37 where REPORT_NUMBER = 'REPRS - 1204';
update PP_REPORTS set PP_REPORT_FILTER_ID = 35 where REPORT_NUMBER = 'REPRS - 1205';

update RETIREMENT_UNIT set REPAIR_QTY_INCLUDE = 1 where REPAIR_QTY_INCLUDE is null;

alter table RETIREMENT_UNIT modify REPAIR_QTY_INCLUDE not null;

delete from PPBASE_MENU_ITEMS where IDENTIFIER = 'menu_wksp_processing_wo_test';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (337, 0, 10, 4, 0, 0, 11206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011206_tax_repairs5.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;