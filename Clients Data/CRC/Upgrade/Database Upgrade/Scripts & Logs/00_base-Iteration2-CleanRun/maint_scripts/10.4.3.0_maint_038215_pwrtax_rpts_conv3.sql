/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038215_pwrtax_rpts_conv3.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 07/22/2014 Anand Rajashekar    Third set of PowerTax reports
||                                         conversion to Base Gui
||============================================================================
*/

--Anand Report Id 54, 400008

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (400008, 'PWRPLANT', TO_DATE('16-JUL-14', 'DD-MON-RR'), 'Book/Tax Activity Report Vintage',
    'Summarizes the Book and Tax Activity. Vintage with Tax Class underneath  Run for 1 Tax Year ONLY', 'PowerTax',
    'dw_tax_rpt_book_and_tax_act_tc', null, 'PowerTax_Rollup', null, 'PwrTax - 54', null, null, null, 1, 29, 52, 69,
    1, 3, null, null, null, null, null, 0);

--Anand Report Id 55, 400009

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (400009, 'PWRPLANT', TO_DATE('17-JUL-14', 'DD-MON-RR'), 'Book/Tax Activity Report Tax Class',
    'Summarizes the Book and Tax Activity. Tax Class with Vintage Underneath  Run for 1 Tax Year ONLY', 'PowerTax',
    'dw_tax_rpt_book_and_tax_act', null, 'PowerTax_Rollup', null, 'PwrTax - 55', null, 'DETAIL', null, 1, 29, 52, 69,
    1, 3, null, null, null, null, null, 0);

--Anand Report Id 612, 401001

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (401001, 'PWRPLANT', TO_DATE('21-JUL-14', 'DD-MON-RR'), 'Tax Fcst Detail Report', 'Tax Fcst Detail Report',
    'PowerTaxFcst', 'dw_tax_rpt_fcst_detail', null, ' ', null, 'PwrTax - 612', null, null, null, 2, 30, 0, 62, 1,
    3, null, null, null, null, null, 0);

update PP_REPORTS_FILTER set FILTER_UO_NAME = 'uo_tax_selecttabs_rpts_rollup_cons' where PP_REPORT_FILTER_ID = 62;

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (170, 'FCST-Tax Year', 'dw', 'tax_forecast_output.tax_year', null, 'dw_tax_year_by_fcst_version_filter',
    'tax_forecast_output_tax_year', 'tax_forecast_output_tax_year', 'N', 1, 0, null, null, 'PWRPLANT',
    TO_DATE('21-JUL-14', 'DD-MON-RR'), 0, 0, 0);

update PP_DYNAMIC_FILTER set LABEL = 'Company' where FILTER_ID = 142;

insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP)
values
   (55, 170, 146, 'tax_forecast_output.tax_forecast_version_id', null, 'PWRPLANT', TO_DATE('21-JUL-14', 'DD-MON-RR'));

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP) values (62, 170, 'PWRPLANT', TO_DATE('20-JUL-14', 'DD-MON-RR'));

--Anand Report Id 617, 401002

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (71, TO_DATE('22-JUL-14', 'DD-MON-RR'), 'PWRPLANT', 'PowerTax - Forecasting - TaxBook', null,
    'uo_tax_selecttabs_rpts_rollup_cons');

insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (401002, sysdate, user, 'Tax Fcst Report 7', 'Tax Fcst Report 7', 'PowerTaxFcst', 'dw_tax_rpt_fcst_7',
    'tax_book_view                      ', ' ', '', 'PwrTax - 617', '', '', '', 2, 30, 0, 71, 1, 3, '', '', null, '', '',
    0);

Insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (71,105,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
Insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (71,142,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
Insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (71,144,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
Insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (71,145,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
Insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (71,146,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
Insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (71,170,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));

--Anand Report Id 620, 401003

Insert into pwrplant.pp_reports ( report_id, time_stamp, user_id, description, long_description, subsystem, datawindow, special_note, report_type, time_option, report_number, input_window, 
filter_option, status, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, 
user_comment, last_approved_date, pp_report_number, old_report_number, dynamic_dw ) 
values (  401003, sysdate, user, 'Tax Fcst Group Report', 'Tax Fcst Group Report', 'PowerTaxFcst', 'dw_tax_rpt_fcst_group', 'tax_book_view                      ', ' ', '', 
'PwrTax - 620', '', '', '', 2, 30, 0, 71, 1, 3, '', '', null, '', '', 0 );

--Anand Report Id 625, 401004

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (73, TO_DATE('22-JUL-14', 'DD-MON-RR'), 'PWRPLANT', 'PowerTax - Forecasting - Service', null,
    'uo_ppbase_tab_filter_dynamic');

insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (401004, sysdate, user, 'Tax Fcst Service Report', 'Tax Fcst Service Report', 'PowerTaxFcst',
    'dw_tax_rpt_fcst_service', 'tax_book_view                      ', ' ', '', 'PwrTax - 625', '', '', '', 2, 30,
    0, 73, 1, 3, '', '', null, '', '', 0);

Insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (73,101,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
Insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (73,105,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
Insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (73,146,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
Insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (73,170,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
Insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (73,145,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
Insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (73,144,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));

--Anand Report Id 635, 401005

insert into pwrplant.pp_reports ( report_id, time_stamp, user_id, description, long_description, subsystem, datawindow, special_note, report_type, time_option, report_number, input_window, 
filter_option, status, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, user_comment, last_approved_date, 
pp_report_number, old_report_number, dynamic_dw ) 
values (  401005, sysdate, user, 'Tax Fcst Summary Report', 'Tax Fcst Summary Report', 'PowerTaxFcst', 'dw_tax_rpt_fcst_summary', 'tax_book_view                      ', ' ', '', 
'PwrTax - 635', '', '', '', 2, 30, 0, 73, 1, 3, '', '', null, '', '', 0 );

--Anand Report Id 640, 401006

insert into pwrplant.pp_reports ( report_id, time_stamp, user_id, description, long_description, subsystem, datawindow, special_note, report_type, time_option, report_number, input_window, 
filter_option, status, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, user_comment, last_approved_date, 
pp_report_number, old_report_number, dynamic_dw ) 
values (  401006, sysdate, user, 'Tax Fcst Summary Group Report', 'Tax Fcst Summary Group Report', 'PowerTaxFcst', 'dw_tax_rpt_fcst_year', 'tax_book_view                      ', ' ', '', 
'PwrTax - 640', '', '', '', 2, 30, 0, 73, 1, 3, '', '', null, '', '', 0 );



--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1266, 0, 10, 4, 3, 0, 38215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038215_pwrtax_rpts_conv3.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;