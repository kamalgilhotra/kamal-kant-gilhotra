/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052505_lessee_01_update_gl_account_balances_query_is_om_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.3 10/11/2018 C. Yura    change is_om field for las table instead of lct
||============================================================================
*/
update 
pp_any_query_criteria 
set sql =  replace(sql,'lct.is_om','las.is_om'),
sql2 =  replace(sql2,'lct.is_om','las.is_om'),
sql3 =  replace(sql3,'lct.is_om','las.is_om'),
sql4 =  replace(sql4,'lct.is_om','las.is_om')
where description = 'Lease GL Account Balances by Asset (Company Currency)';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (10283, 0, 2017, 4, 0, 3, 52505, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052505_lessee_01_update_gl_account_balances_query_is_om_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;