/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045587_reg_5_prov_add_entity_to_other_tax_dml.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.2.0 05/01/2016 Sarah Byers  	 Updated Tax Provision Pull
||============================================================================
*/

update reg_ta_control_map c
	set entity_id = (
		select min(m.entity_id) from reg_ta_m_type_entity_map m
		 where m.historic_version_id = c.historic_version_id
			and m.forecast_version_id = c.forecast_version_id
			and m.tax_control_id = c.tax_control_id)
 where exists (
		select 1 from reg_ta_m_type_entity_map m
		 where m.historic_version_id = c.historic_version_id
			and m.forecast_version_id = c.forecast_version_id
			and m.tax_control_id = c.tax_control_id);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3178, 0, 2015, 2, 2, 0, 045587, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045587_reg_5_prov_add_entity_to_other_tax_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;