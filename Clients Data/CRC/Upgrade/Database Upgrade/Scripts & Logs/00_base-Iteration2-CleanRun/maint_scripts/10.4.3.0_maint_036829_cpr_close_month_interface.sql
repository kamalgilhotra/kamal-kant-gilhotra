/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_036829_cpr_close_month_interface.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/04/2014 Kyle Peterson
||============================================================================
*/

update PP_PROCESSES
   set EXECUTABLE_FILE = 'ssp_cpr_close_month.exe',
       VERSION = '10.4.3.0',
       ALLOW_CONCURRENT = 0,
       SYSTEM_LOCK_ENABLED = 0,
       SYSTEM_LOCK = 0,
       ASYNC_EMAIL_ON_COMPLETE = 0
 where DESCRIPTION = 'CPR MONTHLY CLOSE';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1372, 0, 10, 4, 3, 0, 36829, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_036829_cpr_close_month_interface.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;