/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051037_lessee_02_backfill_ls_ilr_weighted_avg_rates_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/24/2018 Josh Sandler 	  Populate new table: LS_ILR_WEIGHTED_AVG_RATES
||============================================================================
*/

--Non-remeasurements and OM
INSERT INTO ls_ilr_weighted_avg_rates (ilr_id, revision, set_of_books_id, gross_weighted_avg_rate, net_weighted_avg_rate)
SELECT lio.ilr_id, lio.revision, csob.set_of_books_id,
lio.in_service_exchange_rate,
lio.in_service_exchange_rate
FROM ls_ilr ilr
  JOIN ls_ilr_options lio ON lio.ilr_id = ilr.ilr_id
  JOIN company_set_of_books csob ON csob.company_id = ilr.company_id
  JOIN ls_lease_cap_type lct ON lct.ls_lease_cap_type_id = lio.lease_cap_type_id
  JOIN ls_ilr_approval lia ON lia.ilr_id = lio.ilr_id AND lia.revision = lio.revision
WHERE lio.revision > 0
AND lia.approval_status_id IN (3,6)
AND (remeasurement_date IS NULL OR lct.is_om = 1)
AND (lio.ilr_id, lio.revision, set_of_books_id) NOT IN (SELECT ilr_id, revision, set_of_books_id FROM ls_ilr_weighted_avg_rates)
;

--Off Balance Sheet to On Balance Sheet Remeasurements
INSERT INTO ls_ilr_weighted_avg_rates (ilr_id, revision, set_of_books_id, gross_weighted_avg_rate, net_weighted_avg_rate)
WITH approved_revs AS (
  SELECT ilr_id,revision
  FROM ls_ilr_approval
  WHERE approval_status_id IN (3,6)
), sch AS (
  SELECT sch.ilr_id, sch.revision, sch.set_of_books_id, sch.is_om cur_is_om, Lag(sch.is_om, 1) OVER(PARTITION BY sch.ilr_id, sch.revision, set_of_books_id ORDER BY month) prev_is_om
  FROM ls_ilr_schedule sch
  WHERE sch.revision > 0
  AND (sch.ilr_id, sch.revision) IN (SELECT ilr_id, revision FROM approved_revs)
)
SELECT DISTINCT sch.ilr_id, sch.revision, sch.set_of_books_id, lio.in_service_exchange_rate, lio.in_service_exchange_rate
FROM sch
  JOIN ls_ilr_options lio ON lio.ilr_id = sch.ilr_id AND lio.revision = sch.revision
WHERE cur_is_om = 0
AND prev_is_om = 1
AND lio.remeasurement_date IS NOT NULL
AND (sch.ilr_id, sch.revision, set_of_books_id) NOT IN (SELECT ilr_id, revision, set_of_books_id FROM ls_ilr_weighted_avg_rates)
;


--On Balance Sheet Remeasurements
--Need to loop in order of effective date to build weighted rates on top of previous revision
BEGIN
  FOR L_REMEASURED_ILRS IN (
    SELECT sch.ilr_id, sch.revision, set_of_books_id, MONTH,
        (SELECT lio2.revision FROM ls_ilr ilr2
          JOIN ls_ilr_options lio2 ON ilr2.ilr_id = lio2.ilr_id
          WHERE Nvl(remeasurement_date, est_in_svc_date) =
                      (
                      SELECT Max(Nvl(lio3.remeasurement_date, ilr.est_in_svc_date))
                      FROM ls_ilr ilr
                        JOIN ls_ilr_options lio3 ON ilr.ilr_id = lio3.ilr_id
                      WHERE lio3.ilr_id = sch.ilr_id
                      AND lio3.revision <> sch.revision
                      AND Trunc(Nvl(lio3.remeasurement_date,To_Date('180001','yyyymm')),'month') < sch.MONTH
                      AND Trunc(Nvl(ilr.est_in_svc_date,To_Date('180001','yyyymm')),'month') < sch.MONTH
                      AND ilr.ilr_id = ilr2.ilr_id
                      AND lio3.revision > 0
                      AND (lio3.ilr_id, lio3.revision) IN (SELECT ilr_id,revision FROM ls_ilr_approval WHERE approval_status_id IN (3,6))
                      )
          AND lio2.revision > 0
          AND (lio2.ilr_id, lio2.revision) IN (SELECT ilr_id,revision FROM ls_ilr_approval WHERE approval_status_id IN (3,6))
        ) previous_revision
    FROM ls_ilr_schedule sch
      JOIN ls_ilr_options lio ON lio.ilr_id = sch.ilr_id AND lio.revision = sch.revision
    WHERE (sch.ilr_id, sch.revision) IN (SELECT ilr_id,revision FROM ls_ilr_approval WHERE approval_status_id IN (3,6))
    AND is_om = 0
    AND liability_remeasurement <> 0
    AND beg_capital_cost <> 0
    AND lio.remeasurement_date IS NOT NULL
    AND lio.revision > 0
    AND (sch.ilr_id, sch.revision, set_of_books_id) NOT IN (SELECT ilr_id, revision, set_of_books_id FROM ls_ilr_weighted_avg_rates)
    ORDER BY ilr_id, MONTH ASC
  )
  LOOP
    merge INTO ls_ilr_weighted_avg_rates war USING (
      WITH
      amounts AS (
        SELECT DISTINCT
        sch.ilr_id, sch.revision, sch.set_of_books_id,
        lio.in_service_exchange_rate,
        war.gross_weighted_avg_rate,
        war.net_weighted_avg_rate,
        beg_net_rou_asset,
        liability_remeasurement,
        beg_capital_cost,
        end_capital_cost
        FROM ls_ilr_schedule sch
          JOIN ls_ilr_options lio ON lio.ilr_id = sch.ilr_id AND lio.revision = sch.revision
          JOIN ls_ilr_weighted_avg_rates war ON war.ilr_id = sch.ilr_id AND war.set_of_books_id = sch.set_of_books_id
        WHERE sch.ilr_id = L_REMEASURED_ILRS.ILR_ID
        AND sch.revision = L_REMEASURED_ILRS.REVISION
        AND sch.MONTH =Trunc(lio.remeasurement_date,'month')
        AND war.revision = L_REMEASURED_ILRS.PREVIOUS_REVISION
        AND sch.is_om = 0
      )
      SELECT amounts.ilr_id, amounts.revision, amounts.set_of_books_id,
      (amounts.net_weighted_avg_rate*amounts.beg_net_rou_asset + amounts.in_service_exchange_rate*amounts.liability_remeasurement)/(amounts.beg_net_rou_asset + amounts.liability_remeasurement) new_net_weighted_avg_rate,
      (amounts.gross_weighted_avg_rate*amounts.beg_capital_cost + amounts.in_service_exchange_rate*amounts.liability_remeasurement)/amounts.end_capital_cost new_gross_weighted_avg_rate
      FROM amounts
    ) rate_calc
    ON (war.ilr_id = rate_calc.ilr_id AND war.revision = rate_calc.revision AND war.set_of_books_id = rate_calc.set_of_books_id)
    WHEN matched THEN
    UPDATE SET war.gross_weighted_avg_rate = rate_calc.new_gross_weighted_avg_rate,
                war.net_weighted_avg_rate = rate_calc.new_net_weighted_avg_rate
    WHEN NOT matched THEN
    INSERT (
    war.ilr_id,
    war.revision,
    war.set_of_books_id,
    war.gross_weighted_avg_rate,
    war.net_weighted_avg_rate
    )
    VALUES (
    rate_calc.ilr_id,
    rate_calc.revision,
    rate_calc.set_of_books_id,
    rate_calc.new_gross_weighted_avg_rate,
    rate_calc.new_net_weighted_avg_rate
    );
  END LOOP;
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6363, 0, 2017, 4, 0, 0, 51037, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051037_lessee_02_backfill_ls_ilr_weighted_avg_rates_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;