/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Lessee
|| File Name:   maint_041639_lessee_comp_status_dml.sql
||==========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.2 12/10/2014 B.Beck    	 Set component statuses
||==========================================================================================
*/

update ls_component_status
set description = 'Actual', long_description = 'Actual'
where ls_component_status_id = 3;

update ls_component_status
set description = 'Received', long_description = 'Received'
where ls_component_status_id = 2;

update ls_component_status
set description = 'Committed', long_description = 'Committed'
where ls_component_status_id = 1;

commit;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2109, 0, 10, 4, 3, 2, 041639, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041639_lessee_comp_status_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;