/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042223_pcm_other_workspace.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/10/2015 Mardis           Install the "Other" workspace
||============================================================================
*/

update ppbase_workspace
set workspace_uo_name = 'uo_pcm_maint_wksp_other'
where module = 'pcm'
and workspace_identifier in ('fp_maint_other','wo_maint_other');



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2275, 0, 2015, 1, 0, 0, 042223, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042223_pcm_other_workspace.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;