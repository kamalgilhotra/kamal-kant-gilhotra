/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009454_budget.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   02/13/2012 Chris Mardis   Point Release
||============================================================================
*/

alter table BUDGET_REIMB_RATES modify PERCENT_REIM number(22,8);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (94, 0, 10, 3, 4, 0, 9454, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009454_budget.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
