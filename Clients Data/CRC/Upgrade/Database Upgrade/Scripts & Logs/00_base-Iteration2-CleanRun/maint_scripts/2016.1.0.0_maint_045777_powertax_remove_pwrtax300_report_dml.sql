 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045777_powertax_remove_pwrtax300_report_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || ----------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0  10/12/2016 Jared Watkins  Delete the PwrTax 300 report from all the
 ||                                   report tables in the DB, since it is replaced by plant recon
 ||============================================================================
 */

--leaf nodes
delete from PP_MYPP_TAX_REPORT_FILTER where pp_mypp_report_id in (select pp_mypp_report_id from PP_MYPP_USER_REPORT where report_id in (select report_id from pp_reports where report_number = 'PwrTax - 300'));
delete from PP_MYPP_USER_REPORT_PARM where pp_mypp_report_id in (select pp_mypp_report_id from PP_MYPP_USER_REPORT where report_id in (select report_id from pp_reports where report_number = 'PwrTax - 300'));
delete from PP_REPORT_FILTER_COLUMNS where report_filter_id in (select report_filter_id from PP_REPORT_FILTER where report_id in (select report_id from pp_reports where report_number = 'PwrTax - 300'));

--internal tree nodes
delete from PP_REPORTS_LINK_SUBSTITUTION where report_id in (select report_id from pp_reports where report_number = 'PwrTax - 300');
delete from PP_REPORTS_LINK where report_id in (select report_id from pp_reports where report_number = 'PwrTax - 300');
delete from PPBASE_REPORT_PACKAGE_REPORTS where report_id in (select report_id from pp_reports where report_number = 'PwrTax - 300');
delete from PP_REPORT_FILTER where report_id in (select report_id from pp_reports where report_number = 'PwrTax - 300');
delete from PP_REPORTS_ARGS where report_id in (select report_id from pp_reports where report_number = 'PwrTax - 300');
delete from TAX_ACCRUAL_REPORTS_CUSTOMIZE where report_id in (select report_id from pp_reports where report_number = 'PwrTax - 300');
delete from PP_MYPP_USER_REPORT where report_id in (select report_id from pp_reports where report_number = 'PwrTax - 300');
delete from REG_ANALYSIS_REPORTS where report_id in (select report_id from pp_reports where report_number = 'PwrTax - 300');

 --root node
delete from pp_reports where report_number = 'PwrTax - 300';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3313, 0, 2016, 1, 0, 0, 045777, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045777_powertax_remove_pwrtax300_report_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;