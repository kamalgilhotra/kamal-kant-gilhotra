/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009840_cwip_fast106.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version   Date       Revised By     Reason for Change
|| --------- ---------- -------------- --------------------------------------
|| 10.4.0.0  01/16/2013 Sunjin Cone    Point Release
||============================================================================
*/

create global temporary table WO_AUTO106_TEMP_WOS
(
 WORK_ORDER_ID number(22,0),
 NOTES         varchar2(2000)
) on commit preserve rows;

create global temporary table WO_AUTO106_WOS
(
 COMPANY_ID              number(22,0) not null,
 ACCOUNTING_MONTH        date not null,
 WORK_ORDER_ID           number(22,0) not null,
 CLOSING_OPTION_ID       number(22,0) not null,
 WO_STATUS_ID            number(22,0) not null,
 IN_SERVICE_DATE         date,
 COMPLETION_DATE         date,
 LATE_CHG_WAIT_PERIOD    number(22,0) ,
 NON_UNITIZED_GL_ACCOUNT number(22,0) not null,
 WORK_ORDER_NUMBER       varchar2(35) not null,
 BUS_SEGMENT_ID          number(22,0),
 ASSET_LOCATION_ID       number(22,0),
 ERROR_MSG               varchar2(2000),
 TIME_STAMP              date,
 USER_ID                 varchar2(18)
) on commit preserve rows;

create table WO_AUTO106_CHARGES
(
 COMPANY_ID          number(22,0) not null,
 ACCOUNTING_MONTH    date not null,
 RUN                 number(22,0),
 WORK_ORDER_ID       number(22,0) not null,
 CHARGE_ID           number(22,0) not null,
 CHARGE_TYPE_ID      number(22,0),
 BOOK_SUMMARY_ID     number(22,0),
 AMOUNT              number(22,2),
 BUS_SEGMENT_ID      number(22,0),
 UTILITY_ACCOUNT_ID  number(22,0),
 SUB_ACCOUNT_ID      number(22,0),
 ASSET_LOCATION_ID   number(22,0),
 EXPENDITURE_TYPE_ID number(22,0),
 WIP_COMPUTATION_ID  number(22,0),
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);

alter table WO_AUTO106_CHARGES
   add constraint PK_WO_AUTO106_CHARGES
       primary key (WORK_ORDER_ID, CHARGE_ID)
       using index tablespace PWRPLANT_IDX;

alter table WO_AUTO106_CHARGES
   add constraint R_WO_AUTO106_CHARGES2
       foreign key (CHARGE_ID)
       references CWIP_CHARGE;

create global temporary table WO_AUTO106_CHARGES_SUMM
(
 COMPANY_ID       number(22,0) not null,
 ACCOUNTING_MONTH date not null,
 WORK_ORDER_ID    number(22,0) not null,
 BOOK_SUMMARY_ID  number(22,0),
 TOTAL_AMOUNT     number(22,2) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18)
) on commit preserve rows;

create global temporary table WO_AUTO106_PENDING_TRANS
(
 COMPANY_ID          number(22,0) not null,
 ACCOUNTING_MONTH    date not null,
 WORK_ORDER_ID       number(22,0) not null,
 WORK_ORDER_NUMBER   varchar2(35) not null,
 MAX_UNIT_ID         number(22,0) not null,
 REVISION            number(22,0),
 BUS_SEGMENT_ID      number(22,0),
 UTILITY_ACCOUNT_ID  number(22,0),
 SUB_ACCOUNT_ID      number(22,0),
 PROPERTY_GROUP_ID   number(22,0),
 ASSET_LOCATION_ID   number(22,0),
 QUANTITY            number(22,2),
 POSTING_QUANTITY    number(22,2),
 AMOUNT              number(22,2),
 ALLOC_STATISTIC_AMT number(22,2) not null,
 WO_DESCRIPTION      varchar2(35),
 WO_LONG_DESCRIPTION varchar2(254),
 SOURCE              varchar2(10),
 ERROR_MSG           varchar2(2000),
 UA_FUNC_CLASS_ID    number(22,0),
 BASIS_1             number(22,2) not null,
 BASIS_2             number(22,2) not null,
 BASIS_3             number(22,2) not null,
 BASIS_4             number(22,2) not null,
 BASIS_5             number(22,2) not null,
 BASIS_6             number(22,2) not null,
 BASIS_7             number(22,2) not null,
 BASIS_8             number(22,2) not null,
 BASIS_9             number(22,2) not null,
 BASIS_10            number(22,2) not null,
 BASIS_11            number(22,2) not null,
 BASIS_12            number(22,2) not null,
 BASIS_13            number(22,2) not null,
 BASIS_14            number(22,2) not null,
 BASIS_15            number(22,2) not null,
 BASIS_16            number(22,2) not null,
 BASIS_17            number(22,2) not null,
 BASIS_18            number(22,2) not null,
 BASIS_19            number(22,2) not null,
 BASIS_20            number(22,2) not null,
 BASIS_21            number(22,2) not null,
 BASIS_22            number(22,2) not null,
 BASIS_23            number(22,2) not null,
 BASIS_24            number(22,2) not null,
 BASIS_25            number(22,2) not null,
 BASIS_26            number(22,2) not null,
 BASIS_27            number(22,2) not null,
 BASIS_28            number(22,2) not null,
 BASIS_29            number(22,2) not null,
 BASIS_30            number(22,2) not null,
 BASIS_31            number(22,2) not null,
 BASIS_32            number(22,2) not null,
 BASIS_33            number(22,2) not null,
 BASIS_34            number(22,2) not null,
 BASIS_35            number(22,2) not null,
 BASIS_36            number(22,2) not null,
 BASIS_37            number(22,2) not null,
 BASIS_38            number(22,2) not null,
 BASIS_39            number(22,2) not null,
 BASIS_40            number(22,2) not null,
 BASIS_41            number(22,2) not null,
 BASIS_42            number(22,2) not null,
 BASIS_43            number(22,2) not null,
 BASIS_44            number(22,2) not null,
 BASIS_45            number(22,2) not null,
 BASIS_46            number(22,2) not null,
 BASIS_47            number(22,2) not null,
 BASIS_48            number(22,2) not null,
 BASIS_49            number(22,2) not null,
 BASIS_50            number(22,2) not null,
 BASIS_51            number(22,2) not null,
 BASIS_52            number(22,2) not null,
 BASIS_53            number(22,2) not null,
 BASIS_54            number(22,2) not null,
 BASIS_55            number(22,2) not null,
 BASIS_56            number(22,2) not null,
 BASIS_57            number(22,2) not null,
 BASIS_58            number(22,2) not null,
 BASIS_59            number(22,2) not null,
 BASIS_60            number(22,2) not null,
 BASIS_61            number(22,2) not null,
 BASIS_62            number(22,2) not null,
 BASIS_63            number(22,2) not null,
 BASIS_64            number(22,2) not null,
 BASIS_65            number(22,2) not null,
 BASIS_66            number(22,2) not null,
 BASIS_67            number(22,2) not null,
 BASIS_68            number(22,2) not null,
 BASIS_69            number(22,2) not null,
 BASIS_70            number(22,2) not null,
 TOTAL_AMOUNT        number(22,2),
 PEND_TRANS_ID       number(22,0),
 TIME_STAMP          date,
 USER_ID             varchar2(18)
) on commit preserve rows;

create global temporary table WO_AUTO106_CLASS_CODES
(
 WORK_ORDER_ID number(22,0) not null,
 MAX_UNIT_ID   number(22,0) not null,
 CLASS_CODE_ID number(22,0) not null,
 CC_VALUE      varchar2(254) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18)
) on commit preserve rows;

create table WO_AUTO106_PENDING_TRANS_ARC
(
 COMPANY_ID          number(22,0) not null,
 ACCOUNTING_MONTH    date not null,
 WORK_ORDER_ID       number(22,0) not null,
 WORK_ORDER_NUMBER   varchar2(35) not null,
 MAX_UNIT_ID         number(22,0) not null,
 REVISION            number(22,0),
 BUS_SEGMENT_ID      number(22,0),
 UTILITY_ACCOUNT_ID  number(22,0),
 SUB_ACCOUNT_ID      number(22,0),
 PROPERTY_GROUP_ID   number(22,0),
 ASSET_LOCATION_ID   number(22,0),
 QUANTITY            number(22,2),
 POSTING_QUANTITY    number(22,2),
 AMOUNT              number(22,2),
 ALLOC_STATISTIC_AMT number(22,2) not null,
 WO_DESCRIPTION      varchar2(35),
 WO_LONG_DESCRIPTION varchar2(254),
 SOURCE              varchar2(10),
 ERROR_MSG           varchar2(2000),
 UA_FUNC_CLASS_ID    number(22,0),
 BASIS_1             number(22,2) not null,
 BASIS_2             number(22,2) not null,
 BASIS_3             number(22,2) not null,
 BASIS_4             number(22,2) not null,
 BASIS_5             number(22,2) not null,
 BASIS_6             number(22,2) not null,
 BASIS_7             number(22,2) not null,
 BASIS_8             number(22,2) not null,
 BASIS_9             number(22,2) not null,
 BASIS_10            number(22,2) not null,
 BASIS_11            number(22,2) not null,
 BASIS_12            number(22,2) not null,
 BASIS_13            number(22,2) not null,
 BASIS_14            number(22,2) not null,
 BASIS_15            number(22,2) not null,
 BASIS_16            number(22,2) not null,
 BASIS_17            number(22,2) not null,
 BASIS_18            number(22,2) not null,
 BASIS_19            number(22,2) not null,
 BASIS_20            number(22,2) not null,
 BASIS_21            number(22,2) not null,
 BASIS_22            number(22,2) not null,
 BASIS_23            number(22,2) not null,
 BASIS_24            number(22,2) not null,
 BASIS_25            number(22,2) not null,
 BASIS_26            number(22,2) not null,
 BASIS_27            number(22,2) not null,
 BASIS_28            number(22,2) not null,
 BASIS_29            number(22,2) not null,
 BASIS_30            number(22,2) not null,
 BASIS_31            number(22,2) not null,
 BASIS_32            number(22,2) not null,
 BASIS_33            number(22,2) not null,
 BASIS_34            number(22,2) not null,
 BASIS_35            number(22,2) not null,
 BASIS_36            number(22,2) not null,
 BASIS_37            number(22,2) not null,
 BASIS_38            number(22,2) not null,
 BASIS_39            number(22,2) not null,
 BASIS_40            number(22,2) not null,
 BASIS_41            number(22,2) not null,
 BASIS_42            number(22,2) not null,
 BASIS_43            number(22,2) not null,
 BASIS_44            number(22,2) not null,
 BASIS_45            number(22,2) not null,
 BASIS_46            number(22,2) not null,
 BASIS_47            number(22,2) not null,
 BASIS_48            number(22,2) not null,
 BASIS_49            number(22,2) not null,
 BASIS_50            number(22,2) not null,
 BASIS_51            number(22,2) not null,
 BASIS_52            number(22,2) not null,
 BASIS_53            number(22,2) not null,
 BASIS_54            number(22,2) not null,
 BASIS_55            number(22,2) not null,
 BASIS_56            number(22,2) not null,
 BASIS_57            number(22,2) not null,
 BASIS_58            number(22,2) not null,
 BASIS_59            number(22,2) not null,
 BASIS_60            number(22,2) not null,
 BASIS_61            number(22,2) not null,
 BASIS_62            number(22,2) not null,
 BASIS_63            number(22,2) not null,
 BASIS_64            number(22,2) not null,
 BASIS_65            number(22,2) not null,
 BASIS_66            number(22,2) not null,
 BASIS_67            number(22,2) not null,
 BASIS_68            number(22,2) not null,
 BASIS_69            number(22,2) not null,
 BASIS_70            number(22,2) not null,
 TOTAL_AMOUNT        number(22,2),
 PEND_TRANS_ID       number(22,0),
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 RUN                 number(22,2)
);

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'AUTO106 - FAST NO LOOP',
          'No',
          'dw_yes_no;1',
          '"Yes" will cause auto non-unitization processing to avoid looping by individual work orders and is faster. "No" will cause auto non-unitization processing to loop by individual work orders.  "No" is the default.',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (275, 0, 10, 4, 0, 0, 9840, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_009840_cwip_fast106.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
