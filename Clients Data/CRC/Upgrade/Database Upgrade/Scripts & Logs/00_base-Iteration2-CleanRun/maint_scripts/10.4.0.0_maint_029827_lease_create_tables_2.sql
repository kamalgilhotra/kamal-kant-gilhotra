SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029827_lease_create_tables_2.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/25/2013 Lee Quinn      Point Release
||============================================================================
*/

declare
   TABLE_DOES_NOT_EXIST exception;
   pragma exception_init(TABLE_DOES_NOT_EXIST, -00942);

   LOOPING_CHAIN_OF_SYNONYMS exception;
   pragma exception_init(LOOPING_CHAIN_OF_SYNONYMS, -01775);

   type CV_TYP is ref cursor;
   CV CV_TYP;

   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   L_COUNT             number := 1;
   LB_LEASE_BEING_USED boolean := true;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   begin
      open CV for 'select count(*) from LS_ASSET where ROWNUM < 10';
      fetch CV
         into L_COUNT;
      close CV;

   if L_COUNT = 0 then
      LB_LEASE_BEING_USED := false;
   end if;

   exception
      when TABLE_DOES_NOT_EXIST then
         LB_LEASE_BEING_USED := false;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'LS_ASSET table Doesn''t exist so Lease Module can be refreshed.');
      when LOOPING_CHAIN_OF_SYNONYMS then
         LB_LEASE_BEING_USED := false;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Looping Chain of Synonyms - LS_ASSET table Doesn''t exist so the Lease Module can be refreshed.');
   end;

   if LB_LEASE_BEING_USED then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Lease is being used so no Tables will be created.');
   else

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_CAPITAL_VS_OPERATING.');
      execute immediate 'create table LS_ASSET_CAPITAL_VS_OPERATING
                         (
                          LEASE_ID                     number(22,22),
                          TIME_STAMP                   date,
                          USER_ID                      varchar2(18),
                          IS_OPERATING                 number(22,1),
                          IS_BARGAIN_PURCHASE_OPTION   number(22,1),
                          IS_BEGIN_TERM_WITHIN_25      number(22,1),
                          IS_NPV_EQUAL_PERCENT_OF_FMV  number(22,22),
                          IS_LEASE_TERM_75_OF_EST_LIFE number(22,1),
                          INTEREST_RATE                number(22,22),
                          FAIR_MARKET_VALUE            number(22,22),
                          LESSEE_IBR                   number(22,22),
                          IS_OWNERSHIP_TRANSFERABLE    number(22,1),
                          IRR                          number(22,22),
                          NPV                          number(22,22),
                          NPV_PERCENT_OF_FMV           number(22,22)
                         )';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_LOC_LOCAL_TAX_DISTRI.');
      execute immediate 'create table LS_ASSET_LOC_LOCAL_TAX_DISTRI
                         (
                          ASSET_LOCATION_ID     number(22,0),
                          LOCAL_TAX_DISTRICT_ID number(22,0),
                          TIME_STAMP            date,
                          USER_ID               varchar2(18)
                         )';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_TRANSFER_HISTORY.');
      execute immediate 'create table LS_ASSET_TRANSFER_HISTORY
                         (
                          FROM_LS_ASSET_ID     number(22,0),
                          TO_LS_ASSET_ID       number(22,0),
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          CALC_MONTH           number(22,0),
                          RETIREMENT_STATUS_ID number(22,0)
                         )';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_DIST_DEF_VALIDATE.');
      execute immediate 'create table LS_DIST_DEF_VALIDATE
                         (
                          DK_ID              number(22,0),
                          EFFDT              date,
                          COMPANY_ID         number(22,0),
                          DIVISION_ID        number(22,0),
                          DEPARTMENT_ID      number(22,0),
                          DIST_GL_ACCOUNT_ID number(22,0),
                          WORK_ORDER_NUMBER  varchar2(35),
                          COST_ELEMENT_ID    number(22,0),
                          ASSET_LOCATION_ID  number(22,0),
                          STATE_ID           char(18),
                          ELEMENT_1          varchar2(35),
                          ELEMENT_2          varchar2(35),
                          ELEMENT_3          varchar2(35),
                          ELEMENT_4          varchar2(35),
                          ELEMENT_5          varchar2(35),
                          ELEMENT_6          varchar2(35),
                          ELEMENT_7          varchar2(35),
                          ELEMENT_8          varchar2(35),
                          ELEMENT_9          varchar2(35),
                          ELEMENT_10         varchar2(35),
                          GL_ERROR_MESSAGE   varchar2(512),
                          PROCESSED_YN_SW    number(22,0),
                          TIME_STAMP         date,
                          USER_ID            varchar2(18)
                         )';

      --Index

      execute immediate 'create index IX_DD_VALIDATE
                            on LS_DIST_DEF_VALIDATE (DK_ID)
                            tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_FCST_ALLOCATION_CREDITS.');
      execute immediate 'create table LS_FCST_ALLOCATION_CREDITS
                         (
                          DIST_LINE_ID            number(22,0),
                          TIME_STAMP              date,
                          USER_ID                 varchar2(18),
                          DIST_DEF_TYPE_ID        number(22,0),
                          DISTRIBUTION_TYPE_ID    number(22,0),
                          LS_ASSET_ID             number(22,0),
                          COMPANY_ID              number(22,0),
                          DIVISION_ID             number(22,0),
                          DEPARTMENT_ID           number(22,0),
                          DIST_GL_ACCOUNT_ID      number(22,0),
                          WORK_ORDER_NUMBER       varchar2(35),
                          COST_ELEMENT_ID         number(22,0),
                          ASSET_LOCATION_ID       number(22,0),
                          STATE_ID                char(18),
                          ELEMENT_1               varchar2(35),
                          ELEMENT_2               varchar2(35),
                          ELEMENT_3               varchar2(35),
                          ELEMENT_4               varchar2(35),
                          ELEMENT_5               varchar2(35),
                          ELEMENT_6               varchar2(35),
                          ELEMENT_7               varchar2(35),
                          ELEMENT_8               varchar2(35),
                          ELEMENT_9               varchar2(35),
                          ELEMENT_10              varchar2(35),
                          GL_JE_CODE              varchar2(35),
                          GL_POSTING_DATE         date,
                          GL_POSTING_MONTHNUM     number(22,0),
                          GL_POSTING_TIMESTAMP    date,
                          GL_POSTING_USERID       varchar2(18),
                          LEASE_CALC_RUN_ID       number(22,0),
                          MASS_CHANGE_ID          number(22,0),
                          DESCRIPTION             varchar2(35),
                          LONG_DESCRIPTION        varchar2(254),
                          AMOUNT                  number(22,2),
                          DEPR_EXPENSE            number(22,2),
                          INTEREST_ACCRUAL        number(22,2),
                          PRINCIPAL_ACCRUAL       number(22,2),
                          EXTENDED_RENTAL_ACCRUAL number(22,2),
                          SALES_TAX_ACCRUAL       number(22,2),
                          USE_TAX_ACCRUAL         number(22,2),
                          SUM_AMOUNT              number(22,2),
                          DISTRIB_PCT             number(22,8),
                          JE_METHOD_ID            number(22,0),
                          LT_3_ACCRUAL            number(22,2),
                          LT_4_ACCRUAL            number(22,2),
                          LT_5_ACCRUAL            number(22,2),
                          LT_6_ACCRUAL            number(22,2),
                          LT_7_ACCRUAL            number(22,2),
                          LT_8_ACCRUAL            number(22,2),
                          LT_9_ACCRUAL            number(22,2),
                          LT_10_ACCRUAL           number(22,2),
                          LEASE_ID                number(22,0),
                          DC_INDICATOR            number(22,0),
                          LEASE_NUMBER            varchar2(35),
                          FCST_CASE_ID            number(22,0)
                         )';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_JE_ALLOCATION_CREDITS.');
      execute immediate 'create table LS_JE_ALLOCATION_CREDITS
                         (
                          DIST_LINE_ID            number(22,0),
                          TIME_STAMP              date,
                          USER_ID                 varchar2(18),
                          DIST_DEF_TYPE_ID        number(22,0),
                          DISTRIBUTION_TYPE_ID    number(22,0),
                          LS_ASSET_ID             number(22,0),
                          COMPANY_ID              number(22,0),
                          DIVISION_ID             number(22,0),
                          DEPARTMENT_ID           number(22,0),
                          DIST_GL_ACCOUNT_ID      number(22,0),
                          WORK_ORDER_NUMBER       varchar2(35),
                          COST_ELEMENT_ID         number(22,0),
                          ASSET_LOCATION_ID       number(22,0),
                          STATE_ID                char(18),
                          ELEMENT_1               varchar2(35),
                          ELEMENT_2               varchar2(35),
                          ELEMENT_3               varchar2(35),
                          ELEMENT_4               varchar2(35),
                          ELEMENT_5               varchar2(35),
                          ELEMENT_6               varchar2(35),
                          ELEMENT_7               varchar2(35),
                          ELEMENT_8               varchar2(35),
                          ELEMENT_9               varchar2(35),
                          ELEMENT_10              varchar2(35),
                          GL_JE_CODE              varchar2(35),
                          GL_POSTING_DATE         date,
                          GL_POSTING_MONTHNUM     number(22,0),
                          GL_POSTING_TIMESTAMP    date,
                          GL_POSTING_USERID       varchar2(18),
                          LEASE_CALC_RUN_ID       number(22,0),
                          MASS_CHANGE_ID          number(22,0),
                          DESCRIPTION             varchar2(35),
                          LONG_DESCRIPTION        varchar2(254),
                          AMOUNT                  number(22,2),
                          DEPR_EXPENSE            number(22,2),
                          INTEREST_ACCRUAL        number(22,2),
                          PRINCIPAL_ACCRUAL       number(22,2),
                          EXTENDED_RENTAL_ACCRUAL number(22,2),
                          SALES_TAX_ACCRUAL       number(22,2),
                          USE_TAX_ACCRUAL         number(22,2),
                          SUM_AMOUNT              number(22,2),
                          DISTRIB_PCT             number(22,8),
                          JE_METHOD_ID            number(22,0),
                          LT_3_ACCRUAL            number(22,2),
                          LT_4_ACCRUAL            number(22,2),
                          LT_5_ACCRUAL            number(22,2),
                          LT_6_ACCRUAL            number(22,2),
                          LT_7_ACCRUAL            number(22,2),
                          LT_8_ACCRUAL            number(22,2),
                          LT_9_ACCRUAL            number(22,2),
                          LT_10_ACCRUAL           number(22,2),
                          LEASE_ID                number(22,0),
                          DC_INDICATOR            number(22,0),
                          LEASE_NUMBER            varchar2(35)
                         )';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_JE_EXCHANGE_STLT_RECAT.');
      execute immediate 'create table LS_JE_EXCHANGE_STLT_RECAT
                         (
                          DIST_LINE_ID             number(22,0),
                          TIME_STAMP               date,
                          USER_ID                  varchar2(18),
                          DIST_DEF_TYPE_ID         number(22,0),
                          DISTRIBUTION_TYPE_ID     number(22,0),
                          LS_ASSET_ID              number(22,0),
                          GL_COMPANY_NO            varchar2(35),
                          DIVISION_ID              number(22,0),
                          EXTERNAL_DEPARTMENT_CODE varchar2(35),
                          EXTERNAL_ACCOUNT_CODE    varchar2(35),
                          WORK_ORDER_NUMBER        varchar2(35),
                          EXTERNAL_COST_ELEMENT    varchar2(35),
                          EXT_ASSET_LOCATION       varchar2(35),
                          STATE_CODE               char(18),
                          ELEMENT_1                varchar2(35),
                          ELEMENT_2                varchar2(35),
                          ELEMENT_3                varchar2(35),
                          ELEMENT_4                varchar2(35),
                          ELEMENT_5                varchar2(35),
                          ELEMENT_6                varchar2(35),
                          ELEMENT_7                varchar2(35),
                          ELEMENT_8                varchar2(35),
                          ELEMENT_9                varchar2(35),
                          ELEMENT_10               varchar2(35),
                          GL_JE_CODE               varchar2(35),
                          GL_POSTING_DATE          date not null,
                          GL_POSTING_MONTHNUM      number(22,0) not null,
                          GL_POSTING_TIMESTAMP     date,
                          GL_POSTING_USERID        varchar2(18),
                          LEASE_CALC_RUN_ID        number(22,0),
                          MASS_CHANGE_ID           number(22,0),
                          DESCRIPTION              varchar2(35),
                          LONG_DESCRIPTION         varchar2(254),
                          AMOUNT                   number(22,2) not null,
                          POSTING_STATUS           number(22,0) not null,
                          LEASE_ID                 number(22,0),
                          DC_INDICATOR             number(22,0),
                          JE_METHOD_ID             number(22,0),
                          LEASE_NUMBER             varchar2(35),
                          COMPANY_ID               number(22,0),
                          DEPARTMENT_ID            number(22,0),
                          GL_ACCOUNT_ID            number(22,0),
                          COST_ELEMENT_ID          number(22,0),
                          ASSET_LOCATION_ID        number(22,0)
                         )';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_JE_STAGING.');
      execute immediate 'create table LS_JE_STAGING
                         (
                          COMPANY_ID          number(22,0),
                          GL_POSTING_MONTHNUM number(22,0),
                          TIME_STAMP          date,
                          USER_ID             varchar2(18)
                         )';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_TEMP_CALC_ALLMONTHS.');
      execute immediate 'create table LS_TEMP_CALC_ALLMONTHS
                         (
                          ILR_ID number(22,0) not null
                         )';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ACTIVITY_TYPE.');
      execute immediate 'create table LS_ACTIVITY_TYPE
                         (
                          LS_ACTIVITY_TYPE_ID number(22,0) not null,
                          TIME_STAMP          date,
                          USER_ID             varchar2(18),
                          DESCRIPTION         varchar2(35) not null,
                          LONG_DESCRIPTION    varchar2(254) not null,
                          AM_CALC_FLAG        varchar2(4),
                          FERC_ACTIVITY_CODE  number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_ACTIVITY_TYPE
                            add constraint PK_LS_ACTIVITY_TYPE
                                primary key (LS_ACTIVITY_TYPE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_ASSET.');
      execute immediate 'create table LS_API_ASSET
                         (
                          LEASED_ASSET_NUMBER         varchar2(35) not null,
                          ILR_NUMBER                  varchar2(35) not null,
                          LS_ASSET_STATUS_ID          number(22,0) not null,
                          DESCRIPTION                 varchar2(35) not null,
                          LONG_DESCRIPTION            varchar2(254) not null,
                          TAG_NUMBER                  varchar2(35),
                          EXTERNAL_SYS                varchar2(35),
                          EXTERNAL_SYS_ID             varchar2(35),
                          CONVERSION_ID               varchar2(35),
                          CURRENT_LEASE_DATE          date not null,
                          PROPERTY_TAX_MO_YR          number(22,0),
                          PROPERTY_TAX_COST           number(22,0),
                          RETIREMENT_DATE             date,
                          TERMINATION_PENALTY_AMOUNT  number(22,2),
                          SALE_PROCEED_AMOUNT         number(22,2),
                          EXPECTED_LIFE               number(22,2),
                          RESIDUAL_AMOUNT             number(22,2),
                          EXT_RETIREMENT_UNIT         varchar2(35) not null,
                          EXT_ASSET_LOCATION          varchar2(35) not null,
                          EXT_UTILITY_ACCOUNT         varchar2(35) not null,
                          EXT_GL_ACCOUNT              varchar2(35) not null,
                          RETIREMENT_UNIT_ID          number(22,0),
                          BUS_SEGMENT_ID              number(22,0) not null,
                          COMPANY_ID                  number(22,0) not null,
                          UTILITY_ACCOUNT_ID          number(22,0),
                          GL_ACCOUNT_ID               number(22,0),
                          ASSET_LOCATION_ID           number(22,0),
                          SUB_ACCOUNT_ID              number(22,0),
                          WORK_ORDER_NUMBER           varchar2(35) not null,
                          IN_SERVICE_YEAR             date not null,
                          ENG_IN_SERVICE_YEAR         date not null,
                          FLEX_1                      number(22,8),
                          FLEX_2                      number(22,8),
                          FLEX_3                      number(22,8),
                          FLEX_4                      number(22,8),
                          FLEX_5                      number(22,8),
                          FLEX_6                      varchar2(35),
                          FLEX_7                      varchar2(35),
                          FLEX_8                      varchar2(35),
                          FLEX_9                      varchar2(35),
                          FLEX_10                     varchar2(35),
                          FLEX_11                     varchar2(254),
                          FLEX_12                     varchar2(254),
                          FLEX_13                     varchar2(254),
                          FLEX_14                     varchar2(254),
                          FLEX_15                     varchar2(254),
                          FLEX_16                     varchar2(2000),
                          FLEX_17                     varchar2(2000),
                          FLEX_18                     varchar2(2000),
                          FLEX_19                     varchar2(2000),
                          FLEX_20                     varchar2(2000),
                          LOCAL_TAX_1_YN              number(22,0) not null,
                          LOCAL_TAX_2_YN              number(22,0) not null,
                          LOCAL_TAX_3_YN              number(22,0) not null,
                          LOCAL_TAX_4_YN              number(22,0) not null,
                          LOCAL_TAX_5_YN              number(22,0) not null,
                          LOCAL_TAX_6_YN              number(22,0) not null,
                          LOCAL_TAX_7_YN              number(22,0) not null,
                          LOCAL_TAX_8_YN              number(22,0) not null,
                          LOCAL_TAX_9_YN              number(22,0) not null,
                          LOCAL_TAX_10_YN             number(22,0) not null,
                          JE_METHOD_ID                number(22,0) not null,
                          API_STATUS                  number(22,0) not null,
                          API_INSERT_TIME_STAMP       date not null,
                          API_ERROR_MESSAGE           varchar2(2000),
                          CURRENT_LEASE_COST          number(22,2),
                          INTERIM_INTEREST_BEGIN_DATE date,
                          GL_POSTING_BEGIN_DATE       date,
                          LS_ASSET_ID                 number(22,0),
                          API_PROCESS_TIME_STAMP      date,
                          API_BATCH_ID                number(22,0) not null,
                          PROPERTY_GROUP_ID           number(22,0),
                          TIME_STAMP                  date,
                          USER_ID                     varchar2(18),
                          ECONOMIC_LIFE               number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_ASSET
                            add constraint PK_LS_API_ASSET
                                primary key (LEASED_ASSET_NUMBER)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_ASSET_TMP.');
      execute immediate 'create table LS_API_ASSET_TMP
                         (
                          LEASED_ASSET_NUMBER         varchar2(35) not null,
                          ILR_NUMBER                  varchar2(35) not null,
                          LS_ASSET_STATUS_ID          number(22,0) not null,
                          DESCRIPTION                 varchar2(35) not null,
                          LONG_DESCRIPTION            varchar2(254) not null,
                          TAG_NUMBER                  varchar2(35),
                          EXTERNAL_SYS                varchar2(35),
                          EXTERNAL_SYS_ID             varchar2(35),
                          CONVERSION_ID               varchar2(35),
                          CURRENT_LEASE_DATE          date not null,
                          PROPERTY_TAX_MO_YR          number(22,0),
                          PROPERTY_TAX_COST           number(22,0),
                          RETIREMENT_DATE             date,
                          TERMINATION_PENALTY_AMOUNT  number(22,2),
                          SALE_PROCEED_AMOUNT         number(22,2),
                          EXPECTED_LIFE               number(22,2),
                          RESIDUAL_AMOUNT             number(22,2),
                          EXT_RETIREMENT_UNIT         varchar2(35) not null,
                          EXT_ASSET_LOCATION          varchar2(35) not null,
                          EXT_UTILITY_ACCOUNT         varchar2(35) not null,
                          EXT_GL_ACCOUNT              varchar2(35) not null,
                          RETIREMENT_UNIT_ID          number(22,0),
                          BUS_SEGMENT_ID              number(22,0) not null,
                          COMPANY_ID                  number(22,0) not null,
                          UTILITY_ACCOUNT_ID          number(22,0),
                          GL_ACCOUNT_ID               number(22,0),
                          ASSET_LOCATION_ID           number(22,0),
                          SUB_ACCOUNT_ID              number(22,0),
                          WORK_ORDER_NUMBER           varchar2(35) not null,
                          IN_SERVICE_YEAR             date not null,
                          ENG_IN_SERVICE_YEAR         date not null,
                          FLEX_1                      number(22,8),
                          FLEX_2                      number(22,8),
                          FLEX_3                      number(22,8),
                          FLEX_4                      number(22,8),
                          FLEX_5                      number(22,8),
                          FLEX_6                      varchar2(35),
                          FLEX_7                      varchar2(35),
                          FLEX_8                      varchar2(35),
                          FLEX_9                      varchar2(35),
                          FLEX_10                     varchar2(35),
                          FLEX_11                     varchar2(254),
                          FLEX_12                     varchar2(254),
                          FLEX_13                     varchar2(254),
                          FLEX_14                     varchar2(254),
                          FLEX_15                     varchar2(254),
                          FLEX_16                     varchar2(2000),
                          FLEX_17                     varchar2(2000),
                          FLEX_18                     varchar2(2000),
                          FLEX_19                     varchar2(2000),
                          FLEX_20                     varchar2(2000),
                          LOCAL_TAX_1_YN              number(22,0) not null,
                          LOCAL_TAX_2_YN              number(22,0) not null,
                          LOCAL_TAX_3_YN              number(22,0) not null,
                          LOCAL_TAX_4_YN              number(22,0) not null,
                          LOCAL_TAX_5_YN              number(22,0) not null,
                          LOCAL_TAX_6_YN              number(22,0) not null,
                          LOCAL_TAX_7_YN              number(22,0) not null,
                          LOCAL_TAX_8_YN              number(22,0) not null,
                          LOCAL_TAX_9_YN              number(22,0) not null,
                          LOCAL_TAX_10_YN             number(22,0) not null,
                          JE_METHOD_ID                number(22,0) not null,
                          API_STATUS                  number(22,0) not null,
                          API_INSERT_TIME_STAMP       date  not null,
                          API_ERROR_MESSAGE           varchar2(2000),
                          CURRENT_LEASE_COST          number(22,2),
                          INTERIM_INTEREST_BEGIN_DATE date,
                          GL_POSTING_BEGIN_DATE       date,
                          LS_ASSET_ID                 number(22,0),
                          API_PROCESS_TIME_STAMP      date,
                          API_BATCH_ID                number(22,0)  not null,
                          PROPERTY_GROUP_ID           number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_ASSET_TMP
                            add constraint PK_LS_API_ASSET_TMP
                                primary key (LEASED_ASSET_NUMBER)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_BATCH.');
      execute immediate 'create table LS_API_BATCH
                         (
                          API_BATCH_ID            number(22,0) not null,
                          TIME_STAMP              date not null,
                          USER_ID                 varchar2(18) not null,
                          DESCRIPTION             varchar2(35) not null,
                          LONG_DESCRIPTION        varchar2(254) not null,
                          API_BATCH_STATUS_ID     number(22,0) not null,
                          PROCESS_BEGIN_TIMESTAMP date,
                          PROCESS_END_TIMESTAMP   date
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_BATCH
                            add constraint PK_LS_API_BATCH
                                primary key (API_BATCH_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_BATCH_STATUS.');
      execute immediate 'create table LS_API_BATCH_STATUS
                         (
                          API_BATCH_STATUS_ID number(22,0) not null,
                          TIME_STAMP          date,
                          USER_ID             varchar2(18),
                          DESCRIPTION         varchar2(35),
                          LONG_DESCRIPTION    varchar2(254)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_BATCH_STATUS
                            add constraint PK_LS_API_BATCH_STATUS
                                primary key (API_BATCH_STATUS_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_COMPONENT.');
      execute immediate 'create table LS_API_COMPONENT
                         (
                          LEASED_ASSET_NUMBER         varchar2(35) not null,
                          COMPONENT_ID                number(22,0) not null,
                          DESCRIPTION                 varchar2(35),
                          LONG_DESCRIPTION            varchar2(254),
                          LS_COMPONENT_STATUS_ID      number(22,0) not null,
                          PO_ID                       varchar2(35),
                          MANUFACTURER                varchar2(35),
                          MODEL                       varchar2(35),
                          SERIAL_NUMBER               varchar2(35),
                          API_STATUS                  number(22,0) not null,
                          API_INSERT_TIME_STAMP       date not null,
                          API_ERROR_MESSAGE           varchar2(2000),
                          CURRENT_LEASE_COST          number(22,2),
                          INTERIM_INTEREST_BEGIN_DATE date,
                          GL_POSTING_BEGIN_DATE       date,
                          LS_ASSET_ID                 number(22,0),
                          API_PROCESS_TIME_STAMP      date,
                          API_BATCH_ID                number(22,0) not null,
                          TIME_STAMP                  date,
                          USER_ID                     varchar2(18)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_COMPONENT
                            add constraint PK_LS_API_COMPONENT
                                primary key (LEASED_ASSET_NUMBER, COMPONENT_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_DIST_DEF.');
      execute immediate 'create table LS_API_DIST_DEF
                         (
                          LEASED_ASSET_NUMBER    varchar2(35) not null,
                          DIST_DEF_TYPE_ID       number(22,0) not null,
                          EFFDT                  date not null,
                          DIST_DEF_SEQ           number(22,0) not null,
                          DIST_DEF_COUNTER       number(22,0) not null,
                          TIME_STAMP             date not null,
                          USER_ID                varchar2(18) not null,
                          MASS_CHANGE_ID         number(22,0),
                          COMPANY_ID             number(22,0) not null,
                          DIVISION_ID            number(22,0),
                          DEPARTMENT_ID          number(22,0) not null,
                          DIST_GL_ACCOUNT_ID     number(22,0) not null,
                          WORK_ORDER_NUMBER      varchar2(35),
                          COST_ELEMENT_ID        number(22,0) not null,
                          ASSET_LOCATION_ID      number(22,0),
                          STATE_ID               char(18),
                          ELEMENT_1              varchar2(35),
                          ELEMENT_2              varchar2(35),
                          ELEMENT_3              varchar2(35),
                          ELEMENT_4              varchar2(35),
                          ELEMENT_5              varchar2(35),
                          ELEMENT_6              varchar2(35),
                          ELEMENT_7              varchar2(35),
                          ELEMENT_8              varchar2(35),
                          ELEMENT_9              varchar2(35),
                          ELEMENT_10             varchar2(35),
                          DISTRIB_PCT            number(22,8) not null,
                          API_STATUS             number(22,0) not null,
                          API_INSERT_TIME_STAMP  date not null,
                          API_ERROR_MESSAGE      varchar2(2000),
                          LS_ASSET_ID            number(22,0),
                          API_PROCESS_TIME_STAMP date,
                          API_BATCH_ID           number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_DIST_DEF
                            add constraint PK_LS_API_DIST_DEF
                                primary key (LEASED_ASSET_NUMBER, DIST_DEF_TYPE_ID, EFFDT, DIST_DEF_SEQ, DIST_DEF_COUNTER)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_DIST_DEF_TMP.');
      execute immediate 'create table LS_API_DIST_DEF_TMP
                         (
                          LEASED_ASSET_NUMBER    varchar2(35) not null,
                          DIST_DEF_TYPE_ID       number(22,0) not null,
                          EFFDT                  number(22,0) not null,
                          DIST_DEF_SEQ           number(22,0) not null,
                          DIST_DEF_COUNTER       number(22,0) not null,
                          TIME_STAMP             date not null,
                          USER_ID                varchar2(18) not null,
                          MASS_CHANGE_ID         number(22,0),
                          COMPANY_ID             number(22,0) not null,
                          DIVISION_ID            number(22,0),
                          DEPARTMENT_ID          number(22,0) not null,
                          DIST_GL_ACCOUNT_ID     number(22,0) not null,
                          WORK_ORDER_NUMBER      varchar2(35),
                          COST_ELEMENT_ID        number(22,0) not null,
                          ASSET_LOCATION_ID      number(22,0),
                          STATE_ID               char(18),
                          ELEMENT_1              varchar2(35),
                          ELEMENT_2              varchar2(35),
                          ELEMENT_3              varchar2(35),
                          ELEMENT_4              varchar2(35),
                          ELEMENT_5              varchar2(35),
                          ELEMENT_6              varchar2(35),
                          ELEMENT_7              varchar2(35),
                          ELEMENT_8              varchar2(35),
                          ELEMENT_9              varchar2(35),
                          ELEMENT_10             varchar2(35),
                          DISTRIB_PCT            number(22,8) not null,
                          API_STATUS             number(22,0) not null,
                          API_INSERT_TIME_STAMP  date not null,
                          API_ERROR_MESSAGE      varchar2(2000),
                          LS_ASSET_ID            number(22,0),
                          API_PROCESS_TIME_STAMP date,
                          API_BATCH_ID           number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_DIST_DEF_TMP
                            add constraint PK_LS_API_DIST_DEF_TMP
                                primary key (LEASED_ASSET_NUMBER, DIST_DEF_TYPE_ID, EFFDT, DIST_DEF_SEQ, DIST_DEF_COUNTER)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_ILR.');
      execute immediate 'create table LS_API_ILR
                         (
                          ILR_NUMBER             varchar2(35) not null,
                          LEASE_ID               number(22,0) not null,
                          COMPANY_ID             number(22,0) not null,
                          INCEPTION_AIR          number(22,8),
                          EXTERNAL_ILR           varchar2(35),
                          ILR_STATUS_ID          number(22,0),
                          API_STATUS             number(22,0) not null,
                          API_INSERT_TIME_STAMP  date not null,
                          API_ERROR_MESSAGE      varchar2(2000),
                          ILR_ID                 number(22,0),
                          API_PROCESS_TIME_STAMP date,
                          API_BATCH_ID           number(22,0) not null,
                          TIME_STAMP             date,
                          USER_ID                varchar2(18)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_ILR
                            add constraint PK_LS_API_ILR
                                primary key (ILR_NUMBER)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_ILR_TERMS.');
      execute immediate 'create table LS_API_ILR_TERMS
                         (
                          ILR_NUMBER             varchar2(35) not null,
                          PAYMENT_TERM_ID        number(22,0),
                          PAYMENT_TERM_TYPE_ID   number(22,0) not null,
                          PAYMENT_TERM_DATE      date not null,
                          PAYMENT_FREQ_ID        number(22,0) not null,
                          NUMBER_OF_TERMS        number(22,0) not null,
                          EST_EXECUTORY_COST     number(22,2),
                          PAID_AMOUNT            number(22,2),
                          API_STATUS             number(22,0) not null,
                          API_INSERT_TIME_STAMP  date not null,
                          API_ERROR_MESSAGE      varchar2(2000),
                          ILR_ID                 number(22,0),
                          API_PROCESS_TIME_STAMP date,
                          API_BATCH_ID           number(22,0) not null,
                          LEASE_TYPE_ID          number(22,0),
                          TIME_STAMP             date,
                          USER_ID                varchar2(18)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_ILR_TERMS
                            add constraint PK_LS_API_ILR_TERMS
                                primary key (ILR_NUMBER, PAYMENT_TERM_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_ILR_TMP.');
      execute immediate 'create table LS_API_ILR_TMP
                         (
                          ILR_NUMBER             varchar2(35) not null,
                          LEASE_ID               number(22,0) not null,
                          COMPANY_ID             number(22,0) not null,
                          INCEPTION_AIR          number(22,8),
                          EXTERNAL_ILR           varchar2(35),
                          ILR_STATUS_ID          number(22,0),
                          API_STATUS             number(22,0) not null,
                          API_INSERT_TIME_STAMP  date not null,
                          API_ERROR_MESSAGE      varchar2(2000),
                          ILR_ID                 number(22,0),
                          API_PROCESS_TIME_STAMP date,
                          API_BATCH_ID           number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_ILR_TMP
                            add constraint PK_LS_API_ILR_TMP
                                primary key (ILR_NUMBER)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_INVOICE.');
      execute immediate 'create table LS_API_INVOICE
                         (
                          LEASED_ASSET_NUMBER    varchar2(35) not null,
                          COMPONENT_ID           number(22,0) not null,
                          INVOICE_NUMBER         varchar2(35) not null,
                          AMOUNT                 number(22,2) not null,
                          SUBMITTED_DATE         date not null,
                          PAID_DATE              date,
                          API_STATUS             number(22,0) not null,
                          API_INSERT_TIME_STAMP  date not null,
                          API_ERROR_MESSAGE      varchar2(2000),
                          LS_ASSET_ID            number(22,0),
                          CHARGE_ID              number(22,0),
                          API_PROCESS_TIME_STAMP date,
                          API_BATCH_ID           number(22,0) not null,
                          TIME_STAMP             date,
                          USER_ID                varchar2(18)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_INVOICE
                            add constraint PK_LS_API_INVOICE
                                primary key (LEASED_ASSET_NUMBER, COMPONENT_ID, INVOICE_NUMBER)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_PICK_PROPGRP.');
      execute immediate 'create table LS_API_PICK_PROPGRP
                         (
                          RETIREMENT_UNIT_ID number(22,0) not null,
                          UTILITY_ACCOUNT_ID number(22,0) not null,
                          BUS_SEGMENT_ID     number(22,0) not null,
                          TIME_STAMP         date,
                          USER_ID            varchar2(18),
                          PROPERTY_GROUP_ID  number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_PICK_PROPGRP
                            add constraint PK_LS_API_PICK_PROPGRP
                                primary key (RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_UNLOAD_ASSET.');
      execute immediate 'create table LS_API_UNLOAD_ASSET
                         (
                          LS_ASSET_ID  number(22,0) not null,
                          TIME_STAMP   date,
                          USER_ID      varchar2(18),
                          API_BATCH_ID number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_UNLOAD_ASSET
                            add constraint PK_LS_API_UNLOAD_ASSET
                                primary key (LS_ASSET_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_API_UNLOAD_ILR.');
      execute immediate 'create table LS_API_UNLOAD_ILR
                         (
                          ILR_ID       number(22,0) not null,
                          TIME_STAMP   date,
                          USER_ID      varchar2(18),
                          API_BATCH_ID number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_API_UNLOAD_ILR
                            add constraint PK_LS_API_UNLOAD_ILR
                                primary key (ILR_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_DELETE_LT_ACTIVITY.');
      execute immediate 'create table LS_ASSET_DELETE_LT_ACTIVITY
                         (
                          LS_ASSET_ID          number(22,0) not null,
                          LS_ASSET_ACTIVITY_ID number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          LEASE_CALC_RUN_ID    number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_ASSET_DELETE_LT_ACTIVITY
                            add constraint PK_LS_ASSET_DELETE_LT_ACTIVITY
                                primary key (LS_ASSET_ID, LS_ASSET_ACTIVITY_ID, LEASE_CALC_RUN_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_DELETELTACTIVITY_RUNID
                            on LS_ASSET_DELETE_LT_ACTIVITY (LEASE_CALC_RUN_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_FLEX_VALUES.');
      execute immediate 'create table LS_ASSET_FLEX_VALUES
                         (
                          ASSET_FLEX_ID  number(22,0) not null,
                          FLEX_VALUE     varchar2(254) not null,
                          TIME_STAMP     date,
                          USER_ID        varchar2(18),
                          STATUS_CODE_ID number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_ASSET_FLEX_VALUES
                            add constraint PK_LS_ASSET_FLEX_VALUES
                                primary key (ASSET_FLEX_ID, FLEX_VALUE)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_RETIREMENT_HISTORY.');
      execute immediate 'create table LS_ASSET_RETIREMENT_HISTORY
                         (
                          LS_ASSET_ID          number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          RETIREMENT_MONTH     number(22,0),
                          DISPOSITION_CODE     number(22,0),
                          CALC_INCLUSION       number(22,0),
                          RETIREMENT_STATUS_ID number(22,0),
                          LEASE_CALC_RUN_ID    number(22,0),
                          LONG_DESCRIPTION     varchar2(254),
                          WORK_ORDER_NUMBER    varchar2(35),
                          GL_JE_CODE           varchar2(12),
                          OUT_OF_SERVICE_DATE  date
                         )';

      execute immediate 'alter table LS_ASSET_RETIREMENT_HISTORY
                            add constraint R_LS_ASSET_RETIREMENT_HISTO1
                                foreign key (DISPOSITION_CODE)
                                references DISPOSITION_CODE (DISPOSITION_CODE)';

      --PK Constraint

      execute immediate 'alter table LS_ASSET_RETIREMENT_HISTORY
                            add constraint PK_LS_ASSET_RETIREMENT_HISTORY
                                primary key (LS_ASSET_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LS_ASSET_RETIRE_MONTH
                            on LS_ASSET_RETIREMENT_HISTORY (RETIREMENT_MONTH)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_STATUS.');
      execute immediate 'create table LS_ASSET_STATUS
                         (
                          LS_ASSET_STATUS_ID number(22,0) not null,
                          TIME_STAMP         date,
                          USER_ID            varchar2(18),
                          DESCRIPTION        varchar2(35) not null,
                          LONG_DESCRIPTION   varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_ASSET_STATUS
                            add constraint PK_LS_ASSET_STATUS
                                primary key (LS_ASSET_STATUS_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_CALC_APPROVAL_RULE.');
      execute immediate 'create table LS_CALC_APPROVAL_RULE
                         (
                          CALC_APPROVAL_RULE_ID number(22,0) not null,
                          TIME_STAMP            date,
                          USER_ID               varchar2(18),
                          DESCRIPTION           varchar2(35),
                          LONG_DESCRIPTION      varchar2(254)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_CALC_APPROVAL_RULE
                            add constraint PK_LS_CALC_APPROVAL_RULE
                                primary key (CALC_APPROVAL_RULE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_CANCELABLE_TYPE.');
      execute immediate 'create table LS_CANCELABLE_TYPE
                         (
                          CANCELABLE_TYPE_ID number(22,0) not null,
                          TIME_STAMP         date,
                          USER_ID            varchar2(18),
                          DESCRIPTION        varchar2(35) not null,
                          LONG_DESCRIPTION   varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_CANCELABLE_TYPE
                            add constraint PK_LS_CANCELABLE_TYPE
                                primary key (CANCELABLE_TYPE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_COLUMN_USAGE.');
      execute immediate 'create table LS_COLUMN_USAGE
                         (
                          COLUMN_USAGE_ID  number(22,0) not null,
                          TIME_STAMP       date,
                          USER_ID          varchar2(18),
                          DESCRIPTION      varchar2(35) not null,
                          LONG_DESCRIPTION varchar2(254)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_COLUMN_USAGE
                            add constraint PK_LS_COLUMN_USAGE
                                primary key (COLUMN_USAGE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_COMPONENT_STATUS.');
      execute immediate 'create table LS_COMPONENT_STATUS
                         (
                          LS_COMPONENT_STATUS_ID number(22,0) not null,
                          TIME_STAMP             date,
                          USER_ID                varchar2(18),
                          DESCRIPTION            varchar2(35) not null,
                          LONG_DESCRIPTION       varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_COMPONENT_STATUS
                            add constraint PK_LS_COMPONENT_STATUS
                                primary key (LS_COMPONENT_STATUS_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_COST_ELEMENT_VALID.');
      execute immediate 'create table LS_COST_ELEMENT_VALID
                         (
                          COST_ELEMENT_ID number(22,0) not null,
                          TIME_STAMP      date,
                          USER_ID         varchar2(18)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_COST_ELEMENT_VALID
                            add constraint PK_LS_COST_ELEMENT_VALID
                                primary key (COST_ELEMENT_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_DISTRIBUTION_TYPE.');
      execute immediate 'create table LS_DISTRIBUTION_TYPE
                         (
                          DISTRIBUTION_TYPE_ID number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          DESCRIPTION          varchar2(35) not null,
                          LONG_DESCRIPTION     varchar2(254) not null,
                          EXP_BAL_FLAG         varchar2(35)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_DISTRIBUTION_TYPE
                            add constraint PK_LS_DISTRIBUTION_TYPE
                                primary key (DISTRIBUTION_TYPE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_DIST_DEF_PARMS.');
      execute immediate 'create table LS_DIST_DEF_PARMS
                         (
                          PARM_DESCR varchar2(254) not null,
                          PARM_VALUE varchar2(254),
                          TIME_STAMP date,
                          USER_ID    varchar2(18)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_DIST_DEF_PARMS
                            add constraint PK_LS_DIST_DEF_PARMS
                                primary key (PARM_DESCR)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_DIST_DEF_TYPE.');
      execute immediate 'create table LS_DIST_DEF_TYPE
                         (
                          DIST_DEF_TYPE_ID number(22,0) not null,
                          TIME_STAMP       date,
                          USER_ID          varchar2(18),
                          DESCRIPTION      varchar2(35),
                          LONG_DESCRIPTION varchar2(254),
                          COLUMN_USAGE_ID  number(1,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_DIST_DEF_TYPE
                            add constraint PK_LS_DIST_DEF_TYPE
                                primary key (DIST_DEF_TYPE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_DIST_DEPARTMENT.');
      execute immediate 'create table LS_DIST_DEPARTMENT
                         (
                          DEPARTMENT_ID            number(22,0) not null,
                          TIME_STAMP               date,
                          USER_ID                  varchar2(18),
                          DESCRIPTION              varchar2(35),
                          LONG_DESCRIPTION         varchar2(254),
                          EXTERNAL_DEPARTMENT_CODE varchar2(35),
                          STATUS_CODE_ID           number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_DIST_DEPARTMENT
                            add constraint PK_LS_DIST_DEPARTMENT
                                primary key (DEPARTMENT_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSDIST_EXTDEPT
                            on LS_DIST_DEPARTMENT (EXTERNAL_DEPARTMENT_CODE)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_DIST_GL_ACCOUNT.');
      execute immediate 'create table LS_DIST_GL_ACCOUNT
                         (
                          DIST_GL_ACCOUNT_ID    number(22,0) not null,
                          TIME_STAMP            date,
                          USER_ID               varchar2(18),
                          FERC_SYS_OF_ACCTS_ID  number(22,0),
                          ACCOUNT_TYPE_ID       number(22,0),
                          DESCRIPTION           varchar2(35),
                          LONG_DESCRIPTION      varchar2(254),
                          STATUS_CODE_ID        number(22,0),
                          EXTERNAL_ACCOUNT_CODE varchar2(35)
                         )';

      execute immediate 'alter table LS_DIST_GL_ACCOUNT
                            add constraint FK_DISTGLACCT_ACCTTYPEID
                                foreign key (ACCOUNT_TYPE_ID)
                                references ACCOUNT_TYPE (ACCOUNT_TYPE_ID)';

      execute immediate 'alter table LS_DIST_GL_ACCOUNT
                            add constraint FK_DISTGLACCT_FERCSYSACCT
                                foreign key (FERC_SYS_OF_ACCTS_ID)
                                references FERC_SYS_OF_ACCTS (FERC_SYS_OF_ACCTS_ID)';

      execute immediate 'alter table LS_DIST_GL_ACCOUNT
                            add constraint FK_DISTGLACCT_STATUSID
                                foreign key (STATUS_CODE_ID)
                                references STATUS_CODE (STATUS_CODE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_DIST_GL_ACCOUNT
                            add constraint PK_LS_DIST_GL_ACCOUNT
                                primary key (DIST_GL_ACCOUNT_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSDIST_EXTACCT
                            on LS_DIST_GL_ACCOUNT (EXTERNAL_ACCOUNT_CODE)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_EOL_DEFAULTS.');
      execute immediate 'create table LS_EOL_DEFAULTS
                         (
                          FIELD_NAME varchar2(35) not null,
                          TIME_STAMP date,
                          USER_ID varchar2(18),
                          FIELD_VALUE varchar2(254)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_EOL_DEFAULTS
                            add constraint PK_LS_EOL_DEFAULTS
                                primary key (FIELD_NAME)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_EXTENDED_RENTAL_TYPE.');
      execute immediate 'create table LS_EXTENDED_RENTAL_TYPE
                         (
                          EXTENDED_RENTAL_TYPE_ID number(22,0) not null,
                          TIME_STAMP              date,
                          USER_ID                 varchar2(18),
                          DESCRIPTION             varchar2(35) not null,
                          LONG_DESCRIPTION        varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_EXTENDED_RENTAL_TYPE
                            add constraint PK_LS_EXTENDED_RENTAL_TYPE
                                primary key (EXTENDED_RENTAL_TYPE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_FCST_ALLOCATION.');
      execute immediate 'create table LS_FCST_ALLOCATION
                         (
                          DIST_LINE_ID            number(22,0) not null,
                          TIME_STAMP              date,
                          USER_ID                 varchar2(18),
                          DIST_DEF_TYPE_ID        number(22,0),
                          DISTRIBUTION_TYPE_ID    number(22,0),
                          LS_ASSET_ID             number(22,0),
                          COMPANY_ID              number(22,0),
                          DIVISION_ID             number(22,0),
                          DEPARTMENT_ID           number(22,0),
                          DIST_GL_ACCOUNT_ID      number(22,0),
                          WORK_ORDER_NUMBER       varchar2(35),
                          COST_ELEMENT_ID         number(22,0),
                          ASSET_LOCATION_ID       number(22,0),
                          STATE_ID                char(18),
                          ELEMENT_1               varchar2(35),
                          ELEMENT_2               varchar2(35),
                          ELEMENT_3               varchar2(35),
                          ELEMENT_4               varchar2(35),
                          ELEMENT_5               varchar2(35),
                          ELEMENT_6               varchar2(35),
                          ELEMENT_7               varchar2(35),
                          ELEMENT_8               varchar2(35),
                          ELEMENT_9               varchar2(35),
                          ELEMENT_10              varchar2(35),
                          GL_JE_CODE              varchar2(35),
                          GL_POSTING_DATE         date,
                          GL_POSTING_MONTHNUM     number(22,0),
                          GL_POSTING_TIMESTAMP    date,
                          GL_POSTING_USERID       varchar2(18),
                          LEASE_CALC_RUN_ID       number(22,0),
                          MASS_CHANGE_ID          number(22,0),
                          DESCRIPTION             varchar2(35),
                          LONG_DESCRIPTION        varchar2(254),
                          AMOUNT                  number(22,2),
                          DEPR_EXPENSE            number(22,2),
                          INTEREST_ACCRUAL        number(22,2),
                          PRINCIPAL_ACCRUAL       number(22,2),
                          EXTENDED_RENTAL_ACCRUAL number(22,2),
                          SALES_TAX_ACCRUAL       number(22,2),
                          USE_TAX_ACCRUAL         number(22,2),
                          SUM_AMOUNT              number(22,2),
                          DISTRIB_PCT             number(22,8),
                          JE_METHOD_ID            number(22,0),
                          LT_3_ACCRUAL            number(22,2),
                          LT_4_ACCRUAL            number(22,2),
                          LT_5_ACCRUAL            number(22,2),
                          LT_6_ACCRUAL            number(22,2),
                          LT_7_ACCRUAL            number(22,2),
                          LT_8_ACCRUAL            number(22,2),
                          LT_9_ACCRUAL            number(22,2),
                          LT_10_ACCRUAL           number(22,2),
                          LEASE_ID                number(22,0),
                          DC_INDICATOR            number(22,0),
                          LEASE_NUMBER            varchar2(35),
                          FCST_CASE_ID            number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_FCST_ALLOCATION
                            add constraint PK_LS_FCST_ALLOCATION
                                primary key (DIST_LINE_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSFCSTALLOC_ASSETCOMONTH
                            on LS_FCST_ALLOCATION (LS_ASSET_ID, COMPANY_ID, GL_POSTING_MONTHNUM)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_FCST_ASSETS.');
      execute immediate 'create table LS_FCST_ASSETS
                         (
                          LS_ASSET_ID         number(22,0) not null,
                          GL_POSTING_MONTHNUM number(22,0) not null,
                          FCST_CASE_ID        number(22,0) not null,
                          TIME_STAMP          date,
                          USER_ID             varchar2(18)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_FCST_ASSETS
                            add constraint PK_LS_FCST_ASSETS
                                primary key (LS_ASSET_ID, GL_POSTING_MONTHNUM, FCST_CASE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_FCST_ASSET_ACTIVITY.');
      execute immediate 'create table LS_FCST_ASSET_ACTIVITY
                         (
                          LS_ASSET_ID                  number(22,0) not null,
                          LS_ASSET_ACTIVITY_ID         number(22,0) not null,
                          TIME_STAMP                   date,
                          USER_ID                      varchar2(18),
                          GL_MONTH_NUMBER              number(22,0) not null,
                          LS_ACTIVITY_TYPE_ID          number(22,0) not null,
                          LEASE_CALC_RUN_ID            number(22,0),
                          CURRENT_LEASE_COST           number(22,2),
                          BEG_CAPITALIZED_LEASE_COST   number(22,2),
                          END_CAPITALIZED_LEASE_COST   number(22,2),
                          BEG_LEASE_OBLIGATION         number(22,2),
                          END_LEASE_OBLIGATION         number(22,2),
                          BEG_NONCURR_LEASE_OBLIGATION number(22,2),
                          END_NONCURR_LEASE_OBLIGATION number(22,2),
                          REMAINING_LIFE               number(22,0),
                          DEPR_EXPENSE                 number(22,2),
                          BEG_DEPR_RESERVE             number(22,2),
                          END_DEPR_RESERVE             number(22,2),
                          REGULATORY_AMORT             number(22,2),
                          BEG_REGULATORY_AMORT         number(22,2),
                          END_REGULATORY_AMORT         number(22,2),
                          UNPAID_BALANCE               number(22,2),
                          LEVEL_PAYMENT_EXPENSE        number(22,2),
                          INTEREST_ACCRUAL             number(22,2),
                          PRINCIPAL_ACCRUAL            number(22,2),
                          EXTENDED_RENTAL_ACCRUAL      number(22,2),
                          SALES_TAX_ACCRUAL            number(22,2),
                          USE_TAX_ACCRUAL              number(22,2),
                          INTEREST_AMOUNT              number(22,2),
                          PRINCIPAL_AMOUNT             number(22,2),
                          EXTENDED_RENTAL_AMOUNT       number(22,2),
                          SALES_TAX_AMOUNT             number(22,2),
                          INTEREST_ADJUSTMENT          number(22,2),
                          PRINCIPAL_ADJUSTMENT         number(22,2),
                          EXTENDED_RENTAL_ADJUSTMENT   number(22,2),
                          SALES_TAX_ADJUSTMENT         number(22,2),
                          USE_TAX_ADJUSTMENT           number(22,2),
                          COMPANY_ID                   number(22,0),
                          ACCOUNTING_DEFAULT_ID        number(22,0),
                          JE_METHOD_ID                 number(22,0),
                          LT_3_ACCRUAL                 number(22,2),
                          LT_4_ACCRUAL                 number(22,2),
                          LT_5_ACCRUAL                 number(22,2),
                          LT_6_ACCRUAL                 number(22,2),
                          LT_7_ACCRUAL                 number(22,2),
                          LT_8_ACCRUAL                 number(22,2),
                          LT_9_ACCRUAL                 number(22,2),
                          LT_10_ACCRUAL                number(22,2),
                          LT_3_AMOUNT                  number(22,2),
                          LT_4_AMOUNT                  number(22,2),
                          LT_5_AMOUNT                  number(22,2),
                          LT_6_AMOUNT                  number(22,2),
                          LT_7_AMOUNT                  number(22,2),
                          LT_8_AMOUNT                  number(22,2),
                          LT_9_AMOUNT                  number(22,2),
                          LT_10_AMOUNT                 number(22,2),
                          LT_3_ADJUST                  number(22,2),
                          LT_4_ADJUST                  number(22,2),
                          LT_5_ADJUST                  number(22,2),
                          LT_6_ADJUST                  number(22,2),
                          LT_7_ADJUST                  number(22,2),
                          LT_8_ADJUST                  number(22,2),
                          LT_9_ADJUST                  number(22,2),
                          LT_10_ADJUST                 number(22,2),
                          LEASE_ID                     number(22,0),
                          DC_INDICATOR                 number(22,0),
                          JE_MONTH_NUMBER              number(22,0),
                          FCST_CASE_ID                 number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_FCST_ASSET_ACTIVITY
                            add constraint PK_LS_FCST_ASSET_ACTIVITY
                                primary key (LS_ASSET_ID, LS_ASSET_ACTIVITY_ID, FCST_CASE_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSFCSTACT_COMONTHFCST
                            on LS_FCST_ASSET_ACTIVITY (COMPANY_ID, GL_MONTH_NUMBER, FCST_CASE_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_FCST_CONTROL.');
      execute immediate 'create table LS_FCST_CONTROL
                         (
                          FCST_CASE_ID     number(22,0) not null,
                          MODIFIED_DATE    date,
                          TIME_STAMP       date,
                          USER_ID          varchar2(18),
                          LONG_DESCRIPTION varchar2(254),
                          WHERE_CLAUSE     varchar2(1000),
                          COMPANY_STRING   varchar2(1000)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_FCST_CONTROL
                            add constraint PK_LS_FCST_CONTROL
                                primary key (FCST_CASE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_FCST_DIST_DEF.');
      execute immediate 'create table LS_FCST_DIST_DEF
                         (
                          COMPANY_ID          number(22,0) not null,
                          GL_POSTING_MONTHNUM number(22,0) not null,
                          LS_ASSET_ID         number(22,0) not null,
                          EFFDT_NUMBER        number(22,0) not null,
                          DIST_DEF_TYPE_ID    number(22,0) not null,
                          JE_METHOD_ID        number(22,0) not null,
                          TIME_STAMP          date,
                          USER_ID             varchar2(18),
                          FCST_CASE_ID        number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_FCST_DIST_DEF
                            add constraint PK_LS_FCST_DIST_DEF
                                primary key (COMPANY_ID, GL_POSTING_MONTHNUM, LS_ASSET_ID, EFFDT_NUMBER, DIST_DEF_TYPE_ID, JE_METHOD_ID, FCST_CASE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_FCST_EXCHANGE.');
      execute immediate 'create table LS_FCST_EXCHANGE
                         (
                          DIST_LINE_ID             number(22,0) not null,
                          TIME_STAMP               date,
                          USER_ID                  varchar2(18),
                          DIST_DEF_TYPE_ID         number(22,0),
                          DISTRIBUTION_TYPE_ID     number(22,0),
                          LS_ASSET_ID              number(22,0),
                          GL_COMPANY_NO            varchar2(35),
                          DIVISION_ID              number(22,0),
                          EXTERNAL_DEPARTMENT_CODE varchar2(35),
                          EXTERNAL_ACCOUNT_CODE    varchar2(35),
                          WORK_ORDER_NUMBER        varchar2(35),
                          EXTERNAL_COST_ELEMENT    varchar2(35),
                          EXT_ASSET_LOCATION       varchar2(35),
                          STATE_CODE               char(18),
                          ELEMENT_1                varchar2(35),
                          ELEMENT_2                varchar2(35),
                          ELEMENT_3                varchar2(35),
                          ELEMENT_4                varchar2(35),
                          ELEMENT_5                varchar2(35),
                          ELEMENT_6                varchar2(35),
                          ELEMENT_7                varchar2(35),
                          ELEMENT_8                varchar2(35),
                          ELEMENT_9                varchar2(35),
                          ELEMENT_10               varchar2(35),
                          GL_JE_CODE               varchar2(35),
                          GL_POSTING_DATE          date not null,
                          GL_POSTING_MONTHNUM      number(22,0) not null,
                          GL_POSTING_TIMESTAMP     date,
                          GL_POSTING_USERID        varchar2(18),
                          LEASE_CALC_RUN_ID        number(22,0),
                          MASS_CHANGE_ID           number(22,0),
                          DESCRIPTION              varchar2(35),
                          LONG_DESCRIPTION         varchar2(254),
                          AMOUNT                   number(22,2) not null,
                          POSTING_STATUS           number(22,0) not null,
                          LEASE_ID                 number(22,0),
                          DC_INDICATOR             number(22,0),
                          JE_METHOD_ID             number(22,0),
                          LEASE_NUMBER             varchar2(35),
                          COMPANY_ID               number(22,0),
                          DEPARTMENT_ID            number(22,0),
                          GL_ACCOUNT_ID            number(22,0),
                          COST_ELEMENT_ID          number(22,0),
                          ASSET_LOCATION_ID        number(22,0),
                          FCST_CASE_ID             number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_FCST_EXCHANGE
                            add constraint PK_LS_FCST_EXCHANGE
                                primary key (DIST_LINE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_FCST_ROUNDING.');
      execute immediate 'create table LS_FCST_ROUNDING
                         (
                          LS_ASSET_ID   number(22,0) not null,
                          TARGET_AMOUNT number(22,2),
                          ALLOC_AMOUNT  number(22,2),
                          DELTA         number(22,2),
                          DIST_LINE_ID  number(22,2),
                          FCST_CASE_ID  number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_FCST_ROUNDING
                            add constraint PK_LS_FCST_ROUNDING
                                primary key (LS_ASSET_ID, FCST_CASE_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LS_FCST_ROUND_DISTLNID
                            on LS_FCST_ROUNDING (DIST_LINE_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LSFCSTROUND_ASSET
                            on LS_FCST_ROUNDING (LS_ASSET_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_FCST_ROUNDING2.');
      execute immediate 'create table LS_FCST_ROUNDING2
                         (
                          COMPANY_ID   number(22,0) not null,
                          LEASE_ID     number(22,0) not null,
                          DIFF         number(22,2),
                          DELTA        number(22,2),
                          DIST_LINE_ID number(22,0),
                          FCST_CASE_ID number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_FCST_ROUNDING2
                            add constraint PK_LS_FCST_ROUNDING2
                                primary key (COMPANY_ID, LEASE_ID, FCST_CASE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_FCST_STAGING.');
      execute immediate 'create table LS_FCST_STAGING
                         (
                          COMPANY_ID          number(22,0) not null,
                          GL_POSTING_MONTHNUM number(22,0) not null,
                          TIME_STAMP          date,
                          USER_ID             varchar2(18),
                          FCST_CASE_ID        number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_FCST_STAGING
                            add constraint PK_LS_FCST_STAGING
                                primary key (COMPANY_ID, GL_POSTING_MONTHNUM, FCST_CASE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_GL_TRANSACTION.');
      execute immediate 'create table LS_GL_TRANSACTION
                         (
                          GL_TRANS_ID            number(22,0) not null,
                          TIME_STAMP             date,
                          USER_ID                varchar2(18),
                          MONTH                  date not null,
                          COMPANY_NUMBER         char(10),
                          GL_ACCOUNT             varchar2(254),
                          DEBIT_CREDIT_INDICATOR number(22,0),
                          AMOUNT                 number(22,2) default 0,
                          GL_JE_CODE             char(18),
                          GL_STATUS_ID           number(22,0) not null,
                          DESCRIPTION            varchar2(35),
                          LS_ASSET_ID            number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_GL_TRANSACTION
                            add constraint PK_LS_GL_TRANSACTION
                                primary key (GL_TRANS_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSGLTRANSACCT
                            on LS_GL_TRANSACTION (GL_ACCOUNT)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_GRANDFATHER_LEVEL.');
      execute immediate 'create table LS_GRANDFATHER_LEVEL
                         (
                          GRANDFATHER_LEVEL_ID number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          DESCRIPTION          varchar2(35) not null,
                          LONG_DESCRIPTION     varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_GRANDFATHER_LEVEL
                            add constraint PK_LS_GRANDFATHER_LEVEL
                                primary key (GRANDFATHER_LEVEL_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ILR_GROUP_AIR.');
      execute immediate 'create table LS_ILR_GROUP_AIR
                         (
                          LEASE_ID          number(22,0) not null,
                          ILR_GROUP_ID      number(22,0) not null,
                          PAYMENT_TERM_DATE date not null,
                          TIME_STAMP        date,
                          USER_ID           varchar2(18),
                          AIR_ADJUSTMENT    number(22,8)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_ILR_GROUP_AIR
                            add constraint PK_LS_ILR_GROUP_AIR
                                primary key (LEASE_ID, ILR_GROUP_ID, PAYMENT_TERM_DATE)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ILR_SCHEDULE.');
      execute immediate 'create table LS_ILR_SCHEDULE
                         (
                          ILR_ID                     number(22,0) not null,
                          GL_MONTH_NUMBER            number(22,0) not null,
                          TIME_STAMP                 date,
                          USER_ID                    varchar2(18),
                          PAYMENT_TERM_TYPE_ID       number(22,0),
                          CURRENT_LEASE_COST         number(22,2),
                          CAPITALIZED_COST           number(22,2),
                          BEG_LEASE_OBLIGATION       number(22,2),
                          END_LEASE_OBLIGATION       number(22,2),
                          BEG_UNPAID_BALANCE         number(22,2),
                          END_UNPAID_BALANCE         number(22,2),
                          DEPR_EXPENSE               number(22,2),
                          DEPR_RESERVE               number(22,2),
                          INTEREST_ACCRUAL           number(22,2),
                          PRINCIPAL_ACCRUAL          number(22,2),
                          EXTENDED_RENTAL_ACCRUAL    number(22,2),
                          INTEREST_AMOUNT            number(22,2),
                          PRINCIPAL_AMOUNT           number(22,2),
                          EXTENDED_RENTAL_AMOUNT     number(22,2),
                          INTEREST_ADJUSTMENT        number(22,2),
                          PRINCIPAL_ADJUSTMENT       number(22,2),
                          EXTENDED_RENTAL_ADJUSTMENT number(22,2),
                          LEASE_CALC_RUN_ID          number(22,2),
                          CALC_TIME_STAMP            date,
                          PAYMT_MONTH_YN             number(22,0),
                          EXECUTORY_ACCRUAL          number(22,2) default 0,
                          EXECUTORY_AMOUNT           number(22,2) default 0,
                          CONTINGENT_ACCRUAL         number(22,2) default 0,
                          CONTINGENT_AMOUNT          number(22,2) default 0,
                          EXECUTORY_ADJUSTMENT       number(22,2),
                          CONTINGENT_ADJUSTMENT      number(22,2),
                          PAYMENT_AMOUNT             number(22,2)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_ILR_SCHEDULE
                            add constraint PK_LS_ILR_SCHEDULE
                                primary key (ILR_ID, GL_MONTH_NUMBER)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSILRSCHED_CALCRUNID
                            on LS_ILR_SCHEDULE (LEASE_CALC_RUN_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ILR_STATUS.');
      execute immediate 'create table LS_ILR_STATUS
                         (
                          ILR_STATUS_ID    number(22,0) not null,
                          TIME_STAMP       date,
                          USER_ID          varchar2(18),
                          DESCRIPTION      varchar2(35) not null,
                          LONG_DESCRIPTION varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_ILR_STATUS
                            add constraint PK_LS_ILR_STATUS
                                primary key (ILR_STATUS_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_JE_ALLOCATION.');
      execute immediate 'create table LS_JE_ALLOCATION
                         (
                          DIST_LINE_ID            number(22,0) not null,
                          TIME_STAMP              date,
                          USER_ID                 varchar2(18),
                          DIST_DEF_TYPE_ID        number(22,0),
                          DISTRIBUTION_TYPE_ID    number(22,0),
                          LS_ASSET_ID             number(22,0),
                          COMPANY_ID              number(22,0),
                          DIVISION_ID             number(22,0),
                          DEPARTMENT_ID           number(22,0),
                          DIST_GL_ACCOUNT_ID      number(22,0),
                          WORK_ORDER_NUMBER       varchar2(35),
                          COST_ELEMENT_ID         number(22,0),
                          ASSET_LOCATION_ID       number(22,0),
                          STATE_ID                char(18),
                          ELEMENT_1               varchar2(35),
                          ELEMENT_2               varchar2(35),
                          ELEMENT_3               varchar2(35),
                          ELEMENT_4               varchar2(35),
                          ELEMENT_5               varchar2(35),
                          ELEMENT_6               varchar2(35),
                          ELEMENT_7               varchar2(35),
                          ELEMENT_8               varchar2(35),
                          ELEMENT_9               varchar2(35),
                          ELEMENT_10              varchar2(35),
                          GL_JE_CODE              varchar2(35),
                          GL_POSTING_DATE         date,
                          GL_POSTING_MONTHNUM     number(22,0),
                          GL_POSTING_TIMESTAMP    date,
                          GL_POSTING_USERID       varchar2(18),
                          LEASE_CALC_RUN_ID       number(22,0),
                          MASS_CHANGE_ID          number(22,0),
                          DESCRIPTION             varchar2(35),
                          LONG_DESCRIPTION        varchar2(254),
                          AMOUNT                  number(22,2),
                          DEPR_EXPENSE            number(22,2),
                          INTEREST_ACCRUAL        number(22,2),
                          PRINCIPAL_ACCRUAL       number(22,2),
                          EXTENDED_RENTAL_ACCRUAL number(22,2),
                          SALES_TAX_ACCRUAL       number(22,2),
                          USE_TAX_ACCRUAL         number(22,2),
                          SUM_AMOUNT              number(22,2),
                          DISTRIB_PCT             number(22,8),
                          JE_METHOD_ID            number(22,0),
                          LT_3_ACCRUAL            number(22,2),
                          LT_4_ACCRUAL            number(22,2),
                          LT_5_ACCRUAL            number(22,2),
                          LT_6_ACCRUAL            number(22,2),
                          LT_7_ACCRUAL            number(22,2),
                          LT_8_ACCRUAL            number(22,2),
                          LT_9_ACCRUAL            number(22,2),
                          LT_10_ACCRUAL           number(22,2),
                          LEASE_ID                number(22,0),
                          DC_INDICATOR            number(22,0),
                          LEASE_NUMBER            varchar2(35)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_JE_ALLOCATION
                            add constraint PK_LS_JE_ALLOCATION
                                primary key (DIST_LINE_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSJE_ALLOC_ASSETRUNID
                            on LS_JE_ALLOCATION (LS_ASSET_ID, LEASE_CALC_RUN_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LSJEALLOC_LEASE_ID
                            on LS_JE_ALLOCATION (LEASE_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LS_JE_ALLOC
                            on LS_JE_ALLOCATION (COMPANY_ID, GL_POSTING_MONTHNUM)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LS_JE_ALLOC_AMOUNT
                            on LS_JE_ALLOCATION (LS_ASSET_ID, AMOUNT)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_JE_ASSET_ACTIVITY.');
      execute immediate 'create table LS_JE_ASSET_ACTIVITY
                         (
                          LS_ASSET_ID                  number(22,0) not null,
                          LS_ASSET_ACTIVITY_ID         number(22,0) not null,
                          TIME_STAMP                   date,
                          USER_ID                      varchar2(18),
                          GL_MONTH_NUMBER              number(22,0) not null,
                          LS_ACTIVITY_TYPE_ID          number(22,0) not null,
                          LEASE_CALC_RUN_ID            number(22,0),
                          CURRENT_LEASE_COST           number(22,2),
                          BEG_CAPITALIZED_LEASE_COST   number(22,2),
                          END_CAPITALIZED_LEASE_COST   number(22,2),
                          BEG_LEASE_OBLIGATION         number(22,2),
                          END_LEASE_OBLIGATION         number(22,2),
                          BEG_NONCURR_LEASE_OBLIGATION number(22,2),
                          END_NONCURR_LEASE_OBLIGATION number(22,2),
                          REMAINING_LIFE               number(22,0),
                          DEPR_EXPENSE                 number(22,2),
                          BEG_DEPR_RESERVE             number(22,2),
                          END_DEPR_RESERVE             number(22,2),
                          REGULATORY_AMORT             number(22,2),
                          BEG_REGULATORY_AMORT         number(22,2),
                          END_REGULATORY_AMORT         number(22,2),
                          UNPAID_BALANCE               number(22,2),
                          LEVEL_PAYMENT_EXPENSE        number(22,2),
                          INTEREST_ACCRUAL             number(22,2),
                          PRINCIPAL_ACCRUAL            number(22,2),
                          EXTENDED_RENTAL_ACCRUAL      number(22,2),
                          SALES_TAX_ACCRUAL            number(22,2),
                          USE_TAX_ACCRUAL              number(22,2),
                          INTEREST_AMOUNT              number(22,2),
                          PRINCIPAL_AMOUNT             number(22,2),
                          EXTENDED_RENTAL_AMOUNT       number(22,2),
                          SALES_TAX_AMOUNT             number(22,2),
                          INTEREST_ADJUSTMENT          number(22,2),
                          PRINCIPAL_ADJUSTMENT         number(22,2),
                          EXTENDED_RENTAL_ADJUSTMENT   number(22,2),
                          SALES_TAX_ADJUSTMENT         number(22,2),
                          USE_TAX_ADJUSTMENT           number(22,2),
                          COMPANY_ID                   number(22,0),
                          ACCOUNTING_DEFAULT_ID        number(22,0),
                          JE_METHOD_ID                 number(22,0),
                          LT_3_ACCRUAL                 number(22,2),
                          LT_4_ACCRUAL                 number(22,2),
                          LT_5_ACCRUAL                 number(22,2),
                          LT_6_ACCRUAL                 number(22,2),
                          LT_7_ACCRUAL                 number(22,2),
                          LT_8_ACCRUAL                 number(22,2),
                          LT_9_ACCRUAL                 number(22,2),
                          LT_10_ACCRUAL                number(22,2),
                          LT_3_AMOUNT                  number(22,2),
                          LT_4_AMOUNT                  number(22,2),
                          LT_5_AMOUNT                  number(22,2),
                          LT_6_AMOUNT                  number(22,2),
                          LT_7_AMOUNT                  number(22,2),
                          LT_8_AMOUNT                  number(22,2),
                          LT_9_AMOUNT                  number(22,2),
                          LT_10_AMOUNT                 number(22,2),
                          LT_3_ADJUST                  number(22,2),
                          LT_4_ADJUST                  number(22,2),
                          LT_5_ADJUST                  number(22,2),
                          LT_6_ADJUST                  number(22,2),
                          LT_7_ADJUST                  number(22,2),
                          LT_8_ADJUST                  number(22,2),
                          LT_9_ADJUST                  number(22,2),
                          LT_10_ADJUST                 number(22,2),
                          LEASE_ID                     number(22,0),
                          DC_INDICATOR                 number(22,0),
                          JE_MONTH_NUMBER              number(22,0),
                          EXECUTORY_ACCRUAL            number(22,2) default 0,
                          EXECUTORY_AMOUNT             number(22,2) default 0,
                          EXECUTORY_ADJUSTMENT         number(22,2) default 0,
                          CONTINGENT_ACCRUAL           number(22,2) default 0,
                          CONTINGENT_AMOUNT            number(22,2) default 0,
                          CONTINGENT_ADJUSTMENT        number(22,2) default 0
                         )';

      --PK Constraint

      execute immediate 'alter table LS_JE_ASSET_ACTIVITY
                            add constraint PK_LS_JE_ASSET_ACTIVITY
                                primary key (LS_ASSET_ID, LS_ASSET_ACTIVITY_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LS_JE_ASSETACT
                            on LS_JE_ASSET_ACTIVITY (COMPANY_ID, GL_MONTH_NUMBER)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index LS_JE_ASSET_ACTIVITY_MIKE1
                            on LS_JE_ASSET_ACTIVITY (JE_MONTH_NUMBER)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LS_JE_ALLOC_COID
                             on LS_JE_ASSET_ACTIVITY (COMPANY_ID)
                                tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_JE_EXCHANGE.');
      execute immediate 'create table LS_JE_EXCHANGE
                         (
                          DIST_LINE_ID             number(22,0) not null,
                          TIME_STAMP               date,
                          USER_ID                  varchar2(18),
                          DIST_DEF_TYPE_ID         number(22,0),
                          DISTRIBUTION_TYPE_ID     number(22,0),
                          LS_ASSET_ID              number(22,0),
                          GL_COMPANY_NO            varchar2(35),
                          DIVISION_ID              number(22,0),
                          EXTERNAL_DEPARTMENT_CODE varchar2(35),
                          EXTERNAL_ACCOUNT_CODE    varchar2(35),
                          WORK_ORDER_NUMBER        varchar2(35),
                          EXTERNAL_COST_ELEMENT    varchar2(35),
                          EXT_ASSET_LOCATION       varchar2(35),
                          STATE_CODE               char(18),
                          ELEMENT_1                varchar2(35),
                          ELEMENT_2                varchar2(35),
                          ELEMENT_3                varchar2(35),
                          ELEMENT_4                varchar2(35),
                          ELEMENT_5                varchar2(35),
                          ELEMENT_6                varchar2(35),
                          ELEMENT_7                varchar2(35),
                          ELEMENT_8                varchar2(35),
                          ELEMENT_9                varchar2(35),
                          ELEMENT_10               varchar2(35),
                          GL_JE_CODE               varchar2(35),
                          GL_POSTING_DATE          date not null,
                          GL_POSTING_MONTHNUM      number(22,0) not null,
                          GL_POSTING_TIMESTAMP     date,
                          GL_POSTING_USERID        varchar2(18),
                          LEASE_CALC_RUN_ID        number(22,0),
                          MASS_CHANGE_ID           number(22,0),
                          DESCRIPTION              varchar2(35),
                          LONG_DESCRIPTION         varchar2(254),
                          AMOUNT                   number(22,2) not null,
                          POSTING_STATUS           number(22,0) not null,
                          LEASE_ID                 number(22,0),
                          DC_INDICATOR             number(22,0),
                          JE_METHOD_ID             number(22,0),
                          LEASE_NUMBER             varchar2(35),
                          COMPANY_ID               number(22,0),
                          DEPARTMENT_ID            number(22,0),
                          GL_ACCOUNT_ID            number(22,0),
                          COST_ELEMENT_ID          number(22,0),
                          ASSET_LOCATION_ID        number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_JE_EXCHANGE
                            add constraint PK_LS_JE_EXCHANGE
                                primary key (DIST_LINE_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index LS_JEEXCH_DISTTYPE
                            on LS_JE_EXCHANGE (DISTRIBUTION_TYPE_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_JE_LSDIST_EXTACCT
                            on LS_JE_EXCHANGE (EXTERNAL_ACCOUNT_CODE)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_JE_LSDIST_EXTDEPT
                            on LS_JE_EXCHANGE (EXTERNAL_DEPARTMENT_CODE)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_JE_EXT_COSTELEMENT
                            on LS_JE_EXCHANGE (EXTERNAL_COST_ELEMENT)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_JE_EXT_ASSETLOC
                            on LS_JE_EXCHANGE (EXT_ASSET_LOCATION)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_JEEXCH_CO
                            on LS_JE_EXCHANGE (GL_COMPANY_NO)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_JEEXCH_LEASE
                            on LS_JE_EXCHANGE (LEASE_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_JEEXCH_CO_MONTH
                            on LS_JE_EXCHANGE (GL_COMPANY_NO, GL_POSTING_MONTHNUM)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_JE_METHOD.');
      execute immediate 'create table LS_JE_METHOD
                         (
                          JE_METHOD_ID     number(22,0) not null,
                          TIME_STAMP       date,
                          USER_ID          varchar2(18),
                          DESCRIPTION      varchar2(35) not null,
                          LONG_DESCRIPTION varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_JE_METHOD
                            add constraint PK_LS_JE_METHOD
                                primary key (JE_METHOD_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_JE_ROUNDING.');
      execute immediate 'create table LS_JE_ROUNDING
                         (
                          LS_ASSET_ID       number(22,0) not null,
                          LEASE_CALC_RUN_ID number(22,0) not null,
                          TARGET_AMOUNT     number(22,2),
                          ALLOC_AMOUNT      number(22,2),
                          DELTA             number(22,2),
                          DIST_LINE_ID      number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_JE_ROUNDING
                            add constraint PK_LS_JE_ROUNDING
                                primary key (LS_ASSET_ID, LEASE_CALC_RUN_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSJE_ROUND_ASSET
                            on LS_JE_ROUNDING (LS_ASSET_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LSJE_ROUND_DISTLINE
                            on LS_JE_ROUNDING (DIST_LINE_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_JE_ROUNDING2.');
      execute immediate 'create table LS_JE_ROUNDING2
                         (
                          COMPANY_ID   number(22,0) not null,
                          LEASE_ID     number(22,0) not null,
                          DIFF         number(22,2),
                          DELTA        number(22,2),
                          DIST_LINE_ID number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_JE_ROUNDING2
                            add constraint PK_LS_JE_ROUNDING2
                                primary key (COMPANY_ID, LEASE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_APPROVAL.');
      execute immediate 'create table LS_LEASE_APPROVAL
                         (
                          LEASE_APPROVAL_ID number(22,0) not null,
                          TIME_STAMP        date,
                          USER_ID           varchar2(18),
                          DESCRIPTION       varchar2(35) not null,
                          LONG_DESCRIPTION  varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_APPROVAL
                            add constraint PK_LS_LEASE_APPROVAL
                                primary key (LEASE_APPROVAL_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_CALC_BALANCES.');
      execute immediate 'create table LS_LEASE_CALC_BALANCES
                         (
                          LS_ASSET_ID           number(22,0) not null,
                          LS_ASSET_ACTIVITY_ID  number(22,0) not null,
                          LEASE_CALC_RUN_ID     number(22,0),
                          PREV_END_CAP_COST     number(22,2),
                          CALC_BEG_CAP_COST     number(22,2),
                          PREV_END_OBLIG        number(22,2),
                          CALC_BEG_OBLIG        number(22,2),
                          PREV_END_NONCURR      number(22,2),
                          CALC_BEG_NONCURR      number(22,2),
                          PREV_END_REG_AMORT    number(22,2),
                          CALC_BEG_REG_AMORT    number(22,2),
                          PREV_END_DEPR_RESERVE number(22,2),
                          CALC_END_DEPR_RESERVE number(22,2),
                          TIME_STAMP            date,
                          USER_ID               varchar2(18)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_CALC_BALANCES
                            add constraint PK_LS_LEASE_CALC_BALANCES
                                primary key (LS_ASSET_ID, LS_ASSET_ACTIVITY_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LEASE_CALC_BAL_RUNID
                            on LS_LEASE_CALC_BALANCES (LEASE_CALC_RUN_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_CALC_ROLE.');
      execute immediate 'create table LS_LEASE_CALC_ROLE
                         (
                          LEASE_CALC_ROLE_ID number(22,0) not null,
                          TIME_STAMP         date,
                          USER_ID            varchar2(18),
                          DESCRIPTION        varchar2(35),
                          LONG_DESCRIPTION   varchar2(254)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_CALC_ROLE
                            add constraint PK_LS_LEASE_CALC_ROLE
                                primary key (LEASE_CALC_ROLE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_CALC_ROLE_USER.');
      execute immediate 'create table LS_LEASE_CALC_ROLE_USER
                         (
                          USERS              varchar2(18) not null,
                          LEASE_CALC_ROLE_ID number(22,0) not null,
                          TIME_STAMP         date,
                          USER_ID            varchar2(18)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_CALC_ROLE_USER
                            add constraint PK_LS_LEASE_CALC_ROLE_USER
                                primary key (USERS, LEASE_CALC_ROLE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_CALC_STAGING.');
      execute immediate 'create table LS_LEASE_CALC_STAGING
                         (
                          LEASE_EXPENSE_ID number(22,0) not null,
                          USER_ID          varchar2(18),
                          SESSION_ID       number(22,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_CALC_STAGING
                            add constraint PK_LS_LEASE_CALC_STAGING
                                primary key (LEASE_EXPENSE_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSCALC_STAGING
                            on LS_LEASE_CALC_STAGING (USER_ID, SESSION_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_CALC_STATUS.');
      execute immediate 'create table LS_LEASE_CALC_STATUS
                         (
                          LEASE_CALC_STATUS_ID number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          DESCRIPTION          varchar2(35),
                          LONG_DESCRIPTION     varchar2(254)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_CALC_STATUS
                            add constraint PK_LS_LEASE_CALC_STATUS
                                primary key (LEASE_CALC_STATUS_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_CALC_TYPE.');
      execute immediate 'create table LS_LEASE_CALC_TYPE
                         (
                          LEASE_CALC_TYPE_ID number(22,0) not null,
                          TIME_STAMP         date,
                          USER_ID            varchar2(18),
                          DESCRIPTION        varchar2(35),
                          LONG_DESCRIPTION   varchar2(254)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_CALC_TYPE
                            add constraint PK_LS_LEASE_CALC_TYPE
                                primary key (LEASE_CALC_TYPE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_CALC_USER_APPROVER.');
      execute immediate 'create table LS_LEASE_CALC_USER_APPROVER
                         (
                          SUBMIT_USER_ID  varchar2(18) not null,
                          APPROVE_USER_ID varchar2(18) not null,
                          TIME_STAMP      date,
                          USER_ID         varchar2(18)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_CALC_USER_APPROVER
                            add constraint PK_LS_LEASE_CALC_USER_APPROVER
                                primary key (SUBMIT_USER_ID, APPROVE_USER_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_COMPANY.');
      execute immediate 'create table LS_LEASE_COMPANY
                         (
                          LEASE_ID       number(22,0) not null,
                          COMPANY_ID     number(22,0) not null,
                          TIME_STAMP     date,
                          USER_ID        varchar2(18),
                          DEPOSIT_AMOUNT number(22,2) not null,
                          MAX_LEASE_LINE number(22,2) not null
                         )';

      execute immediate 'alter table LS_LEASE_COMPANY
                            add constraint FK_LEASECO_COMPANY
                                foreign key (COMPANY_ID)
                                references COMPANY_SETUP (COMPANY_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_COMPANY
                            add constraint PK_LS_LEASE_COMPANY
                                primary key (LEASE_ID, COMPANY_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_DISPOSITION_CODE.');
      execute immediate 'create table LS_LEASE_DISPOSITION_CODE
                         (
                          LEASE_ID         number(22,0) not null,
                          DISPOSITION_CODE number(22,0) not null,
                          TIME_STAMP       date,
                          USER_ID          varchar2(18)
                         )';

      execute immediate 'alter table LS_LEASE_DISPOSITION_CODE
                            add constraint FK_LEASE_DISPCODE_DISPCODE
                                foreign key (DISPOSITION_CODE)
                                references DISPOSITION_CODE (DISPOSITION_CODE)';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_DISPOSITION_CODE
                            add constraint PK_LS_LEASE_DISPOSITION_CODE
                                primary key (LEASE_ID, DISPOSITION_CODE)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_GROUP.');
      execute immediate 'create table LS_LEASE_GROUP
                         (
                          LEASE_GROUP_ID   number(22,0) not null,
                          TIME_STAMP       date,
                          USER_ID          varchar2(18),
                          DESCRIPTION      varchar2(35),
                          LOND_DESCRIPTION varchar2(254)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_GROUP
                            add constraint PK_LS_LEASE_GROUP
                                primary key (LEASE_GROUP_ID)
                                using index tablespace PWRPLANT_IDX';

      -- Index
      execute immediate 'create unique index IX_LS_LEASE_GROUP
                            on LS_LEASE_GROUP (DESCRIPTION)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_RULE.');
      execute immediate 'create table LS_LEASE_RULE
                         (
                          LEASE_RULE_ID            number(22,0) not null,
                          TIME_STAMP               date,
                          USER_ID                  varchar2(18),
                          DESCRIPTION              varchar2(35) not null,
                          LONG_DESCRIPTION         varchar2(254),
                          RENT_HOLIDAY_SW          number(1,0),
                          CURR_MO_INT_OVERRIDE_SW  number(1,0),
                          PERIOD_OVERRIDE          number(22,0),
                          RESET_INTEREST_SW        number(1,0),
                          RESET_EXTENDED_RENTAL_SW number(1,0),
                          RESET_PRETAX_PAYMENT_SW  number(1,0),
                          ALLOW_NEG_PRETAX_SW      number(1,0),
                          INTEREST_ROUND_SW        number(1,0),
                          INTEREST_ROUND_POSITION  number(22,0),
                          PRINCIPLE_ROUND_SW       number(22,0),
                          PRINCIPLE_ROUND          number(22,0),
                          FACTOR_ROUND_SW          number(1,0),
                          FACTOR_ROUND             number(22,0),
                          USE_AGE_SW               number(1,0)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_RULE
                            add constraint PK_LS_LEASE_RULE
                                primary key (LEASE_RULE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_STATUS.');
      execute immediate 'create table LS_LEASE_STATUS
                         (
                          LEASE_STATUS_ID  number(22,0) not null,
                          TIME_STAMP       date,
                          USER_ID          varchar2(18),
                          DESCRIPTION      varchar2(35) not null,
                          LONG_DESCRIPTION varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_STATUS
                            add constraint PK_LS_LEASE_STATUS
                                primary key (LEASE_STATUS_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_TYPE.');
      execute immediate 'create table LS_LEASE_TYPE
                         (
                          LEASE_TYPE_ID    number(22,0) not null,
                          TIME_STAMP       date,
                          USER_ID          varchar2(18),
                          DESCRIPTION      varchar2(35) not null,
                          LONG_DESCRIPTION varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_TYPE
                            add constraint PK_LS_LEASE_TYPE
                                primary key (LEASE_TYPE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LESSOR.');
      execute immediate 'create table LS_LESSOR
                         (
                          LESSOR_ID          number(22,0) not null,
                          TIME_STAMP         date,
                          USER_ID            varchar2(18),
                          DESCRIPTION        varchar2(35) not null,
                          LONG_DESCRIPTION   varchar2(254) not null,
                          ADDRESS1           varchar2(35),
                          ADDRESS2           varchar2(35),
                          ADDRESS3           varchar2(35),
                          ADDRESS4           varchar2(35),
                          ZIP                number(5,0) not null,
                          POSTAL             number(4,0),
                          CITY               varchar2(35),
                          COUNTY_ID          char(18) not null,
                          STATE_ID           char(18) not null,
                          COUNTRY_ID         char(18) not null,
                          PHONE_NUMBER       varchar2(18),
                          EXTENSION          varchar2(8),
                          FAX_NUMBER         varchar2(18),
                          SITE_CODE          varchar2(50),
                          EXTERNAL_VENDOR_ID varchar2(35)
                         )';

      execute immediate 'alter table LS_LESSOR
                            add constraint FK_LESSOR_COUNTRY
                                foreign key (COUNTRY_ID)
                                references COUNTRY (COUNTRY_ID)';

      execute immediate 'alter table LS_LESSOR
                            add constraint FK_LESSOR_COUNTY_STATE
                                foreign key (COUNTY_ID, STATE_ID)
                                 references COUNTY (COUNTY_ID, STATE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LESSOR
                            add constraint PK_LS_LESSOR
                                primary key (LESSOR_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LESSOR_INVOICE_STATUS.');
      execute immediate 'create table LS_LESSOR_INVOICE_STATUS
                         (
                          STATUS_ID        number(22,0) not null,
                          TIME_STAMP       date,
                          USER_ID          varchar2(35),
                          DESCRIPTION      varchar2(35),
                          LONG_DESCRIPTION varchar2(255)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LESSOR_INVOICE_STATUS
                            add constraint PK_LS_LESSOR_INVOICE_STATUS
                                primary key (STATUS_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LOCAL_TAX_DEFAULT.');
      execute immediate 'create table LS_LOCAL_TAX_DEFAULT
                         (
                          LOCAL_TAX_DEFAULT_ID number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          DESCRIPTION          varchar2(35) not null,
                          LONG_DESCRIPTION     varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LOCAL_TAX_DEFAULT
                            add constraint PK_LS_LOCAL_TAX_DEFAULT
                                primary key (LOCAL_TAX_DEFAULT_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LOCAL_TAX_DISTRICT.');
      execute immediate 'create table LS_LOCAL_TAX_DISTRICT
                         (
                          LOCAL_TAX_DISTRICT_ID number(22,0) not null,
                          TIME_STAMP            date,
                          USER_ID               varchar2(18),
                          DESCRIPTION           varchar2(35) not null,
                          LONG_DESCRIPTION      varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_LOCAL_TAX_DISTRICT
                            add constraint PK_LS_LOCAL_TAX_DISTRICT
                                primary key (LOCAL_TAX_DISTRICT_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_MASS_CHANGE_CONTROL.');
      execute immediate 'create table LS_MASS_CHANGE_CONTROL
                         (
                          MASS_CHANGE_ID        number(22,0) not null,
                          TIME_STAMP            date,
                          USER_ID               varchar2(18),
                          DESCRIPTION           varchar2(35),
                          LONG_DESCRIPTION      varchar2(254),
                          SQL_STATEMENT         clob,
                          COMPLETION_TIME_STAMP date
                         )';

      --PK Constraint

      execute immediate 'alter table LS_MASS_CHANGE_CONTROL
                            add constraint PK_LS_MASS_CHANGE_CONTROL
                                primary key (MASS_CHANGE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_PAYMENT_FREQ.');
      execute immediate 'create table LS_PAYMENT_FREQ
                         (
                          PAYMENT_FREQ_ID  number(22,0) not null,
                          TIME_STAMP       date,
                          USER_ID          varchar2(18),
                          DESCRIPTION      varchar2(35) not null,
                          LONG_DESCRIPTION varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_PAYMENT_FREQ
                            add constraint PK_LS_PAYMENT_FREQ
                                primary key (PAYMENT_FREQ_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_PAYMENT_TERM_TYPE.');
      execute immediate 'create table LS_PAYMENT_TERM_TYPE
                         (
                          PAYMENT_TERM_TYPE_ID number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          DESCRIPTION          varchar2(35) not null,
                          LONG_DESCRIPTION     varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_PAYMENT_TERM_TYPE
                            add constraint PK_LS_PAYMENT_TERM_TYPE
                                primary key (PAYMENT_TERM_TYPE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_POST_CPR_FIND.');
      execute immediate 'create table LS_POST_CPR_FIND
                         (
                          ASSET_ID   number(22,0) not null,
                          MASTER_ID  varchar2(254),
                          MASTER_ID2 varchar2(254),
                          TIME_STAMP date,
                          USER_ID    varchar2(18)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_POST_CPR_FIND
                            add constraint PK_LS_POST_CPR_FIND
                                primary key (ASSET_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSCPRFIND_MASTER
                            on LS_POST_CPR_FIND (MASTER_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LSCPRFIND_MASTER2
                            on LS_POST_CPR_FIND (MASTER_ID2)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_PRE_API_ASSET.');
      execute immediate 'create table LS_PRE_API_ASSET
                         (
                          ID                            number(22,0) not null,
                          LEASE_NUMBER                  varchar2(35),
                          LEASE_ID                      varchar2(35),
                          LESSOR_NAME                   varchar2(35),
                          CUT_OFF_DAY                   number(22,0),
                          PYMT_DUE_DATE                 number(22,0),
                          DAYS_IN_BASIS                 number(22,0),
                          LEASE_DESCRIPTION             varchar2(35),
                          LEASE_LONG_DESCRIPTION        varchar2(254),
                          BOOK_CLASSIFICATION           varchar2(35),
                          TAX_CLASSIFICATION            varchar2(35),
                          LEASE_TYPE                    varchar2(35),
                          IN_SERVICE_DATE               varchar2(35),
                          PRE_PAY_SW                    varchar2(35),
                          BARGAIN_PURCHASE_OPTION       varchar2(35),
                          RENEWAL_OPTION                varchar2(35),
                          NUMBER_OF_RENEWALS            varchar2(35),
                          RENEWAL_NOTIFICATION_DAYS     varchar2(35),
                          EXTENDED_RENTAL_OPTION        varchar2(35),
                          EXTENDED_RENTAL_AMOUNT_OR_PCT varchar2(35),
                          CANCELABLE_SW                 varchar2(35),
                          TERMINATION_PENALTY_AMOUNT    varchar2(35),
                          LEASE_RULE_ID                 varchar2(35),
                          LEASE                         varchar2(35),
                          VENDOR_ID                     varchar2(35),
                          SITE_CODE                     varchar2(35),
                          IMPORT_TEMPLATE               varchar2(35)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_PRE_API_ASSET
                            add constraint PK_LS_PRE_API_ASSET
                                primary key (ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_PRE_API_ASSET_ATTRIBUTES.');
      execute immediate 'create table LS_PRE_API_ASSET_ATTRIBUTES
                         (
                          ID                            number(22,0) not null,
                          LEASED_ASSET_NUMBER           varchar2(35),
                          ILR_NUMBER                    varchar2(35),
                          IN_SERVICE_DATE               varchar2(35),
                          LEASED_ASSET_DESCRIPTION      varchar2(50),
                          LEASED_ASSET_LONG_DESCRIPTION varchar2(254),
                          BUSINESS_SEGMENT              varchar2(35),
                          WORK_ORDER_NUMBER             varchar2(35),
                          EXTERNAL_ASSET_LOCATION       varchar2(35),
                          ASSET_LOCATION_ID             varchar2(35),
                          ASSET_LOCATION_DESCRIPTION    varchar2(35),
                          CPR_PROPERTY_GROUP            varchar2(35),
                          CPR_RETIREMENT_UNIT           varchar2(75),
                          CPR_FERC_UTILITY_ACCOUNT      number(22,2),
                          FAIR_MARKET_VALUE             number(22,2),
                          ESTIMATED_RESIDUAL_AMOUNT     number(22,2),
                          COMPONENT_ID                  number(22,0),
                          COMPONENT_DESCRIPTION         varchar2(50),
                          ITEM_NUMBER                   varchar2(35),
                          VEHICLE_ASSIGNMENT            varchar2(100),
                          IMPORT_TEMPLATE               varchar2(35)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_PRE_API_ASSET_ATTRIBUTES
                            add constraint PK_LS_PRE_API_ASSET_ATTRIBUTES
                                primary key (ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_PRE_API_CODE_BLOCK.');
      execute immediate 'create table LS_PRE_API_CODE_BLOCK
                         (
                          ID                        number(22,0) not null,
                          LEASED_ASSET_NUMBER       varchar2(35),
                          EXPENSE_DISTRIBUTION_TYPE varchar2(35),
                          BUSINESS_SEGMENT          number(22,0),
                          COMPANY                   varchar2(35),
                          EXPENSE_ACCOUNT           varchar2(35),
                          OFFSET_ACCOUNT            varchar2(35),
                          ELEMENT_1                 varchar2(35),
                          ELEMENT_2                 varchar2(35),
                          ELEMENT_3                 varchar2(35),
                          ELEMENT_4                 varchar2(35),
                          ELEMENT_5                 varchar2(35),
                          ELEMENT_6                 varchar2(35),
                          ELEMENT_7                 varchar2(35),
                          ELEMENT_8                 varchar2(35),
                          ELEMENT_9                 varchar2(35),
                          ELEMENT_10                varchar2(35),
                          DISTRIB_PCT               varchar2(35),
                          LINE_DESCRIPTION          varchar2(100),
                          DIST_DEF_COUNTER          number(22,0),
                          IMPORT_TEMPLATE           varchar2(35)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_PRE_API_CODE_BLOCK
                            add constraint PK_LS_PRE_API_CODE_BLOCK
                                primary key (ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_PRE_API_ILR.');
      execute immediate 'create table LS_PRE_API_ILR
                         (
                          ID                   number(22,0) not null,
                          ILR_NUMBER           varchar2(35),
                          COMPANY_ID           varchar2(35),
                          LEASE_NUMBER         varchar2(35),
                          ILR_DESCRIPTION      varchar2(35),
                          ILR_LONG_DESCRIPTION varchar2(100),
                          BORROWING_RATE       varchar2(35),
                          IMPORT_TEMPLATE      varchar2(35)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_PRE_API_ILR
                            add constraint PK_LS_PRE_API_ILR
                                primary key (ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_PRE_API_LESSOR.');
      execute immediate 'create table LS_PRE_API_LESSOR
                         (
                          ID                        number(22,0) not null,
                          LESSOR_NAME               varchar2(35),
                          LESSOR_LONG_DESCRIPTION   varchar2(100),
                          ADDRESS_1                 varchar2(35),
                          ADDRESS_2                 varchar2(35),
                          ADDRESS_3                 varchar2(35),
                          ADDRESS_4                 varchar2(35),
                          ZIP_CODE                  varchar2(35),
                          POSTAL                    varchar2(35),
                          CITY                      varchar2(35),
                          COUNTY_ID                 varchar2(35),
                          STATE_ID                  varchar2(35),
                          COUNTRY_ID                varchar2(35),
                          PHONE_NUMBER              varchar2(35),
                          EXTENSION                 varchar2(35),
                          FAX_NUMBER                varchar2(35),
                          EXTERNAL_VENDOR_ID        varchar2(35),
                          EXTERNAL_VENDOR_SITE_CODE varchar2(35),
                          ROW_LESSOR                varchar2(35),
                          IMPORT_TEMPLATE           varchar2(35)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_PRE_API_LESSOR
                            add constraint PK_LS_PRE_API_LESSOR
                                primary key (ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_PRE_API_PAYMENT_INFO.');
      execute immediate 'create table LS_PRE_API_PAYMENT_INFO
                         (
                          ID                         number(22,0) not null,
                          ILR_NUMBER                 varchar2(35),
                          ILR_DESCRIPTION            varchar2(100),
                          FIXED_LEASE_PAYMENT_AMOUNT varchar2(35),
                          CONTINGENT_RENTAL          varchar2(35),
                          NUMBER_OF_PAYMENTS         varchar2(35),
                          PAYMENT_FREQUENCY          varchar2(35),
                          PYMT_START_DATE            varchar2(35),
                          INCEPTION_ANNUAL_INTEREST  varchar2(35),
                          EXECUTORY_COST             varchar2(35),
                          PYMT_TERM_ID               number(22,0),
                          PYMT_TERM_TYPE             varchar2(35),
                          ADDITIONS_ADJUSTMENTS      varchar2(35),
                          UPFRONT_SALES_TAX          varchar2(35),
                          IMPORT_TEMPLATE            varchar2(35)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_PRE_API_PAYMENT_INFO
                            add constraint PK_LS_PRE_API_PAYMENT_INFO
                                primary key (ID) using index
                                tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_PROCESS_CONTROL.');
      execute immediate 'create table LS_PROCESS_CONTROL
                         (
                          COMPANY_ID       number(22,0) not null,
                          GL_POSTING_MO_YR date not null,
                          TIME_STAMP       date,
                          USER_ID          varchar2(18),
                          LAM_CLOSED       date,
                          PROCESS_1        date,
                          PROCESS_2        date,
                          PROCESS_3        date,
                          PROCESS_4        date,
                          PROCESS_5        date,
                          PROCESS_6        date,
                          PROCESS_7        date,
                          PROCESS_8        date,
                          PROCESS_9        date,
                          PROCESS_10       date
                         )';

      execute immediate 'alter table LS_PROCESS_CONTROL
                            add constraint FK_PROCESSCONTROL_COMPANY
                                foreign key (COMPANY_ID)
                                references COMPANY_SETUP (COMPANY_ID)';

      --PK Constraint

      execute immediate 'alter table LS_PROCESS_CONTROL
                            add constraint PK_LS_PROCESS_CONTROL
                                primary key (COMPANY_ID, GL_POSTING_MO_YR)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_PURCHASE_OPTION_TYPE.');
      execute immediate 'create table LS_PURCHASE_OPTION_TYPE
                         (
                          PURCHASE_OPTION_TYPE_ID number(22,0) not null,
                          TIME_STAMP              date,
                          USER_ID                 varchar2(18),
                          DESCRIPTION             varchar2(35) not null,
                          LONG_DESCRIPTION        varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_PURCHASE_OPTION_TYPE
                            add constraint PK_LS_PURCHASE_OPTION_TYPE
                                primary key (PURCHASE_OPTION_TYPE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_RENEWAL_OPTION_TYPE.');
      execute immediate 'create table LS_RENEWAL_OPTION_TYPE
                         (
                          RENEWAL_OPTION_TYPE_ID number(22,0) not null,
                          TIME_STAMP             date,
                          USER_ID                varchar2(18),
                          DESCRIPTION            varchar2(35) not null,
                          LONG_DESCRIPTION       varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_RENEWAL_OPTION_TYPE
                            add constraint PK_LS_RENEWAL_OPTION_TYPE
                                primary key (RENEWAL_OPTION_TYPE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_RETIREMENT_STATUS.');
      execute immediate 'create table LS_RETIREMENT_STATUS
                         (
                          RETIREMENT_STATUS_ID number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          DESCRIPTION          varchar2(35)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_RETIREMENT_STATUS
                            add constraint PK_LS_RETIREMENT_STATUS
                                primary key (RETIREMENT_STATUS_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_SOB_TYPE.');
      execute immediate 'create table LS_SOB_TYPE
                         (
                          SOB_TYPE_ID number(22,0) not null,
                          TIME_STAMP  date,
                          USER_ID     varchar2(35),
                          DESCRIPTION varchar2(35)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_SOB_TYPE
                            add constraint PK_LS_SOB_TYPE
                                primary key (SOB_TYPE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_SUMMARY_LOCAL_TAX.');
      execute immediate 'create table LS_SUMMARY_LOCAL_TAX
                         (
                          SUMMARY_LOCAL_TAX_ID number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          DESCRIPTION          varchar2(35) not null,
                          LONG_DESCRIPTION     varchar2(254) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_SUMMARY_LOCAL_TAX
                            add constraint PK_LS_SUMMARY_LOCAL_TAX
                                primary key (SUMMARY_LOCAL_TAX_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_TEMP_ASSET.');
      execute immediate 'create table LS_TEMP_ASSET
                         (
                          USER_ID         varchar2(18) not null,
                          SESSION_ID      number(22,0),
                          LS_ASSET_ID     number(22,0) not null,
                          BATCH_REPORT_ID number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_TEMP_ASSET
                            add constraint PK_LS_TEMP_ASSET
                                primary key (USER_ID, SESSION_ID, LS_ASSET_ID, BATCH_REPORT_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_TEMP_COMPONENT.');
      execute immediate 'create table LS_TEMP_COMPONENT
                         (
                          USER_ID         varchar2(18) not null,
                          SESSION_ID      number(22,0) not null,
                          LS_ASSET_ID     number(22,0) not null,
                          LS_COMPONENT_ID number(22,0) not null,
                          BATCH_REPORT_ID number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_TEMP_COMPONENT
                            add constraint PK_LS_TEMP_COMPONENT
                                primary key (USER_ID, SESSION_ID, LS_ASSET_ID, LS_COMPONENT_ID, BATCH_REPORT_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_TEMP_ILR.');
      execute immediate 'create table LS_TEMP_ILR
                         (
                          USER_ID         varchar2(18) not null,
                          SESSION_ID      number(22,0) not null,
                          ILR_ID          number(22,0) not null,
                          BATCH_REPORT_ID number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_TEMP_ILR
                            add constraint PK_LS_TEMP_ILR
                                primary key (USER_ID, SESSION_ID, ILR_ID, BATCH_REPORT_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_TEMP_LEASE.');
      execute immediate 'create table LS_TEMP_LEASE
                         (
                          USER_ID         varchar2(18) not null,
                          SESSION_ID      number(22,0) not null,
                          LEASE_ID        number(22,0) not null,
                          BATCH_REPORT_ID number(22,0) not null
                         )';

      --PK Constraint

      execute immediate 'alter table LS_TEMP_LEASE
                            add constraint PK_LS_TEMP_LEASE
                                primary key (USER_ID, SESSION_ID, LEASE_ID, BATCH_REPORT_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_TEMPLEASE_LEASEID
                            on LS_TEMP_LEASE (LEASE_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_VALID_COMPANY_PO.');
      execute immediate 'create table LS_VALID_COMPANY_PO
                         (
                          COMPANY_ID number(22,0) not null,
                          PO_ID      varchar2(35) not null,
                          TIME_STAMP date,
                          USER_ID    varchar2(18)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_VALID_COMPANY_PO
                            add constraint PK_LS_VALID_COMPANY_PO
                                primary key (COMPANY_ID, PO_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_VALID_FUNDING_PROJECTS.');
      execute immediate 'create table LS_VALID_FUNDING_PROJECTS
                         (
                          WORK_ORDER_ID number(22,0) not null,
                          TIME_STAMP    date,
                          USER_ID       varchar2(35)
                         )';

      execute immediate 'alter table LS_VALID_FUNDING_PROJECTS
                            add constraint FK_LS_VALID_FUND_PROJ
                                foreign key (WORK_ORDER_ID)
                                references WORK_ORDER_CONTROL (WORK_ORDER_ID)';

      --PK Constraint

      execute immediate 'alter table LS_VALID_FUNDING_PROJECTS
                            add constraint PK_LS_VALID_FUNDING_PROJECTS
                                primary key (WORK_ORDER_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_VOUCHER_STATUS.');
      execute immediate 'create table LS_VOUCHER_STATUS
                         (
                          VOUCHER_STATUS_ID number(22,0) not null,
                          TIME_STAMP        date,
                          USER_ID           nvarchar2(18),
                          DESCRIPTION       nvarchar2(35),
                          LONG_DESCRIPTION  nvarchar2(254)
                         )';

      --PK Constraint

      execute immediate 'alter table LS_VOUCHER_STATUS
                            add constraint PK_LS_VOUCHER_STATUS
                                primary key (VOUCHER_STATUS_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index
      execute immediate 'create unique index IX_LS_VOUCHER_STATUS_DESC
                            on LS_VOUCHER_STATUS (DESCRIPTION)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ACCOUNTING_DEFAULTS.');
      execute immediate 'create table LS_ACCOUNTING_DEFAULTS
                         (
                          ACCOUNTING_DEFAULT_ID        number(22,0) not null,
                          TIME_STAMP                   date,
                          USER_ID                      varchar2(18),
                          COMPANY_ID                   number(22,0) not null,
                          GL_ACCOUNT_ID                number(22,0),
                          BUS_SEGMENT_ID               number(22,0),
                          FUNC_CLASS_ID                number(22,0),
                          JE_METHOD_ID                 number(22,0),
                          LESSOR_PAYABLE_ACCT          number(22,0),
                          CURRENT_LEASE_OBLIG_ACCT     number(22,0),
                          NON_CURRENT_LEASE_OBLIG_ACCT number(22,0),
                          INTEREST_EXPENSE_ACCT        number(22,0),
                          LOCAL_TAX_1_EXP              number(22,0),
                          LOCAL_TAX_1_PAYABLE          number(22,0),
                          LOCAL_TAX_2_EXP              number(22,0),
                          LOCAL_TAX_2_PAYABLE          number(22,0),
                          LOCAL_TAX_3_EXP              number(22,0),
                          LOCAL_TAX_3_PAYABLE          number(22,0),
                          LOCAL_TAX_4_EXP              number(22,0),
                          LOCAL_TAX_4_PAYABLE          number(22,0),
                          LOCAL_TAX_5_EXP              number(22,0),
                          LOCAL_TAX_5_PAYABLE          number(22,0),
                          LOCAL_TAX_6_EXP              number(22,0),
                          LOCAL_TAX_6_PAYABLE          number(22,0),
                          LOCAL_TAX_7_EXP              number(22,0),
                          LOCAL_TAX_7_PAYABLE          number(22,0),
                          LOCAL_TAX_8_EXP              number(22,0),
                          LOCAL_TAX_8_PAYABLE          number(22,0),
                          LOCAL_TAX_9_EXP              number(22,0),
                          LOCAL_TAX_9_PAYABLE          number(22,0),
                          LOCAL_TAX_10_EXP             number(22,0),
                          LOCAL_TAX_10_PAYABLE         number(22,0),
                          STATUS_CODE_ID               number(22,0),
                          INTEREST_ACCRUAL_ACCT        number(22,0),
                          DESCRIPTION                  varchar2(35)
                         )';

      execute immediate 'alter table LS_ACCOUNTING_DEFAULTS
                            add constraint FK_LSACCTDEFAULT_COID
                                foreign key (COMPANY_ID)
                                references COMPANY_SETUP (COMPANY_ID)';

      execute immediate 'alter table LS_ACCOUNTING_DEFAULTS
                            add constraint R_LS_ACCOUNTING_DEFAULTS1
                                foreign key (CURRENT_LEASE_OBLIG_ACCT)
                                references LS_DIST_GL_ACCOUNT (DIST_GL_ACCOUNT_ID)';

      execute immediate 'alter table LS_ACCOUNTING_DEFAULTS
                            add constraint R_LS_ACCOUNTING_DEFAULTS2
                                foreign key (LESSOR_PAYABLE_ACCT)
                                references LS_DIST_GL_ACCOUNT (DIST_GL_ACCOUNT_ID)';

      execute immediate 'alter table LS_ACCOUNTING_DEFAULTS
                            add constraint R_LS_ACCOUNTING_DEFAULTS3
                                foreign key (LOCAL_TAX_1_PAYABLE)
                                references LS_DIST_GL_ACCOUNT (DIST_GL_ACCOUNT_ID)';

      execute immediate 'alter table LS_ACCOUNTING_DEFAULTS
                            add constraint R_LS_ACCOUNTING_DEFAULTS4
                                foreign key (LOCAL_TAX_2_PAYABLE)
                                references LS_DIST_GL_ACCOUNT (DIST_GL_ACCOUNT_ID)';

      execute immediate 'alter table LS_ACCOUNTING_DEFAULTS
                            add constraint R_LS_ACCOUNTING_DEFAULTS5
                                foreign key (LOCAL_TAX_3_PAYABLE)
                                references LS_DIST_GL_ACCOUNT (DIST_GL_ACCOUNT_ID)';

      execute immediate 'alter table LS_ACCOUNTING_DEFAULTS
                            add constraint R_LS_ACCOUNTING_DEFAULTS6
                                foreign key (JE_METHOD_ID)
                                references LS_JE_METHOD (JE_METHOD_ID)';

      execute immediate 'alter table LS_ACCOUNTING_DEFAULTS
                            add constraint R_LS_ACCOUNTING_DEFAULTS7
                                foreign key (GL_ACCOUNT_ID)
                                references GL_ACCOUNT (GL_ACCOUNT_ID)';

      execute immediate 'alter table LS_ACCOUNTING_DEFAULTS
                            add constraint R_LS_ACCOUNTING_DEFAULTS8
                                foreign key (BUS_SEGMENT_ID)
                                references BUSINESS_SEGMENT (BUS_SEGMENT_ID)';

      execute immediate 'alter table LS_ACCOUNTING_DEFAULTS
                            add constraint R_LS_ACCOUNTING_DEFAULTS9
                                foreign key (LOCAL_TAX_4_PAYABLE)
                                references LS_DIST_GL_ACCOUNT (DIST_GL_ACCOUNT_ID)';

      --PK Constraint

      execute immediate 'alter table LS_ACCOUNTING_DEFAULTS
                            add constraint PK_LS_ACCOUNTING_DEFAULTS
                                primary key (ACCOUNTING_DEFAULT_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LS_ACCT_DEFAULT_JE
                            on LS_ACCOUNTING_DEFAULTS (JE_METHOD_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_FLEX_CONTROL.');
      execute immediate 'create table LS_ASSET_FLEX_CONTROL
                         (
                          ASSET_FLEX_ID    number(22,0) not null,
                          TIME_STAMP       date,
                          USER_ID          varchar2(18),
                          COLUMN_USAGE_ID  number(22,0),
                          ASSET_FLEX_LABEL varchar2(35),
                          DDDW_NAME        varchar2(35)
                         )';

      execute immediate 'alter table LS_ASSET_FLEX_CONTROL
                           add constraint R_LS_ASSET_FLEX_CONTROL1
                                foreign key (COLUMN_USAGE_ID)
                                references LS_COLUMN_USAGE (COLUMN_USAGE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_ASSET_FLEX_CONTROL
                            add constraint PK_LS_ASSET_FLEX_CONTROL
                                primary key (ASSET_FLEX_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_DIST_DEF.');
      execute immediate 'create table LS_DIST_DEF
                         (
                          LS_ASSET_ID        number(22,0) not null,
                          DIST_DEF_TYPE_ID   number(22,0) not null,
                          EFFDT              date not null,
                          DIST_DEF_SEQ       number(22,0) not null,
                          DIST_DEF_COUNTER   number(22,0) not null,
                          TIME_STAMP         date,
                          USER_ID            varchar2(18),
                          MASS_CHANGE_ID     number(22,0),
                          COMPANY_ID         number(22,0),
                          DIVISION_ID        number(22,0),
                          DEPARTMENT_ID      number(22,0),
                          DIST_GL_ACCOUNT_ID number(22,0),
                          WORK_ORDER_NUMBER  varchar2(35),
                          COST_ELEMENT_ID    number(22,0),
                          ASSET_LOCATION_ID  number(22,0),
                          STATE_ID           char(18),
                          ELEMENT_1          varchar2(35),
                          ELEMENT_2          varchar2(35),
                          ELEMENT_3          varchar2(35),
                          ELEMENT_4          varchar2(35),
                          ELEMENT_5          varchar2(35),
                          ELEMENT_6          varchar2(35),
                          ELEMENT_7          varchar2(35),
                          ELEMENT_8          varchar2(35),
                          ELEMENT_9          varchar2(35),
                          ELEMENT_10         varchar2(35),
                          DISTRIB_PCT        number(22,8),
                          GL_ERROR_MESSAGE   varchar2(512)
                         )';

      execute immediate 'alter table LS_DIST_DEF
                            add constraint FK_DISTDEF_COID
                                foreign key (COMPANY_ID)
                                references COMPANY_SETUP (COMPANY_ID)';

      execute immediate 'alter table LS_DIST_DEF
                            add constraint FK_DISTDEF_DIVID
                                foreign key (DIVISION_ID)
                                references DIVISION (DIVISION_ID)';

      execute immediate 'alter table LS_DIST_DEF
                            add constraint R_LS_DIST_DEF1
                                foreign key (DEPARTMENT_ID)
                                references LS_DIST_DEPARTMENT (DEPARTMENT_ID)';

      execute immediate 'alter table LS_DIST_DEF
                            add constraint R_LS_DIST_DEF2
                                foreign key (DIST_DEF_TYPE_ID)
                                references LS_DIST_DEF_TYPE (DIST_DEF_TYPE_ID)';

      execute immediate 'alter table LS_DIST_DEF
                            add constraint R_LS_DIST_DEF3
                                foreign key (MASS_CHANGE_ID)
                                references LS_MASS_CHANGE_CONTROL (MASS_CHANGE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_DIST_DEF
                            add constraint PK_LS_DIST_DEF
                                primary key (LS_ASSET_ID, DIST_DEF_TYPE_ID, EFFDT, DIST_DEF_SEQ, DIST_DEF_COUNTER)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_DISTDEF_COMPANYDEPT
                            on LS_DIST_DEF (COMPANY_ID, DEPARTMENT_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_DIST_DEF_CONTROL.');
      execute immediate 'create table LS_DIST_DEF_CONTROL
                         (
                          ELEMENT_ID      number(22,0) not null,
                          TIME_STAMP      date,
                          USER_ID         varchar2(18),
                          COLUMN_USAGE_ID number(22,0),
                          ELEMENT_LABEL   varchar2(35),
                          COMPANY_ID      number(22,0) not null
                         )';

      execute immediate 'alter table LS_DIST_DEF_CONTROL
                            add constraint FK_DISTDEF_COL_USAGE
                                foreign key (COLUMN_USAGE_ID)
                                references LS_COLUMN_USAGE (COLUMN_USAGE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_DIST_DEF_CONTROL
                            add constraint PK_LS_DIST_DEF_CONTROL
                                primary key (ELEMENT_ID, COMPANY_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ILR.');
      execute immediate 'create table LS_ILR
                         (
                          ILR_ID                      number(22,0) not null,
                          TIME_STAMP                  date,
                          USER_ID                     varchar2(18),
                          ILR_NUMBER                  varchar2(35) not null,
                          LEASE_ID                    number(22,0) not null,
                          COMPANY_ID                  number(22,0) not null,
                          INCEPTION_AIR               number(22,8) not null,
                          EST_IN_SVC_DATE             date not null,
                          TOTAL_LEASE_TERMS           number(22,0),
                          FINAL_EOT_DATE              date not null,
                          INTERIM_INTEREST_BEGIN_DATE date,
                          MODIFY_SCHEDULE_SW          number(1,0),
                          EXTERNAL_ILR                varchar2(35),
                          ILR_STATUS_ID               number(22,0) not null,
                          NPV                         number(22,2),
                          IRR                         number(22,8),
                          CURRENT_LEASE_COST          number(22,2),
                          ILR_GROUP_ID                number(22,0),
                          NPV2                        number(22,2),
                          AIR2                        number(22,8)
                         )';

      execute immediate 'alter table LS_ILR
                            add constraint FK_ILR_LEASE
                                foreign key (LEASE_ID, COMPANY_ID)
                                references LS_LEASE_COMPANY (LEASE_ID, COMPANY_ID)';

      execute immediate 'alter table LS_ILR
                            add constraint R_LS_ILR1
                                foreign key (ILR_STATUS_ID)
                                references LS_ILR_STATUS (ILR_STATUS_ID)';

      --PK Constraint

      execute immediate 'alter table LS_ILR
                            add constraint PK_LS_ILR
                                primary key (ILR_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSILR_LEASEID
                            on LS_ILR (LEASE_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_ILR_COLEASE
                            on LS_ILR (COMPANY_ID, LEASE_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_JE_DIST_DEF.');
      execute immediate 'create table LS_JE_DIST_DEF
                         (
                          COMPANY_ID          number(22,0) not null,
                          GL_POSTING_MONTHNUM number(22,0) not null,
                          LS_ASSET_ID         number(22,0) not null,
                          EFFDT_NUMBER        number(22,0) not null,
                          DIST_DEF_TYPE_ID    number(22,0) not null,
                          JE_METHOD_ID        number(22,0) not null,
                          TIME_STAMP          date,
                          USER_ID             varchar2(18)
                         )';

      execute immediate 'alter table LS_JE_DIST_DEF
                            add constraint FK1_LS_JE_DIST_DEF
                                foreign key (DIST_DEF_TYPE_ID)
                                references LS_DIST_DEF_TYPE (DIST_DEF_TYPE_ID)';

      execute immediate 'alter table LS_JE_DIST_DEF
                            add constraint FK2_LS_JE_DIST_DEF
                                foreign key (JE_METHOD_ID)
                                references LS_JE_METHOD (JE_METHOD_ID)';

      --PK Constraint

      execute immediate 'alter table LS_JE_DIST_DEF
                            add constraint PK_LS_JE_DIST_DEF
                                primary key (COMPANY_ID, GL_POSTING_MONTHNUM, LS_ASSET_ID, EFFDT_NUMBER, DIST_DEF_TYPE_ID, JE_METHOD_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index LS_JE_DIST_DEF_MIKE1
                            on LS_JE_DIST_DEF (LS_ASSET_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index LS_JE_DIST_DEF_MIKE2
                            on LS_JE_DIST_DEF (EFFDT_NUMBER)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index LS_JE_DIST_DEF_MIKE3
                            on LS_JE_DIST_DEF (LS_ASSET_ID, EFFDT_NUMBER)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE.');
      execute immediate 'create table LS_LEASE
                         (
                          LEASE_ID                    number(22,0) not null,
                          TIME_STAMP                  date,
                          USER_ID                     varchar2(18),
                          LEASE_NUMBER                varchar2(35) not null,
                          DESCRIPTION                 varchar2(35) not null,
                          LONG_DESCRIPTION            varchar2(254) not null,
                          LESSOR_ID                   number(22,0) not null,
                          LEASE_TYPE_ID               number(22,0) not null,
                          LEASE_STATUS_ID             number(22,0) not null,
                          LEASE_RULE_ID               number(22,0) not null,
                          LEASE_APPROVAL_ID           number(22,0),
                          APPROVAL_DATE               date,
                          MASTER_AGREEMENT_DATE       date,
                          RIDER_AGREEMENT_DATE        date,
                          NOTIFICATION_DATE           date,
                          CUT_OFF_DAY                 number(22,0) not null,
                          PAYMENT_DUE_DAY             number(22,0) not null,
                          DAYS_IN_BASIS_YEAR          number(22,0) not null,
                          EXTENDED_RENTAL_TYPE_ID     number(22,0) not null,
                          EXTENDED_RENTAL_AMT         number(22,2),
                          EXTENDED_RENTAL_PCT         number(22,8),
                          PURCHASE_OPTION_TYPE_ID     number(22,0) not null,
                          PURCHASE_OPTION_NOTC        number(22,0),
                          PURCHASE_OPTION_AMT         number(22,2),
                          PURCHASE_OPTION_PCT         number(22,8),
                          RENEWAL_OPTION_TYPE_ID      number(22,0) not null,
                          RENEWAL_OPTION_NOTIFICATION number(22,0),
                          CANCELABLE_TYPE_ID          number(22,0) not null,
                          CANCEL_NOTIFICATION         number(22,0),
                          CANCEL_TERM                 varchar2(15),
                          LIMIT_ORIG_COST             number(22,2),
                          LIMIT_UNPAID_BAL            number(22,2),
                          ESTIMATED_PAYMENT           number(22,2),
                          ITC_SW                      number(1,0),
                          PARTIAL_RETIRE_SW           number(1,0),
                          PRE_PAYMENT_SW              number(1,0) not null,
                          SUBLET_SW                   number(1,0),
                          CAPITAL_YN_SW               number(1,0) not null,
                          MUNI_BO_SW                  number(1,0) not null,
                          CALC_APPROVAL_RULE_ID       number(22,0) not null,
                          MONTHS_FOR_LOCAL_TAX_BASE   number(22,0),
                          TAX_CAPITAL_YN_SW           number(1,0) default 0 not null,
                          IFRS_CAPITAL_YN_SW          number(1,0) not null,
                          PREV_MONTH_PAY_YN_SW        number(1,0) not null
                         )';

      execute immediate 'alter table LS_LEASE
                            add constraint FK_LEASE_GAAP_CAPITAL
                                foreign key (CAPITAL_YN_SW)
                                references YES_NO (YES_NO_ID)';

      execute immediate 'alter table LS_LEASE
                            add constraint FK_LEASE_TAX_CAPITAL
                                foreign key (TAX_CAPITAL_YN_SW)
                                references YES_NO (YES_NO_ID)';

      execute immediate 'alter table LS_LEASE
                            add constraint FK_LEASE_IFRS_CAPITAL
                                foreign key (IFRS_CAPITAL_YN_SW)
                                references YES_NO (YES_NO_ID)';

      execute immediate 'alter table LS_LEASE
                            add constraint FK_CANCELABLE_TYP
                                foreign key (CANCELABLE_TYPE_ID)
                                references LS_CANCELABLE_TYPE (CANCELABLE_TYPE_ID)';

      execute immediate 'alter table LS_LEASE
                            add constraint FK_EXT_RNT_TYP
                                foreign key (EXTENDED_RENTAL_TYPE_ID)
                                references LS_EXTENDED_RENTAL_TYPE (EXTENDED_RENTAL_TYPE_ID)';

      execute immediate 'alter table LS_LEASE
                            add constraint FK_LEASE_APPROVAL
                                foreign key (LEASE_APPROVAL_ID)
                                references LS_LEASE_APPROVAL (LEASE_APPROVAL_ID)';

      execute immediate 'alter table LS_LEASE
                            add constraint FK_LEASE_LESSOR
                                foreign key (LESSOR_ID)
                                references LS_LESSOR (LESSOR_ID)';

      execute immediate 'alter table LS_LEASE
                            add constraint FK_LEASE_RULE
                                foreign key (LEASE_RULE_ID)
                                references LS_LEASE_RULE (LEASE_RULE_ID)';

      execute immediate 'alter table LS_LEASE
                            add constraint FK_LEASE_STATUS
                                foreign key (LEASE_STATUS_ID)
                                references LS_LEASE_STATUS (LEASE_STATUS_ID)';

      execute immediate 'alter table LS_LEASE
                            add constraint FK_LEASE_TYPE
                                foreign key (LEASE_TYPE_ID)
                                references LS_LEASE_TYPE (LEASE_TYPE_ID)';

      execute immediate 'alter table LS_LEASE
                            add constraint FK_PURCH_OPT_TYP
                                foreign key (PURCHASE_OPTION_TYPE_ID)
                                references LS_PURCHASE_OPTION_TYPE (PURCHASE_OPTION_TYPE_ID)';

      execute immediate 'alter table LS_LEASE
                            add constraint FK_RENWAL_OPT_TYP
                                foreign key (RENEWAL_OPTION_TYPE_ID)
                                references LS_RENEWAL_OPTION_TYPE (RENEWAL_OPTION_TYPE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LEASE
                            add constraint PK_LS_LEASE
                                primary key (LEASE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_CALC_CONTROL.');
      execute immediate 'create table LS_LEASE_CALC_CONTROL
                         (
                          LEASE_CALC_RUN_ID     number(22,0) not null,
                          TIME_STAMP            date,
                          USER_ID               varchar2(18),
                          LEASE_CALC_TYPE_ID    number(22,0),
                          LEASE_CALC_STATUS_ID  number(22,0),
                          LEASE_CALC_MO_YR      date,
                          LEASE_CALC_MONTHNUM   number(22,0),
                          LEASE_CALC_START_TIME date,
                          LEASE_CALC_END_TIME   date
                         )';

      execute immediate 'alter table LS_LEASE_CALC_CONTROL
                            add constraint FK_LSCALCCNTRL_STATUSID
                                foreign key (LEASE_CALC_STATUS_ID)
                                references LS_LEASE_CALC_STATUS (LEASE_CALC_STATUS_ID)';

      execute immediate 'alter table LS_LEASE_CALC_CONTROL
                            add constraint FK_LSCALCCNTRL_TYPEID
                                foreign key (LEASE_CALC_TYPE_ID)
                                references LS_LEASE_CALC_TYPE (LEASE_CALC_TYPE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_CALC_CONTROL
                            add constraint PK_LS_LEASE_CALC_CONTROL
                                primary key (LEASE_CALC_RUN_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_CALCCNTRL_MONTH
                            on LS_LEASE_CALC_CONTROL (LEASE_CALC_MONTHNUM)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_EXPENSE.');
      execute immediate 'create table LS_LEASE_EXPENSE
                         (
                          LEASE_EXPENSE_ID         number(22,0) not null,
                          TIME_STAMP               date,
                          USER_ID                  varchar2(18),
                          LEASE_ID                 number(22,0) not null,
                          COMPANY_ID               number(22,0) not null,
                          LEASE_CALC_RUN_ID        number(22,0) not null,
                          GL_POSTING_MO_YR         date not null,
                          GL_POSTING_MONTHNUM      number(22,0) not null,
                          GL_POSTING_TIMESTAMP     date,
                          GL_POSTING_USERID        varchar2(18),
                          PAYMENT_START_DATE       date,
                          PAYMENT_END_DATE         date,
                          LEASE_PYMT_INTEREST_RATE number(22,8),
                          INVOICE_AMOUNT           number(22,2),
                          INVOICE_PRINCIPAL        number(22,2),
                          INVOICE_INTEREST         number(22,2),
                          INVOICE_UNPAID_BALANCE   number(22,2),
                          INVOICE_SALES_TAX        number(22,2),
                          CALC_AMOUNT              number(22,2),
                          CALC_PRINCIPAL           number(22,2),
                          CALC_INTEREST            number(22,2),
                          CALC_UNPAID_BALANCE      number(22,2),
                          CALC_SALES_TAX           number(22,2),
                          SUBMIT_USER_ID           varchar2(18),
                          SUBMIT_TIME_STAMP        date,
                          APPROVE_USER_ID          varchar2(18),
                          APPROVE_TIME_STAMP       date,
                          REVERSAL_ID              number(22,0),
                          ADJUST_AMOUNT            number(22,2) default 0,
                          ADJUST_PRINCIPAL         number(22,2) default 0,
                          ADJUST_INTEREST          number(22,2) default 0,
                          ADJUST_UNPAID_BALANCE    number(22,2) default 0,
                          ADJUST_SALES_TAX         number(22,2) default 0,
                          CALC_CONTINGENT          number(22,2),
                          CALC_EXECUTORY           number(22,2),
                          ADJUST_CONTINGENT        number(22,2),
                          ADJUST_EXECUTORY         number(22,2),
                          INVOICE_ID               number(22,0)
                         )';

      execute immediate 'alter table LS_LEASE_EXPENSE
                            add constraint FK_LEASECALCEXP_LSCOID
                                foreign key (LEASE_ID, COMPANY_ID)
                                references LS_LEASE_COMPANY (LEASE_ID, COMPANY_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_EXPENSE
                            add constraint PK_LS_LEASE_EXPENSE
                                primary key (LEASE_EXPENSE_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSEXPENSE_COLEASE
                            on LS_LEASE_EXPENSE (COMPANY_ID, LEASE_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LSLEASEEXP_CALCRUNID
                            on LS_LEASE_EXPENSE (LEASE_CALC_RUN_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_TYPE_TOLERANCE.');
      execute immediate 'create table LS_LEASE_TYPE_TOLERANCE
                         (
                          LEASE_TYPE_ID        number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          PRINCIPAL_VAR_PCT    number(22,8),
                          PRINCIPAL_VAR_LIMIT  number(22,2),
                          INTEREST_VAR_PCT     number(22,8),
                          INTEREST_VAR_LIMIT   number(22,2),
                          UNPAID_BAL_VAR_PCT   number(22,8),
                          UNPAID_BAL_VAR_LIMIT number(22,2),
                          SALES_TAX_VAR_PCT    number(22,8),
                          SALES_TAX_VAR_LIMIT  number(22,2),
                          TOTAL_VAR_PCT        number(22,8),
                          TOTAL_VAR_LIMIT      number(22,2)
                         )';

      execute immediate 'alter table LS_LEASE_TYPE_TOLERANCE
                            add constraint FK_LEASETYPE_TOL_LEASETYPE
                                foreign key (LEASE_TYPE_ID)
                                references LS_LEASE_TYPE (LEASE_TYPE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_TYPE_TOLERANCE
                            add constraint PK_LS_LEASE_TYPE_TOLERANCE
                                primary key (LEASE_TYPE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LESSOR_INVOICE.');
      execute immediate 'create table LS_LESSOR_INVOICE
                         (
                          INVOICE_ID              number(22,0) not null,
                          TIME_STAMP              date,
                          USER_ID                 varchar2(35),
                          LESSOR_ID               number(22,0) not null,
                          GL_POSTING_MONTH        number(22,0) not null,
                          STATUS                  number(22,0) not null,
                          INVOICE_NUMBER          varchar2(35),
                          INVOICE_DATE            date,
                          INVOICE_DESCRIPTION     varchar2(35),
                          INVOICE_NOTES           varchar2(256),
                          AP_SEND_DATE            date,
                          DOC_ID                  varchar2(35),
                          NUM_PAGES               number(22,0),
                          PAYMENT_MTHD            varchar2(35),
                          PAID_DATE               date,
                          VOUCHER_ID              number(22,0),
                          INVOICE_AMOUNT          number(22,2),
                          INVOICE_PRINCIPAL       number(22,2),
                          INVOICE_INTEREST        number(22,2),
                          INVOICE_SALES_TAX       number(22,2),
                          INVOICE_USE_TAX         number(22,2),
                          INVOICE_EXECUTORY       number(22,2),
                          INVOICE_CONTINGENT      number(22,2),
                          INVOICE_EXTENDED_RENTAL number(22,2),
                          CALC_AMOUNT             number(22,2),
                          CALC_PRINCIPAL          number(22,2),
                          CALC_INTEREST           number(22,2),
                          CALC_SALES_TAX          number(22,2),
                          CALC_USE_TAX            number(22,2),
                          CALC_EXECUTORY          number(22,2),
                          CALC_CONTINGENT         number(22,2),
                          CALC_EXTENDED_RENTAL    number(22,2),
                          APPROVED_SW             number(1,0)
                         )';

      execute immediate 'alter table LS_LESSOR_INVOICE
                            add constraint FK_LESSOR_INVOICE_STATUS
                                foreign key (STATUS)
                                references LS_LESSOR_INVOICE_STATUS (STATUS_ID)';

      execute immediate 'alter table LS_LESSOR_INVOICE
                            add constraint FK_LS_LESSOR_INVOICE_LESSOR_ID
                                foreign key (LESSOR_ID)
                                references LS_LESSOR (LESSOR_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LESSOR_INVOICE
                            add constraint PK_LS_LESSOR_INVOICE
                                primary key (INVOICE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LESSOR_STATE.');
      execute immediate 'create table LS_LESSOR_STATE
                         (
                          LESSOR_ID  number(22,0) not null,
                          STATE_ID   char(18) not null,
                          TIME_STAMP date,
                          USER_ID    varchar2(18)
                         )';

      execute immediate 'alter table LS_LESSOR_STATE
                            add constraint FK_LESSORSTATE_LEASE_ID
                                foreign key (LESSOR_ID)
                                references LS_LESSOR (LESSOR_ID)';

      execute immediate 'alter table LS_LESSOR_STATE
                            add constraint FK_LESSORSTATE_STATE
                                foreign key (STATE_ID)
                                references STATE (STATE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LESSOR_STATE
                            add constraint PK_LS_LESSOR_STATE
                                primary key (LESSOR_ID, STATE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LOCAL_TAX.');
      execute immediate 'create table LS_LOCAL_TAX
                         (
                          LOCAL_TAX_ID         number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          DESCRIPTION          varchar2(35) not null,
                          LONG_DESCRIPTION     varchar2(254) not null,
                          SUMMARY_LOCAL_TAX_ID number(22,0) not null,
                          LESSOR_TAX_YN_SW     number(22,0)
                         )';

      execute immediate 'alter table LS_LOCAL_TAX
                            add constraint R_LS_LOCAL_TAX1
                                foreign key (SUMMARY_LOCAL_TAX_ID)
                                references LS_SUMMARY_LOCAL_TAX (SUMMARY_LOCAL_TAX_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LOCAL_TAX
                            add constraint PK_LS_LOCAL_TAX
                                primary key (LOCAL_TAX_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LOCAL_TAX_RATES.');
      execute immediate 'create table LS_LOCAL_TAX_RATES
                         (
                          LOCAL_TAX_DISTRICT_ID number(22,0) not null,
                          SUMMARY_LOCAL_TAX_ID  number(22,0) not null,
                          EFFDT                 date not null,
                          TIME_STAMP            date,
                          USER_ID               varchar2(18),
                          RATE                  number(22,8)
                         )';

      execute immediate 'alter table LS_LOCAL_TAX_RATES
                            add constraint FK_LOCAL_TAX_RATES_SUMMLTID
                                foreign key (SUMMARY_LOCAL_TAX_ID)
                                references LS_SUMMARY_LOCAL_TAX (SUMMARY_LOCAL_TAX_ID)';

      execute immediate 'alter table LS_LOCAL_TAX_RATES
                            add constraint FK_LOCAL_TAX_RATES_DISTRI
                                foreign key (LOCAL_TAX_DISTRICT_ID)
                                references LS_LOCAL_TAX_DISTRICT (LOCAL_TAX_DISTRICT_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LOCAL_TAX_RATES
                            add constraint PK_LS_LOCAL_TAX_RATES
                                primary key (LOCAL_TAX_DISTRICT_ID, SUMMARY_LOCAL_TAX_ID, EFFDT)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_RECURRING_VOUCHER.');
      execute immediate 'create table LS_RECURRING_VOUCHER
                         (
                          VOUCHER_ID              number(22,0) not null,
                          TIME_STAMP              date,
                          USER_ID                 varchar2(35),
                          LESSOR_ID               number(22,0) not null,
                          BEGIN_MONTH             number(22,0),
                          END_MONTH               number(22,0),
                          VOUCHER_DESCRIPTION     varchar2(35),
                          VOUCHER_NOTES           varchar2(256),
                          PAYMENT_MTHD            varchar2(35),
                          NUM_PAGES               number(22,0),
                          VOUCHER_AMOUNT          number(22,2),
                          VOUCHER_PRINCIPAL       number(22,2),
                          VOUCHER_INTEREST        number(22,2),
                          VOUCHER_SALES_TAX       number(22,2),
                          VOUCHER_USE_TAX         number(22,2),
                          VOUCHER_EXECUTORY       number(22,2),
                          VOUCHER_CONTINGENT      number(22,2),
                          VOUCHER_EXTENDED_RENTAL number(22,2)
                         )';

      execute immediate 'alter table LS_RECURRING_VOUCHER
                            add constraint FK_LS_RECURRING_VOUCHER_LESSOR
                                foreign key (LESSOR_ID)
                                references LS_LESSOR (LESSOR_ID)';

      --PK Constraint

      execute immediate 'alter table LS_RECURRING_VOUCHER
                            add constraint PK_LS_RECURRING_VOUCHER
                                primary key (VOUCHER_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_SET_OF_BOOKS.');
      execute immediate 'create table LS_SET_OF_BOOKS
                         (
                          SET_OF_BOOKS_ID number(22,0) not null,
                          TIME_STAMP      date,
                          USER_ID         varchar2(35),
                          SOB_TYPE_ID     number(22,0) not null
                         )';

      execute immediate 'alter table LS_SET_OF_BOOKS
                            add constraint FK_LS_SOB_SOB_ID
                                foreign key (SET_OF_BOOKS_ID)
                                references SET_OF_BOOKS (SET_OF_BOOKS_ID)';

      execute immediate 'alter table LS_SET_OF_BOOKS
                            add constraint FK_LS_SOB_SOB_TYPE_ID
                                foreign key (SOB_TYPE_ID)
                                references LS_SOB_TYPE (SOB_TYPE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_SET_OF_BOOKS
                            add constraint PK_LS_SET_OF_BOOKS
                                primary key (SET_OF_BOOKS_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_STATE_LOCAL_TAX_RATES.');
      execute immediate 'create table LS_STATE_LOCAL_TAX_RATES
                         (
                          STATE_ID             char(18) not null,
                          SUMMARY_LOCAL_TAX_ID number(22,0) not null,
                          EFFDT                date not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          RATE                 number(22,8)
                         )';

      execute immediate 'alter table LS_STATE_LOCAL_TAX_RATES
                            add constraint FK_STATE_LOCAL_TAX_LTID
                                foreign key (SUMMARY_LOCAL_TAX_ID)
                                references LS_SUMMARY_LOCAL_TAX (SUMMARY_LOCAL_TAX_ID)';

      execute immediate 'alter table LS_STATE_LOCAL_TAX_RATES
                            add constraint FK_STATE_LOCAL_TAX_STID
                                foreign key (STATE_ID)
                                references STATE (STATE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_STATE_LOCAL_TAX_RATES
                            add constraint PK_LS_STATE_LOCAL_TAX_RATES
                                primary key (STATE_ID, SUMMARY_LOCAL_TAX_ID, EFFDT)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_VOUCHER.');
      execute immediate 'create table LS_VOUCHER
                         (
                          VOUCHER_ID           number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              nvarchar2(18),
                          VOUCHER_NUMBER       nvarchar2(35),
                          DESCRIPTION          nvarchar2(35),
                          LONG_DESCRIPTION     nvarchar2(254),
                          GL_MONTH_NUMBER      number(22,0),
                          VENDOR_ID            number(22,0),
                          VOUCHER_STATUS_ID    number(22,0),
                          SCHEDULED_PAY_DATE   date,
                          PAYMENT_RELEASE_DATE date,
                          PAYMENT_RELEASER     nvarchar2(18),
                          AP_REFERENCE_NUMBER  nvarchar2(35),
                          AP_SEND_DATE         date,
                          PAID_DATE            date,
                          CHECK_NUMBER         nvarchar2(35),
                          SHC                  nvarchar2(2000),
                          VOUCHER_AMOUNT       number(22,3),
                          VOUCHER_PRINCIPAL    number(22,3),
                          VOUCHER_INTEREST     number(22,3),
                          VOUCHER_USE_TAX      number(22,3),
                          VOUCHER_EXECUTORY    number(22,3),
                          VOUCHER_CONTINGENT   number(22,3),
                          FLEX_1               nvarchar2(35),
                          FLEX_2               nvarchar2(35),
                          FLEX_3               nvarchar2(35),
                          FLEX_4               nvarchar2(35),
                          FLEX_5               nvarchar2(35),
                          FLEX_6               nvarchar2(35),
                          FLEX_7               nvarchar2(35),
                          FLEX_8               nvarchar2(35),
                          FLEX_9               nvarchar2(254),
                          FLEX_10              nvarchar2(2000)
                         )';

      execute immediate 'alter table LS_VOUCHER
                            add constraint FK_LS_VOUCHER_2
                                foreign key (VOUCHER_STATUS_ID)
                                references LS_VOUCHER_STATUS (VOUCHER_STATUS_ID)';

      --PK Constraint

      execute immediate 'alter table LS_VOUCHER
                            add constraint PK_LS_VOUCHER
                                primary key (VOUCHER_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET.');
      execute immediate 'create table LS_ASSET
                         (
                          LS_ASSET_ID                 number(22,0) not null,
                          TIME_STAMP                  date,
                          USER_ID                     varchar2(18),
                          LEASED_ASSET_NUMBER         varchar2(35) not null,
                          ASSET_ID                    number(22,0),
                          ILR_ID                      number(22,0),
                          LS_ASSET_STATUS_ID          number(22,0) not null,
                          DESCRIPTION                 varchar2(35) not null,
                          LONG_DESCRIPTION            varchar2(254) not null,
                          TAG_NUMBER                  varchar2(35),
                          EXTERNAL_SYS                varchar2(35),
                          EXTERNAL_SYS_ID             varchar2(35),
                          CONVERSION_ID               varchar2(35),
                          CURRENT_LEASE_DATE          date,
                          CURRENT_LEASE_COST          number(22,2) default 0,
                          PROPERTY_TAX_MO_YR          number(22,0),
                          PROPERTY_TAX_COST           number(22,2) default 0,
                          INTERIM_INTEREST_BEGIN_DATE date,
                          GL_POSTING_BEGIN_DATE       date,
                          RETIREMENT_DATE             date,
                          TERMINATION_PENALTY_AMOUNT  number(22,2),
                          SALE_PROCEED_AMOUNT         number(22,2),
                          EXPECTED_LIFE               number(22,0),
                          RESIDUAL_AMOUNT             number(22,2),
                          ACCOUNTING_DEFAULT_ID       number(22,0),
                          ECONOMIC_LIFE               number(22,0)
                         )';

      execute immediate 'alter table LS_ASSET
                            add constraint FK_ASSET_ACCTDEFAULT
                                foreign key (ACCOUNTING_DEFAULT_ID)
                                references LS_ACCOUNTING_DEFAULTS (ACCOUNTING_DEFAULT_ID)';

      execute immediate 'alter table LS_ASSET
                            add constraint FK_ASSET_CPR_ASSET
                                foreign key (ASSET_ID)
                                references CPR_LEDGER (ASSET_ID)';

      execute immediate 'alter table LS_ASSET
                            add constraint FK_ASSET_ILR
                                foreign key (ILR_ID)
                                references LS_ILR (ILR_ID)';

      execute immediate 'alter table LS_ASSET
                            add constraint R_LS_ASSET1
                                foreign key (LS_ASSET_STATUS_ID)
                                references LS_ASSET_STATUS (LS_ASSET_STATUS_ID)';

      --PK Constraint

      execute immediate 'alter table LS_ASSET
                            add constraint PK_LS_ASSET
                                primary key (LS_ASSET_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSASSET_ACCTDEFAULTS
                            on LS_ASSET (ACCOUNTING_DEFAULT_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LSASSET_CONV_BUASSET
                            on LS_ASSET (DESCRIPTION, LEASED_ASSET_NUMBER)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LSASSET_ILRID
                            on LS_ASSET (ILR_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_ACTIVITY_LT.');
      execute immediate 'create table LS_ASSET_ACTIVITY_LT
                         (
                          LS_ASSET_ID          number(22,0) not null,
                          LS_ASSET_ACTIVITY_ID number(22,0) not null,
                          LOCAL_TAX_ID         number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          ACCRUAL              number(22,2),
                          AMOUNT               number(22,2),
                          ADJUSTMENT           number(22,2)
                         )';

      execute immediate 'alter table LS_ASSET_ACTIVITY_LT
                            add constraint R_LS_ASSET_ACTIVITY_LT1
                                foreign key (LOCAL_TAX_ID)
                                references LS_LOCAL_TAX (LOCAL_TAX_ID)';

      --PK Constraint

      execute immediate 'alter table LS_ASSET_ACTIVITY_LT
                            add constraint PK_LS_ASSET_ACTIVITY_LT
                                primary key (LS_ASSET_ID, LS_ASSET_ACTIVITY_ID, LOCAL_TAX_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_ASSETACT_LT_RESET
                            on LS_ASSET_ACTIVITY_LT (LS_ASSET_ID, LS_ASSET_ACTIVITY_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LSASSETACTLT_LOCTAXID
                            on LS_ASSET_ACTIVITY_LT (LOCAL_TAX_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LSASSETACTLT_ASSETID
                            on LS_ASSET_ACTIVITY_LT (LS_ASSET_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ILR_PAYMENT_TERM.');
      execute immediate 'create table LS_ILR_PAYMENT_TERM
                         (
                          ILR_ID               number(22,0) not null,
                          PAYMENT_TERM_ID      number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          PAYMENT_TERM_TYPE_ID number(22,0) not null,
                          PAYMENT_TERM_DATE    date not null,
                          PAYMENT_FREQ_ID      number(22,0) not null,
                          NUMBER_OF_TERMS      number(22,0),
                          EST_EXECUTORY_COST   number(22,2),
                          PAID_AMOUNT          number(22,2),
                          SNAPSHOT_PAID_AMOUNT number(22,2),
                          CONTINGENT_AMOUNT    number(22,2) default 0
                         )';

      execute immediate 'alter table LS_ILR_PAYMENT_TERM
                            add constraint FK_PYMTTERMS_ILR
                                foreign key (ILR_ID)
                                references LS_ILR (ILR_ID)';

      execute immediate 'alter table LS_ILR_PAYMENT_TERM
                            add constraint R_LS_ILR_PAYMENT_TERM1
                                foreign key (PAYMENT_TERM_TYPE_ID)
                                references LS_PAYMENT_TERM_TYPE (PAYMENT_TERM_TYPE_ID)';

      execute immediate 'alter table LS_ILR_PAYMENT_TERM
                            add constraint R_LS_ILR_PAYMENT_TERM2
                                foreign key (PAYMENT_FREQ_ID)
                                references LS_PAYMENT_FREQ (PAYMENT_FREQ_ID)';

      --PK Constraint

      execute immediate 'alter table LS_ILR_PAYMENT_TERM
                            add constraint PK_LS_ILR_PAYMENT_TERM
                                primary key (ILR_ID, PAYMENT_TERM_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_AIR.');
      execute immediate 'create table LS_LEASE_AIR
                         (
                          LEASE_ID   number(22,0) not null,
                          EFFDT      date not null,
                          TIME_STAMP date,
                          USER_ID    varchar2(18),
                          LEASE_AIR  number(22,8)
                         )';

      execute immediate 'alter table LS_LEASE_AIR
                            add constraint FK_LEASEAIR_LEASE
                                foreign key (LEASE_ID)
                                references LS_LEASE (LEASE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_AIR
                            add constraint PK_LS_LEASE_AIR
                                primary key (LEASE_ID, EFFDT)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_INVOICE.');
      execute immediate 'create table LS_LEASE_INVOICE
                         (
                          INVOICE_ID number(22,0) not null,
                          LEASE_ID   number(22,0) not null,
                          COMPANY_ID number(22,0) not null,
                          TIME_STAMP date,
                          USER_ID    varchar2(35),
                          LESSOR_ID  number(22,0)
                         )';

      execute immediate 'alter table LS_LEASE_INVOICE
                            add constraint FK_LS_LEASE_INVOICE_INVOICE_ID
                                foreign key (INVOICE_ID)
                                references LS_LESSOR_INVOICE (INVOICE_ID)';

      execute immediate 'alter table LS_LEASE_INVOICE
                             add constraint FK_LS_LEASE_INVOICE_LEASE_ID
                                 foreign key (LEASE_ID, COMPANY_ID)
                                 references LS_LEASE_COMPANY (LEASE_ID, COMPANY_ID)';

      execute immediate 'alter table LS_LEASE_INVOICE
                            add constraint FK_LS_LEASE_INVOICE_LESSOR_ID
                                foreign key (LESSOR_ID)
                                references LS_LESSOR (LESSOR_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_INVOICE
                            add constraint PK_LS_LEASE_INVOICE
                                primary key (INVOICE_ID, LEASE_ID, COMPANY_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_PAYMENT_TERM.');
      execute immediate 'create table LS_LEASE_PAYMENT_TERM
                         (
                          LEASE_ID          number(22,0) not null,
                          PAYMENT_TERM_ID   number(22,0) not null,
                          TIME_STAMP        date,
                          USER_ID           varchar2(18),
                          PAYMENT_TERM_DATE date,
                          PAYMENT_FREQ_ID   number(22,0),
                          NUMBER_OF_TERMS   number(22,0)
                         )';

      execute immediate 'alter table LS_LEASE_PAYMENT_TERM
                            add constraint FK_LEASEPYMTTERM_LEASE
                                foreign key (LEASE_ID)
                                references LS_LEASE (LEASE_ID)';

      execute immediate 'alter table LS_LEASE_PAYMENT_TERM
                            add constraint FK_PYMT_FREQ
                                foreign key (PAYMENT_FREQ_ID)
                                references LS_PAYMENT_FREQ (PAYMENT_FREQ_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_PAYMENT_TERM
                            add constraint PK_LS_LEASE_PAYMENT_TERM
                                primary key (LEASE_ID, PAYMENT_TERM_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LEASE_RECURRING_VOUCHER.');
      execute immediate 'create table LS_LEASE_RECURRING_VOUCHER
                         (
                          VOUCHER_ID number(22,0) not null,
                          LEASE_ID   number(22,0) not null,
                          COMPANY_ID number(22,0) not null,
                          TIME_STAMP date,
                          USER_ID    varchar2(35),
                          LESSOR_ID  number(22,0)
                         )';

      execute immediate 'alter table LS_LEASE_RECURRING_VOUCHER
                            add constraint FK_LS_LEASE_RECURRING_VOUCHER1
                                foreign key (VOUCHER_ID)
                                references LS_RECURRING_VOUCHER (VOUCHER_ID)';

      execute immediate 'alter table LS_LEASE_RECURRING_VOUCHER
                            add constraint FK_LS_LEASE_RECURRING_VOUCHER2
                                foreign key (LEASE_ID, COMPANY_ID)
                                references LS_LEASE_COMPANY (LEASE_ID, COMPANY_ID)';

      execute immediate 'alter table LS_LEASE_RECURRING_VOUCHER
                            add constraint FK_LS_RECURRING_VOUCHER3
                                foreign key (LESSOR_ID)
                                references LS_LESSOR (LESSOR_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LEASE_RECURRING_VOUCHER
                            add constraint PK_LS_LEASE_RECURRING_VOUCHER
                                primary key (VOUCHER_ID, LEASE_ID, COMPANY_ID)
                             using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LESSOR_INVOICE_LINE.');
      execute immediate 'create table LS_LESSOR_INVOICE_LINE
                         (
                          INVOICE_ID                   number(22,0) not null,
                          INVOICE_LINE_ID              number(22,0) not null,
                          TIME_STAMP                   date,
                          USER_ID                      varchar2(18),
                          INVOICE_LINE_NUMBER          varchar2(35),
                          INVOICE_LINE_DESCRIPTION     varchar2(35),
                          LONG_DESCRIPTION             varchar2(255),
                          LEASE_ID                     number(22,0),
                          ILR_ID                       number(22,0),
                          LS_ASSET_ID                  number(22,0),
                          COMPANY_ID                   number(22,0),
                          DIVISION_ID                  number(22,0),
                          DEPARTMENT_ID                number(22,0),
                          DIST_GL_ACCOUNT_ID           number(22,0),
                          WORK_ORDER_NUMBER            varchar2(35),
                          COST_ELEMENT_ID              number(22,0),
                          ASSET_LOCATION_ID            number(22,0),
                          STATE_ID                     char(18),
                          ELEMENT_1                    varchar2(35),
                          ELEMENT_2                    varchar2(35),
                          ELEMENT_3                    varchar2(35),
                          ELEMENT_4                    varchar2(35),
                          ELEMENT_5                    varchar2(35),
                          ELEMENT_6                    varchar2(35),
                          ELEMENT_7                    varchar2(35),
                          ELEMENT_8                    varchar2(35),
                          ELEMENT_9                    varchar2(35),
                          ELEMENT_10                   varchar2(35),
                          DISTRIB_PCT                  number(22,8),
                          INVOICE_LINE_AMOUNT          number(22,2),
                          INVOICE_LINE_PRINCIPAL       number(22,2),
                          INVOICE_LINE_INTEREST        number(22,2),
                          INVOICE_LINE_SALES_TAX       number(22,2),
                          INVOICE_LINE_USE_TAX         number(22,2),
                          INVOICE_LINE_EXECUTORY       number(22,2),
                          INVOICE_LINE_CONTINGENT      number(22,2),
                          INVOICE_LINE_EXTENDED_RENTAL number(22,2),
                          CALC_AMOUNT                  number(22,2),
                          CALC_PRINCIPAL               number(22,2),
                          CALC_INTEREST                number(22,2),
                          CALC_SALES_TAX               number(22,2),
                          CALC_USE_TAX                 number(22,2),
                          CALC_EXECUTORY               number(22,2),
                          CALC_CONTINGENT              number(22,2),
                          CALC_EXTENDED_RENTAL         number(22,2)
                         )';

      execute immediate 'alter table LS_LESSOR_INVOICE_LINE
                            add constraint FK_LS_LESSSOR_INVIOCE_LINE1
                                foreign key (INVOICE_ID)
                                references LS_LESSOR_INVOICE (INVOICE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LESSOR_INVOICE_LINE
                            add constraint PK_LS_LESSOR_INVOICE_LINE
                                primary key (INVOICE_ID, INVOICE_LINE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LOCAL_TAX_ELIG.');
      execute immediate 'create table LS_LOCAL_TAX_ELIG
                         (
                          COMPANY_ID           number(22,0) not null,
                          STATE_ID             char(18) not null,
                          LOCAL_TAX_ID         number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          LOCAL_TAX_DEFAULT_ID number(22,0)
                         )';

      execute immediate 'alter table LS_LOCAL_TAX_ELIG
                            add constraint FK_LS_LOCTAXELIG_COID
                                foreign key (COMPANY_ID)
                                references COMPANY_SETUP (COMPANY_ID)';

      execute immediate 'alter table LS_LOCAL_TAX_ELIG
                            add constraint R_LS_LOCAL_TAX_ELIG1
                                foreign key (LOCAL_TAX_DEFAULT_ID)
                                references LS_LOCAL_TAX_DEFAULT (LOCAL_TAX_DEFAULT_ID)';

      execute immediate 'alter table LS_LOCAL_TAX_ELIG
                            add constraint R_LS_LOCAL_TAX_ELIG2
                                foreign key (LOCAL_TAX_ID)
                                references LS_LOCAL_TAX (LOCAL_TAX_ID)';

      execute immediate 'alter table LS_LOCAL_TAX_ELIG
                            add constraint R_LS_LOCAL_TAX_ELIG3
                                foreign key (STATE_ID)
                                references STATE (STATE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LOCAL_TAX_ELIG
                            add constraint PK_LS_LOCAL_TAX_ELIG
                                primary key (COMPANY_ID, STATE_ID, LOCAL_TAX_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_STATE_GRANDFATHER_DATE.');
      execute immediate 'create table LS_STATE_GRANDFATHER_DATE
                         (
                          STATE_ID             char(18) not null,
                          LOCAL_TAX_ID         number(22,0) not null,
                          GRANDFATHER_LEVEL_ID number(22,0) not null,
                          GRANDFATHER_DATE     date,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18)
                         )';

      execute immediate 'alter table LS_STATE_GRANDFATHER_DATE
                            add constraint FK_LSSTATEGF_DATE_LEVELID
                                foreign key (GRANDFATHER_LEVEL_ID)
                                references LS_GRANDFATHER_LEVEL (GRANDFATHER_LEVEL_ID)';

      execute immediate 'alter table LS_STATE_GRANDFATHER_DATE
                            add constraint FK_LSSTATEGF_DATE_LOCTAX
                                foreign key (LOCAL_TAX_ID)
                                references LS_LOCAL_TAX (LOCAL_TAX_ID)';

      execute immediate 'alter table LS_STATE_GRANDFATHER_DATE
                            add constraint FK_LSSTATEGF_DATE_STATEID
                                foreign key (STATE_ID)
                                references STATE (STATE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_STATE_GRANDFATHER_DATE
                            add constraint PK_LS_STATE_GRANDFATHER_DATE
                                primary key (STATE_ID, LOCAL_TAX_ID, GRANDFATHER_LEVEL_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_VOUCHER_INVOICE.');
      execute immediate 'create table LS_VOUCHER_INVOICE
                         (
                          VOUCHER_ID number(22,0) not null,
                          INVOICE_ID number(22,0) not null,
                          TIME_STAMP date,
                          USER_ID    nvarchar2(18)
                         )';

      execute immediate 'alter table LS_VOUCHER_INVOICE
                            add constraint FK_LS_VOUCHER_INVOICE_1
                                foreign key (VOUCHER_ID)
                                references LS_VOUCHER (VOUCHER_ID)';

      --PK Constraint

      execute immediate 'alter table LS_VOUCHER_INVOICE
                            add constraint PK_LS_VOUCHER_INVOICE
                                primary key (VOUCHER_ID, INVOICE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_VOUCHER_LINE.');
      execute immediate 'create table LS_VOUCHER_LINE
                         (
                          VOUCHER_ID               number(22,0) not null,
                          VOUCHER_LINE_ID          number(22,0) not null,
                          TIME_STAMP               date,
                          USER_ID                  nvarchar2(18),
                          VOUCHER_LINE_NUMBER      nvarchar2(35),
                          VOUCHER_LINE_DESCRIPTION nvarchar2(35),
                          ILR_ASSET_IND            number(22,0),
                          ILR_ASSET_ID             number(22,0) not null,
                          VOUCHER_LINE_AMOUNT      number(22,3),
                          VOUCHER_LINE_PRINCIPAL   number(22,3),
                          VOUCHER_LINE_INTEREST    number(22,3),
                          VOUCHER_LINE_USE_TAX     number(22,3),
                          VOUCHER_LINE_EXECUTORY   number(22,3),
                          VOUCHER_LINE_CONTINGENT  number(22,3)
                         )';

      execute immediate 'alter table LS_VOUCHER_LINE
                            add constraint FK_LS_VOUCHER_LINE_1
                                foreign key (VOUCHER_ID)
                                references LS_VOUCHER (VOUCHER_ID)';

      --PK Constraint

      execute immediate 'alter table LS_VOUCHER_LINE
                            add constraint PK_LS_VOUCHER_LINE
                                primary key (VOUCHER_ID, VOUCHER_LINE_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_ACTIVITY.');
      execute immediate 'create table LS_ASSET_ACTIVITY
                         (
                          LS_ASSET_ID                  number(22,0) not null,
                          LS_ASSET_ACTIVITY_ID         number(22,0) not null,
                          TIME_STAMP                   date,
                          USER_ID                      varchar2(18),
                          GL_MONTH_NUMBER              number(22,0) not null,
                          LS_ACTIVITY_TYPE_ID          number(22,0) not null,
                          LEASE_CALC_RUN_ID            number(22,0),
                          CURRENT_LEASE_COST           number(22,2),
                          BEG_CAPITALIZED_LEASE_COST   number(22,2),
                          END_CAPITALIZED_LEASE_COST   number(22,2),
                          BEG_LEASE_OBLIGATION         number(22,2),
                          END_LEASE_OBLIGATION         number(22,2),
                          BEG_NONCURR_LEASE_OBLIGATION number(22,2),
                          END_NONCURR_LEASE_OBLIGATION number(22,2),
                          REMAINING_LIFE               number(22,0),
                          DEPR_EXPENSE                 number(22,2),
                          BEG_DEPR_RESERVE             number(22,2),
                          END_DEPR_RESERVE             number(22,2),
                          REGULATORY_AMORT             number(22,2),
                          BEG_REGULATORY_AMORT         number(22,2),
                          END_REGULATORY_AMORT         number(22,2),
                          UNPAID_BALANCE               number(22,2),
                          LEVEL_PAYMENT_EXPENSE        number(22,2),
                          INTEREST_ACCRUAL             number(22,2),
                          PRINCIPAL_ACCRUAL            number(22,2),
                          EXTENDED_RENTAL_ACCRUAL      number(22,2),
                          SALES_TAX_ACCRUAL            number(22,2),
                          USE_TAX_ACCRUAL              number(22,2),
                          INTEREST_AMOUNT              number(22,2),
                          PRINCIPAL_AMOUNT             number(22,2),
                          EXTENDED_RENTAL_AMOUNT       number(22,2),
                          SALES_TAX_AMOUNT             number(22,2),
                          INTEREST_ADJUSTMENT          number(22,2) default 0,
                          PRINCIPAL_ADJUSTMENT         number(22,2) default 0,
                          EXTENDED_RENTAL_ADJUSTMENT   number(22,2) default 0,
                          SALES_TAX_ADJUSTMENT         number(22,2) default 0,
                          USE_TAX_ADJUSTMENT           number(22,2) default 0,
                          EXECUTORY_ACCRUAL            number(22,2) default 0,
                          EXECUTORY_AMOUNT             number(22,2) default 0,
                          EXECUTORY_ADJUSTMENT         number(22,2) default 0,
                          CONTINGENT_ACCRUAL           number(22,2) default 0,
                          CONTINGENT_AMOUNT            number(22,2) default 0,
                          CONTINGENT_ADJUSTMENT        number(22,2) default 0,
                          PAYMENT_AMOUNT               number(22,2)
                         )';

      execute immediate 'alter table LS_ASSET_ACTIVITY
                            add constraint FK_ASSETACTIVITY_LSASSET
                                foreign key (LS_ASSET_ID)
                                references LS_ASSET (LS_ASSET_ID)';

      execute immediate 'alter table LS_ASSET_ACTIVITY
                            add constraint R_LS_ASSET_ACTIVITY1
                                foreign key (LS_ACTIVITY_TYPE_ID)
                                references LS_ACTIVITY_TYPE (LS_ACTIVITY_TYPE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_ASSET_ACTIVITY
                            add constraint PK_LS_ASSET_ACTIVITY
                                primary key (LS_ASSET_ID, LS_ASSET_ACTIVITY_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSASSETACT_CALCRUNID
                            on LS_ASSET_ACTIVITY (LEASE_CALC_RUN_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LSASSETACT_ACTTYPE2
                            on LS_ASSET_ACTIVITY (LS_ASSET_ID, LS_ACTIVITY_TYPE_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LSASSETACT_GLMONTHACTTYPE
                            on LS_ASSET_ACTIVITY (GL_MONTH_NUMBER, LS_ASSET_ID, LS_ACTIVITY_TYPE_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_COMPONENT.');
      execute immediate 'create table LS_ASSET_COMPONENT
                         (
                          LS_ASSET_ID                 number(22,0) not null,
                          LS_COMPONENT_ID             number(22,0) not null,
                          TIME_STAMP                  date,
                          USER_ID                     varchar2(18),
                          DESCRIPTION                 varchar2(35),
                          LONG_DESCRIPTION            varchar2(254),
                          LS_COMPONENT_STATUS_ID      number(22,0) not null,
                          INTERIM_INTEREST_BEGIN_DATE date,
                          GL_POSTING_BEGIN_DATE       date,
                          PO_ID                       varchar2(35),
                          MANUFACTURER                varchar2(35),
                          MODEL                       varchar2(35),
                          SERIAL_NUMBER               varchar2(35),
                          CURRENT_LEASE_COST          number(22,2) default 0,
                          COMMITTED_COST_ESTIMATE     number(22,2) default 0,
                          CWIP_CPR_POSTED_COST        number(22,2) default 0
                         )';

      execute immediate 'alter table LS_ASSET_COMPONENT
                            add constraint FK_LS_ASSET_COMPNT
                                foreign key (LS_ASSET_ID)
                                references LS_ASSET (LS_ASSET_ID)';

      execute immediate 'alter table LS_ASSET_COMPONENT
                            add constraint R_LS_ASSET_COMPONENT1
                                foreign key (LS_COMPONENT_STATUS_ID)
                                references LS_COMPONENT_STATUS (LS_COMPONENT_STATUS_ID)';

      --PK Constraint

      execute immediate 'alter table LS_ASSET_COMPONENT
                            add constraint PK_LS_ASSET_COMPONENT
                                primary key (LS_ASSET_ID, LS_COMPONENT_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_ASSET_COMP_PO
                            on LS_ASSET_COMPONENT (PO_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_CPR_FIELDS.');
      execute immediate 'create table LS_ASSET_CPR_FIELDS
                         (
                          LS_ASSET_ID         number(22,0) not null,
                          TIME_STAMP          date,
                          USER_ID             varchar2(18),
                          PROPERTY_GROUP_ID   number(22,0),
                          DEPR_GROUP_ID       number(22,0),
                          BOOKS_SCHEMA_ID     number(22,0),
                          RETIREMENT_UNIT_ID  number(22,0),
                          BUS_SEGMENT_ID      number(22,0) not null,
                          COMPANY_ID          number(22,0) not null,
                          FUNC_CLASS_ID       number(22,0),
                          UTILITY_ACCOUNT_ID  number(22,0),
                          GL_ACCOUNT_ID       number(22,0),
                          ASSET_LOCATION_ID   number(22,0),
                          SUB_ACCOUNT_ID      number(22,0),
                          WORK_ORDER_ID       number(22,0),
                          WORK_ORDER_NUMBER   varchar2(35) not null,
                          IN_SERVICE_YEAR     date,
                          ENG_IN_SERVICE_YEAR date,
                          STATE_ID            char(18) not null,
                          SUBLEDGER_INDICATOR number(22,0),
                          ASSET_ID            number(22,0)
                         )';

      execute immediate 'alter table LS_ASSET_CPR_FIELDS
                            add constraint R_LS_ASSET_CPR_FIELDS7
                                foreign key (UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID)
                                references SUB_ACCOUNT (UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID)';

      execute immediate 'alter table LS_ASSET_CPR_FIELDS
                            add constraint FK_LS_CPR_FIELDS_COMPANY
                                foreign key (COMPANY_ID)
                                references COMPANY_SETUP (COMPANY_ID)';

      execute immediate 'alter table LS_ASSET_CPR_FIELDS
                            add constraint FK_LS_CPR_FIELDS_DEPR_GROUP
                                foreign key (DEPR_GROUP_ID)
                                references DEPR_GROUP (DEPR_GROUP_ID)';

      execute immediate 'alter table LS_ASSET_CPR_FIELDS
                            add constraint R_LS_ASSET_CPR_FIELDS1
                                foreign key (GL_ACCOUNT_ID)
                                references GL_ACCOUNT (GL_ACCOUNT_ID)';

      execute immediate 'alter table LS_ASSET_CPR_FIELDS
                            add constraint R_LS_ASSET_CPR_FIELDS2
                                foreign key (ASSET_LOCATION_ID)
                                references ASSET_LOCATION (ASSET_LOCATION_ID)';

      execute immediate 'alter table LS_ASSET_CPR_FIELDS
                            add constraint R_LS_ASSET_CPR_FIELDS3
                                foreign key (BOOKS_SCHEMA_ID)
                                references BOOK_SCHEMA (BOOKS_SCHEMA_ID)';

      execute immediate 'alter table LS_ASSET_CPR_FIELDS
                            add constraint R_LS_ASSET_CPR_FIELDS4
                                foreign key (FUNC_CLASS_ID)
                                references FUNC_CLASS (FUNC_CLASS_ID)';

      execute immediate 'alter table LS_ASSET_CPR_FIELDS
                            add constraint R_LS_ASSET_CPR_FIELDS5
                                foreign key (LS_ASSET_ID)
                                references LS_ASSET (LS_ASSET_ID)';

      execute immediate 'alter table LS_ASSET_CPR_FIELDS
                            add constraint R_LS_ASSET_CPR_FIELDS6
                                foreign key (STATE_ID)
                                references STATE (STATE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_ASSET_CPR_FIELDS
                             add constraint PK_LS_ASSET_CPR_FIELDS
                                 primary key (LS_ASSET_ID)
                                 using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index ID_CO
                            on LS_ASSET_CPR_FIELDS (LS_ASSET_ID, COMPANY_ID)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index LS_ASSET_CPR_FIELDS_WOID_IDX
                             on LS_ASSET_CPR_FIELDS (WORK_ORDER_ID)
                                tablespace PWRPLANT_IDX';

      execute immediate 'create index IX_LS_CPRFIELDS_CPRID
                            on LS_ASSET_CPR_FIELDS (ASSET_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_FLEX_FIELDS.');
      execute immediate 'create table LS_ASSET_FLEX_FIELDS
                         (
                          LS_ASSET_ID number(22,0) not null,
                          TIME_STAMP  date,
                          USER_ID     varchar2(18),
                          FLEX_1      number(22,8),
                          FLEX_2      number(22,8),
                          FLEX_3      number(22,8),
                          FLEX_4      number(22,8),
                          FLEX_5      number(22,8),
                          FLEX_6      varchar2(35),
                          FLEX_7      varchar2(35),
                          FLEX_8      varchar2(35),
                          FLEX_9      varchar2(35),
                          FLEX_10     varchar2(35),
                          FLEX_11     varchar2(254),
                          FLEX_12     varchar2(254),
                          FLEX_13     varchar2(254),
                          FLEX_14     varchar2(254),
                          FLEX_15     varchar2(254),
                          FLEX_16     varchar2(2000),
                          FLEX_17     varchar2(2000),
                          FLEX_18     varchar2(2000),
                          FLEX_19     varchar2(2000),
                          FLEX_20     varchar2(2000)
                         )';

      execute immediate 'alter table LS_ASSET_FLEX_FIELDS
                            add constraint FK_LS_FLEX_ASSET
                                foreign key (LS_ASSET_ID)
                                references LS_ASSET (LS_ASSET_ID)';

      --PK Constraint

      execute immediate 'alter table LS_ASSET_FLEX_FIELDS
                            add constraint PK_LS_ASSET_FLEX_FIELDS
                                primary key (LS_ASSET_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_ASSET_LOCAL_TAX.');
      execute immediate 'create table LS_ASSET_LOCAL_TAX
                         (
                          LS_ASSET_ID  number(22,0) not null,
                          LOCAL_TAX_ID number(22,0) not null,
                          TIME_STAMP   date,
                          USER_ID      varchar2(18)
                         )';

      execute immediate 'alter table LS_ASSET_LOCAL_TAX
                            add constraint FK_LS_ASSET_ID
                                foreign key (LS_ASSET_ID)
                                references LS_ASSET (LS_ASSET_ID)';

      --PK Constraint

      execute immediate 'alter table LS_ASSET_LOCAL_TAX
                            add constraint PK_LS_ASSET_LOCAL_TAX
                                primary key (LS_ASSET_ID, LOCAL_TAX_ID)
                                using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_DIST_LINE.');
      execute immediate 'create table LS_DIST_LINE
                         (
                          DIST_LINE_ID         number(22,0),
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          DIST_DEF_TYPE_ID     number(22,0),
                          DISTRIBUTION_TYPE_ID number(22,0),
                          LS_ASSET_ID          number(22,0),
                          COMPANY_ID           number(22,0),
                          DIVISION_ID          number(22,0),
                          DEPARTMENT_ID        number(22,0),
                          DIST_GL_ACCOUNT_ID   number(22,0),
                          WORK_ORDER_NUMBER    varchar2(35),
                          COST_ELEMENT_ID      number(22,0),
                          ASSET_LOCATION_ID    number(22,0),
                          STATE_ID             char(18),
                          ELEMENT_1            varchar2(35),
                          ELEMENT_2            varchar2(35),
                          ELEMENT_3            varchar2(35),
                          ELEMENT_4            varchar2(35),
                          ELEMENT_5            varchar2(35),
                          ELEMENT_6            varchar2(35),
                          ELEMENT_7            varchar2(35),
                          ELEMENT_8            varchar2(35),
                          ELEMENT_9            varchar2(35),
                          ELEMENT_10           varchar2(35),
                          GL_JE_CODE           varchar2(35),
                          GL_POSTING_DATE      date,
                          GL_POSTING_MONTHNUM  number(22,0),
                          GL_POSTING_TIMESTAMP date,
                          GL_POSTING_USERID    varchar2(18),
                          LEASE_CALC_RUN_ID    number(22,0),
                          MASS_CHANGE_ID       number(22,0),
                          DESCRIPTION          varchar2(35),
                          LONG_DESCRIPTION     varchar2(254),
                          AMOUNT               number(22,2),
                          LEASE_ID             number(22,0),
                          DC_INDICATOR         number(22,0),
                          JE_METHOD_ID         number(22,0),
                          LEASE_NUMBER         varchar2(35)
                         )';

      execute immediate 'alter table LS_DIST_LINE
                            add constraint FK_DISTLN_COID foreign key (COMPANY_ID)
                                references COMPANY_SETUP (COMPANY_ID)';

      execute immediate 'alter table LS_DIST_LINE
                            add constraint FK_DISTLN_DIVID foreign key (DIVISION_ID)
                                references DIVISION (DIVISION_ID)';

      execute immediate 'alter table LS_DIST_LINE
                            add constraint FK_DISTLN_LSASSETID foreign key (LS_ASSET_ID)
                                references LS_ASSET (LS_ASSET_ID)';

      execute immediate 'alter table LS_DIST_LINE
                            add constraint R_LS_DIST_LINE1 foreign key (STATE_ID)
                                references STATE (STATE_ID)';

      execute immediate 'alter table LS_DIST_LINE
                            add constraint R_LS_DIST_LINE2 foreign key (COST_ELEMENT_ID)
                                references COST_ELEMENT (COST_ELEMENT_ID)';

      execute immediate 'alter table LS_DIST_LINE
                            add constraint R_LS_DIST_LINE3 foreign key (DIST_GL_ACCOUNT_ID)
                                references LS_DIST_GL_ACCOUNT (DIST_GL_ACCOUNT_ID)';

      execute immediate 'alter table LS_DIST_LINE
                            add constraint R_LS_DIST_LINE4 foreign key (DISTRIBUTION_TYPE_ID)
                                references LS_DISTRIBUTION_TYPE (DISTRIBUTION_TYPE_ID)';

      execute immediate 'alter table LS_DIST_LINE
                            add constraint R_LS_DIST_LINE5 foreign key (LEASE_CALC_RUN_ID)
                                references LS_LEASE_CALC_CONTROL (LEASE_CALC_RUN_ID)';

      --Index

      execute immediate 'create index IX_DISTLINE_CO_MONTH
                            on LS_DIST_LINE (COMPANY_ID, GL_POSTING_MONTHNUM)
                               tablespace PWRPLANT_IDX';

      execute immediate 'create index LS_DIST_LINE_ASSET_IDX
                            on LS_DIST_LINE (LS_ASSET_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LOCAL_TAX_SNAPSHOT.');
      execute immediate 'create table LS_LOCAL_TAX_SNAPSHOT
                         (
                          LEASE_CALC_RUN_ID     number(22,0) not null,
                          LS_ASSET_ID           number(22,0) not null,
                          ASSET_LOCATION_ID     number(22,0) not null,
                          SUMMARY_LOCAL_TAX_ID  number(22,0) not null,
                          LOCAL_TAX_DISTRICT_ID number(22,0) not null,
                          TIME_STAMP            date,
                          USER_ID               varchar2(18),
                          RATE                  number(22,8)
                         )';

      execute immediate 'alter table LS_LOCAL_TAX_SNAPSHOT
                            add constraint FK_LOCTAXSNAP_LSASSETID
                                foreign key (LS_ASSET_ID)
                                references LS_ASSET (LS_ASSET_ID)';

      execute immediate 'alter table LS_LOCAL_TAX_SNAPSHOT
                             add constraint R_LS_LOCAL_TAX_SNAPSHOT1
                                 foreign key (ASSET_LOCATION_ID)
                                 references ASSET_LOCATION (ASSET_LOCATION_ID)';

      execute immediate 'alter table LS_LOCAL_TAX_SNAPSHOT
                            add constraint R_LS_LOCAL_TAX_SNAPSHOT2
                                foreign key (SUMMARY_LOCAL_TAX_ID)
                                references LS_SUMMARY_LOCAL_TAX (SUMMARY_LOCAL_TAX_ID)';

      execute immediate 'alter table LS_LOCAL_TAX_SNAPSHOT
                            add constraint R_LS_LOCAL_TAX_SNAPSHOT3
                                foreign key (LOCAL_TAX_DISTRICT_ID)
                                references LS_LOCAL_TAX_DISTRICT (LOCAL_TAX_DISTRICT_ID)';

      execute immediate 'alter table LS_LOCAL_TAX_SNAPSHOT
                            add constraint R_LS_LOCAL_TAX_SNAPSHOT4
                                foreign key (LEASE_CALC_RUN_ID)
                                references LS_LEASE_CALC_CONTROL (LEASE_CALC_RUN_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LOCAL_TAX_SNAPSHOT
                            add constraint PK_LS_LOCAL_TAX_SNAPSHOT
                                primary key (LEASE_CALC_RUN_ID, LS_ASSET_ID, ASSET_LOCATION_ID, SUMMARY_LOCAL_TAX_ID, LOCAL_TAX_DISTRICT_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index LS_LOCAL_TAX_SNAPSHOT_IDX
                            on LS_LOCAL_TAX_SNAPSHOT (LS_ASSET_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_LOCAL_TAX_SNAPSHOT_STATE.');
      execute immediate 'create table LS_LOCAL_TAX_SNAPSHOT_STATE
                         (
                          LEASE_CALC_RUN_ID    number(22,0) not null,
                          LS_ASSET_ID          number(22,0) not null,
                          ASSET_LOCATION_ID    number(22,0) not null,
                          SUMMARY_LOCAL_TAX_ID number(22,0) not null,
                          TIME_STAMP           date,
                          USER_ID              varchar2(18),
                          STATE_ID             char(18),
                          RATE                 number(22,8)
                         )';

      execute immediate 'alter table LS_LOCAL_TAX_SNAPSHOT_STATE
                            add constraint FK_LOCTAXSNAPSTATE_LSASSETID
                                foreign key (LS_ASSET_ID)
                                references LS_ASSET (LS_ASSET_ID)';

      --PK Constraint

      execute immediate 'alter table LS_LOCAL_TAX_SNAPSHOT_STATE
                            add constraint PK_LS_LOCAL_TAX_SNAPSHOT_STATE
                                primary key (LEASE_CALC_RUN_ID, LS_ASSET_ID, ASSET_LOCATION_ID, SUMMARY_LOCAL_TAX_ID)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index LS_LOCTAX_SNAPSHOT_IDX
                            on LS_LOCAL_TAX_SNAPSHOT_STATE (LS_ASSET_ID)
                               tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_VOUCHER_DETAIL.');
      execute immediate 'create table LS_VOUCHER_DETAIL
                         (
                          VOUCHER_ID                number(22,0) not null,
                          VOUCHER_LINE_ID           number(22,0) not null,
                          VOUCHER_DETAIL_ID         number(22,0) not null,
                          TIME_STAMP                date,
                          USER_ID                   nvarchar2(18),
                          LS_ASSET_ID               number(22,0),
                          VOUCHER_DETAIL_AMOUNT     number(22,3),
                          VOUCHER_DETAIL_PRINCIPAL  number(22,3),
                          VOUCHER_DETAIL_INTEREST   number(22,3),
                          VOUCHER_DETAIL_USE_TAX    number(22,3),
                          VOUCHER_DETAIL_EXECUTORY  number(22,3),
                          VOUCHER_DETAIL_CONTINGENT number(22,3),
                          GL_COMPANY_NO             nvarchar2(35),
                          EXTERNAL_DEPARTMENT_CODE  nvarchar2(35),
                          EXTERNAL_ACCOUNT_CODE     nvarchar2(35),
                          WORK_ORDER_NUMBER         nvarchar2(35),
                          EXTERNAL_COST_ELEMENT     nvarchar2(35),
                          EXT_ASSET_LOCATION        nvarchar2(35),
                          STATE_ID                  char(18),
                          ELEMENT_1                 nvarchar2(35),
                          ELEMENT_2                 nvarchar2(35),
                          ELEMENT_3                 nvarchar2(35),
                          ELEMENT_4                 nvarchar2(35),
                          ELEMENT_5                 nvarchar2(35),
                          ELEMENT_6                 nvarchar2(35),
                          ELEMENT_7                 nvarchar2(35),
                          ELEMENT_8                 nvarchar2(35),
                          ELEMENT_9                 nvarchar2(35),
                          ELEMENT_10                nvarchar2(35),
                          ELEMENT_11                nvarchar2(35),
                          ELEMENT_12                nvarchar2(35),
                          ELEMENT_13                nvarchar2(35),
                          ELEMENT_14                nvarchar2(35),
                          ELEMENT_15                nvarchar2(35),
                          ELEMENT_16                nvarchar2(35),
                          ELEMENT_17                nvarchar2(35),
                          ELEMENT_18                nvarchar2(35),
                          ELEMENT_19                nvarchar2(35),
                          ELEMENT_20                nvarchar2(35),
                          ELEMENT_21                nvarchar2(35),
                          ELEMENT_22                nvarchar2(35),
                          ELEMENT_23                nvarchar2(35),
                          ELEMENT_24                nvarchar2(35),
                          ELEMENT_25                nvarchar2(35),
                          ELEMENT_26                nvarchar2(35),
                          ELEMENT_27                nvarchar2(35),
                          ELEMENT_28                nvarchar2(35),
                          ELEMENT_29                nvarchar2(35),
                          ELEMENT_30                nvarchar2(35)
                         )';

      execute immediate 'alter table LS_VOUCHER_DETAIL
                            add constraint FK_LS_VOUCHER_DETAIL_1
                                foreign key (VOUCHER_ID, VOUCHER_LINE_ID)
                                references LS_VOUCHER_LINE (VOUCHER_ID, VOUCHER_LINE_ID)';

      --PK Constraint

      execute immediate 'alter table LS_VOUCHER_DETAIL
                             add constraint PK_LS_VOUCHER_DETAIL
                                 primary key (VOUCHER_ID, VOUCHER_LINE_ID, VOUCHER_DETAIL_ID)
                                 using index tablespace PWRPLANT_IDX';

      --*****************************************************************
      -- Table
      --*****************************************************************
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Create table LS_COMPONENT_CHARGE.');
      execute immediate 'create table LS_COMPONENT_CHARGE
                         (
                          LS_ASSET_ID                 number(22,0) not null,
                          LS_COMPONENT_ID             number(22,0) not null,
                          INVOICE_NUMBER              varchar2(35) not null,
                          TIME_STAMP                  date,
                          USER_ID                     varchar2(18),
                          CHARGE_ID                   number(22,0),
                          AMOUNT                      number(22,2) not null,
                          INTERIM_INTEREST_BEGIN_DATE date
                         )';

      execute immediate 'alter table LS_COMPONENT_CHARGE
                            add constraint FK_LS_CMPNTCHG_COMPNT
                                foreign key (LS_ASSET_ID, LS_COMPONENT_ID)
                                references LS_ASSET_COMPONENT (LS_ASSET_ID, LS_COMPONENT_ID)';

      --PK Constraint

      execute immediate 'alter table LS_COMPONENT_CHARGE
                            add constraint PK_LS_COMPONENT_CHARGE
                                primary key (LS_ASSET_ID, LS_COMPONENT_ID, INVOICE_NUMBER)
                                using index tablespace PWRPLANT_IDX';

      --Index

      execute immediate 'create index IX_LSCOMPCHG_CHGID
                            on LS_COMPONENT_CHARGE (CHARGE_ID)
                               tablespace PWRPLANT_IDX';
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (358, 0, 10, 4, 0, 0, 29827, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029827_lease_create_tables_2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
