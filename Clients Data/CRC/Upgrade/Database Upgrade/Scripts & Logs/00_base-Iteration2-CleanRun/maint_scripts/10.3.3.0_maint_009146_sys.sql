/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009146_sys.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   12/13/2011 Luis Kramarz   Point Release
||============================================================================
*/

insert into PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'RWIP Closeout - Calc gain/loss',
          'Yes',
          'dw_yes_no;1',
          '"Yes" will require Post to calculate Gain/Loss for the other set of books. "NO"  will require Post to set the Gain/Loss to zero for the other set of books". Default is "Yes"',
          -1
     from PWRPLANT.PP_SYSTEM_CONTROL_COMPANY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (72, 0, 10, 3, 3, 0, 9146, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_009146_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
