/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029581_pwrtax_TAX_DEPR_CALC.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   03/14/2013 Roger Roach      Changed the Fast Tax to use Oracle Packages
||============================================================================
*/

create or replace package TAX_DEPR_CALC as
   --||============================================================================
   --|| Application: PowerPlan
   --|| Object Name: TAX_DEPR_CALC
   --|| Description:
   --||============================================================================
   --|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   --||============================================================================
   --|| Version  Date       Revised By     Reason for Change
   --|| -------- ---------- -------------- ----------------------------------------
   --|| 5.0      02/13/2013 Roger Roach    Original Version
   --|| 5.1      03/12/2013 Roger Roach    change the organization of the collection
   --|| 5.2      03/27/2013 Roger Roach    added columns compare_rate and tax_activity_type_id
   --|| 5.21     04/01/2013 Roger Roach    changed round with trunc
   --|| 5.22     04/05/2013 Roger Roach    added session parameter option
   --||============================================================================

   -- ============================== Tax Program Structure ========================
   --
   --                            calc_depr  -->  read tax_job_params
   --                                |
   --                                |
   --                                |
   --                                |--------------> Get Records
   --                                |
   --                  Loop  ------->|
   --                        ^       |--------------> Filter Records
   --                        |       |
   --                        |       |
   --                        |       |--------------> Calc Records
   --                        |
   --                        |       | <- Update Depreciation Records
   --                        <-------|
   --                                | <- Update Reconcile Records
   --                        |
   --                                | <- Commit
   --                               END
   --
   -- =============================================================================

   G_LIMIT constant pls_integer := 10000;
   subtype HASH_T is varchar2(100);
   G_SEP constant varchar2(1) := '-';
   G_START_TIME timestamp;

   type TAX_BAL_REC_REC is record(
      ID             number(22, 0),
      RECONCILE_ITEM number(22, 0),
      TAX_YEAR       number(22, 2));

   type TAX_REC_BAL_TYPE is table of TAX_BAL_REC_REC index by pls_integer;

   type TAX_REC_BAL_TABLE_TYPE is table of TAX_REC_BAL_TYPE index by HASH_T;

   G_TAX_RECONCILE_BAL_HASH TAX_REC_BAL_TABLE_TYPE;

   type TAX_DEPR_BALS_REC is record(
      ID       number(22, 0),
      TAX_YEAR number(22, 2));

   type TAX_DEPR_BALS_REC_TYPE is table of TAX_DEPR_BALS_REC index by pls_integer;

   type TAX_DEPR_BALS_TABLE_TYPE is table of TAX_DEPR_BALS_REC_TYPE index by HASH_T;

   G_TAX_DEPR_BALS_HASH TAX_DEPR_BALS_TABLE_TYPE;

   type TAX_REC_REC is record(
      ID             number(22, 0),
      RECONCILE_ITEM number(22, 0));

   type TAX_REC_TYPE is table of TAX_REC_REC index by pls_integer;

   type TAX_REC_TABLE_TYPE is table of TAX_REC_TYPE index by HASH_T;

   G_TAX_RECONCILE_HASH TAX_REC_TABLE_TYPE;

   type TAX_BOOK_ACTIVITY_TYPE is table of pls_integer index by pls_integer;

   type TAX_BOOK_ACTIVITY_TABLE_TYPE is table of TAX_BOOK_ACTIVITY_TYPE index by HASH_T;

   G_TAX_BOOK_ACTIVITY_HASH TAX_BOOK_ACTIVITY_TABLE_TYPE;

   type TAX_DEPR_HASH_TYPE is table of number(22, 0) index by HASH_T;

   G_TAX_DEPR_HASH     TAX_DEPR_HASH_TYPE;
   G_TAX_CALC_ADJ_HASH TAX_DEPR_HASH_TYPE;

   type TAX_CONVENTION_TABLE_TYPE is table of pls_integer index by binary_integer;

   G_TAX_CONVENTION_HASH TAX_CONVENTION_TABLE_TYPE;

   type TAX_RATES_REC is record(
      ID      number(22, 0),
      RATE_ID number(22, 0));

   type TAX_RATES_REC_TYPE is table of TAX_RATES_REC index by pls_integer;

   type TAX_RATES_TABLE_TYPE is table of TAX_RATES_REC_TYPE index by HASH_T;

   G_TAX_RATES_HASH  TAX_RATES_TABLE_TYPE;
   G_TAX_LIMIT2_HASH TAX_RATES_TABLE_TYPE;

   G_TABLE_BOOK_IDS TABLE_LIST_ID_TYPE;

   G_VERSION           number(22, 0);
   G_START_YEAR        number(22, 2);
   G_END_YEAR          number(22, 2);

   G_TABLE_VINTAGE_IDS TABLE_LIST_ID_TYPE;
   G_TABLE_CLASS_IDS   TABLE_LIST_ID_TYPE;
   G_TABLE_COMPANY_IDS TABLE_LIST_ID_TYPE;

   G_TAX_YEAR_COUNT      number(22, 2);
   G_TABLE_BOOK_COUNT    number(22, 0);
   G_TABLE_VINTAGE_COUNT number(22, 0);
   G_TABLE_CLASS_COUNT   number(22, 0);
   G_TABLE_COMPANY_COUNT number(22, 0);

   cursor TAX_DEPR_CUR is
      select distinct TD.TAX_RECORD_ID,
                      TD.TAX_BOOK_ID,
                      TD.TAX_YEAR,
                      TD.BOOK_BALANCE,
                      TD.TAX_BALANCE,
                      REMAINING_LIFE,
                      TD.ACCUM_RESERVE,
                      TD.SL_RESERVE,
                      DEPRECIABLE_BASE,
                      TD.FIXED_DEPRECIABLE_BASE,
                      ACTUAL_SALVAGE,
                      TD.ESTIMATED_SALVAGE,
                      TD.ACCUM_SALVAGE,
                      ADDITIONS,
                      TRANSFERS,
                      ADJUSTMENTS,
                      RETIREMENTS,
                      EXTRAORDINARY_RETIRES,
                      TD.ACCUM_ORDINARY_RETIRES,
                      COST_OF_REMOVAL,
                      EST_SALVAGE_PCT,
                      RETIRE_INVOL_CONV,
                      SALVAGE_INVOL_CONV,
                      SALVAGE_EXTRAORD,
                      TD.RESERVE_AT_SWITCH,
                      TD.QUANTITY,
                      CONVENTION_ID,
                      EXTRAORDINARY_CONVENTION,
                      TAX_LIMIT_ID,
                      TAX_RATE_ID,
                      TD.NUMBER_MONTHS_BEG,
                      TD.NUMBER_MONTHS_END,
                      NVL(TR.BOOK_BALANCE, 0) BOOK_BALANCE_XFER,
                      NVL(TR.TAX_BALANCE, 0) TAX_BALANCE_XFER,
                      NVL(TR.ACCUM_RESERVE, 0) ACCUM_RESERVE_XFER,
                      NVL(TR.SL_RESERVE, 0) SL_RESERVE_XFER,
                      NVL(TR.FIXED_DEPRECIABLE_BASE, 0) FIXED_DEPRECIABLE_BASE_XFER,
                      NVL(TR.ESTIMATED_SALVAGE, 0) ESTIMATED_SALVAGE_XFER,
                      NVL(TR.ACCUM_SALVAGE, 0) ACCUM_SALVAGE_XFER,
                      NVL(TR.ACCUM_ORDINARY_RETIRES, 0) ACCUM_ORDINARY_RETIRES_XFER,
                      NVL(TR.RESERVE_AT_SWITCH, 0) RESERVE_AT_SWITCH_XFER,
                      NVL(TR.QUANTITY, 0) QUANTITY_XFER,
                      TC.TAX_CREDIT_ID,
                      V.YEAR VINTAGE_YEAR,
                      TD.JOB_CREATION_AMOUNT,
                      TD.COR_RES_IMPACT,
                      TD.COR_EXPENSE,
                      TD.SALVAGE_RES_IMPACT,
                      TD.COR_EXPENSE ACCUM_ORDIN_RETIRES_END,
                      TD.ACCUM_SALVAGE_END,
                      TD.SL_RESERVE_END,
                      TD.ACCUM_RESERVE_END,
                      TD.TAX_BALANCE_END,
                      TD.BOOK_BALANCE_END,
                      TD.ADJUSTED_RETIRE_BASIS,
                      TD.TRANSFER_RES_IMPACT,
                      TD.EX_RETIRE_RES_IMPACT,
                      TD.RETIRE_RES_IMPACT,
                      TD.OVER_ADJ_DEPRECIATION,
                      TD.CALC_DEPRECIATION,
                      TD.CAPITAL_GAIN_LOSS,
                      TD.EX_GAIN_LOSS,
                      TD.GAIN_LOSS,
                      TD.DEPRECIATION,
                      TD.RESERVE_AT_SWITCH_END,
                      TD.ESTIMATED_SALVAGE_END
        from VINTAGE V,
             --    temp_vintage_tbl tv,
             --    temp_tax_book_tbl ttb,
             --    temp_tax_class_tbl ttc,
             --    temp_company_id_tbl ttcm,
             TAX_CONTROL TC,
             TAX_RECORD_CONTROL TRC,
             TAX_DEPRECIATION TD,
             (select TAX_RECORD_ID,
                     TAX_BOOK_ID,
                     TAX_YEAR,
                     sum(BOOK_BALANCE) BOOK_BALANCE,
                     sum(TAX_BALANCE) TAX_BALANCE,
                     sum(ACCUM_RESERVE) ACCUM_RESERVE,
                     sum(SL_RESERVE) SL_RESERVE,
                     sum(FIXED_DEPRECIABLE_BASE) FIXED_DEPRECIABLE_BASE,
                     sum(ESTIMATED_SALVAGE) ESTIMATED_SALVAGE,
                     sum(ACCUM_SALVAGE) ACCUM_SALVAGE,
                     sum(ACCUM_ORDINARY_RETIRES) ACCUM_ORDINARY_RETIRES,
                     sum(RESERVE_AT_SWITCH) RESERVE_AT_SWITCH,
                     sum(QUANTITY) QUANTITY
                from TAX_DEPRECIATION_TRANSFER
               group by TAX_RECORD_ID, TAX_BOOK_ID, TAX_YEAR) TR
       where (TRC.VINTAGE_ID in (select * from table(G_TABLE_VINTAGE_IDS)) or
             G_TABLE_VINTAGE_COUNT = 0)
         and (TD.TAX_BOOK_ID in (select * from table(G_TABLE_BOOK_IDS)) or G_TABLE_BOOK_COUNT = 0)
         and (TRC.TAX_CLASS_ID in (select * from table(G_TABLE_CLASS_IDS)) or
             G_TABLE_CLASS_COUNT = 0)
         and (TRC.COMPANY_ID in (select * from table(G_TABLE_COMPANY_IDS)) or
             G_TABLE_COMPANY_COUNT = 0)
         and TD.TAX_YEAR between G_START_YEAR and G_END_YEAR
         and TC.TAX_BOOK_ID = TD.TAX_BOOK_ID
         and TRC.TAX_RECORD_ID = TC.TAX_RECORD_ID
         and TRC.TAX_RECORD_ID = TD.TAX_RECORD_ID
         and TD.TAX_BOOK_ID = TR.TAX_BOOK_ID(+)
         and TD.TAX_RECORD_ID = TR.TAX_RECORD_ID(+)
         and TD.TAX_YEAR = TR.TAX_YEAR(+)
         and V.VINTAGE_ID = TRC.VINTAGE_ID
         and TRC.VERSION_ID = G_VERSION
       order by TD.TAX_RECORD_ID, TD.TAX_BOOK_ID, TD.TAX_YEAR;

   type TYPE_TAX_DEPR_REC is varray(10040) of TAX_DEPR_CUR%rowtype;

   type TYPE_TAX_DEPR_SAVE_REC is varray(40) of TAX_DEPR_CUR%rowtype;

   G_TAX_DEPR_REC       TYPE_TAX_DEPR_REC;
   G_TAX_DEPR_SAVE1_REC TYPE_TAX_DEPR_SAVE_REC;
   G_TAX_DEPR_SAVE2_REC TYPE_TAX_DEPR_SAVE_REC;

   G_TAX_DEPR_LAST_RECORD_ID number(22, 0);
   G_TAX_DEPR_LAST_BOOK_ID   number(22, 0);

   cursor TAX_CALC_ADJ_CUR is
      select TC.TAX_RECORD_ID,
             TC.TAX_BOOK_ID,
             TA.TAX_YEAR,
             DEPRECIATION_ADJUST,
             BOOK_BALANCE_ADJUST,
             ACCUM_RESERVE_ADJUST,
             DEPRECIABLE_BASE_ADJUST,
             GAIN_LOSS_ADJUST,
             CAP_GAIN_LOSS_ADJUST,
             BOOK_BALANCE_ADJUST_METHOD,
             ACCUM_RESERVE_ADJUST_METHOD,
             DEPRECIABLE_BASE_ADJUST_METHOD
        from TAX_RECORD_CONTROL TRC, TAX_CONTROL TC, TAX_DEPR_ADJUST TA
       where TC.TAX_RECORD_ID = TA.TAX_RECORD_ID
         and TC.TAX_BOOK_ID = TA.TAX_BOOK_ID
         and TRC.TAX_RECORD_ID = TC.TAX_RECORD_ID
         and (TRC.VINTAGE_ID in (select * from table(G_TABLE_VINTAGE_IDS)) or
             G_TABLE_VINTAGE_COUNT = 0)
         and (TC.TAX_BOOK_ID in (select * from table(G_TABLE_BOOK_IDS)) or G_TABLE_BOOK_COUNT = 0)
         and (TRC.TAX_CLASS_ID in (select * from table(G_TABLE_CLASS_IDS)) or
             G_TABLE_CLASS_COUNT = 0)
         and (TRC.COMPANY_ID in (select * from table(G_TABLE_COMPANY_IDS)) or
             G_TABLE_COMPANY_COUNT = 0)
         and TA.TAX_YEAR between G_START_YEAR and G_END_YEAR
         and TRC.VERSION_ID = G_VERSION
       order by TA.TAX_RECORD_ID, TA.TAX_BOOK_ID, TA.TAX_YEAR;

   type TYPE_TAX_CALC_ADJ_REC is varray(10040) of TAX_CALC_ADJ_CUR%rowtype;

   type TYPE_TAX_CALC_ADJ_SAVE_REC is varray(40) of TAX_CALC_ADJ_CUR%rowtype;

   G_TAX_CALC_ADJ_REC       TYPE_TAX_CALC_ADJ_REC;
   G_TAX_CALC_ADJ_SAVE1_REC TYPE_TAX_CALC_ADJ_SAVE_REC;
   G_TAX_CALC_ADJ_SAVE2_REC TYPE_TAX_CALC_ADJ_SAVE_REC;

   cursor TAX_CONVENTION_CUR is
      select CONVENTION_ID,
             RETIRE_DEPR_ID,
             RETIRE_BAL_ID,
             RETIRE_RESERVE_ID,
             GAIN_LOSS_ID,
             SALVAGE_ID,
             EST_SALVAGE_ID,
             COST_OF_REMOVAL_ID,
             CAP_GAIN_ID
        from TAX_CONVENTION;

   type TYPE_TAX_CONVENTION_REC is table of TAX_CONVENTION_CUR%rowtype;

   G_TAX_CONVENTION_REC TYPE_TAX_CONVENTION_REC;

   cursor TAX_RATES_CUR is
      select TR.TAX_RATE_ID,
             RATE,
             RATE1,
             TR.YEAR,
             NET_GROSS,
             LIFE,
             REMAINING_LIFE_PLAN,
             START_METHOD,
             ROUNDING_CONVENTION,
             HALF_YEAR_CONVENTION,
             SWITCHED_YEAR
        from TAX_RATES TR, TAX_RATE_CONTROL TRC
       where TR.TAX_RATE_ID = TRC.TAX_RATE_ID
       order by TR.TAX_RATE_ID, TR.YEAR;

   type TYPE_TAX_RATES_REC is table of TAX_RATES_CUR%rowtype;

   G_TAX_RATES_REC TYPE_TAX_RATES_REC;

   cursor TAX_BOOK_ACTIVITY_CUR is
      select TC.TAX_RECORD_ID,
             TC.TAX_BOOK_ID,
             B.TAX_YEAR,
             AMOUNT,
             T.TAX_ACTIVITY_CODE_ID,
             T.TAX_ACTIVITY_TYPE_ID
        from BASIS_AMOUNTS        B,
             TAX_ACTIVITY_CODE    T,
             TAX_CONTROL          TC,
             TAX_RECORD_CONTROL   TRC,
             TAX_INCLUDE_ACTIVITY TIA
       where B.TAX_ACTIVITY_CODE_ID = T.TAX_ACTIVITY_CODE_ID
         and TC.TAX_RECORD_ID = B.TAX_RECORD_ID
         and TRC.TAX_RECORD_ID = TC.TAX_RECORD_ID
         and TRC.TAX_RECORD_ID = B.TAX_RECORD_ID
         and TIA.TAX_INCLUDE_ID = B.TAX_INCLUDE_ID
         and TIA.TAX_BOOK_ID = TC.TAX_BOOK_ID
         and TRC.VERSION_ID = G_VERSION
         and (TRC.VINTAGE_ID in (select * from table(G_TABLE_VINTAGE_IDS)) or
             G_TABLE_VINTAGE_COUNT = 0)
         and (TC.TAX_BOOK_ID in (select * from table(G_TABLE_BOOK_IDS)) or G_TABLE_BOOK_COUNT = 0)
         and (TRC.TAX_CLASS_ID in (select * from table(G_TABLE_CLASS_IDS)) or
             G_TABLE_CLASS_COUNT = 0)
         and (TRC.COMPANY_ID in (select * from table(G_TABLE_COMPANY_IDS)) or
             G_TABLE_COMPANY_COUNT = 0)
         and B.TAX_YEAR between G_START_YEAR and G_END_YEAR
       order by TC.TAX_RECORD_ID, TC.TAX_BOOK_ID, B.TAX_YEAR;

   type TYPE_TAX_BOOK_ACTIVITY_REC is table of TAX_BOOK_ACTIVITY_CUR%rowtype;

   G_TAX_BOOK_ACTIVITY_REC TYPE_TAX_BOOK_ACTIVITY_REC;

   cursor TAX_RECONCILE_CUR is
      select TBR.TAX_RECORD_ID,
             TBR.TAX_YEAR,
             TBR.BASIS_AMOUNT_BEG,
             TBR.BASIS_AMOUNT_END,
             BASIS_AMOUNT_ACTIVITY,
             TBR.RECONCILE_ITEM_ID RECONCILE_ITEM_ID,
             trim(UPPER(TRI.TYPE)) RECONCILE_ITEM_TYPE,
             TBR.TAX_INCLUDE_ID,
             BASIS_AMOUNT_INPUT_RETIRE,
             TRI.INPUT_RETIRE_IND,
             NVL(TBRT.BASIS_AMOUNT_BEG, 0) BASIS_AMOUNT_TRANSFER,
             TRI.CALCED,
             TRI.DEPR_DEDUCTION
        from TAX_BOOK_RECONCILE TBR,
             (select TAX_RECORD_ID,
                     TAX_INCLUDE_ID,
                     RECONCILE_ITEM_ID,
                     TAX_YEAR,
                     sum(BASIS_AMOUNT_BEG) BASIS_AMOUNT_BEG
                from TAX_BOOK_RECONCILE_TRANSFER
               group by TAX_RECORD_ID, TAX_INCLUDE_ID, RECONCILE_ITEM_ID, TAX_YEAR) TBRT,
             TAX_RECORD_CONTROL TRC,
             TAX_RECONCILE_ITEM TRI
       where TBR.TAX_RECORD_ID = TRC.TAX_RECORD_ID
         and TRI.RECONCILE_ITEM_ID = TBR.RECONCILE_ITEM_ID
         and TBRT.TAX_INCLUDE_ID(+) = TBR.TAX_INCLUDE_ID
         and TBRT.TAX_YEAR(+) = TBR.TAX_YEAR
         and TBRT.TAX_RECORD_ID(+) = TBR.TAX_RECORD_ID
         and TBRT.RECONCILE_ITEM_ID(+) = TBR.RECONCILE_ITEM_ID
         and (TRC.VINTAGE_ID in (select * from table(G_TABLE_VINTAGE_IDS)) or
             G_TABLE_VINTAGE_COUNT = 0)
         and (TRC.TAX_CLASS_ID in (select * from table(G_TABLE_CLASS_IDS)) or
             G_TABLE_CLASS_COUNT = 0)
         and (TRC.COMPANY_ID in (select * from table(G_TABLE_COMPANY_IDS)) or
             G_TABLE_COMPANY_COUNT = 0)
         and TBR.TAX_YEAR between G_START_YEAR and G_END_YEAR + 1.1
         and TRC.VERSION_ID = G_VERSION
       order by TBR.TAX_RECORD_ID, TBR.TAX_INCLUDE_ID, TBR.TAX_YEAR;

   type TYPE_TAX_RECONCILE_REC is table of TAX_RECONCILE_CUR%rowtype;

   G_TAX_RECONCILE_REC TYPE_TAX_RECONCILE_REC;

   cursor TAX_LIMITATION_CUR is
      select TAX_LIMITATION.TAX_LIMIT_ID,
             TAX_LIMITATION.LIMITATION,
             year,
             NVL(TAX_LIMIT.COMPARE_RATE, 0) COMPARE_RATE
        from TAX_LIMITATION, TAX_LIMIT
       where TAX_LIMITATION.TAX_LIMIT_ID = TAX_LIMIT.TAX_LIMIT_ID
       order by year;

   type TYPE_TAX_LIMITATION_REC is table of TAX_LIMITATION_CUR%rowtype;

   G_TAX_LIMITATION_REC TYPE_TAX_LIMITATION_REC;

   cursor TAX_INCLUDES_ACTIVITY_CUR is
      select TAX_BOOK_ID, TAX_INCLUDE_ID from TAX_INCLUDE_ACTIVITY order by TAX_BOOK_ID;
   type TAX_INCLUDES_TYPE is table of number(22, 0);

   type TYPE_TAX_INCLUDE_ACTIVITY_TAB is record(
      TAX_BOOK_ID     number(22, 0),
      TAX_INCLUDE_IDS TAX_INCLUDES_TYPE);

   type TAX_INCLUDES_ACTIVITY_TYPE is table of TYPE_TAX_INCLUDE_ACTIVITY_TAB;
   G_TAX_INCLUDES_ACTIVITY_REC TAX_INCLUDES_ACTIVITY_TYPE;


   cursor TAX_LIMIT2_CUR is
      select TL.TAX_LIMIT_ID,
             TL.TAX_CREDIT_PERCENT,
             TL.BASIS_REDUCTION_PERCENT,
             TL.RECONCILE_ITEM_ID,
             TL.TAX_INCLUDE_ID,
             TL.CALC_FUTURE_YEARS,
             TCS.ORDERING,
             TRI.CALCED,
             TRI.DEPR_DEDUCTION,
             TC.TAX_BOOK_ID,
             TCS.TAX_CREDIT_ID,
             TRC.TAX_RECORD_ID
        from TAX_LIMIT          TL,
             TAX_CONTROL        TC,
             TAX_CREDIT_SCHEMA  TCS,
             TAX_RECONCILE_ITEM TRI,
             TAX_RECORD_CONTROL TRC
       where TCS.TAX_CREDIT_ID = TC.TAX_CREDIT_ID
         and TC.TAX_RECORD_ID = TRC.TAX_RECORD_ID
         and TL.TAX_LIMIT_ID = TCS.TAX_LIMIT_ID
         and TL.RECONCILE_ITEM_ID = TRI.RECONCILE_ITEM_ID(+)
         and TRC.VERSION_ID = G_VERSION
       order by TRC.TAX_RECORD_ID, TC.TAX_BOOK_ID, TAX_LIMIT_ID;

   type TYPE_TAX_LIMIT2_REC is table of TAX_LIMIT2_CUR%rowtype;

   G_TAX_LIMIT2_REC TYPE_TAX_LIMIT2_REC;

   function GET_VERSION return varchar2;

   function CALC_DEPR(A_JOB_NO number) return integer;

   function GET_TAX_DEPR(A_START_ROW out pls_integer) return integer;

   function GET_TAX_CALC_ADJ(A_START_ROW out pls_integer) return integer;

   function GET_TAX_CONVENTION return integer;

   function GET_TAX_RATES return integer;

   function GET_TAX_BOOK_ACTIVITY return integer;

   function GET_TAX_RECONCILE return integer;

   function GET_TAX_LIMITATION return integer;

   function GET_TAX_INCLUDE_ACTIVITY return integer;

   function GET_TAX_JOB_CREATION return integer;

   procedure WRITE_LOG(A_JOB_NO     number,
                       A_ERROR_TYPE number,
                       A_CODE       number,
                       A_MSG        varchar2);

   function UPDATE_TAX_DEPR(A_START_ROW pls_integer,
                            A_NUM_REC   pls_integer) return number;

   function UPDATE_TAX_BOOK_RECONCILE(A_NUM_REC pls_integer) return number;

   function INSERT_TAX_BOOK_RECONCILE(A_NUM_REC               pls_integer,
                                      A_ADD_TAX_RECONCILE_REC in out nocopy TYPE_TAX_RECONCILE_REC)
      return number;

   function CALC(A_TAX_DEPR_INDEX            integer,
                 A_TAX_DEPR_BALS_INDEX       integer,
                 A_TAX_CALC_ADJ_INDEX        integer,
                 A_TAX_DEPR_BALS_ROWS        pls_integer,
                 A_TAX_CONVENTION_REC        TAX_CONVENTION_CUR%rowtype,
                 A_TAX_CONVENTION_ROWS       pls_integer,
                 A_EXTRAORD_CONVENTION_REC   TAX_CONVENTION_CUR%rowtype,
                 A_EXTRAORD_CONVENTION_ROWS  pls_integer,
                 A_TAX_LIMITATION_SUB_REC    TYPE_TAX_LIMITATION_REC,
                 A_TAX_LIMITATION_ROWS       pls_integer,
                 A_TAX_RECONCILE_SUB_REC     in out nocopy TYPE_TAX_RECONCILE_REC,
                 A_TAX_RECONCILE_ROWS        in out nocopy pls_integer,
                 A_RECONCILE_BALS_SUB_REC    in out nocopy TYPE_TAX_RECONCILE_REC,
                 A_RECONCILE_BALS_ROWS       in out nocopy pls_integer,
                 A_TAX_JOB_CREATION_SUB_REC  TYPE_TAX_LIMIT2_REC,
                 A_TAX_JOB_CREATION_ROWS     pls_integer,
                 A_TAX_BOOK_ACTIVITY_SUB_REC TYPE_TAX_BOOK_ACTIVITY_REC,
                 A_TAX_BOOK_ACTIVITY_ROWS    pls_integer,
                 A_TAX_RATES_SUB_REC         TYPE_TAX_RATES_REC,
                 A_TAX_RATES_ROWS            pls_integer,
                 A_SHORT_MONTHS              in number,
                 A_ADD_TAX_RECONCILE_REC     in out nocopy TYPE_TAX_RECONCILE_REC,
                 A_ADD_TAX_RECONCILE_ROWS    in out nocopy integer,
                 A_NYEAR                     number) return number;

   procedure SET_SESSION_PARAMETER;

   G_JOB_NO  number(22, 0);
   G_LINE_NO number(22, 0);
end TAX_DEPR_CALC;
/


create or replace package body TAX_DEPR_CALC is
   -- =============================================================================
   --  Function GET_VERSION
   -- =============================================================================

   function GET_VERSION return varchar2 is
   begin
      return '5.22';
   end GET_VERSION;

   -- =============================================================================
   --  Function FILTER_TAX_BOOK_ACTIVITY
   -- =============================================================================
   function FILTER_TAX_BOOK_ACTIVITY(A_TAX_BOOK_ACTIVITY_REC in out nocopy TYPE_TAX_BOOK_ACTIVITY_REC,
                                     A_TAX_RECORD_ID         pls_integer,
                                     A_TAX_BOOK_ID           pls_integer,
                                     A_TAX_YEAR              number) return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_CODE  pls_integer;
   begin

      L_COUNT := G_TAX_BOOK_ACTIVITY_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP || TO_CHAR(A_TAX_BOOK_ID) || G_SEP || TO_CHAR(A_TAX_YEAR)).COUNT;
      A_TAX_BOOK_ACTIVITY_REC.DELETE;

      for I in 1 .. L_COUNT
      loop
         L_ROW := G_TAX_BOOK_ACTIVITY_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP ||
                                           TO_CHAR(A_TAX_BOOK_ID) || G_SEP || TO_CHAR(A_TAX_YEAR)) (I);
         A_TAX_BOOK_ACTIVITY_REC.EXTEND;
         A_TAX_BOOK_ACTIVITY_REC(I) := G_TAX_BOOK_ACTIVITY_REC(L_ROW);
      end loop;
      return L_COUNT;

   exception
      when NO_DATA_FOUND then
         return 0;
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_book_activity ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_TAX_BOOK_ACTIVITY;

   -- =============================================================================
   --  Function FILTER_TAX_RECONCILE
   -- =============================================================================
   function FILTER_TAX_RECONCILE(A_TAX_RECONCILE_REC in out nocopy TYPE_TAX_RECONCILE_REC,
                                 A_TAX_RECORD_ID     pls_integer,
                                 A_INCLUDE_IDS       TAX_INCLUDES_TYPE,
                                 L_TAX_YEAR          number) return pls_integer is
      I                     pls_integer;
      K                     pls_integer;
      L_COUNT               pls_integer;
      L_INDEX               pls_integer;
      L_ROW_COUNT           pls_integer;
      L_TAX_RECONCILE_COUNT pls_integer;
      L_INCLUDE_ID_COUNT    pls_integer;
      L_INCLUDE_ID          pls_integer;
      L_CODE                pls_integer;
   begin
      L_ROW_COUNT := 0;
      A_TAX_RECONCILE_REC.DELETE;
      L_INCLUDE_ID_COUNT := A_INCLUDE_IDS.COUNT;
      for I in 1 .. L_INCLUDE_ID_COUNT
      loop
         L_INCLUDE_ID := A_INCLUDE_IDS(I);
         begin
            L_TAX_RECONCILE_COUNT := G_TAX_RECONCILE_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP || TO_CHAR(L_INCLUDE_ID) || G_SEP || TO_CHAR(L_TAX_YEAR)).COUNT;
         exception
            when NO_DATA_FOUND then
               L_TAX_RECONCILE_COUNT := 0;
         end;

         for K in 1 .. L_TAX_RECONCILE_COUNT
         loop
            L_INDEX     := G_TAX_RECONCILE_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP ||
                                                TO_CHAR(L_INCLUDE_ID) || G_SEP ||
                                                TO_CHAR(L_TAX_YEAR))(K).ID;
            L_ROW_COUNT := L_ROW_COUNT + 1;
            A_TAX_RECONCILE_REC.EXTEND;
            A_TAX_RECONCILE_REC(L_ROW_COUNT) := G_TAX_RECONCILE_REC(L_INDEX);
         end loop;
      end loop;
      I := 0;
      return L_ROW_COUNT;
   exception
      when NO_DATA_FOUND then
         return 0;
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_reconcile_bals ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_TAX_RECONCILE;

   -- =============================================================================
   --  Function FILTER_TAX_RECONCILE_BALS
   -- =============================================================================
   function FILTER_TAX_RECONCILE_BALS(A_TAX_RECONCILE_REC in out nocopy TYPE_TAX_RECONCILE_REC,
                                      A_TAX_RECORD_ID     pls_integer,
                                      A_INCLUDE_IDS       TAX_INCLUDES_TYPE,
                                      A_TAX_YEAR          number) return pls_integer is
      I                     pls_integer;
      L_CODE                pls_integer;
      L_COUNT               pls_integer;
      L_INCLUDE_ID          pls_integer;
      L_INDEX               pls_integer;
      L_ROW_COUNT           pls_integer;
      L_TAX_RECONCILE_COUNT pls_integer;
      L_INCLUDE_ID_COUNT    pls_integer;
      L_LOW_YEAR            pls_integer;
      L_TAX_YEAR            pls_integer;
   begin

      A_TAX_RECONCILE_REC.DELETE;
      L_INCLUDE_ID_COUNT := A_INCLUDE_IDS.COUNT;
      L_LOW_YEAR         := 0;
      -- find the bal year
      for I in 1 .. L_INCLUDE_ID_COUNT
      loop
         L_INCLUDE_ID := A_INCLUDE_IDS(I);
         begin
            L_TAX_RECONCILE_COUNT := G_TAX_RECONCILE_BAL_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP || TO_CHAR(L_INCLUDE_ID)).COUNT;
         exception
            when NO_DATA_FOUND then
               L_TAX_RECONCILE_COUNT := 0;
         end;

         for K in 1 .. L_TAX_RECONCILE_COUNT
         loop
            L_INDEX := G_TAX_RECONCILE_BAL_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP ||
                                                TO_CHAR(L_INCLUDE_ID))(K).ID;
            if (G_TAX_RECONCILE_REC(L_INDEX).TAX_YEAR > A_TAX_YEAR and
                (G_TAX_RECONCILE_REC(L_INDEX).TAX_YEAR < L_LOW_YEAR or L_LOW_YEAR = 0)) then
               L_LOW_YEAR := G_TAX_RECONCILE_REC(L_INDEX).TAX_YEAR;
            end if;
         end loop;
      end loop;

      L_ROW_COUNT := 0;
      L_TAX_YEAR  := L_LOW_YEAR;
      for I in 1 .. L_INCLUDE_ID_COUNT
      loop
         L_INCLUDE_ID := A_INCLUDE_IDS(I);
         begin
            L_TAX_RECONCILE_COUNT := G_TAX_RECONCILE_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP || TO_CHAR(L_INCLUDE_ID) || G_SEP || TO_CHAR(L_TAX_YEAR)).COUNT;
         exception
            when NO_DATA_FOUND then
               L_TAX_RECONCILE_COUNT := 0;
         end;
         for K in 1 .. L_TAX_RECONCILE_COUNT
         loop
            L_INDEX     := G_TAX_RECONCILE_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP ||
                                                TO_CHAR(L_INCLUDE_ID) || G_SEP ||
                                                TO_CHAR(L_TAX_YEAR))(K).ID;
            L_ROW_COUNT := L_ROW_COUNT + 1;
            A_TAX_RECONCILE_REC.EXTEND;
            A_TAX_RECONCILE_REC(L_ROW_COUNT) := G_TAX_RECONCILE_REC(L_INDEX);
         end loop;
      end loop;

      I := 0;
      return L_ROW_COUNT;

   exception
      when NO_DATA_FOUND then
         return 0;
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_reconcile_bals ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_TAX_RECONCILE_BALS;

   -- =============================================================================
   --  Function FILTER_TAX_LIMITATION
   -- =============================================================================
   function FILTER_TAX_LIMITATION(A_TAX_LIMITATION_SUB_REC in out nocopy TYPE_TAX_LIMITATION_REC,
                                  A_LIMIT_ID               pls_integer) return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_CODE  pls_integer;
   begin

      L_COUNT := G_TAX_LIMITATION_REC.COUNT;
      A_TAX_LIMITATION_SUB_REC.DELETE;
      L_ROW := 0;
      for I in 1 .. L_COUNT
      loop
         if G_TAX_LIMITATION_REC(I).TAX_LIMIT_ID = A_LIMIT_ID then
            L_ROW := 1 + L_ROW;
            A_TAX_LIMITATION_SUB_REC.EXTEND;
            A_TAX_LIMITATION_SUB_REC(L_ROW) := G_TAX_LIMITATION_REC(I);
         end if;
      end loop;
      return L_ROW;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_limitation ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_TAX_LIMITATION;

   -- =============================================================================
   --  Function FILTER_TAX_LIMIT
   -- =============================================================================
   function FILTER_TAX_LIMIT(A_TAX_LIMIT_SUB_REC in out nocopy TYPE_TAX_LIMIT2_REC,
                             A_RECORD_ID         pls_integer,
                             A_BOOK_ID           pls_integer) return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_INDEX pls_integer;
      L_CODE  pls_integer;
   begin

      L_COUNT := G_TAX_LIMIT2_REC.COUNT;
      if A_TAX_LIMIT_SUB_REC.EXISTS(1) then
         A_TAX_LIMIT_SUB_REC.DELETE;
      end if;
      L_ROW := 0;

      L_COUNT := G_TAX_LIMIT2_HASH(TO_CHAR(A_RECORD_ID) || G_SEP || TO_CHAR(A_BOOK_ID)).COUNT;
      for I in 1 .. L_COUNT
      loop
         L_INDEX := G_TAX_LIMIT2_HASH(TO_CHAR(A_RECORD_ID) || G_SEP || TO_CHAR(A_BOOK_ID))(I).ID;
         A_TAX_LIMIT_SUB_REC.EXTEND;
         A_TAX_LIMIT_SUB_REC(I) := G_TAX_LIMIT2_REC(L_INDEX);
      end loop;
      return L_COUNT;
   exception
      when NO_DATA_FOUND then
         return 0;
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_limit ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_TAX_LIMIT;

   -- =============================================================================
   --  Function FILTER_TAX_RATES
   -- =============================================================================
   function FILTER_TAX_RATES(A_TAX_RATES_SUB_REC in out nocopy TYPE_TAX_RATES_REC,
                             A_RATE_ID           pls_integer) return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_INDEX pls_integer;
      L_CODE  pls_integer;
   begin

      L_COUNT := G_TAX_RATES_REC.COUNT;
      if A_TAX_RATES_SUB_REC.EXISTS(1) then
         A_TAX_RATES_SUB_REC.DELETE;
      end if;
      L_ROW := 0;

      L_COUNT := G_TAX_RATES_HASH(A_RATE_ID).COUNT;
      for I in 1 .. L_COUNT
      loop
         L_INDEX := G_TAX_RATES_HASH(A_RATE_ID)(I).ID;
         A_TAX_RATES_SUB_REC.EXTEND(1);
         A_TAX_RATES_SUB_REC(I) := G_TAX_RATES_REC(L_INDEX);
      end loop;
      return L_COUNT;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_rates ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_TAX_RATES;

   -- =============================================================================
   --  Function CALC_DEPR
   -- =============================================================================
   function CALC_DEPR(A_JOB_NO number) return integer is
      cursor TAX_JOB_PARAMS_CUR is
         select VERSION, TAX_YEAR, VINTAGE, TAX_BOOK_ID, TAX_CLASS_ID, COMPANY_ID
           from TAX_JOB_PARAMS
          where JOB_NO = A_JOB_NO;
      L_VERSION       number(22, 0);
      L_TAX_YEAR      number(22, 2);
      L_VINTAGE       number(22, 0);
      L_TAX_BOOK_ID   number(22, 0);
      L_TAX_RECORD_ID number(22, 0);
      L_TAX_CLASS_ID  number(22, 0);
      L_COMPANY_ID    number(22, 0);
      type TAX_YEARS_TYPE is table of number(22, 2) index by pls_integer;
      TAX_YEARS             TAX_YEARS_TYPE;
      TAX_YEAR_COUNT        number(22, 2);
      L_CURRENT_YEAR        number(22, 0);
      L_DEPR_INDEX          pls_integer;
      L_TAX_DEPR_ROWS       pls_integer;
      L_TAX_DEPR_BALS_INDEX pls_integer;
      L_CALC_ADJ_INDEX      pls_integer;
      CODE                  integer;
      ROWS                  pls_integer;
      L_SHORT_MONTHS        pls_integer;
      L_NEXT_YEAR           number(22, 2);
      L_YEAR                number(22, 2);
      L_YEAR_COUNT          pls_integer;
      K                     pls_integer;

      L_TAX_BOOK_ACTIVITY_SUB_REC  TYPE_TAX_BOOK_ACTIVITY_REC;
      L_TAX_BOOK_ACTIVITY_SUB_ROWS pls_integer;
      L_COUNT                      pls_integer;
      L_TAX_INCLUDE_ACTIVITY_INDEX pls_integer;
      L_TAX_BOOK_RECONCILE_ROWS    pls_integer;
      L_TAX_RECONCILE_SUB_ROWS     pls_integer;
      L_TAX_RECONCILE_SUB_REC      TYPE_TAX_RECONCILE_REC;
      L_RECONCILE_BALS_SUB_ROWS    pls_integer;
      L_RECONCILE_BALS_SUB_REC     TYPE_TAX_RECONCILE_REC;
      L_TAX_LIMITATION_ROWS        pls_integer;
      L_TAX_LIMITATION_SUB_ROWS    pls_integer;
      L_TAX_RATES_ROWS             pls_integer;
      L_TAX_BOOK_ACTIVITY_ROWS     pls_integer;
      L_TAX_INCLUDE_ACTIVITY_ROWS  pls_integer;
      L_TAX_CONVENTION_ROWS        pls_integer;
      L_EXTRAORDINARY_CONV_INDEX   pls_integer;
      L_TAX_CONVENTION_INDEX       pls_integer;
      L_TAX_LIMITATION_SUB_REC     TYPE_TAX_LIMITATION_REC;
      L_TAX_JOB_CREATION_SUB_REC   TYPE_TAX_LIMIT2_REC;
      L_TAX_JOB_CREATION_SUB_ROWS  pls_integer;
      L_TAX_RATES_SUB_REC          TYPE_TAX_RATES_REC;
      L_TAX_RATES_SUB_ROWS         pls_integer;
      L_ADD_TAX_RECONCILE_REC      TYPE_TAX_RECONCILE_REC;
      L_ADD_TAX_RECONCILE_ROWS     pls_integer;
      L_CODE                       pls_integer;
      L_START_ROW                  pls_integer;
      L_TOTAL_ROWS                 pls_integer := 0;
      M                            pls_integer;
   begin
      G_START_TIME := CURRENT_TIMESTAMP;
      --pp_plsql_debug.debug_procedure_on('fast_tax');
      G_JOB_NO  := A_JOB_NO;
      G_LINE_NO := 1;
      WRITE_LOG(G_JOB_NO, 0, 0, 'Tax Depreciation  Started Version=' || GET_VERSION());
      DBMS_OUTPUT.ENABLE(100000);
      SET_SESSION_PARAMETER();
      G_TABLE_BOOK_IDS         := TABLE_LIST_ID_TYPE(-1);
      G_TABLE_VINTAGE_IDS      := TABLE_LIST_ID_TYPE();
      G_TABLE_CLASS_IDS        := TABLE_LIST_ID_TYPE(-1);
      G_TABLE_COMPANY_IDS      := TABLE_LIST_ID_TYPE(-1);
      G_TAX_DEPR_SAVE1_REC     := TYPE_TAX_DEPR_SAVE_REC();
      G_TAX_DEPR_SAVE2_REC     := TYPE_TAX_DEPR_SAVE_REC();
      G_TAX_CALC_ADJ_SAVE1_REC := TYPE_TAX_CALC_ADJ_SAVE_REC();
      G_TAX_CALC_ADJ_SAVE2_REC := TYPE_TAX_CALC_ADJ_SAVE_REC();

      TAX_YEAR_COUNT        := 0;
      G_TABLE_BOOK_COUNT    := 0;
      G_TABLE_VINTAGE_COUNT := 0;
      G_TABLE_CLASS_COUNT   := 0;
      G_TABLE_COMPANY_COUNT := 0;

      L_TAX_LIMITATION_SUB_REC    := TYPE_TAX_LIMITATION_REC();
      L_TAX_RATES_SUB_REC         := TYPE_TAX_RATES_REC();
      L_TAX_JOB_CREATION_SUB_REC  := TYPE_TAX_LIMIT2_REC();
      L_ADD_TAX_RECONCILE_REC     := TYPE_TAX_RECONCILE_REC();
      L_RECONCILE_BALS_SUB_REC    := TYPE_TAX_RECONCILE_REC();
      L_TAX_BOOK_ACTIVITY_SUB_REC := TYPE_TAX_BOOK_ACTIVITY_REC();

      open TAX_JOB_PARAMS_CUR;
      loop
         fetch TAX_JOB_PARAMS_CUR
            into L_VERSION, L_TAX_YEAR, L_VINTAGE, L_TAX_BOOK_ID, L_TAX_CLASS_ID, L_COMPANY_ID;
         if not L_VERSION is null then
            G_VERSION := L_VERSION;
         end if;
         if not L_TAX_YEAR is null then
            TAX_YEAR_COUNT := TAX_YEAR_COUNT + 1;
            TAX_YEARS(TAX_YEAR_COUNT) := L_TAX_YEAR;
         end if;
         if not L_VINTAGE is null then
            G_TABLE_VINTAGE_COUNT := 1 + G_TABLE_VINTAGE_COUNT;
            G_TABLE_VINTAGE_IDS.EXTEND;
            G_TABLE_VINTAGE_IDS(G_TABLE_VINTAGE_COUNT) := L_VINTAGE;
         end if;
         if not L_TAX_BOOK_ID is null then
            G_TABLE_BOOK_COUNT := 1 + G_TABLE_BOOK_COUNT;
            G_TABLE_BOOK_IDS.EXTEND;
            G_TABLE_BOOK_IDS(G_TABLE_BOOK_COUNT) := L_TAX_BOOK_ID;
         end if;
         if not L_TAX_CLASS_ID is null then
            G_TABLE_CLASS_COUNT := 1 + G_TABLE_CLASS_COUNT;
            G_TABLE_CLASS_IDS.EXTEND;
            G_TABLE_CLASS_IDS(G_TABLE_CLASS_COUNT) := L_TAX_CLASS_ID;
         end if;
         if not L_COMPANY_ID is null then
            G_TABLE_COMPANY_COUNT := 1 + G_TABLE_COMPANY_COUNT;
            G_TABLE_COMPANY_IDS.EXTEND;
            G_TABLE_COMPANY_IDS(G_TABLE_COMPANY_COUNT) := L_COMPANY_ID;
         end if;
         exit when TAX_JOB_PARAMS_CUR%notfound;
      end loop;

      G_START_YEAR := TAX_YEARS(TAX_YEARS.FIRST);
      G_END_YEAR   := TAX_YEARS(TAX_YEARS.LAST);

      L_TAX_CONVENTION_ROWS       := GET_TAX_CONVENTION();
      L_TAX_RATES_ROWS            := GET_TAX_RATES();
      L_TAX_BOOK_ACTIVITY_ROWS    := GET_TAX_BOOK_ACTIVITY();
      L_TAX_BOOK_RECONCILE_ROWS   := GET_TAX_RECONCILE();
      L_TAX_LIMITATION_ROWS       := GET_TAX_LIMITATION();
      L_TAX_INCLUDE_ACTIVITY_ROWS := GET_TAX_INCLUDE_ACTIVITY();
      L_TAX_RECONCILE_SUB_REC     := TYPE_TAX_RECONCILE_REC();

      ROWS := GET_TAX_JOB_CREATION();
      loop
         L_TAX_DEPR_ROWS := GET_TAX_DEPR(L_START_ROW);
         exit when L_TAX_DEPR_ROWS = 0;
         ROWS := GET_TAX_CALC_ADJ(L_START_ROW);

         --write_log( g_job_no,2,rows,'Tax Depreciation');
         L_CURRENT_YEAR := 0;
         L_TOTAL_ROWS   := L_TOTAL_ROWS + (L_TAX_DEPR_ROWS - L_START_ROW + 1);

         for L_DEPR_INDEX in L_START_ROW .. L_TAX_DEPR_ROWS
         loop
            L_TAX_YEAR      := G_TAX_DEPR_REC(L_DEPR_INDEX).TAX_YEAR;
            L_TAX_RECORD_ID := G_TAX_DEPR_REC(L_DEPR_INDEX).TAX_RECORD_ID;
            L_TAX_BOOK_ID   := G_TAX_DEPR_REC(L_DEPR_INDEX).TAX_BOOK_ID;

            -- see if the the tax year has changed from the previous record
            if L_CURRENT_YEAR <> G_TAX_DEPR_REC(L_DEPR_INDEX).TAX_YEAR then
               L_CURRENT_YEAR := G_TAX_DEPR_REC(L_DEPR_INDEX).TAX_YEAR;
               begin
                  select NVL(MONTHS, 0)
                    into L_SHORT_MONTHS
                    from TAX_YEAR_VERSION
                   where VERSION_ID = G_VERSION
                     and TAX_YEAR = L_CURRENT_YEAR;
                  if (L_SHORT_MONTHS <= 0 or L_SHORT_MONTHS >= 12) then
                     L_SHORT_MONTHS := 12;
                  end if;
               exception
                  when NO_DATA_FOUND then
                     L_SHORT_MONTHS := 12;
               end;
               select NVL(min(TAX_YEAR), 0)
                 into L_NEXT_YEAR
                 from TAX_YEAR_VERSION
                where VERSION_ID = G_VERSION
                  and TAX_YEAR > L_CURRENT_YEAR;
            end if;

            -- get tax depr bal record
            L_CURRENT_YEAR := 0;
            if L_TAX_RECORD_ID = 3277040 and L_TAX_YEAR = 2015 and L_TAX_BOOK_ID = 30 then
               M := 0;
            end if;
            L_TAX_DEPR_BALS_INDEX := 0;
            if G_TAX_DEPR_BALS_HASH(TO_CHAR(L_TAX_RECORD_ID) || G_SEP || TO_CHAR(L_TAX_BOOK_ID)).EXISTS(1) then
               L_YEAR_COUNT := G_TAX_DEPR_BALS_HASH(TO_CHAR(L_TAX_RECORD_ID) || G_SEP || TO_CHAR(L_TAX_BOOK_ID)).COUNT;

               for K in 1 .. L_YEAR_COUNT
               loop
                  L_YEAR := G_TAX_DEPR_BALS_HASH(TO_CHAR(L_TAX_RECORD_ID) || G_SEP ||
                                                 TO_CHAR(L_TAX_BOOK_ID))(K).TAX_YEAR;
                  if (L_YEAR > L_TAX_YEAR and (L_YEAR < L_CURRENT_YEAR or L_CURRENT_YEAR = 0)) then
                     L_CURRENT_YEAR        := L_YEAR;
                     L_TAX_DEPR_BALS_INDEX := G_TAX_DEPR_BALS_HASH(TO_CHAR(L_TAX_RECORD_ID) ||
                                                                   G_SEP || TO_CHAR(L_TAX_BOOK_ID))(K).ID;
                  end if;
               end loop;
            end if;

            begin
               -- ge taxcalc adj record as
               L_CALC_ADJ_INDEX := G_TAX_CALC_ADJ_HASH(TO_CHAR(L_TAX_RECORD_ID) || G_SEP ||
                                                       TO_CHAR(L_TAX_BOOK_ID) || G_SEP ||
                                                       TO_CHAR(L_TAX_YEAR));
            exception
               when NO_DATA_FOUND then
                  L_CALC_ADJ_INDEX := 0;
               when others then
                  CODE := sqlcode;
                  WRITE_LOG(G_JOB_NO,
                            4,
                            CODE,
                            'record id =' || TO_CHAR(L_TAX_RECORD_ID) || '' || sqlerrm(CODE) || ' ' ||
                            DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
                  return 0;
            end;
            -- get tax book activity record
            L_TAX_BOOK_ACTIVITY_SUB_ROWS := FILTER_TAX_BOOK_ACTIVITY(L_TAX_BOOK_ACTIVITY_SUB_REC,
                                                                     L_TAX_RECORD_ID,
                                                                     L_TAX_BOOK_ID,
                                                                     L_TAX_YEAR);

            -- check for any activity
            L_TAX_INCLUDE_ACTIVITY_INDEX := 0;
            for K in 1 .. L_TAX_INCLUDE_ACTIVITY_ROWS
            loop
               if G_TAX_INCLUDES_ACTIVITY_REC(K).TAX_BOOK_ID = L_TAX_BOOK_ID then
                  L_TAX_INCLUDE_ACTIVITY_INDEX := K;
                  exit;
               end if;
            end loop;
            if L_TAX_INCLUDE_ACTIVITY_INDEX <= L_TAX_INCLUDE_ACTIVITY_ROWS then
               L_TAX_RECONCILE_SUB_ROWS := FILTER_TAX_RECONCILE(L_TAX_RECONCILE_SUB_REC,
                                                                L_TAX_RECORD_ID,
                                                                G_TAX_INCLUDES_ACTIVITY_REC(L_TAX_INCLUDE_ACTIVITY_INDEX).TAX_INCLUDE_IDS,
                                                                L_TAX_YEAR);

               L_RECONCILE_BALS_SUB_ROWS := FILTER_TAX_RECONCILE_BALS(L_RECONCILE_BALS_SUB_REC,
                                                                      L_TAX_RECORD_ID,
                                                                      G_TAX_INCLUDES_ACTIVITY_REC(L_TAX_INCLUDE_ACTIVITY_INDEX).TAX_INCLUDE_IDS,
                                                                      L_TAX_YEAR);
            else
               L_TAX_RECONCILE_SUB_ROWS  := 0;
               L_RECONCILE_BALS_SUB_ROWS := 0;
            end if;

            L_TAX_CONVENTION_INDEX := G_TAX_CONVENTION_HASH(G_TAX_DEPR_REC(L_DEPR_INDEX).CONVENTION_ID);

            L_EXTRAORDINARY_CONV_INDEX := G_TAX_CONVENTION_HASH(G_TAX_DEPR_REC(L_DEPR_INDEX).EXTRAORDINARY_CONVENTION);

            L_TAX_LIMITATION_SUB_ROWS := FILTER_TAX_LIMITATION(L_TAX_LIMITATION_SUB_REC,
                                                               G_TAX_DEPR_REC(L_DEPR_INDEX).TAX_LIMIT_ID);

            L_TAX_JOB_CREATION_SUB_ROWS := FILTER_TAX_LIMIT(L_TAX_JOB_CREATION_SUB_REC,
                                                            L_TAX_RECORD_ID,
                                                            L_TAX_BOOK_ID);

            L_TAX_RATES_SUB_ROWS := FILTER_TAX_RATES(L_TAX_RATES_SUB_REC,
                                                     G_TAX_DEPR_REC(L_DEPR_INDEX).TAX_RATE_ID);

            if L_TAX_RATES_SUB_ROWS <> 0 then
               L_CODE := CALC(L_DEPR_INDEX,
                              L_TAX_DEPR_BALS_INDEX,
                              L_CALC_ADJ_INDEX,
                              L_TAX_DEPR_BALS_INDEX,
                              G_TAX_CONVENTION_REC(L_TAX_CONVENTION_INDEX),
                              L_TAX_CONVENTION_INDEX,
                              G_TAX_CONVENTION_REC(L_EXTRAORDINARY_CONV_INDEX),
                              L_EXTRAORDINARY_CONV_INDEX,
                              L_TAX_LIMITATION_SUB_REC,
                              L_TAX_LIMITATION_SUB_ROWS,
                              L_TAX_RECONCILE_SUB_REC,
                              L_TAX_RECONCILE_SUB_ROWS,
                              L_RECONCILE_BALS_SUB_REC,
                              L_RECONCILE_BALS_SUB_ROWS,
                              L_TAX_JOB_CREATION_SUB_REC,
                              L_TAX_JOB_CREATION_SUB_ROWS,
                              L_TAX_BOOK_ACTIVITY_SUB_REC,
                              L_TAX_BOOK_ACTIVITY_SUB_ROWS,
                              L_TAX_RATES_SUB_REC,
                              L_TAX_RATES_SUB_ROWS,
                              L_SHORT_MONTHS,
                              L_ADD_TAX_RECONCILE_REC,
                              L_ADD_TAX_RECONCILE_ROWS,
                              L_NEXT_YEAR);
               if L_CODE = -1 then
                  return - 1;
               end if;

            end if;
         end loop;
         L_CODE := UPDATE_TAX_DEPR(L_START_ROW, L_TAX_DEPR_ROWS);
         if L_CODE = -1 then
            return - 1;
         end if;
      end loop;
      WRITE_LOG(G_JOB_NO, 0, 0, 'Total Calculation Rows=' || TO_CHAR(L_TOTAL_ROWS));
      L_CODE := INSERT_TAX_BOOK_RECONCILE(L_ADD_TAX_RECONCILE_ROWS, L_ADD_TAX_RECONCILE_REC);
      L_CODE := UPDATE_TAX_BOOK_RECONCILE(L_TAX_BOOK_RECONCILE_ROWS);
      commit;
      WRITE_LOG(G_JOB_NO, 0, 0, 'Finished');
      --pp_plsql_debug.debug_procedure_off('fast_tax');
      return 0;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end;

   -- =============================================================================
   --  Function GET_TAX_DEPR
   -- =============================================================================
   function GET_TAX_DEPR(A_START_ROW out pls_integer) return integer is
      L_COUNT             integer;
      L_SAVE_COUNT        pls_integer;
      I                   pls_integer;
      J                   pls_integer;
      K                   pls_integer;
      CODE                pls_integer;
      L_CURRENT_RECORD_ID pls_integer;
      L_CURRENT_BOOK_ID   pls_integer;
      L_YEAR_COUNT        pls_integer;
      type ARRAY_OF_INTGERS is table of pls_integer;
      L_RECORDS_NOT_USERS            ARRAY_OF_INTGERS;
      L_TAX_DEPR_LAST_SAVE_RECORD_ID pls_integer;
      L_TAX_DEPR_LAST_SAVE_BOOK_ID   pls_integer;
      L_MOVE_ROW_COUNT               pls_integer;
   begin
      if not TAX_DEPR_CUR%isopen then
         WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Depreciation Rows: 0 ');
         open TAX_DEPR_CUR;
      else
         G_TAX_DEPR_HASH.DELETE;
         G_TAX_DEPR_BALS_HASH.DELETE;
      end if;
      A_START_ROW := 1;
      fetch TAX_DEPR_CUR bulk collect
         into G_TAX_DEPR_REC limit G_LIMIT;

      I       := G_TAX_DEPR_REC.FIRST;
      L_COUNT := G_TAX_DEPR_REC.COUNT;

      if L_COUNT = 0 then
         close TAX_DEPR_CUR;

      else
         G_TAX_DEPR_LAST_RECORD_ID := G_TAX_DEPR_REC(L_COUNT).TAX_RECORD_ID;
         G_TAX_DEPR_LAST_BOOK_ID   := G_TAX_DEPR_REC(L_COUNT).TAX_BOOK_ID;
      end if;

      -- save the last records that are not complete
      K := 0;
      G_TAX_DEPR_SAVE2_REC.DELETE;
      if L_COUNT = G_LIMIT then
         J := 1;
         for J in reverse 1 .. L_COUNT
         loop
            if G_TAX_DEPR_LAST_RECORD_ID = G_TAX_DEPR_REC(J).TAX_RECORD_ID and
               G_TAX_DEPR_LAST_BOOK_ID = G_TAX_DEPR_REC(J).TAX_BOOK_ID then
               G_TAX_DEPR_SAVE2_REC.EXTEND;
               K := K + 1;
               G_TAX_DEPR_SAVE2_REC(K) := G_TAX_DEPR_REC(J);
            else
               exit;
            end if;
         end loop;
         L_COUNT := L_COUNT - K;
      else
         G_TAX_DEPR_LAST_RECORD_ID := 0;
         G_TAX_DEPR_LAST_BOOK_ID   := 0;
      end if;

      -- restore the save records
      J := 0;

      if G_TAX_DEPR_SAVE1_REC.COUNT > 0 then
         L_SAVE_COUNT := G_TAX_DEPR_SAVE1_REC.COUNT;
         K            := 0;
         for J in 1 .. L_SAVE_COUNT
         loop
            G_TAX_DEPR_REC.EXTEND;
            G_TAX_DEPR_REC(L_COUNT + J) := G_TAX_DEPR_SAVE1_REC(J);
            K := K + 1;
         end loop;
         L_COUNT := L_COUNT + L_SAVE_COUNT;
         -- we must reorganize the collection
         L_TAX_DEPR_LAST_SAVE_RECORD_ID := G_TAX_DEPR_SAVE1_REC(1).TAX_RECORD_ID;
         L_TAX_DEPR_LAST_SAVE_BOOK_ID   := G_TAX_DEPR_SAVE1_REC(1).TAX_BOOK_ID;
         L_MOVE_ROW_COUNT               := 0;
         for J in 1 .. L_COUNT
         loop
            if L_TAX_DEPR_LAST_SAVE_RECORD_ID = G_TAX_DEPR_REC(J).TAX_RECORD_ID and
               L_TAX_DEPR_LAST_SAVE_BOOK_ID = G_TAX_DEPR_REC(J).TAX_BOOK_ID then
               L_MOVE_ROW_COUNT := L_MOVE_ROW_COUNT + 1;
            else
               exit;
            end if;
         end loop;

         for J in 1 .. L_MOVE_ROW_COUNT
         loop
            G_TAX_DEPR_REC.EXTEND;
            G_TAX_DEPR_REC(L_COUNT + J) := G_TAX_DEPR_REC(J);
         end loop;
         L_COUNT     := L_COUNT + L_MOVE_ROW_COUNT;
         A_START_ROW := L_MOVE_ROW_COUNT + 1;
      end if;

      G_TAX_DEPR_SAVE1_REC := G_TAX_DEPR_SAVE2_REC;

      -- create an index to the data

      I                   := A_START_ROW;
      L_CURRENT_RECORD_ID := 0;
      L_CURRENT_BOOK_ID   := 0;
      while I <= L_COUNT
      loop
         /*if g_tax_depr_last_record_id = g_tax_depr_rec(i).tax_record_id and g_tax_depr_last_book_id = g_tax_depr_rec(i).tax_book_id then
            i := i + 1;
            continue;
         end if;*/
         G_TAX_DEPR_HASH(TO_CHAR(G_TAX_DEPR_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_TAX_DEPR_REC(I).TAX_BOOK_ID) || G_SEP || TO_CHAR(G_TAX_DEPR_REC(I).TAX_YEAR)) := I;

         if L_CURRENT_RECORD_ID = G_TAX_DEPR_REC(I).TAX_RECORD_ID and
            L_CURRENT_BOOK_ID = G_TAX_DEPR_REC(I).TAX_BOOK_ID then
            L_YEAR_COUNT := L_YEAR_COUNT + 1;
            G_TAX_DEPR_BALS_HASH(TO_CHAR(G_TAX_DEPR_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_TAX_DEPR_REC(I).TAX_BOOK_ID))(L_YEAR_COUNT).TAX_YEAR := G_TAX_DEPR_REC(I).TAX_YEAR;
            G_TAX_DEPR_BALS_HASH(TO_CHAR(G_TAX_DEPR_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_TAX_DEPR_REC(I).TAX_BOOK_ID))(L_YEAR_COUNT).ID := I;
         else
            L_YEAR_COUNT        := 1;
            L_CURRENT_RECORD_ID := G_TAX_DEPR_REC(I).TAX_RECORD_ID;
            L_CURRENT_BOOK_ID   := G_TAX_DEPR_REC(I).TAX_BOOK_ID;
            if L_CURRENT_RECORD_ID = 117152 then
               J := 0;
            end if;
            G_TAX_DEPR_BALS_HASH(TO_CHAR(L_CURRENT_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_BOOK_ID))(L_YEAR_COUNT).TAX_YEAR := G_TAX_DEPR_REC(I).TAX_YEAR;
            G_TAX_DEPR_BALS_HASH(TO_CHAR(L_CURRENT_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_BOOK_ID))(L_YEAR_COUNT).ID := I;
         end if;
         I := I + 1;
      end loop;

      -- g_tax_depr_rec.delete;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Depreciation Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;

   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   CODE,
                   ' Tax Depr ' || sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_DEPR;

   -- =============================================================================
   --  Function GET_TAX_CALC_ADJ
   -- =============================================================================
   function GET_TAX_CALC_ADJ(A_START_ROW out pls_integer) return integer is
      L_COUNT                       integer;
      L_SAVE_COUNT                  pls_integer;
      I                             pls_integer;
      J                             pls_integer := 0;
      K                             pls_integer;
      CODE                          pls_integer;
      L_TAX_CALC_ADJ_SAVE_RECORD_ID pls_integer;
      L_TAX_CALC_ADJ_SAVE_BOOK_ID   pls_integer;
      L_MOVE_ROW_COUNT              pls_integer;
   begin
      A_START_ROW := 1;
      if G_TAX_CALC_ADJ_REC.EXISTS(1) then
         G_TAX_CALC_ADJ_REC.DELETE;
      end if;
      if not TAX_CALC_ADJ_CUR%isopen then
         open TAX_CALC_ADJ_CUR;
      end if;

      fetch TAX_CALC_ADJ_CUR bulk collect
         into G_TAX_CALC_ADJ_REC limit G_LIMIT;

      I       := G_TAX_CALC_ADJ_REC.FIRST;
      L_COUNT := G_TAX_CALC_ADJ_REC.COUNT;

      if L_COUNT = 0 then
         close TAX_CALC_ADJ_CUR;
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(sysdate, 'HH24:MI:SS') || 'Tax Calc Rows: ' ||
                              TO_CHAR(L_COUNT));
         return 0;
      end if;

      -- save the last records that are not complete

      G_TAX_CALC_ADJ_SAVE2_REC.DELETE;
      if L_COUNT = G_LIMIT then
         K := 0;
         for J in reverse 1 .. L_COUNT
         loop
            if G_TAX_DEPR_LAST_RECORD_ID = G_TAX_CALC_ADJ_REC(J).TAX_RECORD_ID and
               G_TAX_DEPR_LAST_BOOK_ID = G_TAX_CALC_ADJ_REC(J).TAX_BOOK_ID then
               G_TAX_CALC_ADJ_SAVE2_REC.EXTEND;
               K := K + 1;
               G_TAX_CALC_ADJ_SAVE2_REC(K) := G_TAX_CALC_ADJ_REC(J);
            else
               exit;
            end if;
         end loop;
         L_COUNT := L_COUNT - K;
      end if;

      -- restore the save records
      if G_TAX_CALC_ADJ_SAVE1_REC.COUNT > 0 then
         L_SAVE_COUNT := G_TAX_CALC_ADJ_SAVE1_REC.COUNT;
         K            := 0;
         for J in 1 .. L_SAVE_COUNT
         loop
            if G_TAX_CALC_ADJ_REC.EXISTS(L_COUNT + J) = false then
               G_TAX_CALC_ADJ_REC.EXTEND;
            end if;
            K := K + 1;
            G_TAX_CALC_ADJ_REC(L_COUNT + J) := G_TAX_CALC_ADJ_SAVE1_REC(J);
         end loop;
         L_COUNT := L_COUNT + L_SAVE_COUNT;
         -- we must reorganize the collection
         L_TAX_CALC_ADJ_SAVE_RECORD_ID := G_TAX_CALC_ADJ_REC(1).TAX_RECORD_ID;
         L_TAX_CALC_ADJ_SAVE_BOOK_ID   := G_TAX_CALC_ADJ_REC(1).TAX_BOOK_ID;
         L_MOVE_ROW_COUNT              := 0;
         for J in 1 .. L_COUNT
         loop
            if L_TAX_CALC_ADJ_SAVE_RECORD_ID = G_TAX_CALC_ADJ_REC(J).TAX_RECORD_ID and
               L_TAX_CALC_ADJ_SAVE_BOOK_ID = G_TAX_CALC_ADJ_REC(J).TAX_BOOK_ID then
               L_MOVE_ROW_COUNT := L_MOVE_ROW_COUNT + 1;
            else
               exit;
            end if;
         end loop;

         for J in 1 .. L_MOVE_ROW_COUNT
         loop
            if G_TAX_CALC_ADJ_REC.EXISTS(L_COUNT + J) = false then
               G_TAX_CALC_ADJ_REC.EXTEND;
            end if;
            G_TAX_CALC_ADJ_REC(L_COUNT + J) := G_TAX_CALC_ADJ_REC(J);
         end loop;
         L_COUNT     := L_COUNT + L_MOVE_ROW_COUNT;
         A_START_ROW := L_MOVE_ROW_COUNT + 1;
      end if;

      G_TAX_CALC_ADJ_SAVE1_REC := G_TAX_CALC_ADJ_SAVE2_REC;

      -- create an index to the data

      I := A_START_ROW;
      while I <= L_COUNT
      loop
         if G_TAX_DEPR_LAST_RECORD_ID = G_TAX_CALC_ADJ_REC(I).TAX_RECORD_ID and
            G_TAX_DEPR_LAST_BOOK_ID = G_TAX_CALC_ADJ_REC(I).TAX_BOOK_ID then
            I := I + 1;
            CONTINUE;
         end if;
         G_TAX_CALC_ADJ_HASH(TO_CHAR(G_TAX_CALC_ADJ_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_TAX_CALC_ADJ_REC(I).TAX_BOOK_ID) || G_SEP || TO_CHAR(G_TAX_CALC_ADJ_REC(I).TAX_YEAR)) := I;
         I := I + 1;
      end loop;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Calc Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         DBMS_OUTPUT.PUT_LINE(sqlerrm(CODE) || ' Rows=' || TO_CHAR(I) || ' ' ||
                              DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         WRITE_LOG(G_JOB_NO,
                   4,
                   CODE,
                   sqlerrm(CODE) || ' ' || TO_CHAR(I) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_CALC_ADJ;

   -- =============================================================================
   --  Function GET_TAX_CONVENTION
   -- =============================================================================
   function GET_TAX_CONVENTION return integer is
      L_COUNT integer;
      I       pls_integer;
      CODE    pls_integer;
   begin
      if not TAX_CONVENTION_CUR%isopen then
         open TAX_CONVENTION_CUR;
      end if;

      fetch TAX_CONVENTION_CUR bulk collect
         into G_TAX_CONVENTION_REC;

      I       := G_TAX_CONVENTION_REC.FIRST;
      L_COUNT := G_TAX_CONVENTION_REC.COUNT;

      if L_COUNT = 0 then
         close TAX_CONVENTION_CUR;
      end if;

      I := 1;
      while I <= L_COUNT
      loop
         G_TAX_CONVENTION_HASH(G_TAX_CONVENTION_REC(I).CONVENTION_ID) := I;
         I := I + 1;
      end loop;

      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Convention Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_CONVENTION;

   -- =============================================================================
   --  Function GET_TAX_RATES
   -- =============================================================================
   function GET_TAX_RATES return integer is
      L_COUNT        integer;
      I              pls_integer;
      CODE           pls_integer;
      L_LAST_RATE_ID pls_integer;
      L_ROWS         pls_integer;
   begin
      if not TAX_RATES_CUR%isopen then
         open TAX_RATES_CUR;
      end if;

      fetch TAX_RATES_CUR bulk collect
         into G_TAX_RATES_REC;

      I       := G_TAX_RATES_REC.FIRST;
      L_COUNT := G_TAX_RATES_REC.COUNT;

      if L_COUNT = 0 then
         close TAX_RATES_CUR;
      end if;

      -- create an index to the data
      L_COUNT        := G_TAX_RATES_REC.COUNT;
      I              := 1;
      L_LAST_RATE_ID := 0;
      while I <= L_COUNT
      loop
         if G_TAX_RATES_REC(I).TAX_RATE_ID = L_LAST_RATE_ID then
            L_ROWS := L_ROWS + 1;
            G_TAX_RATES_HASH(TO_CHAR(L_LAST_RATE_ID))(L_ROWS).ID := I;
         else
            L_ROWS := 1;
            L_LAST_RATE_ID := G_TAX_RATES_REC(I).TAX_RATE_ID;
            G_TAX_RATES_HASH(TO_CHAR(L_LAST_RATE_ID))(L_ROWS).ID := I;
         end if;
         I := I + 1;
      end loop;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Rates Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_RATES;

   -- =============================================================================
   --  Function GET_TAX_BOOK_ACTIVITY
   -- =============================================================================
   function GET_TAX_BOOK_ACTIVITY return integer is
      L_COUNT     integer;
      I           pls_integer;
      CODE        pls_integer;
      L_ROW_COUNT pls_integer;
      L_FOUND     boolean;
   begin
      if not TAX_BOOK_ACTIVITY_CUR%isopen then
         open TAX_BOOK_ACTIVITY_CUR;
      end if;

      fetch TAX_BOOK_ACTIVITY_CUR bulk collect
         into G_TAX_BOOK_ACTIVITY_REC;

      I       := G_TAX_BOOK_ACTIVITY_REC.FIRST;
      L_COUNT := G_TAX_BOOK_ACTIVITY_REC.COUNT;

      close TAX_BOOK_ACTIVITY_CUR;

      -- create an index to the data
      L_COUNT     := G_TAX_BOOK_ACTIVITY_REC.COUNT;
      I           := 1;
      L_ROW_COUNT := 0;
      while I <= L_COUNT
      loop
         begin
            if G_TAX_BOOK_ACTIVITY_REC(I).TAX_RECORD_ID = 18113 then
               null;
            end if;
            L_FOUND := G_TAX_BOOK_ACTIVITY_HASH(TO_CHAR(G_TAX_BOOK_ACTIVITY_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_TAX_BOOK_ACTIVITY_REC(I).TAX_BOOK_ID) || G_SEP || TO_CHAR(G_TAX_BOOK_ACTIVITY_REC(I).TAX_YEAR)).EXISTS(1);
         exception
            when NO_DATA_FOUND then
               L_FOUND := false;
         end;
         if L_FOUND = true then
            L_ROW_COUNT := 1 + L_ROW_COUNT;
         else
            L_ROW_COUNT := 1;
         end if;
         G_TAX_BOOK_ACTIVITY_HASH(TO_CHAR(G_TAX_BOOK_ACTIVITY_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_TAX_BOOK_ACTIVITY_REC(I).TAX_BOOK_ID) || G_SEP || TO_CHAR(G_TAX_BOOK_ACTIVITY_REC(I).TAX_YEAR))(L_ROW_COUNT) := I;
         I := I + 1;
      end loop;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Book Activity Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   CODE,
                   'Tax Book Activity i=' || TO_CHAR(I) || ' ' || sqlerrm(CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_BOOK_ACTIVITY;

   -- =============================================================================
   --  Function GET_TAX_RECONCILE
   -- =============================================================================
   function GET_TAX_RECONCILE return integer is
      L_COUNT                  integer;
      I                        pls_integer;
      CODE                     pls_integer;
      L_CURRENT_RECORD_ID      pls_integer;
      L_CURRENT_INCLUDE_ID     pls_integer;
      L_CURRENT_BAL_RECORD_ID  pls_integer;
      L_CURRENT_BAL_INCLUDE_ID pls_integer;

      L_YEAR_COUNT              pls_integer;
      L_TAX_RECONCILE_BAL_COUNT pls_integer;
      L_TAX_RECONCILE_COUNT     pls_integer;
      L_CURRENT_YEAR            number(22, 2);
   begin
      if not TAX_RECONCILE_CUR%isopen then
         open TAX_RECONCILE_CUR;
      end if;

      fetch TAX_RECONCILE_CUR bulk collect
         into G_TAX_RECONCILE_REC;

      I       := G_TAX_RECONCILE_REC.FIRST;
      L_COUNT := G_TAX_RECONCILE_REC.COUNT;

      if L_COUNT = 0 then
         close TAX_RECONCILE_CUR;
      end if;

      -- create an index to the data
      L_COUNT                   := G_TAX_RECONCILE_REC.COUNT;
      I                         := 1;
      L_CURRENT_BAL_RECORD_ID   := 0;
      L_CURRENT_BAL_INCLUDE_ID  := 0;
      L_CURRENT_RECORD_ID       := 0;
      L_CURRENT_INCLUDE_ID      := 0;
      L_CURRENT_YEAR            := 0;
      L_TAX_RECONCILE_BAL_COUNT := 0;
      while I <= L_COUNT
      loop
         if L_CURRENT_RECORD_ID = G_TAX_RECONCILE_REC(I).TAX_RECORD_ID and
            L_CURRENT_INCLUDE_ID = G_TAX_RECONCILE_REC(I).TAX_INCLUDE_ID and G_TAX_RECONCILE_REC(I).TAX_YEAR = L_CURRENT_YEAR then
            L_TAX_RECONCILE_COUNT := 1 + L_TAX_RECONCILE_COUNT;

         else
            L_TAX_RECONCILE_COUNT := 1;
            L_CURRENT_RECORD_ID   := G_TAX_RECONCILE_REC(I).TAX_RECORD_ID;
            L_CURRENT_INCLUDE_ID  := G_TAX_RECONCILE_REC(I).TAX_INCLUDE_ID;
            L_CURRENT_YEAR        := G_TAX_RECONCILE_REC(I).TAX_YEAR;

         end if;
         G_TAX_RECONCILE_HASH(TO_CHAR(L_CURRENT_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_INCLUDE_ID) || G_SEP || TO_CHAR(L_CURRENT_YEAR))(L_TAX_RECONCILE_COUNT).RECONCILE_ITEM := G_TAX_RECONCILE_REC(I).RECONCILE_ITEM_ID;
         G_TAX_RECONCILE_HASH(TO_CHAR(L_CURRENT_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_INCLUDE_ID) || G_SEP || TO_CHAR(L_CURRENT_YEAR))(L_TAX_RECONCILE_COUNT).ID := I;

         if L_CURRENT_BAL_RECORD_ID = G_TAX_RECONCILE_REC(I).TAX_RECORD_ID and
            L_CURRENT_BAL_INCLUDE_ID = G_TAX_RECONCILE_REC(I).TAX_INCLUDE_ID then
            L_TAX_RECONCILE_BAL_COUNT := L_TAX_RECONCILE_BAL_COUNT + 1;
         else
            L_TAX_RECONCILE_BAL_COUNT := 1;
            L_CURRENT_BAL_INCLUDE_ID  := G_TAX_RECONCILE_REC(I).TAX_INCLUDE_ID;
            L_CURRENT_BAL_RECORD_ID   := G_TAX_RECONCILE_REC(I).TAX_RECORD_ID;
         end if;

         G_TAX_RECONCILE_BAL_HASH(TO_CHAR(L_CURRENT_BAL_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_BAL_INCLUDE_ID))(L_TAX_RECONCILE_BAL_COUNT).TAX_YEAR := G_TAX_RECONCILE_REC(I).TAX_YEAR;
         G_TAX_RECONCILE_BAL_HASH(TO_CHAR(L_CURRENT_BAL_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_BAL_INCLUDE_ID))(L_TAX_RECONCILE_BAL_COUNT).RECONCILE_ITEM := G_TAX_RECONCILE_REC(I).RECONCILE_ITEM_ID;
         G_TAX_RECONCILE_BAL_HASH(TO_CHAR(L_CURRENT_BAL_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_BAL_INCLUDE_ID))(L_TAX_RECONCILE_BAL_COUNT).ID := I;

         I := I + 1;
      end loop;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Reconcile Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_RECONCILE;

   -- =============================================================================
   --  Function GET_TAX_LIMITATION
   -- =============================================================================
   function GET_TAX_LIMITATION return integer is
      L_COUNT integer;
      I       pls_integer;
      CODE    pls_integer;
   begin
      if not TAX_LIMITATION_CUR%isopen then
         open TAX_LIMITATION_CUR;
      end if;

      fetch TAX_LIMITATION_CUR bulk collect
         into G_TAX_LIMITATION_REC;

      I       := G_TAX_LIMITATION_REC.FIRST;
      L_COUNT := G_TAX_LIMITATION_REC.COUNT;

      if L_COUNT = 0 then
         close TAX_LIMITATION_CUR;
      end if;

      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax limitation Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_LIMITATION;

   -- =============================================================================
   --  Function GET_TAX_INCLUDE_ACTIVITY
   -- =============================================================================
   function GET_TAX_INCLUDE_ACTIVITY return integer is
      L_COUNT          integer;
      I                pls_integer;
      CODE             pls_integer;
      L_TAX_BOOK_ID    pls_integer;
      L_PREV_BOOK_ID   pls_integer;
      L_TAX_INCLUDE_ID pls_integer;
      L_ID_COUNT       pls_integer;
      L_ROW_COUNT      pls_integer := 0;
   begin
      I                           := 0;
      G_TAX_INCLUDES_ACTIVITY_REC := TAX_INCLUDES_ACTIVITY_TYPE();
      L_PREV_BOOK_ID              := -111111;
      open TAX_INCLUDES_ACTIVITY_CUR;
      loop
         fetch TAX_INCLUDES_ACTIVITY_CUR
            into L_TAX_BOOK_ID, L_TAX_INCLUDE_ID;
         exit when TAX_INCLUDES_ACTIVITY_CUR%notfound;
         L_ROW_COUNT := L_ROW_COUNT + 1;
         if L_TAX_BOOK_ID <> L_PREV_BOOK_ID then
            I := 1 + I;
            G_TAX_INCLUDES_ACTIVITY_REC.EXTEND;
            G_TAX_INCLUDES_ACTIVITY_REC(I).TAX_BOOK_ID := L_TAX_BOOK_ID;
            G_TAX_INCLUDES_ACTIVITY_REC(I).TAX_INCLUDE_IDS := TAX_INCLUDES_TYPE();
            G_TAX_INCLUDES_ACTIVITY_REC(I).TAX_INCLUDE_IDS.EXTEND;
            G_TAX_INCLUDES_ACTIVITY_REC(I).TAX_INCLUDE_IDS(1) := L_TAX_INCLUDE_ID;
            L_ID_COUNT := 1;
            L_PREV_BOOK_ID := L_TAX_BOOK_ID;
         else
            G_TAX_INCLUDES_ACTIVITY_REC(I).TAX_INCLUDE_IDS.EXTEND;
            L_ID_COUNT := L_ID_COUNT + 1;
            G_TAX_INCLUDES_ACTIVITY_REC(I).TAX_INCLUDE_IDS(L_ID_COUNT) := L_TAX_INCLUDE_ID;

         end if;
      end loop;
      L_COUNT := G_TAX_INCLUDES_ACTIVITY_REC.COUNT;
      close TAX_INCLUDES_ACTIVITY_CUR;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Include Acitivity Rows: ' || TO_CHAR(L_ROW_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         DBMS_OUTPUT.PUT_LINE('Tax Include Acitivity ' || sqlerrm(CODE) || ' ' ||
                              DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_INCLUDE_ACTIVITY;

   -- =============================================================================
   --  Function GET_TAX_JOB_CREATION
   -- =============================================================================
   function GET_TAX_JOB_CREATION return integer is
      L_COUNT          integer;
      I                pls_integer;
      CODE             pls_integer;
      L_ROW            pls_integer := 0;
      L_LAST_BOOK_ID   pls_integer;
      L_LAST_RECORD_ID pls_integer;
   begin
      if not TAX_LIMIT2_CUR%isopen then
         open TAX_LIMIT2_CUR;
      end if;

      fetch TAX_LIMIT2_CUR bulk collect
         into G_TAX_LIMIT2_REC;

      I       := G_TAX_LIMIT2_REC.FIRST;
      L_COUNT := G_TAX_LIMIT2_REC.COUNT;

      close TAX_LIMIT2_CUR;

      -- create an index to the data
      L_COUNT          := G_TAX_LIMIT2_REC.COUNT;
      I                := 1;
      L_LAST_RECORD_ID := 0;
      L_LAST_BOOK_ID   := 0;
      while I <= L_COUNT
      loop
         if G_TAX_LIMIT2_REC(I).TAX_RECORD_ID = 56529 and G_TAX_LIMIT2_REC(I).TAX_BOOK_ID = 10 then
            null;
         end if;
         if G_TAX_LIMIT2_REC(I)
          .TAX_RECORD_ID = L_LAST_RECORD_ID and G_TAX_LIMIT2_REC(I).TAX_BOOK_ID = L_LAST_BOOK_ID then
            L_ROW := 1 + L_ROW;
         else
            L_ROW            := 1;
            L_LAST_RECORD_ID := G_TAX_LIMIT2_REC(I).TAX_RECORD_ID;
            L_LAST_BOOK_ID   := G_TAX_LIMIT2_REC(I).TAX_BOOK_ID;
         end if;
         G_TAX_LIMIT2_HASH(TO_CHAR(G_TAX_LIMIT2_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_TAX_LIMIT2_REC(I).TAX_BOOK_ID))(L_ROW).ID := I;
         I := I + 1;
      end loop;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Job Creation Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_JOB_CREATION;

   -- =============================================================================
   --  Function UPDATE_TAX_DEPR
   -- =============================================================================
   function UPDATE_TAX_DEPR(A_START_ROW pls_integer,
                            A_NUM_REC   pls_integer) return number as
      I      pls_integer;
      L_CODE pls_integer;
   begin
      forall I in A_START_ROW .. A_NUM_REC
         update TAX_DEPRECIATION
            set BOOK_BALANCE = G_TAX_DEPR_REC(I).BOOK_BALANCE,
                TAX_BALANCE = G_TAX_DEPR_REC(I).TAX_BALANCE,
                REMAINING_LIFE = G_TAX_DEPR_REC(I).REMAINING_LIFE,
                ACCUM_RESERVE = G_TAX_DEPR_REC(I).ACCUM_RESERVE,
                SL_RESERVE = G_TAX_DEPR_REC(I).SL_RESERVE,
                DEPRECIABLE_BASE = G_TAX_DEPR_REC(I).DEPRECIABLE_BASE,
                FIXED_DEPRECIABLE_BASE = G_TAX_DEPR_REC(I).FIXED_DEPRECIABLE_BASE,
                ACTUAL_SALVAGE = G_TAX_DEPR_REC(I).ACTUAL_SALVAGE,
                ESTIMATED_SALVAGE_END = G_TAX_DEPR_REC(I).ESTIMATED_SALVAGE_END,
                ACCUM_SALVAGE = G_TAX_DEPR_REC(I).ACCUM_SALVAGE,
                ADDITIONS = G_TAX_DEPR_REC(I).ADDITIONS, TRANSFERS = G_TAX_DEPR_REC(I).TRANSFERS,
                ADJUSTMENTS = G_TAX_DEPR_REC(I).ADJUSTMENTS,
                RETIREMENTS = G_TAX_DEPR_REC(I).RETIREMENTS,
                EXTRAORDINARY_RETIRES = G_TAX_DEPR_REC(I).EXTRAORDINARY_RETIRES,
                ACCUM_ORDINARY_RETIRES = G_TAX_DEPR_REC(I).ACCUM_ORDINARY_RETIRES,
                COST_OF_REMOVAL = G_TAX_DEPR_REC(I).COST_OF_REMOVAL,
                EST_SALVAGE_PCT = G_TAX_DEPR_REC(I).EST_SALVAGE_PCT,
                RETIRE_INVOL_CONV = G_TAX_DEPR_REC(I).RETIRE_INVOL_CONV,
                SALVAGE_INVOL_CONV = G_TAX_DEPR_REC(I).SALVAGE_INVOL_CONV,
                SALVAGE_EXTRAORD = G_TAX_DEPR_REC(I).SALVAGE_EXTRAORD,
                RESERVE_AT_SWITCH = G_TAX_DEPR_REC(I).RESERVE_AT_SWITCH,
                RESERVE_AT_SWITCH_END = G_TAX_DEPR_REC(I).RESERVE_AT_SWITCH_END,
                DEPRECIATION = G_TAX_DEPR_REC(I).DEPRECIATION,
                GAIN_LOSS = G_TAX_DEPR_REC(I).GAIN_LOSS,
                EX_GAIN_LOSS = G_TAX_DEPR_REC(I).EX_GAIN_LOSS,
                CAPITAL_GAIN_LOSS = G_TAX_DEPR_REC(I).CAPITAL_GAIN_LOSS,
                CALC_DEPRECIATION = G_TAX_DEPR_REC(I).CALC_DEPRECIATION,
                OVER_ADJ_DEPRECIATION = G_TAX_DEPR_REC(I).OVER_ADJ_DEPRECIATION,
                RETIRE_RES_IMPACT = G_TAX_DEPR_REC(I).RETIRE_RES_IMPACT,
                EX_RETIRE_RES_IMPACT = G_TAX_DEPR_REC(I).EX_RETIRE_RES_IMPACT,
                TRANSFER_RES_IMPACT = G_TAX_DEPR_REC(I).TRANSFER_RES_IMPACT,
                SALVAGE_RES_IMPACT = G_TAX_DEPR_REC(I).SALVAGE_RES_IMPACT,
                ADJUSTED_RETIRE_BASIS = G_TAX_DEPR_REC(I).ADJUSTED_RETIRE_BASIS,
                BOOK_BALANCE_END = G_TAX_DEPR_REC(I).BOOK_BALANCE_END,
                TAX_BALANCE_END = G_TAX_DEPR_REC(I).TAX_BALANCE_END,
                ACCUM_RESERVE_END = G_TAX_DEPR_REC(I).ACCUM_RESERVE_END,
                SL_RESERVE_END = G_TAX_DEPR_REC(I).SL_RESERVE_END,
                ACCUM_SALVAGE_END = G_TAX_DEPR_REC(I).ACCUM_SALVAGE_END,
                ACCUM_ORDIN_RETIRES_END = G_TAX_DEPR_REC(I).ACCUM_ORDIN_RETIRES_END,
                COR_EXPENSE = G_TAX_DEPR_REC(I).COR_EXPENSE,
                COR_RES_IMPACT = G_TAX_DEPR_REC(I).COR_RES_IMPACT,
                JOB_CREATION_AMOUNT = G_TAX_DEPR_REC(I).JOB_CREATION_AMOUNT,
                NUMBER_MONTHS_BEG = G_TAX_DEPR_REC(I).NUMBER_MONTHS_BEG,
                NUMBER_MONTHS_END = G_TAX_DEPR_REC(I).NUMBER_MONTHS_END,
                QUANTITY_END = G_TAX_DEPR_REC(I).QUANTITY
          where TAX_BOOK_ID = G_TAX_DEPR_REC(I).TAX_BOOK_ID
            and TAX_RECORD_ID = G_TAX_DEPR_REC(I).TAX_RECORD_ID
            and TAX_YEAR = G_TAX_DEPR_REC(I).TAX_YEAR;
      WRITE_LOG(G_JOB_NO, 3, 0, 'Update Tax Depreciation Rows=' || TO_CHAR(A_NUM_REC));
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'Update Tax Depr ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end UPDATE_TAX_DEPR;

   -- =============================================================================
   --  Function INSERT_TAX_BOOK_RECONCILE
   -- =============================================================================
   function INSERT_TAX_BOOK_RECONCILE(A_NUM_REC               pls_integer,
                                      A_ADD_TAX_RECONCILE_REC in out nocopy TYPE_TAX_RECONCILE_REC)
      return number as
      I      pls_integer;
      L_CODE pls_integer;
   begin
      forall I in 1 .. A_NUM_REC
         insert into TAX_BOOK_RECONCILE
            (TAX_INCLUDE_ID, TAX_RECORD_ID, TAX_YEAR, RECONCILE_ITEM_ID, BASIS_AMOUNT_BEG,
             BASIS_AMOUNT_END, BASIS_AMOUNT_ACTIVITY, BASIS_AMOUNT_TRANSFER)
         --calced,");
         --depr_deduction ");
         values
            (A_ADD_TAX_RECONCILE_REC(I).TAX_INCLUDE_ID, A_ADD_TAX_RECONCILE_REC(I).TAX_RECORD_ID,
             A_ADD_TAX_RECONCILE_REC(I).TAX_YEAR, A_ADD_TAX_RECONCILE_REC(I).RECONCILE_ITEM_ID,
             A_ADD_TAX_RECONCILE_REC(I).BASIS_AMOUNT_BEG,
             A_ADD_TAX_RECONCILE_REC(I).BASIS_AMOUNT_END,
             A_ADD_TAX_RECONCILE_REC(I).BASIS_AMOUNT_ACTIVITY,
             A_ADD_TAX_RECONCILE_REC(I).BASIS_AMOUNT_TRANSFER);
      return 0;
      WRITE_LOG(G_JOB_NO, 3, 0, 'Insert Tax Book Reconcile Rows=' || TO_CHAR(A_NUM_REC));
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'Insert Tax Book Reconcile ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end INSERT_TAX_BOOK_RECONCILE;

   -- =============================================================================
   --  Function UPDATE_TAX_BOOK_RECONCILE
   -- =============================================================================
   function UPDATE_TAX_BOOK_RECONCILE(A_NUM_REC pls_integer) return number as
      I      pls_integer;
      L_CODE pls_integer;
   begin
      forall I in 1 .. A_NUM_REC
         update TAX_BOOK_RECONCILE
            set BASIS_AMOUNT_BEG = G_TAX_RECONCILE_REC(I).BASIS_AMOUNT_BEG,
                BASIS_AMOUNT_END = G_TAX_RECONCILE_REC(I).BASIS_AMOUNT_END,
                BASIS_AMOUNT_ACTIVITY = G_TAX_RECONCILE_REC(I).BASIS_AMOUNT_ACTIVITY,
                BASIS_AMOUNT_TRANSFER = G_TAX_RECONCILE_REC(I).BASIS_AMOUNT_TRANSFER
         -- calced = :SQL_ARGUMENT(tax_reconcile_calced_val),
         -- depr_deduction = :SQL_ARGUMENT(tax_reconcile_depr_deduction)
          where TAX_INCLUDE_ID = G_TAX_RECONCILE_REC(I).TAX_INCLUDE_ID
            and TAX_RECORD_ID = G_TAX_RECONCILE_REC(I).TAX_RECORD_ID
            and TAX_YEAR = G_TAX_RECONCILE_REC(I).TAX_YEAR
            and RECONCILE_ITEM_ID = G_TAX_RECONCILE_REC(I).RECONCILE_ITEM_ID;

      WRITE_LOG(G_JOB_NO, 6, 0, 'Update Tax Book Reconcile Rows=' || TO_CHAR(A_NUM_REC));
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'Update Tax Book Reconcile ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end UPDATE_TAX_BOOK_RECONCILE;
   -- ************************************** Calc ************************************************
   function CALC(A_TAX_DEPR_INDEX            integer,
                 A_TAX_DEPR_BALS_INDEX       integer,
                 A_TAX_CALC_ADJ_INDEX        integer,
                 A_TAX_DEPR_BALS_ROWS        pls_integer,
                 A_TAX_CONVENTION_REC        TAX_CONVENTION_CUR%rowtype,
                 A_TAX_CONVENTION_ROWS       pls_integer,
                 A_EXTRAORD_CONVENTION_REC   TAX_CONVENTION_CUR%rowtype,
                 A_EXTRAORD_CONVENTION_ROWS  pls_integer,
                 A_TAX_LIMITATION_SUB_REC    TYPE_TAX_LIMITATION_REC,
                 A_TAX_LIMITATION_ROWS       pls_integer,
                 A_TAX_RECONCILE_SUB_REC     in out nocopy TYPE_TAX_RECONCILE_REC,
                 A_TAX_RECONCILE_ROWS        in out nocopy pls_integer,
                 A_RECONCILE_BALS_SUB_REC    in out nocopy TYPE_TAX_RECONCILE_REC,
                 A_RECONCILE_BALS_ROWS       in out nocopy pls_integer,
                 A_TAX_JOB_CREATION_SUB_REC  TYPE_TAX_LIMIT2_REC,
                 A_TAX_JOB_CREATION_ROWS     pls_integer,
                 A_TAX_BOOK_ACTIVITY_SUB_REC TYPE_TAX_BOOK_ACTIVITY_REC,
                 A_TAX_BOOK_ACTIVITY_ROWS    pls_integer,
                 A_TAX_RATES_SUB_REC         TYPE_TAX_RATES_REC,
                 A_TAX_RATES_ROWS            pls_integer,
                 A_SHORT_MONTHS              in number,
                 A_ADD_TAX_RECONCILE_REC     in out nocopy TYPE_TAX_RECONCILE_REC,
                 A_ADD_TAX_RECONCILE_ROWS    in out nocopy integer,
                 A_NYEAR                     number) return number is

      L_TAX_JOB_CREATION_ORDERED    TYPE_TAX_LIMIT2_REC;
      L_ADD_TAX_RECONCILE_BALS_FLAG boolean;
      L_NET                         character(10);
      L_METHOD                      pls_integer;
      L_CODE                        pls_integer;
      L_RLIFE                       pls_integer;
      L_WLIFE                       pls_integer;
      L_SWITCHED                    pls_integer;
      L_HCONV                       pls_integer;
      L_TRID                        pls_integer;
      L_JROW                        pls_integer;
      L_CALC_FUTURE_YEARS           pls_integer;
      L_CALCED                      pls_integer;
      L_DEPR_DEDUCTION              pls_integer;
      L_COMPARE_RATE                pls_integer;

      L_LIFE      BINARY_DOUBLE;
      L_ADD_RATIO BINARY_DOUBLE;

      L_REM_LIFE                   BINARY_DOUBLE := 0;
      L_FLIFE                      BINARY_DOUBLE := 0;
      L_JOB_CREATION               BINARY_DOUBLE;
      L_JOB_CREATION_PERCENT       BINARY_DOUBLE;
      L_JOB_CREATION_AMOUNT        BINARY_DOUBLE := 0;
      L_RESERVE_AT_SWITCH_INIT     BINARY_DOUBLE;
      L_BASIS_REDUCTION_PERCENT    BINARY_DOUBLE;
      L_TAX_CREDIT_BASE            BINARY_DOUBLE;
      L_TAX_CREDIT_AMOUNT          BINARY_DOUBLE;
      L_TAX_CREDIT_BASIS_REDUCTION BINARY_DOUBLE;

      L_YEARS_USED            BINARY_DOUBLE := 0;
      L_MONTHS_USED           BINARY_DOUBLE := 0;
      L_YR1_FRACTION          BINARY_DOUBLE := 0;
      L_YR2_FRACTION          BINARY_DOUBLE := 0;
      L_ORIG_SHORT_MONTHS     BINARY_DOUBLE := 0;
      L_SHORT_YEAR_NET_ADJUST BINARY_DOUBLE := 0;
      L_YR1_RATE              BINARY_DOUBLE := 0;
      L_YR2_RATE              BINARY_DOUBLE := 0;

      L_SCOOP boolean;

      L_TAX_LIMITATION BINARY_DOUBLE := 0;
      I                pls_integer;
      J                pls_integer;
      K                pls_integer;
      L_NUM            pls_integer;
      L_POS            pls_integer;
      L_VINTAGE        pls_integer;
      L_JC             pls_integer;
      R                pls_integer;
      L_FOUND          boolean;

      L_RATIO     BINARY_DOUBLE := 0;
      L_RATIO1    BINARY_DOUBLE := 0;
      L_CHANGE    BINARY_DOUBLE := 0;
      L_AMOUNT    BINARY_DOUBLE := 0;
      L_DEPR_RATE BINARY_DOUBLE;
      type NUM_ARY200_TYPE is table of BINARY_DOUBLE index by binary_integer;
      L_TAX_RATES NUM_ARY200_TYPE;

      L_RETIRE_DEPR_CONV     BINARY_DOUBLE := 0;
      L_RETIRE_BAL_CONV      BINARY_DOUBLE := 0;
      L_RETIRE_RES_CONV      BINARY_DOUBLE := 0;
      L_GAIN_LOSS_CONV       BINARY_DOUBLE := 0;
      L_CAP_GAIN_CONV        BINARY_DOUBLE := 0;
      L_SALVAGE_CONV         BINARY_DOUBLE := 0;
      L_EST_SALVAGE_CONV     BINARY_DOUBLE := 0;
      L_COST_OF_REMOVAL_CONV BINARY_DOUBLE := 0;
      L_RETIRE_DEPR_RATIO    BINARY_DOUBLE := 0;
      L_RETIRE_RES_RATIO     BINARY_DOUBLE := 0;
      L_RETIRE_BAL_RATIO     BINARY_DOUBLE := 0;
      L_SALVAGE_RATIO        BINARY_DOUBLE := 0;
      L_ADR_SALVAGE_RATIO    BINARY_DOUBLE := 0;
      L_BEG_SALVAGE_RATIO    BINARY_DOUBLE := 0;
      L_COR_RATIO            BINARY_DOUBLE := 0;
      L_DEPR_SALVAGE_RATIO   BINARY_DOUBLE := 0;

      L_EX_RETIRE_DEPR_CONV   BINARY_DOUBLE := 0;
      L_EX_RETIRE_BAL_CONV    BINARY_DOUBLE := 0;
      L_EX_RETIRE_RES_CONV    BINARY_DOUBLE := 0;
      L_EX_GAIN_LOSS_CONV     BINARY_DOUBLE := 0;
      L_EX_CAP_GAIN_CONV      BINARY_DOUBLE := 0;
      L_EX_SALVAGE_CONV       BINARY_DOUBLE := 0;
      L_EX_EST_SALVAGE_CONV   BINARY_DOUBLE := 0;
      L_EX_RETIRE_DEPR_RATIO  BINARY_DOUBLE := 0;
      L_EX_RETIRE_RES_RATIO   BINARY_DOUBLE := 0;
      L_EX_RETIRE_BAL_RATIO   BINARY_DOUBLE := 0;
      L_EX_SALVAGE_RATIO      BINARY_DOUBLE := 0;
      L_EX_BEG_SALVAGE_RATIO  BINARY_DOUBLE := 0;
      L_EX_DEPR_SALVAGE_RATIO BINARY_DOUBLE := 0;

      L_TAX_RECONCILE_BEG      NUM_ARY200_TYPE;
      L_TAX_RECONCILE_ACT      NUM_ARY200_TYPE;
      L_TAX_RECONCILE_END      NUM_ARY200_TYPE;
      L_TAX_RECONCILE_RET      NUM_ARY200_TYPE;
      L_TAX_RECONCILE_TRANSFER NUM_ARY200_TYPE;
      L_TAX_RECONCILE_TYPE     NUM_ARY200_TYPE;

      type PLS_INTEGER_ARY_TYPE is table of pls_integer index by binary_integer;
      L_INPUT_RETIRE_IND PLS_INTEGER_ARY_TYPE;

      L_BEGIN_RES_IMPACT   BINARY_DOUBLE := 0;
      L_RESERVE            BINARY_DOUBLE := 0;
      L_RETIREMENT_DEPR    BINARY_DOUBLE := 0;
      L_EX_RETIREMENT_DEPR BINARY_DOUBLE := 0;
      L_ROUNDING           pls_integer;

      --* Activity Variables

      L_BOOK_RETIREMENT          BINARY_DOUBLE := 0;
      L_BOOK_ADJUSTMENT          BINARY_DOUBLE := 0;
      L_BOOK_ADDITION            BINARY_DOUBLE := 0;
      L_BOOK_EXTRAORD_RETIREMENT BINARY_DOUBLE := 0;

      --* Adjustment Variables

      L_BOOK_BALANCE_ADJUST     BINARY_DOUBLE := 0;
      L_ACCUM_RESERVE_ADJUST    BINARY_DOUBLE := 0;
      L_DEPRECIABLE_BASE_ADJUST BINARY_DOUBLE := 0;
      L_DEPRECIATION_ADJUST     BINARY_DOUBLE := 0;
      L_GAIN_LOSS_ADJUST        BINARY_DOUBLE := 0;
      L_CAP_GAIN_LOSS_ADJUST    BINARY_DOUBLE := 0;

      L_BOOK_BALANCE_ADJUST_METHOD   BINARY_DOUBLE;
      L_ACCUM_RESERVE_ADJUST_METHOD  BINARY_DOUBLE;
      L_DEPRECIABLE_BASE_ADJUST_METH BINARY_DOUBLE;
      L_EX_BEGIN_RES_IMPACT          BINARY_DOUBLE := 0;
      L_EX_RETIRE_RES_IMPACT         BINARY_DOUBLE := 0;

      --* EXEC SQL BEGIN DECLARE SECTION;
      L_ROWID                  BINARY_DOUBLE;
      L_TAX_RECONCILE_BEG_VAL  BINARY_DOUBLE;
      L_TAX_RECONCILE_ACT_VAL  BINARY_DOUBLE;
      L_TAX_RECONCILE_END_VAL  BINARY_DOUBLE;
      L_TAX_YEAR               BINARY_DOUBLE := 0;
      L_TAX_YEAR_1             BINARY_DOUBLE := 0;
      L_TAX_BOOK_ID            BINARY_DOUBLE := 0;
      L_TAX_RECORD_ID          BINARY_DOUBLE := 0;
      L_RECONCILE_ITEM_ID      BINARY_DOUBLE := 0;
      L_BOOK_BALANCE           BINARY_DOUBLE := 0;
      L_TAX_BALANCE            BINARY_DOUBLE := 0;
      L_REMAINING_LIFE         BINARY_DOUBLE := 0;
      L_ACCUM_RESERVE          BINARY_DOUBLE := 0;
      L_SL_RESERVE             BINARY_DOUBLE := 0;
      L_DEPRECIABLE_BASE       BINARY_DOUBLE := 0;
      L_FIXED_DEPRECIABLE_BASE BINARY_DOUBLE := 0;
      L_ACTUAL_SALVAGE         BINARY_DOUBLE := 0;
      L_ESTIMATED_SALVAGE      BINARY_DOUBLE := 0;
      L_ACCUM_SALVAGE          BINARY_DOUBLE := 0;
      L_ADDITIONS              BINARY_DOUBLE := 0;
      L_TRANSFERS              BINARY_DOUBLE := 0;
      L_ADJUSTMENTS            BINARY_DOUBLE := 0;
      L_RETIREMENTS            BINARY_DOUBLE := 0;

      L_CAP_GAIN_RETIREMENTS    BINARY_DOUBLE;
      L_CAP_GAIN_EX_RETIREMENTS BINARY_DOUBLE;
      L_EXTRAORDINARY_RETIRES   BINARY_DOUBLE := 0;
      L_ACCUM_ORDINARY_RETIRES  BINARY_DOUBLE := 0;
      L_DEPRECIATION            BINARY_DOUBLE := 0;
      L_COST_OF_REMOVAL         BINARY_DOUBLE := 0;
      L_GAIN_LOSS               BINARY_DOUBLE := 0;
      L_CAPITAL_GAIN_LOSS       BINARY_DOUBLE := 0;
      L_EX_CAPITAL_GAIN_LOSS    BINARY_DOUBLE := 0;
      L_EST_SALVAGE_PCT         BINARY_DOUBLE := 0;
      L_BOOK_BALANCE_END        BINARY_DOUBLE := 0;
      L_TAX_BALANCE_END         BINARY_DOUBLE := 0;
      L_ACCUM_RESERVE_END       BINARY_DOUBLE := 0;
      L_SL_RESERVE_END          BINARY_DOUBLE := 0;
      L_ACCUM_SALVAGE_END       BINARY_DOUBLE := 0;
      L_ACCUM_ORDIN_RETIRES_END BINARY_DOUBLE := 0;
      L_RETIRE_INVOL_CONV       BINARY_DOUBLE := 0;
      L_SALVAGE_INVOL_CONV      BINARY_DOUBLE := 0;
      L_SALVAGE_EXTRAORD        BINARY_DOUBLE := 0;
      L_CALC_DEPRECIATION       BINARY_DOUBLE := 0;
      L_OVER_ADJ_DEPRECIATION   BINARY_DOUBLE := 0;
      L_RETIRE_RES_IMPACT       BINARY_DOUBLE := 0;
      L_TRANSFER_RES_IMPACT     BINARY_DOUBLE := 0;
      L_SALVAGE_RES_IMPACT      BINARY_DOUBLE := 0;
      L_ADJUSTED_RETIRE_BASIS   BINARY_DOUBLE := 0;
      L_RESERVE_AT_SWITCH       BINARY_DOUBLE := 0;
      L_RESERVE_AT_SWITCH_END   BINARY_DOUBLE := 0;
      L_QUANTITY                BINARY_DOUBLE := 0;
      L_QUANTITY_END            BINARY_DOUBLE := 0;
      L_COR_RES_IMPACT          BINARY_DOUBLE := 0;
      L_COR_EXPENSE             BINARY_DOUBLE := 0;
      L_NUMBER_MONTHS_BEG       BINARY_DOUBLE := 0;
      L_NUMBER_MONTHS_END       BINARY_DOUBLE := 0;
      L_EX_GAIN_LOSS            BINARY_DOUBLE := 0;

      --* Transfer Variables

      L_BOOK_BALANCE_TRANSFER        BINARY_DOUBLE;
      L_TAX_BALANCE_TRANSFER         BINARY_DOUBLE;
      L_ACCUM_RESERVE_TRANSFER       BINARY_DOUBLE;
      L_SL_RESERVE_TRANSFER          BINARY_DOUBLE;
      L_FIXED_DEPRECIABLE_BASE_TRANS BINARY_DOUBLE;
      L_ESTIMATED_SALVAGE_TRANSFER   BINARY_DOUBLE;
      L_ACCUM_SALVAGE_TRANSFER       BINARY_DOUBLE;
      L_ACCUM_ORDINARY_RETIRES_TRANS BINARY_DOUBLE;
      L_RESERVE_AT_SWITCH_TRANSFER   BINARY_DOUBLE;
      L_QUANTITY_TRANSFER            BINARY_DOUBLE;

      /* EXEC SQL END DECLARE SECTION;        */
      /*l_tax_convention_rows pls_integer;
      l_extraord_convention_rows pls_integer;
      l_tax_rates_rows pls_integer;
      l_tax_reconcile_rows pls_integer;
      l_tax_limitation_rows pls_integer;
      l_tax_job_creation_rows pls_integer;
      l_tax_book_activity_rows pls_integer;
      l_tax_depr_bals_rows pls_integer;
      l_reconcile_bals_rows pls_integer; */
      L_ROWS  pls_integer;
      L_NYEAR pls_integer;
      -- Maint -10475
      L_FIRST_YEAR_RETIRE    boolean;
      L_EX_FIRST_YEAR_RETIRE boolean;
      L_SHORT_MONTHS         BINARY_DOUBLE;
   begin
      L_SHORT_MONTHS := A_SHORT_MONTHS;
      /*
      l_tax_convention_rows := a_tax_convention_sub_rec.count;
      l_extraord_convention_rows := a_tax_convention_sub_rec.count;
      l_tax_rates_rows := a_tax_rates_sub_rec.count;
      l_tax_reconcile_rows := a_tax_reconcile_sub_rec.count;
      l_tax_limitation_rows := a_tax_limitation_sub_rec.count;
      l_tax_job_creation_rows  := a_tax_job_creation_rec.count;
      l_tax_book_activity_rows  := a_tax_book_activity_rec.count;
      l_tax_depr_bals_rows  := a_tax_depr_bals_sub_rec.count;
      l_reconcile_bals_rows  := a_reconcile_bals_sub_rec.count;
      */

      /* move the items in the tax_depreciation table to variables
      get rid of the statements we don't end up needing  */

      --if dw_tax_depr.rowcount() > 0 then
      L_BOOK_BALANCE_TRANSFER        := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).BOOK_BALANCE_XFER, 0);
      L_TAX_BALANCE_TRANSFER         := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).TAX_BALANCE_XFER, 0);
      L_ACCUM_RESERVE_TRANSFER       := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ACCUM_RESERVE_XFER, 0);
      L_SL_RESERVE_TRANSFER          := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).SL_RESERVE_XFER, 0);
      L_FIXED_DEPRECIABLE_BASE_TRANS := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).FIXED_DEPRECIABLE_BASE_XFER, 0);
      L_ESTIMATED_SALVAGE_TRANSFER   := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ESTIMATED_SALVAGE_XFER, 0);
      L_ACCUM_SALVAGE_TRANSFER       := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ACCUM_SALVAGE_XFER, 0);
      L_ACCUM_ORDINARY_RETIRES_TRANS := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ACCUM_ORDINARY_RETIRES_XFER, 0);
      L_RESERVE_AT_SWITCH_TRANSFER   := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).RESERVE_AT_SWITCH_XFER, 0);
      L_QUANTITY_TRANSFER            := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).QUANTITY_XFER, 0);
      /*
      else
      {
        book_balance_transfer             =  0;
        tax_balance_transfer                     =  0;
        accum_reserve_transfer             =  0;
        sl_reserve_transfer                   =  0;
        fixed_depreciable_base_transfer     =  0;
        estimated_salvage_transfer           =  0;
        accum_salvage_transfer               =  0;
        accum_ordinary_retires_transfer       =  0;
        reserve_at_switch_transfer               =  0;
        quantity_transfer                  =  0;
      }
      */
      A_ADD_TAX_RECONCILE_ROWS := 0;

      L_TAX_YEAR               := G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).TAX_YEAR;
      L_BOOK_BALANCE           := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).BOOK_BALANCE, 0) +
                                  L_BOOK_BALANCE_TRANSFER;
      L_TAX_BALANCE            := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).TAX_BALANCE, 0) +
                                  L_TAX_BALANCE_TRANSFER;
      L_REMAINING_LIFE         := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).REMAINING_LIFE, 0);
      L_ACCUM_RESERVE          := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ACCUM_RESERVE, 0) +
                                  L_ACCUM_RESERVE_TRANSFER;
      L_SL_RESERVE             := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).SL_RESERVE, 0) +
                                  L_SL_RESERVE_TRANSFER;
      L_FIXED_DEPRECIABLE_BASE := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).FIXED_DEPRECIABLE_BASE, 0);

      if (L_FIXED_DEPRECIABLE_BASE <> 0) then
         L_FIXED_DEPRECIABLE_BASE := L_FIXED_DEPRECIABLE_BASE + L_FIXED_DEPRECIABLE_BASE_TRANS;
      end if;

      L_ACTUAL_SALVAGE    := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ACTUAL_SALVAGE, 0);
      L_ESTIMATED_SALVAGE := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ESTIMATED_SALVAGE, 0);

      if (L_ESTIMATED_SALVAGE <> 0) then
         L_ESTIMATED_SALVAGE := L_ESTIMATED_SALVAGE + L_ESTIMATED_SALVAGE_TRANSFER;
      end if;

      L_ACCUM_SALVAGE          := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ACCUM_SALVAGE, 0) + L_ACCUM_SALVAGE_TRANSFER;
      L_ADDITIONS              := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ADDITIONS, 0);
      L_TRANSFERS              := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).TRANSFERS, 0);
      L_ADJUSTMENTS            := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ADJUSTMENTS, 0);
      L_RETIREMENTS            := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).RETIREMENTS, 0);
      L_EXTRAORDINARY_RETIRES  := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).EXTRAORDINARY_RETIRES, 0);
      L_ACCUM_ORDINARY_RETIRES := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ACCUM_ORDINARY_RETIRES, 0) + L_ACCUM_ORDINARY_RETIRES_TRANS;
      L_COST_OF_REMOVAL        := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).COST_OF_REMOVAL, 0);
      L_EST_SALVAGE_PCT        := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).EST_SALVAGE_PCT, 0);
      L_RETIRE_INVOL_CONV      := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).RETIRE_INVOL_CONV, 0);
      L_SALVAGE_INVOL_CONV     := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).SALVAGE_INVOL_CONV, 0);
      L_SALVAGE_EXTRAORD       := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).SALVAGE_EXTRAORD, 0);
      L_RESERVE_AT_SWITCH      := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).RESERVE_AT_SWITCH, 0) + L_RESERVE_AT_SWITCH_TRANSFER;
      L_QUANTITY               := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).QUANTITY + L_QUANTITY_TRANSFER, 0);
      L_NUMBER_MONTHS_BEG      := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).NUMBER_MONTHS_BEG, 0);
      L_RESERVE_AT_SWITCH_INIT := 0;

      L_ADD_RATIO := 1;

      --* Get The Tax Adjustments

      --if(tax_depr->tax_record_id == 366 || tax_depr->tax_record_id == 713 || tax_depr->tax_record_id == 1195 ||
      --    tax_depr->tax_record_id == 2787 || tax_depr->tax_record_id == 3796)
      --   i = 1;

      if (A_TAX_CALC_ADJ_INDEX > 0) then
         L_BOOK_BALANCE_ADJUST     := NVL(G_TAX_CALC_ADJ_REC(A_TAX_CALC_ADJ_INDEX).BOOK_BALANCE_ADJUST, 0);
         L_ACCUM_RESERVE_ADJUST    := NVL(G_TAX_CALC_ADJ_REC(A_TAX_CALC_ADJ_INDEX).ACCUM_RESERVE_ADJUST, 0);
         L_DEPRECIABLE_BASE_ADJUST := NVL(G_TAX_CALC_ADJ_REC(A_TAX_CALC_ADJ_INDEX).DEPRECIABLE_BASE_ADJUST, 0);
         L_DEPRECIATION_ADJUST     := NVL(G_TAX_CALC_ADJ_REC(A_TAX_CALC_ADJ_INDEX).DEPRECIATION_ADJUST, 0);
         L_GAIN_LOSS_ADJUST        := NVL(G_TAX_CALC_ADJ_REC(A_TAX_CALC_ADJ_INDEX).GAIN_LOSS_ADJUST, 0);
         L_CAP_GAIN_LOSS_ADJUST    := NVL(G_TAX_CALC_ADJ_REC(A_TAX_CALC_ADJ_INDEX).CAP_GAIN_LOSS_ADJUST, 0);

         L_BOOK_BALANCE_ADJUST_METHOD   := NVL(G_TAX_CALC_ADJ_REC(A_TAX_CALC_ADJ_INDEX).BOOK_BALANCE_ADJUST_METHOD, 0);
         L_ACCUM_RESERVE_ADJUST_METHOD  := NVL(G_TAX_CALC_ADJ_REC(A_TAX_CALC_ADJ_INDEX).ACCUM_RESERVE_ADJUST_METHOD, 0);
         L_DEPRECIABLE_BASE_ADJUST_METH := NVL(G_TAX_CALC_ADJ_REC(A_TAX_CALC_ADJ_INDEX).DEPRECIABLE_BASE_ADJUST_METHOD, 0);

      else
         L_BOOK_BALANCE_ADJUST          := 0;
         L_ACCUM_RESERVE_ADJUST         := 0;
         L_DEPRECIABLE_BASE_ADJUST      := 0;
         L_DEPRECIATION_ADJUST          := 0;
         L_GAIN_LOSS_ADJUST             := 0;
         L_CAP_GAIN_LOSS_ADJUST         := 0;
         L_BOOK_BALANCE_ADJUST_METHOD   := 1;
         L_ACCUM_RESERVE_ADJUST_METHOD  := 1;
         L_DEPRECIABLE_BASE_ADJUST_METH := 1;
      end if;

      --* Get The Tax Conventions
      if (A_TAX_CONVENTION_ROWS = 0) then
         L_RETIRE_DEPR_CONV := 0;
         L_RETIRE_BAL_CONV  := 0;
         L_RETIRE_RES_CONV  := 0;
         L_GAIN_LOSS_CONV   := 0;
         L_SALVAGE_CONV     := 0;
         L_EST_SALVAGE_CONV := 0;
         L_CAP_GAIN_CONV    := 0;
      else
         L_RETIRE_DEPR_CONV     := A_TAX_CONVENTION_REC.RETIRE_DEPR_ID;
         L_RETIRE_BAL_CONV      := A_TAX_CONVENTION_REC.RETIRE_BAL_ID;
         L_RETIRE_RES_CONV      := A_TAX_CONVENTION_REC.RETIRE_RESERVE_ID;
         L_GAIN_LOSS_CONV       := A_TAX_CONVENTION_REC.GAIN_LOSS_ID;
         L_SALVAGE_CONV         := A_TAX_CONVENTION_REC.SALVAGE_ID;
         L_EST_SALVAGE_CONV     := A_TAX_CONVENTION_REC.EST_SALVAGE_ID;
         L_COST_OF_REMOVAL_CONV := A_TAX_CONVENTION_REC.COST_OF_REMOVAL_ID;
         L_CAP_GAIN_CONV        := A_TAX_CONVENTION_REC.CAP_GAIN_ID;
      end if;

      if (A_EXTRAORD_CONVENTION_ROWS = 0) then
         L_EX_RETIRE_DEPR_CONV := 0;
         L_EX_RETIRE_BAL_CONV  := 0;
         L_EX_RETIRE_RES_CONV  := 0;
         L_EX_GAIN_LOSS_CONV   := 0;
         L_EX_SALVAGE_CONV     := 0;
         L_EX_EST_SALVAGE_CONV := 0;
      else
         L_EX_RETIRE_DEPR_CONV := A_EXTRAORD_CONVENTION_REC.RETIRE_DEPR_ID;
         L_EX_RETIRE_BAL_CONV  := A_EXTRAORD_CONVENTION_REC.RETIRE_BAL_ID;
         L_EX_RETIRE_RES_CONV  := A_EXTRAORD_CONVENTION_REC.RETIRE_RESERVE_ID;
         L_EX_GAIN_LOSS_CONV   := A_EXTRAORD_CONVENTION_REC.GAIN_LOSS_ID;
         L_EX_SALVAGE_CONV     := A_EXTRAORD_CONVENTION_REC.SALVAGE_ID;
         L_EX_EST_SALVAGE_CONV := A_EXTRAORD_CONVENTION_REC.EST_SALVAGE_ID;
         L_EX_CAP_GAIN_CONV    := A_EXTRAORD_CONVENTION_REC.CAP_GAIN_ID;
      end if;

      --* get the tax rates, and lives and conventions for remaining life rates.

      /*  dw_tax_rates.setsort('tax_rates_year a')
      dw_tax_rates.sort() */

      L_NUM := A_TAX_RATES_ROWS;

      for I in 1 .. L_NUM
      loop
         L_TAX_RATES(I) := A_TAX_RATES_SUB_REC(I).RATE;
      end loop;
      L_NET      := A_TAX_RATES_SUB_REC(1).NET_GROSS;
      L_ROUNDING := NVL(A_TAX_RATES_SUB_REC(1).ROUNDING_CONVENTION, 0);
      L_LIFE     := NVL(A_TAX_RATES_SUB_REC(1).LIFE, 0);
      L_RLIFE    := NVL(A_TAX_RATES_SUB_REC(1).REMAINING_LIFE_PLAN, 0);
      L_METHOD   := NVL(A_TAX_RATES_SUB_REC(1).START_METHOD, 0);
      L_VINTAGE  := NVL(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).VINTAGE_YEAR, 0);
      L_SWITCHED := NVL(A_TAX_RATES_SUB_REC(1).SWITCHED_YEAR, 0);
      L_HCONV    := NVL(A_TAX_RATES_SUB_REC(1).HALF_YEAR_CONVENTION, 0);

      --* upper is important for good match

      L_NET := UPPER(L_NET);

      /*i = round; */

      -- which depreciation rates should we use for this tax year, if not on remaining life.

      if (TRUNC(L_TAX_YEAR) <> L_VINTAGE) then
         if (L_NUMBER_MONTHS_BEG = -1 or L_NUMBER_MONTHS_BEG = 0) then
            L_NUMBER_MONTHS_BEG := (ROUND(L_TAX_YEAR) - L_VINTAGE) * 12.0;
         end if;
      end if;

      L_ORIG_SHORT_MONTHS := L_SHORT_MONTHS;

      if (TRUNC(L_TAX_YEAR) = L_VINTAGE) then
         if (L_NUMBER_MONTHS_BEG = -1 or L_NUMBER_MONTHS_BEG = 0) then
            if (L_HCONV <> 1) then
               L_SHORT_MONTHS := 12;
            else
               L_SHORT_MONTHS := L_SHORT_MONTHS / 2;
            end if;
         end if;
      end if;

      L_NUMBER_MONTHS_END := L_NUMBER_MONTHS_BEG + L_SHORT_MONTHS;
      L_YEARS_USED        := TRUNC((L_NUMBER_MONTHS_BEG / 12));
      --l_months_used              =  (l_number_months_beg % 12);
      L_MONTHS_USED := mod(L_NUMBER_MONTHS_BEG, 12.0);
      --yr1_fraction                :=  mid(12.0 - l_months_used,l_short_month);
      if (12.0 - L_MONTHS_USED) > L_SHORT_MONTHS then
         L_YR1_FRACTION := L_SHORT_MONTHS;
      else
         L_YR1_FRACTION := 12.0 - L_MONTHS_USED;
      end if;
      L_YR2_FRACTION := L_SHORT_MONTHS - L_YR1_FRACTION;

      L_YR1_FRACTION := L_YR1_FRACTION / 12.0;
      L_YR2_FRACTION := L_YR2_FRACTION / 12.0;

      I := L_YEARS_USED + 1;

      --i =  l_tax_year - l_vintage + 1;

      L_SCOOP := false;
      if (I != 1) then
         if (I >= A_TAX_RATES_ROWS and (L_MONTHS_USED + L_SHORT_MONTHS) >= 12 and
            L_ACCUM_RESERVE <> 0) then
            L_SCOOP := true;
         end if;
         if (I >= (A_TAX_RATES_ROWS + 1) and L_ACCUM_RESERVE <> 0) then
            L_SCOOP := true;
         end if;
      else
         if (L_TAX_RATES(I) = 0 and L_ACCUM_RESERVE <> 0) then
            L_SCOOP := true;
         end if;
      end if;
      --* If a life rate, then there are only 2 rates, the first year and all subsequent years
      --* there us never scoop

      if (L_METHOD = 6) then
         if (I > 1) then
            I := 2;
         end if;
         L_SCOOP := false;
      end if;

      --* If a vintage rate, match the tax_year with the designated year

      if (L_METHOD = 7) then
         L_NUM := A_TAX_RATES_ROWS;
         for I in 1 .. L_NUM
         loop
            exit when TRUNC(L_TAX_YEAR) = A_TAX_RATES_SUB_REC(I).YEAR;
         end loop;
         if (I > L_NUM) then
            I := A_TAX_RATES_ROWS + 1;
         else
            -- IF this is the first year, then replace the first year rates.
            if (TRUNC(L_TAX_YEAR) = L_VINTAGE and L_NUMBER_MONTHS_BEG = 0) then
               L_TAX_RATES(I) := A_TAX_RATES_SUB_REC(I).RATE1 * (L_ORIG_SHORT_MONTHS / 12);
            end if;
         end if;

         L_SCOOP := false;
         if (I = A_TAX_RATES_ROWS and (L_MONTHS_USED + L_SHORT_MONTHS) >= 12 and
            L_ACCUM_RESERVE <> 0) then
            L_SCOOP := true;
         end if;
      end if;

      if (L_RLIFE <> 1) then
         if ((I > A_TAX_RATES_ROWS) or (I < 1)) then
            L_DEPR_RATE := 0;
         else
            if (L_METHOD = 6 and I = 2) then
               --  rate should just be the 2nd year of a like rate
               L_DEPR_RATE := L_TAX_RATES(I) * (L_YR1_FRACTION + L_YR2_FRACTION);
            else
               L_DEPR_RATE := L_TAX_RATES(I) * L_YR1_FRACTION;
            end if;

            if (I + 1 <= A_TAX_RATES_ROWS) then
               L_DEPR_RATE := L_DEPR_RATE + (L_TAX_RATES(I) * L_YR2_FRACTION);
            end if;
            if (I > A_TAX_RATES_ROWS) then
               L_YR1_RATE := 0;
            else
               L_YR1_RATE := L_TAX_RATES(I);
            end if;
            if ((I + 1) > A_TAX_RATES_ROWS) then
               L_YR2_RATE := 0;
            else
               L_YR2_RATE := L_TAX_RATES(I + 1);
            end if;
            if (trim(L_NET) = 'GRNET') then
               if (I < L_SWITCHED) then
                  L_NET := 'GROSS';
               else
                  L_NET := 'NET';
               end if;
            end if;
            if (trim(L_NET) = 'NETGR') then
               if (I = L_SWITCHED) then
                  L_RESERVE_AT_SWITCH_INIT := L_ACCUM_RESERVE;
                  L_RESERVE_AT_SWITCH      := L_RESERVE_AT_SWITCH + L_RESERVE_AT_SWITCH_INIT;
               end if;
               if (I < L_SWITCHED) then
                  L_NET := 'NET';
               else
                  L_NET := 'GROSS';
               end if;
            end if;
         end if;
      end if;

      --* calculate the depreciation rate to use this tax year, if on remaining life plan.

      if (L_RLIFE = 1) then
         L_NET := 'NET';
         if (I < 1) then
            L_DEPR_RATE := 0;
            L_YR1_RATE  := 0;
            L_YR2_RATE  := 0;
         else
            if (L_TAX_BALANCE = 0) then
               --* check for divide by zero
               L_REM_LIFE := 0;
            else
               L_REM_LIFE := L_LIFE * (L_SL_RESERVE / L_TAX_BALANCE);
            end if;
            if (L_REM_LIFE > L_LIFE) then
               L_REM_LIFE := L_LIFE;
            end if;

            if (L_REM_LIFE < 1) then
               L_REM_LIFE := 1;
            end if;
            --* sl case is easy

            if (L_METHOD != 2) then
               --* sl case
               if (L_REM_LIFE != 0) then
                  --* div by 0 protect
                  L_DEPR_RATE := (1.0 / L_REM_LIFE) * (L_SHORT_MONTHS / 12.0);
                  L_YR1_RATE  := 1 / L_REM_LIFE;
                  L_YR2_RATE  := 1 / L_REM_LIFE;
               else
                  L_DEPR_RATE := 0;
                  L_YR1_RATE  := 0;
                  L_YR2_RATE  := 0;
               end if;
               --*syd case requires syd calc
            else
               --* syd case

               L_WLIFE := L_REM_LIFE; --* whole number part
               L_FLIFE := L_REM_LIFE - L_WLIFE; --* fractional part

               if (((L_WLIFE = 0) and (L_FLIFE = 0)) or L_WLIFE = -1) then
                  --* div by zero check
                  L_DEPR_RATE := 0;
                  L_YR1_RATE  := 0;
                  L_YR2_RATE  := 0;
               else
                  L_DEPR_RATE := L_REM_LIFE /
                                 (((L_WLIFE + 1) * L_WLIFE / 2) + ((L_WLIFE + 1) * L_FLIFE));
                  L_YR1_RATE  := L_DEPR_RATE;
                  L_YR2_RATE  := L_DEPR_RATE;
                  L_DEPR_RATE := L_DEPR_RATE * (L_SHORT_MONTHS / 12.0);
               end if;

            end if; --* syd case
         end if;

         if (ABS(L_DEPR_RATE) > 1) then
            L_DEPR_RATE := 1;
         end if;
         if (ABS(L_YR1_RATE) > 1) then
            L_YR1_RATE := 1;
         end if;
         if (ABS(L_YR2_RATE) > 1) then
            L_YR2_RATE := 1;
         end if;
         L_DEPR_RATE := ROUND(L_DEPR_RATE, 5);
         L_YR1_RATE  := ROUND(L_YR1_RATE, 5);
         L_YR2_RATE  := ROUND(L_YR2_RATE, 5);

         --* dont scoop remaining life plan stuff if the depreciation rate is still alive
         --* otherwise let the scoop decision stand as before.

         if (L_DEPR_RATE <> 0) then
            L_SCOOP := false;
         end if;
      end if; --* rlife = 1

      --*
      --* establish tax limitations (e.g. luxury auto if applicable)
      --*
      L_TAX_LIMITATION := -1;

      if (A_TAX_LIMITATION_ROWS <> 0) then
         /*dw_tax_limitation.setsort('year a')
         dw_tax_limitation.sort() */

         I := TRUNC(L_TAX_YEAR) - L_VINTAGE + 1;

         if (I > A_TAX_LIMITATION_ROWS or I < 1) then
            L_TAX_LIMITATION := -1;
         else
            L_TAX_LIMITATION := NVL(A_TAX_LIMITATION_SUB_REC(I).LIMITATION, 0) *
                                (L_SHORT_MONTHS / 12.0);
            L_COMPARE_RATE   := NVL(A_TAX_LIMITATION_SUB_REC(I).COMPARE_RATE, 0);
            if (L_COMPARE_RATE != 1) then
               L_COMPARE_RATE := 0;
            end if;
         end if;
      end if;

      --* 9/11/2001 job creation.
      --* Make the Reconciling Item exist, and make sure it has the right include_id

      L_JROW                 := 0;
      L_JOB_CREATION         := -1;
      L_JOB_CREATION_PERCENT := 0;

      --trid :=   dw_tax_depr.getitemnumber(1,'tax_record_id')
      --nyear := get_next_year(tax_year,tax_depr->tax_record_id); //select min(tax_year) into :nyear  from tax_depreciation where tax_year > :tax_year and tax_record_id =  :trid;

      -- Zero out any activity in calculated basis difference before starting
      for J in 1 .. A_TAX_RECONCILE_ROWS
      loop
         if (A_TAX_RECONCILE_SUB_REC(J).CALCED = 1) then
            A_TAX_RECONCILE_SUB_REC(J).BASIS_AMOUNT_ACTIVITY := 0;
         end if;
      end loop;

      if (A_TAX_JOB_CREATION_ROWS != 0) then
         -- sort tax_job_creation
         L_TAX_JOB_CREATION_ORDERED := TYPE_TAX_LIMIT2_REC();
         L_TAX_JOB_CREATION_ORDERED.EXTEND(A_TAX_JOB_CREATION_ROWS);
         for J in 1 .. A_TAX_JOB_CREATION_ROWS
         loop
            L_POS := 1;
            for K in 1 .. A_TAX_JOB_CREATION_ROWS
            loop
               if (A_TAX_JOB_CREATION_SUB_REC(J).ORDERING > A_TAX_JOB_CREATION_SUB_REC(K).ORDERING) then
                  L_POS := L_POS + 1;
               end if;
            end loop;
            L_TAX_JOB_CREATION_ORDERED(L_POS) := A_TAX_JOB_CREATION_SUB_REC(J);
         end loop;

         L_TAX_CREDIT_BASE     := 0;
         L_JOB_CREATION_AMOUNT := 0;
         for L_JC in 1 .. A_TAX_JOB_CREATION_ROWS
         loop
            L_JOB_CREATION            := L_TAX_JOB_CREATION_ORDERED(L_JC).RECONCILE_ITEM_ID;
            L_JOB_CREATION_PERCENT    := L_TAX_JOB_CREATION_ORDERED(L_JC).TAX_CREDIT_PERCENT;
            L_BASIS_REDUCTION_PERCENT := L_TAX_JOB_CREATION_ORDERED(L_JC).BASIS_REDUCTION_PERCENT;
            L_CALC_FUTURE_YEARS       := L_TAX_JOB_CREATION_ORDERED(L_JC).CALC_FUTURE_YEARS;
            L_CALCED                  := L_TAX_JOB_CREATION_ORDERED(L_JC).CALCED;
            L_DEPR_DEDUCTION          := L_TAX_JOB_CREATION_ORDERED(L_JC).DEPR_DEDUCTION;
            if (L_CALC_FUTURE_YEARS is null) then
               -- is null
               L_CALC_FUTURE_YEARS := 1;
            end if;
            L_TAX_CREDIT_AMOUNT := 0;
            L_TAX_CREDIT_BASE   := 0;
            -- first time through loop - tax_credit_base = book additions and basis difference activity for items not calced
            if (L_JC = 1) then
               for J in 1 .. A_TAX_BOOK_ACTIVITY_ROWS
               loop
                  -- Maint-10475
                  --if(tax_book_activity[j]->tax_activity_code_id == 1)
                  if (NVL(A_TAX_BOOK_ACTIVITY_SUB_REC(J).TAX_ACTIVITY_TYPE_ID, 1) = 1) then
                     L_TAX_CREDIT_BASE := L_TAX_CREDIT_BASE + A_TAX_BOOK_ACTIVITY_SUB_REC(J).AMOUNT;
                  end if;
               end loop;
               for J in 1 .. A_TAX_RECONCILE_ROWS
               loop
                  if (A_TAX_RECONCILE_SUB_REC(J).CALCED = 0) then
                     L_TAX_CREDIT_BASE := L_TAX_CREDIT_BASE + A_TAX_RECONCILE_SUB_REC(J).BASIS_AMOUNT_ACTIVITY;
                  end if;
               end loop;
            end if;
            L_TAX_CREDIT_AMOUNT := L_TAX_CREDIT_BASE * L_JOB_CREATION_PERCENT;
            if (L_NUMBER_MONTHS_BEG != 0) then
               L_TAX_CREDIT_AMOUNT := L_TAX_CREDIT_AMOUNT * L_CALC_FUTURE_YEARS;
            end if;
            L_TAX_CREDIT_BASIS_REDUCTION := -L_TAX_CREDIT_AMOUNT * L_BASIS_REDUCTION_PERCENT;
            L_TAX_CREDIT_BASE            := L_TAX_CREDIT_BASE + L_TAX_CREDIT_BASIS_REDUCTION;
            L_JOB_CREATION_AMOUNT        := L_JOB_CREATION_AMOUNT +
                                            (L_TAX_CREDIT_AMOUNT * L_DEPR_DEDUCTION);

            L_JROW := -1;
            -- find reconcile_item_id
            for J in 1 .. A_TAX_RECONCILE_ROWS
            loop
               if (A_TAX_RECONCILE_SUB_REC(J).RECONCILE_ITEM_ID = L_JOB_CREATION) then
                  L_JROW := J;
               end if;
            end loop;
            if (L_JROW = -1) then
               -- No row found
               L_JROW := 1;
               A_ADD_TAX_RECONCILE_REC.EXTEND;
               J := A_ADD_TAX_RECONCILE_REC.COUNT;
               A_ADD_TAX_RECONCILE_REC(J).RECONCILE_ITEM_ID := L_JOB_CREATION;
               A_ADD_TAX_RECONCILE_REC(J).TAX_RECORD_ID := G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).TAX_RECORD_ID;
               A_ADD_TAX_RECONCILE_REC(J).TAX_YEAR := L_TAX_YEAR;
               A_ADD_TAX_RECONCILE_REC(J).CALCED := L_CALCED;
               A_ADD_TAX_RECONCILE_REC(J).DEPR_DEDUCTION := L_DEPR_DEDUCTION;
               A_ADD_TAX_RECONCILE_REC(J).BASIS_AMOUNT_ACTIVITY := L_TAX_CREDIT_BASIS_REDUCTION;
               A_ADD_TAX_RECONCILE_REC(J).BASIS_AMOUNT_END := 0;
               A_ADD_TAX_RECONCILE_REC(J).BASIS_AMOUNT_BEG := 0;
               A_ADD_TAX_RECONCILE_REC(J).TAX_INCLUDE_ID := L_TAX_JOB_CREATION_ORDERED(L_JC).TAX_INCLUDE_ID;
               A_ADD_TAX_RECONCILE_ROWS := A_ADD_TAX_RECONCILE_ROWS + 1;

               --tax_reconcile[tax_reconcile_rows]->basis_amount_transfer := 0;
               --tax_reconcile[tax_reconcile_rows]->input_retire_ind := 0;
               --tax_reconcile[tax_reconcile_rows]->depr_deduction := dl_epr_deduction;
               --tax_reconcile[tax_reconcile_rows]->tax_include_id := l_tax_job_creation_ordered[jc]->tax_include_id;

               G_TAX_RECONCILE_REC.EXTEND;
               L_ROWS := G_TAX_RECONCILE_REC.COUNT;
               G_TAX_RECONCILE_REC(L_ROWS) := A_ADD_TAX_RECONCILE_REC(J);

               -- add record to hash for tax_reconcile and tax_reconcile_bal
            else
               --a_tax_reconcile_sub_rec.extend;
               A_TAX_RECONCILE_SUB_REC(L_JROW).BASIS_AMOUNT_ACTIVITY := L_TAX_CREDIT_BASIS_REDUCTION;
               A_TAX_RECONCILE_SUB_REC(L_JROW).TAX_INCLUDE_ID := L_TAX_JOB_CREATION_ORDERED(L_JC).TAX_INCLUDE_ID;
            end if;

            if (A_NYEAR <> 0) then
               --if not isnull(nyear) then
               L_FOUND := false;
               for J in 1 .. A_RECONCILE_BALS_ROWS
               loop
                  if (A_RECONCILE_BALS_SUB_REC(J).RECONCILE_ITEM_ID = L_JOB_CREATION) then
                     L_FOUND := true;
                     L_JROW  := J;
                  end if;
               end loop;
               --format := 'reconcile_item_id = ' + string(job_creation)
               --jrow := dw_reconcile_bals.find(fformat,1,dw_reconcile_bals.rowcount())
               if (L_FOUND = false) then
                  --  jrow = 0 then
                  A_ADD_TAX_RECONCILE_REC.EXTEND;
                  J := A_ADD_TAX_RECONCILE_REC.COUNT;
                  A_ADD_TAX_RECONCILE_REC(J).RECONCILE_ITEM_ID := L_JOB_CREATION;
                  A_ADD_TAX_RECONCILE_REC(J).TAX_RECORD_ID := G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).TAX_RECORD_ID;
                  A_ADD_TAX_RECONCILE_REC(J).TAX_YEAR := A_NYEAR;
                  A_ADD_TAX_RECONCILE_REC(J).TAX_INCLUDE_ID := L_TAX_JOB_CREATION_ORDERED(L_JC).TAX_INCLUDE_ID;
                  A_ADD_TAX_RECONCILE_REC(J).BASIS_AMOUNT_END := 0;
                  A_ADD_TAX_RECONCILE_REC(J).BASIS_AMOUNT_ACTIVITY := 0;
                  A_ADD_TAX_RECONCILE_REC(J).BASIS_AMOUNT_BEG := 0;
                  A_ADD_TAX_RECONCILE_REC(J).CALCED := L_CALCED;
                  A_ADD_TAX_RECONCILE_REC(J).DEPR_DEDUCTION := L_DEPR_DEDUCTION;
                  A_ADD_TAX_RECONCILE_ROWS := A_ADD_TAX_RECONCILE_ROWS + 1;
                  L_ADD_TAX_RECONCILE_BALS_FLAG := true;

                  -- add record to hash for tax_reconcile and tax_reconcile_bal

                  A_RECONCILE_BALS_SUB_REC.EXTEND;
                  A_RECONCILE_BALS_ROWS := A_RECONCILE_BALS_SUB_REC.COUNT;
                  A_RECONCILE_BALS_SUB_REC(A_RECONCILE_BALS_ROWS).BASIS_AMOUNT_ACTIVITY := 0;
                  A_RECONCILE_BALS_SUB_REC(A_RECONCILE_BALS_ROWS).BASIS_AMOUNT_END := 0;
                  --reconcile_bals[reconcile_bals_rows]->basis_amount_beg :=  -job_creation_amount;
                  --reconcile_bals[reconcile_bals_rows]->basis_amount_transfer := 0;
                  --reconcile_bals[reconcile_bals_rows]->calced := calced;
                  A_RECONCILE_BALS_SUB_REC(A_RECONCILE_BALS_ROWS).INPUT_RETIRE_IND := 0;

                  G_TAX_RECONCILE_REC.EXTEND;
                  L_ROWS := G_TAX_RECONCILE_REC.COUNT;
                  G_TAX_RECONCILE_REC(L_ROWS) := A_ADD_TAX_RECONCILE_REC(J);

               else
                  --jrow = 0
                  A_RECONCILE_BALS_SUB_REC(L_JROW).TAX_INCLUDE_ID := L_TAX_JOB_CREATION_ORDERED(L_JC).TAX_INCLUDE_ID;
               end if;

            end if;

         end loop;

         -- if an account does not point to a job creation reconciling item, then make sure the amount is 0 this year.
         -- Must hardcode the reconcile item id

         --if(tax_job_creation_rows == 0 )
         --  {
         --    /*fformat = 'reconcile_item_id = -99'  */
         --    /*jrow = dw_tax_reconcile.find(fformat,1,dw_tax_Reconcile.rowcount()) */
         --     found = FALSE;
         --    for(j = 0; j < tax_reconcile_rows;++j)
         --    {
         --        if(tax_reconcile[j]->reconcile_item_id == -99)
         --      {
         --             found := TRUE;
         --       jrow := j;
         --      }
         --    }
         --     if(found == TRUE)
         --    tax_reconcile[jrow]->basis_amount_activity := 0;

      end if; /*  dw_tax_job_creation.rowcount() = 0 */

      --*
      --* process current year activity:
      --*
      if L_QUANTITY is null then
         L_QUANTITY := 0;
      end if;

      --*
      --* get current year activity: book retirements, additions and adjustments
      --*
      -- Maint-10475
      L_FIRST_YEAR_RETIRE    := false;
      L_EX_FIRST_YEAR_RETIRE := false;

      for I in 1 .. A_TAX_BOOK_ACTIVITY_ROWS
      loop
         L_AMOUNT := NVL(A_TAX_BOOK_ACTIVITY_SUB_REC(I).AMOUNT, 0);
         -- Maint-10475
         -- j := a_tax_book_activity_sub_rec(i).tax_activity_code_id;
         J := NVL(A_TAX_BOOK_ACTIVITY_SUB_REC(I).TAX_ACTIVITY_TYPE_ID, 1);
         if A_TAX_BOOK_ACTIVITY_SUB_REC(I).TAX_ACTIVITY_CODE_ID = 10 then
            if J = 4 then
               L_EX_FIRST_YEAR_RETIRE := true;
            else
               L_FIRST_YEAR_RETIRE := true;
            end if;
         end if;
         case --* type of activity
            when J = 1 or J = 5 or J = 6 or J = 7 or J = 8 then
               L_BOOK_ADDITION := L_BOOK_ADDITION + L_AMOUNT;
            when J = 2 or J = 12 then
               L_BOOK_RETIREMENT := L_BOOK_RETIREMENT + L_AMOUNT;
            when J = 3 then
               L_BOOK_ADJUSTMENT := L_BOOK_ADJUSTMENT + L_AMOUNT;
            when J = 4 then
               L_BOOK_EXTRAORD_RETIREMENT := L_BOOK_EXTRAORD_RETIREMENT + L_AMOUNT;
         end case;
      end loop;

      --*
      --* put the book to tax basis reconciliation activity in an array and also
      --* put the beginning tax to book differences in an array. pay attention to sign
      --* book difference is negative, tax difference is positive
      --*

      for I in 1 .. A_TAX_RECONCILE_ROWS
      loop
         L_TAX_RECONCILE_BEG(I) := A_TAX_RECONCILE_SUB_REC(I).BASIS_AMOUNT_BEG + A_TAX_RECONCILE_SUB_REC(I).BASIS_AMOUNT_TRANSFER;
         L_TAX_RECONCILE_ACT(I) := A_TAX_RECONCILE_SUB_REC(I).BASIS_AMOUNT_ACTIVITY;
         L_TAX_RECONCILE_RET(I) := A_TAX_RECONCILE_SUB_REC(I).BASIS_AMOUNT_INPUT_RETIRE;
         L_TAX_RECONCILE_TRANSFER(I) := A_TAX_RECONCILE_SUB_REC(I).BASIS_AMOUNT_TRANSFER;
         L_INPUT_RETIRE_IND(I) := A_TAX_RECONCILE_SUB_REC(I).INPUT_RETIRE_IND;

         if (L_INPUT_RETIRE_IND(I) != 0) then
            L_INPUT_RETIRE_IND(I) := 1;
         end if;

         if INSTR(UPPER(NVL(A_TAX_RECONCILE_SUB_REC(I).RECONCILE_ITEM_TYPE, ' ')), 'CREDIT') = 0 then
            L_TAX_RECONCILE_TYPE(I) := 1;
         else
            L_TAX_RECONCILE_TYPE(I) := 0;
         end if;
         /*    if(tax_reconcile[j]->basis_amount_beg_isnull == 1)
                tax_reconcile_beg[i] = 0;
              done while retrieving data from database
         */

      /*    if(tax_reconcile[j]->BASIS_AMOUNT_ACTIVITY.IsNull then
                   tax_reconcile_act[i] = 0;
            */

      end loop;

      --*
      --* Determine Tax Basis Additions.  If any tax book reconciling items are input,
      --* treat as adjustments if there are no book additions
      --*

      L_ADDITIONS             := L_BOOK_ADDITION;
      L_ADJUSTMENTS           := L_BOOK_ADJUSTMENT;
      L_RETIREMENTS           := L_BOOK_RETIREMENT;
      L_EXTRAORDINARY_RETIRES := L_BOOK_EXTRAORD_RETIREMENT;

      I := A_TAX_RECONCILE_ROWS; /*upperbound(l_tax_reconcile_beg) */

      for J in 1 .. I
      loop
         -- if(fabs(book_addition) > 1)
         L_ADDITIONS := L_ADDITIONS + L_TAX_RECONCILE_ACT(J);
         --   else
      --       adjustments := adjustments + tax_reconcile_act[j - 1];
      end loop;

      --* Process the Book Adjustments by Ratioing Through the Tax to Book differences.

      if (L_BOOK_BALANCE != 0.0) then
         L_RATIO := L_BOOK_ADJUSTMENT / L_BOOK_BALANCE;
      else
         L_RATIO := 0;
      end if;

      for J in 1 .. I
      loop
         L_CHANGE := L_TAX_RECONCILE_BEG(J) * L_RATIO;

         L_TAX_RECONCILE_END(J) := L_TAX_RECONCILE_BEG(J) + L_CHANGE + L_TAX_RECONCILE_ACT(J);

         L_ADJUSTMENTS := L_ADJUSTMENTS + L_CHANGE;
      end loop;

      --* Process the Book Retirements by Ratioing Through
      --* the Tax to Book Differences,
      --* and Determine Tax Basis Retirement.

      L_RATIO1 := (L_BOOK_RETIREMENT + L_BOOK_EXTRAORD_RETIREMENT);
      if (L_RATIO1 != 0) then
         L_RATIO1 := L_BOOK_RETIREMENT / (L_BOOK_RETIREMENT + L_BOOK_EXTRAORD_RETIREMENT);
      end if;

      L_RATIO := (L_BOOK_BALANCE + L_BOOK_ADDITION + L_BOOK_ADJUSTMENT);
      if (L_RATIO != 0) then

         L_RATIO := (L_BOOK_RETIREMENT + L_BOOK_EXTRAORD_RETIREMENT) /
                    (L_BOOK_BALANCE + L_BOOK_ADDITION + L_BOOK_ADJUSTMENT);
      else
         if (ROUND(L_TAX_BALANCE, 2) <> 0) then
            L_RATIO                    := (L_BOOK_RETIREMENT + L_BOOK_EXTRAORD_RETIREMENT) /
                                          L_TAX_BALANCE;
            L_BOOK_RETIREMENT          := 0;
            L_BOOK_EXTRAORD_RETIREMENT := 0;
            L_RETIREMENTS              := 0;
            L_EXTRAORDINARY_RETIRES    := 0;
         else
            L_RATIO := 0;
         end if;
      end if;

      L_CAP_GAIN_RETIREMENTS    := L_RETIREMENTS;
      L_CAP_GAIN_EX_RETIREMENTS := L_EXTRAORDINARY_RETIRES;

      if (L_RATIO != 0) then
         for J in 1 .. I
         loop
            L_CHANGE := (L_TAX_RECONCILE_END(J) * L_RATIO * (1 - L_INPUT_RETIRE_IND(J))) +
                        L_TAX_RECONCILE_RET(J);
            L_TAX_RECONCILE_END(J) := L_TAX_RECONCILE_END(J) - L_CHANGE;
            L_RETIREMENTS := L_RETIREMENTS + (L_CHANGE * L_RATIO1);
            L_CAP_GAIN_RETIREMENTS := L_CAP_GAIN_RETIREMENTS +
                                      (L_CHANGE * L_RATIO1 * L_TAX_RECONCILE_TYPE(J));
            L_EXTRAORDINARY_RETIRES := L_EXTRAORDINARY_RETIRES + (L_CHANGE * (1 - L_RATIO1));
            L_CAP_GAIN_EX_RETIREMENTS := L_CAP_GAIN_EX_RETIREMENTS +
                                         (L_CHANGE * (1 - L_RATIO1) * L_TAX_RECONCILE_TYPE(J));
         end loop;
      end if;

      --*
      --* Determine Ending Book Basis Balance
      --*

      L_BOOK_BALANCE_END := L_BOOK_BALANCE + L_BOOK_ADDITION + L_BOOK_ADJUSTMENT -
                            L_BOOK_RETIREMENT - L_BOOK_EXTRAORD_RETIREMENT + L_BOOK_BALANCE_ADJUST;

      --* Set The Retirement Depreciation Convention

      case TRUNC(L_RETIRE_DEPR_CONV)
         when 1 then
            L_RETIRE_DEPR_RATIO := 0;
         when 2 then
            L_RETIRE_DEPR_RATIO := 0.5;
         when 3 then
            L_RETIRE_DEPR_RATIO := 0.5;
         when 4 then
            L_RETIRE_DEPR_RATIO := 1;
      end case;

      if (L_RETIRE_DEPR_RATIO <> 1 and TRUNC(L_TAX_YEAR) = L_VINTAGE) then
         L_RETIRE_DEPR_RATIO := 0; --* First year retirements have 0 depr.
         L_FIRST_YEAR_RETIRE := true; -- Maint-10475
      end if;

      -- Maint-10475 - Make Last year of recovery completely depreciation for retire depr conv = 3
      if L_RETIRE_DEPR_CONV = 3 and L_SCOOP = true then
         L_RETIRE_DEPR_RATIO := 1;
      end if;

      --* Set The Retirement balance Convention

      case
         when TRUNC(L_RETIRE_BAL_CONV) = 1 or TRUNC(L_RETIRE_BAL_CONV) = 3 or
              TRUNC(L_RETIRE_BAL_CONV) = 4 then
            L_RETIRE_BAL_RATIO := 0;
         when TRUNC(L_RETIRE_BAL_CONV) = 2 then
            L_RETIRE_BAL_RATIO := 1;
      end case;

      -- If an ordinary or extraordinary retirement finishes off the tax balance, and
      -- this is an ADR-type convention for retirements, then scoop

      -- Maint - 10475 commented out accum_ordinary retires
      if (((L_RETIRE_BAL_CONV = 3 or L_RETIRE_BAL_CONV = 4 /*or l_accum_ordinary_retires <> 0 */
         ) and
         ROUND(L_TAX_BALANCE - L_RETIREMENTS - L_EXTRAORDINARY_RETIRES - L_ACCUM_ORDINARY_RETIRES,
                 0) <= 0 and L_TAX_BALANCE > 0) or
         ((L_RETIRE_BAL_CONV = 3 or L_RETIRE_BAL_CONV = 4 /* or l_accum_ordinary_retires <> 0  */
         ) and
         ROUND(L_TAX_BALANCE - L_RETIREMENTS - L_EXTRAORDINARY_RETIRES - L_ACCUM_ORDINARY_RETIRES,
                 0) >= 0 and L_TAX_BALANCE < 0)) then
         L_SCOOP := true;
      end if;

      --* Set The Retirement Reserve Convention

      case
         when TRUNC(L_RETIRE_RES_CONV) = 1 then
            L_RETIRE_RES_RATIO := 0;
         when TRUNC(L_RETIRE_RES_CONV) = 2 or ROUND(L_RETIRE_RES_CONV) = 4 then
            if ((L_TAX_BALANCE + L_ADDITIONS) <> 0) then

               L_RETIRE_RES_RATIO := (L_ACCUM_RESERVE - (L_ACCUM_SALVAGE * L_SALVAGE_RATIO) +
                                     L_ACCUM_RESERVE_ADJUST * L_ACCUM_RESERVE_ADJUST_METHOD) /
                                     (L_TAX_BALANCE + L_ADDITIONS);
            else
               L_RETIRE_RES_RATIO := 0;
            end if;
         when TRUNC(L_RETIRE_RES_CONV) = 3 then
            L_RETIRE_RES_RATIO := 1;
      end case;

      --* Set The Salvage Convention

      case
         when TRUNC(L_SALVAGE_CONV) = 1 then
            L_SALVAGE_RATIO      := 0;
            L_BEG_SALVAGE_RATIO  := 0;
            L_DEPR_SALVAGE_RATIO := 0;
         when TRUNC(L_SALVAGE_CONV) = 2 then
            L_SALVAGE_RATIO      := 1;
            L_BEG_SALVAGE_RATIO  := 1;
            L_DEPR_SALVAGE_RATIO := 0;
         when TRUNC(L_SALVAGE_CONV) = 3 then
            L_SALVAGE_RATIO      := 1;
            L_BEG_SALVAGE_RATIO  := 0;
            L_DEPR_SALVAGE_RATIO := 0;
         when TRUNC(L_SALVAGE_CONV) = 4 then
            L_SALVAGE_RATIO      := 1;
            L_BEG_SALVAGE_RATIO  := 1;
            L_DEPR_SALVAGE_RATIO := 1;
         when TRUNC(L_SALVAGE_CONV) = 5 then
            L_SALVAGE_RATIO      := 1;
            L_BEG_SALVAGE_RATIO  := .5;
            L_DEPR_SALVAGE_RATIO := 0;
            L_ADD_RATIO          := .5;

      end case;

      -- Set The cost of removal Convention

      case
         when TRUNC(L_COST_OF_REMOVAL_CONV) = 1 then
            L_COR_RATIO := 0;
         when TRUNC(L_COST_OF_REMOVAL_CONV) = 2 then
            L_COR_RATIO := 1;
      end case;

      case
         when ROUND(L_EST_SALVAGE_CONV) = 4 then
            L_ADR_SALVAGE_RATIO := 0;

         else
            L_ADR_SALVAGE_RATIO := 1;
      end case;
      --* Set The Retirement Depreciation Convention

      case
         when TRUNC(L_EX_RETIRE_DEPR_CONV) = 1 then
            L_EX_RETIRE_DEPR_RATIO := 0;
         when TRUNC(L_EX_RETIRE_DEPR_CONV) = 2 then
            L_EX_RETIRE_DEPR_RATIO := 0.5;
         when TRUNC(L_EX_RETIRE_DEPR_CONV) = 3 then
            L_EX_RETIRE_DEPR_RATIO := 0.5;
         when TRUNC(L_EX_RETIRE_DEPR_CONV) = 4 then
            L_EX_RETIRE_DEPR_RATIO := 1;
      end case;

      if (L_EX_RETIRE_DEPR_RATIO <> 1 and L_TAX_YEAR = L_VINTAGE) then
         L_EX_RETIRE_DEPR_RATIO := 0; -- First year retirements have 0 depr.
         L_EX_FIRST_YEAR_RETIRE := true; -- Maint-10475
      end if;

      -- Maint-10475
      if L_EX_RETIRE_BAL_CONV = 3 and L_SCOOP = true then
         L_EX_RETIRE_DEPR_RATIO := 1;
      end if;
      --* Set The Retirement balance Convention

      case
         when TRUNC(L_EX_RETIRE_BAL_CONV) = 1 or TRUNC(L_EX_RETIRE_BAL_CONV) = 3 or
              TRUNC(L_EX_RETIRE_BAL_CONV) = 4 then
            L_EX_RETIRE_BAL_RATIO := 0;
         when TRUNC(L_EX_RETIRE_BAL_CONV) = 2 then
            L_EX_RETIRE_BAL_RATIO := 1;
      end case;

      --* Set The Retirement Reserve Convention

      case
         when TRUNC(L_EX_RETIRE_RES_CONV) = 1 then
            L_EX_RETIRE_RES_RATIO := 0;
         when TRUNC(L_EX_RETIRE_RES_CONV) = 2 or TRUNC(L_EX_RETIRE_RES_CONV) = 4 then
            if ((L_TAX_BALANCE + L_ADDITIONS) != 0) then
               L_EX_RETIRE_RES_RATIO := (L_ACCUM_RESERVE - (L_ACCUM_SALVAGE * L_SALVAGE_RATIO) +
                                        L_ACCUM_RESERVE_ADJUST * L_ACCUM_RESERVE_ADJUST_METHOD) /
                                        (L_TAX_BALANCE + L_ADDITIONS);
            else
               L_EX_RETIRE_RES_RATIO := 0;
            end if;
         when TRUNC(L_EX_RETIRE_RES_CONV) = 3 then
            L_EX_RETIRE_RES_RATIO := 1;
      end case;

      --* Set The Salvage Convention

      case
         when TRUNC(L_EX_SALVAGE_CONV) = 1 then
            L_EX_SALVAGE_RATIO      := 0;
            L_EX_BEG_SALVAGE_RATIO  := 0;
            L_EX_DEPR_SALVAGE_RATIO := 0;
         when TRUNC(L_EX_SALVAGE_CONV) = 2 then
            L_EX_SALVAGE_RATIO      := 1;
            L_EX_BEG_SALVAGE_RATIO  := 1;
            L_EX_DEPR_SALVAGE_RATIO := 0;
         when TRUNC(L_EX_SALVAGE_CONV) = 3 then
            L_EX_SALVAGE_RATIO      := 1;
            L_EX_BEG_SALVAGE_RATIO  := 0;
            L_EX_DEPR_SALVAGE_RATIO := 0;
         when TRUNC(L_EX_SALVAGE_CONV) = 4 then
            L_EX_SALVAGE_RATIO      := 1;
            L_EX_BEG_SALVAGE_RATIO  := 1;
            L_EX_DEPR_SALVAGE_RATIO := 1;
      end case;

      case
         when TRUNC(L_BOOK_BALANCE_ADJUST_METHOD) = 1 then
            L_BOOK_BALANCE_ADJUST_METHOD := 1;
         when TRUNC(L_BOOK_BALANCE_ADJUST_METHOD) = 2 then
            L_BOOK_BALANCE_ADJUST_METHOD := 0.5;
         when TRUNC(L_BOOK_BALANCE_ADJUST_METHOD) = 3 then
            L_BOOK_BALANCE_ADJUST_METHOD := 0;
         else
            null;
      end case;
      /*
      switch((int)accum_reserve_adjust_method)
          {
          case 1:
               accum_reserve_adjust_method = 1;
               break;
      /*    case 2
              accum_reserve_adjust_method = .5
              break; *
            default:
               accum_reserve_adjust_method = 0;
               break;
          };*/

      if (L_ACCUM_RESERVE_ADJUST_METHOD = 1) then
         L_ACCUM_RESERVE_ADJUST_METHOD := 1;
         /*else if(accum_reserve_adjust_method == 2)
         accum_reserve_adjust_method = .5; */
      else
         L_ACCUM_RESERVE_ADJUST_METHOD := 0;
      end if;
      /*
      switch((int)depreciable_base_adjust_method)
          {
          case 1:
               depreciable_base_adjust_method := 1;
               break;
          case 2:
               depreciable_base_adjust_method := 1 ; /*.5;*
               break;
          case 3:
               depreciable_base_adjust_method := 0;
               break;
          }; */
      if (L_DEPRECIABLE_BASE_ADJUST_METH = 1) then
         L_DEPRECIABLE_BASE_ADJUST_METH := 1;
      elsif (L_DEPRECIABLE_BASE_ADJUST_METH = 2) then
         L_DEPRECIABLE_BASE_ADJUST_METH := .5;
      elsif (L_DEPRECIABLE_BASE_ADJUST_METH = 3) then
         L_DEPRECIABLE_BASE_ADJUST_METH := 0;
      end if;

      --* If cnstant dollar-amount estimated salvage, than calculate an estimated salvage percent
      if ((L_EST_SALVAGE_CONV = 5 or L_EST_SALVAGE_CONV = 6) and L_ESTIMATED_SALVAGE <> 0) then
         if (ABS(L_TAX_BALANCE + L_ADDITIONS + L_ADJUSTMENTS - L_RETIREMENTS -
                 L_EXTRAORDINARY_RETIRES - L_ACCUM_ORDINARY_RETIRES) < 0.0001) then
            L_EST_SALVAGE_PCT := 0;
         else
            L_EST_SALVAGE_PCT := (L_ESTIMATED_SALVAGE -
                                 (L_ACCUM_SALVAGE + L_ACTUAL_SALVAGE + L_SALVAGE_EXTRAORD)) /
                                 (L_TAX_BALANCE + L_ADDITIONS + L_ADJUSTMENTS - L_RETIREMENTS -
                                 L_EXTRAORDINARY_RETIRES - L_ACCUM_ORDINARY_RETIRES);
         end if;
         if ((L_EST_SALVAGE_PCT * L_ESTIMATED_SALVAGE) <= 0) then
            L_EST_SALVAGE_PCT := 0;
         end if;
      end if;

      L_BEGIN_RES_IMPACT := L_RETIREMENTS * (L_RETIRE_RES_RATIO);

      L_EX_BEGIN_RES_IMPACT := L_EXTRAORDINARY_RETIRES * (L_EX_RETIRE_RES_RATIO);

      if (L_NET = 'GROSS') then
         L_RETIREMENT_DEPR    := L_RETIREMENTS * L_RETIRE_DEPR_RATIO * L_DEPR_RATE;
         L_EX_RETIREMENT_DEPR := L_EXTRAORDINARY_RETIRES * L_EX_RETIRE_DEPR_RATIO * L_DEPR_RATE;
      else
         L_RETIREMENT_DEPR    := (L_RETIREMENTS - L_BEGIN_RES_IMPACT) * L_RETIRE_DEPR_RATIO *
                                 L_DEPR_RATE;
         L_EX_RETIREMENT_DEPR := (L_EXTRAORDINARY_RETIRES - L_EX_BEGIN_RES_IMPACT) *
                                 L_EX_RETIRE_DEPR_RATIO * L_DEPR_RATE;
      end if;

      -- retire_res_impact := begin_res_impact + ex_begin_res_impact;

      if (L_RETIRE_RES_RATIO <> 0) then
         if (ABS(L_BEGIN_RES_IMPACT + L_RETIREMENT_DEPR) > ABS(L_RETIREMENTS) and
            L_RETIRE_RES_CONV <> 4) then
            L_RETIRE_RES_IMPACT := L_RETIREMENTS;
         else
            L_RETIRE_RES_IMPACT := L_BEGIN_RES_IMPACT + L_RETIREMENT_DEPR;
         end if;
      end if;

      if (L_EX_RETIRE_RES_RATIO <> 0) then
         if (ABS(L_EX_BEGIN_RES_IMPACT + L_EX_RETIREMENT_DEPR) > ABS(L_EXTRAORDINARY_RETIRES) and
            L_EX_RETIRE_RES_CONV <> 4) then
            L_EX_RETIRE_RES_IMPACT := L_EXTRAORDINARY_RETIRES;
         else
            L_EX_RETIRE_RES_IMPACT := L_EX_BEGIN_RES_IMPACT + L_EX_RETIREMENT_DEPR;
         end if;
      end if;

      L_RETIRE_RES_IMPACT := L_RETIRE_RES_IMPACT + L_EX_RETIRE_RES_IMPACT;

      L_DEPRECIABLE_BASE := L_TAX_BALANCE + L_ADJUSTMENTS * L_BOOK_BALANCE_ADJUST_METHOD +
                            L_ADDITIONS * L_ADD_RATIO - L_RETIREMENTS - L_EXTRAORDINARY_RETIRES +
                            (L_RETIREMENTS * L_RETIRE_DEPR_RATIO) +
                            (L_EXTRAORDINARY_RETIRES * L_EX_RETIRE_DEPR_RATIO) +
                            (L_DEPRECIABLE_BASE_ADJUST * L_DEPRECIABLE_BASE_ADJUST_METH);

      if (L_TAX_BALANCE <> 0) then
         L_DEPRECIABLE_BASE := L_DEPRECIABLE_BASE -
                               (L_RESERVE_AT_SWITCH * L_DEPRECIABLE_BASE / L_TAX_BALANCE);
      end if;

      if ((L_ACCUM_RESERVE - (L_ACCUM_SALVAGE * L_SALVAGE_RATIO)) = 0.0) then
         L_RESERVE_AT_SWITCH_END := 0;
      else
         L_RESERVE_AT_SWITCH_END := L_RESERVE_AT_SWITCH -
                                    ((L_RESERVE_AT_SWITCH /
                                    (L_ACCUM_RESERVE - (L_ACCUM_SALVAGE * L_SALVAGE_RATIO))) *
                                    (L_BEGIN_RES_IMPACT + L_EX_BEGIN_RES_IMPACT));
      end if;

      --* Estimated Salvage Reduces Depreciable Base Election

      if (L_EST_SALVAGE_CONV = 2 or L_EST_SALVAGE_CONV = 6) then
         L_DEPRECIABLE_BASE := L_DEPRECIABLE_BASE * (1 - L_EST_SALVAGE_PCT);
      end if;

      --* Calculate the salvage impact on the beginning reserve
      L_SALVAGE_RES_IMPACT := L_ACTUAL_SALVAGE * L_SALVAGE_RATIO * L_BEG_SALVAGE_RATIO +
                              L_SALVAGE_EXTRAORD * L_EX_SALVAGE_RATIO * L_EX_BEG_SALVAGE_RATIO;

      L_COR_RES_IMPACT := L_COST_OF_REMOVAL * L_COR_RATIO;

      L_SHORT_YEAR_NET_ADJUST := ((L_TAX_BALANCE - L_RESERVE_AT_SWITCH - L_ACCUM_RESERVE) *
                                 L_YR1_RATE * L_MONTHS_USED / 12) /
                                 (1 - (L_YR1_RATE * L_MONTHS_USED / 12));

      if (L_NET = 'NET') then
         L_RESERVE := L_ACCUM_RESERVE + L_SALVAGE_RES_IMPACT - L_COR_RES_IMPACT -
                      (L_BEGIN_RES_IMPACT * (1 - L_RETIRE_DEPR_RATIO)) -
                      (L_EX_BEGIN_RES_IMPACT * (1 - L_EX_RETIRE_DEPR_RATIO)) +
                      (L_ACCUM_RESERVE_ADJUST * L_ACCUM_RESERVE_ADJUST_METHOD) -
                      L_SHORT_YEAR_NET_ADJUST;
      end if;

      --* Re-Calculate the salvage impact on the ending reserve

      L_SALVAGE_RES_IMPACT := (L_ACTUAL_SALVAGE * L_SALVAGE_RATIO) +
                              (L_SALVAGE_EXTRAORD * L_EX_SALVAGE_RATIO);

      if ((L_RESERVE * L_DEPRECIABLE_BASE) >= 0 and ABS(L_RESERVE) >= ABS(L_DEPRECIABLE_BASE)) then
         L_DEPRECIABLE_BASE := 0;
      else
         if (L_NET = 'NET') then
            L_DEPRECIABLE_BASE := L_DEPRECIABLE_BASE - L_RESERVE +
                                  (L_ACCUM_SALVAGE + L_SALVAGE_RES_IMPACT) *
                                  (L_DEPR_SALVAGE_RATIO * L_SALVAGE_RATIO);
         end if;
      end if;

      if (L_FIXED_DEPRECIABLE_BASE <> 0) then
         L_DEPRECIABLE_BASE := L_FIXED_DEPRECIABLE_BASE;
      end if;

      if (L_NET = 'NET') then
         L_CALC_DEPRECIATION := L_DEPRECIABLE_BASE * L_YR1_RATE * L_YR1_FRACTION;
         L_DEPRECIABLE_BASE  := L_DEPRECIABLE_BASE - L_SHORT_YEAR_NET_ADJUST - L_CALC_DEPRECIATION;
         L_CALC_DEPRECIATION := L_CALC_DEPRECIATION +
                                (L_DEPRECIABLE_BASE * L_YR2_RATE * L_YR2_FRACTION);
         if ((L_YR1_RATE * L_YR1_FRACTION) + (L_YR2_RATE * L_YR2_FRACTION)) <> 0 then
            -- div by 0 check
            L_DEPRECIABLE_BASE := L_CALC_DEPRECIATION /
                                  ((L_YR1_RATE * L_YR1_FRACTION) + (L_YR2_RATE * L_YR2_FRACTION));
         end if;
      else
         L_CALC_DEPRECIATION := L_DEPRECIABLE_BASE * L_DEPR_RATE;
      end if;
      --*
      --* Check tax limitation e.g. luxury autos
      --*

      if (L_TAX_LIMITATION != -1 and L_QUANTITY > 0) then
         --  if( (l_quantity * tax_limitation) < l_calc_depreciation)
         if (L_COMPARE_RATE = 1) then
            if ((L_QUANTITY * L_TAX_LIMITATION) < L_CALC_DEPRECIATION) then
               L_CALC_DEPRECIATION := L_QUANTITY * L_TAX_LIMITATION;
            end if;
         else
            L_CALC_DEPRECIATION := L_QUANTITY * L_TAX_LIMITATION;
         end if;
      end if;

      --*
      --* Determine Ending Tax Basis Balance and Other Balances
      --*

      L_TAX_BALANCE_END := L_TAX_BALANCE + L_ADDITIONS + L_ADJUSTMENTS -
                           (L_RETIREMENTS * L_RETIRE_BAL_RATIO) -
                           (L_EXTRAORDINARY_RETIRES * L_EX_RETIRE_BAL_RATIO);

      L_ACCUM_ORDIN_RETIRES_END := L_ACCUM_ORDINARY_RETIRES +
                                   (L_RETIREMENTS * (1 - L_RETIRE_BAL_RATIO)) +
                                   (L_EXTRAORDINARY_RETIRES * (1 - L_EX_RETIRE_BAL_RATIO));

      L_ACCUM_RESERVE_END := L_ACCUM_RESERVE + L_CALC_DEPRECIATION - L_RETIRE_RES_IMPACT +
                             L_SALVAGE_RES_IMPACT - L_COR_RES_IMPACT + L_ACCUM_RESERVE_ADJUST +
                             L_DEPRECIATION_ADJUST;

      L_ACCUM_RESERVE_END       := ROUND(L_ACCUM_RESERVE_END, 2);
      L_ACCUM_ORDIN_RETIRES_END := ROUND(L_ACCUM_ORDIN_RETIRES_END, 2);
      L_TAX_BALANCE_END         := ROUND(L_TAX_BALANCE_END, 2);

      if ((L_ACCUM_RESERVE_END * (L_TAX_BALANCE_END - (L_TAX_BALANCE_END - (L_ACCUM_ORDIN_RETIRES_END *
         L_ADR_SALVAGE_RATIO)) * L_EST_SALVAGE_PCT)) >= 0 and
         ABS(L_ACCUM_RESERVE_END) >=
         (ABS(L_TAX_BALANCE_END -
               ((L_TAX_BALANCE_END - (L_ACCUM_ORDIN_RETIRES_END * L_ADR_SALVAGE_RATIO)) *
               L_EST_SALVAGE_PCT)))) then
         L_OVER_ADJ_DEPRECIATION := L_ACCUM_RESERVE_END -
                                    (L_TAX_BALANCE_END - ((L_TAX_BALANCE_END - (L_ACCUM_ORDIN_RETIRES_END *
                                    L_ADR_SALVAGE_RATIO)) * L_EST_SALVAGE_PCT));

         if (ABS(L_OVER_ADJ_DEPRECIATION) > ABS(L_CALC_DEPRECIATION + L_DEPRECIATION_ADJUST)) then
            L_OVER_ADJ_DEPRECIATION := ROUND(L_CALC_DEPRECIATION + L_DEPRECIATION_ADJUST, 2);
            if (L_EST_SALVAGE_PCT >= 0) then
               if (ABS(L_ACCUM_RESERVE_END - L_OVER_ADJ_DEPRECIATION) > ABS(L_TAX_BALANCE_END)) then
                  L_GAIN_LOSS := L_ACCUM_RESERVE_END - L_OVER_ADJ_DEPRECIATION - L_TAX_BALANCE_END;
                  if (L_GAIN_LOSS > L_SALVAGE_RES_IMPACT) then
                     /*  l_accum_reserve_adjust := l_accum_reserve_adjust - (l_gain_loss - l_salvage_res_impact);
                     l_salvage_res_impact := 0; */
                     L_OVER_ADJ_DEPRECIATION := L_OVER_ADJ_DEPRECIATION -
                                                (L_SALVAGE_RES_IMPACT - L_GAIN_LOSS);
                     L_GAIN_LOSS             := L_SALVAGE_RES_IMPACT;
                     L_SALVAGE_RES_IMPACT    := 0;
                  else
                     L_SALVAGE_RES_IMPACT := L_SALVAGE_RES_IMPACT - L_GAIN_LOSS;
                  end if;
               end if;
            else
               --* est_salvage_pct < 0
               if (ABS(L_ACCUM_RESERVE_END - L_OVER_ADJ_DEPRECIATION) >
                  ABS(L_TAX_BALANCE_END * (1 - L_EST_SALVAGE_PCT))) then
                  L_GAIN_LOSS := L_ACCUM_RESERVE_END - L_OVER_ADJ_DEPRECIATION -
                                 (L_TAX_BALANCE_END * (1 - L_EST_SALVAGE_PCT));
                  if (L_GAIN_LOSS > L_SALVAGE_RES_IMPACT) then
                     /*  accum_reserve_adjust := l_accum_reserve_adjust - (l_gain_loss - l_salvage_res_impact);
                     l_salvage_res_impact := 0; */
                     L_OVER_ADJ_DEPRECIATION := L_OVER_ADJ_DEPRECIATION -
                                                (L_SALVAGE_RES_IMPACT - L_GAIN_LOSS);
                     L_GAIN_LOSS             := L_SALVAGE_RES_IMPACT;
                     L_SALVAGE_RES_IMPACT    := 0;
                  else
                     L_SALVAGE_RES_IMPACT := L_SALVAGE_RES_IMPACT - L_GAIN_LOSS;
                  end if;
               end if;
            end if; --* est_salvage_pct >=  0
         end if;
         L_ACCUM_RESERVE_END := L_ACCUM_RESERVE_END - L_GAIN_LOSS - L_OVER_ADJ_DEPRECIATION;
         L_SCOOP             := false;
      end if;

      if (L_SCOOP = true) then
         --* scoop rest of depreciation in the last year of depreciation
         L_AMOUNT := (L_TAX_BALANCE_END -
                     (L_TAX_BALANCE_END - (L_ACCUM_ORDIN_RETIRES_END * L_ADR_SALVAGE_RATIO)) *
                     L_EST_SALVAGE_PCT) - L_ACCUM_RESERVE_END;

         L_ACCUM_RESERVE_END := L_ACCUM_RESERVE_END + L_AMOUNT;

         L_OVER_ADJ_DEPRECIATION := L_OVER_ADJ_DEPRECIATION - L_AMOUNT;

      end if; --* scoop
      if (L_RETIRE_BAL_CONV = 4) then
         if (ABS(L_TAX_BALANCE_END - L_ACCUM_RESERVE_END) < 0.1 and
            ABS(L_TAX_BALANCE_END - L_ACCUM_ORDIN_RETIRES_END) < 0.1) then
            L_RETIRE_RES_IMPACT       := L_RETIRE_RES_IMPACT + L_ACCUM_RESERVE_END;
            L_TAX_BALANCE_END         := 0;
            L_ACCUM_RESERVE_END       := 0;
            L_ACCUM_ORDIN_RETIRES_END := 0;
         end if;
      end if;

      L_DEPRECIATION := L_CALC_DEPRECIATION + L_DEPRECIATION_ADJUST - L_OVER_ADJ_DEPRECIATION;

      L_ACCUM_SALVAGE_END := (L_ACCUM_SALVAGE * L_SALVAGE_RATIO) + L_SALVAGE_RES_IMPACT;

      if (L_RLIFE = 1 and L_LIFE > 0) then
         L_SL_RESERVE_END := L_SL_RESERVE - ((L_TAX_BALANCE + L_TAX_BALANCE_END) / (L_LIFE * 2));
      else
         L_SL_RESERVE_END := 0;
      end if;

      --if((tax_balance + additions) != 0) /* div by 0 check */
      --  {
      if (L_GAIN_LOSS_CONV = 2) then
         --* On Asset
         L_GAIN_LOSS := L_GAIN_LOSS + L_ACTUAL_SALVAGE -
                        (L_RETIREMENTS - (L_RETIRE_RES_IMPACT - L_EX_RETIRE_RES_IMPACT));

         --  (((al_ccum_reserve + (l_accum_reserve_adjust * l_accum_reserve_adjust_method)
         --  - (l_accum_salvage * l_salvage_ratio )) / (l_tax_balance + l_additions)
         --     * l_retirements) + l_retirement_depr)) ;
         -- if(l_cap_gain_conv ==  3 l_actual_salvage - l_cap_gain_retirements > 0 )
         --     capital_gain_loss = actual_salvage - cap_gain_retirements;

         if ((L_CAP_GAIN_CONV = 3 or L_CAP_GAIN_CONV = 4 or L_CAP_GAIN_CONV = 5 or
            L_CAP_GAIN_CONV = 6) and (L_ACTUAL_SALVAGE - L_CAP_GAIN_RETIREMENTS) > 0) then
            L_CAPITAL_GAIN_LOSS := L_ACTUAL_SALVAGE - L_CAP_GAIN_RETIREMENTS;
         end if;
         if ((L_CAP_GAIN_CONV = 4 or L_CAP_GAIN_CONV = 5) and L_GAIN_LOSS > 0) then
            L_CAPITAL_GAIN_LOSS := (L_GAIN_LOSS - (L_GAIN_LOSS - L_CAPITAL_GAIN_LOSS) * .2);
         end if;
         if ((L_CAP_GAIN_CONV = 3 --* 1245 -  Loss Ordinary
            or L_CAP_GAIN_CONV = 4 --* 1250 Loss Ordinary
            ) and L_GAIN_LOSS < 0) then
            L_CAPITAL_GAIN_LOSS := 0;
         elsif ((L_CAP_GAIN_CONV = 5 --* 1245 - Loss Capital
               or L_CAP_GAIN_CONV = 6 --* 1250 Loss Capital
               ) and L_GAIN_LOSS < 0) then
            L_CAPITAL_GAIN_LOSS := L_GAIN_LOSS;
         end if;
         if L_FIRST_YEAR_RETIRE = true then
            L_CAPITAL_GAIN_LOSS := 0; -- Maint 10475
         end if;
      end if;
      if (L_EX_GAIN_LOSS_CONV = 2) then
         --* On Asset
         L_GAIN_LOSS    := L_GAIN_LOSS + L_SALVAGE_EXTRAORD -
                           (L_EXTRAORDINARY_RETIRES - (L_EX_RETIRE_RES_IMPACT)); -- - ex_retire_res_impact));
         L_EX_GAIN_LOSS := L_SALVAGE_EXTRAORD -
                           (L_EXTRAORDINARY_RETIRES - (L_EX_RETIRE_RES_IMPACT));

         --    (((l_accum_reserve + (l_accum_reserve_adjust * l_accum_reserve_adjust_method)
         --     - (accum_salvage * salvage_ratio )) / (l_tax_balance + l_additions)
         --   * extraordinary_retires) + ex_retirement_depr));
         if ((L_EX_CAP_GAIN_CONV = 3 or L_EX_CAP_GAIN_CONV = 4 or L_EX_CAP_GAIN_CONV = 5 or
            L_EX_CAP_GAIN_CONV = 6) and L_SALVAGE_EXTRAORD - L_CAP_GAIN_EX_RETIREMENTS > 0) then
            L_EX_CAPITAL_GAIN_LOSS := L_CAPITAL_GAIN_LOSS + L_SALVAGE_EXTRAORD -
                                      L_CAP_GAIN_EX_RETIREMENTS;
         end if;

         if ((L_EX_CAP_GAIN_CONV = 4 or L_EX_CAP_GAIN_CONV = 5) and L_EX_GAIN_LOSS > 0) then
            L_EX_CAPITAL_GAIN_LOSS := (L_EX_GAIN_LOSS -
                                      ((L_EX_GAIN_LOSS - L_EX_CAPITAL_GAIN_LOSS) * .2));
         end if;

         if ((L_EX_CAP_GAIN_CONV = 3 --* 1245 - Loss Ordinary
            or L_EX_CAP_GAIN_CONV = 4 --* 1250 Loss Ordinary
            ) and L_GAIN_LOSS < 0) then
            L_EX_CAPITAL_GAIN_LOSS := 0;
         elsif ((L_EX_CAP_GAIN_CONV = 5 --* 1245 - Loss Capital
               or L_EX_CAP_GAIN_CONV = 6 --* 1250 Loss Capital
               ) and L_GAIN_LOSS < 0) then
            L_EX_CAPITAL_GAIN_LOSS := L_EX_GAIN_LOSS;
         end if;
         if L_EX_FIRST_YEAR_RETIRE = true then
            L_CAPITAL_GAIN_LOSS := 0; -- Maint 10475
         end if;
      end if;
      -- }

      if (L_GAIN_LOSS_CONV = 3) then
         --  Salvage = Gain
         L_GAIN_LOSS := L_GAIN_LOSS + L_ACTUAL_SALVAGE;
      end if;

      if (L_EX_GAIN_LOSS_CONV = 3) then
         -- Salvage = Gain
         L_GAIN_LOSS    := L_GAIN_LOSS + L_SALVAGE_EXTRAORD;
         L_EX_GAIN_LOSS := L_SALVAGE_EXTRAORD;
      end if;

      L_GAIN_LOSS := L_GAIN_LOSS + L_GAIN_LOSS_ADJUST;

      L_COR_EXPENSE := L_COST_OF_REMOVAL * (1 - L_COR_RATIO);

      if (L_CAP_GAIN_CONV = 1) then
         L_CAPITAL_GAIN_LOSS := L_GAIN_LOSS - L_EX_GAIN_LOSS;
      end if;

      if (L_EX_CAP_GAIN_CONV = 1) then
         L_EX_CAPITAL_GAIN_LOSS := L_EX_GAIN_LOSS;
      end if;

      L_CAPITAL_GAIN_LOSS := L_CAPITAL_GAIN_LOSS + L_EX_CAPITAL_GAIN_LOSS;
      L_TAX_BOOK_ID       := G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).TAX_BOOK_ID;
      L_TAX_RECORD_ID     := G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).TAX_RECORD_ID;
      L_TAX_YEAR_1        := L_TAX_YEAR + 1;
      for I in 1 .. A_TAX_RECONCILE_ROWS
      loop
         if (ABS(L_TAX_RECONCILE_BEG(I)) < .0000001) then
            L_TAX_RECONCILE_BEG(I) := 0;
         end if;
         -- tax_reconcile[i - 1]->basis_amount_beg   := l_tax_reconcile_beg(i);
         if (ABS(L_TAX_RECONCILE_ACT(I)) < .0000001) then
            L_TAX_RECONCILE_ACT(I) := 0;
         end if;
         A_TAX_RECONCILE_SUB_REC(I).BASIS_AMOUNT_ACTIVITY := L_TAX_RECONCILE_ACT(I);
         if (ABS(L_TAX_RECONCILE_END(I)) < .0000001) then
            L_TAX_RECONCILE_END(I) := 0;
         end if;
         A_TAX_RECONCILE_SUB_REC(I).BASIS_AMOUNT_END := L_TAX_RECONCILE_END(I);
         if (ABS(L_TAX_RECONCILE_TRANSFER(I)) < .0000001) then
            L_TAX_RECONCILE_TRANSFER(I) := 0;
         end if;
         A_TAX_RECONCILE_SUB_REC(I).BASIS_AMOUNT_TRANSFER := L_TAX_RECONCILE_TRANSFER(I);
         /*if(reconcile_bals_rows >= i)
             {
         reconcile_bals[i - 1]->basis_amount_beg = l_tax_reconcile_end[i - 1];
             } */
         if (A_RECONCILE_BALS_ROWS > 0) then
            /* tax_reconcile[i - 1]->tax_year + 1 */
            L_FOUND := false;
            L_NYEAR := 0;
            for J in 1 .. A_RECONCILE_BALS_ROWS
            loop
               if (A_RECONCILE_BALS_SUB_REC(J).TAX_INCLUDE_ID = A_TAX_RECONCILE_SUB_REC(I).TAX_INCLUDE_ID and
                   A_RECONCILE_BALS_SUB_REC(J).TAX_INCLUDE_ID = A_TAX_RECONCILE_SUB_REC(I).TAX_RECORD_ID and
                   A_RECONCILE_BALS_SUB_REC(J).TAX_YEAR > A_TAX_RECONCILE_SUB_REC(I).TAX_YEAR and
                   A_RECONCILE_BALS_SUB_REC(J).RECONCILE_ITEM_ID = A_TAX_RECONCILE_SUB_REC(I).RECONCILE_ITEM_ID) then
                  L_FOUND := true;
                  if (A_RECONCILE_BALS_SUB_REC(J).TAX_YEAR < L_NYEAR and L_NYEAR = 0) then
                     L_NYEAR := A_RECONCILE_BALS_SUB_REC(J).TAX_YEAR;
                     L_POS   := J - 1;
                  end if;
               end if;
            end loop;
            if (L_FOUND) then
               A_RECONCILE_BALS_SUB_REC(L_POS).BASIS_AMOUNT_BEG := L_TAX_RECONCILE_END(I);
            end if;
         end if;
      end loop;

      /*rowid = tax_depr->rowid;*/
      -- if(fabs(book_balance) < .0000001)
      --    book_balance = 0;
      -- tax_depr->book_balance=book_balance;

      -- if(fabs(tax_balance) < .0000001)
      --    tax_balance = 0;
      --    tax_depr->tax_balance=tax_balance;

      if (ABS(L_REMAINING_LIFE) < .0000001) then
         L_REMAINING_LIFE := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).REMAINING_LIFE := L_REMAINING_LIFE;

      --    if(fabs(accum_reserve) < .0000001)
      --    accum_reserve = 0;
      --    tax_depr->accum_reserve=accum_reserve;

      --    if(fabs(sl_reserve) < .0000001)
      --    sl_reserve = 0;
      --    tax_depr->sl_reserve=sl_reserve;

      if (ABS(L_DEPRECIABLE_BASE) < .0000001) then
         L_DEPRECIABLE_BASE := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).DEPRECIABLE_BASE := L_DEPRECIABLE_BASE;

      --   if(fabs(fixed_depreciable_base) < .0000001)
      --    fixed_depreciable_base = 0;
      --   tax_depr->fixed_depreciable_base=fixed_depreciable_base;

      --   if(fabs(actual_salvage) < .0000001)
      --    actual_salvage = 0;
      --   tax_depr->actual_salvage=actual_salvage;

      if (ABS(L_ESTIMATED_SALVAGE) < .0000001) then
         L_ESTIMATED_SALVAGE := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ESTIMATED_SALVAGE_END := L_ESTIMATED_SALVAGE;

      if (ABS(L_ACCUM_SALVAGE) < .0000001) then
         L_ACCUM_SALVAGE := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ACCUM_SALVAGE := L_ACCUM_SALVAGE;

      if (ABS(L_ADDITIONS) < .0000001) then
         L_ADDITIONS := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ADDITIONS := L_ADDITIONS;

      if (ABS(L_TAX_BALANCE_TRANSFER) < .0000001) then
         L_TAX_BALANCE_TRANSFER := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).TRANSFERS := L_TAX_BALANCE_TRANSFER;
      if (ABS(L_ADJUSTMENTS) < .0000001) then
         L_ADJUSTMENTS := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ADJUSTMENTS := L_ADJUSTMENTS;

      if (ABS(L_RETIREMENTS) < .0000001) then
         L_RETIREMENTS := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).RETIREMENTS := L_RETIREMENTS;

      if (ABS(L_EXTRAORDINARY_RETIRES) < .0000001) then
         L_EXTRAORDINARY_RETIRES := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).EXTRAORDINARY_RETIRES := L_EXTRAORDINARY_RETIRES;

      --    if(fabs(accum_ordinary_retires) < .0000001)
      --       accum_ordinary_retires = 0;
      --    tax_depr->accum_ordinary_retires=accum_ordinary_retires;

      if (ABS(L_COST_OF_REMOVAL) < .0000001) then
         L_COST_OF_REMOVAL := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).COST_OF_REMOVAL := L_COST_OF_REMOVAL;

      if (ABS(L_COR_EXPENSE) < .0000001) then
         L_COR_EXPENSE := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).COR_EXPENSE := L_COR_EXPENSE;

      if (ABS(L_COR_RES_IMPACT) < .0000001) then
         L_COR_RES_IMPACT := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).COR_RES_IMPACT := L_COR_RES_IMPACT;

      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).NUMBER_MONTHS_BEG := L_NUMBER_MONTHS_BEG;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).NUMBER_MONTHS_END := L_NUMBER_MONTHS_END;

      if (ABS(L_DEPRECIATION) < .0000001) then
         L_DEPRECIATION := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).DEPRECIATION := L_DEPRECIATION + L_JOB_CREATION_AMOUNT;

      if (ABS(L_GAIN_LOSS) < .0000001) then
         L_GAIN_LOSS := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).GAIN_LOSS := L_GAIN_LOSS;

      if (ABS(L_EX_GAIN_LOSS) < .0000001) then
         L_EX_GAIN_LOSS := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).EX_GAIN_LOSS := L_EX_GAIN_LOSS;

      if (ABS(L_CAPITAL_GAIN_LOSS) < .0000001) then
         L_CAPITAL_GAIN_LOSS := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).CAPITAL_GAIN_LOSS := L_CAPITAL_GAIN_LOSS;

      --if(fabs(est_salvage_pct) < .0000001)
      --  est_salvage_pct := 0;
      --ax_depr->est_salvage_pct:=est_salvage_pct;

      if (ABS(L_BOOK_BALANCE_END) < .0000001) then
         L_BOOK_BALANCE_END := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).BOOK_BALANCE_END := L_BOOK_BALANCE_END;

      if (ABS(L_TAX_BALANCE_END) < .0000001) then
         L_TAX_BALANCE_END := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).TAX_BALANCE_END := L_TAX_BALANCE_END;

      if (ABS(L_ACCUM_RESERVE_END) < .0000001) then
         L_ACCUM_RESERVE_END := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ACCUM_RESERVE_END := L_ACCUM_RESERVE_END;

      if (ABS(L_SL_RESERVE_END) < .0000001) then
         L_SL_RESERVE_END := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).SL_RESERVE_END := L_SL_RESERVE_END;

      if (ABS(L_ACCUM_SALVAGE_END) < .0000001) then
         L_ACCUM_SALVAGE_END := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ACCUM_SALVAGE_END := L_ACCUM_SALVAGE_END;

      if (ABS(L_ACCUM_ORDIN_RETIRES_END) < .0000001) then
         L_ACCUM_ORDIN_RETIRES_END := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ACCUM_ORDIN_RETIRES_END := L_ACCUM_ORDIN_RETIRES_END;

      if (ABS(L_RETIRE_INVOL_CONV) < .0000001) then
         L_RETIRE_INVOL_CONV := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).RETIRE_INVOL_CONV := L_RETIRE_INVOL_CONV;

      if (ABS(L_SALVAGE_INVOL_CONV) < .0000001) then
         L_SALVAGE_INVOL_CONV := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).SALVAGE_INVOL_CONV := L_SALVAGE_INVOL_CONV;

      if (ABS(L_SALVAGE_EXTRAORD) < .0000001) then
         L_SALVAGE_EXTRAORD := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).SALVAGE_EXTRAORD := L_SALVAGE_EXTRAORD;

      if (ABS(L_CALC_DEPRECIATION) < .0000001) then
         L_CALC_DEPRECIATION := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).CALC_DEPRECIATION := L_CALC_DEPRECIATION;

      if (ABS(L_OVER_ADJ_DEPRECIATION) < .0000001) then
         L_OVER_ADJ_DEPRECIATION := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).OVER_ADJ_DEPRECIATION := L_OVER_ADJ_DEPRECIATION;

      if (ABS(L_RETIRE_RES_IMPACT) < .0000001) then
         L_RETIRE_RES_IMPACT := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).RETIRE_RES_IMPACT := L_RETIRE_RES_IMPACT;

      if (ABS(L_EX_RETIRE_RES_IMPACT) < .0000001) then
         L_EX_RETIRE_RES_IMPACT := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).EX_RETIRE_RES_IMPACT := L_EX_RETIRE_RES_IMPACT;

      if (ABS(L_ACCUM_RESERVE_TRANSFER) < .0000001) then
         L_ACCUM_RESERVE_TRANSFER := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).TRANSFER_RES_IMPACT := L_ACCUM_RESERVE_TRANSFER;

      if (ABS(L_SALVAGE_RES_IMPACT) < .0000001) then
         L_SALVAGE_RES_IMPACT := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).SALVAGE_RES_IMPACT := L_SALVAGE_RES_IMPACT;

      if (ABS(L_ADJUSTED_RETIRE_BASIS) < .0000001) then
         L_ADJUSTED_RETIRE_BASIS := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ADJUSTED_RETIRE_BASIS := L_ADJUSTED_RETIRE_BASIS;
      if (ABS(L_RESERVE_AT_SWITCH) < .0000001) then
         L_RESERVE_AT_SWITCH := 0;
      end if;
      if (L_RESERVE_AT_SWITCH_INIT <> 0) then
         G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).RESERVE_AT_SWITCH := L_RESERVE_AT_SWITCH_INIT;
      end if;

      if (ABS(L_RESERVE_AT_SWITCH_END) < .0000001) then
         L_RESERVE_AT_SWITCH_END := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).RESERVE_AT_SWITCH_END := L_RESERVE_AT_SWITCH_END;

      if (ABS(L_ESTIMATED_SALVAGE) < .0000001) then
         L_ESTIMATED_SALVAGE := 0;
      end if;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).ESTIMATED_SALVAGE_END := L_ESTIMATED_SALVAGE;
      G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).JOB_CREATION_AMOUNT := L_JOB_CREATION_AMOUNT;
      if (A_TAX_DEPR_BALS_ROWS > 0) then
         /*rowid = tax_depr_bals->rowid; */
         if (ABS(L_BOOK_BALANCE_END) < .0000001) then
            L_BOOK_BALANCE_END := 0;
         end if;
         G_TAX_DEPR_REC(A_TAX_DEPR_BALS_INDEX).BOOK_BALANCE := L_BOOK_BALANCE_END;

         if (ABS(L_ESTIMATED_SALVAGE) < .0000001) then
            L_ESTIMATED_SALVAGE := 0;
         end if;
         G_TAX_DEPR_REC(A_TAX_DEPR_BALS_INDEX).ESTIMATED_SALVAGE_END := L_ESTIMATED_SALVAGE;

         if (ABS(L_TAX_BALANCE_END) < .0000001) then
            L_TAX_BALANCE_END := 0;
         end if;
         G_TAX_DEPR_REC(A_TAX_DEPR_BALS_INDEX).TAX_BALANCE := L_TAX_BALANCE_END;

         if (ABS(L_ACCUM_RESERVE_END) < .0000001) then
            L_ACCUM_RESERVE_END := 0;
         end if;
         G_TAX_DEPR_REC(A_TAX_DEPR_BALS_INDEX).ACCUM_RESERVE := L_ACCUM_RESERVE_END;

         if (ABS(L_RESERVE_AT_SWITCH_END) < .0000001) then
            L_RESERVE_AT_SWITCH_END := 0;
         end if;
         G_TAX_DEPR_REC(A_TAX_DEPR_BALS_INDEX).RESERVE_AT_SWITCH := L_RESERVE_AT_SWITCH_END;

         if (ABS(L_SL_RESERVE_END) < .0000001) then
            L_SL_RESERVE_END := 0;
         end if;
         G_TAX_DEPR_REC(A_TAX_DEPR_BALS_INDEX).SL_RESERVE := L_SL_RESERVE_END;

         if (ABS(L_ACCUM_SALVAGE_END) < .0000001) then
            L_ACCUM_SALVAGE_END := 0;
         end if;
         G_TAX_DEPR_REC(A_TAX_DEPR_BALS_INDEX).ACCUM_SALVAGE := L_ACCUM_SALVAGE_END;

         if (ABS(L_ACCUM_ORDIN_RETIRES_END) < .0000001) then
            L_ACCUM_ORDIN_RETIRES_END := 0;
         end if;
         G_TAX_DEPR_REC(A_TAX_DEPR_BALS_INDEX).ACCUM_ORDINARY_RETIRES := L_ACCUM_ORDIN_RETIRES_END;

         if (ABS(L_FIXED_DEPRECIABLE_BASE) < .0000001) then
            L_FIXED_DEPRECIABLE_BASE := 0;
         end if;
         G_TAX_DEPR_REC(A_TAX_DEPR_BALS_INDEX).FIXED_DEPRECIABLE_BASE := L_FIXED_DEPRECIABLE_BASE;
         -- Rake foreward est_salvage pst. If user wants to turn offf est salvage for a tax year,
         -- then input very low number (.e.g. .00001) and the rake forward will not occur.

         if (G_TAX_DEPR_REC(A_TAX_DEPR_BALS_INDEX).EST_SALVAGE_PCT = 0) then
            G_TAX_DEPR_REC(A_TAX_DEPR_BALS_INDEX).EST_SALVAGE_PCT := L_EST_SALVAGE_PCT;
         end if;
         G_TAX_DEPR_REC(A_TAX_DEPR_BALS_INDEX).NUMBER_MONTHS_BEG := L_NUMBER_MONTHS_END;
      end if;

      return 0;

   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'Calc Record ID=' || TO_CHAR(G_TAX_DEPR_REC(A_TAX_DEPR_INDEX).TAX_RECORD_ID) || '  ' ||
                   sqlerrm(L_CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return - 1;
   end CALC;

   -- =============================================================================
   --  Procedure SET_SESSION_PARAMETER
   -- =============================================================================
   procedure SET_SESSION_PARAMETER is
      cursor SESSION_PARAM_CUR is
         select PARAMETER, value, type
           from PP_SESSION_PARAMETERS
          where LOWER(USERS) = 'all'
             or LOWER(USERS) = 'depr';
      L_COUNT pls_integer;
      I       pls_integer;
      type TYPE_SESSION_PARAM_REC is table of SESSION_PARAM_CUR%rowtype;
      L_SESSION_PARAM_REC TYPE_SESSION_PARAM_REC;
      L_CODE              pls_integer;
      L_SQL               varchar2(200);
   begin

      open SESSION_PARAM_CUR;
      fetch SESSION_PARAM_CUR bulk collect
         into L_SESSION_PARAM_REC;
      close SESSION_PARAM_CUR;

      L_COUNT := L_SESSION_PARAM_REC.COUNT;
      for I in 1 .. L_COUNT
      loop
         if L_SESSION_PARAM_REC(I).TYPE = 'number' or L_SESSION_PARAM_REC(I).TYPE = 'boolean' then
            L_SQL := 'alter session set ' || L_SESSION_PARAM_REC(I).PARAMETER || ' = ' || L_SESSION_PARAM_REC(I).VALUE;
         elsif L_SESSION_PARAM_REC(I).TYPE = 'string' then
            L_SQL := 'alter session set ' || L_SESSION_PARAM_REC(I).PARAMETER || ' = ''' || L_SESSION_PARAM_REC(I).VALUE || '''';
         end if;
         execute immediate L_SQL;
         WRITE_LOG(G_JOB_NO,
                   0,
                   0,
                   'Set Session Parameter ' || L_SESSION_PARAM_REC(I).PARAMETER || ' = ' || L_SESSION_PARAM_REC(I).VALUE);
      end loop;
      return;
   exception
      when NO_DATA_FOUND then
         return;
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   5,
                   L_CODE,
                   'Set Session Parameter ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return;
   end SET_SESSION_PARAMETER;

   -- =============================================================================
   --  Procedure WRITE_LOG
   -- =============================================================================
   procedure WRITE_LOG(A_JOB_NO     number,
                       A_ERROR_TYPE number,
                       A_CODE       number,
                       A_MSG        varchar2) as
      pragma autonomous_transaction;
      L_CODE     pls_integer;
      L_MSG      varchar2(2000);
      L_CUR_TS   timestamp;
      L_DATE_STR varchar2(100);
   begin
      L_CUR_TS   := CURRENT_TIMESTAMP;
      L_DATE_STR := TO_CHAR(sysdate, 'HH24:MI:SS');
      L_MSG      := A_MSG;
      DBMS_OUTPUT.PUT_LINE(L_MSG);
      insert into TAX_JOB_LOG
         (JOB_NO, LINE_NO, LOG_DATE, ERROR_TYPE, error_code, MSG)
      values
         (A_JOB_NO, G_LINE_NO, sysdate, A_ERROR_TYPE, A_CODE, L_MSG);
      commit;
      G_LINE_NO := G_LINE_NO + 1;
      return;
   exception
      when others then
         L_CODE := sqlcode;
         DBMS_OUTPUT.PUT_LINE('Write Log ' || sqlerrm(L_CODE) || ' ' ||
                              DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return;
   end WRITE_LOG;

end TAX_DEPR_CALC;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (408, 0, 10, 4, 1, 0, 29581, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029581_pwrtax_TAX_DEPR_CALC.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
