/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035676_deprfcst_load.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/21/2014 Sunjin Cone    Incremental Budget Load by Fund Proj
||============================================================================
*/

create global temporary table FCST_BUDGET_DEPR_CLOS_FP_TEMP
(
 SET_OF_BOOKS_ID       number(22,0) not null,
 FUNDING_WO_ID         number(22,0),
 REVISION              number(22,0),
 FCST_DEPR_GROUP_ID    number(22,0) not null,
 GL_POST_MO_YR         date not null,
 FCST_DEPR_VERSION_ID  number(22,0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 ADDITIONS             number(22,2) default 0,
 RETIREMENTS           number(22,2) default 0,
 COST_OF_REMOVAL       number(22,2) default 0,
 SALVAGE_CASH          number(22,2) default 0,
 TAX_BASIS_ADJUSTMENTS number(22,2) default 0
) on commit preserve rows;

comment on table FCST_BUDGET_DEPR_CLOS_FP_TEMP is '(C) [05] This global table temporarily holds the forecasted closings by depreciation group, funding project, and revision for a forecasted depreciation version.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.SET_OF_BOOKS_ID is 'The system assigned unique identifer set of books being forecasted.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.FUNDING_WO_ID is 'The system assigned unique identifer of the funding project being forecasted.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.REVISION is 'The revision of the funding project being forecasted.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.FCST_DEPR_GROUP_ID is 'The system assigned unique identifer of the forecast depr group being forecasted.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.GL_POST_MO_YR is 'The month being forecasted.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.FCST_DEPR_VERSION_ID is 'The system assigned unique identifer of the forecast depr version being forecasted.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.TIME_STAMP is 'Standard system-assigned timestamp.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.USER_ID is 'Standard system-assigned user id.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.ADDITIONS is 'The forecasted additions.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.RETIREMENTS is 'The forecasted retirements.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.COST_OF_REMOVAL is 'The forecasted cost of removal.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.SALVAGE_CASH is 'The forecasted salvage cash.';
comment on column FCST_BUDGET_DEPR_CLOS_FP_TEMP.TAX_BASIS_ADJUSTMENTS is 'The forecasted tax basis offset.';

create global temporary table FCST_BUDG_CPR_DEPR_CLOS_FP_TMP
(
 ID                    number(22,0) not null,
 LOAD_ID               number(22,0) not null,
 ASSET_ID              number(22,0),
 FUNDING_WO_ID         number(22,0),
 REVISION              number(22,0),
 SET_OF_BOOKS_ID       number(22,0) not null,
 FCST_DEPR_GROUP_ID    number(22,0),
 GL_POSTING_MO_YR      date not null,
 FCST_DEPR_VERSION_ID  number(22,0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 INIT_LIFE             number(22,2) default 0,
 NET_ADDS_AND_ADJUST   number(22,2) default 0,
 RETIREMENTS           number(22,2) default 0,
 COST_OF_REMOVAL       number(22,2) default 0,
 SALVAGE_DOLLARS       number(22,2) default 0,
 TAX_BASIS_ADJUSTMENTS number(22,2) default 0,
 LONG_DESCRIPTION      varchar2(254)
) on commit preserve rows;

comment on table FCST_BUDG_CPR_DEPR_CLOS_FP_TMP is '(C) [05] This global table temporarily holds the forecasted closings by asset id, funding project, and revision for a forecasted depreciation version.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.ID is 'Primary Key.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.LOAD_ID is 'A sequential number to keep track of spearate batches.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.ASSET_ID is 'The system assigned unique identifer the asset being forecasted.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.FUNDING_WO_ID is 'The system assigned unique identifer of the funding project being forecasted.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.REVISION is 'The revision of the funding project being forecasted.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.SET_OF_BOOKS_ID is 'The system assigned unique identifer set of books being forecasted.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.FCST_DEPR_GROUP_ID is 'The system assigned unique identifer of the forecast depr group being forecasted.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.GL_POSTING_MO_YR is 'The month being forecasted.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.FCST_DEPR_VERSION_ID is 'The system assigned unique identifer of the forecast depr version being forecasted.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.TIME_STAMP is 'Standard system-assigned timestamp.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.USER_ID is 'Standard system-assigned user id.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.INIT_LIFE is 'The initial life of the asset.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.NET_ADDS_AND_ADJUST is 'The forecasted additions and adjustments.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.RETIREMENTS is 'The forecasted retirements.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.COST_OF_REMOVAL is 'The forecasted cost of removal.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.SALVAGE_DOLLARS is 'The forecasted salvage cash.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.TAX_BASIS_ADJUSTMENTS is 'The forecasted tax basis offset.';
comment on column FCST_BUDG_CPR_DEPR_CLOS_FP_TMP.LONG_DESCRIPTION is 'Long description of the asset.';


CREATE TABLE FCST_CPR_DEPR_FP
(
 ASSET_ID                       number(22,0) not null,
 FUNDING_WO_ID                  number(22,0),
 REVISION                       number(22,0),
 SET_OF_BOOKS_ID                number(22,0) not null,
 GL_POSTING_MO_YR               date not null,
 FCST_DEPR_VERSION_ID           number(22,0) not null,
 TIME_STAMP                     date,
 USER_ID                        varchar2(18),
 INIT_LIFE                      number(22,2) default 0,
 REMAINING_LIFE                 number(22,2) default 0,
 ESTIMATED_SALVAGE              number(22,8) default 0,
 BEG_ASSET_DOLLARS              number(22,2) default 0,
 NET_ADDS_AND_ADJUST            number(22,2) default 0,
 RETIREMENTS                    number(22,2) default 0,
 TRANSFERS_IN                   number(22,2) default 0,
 TRANSFERS_OUT                  number(22,2) default 0,
 ASSET_DOLLARS                  number(22,2) default 0,
 BEG_RESERVE_MONTH              number(22,2) default 0,
 SALVAGE_DOLLARS                number(22,2) default 0,
 RESERVE_ADJUSTMENT             number(22,2) default 0,
 COST_OF_REMOVAL                number(22,2) default 0,
 RESERVE_TRANS_IN               number(22,2) default 0,
 RESERVE_TRANS_OUT              number(22,2) default 0,
 DEPR_EXP_ADJUST                number(22,2) default 0,
 OTHER_CREDITS_AND_ADJUST       number(22,2) default 0,
 GAIN_LOSS                      number(22,2) default 0,
 DEPRECIATION_BASE              number(22,2) default 0,
 CURR_DEPR_EXPENSE              number(22,2) default 0,
 DEPR_RESERVE                   number(22,2) default 0,
 BEG_RESERVE_YEAR               number(22,2) default 0,
 YTD_DEPR_EXPENSE               number(22,2) default 0,
 YTD_DEPR_EXP_ADJUST            number(22,2) default 0,
 PRIOR_YTD_DEPR_EXPENSE         number(22,2) default 0,
 PRIOR_YTD_DEPR_EXP_ADJUST      number(22,2) default 0,
 ACCT_DISTRIB                   varchar2(254),
 MONTH_RATE                     number(22,8) default 0,
 COMPANY_ID                     number(22,0),
 MID_PERIOD_METHOD              varchar2(35),
 MID_PERIOD_CONV                number(22,8),
 FCST_DEPR_GROUP_ID             number(22,0),
 DEPR_EXP_ALLOC_ADJUST          number(22,2) default 0,
 FCST_DEPR_METHOD_ID            number(22,0),
 LONG_DESCRIPTION               varchar2(254),
 SALVAGE_EXPENSE                number(22,2) default 0,
 SALVAGE_EXP_ADJUST             number(22,2) default 0,
 SALVAGE_EXP_ALLOC_ADJUST       number(22,2) default 0,
 IMPAIRMENT_ASSET_AMOUNT        number(22,2) default 0,
 IMPAIRMENT_EXPENSE_AMOUNT      number(22,2) default 0,
 IMPAIRMENT_ASSET_ACTIVITY_SALV number(22,2) default 0,
 IMPAIRMENT_ASSET_BEGIN_BALANCE number(22,2) default 0
);

alter table FCST_CPR_DEPR_FP
   add constraint FCST_CPR_DEPR_FP_PK
       primary key (FCST_DEPR_VERSION_ID, ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR,  FUNDING_WO_ID , REVISION)
       using index tablespace PWRPLANT_IDX;

alter table FCST_CPR_DEPR_FP
   add constraint R_FCST_CPR_DEPRFP2
       foreign key (FCST_DEPR_VERSION_ID, SET_OF_BOOKS_ID)
       references FCST_VERSION_SET_OF_BOOKS;

alter table FCST_CPR_DEPR_FP
   add constraint R_FCST_CPR_DEPRFP3
       foreign key (FCST_DEPR_GROUP_ID, FCST_DEPR_VERSION_ID)
       references FCST_DEPR_GROUP_VERSION;

comment on table FCST_CPR_DEPR_FP is '(C) [05] This table holds the forecasted closings and calculations by asset id, funding project, and revision for a forecasted depreciation version.';
comment on column FCST_CPR_DEPR_FP.ASSET_ID is 'The system assigned unique identifer the asset being forecasted.';
comment on column FCST_CPR_DEPR_FP.FUNDING_WO_ID is 'The system assigned unique identifer of the funding project being forecasted.';
comment on column FCST_CPR_DEPR_FP.REVISION is 'The revision of the funding project being forecasted.';
comment on column FCST_CPR_DEPR_FP.SET_OF_BOOKS_ID is 'The system assigned unique identifer set of books being forecasted.';
comment on column FCST_CPR_DEPR_FP.GL_POSTING_MO_YR is 'The month being forecasted.';
comment on column FCST_CPR_DEPR_FP.FCST_DEPR_VERSION_ID is 'The system assigned unique identifer of the forecast depr version being forecasted.';
comment on column FCST_CPR_DEPR_FP.TIME_STAMP is 'Standard system-assigned timestamp.';
comment on column FCST_CPR_DEPR_FP.USER_ID is 'Standard system-assigned user id.';
comment on column FCST_CPR_DEPR_FP.INIT_LIFE is 'The initial life of the asset.';
comment on column FCST_CPR_DEPR_FP.REMAINING_LIFE is 'The remaining life of the asset.';
comment on column FCST_CPR_DEPR_FP.ESTIMATED_SALVAGE is 'Estimated net salvage as a percent (deducted from depreciable base.  Entered as a decimal.';
comment on column FCST_CPR_DEPR_FP.BEG_ASSET_DOLLARS is 'Balance of the asset in dollars at the beginning of the month.';
comment on column FCST_CPR_DEPR_FP.NET_ADDS_AND_ADJUST is 'Current month net additions and adjustments.';
comment on column FCST_CPR_DEPR_FP.RETIREMENTS is 'Current month retirements.';
comment on column FCST_CPR_DEPR_FP.TRANSFERS_IN is 'Dollars transferred from this asset this month.';
comment on column FCST_CPR_DEPR_FP.TRANSFERS_OUT is 'Dollars transferred to this asset this month.';
comment on column FCST_CPR_DEPR_FP.ASSET_DOLLARS is 'Balance of the asset in dollars at the end of the month.';
comment on column FCST_CPR_DEPR_FP.BEG_RESERVE_MONTH is 'Beginning of year depreciation reserve in dollars.';
comment on column FCST_CPR_DEPR_FP.SALVAGE_DOLLARS is 'The current month salvage dollars.';
comment on column FCST_CPR_DEPR_FP.RESERVE_ADJUSTMENT is 'The current month reserve adjustments.';
comment on column FCST_CPR_DEPR_FP.COST_OF_REMOVAL is 'The current month cost of removal.';
comment on column FCST_CPR_DEPR_FP.RESERVE_TRANS_IN is 'Reserve transferred from this asset this month.';
comment on column FCST_CPR_DEPR_FP.RESERVE_TRANS_OUT is 'Reserve transferred to this asset this month.';
comment on column FCST_CPR_DEPR_FP.DEPR_EXP_ADJUST is 'Input adjustment to depreciation expense in dollars.';
comment on column FCST_CPR_DEPR_FP.OTHER_CREDITS_AND_ADJUST is 'Current amount of other reserve adjustment (e.g., insurance proceeds) in dollars calculated by the system.';
comment on column FCST_CPR_DEPR_FP.GAIN_LOSS is 'The current month gain loss.';
comment on column FCST_CPR_DEPR_FP.DEPRECIATION_BASE is 'Depreciable base (in dollars) to which the rate is applied; saved for audit purposes.';
comment on column FCST_CPR_DEPR_FP.CURR_DEPR_EXPENSE is 'Current month depreciation or amortization expense in dollars.';
comment on column FCST_CPR_DEPR_FP.DEPR_RESERVE is 'End of month depreciation reserve in dollars.';
comment on column FCST_CPR_DEPR_FP.BEG_RESERVE_YEAR is 'Beginning of year depreciation reserve in dollars.';
comment on column FCST_CPR_DEPR_FP.YTD_DEPR_EXPENSE is 'Year-to-date calculated cumulative depreciation or amortization expense in at the end of the month.  Excludes input depr_exp_adjust.';
comment on column FCST_CPR_DEPR_FP.YTD_DEPR_EXP_ADJUST is 'Cumulative year-to-date depreciation expense adjustment (depr_exp_adjust) in dollars at the end of the month.';
comment on column FCST_CPR_DEPR_FP.PRIOR_YTD_DEPR_EXPENSE is 'Prior Month ending year-to-date cumulative depreciation or amortization expense in dollars.  Excludes input depr_exp_adjust. (i.e. beginning of month amount).';
comment on column FCST_CPR_DEPR_FP.PRIOR_YTD_DEPR_EXP_ADJUST is 'Prior Month ending year-to-date cumulative input depreciation expense adjustment (depr_exp_adjust) in dollars. (i.e. beginning of month amount).';
comment on column FCST_CPR_DEPR_FP.ACCT_DISTRIB is 'Additional accounting distribution entry (e.g. department) to supplement account information given at the deprecation group level.';
comment on column FCST_CPR_DEPR_FP.MONTH_RATE is 'Rate used in the depreciation calculation; saved for audit purposes.';
comment on column FCST_CPR_DEPR_FP.COMPANY_ID is 'System assigned unique identifier of the company.';
comment on column FCST_CPR_DEPR_FP.MID_PERIOD_METHOD is 'Depreciation method used at the time (SLRL, SYDRL, DBRL); saved for audit purposes..';
comment on column FCST_CPR_DEPR_FP.MID_PERIOD_CONV is 'Mid-month convention used (e.g., 0 = beginning of month, .5 = mid month, 1.0 = end of month); saved for audit purposes.';
comment on column FCST_CPR_DEPR_FP.FCST_DEPR_GROUP_ID is 'The system assigned unique identifer of the forecast depr group being forecasted.';
comment on column FCST_CPR_DEPR_FP.DEPR_EXP_ALLOC_ADJUST is 'This is the depreciation expense, generally initially calculated, to true up depreciation to the actual in-service month and life at the beginning of the calculation.';
comment on column FCST_CPR_DEPR_FP.FCST_DEPR_METHOD_ID is 'This allows for the override of the depreciation method (which can be associated with a depreciation schedule) at the individual asset record.  It is inherited from the CPR Depr table.';
comment on column FCST_CPR_DEPR_FP.LONG_DESCRIPTION is 'Records an optional more detailed description of the forecast CPR depreciation item.';
comment on column FCST_CPR_DEPR_FP.SALVAGE_EXPENSE is 'Salvage �expense� provison (when identified) in dollars. The default is 0.   Generally negative.';
comment on column FCST_CPR_DEPR_FP.SALVAGE_EXP_ADJUST is 'Input salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column FCST_CPR_DEPR_FP.SALVAGE_EXP_ALLOC_ADJUST is 'Calculated salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column FCST_CPR_DEPR_FP.IMPAIRMENT_ASSET_AMOUNT is 'The amount to be posted to the CPR and recorded in an asset impairment bucket which reduces the asset cost and reduces the reserve balance.';
comment on column FCST_CPR_DEPR_FP.IMPAIRMENT_EXPENSE_AMOUNT is 'The expense amount is equal to the change in NBV, e.g. the amount booked to an expense account, and only affects the reserve balance.';
comment on column FCST_CPR_DEPR_FP.IMPAIRMENT_ASSET_ACTIVITY_SALV is 'The asset activity for the basis bucket denoted as the impairment basis bucket.  This field is used to determine the appropriate amount of Estimated Salvage.';
comment on column FCST_CPR_DEPR_FP.IMPAIRMENT_ASSET_BEGIN_BALANCE is 'The beginning balance for the basis bucket denoted as the impairment basis bucket.  This field is used to determine the appropriate amount of Estimated Salvage.';

CREATE TABLE FCST_DEPR_LEDGER_FP
(
 FCST_DEPR_GROUP_ID             number(22,0) not null,
 FUNDING_WO_ID                  number(22,0),
 REVISION                       number(22,0),
 SET_OF_BOOKS_ID                number(22,0) not null,
 GL_POST_MO_YR                  date not null,
 FCST_DEPR_VERSION_ID           number(22,0) not null,
 TIME_STAMP                     date,
 USER_ID                        varchar2(18),
 DEPR_LEDGER_STATUS             number(22,0) not null,
 BEGIN_RESERVE                  number(22,2) default 0,
 END_RESERVE                    number(22,2) default 0,
 RESERVE_BAL_PROVISION          number(22,2) default 0,
 RESERVE_BAL_COR                number(22,2) default 0,
 SALVAGE_BALANCE                number(22,2) default 0,
 RESERVE_BAL_ADJUST             number(22,2) default 0,
 RESERVE_BAL_RETIREMENTS        number(22,2) default 0,
 RESERVE_BAL_TRAN_IN            number(22,2) default 0,
 RESERVE_BAL_TRAN_OUT           number(22,2) default 0,
 RESERVE_BAL_OTHER_CREDITS      number(22,2) default 0,
 RESERVE_BAL_GAIN_LOSS          number(22,2) default 0,
 RESERVE_ALLOC_FACTOR           number(22,8) default 0,
 BEGIN_BALANCE                  number(22,2) default 0,
 ADDITIONS                      number(22,2) default 0,
 RETIREMENTS                    number(22,2) default 0,
 TRANSFERS_IN                   number(22,2) default 0,
 TRANSFERS_OUT                  number(22,2) default 0,
 ADJUSTMENTS                    number(22,2) default 0,
 DEPRECIATION_BASE              number(22,2) default 0,
 END_BALANCE                    number(22,2) default 0,
 DEPRECIATION_RATE              number(22,8) default 0,
 DEPRECIATION_EXPENSE           number(22,2) default 0,
 DEPR_EXP_ADJUST                number(22,2) default 0,
 DEPR_EXP_ALLOC_ADJUST          number(22,2) default 0,
 COST_OF_REMOVAL                number(22,2) default 0,
 RESERVE_RETIREMENTS            number(22,2) default 0,
 SALVAGE_RETURNS                number(22,2) default 0,
 SALVAGE_CASH                   number(22,2) default 0,
 RESERVE_CREDITS                number(22,2) default 0,
 RESERVE_ADJUSTMENTS            number(22,2) default 0,
 RESERVE_TRAN_IN                number(22,2) default 0,
 RESERVE_TRAN_OUT               number(22,2) default 0,
 GAIN_LOSS                      number(22,2) default 0,
 VINTAGE_NET_SALVAGE_AMORT      number(22,2) default 0,
 VINTAGE_NET_SALVAGE_RESERVE    number(22,2) default 0,
 CURRENT_NET_SALVAGE_AMORT      number(22,2) default 0,
 CURRENT_NET_SALVAGE_RESERVE    number(22,2) default 0,
 IMPAIRMENT_RESERVE_BEG         number(22,2) default 0,
 IMPAIRMENT_RESERVE_ACT         number(22,2) default 0,
 IMPAIRMENT_RESERVE_END         number(22,2) default 0,
 EST_ANN_NET_ADDS               number(22,2) default 0,
 RWIP_ALLOCATION                number(22,2) default 0,
 NEW_DEPRECIATION               number(22,2) default 0,
 NEW_ADDITIONS                  number(22,2) default 0,
 TAX_BASIS_ADJUSTMENTS          number(22,2) default 0,
 TAX_BASIS_ADJUST_NORM          number(22,2) default 0,
 COR_BEG_RESERVE                number(22,2) default 0,
 COR_EXPENSE                    number(22,2) default 0,
 COR_EXP_ADJUST                 number(22,2) default 0,
 COR_RES_TRAN_IN                number(22,2) default 0,
 COR_RES_TRAN_OUT               number(22,2) default 0,
 COR_RES_ADJUST                 number(22,2) default 0,
 COR_EXP_ALLOC_ADJUST           number(22,2) default 0,
 COST_OF_REMOVAL_BASE           number(22,2) default 0,
 COST_OF_REMOVAL_RATE           number(22,8) default 0,
 COR_END_RESERVE                number(22,2) default 0,
 IMPAIRMENT_ASSET_ACTIVITY_SALV number(22,2) default 0,
 IMPAIRMENT_ASSET_BEGIN_BALANCE number(22,2) default 0,
 COR_BLENDING_ADJUSTMENT        number(22,2),
 COR_BLENDING_TRANSFER          number(22,2),
 IMPAIRMENT_ASSET_AMOUNT        number(22,2),
 IMPAIRMENT_EXPENSE_AMOUNT      number(22,2),
 RESERVE_BAL_IMPAIRMENT         number(22,2),
 RESERVE_BAL_SALVAGE_EXP        number(22,2),
 RESERVE_BLENDING_ADJUSTMENT    number(22,2),
 RESERVE_BLENDING_TRANSFER      number(22,2),
 RWIP_COST_OF_REMOVAL           number(22,2),
 RWIP_RESERVE_CREDITS           number(22,2),
 RWIP_SALVAGE_CASH              number(22,2),
 RWIP_SALVAGE_RETURNS           number(22,2),
 SALVAGE_BASE                   number(22,2),
 SALVAGE_EXPENSE                number(22,2),
 SALVAGE_EXP_ADJUST             number(22,2),
 SALVAGE_EXP_ALLOC_ADJUST       number(22,2),
 SALVAGE_RATE                   number(22,8)
);

alter table FCST_DEPR_LEDGER_FP
   add constraint PK_FCST_DEPR_LEDGERFP
       primary key (FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, FCST_DEPR_VERSION_ID,  FUNDING_WO_ID, REVISION)
       using index tablespace PWRPLANT_IDX;

alter table FCST_DEPR_LEDGER_FP
   add constraint R_FCST_DEPR_LEDGERFP1
       foreign key (FCST_DEPR_GROUP_ID, FCST_DEPR_VERSION_ID)
       references FCST_DEPR_GROUP_VERSION;

alter table FCST_DEPR_LEDGER_FP
   add constraint R_FCST_DEPR_LEDGERFP2
       foreign key (FCST_DEPR_VERSION_ID, SET_OF_BOOKS_ID)
       references FCST_VERSION_SET_OF_BOOKS;

comment on table FCST_DEPR_LEDGER_FP is '(C) [05] The Forecast Depreciation Ledger FP table maintains a record of the forecasted depreciable balance and depreciation expense each month by funding project, revision, depreciation group and forecast version as well as the depreciation reserve and the activity that occurred against it.  A forecasted depreciation ledger may exist for each set of books (or just the financial set of books), so that separate bases and even depreciation rates can be maintained for different sets of books.';
comment on column FCST_DEPR_LEDGER_FP.FCST_DEPR_GROUP_ID is 'System-assigned identifier of a particular forecast depreciation group.';
comment on column FCST_DEPR_LEDGER_FP.FUNDING_WO_ID is 'The system assigned unique identifer of the funding project being forecasted.';
comment on column FCST_DEPR_LEDGER_FP.REVISION is 'The revision of the funding project being forecasted.';
comment on column FCST_DEPR_LEDGER_FP.SET_OF_BOOKS_ID is 'System-assigned identifier of a unique set of books maintained by the company in PowerPlan.  According to the CPR_DEPR_SET_OF_BOOKS in PP System Control, this is either the financial set of forecast books or all sets of books.';
comment on column FCST_DEPR_LEDGER_FP.GL_POST_MO_YR is 'Records for a given depreciation group the accounting month and year of activity the entry represents.  Each month a new set of entries is filled in by the system.';
comment on column FCST_DEPR_LEDGER_FP.FCST_DEPR_VERSION_ID is 'System-assigned identifier of forecast depreciation version.';
comment on column FCST_DEPR_LEDGER_FP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column FCST_DEPR_LEDGER_FP.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column FCST_DEPR_LEDGER_FP.DEPR_LEDGER_STATUS is 'Records the processing status of a depreciation ledger record.  An example would be OPEN, meaning the record is available for posting.  These are coded values for program processing only.';
comment on column FCST_DEPR_LEDGER_FP.BEGIN_RESERVE is 'Records the beginning balance of the Depreciation Reserve (including the Accumulated Provision) for the depreciation group for the month in dollars.  Note that a normal reserve (credit balance) is positive.  This excludes the impairment reserve.';
comment on column FCST_DEPR_LEDGER_FP.END_RESERVE is 'Records the ending balance of the Depreciation Reserve for the depreciation group for the month in dollars.  Note that a normal reserve (credit balance) is positive.  This excludes the impairment reserve.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_BAL_PROVISION is 'Records the accumulated provision for depreciation included in the reserve for the depreciation group in dollars at month-end for closed months.  Note that the normal accumulated depreciation provision (credit balance) is positive.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_BAL_COR is 'Records the accumulated cost of removal in the reserve for the depreciation group in dollars at month end for closed months.  Note that the normal accumulated cost of removal (debit balance) is held as a negative.';
comment on column FCST_DEPR_LEDGER_FP.SALVAGE_BALANCE is 'Records the accumulated salvage proceeds in the reserve for the depreciation group in dollars at month-end for closed months.  Note that the normal accumulated salvage (credit balance) is held as a positive.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_BAL_ADJUST is 'Records the accumulated reserve adjustments in the reserve for the depreciation group in dollars at month-end for closed months.  An accumulated credit is positive; an accumulated debit is negative.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_BAL_RETIREMENTS is 'Records the accumulated retirements in the reserve for the depreciation group at month-end for closed months.  Note that the original cost allowed is included in this number, even when a gain/loss is explicitly calculated and not rolled into the reserve.  In this case the reserve_bal_gain_loss will hold the offset.  The normal balance (debit) is negative.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_BAL_TRAN_IN is 'Records the accumulated reserve transfers into the depreciation group in dollars at month-end for closed months.  The normal balance (credit) is positive.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_BAL_TRAN_OUT is 'Records the accumulated reserve transfers out of the depreciation group in dollars at month-end for closed months.  The normal balance (debit) is negative.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_BAL_OTHER_CREDITS is 'Records the accumulated other credits in the reserve for the depreciation group at month-end for closed months.  The normal balance (credit) is positive.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_BAL_GAIN_LOSS is 'Records the accumulated gain/loss recorded on the income statement for asset dispositions.  At month-end for closed months this is essentially a contra to the other accumulated reserve activities.  The normal balance (credit) is negative.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_ALLOC_FACTOR is '(Not currently used)';
comment on column FCST_DEPR_LEDGER_FP.BEGIN_BALANCE is 'Records the asset balance for the depreciation group at the beginning of the month in dollars.';
comment on column FCST_DEPR_LEDGER_FP.ADDITIONS is 'Records the asset additions activity that occurred during the month for the depreciation group in dollars.  The interpretation is the FERC activity, e.g., including adjustment transactions reclassified as a FERC add.  Normally positive.';
comment on column FCST_DEPR_LEDGER_FP.RETIREMENTS is 'Records the asset retirement activity that occurred during the month in dollars.  These are just the retirement transactions.  Normally negative.  These are also used to reduce the reserve.';
comment on column FCST_DEPR_LEDGER_FP.TRANSFERS_IN is 'Records the asset transfers into the depreciation group for the month in dollars. (Excludes transfers directly from non-unitized to unitized.).  The interpretation is the FERC activity, e.g. it excludes transfer transactions that have been reclassified as FERC additions.  Normally positive.';
comment on column FCST_DEPR_LEDGER_FP.TRANSFERS_OUT is 'Records the asset transfers out of the depreciation group that occurred during the month in dollars. (Excludes transfers directly from non-unitized to unitized.).  The interpretation is the FERC activity, e.g. it excludes transfer transactions that have been reclassified as FERC additions.  Normally negative.';
comment on column FCST_DEPR_LEDGER_FP.ADJUSTMENTS is 'Records the asset adjustments that occurred to the depreciation group during the month in dollars.  The interpretation is the FERC activity, e.g. it excludes adjustment transactions that have been reclassified as FERC additions or retirements.  (Increases in the depreciable balance are positive.)';
comment on column FCST_DEPR_LEDGER_FP.DEPRECIATION_BASE is 'Records the calculated depreciable base for the depreciation group for the month in dollars.';
comment on column FCST_DEPR_LEDGER_FP.END_BALANCE is 'Records the asset balance for the depreciation group at the end of the month in dollars.';
comment on column FCST_DEPR_LEDGER_FP.DEPRECIATION_RATE is 'Records the rate initially used to calculate the monthly depreciation expense, (for audit trail only).  It is not necessarily the effective rates; it does not take into account input adjustments or retroactive true-up (curve method).';
comment on column FCST_DEPR_LEDGER_FP.DEPRECIATION_EXPENSE is 'Records the depreciation calculated for the depreciation group for the month in dollars.  (This may be charged to an account other than depreciation expense.)';
comment on column FCST_DEPR_LEDGER_FP.DEPR_EXP_ADJUST is 'Records any input adjustment to the depreciation expense for the depreciation group for the month in dollars.';
comment on column FCST_DEPR_LEDGER_FP.DEPR_EXP_ALLOC_ADJUST is 'Records any calculated adjustment to depreciation expense for the depreciation group for the month, for example, combined group adjustments.';
comment on column FCST_DEPR_LEDGER_FP.COST_OF_REMOVAL is 'Records the unitized removal costs incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is negative, reducing the reserve.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_RETIREMENTS is '(Not Used.  See retirements).';
comment on column FCST_DEPR_LEDGER_FP.SALVAGE_RETURNS is 'Records the unitized salvaged returns costs incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column FCST_DEPR_LEDGER_FP.SALVAGE_CASH is 'Records the unitized cash salvage incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_CREDITS is 'Records the other credits incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_ADJUSTMENTS is 'Records the input dollar adjustments incurred against the Depreciation reserve for the depreciation group for the month.  (An increase to reserve, e.g. a credit, is positive.)';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_TRAN_IN is 'Records the reserve transfers in to the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_TRAN_OUT is 'Records the reserve transfers out of the depreciation group for the month.  The normal sign is negative, decreasing the reserve.';
comment on column FCST_DEPR_LEDGER_FP.GAIN_LOSS is 'Gain_loss records the gain/loss calculated for the month.  It is a contra to the retirements and is accumulated in reserve_bal_gain_loss.  A gain is normally negative; a loss positive.';
comment on column FCST_DEPR_LEDGER_FP.VINTAGE_NET_SALVAGE_AMORT is 'Amount of net salvage amortization in dollars for the current period pertaining to the particular salvage given by the gl_post_mo_year.  E.g., for the 8/1998 gl_post_mo_yr, this item will contain the amortization of 8/1998 (vintage) net salvage in all successive months.  Note that the amortization from a previous month is overlaid.  That is why it is saved in total in current_net_salvage_amort below.  This variable applies only if the salvage treatment or COR treatment on depr group is Amort.';
comment on column FCST_DEPR_LEDGER_FP.VINTAGE_NET_SALVAGE_RESERVE is 'Cumulative amount of net salvage amortization in dollars for the current period pertaining to the particular salvage and cost of removal for the vintage month/year given by the gl_post_mo_yr.  E.g., for the 8/1998 gl_post_mo_yr this item will contain the cumulative month-end amortization of 8/1998 (vintage) net salvage in all successive months.  Note that the cumulative amortization from a previous month is overlaid.  That is why it is saved in total in current_net_salvage_reserve below.  This variable applies only if the salvage treatment or COR treatment on depr group is Amort.';
comment on column FCST_DEPR_LEDGER_FP.CURRENT_NET_SALVAGE_AMORT is 'Total amount in dollars of net salvage amortization for the current month, summarized and saved.  This variable applies only if the salvage treatment or COR treatment on depr group is Amort.  Note:  This variable has the opposite sign � an expense is negative!';
comment on column FCST_DEPR_LEDGER_FP.CURRENT_NET_SALVAGE_RESERVE is 'Total cumulative amount (in dollars) of net salvage for the end of the month, summarized and saved.  This variable applies only if the salvage treatment or COR treatment on depr group is Amort.';
comment on column FCST_DEPR_LEDGER_FP.IMPAIRMENT_RESERVE_BEG is 'Beginning of the month impairment reserve in dollars.  This is excluded from the other reserve balances.  A credit balance is positive.';
comment on column FCST_DEPR_LEDGER_FP.IMPAIRMENT_RESERVE_ACT is 'Impairment reserve activity for the month in dollars whether input or calculated from retirements.  A credit balance is positive.';
comment on column FCST_DEPR_LEDGER_FP.IMPAIRMENT_RESERVE_END is 'Impairment reserve at the end of the month in dollars.  This is excluded from the other reserve balances.  A credit balance is positive.';
comment on column FCST_DEPR_LEDGER_FP.EST_ANN_NET_ADDS is 'Records the estimated annual net additions used during that monthly calculation used for audit trail only.';
comment on column FCST_DEPR_LEDGER_FP.RWIP_ALLOCATION is 'Records the optional allocation of the month end RWIP balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER_FP.NEW_DEPRECIATION is 'Records the depreciation on forecasted additions in dollars.';
comment on column FCST_DEPR_LEDGER_FP.NEW_ADDITIONS is 'Total of forecasted tax additions included book additions and calculated tax adjustments';
comment on column FCST_DEPR_LEDGER_FP.TAX_BASIS_ADJUSTMENTS is 'Non-normalized book to tax basis adjustments in the forecasted additions. (These will not get deferred taxes.)';
comment on column FCST_DEPR_LEDGER_FP.TAX_BASIS_ADJUST_NORM is 'Normalized book to tax basis (m-item) adjustments in the forecasted additions.  (These will get deferred taxes.)';
comment on column FCST_DEPR_LEDGER_FP.COR_BEG_RESERVE is 'Beginning of the month cost of removal reserve balance in dollars.  Includes both the book provision and actual incurred.';
comment on column FCST_DEPR_LEDGER_FP.COR_EXPENSE is 'Cost of removal expense provision in dollars.';
comment on column FCST_DEPR_LEDGER_FP.COR_EXP_ADJUST is 'Input cost of removal expense adjustment for the month, in dollars.';
comment on column FCST_DEPR_LEDGER_FP.COR_RES_TRAN_IN is 'Cost of removal balance transferred in during the month, normally associated with asset transfers.';
comment on column FCST_DEPR_LEDGER_FP.COR_RES_TRAN_OUT is 'Cost of removal transferred out during the month in dollars, normally associated with asset transfers.';
comment on column FCST_DEPR_LEDGER_FP.COR_RES_ADJUST is 'Input adjustments to the cost of removal reserve for the month in dollars.';
comment on column FCST_DEPR_LEDGER_FP.COR_EXP_ALLOC_ADJUST is 'Calculated cost of removal expense adjustment for the month, in dollars.';
comment on column FCST_DEPR_LEDGER_FP.COST_OF_REMOVAL_BASE is 'Records the dollar base against which the cost of removal rate was applied in that month.';
comment on column FCST_DEPR_LEDGER_FP.COST_OF_REMOVAL_RATE is 'Annual rate used to calculate the cost of removal provision; Expressed as a decimal.  Not the effective rate (e.g., it does not take into account adjustments).';
comment on column FCST_DEPR_LEDGER_FP.COR_END_RESERVE is 'Month end (net) cost of removal reserve balance in dollars.';
comment on column FCST_DEPR_LEDGER_FP.IMPAIRMENT_ASSET_ACTIVITY_SALV is 'The asset activity for the basis bucket denoted as the impairment basis bucket.  This field is used to determine the appropriate amount of Estimated Salvage.';
comment on column FCST_DEPR_LEDGER_FP.IMPAIRMENT_ASSET_BEGIN_BALANCE is 'The beginning balance for the basis bucket denoted as the impairment basis bucket.  This field is used to determine the appropriate amount of Estimated Salvage.';
comment on column FCST_DEPR_LEDGER_FP.COR_BLENDING_ADJUSTMENT is 'The COR reserve balance adjustment due to a change in blending percentage from last month to this month.';
comment on column FCST_DEPR_LEDGER_FP.COR_BLENDING_TRANSFER is 'The COR reserve transfer necessary to offset the reserve blending adjustment to zero across all source sets of books.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_BLENDING_ADJUSTMENT is 'The life reserve balance adjustment due to a change in blending percentage from last month to this month.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_BLENDING_TRANSFER is 'The life reserve transfer necessary to offset the reserve blending adjustment to zero across all source sets of books.';
comment on column FCST_DEPR_LEDGER_FP.IMPAIRMENT_ASSET_AMOUNT is 'The amount to be posted to the CPR and recorded in an asset impairment bucket which reduces the asset cost and reduces the reserve balance.';
comment on column FCST_DEPR_LEDGER_FP.IMPAIRMENT_EXPENSE_AMOUNT is 'The expense amount is equal to the change in NBV, e.g. the amount booked to an expense account, and only affects the reserve balance.';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_BAL_IMPAIRMENT is 'The accumulated impairment reserve balance (impairment_asset_amount + impairment_expense_amount)';
comment on column FCST_DEPR_LEDGER_FP.RESERVE_BAL_SALVAGE_EXP is 'Records the accumulated salvage provision (when identified) for depreciation included in the reserve for the depreciation group in dollars at month-end for closed months. Note that the normal accumulated depreciation salvage provision (debit balance) is negative because it decreases total reserve.';
comment on column FCST_DEPR_LEDGER_FP.RWIP_COST_OF_REMOVAL is 'Records the optional allocation of the month end RWIP cost of removal balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER_FP.RWIP_RESERVE_CREDITS is 'Records the optional allocation of the month end RWIP reserve credits balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER_FP.RWIP_SALVAGE_CASH is 'Records the optional allocation of the month end RWIP salvage cash balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER_FP.RWIP_SALVAGE_RETURNS is 'Records the optional allocation of the month end RWIP salvage returns balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER_FP.SALVAGE_BASE is 'Records the dollar base against which the salvage rate was applied in that month. The default is 0.';
comment on column FCST_DEPR_LEDGER_FP.SALVAGE_EXPENSE is 'Salvage ""expense"" provison (when identified) in dollars. The default is 0.   Generally negative.';
comment on column FCST_DEPR_LEDGER_FP.SALVAGE_EXP_ADJUST is 'Input salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column FCST_DEPR_LEDGER_FP.SALVAGE_EXP_ALLOC_ADJUST is 'Calculated salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column FCST_DEPR_LEDGER_FP.SALVAGE_RATE is 'Annual rate used to calculate the salvage provision; Expressed as a decimal. Not the effective rate (i.e. it does not take into expense adjustments). The default is 0.';

comment on column FCST_DEPR_LEDGER.COR_BLENDING_ADJUSTMENT is 'The COR reserve balance adjustment due to a change in blending percentage from last month to this month.';
comment on column FCST_DEPR_LEDGER.COR_BLENDING_TRANSFER is 'The COR reserve transfer necessary to offset the reserve blending adjustment to zero across all source sets of books.';
comment on column FCST_DEPR_LEDGER.RESERVE_BLENDING_ADJUSTMENT is 'The life reserve balance adjustment due to a change in blending percentage from last month to this month.';
comment on column FCST_DEPR_LEDGER.RESERVE_BLENDING_TRANSFER is 'The life reserve transfer necessary to offset the reserve blending adjustment to zero across all source sets of books.';

comment on column DEPR_LEDGER.COR_BLENDING_ADJUSTMENT is 'The COR reserve balance adjustment due to a change in blending percentage from last month to this month.';
comment on column DEPR_LEDGER.COR_BLENDING_TRANSFER is 'The COR reserve transfer necessary to offset the reserve blending adjustment to zero across all source sets of books.';
comment on column DEPR_LEDGER.RESERVE_BLENDING_ADJUSTMENT is 'The life reserve balance adjustment due to a change in blending percentage from last month to this month.';
comment on column DEPR_LEDGER.RESERVE_BLENDING_TRANSFER is 'The life reserve transfer necessary to offset the reserve blending adjustment to zero across all source sets of books.';

create table FCST_BUDGET_LOAD_ADDITIONS_FP
(
 ID                   number(22,0) not null,
 FCST_DEPR_VERSION_ID number(22,0),
 FCST_DEPR_GROUP_ID   number(22,0),
 FUNDING_WO_ID        number(22,0),
 REVISION             number(22,0),
 RECONCILE_ITEM_ID    number(22,0),
 MONTH_NUMBER         number(22,0),
 AMOUNT               number(22,2),
 TIME_STAMP           date,
 USER_ID              varchar2(18)
);

alter table FCST_BUDGET_LOAD_ADDITIONS_FP
   add constraint PK_FCST_BUDGET_LOAD_ADDSFP
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

comment on table FCST_BUDGET_LOAD_ADDITIONS_FP is '(C) [09] The Fcst Budget Load Additions table holds a record of Tax Additions by Funding Project and Revision when loading additions activity from the Capital Budgeting module to the Depreciation Forecast module.  Each record is a distinct combination of the forecast depreciation version being loaded as well as each applicable forecast depreciation group, tax reconciling item, and month of additions activity.';
comment on column FCST_BUDGET_LOAD_ADDITIONS_FP.ID is 'Sequential unique identifier for each record (pwerplant1.nextval)';
comment on column FCST_BUDGET_LOAD_ADDITIONS_FP.FCST_DEPR_VERSION_ID is 'Id of the corresponding depreciation forecast version being loaded.';
comment on column FCST_BUDGET_LOAD_ADDITIONS_FP.FCST_DEPR_GROUP_ID is 'Id of the forecast depreciation group to which the additions have been mapped.';
comment on column FCST_BUDGET_LOAD_ADDITIONS_FP.FUNDING_WO_ID is 'The system assigned unique identifer of the funding project.';
comment on column FCST_BUDGET_LOAD_ADDITIONS_FP.REVISION is 'The revision of the funding project.';
comment on column FCST_BUDGET_LOAD_ADDITIONS_FP.RECONCILE_ITEM_ID is 'Id of the tax reconciling item to which the additions have been mapped.';
comment on column FCST_BUDGET_LOAD_ADDITIONS_FP.MONTH_NUMBER is 'Month of the corresponding additions activity.';
comment on column FCST_BUDGET_LOAD_ADDITIONS_FP.AMOUNT is 'Forecasted additions sent to PowerTax to capture Tax Additions.';
comment on column FCST_BUDGET_LOAD_ADDITIONS_FP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column FCST_BUDGET_LOAD_ADDITIONS_FP.USER_ID is 'Standard system-assigned user id used for audit purposes.';


alter table FCST_CPR_DEPR
   add (SALVAGE_EXPENSE           number(22,2) default 0,
        SALVAGE_EXP_ADJUST        number(22,2) default 0,
        SALVAGE_EXP_ALLOC_ADJUST  number(22,2) default 0,
        IMPAIRMENT_ASSET_AMOUNT   number(22,2) default 0,
        IMPAIRMENT_EXPENSE_AMOUNT number(22,2) default 0);

comment on column FCST_CPR_DEPR.SALVAGE_EXPENSE is 'Salvage ""expense"" provison (when identified) in dollars. The default is 0.   Generally negative.';
comment on column FCST_CPR_DEPR.SALVAGE_EXP_ADJUST is 'Input salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column FCST_CPR_DEPR.SALVAGE_EXP_ALLOC_ADJUST is 'Calculated salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column FCST_CPR_DEPR.IMPAIRMENT_ASSET_AMOUNT is 'The amount to be posted to the CPR and recorded in an asset impairment bucket which reduces the asset cost and reduces the reserve balance.';
comment on column FCST_CPR_DEPR.IMPAIRMENT_EXPENSE_AMOUNT is 'The expense amount is equal to the change in NBV, e.g. the amount booked to an expense account, and only affects the reserve balance.';


alter table FCST_CPR_DEPR_TEMP
   add (SALVAGE_EXPENSE                number(22,2) default 0,
        SALVAGE_EXP_ADJUST             number(22,2) default 0,
        SALVAGE_EXP_ALLOC_ADJUST       number(22,2) default 0,
        IMPAIRMENT_ASSET_AMOUNT        number(22,2) default 0,
        IMPAIRMENT_EXPENSE_AMOUNT      number(22,2) default 0,
        IMPAIRMENT_ASSET_ACTIVITY_SALV number(22,2) default 0,
        IMPAIRMENT_ASSET_BEGIN_BALANCE number(22,2) default 0);

comment on column FCST_CPR_DEPR_TEMP.SALVAGE_EXPENSE is 'Salvage ""expense"" provison (when identified) in dollars. The default is 0.   Generally negative.';
comment on column FCST_CPR_DEPR_TEMP.SALVAGE_EXP_ADJUST is 'Input salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column FCST_CPR_DEPR_TEMP.SALVAGE_EXP_ALLOC_ADJUST is 'Calculated salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column FCST_CPR_DEPR_TEMP.IMPAIRMENT_ASSET_AMOUNT is 'The amount to be posted to the CPR and recorded in an asset impairment bucket which reduces the asset cost and reduces the reserve balance.';
comment on column FCST_CPR_DEPR_TEMP.IMPAIRMENT_EXPENSE_AMOUNT is 'The expense amount is equal to the change in NBV, e.g. the amount booked to an expense account, and only affects the reserve balance.';
comment on column FCST_CPR_DEPR_TEMP.IMPAIRMENT_ASSET_ACTIVITY_SALV is 'The asset activity for the basis bucket denoted as the impairment basis bucket.  This field is used to determine the appropriate amount of Estimated Salvage.';
comment on column FCST_CPR_DEPR_TEMP.IMPAIRMENT_ASSET_BEGIN_BALANCE is 'The beginning balance for the basis bucket denoted as the impairment basis bucket.  This field is used to determine the appropriate amount of Estimated Salvage.';

alter table FCST_BUDGET_LOAD
   add (REVISION       number(22,0),
        LOAD_STATUS_FP number(22,0));

comment on column FCST_BUDGET_LOAD.REVISION is 'The revision of the funding project.';
comment on column FCST_BUDGET_LOAD.LOAD_STATUS_FP is 'Records status:  Staged only = 0, Verified = 1, Error = 2, Loaded = 3, Split = 4.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (895, 0, 10, 4, 2, 0, 35676, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035676_deprfcst_load.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;