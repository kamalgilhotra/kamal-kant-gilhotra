/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053054_pwrtax_01_rpt_dtl_curr_diff_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 02/28/2019 K. Powers      PowerTax Report 217 (Detailed curr diff)
||============================================================================
*/

delete from pp_reports where trim(lower(report_number)) = 'pwrtax - 217';

INSERT INTO pp_reports (report_id, description, long_description, subsystem, datawindow, special_note, report_type, time_option, report_number, input_window, filter_option, status, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, user_comment, last_approved_date, pp_report_number, old_report_number, dynamic_dw, turn_off_multi_thread)
SELECT  (SELECT nvl(Max(report_id),0) FROM pp_reports WHERE report_id BETWEEN 400000 AND 499999)+1 report_id
      , 'Deferred Tax Recovery Detail' description
      , 'Deferred tax recovery broken out by norm_schema, tax_class, and vintage.' long_description
      , subsystem
      , 'dw_tax_rpt_curr_diff_detail' datawindow
      , special_note
      , report_type
      , time_option
      , 'PwrTax - 217' report_number
      , input_window
      , filter_option
      , status
      , pp_report_subsystem_id
      , report_type_id
      , pp_report_time_option_id
      , pp_report_filter_id
      , pp_report_status_id
      , pp_report_envir_id
      , documentation
      , user_comment
      , last_approved_date
      , pp_report_number
      , old_report_number
      , dynamic_dw
      , turn_off_multi_thread
FROM pp_reports
WHERE trim(lower(report_number)) = 'pwrtax - 216';

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15742, 0, 2018, 2, 0, 0, 53054, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053054_pwrtax_01_rpt_dtl_curr_diff_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
