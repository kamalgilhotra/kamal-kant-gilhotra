/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036986_taxrpr_tax_expensing_ccv.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 03/07/2014 Alex P.
||============================================================================
*/

insert into CLASS_CODE_VALUES
   (CLASS_CODE_ID, value, EXT_CLASS_CODE_VALUE)
   select CLASS_CODE_ID, 'Tax Expense', '6'
     from CLASS_CODE
    where DESCRIPTION = 'Tax Expensing'
      and not exists
    (select 1 from CLASS_CODE_VALUES CCV where CCV.CLASS_CODE_ID = CLASS_CODE.CLASS_CODE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1015, 0, 10, 4, 2, 0, 36986, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036986_taxrpr_tax_expensing_ccv.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;