/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047724_lessee_03_mc_disclosure_lease_classification_rpt_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  05/22/2018 David Conway     Add new lease disclosure lease classification report.
||============================================================================
*/

update pp_reports
set pp_report_time_option_id = 207
where report_number = 'Lessee - 2015';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5803, 0, 2017, 4, 0, 0, 47724, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_047724_lessee_03_mc_disclosure_lease_classification_rpt_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
