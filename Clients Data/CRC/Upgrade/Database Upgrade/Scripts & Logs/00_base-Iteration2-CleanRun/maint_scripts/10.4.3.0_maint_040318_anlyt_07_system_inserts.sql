/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040318_anlyt_07_system_inserts.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/24/2014 Chad Theilman    Asset Analytics - System Data
||============================================================================
*/

declare
    begin
	
	delete from PWRANLYT.PA_CHART;
    
    insert into PWRANLYT.PA_CHART values (1000,'PR_UnderCost','Bar2D','SingleSeries','Under Retired by Cost','ferc_plt_acct',1000,1000,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1001,'PR_OverCost','Bar2D','SingleSeries','Over Retired by Cost','ferc_plt_acct',1001,1001,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1002,'PR_YearlyCost_NOTUSED','HeatMap','HeatMap','Yearly Breakdown of Retirement Costs','process_year',1002,1002,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1003,'PR_UnderQty','Bar2D','SingleSeries','Under Retired by Quantity','ferc_plt_acct',1047,1003,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1004,'PR_OverQty','Bar2D','SingleSeries','Over Retired by Quantity','ferc_plt_acct',1048,1004,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1005,'PR_YearlyQty_NOTUSED','HeatMap','HeatMap','Yearly Breakdown of Retirements','ferc_plt_acct',1049,1005,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1006,'SD_UnitzCombo','MSCombiDY2D','MultiSeries','Survivor Distribution - Unitized','VINTAGE_INTERVAL',1003,1025,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1007,'SD_TotalCombo','MSCombiDY2D','MultiSeries','Survivor Distribution - Total','VINTAGE_INTERVAL',1005,1026,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1008,'SD_CostPerUnit','Column2D','SingleSeries','Unitized Cost per Unit','VINTAGE_INTERVAL',1007,1025,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1009,'LA_AgeDist_Cost','ScrollStackedColumn2D','MultiSeries','Asset Distribution by Age and Cost','AGE',1008,1035,5,50,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1010,'LA_PastExp_Cost','Column2D','SingleSeries','Assets Past Expected Life by Cost (Years Past Expected Life)','YEARS_PAST_EXPECTED',1011,1036,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1011,'LA_PastMax_Cost','Column2D','SingleSeries','Assets Past Max Life by Cost (Years Past Max Life)','YEARS_PAST_MAX',1012,1037,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1012,'LA_AgeDist_Qty','ScrollStackedColumn2D','MultiSeries','Asset Distribution by Age and Quantity','AGE',1013,1035,5,50,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1013,'LA_PastExp_Qty','Column2D','SingleSeries','Assets Past Expected Life by Quantity (Years Past Expected Life)','YEARS_PAST_EXPECTED',1016,1036,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1014,'LA_PastMax_Qty','Column2D','SingleSeries','Assets Past Max Life by Quantity (Years Past Max Life)','YEARS_PAST_MAX',1017,1037,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1015,'LA_AgeDist_Esc','ScrollStackedColumn2D','MultiSeries','Asset Distribution by Age and Escalated Cost','AGE',1018,1035,5,50,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1016,'LA_PastExp_Esc','Column2D','SingleSeries','Assets Past Expected Life by Escalated (Years Past Expected Life)','YEARS_PAST_EXPECTED',1021,1036,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1017,'LA_PastMax_Esc','Column2D','SingleSeries','Assets Past Max Life by Escalated Cost (Years Past Max Life)','YEARS_PAST_MAX',1022,1037,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1018,'LE_ZeroQtyCosts','StackedColumn2D','MultiSeries','Percent (%) of Costs Assigned to Assets with Zero Quantity','COMPANY',1023,1048,null,null,null,null,null,'%',null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1019,'LE_NegQtyPosCost','Bar2D','SingleSeries','Count of Asset Records with (-) Qty, (+) Costs','COMPANY',1025,1049,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1020,'LE_FutureAssets','MSCombiDY2D','MultiSeries','Future Assets (Cost &' || ' Quantity)','VINTAGE_YEAR',1026,1050,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1021,'LE_PosQtyNegCosts','Bar2D','SingleSeries','Count of Asset Records with (+) Qty, (-) Costs','COMPANY',1025,1051,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1022,'LE_ZeroCostQtys','Bar2D','SingleSeries','Count of Asset Records with Quantity but Zero Cost','COMPANY',1025,1052,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1023,'RP_AnnualRetirements','MSCombiDY2D','MultiSeries','Annual Retirements (Inverted Cost &' || ' Quantity)','PROCESS_YEAR',1028,1071,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1024,'RP_HeatMap_NOTUSED','HeatMap','HeatMap','Retirement Patterns','PROCESS_YEAR',1030,1076,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1025,'RP_UnRetireActivity','Column2D','SingleSeries','Gross UnRetirement Activity by Cost','VINTAGE_INTERVAL',1031,1081,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1026,'RP_HeatMapQty_NOTUSED','HeatMap','HeatMap','Retirement Patterns by Quantity','PROCESS_YEAR',1030,1086,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1027,'RP_UnRetireActivityQty','Column2D','SingleSeries','Gross UnRetirement Activity by Quantity','VINTAGE_INTERVAL',1032,1091,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1028,'AP_AnnualAdditions','MSCombiDY2D','MultiSeries','Annual Additions (Cost &' || ' Quantity)','PROCESS_YEAR',1033,1096,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1029,'AP_HeatMapCost_NOTUSED','HeatMap','HeatMap','Addition Patterns Heat Map','PROCESS_YEAR',1035,1101,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1030,'AP_NegativeAddsCost','Column2D','SingleSeries','Negative Addition Costs','VINTAGE_INTERVAL',1036,1106,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1031,'AP_HeatMapQty_NOTUSED','HeatMap','HeatMap','Addition Patterns by Quantity','PROCESS_YEAR',1035,1111,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1032,'AP_NegativeAddsQty','Column2D','SingleSeries','Negative Addition Quantities','VINTAGE_INTERVAL',1037,1116,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1033,'UC_AverageCosts','MSCombiDY2D','MultiSeries','Average Unit Costs - Handy Whitman Rates','VINTAGE_INTERVAL',1038,1151,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1034,'RA_EndReserveTrend','MSLine','MultiSeries','Annual Trend in Life Reserve (Actual &' || 'Theoretical)','GL_POSTING_YEAR',1040,1122,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1035,'RA_ReserveRatio_NOTUSED','HeatMap','HeatMap','Percent Over - Under Reserved','GL_POSTING_YEAR',1042,1123,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1036,'RA_AnnualChange','Column2D','SingleSeries','Annual Change in Life Reserve','GL_POSTING_YEAR',1043,1124,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1037,'CR_RemovalPerRetirement','MSCombiDY2D','MultiSeries','Cost of Removal per Retirement Quantity','PROCESS_YEAR',1044,1137,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1038,'CR_RemovalPerAddition','MSCombiDY2D','MultiSeries','Inverted Cost of Removal Per Addition Quantity','PROCESS_YEAR',1044,1142,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1039,'PR_AnnualCost','MSColumn2D','MultiSeries','Actual vs. Projected Retirement Costs','PROCESS_YEAR',1078,1147,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1040,'LA_PctAgeBand_NOTUSED','Pie2D','SingleSeries','Asset Costs by Percent of Expected Life','LIFE_BAND',1052,1156,null,null,null,'00FF00,99FF00,FFFF00,FF9900,FF0000','$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1041,'LA_PctAgeBandQty_NOTUSED','Pie2D','SingleSeries','Asset Quantity by Percent of Expected Life','LIFE_BAND',1053,1156,null,null,null,'00FF00,99FF00,FFFF00,FF9900,FF0000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1042,'CR_CorRetireMismatch_NOTUSED','HeatMap','HeatMap','Removal Per Vintage Anomalies','PROCESS_YEAR',1054,1161,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1043,'DL_AnnualEndBalance','Line','SingleSeries','Ending Balances','GL_POSTING_YEAR',1055,1166,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1044,'DL_AnnualRetires','Line','SingleSeries','Retirements','GL_POSTING_YEAR',1056,1166,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1045,'DL_AnnualAdditions','Line','SingleSeries','Additions','GL_POSTING_YEAR',1057,1166,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1046,'DL_AnnualCOR','Line','SingleSeries','Cost of Removal','GL_POSTING_YEAR',1058,1166,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1047,'DL_AnnualCorVsRetires','MSLine','MultiSeries','Cost of Removal vs. Retirements','GL_POSTING_YEAR',1058,1166,null,null,null,null,'$',null,'$',null,null,null);
    insert into PWRANLYT.PA_CHART values (1048,'DL_AnnualCorVsAdds','MSLine','MultiSeries','Cost of Removal vs. Additions','GL_POSTING_YEAR',1058,1166,null,null,null,null,'$',null,'$',null,null,null);
    insert into PWRANLYT.PA_CHART values (1049,'DL_CorBeginReserve','Line','SingleSeries','COR / Begin COR Reserve','GL_POSTING_YEAR',1059,1170,null,null,null,null,null,'%',null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1050,'DL_CORExpCost','Line','SingleSeries','COR Expense Added to Reserve','GL_POSTING_YEAR',1087,1174,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1051,'DL_CorReserveChange','Column2D','SingleSeries','Annual Change in COR Reserve','GL_POSTING_YEAR',1060,1178,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1052,'DL_RetireReserveRatio','Line','SingleSeries','Retirements / Begin Life Reserve','GL_POSTING_YEAR',1061,1182,null,null,null,null,null,'%',null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1053,'DL_DeprExpCost','Line','SingleSeries','Depreciation Expense Added to Reserve','GL_POSTING_YEAR',1088,1186,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1054,'DL_LifeReserveChange','Column2D','SingleSeries','Annual Change in Life Reserve','GL_POSTING_YEAR',1062,1190,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1055,'AA_WeightedAges','MSLine','MultiSeries','Average Age Trend','PROCESS_YEAR',1063,1194,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1056,'AA_Additions','MSCombiDY2D','MultiSeries','Addition Activity','PROCESS_YEAR',1067,1199,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1057,'AA_Retirements','MSCombiDY2D','MultiSeries','Retirement Activity','PROCESS_YEAR',1069,1204,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1058,'AA_AnnualChange','MSLine','MultiSeries','Large Annual Age Changes','PROCESS_YEAR',1063,1209,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1059,'AA_PeriodChange','MSLine','MultiSeries','Large Period Age Changes','PROCESS_YEAR',1063,1212,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1060,'RE_ZCost_Qty','Column2D','SingleSeries','Zero Cost, Non-Zero Quantity (Total Quantity)','GL_POSTING_YEAR',1070,1215,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1061,'RE_ZQty_Cost','Column2D','SingleSeries','Zero Quantity, Non-Zero Cost (Total Cost)','GL_POSTING_YEAR',1071,1218,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1062,'AP_VintageAdditions','MSCombiDY2D','MultiSeries','Additions by Vintage (Cost &' || ' Quantity)','VINTAGE_INTERVAL',1072,1223,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1063,'RP_VintageRetirements','MSCombiDY2D','MultiSeries','Retirements by Vintage (Inverted Cost &' || ' Quantity)','VINTAGE_INTERVAL',1081,1224,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1064,'PR_VintageCostCurve','MSLine','MultiSeries','Retirement Cost Curve (Inverted)','VINTAGE_INTERVAL',1076,1225,null,null,null,null,'$',null,'$',null,null,null);
    insert into PWRANLYT.PA_CHART values (1065,'PR_AnnualCost_NOTUSED','MSCombiDY2D','MultiSeries','Actual vs. Projected Retirement Costs (not used)','PROCESS_YEAR',1078,1227,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1066,'PR_VintageQtyCurve','MSLine','MultiSeries','Retirement Quantity Curve (Inverted)','VINTAGE_INTERVAL',1077,1226,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1067,'PR_AnnualQty','MSColumn2D','MultiSeries','Actual vs. Projected Retirement Quantities','PROCESS_YEAR',1079,1228,null,null,null,null,null,null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1068,'RA_PctOverUnder','Bar2D','SingleSeries','Percent Over - Under Reserved','GL_POSTING_YEAR',1084,1229,null,null,null,null,null,'%',null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1069,'CR_CORRetireMissing','Column2D','SingleSeries','COR ($) without Retirement Cost or Quantity','VINTAGE_INTERVAL',1085,1230,null,null,null,null,'$',null,null,null,null,null);
    insert into PWRANLYT.PA_CHART values (1070,'CR_CORperUnit','Column2D','SingleSeries','Cost of Removal ($) per Unit Retired','PROCESS_YEAR',1086,1137,null,null,null,null,'$',null,null,null,null,null);
    

    delete from PWRANLYT.PA_CHART_SERIES;
    

    insert into PWRANLYT.PA_CHART_SERIES values (1000,'SD_Unitz_Cost',null,null,null,null,'006699','Unitized Cost',1006,1003,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1001,'SD_Unitz_Qty','Line','S',null,5,'FF9933','Unitized Quantity',1006,1004,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1002,'SD_Total_Cost',null,null,null,null,'006699','Total Cost',1007,1005,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1003,'SD_Total_Qty','Line','S',null,5,'FF9933','Total Quantity',1007,1006,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1004,'LA_TotalCost_BEL','Bar',null,0,0,'66CC66','Before Expected Life',1009,1008,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1005,'LA_TotalCost_PEL','Bar',null,0,0,'FFCC33','Past Expected Life',1009,1009,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1006,'LA_TotalCost_PML','Bar',null,0,0,'990000','Past Max Life',1009,1010,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1007,'LA_TotalQty_BEL','Bar',null,0,0,'66CC66','Before Expected Life',1012,1013,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1008,'LA_TotalQty_PEL','Bar',null,0,0,'FFCC33','Past Expected Life',1012,1014,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1009,'LA_TotalQty_PML','Bar',null,0,0,'990000','Past Max Life',1012,1015,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1010,'LA_TotalEsc_BEL','Bar',null,0,0,'66CC66','Before Expected Life',1015,1018,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1011,'LA_TotalEsc_PEL','Bar',null,0,0,'FFCC33','Past Expected Life',1015,1019,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1012,'LA_TotalEsc_PML','Bar',null,0,0,'990000','Past Max Life',1015,1020,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1013,'LE_ZeroQtyCosts','Bar',null,0,0,'990000','Zero Qty Cost %',1018,1023,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1014,'LE_NonZeroQtyCosts','Bar',null,0,0,'006699','Non Zero Qty Cost %',1018,1024,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1015,'LE_TotalCost','Bar',null,null,null,'006699','Total Cost',1020,1027,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1016,'LE_TotalQty','Line','S',1,5,'FF9933','Total Quantity',1020,1026,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1017,'RP_RetireCost','Bar',null,null,null,'006699','Retirement Cost',1023,1028,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1018,'RP_RetireQty','Line','S',1,5,'FF9933','Retirement Quantity',1023,1029,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1019,'AP_AdditionCost','Bar',null,null,null,'006699','Addition Cost',1028,1033,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1020,'AP_AdditionQty','Line','S',1,5,'FF9933','Addition Quantity',1028,1034,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1021,'SC_AvgServiceCost','Bar',null,null,null,'006699','Average Service Cost',1033,1038,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1022,'SC_HandyWhitman','Line','S',1,2,'FF9933','Handy Whitman Rate',1033,1039,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1023,'RA_LifeReserveTrend','Line',null,1,2,'006699','Ending Life Reserve',1034,1040,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1024,'RA_TheoReserve','Line','S',1,2,'FF9933','Ending Theo Reserve',1034,1041,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1025,'CR_RemovalCost','Bar',null,null,null,'006699','Cost of Removal',1037,1044,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1026,'CR_RetireQty','Line','S',1,2,'FF9933','Retirement Quantity',1037,1045,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1027,'CR_RemovalCost','Bar',null,null,null,'006699','Cost of Removal',1038,1044,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1028,'CR_AdditionQty','Line','S',1,2,'FF9933','Addition Quantity',1038,1046,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1029,'PR_RetireCost','Bar',null,null,null,'006699','Retirement Cost',1039,1050,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1030,'PR_ProjectedCost','Bar','S',null,null,'FF9933','Projected Retirement Cost',1039,1051,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1031,'DL_CostOfRemoval','Line',null,null,null,'006699','Cost Of Removal',1047,1058,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1032,'DL_Retirements','Line','S',1,null,'FF9933','Retirements',1047,1056,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1033,'DL_CostOfRemoval','Line',null,null,null,'006699','Cost of Removal',1048,1058,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1034,'DL_Additions','Line','S',1,null,'FF9933','Additions',1048,1057,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1035,'AA_CostWeightedAge','Line',null,2,3,'006699','Cost Weighted Age',1055,1063,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1036,'AA_QuantityWeightedAge','Line','S',2,3,'FF9933','Qty Weighted Age',1055,1064,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1037,'AA_EscCostWeightedAge','Line','S',2,3,'66CC66','Escalated Cost Weighted Age',1055,1065,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1038,'AA_AdditionCost','Bar',null,null,null,'006699','Addition Cost',1056,1067,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1039,'AA_AdditionQty','Line','S',2,5,'FF9933','Addition Quantity',1056,1066,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1040,'AA_RetireCost','Bar',null,null,null,'006699','Retirement Cost',1057,1069,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1041,'AA_RetireQty','Line','S',2,5,'FF9933','Retirement Quantity',1057,1068,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1042,'AA_LCC_CostWeightedAge','Line',null,2,5,'006699','Cost Weighted Age',1058,1063,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1043,'AA_LCC_QtyWeightedAge','Line','S',2,5,'FF9933','Qty Weighted Age',1058,1064,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1044,'AA_LCC_EscWeightedAge','Line','S',2,5,'66CC66','Escalated Cost Weighted Age',1058,1065,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1045,'AA_LPC_CostWeightedAge','Line',null,2,5,'006699','Cost Weighted Age',1059,1063,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1046,'AA_LPC_QtyWeightedAge','Line','S',2,5,'FF9933','Qty Weighted Age',1059,1064,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1047,'AA_LPC_EscWeightedAge','Line','S',2,5,'66CC66','Escalated Cost Weighted Age',1059,1065,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1048,'AP_VintageAdds_Cost','Bar',null,0,0,'006699','Addition Cost',1062,1072,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1049,'AP_VintageAdds_Qty','Line','S',null,5,'FF9933','Addition Quantity',1062,1073,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1050,'RP_VintageRets_Cost','Bar',null,null,null,'006699','Retirement Cost',1063,1074,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1051,'RP_VintageRets_Qty','Line','S',null,5,'FF9933','Retirement Quantity',1063,1075,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1052,'PR_VintageCostCurve','Line',null,null,null,'006699','Retirement Cost',1064,1076,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1053,'PR_VintageCostCurveProj','Line','S',1,null,'FF9933','Projected Retirement Cost',1064,1080,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1054,'PR_VintageQtyCurve','Line',null,null,null,'006699','Retirement Quantity',1066,1077,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1055,'PR_VintageQtyCurve_Proj','Line','S',1,null,'FF9933','Projected Retirement Quantity',1066,1081,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1056,'PR_AnnualCost','Bar',null,0,0,'006699','Retirement Cost',1065,1078,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1057,'PR_AnnualCost_Proj','Bar',null,0,0,'FF9933','Projected Retirement Cost',1065,1082,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1058,'PR_AnnualQty','Bar',null,0,0,'006699','Retirement Quantity',1067,1079,null);
    insert into PWRANLYT.PA_CHART_SERIES values (1059,'PR_AnnualQty_Proj','Bar',null,0,0,'FF9933','Projected Retirement Quantity',1067,1083,null);
    commit;

    delete from PWRANLYT.PA_COLUMN_FORMAT;
   

    -- Inserts for PA_COLUMN_FORMAT
    insert into PWRANLYT.PA_COLUMN_FORMAT values (1,'CANCELLED','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (2,'COMPLETED','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (3,'IN_SERVICE','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (4,'OPEN','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (5,'POSTED_TO_CPR','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (6,'UNITIZED','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (7,'PROJRETIREQTY','{0:N}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (8,'RETIRECOST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (9,'PROJRETIRECOST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (10,'TOTALQUANTITY','{0:N}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (11,'TOTALCOST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (12,'UNITIZED_ORIGINAL_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (14,'UNITIZED_ESCALATED_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (15,'ORIGINAL_COST_PER_UNIT','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (16,'ESCALATED_COST_PER_UNIT','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (17,'ZERO_QTY_PCT','{0:P}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (18,'NONZERO_QTY_PCT','{0:P}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (19,'RETIRE_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (20,'PROJ_RETIRE_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (21,'DELTA','{0:N}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (22,'UNITIZED_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (23,'SCALED_DIFF','{0:N}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (24,'RETIRE_QTY','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (25,'PROJ_RETIRE_QTY','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (27,'UNITIZED_QTY','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (28,'COST_PER_UNIT','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (29,'TOTAL_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (30,'TOTAL_QTY','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (31,'TOTAL_QUANTITY','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (32,'TOTAL_ESCALATED_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (33,'ZERO_QTY_COSTS','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (34,'RECORDCOUNT','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (35,'TOTALRECORDS','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (36,'TOTAL_COSTS','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (37,'CURR_QTY','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (38,'CURR_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (39,'ACTIVITY_QTY','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (40,'ACTIVITY_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (41,'SCALED_VALUE','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (42,'ADDITION_QTY','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (43,'ADDITION_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (44,'AVG_SERVICE_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (45,'AVG_HW_RATE','{0:N2}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (46,'END_LIFE_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (47,'END_THEO_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (48,'END_RATIO','{0:N2}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (49,'BEGIN_LIFE_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (50,'ANNUAL_CHANGE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (51,'CURR_END_LIFE_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (52,'CURR_END_THEO_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (53,'END_BALANCE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (54,'BEGIN_BALANCE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (55,'PCT_DELTA','{0:P}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (56,'CURR_END_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (57,'GL_POSTING_YEAR','{0:####}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (58,'GL_POSTING_DATE','{0:yyyy-MM-dd}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (59,'VINTAGE_YEAR','{0:####}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (60,'PROCESS_YEAR','{0:####}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (61,'COR','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (62,'CORPERUNIT','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (63,'SCALEDVALUE','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (64,'QTY_DELTA','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (65,'QTY_SCALED_DIFF','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (68,'COR_FLAG','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (69,'COR_PER_UNIT','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (70,'PCT_COR_RETIRE_COST','{0:P2}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (71,'PCT_COR_ADDITION_COST','{0:P2}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (72,'AVG_END_BALANCE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (73,'ADDITIONS','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (74,'RETIREMENTS','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (75,'COST_OF_REMOVAL','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (76,'BEGIN_COR_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (77,'PCT_INCREASE_COR_RESERVE','{0:#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (78,'COR_EXPENSE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (79,'END_COR_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (80,'AVG_BEGIN_COR_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (81,'PCT_CHANGE_COR_RESERVE','{0:#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (82,'AVG_END_COR_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (83,'BEG_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (84,'PCT_BEGIN_RESERVE','{0:#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (85,'AVG_BEG_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (86,'DEPR_EXPENSE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (87,'END_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (88,'BEGIN_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (89,'PCT_CHANGE_LIFE_RESERVE','{0:#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (90,'AVG_BEGIN_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (91,'AVG_END_RESERVE','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (92,'RETIREMENT_QTY','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (93,'RECORD_COUNT','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (94,'POS_RETIREMENT_QTY','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (95,'NEG_RETIREMENT_QTY','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (96,'NET_RETIREMENT_QTY','{0:N0}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (97,'POS_RETIREMENT_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (98,'NEG_RETIREMENT_COST','{0:$#,##0.00}');
    insert into PWRANLYT.PA_COLUMN_FORMAT values (99,'NET_RETIREMENT_COST','{0:$#,##0.00}');
    

    delete from PWRANLYT.PA_CONFIG;
    

    -- Inserts for PA_CONFIG
    insert into PWRANLYT.PA_CONFIG values (1,'DEFAULT_VINTAGEINTERVAL','5');
    insert into PWRANLYT.PA_CONFIG values (2,'REQUIRED_FIELDS','VINTAGEINTERVAL,GROUPBY');
    insert into PWRANLYT.PA_CONFIG values (3,'SET_OF_BOOKS','1');
    insert into PWRANLYT.PA_CONFIG values (4,'LEDGER_DATE','20130331');
    insert into PWRANLYT.PA_CONFIG values (5,'STARTYEAR','2008');
    insert into PWRANLYT.PA_CONFIG values (6,'ENDYEAR','2013');
    

    delete from PWRANLYT.PA_DASHBOARD;
    

    -- Inserts for PA_DASHBOARD
    insert into PWRANLYT.PA_DASHBOARD values (1,1,'QA Single Series','A QA dashboard containing an example of every single series chart type used within the application.','Quality Control Dashboard for Single Series Charts','Dashboard/SummaryDashboard/?DashboardId=1','Y','QA Single Series','QASingleSeries');
    insert into PWRANLYT.PA_DASHBOARD values (2,2,'QA Multi Series','A QA dashboard containing an example of every multi series chart type used within the application.','Quality Control Dashboard for Multi Series Charts','Dashboard/SummaryDashboard/?DashboardId=2','Y','QA Multi Series','QAMultiSeries');
    insert into PWRANLYT.PA_DASHBOARD values (3,3,'QA Custom Series','A QA dashboard containing an example of every special or custom chart type used within the application.','Quality Control Dashboard for Special Charts','Dashboard/SummaryDashboard/?DashboardId=3','Y','QA Custom Series','QACustomSeries');
    insert into PWRANLYT.PA_DASHBOARD values (4,4,'QA Filter Group Vintage Spread','A QA dashboard containing a chart for testing Filter Group: Vintage Spreads','Quality Control Dashboard for Vintage Spreads Filters','Dashboard/SummaryDashboard/?DashboardId=4','Y','QA Vintage Spread Filter','QAVintageSpreadFilter');
    insert into PWRANLYT.PA_DASHBOARD values (5,5,'QA Filter Group CPR Ledger','A QA dashboard containing a chart for testing Filter Group: CPR Ledger','Quality Control Dashboard for CPR Ledger Filters','Dashboard/SummaryDashboard/?DashboardId=5','Y','QA CPR Ledger Filter','QACPRLedgerFilter');
    insert into PWRANLYT.PA_DASHBOARD values (6,6,'QA Filter Group CPR Balances','A QA dashboard containing a chart for testing Filter Group: CPR Balances','Quality Control Dashboard for CPR Balances Filters','Dashboard/SummaryDashboard/?DashboardId=6','Y','QA CPR Balances Filter','QACPRBalancesFilter');
    insert into PWRANLYT.PA_DASHBOARD values (7,7,'QA Filter Group DEPR Balances','A QA dashboard containing a chart for testing Filter Group: DEPR Balances','Quality Control Dashboard for DEPR Balances Filters','Dashboard/SummaryDashboard/?DashboardId=7','Y','QA DEPR Balances Filter','QADEPRBalancesFilter');
    insert into PWRANLYT.PA_DASHBOARD values (8,8,'QA Filter Group Avg Ages','A QA dashboard containing a chart for testing Filter Group: Average Ages','Quality Control Dashboard for Average Age Filters','Dashboard/SummaryDashboard/?DashboardId=8','Y','QA Average Age Filter','QAAverageAgeFilter');
    insert into PWRANLYT.PA_DASHBOARD values (9,9,'QA Filter Group CPR Activity','A QA dashboard containing a chart for testing Filter Group: CPR Activity','Quality Control Dashboard for CPR Activity Filters','Dashboard/SummaryDashboard/?DashboardId=9','Y','QA CPR Activity Filter','QACPRActivityFilter');
    insert into PWRANLYT.PA_DASHBOARD values (99,99,'Under Development','This dashboard is used to test new visuals which are under development','New visuals which are under development','Dashboard/SummaryDashboard/?DashboardId=99','Y','Under Development Dashboard','UnderDevelopment');
    insert into PWRANLYT.PA_DASHBOARD values (1000,1000,'Projected Retirements by Cost','Missed retirements have significant tax and regulatory implications, as well as increase the average age of assets on the books and decrease future depreciation rates.  Significant under-retirements can signify an inappropriate curve and/or expected life and should be investigated.  Similarly, over-retirements can also be problematic in that they may indicate that asset costs are not being recovered appropriately relative to their use.  Both scenarios should be monitored closely as they will show up in depreciation studies and rate cases.','The Projected Retirements by Cost dashboard is used to identify areas where retirements are potentially being missed and/or over-recorded by providing actual versus projected retirement comparisons based on cost distribution. The projections are calculated based on the vintage distribution of asset costs, assigned mortality curves and the expected lives of surviving assets. The values show whether an asset group is performing according to expectations for the assigned curve and life, with prominent under- and over-retired accounts highlighted for analysis.  Variance from projections are highlighted for analysis, with red indicating (-) variance (under-retirements) and yellow indicating (+) variance (over-retirements).  Additionally, total yearly projections are plotted against actual retirement costs, as well as vintage cost projections against vintage cost actuals. ','Dashboard/SummaryDashboard/?DashboardId=1000','Y','Projected Retirements by Cost Dashboard','ProjectedRetirementsCost');
    insert into PWRANLYT.PA_DASHBOARD values (1001,1001,'Projected Retirements by Quantity','Missed retirements have significant tax and regulatory implications, as well as increase the average age of assets on the books and decrease future depreciation rates.  Significant under-retirements can signify an inappropriate curve and/or expected life and should be investigated.  Similarly, over-retirements can also be problematic in that they may indicate that asset costs are not being recovered appropriately relative to their use.  Both scenarios should be monitored closely as they will show up in depreciation studies and rate cases.','The Projected Retirements by Quantity dashboard is used to identify areas where retirements are potentially being missed and/or over-recorded by providing actual versus projected retirement comparisons based on quantity distribution. The projections are calculated based on the vintage distribution of asset quantities, assigned mortality curves and the expected lives of surviving assets. The values show whether an asset group is performing according to expectations for the assigned curve and life, with prominent under- and over-retired accounts highlighted for analysis.  Variance from projections are highlighted for analysis, with red indicating (-) variance (under-retirements) and yellow indicating (+) variance (over-retirements).  Additionally, total yearly projections are plotted against actual retirement quantities, as well as vintage quantity projections against vintage quantity actuals. ','Dashboard/SummaryDashboard/?DashboardId=1001','Y','Projected Retirements by Quantity Dashboard','ProjectedRetirementsQty');
    insert into PWRANLYT.PA_DASHBOARD values (1002,1002,'Survivor Distribution','The survivor distribution dashboard allows for an analysis of vintage composition, helping to identify cost and/or quantity distortions.  Inaccurate vintage compositions can distort retirement data (for curve retired assets) as well as projected retirement data.  Large swings in cost per unit can pinpoint a cost or quantity distortion.  Non-unitized costs (total cost - unitized cost) should typically not exist more than one year after their in-service date, particularly with regards to mass assets, and their existence can indicate a unitization issue in particular accounts.','The Survivor Distribution dashboard visually displays the surviving asset balances (in-service assets) based on cost and quantity. The charts provided for this dashboard include both unitized (101 accounts) and non-unitized (106 accounts) balance information, allowing for unitized asset balances to be separately analyzed from total asset balances (unitized + non-unitized).  Additionally, the unitized cost per unit by vintage is displayed.  This dashboard is used to identify asset vintage distortions which can create problems when processing mass retirements.','Dashboard/SummaryDashboard/?DashboardId=1002','Y','Survivor Distribution Dashboard','SurvivorDistribution');
    insert into PWRANLYT.PA_DASHBOARD values (1003,1003,'Life Analysis by Cost','This dashboard is used to identify assets which are potential â€˜ghostsâ€™ within the system, meaning they have been physically retired but have not been recorded as retired on the books.  â€˜Ghostsâ€™ can be identified in the system by looking for assets that have fallen off their mortality curves (theoretically past their maximum life expectancy).  Assets with spikes in cost towards the tail-end of their expected lives indicate the same potential issue â€“ assets displaying these conditions are potential missed retirements and should be dealt with appropriately.','The Life Analysis by Cost dashboard provides a cost-based breakdown of survivor (in-service) asset balances based on their relative position to their assigned expected lives.  Assets are assigned mortality curves and expected lives, and this dashboard illustrates situations in which assets are trending towards, are past and are significantly outliving their expected lives.  The groupings shown are assets Before Expected Life (BEL), Past Expected Life (PEL) and Past Max Life (PML). Assets that are past their expected lives (colored yellow) or maximum lives (colored red) are highlighted for analysis. ','Dashboard/SummaryDashboard/?DashboardId=1003','Y','Life Analysis by Cost Dashboard','LifeAnalysisCost');
    insert into PWRANLYT.PA_DASHBOARD values (1004,1004,'Life Analysis by Quantity','This dashboard is used to identify assets which are potential â€˜ghostsâ€™ within the system, meaning they have been physically retired but have not been recorded as retired on the books.  â€˜Ghostsâ€™ can be identified in the system by looking for assets that have fallen off their mortality curves (theoretically past their maximum life expectancy).  Assets with spikes in quantity towards the tail-end of their expected lives indicate the same potential issue â€“ assets displaying these conditions are potential missed retirements and should be dealt with appropriately.','The Life Analysis by Quantity dashboard provides a quantity-based breakdown of survivor (in-service) asset balances based on their relative position to their assigned expected lives.  Assets are assigned mortality curves and expected lives, and this dashboard illustrates situations in which assets are trending towards, are past and are significantly outliving their expected lives.  The groupings shown are assets Before Expected Life (BEL), Past Expected Life (PEL) and Past Max Life (PML). Assets that are past their expected lives (colored yellow) or maximum lives (colored red) are highlighted for analysis.','Dashboard/SummaryDashboard/?DashboardId=1004','Y','Life Analysis by Quantity Dashboard','LifeAnalysisQty');
    insert into PWRANLYT.PA_DASHBOARD values (1005,1005,'Life Analysis by Escalated Cost','This dashboard is used to identify assets which are potential â€˜ghostsâ€™ within the system, meaning they have been physically retired but have not been recorded as retired on the books.  â€˜Ghostsâ€™ can be identified in the system by looking for assets that have fallen off their mortality curves (theoretically past their maximum life expectancy).  Assets with spikes in escalated cost towards the tail-end of their expected lives indicate the same potential issue â€“ assets displaying these conditions are potential missed retirements and should be dealt with appropriately.','The Life Analysis by Escalated Cost dashboard provides a escalated cost-based breakdown of survivor (in-service) asset balances based on their relative position to their assigned expected lives.  Assets are assigned mortality curves and expected lives, and this dashboard illustrates situations in which assets are trending towards, are past and/or are significantly outliving their expected lives.  The groupings shown are assets Before Expected Life (BEL), Past Expected Life (PEL) and Past Max Life (PML). Assets that are past their expected lives (colored yellow) or maximum lives (colored red) are highlighted for analysis.','Dashboard/SummaryDashboard/?DashboardId=1005','Y','Life Analysis by Escalated Cost Dashboard','LifeAnalysisEsc');
    insert into PWRANLYT.PA_DASHBOARD values (1006,1006,'CPR Ledger Anomalies','Any assets with vintages in the future are anomalous records and should be adjusted.  Similarly, there are no conditions where quantity is negative and cost is positive.  While negative quantity and cost conditions can exist, as can positive quantities with negative costs, these conditions are atypical and should be investigated for accuracy.','The CPR Ledger Anomalies dashboard highlights unusual, irregular or error conditions in the CPR Ledger that necessitate investigation.  These issues include costs assigned to assets with zero quantities, quantities assigned to assets with zero costs, opposite signed cost and quantity records (cost is positive and quantity is negative, and vice versa) and assets with vintages in the future.','Dashboard/SummaryDashboard/?DashboardId=1006','Y','Ledger Anomalies','LedgerAnomalies');
    insert into PWRANLYT.PA_DASHBOARD values (1007,1007,'Retirement Patterns by Cost','Large swings in annual retirement costs should be investigated, particularly in vintage accounts.  Significantly greater retirements in a given vintage in any particular year can indicate a vintage distortion that should be remediated. Similarly, any retirement data that is net positive for a particular year/vintage is an anomalous condition that should be investigated (retirements costs are negative).  While unretirement activity occurs, significant unretired costs should be validated for accuracy.','The Retirement Patterns by Cost dashboard provides information about annual retirement costs.  The visuals in this dashboard provide a look at the pattern of retirement activity on a yearly basis and can be used to identify potential missed retirements within the system by comparing yearly trends in certain accounts.  The visuals illustrate annual retirement activity by cost, the distribution of retirement costs by vintage, as well as the gross $ amount of unretirement activity by vintage. ','Dashboard/SummaryDashboard/?DashboardId=1007','Y','Retirement Patterns by Cost Dashboard','RetirementPatternsCost');
    insert into PWRANLYT.PA_DASHBOARD values (1008,1008,'Retirement Patterns by Quantity','Large swings in annual retirement quantities should be investigated, particularly in vintage accounts.  Significantly greater retirements in a given vintage in any particular year can indicate a vintage distortion that should be remediated.  Similarly, any retirement data that is net positive for a particular year/vintage is an anomalous condition that should be investigated (retirements quantities are negative).  While unretirement activity occurs, significant unretired quantities should be validated for accuracy.','The Retirement Patterns by Quantity dashboard provides information about annual retirement quantity.  The visuals in this dashboard provide a look at the pattern of retirement activity on a yearly basis and can be used to identify potential missed retirements within the system by comparing yearly trends in certain accounts.  The visuals illustrate annual retirement activity by quantity, the distribution of retirement quantities by vintage, as well as the gross quantity amount of unretirement activity by vintage. ','Dashboard/SummaryDashboard/?DashboardId=1008','Y','Retirement Patterns by Quantity Dashboard','RetirementPatternsQty');
    insert into PWRANLYT.PA_DASHBOARD values (1009,1009,'Addition Patterns by Cost','Large swings in annual addition costs should be investigated, particularly in vintage accounts.  Significantly greater additions in a given vintage in any particular year can indicate a vintage distortion that should be remediated.  Any addition data that is net negative for a particular year/ vintage is an anomalous condition that should be investigated (addition costs are positive).','The Addition Patterns by Cost dashboard provides information about annual addition costs.  The visuals in this dashboard provide a look at the pattern of addition activity on a yearly basis and can be compared against retirement activity to identify trends.  The dashboard is used in conjunction with the Retirement Patterns dashboards to show time periods where you would expect to see increased retirement activity due to increased overall addition activity levels (and vice-versa).  The visuals illustrate annual addition activity by cost, the distribution of addition costs by vintage, as well additions with negative costs (anomalous conditions).  ','Dashboard/SummaryDashboard/?DashboardId=1009','Y','Addition Patterns by Cost Dashboard','AdditionPatternsCost');
    insert into PWRANLYT.PA_DASHBOARD values (1010,1010,'Addition Patterns by Quantity','Large swings in annual addition quantities should be investigated, particularly in vintage accounts.  Significantly greater additions in a given vintage in any particular year can indicate a vintage distortion that should be remediated.  Any addition data that is net negative for a particular year/ vintage is an anomalous condition that should be investigated (addition quantities are positive).','The Addition Patterns by Quantity dashboard provides information about annual addition quantities.  The visuals in this dashboard provide a look at the pattern of addition activity on a yearly basis and can be compared against retirement activity to identify trends.  The dashboard is used in conjunction with the Retirement Patterns dashboards to show time periods where you would expect to see increased retirement activity due to increased overall addition activity levels (and vice-versa).  The visuals illustrate annual addition activity by quantity, the distribution of addition quantities by vintage, as well additions with negative quantities (anomalous conditions). ','Dashboard/SummaryDashboard/?DashboardId=1010','Y','Addition Patterns by Quantity Dashboard','AdditionPatternsQty');
    insert into PWRANLYT.PA_DASHBOARD values (1011,1011,'Reserve Adequacy','If the actual reserve is small relative to the theoretical reserve, this indicates an under-reserved condition where sufficient funds are not being collected to replace the asset at the end of its expected life.  If the actual reserve is large relative to the theoretical reserve, this indicates an over-reserved condition where more depreciation has been collected on assets than previously estimated (indicating missed retirements/ghost assets).  When more depreciation has been collected on assets than previously estimated (actual reserves > theoretical reserves) it means that the average age of assets is increasing due to natural/technological factors or, more significantly, missed retirements.  Significant over-reserved conditions can lead to ratepayer refunds or credits and should be investigated thoroughly.  As a best practice, actual reserves should be trending toward their associated theoretical reserves.','The Reserve Adequacy dashboard highlights reserve imbalances between the Theoretical and Actual Life Reserves and is used to understand where you have under- or over-reserved situations that are significant enough to warrant additional scrutiny in a rate case (the Theoretical Life Reserve is calculated based on the remaining life calculations from the depreciation module, and is a useful tool for gauging retirement activity).  Reserve imbalances are highlighted by plotting the annual trend of the Life Reserve against the Theoretical Life Reserve.  Additionally, the magnitude of the difference between the Actual and Theoretical Life Reserves is shown as a percent value and the annual change in the Life Reserve is plotted as a line graph for trend analysis.  ','Dashboard/SummaryDashboard/?DashboardId=1011','Y','Reserve Adequacy Dashboard','ReserveAdequacy');
    insert into PWRANLYT.PA_DASHBOARD values (1012,1012,'Cost of Removal per Unit','Allocating the appropriate COR to retired assets has beneficial tax implications, and failing to do so can have negative regulatory implications.  Plotting COR $ relative to retirement and addition $ allows for a trend analysis of COR data, highlighting potentially misallocated costs in particular years and accounts.  No COR should ever be taken without an associated retirement.  While retirements can occur without COR, specific accounts should be investigated where COR rates are applied but no dollars are accumulated.','The Cost of Removal per Unit dashboard provides information about how cost of removal (COR) per unit is trending over time (utilize filters to analyze specific accounts).  This dashboard is used to determine areas where COR and/or retirements are not being processed.  The dashboard compares the yearly COR to retirement and addition $.  Additionally, bar charts display potentially anomalous conditions in which COR has been taken without an associated retirement, or a retirement has been recorded without any associated COR.   ','Dashboard/SummaryDashboard/?DashboardId=1012','Y','Cost of Removal Per Unit','CostOfRemovalPerUnit');
    insert into PWRANLYT.PA_DASHBOARD values (1013,1013,'Average Unit Costs','Average unit costs should have a positive correlation with the inflation rates, and thus should share a general slope.  If there is significant diversion, quantity and/or cost distortions may have occurred and appropriate accounts should be investigated.','The Average Unit Costs dashboard provides a comparison of the average asset cost per unit compared with the Handy Whitman inflation index (standardized rate of inflation).  This dashboard is used to highlight areas where unusual activity has created an average asset cost per unit that does not align with the general rate of inflation.  The dashboard trends average unit costs by GL posting year relative to the Handy Whitman rate.  Hover over the Handy Whitman rates (orange line) to see the rate for a given year (it does not share the same Y-axis scale as the unit costs, which are in dollars). ','Dashboard/SummaryDashboard/?DashboardId=1013','Y','Average Unit Costs','AverageUnitCosts');
    insert into PWRANLYT.PA_DASHBOARD values (1014,1014,'Depreciation Ledger Trends','Additions and retirements should generally trend together, particularly in mass accounts, unless significant infrastructure expansions have occurred â€“ significant divergences should be investigated for accuracy.  Validate addition and retirement costs against ending balances.  Plotting COR $ relative to retirement $ allows for a trend analysis of COR data, highlighting potentially misallocated costs in particular years and accounts.','The Depreciation Ledger Trends dashboard provides annual trend information for various components of the depreciation ledger:  yearly ending asset balance, additions, retirements and cost of removal (COR).  Additionally, COR $ are plotted against both addition and retirement $. ','Dashboard/SummaryDashboard/?DashboardId=1014','Y','Depr Ledger Trends','DeprLedgerTrends');
    insert into PWRANLYT.PA_DASHBOARD values (1015,1015,'COR Reserve Trends','Utilize the dashboard to investigate and validate fluctuations in the COR Reserve.  Once an issue has been identified, validate the fluctuations (or oppositely identify the problemâ€™s root cause) by investigating the annual COR expense added to the reserve and annual COR $ per Beginning COR Reserve.','The COR Reserve Trends dashboard highlights cost of removal (COR) trends from the depreciation ledger, particularly relating to the COR Reserve.  Annual COR expense added the reserve, the annual change in the reserve balance and the ratio of COR/Begin COR Reserve are trended over GL posting years. ','Dashboard/SummaryDashboard/?DashboardId=1015','Y','Depr Ledger COR Ratios','DeprLedgerCorRatios');
    insert into PWRANLYT.PA_DASHBOARD values (1016,1016,'Life Reserve Trends','Utilize the dashboard to investigate and validate fluctuations in the Life Reserve.  Once an issue has been identified, validate the fluctuations (or oppositely identify the problemâ€™s root cause) by investigating the annual depreciation added to the reserve and annual retirement $ per beginning Life Reserve.','The Life Reserve Trends dashboard highlights retirement trends from the depreciation ledger, particularly relating to the Life Reserve.  Annual asset depreciation added the reserve, the annual change in the reserve and the ratio of Retirements/Begin Life Reserve are trended over GL posting years. ','Dashboard/SummaryDashboard/?DashboardId=1016','Y','Depr Ledger Life Ratios','DeprLedgerLifeRatios');
    insert into PWRANLYT.PA_DASHBOARD values (1017,1017,'Average Age Trends','Original cost weighted age, escalated cost weighted age and quantity weighted age are all calculated to provide the fullest picture of asset age composition.  Plotting these average ages, and the associated retirement and addition data, allows for analysis of age fluctuations that can occur from year to year.  Large fluctuations in annual average ages can signify significant retirement and/or addition activity, which should be investigated for accuracy.  Small changes in annual or periodic ages represent a more static condition where additions and/or retirements offset the natural year-by-year increase in the age of assets. ','The Age Trends dashboard highlights how asset ages are changing over time.  Age trends are complemented with retirement and addition activity data to give insight into the reasons for the changing ages.   Accounts whose ages have changed by more than two (2) years (both positive and negative) in a given year are highlighted as large annual changes.  Accounts whose ages have changed by more than one (1) year over a span of multiple years are highlighted as large period changes. ','Dashboard/SummaryDashboard/?DashboardId=1017','Y','Average Age Trends','AverageAgeTrends');
    insert into PWRANLYT.PA_DASHBOARD values (1018,1018,'Retirement Activity Anomalies','Conditions where there are costs with zero associated quantity and quantities with zero associated cost are potential anomalies in the data and should be investigated for accuracy.  While circumstances can occur where quantity exists with zero associated cost, as well as negative quantity and cost conditions, these occurrences are atypical and should be validated for accuracy.','The Retirement Activity Anomalies dashboard highlights anomalies in the CPR Activity table revolving around retirements.  Conditions where there are non-zero quantities with zero costs and where there are non-zero costs with zero quantities are highlighted for investigation.  This dashboard is used to identify potential errors within the CPR Activity table.','Dashboard/SummaryDashboard/?DashboardId=1018','Y','Retirement Anomalies','RetirementAnomalies');
    

    delete from PWRANLYT.PA_DASHBOARD_CHARTS;
    

    -- Inserts for PWRANLYT.PA_DASHBOARD_CHARTS
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1,1,1000,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (2,1,1010,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (3,1,1036,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (4,1,1040,3);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (5,2,1006,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (6,2,1009,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (7,2,1018,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (8,2,1034,3);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (9,2,1039,4);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (10,3,1002,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (11,3,1024,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (12,4,1006,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (13,6,1000,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (14,5,1018,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (15,7,1034,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (16,8,1055,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (17,9,1061,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1000,1000,1000,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1001,1000,1001,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1002,1000,1039,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1003,1001,1003,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1004,1001,1004,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1005,1001,1067,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1006,1002,1006,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1007,1002,1007,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1008,1002,1008,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1009,1003,1009,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1010,1003,1010,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1011,1003,1011,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1012,1004,1012,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1013,1004,1013,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1014,1004,1014,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1015,1005,1015,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1016,1005,1016,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1017,1005,1017,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1018,1006,1018,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1019,1006,1019,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1020,1006,1020,4);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1021,1006,1021,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1022,1006,1022,3);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1023,1007,1023,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1024,1007,1063,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1025,1007,1025,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1026,1008,1023,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1027,1008,1063,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1028,1008,1027,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1029,1009,1028,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1030,1009,1062,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1031,1009,1030,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1032,1010,1028,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1033,1010,1062,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1034,1010,1032,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1035,1011,1034,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1036,1011,1068,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1037,1011,1036,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1038,1012,1037,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1039,1012,1038,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1041,1013,1033,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1044,1012,1070,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1045,1014,1043,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1046,1014,1045,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1047,1014,1044,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1048,1014,1046,3);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1049,1014,1047,4);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1050,1014,1048,5);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1051,1015,1049,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1052,1015,1050,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1053,1015,1051,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1054,1016,1052,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1055,1016,1053,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1056,1016,1054,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1057,1017,1055,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1058,1017,1056,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1059,1017,1057,2);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1060,1017,1058,3);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1061,1017,1059,4);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1062,1018,1060,0);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1063,1018,1061,1);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1064,1000,1064,3);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1065,1001,1066,3);
    insert into PWRANLYT.PA_DASHBOARD_CHARTS values (1066,1012,1069,3);
    

    delete from PWRANLYT.PA_COLUMNGROUP;
    

    -- Inserts for PA_COLUMNGROUP
    insert into PWRANLYT.PA_COLUMNGROUP values (1,'CPR_BALANCES','BUS_SEGMENT,FUNC_CLASS,FERC_PLT_ACCT,UTIL_ACCT,DEPR_GROUP,STATE,MAJOR_LOCATION','FUNC_CLASS');
    insert into PWRANLYT.PA_COLUMNGROUP values (2,'VINTAGE_SPREADS','BUS_SEGMENT,FUNC_CLASS,FERC_PLT_ACCT,UTIL_ACCT,DEPR_GROUP,STATE,MAJOR_LOCATION','FUNC_CLASS');
    

    delete from PWRANLYT.PA_DATAPOINT;
    

    -- Inserts for PWRANLYT.PA_DATAPOINT
    insert into PWRANLYT.PA_DATAPOINT values (1000,'ferc_plt_acct','delta','FF0000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1001,'ferc_plt_acct','delta','FFCC33',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1002,'HeatMapLabel','scaled_diff','FF0000','vintage_year','process_year',1,'Under','Over','FF0000');
    insert into PWRANLYT.PA_DATAPOINT values (1003,'VINTAGE_INTERVAL','UNITIZED_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1004,'VINTAGE_INTERVAL','UNITIZED_QTY','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1005,'VINTAGE_INTERVAL','TOTAL_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1006,'VINTAGE_INTERVAL','TOTAL_QTY','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1007,'VINTAGE_INTERVAL','COST_PER_UNIT','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1008,'AGE','BEL_Total_Cost','66CC66',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1009,'AGE','PEL_Total_Cost','FFCC33',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1010,'AGE','PML_Total_Cost','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1011,'YEARS_PAST_EXPECTED','TOTAL_COST','FFCC33',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1012,'YEARS_PAST_MAX','TOTAL_COST','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1013,'AGE','BEL_Total_Quantity','66CC66',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1014,'AGE','PEL_Total_Quantity','FFCC33',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1015,'AGE','PML_Total_Quantity','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1016,'YEARS_PAST_EXPECTED','TOTAL_QUANTITY','FFCC33',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1017,'YEARS_PAST_MAX','TOTAL_QUANTITY','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1018,'AGE','BEL_Total_Escalated_Cost','66CC66',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1019,'AGE','PEL_Total_Escalated_Cost','FFCC33',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1020,'AGE','PML_Total_Escalated_Cost','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1021,'YEARS_PAST_EXPECTED','TOTAL_ESCALATED_COST','FFCC33',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1022,'YEARS_PAST_MAX','TOTAL_ESCALATED_COST','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1023,'COMPANY','ZERO_QTY_PCT','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1024,'COMPANY','NONZERO_QTY_PCT','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1025,'COMPANY','RECORDCOUNT','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1026,'VINTAGE_YEAR','TOTAL_QUANTITY','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1027,'VINTAGE_YEAR','TOTAL_COST','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1028,'PROCESS_YEAR','RETIRE_COST','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1029,'PROCESS_YEAR','RETIRE_QTY','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1030,'HeatMapLabel','SCALEDVALUE','FF0000','VINTAGE_YEAR','PROCESS_YEAR',1,'Low','High','FF0000');
    insert into PWRANLYT.PA_DATAPOINT values (1031,'VINTAGE_INTERVAL','RETIRE_COST','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1032,'VINTAGE_INTERVAL','RETIRE_QTY','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1033,'PROCESS_YEAR','ADDITION_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1034,'PROCESS_YEAR','ADDITION_QTY','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1035,'PROCESS_YEAR','SCALEDVALUE','66CC66','VINTAGE_YEAR','PROCESS_YEAR',1,'Low','High','FFFFFF');
    insert into PWRANLYT.PA_DATAPOINT values (1036,'VINTAGE_INTERVAL','ADDITION_COST','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1037,'VINTAGE_INTERVAL','ADDITION_QTY','990000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1038,'VINTAGE_INTERVAL','AVG_SERVICE_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1039,'VINTAGE_INTERVAL','AVG_HW_RATE','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1040,'GL_POSTING_YEAR','END_LIFE_RESERVE','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1041,'GL_POSTING_YEAR','END_THEO_RESERVE','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1042,'HeatMapLabel','END_RATIO','FF342D','GL_POSTING_YEAR','COMPANY',1,'Over Reserved','Under Reserved','FF342D');
    insert into PWRANLYT.PA_DATAPOINT values (1043,'GL_POSTING_YEAR','ANNUAL_CHANGE','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1044,'PROCESS_YEAR','COR','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1045,'PROCESS_YEAR','RETIRE_QTY','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1046,'PROCESS_YEAR','ADDITION_QTY','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1047,'ferc_plt_acct','qty_delta','FF0000',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1048,'ferc_plt_acct','qty_delta','FFCC33',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1049,'HeatMapLabel','qty_scaled_diff','FF0000','vintage_year','process_year',1,'Under','Over','FF0000');
    insert into PWRANLYT.PA_DATAPOINT values (1050,'PROCESS_YEAR','RETIRE_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1051,'PROCESS_YEAR','PROJ_RETIRE_COST','FFCC33',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1052,'LIFE_BAND','CURR_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1053,'LIFE_BAND','CURR_QTY','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1054,'HeatMapLabel','COR_FLAG','FF0000','VINTAGE_YEAR','PROCESS_YEAR',1,'No COR','No Retirement','FF0000');
    insert into PWRANLYT.PA_DATAPOINT values (1055,'GL_POSTING_YEAR','END_BALANCE','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1056,'GL_POSTING_YEAR','RETIREMENTS','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1057,'GL_POSTING_YEAR','ADDITIONS','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1058,'GL_POSTING_YEAR','COST_OF_REMOVAL','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1059,'GL_POSTING_YEAR','PCT_INCREASE_COR_RESERVE','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1060,'GL_POSTING_YEAR','ANNUAL_CHANGE_COR_RESERVE','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1061,'GL_POSTING_YEAR','PCT_BEGIN_RESERVE','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1062,'GL_POSTING_YEAR','ANNUAL_CHANGE_LIFE_RESERVE','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1063,'PROCESS_YEAR','COST_WEIGHTED_AGE','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1064,'PROCESS_YEAR','QUANTITY_WEIGHTED_AGE','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1065,'PROCESS_YEAR','ESCALATED_COST_WEIGHTED_AGE','66CC66',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1066,'PROCESS_YEAR','ADDITION_QTY','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1067,'PROCESS_YEAR','ADDITION_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1068,'PROCESS_YEAR','RETIRE_QTY','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1069,'PROCESS_YEAR','RETIRE_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1070,'GL_POSTING_YEAR','NET_RETIREMENT_QTY','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1071,'GL_POSTING_YEAR','NET_RETIREMENT_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1072,'VINTAGE_INTERVAL','ADDITION_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1073,'VINTAGE_INTERVAL','ADDITION_QTY','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1074,'VINTAGE_INTERVAL','RETIRE_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1075,'VINTAGE_INTERVAL','RETIRE_QTY','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1076,'VINTAGE_INTERVAL','RETIRE_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1077,'VINTAGE_INTERVAL','RETIRE_QTY','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1078,'PROCESS_YEAR','RETIRE_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1079,'PROCESS_YEAR','RETIRE_QTY','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1080,'VINTAGE_INTERVAL','PROJ_RETIRE_COST','FF9933',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1081,'VINTAGE_INTERVAL','PROJ_RETIRE_QTY','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1082,'PROCESS_YEAR','PROJ_RETIRE_COST','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1083,'PROCESS_YEAR','PROJ_RETIRE_QTY','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1084,'GL_POSTING_YEAR','PCT_RESERVED','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1085,'VINTAGE_INTERVAL','COR','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1086,'PROCESS_YEAR','COR_PER_UNIT','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1087,'GL_POSTING_YEAR','COR_EXPENSE','006699',null,null,null,null,null,null);
    insert into PWRANLYT.PA_DATAPOINT values (1088,'GL_POSTING_YEAR','DEPR_EXPENSE','006699',null,null,null,null,null,null);
    

    delete from PWRANLYT.PA_DATASOURCE;
    

    -- Inserts for PWRANLYT.PA_DATASOURCE
    insert into PWRANLYT.PA_DATASOURCE values (1000,'PR_UnderCost_Chart','ferc_plt_acct, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES','sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)) < 0','ferc_plt_acct','delta',null,null,10,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1001,'PR_OverCost_Chart','ferc_plt_acct, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES','sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)) > 0','ferc_plt_acct','delta desc',null,null,10,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1002,'PR_CostHeatMap_Chart','process_year, vintage_year, round(sum(nvl(retire_cost,0)),2) retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta, case when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) = 0 then 0 when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) < 0 then 100 when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) > 0 then -100 when (sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1) > 1 then 100 when (sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1) < -1 then -100 else round(((sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1))*100,0) end scaled_diff','PWRANLYT.V_PA_CPR_BALANCES',null,'process_year, vintage_year','1, 2','proj_retire_cost is not null 
    ',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1003,'PR_UnderQty_Chart','ferc_plt_acct, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRANLYT.V_PA_CPR_BALANCES','sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)) < 0','ferc_plt_acct','qty_delta',null,null,10,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1004,'PR_OverQty_Chart','ferc_plt_acct, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRANLYT.V_PA_CPR_BALANCES','sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)) > 0','ferc_plt_acct','qty_delta desc',null,null,10,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1005,'PR_QtyHeatMap_Chart','process_year, vintage_year, round(sum(nvl(retire_qty,0)),2) retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta, case when sum(proj_retire_qty*-1) = 0 and sum(nvl(retire_qty,0)) = 0 then 0 when sum(proj_retire_qty*-1) = 0 and sum(nvl(retire_qty,0)) < 0 then 100 when sum(proj_retire_qty*-1) = 0 and sum(nvl(retire_qty,0)) > 0 then -100 when (sum(nvl(retire_qty,0)) - sum(proj_retire_qty*-1))/sum(proj_retire_qty*-1) > 1 then 100 when (sum(nvl(retire_qty,0)) - sum(proj_retire_qty*-1))/sum(proj_retire_qty*-1) < -1 then -100 else round(((sum(nvl(retire_qty,0)) - sum(proj_retire_qty*-1))/sum(proj_retire_qty*-1))*100,0) end qty_scaled_diff','PWRANLYT.V_PA_CPR_BALANCES',null,'process_year, vintage_year','1, 2','proj_retire_qty is not null 
    ',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1006,'PR_UnderCost_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1007,'PR_UnderCost_Locn','state, major_location, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'state, major_location','state, major_location',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1008,'PR_UnderCost_Comp','company, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'company','company',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1009,'PR_OverCost_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1010,'PR_OverCost_Locn','state, major_location, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'state, major_location','state, major_location',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1011,'PR_OverCost_Comp','company, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'company','company',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1012,'PR_CostHeatMap_Acct_NOTUSED','bus_segment, func_class, ferc_plt_acct, util_acct, round(sum(nvl(retire_cost,0)),2) retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta, case when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) = 0 then 0 when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) < 0 then 100 when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) > 0 then -100 when (sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1) > 1 then 100 when (sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1) < -1 then -100 else round(((sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1))*100,0) end scaled_diff','PWRANLYT.V_PA_CPR_BALANCES',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','proj_retire_cost is not null 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1013,'PR_CostHeatMap_Locn_NOTUSED','state, major_location, round(sum(nvl(retire_cost,0)),2) retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta, case when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) = 0 then 0 when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) < 0 then 100 when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) > 0 then -100 when (sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1) > 1 then 100 when (sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1) < -1 then -100 else round(((sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1))*100,0) end scaled_diff','PWRANLYT.V_PA_CPR_BALANCES',null,'state, major_location','state, major_location','proj_retire_cost is not null 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1014,'PR_CostHeatMap_Comp_NOTUSED','company, round(sum(nvl(retire_cost,0)),2) retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta, case when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) = 0 then 0 when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) < 0 then 100 when sum(proj_retire_cost*-1) = 0 and sum(nvl(retire_cost,0)) > 0 then -100 when (sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1) > 1 then 100 when (sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1) < -1 then -100 else round(((sum(nvl(retire_cost,0)) - sum(proj_retire_cost*-1))/sum(proj_retire_cost*-1))*100,0) end scaled_diff','PWRANLYT.V_PA_CPR_BALANCES',null,'company','company','proj_retire_cost is not null 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1015,'PR_UnderQty_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1016,'PR_UnderQty_Locn','state, major_location, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'state, major_location','state, major_location',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1017,'PR_UnderQty_Comp','company, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'company','company',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1018,'PR_OverQty_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1019,'PR_OverQty_Locn','state, major_location, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'state, major_location','state, major_location',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1020,'PR_OverQty_Comp','company, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'company','company',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1021,'PR_QtyHeatMap_Acct_NOTUSED','bus_segment, func_class, ferc_plt_acct, util_acct, round(sum(nvl(retire_qty,0)),2) retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta, case when sum(proj_retire_qty*-1) = 0 and sum(nvl(retire_qty,0)) = 0 then 0 when sum(proj_retire_qty*-1) = 0 and sum(nvl(retire_qty,0)) < 0 then 100 when sum(proj_retire_qty*-1) = 0 and sum(nvl(retire_qty,0)) > 0 then -100 when (sum(nvl(retire_qty,0)) - sum(proj_retire_qty*-1))/sum(proj_retire_qty*-1) > 1 then 100 when (sum(nvl(retire_qty,0)) - sum(proj_retire_qty*-1))/sum(proj_retire_qty*-1) < -1 then -100 else round(((sum(nvl(retire_qty,0)) - sum(proj_retire_qty*-1))/sum(proj_retire_qty*-1))*100,0) end scaled_diff','PWRANLYT.V_PA_CPR_BALANCES',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','proj_retire_qty is not null 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1022,'PR_QtyHeatMap_Locn_NOTUSED','state, major_location, round(sum(nvl(retire_qty,0)),2) retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta, case when sum(proj_retire_qty*-1) = 0 and sum(nvl(retire_qty,0)) = 0 then 0 when sum(proj_retire_qty*-1) = 0 and sum(nvl(retire_qty,0)) < 0 then 100 when sum(proj_retire_qty*-1) = 0 and sum(nvl(retire_qty,0)) > 0 then -100 when (sum(nvl(retire_qty,0)) - sum(proj_retire_qty*-1))/sum(proj_retire_qty*-1) > 1 then 100 when (sum(nvl(retire_qty,0)) - sum(proj_retire_qty*-1))/sum(proj_retire_qty*-1) < -1 then -100 else round(((sum(nvl(retire_qty,0)) - sum(proj_retire_qty*-1))/sum(proj_retire_qty*-1))*100,0) end scaled_diff','PWRANLYT.V_PA_CPR_BALANCES',null,'state, major_location','state, major_location','proj_retire_qty is not null',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1023,'PR_QtyHeatMap_Comp_NOTUSED','company, round(sum(nvl(retire_qty,0)),2) retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta, case when sum(proj_retire_qty*-1) = 0 and sum(nvl(retire_qty,0)) = 0 then 0 when sum(proj_retire_qty*-1) = 0 and sum(nvl(retire_qty,0)) < 0 then 100 when sum(proj_retire_qty*-1) = 0 and sum(nvl(retire_qty,0)) > 0 then -100 when (sum(nvl(retire_qty,0)) - sum(proj_retire_qty*-1))/sum(proj_retire_qty*-1) > 1 then 100 when (sum(nvl(retire_qty,0)) - sum(proj_retire_qty*-1))/sum(proj_retire_qty*-1) < -1 then -100 else round(((sum(nvl(retire_qty,0)) - sum(proj_retire_qty*-1))/sum(proj_retire_qty*-1))*100,0) end scaled_diff','PWRANLYT.V_PA_CPR_BALANCES',null,'company','company','proj_retire_qty is not null 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1024,'PR_RetireActivity_Detail','company, state, major_location, bus_segment, func_class, ferc_plt_acct, util_acct, gl_posting_date, sum(activity_cost) retire_cost, sum(activity_qty) retire_qty','PWRANLYT.V_PA_CPR_ACTIVITY',null,'company, state, major_location, bus_segment, func_class, ferc_plt_acct, util_acct, gl_posting_date','company, state, major_location, bus_segment, func_class, ferc_plt_acct, util_acct, gl_posting_date','ferc_activity = ''Retirement'' AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1025,'SD_UnitzCombo_Chart','VINTAGE_INTERVAL, ROUND(SUM(UNTZ_COST),2) UNITIZED_COST, ROUND(SUM(UNTZ_QTY),2) UNITIZED_QTY, CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END COST_PER_UNIT','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'VINTAGE_INTERVAL','1',null,null,null,null,2);
    insert into PWRANLYT.PA_DATASOURCE values (1026,'SD_TotalCombo_Chart','VINTAGE_INTERVAL, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END COST_PER_UNIT','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'VINTAGE_INTERVAL','1',null,null,null,null,2);
    insert into PWRANLYT.PA_DATASOURCE values (1027,'SD_UnitzCombo_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, ROUND(SUM(UNTZ_COST),2) UNITIZED_COST, ROUND(SUM(UNTZ_QTY),2) UNITIZED_QTY, CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END COST_PER_UNIT','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1028,'SD_UnitzCombo_Locn','state, major_location, ROUND(SUM(UNTZ_COST),2) UNITIZED_COST, ROUND(SUM(UNTZ_QTY),2) UNITIZED_QTY, CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END COST_PER_UNIT','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'state, major_location','state, major_location',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1029,'SD_UnitzCombo_Comp','company, ROUND(SUM(UNTZ_COST),2) UNITIZED_COST, ROUND(SUM(UNTZ_QTY),2) UNITIZED_QTY, CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END COST_PER_UNIT','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'company','company',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1030,'SD_TotalCombo_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END COST_PER_UNIT','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1031,'SD_TotalCombo_Locn','state, major_location, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END COST_PER_UNIT','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'state, major_location','state, major_location',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1032,'SD_TotalCombo_Comp','company, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END COST_PER_UNIT','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'company','company',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1033,'SD_Unitz_Detail','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, ROUND(SUM(UNTZ_COST),2) UNITIZED_COST, ROUND(SUM(UNTZ_QTY),2) UNITIZED_QTY, CASE WHEN SUM(UNTZ_QTY) = 0 THEN 0 ELSE ROUND(SUM(UNTZ_COST)/SUM(UNTZ_QTY),2) END COST_PER_UNIT, age, age_group, expected_life, max_life','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, age, age_group, expected_life, max_life','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, age, age_group, expected_life, max_life',null,null,null,null,2);
    insert into PWRANLYT.PA_DATASOURCE values (1034,'SD_Total_Detail','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_QTY),2) TOTAL_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END COST_PER_UNIT, age, age_group, expected_life, max_life','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, age, age_group, expected_life, max_life','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, age, age_group, expected_life, max_life',null,null,null,null,2);
    insert into PWRANLYT.PA_DATASOURCE values (1035,'LA_AssetAgeDist_Chart','AGE, 
    AGE_GROUP,
    ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'AGE, AGE_GROUP','1',null,null,null,'AgeGroupDataTableLoader',2);
    insert into PWRANLYT.PA_DATASOURCE values (1036,'LA_YearsPastExpected_Chart','YEARS_PAST_EXPECTED, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'YEARS_PAST_EXPECTED','1','YEARS_PAST_EXPECTED > 0 AND (YEARS_PAST_MAX <=0 OR YEARS_PAST_MAX IS NULL) 
    ',null,null,null,2);
    insert into PWRANLYT.PA_DATASOURCE values (1037,'LA_YearsPastMax_Chart','YEARS_PAST_MAX, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'YEARS_PAST_MAX','1','YEARS_PAST_MAX > 0 
    ',null,null,null,2);
    insert into PWRANLYT.PA_DATASOURCE values (1038,'LA_AssetAgeDist_Acct','bus_segment, func_class, ferc_plt_acct, util_acct,

    ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1039,'LA_AssetAgeDist_Locn','state, major_location,

    ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'state, major_location','state, major_location',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1040,'LA_AssetAgeDist_Comp','company,

    ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'company','company',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1041,'LA_YearsPastExp_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','YEARS_PAST_EXPECTED > 0 AND (YEARS_PAST_MAX <=0 OR YEARS_PAST_MAX IS NULL) 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1042,'LA_YearsPastExp_Locn','state, major_location, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'state, major_location','state, major_location','YEARS_PAST_EXPECTED > 0 AND (YEARS_PAST_MAX <=0 OR YEARS_PAST_MAX IS NULL) 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1043,'LA_YearsPastExp_Comp','company, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'company','company','YEARS_PAST_EXPECTED > 0 AND (YEARS_PAST_MAX <=0 OR YEARS_PAST_MAX IS NULL) 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1044,'LA_YearsPastMax_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','YEARS_PAST_MAX > 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1045,'LA_YearsPastMax_Locn','state, major_location, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS 
    ',null,'state, major_location','state, major_location','YEARS_PAST_MAX > 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1046,'LA_YearsPastMax_Comp','company, ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'company','company','YEARS_PAST_MAX > 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1047,'LA_Age_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
    AGE_GROUP,  expected_life, max_life, mort_curve_desc, 
    ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
    AGE_GROUP,  expected_life, max_life, mort_curve_desc','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
    AGE_GROUP,  expected_life, max_life, mort_curve_desc',null,null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1048,'LE_ZeroQtyCosts_Chart','COMPANY, sum(curr_cost) TOTAL_COSTS, nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end),0) ZERO_QTY_COSTS, round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),2)*100 ZERO_QTY_PCT, (1 - round(nvl(sum(case when curr_qty = 0 and curr_cost <> 0 then curr_cost end) / sum (curr_cost),0),2))*100 NONZERO_QTY_PCT','PWRANLYT.V_PA_CPR_LEDGER','sum(curr_cost) <> 0','COMPANY','1',null,null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1049,'LE_NegQtyPosCost_Chart','COMPANY, SUM(case when curr_qty < 0 AND curr_cost > 0 then 1 else 0 end) as RECORDCOUNT, COUNT(*) AS TOTALRECORDS','PWRANLYT.V_PA_CPR_LEDGER',null,'COMPANY','1',null,null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1050,'LE_FutureAssets_Chart','VINTAGE_YEAR, COUNT(*) RECORD_COUNT, SUM(CURR_QTY) TOTAL_QUANTITY, SUM(CURR_COST) TOTAL_COST','PWRANLYT.V_PA_CPR_LEDGER',null,'VINTAGE_YEAR','1','VINTAGE_YEAR > EXTRACT(YEAR FROM SYSDATE) 
    ',null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1051,'LE_PosQtyNegCost_Chart','COMPANY, SUM(case when curr_qty > 0 AND curr_cost < 0 then 1 else 0 end) as RECORDCOUNT, COUNT(*) AS TOTALRECORDS','PWRANLYT.V_PA_CPR_LEDGER',null,'COMPANY','1',null,null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1052,'LE_ZeroCostQty_Chart','COMPANY, SUM(case when curr_qty <> 0 AND curr_cost = 0 then 1 else 0 end) as RECORDCOUNT, COUNT(*) AS TOTALRECORDS','PWRANLYT.V_PA_CPR_LEDGER',null,'COMPANY','1',null,null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1053,'LE_ZeroQtyCosts_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, sum(curr_cost) TOTAL_COSTS, sum(curr_qty) TOTAL_QUANTITY','PWRANLYT.V_PA_CPR_LEDGER',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','CURR_QTY = 0 AND CURR_COST <> 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1054,'LE_ZeroQtyCosts_Locn','state, major_location, sum(curr_cost) TOTAL_COSTS, sum(curr_qty) TOTAL_QUANTITY','PWRANLYT.V_PA_CPR_LEDGER',null,'state, major_location','state, major_location','CURR_QTY = 0 AND CURR_COST <> 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1055,'LE_ZeroQtyCosts_Comp','company, sum(curr_cost) TOTAL_COSTS, sum(curr_qty) TOTAL_QUANTITY','PWRANLYT.V_PA_CPR_LEDGER',null,'company','company','CURR_QTY = 0 AND CURR_COST <> 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1056,'LE_ZeroQtyCosts_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, sum(curr_qty) total_qty, sum(curr_cost) total_cost, sum(curr_adj) total_escalated_cost','PWRANLYT.V_PA_CPR_LEDGER',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','CURR_QTY = 0 AND CURR_COST <> 0 
    ',null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1057,'LE_NegQtyPosCost_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, sum(curr_cost) TOTAL_COSTS, sum(curr_qty) TOTAL_QUANTITY','PWRANLYT.V_PA_CPR_LEDGER',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','CURR_QTY < 0 AND CURR_COST > 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1058,'LE_NegQtyPosCost_Locn','state, major_location, sum(curr_cost) TOTAL_COSTS, sum(curr_qty) TOTAL_QUANTITY','PWRANLYT.V_PA_CPR_LEDGER',null,'state, major_location','state, major_location','CURR_QTY < 0 AND CURR_COST > 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1059,'LE_NegQtyPosCost_Comp','company, sum(curr_cost) TOTAL_COSTS, sum(curr_qty) TOTAL_QUANTITY','PWRANLYT.V_PA_CPR_LEDGER 
    ',null,'company','company','CURR_QTY < 0 AND CURR_COST > 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1060,'LE_NegQtyPosCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, sum(curr_qty) total_qty, sum(curr_cost) total_cost, sum(curr_adj) total_escalated_cost','PWRANLYT.V_PA_CPR_LEDGER',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','CURR_QTY < 0 AND CURR_COST > 0
    ',null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1061,'LE_PosQtyNegCost_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, sum(curr_cost) TOTAL_COSTS, sum(curr_qty) TOTAL_QUANTITY','PWRANLYT.V_PA_CPR_LEDGER',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','CURR_QTY > 0 AND CURR_COST < 0
     ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1062,'LE_PosQtyNegCost_Locn','state, major_location, sum(curr_cost) TOTAL_COSTS, sum(curr_qty) TOTAL_QUANTITY','PWRANLYT.V_PA_CPR_LEDGER',null,'state, major_location','state, major_location','CURR_QTY > 0 AND CURR_COST < 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1063,'LE_PosQtyNegCost_Comp','company, sum(curr_cost) TOTAL_COSTS, sum(curr_qty) TOTAL_QUANTITY','PWRANLYT.V_PA_CPR_LEDGER',null,'company','company','CURR_QTY > 0 AND CURR_COST < 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1064,'LE_PosQtyNegCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, sum(curr_qty) total_qty, sum(curr_cost) total_cost, sum(curr_adj) total_escalated_cost','PWRANLYT.V_PA_CPR_LEDGER',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','CURR_QTY > 0 AND CURR_COST < 0 
    ',null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1065,'LE_FutureAssets_Comp','COMPANY, COUNT(*) RECORD_COUNT, SUM(CURR_QTY) TOTAL_QUANTITY, SUM(CURR_COST) TOTAL_COST','PWRANLYT.V_PA_CPR_LEDGER',null,'COMPANY','COMPANY','VINTAGE_YEAR > EXTRACT(YEAR FROM SYSDATE) 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1066,'LE_FutureAssets_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, CURR_AGE, SUM(CURR_QTY) TOTAL_QUANTITY, SUM(CURR_COST) TOTAL_COST, SUM(CURR_ADJ) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_CPR_LEDGER',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, CURR_AGE','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, CURR_AGE','VINTAGE_YEAR > EXTRACT(YEAR FROM SYSDATE) 
    ',null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1067,'LE_ZeroCostQty_Acct','bus_segment, func_class, ferc_plt_acct, util_acct, sum(curr_cost) TOTAL_COSTS, sum(curr_qty) TOTAL_QUANTITY','PWRANLYT.V_PA_CPR_LEDGER',null,'bus_segment, func_class, ferc_plt_acct, util_acct','bus_segment, func_class, ferc_plt_acct, util_acct','CURR_QTY <> 0 AND CURR_COST = 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1068,'LE_ZeroCostQty_Locn','state, major_location, sum(curr_cost) TOTAL_COSTS, sum(curr_qty) TOTAL_QUANTITY','PWRANLYT.V_PA_CPR_LEDGER',null,'state, major_location','state, major_location','CURR_QTY <> 0 AND CURR_COST = 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1069,'LE_ZeroCostQty_Comp','company, sum(curr_cost) TOTAL_COSTS, sum(curr_qty) TOTAL_QUANTITY','PWRANLYT.V_PA_CPR_LEDGER',null,'company','company','CURR_QTY <> 0 AND CURR_COST = 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1070,'LE_ZeroCostQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, sum(curr_qty) total_qty, sum(curr_cost) total_cost, sum(curr_adj) total_escalated_cost','PWRANLYT.V_PA_CPR_LEDGER',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','CURR_QTY <> 0 AND CURR_COST = 0',null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1071,'RP_AnnualRetirements_Chart','PROCESS_YEAR, (SUM(RETIRE_COST)*-1) RETIRE_COST, (SUM(RETIRE_QTY)*-1) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'PROCESS_YEAR','1',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1072,'RP_AnnualRetire_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1073,'RP_AnnualRetire_Locn','STATE, MAJOR_LOCATION, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1074,'RP_AnnualRetire_Comp','COMPANY, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1075,'RP_AnnualRetire_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, (SUM(RETIRE_COST)*-1) RETIRE_COST, (SUM(RETIRE_QTY)*-1) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1076,'RP_HeatMap_Chart_NOTUSED','PROCESS_YEAR, VINTAGE_YEAR, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY, ROUND((((SUM(RETIRE_COST)-MIN(SUM(RETIRE_COST)) OVER()) * (0 + 20)) / (MAX(SUM(RETIRE_COST)) OVER() - MIN(SUM(RETIRE_COST)) OVER())) - 20,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(RETIRE_COST) < 0','PROCESS_YEAR, VINTAGE_YEAR','1,2','RETIRE_UNIT_ID > 5',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1077,'RP_HeatMap_Acct_NOTUSED','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY, ROUND((((SUM(RETIRE_COST)-MIN(SUM(RETIRE_COST)) OVER()) * (0 + 20)) / (MAX(SUM(RETIRE_COST)) OVER() - MIN(SUM(RETIRE_COST)) OVER())) - 20,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(RETIRE_COST) < 0','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1078,'RP_HeatMap_Locn_NOTUSED','STATE, MAJOR_LOCATION, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY, ROUND((((SUM(RETIRE_COST)-MIN(SUM(RETIRE_COST)) OVER()) * (0 + 20)) / (MAX(SUM(RETIRE_COST)) OVER() - MIN(SUM(RETIRE_COST)) OVER())) - 20,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(RETIRE_COST) < 0','STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1079,'RP_HeatMap_Comp_NOTUSED','COMPANY, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY, ROUND((((SUM(RETIRE_COST)-MIN(SUM(RETIRE_COST)) OVER()) * (0 + 20)) / (MAX(SUM(RETIRE_COST)) OVER() - MIN(SUM(RETIRE_COST)) OVER())) - 20,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(RETIRE_COST) < 0','COMPANY','COMPANY','RETIRE_UNIT_ID > 5',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1080,'RP_HeatMap_Detail_NOTUSED','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','UPPER(FERC_ACTIVITY) = ''RETIREMENT'' AND ACTIVITY_COST < 0 AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,500,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1081,'RP_UnRetireActivity_Chart','VINTAGE_INTERVAL, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1','RETIRE_COST > 0 
    ',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1082,'RP_UnRetireActivity_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_COST > 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1083,'RP_UnRetireActivity_Locn','STATE, MAJOR_LOCATION, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_COST > 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1084,'RP_UnRetireActivity_Comp','COMPANY, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_COST > 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1085,'RP_UnRetireActivity_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_COST > 0 ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1086,'RP_HeatMapQty_Chart_NOTUSED','PROCESS_YEAR, VINTAGE_YEAR, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY, ROUND((((SUM(RETIRE_QTY)-MIN(SUM(RETIRE_QTY)) OVER()) * (0 + 20)) / (MAX(SUM(RETIRE_QTY)) OVER() - MIN(SUM(RETIRE_QTY)) OVER())) - 20,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(RETIRE_QTY) < 0','PROCESS_YEAR, VINTAGE_YEAR','1,2','RETIRE_UNIT_ID > 5',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1087,'RP_HeatMapQty_Acct_NOTUSED','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY, ROUND((((SUM(RETIRE_QTY)-MIN(SUM(RETIRE_QTY)) OVER()) * (0 + 20)) / (MAX(SUM(RETIRE_QTY)) OVER() - MIN(SUM(RETIRE_QTY)) OVER())) - 20,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(RETIRE_QTY) < 0','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_UNIT_ID > 5',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1088,'RP_HeatMapQty_Locn_NOTUSED','STATE, MAJOR_LOCATION, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY, ROUND((((SUM(RETIRE_QTY)-MIN(SUM(RETIRE_QTY)) OVER()) * (0 + 20)) / (MAX(SUM(RETIRE_QTY)) OVER() - MIN(SUM(RETIRE_QTY)) OVER())) - 20,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(RETIRE_QTY) < 0','STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_UNIT_ID > 5',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1089,'RP_HeatMapQty_Comp_NOTUSED','COMPANY, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY, ROUND((((SUM(RETIRE_QTY)-MIN(SUM(RETIRE_QTY)) OVER()) * (0 + 20)) / (MAX(SUM(RETIRE_QTY)) OVER() - MIN(SUM(RETIRE_QTY)) OVER())) - 20,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(RETIRE_QTY) < 0','COMPANY','COMPANY','RETIRE_UNIT_ID > 5',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1090,'RP_HeatMapQty_Detail_NOTUSED','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','UPPER(FERC_ACTIVITY) = ''RETIREMENT'' AND ACTIVITY_COST < 0 AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,500,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1091,'RP_UnRetireActivityQty_Chart','VINTAGE_INTERVAL, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1','RETIRE_QTY > 0 
    ',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1092,'RP_UnRetireActivityQty_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','RETIRE_QTY > 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1093,'RP_UnRetireActivityQty_Locn','STATE, MAJOR_LOCATION, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','RETIRE_QTY > 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1094,'RP_UnRetireActivityQty_Comp','COMPANY, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY','RETIRE_QTY > 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1095,'RP_UnRetireActivityQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, SUM(RETIRE_COST) RETIRE_COST, SUM(RETIRE_QTY) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','RETIRE_QTY > 0 ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1096,'AP_AnnualAdds_Chart','PROCESS_YEAR, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'PROCESS_YEAR','1',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1097,'AP_AnnualAdds_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1098,'AP_AnnualAdds_Locn','STATE, MAJOR_LOCATION, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1099,'AP_AnnualAdds_Comp','COMPANY, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1100,'AP_AnnualAdds_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1101,'AP_HeatMapCost_Chart_NOTUSED','PROCESS_YEAR, VINTAGE_YEAR, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, ROUND((((SUM(ADDITION_COST)-MIN(SUM(ADDITION_COST)) OVER()) * (20 - 0)) / (MAX(SUM(ADDITION_COST)) OVER() - MIN(SUM(ADDITION_COST)) OVER())) + 0,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(ADDITION_COST) > 0','PROCESS_YEAR, VINTAGE_YEAR','1,2',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1102,'AP_HeatMapCost_Acct_NOTUSED','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, ROUND((((SUM(ADDITION_COST)-MIN(SUM(ADDITION_COST)) OVER()) * (20 - 0)) / (MAX(SUM(ADDITION_COST)) OVER() - MIN(SUM(ADDITION_COST)) OVER())) + 0,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(ADDITION_COST) > 0','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1103,'AP_HeatMapCost_Locn_NOTUSED','STATE, MAJOR_LOCATION, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, ROUND((((SUM(ADDITION_COST)-MIN(SUM(ADDITION_COST)) OVER()) * (20 - 0)) / (MAX(SUM(ADDITION_COST)) OVER() - MIN(SUM(ADDITION_COST)) OVER())) + 0,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(ADDITION_COST) > 0','STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1104,'AP_HeatMapCost_Comp_NOTUSED','COMPANY, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, ROUND((((SUM(ADDITION_COST)-MIN(SUM(ADDITION_COST)) OVER()) * (20 - 0)) / (MAX(SUM(ADDITION_COST)) OVER() - MIN(SUM(ADDITION_COST)) OVER())) + 0,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(ADDITION_COST) > 0','COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1105,'AP_HeatMapCost_Detail_NOTUSED','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','UPPER(FERC_ACTIVITY) = ''ADDITION'' AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,500,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1106,'AP_NegativeAddsCost_Chart','VINTAGE_INTERVAL, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1','ADDITION_COST < 0 
    ',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1107,'AP_NegativeAddsCost_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','ADDITION_COST < 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1108,'AP_NegativeAddsCost_Locn','STATE, MAJOR_LOCATION, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','ADDITION_COST < 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1109,'AP_NegativeAddsCost_Comp','COMPANY, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY','ADDITION_COST < 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1110,'AP_NegativeAddsCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','ADDITION_COST < 0 ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1111,'AP_HeatMapQty_Chart_NOTUSED','PROCESS_YEAR, VINTAGE_YEAR, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, ROUND((((SUM(ADDITION_QTY)-MIN(SUM(ADDITION_QTY)) OVER()) * (20 - 0)) / (MAX(SUM(ADDITION_QTY)) OVER() - MIN(SUM(ADDITION_QTY)) OVER())) + 0,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(ADDITION_QTY) > 0','PROCESS_YEAR, VINTAGE_YEAR','1,2',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1112,'AP_HeatMapQty_Acct_NOTUSED','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, ROUND((((SUM(ADDITION_QTY)-MIN(SUM(ADDITION_QTY)) OVER()) * (20 - 0)) / (MAX(SUM(ADDITION_QTY)) OVER() - MIN(SUM(ADDITION_QTY)) OVER())) + 0,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(ADDITION_QTY) > 0','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1113,'AP_HeatMapQty_Locn_NOTUSED','STATE, MAJOR_LOCATION, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, ROUND((((SUM(ADDITION_QTY)-MIN(SUM(ADDITION_QTY)) OVER()) * (20 - 0)) / (MAX(SUM(ADDITION_QTY)) OVER() - MIN(SUM(ADDITION_QTY)) OVER())) + 0,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(ADDITION_QTY) > 0','STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1114,'AP_HeatMapQty_Comp_NOTUSED','COMPANY, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY, ROUND((((SUM(ADDITION_QTY)-MIN(SUM(ADDITION_QTY)) OVER()) * (20 - 0)) / (MAX(SUM(ADDITION_QTY)) OVER() - MIN(SUM(ADDITION_QTY)) OVER())) + 0,0) SCALEDVALUE','PWRANLYT.V_PA_CPR_BALANCES','SUM(ADDITION_QTY) > 0','COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1115,'AP_HeatMapQty_Detail_NOTUSED','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','UPPER(FERC_ACTIVITY) = ''ADDITION''',null,500,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1116,'AP_NegativeAddsQty_Chart','VINTAGE_INTERVAL, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1','ADDITION_QTY < 0 
    ',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1117,'AP_NegativeAddsQty_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','ADDITION_QTY < 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1118,'AP_NegativeAddsQty_Locn','STATE, MAJOR_LOCATION, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','ADDITION_QTY < 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1119,'AP_NegativeAddsQty_Comp','COMPANY, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY','ADDITION_QTY < 0 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1120,'AP_NegativeAddsQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','ADDITION_QTY < 0 ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1121,'UC_AvgServiceCost_ChartOLD__NOTUSED','VINTAGE_YEAR, SUM(CURR_COST) CURR_COST, SUM(CURR_QTY) CURR_QTY, ROUND(SUM(CURR_COST) / SUM(CURR_QTY),2) AVG_SERVICE_COST, ROUND(NVL(AVG(VRATE),0),2) AS AVG_HW_RATE','PWRANLYT.PA_CPR_LEDGER','SUM(CURR_QTY) <> 0','VINTAGE_YEAR','1','RETIRE_UNIT_ID > 5',null,null,null,2);
    insert into PWRANLYT.PA_DATASOURCE values (1122,'RA_EndReserveTrend_Chart','GL_POSTING_YEAR, ROUND(SUM(END_RESERVE),2) END_LIFE_RESERVE, ROUND(SUM(END_THEO_RESERVE),2) END_THEO_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1123,'RA_ReserveDelta_Chart','COMPANY, GL_POSTING_YEAR, SUM(END_RESERVE) END_RESERVE, SUM(END_THEO_RESERVE) END_THEO_RESERVE, CASE WHEN (SUM(END_THEO_RESERVE) = 0 AND SUM(END_RESERVE) <> 0) THEN 100 WHEN ROUND((1-(SUM(END_RESERVE)/SUM(END_THEO_RESERVE)))*100,0) < -20 THEN -20 WHEN ROUND((1-(SUM(END_RESERVE)/SUM(END_THEO_RESERVE)))*100,0) > 20 THEN 20 ELSE ROUND((1-(SUM(END_RESERVE)/SUM(END_THEO_RESERVE)))*100,0) END END_RATIO ','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY, GL_POSTING_YEAR','1,2','(END_RESERVE <> 0 OR END_THEO_RESERVE <> 0)',null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1124,'RA_AnnualChange_Chart','GL_POSTING_YEAR, SUM(BEGIN_RESERVE) BEGIN_LIFE_RESERVE, SUM(END_RESERVE) END_LIFE_RESERVE, SUM(END_RESERVE) - SUM(BEGIN_RESERVE) ANNUAL_CHANGE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1125,'RA_EndReserveTrend_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(END_RESERVE),2) CURR_END_LIFE_RESERVE, ROUND(SUM(END_THEO_RESERVE),2) CURR_END_THEO_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1127,'RA_EndReserveTrend_Comp','COMPANY, ROUND(SUM(END_RESERVE),2) CURR_END_LIFE_RESERVE, ROUND(SUM(END_THEO_RESERVE),2) CURR_END_THEO_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1128,'RA_EndReserveTrend_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, ROUND(SUM(END_RESERVE),2) END_LIFE_RESERVE, ROUND(SUM(END_THEO_RESERVE),2) END_THEO_RESERVE, CASE WHEN SUM(END_THEO_RESERVE) = 0 THEN NULL ELSE ROUND(((SUM(END_RESERVE)/SUM(END_THEO_RESERVE)-1))*100,2) END PCT_DELTA, SUM(BEGIN_BALANCE) BEGIN_BALANCE, SUM(END_BALANCE) END_BALANCE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1129,'RA_ReserveDelta_Acct_NOTUSED','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(END_RESERVE),2) CURR_END_RESERVE, ROUND(SUM(END_THEO_RESERVE),2) CURR_END_THEO_RESERVE, CASE WHEN (SUM(END_THEO_RESERVE) = 0 AND SUM(END_RESERVE) <> 0) THEN 100 WHEN ROUND((1-(SUM(END_RESERVE)/SUM(END_THEO_RESERVE)))*100,0) < -20 THEN -20 WHEN ROUND((1-(SUM(END_RESERVE)/SUM(END_THEO_RESERVE)))*100,0) > 20 THEN 20 ELSE ROUND((1-(SUM(END_RESERVE)/SUM(END_THEO_RESERVE)))*100,0) END END_RATIO','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(END_RESERVE <> 0 OR END_THEO_RESERVE <> 0)',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1131,'RA_ReserveDelta_Comp_NOTUSED','COMPANY, ROUND(SUM(END_RESERVE),2) CURR_END_RESERVE, ROUND(SUM(END_THEO_RESERVE),2) CURR_END_THEO_RESERVE, CASE WHEN (SUM(END_THEO_RESERVE) = 0 AND SUM(END_RESERVE) <> 0) THEN 100 WHEN ROUND((1-(SUM(END_RESERVE)/SUM(END_THEO_RESERVE)))*100,0) < -20 THEN -20 WHEN ROUND((1-(SUM(END_RESERVE)/SUM(END_THEO_RESERVE)))*100,0) > 20 THEN 20 ELSE ROUND((1-(SUM(END_RESERVE)/SUM(END_THEO_RESERVE)))*100,0) END END_RATIO','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY','(END_RESERVE <> 0 OR END_THEO_RESERVE <> 0)  ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1132,'RA_ReserveDelta_Detail_NOTUSED','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, GL_POSTING_YEAR, ROUND(SUM(END_RESERVE),2) END_LIFE_RESERVE, ROUND(SUM(END_THEO_RESERVE),2) END_THEO_RESERVE, CASE WHEN SUM(END_THEO_RESERVE) = 0 THEN NULL ELSE ROUND((1-(SUM(END_RESERVE)/SUM(END_THEO_RESERVE)))*100,2) END PCT_DELTA, SUM(BEGIN_BALANCE) BEGIN_BALANCE, SUM(END_BALANCE) END_BALANCE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, GL_POSTING_YEAR','(END_RESERVE <> 0 OR END_THEO_RESERVE <> 0)',null,500,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1133,'RA_AnnualChange_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(BEGIN_RESERVE) BEGIN_LIFE_RESERVE, SUM(END_RESERVE) END_LIFE_RESERVE, SUM(END_RESERVE) - SUM(BEGIN_RESERVE) ANNUAL_CHANGE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1135,'RA_AnnualChange_Comp','COMPANY, SUM(BEGIN_RESERVE) BEGIN_LIFE_RESERVE, SUM(END_RESERVE) END_LIFE_RESERVE, SUM(END_RESERVE) - SUM(BEGIN_RESERVE) ANNUAL_CHANGE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1136,'RA_AnnualChange_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, ROUND(SUM(END_RESERVE),2) END_LIFE_RESERVE, ROUND(SUM(END_THEO_RESERVE),2) END_THEO_RESERVE, CASE WHEN SUM(END_THEO_RESERVE) = 0 THEN NULL ELSE ROUND(((SUM(END_RESERVE)/SUM(END_THEO_RESERVE)-1))*100,2) END PCT_DELTA, SUM(BEGIN_BALANCE) BEGIN_BALANCE, SUM(END_BALANCE) END_BALANCE, SUM(END_RESERVE) - SUM(BEGIN_RESERVE) ANNUAL_CHANGE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1137,'CR_RemovalPerRetirement_Chart','process_year, sum(cost_of_removal) cor, sum(retire_qty) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(cost_of_removal)/sum(retire_qty) end,2) cor_per_unit, sum(retire_cost) retire_cost, case when sum(retire_cost) = 0 then null else round(sum(cost_of_removal)/sum(retire_cost) * 100,2) end pct_cor_retire_cost','PWRANLYT.V_PA_CPR_BALANCES',null,'process_year','1',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1138,'CR_RemovalPerRetirement_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, sum(cost_of_removal) cor, sum(retire_qty) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(cost_of_removal)/sum(retire_qty) end,2) cor_per_unit, sum(retire_cost) retire_cost, case when sum(retire_cost) = 0 then null else round(sum(cost_of_removal)/sum(retire_cost),2) end pct_cor_retire_cost','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1139,'CR_RemovalPerRetirement_Locn','STATE, MAJOR_LOCATION, sum(cost_of_removal) cor, sum(retire_qty) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(cost_of_removal)/sum(retire_qty) end,2) cor_per_unit, sum(retire_cost) retire_cost, case when sum(retire_cost) = 0 then null else round(sum(cost_of_removal)/sum(retire_cost),2) end pct_cor_retire_cost','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1140,'CR_RemovalPerRetirement_Comp','COMPANY, sum(cost_of_removal) cor, sum(retire_qty) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(cost_of_removal)/sum(retire_qty) end,2) cor_per_unit, sum(retire_cost) retire_cost, case when sum(retire_cost) = 0 then null else round(sum(cost_of_removal)/sum(retire_cost),2) end pct_cor_retire_cost','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1141,'CR_RemovalPerRetirement_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, sum(cost_of_removal) cor, sum(retire_qty) retire_qty, round(case when sum(retire_qty) = 0 then null else sum(cost_of_removal)/sum(retire_qty) end,2) cor_per_unit, sum(retire_cost) retire_cost, case when sum(retire_cost) = 0 then null else round(sum(cost_of_removal)/sum(retire_cost),2) end pct_cor_retire_cost','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1142,'CR_RemovalPerAddition_Chart','process_year, sum(cost_of_removal) cor, sum(addition_qty) addition_qty, round(case when sum(addition_qty) = 0 then null else sum(cost_of_removal)/sum(addition_qty) end,2) cor_per_unit, sum(addition_cost) addition_cost, case when sum(addition_cost) = 0 then null else round(sum(cost_of_removal)/sum(addition_cost) * 100,2) end pct_cor_addition_cost','PWRANLYT.V_PA_CPR_BALANCES',null,'process_year','1',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1143,'CR_RemovalPerAddition_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, sum(cost_of_removal) cor, sum(addition_qty) addition_qty, round(case when sum(addition_qty) = 0 then null else sum(cost_of_removal)/sum(addition_qty) end,2) cor_per_unit, sum(addition_cost) addition_cost, case when sum(addition_cost) = 0 then null else round(sum(cost_of_removal)/sum(addition_cost) * 100,2) end pct_cor_addition_cost','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1144,'CR_RemovalPerAddition_Locn','STATE, MAJOR_LOCATION, sum(cost_of_removal) cor, sum(addition_qty) addition_qty, round(case when sum(addition_qty) = 0 then null else sum(cost_of_removal)/sum(addition_qty) end,2) cor_per_unit, sum(addition_cost) addition_cost, case when sum(addition_cost) = 0 then null else round(sum(cost_of_removal)/sum(addition_cost) * 100,2) end pct_cor_addition_cost','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1145,'CR_RemovalPerAddition_Comp','COMPANY, sum(cost_of_removal) cor, sum(addition_qty) addition_qty, round(case when sum(addition_qty) = 0 then null else sum(cost_of_removal)/sum(addition_qty) end,2) cor_per_unit, sum(addition_cost) addition_cost, case when sum(addition_cost) = 0 then null else round(sum(cost_of_removal)/sum(addition_cost) * 100,2) end pct_cor_addition_cost','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1146,'CR_RemovalPerAddition_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, sum(cost_of_removal) cor, sum(addition_qty) addition_qty, round(case when sum(addition_qty) = 0 then null else sum(cost_of_removal)/sum(addition_qty) end,2) cor_per_unit, sum(addition_cost) addition_cost, case when sum(addition_cost) = 0 then null else round(sum(cost_of_removal)/sum(addition_cost) * 100,2) end pct_cor_addition_cost','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1147,'PR_AnnualCost_Chart','process_year, round(sum(nvl(retire_cost,0)),2) retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(nvl(retire_cost,0)),2) - sum(proj_retire_cost*-1) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'process_year','1','proj_retire_cost is not null 
    ',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1148,'PR_AnnualCost_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, round(sum(nvl(retire_cost,0)),2) retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(nvl(retire_cost,0)),2) - sum(proj_retire_cost*-1) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','proj_retire_cost is not null 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1149,'PR_AnnualCost_Locn','STATE, MAJOR_LOCATION, round(sum(nvl(retire_cost,0)),2) retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(nvl(retire_cost,0)),2) - sum(proj_retire_cost*-1) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','proj_retire_cost is not null 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1150,'PR_AnnualCost_Comp','COMPANY, round(sum(nvl(retire_cost,0)),2) retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(nvl(retire_cost,0)),2) - sum(proj_retire_cost*-1) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY','proj_retire_cost is not null
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1151,'UC_AvgServiceCost_Chart','VINTAGE_INTERVAL, SUM(CURR_COST) CURR_COST, SUM(CURR_QTY) CURR_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END AS AVG_SERVICE_COST, ROUND(NVL(AVG(HWRATE),0),2) AS AVG_HW_RATE
    ','PWRANLYT.V_PA_CPR_LEDGER
    ',null,'VINTAGE_INTERVAL','1',null,null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1152,'UC_AvgServiceCost_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(CURR_COST) CURR_COST, SUM(CURR_QTY) CURR_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END AS AVG_SERVICE_COST, ROUND(NVL(AVG(HWRATE),0),2) AS AVG_HW_RATE','PWRANLYT.V_PA_CPR_LEDGER',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1153,'UC_AvgServiceCost_Locn','STATE, MAJOR_LOCATION, SUM(CURR_COST) CURR_COST, SUM(CURR_QTY) CURR_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END AS AVG_SERVICE_COST, ROUND(NVL(AVG(HWRATE),0),2) AS AVG_HW_RATE','PWRANLYT.V_PA_CPR_LEDGER','SUM(CURR_QTY) <> 0','STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1154,'UC_AvgServiceCost_Comp','COMPANY, SUM(CURR_COST) CURR_COST, SUM(CURR_QTY) CURR_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END AS AVG_SERVICE_COST, ROUND(NVL(AVG(HWRATE),0),2) AS AVG_HW_RATE','PWRANLYT.V_PA_CPR_LEDGER',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1155,'UC_AvgServiceCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, SUM(CURR_COST) CURR_COST, SUM(CURR_QTY) CURR_QTY, CASE WHEN SUM(CURR_QTY) = 0 THEN 0 ELSE ROUND(SUM(CURR_COST)/SUM(CURR_QTY),2) END AS AVG_SERVICE_COST, ROUND(NVL(AVG(HWRATE),0),2) AS AVG_HW_RATE','PWRANLYT.V_PA_CPR_LEDGER',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR',null,null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1156,'LA_PctAgeBand_Chart_NOTUSED','LIFE_BAND, SUM(CURR_QTY) CURR_QTY, SUM(CURR_COST) CURR_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'LIFE_BAND','1','EXPECTED_LIFE IS NOT NULL 
    and RETIRE_UNIT_ID > 5',null,null,null,2);
    insert into PWRANLYT.PA_DATASOURCE values (1157,'LA_PctAgeBand_Acct_NOTUSED','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(CURR_QTY) CURR_QTY, SUM(CURR_COST) CURR_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','EXPECTED_LIFE IS NOT NULL 
    and RETIRE_UNIT_ID > 5',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1158,'LA_PctAgeBand_Locn_NOTUSED','STATE, MAJOR_LOCATION, SUM(CURR_QTY) CURR_QTY, SUM(CURR_COST) CURR_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','EXPECTED_LIFE IS NOT NULL 
    and RETIRE_UNIT_ID > 5',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1159,'LA_PctAgeBand_Comp_NOTUSED','COMPANY, SUM(CURR_QTY) CURR_QTY, SUM(CURR_COST) CURR_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'COMPANY','COMPANY','EXPECTED_LIFE IS NOT NULL 
    and RETIRE_UNIT_ID > 5',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1160,'LA_PctAgeBand_Detail_NOTUSED','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, COMPANY, SUM(CURR_QTY) CURR_QTY, SUM(CURR_COST) CURR_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, COMPANY','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, COMPANY','EXPECTED_LIFE IS NOT NULL 
    and RETIRE_UNIT_ID > 5',null,500,null,2);
    insert into PWRANLYT.PA_DATASOURCE values (1161,'CR_CorRetireMismatch_Chart_NOTUSED','process_year, vintage_year, sum(nvl(cost_of_removal,0)) cor, sum(nvl(retire_qty,0)) retire_qty, case when sum(nvl(retire_qty,0)) = 0 and sum(nvl(cost_of_removal,0)) <> 0 then 100 when sum(nvl(retire_qty,0)) <> 0 and sum(nvl(cost_of_removal,0)) = 0 then -100 else 0 end cor_flag, round(case when sum(nvl(retire_qty,0)) = 0 or sum(nvl(cost_of_removal,0)) = 0 then 0 else sum(nvl(cost_of_removal,0))/sum(nvl(retire_qty,0)) end,2) cor_per_unit','PWRANLYT.V_PA_CPR_BALANCES',null,'process_year, vintage_year','1,2',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1162,'CR_CorRetireMismatch_Acct_NOTUSED','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, sum(nvl(cost_of_removal,0)) cor, sum(nvl(retire_qty,0)) retire_qty, case when sum(nvl(retire_qty,0)) = 0 and sum(nvl(cost_of_removal,0)) <> 0 then 100 when sum(nvl(retire_qty,0)) <> 0 and sum(nvl(cost_of_removal,0)) = 0 then -100 else 0 end cor_flag, round(case when sum(nvl(retire_qty,0)) = 0 or sum(nvl(cost_of_removal,0)) = 0 then 0 else sum(nvl(cost_of_removal,0))/sum(nvl(retire_qty,0)) end,2) cor_per_unit','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1163,'CR_CorRetireMismatch_Locn_NOTUSED','STATE, MAJOR_LOCATION, sum(nvl(cost_of_removal,0)) cor, sum(nvl(retire_qty,0)) retire_qty, case when sum(nvl(retire_qty,0)) = 0 and sum(nvl(cost_of_removal,0)) <> 0 then 100 when sum(nvl(retire_qty,0)) <> 0 and sum(nvl(cost_of_removal,0)) = 0 then -100 else 0 end cor_flag, round(case when sum(nvl(retire_qty,0)) = 0 or sum(nvl(cost_of_removal,0)) = 0 then 0 else sum(nvl(cost_of_removal,0))/sum(nvl(retire_qty,0)) end,2) cor_per_unit','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1164,'CR_CorRetireMismatch_Comp_NOTUSED','COMPANY, sum(nvl(cost_of_removal,0)) cor, sum(nvl(retire_qty,0)) retire_qty, case when sum(nvl(retire_qty,0)) = 0 and sum(nvl(cost_of_removal,0)) <> 0 then 100 when sum(nvl(retire_qty,0)) <> 0 and sum(nvl(cost_of_removal,0)) = 0 then -100 else 0 end cor_flag, round(case when sum(nvl(retire_qty,0)) = 0 or sum(nvl(cost_of_removal,0)) = 0 then 0 else sum(nvl(cost_of_removal,0))/sum(nvl(retire_qty,0)) end,2) cor_per_unit','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1165,'CR_CorRetireMismatch_Detail_NOTUSED','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, sum(nvl(cost_of_removal,0)) cor, sum(nvl(retire_qty,0)) retire_qty, case when sum(nvl(retire_qty,0)) = 0 and sum(nvl(cost_of_removal,0)) <> 0 then 100 when sum(nvl(retire_qty,0)) <> 0 and sum(nvl(cost_of_removal,0)) = 0 then -100 else 0 end cor_flag, round(case when sum(nvl(retire_qty,0)) = 0 or sum(nvl(cost_of_removal,0)) = 0 then 0 else sum(nvl(cost_of_removal,0))/sum(nvl(retire_qty,0)) end,2) cor_per_unit','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR',null,null,500,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1166,'DL_AnnualDeprLedger_Chart','gl_posting_year, sum(end_balance) end_balance, sum(additions) additions, sum(retirements) retirements, sum(cost_of_removal) cost_of_removal','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'gl_posting_year','1',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1167,'DL_AnnualDeprLedger_Comp','company, round(avg(end_balance),2) avg_end_balance, sum(additions) additions, sum(retirements) retirements, sum(cost_of_removal) cost_of_removal','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'company','company',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1168,'DL_AnnualDeprLedger_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, round(avg(end_balance),2) avg_end_balance, sum(additions) additions, sum(retirements) retirements, sum(cost_of_removal) cost_of_removal','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1169,'DL_AnnualDeprLedger_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, sum(end_balance) end_balance, sum(additions) additions, sum(retirements) retirements, sum(cost_of_removal) cost_of_removal','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1170,'DL_CorBeginReserve_Chart','GL_POSTING_YEAR, ROUND(SUM(COST_OF_REMOVAL),2) COST_OF_REMOVAL, ROUND(SUM(COR_BEG_RESERVE),2) BEGIN_COR_RESERVE, CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN NULL ELSE ROUND(100*((SUM(COST_OF_REMOVAL)*-1)/SUM(COR_BEG_RESERVE)),4) END PCT_INCREASE_COR_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1171,'DL_CorBeginReserve_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(COST_OF_REMOVAL),2) COST_OF_REMOVAL, ROUND(AVG(COR_BEG_RESERVE),2) AVG_BEGIN_COR_RESERVE, CASE WHEN AVG(NVL(COR_BEG_RESERVE,0)) = 0 THEN NULL ELSE ROUND(100*((SUM(COST_OF_REMOVAL)*-1)/SUM(COR_BEG_RESERVE)),4) END PCT_INCREASE_COR_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1172,'DL_CorBeginReserve_Comp','COMPANY, ROUND(SUM(COST_OF_REMOVAL),2) COST_OF_REMOVAL, ROUND(AVG(COR_BEG_RESERVE),2) AVG_BEGIN_COR_RESERVE, CASE WHEN AVG(NVL(COR_BEG_RESERVE,0)) = 0 THEN NULL ELSE ROUND(100*((SUM(COST_OF_REMOVAL)*-1)/SUM(COR_BEG_RESERVE)),4) END PCT_INCREASE_COR_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1173,'DL_CorBeginReserve_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, ROUND(SUM(COST_OF_REMOVAL),2) COST_OF_REMOVAL, ROUND(SUM(COR_BEG_RESERVE),2) BEGIN_COR_RESERVE, CASE WHEN AVG(NVL(COR_BEG_RESERVE,0)) = 0 THEN NULL ELSE ROUND(100*((SUM(COST_OF_REMOVAL)*-1)/SUM(COR_BEG_RESERVE)),4) END PCT_INCREASE_COR_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1174,'DL_CorAddedReserve_Chart','GL_POSTING_YEAR, ROUND(SUM(COR_EXPENSE),2) COR_EXPENSE, ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2) BEGIN_COR_RESERVE, CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN 0 ELSE ROUND(100*(SUM(COR_EXPENSE)/SUM(COR_BEG_RESERVE)),4) END PCT_INCREASE_COR_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1175,'DL_CorAddedReserve_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(COR_EXPENSE),2) COR_EXPENSE, ROUND(AVG(NVL(COR_BEG_RESERVE,0)),2) AVG_BEGIN_COR_RESERVE, CASE WHEN AVG(NVL(COR_BEG_RESERVE,0)) = 0 THEN 0 ELSE ROUND(100*(SUM(COR_EXPENSE)/SUM(COR_BEG_RESERVE)),4) END PCT_INCREASE_COR_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1176,'DL_CorAddedReserve_Comp','COMPANY, ROUND(SUM(COR_EXPENSE),2) COR_EXPENSE, ROUND(AVG(NVL(COR_BEG_RESERVE,0)),2) AVG_BEGIN_COR_RESERVE, CASE WHEN AVG(NVL(COR_BEG_RESERVE,0)) = 0 THEN 0 ELSE ROUND(100*(SUM(COR_EXPENSE)/SUM(COR_BEG_RESERVE)),4) END PCT_INCREASE_COR_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1177,'DL_CorAddedReserve_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, ROUND(SUM(COR_EXPENSE),2) COR_EXPENSE, ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2) BEGIN_COR_RESERVE, CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN 0 ELSE ROUND(100*(SUM(COR_EXPENSE)/SUM(COR_BEG_RESERVE)),4) END PCT_INCREASE_COR_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP,  GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP,  GL_POSTING_YEAR',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1178,'DL_CorReserveChange_Chart','GL_POSTING_YEAR, ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2) BEGIN_COR_RESERVE, ROUND(SUM(NVL(COR_END_RESERVE,0)),2) END_COR_RESERVE, ROUND(100*(CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN 0 ELSE SUM(COR_END_RESERVE-COR_BEG_RESERVE)/SUM(COR_BEG_RESERVE) END),4) PCT_CHANGE_COR_RESERVE, (ROUND(SUM(NVL(COR_END_RESERVE,0)),2)) - (ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2))  ANNUAL_CHANGE_COR_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1179,'DL_CorReserveChange_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(AVG(NVL(COR_BEG_RESERVE,0)),2) AVG_BEGIN_COR_RESERVE, ROUND(AVG(NVL(COR_END_RESERVE,0)),2) AVG_END_COR_RESERVE, ROUND(100*(CASE WHEN AVG(NVL(COR_BEG_RESERVE,0)) = 0 THEN 0 ELSE AVG(COR_END_RESERVE-COR_BEG_RESERVE)/AVG(COR_BEG_RESERVE) END),4) PCT_CHANGE_COR_RESERVE, (ROUND(SUM(NVL(COR_END_RESERVE,0)),2)) - (ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2))  ANNUAL_CHANGE_COR_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1180,'DL_CorReserveChange_Comp','COMPANY, ROUND(AVG(NVL(COR_BEG_RESERVE,0)),2) AVG_BEGIN_COR_RESERVE, ROUND(AVG(NVL(COR_END_RESERVE,0)),2) AVG_END_COR_RESERVE, ROUND(100*(CASE WHEN AVG(NVL(COR_BEG_RESERVE,0)) = 0 THEN 0 ELSE AVG(COR_END_RESERVE-COR_BEG_RESERVE)/AVG(COR_BEG_RESERVE) END),4) PCT_CHANGE_COR_RESERVE, (ROUND(SUM(NVL(COR_END_RESERVE,0)),2)) - (ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2))  ANNUAL_CHANGE_COR_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1181,'DL_CorReserveChange_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2) BEGIN_COR_RESERVE, ROUND(SUM(NVL(COR_END_RESERVE,0)),2) END_COR_RESERVE, ROUND(100*(CASE WHEN SUM(NVL(COR_BEG_RESERVE,0)) = 0 THEN 0 ELSE SUM(COR_END_RESERVE-COR_BEG_RESERVE)/SUM(COR_BEG_RESERVE) END),4) PCT_CHANGE_COR_RESERVE, (ROUND(SUM(NVL(COR_END_RESERVE,0)),2)) - (ROUND(SUM(NVL(COR_BEG_RESERVE,0)),2))  ANNUAL_CHANGE_COR_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1182,'DL_RetireRatio_Chart','GL_POSTING_YEAR, SUM(BEGIN_RESERVE) BEG_RESERVE, SUM(RETIREMENTS) RETIREMENTS, ROUND(100*(CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE SUM(RETIREMENTS)/SUM(BEGIN_RESERVE) END),4) PCT_BEGIN_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1183,'DL_RetireRatio_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(AVG(BEGIN_RESERVE),2) AVG_BEG_RESERVE, SUM(RETIREMENTS) RETIREMENTS, ROUND(100*(CASE WHEN AVG(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE SUM(RETIREMENTS)/AVG(BEGIN_RESERVE) END),4) PCT_BEGIN_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1184,'DL_RetireRatio_Comp','COMPANY, ROUND(AVG(BEGIN_RESERVE),2) AVG_BEG_RESERVE, SUM(RETIREMENTS) RETIREMENTS, ROUND(100*(CASE WHEN AVG(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE SUM(RETIREMENTS)/AVG(BEGIN_RESERVE) END),4) PCT_BEGIN_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1185,'DL_RetireRatio_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, SUM(BEGIN_RESERVE) BEG_RESERVE, SUM(RETIREMENTS) RETIREMENTS, ROUND(100*(CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE SUM(RETIREMENTS)/SUM(BEGIN_RESERVE) END),4) PCT_BEGIN_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1186,'DL_DeprExpRatio_Chart','GL_POSTING_YEAR, SUM(BEGIN_RESERVE) BEG_RESERVE, SUM(END_RESERVE) END_RESERVE, SUM(DEPRECIATION_EXPENSE) DEPR_EXPENSE
     , ROUND(100*(CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE SUM(DEPRECIATION_EXPENSE)/SUM(BEGIN_RESERVE) END),4) PCT_BEGIN_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1187,'DL_DeprExpRatio_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(AVG(BEGIN_RESERVE),2) AVG_BEG_RESERVE, SUM(DEPRECIATION_EXPENSE) DEPR_EXPENSE, ROUND(100*(CASE WHEN AVG(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE SUM(DEPRECIATION_EXPENSE)/AVG(BEGIN_RESERVE) END),4) PCT_BEGIN_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1188,'DL_DeprExpRatio_Comp','COMPANY, ROUND(AVG(BEGIN_RESERVE),2) AVG_BEG_RESERVE, SUM(DEPRECIATION_EXPENSE) DEPR_EXPENSE, ROUND(100*(CASE WHEN AVG(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE SUM(DEPRECIATION_EXPENSE)/AVG(BEGIN_RESERVE) END),4) PCT_BEGIN_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1189,'DL_DeprExpRatio_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, ROUND(AVG(BEGIN_RESERVE),2) AVG_BEG_RESERVE, SUM(DEPRECIATION_EXPENSE) DEPR_EXPENSE, ROUND(100*(CASE WHEN AVG(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE SUM(DEPRECIATION_EXPENSE)/AVG(BEGIN_RESERVE) END),4) PCT_BEGIN_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1190,'DL_LifeReserveChange_Chart','GL_POSTING_YEAR, ROUND(SUM(NVL(BEGIN_RESERVE,0)),2) BEGIN_RESERVE, ROUND(SUM(NVL(END_RESERVE,0)),2) END_RESERVE, ROUND(100*(CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE SUM(END_RESERVE-BEGIN_RESERVE)/SUM(BEGIN_RESERVE) END),4) PCT_CHANGE_LIFE_RESERVE, ROUND(SUM(NVL(END_RESERVE,0)),2) - (ROUND(SUM(NVL(BEGIN_RESERVE,0)),2)) ANNUAL_CHANGE_LIFE_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1191,'DL_LifeReserveChange_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(AVG(NVL(BEGIN_RESERVE,0)),2) AVG_BEGIN_RESERVE, ROUND(AVG(NVL(END_RESERVE,0)),2) AVG_END_RESERVE, ROUND(100*(CASE WHEN AVG(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE AVG(END_RESERVE-BEGIN_RESERVE)/AVG(BEGIN_RESERVE) END),4) PCT_CHANGE_LIFE_RESERVE, ROUND(SUM(NVL(END_RESERVE,0)),2) - (ROUND(SUM(NVL(BEGIN_RESERVE,0)),2)) ANNUAL_CHANGE_LIFE_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1192,'DL_LifeReserveChange_Comp','COMPANY, ROUND(AVG(NVL(BEGIN_RESERVE,0)),2) AVG_BEGIN_RESERVE, ROUND(AVG(NVL(END_RESERVE,0)),2) AVG_END_RESERVE, ROUND(100*(CASE WHEN AVG(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE AVG(END_RESERVE-BEGIN_RESERVE)/AVG(BEGIN_RESERVE) END),4) PCT_CHANGE_LIFE_RESERVE, ROUND(SUM(NVL(END_RESERVE,0)),2) - (ROUND(SUM(NVL(BEGIN_RESERVE,0)),2)) ANNUAL_CHANGE_LIFE_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1193,'DL_LifeReserveChange_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, ROUND(SUM(NVL(BEGIN_RESERVE,0)),2) BEGIN_RESERVE, ROUND(SUM(NVL(END_RESERVE,0)),2) END_RESERVE, ROUND(100*(CASE WHEN SUM(NVL(BEGIN_RESERVE,0)) = 0 THEN 0 ELSE SUM(END_RESERVE-BEGIN_RESERVE)/SUM(BEGIN_RESERVE) END),4) PCT_CHANGE_LIFE_RESERVE, ROUND(SUM(NVL(END_RESERVE,0)),2) - (ROUND(SUM(NVL(BEGIN_RESERVE,0)),2)) ANNUAL_CHANGE_LIFE_RESERVE','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1194,'AA_WeightedAges_Chart','PROCESS_YEAR, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE ','PWRANLYT.V_PA_AVERAGE_AGES',null,'PROCESS_YEAR','1',null,null,null,null,1);
    insert into PWRANLYT.PA_DATASOURCE values (1195,'AA_WeightedAges_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE ','PWRANLYT.V_PA_AVERAGE_AGES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1196,'AA_WeightedAges_Locn','STATE, MAJOR_LOCATION, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE ','PWRANLYT.V_PA_AVERAGE_AGES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1197,'AA_WeightedAges_Comp','COMPANY, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE ','PWRANLYT.V_PA_AVERAGE_AGES',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1198,'AA_WeightedAges_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE, 
    SUM(ADDITION_QTY) ADDITION_QTY, SUM(ADDITION_COST) ADDITION_COST,
    SUM(RETIRE_QTY) RETIRE_QTY, SUM(RETIRE_COST) RETIRE_COST','PWRANLYT.V_PA_AVERAGE_AGES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR',null,null,null,null,1);
    insert into PWRANLYT.PA_DATASOURCE values (1199,'AA_Additions_Chart','PROCESS_YEAR, SUM(ADDITION_QTY) ADDITION_QTY, SUM(ADDITION_COST) ADDITION_COST','PWRANLYT.V_PA_AVERAGE_AGES',null,'PROCESS_YEAR','1',null,null,null,null,1);
    insert into PWRANLYT.PA_DATASOURCE values (1200,'AA_Additions_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(ADDITION_QTY) ADDITION_QTY, SUM(ADDITION_COST) ADDITION_COST','PWRANLYT.V_PA_AVERAGE_AGES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1201,'AA_Additions_Locn','STATE, MAJOR_LOCATION, SUM(ADDITION_QTY) ADDITION_QTY, SUM(ADDITION_COST) ADDITION_COST','PWRANLYT.V_PA_AVERAGE_AGES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1202,'AA_Additions_Comp','COMPANY, SUM(ADDITION_QTY) ADDITION_QTY, SUM(ADDITION_COST) ADDITION_COST','PWRANLYT.V_PA_AVERAGE_AGES',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1203,'AA_Additions_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, SUM(ADDITION_QTY) ADDITION_QTY, SUM(ADDITION_COST) ADDITION_COST,
    SUM(RETIRE_QTY) RETIRE_QTY, SUM(RETIRE_COST) RETIRE_COST, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE','PWRANLYT.V_PA_AVERAGE_AGES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR',null,null,null,null,1);
    insert into PWRANLYT.PA_DATASOURCE values (1204,'AA_Retirements_Chart','PROCESS_YEAR, SUM(RETIRE_QTY) RETIRE_QTY, SUM(RETIRE_COST) RETIRE_COST','PWRANLYT.V_PA_AVERAGE_AGES',null,'PROCESS_YEAR','1',null,null,null,null,1);
    insert into PWRANLYT.PA_DATASOURCE values (1205,'AA_Retirements_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(RETIRE_QTY) RETIRE_QTY, SUM(RETIRE_COST) RETIRE_COST','PWRANLYT.V_PA_AVERAGE_AGES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1206,'AA_Retirements_Locn','STATE, MAJOR_LOCATION, SUM(RETIRE_QTY) RETIRE_QTY, SUM(RETIRE_COST) RETIRE_COST','PWRANLYT.V_PA_AVERAGE_AGES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1207,'AA_Retirements_Comp','COMPANY, SUM(RETIRE_QTY) RETIRE_QTY, SUM(RETIRE_COST) RETIRE_COST','PWRANLYT.V_PA_AVERAGE_AGES',null,'COMPANY','COMPANY',null,null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1208,'AA_Retirements_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, SUM(RETIRE_QTY) RETIRE_QTY, SUM(RETIRE_COST) RETIRE_COST, 
    SUM(ADDITION_QTY) ADDITION_QTY, SUM(ADDITION_COST) ADDITION_COST,
     ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE','PWRANLYT.V_PA_AVERAGE_AGES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR',null,null,null,null,1);
    insert into PWRANLYT.PA_DATASOURCE values (1209,'AA_AnnualChange_Chart','PROCESS_YEAR, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE ','PWRANLYT.V_PA_AVERAGE_AGES',null,'PROCESS_YEAR','1','(FLAG_ACP2_COST = ''Y'' OR FLAG_ACM2_COST = ''Y'' OR FLAG_ACP2_QTY = ''Y'' OR FLAG_ACM2_QTY = ''Y'') 
    ',null,null,null,1);
    insert into PWRANLYT.PA_DATASOURCE values (1210,'AA_AnnualChange_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE ','PWRANLYT.V_PA_AVERAGE_AGES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(FLAG_ACP2_COST = ''Y'' OR FLAG_ACM2_COST = ''Y'' OR FLAG_ACP2_QTY = ''Y'' OR FLAG_ACM2_QTY = ''Y'') 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1211,'AA_AnnualChange_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE, 
    SUM(ADDITION_QTY) ADDITION_QTY, SUM(ADDITION_COST) ADDITION_COST,
    SUM(RETIRE_QTY) RETIRE_QTY, SUM(RETIRE_COST) RETIRE_COST','PWRANLYT.V_PA_AVERAGE_AGES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR','(FLAG_ACP2_COST = ''Y'' OR FLAG_ACM2_COST = ''Y'' OR FLAG_ACP2_QTY = ''Y'' OR FLAG_ACM2_QTY = ''Y'') 
    ',null,null,null,1);
    insert into PWRANLYT.PA_DATASOURCE values (1212,'AA_PeriodChange_Chart','PROCESS_YEAR, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE ','PWRANLYT.V_PA_AVERAGE_AGES',null,'PROCESS_YEAR','1','(FLAG_PCP1_COST = ''Y'' OR FLAG_PCM1_COST = ''Y'' OR FLAG_PCP1_QTY = ''Y'' OR FLAG_PCM1_QTY = ''Y'') 
    ',null,null,null,1);
    insert into PWRANLYT.PA_DATASOURCE values (1213,'AA_PeriodChange_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE ','PWRANLYT.V_PA_AVERAGE_AGES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','(FLAG_PCP1_COST = ''Y'' OR FLAG_PCM1_COST = ''Y'' OR FLAG_PCP1_QTY = ''Y'' OR FLAG_PCM1_QTY = ''Y'') 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1214,'AA_PeriodChange_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE, 
    SUM(ADDITION_QTY) ADDITION_QTY, SUM(ADDITION_COST) ADDITION_COST,
    SUM(RETIRE_QTY) RETIRE_QTY, SUM(RETIRE_COST) RETIRE_COST','PWRANLYT.V_PA_AVERAGE_AGES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR','(FLAG_PCP1_COST = ''Y'' OR FLAG_PCM1_COST = ''Y'' OR FLAG_PCP1_QTY = ''Y'' OR FLAG_PCM1_QTY = ''Y'') 
    ',null,null,null,1);
    insert into PWRANLYT.PA_DATASOURCE values (1215,'RE_ZCost_Qty_Chart','GL_POSTING_YEAR, ROUND(SUM(ACTIVITY_COST),2) RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_QTY > 0 THEN ACTIVITY_QTY ELSE 0 END),2) POS_RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_QTY < 0 THEN ACTIVITY_QTY ELSE 0 END),2) NEG_RETIREMENT_QTY, ROUND(SUM(ACTIVITY_QTY),2) NET_RETIREMENT_QTY, COUNT(*) RECORD_COUNT','PWRANLYT.V_PA_CPR_ACTIVITY',null,'GL_POSTING_YEAR','1','FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_COST = 0 AND ACTIVITY_QTY <> 0 AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1216,'RE_ZCost_Qty_Comp','COMPANY, ROUND(SUM(ACTIVITY_COST),2) RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_QTY > 0 THEN ACTIVITY_QTY ELSE 0 END),2) POS_RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_QTY < 0 THEN ACTIVITY_QTY ELSE 0 END),2) NEG_RETIREMENT_QTY, ROUND(SUM(ACTIVITY_QTY),2) NET_RETIREMENT_QTY, COUNT(*) RECORD_COUNT','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY','COMPANY','FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_COST = 0 AND ACTIVITY_QTY <> 0 AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1217,'RE_ZCost_Qty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, ASSET_ID, ROUND(SUM(ACTIVITY_COST),2) RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_QTY > 0 THEN ACTIVITY_QTY ELSE 0 END),2) POS_RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_QTY < 0 THEN ACTIVITY_QTY ELSE 0 END),2) NEG_RETIREMENT_QTY, ROUND(SUM(ACTIVITY_QTY),2) NET_RETIREMENT_QTY, COUNT(*) RECORD_COUNT','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, ASSET_ID','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, ASSET_ID','FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_COST = 0 AND ACTIVITY_QTY <> 0 AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1218,'RE_ZQty_Cost_Chart','GL_POSTING_YEAR, ROUND(SUM(ACTIVITY_QTY),2) RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_COST > 0 THEN ACTIVITY_COST ELSE 0 END),2) POS_RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_COST < 0 THEN ACTIVITY_COST ELSE 0 END),2) NEG_RETIREMENT_COST, ROUND(SUM(ACTIVITY_COST),2) NET_RETIREMENT_COST, COUNT(*) RECORD_COUNT','PWRANLYT.V_PA_CPR_ACTIVITY',null,'GL_POSTING_YEAR','1','FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_QTY = 0 AND ACTIVITY_COST <> 0 AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5)',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1219,'RE_ZQty_Cost_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(ACTIVITY_QTY),2) RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_COST > 0 THEN ACTIVITY_COST ELSE 0 END),2) POS_RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_COST < 0 THEN ACTIVITY_COST ELSE 0 END),2) NEG_RETIREMENT_COST, ROUND(SUM(ACTIVITY_COST),2) NET_RETIREMENT_COST, COUNT(*) RECORD_COUNT','PWRANLYT.V_PA_CPR_ACTIVITY',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_QTY = 0 AND ACTIVITY_COST <> 0 AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5)',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1220,'RE_ZQty_Cost_Locn','STATE, MAJOR_LOCATION, ROUND(SUM(ACTIVITY_QTY),2) RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_COST > 0 THEN ACTIVITY_COST ELSE 0 END),2) POS_RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_COST < 0 THEN ACTIVITY_COST ELSE 0 END),2) NEG_RETIREMENT_COST, ROUND(SUM(ACTIVITY_COST),2) NET_RETIREMENT_COST, COUNT(*) RECORD_COUNT','PWRANLYT.V_PA_CPR_ACTIVITY',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_QTY = 0 AND ACTIVITY_COST <> 0 AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5)',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1221,'RE_ZQty_Cost_Comp','COMPANY, ROUND(SUM(ACTIVITY_QTY),2) RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_COST > 0 THEN ACTIVITY_COST ELSE 0 END),2) POS_RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_COST < 0 THEN ACTIVITY_COST ELSE 0 END),2) NEG_RETIREMENT_COST, ROUND(SUM(ACTIVITY_COST),2) NET_RETIREMENT_COST, COUNT(*) RECORD_COUNT','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY','COMPANY','FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_QTY = 0 AND ACTIVITY_COST <> 0 AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5)',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1222,'RE_ZQty_Cost_Details','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, COMPANY, ASSET_ID, ROUND(SUM(ACTIVITY_QTY),2) RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_COST > 0 THEN ACTIVITY_COST ELSE 0 END),2) POS_RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_COST < 0 THEN ACTIVITY_COST ELSE 0 END),2) NEG_RETIREMENT_COST, ROUND(SUM(ACTIVITY_COST),2) NET_RETIREMENT_COST, COUNT(*) RECORD_COUNT','PWRANLYT.V_PA_CPR_ACTIVITY',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, COMPANY, ASSET_ID','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, COMPANY, ASSET_ID','FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_QTY = 0 AND ACTIVITY_COST <> 0 AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5)',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1223,'AP_VintageAdds_Chart','VINTAGE_INTERVAL, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1224,'RP_VintageRets_Chart','VINTAGE_INTERVAL, (SUM(RETIRE_COST)*-1) RETIRE_COST, (SUM(RETIRE_QTY)*-1) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1225,'PR_VintageCost_Chart','VINTAGE_INTERVAL, round(sum(nvl((retire_cost*-1),0)),2) as retire_cost, round(sum(proj_retire_cost),2) as proj_retire_cost, round(sum(proj_retire_cost) - sum(nvl((retire_cost*-1),0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1226,'PR_VintageQty_Chart','VINTAGE_INTERVAL, round(sum(nvl((retire_qty*-1),0)),2) as retire_qty, round(sum(proj_retire_qty),2) as proj_retire_qty, round(sum(proj_retire_qty) - sum(nvl((retire_qty*-1),0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'VINTAGE_INTERVAL','1',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1227,'PR_AnnualCost_Chart_NOTUSED','process_year, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'process_year','1',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1228,'PR_AnnualQty_Chart','process_year, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'process_year','1',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1229,'RA_PctOverUnder_Chart','GL_POSTING_YEAR, SUM(END_RESERVE) END_RESERVE, SUM(END_THEO_RESERVE) END_THEO_RESERVE, case when sum(end_theo_reserve) = 0 then NULL else ROUND(((SUM(END_RESERVE) - SUM(END_THEO_RESERVE))/SUM(END_THEO_RESERVE))*100,2) end PCT_RESERVED','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'GL_POSTING_YEAR','1',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1230,'CR_CorRetireMissing_Chart','vintage_interval, sum(nvl(cost_of_removal,0)) cor, sum(nvl(retire_qty,0)) retire_qty, sum(nvl(retire_cost,0)) retire_cost,  round(case when sum(nvl(retire_qty,0)) = 0 or sum(nvl(cost_of_removal,0)) = 0 then 0 else sum(nvl(cost_of_removal,0))/sum(nvl(retire_qty,0)) end,2) cor_per_unit','PWRANLYT.V_PA_CPR_BALANCES',null,'vintage_interval','1,2','retire_qty = 0 or retire_cost = 0',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1231,'RE_ZCost_Qty_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, ROUND(SUM(ACTIVITY_COST),2) RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_QTY > 0 THEN ACTIVITY_QTY ELSE 0 END),2) POS_RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_QTY < 0 THEN ACTIVITY_QTY ELSE 0 END),2) NEG_RETIREMENT_QTY, ROUND(SUM(ACTIVITY_QTY),2) NET_RETIREMENT_QTY, COUNT(*) RECORD_COUNT','PWRANLYT.V_PA_CPR_ACTIVITY',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_COST = 0 AND ACTIVITY_QTY <> 0 AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1232,'RE_ZCost_Qty_Locn','STATE, MAJOR_LOCATION, ROUND(SUM(ACTIVITY_COST),2) RETIREMENT_COST, ROUND(SUM(CASE WHEN ACTIVITY_QTY > 0 THEN ACTIVITY_QTY ELSE 0 END),2) POS_RETIREMENT_QTY, ROUND(SUM(CASE WHEN ACTIVITY_QTY < 0 THEN ACTIVITY_QTY ELSE 0 END),2) NEG_RETIREMENT_QTY, ROUND(SUM(ACTIVITY_QTY),2) NET_RETIREMENT_QTY, COUNT(*) RECORD_COUNT','PWRANLYT.V_PA_CPR_ACTIVITY',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','FERC_ACTIVITY = ''Retirement'' AND ACTIVITY_COST = 0 AND ACTIVITY_QTY <> 0 AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1233,'AA_AnnualChange_Comp','COMPANY, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE ','PWRANLYT.V_PA_AVERAGE_AGES',null,'COMPANY','COMPANY','(FLAG_ACP2_COST = ''Y'' OR FLAG_ACM2_COST = ''Y'' OR FLAG_ACP2_QTY = ''Y'' OR FLAG_ACM2_QTY = ''Y'') 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1234,'AA_AnnualChange_Locn','STATE, MAJOR_LOCATION, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE ','PWRANLYT.V_PA_AVERAGE_AGES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','(FLAG_ACP2_COST = ''Y'' OR FLAG_ACM2_COST = ''Y'' OR FLAG_ACP2_QTY = ''Y'' OR FLAG_ACM2_QTY = ''Y'') 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1235,'AA_PeriodChange_Locn','STATE, MAJOR_LOCATION, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE ','PWRANLYT.V_PA_AVERAGE_AGES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','(FLAG_PCP1_COST = ''Y'' OR FLAG_PCM1_COST = ''Y'' OR FLAG_PCP1_QTY = ''Y'' OR FLAG_PCM1_QTY = ''Y'') 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1236,'AA_PeriodChange_Comp','COMPANY, ROUND((CASE SUM(CURR_COST)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_CST)/SUM(W_COST)
    END),2) COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_ADJ)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_ADJ)/SUM(W_ADJ)
    END),2) ESCALATED_COST_WEIGHTED_AGE, 
    ROUND((CASE SUM(CURR_QTY)
      WHEN 0 THEN 0 
      ELSE SUM(AGE_W_QTY)/SUM(W_QTY)
    END),2) QUANTITY_WEIGHTED_AGE ','PWRANLYT.V_PA_AVERAGE_AGES',null,'COMPANY','COMPANY','(FLAG_PCP1_COST = ''Y'' OR FLAG_PCM1_COST = ''Y'' OR FLAG_PCP1_QTY = ''Y'' OR FLAG_PCM1_QTY = ''Y'') 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1237,'LE_FutureAssets_Location','STATE, MAJOR_LOCATION, COUNT(*) RECORD_COUNT, SUM(CURR_QTY) TOTAL_QUANTITY, SUM(CURR_COST) TOTAL_COST','PWRANLYT.V_PA_CPR_LEDGER',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','VINTAGE_YEAR > EXTRACT(YEAR FROM SYSDATE) 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1238,'LE_FutureAssets_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, COUNT(*) RECORD_COUNT, SUM(CURR_QTY) TOTAL_QUANTITY, SUM(CURR_COST) TOTAL_COST','PWRANLYT.V_PA_CPR_LEDGER',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','VINTAGE_YEAR > EXTRACT(YEAR FROM SYSDATE) 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1239,'PR_AnnualQty_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1240,'PR_AnnualQty_Comp','COMPANY, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1241,'PR_AnnualQty_Locn','STATE, MAJOR_LOCATION, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1242,'RA_PctOverUnder_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(END_RESERVE) END_RESERVE, SUM(END_THEO_RESERVE) END_THEO_RESERVE, case when sum(end_theo_reserve) = 0 then null else ROUND(((SUM(END_RESERVE) - SUM(END_THEO_RESERVE))/SUM(END_THEO_RESERVE))*100,2) end PCT_RESERVED','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1243,'RA_PctOverUnder_Comp','COMPANY, SUM(END_RESERVE) END_RESERVE, SUM(END_THEO_RESERVE) END_THEO_RESERVE, case when sum(end_theo_reserve) = 0 then null else ROUND(((SUM(END_RESERVE) - SUM(END_THEO_RESERVE))/SUM(END_THEO_RESERVE))*100,2) end PCT_RESERVED','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY','COMPANY',null,null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1244,'AP_VintageAdds_Comp','COMPANY, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1245,'AP_VintageAdds_Locn','STATE, MAJOR_LOCATION, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1246,'AP_VintageAdds_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1247,'RP_VintageRets_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, (SUM(RETIRE_COST)*-1) RETIRE_COST, (SUM(RETIRE_QTY)*-1) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1248,'RP_VintageRets_Locn','STATE, MAJOR_LOCATION, (SUM(RETIRE_COST)*-1) RETIRE_COST, (SUM(RETIRE_QTY)*-1) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1249,'RP_VintageRets_Comp','COMPANY, (SUM(RETIRE_COST)*-1) RETIRE_COST, (SUM(RETIRE_QTY)*-1) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1250,'PR_VintageCost_Comp','COMPANY, round(sum(nvl((retire_cost*-1),0)),2) as retire_cost, round(sum(proj_retire_cost),2) as proj_retire_cost, round(sum(proj_retire_cost) - sum(nvl((retire_cost*-1),0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1251,'PR_VintageCost_Locn','STATE, MAJOR_LOCATION, round(sum(nvl((retire_cost*-1),0)),2) as retire_cost, round(sum(proj_retire_cost),2) as proj_retire_cost, round(sum(proj_retire_cost) - sum(nvl((retire_cost*-1),0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1252,'PR_VintageCost_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, round(sum(nvl((retire_cost*-1),0)),2) as retire_cost, round(sum(proj_retire_cost),2) as proj_retire_cost, round(sum(proj_retire_cost) - sum(nvl((retire_cost*-1),0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1253,'PR_VintageQty_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, round(sum(nvl((retire_qty*-1),0)),2) as retire_qty, round(sum(proj_retire_qty),2) as proj_retire_qty, round(sum(proj_retire_qty) - sum(nvl((retire_qty*-1),0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1254,'PR_VintageQty_Comp','COMPANY, round(sum(nvl((retire_qty*-1),0)),2) as retire_qty, round(sum(proj_retire_qty),2) as proj_retire_qty, round(sum(proj_retire_qty) - sum(nvl((retire_qty*-1),0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY','COMPANY',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1255,'PR_VintageQty_Locn','STATE, MAJOR_LOCATION, round(sum(nvl((retire_qty*-1),0)),2) as retire_qty, round(sum(proj_retire_qty),2) as proj_retire_qty, round(sum(proj_retire_qty) - sum(nvl((retire_qty*-1),0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1256,'CR_CorRetireMissing_Comp','company, sum(nvl(cost_of_removal,0)) cor, sum(nvl(retire_qty,0)) retire_qty, sum(nvl(retire_cost,0)) retire_cost,  round(case when sum(nvl(retire_qty,0)) = 0 or sum(nvl(cost_of_removal,0)) = 0 then 0 else sum(nvl(cost_of_removal,0))/sum(nvl(retire_qty,0)) end,2) cor_per_unit','PWRANLYT.V_PA_CPR_BALANCES',null,'company','company','retire_qty = 0 or retire_cost = 0',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1257,'CR_CorRetireMissing_Acct','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, sum(nvl(cost_of_removal,0)) cor, sum(nvl(retire_qty,0)) retire_qty, sum(nvl(retire_cost,0)) retire_cost,  round(case when sum(nvl(retire_qty,0)) = 0 or sum(nvl(cost_of_removal,0)) = 0 then 0 else sum(nvl(cost_of_removal,0))/sum(nvl(retire_qty,0)) end,2) cor_per_unit','PWRANLYT.V_PA_CPR_BALANCES',null,'BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT','retire_qty = 0 or retire_cost = 0',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1258,'CR_CorRetireMissing_Locn','STATE, MAJOR_LOCATION, sum(nvl(cost_of_removal,0)) cor, sum(nvl(retire_qty,0)) retire_qty, sum(nvl(retire_cost,0)) retire_cost,  round(case when sum(nvl(retire_qty,0)) = 0 or sum(nvl(cost_of_removal,0)) = 0 then 0 else sum(nvl(cost_of_removal,0))/sum(nvl(retire_qty,0)) end,2) cor_per_unit','PWRANLYT.V_PA_CPR_BALANCES',null,'STATE, MAJOR_LOCATION','STATE, MAJOR_LOCATION','retire_qty = 0 or retire_cost = 0',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1259,'CR_CorRetireMissing_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, sum(nvl(cost_of_removal,0)) cor, sum(nvl(retire_qty,0)) retire_qty, sum(nvl(retire_cost,0)) retire_cost,  round(case when sum(nvl(retire_qty,0)) = 0 or sum(nvl(cost_of_removal,0)) = 0 then 0 else sum(nvl(cost_of_removal,0))/sum(nvl(retire_qty,0)) end,2) cor_per_unit','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','retire_qty = 0 or retire_cost = 0',null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1260,'PR_VintageQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, round(sum(nvl((retire_qty*-1),0)),2) as retire_qty, round(sum(proj_retire_qty),2) as proj_retire_qty, round(sum(proj_retire_qty) - sum(nvl((retire_qty*-1),0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1261,'PR_VintageCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, round(sum(nvl((retire_cost*-1),0)),2) as retire_cost, round(sum(proj_retire_cost),2) as proj_retire_cost, round(sum(proj_retire_cost) - sum(nvl((retire_cost*-1),0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1262,'RP_VintageRets_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR,  (SUM(RETIRE_COST)*-1) RETIRE_COST, (SUM(RETIRE_QTY)*-1) RETIRE_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1263,'AP_VintageAdds_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, SUM(ADDITION_COST) ADDITION_COST, SUM(ADDITION_QTY) ADDITION_QTY','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1264,'RA_PctOverUnder_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR, SUM(END_RESERVE) END_RESERVE, SUM(END_THEO_RESERVE) END_THEO_RESERVE, case when sum(end_theo_reserve) = 0 then null else ROUND(((SUM(END_RESERVE) - SUM(END_THEO_RESERVE))/SUM(END_THEO_RESERVE))*100,2) end PCT_RESERVED','PWRANLYT.V_PA_DEPR_BALANCES_YR',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, DEPR_GROUP, GL_POSTING_YEAR','(END_RESERVE <> 0 OR END_THEO_RESERVE <> 0)',null,null,null,8);
    insert into PWRANLYT.PA_DATASOURCE values (1265,'PR_AnnualQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1266,'PR_AnnualCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, round(sum(nvl(retire_cost,0)),2) retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(nvl(retire_cost,0)),2) - sum(proj_retire_cost*-1) delta','PWRANLYT.V_PA_CPR_BALANCES',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','proj_retire_cost is not null 
    ',null,null,null,null);
    insert into PWRANLYT.PA_DATASOURCE values (1267,'PR_UnderCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES','sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)) < 0','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1268,'PR_OverCost_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, round(sum(nvl(retire_cost,0)),2) as retire_cost, round(sum(proj_retire_cost*-1),2) as proj_retire_cost, round(sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)),2) delta','PWRANLYT.V_PA_CPR_BALANCES','sum(proj_retire_cost*-1) - sum(nvl(retire_cost,0)) > 0','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1269,'PR_UnderQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRANLYT.V_PA_CPR_BALANCES','sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)) < 0','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1270,'PR_OverQty_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR, round(sum(nvl(retire_qty,0)),2) as retire_qty, round(sum(proj_retire_qty*-1),2) as proj_retire_qty, round(sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)),2) qty_delta','PWRANLYT.V_PA_CPR_BALANCES','sum(proj_retire_qty*-1) - sum(nvl(retire_qty,0)) > 0','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, PROCESS_YEAR, VINTAGE_YEAR',null,null,null,null,3);
    insert into PWRANLYT.PA_DATASOURCE values (1271,'RP_AnnualRetire_Detail_ACT','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','UPPER(FERC_ACTIVITY) = ''RETIREMENT'' AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1272,'RP_UnRetireActivity_Detail_ACT','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','ACTIVITY_COST > 0 AND UPPER(FERC_ACTIVITY)=''RETIREMENT'' AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1273,'RP_UnRetireActivityQty_Detail_ACT','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','ACTIVITY_QTY > 0 AND UPPER(FERC_ACTIVITY)=''RETIREMENT'' AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1274,'AP_AnnualAdds_Detail_ACT','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','UPPER(FERC_ACTIVITY) = ''ADDITION'' AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1275,'AP_NegativeAddsCost_Detail_ACT','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','ACTIVITY_COST < 0 AND UPPER(FERC_ACTIVITY)=''ADDITION'' AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1276,'AP_NegativeAddsQty_Detail_ACT','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR, SUM(ACTIVITY_QTY) ACTIVITY_QTY, SUM(ACTIVITY_COST) ACTIVITY_COST','PWRANLYT.V_PA_CPR_ACTIVITY',null,'COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','COMPANY, STATE, MAJOR_LOCATION, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, VINTAGE_YEAR','ACTIVITY_QTY < 0 AND UPPER(FERC_ACTIVITY)=''ADDITION'' AND RETIRE_UNIT IN (SELECT RETIRE_UNIT FROM PWRANLYT.PA_PROPERTY WHERE RETIRE_UNIT_ID > 5) ',null,null,null,5);
    insert into PWRANLYT.PA_DATASOURCE values (1277,'LA_PastMax_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, AGE_GROUP, expected_life, max_life, mort_curve_desc, YEARS_PAST_MAX, 
    ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
    AGE_GROUP, 
    expected_life, max_life, mort_curve_desc, YEARS_PAST_MAX','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
    AGE_GROUP, 
    YEARS_PAST_MAX','YEARS_PAST_MAX >= 0 ',null,null,null,6);
    insert into PWRANLYT.PA_DATASOURCE values (1278,'LA_PastExpected_Details','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, AGE_GROUP, expected_life, max_life, mort_curve_desc, 
    YEARS_PAST_EXPECTED, 
    ROUND(SUM(CURR_QTY),2) TOTAL_QUANTITY, ROUND(SUM(CURR_COST),2) TOTAL_COST, ROUND(SUM(CURR_ADJ),2) TOTAL_ESCALATED_COST','PWRANLYT.V_PA_VINTAGE_SPREADS',null,'COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
    AGE_GROUP, expected_life, max_life, mort_curve_desc, 
    YEARS_PAST_EXPECTED','COMPANY, BUS_SEGMENT, FUNC_CLASS, FERC_PLT_ACCT, UTIL_ACCT, STATE, MAJOR_LOCATION, VINTAGE_YEAR, AGE, 
    AGE_GROUP, 
    YEARS_PAST_EXPECTED','YEARS_PAST_EXPECTED > 0 AND (YEARS_PAST_MAX <=0 OR YEARS_PAST_MAX IS NULL) ',null,null,null,6);
    

    delete from PWRANLYT.PA_EXPLORATION;
    

    -- Inserts for PWRANLYT.PA_EXPLORATION
    insert into PWRANLYT.PA_EXPLORATION values (1000,1000,1008,1267,1);
    insert into PWRANLYT.PA_EXPLORATION values (1001,1001,1011,1268,1);
    insert into PWRANLYT.PA_EXPLORATION values (1003,1003,1017,1269,1);
    insert into PWRANLYT.PA_EXPLORATION values (1004,1004,1020,1270,1);
    insert into PWRANLYT.PA_EXPLORATION values (1006,1006,1029,1033,2);
    insert into PWRANLYT.PA_EXPLORATION values (1007,1007,1032,1034,2);
    insert into PWRANLYT.PA_EXPLORATION values (1008,1008,1029,1033,2);
    insert into PWRANLYT.PA_EXPLORATION values (1009,1009,1040,1047,2);
    insert into PWRANLYT.PA_EXPLORATION values (1010,1010,1043,1278,2);
    insert into PWRANLYT.PA_EXPLORATION values (1011,1011,1046,1277,2);
    insert into PWRANLYT.PA_EXPLORATION values (1012,1012,1040,1047,2);
    insert into PWRANLYT.PA_EXPLORATION values (1013,1013,1043,1278,2);
    insert into PWRANLYT.PA_EXPLORATION values (1014,1014,1046,1277,2);
    insert into PWRANLYT.PA_EXPLORATION values (1015,1015,1040,1047,2);
    insert into PWRANLYT.PA_EXPLORATION values (1016,1016,1043,1047,2);
    insert into PWRANLYT.PA_EXPLORATION values (1017,1017,1046,1047,2);
    insert into PWRANLYT.PA_EXPLORATION values (1018,1018,1055,1056,1);
    insert into PWRANLYT.PA_EXPLORATION values (1019,1019,1059,1060,1);
    insert into PWRANLYT.PA_EXPLORATION values (1020,1020,1065,1066,1);
    insert into PWRANLYT.PA_EXPLORATION values (1021,1021,1063,1064,1);
    insert into PWRANLYT.PA_EXPLORATION values (1022,1022,1069,1070,1);
    insert into PWRANLYT.PA_EXPLORATION values (1023,1023,1074,1075,1);
    insert into PWRANLYT.PA_EXPLORATION values (1025,1025,1084,1085,1);
    insert into PWRANLYT.PA_EXPLORATION values (1027,1027,1094,1095,1);
    insert into PWRANLYT.PA_EXPLORATION values (1028,1028,1099,1100,1);
    insert into PWRANLYT.PA_EXPLORATION values (1030,1030,1109,1110,2);
    insert into PWRANLYT.PA_EXPLORATION values (1032,1032,1119,1120,2);
    insert into PWRANLYT.PA_EXPLORATION values (1033,1033,1154,1155,1);
    insert into PWRANLYT.PA_EXPLORATION values (1034,1034,1127,1128,1);
    insert into PWRANLYT.PA_EXPLORATION values (1036,1036,1135,1136,1);
    insert into PWRANLYT.PA_EXPLORATION values (1037,1037,1140,1141,1);
    insert into PWRANLYT.PA_EXPLORATION values (1038,1038,1145,1146,1);
    insert into PWRANLYT.PA_EXPLORATION values (1039,1039,1150,1266,1);
    insert into PWRANLYT.PA_EXPLORATION values (1043,1043,1167,1169,1);
    insert into PWRANLYT.PA_EXPLORATION values (1044,1044,1167,1169,1);
    insert into PWRANLYT.PA_EXPLORATION values (1045,1045,1167,1169,1);
    insert into PWRANLYT.PA_EXPLORATION values (1046,1046,1167,1169,1);
    insert into PWRANLYT.PA_EXPLORATION values (1047,1047,1167,1169,1);
    insert into PWRANLYT.PA_EXPLORATION values (1048,1048,1167,1169,1);
    insert into PWRANLYT.PA_EXPLORATION values (1049,1049,1172,1173,1);
    insert into PWRANLYT.PA_EXPLORATION values (1050,1050,1176,1177,1);
    insert into PWRANLYT.PA_EXPLORATION values (1051,1051,1180,1181,1);
    insert into PWRANLYT.PA_EXPLORATION values (1052,1052,1184,1185,1);
    insert into PWRANLYT.PA_EXPLORATION values (1053,1053,1188,1189,1);
    insert into PWRANLYT.PA_EXPLORATION values (1054,1054,1192,1193,1);
    insert into PWRANLYT.PA_EXPLORATION values (1055,1055,1197,1198,1);
    insert into PWRANLYT.PA_EXPLORATION values (1056,1056,1202,1203,1);
    insert into PWRANLYT.PA_EXPLORATION values (1057,1057,1207,1208,1);
    insert into PWRANLYT.PA_EXPLORATION values (1058,1058,1210,1211,1);
    insert into PWRANLYT.PA_EXPLORATION values (1059,1059,1213,1214,1);
    insert into PWRANLYT.PA_EXPLORATION values (1060,1060,1216,1217,1);
    insert into PWRANLYT.PA_EXPLORATION values (1061,1061,1221,1222,1);
    insert into PWRANLYT.PA_EXPLORATION values (1062,1062,1244,1263,1);
    insert into PWRANLYT.PA_EXPLORATION values (1063,1063,1249,1262,1);
    insert into PWRANLYT.PA_EXPLORATION values (1064,1064,1250,1261,1);
    insert into PWRANLYT.PA_EXPLORATION values (1066,1066,1254,1260,1);
    insert into PWRANLYT.PA_EXPLORATION values (1067,1067,1240,1265,1);
    insert into PWRANLYT.PA_EXPLORATION values (1068,1068,1243,1264,1);
    insert into PWRANLYT.PA_EXPLORATION values (1069,1069,1256,1259,1);
    insert into PWRANLYT.PA_EXPLORATION values (1070,1070,1140,1141,1);
    

    delete from PWRANLYT.PA_EXPLORE_DATASOURCE;
    

    -- Inserts for PWRANLYT.PA_EXPLORE_DATASOURCE
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (1,1000,1,'Account',1006);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (2,1000,2,'Location',1007);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (3,1000,3,'Company',1008);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (4,1001,1,'Account',1009);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (5,1001,2,'Location',1010);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (6,1001,3,'Company',1011);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (7,1002,1,'Account',1012);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (8,1002,2,'Location',1013);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (9,1002,3,'Company',1014);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (10,1003,1,'Account',1015);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (11,1003,2,'Location',1016);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (12,1003,3,'Company',1017);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (13,1004,1,'Account',1018);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (14,1004,2,'Location',1019);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (15,1004,3,'Company',1020);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (16,1005,1,'Account',1021);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (17,1005,2,'Location',1022);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (18,1005,3,'Company',1023);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (19,1006,1,'Account',1027);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (20,1006,2,'Location',1028);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (21,1006,3,'Company',1029);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (22,1007,1,'Account',1030);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (23,1007,2,'Location',1031);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (24,1007,3,'Company',1032);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (25,1009,1,'Account',1038);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (26,1009,2,'Location',1039);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (27,1009,3,'Company',1040);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (28,1010,1,'Account',1041);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (29,1010,2,'Location',1042);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (30,1010,3,'Company',1043);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (31,1011,1,'Account',1044);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (32,1011,2,'Location',1045);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (33,1011,3,'Company',1046);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (34,1012,1,'Account',1038);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (35,1012,2,'Location',1039);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (36,1012,3,'Company',1040);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (37,1013,1,'Account',1041);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (38,1013,2,'Location',1042);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (39,1013,3,'Company',1043);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (40,1014,1,'Account',1044);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (41,1014,2,'Location',1045);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (42,1014,3,'Company',1046);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (43,1015,1,'Account',1038);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (44,1015,2,'Location',1039);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (45,1015,3,'Company',1040);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (46,1016,1,'Account',1041);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (47,1016,2,'Location',1042);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (48,1016,3,'Company',1043);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (49,1017,1,'Account',1044);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (50,1017,2,'Location',1045);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (51,1017,3,'Company',1046);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (52,1018,1,'Account',1053);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (53,1018,2,'Location',1054);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (54,1018,3,'Company',1055);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (55,1019,1,'Account',1057);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (56,1019,2,'Location',1058);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (57,1019,3,'Company',1059);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (60,1020,3,'Company',1065);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (61,1021,1,'Account',1061);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (62,1021,2,'Location',1062);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (63,1021,3,'Company',1063);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (64,1022,1,'Account',1067);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (65,1022,2,'Location',1068);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (66,1022,3,'Company',1069);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (67,1023,1,'Account',1072);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (68,1023,2,'Location',1073);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (69,1023,3,'Company',1074);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (70,1024,1,'Account',1077);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (71,1024,2,'Location',1078);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (72,1024,3,'Company',1079);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (73,1025,1,'Account',1082);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (74,1025,2,'Location',1083);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (75,1025,3,'Company',1084);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (76,1026,1,'Account',1087);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (77,1026,2,'Location',1088);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (78,1026,3,'Company',1089);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (79,1027,1,'Account',1092);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (80,1027,2,'Location',1093);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (81,1027,3,'Company',1094);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (82,1028,1,'Account',1097);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (83,1028,2,'Location',1098);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (84,1028,3,'Company',1099);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (85,1029,1,'Account',1102);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (86,1029,2,'Location',1103);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (87,1029,3,'Company',1104);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (88,1030,1,'Account',1107);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (89,1030,2,'Location',1108);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (90,1030,3,'Company',1109);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (91,1031,1,'Account',1112);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (92,1031,2,'Location',1113);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (93,1031,3,'Company',1114);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (94,1034,1,'Account',1125);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (95,1034,3,'Company',1127);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (96,1036,1,'Account',1133);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (97,1036,3,'Company',1135);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (98,1035,1,'Account',1129);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (99,1035,3,'Company',1131);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (100,1037,1,'Account',1138);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (101,1037,2,'Location',1139);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (102,1037,3,'Company',1140);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (103,1039,1,'Account',1148);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (104,1039,2,'Location',1149);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (105,1039,3,'Company',1150);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (106,1033,1,'Account',1152);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (107,1033,2,'Location',1153);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (108,1033,3,'Company',1154);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (109,1032,1,'Account',1117);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (110,1032,2,'Location',1118);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (111,1032,3,'Company',1119);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (112,1040,1,'Account',1157);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (113,1040,2,'Location',1158);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (114,1040,3,'Company',1159);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (115,1041,1,'Account',1157);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (116,1041,2,'Location',1158);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (117,1041,3,'Company',1159);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (118,1038,1,'Account',1143);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (119,1038,2,'Location',1144);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (120,1038,3,'Company',1145);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (121,1042,1,'Account',1162);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (122,1042,2,'Location',1163);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (123,1042,3,'Company',1164);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (124,1043,1,'Account',1168);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (125,1043,3,'Company',1167);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (126,1045,1,'Account',1168);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (127,1045,3,'Company',1167);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (128,1044,1,'Account',1168);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (129,1044,3,'Company',1167);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (130,1046,1,'Account',1168);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (131,1046,3,'Company',1167);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (132,1049,1,'Account',1171);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (133,1049,3,'Company',1172);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (134,1050,1,'Account',1175);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (135,1050,3,'Company',1176);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (136,1051,1,'Account',1179);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (137,1051,3,'Company',1180);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (138,1052,1,'Account',1183);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (139,1052,3,'Company',1184);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (140,1047,1,'Account',1168);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (141,1047,3,'Company',1167);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (142,1048,1,'Account',1168);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (143,1048,3,'Company',1167);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (144,1053,1,'Account',1187);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (145,1053,3,'Company',1188);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (146,1054,1,'Account',1191);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (147,1054,3,'Company',1192);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (148,1055,1,'Account',1195);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (149,1055,2,'Location',1196);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (150,1055,3,'Company',1197);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (151,1056,1,'Account',1200);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (152,1056,2,'Location',1201);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (153,1056,3,'Company',1202);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (154,1057,1,'Account',1205);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (155,1057,2,'Location',1206);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (156,1057,3,'Company',1207);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (157,1058,1,'Account',1210);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (158,1059,1,'Account',1213);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (159,1060,3,'Company',1216);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (160,1061,1,'Account',1219);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (161,1061,2,'Location',1220);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (162,1061,3,'Company',1221);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (163,1060,1,'Account',1231);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (164,1060,2,'Location',1232);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (165,1058,2,'Location',1234);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (166,1058,3,'Company',1233);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (167,1059,2,'Location',1235);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (168,1059,3,'Company',1236);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (169,1020,2,'Location',1237);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (170,1020,1,'Account',1238);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (171,1067,1,'Account',1239);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (172,1067,2,'Location',1241);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (173,1067,3,'Company',1240);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (174,1070,1,'Account',1138);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (175,1070,2,'Location',1139);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (176,1070,3,'Company',1140);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (177,1068,1,'Account',1242);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (178,1068,3,'Company',1243);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (179,1008,1,'Account',1027);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (180,1008,2,'Location',1028);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (181,1008,3,'Company',1029);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (182,1062,1,'Account',1246);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (183,1062,2,'Location',1245);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (184,1062,3,'Company',1244);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (185,1063,1,'Account',1247);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (186,1063,2,'Location',1248);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (187,1063,3,'Company',1249);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (188,1064,1,'Account',1252);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (189,1064,2,'Location',1251);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (190,1064,3,'Company',1250);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (191,1066,1,'Account',1253);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (192,1066,2,'Location',1255);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (193,1066,3,'Company',1254);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (194,1069,1,'Account',1257);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (195,1069,2,'Location',1258);
    insert into PWRANLYT.PA_EXPLORE_DATASOURCE values (196,1069,3,'Company',1256);
    

    delete from PWRANLYT.PA_EXPLORE_GRIDFILTERS;
    

    -- Inserts for PA_EXPLORE_GRIDFILTERS
    insert into PWRANLYT.PA_EXPLORE_GRIDFILTERS values (1,2);
    insert into PWRANLYT.PA_EXPLORE_GRIDFILTERS values (1,7);
    insert into PWRANLYT.PA_EXPLORE_GRIDFILTERS values (1,8);
    insert into PWRANLYT.PA_EXPLORE_GRIDFILTERS values (1,13);
    insert into PWRANLYT.PA_EXPLORE_GRIDFILTERS values (2,9);
    insert into PWRANLYT.PA_EXPLORE_GRIDFILTERS values (2,12);
    insert into PWRANLYT.PA_EXPLORE_GRIDFILTERS values (3,3);
    insert into PWRANLYT.PA_EXPLORE_GRIDFILTERS values (4,4);
    

    delete from PWRANLYT.PA_CASE_STATUSES;
   

    -- Inserts for PA_CASE_STATUSES
    insert into PWRANLYT.PA_CASE_STATUSES values (1,'Submitted',1);
    insert into PWRANLYT.PA_CASE_STATUSES values (2,'In Progress',2);
    insert into PWRANLYT.PA_CASE_STATUSES values (3,'Completed',3);
    commit;

    delete from PWRANLYT.PA_FILTER2;
    

    -- Inserts for PA_FILTER2
    insert into PWRANLYT.PA_FILTER2 values (1,'ActivityCode','ACTIVITY_CODE','ActivityCode','Activity Code','select distinct activity_code from pwranlyt.v_pa_cpr_actsum order by 1','ACTIVITY_CODE','ACTIVITY_CODE','MULTISELECT','IN',0,1,'Misc',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (2,'BusinessSegment','BUS_SEGMENT','BusinessSegment','Business Segment','select distinct Bus_Segment from PWRANLYT.V_PA_ACCOUNTS t order by 1','BUS_SEGMENT','BUS_SEGMENT','MULTISELECT','IN',0,1,'Account',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (3,'Company','COMPANY','Company','Company','select distinct Description from PWRANLYT.V_PA_COMPANY order by 1','Description','Description','MULTISELECT','IN',0,1,'Company',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (4,'DeprGroup','DEPR_GROUP','DeprGroup','Depr Group','select distinct depr_group from pwranlyt.v_pa_depr_group order by 1','DEPR_GROUP','DEPR_GROUP','TEXT','LIKE',0,1,'Depreciation',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (5,'DeprMethod','DEPR_METHOD','DeprMethod','Depr Method','select distinct depr_method from pwranlyt.v_pa_cpr_actsum order by 1','DEPR_METHOD','DEPR_METHOD','TEXT','LIKE',0,1,'Depreciation',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (6,'FercActivity','FERC_ACTIVITY','FercActivity','FERC Activity','select distinct ferc_activity from pwranlyt.v_pa_cpr_actsum order by 1','FERC_ACTIVITY','FERC_ACTIVITY','MULTISELECT','IN',0,1,'Misc',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (7,'SummaryAccount','FERC_PLT_ACCT','SummaryAccount','Summary Account','select distinct FERC_PLT_ACCT from PWRANLYT.V_PA_ACCOUNTS t order by 1','FERC_PLT_ACCT','FERC_PLT_ACCT','MULTISELECT','IN',0,1,'Account',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (8,'FunctionalClass','FUNC_CLASS','FunctionalClass','Functional Class','select distinct FUNC_CLASS from PWRANLYT.V_PA_ACCOUNTS t order by 1','FUNC_CLASS','FUNC_CLASS','MULTISELECT','IN',0,1,'Account',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (9,'MajorLocation','MAJOR_LOCATION','MajorLocation','Major Location','select distinct major_location from pwranlyt.v_pa_locations order by 1','MAJOR_LOCATION','MAJOR_LOCATION','MULTISELECT','IN',0,1,'Location',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (10,'PostingYear','GL_POSTING_YEAR','PostingYears','Posting Years','select distinct gl_posting_year from pwranlyt.v_pa_cpr_actsum where GL_POSTING_YEAR is not null order by 1','GL_POSTING_YEAR','GL_POSTING_YEAR','SLIDER','BETWEEN',0,0,'Time',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (11,'ProcessYears','PROCESS_YEAR','ProcessYears','Process Years','select distinct PROCESS_YEAR from PWRANLYT.V_PA_AVERAGE_AGES order by 1','PROCESS_YEAR','PROCESS_YEAR','SLIDER','BETWEEN',0,0,'Time',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (12,'State','STATE','State','State','select distinct STATE from PWRANLYT.V_PA_LOCATIONS order by 1','State','STATE','MULTISELECT','IN',0,1,'Location',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (13,'DetailAccount','UTIL_ACCT','DetailAccount','Detail Account','select distinct UTIL_ACCT from PWRANLYT.V_PA_ACCOUNTS t order by 1','UTIL_ACCT','UTIL_ACCT','MULTISELECT','IN',0,1,'Account',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (14,'VintageYear','VINTAGE_YEAR','VintageYear','Vintage Years','select distinct VINTAGE_YEAR from PWRANLYT.V_PA_VINTAGE_SPREADS t order by 1','VINTAGE_YEAR','VINTAGE_YEAR','SLIDER','BETWEEN',0,0,'Time',0,null,1,null,null);
    insert into PWRANLYT.PA_FILTER2 values (15,'VintageInterval','VINTAGE_INTERVAL','VintageInterval','Vintage Interval','select 1 Interval from dual union select 2 Interval from dual union select 3 Interval from dual union select 5 Interval from dual union select 10 Interval from dual union select 20 Interval from dual','Interval','Interval','DROPDOWNLIST','PARAM_REPLACE',1,0,'Time',1,'(VINTAGE_YEAR / [X] ) * [X]',1,'FLOOR(VINTAGE_YEAR/{VintageInterval})*{VintageInterval}','5');
    insert into PWRANLYT.PA_FILTER2 values (16,'Age','Age','Age','Age','select 1 from dual','Age','Age','MULTISELECT','IN',0,0,'Time',0,null,0,null,null);
    insert into PWRANLYT.PA_FILTER2 values (17,'YearsPastExpected','YEARS_PAST_EXPECTED','YearsPastExpected','Years Past Expected','select 1 from dual','YearsPastExpected','YearsPastExpected','MULTISELECT','IN',0,0,'Time',0,null,0,null,null);
    insert into PWRANLYT.PA_FILTER2 values (18,'YearsPastMax','YEARS_PAST_MAX','YearsPastMax','Years Past Max','select 1 from dual','YearsPastMax','YearsPastMax','MULTISELECT','IN',0,0,'Time',0,null,0,null,null);
    insert into PWRANLYT.PA_FILTER2 values (19,'LifeBand','LIFE_BAND','LifeBand','Life Band','select 1 from dual','LifeBand','LifeBand','MULTISELECT','IN',0,0,'Time',0,null,0,null,null);
    insert into PWRANLYT.PA_FILTER2 values (20,'PostingDate','POSTING_YRMO','PostingDate','Posting Date','select distinct posting_yrmo from PWRANLYT.V_PA_DEPR_BALANCES','POSTING_YRMO','POSTING_YRMO','MULTISELECT','IN',0,1,'Time',0,null,1,null,null);
    

    delete from PWRANLYT.PA_FILTERGROUP;
    

    -- Inserts for PA_FILTERGROUP
    insert into PWRANLYT.PA_FILTERGROUP values (1,'AverageAge');
    insert into PWRANLYT.PA_FILTERGROUP values (2,'VintageSpread');
    insert into PWRANLYT.PA_FILTERGROUP values (3,'CprBalances');
    insert into PWRANLYT.PA_FILTERGROUP values (4,'CprActSummary');
    insert into PWRANLYT.PA_FILTERGROUP values (5,'CprActivity');
    insert into PWRANLYT.PA_FILTERGROUP values (6,'CprLedger');
    insert into PWRANLYT.PA_FILTERGROUP values (7,'DeprBalances');
    insert into PWRANLYT.PA_FILTERGROUP values (8,'DeprBalancesYr');
    

    delete from PWRANLYT.PA_FILTERGROUP_FILTERS;
    

    -- Inserts for PWRANLYT.PA_FILTERGROUP_FILTERS
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (1,3,2,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (2,3,3,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (3,3,4,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (4,3,7,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (5,3,8,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (6,3,9,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (7,3,11,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (8,3,12,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (9,3,13,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (10,3,14,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (11,2,2,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (12,2,3,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (13,2,4,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (14,2,7,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (15,2,8,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (16,2,9,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (17,2,12,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (18,2,13,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (19,2,14,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (20,2,15,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (21,6,2,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (22,6,3,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (23,6,4,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (24,6,5,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (25,6,7,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (26,6,8,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (27,6,9,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (28,6,12,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (29,6,13,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (30,6,14,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (31,6,15,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (32,7,2,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (33,7,3,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (34,7,4,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (35,7,7,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (36,7,8,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (37,7,10,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (38,7,13,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (39,8,2,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (40,8,3,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (41,8,4,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (42,8,7,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (43,8,8,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (44,8,10,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (45,8,13,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (46,5,2,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (47,5,3,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (48,5,4,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (49,5,5,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (50,5,7,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (51,5,8,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (52,5,9,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (53,5,10,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (54,5,12,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (55,5,13,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (56,5,14,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (57,5,15,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (58,7,2,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (59,7,3,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (60,7,4,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (61,7,7,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (62,7,8,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (63,7,10,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (64,7,13,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (65,7,20,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (66,1,3,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (67,1,2,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (68,1,8,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (69,1,7,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (70,1,13,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (71,1,4,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (72,1,12,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (73,1,9,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (74,1,11,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (75,4,3,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (76,4,2,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (77,4,8,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (78,4,7,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (79,4,13,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (80,4,12,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (81,4,9,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (82,4,4,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (83,4,10,null);
    insert into PWRANLYT.PA_FILTERGROUP_FILTERS (Id,FilterGroupId,FilterId,AllowedAsXAxis) values (84,3,15,null);
   

    delete from PWRANLYT.PA_CASE_PRIORITIES;
    

    -- Inserts for PA_CASE_PRIORITIES
    insert into PWRANLYT.PA_CASE_PRIORITIES values ('High',3,3);
    insert into PWRANLYT.PA_CASE_PRIORITIES values ('Low',1,1);
    insert into PWRANLYT.PA_CASE_PRIORITIES values ('Medium',2,2);
    

    delete from PWRANLYT.PA_HEATMAP_COLOR;
    

    -- Inserts for PA_HEATMAP_COLOR
    insert into PWRANLYT.PA_HEATMAP_COLOR values (6,3,'#FFFFFF','Ok',-100,0,52);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (7,3,'#FFCC33',null,0,100,52);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (8,35,'#FFFFFF','Ok',-20,0,54);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (9,35,'#FFFF2B','Ok',0,20,54);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (10,-38,'#FF342D','Reversed',-100,0,55);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (11,-38,'#FFFF2B',null,0,100000000,55);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (12,-39,'#FFFFFF','Ok',-100,0,56);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (13,-39,'#FFFF2B','Ok',0,9999999,56);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (14,1002,'#FFFFFF','Ok',-100,0,1002);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (15,1002,'#FFCC33',null,0,100,1002);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (16,1024,'#FFFFFF','Medium',-20,0,1030);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (17,1026,'#FFFFFF','Medium',-20,0,1030);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (18,1029,'#66CC66','Medium',0,20,1035);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (19,1031,'#66CC66','Medium',0,20,1034);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (20,1035,'#FFFFFF','Ok',-20,0,1042);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (21,1035,'#FFFF2B','Ok',0,20,1042);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (22,1005,'#FFFFFF','Ok',-100,0,1049);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (23,1005,'#FFCC33',null,0,100,1049);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (24,1042,'#FFFFFF','Ok',-100,0,1054);
    insert into PWRANLYT.PA_HEATMAP_COLOR values (25,1042,'#FFCC33',null,0,100,1054);
    
    
	
	end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1572, 0, 10, 4, 3, 0, 40318, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040318_anlyt_07_system_inserts.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;