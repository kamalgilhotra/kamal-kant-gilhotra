/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050291_lessor_01_update_v_currency_rate_default_dense_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| --------   ---------- ---------------- --------------------------------------
|| 2017.2.0.0 01/17/2018 Andrew Hill      Include Lessor tables in date search
||============================================================================
*/

CREATE OR REPLACE VIEW V_CURRENCY_RATE_DEFAULT_DENSE AS
WITH min_months AS (SELECT MIN(TRUNC(month, 'MONTH')) AS min_month
                    FROM ls_asset_schedule
                    UNION
                    SELECT MIN(TRUNC(month, 'MONTH')) AS min_month
                    FROM ls_ilr_schedule
                    UNION
                    SELECT MIN(TRUNC(MONTH, 'MONTH')) AS min_month
                    from lsr_ilr_schedule
                    UNION
                    SELECT  MIN(TRUNC(exchange_date, 'MONTH')) AS min_month
                    FROM currency_rate_default
),
max_months AS (SELECT MAX(TRUNC(month, 'MONTH')) AS max_month
                    FROM ls_asset_schedule
                    UNION
                    SELECT MAX(TRUNC(Month, 'MONTH')) AS max_month
                    FROM ls_ilr_schedule
                    UNION
                    SELECT MAX(TRUNC(MONTH, 'MONTH')) AS max_month
                    FROM lsr_ilr_schedule
                    UNION
                    SELECT  MAX(TRUNC(exchange_date, 'MONTH')) AS max_month
                    FROM currency_rate_default
),
date_range AS (SELECT MAX(max_month) AS max_month, MIN(min_month) AS min_month, months_between(MAX(max_month), MIN(min_month)) AS diff
                FROM min_months
                CROSS JOIN max_months
                )
SELECT  MONTH AS exchange_date,
        currency_from,
        currency_to,
        exchange_rate_type_id,
        LAST_VALUE(rate IGNORE NULLS) OVER (PARTITION BY  exchange_rate_type_id,
                                                          currency_from,
                                                          currency_to
                                            ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS rate
FROM (SELECT  dates.month,
              crd.currency_from,
              crd.currency_to,
              crd.exchange_rate_type_id,
              crd.rate
    FROM (SELECT ADD_MONTHS(min_month, LEVEL - 1) AS MONTH
          FROM date_range
          CONNECT BY LEVEL <= diff + 1) dates
    LEFT OUTER JOIN (SELECT exchange_date,
                            currency_from,
                            currency_to,
                            exchange_rate_type_id,
                            rate
                      FROM (SELECT  exchange_date,
                                    currency_from,
                                    currency_to,
                                    exchange_rate_type_id,
                                    rate,
                                    ROW_NUMBER() OVER (PARTITION BY currency_from,
                                                                    currency_to,
                                                                    exchange_rate_type_id,
                                                                    TRUNC(exchange_date, 'MONTH')
                                                       ORDER BY exchange_date DESC) rn
                            FROM currency_rate_default crd)
                     WHERE rn = 1) crd 
      PARTITION BY (currency_from, currency_to, exchange_rate_type_id) 
      ON TRUNC(crd.exchange_date, 'MONTH') = dates.MONTH);

	  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4081, 0, 2017, 3, 0, 0, 50291, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050291_lessor_01_update_v_currency_rate_default_dense_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
