/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038593_lease_PKG_LEASE_IMPORT.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 06/17/2014 Shane Ward          Adding Component and II Rate Import
||============================================================================
*/

CREATE OR REPLACE Package pkg_lease_import As
	/*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_IMPORT
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 08/19/2013 K.Peterson     Original Version
   ||============================================================================
   */
	Type num_array Is Table Of Number(22) Index By Binary_Integer;

	Procedure p_invoice_rollup;

	Procedure p_buildilrs(a_ilr_ids   num_array,
								 a_revisions num_array,
								 a_send_jes  Number);

	Function f_import_invoices(a_run_id Number) Return Varchar2;

	Function f_import_lessors(a_run_id Number) Return Varchar2;

	Function f_validate_invoices(a_run_id Number) Return Varchar2;

	Function f_validate_lessors(a_run_id Number) Return Varchar2;

	Function f_import_ilrs(a_run_id Number) Return Varchar2;

	Function f_validate_ilrs(a_run_id Number) Return Varchar2;

	Function f_import_mlas(a_run_id Number) Return Varchar2;

	Function f_validate_mlas(a_run_id Number) Return Varchar2;

	Function f_import_assets(a_run_id Number) Return Varchar2;

	Function f_validate_assets(a_run_id Number) Return Varchar2;

   Function f_import_components(a_run_id Number) Return Varchar2;

   FUNCTION f_validate_components(a_run_id NUMBER) RETURN VARCHAR2;

   FUNCTION f_import_lease_rates(a_run_id NUMBER) RETURN VARCHAR2;

   FUNCTION f_validate_lease_rates(a_run_id NUMBER) RETURN VARCHAR2;

End pkg_lease_import;
/

CREATE OR REPLACE Package Body pkg_lease_import As
	/*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_IMPORT
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 08/19/2013 K.Peterson    Original Version
   ||============================================================================
   */

	--DECLARE VARIABLES HERE
	Type invoice_import_type Is Table Of ls_import_invoice%Rowtype Index By Pls_Integer;
	Type invoice_import_hdr Is Record(
		vendor_id        ls_import_invoice.vendor_id%Type,
		company_id       ls_import_invoice.company_id%Type,
		lease_id         ls_import_invoice.lease_id%Type,
		gl_posting_mo_yr ls_invoice.gl_posting_mo_yr%Type,
		invoice_number   ls_invoice.invoice_number%Type);
	Type invoice_import_line Is Record(
		invoice_id          ls_invoice.invoice_id%Type,
		invoice_line_number ls_invoice_line.invoice_line_number%Type,
		payment_type_id     ls_import_invoice.payment_type_id%Type,
		gl_posting_mo_yr    ls_invoice_line.gl_posting_mo_yr%Type,
		ls_asset_id         ls_import_invoice.ls_asset_id%Type,
		amount              ls_import_invoice.amount%Type);
	Type invoice_import_hdr_table Is Table Of invoice_import_hdr Index By Pls_Integer;
	Type invoice_line_table Is Table Of invoice_import_line Index By Pls_Integer;
	Type asset_import_table Is Table Of ls_import_asset%Rowtype Index By Pls_Integer;
	Type component_import_table Is Table Of ls_import_component%Rowtype Index By Pls_Integer;
  TYPE interest_import_table IS TABLE OF ls_import_lease_interim_rates%ROWTYPE INDEX BY PLS_INTEGER;
	Type mla_company_line Is Record(
		lease_id       ls_lease_company.lease_id%Type,
		company_id     ls_lease_company.company_id%Type,
		max_lease_line ls_lease_company.max_lease_line%Type,
		vendor_id      ls_lease_vendor.vendor_id%Type,
		payment_pct    ls_lease_vendor.payment_pct%Type);
	Type mla_company_table Is Table Of mla_company_line Index By Pls_Integer;

	--**************************************************************************
	--                            Start Body
	--**************************************************************************
	--**************************************************************************
	--                            PROCEDURES
	--**************************************************************************
	Procedure p_invoice_rollup Is
		l_index Number;
		Type invoice_lines_table Is Table Of ls_invoice_line%Rowtype Index By Pls_Integer;
		invoice_lines invoice_lines_table;
	Begin
		Update ls_invoice
			Set invoice_amount     = 0,
				 invoice_interest   = 0,
				 invoice_executory  = 0,
				 invoice_contingent = 0;

		Select * Bulk Collect Into invoice_lines From ls_invoice_line;

		Forall l_index In Indices Of invoice_lines
			Update ls_invoice
				Set invoice_amount = invoice_amount + invoice_lines(l_index).amount
			 Where invoice_id = invoice_lines(l_index).invoice_id;

		Forall l_index In Indices Of invoice_lines
			Update ls_invoice
				Set invoice_interest = invoice_interest + invoice_lines(l_index).amount
			 Where invoice_id = invoice_lines(l_index).invoice_id
				And invoice_lines(l_index).payment_type_id In (2, 5);

		Forall l_index In Indices Of invoice_lines
			Update ls_invoice
				Set invoice_executory = invoice_executory + invoice_lines(l_index).amount
			 Where invoice_id = invoice_lines(l_index).invoice_id
				And invoice_lines(l_index).payment_type_id In (3, 6);

		Forall l_index In Indices Of invoice_lines
			Update ls_invoice
				Set invoice_contingent = invoice_contingent + invoice_lines(l_index).amount
			 Where invoice_id = invoice_lines(l_index).invoice_id
				And invoice_lines(l_index).payment_type_id In (4, 7);

	End p_invoice_rollup;

	--**************************************************************************
	--                            P_BUILDILRS
	--**************************************************************************
	--
	Procedure p_buildilrs(a_ilr_ids   num_array,
								 a_revisions num_array,
								 a_send_jes  Number) Is
		rtn      Varchar2(4000);
		rtn_num  Number(22, 0);
		l_status Varchar2(2000);

	Begin

		--Turn off the JEs if we don't want them
		If a_send_jes = 0 Then
			pkg_lease_asset_post.g_send_jes := False;
		End If;

		rtn := pkg_lease_schedule.f_mass_process_ilrs(a_ilr_ids, a_revisions);
		If rtn <> 'OK' Then
			Return;
		End If;

		rtn := pkg_lease_schedule.f_process_assets;
		If rtn <> 'OK' Then
			Return;
		End If;

		rtn := pkg_lease_schedule.f_save_schedules;
		If rtn <> 'OK' Then
			Return;
		End If;

		--APPROVE HERE
		For i In 1 .. a_ilr_ids.count
		Loop

			l_status := 'Sending ILR for approval';
			rtn_num  := pkg_lease_calc.f_send_ilr_no_commit(a_ilr_ids(i), a_revisions(i), l_status);
			If rtn_num <> 1 Then
				Rollback;
				raise_application_error(-20000, 'Error Sending: ' || l_status);
				Return;
			End If;

			l_status := 'Auto Approving ILR';
			rtn_num  := pkg_lease_calc.f_approve_ilr_no_commit(a_ilr_ids(i), a_revisions(i), l_status,
																				False);
			If rtn_num <> 1 Then
				Rollback;
				raise_application_error(-20000, 'Error Approving: ' || l_status);
				Return;
			End If;

			Update workflow
				Set date_sent     = Sysdate,
					 date_approved = Sysdate
			 Where id_field1 = a_ilr_ids(i)
				And id_field2 = a_revisions(i)
				And subsystem = 'ilr_approval';

		End Loop;

		Commit;

		--Turn the JEs back on
		pkg_lease_asset_post.g_send_jes := True;

	End p_buildilrs;

	--**************************************************************************
	--                            FUNCTIONS
	--**************************************************************************
	--##########################################################################
	--                            INVOICES
	--##########################################################################
	Function f_import_invoices(a_run_id In Number) Return Varchar2 Is
		l_invoice_line             invoice_line_table;
		l_index                    Number;
		l_invoice_import_hdr_table invoice_import_hdr_table;
		l_msg                      Varchar2(2000);
	Begin
		l_msg := 'Collecting for Invoice headers';
		Select Distinct vendor_id,
							 company_id,
							 lease_id,
							 to_date(gl_posting_mo_yr, 'yyyymm'),
							 invoice_number Bulk Collect
		  Into l_invoice_import_hdr_table
		  From ls_import_invoice
		 Where import_run_id = a_run_id;

		l_msg := 'Inserting dummy invoices into LS_INVOICE';
		Forall l_index In Indices Of l_invoice_import_hdr_table
			Insert Into ls_invoice
				(invoice_id,
				 company_id,
				 lease_id,
				 gl_posting_mo_yr,
				 vendor_id,
				 invoice_number)
				Select ls_invoice_seq.nextval,
						 l_invoice_import_hdr_table(l_index).company_id,
						 l_invoice_import_hdr_table(l_index).lease_id,
						 l_invoice_import_hdr_table(l_index).gl_posting_mo_yr,
						 l_invoice_import_hdr_table(l_index).vendor_id,
						 l_invoice_import_hdr_table(l_index).invoice_number
				  From dual;

		l_msg := 'Collecting for Invoice lines';
		Select li.invoice_id,
				 row_number() over(Partition By to_date(lii.gl_posting_mo_yr, 'yyyymm'), lii.vendor_id, lii.company_id, lii.lease_id Order By to_date(lii.gl_posting_mo_yr, 'yyyymm')),
				 lii.payment_type_id,
				 to_date(lii.gl_posting_mo_yr, 'yyyymm'),
				 lii.ls_asset_id,
				 lii.amount Bulk Collect
		  Into l_invoice_line
		  From ls_import_invoice lii, ls_invoice li
		 Where lii.import_run_id = a_run_id
			And lii.vendor_id = li.vendor_id
			And lii.company_id = li.company_id
			And to_date(lii.gl_posting_mo_yr, 'yyyymm') = li.gl_posting_mo_yr
			And lii.lease_id = li.lease_id;

		l_msg := 'Inserting into LS_INVOICE_LINE';
		Forall l_index In Indices Of l_invoice_line
			Insert Into ls_invoice_line
				(invoice_id,
				 invoice_line_number,
				 payment_type_id,
				 ls_asset_id,
				 gl_posting_mo_yr,
				 amount)
				Select l_invoice_line(l_index).invoice_id,
						 l_invoice_line(l_index).invoice_line_number,
						 l_invoice_line(l_index).payment_type_id,
						 l_invoice_line(l_index).ls_asset_id,
						 l_invoice_line(l_index).gl_posting_mo_yr,
						 l_invoice_line(l_index).amount
				  From dual;

		p_invoice_rollup;

		Return 'OK';
	Exception
		When Others Then
			l_msg := substr(l_msg || ': ' || Sqlerrm, 1, 2000);
			Return l_msg;
	End f_import_invoices;

	Function f_validate_invoices(a_run_id In Number) Return Varchar2 Is
		l_index                    Number;
		l_invoice_import_hdr_table invoice_import_hdr_table;
		l_msg                      Varchar2(2000);
		num_updated                Number;
	Begin
		l_msg := 'Collecting unique vendor/company/lease/month combinations';
		Select Distinct vendor_id,
							 company_id,
							 lease_id,
							 to_date(gl_posting_mo_yr, 'yyyymm'),
							 invoice_number Bulk Collect
		  Into l_invoice_import_hdr_table
		  From ls_import_invoice
		 Where import_run_id = a_run_id;

		l_msg := 'Updating ls_import_invoices with errors';
		Forall l_index In Indices Of l_invoice_import_hdr_table
			Update ls_import_invoice
				Set error_message = 'Invoice already exists for this company/month/vendor/lease combination.'
			 Where Exists
			 (Select 1
						 From ls_invoice
						Where gl_posting_mo_yr = l_invoice_import_hdr_table(l_index).gl_posting_mo_yr
						  And company_id = l_invoice_import_hdr_table(l_index).company_id
						  And lease_id = l_invoice_import_hdr_table(l_index).lease_id
						  And vendor_id = l_invoice_import_hdr_table(l_index).vendor_id)
				And to_date(gl_posting_mo_yr, 'yyyymm') = l_invoice_import_hdr_table(l_index)
					.gl_posting_mo_yr
				And company_id = l_invoice_import_hdr_table(l_index).company_id
				And lease_id = l_invoice_import_hdr_table(l_index).lease_id
				And vendor_id = l_invoice_import_hdr_table(l_index).vendor_id;
		--num_updated := sql%rowcount;

		Update ls_import_invoice a
			Set error_message = error_message ||
									  ' Invoice Number must be the same for this company/month/vendor/lease combination.'
		 Where import_run_id = a_run_id
			And Exists (Select 1
					 From ls_import_invoice b
					Where b.gl_posting_mo_yr = a.gl_posting_mo_yr
					  And b.company_id = a.company_id
					  And b.lease_id = a.lease_id
					  And b.vendor_id = a.vendor_id
					  And nvl(b.invoice_number, '*~*~INVALIDNUMBER~*~*') <>
							nvl(a.invoice_number, '*~*~INVALIDNUMBER~*~*'));

		Update ls_import_invoice a
			Set error_message = error_message ||
									  ' There is already an invoice with this invoice number.'
		 Where import_run_id = a_run_id
			And Exists (Select 1 From ls_invoice b Where b.invoice_number = a.invoice_number);

		Update ls_import_invoice
			Set error_message = error_message || ' All invoice amounts must be positive.'
		 Where import_run_id = a_run_id
			And amount < 0;

		Return 'OK';
	Exception
		When Others Then
			l_msg := substr(l_msg || ': ' || Sqlerrm, 1, 2000);
			Return l_msg;
	End f_validate_invoices;
	--##########################################################################
	--                            LESSORS
	--##########################################################################
	Function f_import_lessors(a_run_id In Number) Return Varchar2 Is
		Type lessor_import_table Is Table Of ls_import_lessor%Rowtype Index By Pls_Integer;
		l_lessor_import lessor_import_table;
		l_index         Number;
		l_msg           Varchar2(2000);
	Begin

		--here we're populating our PK for the lessors. If the previous row has a different unique identifier than the current
		--row we give it a new id, else we give it the previous row's lessor_id but we make it negative so we don't attempt to insert
		--on that row
		Update ls_import_lessor
			Set lessor_id = ls_lessor_seq.nextval
		 Where line_id In (Select line_id
									From (Select line_id,
													 row_number() over(Partition By unique_lessor_identifier Order By unique_lessor_identifier, line_id) num
											  From ls_import_lessor
											 Where import_run_id = a_run_id)
								  Where num = 1)
			And import_run_id = a_run_id;

		Update ls_import_lessor a
			Set a.lessor_id =
				 (Select -b.lessor_id
					 From ls_import_lessor b
					Where a.unique_lessor_identifier = b.unique_lessor_identifier
					  And b.lessor_id Is Not Null
					  And import_run_id = a_run_id)
		 Where a.lessor_id Is Null
			And import_run_id = a_run_id;

		l_msg := 'Collecting for lessor insert';
		Select * Bulk Collect
		  Into l_lessor_import
		  From ls_import_lessor
		 Where import_run_id = a_run_id
			And nvl(lessor_id, -1) > 0;

		l_msg := 'Inserting Lessors';
		Forall l_index In Indices Of l_lessor_import
			Insert Into ls_lessor
				(lessor_id,
				 description,
				 long_description,
				 address1,
				 address2,
				 address3,
				 address4,
				 zip,
				 postal,
				 city,
				 county_id,
				 state_id,
				 country_id,
				 phone_number,
				 extension,
				 fax_number,
				 external_lessor_number,
				 site_code,
				 status_code_id,
				 multi_vendor_yn_sw,
				 lease_group_id)
				(Select l_lessor_import(l_index).lessor_id,
						  l_lessor_import(l_index).description,
						  l_lessor_import(l_index).long_description,
						  l_lessor_import(l_index).address1,
						  l_lessor_import(l_index).address2,
						  l_lessor_import(l_index).address3,
						  l_lessor_import(l_index).address4,
						  l_lessor_import(l_index).zip,
						  l_lessor_import(l_index).postal,
						  l_lessor_import(l_index).city,
						  l_lessor_import(l_index).county_id,
						  l_lessor_import(l_index).state_id,
						  l_lessor_import(l_index).country_id,
						  l_lessor_import(l_index).phone_number,
						  l_lessor_import(l_index).extension,
						  l_lessor_import(l_index).fax_number,
						  l_lessor_import(l_index).external_lessor_number,
						  l_lessor_import(l_index).site_code,
						  1,
						  l_lessor_import(l_index).multi_vendor_yn_sw,
						  l_lessor_import(l_index).lease_group_id
					From dual);

		Update ls_import_lessor Set vendor_id = ls_vendor_seq.nextval Where import_run_id = a_run_id;

		Select * Bulk Collect
		  Into l_lessor_import
		  From ls_import_lessor
		 Where import_run_id = a_run_id;

		Forall l_index In Indices Of l_lessor_import
			Insert Into ls_vendor
				(vendor_id,
				 lessor_id,
				 description,
				 long_description,
				 external_vendor_number,
				 remit_addr,
				 status_code_id,
				 primary)
				Select l_lessor_import(l_index).vendor_id,
						 abs(l_lessor_import(l_index).lessor_id),
						 l_lessor_import(l_index).vendor_description,
						 l_lessor_import(l_index).vendor_long_description,
						 l_lessor_import(l_index).external_vendor_number,
						 l_lessor_import(l_index).remit_addr,
						 1,
						 l_lessor_import(l_index).primary
				  From dual;

		Return 'OK';
	Exception
		When Others Then
			l_msg := substr(l_msg || ': ' || Sqlerrm, 1, 2000);
			Return l_msg;
	End f_import_lessors;

	Function f_validate_lessors(a_run_id In Number) Return Varchar2 Is
		l_msg Varchar2(2000);
	Begin
		--check for duplicate lessor descriptions
		Update ls_import_lessor
			Set error_message = error_message || ' Duplicate lessor description.'
		 Where import_run_id = a_run_id
			And description In
				 (Select description
					 From (Select description, Count(*) As my_count
								From (Select description
										  From ls_lessor
										Union All
										Select description
										  From (Select Distinct unique_lessor_identifier, description
													 From ls_import_lessor
													Where import_run_id = a_run_id))
							  Group By description)
					Where my_count > 1);

		--check for duplicate external_lessor_number
		Update ls_import_lessor
			Set error_message = error_message || ' Duplicate external lessor number.'
		 Where import_run_id = a_run_id
			And external_lessor_number In
				 (Select external_lessor_number
					 From (Select external_lessor_number, Count(*) As my_count
								From (Select external_lessor_number
										  From ls_lessor
										Union All
										Select external_lessor_number
										  From (Select Distinct unique_lessor_identifier, external_lessor_number
													 From ls_import_lessor
													Where import_run_id = a_run_id))
							  Group By external_lessor_number)
					Where my_count > 1);

		--check for duplicate vendor_descriptions
		Update ls_import_lessor
			Set error_message = error_message || ' Duplicate vendor description.'
		 Where import_run_id = a_run_id
			And vendor_description In (Select description As vendor_description
												  From (Select description, Count(*) As my_count
															 From (Select description
																		From ls_vendor
																	 Union All
																	 Select vendor_description As description
																		From ls_import_lessor
																	  Where import_run_id = a_run_id)
															Group By description)
												 Where my_count > 1);

		--check that only one vendor is primary
		Update ls_import_lessor
			Set error_message = error_message || ' Exactly one vendor must be primary for each lessor.'
		 Where import_run_id = a_run_id
			And unique_lessor_identifier In (Select unique_lessor_identifier
														  From (Select unique_lessor_identifier, Sum(primary) sums
																	 From ls_import_lessor
																	Where import_run_id = a_run_id
																	Group By unique_lessor_identifier)
														 Where sums <> 1);

		Return 'OK';
	Exception
		When Others Then
			l_msg := substr(l_msg || ': ' || Sqlerrm, 1, 2000);
			Return l_msg;
	End f_validate_lessors;

	--##########################################################################
	--                            MLAS
	--##########################################################################
	Function f_import_mlas(a_run_id In Number) Return Varchar2 Is
		l_msg Varchar2(2000);
		Type mla_import_table Is Table Of ls_import_lease%Rowtype Index By Pls_Integer;
		l_index            Number;
		mla_company        mla_company_table;
		l_mla_import_table mla_import_table;
		l_cc_sql           Clob;
		cc_needed          Boolean;
		rtn                Number;
	Begin

		Update ls_import_lease
			Set lease_id = ls_lease_seq.nextval
		 Where line_id In (Select line_id
									From (Select line_id,
													 row_number() over(Partition By unique_lease_identifier Order By unique_lease_identifier, line_id) num
											  From ls_import_lease
											 Where import_run_id = a_run_id)
								  Where num = 1)
			And import_run_id = a_run_id;

		Update ls_import_lease a
			Set a.lease_id =
				 (Select -b.lease_id
					 From ls_import_lease b
					Where a.unique_lease_identifier = b.unique_lease_identifier
					  And b.lease_id Is Not Null)
		 Where a.lease_id Is Null
			And import_run_id = a_run_id;

		Select * Bulk Collect
		  Into l_mla_import_table
		  From ls_import_lease
		 Where import_run_id = a_run_id
			And nvl(lease_id, -1) > 0;

		l_msg := 'LS_LEASE';
		Forall l_index In Indices Of l_mla_import_table
			Insert Into ls_lease
				(lease_id,
				 lease_status_id,
				 lease_number,
				 description,
				 long_description,
				 lessor_id,
				 lease_type_id,
				 payment_due_day,
				 pre_payment_sw,
				 lease_group_id,
				 lease_cap_type_id,
				 workflow_type_id,
				 master_agreement_date,
				 notes,
				 current_revision,
				 initiation_date)
				Select l_mla_import_table(l_index).lease_id,
						 1,
						 l_mla_import_table(l_index).lease_number,
						 l_mla_import_table(l_index).description,
						 l_mla_import_table(l_index).long_description,
						 l_mla_import_table(l_index).lessor_id,
						 l_mla_import_table(l_index).lease_type_id,
						 l_mla_import_table(l_index).payment_due_day,
						 l_mla_import_table(l_index).pre_payment_sw,
						 l_mla_import_table(l_index).lease_group_id,
						 l_mla_import_table(l_index).lease_cap_type_id,
						 l_mla_import_table(l_index).workflow_type_id,
						 to_date(l_mla_import_table(l_index).master_agreement_date, 'yyyymm'),
						 l_mla_import_table(l_index).notes,
						 1,
						 Sysdate
				  From dual;

		l_msg := 'LS_LEASE_APPROVAL';
		Forall l_index In Indices Of l_mla_import_table
			Insert Into ls_lease_approval
				(lease_id,
				 revision,
				 approval_type_id,
				 approval_status_id)
				Select l_mla_import_table(l_index).lease_id, 1, a.workflow_type_id, 1
				  From workflow_type a
				 Where a.external_workflow_type = 'AUTO';

		l_msg := 'LS_LEASE_OPTIONS';
		Forall l_index In Indices Of l_mla_import_table
			Insert Into ls_lease_options
				(lease_id,
				 revision,
				 purchase_option_type_id,
				 purchase_option_amt,
				 renewal_option_type_id,
				 cancelable_type_id,
				 itc_sw,
				 partial_retire_sw,
				 sublet_sw,
				 tax_summary_id,
				 tax_rate_option_id,
				 auto_generate_invoices)
			Values
				(l_mla_import_table(l_index).lease_id,
				 1,
				 l_mla_import_table(l_index).purchase_option_type_id,
				 l_mla_import_table(l_index).purchase_option_amt,
				 l_mla_import_table(l_index).renewal_option_type_id,
				 l_mla_import_table(l_index).cancelable_type_id,
				 l_mla_import_table(l_index).itc_sw,
				 l_mla_import_table(l_index).partial_retire_sw,
				 l_mla_import_table(l_index).sublet_sw,
				 nvl(l_mla_import_table(l_index).tax_summary_id, 0),
				 nvl(l_mla_import_table(l_index).tax_rate_option_id, 0),
				 nvl(l_mla_import_table(l_index).auto_generate_invoices, 0));

		l_cc_sql := 'BEGIN ';
		For l_index In 1 .. l_mla_import_table.count
		Loop
			If l_mla_import_table(l_index).class_code_id1 Is Not Null And l_mla_import_table(l_index)
				.class_code_value1 Is Not Null Then
				cc_needed := True;
				l_cc_sql  := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
											values (' || to_char(l_mla_import_table(l_index).class_code_id1) || ', ' ||
								 to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
								 to_char(l_mla_import_table(l_index).class_code_value1) || ''');';
				If l_mla_import_table(l_index).class_code_id2 Is Not Null And l_mla_import_table(l_index)
					.class_code_value2 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id2) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value2) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id3 Is Not Null And l_mla_import_table(l_index)
					.class_code_value3 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id3) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value3) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id4 Is Not Null And l_mla_import_table(l_index)
					.class_code_value4 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id4) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value4) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id5 Is Not Null And l_mla_import_table(l_index)
					.class_code_value5 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id5) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value5) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id6 Is Not Null And l_mla_import_table(l_index)
					.class_code_value6 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id6) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value6) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id7 Is Not Null And l_mla_import_table(l_index)
					.class_code_value7 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id7) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value7) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id8 Is Not Null And l_mla_import_table(l_index)
					.class_code_value8 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id8) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value8) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id9 Is Not Null And l_mla_import_table(l_index)
					.class_code_value9 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id9) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value9) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id10 Is Not Null And l_mla_import_table(l_index)
					.class_code_value10 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id10) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value10) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id11 Is Not Null And l_mla_import_table(l_index)
					.class_code_value11 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id11) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value11) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id12 Is Not Null And l_mla_import_table(l_index)
					.class_code_value12 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id12) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value12) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id13 Is Not Null And l_mla_import_table(l_index)
					.class_code_value13 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id13) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value13) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id14 Is Not Null And l_mla_import_table(l_index)
					.class_code_value14 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id14) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value14) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id15 Is Not Null And l_mla_import_table(l_index)
					.class_code_value15 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id15) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value15) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id16 Is Not Null And l_mla_import_table(l_index)
					.class_code_value16 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id16) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value16) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id17 Is Not Null And l_mla_import_table(l_index)
					.class_code_value17 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id17) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value17) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id18 Is Not Null And l_mla_import_table(l_index)
					.class_code_value18 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id18) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value18) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id19 Is Not Null And l_mla_import_table(l_index)
					.class_code_value19 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id19) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value19) || ''');';
				End If;
				If l_mla_import_table(l_index).class_code_id20 Is Not Null And l_mla_import_table(l_index)
					.class_code_value20 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
												values (' ||
									to_char(l_mla_import_table(l_index).class_code_id20) || ', ' ||
									to_char(l_mla_import_table(l_index).lease_id) || ', ''' ||
									to_char(l_mla_import_table(l_index).class_code_value20) || ''');';
				End If;
			End If;
		End Loop;
		l_cc_sql := l_cc_sql || ' END;';
		If cc_needed Then
			l_msg := l_cc_sql;
			Execute Immediate l_cc_sql;
		End If;

		Select Distinct abs(lease_id), company_id, max_lease_line, 0, 0 Bulk Collect
		  Into mla_company
		  From ls_import_lease
		 Where import_run_id = a_run_id;

		l_msg := 'LS_LEASE_COMPANY';
		Forall l_index In Indices Of mla_company
			Insert Into ls_lease_company
				(lease_id,
				 company_id,
				 max_lease_line)
			Values
				(mla_company(l_index).lease_id,
				 mla_company(l_index).company_id,
				 mla_company(l_index).max_lease_line);

		Select Distinct lease_id, company_id, max_lease_line, vendor_id, payment_pct Bulk Collect
		  Into mla_company
		  From ls_import_lease
		 Where import_run_id = a_run_id;

		l_msg := 'LS_LEASE_VENDOR';
		Forall l_index In Indices Of mla_company
			Insert Into ls_lease_vendor
				(lease_id,
				 company_id,
				 vendor_id,
				 payment_pct)
			Values
				(abs(mla_company(l_index).lease_id),
				 mla_company(l_index).company_id,
				 mla_company(l_index).vendor_id,
				 mla_company(l_index).payment_pct);

		--Auto approve it
		Select * Bulk Collect
		  Into l_mla_import_table
		  From ls_import_lease
		 Where import_run_id = a_run_id
			And nvl(lease_id, -1) > 0
			And (auto_approve = 1 Or auto_approve Is Null)
			And (workflow_type_id In
				 (Select workflow_type_id
					  From workflow_type
					 Where upper(external_workflow_type) = 'AUTO') Or workflow_type_id Is Null);

		--Instead of calling PKG_LEASE_CALC.F_APPROVE_MLA, I'm copying the SQL here since F_APPROVE_MLA is pragma autonomous
		--Since we can't commit before approving, the autonomous transaction won't be to update anything, so we can't use it
		l_msg := 'Auto Approving';
		For l_index In 1 .. l_mla_import_table.count
		Loop
			Update ls_lease_approval
				Set approval_status_id = 3,
					 approval_date      = Sysdate,
					 approver           = User
			 Where lease_id = l_mla_import_table(l_index).lease_id
				And nvl(revision, 0) = 1;

			Update ls_lease l
				Set lease_status_id  = 3,
					 approval_date    = Sysdate,
					 current_revision = 1
			 Where lease_id = l_mla_import_table(l_index).lease_id;
		End Loop;

		Return 'OK';
	Exception
		When Others Then
			l_msg := substr(l_msg || ': ' || Sqlerrm, 1, 2000);
			Return l_msg;
	End f_import_mlas;

	Function f_validate_mlas(a_run_id In Number) Return Varchar2 Is
		l_msg Varchar2(2000);
	Begin
		--check for duplicate lease number
		Update ls_import_lease
			Set error_message = error_message || ' Duplicate Lease Number.'
		 Where import_run_id = a_run_id
			And lease_number In
				 (Select lease_number
					 From (Select lease_number, Count(*) As my_count
								From (Select lease_number
										  From ls_lease
										Union All
										Select lease_number
										  From (Select Distinct lease_number, unique_lease_identifier
													 From ls_import_lease
													Where import_run_id = a_run_id))
							  Group By lease_number)
					Where my_count > 1);

		--check that payment_pct makes sense
		Update ls_import_lease
			Set error_message = error_message || ' Payment Percentage must be between 0 and 1.'
		 Where import_run_id = a_run_id
			And payment_pct Not Between 0.000000001 And 1.000000001; --we dont want a 0 but 1 is ok

		--check that the payment_pct adds up to 1 across the companies
		Update ls_import_lease
			Set error_message = error_message || ' Total payment percentage must be 100% per company.'
		 Where import_run_id = a_run_id
			And (unique_lease_identifier, company_id) In
				 (Select unique_lease_identifier, company_id
					 From (Select unique_lease_identifier, company_id, Sum(payment_pct) total_pct
								From ls_import_lease
							  Where import_run_id = a_run_id
							  Group By unique_lease_identifier, company_id)
					Where total_pct <> 1);

		Update ls_import_lease
			Set error_message = error_message || ' Vendor is not valid for this Lessor.'
		 Where import_run_id = a_run_id
			And (lessor_id, vendor_id) Not In
				 (Select Distinct lessor_id, vendor_id From ls_vendor Where status_code_id <> 2);

		Update ls_import_lease
			Set error_message = error_message ||
									  ' Selected workflow type cannot be automatically approved.'
		 Where import_run_id = a_run_id
			And auto_approve = 1
			And workflow_type_id <>
				 (Select workflow_type_id From workflow_type Where external_workflow_type = 'AUTO');

		Return 'OK';
	Exception
		When Others Then
			l_msg := substr(l_msg || ': ' || Sqlerrm, 1, 2000);
			Return l_msg;
	End f_validate_mlas;

	--##########################################################################
	--                            ILRS
	--##########################################################################
	Function f_import_ilrs(a_run_id In Number) Return Varchar2 Is
		l_msg Varchar2(2000);
		Type ilr_import_table Is Table Of ls_import_ilr%Rowtype;
		l_ilr_import ilr_import_table;
		l_index      Number;
		l_cc_sql     Clob;
		cc_needed    Boolean;
	Begin

		Update ls_import_ilr
			Set ilr_id = ls_ilr_seq.nextval
		 Where line_id In (Select line_id
									From (Select line_id,
													 row_number() over(Partition By unique_ilr_identifier Order By unique_ilr_identifier, line_id) num
											  From ls_import_ilr
											 Where import_run_id = a_run_id)
								  Where num = 1)
			And import_run_id = a_run_id;

		Update ls_import_ilr a
			Set a.ilr_id =
				 (Select -b.ilr_id
					 From ls_import_ilr b
					Where a.unique_ilr_identifier = b.unique_ilr_identifier
					  And b.ilr_id Is Not Null)
		 Where a.ilr_id Is Null
			And import_run_id = a_run_id;

		Update ls_import_ilr a
			Set a.workflow_type_id =
				 (Select workflow_type_id From ls_ilr_group Where ilr_group_id = a.ilr_group_id)
		 Where a.workflow_type_id Is Null
			And a.import_run_id = a_run_id;

		Select * Bulk Collect
		  Into l_ilr_import
		  From ls_import_ilr
		 Where import_run_id = a_run_id
			And nvl(ilr_id, -1) > 0;

		Forall l_index In Indices Of l_ilr_import
			Insert Into ls_ilr
				(ilr_id,
				 ilr_status_id,
				 ilr_number,
				 lease_id,
				 company_id,
				 est_in_svc_date,
				 external_ilr,
				 ilr_group_id,
				 notes,
				 current_revision,
				 workflow_type_id)
			Values
				(l_ilr_import(l_index).ilr_id,
				 1,
				 l_ilr_import(l_index).ilr_number,
				 l_ilr_import(l_index).lease_id,
				 l_ilr_import(l_index).company_id,
				 to_date(l_ilr_import(l_index).est_in_svc_date, 'yyyymm'),
				 l_ilr_import(l_index).external_ilr,
				 l_ilr_import(l_index).ilr_group_id,
				 l_ilr_import(l_index).notes,
				 1,
				 l_ilr_import(l_index).workflow_type_id);

		Forall l_index In Indices Of l_ilr_import
			Insert Into ls_ilr_approval
				(ilr_id,
				 revision,
				 approval_type_id,
				 approval_status_id)
			Values
				(l_ilr_import(l_index).ilr_id,
				 1,
				 4,
				 1);

		Forall l_index In Indices Of l_ilr_import
			Insert Into ls_ilr_options
				(ilr_id,
				 revision,
				 purchase_option_type_id,
				 purchase_option_amt,
				 renewal_option_type_id,
				 cancelable_type_id,
				 itc_sw,
				 partial_retire_sw,
				 sublet_sw,
				 inception_air,
				 lease_cap_type_id,
				 termination_amt,
				 import_run_id,
				 payment_shift)
				(Select l_ilr_import (l_index).ilr_id,
						  1,
						  l_ilr_import (l_index).purchase_option_type_id,
						  l_ilr_import (l_index).purchase_option_amt,
						  l_ilr_import (l_index).renewal_option_type_id,
						  l_ilr_import (l_index).cancelable_type_id,
						  l_ilr_import (l_index).itc_sw,
						  l_ilr_import (l_index).partial_retire_sw,
						  l_ilr_import (l_index).sublet_sw,
						  l_ilr_import (l_index).inception_air,
						  l_ilr_import (l_index).lease_cap_type_id,
						  l_ilr_import (l_index).termination_amt,
						  a_run_id,
						  payment_shift
					From ls_ilr_group
				  Where ilr_group_id = l_ilr_import(l_index).ilr_group_id);

		Forall l_index In Indices Of l_ilr_import
			Insert Into ls_ilr_account
				(ilr_id,
				 int_accrual_account_id,
				 int_expense_account_id,
				 exec_accrual_account_id,
				 exec_expense_account_id,
				 cont_accrual_account_id,
				 cont_expense_account_id,
				 cap_asset_account_id,
				 st_oblig_account_id,
				 lt_oblig_account_id,
				 ap_account_id,
				 res_debit_account_id,
				 res_credit_account_id)
				Select l_ilr_import(l_index).ilr_id,
						 nvl(l_ilr_import(l_index).int_accrual_account_id, int_accrual_account_id),
						 nvl(l_ilr_import(l_index).int_expense_account_id, int_expense_account_id),
						 nvl(l_ilr_import(l_index).exec_accrual_account_id, exec_accrual_account_id),
						 nvl(l_ilr_import(l_index).exec_expense_account_id, exec_expense_account_id),
						 nvl(l_ilr_import(l_index).cont_accrual_account_id, cont_accrual_account_id),
						 nvl(l_ilr_import(l_index).cont_expense_account_id, cont_expense_account_id),
						 nvl(l_ilr_import(l_index).cap_asset_account_id, cap_asset_account_id),
						 nvl(l_ilr_import(l_index).st_oblig_account_id, st_oblig_account_id),
						 nvl(l_ilr_import(l_index).lt_oblig_account_id, lt_oblig_account_id),
						 nvl(l_ilr_import(l_index).ap_account_id, ap_account_id),
						 nvl(l_ilr_import(l_index).res_debit_account_id, res_debit_account_id),
						 nvl(l_ilr_import(l_index).res_credit_account_id, res_credit_account_id)
				  From ls_ilr_group
				 Where ilr_group_id = l_ilr_import(l_index).ilr_group_id;

		l_cc_sql := 'BEGIN ';
		For l_index In 1 .. l_ilr_import.count
		Loop
			If l_ilr_import(l_index)
			 .class_code_id1 Is Not Null And l_ilr_import(l_index).class_code_value1 Is Not Null Then
				cc_needed := True;
				l_cc_sql  := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
											values (' || to_char(l_ilr_import(l_index).class_code_id1) || ', ' ||
								 to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
								 to_char(l_ilr_import(l_index).class_code_value1) || ''');';
				If l_ilr_import(l_index)
				 .class_code_id2 Is Not Null And l_ilr_import(l_index).class_code_value2 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id2) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value2) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id3 Is Not Null And l_ilr_import(l_index).class_code_value3 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id3) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value3) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id4 Is Not Null And l_ilr_import(l_index).class_code_value4 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id4) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value4) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id5 Is Not Null And l_ilr_import(l_index).class_code_value5 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id5) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value5) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id6 Is Not Null And l_ilr_import(l_index).class_code_value6 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id6) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value6) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id7 Is Not Null And l_ilr_import(l_index).class_code_value7 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id7) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value7) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id8 Is Not Null And l_ilr_import(l_index).class_code_value8 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id8) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value8) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id9 Is Not Null And l_ilr_import(l_index).class_code_value9 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id9) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value9) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id10 Is Not Null And l_ilr_import(l_index).class_code_value10 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id10) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value10) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id11 Is Not Null And l_ilr_import(l_index).class_code_value11 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id11) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value11) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id12 Is Not Null And l_ilr_import(l_index).class_code_value12 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id12) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value12) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id13 Is Not Null And l_ilr_import(l_index).class_code_value13 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id13) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value13) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id14 Is Not Null And l_ilr_import(l_index).class_code_value14 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id14) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value14) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id15 Is Not Null And l_ilr_import(l_index).class_code_value15 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id15) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value15) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id16 Is Not Null And l_ilr_import(l_index).class_code_value16 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id16) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value16) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id17 Is Not Null And l_ilr_import(l_index).class_code_value17 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id17) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value17) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id18 Is Not Null And l_ilr_import(l_index).class_code_value18 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id18) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value18) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id19 Is Not Null And l_ilr_import(l_index).class_code_value19 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id19) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value19) || ''');';
				End If;
				If l_ilr_import(l_index)
				 .class_code_id20 Is Not Null And l_ilr_import(l_index).class_code_value20 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
												values (' || to_char(l_ilr_import(l_index).class_code_id20) || ', ' ||
									to_char(l_ilr_import(l_index).ilr_id) || ', ''' ||
									to_char(l_ilr_import(l_index).class_code_value20) || ''');';
				End If;
			End If;
		End Loop;
		l_cc_sql := l_cc_sql || ' END;';
		If cc_needed Then
			l_msg := l_cc_sql;
			Execute Immediate l_cc_sql;
		End If;

		Select * Bulk Collect Into l_ilr_import From ls_import_ilr Where import_run_id = a_run_id;

		Forall l_index In Indices Of l_ilr_import
			Insert Into ls_ilr_payment_term
				(ilr_id,
				 payment_term_id,
				 payment_term_type_id,
				 payment_term_date,
				 payment_freq_id,
				 number_of_terms,
				 est_executory_cost,
				 paid_amount,
				 contingent_amount,
				 revision,
				 c_bucket_1,
				 c_bucket_2,
				 c_bucket_3,
				 c_bucket_4,
				 c_bucket_5,
				 c_bucket_6,
				 c_bucket_7,
				 c_bucket_8,
				 c_bucket_9,
				 c_bucket_10,
				 e_bucket_1,
				 e_bucket_2,
				 e_bucket_3,
				 e_bucket_4,
				 e_bucket_5,
				 e_bucket_6,
				 e_bucket_7,
				 e_bucket_8,
				 e_bucket_9,
				 e_bucket_10)
			Values
				(abs(l_ilr_import(l_index).ilr_id),
				 l_ilr_import(l_index).payment_term_id,
				 2,
				 to_date(l_ilr_import(l_index).payment_term_date, 'yyyymm'),
				 l_ilr_import(l_index).payment_freq_id,
				 l_ilr_import(l_index).number_of_terms,
				 l_ilr_import(l_index).est_executory_cost,
				 l_ilr_import(l_index).paid_amount,
				 l_ilr_import(l_index).contingent_amount,
				 1,
				 l_ilr_import(l_index).c_bucket_1,
				 l_ilr_import(l_index).c_bucket_2,
				 l_ilr_import(l_index).c_bucket_3,
				 l_ilr_import(l_index).c_bucket_4,
				 l_ilr_import(l_index).c_bucket_5,
				 l_ilr_import(l_index).c_bucket_6,
				 l_ilr_import(l_index).c_bucket_7,
				 l_ilr_import(l_index).c_bucket_8,
				 l_ilr_import(l_index).c_bucket_9,
				 l_ilr_import(l_index).c_bucket_10,
				 l_ilr_import(l_index).e_bucket_1,
				 l_ilr_import(l_index).e_bucket_2,
				 l_ilr_import(l_index).e_bucket_3,
				 l_ilr_import(l_index).e_bucket_4,
				 l_ilr_import(l_index).e_bucket_5,
				 l_ilr_import(l_index).e_bucket_6,
				 l_ilr_import(l_index).e_bucket_7,
				 l_ilr_import(l_index).e_bucket_8,
				 l_ilr_import(l_index).e_bucket_9,
				 l_ilr_import(l_index).e_bucket_10);

		Return 'OK';
	Exception
		When Others Then
			l_msg := substr(l_msg || ': ' || Sqlerrm, 1, 2000);
			Return l_msg;
	End f_import_ilrs;

	Function f_validate_ilrs(a_run_id In Number) Return Varchar2 Is
		l_msg Varchar2(2000);
	Begin
		--check we don't have duplicate ILR numbers
		Update ls_import_ilr
			Set error_message = error_message || ' Duplicate ilr number.'
		 Where import_run_id = a_run_id
			And ilr_number In (Select ilr_number
										From (Select ilr_number, Count(*) As my_count
												  From (Select ilr_number
															 From ls_ilr
														  Union All
														  Select ilr_number
															 From (Select Distinct ilr_number, unique_ilr_identifier
																		From ls_import_ilr
																	  Where import_run_id = a_run_id))
												 Group By ilr_number)
									  Where my_count > 1);

		--check we don't have duplicate external ilrs
		Update ls_import_ilr
			Set error_message = error_message || ' Duplicate external ilr.'
		 Where import_run_id = a_run_id
			And external_ilr In
				 (Select external_ilr
					 From (Select external_ilr, Count(*) As my_count
								From (Select external_ilr
										  From ls_ilr
										Union All
										Select external_ilr
										  From (Select Distinct external_ilr, unique_ilr_identifier
													 From ls_import_ilr
													Where import_run_id = a_run_id))
							  Group By external_ilr)
					Where my_count > 1
					  And external_ilr Is Not Null);

		--check the payment terms have sequential payment term ids
		Update ls_import_ilr
			Set error_message = error_message || ' Non sequential payment term ids.'
		 Where import_run_id = a_run_id
			And line_id In (Select line_id
									From (Select line_id,
													 payment_term_id,
													 row_number() over(Partition By unique_ilr_identifier Order By unique_ilr_identifier, payment_term_id) my_terms
											  From ls_import_ilr
											 Where import_run_id = a_run_id)
								  Where payment_term_id <> my_terms);

		--check that one term ends where the next begins
		Update ls_import_ilr
			Set error_message = error_message ||
									  ' Start of ILR term does not match with end of previous term.'
		 Where import_run_id = a_run_id
			And line_id In
				 (Select line_id
					 From (Select line_id,
									  term_start,
									  lag(term_end, 1, to_date('07/04/1776', 'mm/dd/yyyy')) over(Partition By unique_ilr_identifier Order By unique_ilr_identifier, term_start) As term_end
								From (Select line_id,
												 unique_ilr_identifier,
												 to_date(payment_term_date, 'yyyymm') As term_start,
												 add_months(to_date(payment_term_date, 'yyyymm'),
																number_of_terms *
																 decode(payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1)) As term_end
										  From ls_import_ilr
										 Where import_run_id = a_run_id))
					Where term_start <> term_end
					  And term_end <> to_date('07/04/1776', 'mm/dd/yyyy'));

		--make sure the company is valid for the lease
		Update ls_import_ilr
			Set error_message = error_message || ' Company not valid for this MLA.'
		 Where import_run_id = a_run_id
			And company_id Not In
				 (Select company_id From ls_lease_company Where lease_id = ls_import_ilr.lease_id);

		--check that any buckets being imported are enabled.
		Update ls_import_ilr
			Set error_message = error_message || ' Executory bucket 1 is not enabled.'
		 Where Not (e_bucket_1 Is Null Or e_bucket_1 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Executory'
								 And bucket_number = 1
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Executory'
						And bucket_number = 1));

		Update ls_import_ilr
			Set error_message = error_message || ' Executory bucket 2 is not enabled.'
		 Where Not (e_bucket_2 Is Null Or e_bucket_2 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Executory'
								 And bucket_number = 2
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Executory'
						And bucket_number = 2));

		Update ls_import_ilr
			Set error_message = error_message || ' Executory bucket 3 is not enabled.'
		 Where Not (e_bucket_3 Is Null Or e_bucket_3 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Executory'
								 And bucket_number = 3
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Executory'
						And bucket_number = 3));

		Update ls_import_ilr
			Set error_message = error_message || ' Executory bucket 4 is not enabled.'
		 Where Not (e_bucket_4 Is Null Or e_bucket_4 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Executory'
								 And bucket_number = 4
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Executory'
						And bucket_number = 4));

		Update ls_import_ilr
			Set error_message = error_message || ' Executory bucket 5 is not enabled.'
		 Where Not (e_bucket_5 Is Null Or e_bucket_5 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Executory'
								 And bucket_number = 5
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Executory'
						And bucket_number = 5));

		Update ls_import_ilr
			Set error_message = error_message || ' Executory bucket 6 is not enabled.'
		 Where Not (e_bucket_6 Is Null Or e_bucket_6 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Executory'
								 And bucket_number = 6
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Executory'
						And bucket_number = 6));

		Update ls_import_ilr
			Set error_message = error_message || ' Executory bucket 7 is not enabled.'
		 Where Not (e_bucket_7 Is Null Or e_bucket_7 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Executory'
								 And bucket_number = 7
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Executory'
						And bucket_number = 7));

		Update ls_import_ilr
			Set error_message = error_message || ' Executory bucket 8 is not enabled.'
		 Where Not (e_bucket_8 Is Null Or e_bucket_8 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Executory'
								 And bucket_number = 8
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Executory'
						And bucket_number = 8));

		Update ls_import_ilr
			Set error_message = error_message || ' Executory bucket 9 is not enabled.'
		 Where Not (e_bucket_9 Is Null Or e_bucket_9 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Executory'
								 And bucket_number = 9
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Executory'
						And bucket_number = 9));

		Update ls_import_ilr
			Set error_message = error_message || ' Executory bucket 10 is not enabled.'
		 Where Not (e_bucket_10 Is Null Or e_bucket_10 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Executory'
								 And bucket_number = 10
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Executory'
						And bucket_number = 10));

		Update ls_import_ilr
			Set error_message = error_message || ' Contingent bucket 1 is not enabled.'
		 Where Not (c_bucket_1 Is Null Or c_bucket_1 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Contingent'
								 And bucket_number = 1
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Contingent'
						And bucket_number = 1));

		Update ls_import_ilr
			Set error_message = error_message || ' Contingent bucket 2 is not enabled.'
		 Where Not (c_bucket_2 Is Null Or c_bucket_2 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Contingent'
								 And bucket_number = 2
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Contingent'
						And bucket_number = 2));

		Update ls_import_ilr
			Set error_message = error_message || ' Contingent bucket 3 is not enabled.'
		 Where Not (c_bucket_3 Is Null Or c_bucket_3 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Contingent'
								 And bucket_number = 3
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Contingent'
						And bucket_number = 3));

		Update ls_import_ilr
			Set error_message = error_message || ' Contingent bucket 4 is not enabled.'
		 Where Not (c_bucket_4 Is Null Or c_bucket_4 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Contingent'
								 And bucket_number = 4
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Contingent'
						And bucket_number = 4));

		Update ls_import_ilr
			Set error_message = error_message || ' Contingent bucket 5 is not enabled.'
		 Where Not (c_bucket_5 Is Null Or c_bucket_5 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Contingent'
								 And bucket_number = 5
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Contingent'
						And bucket_number = 5));

		Update ls_import_ilr
			Set error_message = error_message || ' Contingent bucket 6 is not enabled.'
		 Where Not (c_bucket_6 Is Null Or c_bucket_6 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Contingent'
								 And bucket_number = 6
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Contingent'
						And bucket_number = 6));

		Update ls_import_ilr
			Set error_message = error_message || ' Contingent bucket 7 is not enabled.'
		 Where Not (c_bucket_7 Is Null Or c_bucket_7 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Contingent'
								 And bucket_number = 7
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Contingent'
						And bucket_number = 7));

		Update ls_import_ilr
			Set error_message = error_message || ' Contingent bucket 8 is not enabled.'
		 Where Not (c_bucket_8 Is Null Or c_bucket_8 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Contingent'
								 And bucket_number = 8
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Contingent'
						And bucket_number = 8));

		Update ls_import_ilr
			Set error_message = error_message || ' Contingent bucket 9 is not enabled.'
		 Where Not (c_bucket_9 Is Null Or c_bucket_9 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Contingent'
								 And bucket_number = 9
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Contingent'
						And bucket_number = 9));

		Update ls_import_ilr
			Set error_message = error_message || ' Contingent bucket 10 is not enabled.'
		 Where Not (c_bucket_10 Is Null Or c_bucket_10 = 0)
			And import_run_id = a_run_id
			And (Exists (Select *
								From ls_rent_bucket_admin
							  Where rent_type = 'Contingent'
								 And bucket_number = 10
								 And status_code_id = 0) Or Not Exists
				  (Select *
					  From ls_rent_bucket_admin
					 Where rent_type = 'Contingent'
						And bucket_number = 10));

		Update ls_import_ilr
			Set error_message = error_message || ' Non-lease Interest Accrual Account.'
		 Where int_accrual_account_id Is Not Null
			And import_run_id = a_run_id
			And (Select gl_account.account_type_id
					 From gl_account
					Where gl_account.gl_account_id = int_accrual_account_id) <> 11;

		Update ls_import_ilr
			Set error_message = error_message || ' Non-lease Interest Expense Account.'
		 Where int_expense_account_id Is Not Null
			And import_run_id = a_run_id
			And (Select gl_account.account_type_id
					 From gl_account
					Where gl_account.gl_account_id = int_expense_account_id) <> 11;

		Update ls_import_ilr
			Set error_message = error_message || ' Non-lease Executory Accrual Account.'
		 Where exec_accrual_account_id Is Not Null
			And import_run_id = a_run_id
			And (Select gl_account.account_type_id
					 From gl_account
					Where gl_account.gl_account_id = exec_accrual_account_id) <> 11;

		Update ls_import_ilr
			Set error_message = error_message || ' Non-lease Executory Expense Account.'
		 Where exec_expense_account_id Is Not Null
			And import_run_id = a_run_id
			And (Select gl_account.account_type_id
					 From gl_account
					Where gl_account.gl_account_id = exec_expense_account_id) <> 11;

		Update ls_import_ilr
			Set error_message = error_message || ' Non-lease Contingent Accrual Account.'
		 Where cont_accrual_account_id Is Not Null
			And import_run_id = a_run_id
			And (Select gl_account.account_type_id
					 From gl_account
					Where gl_account.gl_account_id = cont_accrual_account_id) <> 11;

		Update ls_import_ilr
			Set error_message = error_message || ' Non-lease Contingent Expense Account.'
		 Where cont_accrual_account_id Is Not Null
			And import_run_id = a_run_id
			And (Select gl_account.account_type_id
					 From gl_account
					Where gl_account.gl_account_id = cont_accrual_account_id) <> 11;

		Update ls_import_ilr
			Set error_message = error_message || ' Non-lease Capital Asset Account.'
		 Where cap_asset_account_id Is Not Null
			And import_run_id = a_run_id
			And (Select gl_account.account_type_id
					 From gl_account
					Where gl_account.gl_account_id = cap_asset_account_id) <> 11;

		Update ls_import_ilr
			Set error_message = error_message || ' Non-lease Short-term Obligation Account.'
		 Where st_oblig_account_id Is Not Null
			And import_run_id = a_run_id
			And (Select gl_account.account_type_id
					 From gl_account
					Where gl_account.gl_account_id = st_oblig_account_id) <> 11;

		Update ls_import_ilr
			Set error_message = error_message || ' Non-lease Long-term Obligation Account.'
		 Where lt_oblig_account_id Is Not Null
			And import_run_id = a_run_id
			And (Select gl_account.account_type_id
					 From gl_account
					Where gl_account.gl_account_id = lt_oblig_account_id) <> 11;

		Update ls_import_ilr
			Set error_message = error_message || ' Non-lease AP Account.'
		 Where ap_account_id Is Not Null
			And import_run_id = a_run_id
			And (Select gl_account.account_type_id
					 From gl_account
					Where gl_account.gl_account_id = ap_account_id) <> 11;

		Update ls_import_ilr
			Set error_message = error_message || ' Non-lease Residual Debit Account.'
		 Where res_debit_account_id Is Not Null
			And import_run_id = a_run_id
			And (Select gl_account.account_type_id
					 From gl_account
					Where gl_account.gl_account_id = res_debit_account_id) <> 11;

		Update ls_import_ilr
			Set error_message = error_message || ' Non-lease Residual Credit Account.'
		 Where res_credit_account_id Is Not Null
			And import_run_id = a_run_id
			And (Select gl_account.account_type_id
					 From gl_account
					Where gl_account.gl_account_id = res_credit_account_id) <> 11;

		Return 'OK';
	Exception
		When Others Then
			l_msg := substr(l_msg || ': ' || Sqlerrm, 1, 2000);
			Return l_msg;
	End f_validate_ilrs;
	--##########################################################################
	--                            ASSETS
	--##########################################################################
	Function f_import_assets(a_run_id In Number) Return Varchar2 Is
		l_msg          Varchar2(2000);
		l_asset_import asset_import_table;
		l_index        Number;
		l_cc_sql       Clob;
		cc_needed      Boolean;
	Begin

		Update ls_import_asset Set ls_asset_id = ls_asset_seq.nextval;

		Select * Bulk Collect
		  Into l_asset_import
		  From ls_import_asset
		 Where import_run_id = a_run_id;

		Forall l_index In Indices Of l_asset_import
			Insert Into ls_asset
				(ls_asset_id,
				 ls_asset_status_id,
				 leased_asset_number,
				 description,
				 long_description,
				 quantity,
				 fmv,
				 company_id,
				 bus_segment_id,
				 utility_account_id,
				 sub_account_id,
				 retirement_unit_id,
				 property_group_id,
				 work_order_id,
				 asset_location_id,
				 guaranteed_residual_amount,
				 expected_life,
				 economic_life,
				 notes,
				 ilr_id,
				 serial_number,
				 import_run_id,
				 tax_asset_location_id,
				 department_id)
				Select l_asset_import(l_index).ls_asset_id,
						 1,
						 l_asset_import(l_index).leased_asset_number,
						 l_asset_import(l_index).description,
						 l_asset_import(l_index).long_description,
						 l_asset_import(l_index).quantity,
						 l_asset_import(l_index).fmv,
						 l_asset_import(l_index).company_id,
						 l_asset_import(l_index).bus_segment_id,
						 l_asset_import(l_index).utility_account_id,
						 l_asset_import(l_index).sub_account_id,
						 l_asset_import(l_index).retirement_unit_id,
						 l_asset_import(l_index).property_group_id,
						 l_asset_import(l_index).work_order_id,
						 l_asset_import(l_index).asset_location_id,
						 l_asset_import(l_index).guaranteed_residual_amount,
						 l_asset_import(l_index).expected_life,
						 l_asset_import(l_index).economic_life,
						 l_asset_import(l_index).notes,
						 l_asset_import(l_index).ilr_id,
						 l_asset_import(l_index).serial_number,
						 a_run_id,
						 nvl(l_asset_import(l_index).tax_asset_location_id,
							  l_asset_import(l_index).asset_location_id),
						 l_asset_import(l_index).department_id
				  From dual;

		--Do class code inserts here
		l_cc_sql := 'BEGIN ';
		For l_index In 1 .. l_asset_import.count
		Loop
			If l_asset_import(l_index)
			 .class_code_id1 Is Not Null And l_asset_import(l_index).class_code_value1 Is Not Null Then
				cc_needed := True;
				l_cc_sql  := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
											values (' || to_char(l_asset_import(l_index).class_code_id1) || ', ' ||
								 to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
								 to_char(l_asset_import(l_index).class_code_value1) || ''');';
				If l_asset_import(l_index)
				 .class_code_id2 Is Not Null And l_asset_import(l_index).class_code_value2 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id2) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value2) || ''');';
				End If;
				If l_asset_import(l_index)
				 .class_code_id3 Is Not Null And l_asset_import(l_index).class_code_value3 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id3) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value3) || ''');';
				End If;
				If l_asset_import(l_index)
				 .class_code_id4 Is Not Null And l_asset_import(l_index).class_code_value4 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id4) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value4) || ''');';
				End If;
				If l_asset_import(l_index)
				 .class_code_id5 Is Not Null And l_asset_import(l_index).class_code_value5 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id5) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value5) || ''');';
				End If;
				If l_asset_import(l_index)
				 .class_code_id6 Is Not Null And l_asset_import(l_index).class_code_value6 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id6) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value6) || ''');';
				End If;
				If l_asset_import(l_index)
				 .class_code_id7 Is Not Null And l_asset_import(l_index).class_code_value7 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id7) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value7) || ''');';
				End If;
				If l_asset_import(l_index)
				 .class_code_id8 Is Not Null And l_asset_import(l_index).class_code_value8 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id8) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value8) || ''');';
				End If;
				If l_asset_import(l_index)
				 .class_code_id9 Is Not Null And l_asset_import(l_index).class_code_value9 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id9) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value9) || ''');';
				End If;
				If l_asset_import(l_index).class_code_id10 Is Not Null And l_asset_import(l_index)
					.class_code_value10 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id10) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value10) || ''');';
				End If;
				If l_asset_import(l_index).class_code_id11 Is Not Null And l_asset_import(l_index)
					.class_code_value11 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id11) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value11) || ''');';
				End If;
				If l_asset_import(l_index).class_code_id12 Is Not Null And l_asset_import(l_index)
					.class_code_value12 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id12) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value12) || ''');';
				End If;
				If l_asset_import(l_index).class_code_id13 Is Not Null And l_asset_import(l_index)
					.class_code_value13 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id13) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value13) || ''');';
				End If;
				If l_asset_import(l_index).class_code_id14 Is Not Null And l_asset_import(l_index)
					.class_code_value14 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id14) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value14) || ''');';
				End If;
				If l_asset_import(l_index).class_code_id15 Is Not Null And l_asset_import(l_index)
					.class_code_value15 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id15) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value15) || ''');';
				End If;
				If l_asset_import(l_index).class_code_id16 Is Not Null And l_asset_import(l_index)
					.class_code_value16 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id16) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value16) || ''');';
				End If;
				If l_asset_import(l_index).class_code_id17 Is Not Null And l_asset_import(l_index)
					.class_code_value17 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id17) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value17) || ''');';
				End If;
				If l_asset_import(l_index).class_code_id18 Is Not Null And l_asset_import(l_index)
					.class_code_value18 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id18) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value18) || ''');';
				End If;
				If l_asset_import(l_index).class_code_id19 Is Not Null And l_asset_import(l_index)
					.class_code_value19 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id19) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value19) || ''');';
				End If;
				If l_asset_import(l_index).class_code_id20 Is Not Null And l_asset_import(l_index)
					.class_code_value20 Is Not Null Then
					l_cc_sql := l_cc_sql || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
												values (' || to_char(l_asset_import(l_index).class_code_id20) || ', ' ||
									to_char(l_asset_import(l_index).ls_asset_id) || ', ''' ||
									to_char(l_asset_import(l_index).class_code_value20) || ''');';
				End If;
			End If;
		End Loop;
		l_cc_sql := l_cc_sql || ' END;';
		If cc_needed Then
			l_msg := l_cc_sql;
			Execute Immediate l_cc_sql;
		End If;

		--Insert into LS_ILR_ASSET_MAP
		Insert Into ls_ilr_asset_map
			(ilr_id,
			 ls_asset_id,
			 revision)
			Select ia.ilr_id, ia.ls_asset_id, 1
			  From ls_import_asset ia
			 Where import_run_id = a_run_id
				And ia.ilr_id Is Not Null;

		Insert Into ls_asset_tax_map
			(ls_asset_id,
			 tax_local_id,
			 status_code_id)
			(Select la.ls_asset_id ls_asset_id, tl.tax_local_id tax_local_id, 1 status_code_id
				From ls_asset           la,
					  ls_tax_local       tl,
					  ls_ilr,
					  ls_lease,
					  ls_lease_options,
					  ls_tax_state_rates,
					  asset_location,
					  ls_import_asset
			  Where la.ilr_id = ls_ilr.ilr_id
				 And ls_ilr.lease_id = ls_lease.lease_id
				 And ls_lease.lease_id = ls_lease_options.lease_id
				 And ls_lease.current_revision = ls_lease_options.revision
				 And ls_lease_options.tax_summary_id = tl.tax_summary_id
				 And tl.tax_local_id = ls_tax_state_rates.tax_local_id
				 And ls_tax_state_rates.state_id = asset_location.state_id
				 And la.tax_asset_location_id = asset_location.asset_location_id
				 And la.ls_asset_id = ls_import_asset.ls_asset_id
				 And ls_import_asset.import_run_id = a_run_id
				 And ls_import_asset.ilr_id Is Not Null
			 Union
			 Select la.ls_asset_id ls_asset_id, tl.tax_local_id tax_local_id, 1 status_code_id
				From ls_asset              la,
					  ls_tax_local          tl,
					  ls_ilr,
					  ls_lease,
					  ls_lease_options,
					  ls_tax_district_rates,
					  asset_location,
					  ls_import_asset
			  Where la.ilr_id = ls_ilr.ilr_id
				 And ls_ilr.lease_id = ls_lease.lease_id
				 And ls_lease.lease_id = ls_lease_options.lease_id
				 And ls_lease.current_revision = ls_lease_options.revision
				 And ls_lease_options.tax_summary_id = tl.tax_summary_id
				 And tl.tax_local_id = ls_tax_district_rates.tax_local_id
				 And ls_tax_district_rates.ls_tax_district_id = asset_location.ls_tax_district_id
				 And la.tax_asset_location_id = asset_location.asset_location_id
				 And la.ls_asset_id = ls_import_asset.ls_asset_id
				 And ls_import_asset.import_run_id = a_run_id
				 And ls_import_asset.ilr_id Is Not Null);

		Return 'OK';
	Exception
		When Others Then
			l_msg := substr(l_msg || ': ' || Sqlerrm, 1, 2000);
			Return l_msg;
	End f_import_assets;

	Function f_validate_assets(a_run_id In Number) Return Varchar2 Is
		l_msg          Varchar2(2000);
		l_asset_import asset_import_table;
		l_index        Number;
	Begin
		--check we don't have duplicate LEASED_ASSET_NUMBERs
		Update ls_import_asset
			Set error_message = error_message || ' Duplicate leased asset number.'
		 Where import_run_id = a_run_id
			And leased_asset_number In
				 (Select leased_asset_number
					 From (Select leased_asset_number, Count(*) As my_count
								From (Select leased_asset_number
										  From ls_asset
										Union All
										Select leased_asset_number
										  From (Select Distinct leased_asset_number, unique_asset_identifier
													 From ls_import_asset
													Where import_run_id = a_run_id))
							  Group By leased_asset_number)
					Where my_count > 1);

		--check our CPR validations.
		--valid lease depr group
		Update ls_import_asset
			Set error_message = error_message ||
									  ' Retirement unit not in sub_ledger -100 property unit.'
		 Where import_run_id = a_run_id
			And retirement_unit_id Not In
				 (Select retirement_unit_id
					 From retirement_unit
					Where property_unit_id In
							(Select property_unit_id From property_unit Where subledger_type_id = -100));

		--valid bus_segment
		Update ls_import_asset
			Set error_message = error_message ||
									  ' This business segment is not valid for this company.'
		 Where import_run_id = a_run_id
			And bus_segment_id Not In
				 (Select bus_segment_id
					 From company_bus_segment_control
					Where company_id = ls_import_asset.company_id);

		--valid WO for this bus_Segment/company
		Update ls_import_asset
			Set error_message = error_message ||
									  ' Invalid work order for this company/business segment combination.'
		 Where import_run_id = a_run_id
			And work_order_id Not In
				 (Select work_order_id
					 From work_order_control
					Where bus_segment_id = ls_import_asset.bus_segment_id
					  And company_id = ls_import_asset.company_id);
		--valid asset locations
		Update ls_import_asset
			Set error_message = error_message || ' This asset location is not valid for this company.'
		 Where import_run_id = a_run_id
			And asset_location_id Not In
				 (Select asset_location_id
					 From asset_location
					Where major_location_id In
							(Select major_location_id
								From company_major_location
							  Where ls_import_asset.company_id = company_id));
		--valid utility_account
		Update ls_import_asset
			Set error_message = error_message ||
									  ' This utility account is not valid for this business segment.'
		 Where import_run_id = a_run_id
			And utility_account_id Not In
				 (Select utility_account_id
					 From utility_account
					Where bus_segment_id = ls_import_asset.bus_segment_id);

		--valid retirement_unit
		Update ls_import_asset
			Set error_message = error_message ||
									  ' This retirement unit is not valid for this utility account/business segment.'
		 Where import_run_id = a_run_id
			And retirement_unit_id Not In
				 (Select retirement_unit_id
					 From retirement_unit
					Where property_unit_id In
							(Select property_unit_id
								From util_acct_prop_unit
							  Where bus_segment_id = ls_import_asset.bus_segment_id
								 And utility_account_id = ls_import_asset.utility_account_id));

		--valid sub account...
		Update ls_import_asset
			Set error_message = error_message ||
									  ' This sub account is not valid for this utility account/business segment.'
		 Where import_run_id = a_run_id
			And sub_account_id Not In
				 (Select sub_account_id
					 From sub_account
					Where bus_segment_id = ls_import_asset.bus_segment_id
					  And utility_account_id = ls_import_asset.utility_account_id);

		--valid property group
		Update ls_import_asset
			Set error_message = error_message ||
									  ' This property group is not valid for this retirement unit.'
		 Where import_run_id = a_run_id
			And property_group_id Not In
				 (Select property_group_id
					 From prop_group_prop_unit
					Where property_unit_id =
							(Select property_unit_id
								From retirement_unit
							  Where retirement_unit_id = ls_import_asset.retirement_unit_id));

		Update ls_import_asset
			Set error_message = error_message || ' Quantities must be positive.'
		 Where import_run_id = a_run_id
			And quantity < 0;

		--If an ILR is defined then make sure the company is valid
		Update ls_import_asset
			Set error_message = error_message || ' Not a valid ILR for this company.'
		 Where import_run_id = a_run_id
			And ilr_id Is Not Null
			And company_id Not In
				 (Select company_id From ls_ilr Where ilr_id = ls_import_asset.ilr_id);

		Return 'OK';
	Exception
		When Others Then
			l_msg := substr(l_msg || ': ' || Sqlerrm, 1, 2000);
			Return l_msg;
	End f_validate_assets;

	--##########################################################################
	--                            Components
	--##########################################################################
	Function f_import_components(a_run_id In Number) Return Varchar2 Is
		l_msg              Varchar2(2000);
		l_component_import component_import_table;
		l_index            Number;
	Begin

		Update ls_import_component Set component_id = ls_component_seq.nextval;

		Update ls_import_component
			Set ls_comp_status_id = 1
		 Where date_received Is Null
			And ls_asset_id Is Null
			And import_run_id = a_run_id;

		Update ls_import_component
			Set ls_comp_status_id = 2
		 Where date_received Is Not Null
			And ls_asset_id Is Null
			And import_run_id = a_run_id;

		Update ls_import_component
			Set ls_comp_status_id = 3
		 Where date_received Is Not Null
			And ls_asset_id Is Not Null
			And import_run_id = a_run_id;

		Select * Bulk Collect
		  Into l_component_import
		  From ls_import_component
		 Where import_run_id = a_run_id;

		Forall l_index In Indices Of l_component_import
			Insert Into ls_component
				(component_id,
				 ls_comp_status_id,
				 company_id,
				 date_received,
				 description,
				 long_description,
				 serial_number,
				 po_number,
				 amount,
				 ls_asset_id,
				 interim_interest_start_date,
				 interim_interest)
				Select l_component_import(l_index).component_id,
						 l_component_import(l_index).ls_comp_status_id,
						 l_component_import(l_index).company_id,
						 to_date(l_component_import(l_index).date_received, 'MM/DD/YYYY'),
						 l_component_import(l_index).description,
						 l_component_import(l_index).long_description,
						 l_component_import(l_index).serial_number,
						 l_component_import(l_index).po_number,
						 l_component_import(l_index).amount,
						 l_component_import(l_index).ls_asset_id,
						 l_component_import(l_index).interim_interest_start_date,
						 l_component_import(l_index).interim_interest
				  From dual;

		Return 'OK';
	Exception
		When Others Then
			l_msg := substr(l_msg || ': ' || Sqlerrm, 1, 2000);
			Return l_msg;

	End f_import_components;

  Function f_validate_components(a_run_id In Number) Return Varchar2 Is
		l_msg              Varchar2(2000);
		l_component_import component_import_table;
		l_index            Number;
	BEGIN
--    update ls_import_component
--    set error_message = error_message || ' duplicate component description.'
--		 where import_run_id = a_run_id
--			and description in
--				 (select description
--					 from (select description, count(*) as my_count
--								from (select description
--										  from ls_import_component
--										union all
--										select description
--										  from (select distinct description
--													 from ls_import_component
--													where import_run_id = a_run_id))
--							  group by description)
--					where my_count > 1);
    RETURN 'OK';
  END;

	--##########################################################################
	--                     LEASE INTERIM INTEREST RATES
	--##########################################################################

  FUNCTION f_import_lease_rates(a_run_id IN NUMBER) RETURN VARCHAR2 IS
    l_msg           VARCHAR2(2000);
    l_interest_import interest_import_table;
    l_index         NUMBER;
  BEGIN
	  Select * Bulk Collect
      Into l_interest_import
      FROM ls_import_lease_interim_rates
      Where import_run_id = a_run_id;

		Forall l_index In Indices Of l_interest_import
			Insert Into ls_lease_interim_rates
				(lease_id,
				 Month,
				 rate)
				Select l_interest_import(l_index).lease_id,
						 to_date(l_interest_import(l_index).month, 'mm/dd/yyyy'),
						 l_interest_import(l_index).rate
				  From dual;
    RETURN 'OK';
  END;

  FUNCTION f_validate_lease_rates(a_run_id IN NUMBER) RETURN VARCHAR2 IS
    l_msg           VARCHAR2(2000);
    l_interest_import interest_import_table;
    l_index         NUMBER;
  BEGIN

    RETURN 'OK';
  END;


End pkg_lease_import;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1216, 0, 10, 4, 3, 0, 38593, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038593_lease_PKG_LEASE_IMPORT.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
