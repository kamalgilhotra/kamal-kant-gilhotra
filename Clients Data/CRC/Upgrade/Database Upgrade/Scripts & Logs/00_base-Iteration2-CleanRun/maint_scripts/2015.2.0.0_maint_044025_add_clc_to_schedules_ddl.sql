/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044025_add_clc_to_schedules_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/15/2015 Will Davis     Add current lease cost to asset and ILR schedules
||============================================================================
*/

alter table ls_ilr_schedule add current_Lease_cost number(22,2);
alter table ls_asset_schedule add current_lease_cost number(22,2);

comment on column ls_ilr_schedule.current_lease_cost is 'The total amount funded on the ILR in the given month';
comment on column ls_asset_schedule.current_lease_cost is 'The total amount funded on the leased asset in the given month';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2606, 0, 2015, 2, 0, 0, 044025, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044025_add_clc_to_schedules_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;