/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047226_lease_update_variable_component_import_field_lookup_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/26/2017 Jared Watkins    update the specific template field to use the correct lookup
||============================================================================
*/

update pp_import_template_fields 
  set import_lookup_id = 2508 
where import_type_id = 264 
  and import_lookup_id is not null 
  and column_name = 'formula_component_id'
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3513, 0, 2017, 1, 0, 0, 47226, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047226_lease_update_variable_component_import_field_lookup_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
