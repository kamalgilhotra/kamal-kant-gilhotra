/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052846_lessor_01_lt_def_profit_rem_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.0.0 02/12/2019 Sarah Byers    new remeasurement schedule field
||============================================================================
*/

-- LSR_ILR_SCHEDULE_DIRECT_FIN
alter table lsr_ilr_schedule_direct_fin add (
lt_deferred_profit_remeasure number(22,2));

comment on column lsr_ilr_schedule_direct_fin.lt_deferred_profit_remeasure is 'The Long Term Deferred Profit Remeasurement amount for the schedule month.';

-- lsr_ilr_prior_schedule_temp
alter table lsr_ilr_prior_schedule_temp add (
lt_deferred_profit_remeasure number(22,2));

comment on column lsr_ilr_prior_schedule_temp.lt_deferred_profit_remeasure is 'The Long Term Deferred Profit Remeasurement amount for the schedule month.';

-- ERR$_lsr_ilr_sch_direct_fin
alter table ERR$_lsr_ilr_sch_direct_fin add (
lt_deferred_profit_remeasure number(22,2));

comment on column ERR$_lsr_ilr_sch_direct_fin.lt_deferred_profit_remeasure is 'The Long Term Deferred Profit Remeasurement amount for the schedule month.';

-- Drop the table type first
DROP TYPE LSR_ILR_DF_SCHEDULE_RESULT_TAB
/
-- LSR_ILR_DF_SCHEDULE_RESULT
CREATE OR REPLACE TYPE LSR_ILR_DF_SCHEDULE_RESULT IS OBJECT(MONTH DATE,
                                                            principal_received NUMBER,
                                                            interest_income_received   NUMBER,
                                                            interest_income_accrued    NUMBER,
                                                            principal_accrued            NUMBER,
                                                            begin_receivable       NUMBER,
                                                            end_receivable           NUMBER,
                                                            begin_lt_receivable          NUMBER,
                                                            end_lt_receivable            NUMBER,
                                                            initial_direct_cost          NUMBER,
                                                            executory_accrual1           NUMBER,
                                                            executory_accrual2           NUMBER,
                                                            executory_accrual3           NUMBER,
                                                            executory_accrual4           NUMBER,
                                                            executory_accrual5           NUMBER,
                                                            executory_accrual6           NUMBER,
                                                            executory_accrual7           NUMBER,
                                                            executory_accrual8           NUMBER,
                                                            executory_accrual9           NUMBER,
                                                            executory_accrual10          NUMBER,
                                                            executory_paid1              NUMBER,
                                                            executory_paid2              NUMBER,
                                                            executory_paid3              NUMBER,
                                                            executory_paid4              NUMBER,
                                                            executory_paid5              NUMBER,
                                                            executory_paid6              NUMBER,
                                                            executory_paid7              NUMBER,
                                                            executory_paid8              NUMBER,
                                                            executory_paid9              NUMBER,
                                                            executory_paid10             NUMBER,
                                                            contingent_accrual1          NUMBER,
                                                            contingent_accrual2          NUMBER,
                                                            contingent_accrual3          NUMBER,
                                                            contingent_accrual4          NUMBER,
                                                            contingent_accrual5          NUMBER,
                                                            contingent_accrual6          NUMBER,
                                                            contingent_accrual7          NUMBER,
                                                            contingent_accrual8          NUMBER,
                                                            contingent_accrual9          NUMBER,
                                                            contingent_accrual10         NUMBER,
                                                            contingent_paid1             NUMBER,
                                                            contingent_paid2             NUMBER,
                                                            contingent_paid3             NUMBER,
                                                            contingent_paid4             NUMBER,
                                                            contingent_paid5             NUMBER,
                                                            contingent_paid6             NUMBER,
                                                            contingent_paid7             NUMBER,
                                                            contingent_paid8             NUMBER,
                                                            contingent_paid9             NUMBER,
                                                            contingent_paid10            NUMBER,
                                                            begin_unguaranteed_residual NUMBER,
                                                            int_on_unguaranteed_residual NUMBER,
                                                            end_unguaranteed_residual NUMBER,
                                                            begin_net_investment NUMBER,
                                                            int_on_net_investment NUMBER,
                                                            end_net_investment NUMBER,
                                                            begin_deferred_profit number,
                                                            recognized_profit number,
                                                            end_deferred_profit number,
                                                            receivable_remeasurement number,
                                                            lt_receivable_remeasurement number,
                                                            unguaran_residual_remeasure number,
                                                            rate_implicit FLOAT,
                                                            discount_rate FLOAT,
                                                            rate_implicit_ni FLOAT,
                                                            discount_rate_ni FLOAT,
                                                            begin_lease_receivable number,
                                                            original_net_investment number,
                                                            npv_lease_payments NUMBER,
                                                            npv_guaranteed_residual NUMBER,
                                                            npv_unguaranteed_residual NUMBER,
                                                            selling_profit_loss NUMBER,
                                                            cost_of_goods_sold NUMBER,
                                                            begin_lt_deferred_profit NUMBER,
                                                            end_lt_deferred_profit NUMBER,
                                                            lt_deferred_profit_remeasure NUMBER,
                                                            schedule_rates t_lsr_ilr_schedule_all_rates)
/

-- Recreate the table type
CREATE OR REPLACE TYPE LSR_ILR_DF_SCHEDULE_RESULT_TAB AS TABLE OF LSR_ILR_DF_SCHEDULE_RESULT
/


-- Add fields to the view
CREATE OR REPLACE VIEW V_LSR_ILR_MC_SCHEDULE
(ilr_id, ilr_number, lease_id, lease_number, current_revision, revision, set_of_books_id, month, company_id, open_month, ls_cur_type, exchange_date, 
prev_exchange_date, contract_currency_id, display_currency_id, rate, calculated_rate, previous_calculated_rate, iso_code, currency_display_symbol, 
interest_income_received, interest_income_accrued, beg_deferred_rent, deferred_rent, end_deferred_rent, beg_accrued_rent, accrued_rent, end_accrued_rent, 
beg_receivable, end_receivable, beg_long_term_receivable, end_long_term_receivable,deferred_rev_activity,interest_rental_recvd_spread,end_deferred_rev,
initial_direct_cost, executory_accrual1, executory_accrual2, 
executory_accrual3, executory_accrual4, executory_accrual5, executory_accrual6, executory_accrual7, executory_accrual8, executory_accrual9, 
executory_accrual10, executory_paid1, executory_paid2, executory_paid3, executory_paid4, executory_paid5, executory_paid6, executory_paid7, 
executory_paid8, executory_paid9, executory_paid10, contingent_accrual1, contingent_accrual2, contingent_accrual3, contingent_accrual4, 
contingent_accrual5, contingent_accrual6, contingent_accrual7, contingent_accrual8, contingent_accrual9, contingent_accrual10, contingent_paid1, 
contingent_paid2, contingent_paid3, contingent_paid4, contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8, contingent_paid9, 
contingent_paid10, principal_received, principal_accrued, beg_unguaranteed_residual, interest_unguaranteed_residual, ending_unguaranteed_residual, 
beg_net_investment, interest_net_investment, ending_net_investment, beginning_deferred_profit, recognized_profit, ending_deferred_profit, 
beginning_lt_deferred_profit, ending_lt_deferred_profit,
rates_exchange_date, rates_rate, rates_last_exchange_date, rates_last_rate,average_rate,ST_CURRENCY_GAIN_LOSS,LT_CURRENCY_GAIN_LOSS, gain_loss_fx,
receivable_remeasurement, unguaran_residual_remeasure, lt_deferred_profit_remeasure)
AS
WITH cur AS ( SELECT ls_currency_type_id AS ls_cur_type,
     currency_id,
     currency_display_symbol,
     iso_code,
     CASE
    ls_currency_type_id
    WHEN
    1
    THEN
    1
    ELSE
    NULL
  END
  AS contract_approval_rate
FROM currency CROSS JOIN ls_lease_currency_type),open_month AS ( SELECT company_id,
     MIN(gl_posting_mo_yr) open_month
FROM lsr_process_control WHERE open_next IS NULL GROUP BY
  company_id
),calc_rate AS (  SELECT Nvl(a.company_id, b.company_id) company_id,
           Nvl(a.contract_currency_id, b.contract_currency_id) contract_currency_id,
           Nvl(a.company_currency_id, b.company_currency_id) company_currency_id,
           Nvl(a.accounting_month, Add_Months(b.accounting_month,1)) accounting_month,
           nvl(a.exchange_rate_type_id, b.exchange_rate_type_id) exchange_rate_type_id,
           Nvl(a.exchange_date, b.exchange_date) exchange_date,
           a.rate,
           b.rate prev_rate
      FROM ls_lease_calculated_date_rates a
        FULL OUTER JOIN ls_lease_calculated_date_rates b ON a.company_id = b.company_id
        AND a.contract_currency_id = b.contract_currency_id
        AND a.accounting_month = add_months(b.accounting_month,1)
        and b.exchange_rate_type_id = a.exchange_rate_type_id),
rate_now AS ( SELECT currency_from,
     currency_to,
     rate
FROM ( SELECT currency_from,
     currency_to,
     rate,
     ROW_NUMBER() OVER(PARTITION BY
    currency_from,
    currency_to
    ORDER BY
    exchange_date
    DESC
  ) AS rn
  FROM currency_rate_default_dense WHERE trunc(exchange_date,'MONTH') <= trunc(SYSDATE,'MONTH')
  AND exchange_rate_type_id = 1
  )
WHERE rn = 1 )
SELECT schedule.ilr_id ilr_id,
  schedule.ilr_number,
  lease.lease_id,
  lease.lease_number,
  schedule.current_revision,
  schedule.revision revision,
  schedule.set_of_books_id set_of_books_id,
  schedule.month month,
  open_month.company_id,
  open_month.open_month,
  cur.ls_cur_type AS ls_cur_type,
  rates.exchange_date,
  calc_rate.exchange_date prev_exchange_date,
  lease.contract_currency_id,
  cur.currency_id display_currency_id,
  rates.rate,
  calc_rate.rate calculated_rate,
  calc_rate.prev_rate previous_calculated_rate,
  cur.iso_code,
  cur.currency_display_symbol,
  schedule.interest_income_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) interest_income_received,
  schedule.interest_income_accrued * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) interest_income_accrued,
  schedule.beg_deferred_rent * nvl(calc_rate.prev_rate,rates_last.rate) beg_deferred_rev,
  schedule.deferred_rent * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) deferred_rent,
  schedule.end_deferred_rent * nvl(calc_rate.rate,rates.rate) end_deferred_rent,
  schedule.beg_accrued_rent * nvl(calc_rate.prev_rate,rates_last.rate) beg_accrued_rent,
  schedule.accrued_rent * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) accrued_rent,
  schedule.end_accrued_rent * nvl(calc_rate.rate,rates.rate) end_accrued_rent,
  schedule.beg_receivable * nvl(calc_rate.prev_rate,rates_last.rate) beg_receivable,
  schedule.end_receivable * nvl(calc_rate.rate,rates.rate) end_receivable,
  schedule.beg_lt_receivable * nvl(calc_rate.prev_rate,rates_last.rate) beg_long_term_receivable,
  schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate) end_long_term_receivable,
  schedule.deferred_rev_activity * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) deferred_rev_activity,
  schedule.interest_rental_recvd_spread * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) interest_rental_recvd_spread,
  schedule.end_deferred_rev * nvl(calc_rate.rate,rates.rate) end_deferred_rev,
  schedule.initial_direct_cost * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) initial_direct_cost,
  schedule.executory_accrual1 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual1,
  schedule.executory_accrual2 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual2,
  schedule.executory_accrual3 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual3,
  schedule.executory_accrual4 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual4,
  schedule.executory_accrual5 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual5,
  schedule.executory_accrual6 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual6,
  schedule.executory_accrual7 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual7,
  schedule.executory_accrual8 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual8,
  schedule.executory_accrual9 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual9,
  schedule.executory_accrual10 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual10,
  schedule.executory_paid1 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid1,
  schedule.executory_paid2 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid2,
  schedule.executory_paid3 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid3,
  schedule.executory_paid4 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid4,
  schedule.executory_paid5 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid5,
  schedule.executory_paid6 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid6,
  schedule.executory_paid7 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid7,
  schedule.executory_paid8 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid8,
  schedule.executory_paid9 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid9,
  schedule.executory_paid10 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid10,
  schedule.contingent_accrual1 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual1,
  schedule.contingent_accrual2 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual2,
  schedule.contingent_accrual3 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual3,
  schedule.contingent_accrual4 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual4,
  schedule.contingent_accrual5 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual5,
  schedule.contingent_accrual6 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual6,
  schedule.contingent_accrual7 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual7,
  schedule.contingent_accrual8 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual8,
  schedule.contingent_accrual9 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual9,
  schedule.contingent_accrual10 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual10,
  schedule.contingent_paid1 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid1,
  schedule.contingent_paid2 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid2,
  schedule.contingent_paid3 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid3,
  schedule.contingent_paid4 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid4,
  schedule.contingent_paid5 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid5,
  schedule.contingent_paid6 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid6,
  schedule.contingent_paid7 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid7,
  schedule.contingent_paid8 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid8,
  schedule.contingent_paid9 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid9,
  schedule.contingent_paid10 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid10,
  schedule.principal_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) principal_received,
  schedule.principal_accrued * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) principal_accrued,
  schedule.beg_unguaranteed_residual * nvl(calc_rate.prev_rate,rates_last.rate) beg_unguaranteed_residual,
  schedule.interest_unguaranteed_residual * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) interest_unguaranteed_residual,
  schedule.ending_unguaranteed_residual * nvl(calc_rate.rate,rates.rate) ending_unguaranteed_residual,
  schedule.beg_net_investment * nvl(calc_rate.prev_rate,rates_last.rate) beg_net_investment,
  schedule.interest_net_investment * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) interest_net_investment,
  schedule.ending_net_investment * nvl(calc_rate.rate,rates.rate) ending_net_investment,
  schedule.begin_deferred_profit * nvl(calc_rate.prev_rate,rates_last.rate) beginning_deferred_profit,
  schedule.recognized_profit * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) recognized_profit,
  schedule.end_deferred_profit * nvl(calc_rate.rate,rates.rate) ending_deferred_profit,
  schedule.begin_lt_deferred_profit * nvl(calc_rate.prev_rate,rates_last.rate) beginning_lt_deferred_profit,
  schedule.end_lt_deferred_profit * nvl(calc_rate.rate,rates.rate) ending_lt_deferred_profit,
  rates.exchange_date,
  rates.rate,
  rates_last.exchange_date,
  rates_last.rate,
  Decode(lower(sc.CONTROL_VALUE), 'yes', CALC_AVG_RATE.rate, cr_avg.rate) average_rate,
  case when lease.contract_currency_id = cs.currency_id then 0
  when pkg_pp_system_control.f_pp_system_control_company('Lease: Contract Currency JEs', OPEN_MONTH.COMPANY_ID) = 'yes' then 0   
  else
    case fasb.fasb_cap_type_id 
    when 1 then 
    (
        ((schedule.end_receivable * nvl(calc_rate.rate,rates.rate)) - (schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate)))
        - ((schedule.beg_receivable * nvl(calc_rate.prev_rate,rates_last.rate)) - (schedule.beg_lt_receivable * nvl(calc_rate.prev_rate,rates_last.rate)))
        + (schedule.interest_income_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)))
    - ((schedule.beg_lt_receivable - schedule.end_lt_receivable) * nvl(calc_rate.rate, rates.rate))
    ) 
    else
  (
        ((schedule.end_receivable * nvl(calc_rate.rate,rates.rate)) - (schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate)))
    + (schedule.ending_unguaranteed_residual * nvl(calc_rate.rate,rates.rate))
    - ((schedule.beg_receivable * nvl(calc_rate.prev_rate,rates_last.rate)) - (schedule.beg_lt_receivable * nvl(calc_rate.prev_rate,rates_last.rate)))
    - (schedule.beg_unguaranteed_residual * nvl(calc_rate.prev_rate,rates_last.rate))
    - (schedule.interest_income_accrued * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate))) 
    - (schedule.interest_unguaranteed_residual * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)))
    + (schedule.principal_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate))) 
    + (schedule.interest_income_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)))
    - ((schedule.beg_lt_receivable - schedule.end_lt_receivable) * nvl(calc_rate.rate, rates.rate))
    )
    end 
  end as st_currency_gain_loss,
  case when lease.contract_currency_id = cs.currency_id then 0
  when pkg_pp_system_control.f_pp_system_control_company('Lease: Contract Currency JEs', OPEN_MONTH.COMPANY_ID) = 'yes' then 0
  else
  (
      (schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate))
    - (schedule.beg_lt_receivable * nvl(calc_rate.prev_rate,rates_last.rate))
    + ((schedule.beg_lt_receivable - schedule.end_lt_receivable) * nvl(calc_rate.rate, rates.rate))
  )
  end as lt_currency_gain_loss,
  case when lease.contract_currency_id = cs.currency_id then 0
  when pkg_pp_system_control.f_pp_system_control_company('Lease: Contract Currency JEs', OPEN_MONTH.COMPANY_ID) = 'yes' then 0
  else
  case fasb.fasb_cap_type_id 
  when 1 then 
  (
      ((schedule.end_receivable * nvl(calc_rate.rate,rates.rate)) - (schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate)))
    - ((schedule.beg_receivable * nvl(calc_rate.prev_rate,rates_last.rate)) - (schedule.beg_lt_receivable * nvl(calc_rate.prev_rate,rates_last.rate)))
    + (schedule.interest_income_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)))
    - ((schedule.beg_lt_receivable - schedule.end_lt_receivable) * nvl(calc_rate.rate, rates.rate))
  ) 
  else
  (
      ((schedule.end_receivable * nvl(calc_rate.rate,rates.rate)) - (schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate)))
    + (schedule.ending_unguaranteed_residual * nvl(calc_rate.rate,rates.rate))
    - ((schedule.beg_receivable * nvl(calc_rate.prev_rate,rates_last.rate)) - (schedule.beg_lt_receivable * nvl(calc_rate.prev_rate,rates_last.rate)))
    - (schedule.beg_unguaranteed_residual * nvl(calc_rate.prev_rate,rates_last.rate))
    - (schedule.interest_income_accrued * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate))) 
    - (schedule.interest_unguaranteed_residual * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)))
    + (schedule.principal_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate))) 
    + (schedule.interest_income_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)))
    - ((schedule.beg_lt_receivable - schedule.end_lt_receivable) * nvl(calc_rate.rate, rates.rate))
  )
  end 
  end +
  case lease.contract_currency_id
  when cs.currency_id then 0
  else
  (
      (schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate))
    - (schedule.beg_lt_receivable * nvl(calc_rate.prev_rate,rates_last.rate))
    + ((schedule.beg_lt_receivable - schedule.end_lt_receivable) * nvl(calc_rate.rate, rates.rate))
  )
  end as gain_loss_fx,
  schedule.receivable_remeasurement * nvl(calc_rate.rate,rates.rate) as receivable_remeasurement,
  schedule.unguaran_residual_remeasure * nvl(calc_rate.rate,rates.rate) as unguaran_residual_remeasure,
  schedule.lt_deferred_profit_remeasure * nvl(calc_rate.rate,rates.rate) as lt_deferred_profit_remeasure
FROM (SELECT schedule.ilr_id,
       ilr.ilr_number,
       ilr.current_revision,
       schedule.revision,
       schedule.set_of_books_id,
       schedule.month,
       schedule.interest_income_received,
       schedule.interest_income_accrued,
       schedule.beg_deferred_rent,
       schedule.deferred_rent,
       schedule.end_deferred_rent,
       schedule.beg_accrued_rent,
       schedule.accrued_rent,
       schedule.end_accrued_rent,
       schedule.beg_receivable,
       schedule.end_receivable,
       schedule.beg_lt_receivable,
       schedule.end_lt_receivable,
       schedule.deferred_rev_activity,
       schedule.interest_rental_recvd_spread,
       schedule.end_deferred_rev,
       schedule.initial_direct_cost,
       schedule.executory_accrual1,
       schedule.executory_accrual2,
       schedule.executory_accrual3,
       schedule.executory_accrual4,
       schedule.executory_accrual5,
       schedule.executory_accrual6,
       schedule.executory_accrual7,
       schedule.executory_accrual8,
       schedule.executory_accrual9,
       schedule.executory_accrual10,
       schedule.executory_paid1,
       schedule.executory_paid2,
       schedule.executory_paid3,
       schedule.executory_paid4,
       schedule.executory_paid5,
       schedule.executory_paid6,
       schedule.executory_paid7,
       schedule.executory_paid8,
       schedule.executory_paid9,
       schedule.executory_paid10,
       schedule.contingent_accrual1,
       schedule.contingent_accrual2,
       schedule.contingent_accrual3,
       schedule.contingent_accrual4,
       schedule.contingent_accrual5,
       schedule.contingent_accrual6,
       schedule.contingent_accrual7,
       schedule.contingent_accrual8,
       schedule.contingent_accrual9,
       schedule.contingent_accrual10,
       schedule.contingent_paid1,
       schedule.contingent_paid2,
       schedule.contingent_paid3,
       schedule.contingent_paid4,
       schedule.contingent_paid5,
       schedule.contingent_paid6,
       schedule.contingent_paid7,
       schedule.contingent_paid8,
       schedule.contingent_paid9,
       schedule.contingent_paid10,
       schedule.receivable_remeasurement,
       st_schedule.principal_received,
       st_schedule.principal_accrued,
       st_schedule.beg_unguaranteed_residual,
       st_schedule.interest_unguaranteed_residual,
       st_schedule.ending_unguaranteed_residual,
       st_schedule.beg_net_investment,
       st_schedule.interest_net_investment,
       st_schedule.ending_net_investment,
       st_schedule.unguaran_residual_remeasure,
       df_schedule.begin_deferred_profit,
       df_schedule.recognized_profit,
       df_schedule.end_deferred_profit,
       df_schedule.begin_lt_deferred_profit,
       df_schedule.end_lt_deferred_profit,
       df_schedule.lt_deferred_profit_remeasure,
       ilr.lease_id,
       ilr.company_id,
       options.in_service_exchange_rate,
       options.purchase_option_amt,
       options.termination_amt,
       options.lease_cap_type_id,
       schedule.rowid AS lisrowid,
       options.rowid AS optrowid,
       ilr.rowid AS ilrrowid,
       lease.rowid AS leaserowid,
       st_schedule.rowid AS salesrowid
    FROM lsr_ilr_schedule schedule,
       lsr_ilr_options options,
       lsr_ilr ilr,
       lsr_lease lease,
       lsr_ilr_schedule_sales_direct st_schedule,
       lsr_ilr_schedule_direct_fin df_schedule
    WHERE schedule.ilr_id = options.ilr_id
      AND schedule.revision = options.revision
      AND schedule.ilr_id = ilr.ilr_id
      AND ilr.lease_id = lease.lease_id
      AND st_schedule.ilr_id (+) = schedule.ilr_id
      AND st_schedule.revision (+) = schedule.revision
      AND st_schedule.month (+) = schedule.month
      AND st_schedule.set_of_books_id (+) = schedule.set_of_books_id
      AND df_schedule.ilr_id (+) = schedule.ilr_id
      AND df_schedule.revision (+) = schedule.revision
      AND df_schedule.month (+) = schedule.month
      AND df_schedule.set_of_books_id (+) = schedule.set_of_books_id
  ) schedule
INNER JOIN lsr_lease lease 
  ON schedule.lease_id = lease.lease_id
inner join lsr_fasb_type_sob fasb
  on schedule.lease_cap_type_id = fasb.cap_type_id
  and fasb.set_of_books_id = schedule.set_of_books_id
  INNER JOIN currency_schema cs 
  ON schedule.company_id = cs.company_id
  INNER JOIN cur ON cur.currency_id =
  CASE cur.ls_cur_type
    WHEN 1 THEN lease.contract_currency_id
    WHEN 2 THEN cs.currency_id
  END
  INNER JOIN open_month 
  ON schedule.company_id = open_month.company_id
  INNER JOIN currency_rate_default_dense rates 
  ON cur.currency_id = rates.currency_to
  AND lease.contract_currency_id = rates.currency_from
  AND trunc(rates.exchange_date,'MONTH') = trunc(schedule.month,'MONTH')
  INNER JOIN currency_rate_default_dense rates_last 
  ON cur.currency_id = rates_last.currency_to
  AND lease.contract_currency_id = rates_last.currency_from
  AND trunc(rates_last.exchange_date,'MONTH') = trunc(Add_Months(schedule.MONTH, -1),'MONTH')
  INNER JOIN rate_now 
  ON cur.currency_id = rate_now.currency_to
  AND lease.contract_currency_id = rate_now.currency_from
  inner join pp_system_control_companies sc 
  ON open_month.company_id = sc.company_id
  AND lower(trim(sc.control_name)) = 'lease mc: use average rates'
  LEFT OUTER JOIN calc_rate 
  ON lease.contract_currency_id = calc_rate.contract_currency_id
  AND cur.currency_id = calc_rate.company_currency_id
  AND schedule.company_id = calc_rate.company_id
  AND schedule.month = calc_rate.accounting_month
  and calc_rate.exchange_rate_type_id = 1
  inner join CURRENCY_RATE_DEFAULT_DENSE cr_avg 
  ON CUR.currency_id = cr_avg.currency_to
  AND lease.contract_currency_id = cr_avg.currency_from
  AND Trunc( cr_avg.exchange_date, 'MONTH' ) = Trunc( schedule.month, 'MONTH' )
  and cr_avg.exchange_rate_type_id = decode(lower(sc.control_value),'yes',4,1)
  left outer join calc_rate calc_avg_rate
  ON lease.contract_currency_id = calc_avg_rate.contract_currency_id
  AND CUR.currency_id = calc_avg_rate.company_currency_id
  AND schedule.company_id = calc_avg_rate.company_id
  AND schedule.month = calc_avg_rate.accounting_month
  and calc_avg_rate.exchange_rate_type_id = 4
WHERE cs.currency_type_id = 1
  AND rates.exchange_rate_type_id = 1
  AND rates_last.exchange_rate_type_id = 1;
  
-- Add the new trans types to PP_GL_TRANSACTION
create or replace function PP_GL_TRANSACTION(A_ACTIVITY           number,
                                             A_LDG_ASSET_ID       number,
                                             A_LDG_ASSET_ACT_ID   number,
                                             A_LDG_DEPR_GROUP_ID  number,
                                             A_LDG_WORK_ORDER_ID  number,
                                             A_LDG_GL_ACCOUNT_ID  number,
                                             A_GAIN_LOSS_IND      number,
                                             A_LDG_PEND_TRANS_ID  number,
                                             A_JE_METHOD_ID       number,
                                             A_JE_SET_OF_BOOKS_ID number,
                                             A_JE_REVERSAL        number,
                                             A_JE_AMOUNT_TYPE     number,
                                             A_LSR_ILR_ID         number DEFAULT NULL) return varchar2 is

/***************************************************************************************
 FUNCTION:  PP_GL_TRANSACTION
 CLIENT:    Generic Starting Function

 CUSTOM DESCRIPTION:

 Function Design:

 By default this function will look at PP_JOURNAL_LAYOUTS by looping over the
 CR_ELEMENTS table and create an accounting string that is fixed width and padded to match
 the cr accounting key setup and is delimited by a dash

 For each journal entry type the function will look up the activity in the PP_JOURNAL_LAYOUTS
 table for the specific company.  If no row exists for the company it will look at comapny -1
 (All Companies).  A company value of -2 will result in an "IGNORE" being used for the gl_transaction table.
 If none of those exist an error will be returned.
***************************************************************************************/

   GL_ACCOUNT_STR varchar2(2000);
   ACCT_TYPE      number(22);

   WO_ID        number(22);
   BS_ID        number(22);
   CO_ID        number(22);
   DG_ID        number(22);
   GL_ID        number(22);
   UA_ID        number(22);
   CE_ID        number(22);
   L_CHECK      number(22);
   L_CHECK2     number(22);
   I            number(22);
   J            number(22);
   K            number(22);
   PP_ERR_CODE  varchar2(1000);
   DELIMITER    varchar2(1);
   RTN          varchar2(254);
   NOTES        varchar2(254);
   LAYOUT_CO    number(22);
   SQL_STMT     varchar2(4000);
   KEYWORD_VAL  varchar2(35);
   ARG_STR      varchar2(35);
   ELEMENT_STR  varchar2(35);
   CNT_VAL      varchar2(35);
   IS_KEYWORD   varchar2(35);
   BINDING_ARG1 number;
   BINDING_ARG2 number;
   BINDING_ARG3 number;
   BINDING_ARG4 number;
   PARAM_COUNT  number;
   ARG_COUNT    number;

   type V_ARRAY is table of varchar2(51) index by binary_integer;
   type N_ARRAY is table of number(22) index by binary_integer;
   ACCOUNTING_KEY V_ARRAY;
   ELEMENTS       V_ARRAY;
   DEFAULTS       V_ARRAY;
   WIDTHS         N_ARRAY;
   PARAMS         N_ARRAY;
   BINDING_ARG    N_ARRAY;

begin

   PP_ERR_CODE := '001:' || A_LDG_PEND_TRANS_ID;

   /* Get Company from Pend Trans .
   If activity is lease specifc activity without a pend trans,
      then do not get from pend_trans
      get from lease
   If activity is Regulated ARO activity without a pend trans,
      then get from depr group
   */
   if a_activity in (41,42) then
      --Regulated ARO COR calculation doesn't create a pending transaction, so use depr group
      select A.BUS_SEGMENT_ID, A.COMPANY_ID
      into BS_ID, CO_ID
      from DEPR_GROUP A
      where a.DEPR_GROUP_ID = A_LDG_DEPR_GROUP_ID;
   elsif a_activity <= 3000 then
      select A.BUS_SEGMENT_ID, A.COMPANY_ID
      into BS_ID, CO_ID
      from PEND_TRANSACTION A
      where A.PEND_TRANS_ID = A_LDG_PEND_TRANS_ID;
   elsif a_activity in (3025, 3026, 3030, 3031, 3034, 3035, 3043, 3044, 3047, 3048) then
      -- leased asset retirements, transfers, gain loss have pend transactions.
    begin
      select A.BUS_SEGMENT_ID, A.COMPANY_ID
      into BS_ID, CO_ID
      from PEND_TRANSACTION A
      where A.PEND_TRANS_ID = A_LDG_PEND_TRANS_ID;

    EXCEPTION WHEN No_Data_Found THEN
      select A.BUS_SEGMENT_ID, A.COMPANY_ID
      into BS_ID, CO_ID
      from LS_PEND_TRANSACTION A
      where A.LS_PEND_TRANS_ID = A_LDG_PEND_TRANS_ID;
    END;
   elsif (a_activity in (3001, 3002, 3003, 3042) AND A_LDG_PEND_TRANS_ID <> -1) then
      -- leased asset additions have a different pend transaction table
      select A.BUS_SEGMENT_ID, A.COMPANY_ID
      into BS_ID, CO_ID
      from LS_PEND_TRANSACTION A
      where A.LS_PEND_TRANS_ID = A_LDG_PEND_TRANS_ID;
   elsif a_activity in (4001, 4002, 4003, 4004, 4005, 4006, 4007, 4008, 4009,
                        4010, 4011, 4012, 4013, 4014, 4015, 4016, 4017, 4018, 4019,
            4020, 4021, 4022, 4025, 4026, 4027, 4028,
            4030, 4031, 4032, 4033, 4034, 4035, 4036, 4037,
            4040, 4041, 4042, 4043, 4044, 4045, 4046, 4047,
            4051, 4052, 4055, 4056, 4057,
            4060, 4061, 4062, 4063, 4064, 4065, 4066, 4067, 4068, 4069,
                                                4070, 4071, 4072, 4073, 4074, 4075, 4076, 4077,
                                                4078, 4079, 4080, 4081, 4082, 4083) then
      --get company_id from lessor ILR_ID
      select A.COMPANY_ID
      into CO_ID
      from LSR_ILR A
      where A.ilr_id = A_LSR_ILR_ID;
   else
      -- get company / business segment from the leased asset
      select A.BUS_SEGMENT_ID, A.COMPANY_ID
      into BS_ID, CO_ID
      from LS_ASSET A
      where A.LS_ASSET_ID = A_LDG_ASSET_ID;
   end if;

   PP_ERR_CODE := '002:' || A_ACTIVITY;

   /* Determine which row in the PP_JOURNAL_LAYOUTS to Use */
   select count(*)
     into L_CHECK
     from PP_JOURNAL_LAYOUTS
    where TRANS_TYPE = A_ACTIVITY
      and COMPANY_ID = CO_ID
      and JE_METHOD_ID = A_JE_METHOD_ID;

   PP_ERR_CODE := '002a';

   if L_CHECK = 1 then
      LAYOUT_CO := CO_ID;
   else
      L_CHECK     := 0;
      PP_ERR_CODE := '002b:' || A_ACTIVITY;

      select count(*)
        into L_CHECK
        from PP_JOURNAL_LAYOUTS
       where TRANS_TYPE = TO_NUMBER(A_ACTIVITY)
         and COMPANY_ID = -1
         and JE_METHOD_ID = A_JE_METHOD_ID;

      PP_ERR_CODE := '002bb';

      if L_CHECK = 1 then
         LAYOUT_CO := -1;
      else
         PP_ERR_CODE := '002bbb';
         L_CHECK     := 0;

         select count(*)
           into L_CHECK
           from PP_JOURNAL_LAYOUTS
          where TRANS_TYPE = A_ACTIVITY
            and COMPANY_ID = -2
            and JE_METHOD_ID = A_JE_METHOD_ID;

         if L_CHECK = 1 then
            LAYOUT_CO := -2;
            /* Ignore Row Found */
            return 'IGNORE: Entry has been setup to be ignored in the PP_JOURNAL_LAYOUTS table';
         else
            /* Error Now Row Found */
            return 'ERROR: No record found in the PP_JOURNAL_LAYOUTS for trans_type=' || A_ACTIVITY || ' and company=' || CO_ID || ' or default -1 company';
         end if;
      end if;
   end if;

   /* Place parameters into an array for use in binding arguments */
   PP_ERR_CODE := 'Passing params in array';
   PARAMS(1) := A_ACTIVITY;
   PARAMS(2) := A_LDG_ASSET_ID;
   PARAMS(3) := A_LDG_ASSET_ACT_ID;
   PARAMS(4) := A_LDG_DEPR_GROUP_ID;
   PARAMS(5) := A_LDG_WORK_ORDER_ID;
   PARAMS(6) := A_LDG_GL_ACCOUNT_ID;
   PARAMS(7) := A_GAIN_LOSS_IND;
   PARAMS(8) := A_LDG_PEND_TRANS_ID;
   PARAMS(9) := A_JE_METHOD_ID;
   PARAMS(10) := A_JE_SET_OF_BOOKS_ID;
   PARAMS(11) := A_JE_REVERSAL;
   PARAMS(12) := A_JE_AMOUNT_TYPE;
   PARAMS(13) := A_LSR_ILR_ID;

   /* Determine system control for gl journal category */
   PP_ERR_CODE := 'Finding system control';
   select UPPER(RTRIM(CONTROL_VALUE))
     into CNT_VAL
     from CR_SYSTEM_CONTROL
    where UPPER(RTRIM(CONTROL_NAME)) = 'PPTOCR: GLJC IN GL ACCOUNT';

   /* Get CR Element Details */
   PP_ERR_CODE := 'elements';
   select replace(replace(replace(LOWER(DESCRIPTION), ' ', '_'), '-', '_'), '/', '_') bulk collect
     into ELEMENTS
     from CR_ELEMENTS
    order by "ORDER";

   PP_ERR_CODE := 'widths';
   select WIDTH bulk collect into WIDTHS from CR_ELEMENTS order by "ORDER";

   PP_ERR_CODE := 'defaults';
   select NVL(DEFAULT_VALUE, ' ') bulk collect into DEFAULTS from CR_ELEMENTS order by "ORDER";

   /* Add gl_journal_category to account string */
   PP_ERR_CODE := 'extending arrays';
   if CNT_VAL = 'YES' then
      ELEMENTS(ELEMENTS.LAST + 1) := 'gl_journal_category';
      WIDTHS(WIDTHS.LAST + 1) := '35';
      DEFAULTS(DEFAULTS.LAST + 1) := '0';
   end if;

   /* Get Values from PP_JOURNAL_LAYOUTS */
   for I in ELEMENTS.FIRST .. ELEMENTS.LAST
   loop

      PP_ERR_CODE := 'Convert to upper' || ELEMENTS(I);
      ELEMENTS(I) := '"' || UPPER(ELEMENTS(I)) || '"'; /* Account for reserved words */

      PP_ERR_CODE := 'pp_journal_layouts' || ELEMENTS(I);
      execute immediate 'select ' || ELEMENTS(I) || ' from PP_JOURNAL_LAYOUTS ' ||
                        ' where trans_type = ' || A_ACTIVITY || ' and company_id = ' || LAYOUT_CO ||
                        ' and je_method_id = ' || A_JE_METHOD_ID
         into ACCOUNTING_KEY(I);

      PP_ERR_CODE := 'check if null, element ' || I || ELEMENTS(I) || trim(ACCOUNTING_KEY(I));
      if trim(ACCOUNTING_KEY(I)) = '' or ACCOUNTING_KEY(I) is null then
         PP_ERR_CODE := 'Inside if statement null check';
         ACCOUNTING_KEY(I) := DEFAULTS(I);
      end if;

      /* Now do values from key words */
      PP_ERR_CODE := 'accounting_key' || ACCOUNTING_KEY(I);
      KEYWORD_VAL := ACCOUNTING_KEY(I);

      PP_ERR_CODE := 'Error in selecting whether or not a keyword exists';
      select NVL((select KEYWORD
                   from PP_JOURNAL_KEYWORDS
                  where KEYWORD = KEYWORD_VAL
                    and KEYWORD_TYPE = 1),
                 null)
        into IS_KEYWORD
        from DUAL;

      /* If I am harcoding a value */
      if IS_KEYWORD is null then

         SQL_STMT := 'select ' || ELEMENTS(I) || ' from PP_JOURNAL_LAYOUTS ' ||
                     'where trans_type = ' || A_ACTIVITY || ' and company_id = ' || LAYOUT_CO ||
                     ' and je_method_id = ' || A_JE_METHOD_ID;

         /* Begin logic for existing keywords */
      else
         PP_ERR_CODE := KEYWORD_VAL || I || 'keyword_val';
         select SQLS,
                BIND_ARG_CODE_ID1,
                BIND_ARG_CODE_ID2,
                BIND_ARG_CODE_ID3,
                BIND_ARG_CODE_ID4,
                (NVL2(BIND_ARG_CODE_ID1, 1, 0) + NVL2(BIND_ARG_CODE_ID2, 1, 0) +
                NVL2(BIND_ARG_CODE_ID3, 1, 0) + NVL2(BIND_ARG_CODE_ID4, 1, 0))
           into SQL_STMT, BINDING_ARG1, BINDING_ARG2, BINDING_ARG3, BINDING_ARG4, ARG_COUNT
           from PP_JOURNAL_KEYWORDS
          where KEYWORD = KEYWORD_VAL;

         /* Fill defaults acts like bind arg */
         SQL_STMT := replace(SQL_STMT, '<default>', DEFAULTS(I));

         PP_ERR_CODE := 'Passing binding args in array';
         BINDING_ARG(1) := BINDING_ARG1;
         BINDING_ARG(2) := BINDING_ARG2;
         BINDING_ARG(3) := BINDING_ARG3;
         BINDING_ARG(4) := BINDING_ARG4;

         PP_ERR_CODE := 'Passing binding default in array' || I;

         /* Replace arguments with actual values */
         for J in 1 .. ARG_COUNT
         loop
            ARG_STR  := '<arg' || J || '>';
            SQL_STMT := replace(SQL_STMT, ARG_STR, Nvl(To_Char(PARAMS(BINDING_ARG(J))),'NULL'));
         end loop;

         PP_ERR_CODE := 'Passing elements into array postition ' || I;

         /* Replace elements with previous calculated values */
         for K in 1 .. (I - 1)
         loop
            ELEMENT_STR := '<element' || K || '>';
            SQL_STMT    := replace(SQL_STMT, ELEMENT_STR, ACCOUNTING_KEY(K));
         end loop;

      end if; -- End logic for existing keywords

      DBMS_OUTPUT.PUT_LINE(SQL_STMT);
      DBMS_OUTPUT.PUT(CHR(10));

      /* Execute sql block */
      PP_ERR_CODE := 'Element ' || ELEMENTS(I) || 'for trans_type = ' || A_ACTIVITY ||
                     ' and company_id = ' || LAYOUT_CO || ' and je_method_id = ' || A_JE_METHOD_ID;
      execute immediate SQL_STMT
         into ACCOUNTING_KEY(I);

   end loop;

   /* Now Add Specific Logic for Implementation */

   /* Oracle requires something in each part of the if statement, so put a NULL command  */
   if A_ACTIVITY = 1000 then
      /* addition debit */
      NULL;
   elsif A_ACTIVITY = 1001 then
      /* addition credit */
      NULL;
   elsif A_ACTIVITY = 1002 then
      /* retirement debit */
      NULL;
   elsif A_ACTIVITY = 1003 then
      /* retirement credit */
      NULL;
   elsif A_ACTIVITY = 1004 then
      /* COR debit */
      NULL;
   elsif A_ACTIVITY = 1005 then
      /* COR credit */
      NULL;
   elsif A_ACTIVITY = 1006 then
      /* Salvage debit */
      NULL;
   elsif A_ACTIVITY = 1007 then
      /* Salvage  credit */
      NULL;
   elsif A_ACTIVITY = 1008 then
      /* Gain/Loss debit */
      NULL;
   elsif A_ACTIVITY = 1009 then
      /* Gain/Loss  credit */
      NULL;
   elsif A_ACTIVITY = 1010 then
      /* Transfer_to debit */
      NULL;
   elsif A_ACTIVITY = 1011 then
      /* Transfer_from  credit */
      NULL;
   elsif A_ACTIVITY = 1012 then
      /* Adjust  debit */
      NULL;
   elsif A_ACTIVITY = 1013 then
      /* Adjust  credit */
      NULL;
   elsif A_ACTIVITY = 1014 then
      /* Intercompany transfer debit 'From' side */
      NULL;
   elsif A_ACTIVITY = 1015 then
      /* Intercompany transfer credit 'To' side */
      NULL;
   elsif A_ACTIVITY = 1016 then
      /* ARO Addition debit */
      NULL;
   elsif A_ACTIVITY = 1017 then
      /* ARO addition credit */
      NULL;
   elsif A_ACTIVITY = 1018 then
      /* life reserve transfer debit */
      NULL;
   elsif A_ACTIVITY = 1019 then
      /* life reserve transfer credit */
      NULL;
   elsif A_ACTIVITY = 1020 then
      /* cor reserve transfer debit */
      NULL;
   elsif A_ACTIVITY = 1021 then
      /* cor reserve transfer credit */
      NULL;
   elsif A_ACTIVITY = 1022 then
      NULL;
   elsif A_ACTIVITY = 1023 then
      NULL;
   elsif A_ACTIVITY = 1024 then
      NULL;
   elsif A_ACTIVITY = 1025 then
      NULL;
   elsif A_ACTIVITY = 1026 then
      NULL;
   elsif A_ACTIVITY = 1027 then
      NULL;
   elsif A_ACTIVITY = 1028 then
      NULL;
   elsif A_ACTIVITY = 1029 then
      NULL;
   elsif A_ACTIVITY = 1030 then
      NULL;
   elsif A_ACTIVITY = 3001 then
      NULL;
   elsif A_ACTIVITY = 3002 then
      NULL;
   elsif A_ACTIVITY = 3003 then
      NULL;
   elsif A_ACTIVITY = 3010 then
      NULL;
   elsif A_ACTIVITY = 3011 then
      NULL;
   elsif A_ACTIVITY = 3012 then
      NULL;
   elsif A_ACTIVITY = 3013 then
      NULL;
   elsif A_ACTIVITY = 3014 then
      NULL;
   elsif A_ACTIVITY = 3015 then
      NULL;
   elsif A_ACTIVITY = 3016 then
      NULL;
   elsif A_ACTIVITY = 3017 then
      NULL;
   elsif A_ACTIVITY = 3018 then
      NULL;
   elsif A_ACTIVITY = 3019 then
      NULL;
   elsif A_ACTIVITY = 3020 then
      NULL;
   elsif A_ACTIVITY = 3021 then
      NULL;
   elsif A_ACTIVITY = 3022 then
      NULL;
   elsif A_ACTIVITY = 3023 then
      NULL;
   elsif A_ACTIVITY = 3024 then
      NULL;
   elsif A_ACTIVITY = 3025 then
      NULL;
   elsif A_ACTIVITY = 3026 then
      NULL;
   elsif A_ACTIVITY = 3027 then
      NULL;
   elsif A_ACTIVITY = 3028 then
      NULL;
   elsif A_ACTIVITY = 3029 then
      NULL;
   elsif A_ACTIVITY = 3030 then
      NULL;
   elsif A_ACTIVITY = 3031 then
      NULL;
   elsif A_ACTIVITY = 3032 then
      NULL;
   elsif A_ACTIVITY = 3033 then
      NULL;
   elsif A_ACTIVITY = 3034 then
      NULL;
   elsif A_ACTIVITY = 3035 then
      NULL;
   elsif A_ACTIVITY = 3036 then
      NULL;
   elsif A_ACTIVITY = 3037 then
      NULL;
   elsif A_ACTIVITY = 3038 then
      NULL;
   elsif A_ACTIVITY = 3039 then
      NULL;
   elsif A_ACTIVITY = 3040 then
    NULL;
   elsif A_ACTIVITY = 3041 then
    NULL;
   elsif A_ACTIVITY = 3042 then
    NULL;
   elsif A_ACTIVITY = 3043 then
    NULL;
   elsif A_ACTIVITY = 3044 then
    NULL;
   end if;

   /*PP-39292 - Move delimiter out of the huge IF statement above, and replace above delimiter assignments with NULL commands. */
   DELIMITER := '-';

   PP_ERR_CODE := 'loop_string';
   /* Loop over elements and rpad the values with spaces and build string */
   for I in ELEMENTS.FIRST .. ELEMENTS.LAST
   loop

      PP_ERR_CODE := 'element ' || ELEMENTS(I) || ' string length ' || LENGTH(GL_ACCOUNT_STR);

      if ACCOUNTING_KEY(I) is null then
         --or place defaults?
         return 'ERROR: ' || ELEMENTS(I) || ' is null ';
      end if;

      ACCOUNTING_KEY(I) := RPAD(ACCOUNTING_KEY(I), WIDTHS(I));

      if I = 1 then
         GL_ACCOUNT_STR := ACCOUNTING_KEY(I);
      else
         GL_ACCOUNT_STR := GL_ACCOUNT_STR || DELIMITER || ACCOUNTING_KEY(I);
      end if;

   end loop;

   if NOTES is not null and NOTES <> '' then
      GL_ACCOUNT_STR := GL_ACCOUNT_STR || DELIMITER || NOTES;
   end if;

   return GL_ACCOUNT_STR;

exception
   when others then
      /* this catches all SQL errors, including no_data_found */
      return('ERROR SQL: ' || PP_ERR_CODE || ':' || sqlerrm);
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15123, 0, 2018, 2, 0, 0, 52846, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052846_lessor_01_lt_def_profit_rem_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;