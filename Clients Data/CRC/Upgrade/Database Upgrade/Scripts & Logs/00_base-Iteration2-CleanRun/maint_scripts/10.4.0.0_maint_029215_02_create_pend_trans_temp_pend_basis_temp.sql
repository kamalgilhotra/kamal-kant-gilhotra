/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029215_02_create_pend_trans_temp_pend_basis_temp.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- --------------   ------------------------------------
|| 10.4.0.0   02/05/2013 Charlie Shilling Point Release
||============================================================================
*/

create global temporary table PEND_TRANSACTION_TEMP
   as (select * from PEND_TRANSACTION where 1 = 2);

create global temporary table PEND_BASIS_TEMP
   as (select * from PEND_BASIS where 1 = 2);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (284, 0, 10, 4, 0, 0, 29215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029215_02_create_pend_trans_temp_pend_basis_temp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;