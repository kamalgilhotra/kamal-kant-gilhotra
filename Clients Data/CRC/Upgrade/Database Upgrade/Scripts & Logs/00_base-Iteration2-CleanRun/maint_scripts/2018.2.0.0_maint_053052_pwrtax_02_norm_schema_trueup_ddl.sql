/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053052_pwrtax_02_norm_schema_trueup_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.1.0.2 03/04/2019  David Conway     Merged to base
||============================================================================
*/

---------------
--archive table
---------------
DECLARE
  table_exists NUMBER(22);
BEGIN
  SELECT Count(1)
  INTO table_exists
  FROM tab
  WHERE tname LIKE 'NORM_SCHEMA_TRUEUP_BEG_ARCHIVE';
  IF table_exists>0 THEN
    EXECUTE IMMEDIATE 'DROP TABLE NORM_SCHEMA_TRUEUP_BEG_ARCHIVE';
  END IF;
END;
/

CREATE TABLE norm_schema_trueup_beg_archive
(
  run_id                        NUMBER(22,0),
  tax_record_id                 NUMBER(22,0) DEFAULT 0,
  tax_year                      NUMBER(22,2) DEFAULT 0,
  normalization_id              NUMBER(22,0) DEFAULT 0,
  def_income_tax_balance_beg    NUMBER(22,2) DEFAULT 0,
  aram_rate                     NUMBER(22,8) DEFAULT 0,
  time_stamp                    DATE         DEFAULT SYSDATE,
  user_id                       VARCHAR2(18) DEFAULT null
);

ALTER TABLE norm_schema_trueup_beg_archive
  ADD CONSTRAINT pk_norm_schema_trueup_beg_arc
      PRIMARY KEY (run_id, tax_record_id, tax_year, normalization_id)
      USING INDEX TABLESPACE PWRPLANT_IDX;

ALTER TABLE norm_schema_trueup_beg_archive
  ADD FOREIGN KEY (tax_record_id)
      REFERENCES tax_record_control (tax_record_id);

ALTER TABLE norm_schema_trueup_beg_archive
  ADD FOREIGN KEY (normalization_id)
      REFERENCES normalization_schema (normalization_id);

COMMENT ON TABLE norm_schema_trueup_beg_archive IS 'This table contains a history of the trueup of beginning APB11 (deferred_income_tax_balance_beg) balance to the statutory rate via the uo_tax_dfit_wksp_norm_trueup_beg window.';

COMMENT ON COLUMN norm_schema_trueup_beg_archive.run_id IS 'Unique identifier of the instance norm_schema_trueup_beg was run to generate these archive records.';
COMMENT ON COLUMN norm_schema_trueup_beg_archive.tax_record_id IS 'System-assigned key that identifies an individual tax asset on the tax depreciation (and related) tables.  Tax_record_id records the unique combination of version, tax class, vintage, in service month (optional), and tax location (also optional).  Each tax asset record on the depreciation table is fully described by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';
COMMENT ON COLUMN norm_schema_trueup_beg_archive.tax_year IS 'Filing or calendar year (as opposed to the vintage year).';
COMMENT ON COLUMN norm_schema_trueup_beg_archive.normalization_id IS 'System-assigned key to the normalization schema, which indicates the ''to - from'' depreciation';
COMMENT ON COLUMN norm_schema_trueup_beg_archive.def_income_tax_balance_beg IS 'Beginning (of the year or month) accumulated deferred tax balance in dollars. Also referred to as the APB11 balance in reference to the IRS code.';
COMMENT ON COLUMN norm_schema_trueup_beg_archive.aram_rate IS 'Average Rate Assumption Method (ARAM) rate used to turn around deferred taxes (weighted average rate) from prior period.';
/

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15703, 0, 2018, 2, 0, 0, 53052, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053052_pwrtax_02_norm_schema_trueup_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;