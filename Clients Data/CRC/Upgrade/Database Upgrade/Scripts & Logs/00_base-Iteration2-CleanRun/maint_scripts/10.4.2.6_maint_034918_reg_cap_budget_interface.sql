SET DEFINE OFF

/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_034918_reg_cap_budget_interface.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.6 05/01/2014 Shane "C" Ward Capital Budget Tables Added
||========================================================================================
*/

--Cap Budget Tables
--Component Element
create table REG_BUDGET_COMPONENT_ELEMENT
(
 REG_COMPONENT_ELEMENT_ID number(22,0) not null,
 DESCRIPTION              varchar2(35) not null,
 ID_COLUMN_NAME           varchar2(30) not null,
 DESCRIPTION_COLUMN       varchar2(30) not null,
 DESCRIPTION_TABLE        varchar2(30) not null,
 USER_ID                  varchar2(18) null,
 TIME_STAMP               date         null
);

alter table REG_BUDGET_COMPONENT_ELEMENT
   add constraint PK_REG_BUDG_COMPONENT_ELEMENT
       primary key (REG_COMPONENT_ELEMENT_ID)
       using index tablespace PWRPLANT_IDX;

comment on column REG_BUDGET_COMPONENT_ELEMENT.REG_COMPONENT_ELEMENT_ID is 'SYSTEM ASSIGNED IDENTIFIER OF THE COMPONENT USED IN CWIP INTERFACE';
comment on column REG_BUDGET_COMPONENT_ELEMENT.DESCRIPTION is 'DESCRIPTION OF THE COMPONENT ELEMENT IN THE CWIP INTERFACE';
comment on column REG_BUDGET_COMPONENT_ELEMENT.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_BUDGET_COMPONENT_ELEMENT.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';

--Family Component
create table REG_BUDGET_FAMILY_COMPONENT
(
 REG_COMPONENT_ID      number(22,0)  not null,
 REG_FAMILY_ID         number(22,0)  not null,
 DESCRIPTION           varchar2(35)  not null,
 REG_ACCT_TYPE_DEFAULT number(22,0)  null,
 SUB_ACCT_TYPE_ID      number(22,0)  null,
 REG_ANNUALIZATION_ID  number(22,0)  null,
 ACCT_GOOD_FOR         number(22,0)  null,
 LONG_DESCRIPTION      varchar2(254) null,
 USER_ID               varchar2(18)  null,
 TIME_STAMP            date          null,
 USED_BY_CLIENT        number(1,0)   not null,
 REG_CWIP_SOURCE_ID    number(22,0)  not null,
 HIST_OR_FCST          varchar2(4)   null
);

alter table REG_BUDGET_FAMILY_COMPONENT
   add constraint PK_REG_BUDG_FAMILY_COMPONENT
       primary key (REG_COMPONENT_ID, REG_FAMILY_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_BUDGET_FAMILY_COMPONENT
   add constraint R_REG_BUDG_FAMILY_COMPONENT1
       foreign key (REG_FAMILY_ID)
       references REG_FAMILY (REG_FAMILY_ID);

create index R_REG_BUDG_FAMILY_COMPONENT1
   on REG_BUDGET_FAMILY_COMPONENT(REG_FAMILY_ID)
      tablespace PWRPLANT_IDX;

alter table REG_BUDGET_FAMILY_COMPONENT
   add constraint R_REG_BUDG_FAMILY_COMPONENT2
       foreign key (REG_ACCT_TYPE_DEFAULT)
       references REG_ACCT_TYPE (REG_ACCT_TYPE_ID);

create index R_REG_BUDG_FAMILY_COMPONENT2
   on REG_BUDGET_FAMILY_COMPONENT (REG_ACCT_TYPE_DEFAULT)
      tablespace PWRPLANT_IDX;

alter table REG_BUDGET_FAMILY_COMPONENT
   add constraint R_REG_BUDG_FAMILY_COMPONENT3
       foreign key (REG_ACCT_TYPE_DEFAULT, SUB_ACCT_TYPE_ID)
       references REG_SUB_ACCT_TYPE (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID);

create index R_REG_BUDG_FAMILY_COMPONENT3
   on REG_BUDGET_FAMILY_COMPONENT (REG_ACCT_TYPE_DEFAULT, SUB_ACCT_TYPE_ID)
      tablespace PWRPLANT_IDX;

alter table REG_BUDGET_FAMILY_COMPONENT
   add constraint R_REG_BUDG_FAMILY_COMPONENT4
       foreign key (REG_ANNUALIZATION_ID)
       references REG_ANNUALIZATION_TYPE (REG_ANNUALIZATION_ID);

create index R_REG_BUDG_FAMILY_COMPONENT4
   on REG_BUDGET_FAMILY_COMPONENT (REG_ANNUALIZATION_ID)
      tablespace PWRPLANT_IDX;

alter table REG_BUDGET_FAMILY_COMPONENT
   add constraint R_REG_BUDG_FAMILY_COMPONENT5
       foreign key (REG_CWIP_SOURCE_ID)
       references REG_CWIP_SOURCE (REG_CWIP_SOURCE_ID);

create index R_REG_BUDG_FAMILY_COMPONENT5
   on REG_BUDGET_FAMILY_COMPONENT (REG_CWIP_SOURCE_ID)
      tablespace PWRPLANT_IDX;

comment on column REG_BUDGET_FAMILY_COMPONENT.REG_COMPONENT_ID is 'SYSTEM ASSIGNED IDENTIFIER OF A FAMILY COMPONENT FOR CWIP.  ALSO VALID IS 0 = NOT MAPPED.';
comment on column REG_BUDGET_FAMILY_COMPONENT.REG_FAMILY_ID is 'SYSTEM ASSIGNED IDENTIFIER OF A REG FAMILY FOR CWIP';
comment on column REG_BUDGET_FAMILY_COMPONENT.DESCRIPTION is 'DESCRIPTION OF THE CWIP FAMILY COMPONENT MAPPING';
comment on column REG_BUDGET_FAMILY_COMPONENT.REG_ACCT_TYPE_DEFAULT is '1 = RATE BASE|2 = UTILITY INVESTMENT NOT IN RATE BASE (I.E. NOT EARNING A RETURN)|3 = OPERATING EXPENSE|4 = NON-UTILITY EXPENSE|5 = OPERATING REVENUE|6 = NON-UTILITY REVENUE|7 = NON-UTILITY INVESTMENT|8 = CAPITAL|9 = OTHER ADJUSTMENT|10 = INCOME TAX MEMO';
comment on column REG_BUDGET_FAMILY_COMPONENT.SUB_ACCT_TYPE_ID is 'E.G. FOR ACCOUNT TYPE CAPITAL: PREFERRED, COMMON OR DEBT; FOR RATE BASE: WORKING CAPITAL, PP&E';
comment on column REG_BUDGET_FAMILY_COMPONENT.REG_ANNUALIZATION_ID is 'ANNUALIZATION METHOD:|1 = 13 MONTH SIMPLE AVERAGE|2 = 13 MONTH AVERAGE OF AVERAGES|3 = 12 MONTH AVERAGE|4 = 2 POINT ANNUAL AVERAGE|5 = ENDING BALANCE|6 = SUM OF 12 MONTHS|7 = ENDING MONTH * 12|8  = INPUT';
comment on column REG_BUDGET_FAMILY_COMPONENT.ACCT_GOOD_FOR is 'APPLICABILITY INDICATOR:|1 = HISTORIC ACTUALS|2 = OTHER HISTORIC|3 = FORECAST|4 = HISTORIC ACTUALS AND FORECAST';
comment on column REG_BUDGET_FAMILY_COMPONENT.LONG_DESCRIPTION is 'LONG DESCRIPTION OF THE FAMILY COMPONENT USED IN THE CWIP INTERFACE';
comment on column REG_BUDGET_FAMILY_COMPONENT.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_BUDGET_FAMILY_COMPONENT.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_BUDGET_FAMILY_COMPONENT.USED_BY_CLIENT is 'TURNS THE COMPONENT ON OR OFF FOR THE CWIP INTERFACE';
comment on column REG_BUDGET_FAMILY_COMPONENT.REG_CWIP_SOURCE_ID is 'SYSTEM ASSIGNED IDENTIFIER OF A CWIP SOURCE';

--Component Values
create table REG_BUDGET_COMPONENT_VALUES
(
 REG_COMPONENT_ID         number(22,0) not null,
 REG_FAMILY_ID            number(22,0) not null,
 REG_COMPONENT_ELEMENT_ID number(22,0) not null,
 ID_VALUE                 number(22,0) not null,
 USER_ID                  varchar2(18) null,
 TIME_STAMP               date         null
);

alter table REG_BUDGET_COMPONENT_VALUES
   add constraint PK_REG_BUDG_COMPONENT_VALUES
       primary key (REG_COMPONENT_ID, REG_FAMILY_ID, REG_COMPONENT_ELEMENT_ID, ID_VALUE)
       using index tablespace PWRPLANT_IDX;

alter table REG_BUDGET_COMPONENT_VALUES
   add constraint R_REG_BUDG_COMPONENT_VALUES1
       foreign key (REG_COMPONENT_ID, REG_FAMILY_ID)
       references REG_BUDGET_FAMILY_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);

alter table REG_BUDGET_COMPONENT_VALUES
  add constraint R_REG_BUDG_COMPONENT_VALUES2
      foreign key (REG_COMPONENT_ELEMENT_ID)
      references REG_BUDGET_COMPONENT_ELEMENT (REG_COMPONENT_ELEMENT_ID);

create index R_REG_BUDG_COMPONENT_VALUES2
   on REG_BUDGET_COMPONENT_VALUES (REG_COMPONENT_ELEMENT_ID)
      tablespace PWRPLANT_IDX;

COMMENT ON COLUMN reg_budget_component_values.reg_component_id IS 'SYSTEM ASSIGNED IDENTIFIER OF A FAMILY COMPONENT FOR CWIP.  ALSO VALID IS 0 = NOT MAPPED.';
COMMENT ON COLUMN reg_budget_component_values.reg_family_id IS 'SYSTEM ASSIGNED IDENTIFIER OF A REG FAMILY FOR CWIP';
COMMENT ON COLUMN reg_budget_component_values.reg_component_element_id IS 'SYSTEM ASSIGNED IDENTIFIER OF THE COMPONENT USED IN CWIP INTERFACE';
COMMENT ON COLUMN reg_budget_component_values.user_id IS 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
COMMENT ON COLUMN reg_budget_component_values.time_stamp IS 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';


--Activity View
create or replace view REG_BUDGET_BOOK_SUMMARY
as
(select E.RECONCILE_ITEM_ID RECONCILE_ITEM_ID, E.DESCRIPTION DESCRIPTION, TYPE
  from TAX_RECONCILE_ITEM T, ESTIMATE_CHARGE_TYPE E
 where E.RECONCILE_ITEM_ID = T.RECONCILE_ITEM_ID
   and FUNDING_CHARGE_INDICATOR = 1);


--Balance Table
create table REG_BUDGET_GL_ACCOUNT_TYPE
(
 REG_CWIP_GL_ACCT_TYPE_ID number(22,0)  not null,
 DESCRIPTION              varchar2(35)  not null,
 WO_ACCOUNT_COLUMN        varchar2(30)  not null,
 USER_ID                  varchar2(18)  null,
 TIME_STAMP               date          null,
 AMOUNT_COLUMN            varchar2(254) null
);

alter table REG_BUDGET_GL_ACCOUNT_TYPE
   add constraint PK_REG_BUDGET_GL_ACCOUNT_TYPE
       primary key (REG_CWIP_GL_ACCT_TYPE_ID)
       using index tablespace PWRPLANT_IDX;


COMMENT ON COLUMN reg_budget_gl_account_type.reg_cwip_gl_acct_type_id IS 'SYSTEM ASSIGNED IDENTIFIER OF GL ACCOUNT TYPES USED IN CWIP INTERFACE';
COMMENT ON COLUMN reg_budget_gl_account_type.description IS 'DESCRIPTON OF THE GL ACCOUNT TYPE USED IN CWIP INTERFACE';
COMMENT ON COLUMN reg_budget_gl_account_type.wo_account_column IS 'COLUMN NAME FROM WORK_ORDER_ACCOUNT TO USE FOR THIS GL ACCOUNT TYPE IN THE CWIP INTERFACE';
COMMENT ON COLUMN reg_budget_gl_account_type.user_id IS 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
COMMENT ON COLUMN reg_budget_gl_account_type.time_stamp IS 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
COMMENT ON COLUMN reg_budget_gl_account_type.amount_column IS 'THE COLUMN REFERENCE FOR DOLLAR AMOUNTS FOR CWIP INTEGRATION';

insert into REG_BUDGET_GL_ACCOUNT_TYPE
   (REG_CWIP_GL_ACCT_TYPE_ID, DESCRIPTION, WO_ACCOUNT_COLUMN, AMOUNT_COLUMN)
values
   (1, 'Additions', 'cwip_gl_account', 'nvl(budget_afudc_clac.end_cwip, 0)');
insert into REG_BUDGET_GL_ACCOUNT_TYPE
   (REG_CWIP_GL_ACCT_TYPE_ID, DESCRIPTION, WO_ACCOUNT_COLUMN, AMOUNT_COLUMN)
values
   (2, 'Retirements', 'removal_gl_account', 'nvl(budget_afudc_clac.end_cwip, 0)');


--Component Elements
--Book Summary/Activity
insert into REG_BUDGET_COMPONENT_ELEMENT
   (REG_COMPONENT_ELEMENT_ID, DESCRIPTION, ID_COLUMN_NAME, DESCRIPTION_COLUMN, DESCRIPTION_TABLE)
values
   (2, 'Book Summary', 'reconcile_item_id', 'description', 'reg_budget_book_summary');

--Balance/GL Account Type
insert into REG_BUDGET_COMPONENT_ELEMENT
   (REG_COMPONENT_ELEMENT_ID, DESCRIPTION, ID_COLUMN_NAME, DESCRIPTION_COLUMN, DESCRIPTION_TABLE)
values
   (7, 'GL Account Type', 'reg_cwip_gl_acct_type_id', 'description', 'reg_budget_gl_account_type');

insert into REG_BUDGET_COMPONENT_ELEMENT
   (REG_COMPONENT_ELEMENT_ID, DESCRIPTION, ID_COLUMN_NAME, DESCRIPTION_COLUMN, DESCRIPTION_TABLE)
values
   (8, 'WIP Computation', 'wip_computation_id', 'description', 'wip_computation');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1141, 0, 10, 4, 2, 6, 34918, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_034918_reg_cap_budget_interface.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON