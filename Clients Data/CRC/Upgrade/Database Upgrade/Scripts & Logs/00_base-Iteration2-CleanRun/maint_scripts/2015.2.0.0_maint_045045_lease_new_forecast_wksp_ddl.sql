/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045045_lease_new_forecast_wksp_ddl.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/16/2015 Sarah Byers    Remove FK to set_of_books
||============================================================================
*/

alter table ls_forecast_version drop constraint ls_forecast_version_fk1;

alter table ls_forecast_default_cap_types drop constraint ls_fcst_default_cap_types_fk3;

alter table ls_forecast_default_cap_types
add constraint ls_fcst_default_cap_types_fk3
foreign key (to_cap_type_id)
references ls_fasb_cap_type (fasb_cap_type_id);

create sequence ls_forecast_version_seq start with 1 increment by 1;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2930, 0, 2015, 2, 0, 0, 45045, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045045_lease_new_forecast_wksp_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

