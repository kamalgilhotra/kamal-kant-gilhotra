/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050641_lessor_01_add_df_vp_asset_allocation_view_ddl.sql
|| Description:	Add new rate types to rate type table
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 04/16/2018 Jared Watkins  Create new view to pull the asset allocations of the DF schedule fields for VPs
||============================================================================
*/

CREATE OR REPLACE VIEW v_lsr_pseudo_asset_sch_direct (
  lsr_asset_id,
  ilr_id,
  revision,
  set_of_books_id,
  month,
  begin_deferred_profit,
  recognized_profit,
  end_deferred_profit
) AS
SELECT lsr_asset_id,
      ilr_id,
     revision,
     set_of_books_id,
     month,
     begin_deferred_profit * allocation AS begin_deferred_profit,
     recognized_profit * allocation AS recognized_profit,
     end_deferred_profit * allocation AS end_deferred_profit
FROM ( SELECT asset.lsr_asset_id,
        sch.ilr_id,
       sch.revision,
       sch.set_of_books_id,
       sch.month,
       sch.begin_deferred_profit,
       sch.recognized_profit,
       sch.end_deferred_profit,
       coalesce(
    RATIO_TO_REPORT(
      asset.fair_market_value
    ) OVER(PARTITION BY
      sch.ilr_id,
      sch.revision,
      sch.month,
      sch.set_of_books_id
    ),
    0
  ) AS allocation
FROM lsr_ilr_schedule_direct_fin sch
  JOIN lsr_asset asset ON sch.ilr_id = asset.ilr_id
  AND sch.revision = asset.revision
);

INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4510, 0, 2017, 3, 0, 0, 50641, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050641_lessor_01_add_df_vp_asset_allocation_view_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;