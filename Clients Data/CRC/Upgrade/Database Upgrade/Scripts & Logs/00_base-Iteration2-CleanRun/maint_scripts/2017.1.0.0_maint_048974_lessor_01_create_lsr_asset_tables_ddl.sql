/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048974_lessor_01_create_lsr_asset_tables_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/28/2017 Charlie Shilling Create LSR_ASSET table
|| 2017.1.0.0 10/03/2017 Josh Sandler	  Modify table from initial script creation
||============================================================================
*/
CREATE TABLE lsr_asset (
	lsr_asset_id 					NUMBER(22,0)	NOT NULL,
	ilr_id							NUMBER(22,0)	NOT NULL,
	revision						NUMBER(22,0)	NOT NULL,
	time_stamp						DATE,
	user_id							VARCHAR2(18),
	description						VARCHAR2(35)	NOT NULL,
	fair_market_value				NUMBER(22,2),
	carrying_cost					NUMBER(22,2),
	guaranteed_residual_amount		NUMBER(22,2),
	actual_residual_amount			NUMBER(22,2),
	estimated_residual_pct			NUMBER(22,8),
	unguaranteed_residual_amount	NUMBER(22,2),
	expected_life					NUMBER(22,0)	NOT NULL,
	economic_life					NUMBER(22,0)	NOT NULL,
	serial_number					VARCHAR2(35),
	long_description				VARCHAR2(254)	NOT NULL,
	cpr_asset_id					NUMBER(22,0),
	fair_market_value_comp_curr		NUMBER(22,2),
	carrying_cost_comp_curr			NUMBER(22,2),
	guaranteed_res_amt_comp_curr	NUMBER(22,2),
	actual_residual_amt_comp_curr	NUMBER(22,2)
)
;

ALTER TABLE lsr_asset
  ADD CONSTRAINT pk_lsr_asset PRIMARY KEY (
    lsr_asset_id,
	ilr_id,
	revision
  )
  USING INDEX
    TABLESPACE pwrplant_idx
;

ALTER TABLE lsr_asset
  ADD CONSTRAINT r_lsr_asset1 FOREIGN KEY (
    cpr_asset_id
  ) REFERENCES cpr_ledger (
    asset_id
  )
;

COMMENT ON TABLE lsr_asset IS '(S)  [06]	The LSR_ASSET table is the header table storing Lessor module assets. Assets may be manually entered or created from PowerPlan CPR assets (identified by the cpr_asset_id column value).';

COMMENT ON COLUMN lsr_asset.lsr_asset_id IS 'System-assigned identifier of a particular leased asset.';
COMMENT ON COLUMN lsr_asset.ilr_id IS 'Unique identifier of the leased asset''s individual lease record.';
COMMENT ON COLUMN lsr_asset.revision IS 'The revision of the ILR that the asset is associated with.';
COMMENT ON COLUMN lsr_asset.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_asset.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_asset.description	IS 'Records a brief description of the asset.';
COMMENT ON COLUMN lsr_asset.fair_market_value IS 'The fair market value in contract currency converted from the company currency amount.';
COMMENT ON COLUMN lsr_asset.carrying_cost IS 'The carrying cost in contract currency converted from the company currency amount.';
COMMENT ON COLUMN lsr_asset.guaranteed_residual_amount IS 'The guaranteed residual amount in contract currency converted from the company currency amount.';
COMMENT ON COLUMN lsr_asset.actual_residual_amount IS 'The actual residual amount in contract currency converted from the company currency amount.';
COMMENT ON COLUMN lsr_asset.estimated_residual_pct IS 'The estimated residual percent calculated as Estimated Residual Amount/Fair Market Value.';
COMMENT ON COLUMN lsr_asset.unguaranteed_residual_amount IS 'The unguaranteed residual amount in contract currency calculated as Estimated Residual-Guaranteed Residual.';
COMMENT ON COLUMN lsr_asset.expected_life IS 'The user-entered expected life in months.';
COMMENT ON COLUMN lsr_asset.economic_life IS 'The user-entered economic life in months.';
COMMENT ON COLUMN lsr_asset.serial_number IS 'The serial number.';
COMMENT ON COLUMN lsr_asset.long_description IS 'Records a more detailed description of the asset.';
COMMENT ON COLUMN lsr_asset.cpr_asset_id IS 'The asset_id on the CPR_LEDGER table if the asset was added from the CPR, otherwise left null.';
COMMENT ON COLUMN lsr_asset.fair_market_value_comp_curr IS 'The user-entered fair market value in company currency.';
COMMENT ON COLUMN lsr_asset.carrying_cost_comp_curr IS 'The user-entered or CPR-sourced carrying cost in company currency.';
COMMENT ON COLUMN lsr_asset.guaranteed_res_amt_comp_curr IS 'The user-entered guaranteed residual in contract currency.';
COMMENT ON COLUMN lsr_asset.actual_residual_amt_comp_curr IS 'The user-enterd (at the end of the lease) actual residual in company currency.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3757, 0, 2017, 1, 0, 0, 48974, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048974_lessor_01_create_lsr_asset_tables_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
