/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045368_lease_fasb_cap_type_sob_map_menu_dml.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0  02/03/2016  Anand R        Create new menu item under Lease -> Admin menu
|| 2015.2.2.0 04/14/2016 David Haupt		 Backporting to 2015.2.2
||============================================================================
*/

insert into ppbase_workspace (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID)
values ('LESSEE', 'admin_set_of_books_fasb_type', 'Set of Books FASB Type', 'uo_ls_admincntr_set_of_books_fasb_type_wksp', null, 1);

insert into ppbase_menu_items (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values ('LESSEE', 'admin_set_of_books_fasb_type', 2, 8, 'Set of Books FASB Type', null, 'menu_wksp_admin', 'admin_set_of_books_fasb_type', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3041, 0, 2015, 2, 2, 0, 045368, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045368_lease_fasb_cap_type_sob_map_menu_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
