/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051035_lessee_01_add_paid_spread_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/08/2018 Josh Sandler     Add paid_spread to schedule calc stage tables
||============================================================================
*/

ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD paid_spread NUMBER(22,2);

COMMENT ON COLUMN ls_ilr_asset_schedule_calc_stg.paid_spread IS 'Spread amount of non-monthly prepayments.';

ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD paid_spread NUMBER(22,2);

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.paid_spread IS 'Spread amount of non-monthly prepayments.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5205, 0, 2017, 4, 0, 0, 51035, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051035_lessee_01_add_paid_spread_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;