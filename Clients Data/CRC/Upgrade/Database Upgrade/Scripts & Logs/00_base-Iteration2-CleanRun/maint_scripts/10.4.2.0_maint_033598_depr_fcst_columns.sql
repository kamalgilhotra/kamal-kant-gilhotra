/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033598_depr_fcst_columns.sql
|| Description: Sync columns from Depr tables to Depr Fcst tables
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   11/21/2013 Ryan Oliveria  Point Release
||============================================================================
*/

--* Add columns to FCST tables that only exist on DEPR tables
alter table FCST_DEPR_GROUP add ARO_ID number(22,0);
alter table FCST_DEPR_GROUP add JE_BY_ASSET number(22,0);
alter table FCST_DEPR_GROUP add SALVAGE_EXPENSE_ACCT_ID number(22,0);
alter table FCST_DEPR_GROUP add SALVAGE_RESERVE_ACCT_ID number(22,0);
alter table FCST_DEPR_GROUP add TRUE_UP_CPR_DEPR number(22,0);
alter table FCST_DEPR_GROUP_VERSION add ARO_ID number(22,0);
alter table FCST_DEPR_GROUP_VERSION add JE_BY_ASSET number(22,0);
alter table FCST_DEPR_GROUP_VERSION add SALVAGE_EXPENSE_ACCT_ID number(22,0);
alter table FCST_DEPR_GROUP_VERSION add SALVAGE_RESERVE_ACCT_ID number(22,0);
alter table FCST_DEPR_GROUP_VERSION add TRUE_UP_CPR_DEPR number(22,0);
alter table FCST_DEPR_LEDGER add COR_BLENDING_ADJUSTMENT number(22,2);
alter table FCST_DEPR_LEDGER add COR_BLENDING_TRANSFER number(22,2);
alter table FCST_DEPR_LEDGER add IMPAIRMENT_ASSET_AMOUNT number(22,2);
alter table FCST_DEPR_LEDGER add IMPAIRMENT_EXPENSE_AMOUNT number(22,2);
alter table FCST_DEPR_LEDGER add RESERVE_BAL_IMPAIRMENT number(22,2);
alter table FCST_DEPR_LEDGER add RESERVE_BAL_SALVAGE_EXP number(22,2);
alter table FCST_DEPR_LEDGER add RESERVE_BLENDING_ADJUSTMENT number(22,2);
alter table FCST_DEPR_LEDGER add RESERVE_BLENDING_TRANSFER number(22,2);
alter table FCST_DEPR_LEDGER add RWIP_COST_OF_REMOVAL number(22,2);
alter table FCST_DEPR_LEDGER add RWIP_RESERVE_CREDITS number(22,2);
alter table FCST_DEPR_LEDGER add RWIP_SALVAGE_CASH number(22,2);
alter table FCST_DEPR_LEDGER add RWIP_SALVAGE_RETURNS number(22,2);
alter table FCST_DEPR_LEDGER add SALVAGE_BASE number(22,2);
alter table FCST_DEPR_LEDGER add SALVAGE_EXPENSE number(22,2);
alter table FCST_DEPR_LEDGER add SALVAGE_EXP_ADJUST number(22,2);
alter table FCST_DEPR_LEDGER add SALVAGE_EXP_ALLOC_ADJUST number(22,2);
alter table FCST_DEPR_LEDGER add SALVAGE_RATE number(22,8);
alter table FCST_DEPRECIATION_METHOD add COMPANY_ID number(22,0);
alter table FCST_DEPRECIATION_METHOD add DEPR_BLENDING_TYPE_ID number(22,0);
alter table FCST_DEPRECIATION_METHOD add EXCLUDE_FROM_RWIP number(22,0);
alter table FCST_DEPR_METHOD_RATES add AMORTIZABLE_LIFE number(22,0);
alter table FCST_DEPR_METHOD_RATES add COR_TREATMENT VARCHAR2(5);
alter table FCST_DEPR_METHOD_RATES add DR_COMMENT_ID number(22,0);
alter table FCST_DEPR_METHOD_RATES add GAIN_LOSS_DEFAULT number(22,0);
alter table FCST_DEPR_METHOD_RATES add MASS_EXPECTED_LIFE number(22,8);
alter table FCST_DEPR_METHOD_RATES add MORT_MEM_MORT_CURVE_ID number(22,0);
alter table FCST_DEPR_METHOD_RATES add NET_SALVAGE_AMORT_LIFE number(22,0);
alter table FCST_DEPR_METHOD_RATES add SALVAGE_RATE number(22,8);
alter table FCST_DEPR_METHOD_RATES add SALVAGE_TREATMENT VARCHAR2(5);
alter table FCST_DEPR_RES_ALLO_FACTORS add COR_FACTOR number(22,8);
alter table FCST_DEPR_RES_ALLO_FACTORS add LIFE_FACTOR number(22,8);
alter table FCST_DEPR_VINTAGE_SUMMARY add FCST_COMBINED_DEPR_GROUP_ID number(22,0);

--* Comment on columns that were just created
comment on column FCST_DEPR_GROUP.ARO_ID is 'System-assigned identifier of an ARO to which all the assets of the group are associated.  This facilitates reporting of all regulatory assets and liabilities for an ARO.';
comment on column FCST_DEPR_GROUP.JE_BY_ASSET is 'Allows depr expense journal entries to be made by asset
1 = yes
0 = no (default)';
comment on column FCST_DEPR_GROUP.SALVAGE_EXPENSE_ACCT_ID is 'System-assigned identifier of the general ledger account which is posted for the salvage provision.';
comment on column FCST_DEPR_GROUP.SALVAGE_RESERVE_ACCT_ID is 'System-assigned identifier of the general ledger account which maintains the reserve associated with salvage.';
comment on column FCST_DEPR_GROUP.TRUE_UP_CPR_DEPR is 'For CPR depreciation (i.e., individual asset), this indicates how late in service, late charges, changes in life should be treated:
0 = No true up.
The initial add starts depreciation over the stated life whenever it occurs no matter what the in-service date is.  Subsequent changes are always amortized over the remaining life.  (Note:  this option is not appropriate if using 106.)
1 = Cumulative true up.
The reserve is trued up to the ''theoretical'' every month through the depreciation calculation.  This accommodates late-in-service reporting, late charges (taking back changes in initial or remaining life to initial in-service for the true-up).
2 = Convention weight.
The initial addition is trued up with depreciation back to the in-service month.  Later activity gets convention weight in month of activity but no true-up.';
comment on column FCST_DEPR_GROUP_VERSION.ARO_ID is 'System-assigned identifier of an ARO to which all the assets of the group are associated.  This facilitates reporting of all regulatory assets and liabilities for an ARO.';
comment on column FCST_DEPR_GROUP_VERSION.JE_BY_ASSET is 'Allows depr expense journal entries to be made by asset
1 = yes
0 = no (default)';
comment on column FCST_DEPR_GROUP_VERSION.SALVAGE_EXPENSE_ACCT_ID is 'System-assigned identifier of the general ledger account which is posted for the salvage provision.';
comment on column FCST_DEPR_GROUP_VERSION.SALVAGE_RESERVE_ACCT_ID is 'System-assigned identifier of the general ledger account which maintains the reserve associated with salvage.';
comment on column FCST_DEPR_GROUP_VERSION.TRUE_UP_CPR_DEPR is 'For CPR depreciation (i.e., individual asset), this indicates how late in service, late charges, changes in life should be treated:
0 = No true up.
The initial add starts depreciation over the stated life whenever it occurs no matter what the in-service date is.  Subsequent changes are always amortized over the remaining life.  (Note:  this option is not appropriate if using 106.)
1 = Cumulative true up.
The reserve is trued up to the ''theoretical'' every month through the depreciation calculation.  This accommodates late-in-service reporting, late charges (taking back changes in initial or remaining life to initial in-service for the true-up).
2 = Convention weight.
The initial addition is trued up with depreciation back to the in-service month.  Later activity gets convention weight in month of activity but no true-up.';
comment on column FCST_DEPR_LEDGER.COR_BLENDING_ADJUSTMENT is '';
comment on column FCST_DEPR_LEDGER.COR_BLENDING_TRANSFER is '';
comment on column FCST_DEPR_LEDGER.IMPAIRMENT_ASSET_AMOUNT is 'The amount to be posted to the CPR and recorded in an asset impairment bucket which reduces the asset cost and reduces the reserve balance.';
comment on column FCST_DEPR_LEDGER.IMPAIRMENT_EXPENSE_AMOUNT is 'The expense amount is equal to the change in NBV, e.g. the amount booked to an expense account, and only affects the reserve balance.';
comment on column FCST_DEPR_LEDGER.RESERVE_BAL_IMPAIRMENT is 'The accumulated impairment reserve balance (impairment_asset_amount + impairment_expense_amount)';
comment on column FCST_DEPR_LEDGER.RESERVE_BAL_SALVAGE_EXP is 'Records the accumulated salvage provision (when identified) for depreciation included in the reserve for the depreciation group in dollars at month-end for closed months. Note that the normal accumulated depreciation salvage provision (debit balance) is negative because it decreases total reserve.';
comment on column FCST_DEPR_LEDGER.RESERVE_BLENDING_ADJUSTMENT is '';
comment on column FCST_DEPR_LEDGER.RESERVE_BLENDING_TRANSFER is '';
comment on column FCST_DEPR_LEDGER.RWIP_COST_OF_REMOVAL is 'Records the optional allocation of the month end RWIP cost of removal balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER.RWIP_RESERVE_CREDITS is 'Records the optional allocation of the month end RWIP reserve credits balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER.RWIP_SALVAGE_CASH is 'Records the optional allocation of the month end RWIP salvage cash balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER.RWIP_SALVAGE_RETURNS is 'Records the optional allocation of the month end RWIP salvage returns balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER.SALVAGE_BASE is 'Records the dollar base against which the salvage rate was applied in that month. The default is 0.';
comment on column FCST_DEPR_LEDGER.SALVAGE_EXPENSE is 'Salvage ""expense"" provison (when identified) in dollars. The default is 0.   Generally negative.';
comment on column FCST_DEPR_LEDGER.SALVAGE_EXP_ADJUST is 'Input salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column FCST_DEPR_LEDGER.SALVAGE_EXP_ALLOC_ADJUST is 'Calculated salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column FCST_DEPR_LEDGER.SALVAGE_RATE is 'Annual rate used to calculate the salvage provision; Expressed as a decimal. Not the effective rate (i.e. it does not take into expense adjustments). The default is 0.';
comment on column FCST_DEPRECIATION_METHOD.COMPANY_ID is 'System-assigned identifier of the company to which the method applies.';
comment on column FCST_DEPRECIATION_METHOD.DEPR_BLENDING_TYPE_ID is 'Internal system generated ID
Dept Template
(S)
Department Templates can be defined to automatically default sets of valid Departments to Work Orders and/or Funding Projects upon initiation, based on the Work Order or Funding Project Type.  Once the Template has been defined, users can assign Departments to the template via the Dept Template Assign table, and they can then associate the template to WO/FP types via the WO Type Dept Template table.';
comment on column FCST_DEPRECIATION_METHOD.EXCLUDE_FROM_RWIP is 'Switch which will exclude the associated depreciation groups from the CWIP allocation if the allocation is being used:
No = Don''t exclude.
Yes = Exclude';
comment on column FCST_DEPR_METHOD_RATES.AMORTIZABLE_LIFE is 'For automatic retirements (e.g. ""amortizable"" general plant) using the depreciation group option this is life in months.';
comment on column FCST_DEPR_METHOD_RATES.COR_TREATMENT is '''No'' is normal. Cost of removal is provided in the depreciation rate over the asset life.  ''Annual'' indicates that actual cost of removal is amortized over the number of months provided (net_salvage_amort_life below), starting the next January.  ''Monthly'' indicates that the cost of removal is amortized over the number of months provided beginning in the current month.  Thus ''1'' month with a monthly option will expense cost of removal in the current month.';
comment on column FCST_DEPR_METHOD_RATES.DR_COMMENT_ID is 'System-assigned identifier of a depreciation rates comment id.';
comment on column FCST_DEPR_METHOD_RATES.GAIN_LOSS_DEFAULT is 'Null or zero = ''No Gain/Loss'' is normal for regulated group depreciation.  No gain/loss is generally calculated on a retirement and the full basis is deducted from the reserve.
A ''1'' or ''Gain/Loss'' means that for this group a loss (gain) will be calculated by subtracting from the salvage proceeds the net value asset value less the cost of removal.  This could be appropriate for non-depreciable asset groups such as land or for some non-utility assets.';
comment on column FCST_DEPR_METHOD_RATES.MASS_EXPECTED_LIFE is 'Records the average expected life in years.  With the mort_mem_mort_curve above it is used to process retirements.  Similar to mort_mem_mort_curve above, if blank, it will default to average_expected_life.';
comment on column FCST_DEPR_METHOD_RATES.MORT_MEM_MORT_CURVE_ID is 'Records the mortality curve used to process retirements (to build mortality memory) if the pp_system control ""Mortality Curve Account Lookup"" option is ""Depr Method"".  If that option is selected and this field is blank, the system will use the ''mortal_curve_id'' on this table.  By allowing two curves, and two lives, one can be dollar based, the other quantity based.';
comment on column FCST_DEPR_METHOD_RATES.NET_SALVAGE_AMORT_LIFE is 'If salvage proceeds or actual cost of removal is amortized apart from the life depreciation provision (see cor_treatment and salvage_treatment above), this is the amortizable life in months.  Note that for the annual option the life starts in January of the following year.  For the monthly option, the life starts in the current month; thus a life of ''1'' can expense cost of removal or salvage.';
comment on column FCST_DEPR_METHOD_RATES.SALVAGE_RATE is 'If a separate salvage provision is being calculated, this is the annual provision rate used to calculate that salvage expense. It is expressed as a decimal, normally negative. The default is 0.';
comment on column FCST_DEPR_METHOD_RATES.SALVAGE_TREATMENT is '''No'' is normal.  Salvage is provided in the depreciation rate (or reduced base) or an estimated basis over the asset life.  ''Annual'' indicates that actual salvage is amortized over the number of months provided (net_salvage_amort_life below), starting the next January.  ''Monthly'' indicates that the salvage is amortized over the number of months provided beginning in the current month.  Thus ''1'' month with a monthly option will expense salvage in the current month.';
comment on column FCST_DEPR_RES_ALLO_FACTORS.COR_FACTOR is 'Cost of removal portion of the theoretical factor.';
comment on column FCST_DEPR_RES_ALLO_FACTORS.LIFE_FACTOR is 'Life (life/positive) portion of the theoretical factor.';
comment on column FCST_DEPR_VINTAGE_SUMMARY.FCST_COMBINED_DEPR_GROUP_ID is 'System-assigned identifier of a particular combined depr group.';

--* Fill in default values for new columns above

update FCST_DEPR_GROUP set JE_BY_ASSET = 0 where JE_BY_ASSET is null;

update FCST_DEPR_GROUP_VERSION set JE_BY_ASSET = 0 where JE_BY_ASSET is null;

update FCST_DEPR_LEDGER
   set COR_BLENDING_ADJUSTMENT = 0,
       COR_BLENDING_TRANSFER = 0,
       IMPAIRMENT_ASSET_AMOUNT = 0,
       IMPAIRMENT_EXPENSE_AMOUNT = 0,
       RESERVE_BAL_IMPAIRMENT = 0,
       RESERVE_BAL_SALVAGE_EXP = 0,
       RESERVE_BLENDING_ADJUSTMENT = 0,
       RESERVE_BLENDING_TRANSFER = 0,
       RWIP_COST_OF_REMOVAL = 0,
       RWIP_RESERVE_CREDITS = 0,
       RWIP_SALVAGE_CASH = 0,
       RWIP_SALVAGE_RETURNS = 0,
       SALVAGE_BASE = 0,
       SALVAGE_EXPENSE = 0,
       SALVAGE_EXP_ADJUST = 0,
       SALVAGE_EXP_ALLOC_ADJUST = 0,
       SALVAGE_RATE = 0;

update FCST_DEPR_METHOD_RATES
   set AMORTIZABLE_LIFE = null,
       COR_TREATMENT = 'No',
       DR_COMMENT_ID = null,
       GAIN_LOSS_DEFAULT = 0,
       MASS_EXPECTED_LIFE = null,
       MORT_MEM_MORT_CURVE_ID = null,
       NET_SALVAGE_AMORT_LIFE = null,
       SALVAGE_RATE = 0,
       SALVAGE_TREATMENT = 'No';

update FCST_DEPR_RES_ALLO_FACTORS
   set COR_FACTOR = 0,
       LIFE_FACTOR = FACTOR;

--* Create FCST_DEPR_LEDGER_BLENDING table
CREATE TABLE FCST_DEPR_LEDGER_BLENDING
(
 SET_OF_BOOKS_ID                number(22,0) not null,
 FCST_DEPR_GROUP_ID             number(22,0) not null,
 GL_POST_MO_YR                  date not null,
 TIME_STAMP                     date,
 USER_ID                        varchar2(18),
 DEPR_LEDGER_STATUS             number(22,0),
 SOURCE_PERCENT                 number(22,8),
 BEGIN_RESERVE_BL               number(22,2),
 END_RESERVE_BL                 number(22,2),
 RESERVE_BAL_PROVISION_BL       number(22,2),
 RESERVE_BAL_COR_BL             number(22,2),
 SALVAGE_BALANCE_BL             number(22,2),
 RESERVE_BAL_ADJUST_BL          number(22,2),
 RESERVE_BAL_RETIREMENTS_BL     number(22,2),
 RESERVE_BAL_TRAN_IN_BL         number(22,2),
 RESERVE_BAL_TRAN_OUT_BL        number(22,2),
 RESERVE_BAL_OTHER_CREDITS_BL   number(22,2),
 RESERVE_BAL_GAIN_LOSS_BL       number(22,2),
 RESERVE_ALLOC_FACTOR_BL        number(22,2),
 BEGIN_BALANCE_BL               number(22,2),
 ADDITIONS_BL                   number(22,2),
 RETIREMENTS_BL                 number(22,2),
 TRANSFERS_IN_BL                number(22,2),
 TRANSFERS_OUT_BL               number(22,2),
 ADJUSTMENTS_BL                 number(22,2),
 DEPRECIATION_BASE_BL           number(22,2),
 END_BALANCE_BL                 number(22,2),
 DEPRECIATION_RATE_BL           number(22,8),
 DEPRECIATION_EXPENSE_BL        number(22,2),
 DEPR_EXP_ADJUST_BL             number(22,2),
 DEPR_EXP_ALLOC_ADJUST_BL       number(22,2),
 COST_OF_REMOVAL_BL             number(22,2),
 RESERVE_RETIREMENTS_BL         number(22,2),
 SALVAGE_RETURNS_BL             number(22,2),
 SALVAGE_CASH_BL                number(22,2),
 RESERVE_CREDITS_BL             number(22,2),
 RESERVE_ADJUSTMENTS_BL         number(22,2),
 RESERVE_TRAN_IN_BL             number(22,2),
 RESERVE_TRAN_OUT_BL            number(22,2),
 GAIN_LOSS_BL                   number(22,2),
 VINTAGE_NET_SALVAGE_AMORT_BL   number(22,2),
 VINTAGE_NET_SALVAGE_RESERVE_BL number(22,2),
 CURRENT_NET_SALVAGE_AMORT_BL   number(22,2),
 CURRENT_NET_SALVAGE_RESERVE_BL number(22,2),
 IMPAIRMENT_RESERVE_BEG_BL      number(22,2),
 IMPAIRMENT_RESERVE_ACT_BL      number(22,2),
 IMPAIRMENT_RESERVE_END_BL      number(22,2),
 EST_ANN_NET_ADDS_BL            number(22,2),
 RWIP_ALLOCATION_BL             number(22,2),
 COR_BEG_RESERVE_BL             number(22,2),
 COR_EXPENSE_BL                 number(22,2),
 COR_EXP_ADJUST_BL              number(22,2),
 COR_EXP_ALLOC_ADJUST_BL        number(22,2),
 COR_RES_TRAN_IN_BL             number(22,2),
 COR_RES_TRAN_OUT_BL            number(22,2),
 COR_RES_ADJUST_BL              number(22,2),
 COR_END_RESERVE_BL             number(22,2),
 COST_OF_REMOVAL_RATE_BL        number(22,8),
 COST_OF_REMOVAL_BASE_BL        number(22,2),
 RWIP_COST_OF_REMOVAL_BL        number(22,2),
 RWIP_SALVAGE_CASH_BL           number(22,2),
 RWIP_SALVAGE_RETURNS_BL        number(22,2),
 RWIP_RESERVE_CREDITS_BL        number(22,2),
 SALVAGE_RATE_BL                number(22,8),
 SALVAGE_BASE_BL                number(22,2),
 SALVAGE_EXPENSE_BL             number(22,2),
 SALVAGE_EXP_ADJUST_BL          number(22,2),
 SALVAGE_EXP_ALLOC_ADJUST_BL    number(22,2),
 RESERVE_BAL_SALVAGE_EXP_BL     number(22,2),
 BLENDING_ADJUSTMENT            number(22,2),
 RESERVE_BLENDING_ADJUSTMENT    number(22,2),
 COR_BLENDING_ADJUSTMENT        number(22,2),
 RESERVE_BLENDING_TRANSFER      number(22,2),
 COR_BLENDING_TRANSFER          number(22,2),
 IMPAIRMENT_ASSET_AMOUNT_BL     number(22,2),
 IMPAIRMENT_EXPENSE_AMOUNT_BL   number(22,2),
 RESERVE_BAL_IMPAIRMENT_BL      number(22,2)
);

alter table FCST_DEPR_LEDGER_BLENDING
   add constraint FD_LEDGER_BL_PK
       primary key (SET_OF_BOOKS_ID, FCST_DEPR_GROUP_ID, GL_POST_MO_YR)
       using index tablespace PWRPLANT_IDX;

CREATE INDEX FCST_MO_BOOKS_BL_IDX
   on FCST_DEPR_LEDGER_BLENDING (GL_POST_MO_YR, SET_OF_BOOKS_ID)
      tablespace PWRPLANT_IDX;

--* FCST_DEPR_LEDGER_BLENDING comments
comment on column FCST_DEPR_LEDGER_BLENDING.SET_OF_BOOKS_ID is 'Source Set of Books based on Depreciation Method Blending configuration.  System-assigned identifier of a unique set of books maintained by the utility in PowerPlan.';
comment on column FCST_DEPR_LEDGER_BLENDING.FCST_DEPR_GROUP_ID is 'System-assigned identifier of a particular forecast depreciation group.';
comment on column FCST_DEPR_LEDGER_BLENDING.GL_POST_MO_YR is 'Records for a given depreciation group the accounting month and year of activity the entry represents.  Each month a new set of entries is filled in by the system.';
comment on column FCST_DEPR_LEDGER_BLENDING.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column FCST_DEPR_LEDGER_BLENDING.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column FCST_DEPR_LEDGER_BLENDING.DEPR_LEDGER_STATUS is 'Represents the processing status of a depreciation ledger record.  An example would be ''OPEN'', meaning the record is available for posting.  These are coded values for program processing only.';
comment on column FCST_DEPR_LEDGER_BLENDING.SOURCE_PERCENT is 'Blending percentage from Depreciation Method Blending used to calculate amounts for this record.';
comment on column FCST_DEPR_LEDGER_BLENDING.BEGIN_RESERVE_BL is 'Records the beginning balance of the Depreciation Reserve (including the Accumulated Provision) for the depreciation group for the month in dollars.  Note that a normal reserve (credit balance) is positive.  This excludes 1.) the impairment reserve, 2.) allocated RWIP and 3.) the COR Reserves.';
comment on column FCST_DEPR_LEDGER_BLENDING.END_RESERVE_BL is '=  begin_reserve_bl
+ retirements_bl
+ gain_loss_bl
+ salvage_cash_bl
+ salvage_returns
+ reserve_credits
+ reserve_tran_in_bl
+ reserve_tran_out_bl
+ reserve_adjustment_bl
+ depr_exp_alloc_adjust_bl
+ depr_exp_adjust_bl
+ depreciation_expense_bl
current_net_salvage_amort_bl
+ salvage_expense_bl
+ salvage_exp_adjust_bl
+ salvage_exp_alloc_adjust_bl
+ reserve_blending_adjustment
+ reserve_blending_transfer
+  impairment_asset_amount_bl
+ impairment_expense_amount_bl
Depreciation Ledger Blending Adjustment  and  Transfer Example
1.��� From the losers (in allocation %), transfer out the reserve associated with the ""transfer"" of the asset:
2.��� Allocate the reserve transferred out to the gainers, e.g. the kth gainer gets:
3.��� Note:  Cost of Removal and Salvage Proceeds are applied on a pro rata (Jur%) basis
Year 1   100% Reserve    Jurisdictional Reserve';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_BAL_PROVISION_BL is 'Records the accumulated provision for depreciation included in the reserve for the depreciation group in dollars at month-end for closed months.  Note that the normal accumulated depreciation provision (credit balance) is positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_BAL_COR_BL is 'Records the accumulated cost of removal (incurred) in the COR reserve for the depreciation group in dollars at month end for closed months.  Note that the normal accumulated cost of removal (debit balance) is held as a negative.';
comment on column FCST_DEPR_LEDGER_BLENDING.SALVAGE_BALANCE_BL is 'Records the accumulated salvage proceeds in the reserve for the depreciation group in dollars at month-end for closed months.  Note that the normal accumulated salvage (credit balance) is held as a positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_BAL_ADJUST_BL is 'Records the accumulated reserve adjustments in the reserve for the depreciation group in dollars at month-end for closed months.  An accumulated credit is positive; an accumulated debit is negative.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_BAL_RETIREMENTS_BL is 'Records the accumulated retirements in the reserve for the depreciation group at month-end for closed months.  Note that the original cost allowed is included in this number, even when a gain/loss is explicitly calculated and not rolled into the reserve.  In this case the reserve_bal_gain_loss will hold the offset.  The normal balance (debit) is negative.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_BAL_TRAN_IN_BL is 'Records the accumulated reserve transfers into the depreciation group in dollars at month-end for closed months.  The normal balance (credit) is positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_BAL_TRAN_OUT_BL is 'Records the accumulated reserve transfers out of the depreciation group in dollars at month-end for closed months.  The normal balance (debit) is negative.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_BAL_OTHER_CREDITS_BL is 'Records the accumulated other credits in the reserve for the depreciation group at month-end for closed months.  The normal balance (credit) is positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_BAL_GAIN_LOSS_BL is 'Records the accumulated gain/loss recorded on the income statement for asset dispositions.  At month-end for closed months this is essentially a contra to the other accumulated reserve activities.  The normal balance (credit) is negative.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_ALLOC_FACTOR_BL is '(Not currently used)';
comment on column FCST_DEPR_LEDGER_BLENDING.BEGIN_BALANCE_BL is 'Records the asset balance for the depreciation group at the beginning of the month in dollars.';
comment on column FCST_DEPR_LEDGER_BLENDING.ADDITIONS_BL is 'Records the asset additions activity that occurred during the month for the depreciation group in dollars.  The interpretation is the FERC activity, e.g., including adjustment transactions reclassified as a FERC add.  Normally positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.RETIREMENTS_BL is 'Records the asset retirement activity that occurred during the month in dollars.  These are just the retirement transactions.  Normally negative.  These are also used to reduce the reserve.';
comment on column FCST_DEPR_LEDGER_BLENDING.TRANSFERS_IN_BL is 'Records the asset transfers into the depreciation group for the month in dollars. (Excludes transfers directly from non-unitized to unitized.).  The interpretation is the FERC activity, e.g. it excludes transfer transactions that have been reclassified as FERC additions.  Normally positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.TRANSFERS_OUT_BL is 'Records the asset transfers out of the depreciation group that occurred during the month in dollars. (Excludes transfers directly from non-unitized to unitized.).  The interpretation is the FERC activity, e.g. it excludes transfer transactions that have been reclassified as FERC additions.  Normally negative.';
comment on column FCST_DEPR_LEDGER_BLENDING.ADJUSTMENTS_BL is 'Records the asset adjustments that occurred to the depreciation group during the month in dollars.  The interpretation is the FERC activity, e.g. it excludes adjustment transactions that have been reclassified as FERC additions or retirements.  (Increases in the depreciable balance are positive.)';
comment on column FCST_DEPR_LEDGER_BLENDING.DEPRECIATION_BASE_BL is 'Records the calculated depreciable base for the depreciation group for the month in dollars.  It is the dollar base to which a monthly rate is applied.  This is dependent on the method and convention.  It can also be factored up (or down) for retroactive rate adjustments and adjustments/catch up for the curve and yearly methods.';
comment on column FCST_DEPR_LEDGER_BLENDING.END_BALANCE_BL is 'Records the asset balance for the depreciation group at the end of the month in dollars.';
comment on column FCST_DEPR_LEDGER_BLENDING.DEPRECIATION_RATE_BL is 'Records the annual rate initially used to calculate the month''s depreciation expense, (for audit trail only).  It is not necessarily the effective rate; it does not take into account input adjustments or true-up (curve method) or retroactive true-ups.  (See depreciation_base.)';
comment on column FCST_DEPR_LEDGER_BLENDING.DEPRECIATION_EXPENSE_BL is 'Records the depreciation calculated for the depreciation group for the month in dollars.  (This may be charged to an account other than depreciation expense.)  Note:  If the cost of removal has been broken out, this does not contain the cost of removal provision, which is then in ''cor_expense'' below.';
comment on column FCST_DEPR_LEDGER_BLENDING.DEPR_EXP_ADJUST_BL is 'Records any input adjustment to the depreciation expense for the depreciation group for the month in dollars.';
comment on column FCST_DEPR_LEDGER_BLENDING.DEPR_EXP_ALLOC_ADJUST_BL is 'Records any calculated adjustment to depreciation expense for the depreciation group for the month, for example, combined group adjustments, depreciation check adjustment, or retroactive rate recalculation adjustments.';
comment on column FCST_DEPR_LEDGER_BLENDING.COST_OF_REMOVAL_BL is 'Records the unitized actual removal costs incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is negative, reducing the reserve.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_RETIREMENTS_BL is '(Not Used.  See ''retirements'').';
comment on column FCST_DEPR_LEDGER_BLENDING.SALVAGE_RETURNS_BL is 'Records the unitized salvaged returns costs incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column FCST_DEPR_LEDGER_BLENDING.SALVAGE_CASH_BL is 'Records the unitized cash salvage incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_CREDITS_BL is 'Records the other credits incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_ADJUSTMENTS_BL is 'Records the input dollar adjustments incurred against the Depreciation reserve for the depreciation group for the month.  (An increase to reserve, e.g. a credit, is positive.)';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_TRAN_IN_BL is 'Records the reserve transfers in to the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_TRAN_OUT_BL is 'Records the reserve transfers out of the depreciation group for the month.  The normal sign is negative, decreasing the reserve.';
comment on column FCST_DEPR_LEDGER_BLENDING.GAIN_LOSS_BL is 'Records the gain/loss calculated for the month.  It is a contra to the retirements and is accumulated in reserve_bal_gain_loss.  A gain is normally negative; a loss positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.VINTAGE_NET_SALVAGE_AMORT_BL is 'Amount of net salvage amortization in dollars for the current period pertaining to the particular salvage given by the gl_post_mo_year.  E.g., for the 8/1998 gl_post_mo_yr, this item will contain the amortization of 8/1998 (vintage) net salvage in all successive months.  Note that the amortization from a previous month is overlaid.  That is why it is saved in total in ''current_net_salvage_amort'' below.  This variable applies only if the salvage treatment or COR treatment on depr group is ''Amort''.';
comment on column FCST_DEPR_LEDGER_BLENDING.VINTAGE_NET_SALVAGE_RESERVE_BL is 'Cumulative amount of net salvage amortization in dollars for the current period pertaining to the particular salvage and cost of removal for the vintage month/year given by the gl_post_mo_yr.  E.g., for the 8/1998 gl_post_mo_yr this item will contain the cumulative month-end amortization of 8/1998 (vintage) net salvage in all successive months.  Note that the cumulative amortization from a previous month is overlaid.  That is why it is saved in total in ''current_net_salvage_reserve'' below.  This variable applies only if the salvage treatment or COR treatment on depr group is ''Amort''.';
comment on column FCST_DEPR_LEDGER_BLENDING.CURRENT_NET_SALVAGE_AMORT_BL is 'Total amount in dollars of net salvage amortization for the current month, summarized and saved.  This variable applies only if the salvage treatment or COR treatment on depr group is ''Amort''.  Note:  This variable has the opposite sign � an expense is negative.  Its impact is in the ""end_reserve.""  The equivalent for cost of removal is in the ""cor_expense.""';
comment on column FCST_DEPR_LEDGER_BLENDING.CURRENT_NET_SALVAGE_RESERVE_BL is 'Total cumulative amount (in dollars) of net salvage for the end of the month, summarized and saved.  This variable applies only if the salvage treatment or COR treatment on depr group is ''Amort''.  Reserve is negative if there is negative net salvage.  The reserve is not reduced for fully amortized basis.';
comment on column FCST_DEPR_LEDGER_BLENDING.IMPAIRMENT_RESERVE_BEG_BL is 'Records beginning of the month impairment reserve in dollars.  This is excluded from the other reserve balances.  A credit balance is positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.IMPAIRMENT_RESERVE_ACT_BL is 'Records the impairment reserve activity for the month in dollars whether input or calculated from retirements.  A credit balance is positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.IMPAIRMENT_RESERVE_END_BL is 'Records the impairment reserve at the end of the month in dollars.  This is excluded from the other reserve balances.  A credit balance is positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.EST_ANN_NET_ADDS_BL is 'Records the estimated annual net additions used during that month''s calculation � used for audit trail only.';
comment on column FCST_DEPR_LEDGER_BLENDING.RWIP_ALLOCATION_BL is 'Records the optional allocation of the month end total RWIP balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.  It is the sum of the following four items:  1.) rwip_cost_of_removal, 2.) rwip_salvage_cash, 3.) rwip_salvage_returns, and 4.) rwip_reserve_credits.';
comment on column FCST_DEPR_LEDGER_BLENDING.COR_BEG_RESERVE_BL is 'Beginning of the month cost of removal reserve balance in dollars.  Includes both the book provision and actual incurred.';
comment on column FCST_DEPR_LEDGER_BLENDING.COR_EXPENSE_BL is 'Cost of removal expense provision (when identified) in dollars.  It also includes any cost of removal being amortized.  (See current Net Salvage Amort.)';
comment on column FCST_DEPR_LEDGER_BLENDING.COR_EXP_ADJUST_BL is 'Input cost of removal expense adjustment for the month, in dollars.';
comment on column FCST_DEPR_LEDGER_BLENDING.COR_EXP_ALLOC_ADJUST_BL is 'Calculated cost of removal expense adjustment for the month, in dollars.';
comment on column FCST_DEPR_LEDGER_BLENDING.COR_RES_TRAN_IN_BL is 'Cost of removal balance transferred in during the month, normally associated with asset transfers.';
comment on column FCST_DEPR_LEDGER_BLENDING.COR_RES_TRAN_OUT_BL is 'Cost of removal transferred out during the month in dollars, normally associated with asset transfers.';
comment on column FCST_DEPR_LEDGER_BLENDING.COR_RES_ADJUST_BL is 'Input adjustments to the cost of removal reserve for the month in dollars.';
comment on column FCST_DEPR_LEDGER_BLENDING.COR_END_RESERVE_BL is 'Month end (net) cost of removal reserve balance in dollars.';
comment on column FCST_DEPR_LEDGER_BLENDING.COST_OF_REMOVAL_RATE_BL is 'Annual rate used to calculate the cost of removal provision; Expressed as a decimal.  Not the effective rate (e.g., it does not take into account adjustments).';
comment on column FCST_DEPR_LEDGER_BLENDING.COST_OF_REMOVAL_BASE_BL is 'Records the dollar base against which the cost of removal rate was applied in that month.';
comment on column FCST_DEPR_LEDGER_BLENDING.RWIP_COST_OF_REMOVAL_BL is 'Records the optional allocation of the month end RWIP cost of removal balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.RWIP_SALVAGE_CASH_BL is 'Records the optional allocation of the month end RWIP salvage cash balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.RWIP_SALVAGE_RETURNS_BL is 'Records the optional allocation of the month end RWIP salvage returns balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.RWIP_RESERVE_CREDITS_BL is 'Records the optional allocation of the month end RWIP reserve credits balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column FCST_DEPR_LEDGER_BLENDING.SALVAGE_RATE_BL is 'Annual rate used to calculate the salvage provision; Expressed as a decimal. Not the effective rate (i.e. it does not take into expense adjustments). The default is 0.';
comment on column FCST_DEPR_LEDGER_BLENDING.SALVAGE_BASE_BL is 'Records the dollar base against which the salvage rate was applied in that month. The default is 0.';
comment on column FCST_DEPR_LEDGER_BLENDING.SALVAGE_EXPENSE_BL is 'Salvage ""expense"" provison (when identified) in dollars. The default is 0.   Generally negative.';
comment on column FCST_DEPR_LEDGER_BLENDING.SALVAGE_EXP_ADJUST_BL is 'Input salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column FCST_DEPR_LEDGER_BLENDING.SALVAGE_EXP_ALLOC_ADJUST_BL is 'Calculated salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_BAL_SALVAGE_EXP_BL is 'Records the accumulated salvage provision (when identified) for depreciation included in the reserve for the depreciation group in dollars at month-end for closed months. Note that the normal accumulated depreciation salvage provision (debit balance) is negative because it decreases total reserve.';
comment on column FCST_DEPR_LEDGER_BLENDING.BLENDING_ADJUSTMENT is 'The plant balance adjustment due to a change in blending percentage from last month to this moth (see example in Depr Ledger Blending below).';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_BLENDING_ADJUSTMENT is 'The life reserve balance adjustment due to a change in blending percentage from last month to this month (see example in Depr Ledger Blending below).';
comment on column FCST_DEPR_LEDGER_BLENDING.COR_BLENDING_ADJUSTMENT is 'The COR reserve balance adjustment due to a change in blending percentage from last month to this month (see example in Depr Ledger Blending below).';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_BLENDING_TRANSFER is 'The life reserve transfer necessary to offset the reserve blending adjustment to zero across all source sets of books (see example in Depr Ledger Blending below).';
comment on column FCST_DEPR_LEDGER_BLENDING.COR_BLENDING_TRANSFER is 'The COR reserve transfer necessary to offset the reserve blending adjustment to zero across all source sets of books (see example in Depr Ledger Blending below).';
comment on column FCST_DEPR_LEDGER_BLENDING.IMPAIRMENT_ASSET_AMOUNT_BL is 'The amount to be posted to the CPR and recorded in an asset impairment bucket which reduces the asset cost and reduces the reserve balance.';
comment on column FCST_DEPR_LEDGER_BLENDING.IMPAIRMENT_EXPENSE_AMOUNT_BL is 'The expense amount is equal to the change in NBV, e.g. the amount booked to an expense account, and only affects the reserve balance.';
comment on column FCST_DEPR_LEDGER_BLENDING.RESERVE_BAL_IMPAIRMENT_BL is 'The accumulated impairment reserve balance (impairment_asset_amount_bl + impairment_expense_amount_bl)
PowerPlan Depreciation Ledger Blending Illustration';

--* Forecast calculation archive table
create table FCST_DEPR_CALC_STG_ARC
(
 FCST_DEPR_GROUP_ID             number(22,0),
 FCST_DEPR_VERSION_ID           number(22,0),
 SET_OF_BOOKS_ID                number(22,0),
 GL_POST_MO_YR                  date,
 CALC_MONTH                     date,
 DEPR_CALC_STATUS               number(2,0),
 DEPR_CALC_MESSAGE              varchar2(2000),
 TRF_IN_EST_ADDS                varchar2(35),
 INCLUDE_RWIP_IN_NET            varchar2(35),
 DNSA_COR_BAL                   number(22,2),
 DNSA_SALV_BAL                  number(22,2),
 COR_YTD                        number(22,2),
 SALV_YTD                       number(22,2),
 FISCALYEAROFFSET               number(2,0),
 FISCALYEARSTART                date,
 COMPANY_ID                     number(22,0),
 SUBLEDGER_TYPE_ID              number(22,0),
 DESCRIPTION                    varchar2(254),
 FCST_DEPR_METHOD_ID            number(22,0),
 MID_PERIOD_CONV                number(22,2),
 MID_PERIOD_METHOD              varchar2(35),
 DG_EST_ANN_NET_ADDS            number(22,8),
 TRUE_UP_CPR_DEPR               number(22,0),
 RATE                           number(22,8),
 NET_GROSS                      number(22,0),
 OVER_DEPR_CHECK                number(22,0),
 NET_SALVAGE_PCT                number(22,8),
 RATE_USED_CODE                 number(22,0),
 END_OF_LIFE                    number(22,0),
 EXPECTED_AVERAGE_LIFE          number(22,0),
 RESERVE_RATIO_ID               number(22,0),
 DMR_COST_OF_REMOVAL_RATE       number(22,8),
 COST_OF_REMOVAL_PCT            number(22,8),
 DMR_SALVAGE_RATE               number(22,8),
 INTEREST_RATE                  number(22,8),
 AMORTIZABLE_LIFE               number(22,0),
 COR_TREATMENT                  number(1,0),
 SALVAGE_TREATMENT              number(1,0),
 NET_SALVAGE_AMORT_LIFE         number(22,0),
 ALLOCATION_PROCEDURE           varchar2(5),
 DEPR_LEDGER_STATUS             number(22,0),
 BEGIN_RESERVE                  number(22,2),
 END_RESERVE                    number(22,2),
 RESERVE_BAL_PROVISION          number(22,2),
 RESERVE_BAL_COR                number(22,2),
 SALVAGE_BALANCE                number(22,2),
 RESERVE_BAL_ADJUST             number(22,2),
 RESERVE_BAL_RETIREMENTS        number(22,2),
 RESERVE_BAL_TRAN_IN            number(22,2),
 RESERVE_BAL_TRAN_OUT           number(22,2),
 RESERVE_BAL_OTHER_CREDITS      number(22,2),
 RESERVE_BAL_GAIN_LOSS          number(22,2),
 RESERVE_ALLOC_FACTOR           number(22,2),
 BEGIN_BALANCE                  number(22,2),
 ADDITIONS                      number(22,2),
 RETIREMENTS                    number(22,2),
 TRANSFERS_IN                   number(22,2),
 TRANSFERS_OUT                  number(22,2),
 ADJUSTMENTS                    number(22,2),
 DEPRECIATION_BASE              number(22,2),
 END_BALANCE                    number(22,2),
 DEPRECIATION_RATE              number(22,8),
 DEPRECIATION_EXPENSE           number(22,2),
 DEPR_EXP_ADJUST                number(22,2),
 DEPR_EXP_ALLOC_ADJUST          number(22,2),
 COST_OF_REMOVAL                number(22,2),
 RESERVE_RETIREMENTS            number(22,2),
 SALVAGE_RETURNS                number(22,2),
 SALVAGE_CASH                   number(22,2),
 RESERVE_CREDITS                number(22,2),
 RESERVE_ADJUSTMENTS            number(22,2),
 RESERVE_TRAN_IN                number(22,2),
 RESERVE_TRAN_OUT               number(22,2),
 GAIN_LOSS                      number(22,2),
 VINTAGE_NET_SALVAGE_AMORT      number(22,2),
 VINTAGE_NET_SALVAGE_RESERVE    number(22,2),
 CURRENT_NET_SALVAGE_AMORT      number(22,2),
 CURRENT_NET_SALVAGE_RESERVE    number(22,2),
 IMPAIRMENT_RESERVE_BEG         number(22,2),
 IMPAIRMENT_RESERVE_ACT         number(22,2),
 IMPAIRMENT_RESERVE_END         number(22,2),
 EST_ANN_NET_ADDS               number(22,2),
 RWIP_ALLOCATION                number(22,2),
 COR_BEG_RESERVE                number(22,2),
 COR_EXPENSE                    number(22,2),
 COR_EXP_ADJUST                 number(22,2),
 COR_EXP_ALLOC_ADJUST           number(22,2),
 COR_RES_TRAN_IN                number(22,2),
 COR_RES_TRAN_OUT               number(22,2),
 COR_RES_ADJUST                 number(22,2),
 COR_END_RESERVE                number(22,2),
 COST_OF_REMOVAL_RATE           number(22,8),
 COST_OF_REMOVAL_BASE           number(22,2),
 RWIP_COST_OF_REMOVAL           number(22,2),
 RWIP_SALVAGE_CASH              number(22,2),
 RWIP_SALVAGE_RETURNS           number(22,2),
 RWIP_RESERVE_CREDITS           number(22,2),
 SALVAGE_RATE                   number(22,8),
 SALVAGE_BASE                   number(22,2),
 SALVAGE_EXPENSE                number(22,2),
 SALVAGE_EXP_ADJUST             number(22,2),
 SALVAGE_EXP_ALLOC_ADJUST       number(22,2),
 RESERVE_BAL_SALVAGE_EXP        number(22,2),
 RESERVE_BLENDING_ADJUSTMENT    number(22,2),
 RESERVE_BLENDING_TRANSFER      number(22,2),
 COR_BLENDING_ADJUSTMENT        number(22,2),
 COR_BLENDING_TRANSFER          number(22,2),
 IMPAIRMENT_ASSET_AMOUNT        number(22,2),
 IMPAIRMENT_EXPENSE_AMOUNT      number(22,2),
 RESERVE_BAL_IMPAIRMENT         number(22,2),
 BEGIN_BALANCE_YEAR             number(22,2),
 BEGIN_RESERVE_YEAR             number(22,2),
 BEGIN_BALANCE_QTR              number(22,2),
 BEGIN_RESERVE_QTR              number(22,2),
 PLANT_ACTIVITY_2               number(22,2),
 RESERVE_ACTIVITY_2             number(22,2),
 EST_NET_ADDS                   number(22,2),
 CUMULATIVE_TRANSFERS           number(22,2),
 RESERVE_DIFF                   number(22,2),
 FISCAL_MONTH                   number(2,0),
 FISCALQTRSTART                 date,
 TYPE_2_EXIST                   number(1,0),
 CUM_UOP_DEPR                   number(22,2),
 CUM_UOP_DEPR_2                 number(22,2),
 PRODUCTION                     number(22,2),
 ESTIMATED_PRODUCTION           number(22,2),
 PRODUCTION_2                   number(22,2),
 ESTIMATED_PRODUCTION_2         number(22,2),
 SMOOTH_CURVE                   varchar2(5),
 MIN_CALC                       number(22,2),
 MIN_UOP_EXP                    number(22,2),
 YTD_UOP_DEPR                   number(22,2),
 YTD_UOP_DEPR_2                 number(22,2),
 CURR_UOP_EXP                   number(22,2),
 CURR_UOP_EXP_2                 number(22,2),
 LESS_YEAR_UOP_DEPR             number(22,2),
 DEPRECIATION_UOP_RATE          number(22,8),
 DEPRECIATION_UOP_RATE_2        number(22,8),
 DEPRECIATION_BASE_UOP          number(22,2),
 DEPRECIATION_BASE_UOP_2        number(22,2),
 COR_ACTIVITY_2                 number(22,2),
 OVER_DEPR_ADJ                  number(22,2),
 OVER_DEPR_ADJ_COR              number(22,2),
 OVER_DEPR_ADJ_SALV             number(22,2),
 EFFECTIVE_BEGIN_RESERVE        number(22,2),
 EFFECTIVE_BEGIN_RESERVE_QTR    number(22,2),
 EFFECTIVE_BEGIN_RESERVE_YEAR   number(22,2),
 EFFECTIVE_END_RESERVE          number(22,2),
 EFFECTIVE_COR_BEG_RESERVE      number(22,2),
 EFFECTIVE_COR_END_RESERVE      number(22,2),
 EFFECTIVE_DEPR_BASE            number(22,2),
 EFFECTIVE_SALVAGE_BASE         number(22,2),
 EFFECTIVE_COR_BASE             number(22,2),
 RETRO_DEPR_EXPENSE             number(22,2),
 RETRO_COR_EXPENSE              number(22,2),
 RETRO_SALV_EXPENSE             number(22,2),
 ORIG_DEPR_EXPENSE              number(22,2),
 ORIG_DEPR_EXP_ALLOC_ADJUST     number(22,2),
 ORIG_SALV_EXPENSE              number(22,2),
 ORIG_SALV_EXP_ALLOC_ADJUST     number(22,2),
 ORIG_COR_EXP_ALLOC_ADJUST      number(22,2),
 ORIG_COR_EXPENSE               number(22,2),
 RETRO_DEPR_ADJ                 number(22,2),
 RETRO_COR_ADJ                  number(22,2),
 RETRO_SALV_ADJ                 number(22,2),
 IMPAIRMENT_ASSET_ACTIVITY_SALV number(22,2),
 IMPAIRMENT_ASSET_BEGIN_BALANCE number(22,2),
 ORIG_BEGIN_RESERVE             number(22,2),
 ORIG_COR_BEG_RESERVE           number(22,2),
 BEGIN_COR_YEAR                 number(22,2),
 BEGIN_COR_QTR                  number(22,2),
 PLANT_ACTIVITY                 number(22,2),
 COR_ACTIVITY                   number(22,2),
 RESERVE_ACTIVITY               number(22,2)
);

comment on column FCST_DEPR_CALC_STG_ARC.FCST_DEPR_GROUP_ID is 'Internal ID linking to the depreciation group';
comment on column FCST_DEPR_CALC_STG_ARC.FCST_DEPR_VERSION_ID is 'Internal ID linking to the depreciation version';
comment on column FCST_DEPR_CALC_STG_ARC.SET_OF_BOOKS_ID is 'The set of books';
comment on column FCST_DEPR_CALC_STG_ARC.GL_POST_MO_YR is 'The accounting month';
comment on column FCST_DEPR_CALC_STG_ARC.CALC_MONTH is 'The month being calculated';
comment on column FCST_DEPR_CALC_STG_ARC.DEPR_CALC_STATUS is 'Status column';
comment on column FCST_DEPR_CALC_STG_ARC.DEPR_CALC_MESSAGE is 'An error mesaging';
comment on column FCST_DEPR_CALC_STG_ARC.TRF_IN_EST_ADDS is 'Are transfers included int eh net adds';
comment on column FCST_DEPR_CALC_STG_ARC.INCLUDE_RWIP_IN_NET is 'Is rwip included in the net reserve';
comment on column FCST_DEPR_CALC_STG_ARC.DNSA_COR_BAL is 'Net Salvage Amortization Balance for Cost of Removal';
comment on column FCST_DEPR_CALC_STG_ARC.DNSA_SALV_BAL is 'Net Salvage Amortization Balance for Salvage';
comment on column FCST_DEPR_CALC_STG_ARC.COR_YTD is 'The year to date cost of removal';
comment on column FCST_DEPR_CALC_STG_ARC.SALV_YTD is 'The year to date salvage';
comment on column FCST_DEPR_CALC_STG_ARC.FISCALYEAROFFSET is 'Fiscal year offset';
comment on column FCST_DEPR_CALC_STG_ARC.FISCALYEARSTART is 'The start of the fiscal year';
comment on column FCST_DEPR_CALC_STG_ARC.COMPANY_ID is 'Internal ID linking to company';
comment on column FCST_DEPR_CALC_STG_ARC.SUBLEDGER_TYPE_ID is 'The subledger indicator';
comment on column FCST_DEPR_CALC_STG_ARC.DESCRIPTION is 'The description';
comment on column FCST_DEPR_CALC_STG_ARC.FCST_DEPR_METHOD_ID is 'Internal ID linking to the depreciation method';
comment on column FCST_DEPR_CALC_STG_ARC.MID_PERIOD_CONV is 'The percent of current activity to include in depreciation base';
comment on column FCST_DEPR_CALC_STG_ARC.MID_PERIOD_METHOD is 'The mid period method';
comment on column FCST_DEPR_CALC_STG_ARC.DG_EST_ANN_NET_ADDS is 'The estimated net additions and adjustment';
comment on column FCST_DEPR_CALC_STG_ARC.TRUE_UP_CPR_DEPR is 'Trueup individually depreciated assets';
comment on column FCST_DEPR_CALC_STG_ARC.RATE is 'The rate';
comment on column FCST_DEPR_CALC_STG_ARC.NET_GROSS is '1 means calculate depreciation based on the net balance.';
comment on column FCST_DEPR_CALC_STG_ARC.OVER_DEPR_CHECK is 'Perform overdepreciation checks';
comment on column FCST_DEPR_CALC_STG_ARC.NET_SALVAGE_PCT is 'Percent of beginning balance that should be salvage';
comment on column FCST_DEPR_CALC_STG_ARC.RATE_USED_CODE is 'Has the rate been used in a calculation before';
comment on column FCST_DEPR_CALC_STG_ARC.END_OF_LIFE is 'End of life for end of life methods';
comment on column FCST_DEPR_CALC_STG_ARC.EXPECTED_AVERAGE_LIFE is 'The expected average life';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_RATIO_ID is 'The reserve ration';
comment on column FCST_DEPR_CALC_STG_ARC.DMR_COST_OF_REMOVAL_RATE is 'The cost of removal rate';
comment on column FCST_DEPR_CALC_STG_ARC.COST_OF_REMOVAL_PCT is 'The percentage of the plant balance that is estimated cost of removal';
comment on column FCST_DEPR_CALC_STG_ARC.DMR_SALVAGE_RATE is 'The salvage rate';
comment on column FCST_DEPR_CALC_STG_ARC.INTEREST_RATE is 'The interest rate';
comment on column FCST_DEPR_CALC_STG_ARC.AMORTIZABLE_LIFE is 'The amortizable life of the depreciation group';
comment on column FCST_DEPR_CALC_STG_ARC.COR_TREATMENT is 'How cost of removal should be treated';
comment on column FCST_DEPR_CALC_STG_ARC.SALVAGE_TREATMENT is 'How is salvage treated';
comment on column FCST_DEPR_CALC_STG_ARC.NET_SALVAGE_AMORT_LIFE is 'Net Salvage Amortization life';
comment on column FCST_DEPR_CALC_STG_ARC.ALLOCATION_PROCEDURE is 'The depreciation allocation procedure';
comment on column FCST_DEPR_CALC_STG_ARC.DEPR_LEDGER_STATUS is 'The depr ledger status';
comment on column FCST_DEPR_CALC_STG_ARC.BEGIN_RESERVE is 'The Beginning reserve for the period';
comment on column FCST_DEPR_CALC_STG_ARC.END_RESERVE is 'The end reserve for the period';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_BAL_PROVISION is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_BAL_COR is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.SALVAGE_BALANCE is 'The salvage balance';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_BAL_ADJUST is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_BAL_RETIREMENTS is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_BAL_TRAN_IN is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_BAL_TRAN_OUT is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_BAL_OTHER_CREDITS is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_BAL_GAIN_LOSS is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_ALLOC_FACTOR is 'The reserve allocation factor';
comment on column FCST_DEPR_CALC_STG_ARC.BEGIN_BALANCE is 'The beginning plant balance for the period';
comment on column FCST_DEPR_CALC_STG_ARC.ADDITIONS is 'The plant addidtions';
comment on column FCST_DEPR_CALC_STG_ARC.RETIREMENTS is 'The retirement amount';
comment on column FCST_DEPR_CALC_STG_ARC.TRANSFERS_IN is 'Transfer In Amount';
comment on column FCST_DEPR_CALC_STG_ARC.TRANSFERS_OUT is 'Transfer Out Amount';
comment on column FCST_DEPR_CALC_STG_ARC.ADJUSTMENTS is 'Plant Adjustments';
comment on column FCST_DEPR_CALC_STG_ARC.DEPRECIATION_BASE is 'The deprecation base';
comment on column FCST_DEPR_CALC_STG_ARC.END_BALANCE is 'The end plant balance';
comment on column FCST_DEPR_CALC_STG_ARC.DEPRECIATION_RATE is 'The depreciation rate';
comment on column FCST_DEPR_CALC_STG_ARC.DEPRECIATION_EXPENSE is 'The current period calculated depreciation expense';
comment on column FCST_DEPR_CALC_STG_ARC.DEPR_EXP_ADJUST is 'Depreciation Expense Adjustment';
comment on column FCST_DEPR_CALC_STG_ARC.DEPR_EXP_ALLOC_ADJUST is 'Depreciation Expense Adjustment';
comment on column FCST_DEPR_CALC_STG_ARC.COST_OF_REMOVAL is 'Cost of Removal for the period';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_RETIREMENTS is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.SALVAGE_RETURNS is 'Salvage returns';
comment on column FCST_DEPR_CALC_STG_ARC.SALVAGE_CASH is 'Salvage Cash';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_CREDITS is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_ADJUSTMENTS is 'Reserve adjustment amount';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_TRAN_IN is 'Reserve Transfer Into this group';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_TRAN_OUT is 'Reserve Transfer out of this group';
comment on column FCST_DEPR_CALC_STG_ARC.GAIN_LOSS is 'Gain loss';
comment on column FCST_DEPR_CALC_STG_ARC.VINTAGE_NET_SALVAGE_AMORT is 'The vintaged net salvage amortization';
comment on column FCST_DEPR_CALC_STG_ARC.VINTAGE_NET_SALVAGE_RESERVE is 'The vintaged net salvage reserve';
comment on column FCST_DEPR_CALC_STG_ARC.CURRENT_NET_SALVAGE_AMORT is 'Current Net Salvage Amortization';
comment on column FCST_DEPR_CALC_STG_ARC.CURRENT_NET_SALVAGE_RESERVE is 'Current Net Salvage Reserve';
comment on column FCST_DEPR_CALC_STG_ARC.IMPAIRMENT_RESERVE_BEG is 'The beginning balance for the impairment basis bucket';
comment on column FCST_DEPR_CALC_STG_ARC.IMPAIRMENT_RESERVE_ACT is 'The activity for the impairment basis bucket';
comment on column FCST_DEPR_CALC_STG_ARC.IMPAIRMENT_RESERVE_END is 'The ending reserve for the impairment';
comment on column FCST_DEPR_CALC_STG_ARC.EST_ANN_NET_ADDS is 'Estimate adds and adjustments';
comment on column FCST_DEPR_CALC_STG_ARC.RWIP_ALLOCATION is 'The amount of rwip allocation to this group';
comment on column FCST_DEPR_CALC_STG_ARC.COR_BEG_RESERVE is 'The COR beginning Reserve for the perios';
comment on column FCST_DEPR_CALC_STG_ARC.COR_EXPENSE is 'Current month calculated cost of removal expense';
comment on column FCST_DEPR_CALC_STG_ARC.COR_EXP_ADJUST is 'Current month cost of removal expense adjustment';
comment on column FCST_DEPR_CALC_STG_ARC.COR_EXP_ALLOC_ADJUST is 'Current month cost of removal expense adjustment';
comment on column FCST_DEPR_CALC_STG_ARC.COR_RES_TRAN_IN is 'Cost of removal transferred in';
comment on column FCST_DEPR_CALC_STG_ARC.COR_RES_TRAN_OUT is 'Cost of removal transferred out';
comment on column FCST_DEPR_CALC_STG_ARC.COR_RES_ADJUST is 'Cost of removal reserve adjustment';
comment on column FCST_DEPR_CALC_STG_ARC.COR_END_RESERVE is 'Cost of removal ending reserve';
comment on column FCST_DEPR_CALC_STG_ARC.COST_OF_REMOVAL_RATE is 'The cost of removal rate';
comment on column FCST_DEPR_CALC_STG_ARC.COST_OF_REMOVAL_BASE is 'The basis for calculating cost of removal expense';
comment on column FCST_DEPR_CALC_STG_ARC.RWIP_COST_OF_REMOVAL is 'The amount of RWIP Allocation that is cost of removal';
comment on column FCST_DEPR_CALC_STG_ARC.RWIP_SALVAGE_CASH is 'The amount of RWIP Allocation that is salvage cash';
comment on column FCST_DEPR_CALC_STG_ARC.RWIP_SALVAGE_RETURNS is 'The amount of RWIP Allocation that is salvage returns';
comment on column FCST_DEPR_CALC_STG_ARC.RWIP_RESERVE_CREDITS is 'The amount of RWIP Allocation that is salvage credits';
comment on column FCST_DEPR_CALC_STG_ARC.SALVAGE_RATE is 'The salvage rate';
comment on column FCST_DEPR_CALC_STG_ARC.SALVAGE_BASE is 'The salvage base';
comment on column FCST_DEPR_CALC_STG_ARC.SALVAGE_EXPENSE is 'The salvage expense';
comment on column FCST_DEPR_CALC_STG_ARC.SALVAGE_EXP_ADJUST is 'The salvage expense adjustment';
comment on column FCST_DEPR_CALC_STG_ARC.SALVAGE_EXP_ALLOC_ADJUST is 'The salvage expense adjustment';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_BAL_SALVAGE_EXP is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_BLENDING_ADJUSTMENT is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_BLENDING_TRANSFER is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.COR_BLENDING_ADJUSTMENT is 'The COR adjustment due to blended set of books';
comment on column FCST_DEPR_CALC_STG_ARC.COR_BLENDING_TRANSFER is 'The COR Transfer due to blended set of books';
comment on column FCST_DEPR_CALC_STG_ARC.IMPAIRMENT_ASSET_AMOUNT is 'The amount of the impairment';
comment on column FCST_DEPR_CALC_STG_ARC.IMPAIRMENT_EXPENSE_AMOUNT is 'The expense amount being impaired';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_BAL_IMPAIRMENT is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.BEGIN_BALANCE_YEAR is 'The beginning plant balance for the year';
comment on column FCST_DEPR_CALC_STG_ARC.BEGIN_RESERVE_YEAR is 'The beginning reserve balance for the year';
comment on column FCST_DEPR_CALC_STG_ARC.BEGIN_COR_YEAR is 'The beginning cost of removal reserve balance for the year';
comment on column FCST_DEPR_CALC_STG_ARC.BEGIN_BALANCE_QTR is 'The beginning plant balance for the quarter';
comment on column FCST_DEPR_CALC_STG_ARC.BEGIN_RESERVE_QTR is 'The beginning reserve balance for the quarter';
comment on column FCST_DEPR_CALC_STG_ARC.BEGIN_COR_QTR is 'The beginning cost of removal reserve balance for the quarter.';
comment on column FCST_DEPR_CALC_STG_ARC.PLANT_ACTIVITY is 'The plant activity for current month.';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_ACTIVITY is 'The reserve activity for the month.';
comment on column FCST_DEPR_CALC_STG_ARC.COR_ACTIVITY is 'The cost of removal reserve activity for the month';
comment on column FCST_DEPR_CALC_STG_ARC.PLANT_ACTIVITY_2 is 'The plant activity for the period. This can be different from plant_activity depending on the mid-period method.';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_ACTIVITY_2 is 'The reserve activity for the period. This can be different from plant_activity depending on the mid-period method.';
comment on column FCST_DEPR_CALC_STG_ARC.COR_ACTIVITY_2 is 'The cost of removal reserve activity for the period. This can be different from plant_activity depending on the mid-period method.';
comment on column FCST_DEPR_CALC_STG_ARC.EST_NET_ADDS is 'Estimated Adds';
comment on column FCST_DEPR_CALC_STG_ARC.CUMULATIVE_TRANSFERS is 'Cumulative transfers';
comment on column FCST_DEPR_CALC_STG_ARC.RESERVE_DIFF is 'Internal field used for depreciation calculation';
comment on column FCST_DEPR_CALC_STG_ARC.FISCAL_MONTH is 'The firscal moth';
comment on column FCST_DEPR_CALC_STG_ARC.FISCALQTRSTART is 'The starting quarter for the month';
comment on column FCST_DEPR_CALC_STG_ARC.TYPE_2_EXIST is 'Used for unit of production calcualations';
comment on column FCST_DEPR_CALC_STG_ARC.CUM_UOP_DEPR is 'Unit of Production';
comment on column FCST_DEPR_CALC_STG_ARC.CUM_UOP_DEPR_2 is 'Unit of Production 2';
comment on column FCST_DEPR_CALC_STG_ARC.PRODUCTION is 'Used for unit of production calcualations';
comment on column FCST_DEPR_CALC_STG_ARC.ESTIMATED_PRODUCTION is 'Estimated Production for unit of production';
comment on column FCST_DEPR_CALC_STG_ARC.PRODUCTION_2 is 'Used for unit of production calcualations';
comment on column FCST_DEPR_CALC_STG_ARC.ESTIMATED_PRODUCTION_2 is 'Estimated Production for unit of production approach 2';
comment on column FCST_DEPR_CALC_STG_ARC.SMOOTH_CURVE is 'Should the trueup methodolgy use smoothing';
comment on column FCST_DEPR_CALC_STG_ARC.MIN_CALC is 'Used for unit of production calcualations';
comment on column FCST_DEPR_CALC_STG_ARC.MIN_UOP_EXP is 'Used for unit of production calcualations';
comment on column FCST_DEPR_CALC_STG_ARC.YTD_UOP_DEPR is 'Used for unit of production calcualations';
comment on column FCST_DEPR_CALC_STG_ARC.YTD_UOP_DEPR_2 is 'Used for unit of production calcualations';
comment on column FCST_DEPR_CALC_STG_ARC.CURR_UOP_EXP is 'Expense for unit of production 1';
comment on column FCST_DEPR_CALC_STG_ARC.CURR_UOP_EXP_2 is 'Expense for unit of production 2';
comment on column FCST_DEPR_CALC_STG_ARC.LESS_YEAR_UOP_DEPR is 'Used for unit of production calcualations';
comment on column FCST_DEPR_CALC_STG_ARC.DEPRECIATION_UOP_RATE is 'The depreciation rate for unit of production';
comment on column FCST_DEPR_CALC_STG_ARC.DEPRECIATION_UOP_RATE_2 is 'The depreciation rate for unit of production approach 2';
comment on column FCST_DEPR_CALC_STG_ARC.DEPRECIATION_BASE_UOP is 'The depreciation base for unit of production';
comment on column FCST_DEPR_CALC_STG_ARC.DEPRECIATION_BASE_UOP_2 is 'The depreciation base for unit of production approach 2';
comment on column FCST_DEPR_CALC_STG_ARC.OVER_DEPR_ADJ is 'The over depr adjustment';
comment on column FCST_DEPR_CALC_STG_ARC.OVER_DEPR_ADJ_COR is 'The over depr adjustment from COR';
comment on column FCST_DEPR_CALC_STG_ARC.OVER_DEPR_ADJ_SALV is 'The over depr adjustment from Salvage';
comment on column FCST_DEPR_CALC_STG_ARC.ORIG_BEGIN_RESERVE is 'The original begin_reserve from depr_ledger. Used because the retroactive rate change calculation can change the begin_reserve column on this table.';
comment on column FCST_DEPR_CALC_STG_ARC.ORIG_COR_BEG_RESERVE is 'The original cor_beg_reserve from depr_ledger. Used because the retroactive rate change calculation can change the cor_beg_reserve column on this table.';
comment on column FCST_DEPR_CALC_STG_ARC.ORIG_DEPR_EXPENSE is 'The original depreciation expense from depr_ledger. Used because the retroactive rate change calculation can change the depreciation_expense column on this table.';
comment on column FCST_DEPR_CALC_STG_ARC.ORIG_DEPR_EXP_ALLOC_ADJUST is 'The original depr_exp_alloc_adjust from depr_ledger. Used because the retroactive rate change calculation can change the depr_exp_alloc_adjust column on this table.';
comment on column FCST_DEPR_CALC_STG_ARC.ORIG_SALV_EXPENSE is 'The original salvage expense from depr_ledger. Used because the retroactive rate change calculation can change the salvage_expense column on this table.';
comment on column FCST_DEPR_CALC_STG_ARC.ORIG_SALV_EXP_ALLOC_ADJUST is 'The original salv_exp_alloc_adj from depr_ledger. Used because the retroactive rate change calculation can change the salv_exp_alloc_adjust column on this table.';
comment on column FCST_DEPR_CALC_STG_ARC.ORIG_COR_EXPENSE is 'The original cost of removal expense from depr_ledger. Used because the retroactive rate change calculation can change the cor_expense column on this table.';
comment on column FCST_DEPR_CALC_STG_ARC.ORIG_COR_EXP_ALLOC_ADJUST is 'The original cor_exp_alloc_adjust from depr_ledger. Used because the retroactive rate change calculation can change the cor_exp_alloc_adjust column on this table.';
comment on column FCST_DEPR_CALC_STG_ARC.RETRO_DEPR_ADJ is 'The adjustment to depreciation expense due to the retroactive rate change calculation. This value makes up part of the depr_exp_alloc_adjust column on depr_ledger.';
comment on column FCST_DEPR_CALC_STG_ARC.RETRO_COR_ADJ is 'The adjustment to cost of removal expense due to the retroactive rate change calculation. This value makes up part of the cor_exp_alloc_adjust column on depr_ledger.';
comment on column FCST_DEPR_CALC_STG_ARC.RETRO_SALV_ADJ is 'The adjustment to salvage expene due to the retroactive rate change calculation. This value makes up part of the salv_exp_alloc_adjust column on depr_ledger.';
comment on column FCST_DEPR_CALC_STG_ARC.IMPAIRMENT_ASSET_ACTIVITY_SALV is 'This holds any activity against the impairment bucket.  It is used for correctly processing salvage amounts.  It is populated by post.';
comment on column FCST_DEPR_CALC_STG_ARC.IMPAIRMENT_ASSET_BEGIN_BALANCE is 'This holds the beginning balance for the impairment bucket.  It is used for correctly processing salvage amounts.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (767, 0, 10, 4, 2, 0, 33598, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033598_depr_fcst_columns.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;