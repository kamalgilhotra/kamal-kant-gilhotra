/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044500_ctrl_woest_filter_al_by_ml_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 2015.2   07/23/2015  Andrew Hill     Adding system control referenced in ticket
||============================================================================
*/
INSERT INTO pp_system_control_company(control_id, control_name, control_value, description,
                                        long_description, company_id)
SELECT (SELECT Max(control_id) + 1 from pp_system_control_company), 'WOEST - Filter AL by ML on open', 'YES', 'Filter AL by ML',
          'Filters Asset_Location dropdown on the Unit Estimates by Major_Location', -1
FROM dual;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2710, 0, 2015, 2, 0, 0, 044500, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044500_sys_ctrl_woest_filter_al_by_ml_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;