/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_044149_pwrtax_summ_257_report_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/23/2015 Anand R        new summarized report 257
||============================================================================
*/

insert into pp_reports (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW, TURN_OFF_MULTI_THREAD)
values (402026, 'FAS109 by type - Summary', 'This report must be run for large companies where report 257 fails or takes too long ', 'PowerTax', 'dw_tax_rpt_dfit_sum_fas109_type', 'dfit', 'PowerTax_Rollup', null, 'PwrTax - 258', null, 'DETAIL', null, 1, 129, 52, 81, 1, 3, null, null, null, null, null, 0, null);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2644, 0, 2015, 2, 0, 0, 044149, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044149_pwrtax_summ_257_report_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;