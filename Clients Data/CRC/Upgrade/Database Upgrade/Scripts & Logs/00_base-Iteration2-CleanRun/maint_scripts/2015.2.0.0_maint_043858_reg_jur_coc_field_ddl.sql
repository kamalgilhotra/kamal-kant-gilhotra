/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043858_reg_jur_coc_field_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 05/19/2015 Sarah Byers    Add theo cap struct field to reg_jur_template
||============================================================================
*/

alter table reg_jur_template add theoretical_cap_struct number(22,0);
COMMENT ON COLUMN reg_jur_template.theoretical_cap_struct is 'Specifies whether the template uses theoretical capital structure.  1 = yes.  0 = no.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2563, 0, 2015, 2, 0, 0, 043858, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043858_reg_jur_coc_field_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;