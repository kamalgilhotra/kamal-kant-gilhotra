/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029215_05_GET_CPR_SOB_RESERVE.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.0.0   02/05/2013 Charlie Shilling Point Release
||============================================================================
*/

create or replace function GET_CPR_SOB_RESERVE(P_ASSET_ID        number,
                                               P_SET_OF_BOOKS_ID number,
                                               P_FACTOR          number,
                                               P_FOR_MONTH       date) return number is
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: GET_CPR_SOB_RESERVE
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/05/2013 Charlie Shilling
   ||============================================================================
   */

   V_RTN                number(22, 2);
   V_FACTOR             number(22, 8);
   V_SUBLEDGER_IND      number(22, 0);
   V_FERC_ACTIVITY_CODE number(22, 0);
   V_RESERVE_AMT        number(22, 2);

begin

   select count(1) into V_RTN from CPR_LEDGER where ASSET_ID = P_ASSET_ID;

   if V_RTN < 1 then
      RAISE_APPLICATION_ERROR(-10801,
                              'No Asset Found in Get CPR SOB Resrve Function (asset_id: ' ||
                              P_ASSET_ID || ')');
   end if;

   select NVL(SUBLEDGER_INDICATOR, 0)
     into V_SUBLEDGER_IND
     from CPR_LEDGER
    where ASSET_ID = P_ASSET_ID;

   if V_SUBLEDGER_IND = 0 then
      --If group depreciation then get the allocation factors * sob amount
      select count(1)
        into V_RTN
        from CPR_LEDGER A, DEPR_RES_ALLO_FACTORS B
       where A.DEPR_GROUP_ID = B.DEPR_GROUP_ID
         and TO_NUMBER(TO_CHAR(A.ENG_IN_SERVICE_YEAR, 'YYYY')) = B.VINTAGE
         and A.ASSET_ID = P_ASSET_ID
         and B.SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
         and B.MONTH = P_FOR_MONTH;
      if V_RTN = 1 then
         select ROUND(B.FACTOR * GET_CPR_SOB_AMT(P_ASSET_ID, P_SET_OF_BOOKS_ID, P_FACTOR), 2)
           into V_RESERVE_AMT
           from CPR_LEDGER A, DEPR_RES_ALLO_FACTORS B
          where A.DEPR_GROUP_ID = B.DEPR_GROUP_ID
            and TO_NUMBER(TO_CHAR(A.ENG_IN_SERVICE_YEAR, 'YYYY')) = B.VINTAGE
            and A.ASSET_ID = P_ASSET_ID
            and B.SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
            and B.MONTH = P_FOR_MONTH;
      else
         RAISE_APPLICATION_ERROR(-10802,
                                 'Depr Res Allo Factors not found for asset (asset_id: ' ||
                                 P_ASSET_ID || ')');
      end if;
   else
      --Individually depreciated get from cpr depr by pct.
      select count(1)
        into V_RTN
        from CPR_DEPR
       where ASSET_ID = P_ASSET_ID
         and SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
         and GL_POSTING_MO_YR = P_FOR_MONTH;
      if V_RTN = 1 then
         select ROUND(BEG_RESERVE_MONTH * P_FACTOR, 2)
           into V_RESERVE_AMT
           from CPR_DEPR
          where ASSET_ID = P_ASSET_ID
            and SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
            and GL_POSTING_MO_YR = P_FOR_MONTH;
      else
         RAISE_APPLICATION_ERROR(-10803,
                                 'No CPR Depr Found in Get CPR SOB Resrve Function (asset_id: ' ||
                                 P_ASSET_ID || ', set of book: ' || P_SET_OF_BOOKS_ID || ')');
      end if;
   end if;

   return V_RESERVE_AMT;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (287, 0, 10, 4, 0, 0, 29215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029215_05_GET_CPR_SOB_RESERVE.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;