 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045979_budgetom_driver_enhancement_1_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || ----------- ---------- -------------- -------------------------------------
 || 2016.1.0.0  08/17/2016 Anand R       PP-45979. Budget O&M driver enhancements
 ||============================================================================
 */ 

--create cr_budget_labor_templates and default row
create table CR_BUDGET_LABOR_TEMPLATES (
    LABOR_TEMPLATE_ID number(22,0),
    DESCRIPTION varchar2(255),
    USER_ID varchar2(18),
    TIME_STAMP date);

alter table CR_BUDGET_LABOR_TEMPLATES
add constraint CR_BUDGET_LABOR_TEMPLATES_PK primary key (LABOR_TEMPLATE_ID)
using index tablespace PWRPLANT_IDX;

comment on table  CR_BUDGET_LABOR_TEMPLATES is '(S) [02] A table to store budget labor template information';
comment on column CR_BUDGET_LABOR_TEMPLATES.LABOR_TEMPLATE_ID is 'The system assigned identifier for a labor template';
comment on column CR_BUDGET_LABOR_TEMPLATES.DESCRIPTION is 'User defined description';
comment on column CR_BUDGET_LABOR_TEMPLATES.USER_ID IS 'Standard system-assigned user id used for audit purposes.';
comment on column CR_BUDGET_LABOR_TEMPLATES.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.';

create table CR_BUDGET_LABOR_DRIVERS (
    TAB_INDICATOR number(22,0),
    DESCRIPTION varchar2(255),
    USER_ID varchar2(18),
    TIME_STAMP date);

alter table CR_BUDGET_LABOR_DRIVERS
add constraint CR_BUDGET_LABOR_DRIVERS_PK primary key (TAB_INDICATOR)
using index tablespace PWRPLANT_IDX;

comment on table  CR_BUDGET_LABOR_DRIVERS is '(S) [02] A table to store budget labor driver information';
comment on column CR_BUDGET_LABOR_DRIVERS.TAB_INDICATOR is 'Tab indicator for the driver';
comment on column CR_BUDGET_LABOR_DRIVERS.DESCRIPTION is 'User defined description';
comment on column CR_BUDGET_LABOR_DRIVERS.USER_ID IS 'Standard system-assigned user id used for audit purposes.';
comment on column CR_BUDGET_LABOR_DRIVERS.TIME_STAMP IS 'Standard system-assigned timestamp used for audit purposes.';

--modify cr_budget_data_labor2_define and default rows
alter table CR_BUDGET_DATA_LABOR2_DEFINE add LABOR_TEMPLATE_ID number(22,0);
alter table CR_BUDGET_DATA_LABOR2_DEFINE add TAB_INDICATOR number(22,0);

comment on column CR_BUDGET_DATA_LABOR2_DEFINE.LABOR_TEMPLATE_ID is 'The system assigned identifier for a labor template';
comment on column CR_BUDGET_DATA_LABOR2_DEFINE.TAB_INDICATOR is 'Tab indicator for the driver';
  

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3265, 0, 2016, 1, 0, 0, 045979, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045979_budgetom_driver_enhancement_1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;