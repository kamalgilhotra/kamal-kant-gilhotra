/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045415_jobserver_update_queue_keys_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 05/23/2016 Charlie Shilling change security keys for queue page in integration manager
||============================================================================
*/
UPDATE pp_web_security_perm_control
SET objects = 'JobService.Queue.Killqueuedjob.BudgetProcesses'
WHERE objects = 'IntegrationManager.Queue.KillBudgetProcesses';

UPDATE pp_web_security_permission
SET objects = 'JobService.Queue.Killqueuedjob.BudgetProcesses'
WHERE objects = 'IntegrationManager.Queue.KillBudgetProcesses';

UPDATE pp_web_security_perm_control
SET objects = 'JobService.Queue.Killqueuedjob.CRSystemProcesses'
WHERE objects = 'IntegrationManager.Queue.KillCRSystemProcesses';

UPDATE pp_web_security_permission
SET objects = 'JobService.Queue.Killqueuedjob.CRSystemProcesses'
WHERE objects = 'IntegrationManager.Queue.KillCRSystemProcesses';

UPDATE pp_web_security_perm_control
SET objects = 'JobService.Queue.Killqueuedjob.CustomProcesses'
WHERE objects = 'IntegrationManager.Queue.KillCustomProcesses';

UPDATE pp_web_security_permission
SET objects = 'JobService.Queue.Killqueuedjob.CustomProcesses'
WHERE objects = 'IntegrationManager.Queue.KillCustomProcesses';

UPDATE pp_web_security_perm_control
SET objects = 'JobService.Queue.Killqueuedjob.Input'
WHERE objects = 'IntegrationManager.Queue.KillInputJobs';

UPDATE pp_web_security_permission
SET objects = 'JobService.Queue.Killqueuedjob.Input'
WHERE objects = 'IntegrationManager.Queue.KillInputJobs';

UPDATE pp_web_security_perm_control
SET objects = 'JobService.Queue.Killqueuedjob.Link'
WHERE objects = 'IntegrationManager.Queue.KillJobFlowJobs';

UPDATE pp_web_security_permission
SET objects = 'JobService.Queue.Killqueuedjob.Link'
WHERE objects = 'IntegrationManager.Queue.KillJobFlowJobs';

UPDATE pp_web_security_perm_control
SET objects = 'JobService.Queue.Killqueuedjob.MonthEndProcesses'
WHERE objects = 'IntegrationManager.Queue.KillMonthEndProcesses';

UPDATE pp_web_security_permission
SET objects = 'JobService.Queue.Killqueuedjob.MonthEndProcesses'
WHERE objects = 'IntegrationManager.Queue.KillMonthEndProcesses';

UPDATE pp_web_security_perm_control
SET objects = 'JobService.Queue.Killqueuedjob.Notification'
WHERE objects = 'IntegrationManager.Queue.KillNotification';

UPDATE pp_web_security_permission
SET objects = 'JobService.Queue.Killqueuedjob.Notification'
WHERE objects = 'IntegrationManager.Queue.KillNotification';

UPDATE pp_web_security_perm_control
SET objects = 'JobService.Queue.Killqueuedjob.SQL'
WHERE objects = 'IntegrationManager.Queue.KillSQLJobs';

UPDATE pp_web_security_permission
SET objects = 'JobService.Queue.Killqueuedjob.SQL'
WHERE objects = 'IntegrationManager.Queue.KillSQLJobs';

UPDATE pp_web_security_perm_control
SET objects = 'JobService.Queue.Changequeuedjobpriority.All'
WHERE objects = 'IntegrationManager.Queue.MoveInQueue';

UPDATE pp_web_security_permission
SET objects = 'JobService.Queue.Changequeuedjobpriority.All'
WHERE objects = 'IntegrationManager.Queue.MoveInQueue';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3197, 0, 2016, 1, 0, 0, 045415, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045415_jobserver_update_queue_keys_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;