/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008466_tax_test_cpr_activity.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   03/27/2012 Aaron Smith    Point Release
||============================================================================
*/
-- Add/modify columns
alter table TAX_TEST_CPR_ACTIVITY add TAX_DISPOSITION_CODE number(22,0);
alter table TAX_TEST_CPR_ACTIVITY add TAX_ORIG_MONTH_NUMBER number(22,0);
alter table TAX_TEST_CPR_ACTIVITY add ORIG_ASSET_ACTIVITY_ID number(22,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (111, 0, 10, 3, 4, 0, 8466, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_008466_tax_test_cpr_activity.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

