/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045079_budget_capital_version_security_read_only_1_ddl.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2.0 10/13/2015 Chris Mardis   Add Read Only option to Capital budget version security
 ||============================================================================
 */

alter table budget_version_security add (
   read_only number(22) default 0
   );

comment on column budget_version_security.read_only is 'Flag indicating whether a budget version is "Read Only" for a given user or if they have Full Rights. 1=Read Only 0=Full Rights';

CREATE OR REPLACE VIEW BUDGET_VERSION AS
SELECT
   bvc.BUDGET_VERSION_ID,
   bvc.TIME_STAMP,
   bvc.USER_ID,
   bvc.DESCRIPTION,
   bvc.LONG_DESCRIPTION,
   bvc.LOCKED,
   bvc.START_YEAR,
   bvc.CURRENT_YEAR,
   bvc.END_YEAR,
   bvc.ACTUALS_MONTH,
   bvc.DATE_FINALIZED,
   bvc.CURRENT_VERSION,
   nvl((select case when read_only = 1 then 0 else null end from budget_version_security a where a.budget_version_id = bvc.budget_version_id and lower(a.users) = lower(user)),bvc.OPEN_FOR_ENTRY) open_for_entry,
   bvc.BUDGET_ONLY,
   bvc.EXTERNAL_BUDGET_VERSION,
   bvc.BRING_IN_SUBS,
   bvc.OUTLOOK_NUMBER,
   bvc.LEVEL_ID,
   bvc.REPORTING_CURRENCY_ID,
   bvc.BUDGET_VERSION_TYPE_ID,
   bvc.COMPANY_ID,
   bvc.PROCESS_LEVEL
FROM BUDGET_VERSION_CONTROL bvc
where (
   bvc.budget_version_id in (
      select bvs.budget_version_id
      from budget_version_security bvs
      where lower(bvs.users) = lower(USER)
      )
   OR
   bvc.budget_version_id in (
      select bvsa.budget_version_id
      from budget_version_security_all bvsa
      )
   );

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2915, 0, 2015, 2, 0, 0, 45079, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045079_budget_capital_version_security_read_only_1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;