 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048762_02_lease_ilr_options_dml.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 09/18/2017 Shane "C" Ward   New Lessor ILR Options and new Fields 
 ||											to Lessee ILR Options
 ||============================================================================
 */

INSERT INTO lsr_ilr_rate_types (rate_type_id, description) VALUES (1, 'Sales Type Discount Rate');
INSERT INTO lsr_ilr_rate_types (rate_type_id, description) VALUES (2, 'Direct Finance Discount Rate');
INSERT INTO lsr_ilr_rate_types (rate_type_id, description) values (3, 'Direct Finance FMV Comparison Rate');
INSERT INTO lsr_ilr_rate_types (rate_type_id, description) VALUES (4, 'Direct Finance Interest on Net Inv Rate');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3715, 0, 2017, 1, 0, 0, 48762, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048762_02_lease_ilr_options_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
