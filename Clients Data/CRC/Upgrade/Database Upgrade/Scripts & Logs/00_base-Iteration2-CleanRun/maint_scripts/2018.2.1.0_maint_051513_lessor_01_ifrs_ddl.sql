/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_051513_lessor_01_ifrs_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.1.0 04/08/2019 B. Beck    	Add a flag to the FASB set of books table for IFRS processing
||============================================================================
*/

alter table lsr_fasb_type_sob add use_orig_rate number(1,0) default 0;

comment on column lsr_fasb_type_sob.use_orig_rate is 'A A flag to notify the schedule calculation to use the original rate implicit and not recalculate it for a remeasurement.  This is for IFRS accounting.';


CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_INFO FORCE AUTHID CURRENT_USER AS OBJECT
(
	carrying_cost NUMBER,
	carrying_cost_company_curr NUMBER,
	fair_market_value NUMBER,
	fair_market_value_company_curr NUMBER,
	guaranteed_residual NUMBER,
	estimated_residual NUMBER,
	days_in_year NUMBER,
	purchase_option_amount NUMBER,
	termination_amount NUMBER,
	remeasurement_date DATE,
	investment_amount NUMBER,
	original_profit_loss NUMBER,
	new_beg_receivable NUMBER,
	prior_end_unguaran_residual NUMBER,
	remeasure_month_fixed_payment NUMBER,
	include_idc_sw NUMBER,
	use_orig_rate NUMBER,
	accrued_deferred_rent NUMBER,
	unamortized_idc NUMBER,
	npv LSR_NPV_VALUES,
	selling_profit_loss NUMBER,
	set_of_books_id NUMBER
);
/

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_INFO_TAB as table of lsr_ilr_sales_sch_info;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (17042, 0, 2018, 2, 1, 0, 51513, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.1.0_maint_051513_lessor_01_ifrs_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
