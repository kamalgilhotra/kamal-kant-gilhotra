/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042879_pcm_program_link_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/12/2015 Sarah Byers      Allow for opening non-response/child windows
||============================================================================
*/

alter table ppbase_workspace_links add (linked_window_opensheet number(1,0));
comment on column ppbase_workspace_links.linked_window_opensheet is '1: Open linked window with opensheet function; 0: Open linked window with open function';





--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2288, 0, 2015, 1, 0, 0, 042879, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042879_pcm_program_link_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;