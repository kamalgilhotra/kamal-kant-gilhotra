SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030778_proptax_01_check_module.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/27/2013 Lee Quinn      Point release
||============================================================================
*/

declare
   TABLE_NOT_EXISTS exception;
   pragma exception_init(TABLE_NOT_EXISTS, -00942);

   V_COUNT number;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   execute immediate 'select count(*) from PROPERTY_TAX_YEAR'
      into V_COUNT;

   if V_COUNT > 0 then
      DBMS_OUTPUT.PUT_LINE('Property Tax is being used.');
      -- Inserts into PP_SCHEMA_CHANGE LOG to skip the
      -- PropertyTax Rebuild update scripts
      insert into PP_SCHEMA_CHANGE_LOG
         (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM,
          SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
      values
         (538, 1, 10, 4, 1, 0, 30778, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030778_proptax_02_rebuild_objects.sql', 1, SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
      insert into PP_SCHEMA_CHANGE_LOG
         (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM,
          SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
      values
         (539, 1, 10, 4, 1, 0, 30778, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030778_proptax_03_rebuild_data.sql', 1, SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
      insert into PP_SCHEMA_CHANGE_LOG
         (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM,
          SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
      values
         (540, 1, 10, 4, 1, 0, 30778, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030778_proptax_04_rebuild_comp_view.sql', 1, SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
      insert into PP_SCHEMA_CHANGE_LOG
         (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM,
          SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
      values
         (541, 1, 10, 4, 1, 0, 30778, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030778_proptax_05_rebuild_plant_data.sql', 1, SYSTIMESTAMP,
          SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
          SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

   else
      DBMS_OUTPUT.PUT_LINE('The Property Tax module is NOT being used.');
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (537, 0, 10, 4, 1, 0, 30778, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030778_proptax_01_check_module.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
