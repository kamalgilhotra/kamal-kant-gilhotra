/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049654_lessor_01_create_lessor_pp_processes_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/01/2017 Charlie Shilling Create pp_processes records for Lessor MEC processes
||============================================================================
*/
DECLARE
	max_process_id 		pp_processes.process_id%TYPE;
BEGIN
	--Get the maximum current pp_process_id
	SELECT Nvl(Max(process_id), 0)
	INTO max_process_id
	FROM pp_processes;

	--Accrual Calculation
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent)
	VALUES (max_process_id + 1, 'Lessor - Calculate Accruals', 'Lessor - Calculate Accruals', 'ssp_lease_control.exe LSR_CALC_ACCRUAL', '2017.1.0.0', 1);

	--Accrual Approval
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent)
	VALUES (max_process_id + 2, 'Lessor - Approve Accruals', 'Lessor - Approve Accruals', 'ssp_lease_control.exe LSR_APPR_ACCRUAL', '2017.1.0.0', 1);

	--Invoice Calculation
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent)
	VALUES (max_process_id + 3, 'Lessor - Calculate Invoices', 'Lessor - Calculate Invoices', 'ssp_lease_control.exe LSR_CALC_INVOICE', '2017.1.0.0', 1);

	--Invoice Approval
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent)
	VALUES (max_process_id + 4, 'Lessor - Approve Invoices', 'Lessor - Approve Invoices', 'ssp_lease_control.exe LSR_APPR_INVOICE', '2017.1.0.0', 1);

	--Currency Gain/Loss Approval
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent)
	VALUES (max_process_id + 5, 'Lessor - Approve Currency Gain/Loss', 'Lessor - Approve Currency Gain/Loss', 'ssp_lease_control.exe LSR_APPR_CUR_GL', '2017.1.0.0', 1);

	--Open Month
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent)
	VALUES (max_process_id + 6, 'Lessor - Open Month', 'Lessor - Open Month', 'ssp_lease_control.exe LSR_OPEN_MONTH', '2017.1.0.0', 1);

	--Close Month
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent)
	VALUES (max_process_id + 7, 'Lessor - Close Month', 'Lessor - Close Month', 'ssp_lease_control.exe LSR_CLOSE_MONTH', '2017.1.0.0', 1);

	--Lock Exchange Rate
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent)
	VALUES (max_process_id + 8, 'Lessor - Lock Exchange Rate', 'Lessor - Lock Exchange Rate', 'ssp_lease_control.exe LSR_LOCK_RATE', '2017.1.0.0', 1);

	--Auto-Termination Calculation
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent)
	VALUES (max_process_id + 9, 'Lessor - Calculate Auto Termination', 'Lessor - Calculate Auto Termination', 'ssp_lease_control.exe LSR_CALC_AUTO_TERMINATION', '2017.1.0.0', 1);

	--Auto-Termination Approval
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent)
	VALUES (max_process_id + 10, 'Lessor - Approve Auto Termination', 'Lessor - Approve Auto Termination', 'ssp_lease_control.exe LSR_APPR_AUTO_TERMINATION', '2017.1.0.0', 1);
END;
/
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3894, 0, 2017, 1, 0, 0, 49654, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049654_lessor_01_create_lessor_pp_processes_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;