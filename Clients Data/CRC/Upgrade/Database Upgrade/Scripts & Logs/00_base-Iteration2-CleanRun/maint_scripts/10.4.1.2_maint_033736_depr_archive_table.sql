/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033736_depr_archive_table.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.2   11/05/2013 Kyle Peterson
||============================================================================
*/

CREATE TABLE DEPR_CALC_STG_ARC
(
 DEPR_GROUP_ID                number(22,0),
 SET_OF_BOOKS_ID              number(22,0),
 GL_POST_MO_YR                date,
 CALC_MONTH                   date,
 DEPR_CALC_STATUS             number(2,0),
 DEPR_CALC_MESSAGE            varchar2(2000),
 TRF_IN_EST_ADDS              varchar2(35),
 INCLUDE_RWIP_IN_NET          varchar2(35),
 DNSA_COR_BAL                 number(22,2),
 DNSA_SALV_BAL                number(22,2),
 COR_YTD                      number(22,2),
 SALV_YTD                     number(22,2),
 FISCALYEAROFFSET             number(2,0),
 FISCALYEARSTART              date,
 COMPANY_ID                   number(22,0),
 SUBLEDGER_TYPE_ID            number(22,0),
 DESCRIPTION                  varchar2(254),
 DEPR_METHOD_ID               number(22,0),
 MID_PERIOD_CONV              number(22,2),
 MID_PERIOD_METHOD            varchar2(35),
 DG_EST_ANN_NET_ADDS          number(22,8),
 TRUE_UP_CPR_DEPR             number(22,0),
 RATE                         number(22,8),
 NET_GROSS                    number(22,0),
 OVER_DEPR_CHECK              number(22,0),
 NET_SALVAGE_PCT              number(22,8),
 RATE_USED_CODE               number(22,0),
 END_OF_LIFE                  number(22,0),
 EXPECTED_AVERAGE_LIFE        number(22,0),
 RESERVE_RATIO_ID             number(22,0),
 DMR_COST_OF_REMOVAL_RATE     number(22,8),
 COST_OF_REMOVAL_PCT          number(22,8),
 DMR_SALVAGE_RATE             number(22,8),
 INTEREST_RATE                number(22,8),
 AMORTIZABLE_LIFE             number(22,0),
 COR_TREATMENT                number(1,0),
 SALVAGE_TREATMENT            number(1,0),
 NET_SALVAGE_AMORT_LIFE       number(22,0),
 ALLOCATION_PROCEDURE         varchar2(5),
 DEPR_LEDGER_STATUS           number(22,0),
 BEGIN_RESERVE                number(22,2),
 END_RESERVE                  number(22,2),
 RESERVE_BAL_PROVISION        number(22,2),
 RESERVE_BAL_COR              number(22,2),
 SALVAGE_BALANCE              number(22,2),
 RESERVE_BAL_ADJUST           number(22,2),
 RESERVE_BAL_RETIREMENTS      number(22,2),
 RESERVE_BAL_TRAN_IN          number(22,2),
 RESERVE_BAL_TRAN_OUT         number(22,2),
 RESERVE_BAL_OTHER_CREDITS    number(22,2),
 RESERVE_BAL_GAIN_LOSS        number(22,2),
 RESERVE_ALLOC_FACTOR         number(22,2),
 BEGIN_BALANCE                number(22,2),
 ADDITIONS                    number(22,2),
 RETIREMENTS                  number(22,2),
 TRANSFERS_IN                 number(22,2),
 TRANSFERS_OUT                number(22,2),
 ADJUSTMENTS                  number(22,2),
 DEPRECIATION_BASE            number(22,2),
 END_BALANCE                  number(22,2),
 DEPRECIATION_RATE            number(22,8),
 DEPRECIATION_EXPENSE         number(22,2),
 DEPR_EXP_ADJUST              number(22,2),
 DEPR_EXP_ALLOC_ADJUST        number(22,2),
 COST_OF_REMOVAL              number(22,2),
 RESERVE_RETIREMENTS          number(22,2),
 SALVAGE_RETURNS              number(22,2),
 SALVAGE_CASH                 number(22,2),
 RESERVE_CREDITS              number(22,2),
 RESERVE_ADJUSTMENTS          number(22,2),
 RESERVE_TRAN_IN              number(22,2),
 RESERVE_TRAN_OUT             number(22,2),
 GAIN_LOSS                    number(22,2),
 VINTAGE_NET_SALVAGE_AMORT    number(22,2),
 VINTAGE_NET_SALVAGE_RESERVE  number(22,2),
 CURRENT_NET_SALVAGE_AMORT    number(22,2),
 CURRENT_NET_SALVAGE_RESERVE  number(22,2),
 IMPAIRMENT_RESERVE_BEG       number(22,2),
 IMPAIRMENT_RESERVE_ACT       number(22,2),
 IMPAIRMENT_RESERVE_END       number(22,2),
 EST_ANN_NET_ADDS             number(22,2),
 RWIP_ALLOCATION              number(22,2),
 COR_BEG_RESERVE              number(22,2),
 COR_EXPENSE                  number(22,2),
 COR_EXP_ADJUST               number(22,2),
 COR_EXP_ALLOC_ADJUST         number(22,2),
 COR_RES_TRAN_IN              number(22,2),
 COR_RES_TRAN_OUT             number(22,2),
 COR_RES_ADJUST               number(22,2),
 COR_END_RESERVE              number(22,2),
 COST_OF_REMOVAL_RATE         number(22,8),
 COST_OF_REMOVAL_BASE         number(22,2),
 RWIP_COST_OF_REMOVAL         number(22,2),
 RWIP_SALVAGE_CASH            number(22,2),
 RWIP_SALVAGE_RETURNS         number(22,2),
 RWIP_RESERVE_CREDITS         number(22,2),
 SALVAGE_RATE                 number(22,8),
 SALVAGE_BASE                 number(22,2),
 SALVAGE_EXPENSE              number(22,2),
 SALVAGE_EXP_ADJUST           number(22,2),
 SALVAGE_EXP_ALLOC_ADJUST     number(22,2),
 RESERVE_BAL_SALVAGE_EXP      number(22,2),
 RESERVE_BLENDING_ADJUSTMENT  number(22,2),
 RESERVE_BLENDING_TRANSFER    number(22,2),
 COR_BLENDING_ADJUSTMENT      number(22,2),
 COR_BLENDING_TRANSFER        number(22,2),
 IMPAIRMENT_ASSET_AMOUNT      number(22,2),
 IMPAIRMENT_EXPENSE_AMOUNT    number(22,2),
 RESERVE_BAL_IMPAIRMENT       number(22,2),
 BEGIN_BALANCE_YEAR           number(22,2),
 BEGIN_RESERVE_YEAR           number(22,2),
 BEGIN_BALANCE_QTR            number(22,2),
 BEGIN_RESERVE_QTR            number(22,2),
 PLANT_ACTIVITY_2             number(22,2),
 RESERVE_ACTIVITY_2           number(22,2),
 EST_NET_ADDS                 number(22,2),
 CUMULATIVE_TRANSFERS         number(22,2),
 RESERVE_DIFF                 number(22,2),
 FISCAL_MONTH                 number(2,0),
 FISCALQTRSTART               date,
 TYPE_2_EXIST                 number(1,0),
 CUM_UOP_DEPR                 number(22,2),
 CUM_UOP_DEPR_2               number(22,2),
 PRODUCTION                   number(22,2),
 ESTIMATED_PRODUCTION         number(22,2),
 PRODUCTION_2                 number(22,2),
 ESTIMATED_PRODUCTION_2       number(22,2),
 SMOOTH_CURVE                 varchar2(5),
 MIN_CALC                     number(22,2),
 MIN_UOP_EXP                  number(22,2),
 YTD_UOP_DEPR                 number(22,2),
 YTD_UOP_DEPR_2               number(22,2),
 CURR_UOP_EXP                 number(22,2),
 CURR_UOP_EXP_2               number(22,2),
 LESS_YEAR_UOP_DEPR           number(22,2),
 DEPRECIATION_UOP_RATE        number(22,8),
 DEPRECIATION_UOP_RATE_2      number(22,8),
 DEPRECIATION_BASE_UOP        number(22,2),
 DEPRECIATION_BASE_UOP_2      number(22,2),
 COR_ACTIVITY_2               number(22,2),
 OVER_DEPR_ADJ                number(22,2),
 OVER_DEPR_ADJ_COR            number(22,2),
 OVER_DEPR_ADJ_SALV           number(22,2),
 EFFECTIVE_BEGIN_RESERVE      number(22,2),
 EFFECTIVE_BEGIN_RESERVE_QTR  number(22,2),
 EFFECTIVE_BEGIN_RESERVE_YEAR number(22,2),
 EFFECTIVE_END_RESERVE        number(22,2),
 EFFECTIVE_COR_BEG_RESERVE    number(22,2),
 EFFECTIVE_COR_END_RESERVE    number(22,2),
 EFFECTIVE_DEPR_BASE          number(22,2),
 EFFECTIVE_SALVAGE_BASE       number(22,2),
 EFFECTIVE_COR_BASE           number(22,2),
 RETRO_DEPR_EXPENSE           number(22,2),
 RETRO_COR_EXPENSE            number(22,2),
 RETRO_SALV_EXPENSE           number(22,2),
 ORIG_DEPR_EXPENSE            number(22,2),
 ORIG_DEPR_EXP_ALLOC_ADJUST   number(22,2),
 ORIG_SALV_EXPENSE            number(22,2),
 ORIG_SALV_EXP_ALLOC_ADJUST   number(22,2),
 ORIG_COR_EXP_ALLOC_ADJUST    number(22,2),
 ORIG_COR_EXPENSE             number(22,2),
 RETRO_DEPR_ADJ               number(22,2),
 RETRO_COR_ADJ                number(22,2),
 RETRO_SALV_ADJ               number(22,2)
);

COMMENT ON TABLE DEPR_CALC_STG_ARC IS '(S)  [01]
An archive table holding the results from the previous group depreciation calculation.';

COMMENT ON COLUMN DEPR_CALC_STG_ARC.depr_group_id IS 'Internal ID linking to the depreciation group';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.set_of_books_id IS 'The set of books';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.gl_post_mo_yr IS 'The accounting month';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.calc_month IS 'The month being calculated';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depr_calc_status IS 'Status column';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depr_calc_message IS 'An error mesaging';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.trf_in_est_adds IS 'Are transfers included int eh net adds';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.include_rwip_in_net IS 'Is rwip included in the net reserve';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.dnsa_cor_bal IS 'Net Salvage Amortization Balance for Cost of Removal';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.dnsa_salv_bal IS 'Net Salvage Amortization Balance for Salvage';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_ytd IS 'The year to date cost of removal';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.salv_ytd IS 'The year to date salvage';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.fiscalyearoffset IS 'Fiscal year offset';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.fiscalyearstart IS 'The start of the fiscal year';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.company_id IS 'Internal ID linking to company';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.subledger_type_id IS 'The subledger indicator';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.description IS 'The description';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depr_method_id IS 'Internal ID linking to the depreciation method';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.mid_period_conv IS 'The percent of current activity to include in depreciation base';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.mid_period_method IS 'The mid period method';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.dg_est_ann_net_adds IS 'The estimated net additions and adjustment';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.true_up_cpr_depr IS 'Trueup individually depreciated assets';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.rate IS 'The rate';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.net_gross IS '1 means calculate depreciation based on the net balance.';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.over_depr_check IS 'Perform overdepreciation checks';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.net_salvage_pct IS 'Percent of beginning balance that should be salvage';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.rate_used_code IS 'Has the rate been used in a calculation before';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.end_of_life IS 'End of life for end of life methods';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.expected_average_life IS 'The expected average life';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_ratio_id IS 'The reserve ration';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.dmr_cost_of_removal_rate IS 'The cost of removal rate';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cost_of_removal_pct IS 'The percentage of the plant balance that is estimated cost of removal';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.dmr_salvage_rate IS 'The salvage rate';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.interest_rate IS 'The interest rate';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.amortizable_life IS 'The amortizable life of the depreciation group';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_treatment IS 'How cost of removal should be treated';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.salvage_treatment IS 'How is salvage treated';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.net_salvage_amort_life IS 'Net Salvage Amortization life';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.allocation_procedure IS 'The depreciation allocation procedure';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depr_ledger_status IS 'The depr ledger status';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.begin_reserve IS 'The Beginning reserve for the period';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.end_reserve IS 'The end reserve for the period';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_bal_provision IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_bal_cor IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.salvage_balance IS 'The salvage balance';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_bal_adjust IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_bal_retirements IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_bal_tran_in IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_bal_tran_out IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_bal_other_credits IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_bal_gain_loss IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_alloc_factor IS 'The reserve allocation factor';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.begin_balance IS 'The beginning plant balance for the period';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.additions IS 'The plant addidtions';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.retirements IS 'The retirement amount';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.transfers_in IS 'Transfer In Amount';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.transfers_out IS 'Transfer Out Amount';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.adjustments IS 'Plant Adjustments';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depreciation_base IS 'The deprecation base';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.end_balance IS 'The end plant balance';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depreciation_rate IS 'The depreciation rate';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depreciation_expense IS 'The current period calculated depreciation expense';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depr_exp_adjust IS 'Depreciation Expense Adjustment';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depr_exp_alloc_adjust IS 'Depreciation Expense Adjustment';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cost_of_removal IS 'Cost of Removal for the period';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_retirements IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.salvage_returns IS 'Salvage returns';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.salvage_cash IS 'Salvage Cash';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_credits IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_adjustments IS 'Reserve adjustment amount';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_tran_in IS 'Reserve Transfer Into this group';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_tran_out IS 'Reserve Transfer out of this group';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.gain_loss IS 'Gain loss';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.vintage_net_salvage_amort IS 'The vintaged net salvage amortization';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.vintage_net_salvage_reserve IS 'The vintaged net salvage reserve';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.current_net_salvage_amort IS 'Current Net Salvage Amortization';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.current_net_salvage_reserve IS 'Current Net Salvage Reserve';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.impairment_reserve_beg IS 'The beginning balance for the impairment basis bucket';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.impairment_reserve_act IS 'The activity for the impairment basis bucket';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.impairment_reserve_end IS 'The ending reserve for the impairment';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.est_ann_net_adds IS 'Estimate adds and adjustments';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.rwip_allocation IS 'The amount of rwip allocation to this group';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_beg_reserve IS 'The COR beginning Reserve for the perios';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_expense IS 'Current month calculated cost of removal expense';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_exp_adjust IS 'Current month cost of removal expense adjustment';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_exp_alloc_adjust IS 'Current month cost of removal expense adjustment';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_res_tran_in IS 'Cost of removal transferred in';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_res_tran_out IS 'Cost of removal transferred out';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_res_adjust IS 'Cost of removal reserve adjustment';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_end_reserve IS 'Cost of removal ending reserve';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cost_of_removal_rate IS 'The cost of removal rate';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cost_of_removal_base IS 'The basis for calculating cost of removal expense';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.rwip_cost_of_removal IS 'The amount of RWIP Allocation that is cost of removal';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.rwip_salvage_cash IS 'The amount of RWIP Allocation that is salvage cash';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.rwip_salvage_returns IS 'The amount of RWIP Allocation that is salvage returns';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.rwip_reserve_credits IS 'The amount of RWIP Allocation that is salvage credits';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.salvage_rate IS 'The salvage rate';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.salvage_base IS 'The salvage base';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.salvage_expense IS 'The salvage expense';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.salvage_exp_adjust IS 'The salvage expense adjustment';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.salvage_exp_alloc_adjust IS 'The salvage expense adjustment';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_bal_salvage_exp IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_blending_adjustment IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_blending_transfer IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_blending_adjustment IS 'The COR adjustment due to blended set of books';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_blending_transfer IS 'The COR Transfer due to blended set of books';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.impairment_asset_amount IS 'The amount of the impairment';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.impairment_expense_amount IS 'The expense amount being impaired';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_bal_impairment IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.begin_balance_year IS 'The Beginning Plant Balance for the year';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.begin_reserve_year IS 'The beginning reserve balance for the year';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.begin_balance_qtr IS 'The beginning plant balance for the quarter';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.begin_reserve_qtr IS 'The beginning reserve balance for the quarter';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.plant_activity_2 IS 'Plant activity for the period';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_activity_2 IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.est_net_adds IS 'Estimated Adds';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cumulative_transfers IS 'Cumulative transfers';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.reserve_diff IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.fiscal_month IS 'The firscal moth';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.fiscalqtrstart IS 'The starting quarter for the month';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.type_2_exist IS 'Used for unit of production calcualations';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cum_uop_depr IS 'Unit of Production';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cum_uop_depr_2 IS 'Unit of Production 2';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.production IS 'Used for unit of production calcualations';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.estimated_production IS 'Estimated Production for unit of production';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.production_2 IS 'Used for unit of production calcualations';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.estimated_production_2 IS 'Estimated Production for unit of production approach 2';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.smooth_curve IS 'Should the trueup methodolgy use smoothing';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.min_calc IS 'Used for unit of production calcualations';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.min_uop_exp IS 'Used for unit of production calcualations';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.ytd_uop_depr IS 'Used for unit of production calculations';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.ytd_uop_depr_2 IS 'Used for unit of production calculations';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.curr_uop_exp IS 'Expense for unit of production 1';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.curr_uop_exp_2 IS 'Expense for unit of production 2';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.less_year_uop_depr IS 'Used for unit of production calcualations';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depreciation_uop_rate IS 'The depreciation rate for unit of production';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depreciation_uop_rate_2 IS 'The depreciation rate for unit of production approach 2';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depreciation_base_uop IS 'The depreciation base for unit of production';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.depreciation_base_uop_2 IS 'The depreciation base for unit of production approach 2';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.cor_activity_2 IS 'COR activity for the period';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.over_depr_adj IS 'The over depr adjustment';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.over_depr_adj_cor IS 'The over depr adjustment from COR';
COMMENT ON COLUMN DEPR_CALC_STG_ARC.over_depr_adj_salv IS 'The over depr adjustment from Salvage';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (714, 0, 10, 4, 1, 2, 33736, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_033736_depr_archive_table.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;