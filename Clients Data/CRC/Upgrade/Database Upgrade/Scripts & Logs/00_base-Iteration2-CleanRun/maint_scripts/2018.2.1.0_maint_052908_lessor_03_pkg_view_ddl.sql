/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_052908_lessor_03_pkg_view_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.1.0 03/25/2019 B. Beck    	Create pkg and view
||============================================================================
*/
create or replace PACKAGE pkg_lessor_schedule AS
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LESSOR_SCHEDULE
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version     Date       Revised By     Reason for Change
   || ----------  ---------- -------------- ----------------------------------------
   || 2017.1.0.0  09/12/2017 A. Hill        Original Version
   || 2017.1.0.0  10/05/2017 A. Hill        Add Sales Type Calculation Logic
   || 2017.4.0.1  08/03/2018 S. Byers       Changed f_get_payment_info_from_terms due to low-level Oracle Error at client
   || 2018.1.0.0  10/09/2018 D. Conway      Updates/corrections to DF and Sales Prepay schedule build (Rates implicit, NPV, and various others).
   || 2018.1.0.0  10/22/2018 Anand R        PP-50234 Add logic to include remeasurement for unguaranteed residual for assets.
   || 2018.1.0.0  10/17/2018 D. Conway      Removed extra fixed_payment substraction from penny_plug for DF and Sales Prepay schedule build,
   ||                                          since it now accrues more like arrears.
   || 2018.1.0.0  10/31/2018 Anand R        PP-52340 Add logic for remeasurement when FMV = CC
   || 2018.1.0.0  11/01/2018 D. Conway      Added lease cap type to selection of set_of_books in f_get_sales_type_info, for mixed-type schedule builds.
   || 2018.1.0.0  11/05/2018 Anand R        PP-52341 Add logic for remeasurement when FMV <> CC
   || 2018.1.0.0  11/09/2018 Anand R        PP-52599 Add remeasurement logic for long term receivable
   || 2018.1.0.0  11/16/2018 Anand R        PP-52621 Use new version of IRR calculation
   || 2018.1.0.0  12/7/2018  Alex H.        PP-52709 Adding truncates to remeasurement month comparisons in IRR calcs, sales info functions
   || 2018.1.0.0  02/20/2019 D. Conway      PP-53089 Added Override Rate Convention.
   || 2018.3.0.0  03/01/2019 B. Beck	    Reclassification of FASB types from Operating to Sale Type and Direct Finance.
   ||==================================================================================================================================================
   */
  G_PKG_VERSION varchar(35) := '2018.1.0.0';

  /*****************************************************************************
  * PROCEDURE: p_copy_prior_approved_schedule
  * PURPOSE: Copies the schedule for the approved/current revision on the ILR into a temp table,
  *          so it can be used during calculation of a remeasurement
  * PARAMETERS:
  *   a_ilrs: The ILR ID/Revisions for which to copy the schedule
  ******************************************************************************/
  PROCEDURE p_copy_prior_approved_schedule(a_ilrs t_lsr_ilr_id_revision_tab);

  /*****************************************************************************
  * PROCEDURE: p_process_ilr
  * PURPOSE: Processes the Lessor ILR Schedule for the given ILR/Revision
  * PARAMETERS:
  *   a_ilr_id: ilr_id of the ILR for which to process the schedule
  *   a_revision: Revision of the ILR for which to process the schedule
  ******************************************************************************/
  PROCEDURE p_process_ilr(a_ilr_id NUMBER,
                          a_revision NUMBER);

  /*****************************************************************************
  * PROCEDURE: p_process_ilrs
  * PURPOSE: Processes the Lessor ILR Schedule for the given ILR/Revisions
  * PARAMETERS:
  *   a_ilrs: The ILR ID/Revisions for which to process the schedule
  ******************************************************************************/
  PROCEDURE p_process_ilrs(a_ilrs t_lsr_ilr_id_revision_tab);

  /*****************************************************************************
  * Function: f_ilr_check_df_override
  * PURPOSE: Checks to see if the direct finance ilr has any new override rates since last schedule build
  * PARAMETERS:
  * a_ilr_id: The ILR ID for the ILR for which to retrieve information
  * a_revision: The revision of the ILR for which to retrieve information
  * RETURNS: NUMBER: 0 if none, 1 if implicit, 2 if implicit_ni, 3 if both implicit and implicit_ni
  ******************************************************************************/
  function f_ilr_check_df_override ( a_ilr_id NUMBER, a_revision NUMBER) 
  RETURN number;
  
  /*****************************************************************************
  * Function: f_build_op_schedule
  * PURPOSE: Builds the operating schedule for the given payment terms
  * PARAMETERS:
  *   a_ilr_id: The ID of the ILR we are currently building. Used to filter the prior schedule records.
  *   a_revision: The revision of the ILR we are currently building.
  *   a_payment_terms: The payment terms associated with this ILR. The schedule will
  *                     build for payment_month_frequency (i.e. the number of months between payments),
  *                               payment_term_start_date (i.e. the starting date of the payment term),
  *                               number_of_terms (i.e. the number of payments that will be made),
  *                               payment_amount (i.e. the amount of payment to apply)
  *                               is_prepay (0 = arrears / 1 = prepay)
  *   a_initial_direct_costs: The Initial Direct Costs associated with this ILR
  *   NOTE: Multiple payment terms can be defined. For example, for a 36 month, prepay monthly lease,
  *           with $500 payments in year 1, $550 payments in year 2, and $600 payments in year three,
  *           provide three payment terms (1, <year_1_start>, 12, 500, 1),
  *                                       (1, <year_2_start>, 12, 550, 1),
  *                                       (1, <year_3_start>, 12, 600, 1)
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with schedule results (pipelined)
  ******************************************************************************/
  FUNCTION f_build_op_schedule( a_ilr_id number,
                                a_revision number,
                                a_payment_terms lsr_ilr_op_sch_pay_term_tab,
                                a_initial_direct_costs lsr_init_direct_cost_info_tab,
								a_set_of_books_id NUMBER)
  RETURN lsr_ilr_op_sch_result_tab PIPELINED DETERMINISTIC;

  /*****************************************************************************
  * Function: f_get_op_schedule
  * PURPOSE: Builds and returns the operating schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule. The schedule will
  *             build for payment payment terms given in table lsr_ilr_payment_term
  *   a_revision: The revision of the ILR for which to build the schedule
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with schedule results (pipelined)
  ******************************************************************************/
  FUNCTION f_get_op_schedule( a_ilr_id NUMBER,
                              a_revision number,
							  a_set_of_books_id NUMBER)
  RETURN lsr_ilr_op_sch_result_tab PIPELINED;
  
  /*****************************************************************************
  * Function: f_has_classification_change
  * PURPOSE: Looks up and returns 1 if this set of books has a classification change
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve information
  *   a_revision: The revision of the ILR for which to retrieve information
  *   a_set_of_books_id: The set of books id to check
  * RETURNS: Number
  ******************************************************************************/
  FUNCTION f_has_classification_change(a_ilr_id NUMBER, a_revision NUMBER, a_set_of_books_id NUMBER)
  RETURN NUMBER DETERMINISTIC;

  /*****************************************************************************
  * Function: f_ilr_has_fasb_type
  * PURPOSE: Looks up and returns whether or not the ILR is associated to any 
  * 			sets of books with the passed in fasb type
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR to check
  *   a_revision: The revision of the ILR to check
  *   a_fasb_type_id: number: A fasb_type_id to check
  * RETURNS: 0 if no, 1 if yes
  ******************************************************************************/
  FUNCTION f_ilr_has_fasb_type(a_ilr_id number, a_revision number, a_fasb_type_id number) return number;
  
  /*****************************************************************************
  * Function: f_ilr_has_operating
  * PURPOSE: Looks up and returns whether or not the ILR is associated to any sets of books with operating cap type
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR to check
  *   a_revision: The revision of the ILR to check
  *
  * RETURNS: 0 if no, 1 if yes
  ******************************************************************************/
  FUNCTION f_ilr_has_operating( a_ilr_id NUMBER,
                                a_revision number)
  return number;

  /*****************************************************************************
  * Function: f_ilr_has_sales_type
  * PURPOSE: Looks up and returns whether or not the ILR is associated to any sets of books with sales type cap type
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR to check
  *   a_revision: The revision of the ILR to check
  *
  * RETURNS: 0 if no, 1 if yes
  ******************************************************************************/
  FUNCTION f_ilr_has_sales_type(a_ilr_id number,
                                a_revision number)
  return number;

  /*****************************************************************************
  * Function: f_ilr_has_direct_finance
  * PURPOSE: Looks up and returns whether or not the ILR is associated to any sets of books with direct finance cap type
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR to check
  *   a_revision: The revision of the ILR to check
  *
  * RETURNS: 0 if no, 1 if yes
  ******************************************************************************/
  FUNCTION f_ilr_has_direct_finance(a_ilr_id number,
                                    a_revision number)
  RETURN NUMBER;

  /*****************************************************************************
  * Function: f_get_payment_terms
  * PURPOSE: Looks up and returns the payment terms for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve payment terms
  *   a_revision: The revision of the ILR for which to retrieve payment terms
  *
  * RETURNS: Table with payment terms
  * NOTE: This now looks to the variable payments package for logic to get payment terms
  ******************************************************************************/
  FUNCTION f_get_payment_terms( a_ilr_id NUMBER,
                                a_revision NUMBER)
  RETURN lsr_ilr_op_sch_pay_term_tab;
  
  /*****************************************************************************
  * Function: f_get_payment_info_from_terms
  * PURPOSE: Transforms payment terms into a month-by-month listing of payment information
  * PARAMETERS:
  *   a_payment_terms: The payment terms for which to generate payment info
  *
  * RETURNS: Table with payment information
  ******************************************************************************/
  FUNCTION f_get_payment_info_from_terms(a_payment_terms lsr_ilr_op_sch_pay_term_tab)
  RETURN lsr_ilr_op_sch_pay_info_tab DETERMINISTIC;

  /*****************************************************************************
  * Function: f_get_payments_from_info
  * PURPOSE: Transforms month-by-month payment information listing into month-by-month list of calculated payment amounts
  * PARAMETERS:
  *   a_payment_info: The payment information for which to calculate payment amounts
  *
  * RETURNS: Table with calculated payment amounts (pipelined)
  ******************************************************************************/
  FUNCTION f_get_payments_from_info(a_payment_info lsr_ilr_op_sch_pay_info_tab)
  RETURN lsr_schedule_payment_def_tab DETERMINISTIC;

  /*****************************************************************************
  * Function: f_get_npv_payments_residual
  * PURPOSE: Gets the NPV for payments and residuals based on passed in information
  * PARAMETERS:
  *   a_payments: lsr_schedule_payment_def_tab: 
  *						The payment terms for which to use to calculate NPV
  *	  a_rates_implicit: t_lsr_rates_implicit_in_lease: The rate to use to calculate the NPV
  *	  a_sales_type_info: lsr_ilr_sales_sch_info: The sales type information
  *
  * RETURNS: LSR_NPV_VALUES: The NPV value for the payments and residual amounts
  ******************************************************************************/
  FUNCTION f_get_npv_payments_residual(a_payments lsr_schedule_payment_def_tab, 
									a_rates_implicit t_lsr_rates_implicit_in_lease,
									a_sales_type_info lsr_ilr_sales_sch_info)
  RETURN LSR_NPV_VALUES DETERMINISTIC;

  /*****************************************************************************
  * Function: f_get_initial_direct_costs
  * PURPOSE: Looks up and returns the initial direct costs for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve payment terms
  *   a_revision: The revision of the ILR for which to retrieve payment terms
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with initial direct costs (pipelined)
  ******************************************************************************/
  FUNCTION f_get_initial_direct_costs(a_ilr_id NUMBER,
                                      a_revision NUMBER,
									  a_set_of_books_id NUMBER)
  RETURN lsr_init_direct_cost_info_tab;

  /*****************************************************************************
  * Function: f_get_sales_type_info
  * PURPOSE: Looks up and returns information necessary to complete the building of sales-type schedules
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve infomration
  *   a_revision: The revision of the ILR for which to retrieve information
  *   a_fasb_type: The FASB Cap type the function is being called for, since remeasurement values can differ by cap type.
  *                Valid values are 2 for Sales-Type ILRs and 3 for Direct Finance ILRs
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with sales-type information (pipelined)
  ******************************************************************************/
  FUNCTION f_get_sales_type_info( a_ilr_id NUMBER,
                                  a_revision number,
								  a_fasb_type NUMBER,
								  a_set_of_books_id NUMBER)
  return lsr_ilr_sales_sch_info;

  /*****************************************************************************
  * Function: f_calculate_buckets
  * PURPOSE: Calculates executory and contingent bucket accruals and payments
  * PARAMETERS:
  *   a_payment_terms: The payment terms for which to process buckets
  *
  * RETURNS: Table with bucket information (pipelined)
  ******************************************************************************/
  FUNCTION f_calculate_buckets(payment_info lsr_ilr_op_sch_pay_info_tab)
  RETURN lsr_bucket_result_tab PIPELINED DETERMINISTIC;

  /*****************************************************************************
  * Function: f_calc_rates_implicit
  * PURPOSE: Calculates the rates implicit with the given
  *           payment terms and initial costs
  * PARAMETERS:
  *   a_payments: The payments
  *   a_initial_direct_costs: The initial direct costs
  *   a_sales_type_info: The sales-type-specific info
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve information
  *   a_revision: The revision of the ILR for which to retrieve information
  *   a_set_of_books_id: The set of books id to check
  *	  a_fasb_type: The fasb type (2 = ST, 3 = DF)
  *
  * RETURNS: Rates implicit in lease
  * NOTE: For a sales-type lease, the rates for net investment do not differ from other rates
  ******************************************************************************/
  FUNCTION f_calc_rates_implicit( a_payments lsr_schedule_payment_def_tab,
                                        a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                        a_sales_type_info lsr_ilr_sales_sch_info,
										a_ilr_id NUMBER, a_revision NUMBER, a_set_of_books_id NUMBER,
										a_fasb_type NUMBER)
  RETURN t_lsr_rates_implicit_in_lease DETERMINISTIC;

  /*****************************************************************************
  * Function: f_calc_rates_implicit_sales
  * PURPOSE: Calculates the rates implicit for a sales-type lease with the given
  *           payment terms and initial costs
  * PARAMETERS:
  *   a_payments: The payments
  *   a_initial_direct_costs: The initial direct costs
  *   a_sales_type_info: The sales-type-specific info
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve payment terms
  *   a_revision: The revision of the ILR for which to retrieve payment terms
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Rates implicit in lease
  * NOTE: For a sales-type lease, the rates for net investment do not differ from other rates
  ******************************************************************************/
  FUNCTION f_calc_rates_implicit_sales(a_payments lsr_schedule_payment_def_tab,
                                       a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                       a_sales_type_info lsr_ilr_sales_sch_info,
									   a_ilr_id NUMBER,
									   a_revision NUMBER,
									   a_set_of_books_id NUMBER)
  RETURN t_lsr_rates_implicit_in_lease DETERMINISTIC;

  /*****************************************************************************
  * Function: f_calc_rates_implicit_df
  * PURPOSE: Calculates the rates implicit for a direct-finanace-type lease with the given
  *           payment terms and initial costs
  * PARAMETERS:
  *   a_payments: The payments
  *   a_initial_direct_costs: The initial direct costs
  *   a_sales_type_info: The sales-type-specific info
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve payment terms
  *   a_revision: The revision of the ILR for which to retrieve payment terms
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Rates implicit in lease
  ******************************************************************************/
  function f_calc_rates_implicit_df(a_payments lsr_schedule_payment_def_tab,
                                    a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                    a_sales_type_info lsr_ilr_sales_sch_info,
									a_ilr_id NUMBER,
									a_revision NUMBER,
									a_set_of_books_id NUMBER)
  RETURN t_lsr_rates_implicit_in_lease DETERMINISTIC;

  /*****************************************************************************
  * Function: f_calc_rates_implicit_ni_df
  * PURPOSE: Calculates the rates implicit_ni for a direct-finance-type lease with the given
  *           payment terms and initial costs
  * PARAMETERS:
  *   a_payments: The payments
  *   a_initial_direct_costs: The initial direct costs
  *   a_sales_type_info: The sales-type-specific info
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve information
  *   a_revision: The revision of the ILR for which to retrieve information
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Rates implicit in lease
  ******************************************************************************/
  function f_calc_rates_implicit_ni_df(a_payments lsr_schedule_payment_def_tab,
                                    a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                    a_sales_type_info lsr_ilr_sales_sch_info,
									a_ilr_id NUMBER, a_revision NUMBER, a_set_of_books_id NUMBER)
  RETURN float;

  /*****************************************************************************
  * Function: f_get_override_rates_sales
  * PURPOSE: Looks up and return manual override rates for sales type ILRs
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to look for manually overriden rates
  *   a_ilr_id: The revision of a_ilr_id for which to look for manually overriden rates
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Manually overriden rates
  ******************************************************************************/
  FUNCTION f_get_override_rates_sales(a_ilr_id NUMBER,
                                      a_revision NUMBER,
									  a_set_of_books_id NUMBER)
  RETURN t_lsr_rates_implicit_in_lease;

  /*****************************************************************************
  * Function: f_get_override_rates_df
  * PURPOSE: Looks up and return manual override rates for direct-finance type ILRs
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to look for manually overriden rates
  *   a_ilr_id: The revision of a_ilr_id for which to look for manually overriden rates
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Manually overriden rates
  ******************************************************************************/
  FUNCTION f_get_override_rates_df( a_ilr_id NUMBER,
                                    a_revision NUMBER,
									a_set_of_books_id NUMBER)
  RETURN t_lsr_rates_implicit_in_lease;

  /*****************************************************************************
  * Function: f_get_prelim_info
  * PURPOSE: Looks up and return all preliminary info used in building ST or DF schedule
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to gather preliminary info
  *   a_revision: The revision of a_ilr_id for which to gather preliminary info
  *   a_set_of_books_id: The set of books id to get the information for
  *	  a_fasb_type: The fasb classification type (2 for ST, 3 for DF)
  * RETURNS All preliminary info used in schedule building process
  ******************************************************************************/
  FUNCTION f_get_prelim_info(a_ilr_id NUMBER, a_revision NUMBER,
							a_set_of_books_id NUMBER, a_fasb_type_id NUMBER)
  RETURN t_lsr_ilr_sales_df_prelims DETERMINISTIC;
  
  /*****************************************************************************
  * Function: f_get_prelim_info_sales
  * PURPOSE: Looks up and return all preliminary info used in building a sales schedule
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to gather preliminary info
  *   a_revision: The revision of a_ilr_id for which to gather preliminary info
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: All preliminary info used in schedule building process
  ******************************************************************************/
  FUNCTION f_get_prelim_info_sales( a_ilr_id NUMBER,
                                    a_revision NUMBER,
									a_set_of_books_id NUMBER)
  RETURN t_lsr_ilr_sales_df_prelims DETERMINISTIC;

  /*****************************************************************************
  * Function: f_get_prelim_info_df
  * PURPOSE: Looks up and return all preliminary info used in building DF schedule
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to gather preliminary info
  *   a_revision: The revision of a_ilr_id for which to gather preliminary info
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS All preliminary info used in schedule building process
  ******************************************************************************/
  FUNCTION f_get_prelim_info_df(a_ilr_id NUMBER,
                                a_revision NUMBER,
								a_set_of_books_id NUMBER)
  RETURN t_lsr_ilr_sales_df_prelims DETERMINISTIC;
  
  /*****************************************************************************
  * Function: f_get_selling_profit_loss
  * PURPOSE: returns the selling profit loss given the sales info and Initial Direct Costs
  * PARAMETERS:
  *   a_ilr_id: The ID of the ILR being built
  *   a_revision: The revision of the ILR being built
  *   a_set_of_books_id: The set of books id to check
  *   a_sales_type_info: The sales type information
  *   a_initial_direct_costs: The initial direct costs
  *   a_fasb_type: The fasb type.  2 for ST, 3 for DF.
  *
  * RETURNS: NUMBER: The selling profict loss
  ******************************************************************************/
  FUNCTION f_get_selling_profit_loss( a_ilr_id NUMBER, a_revision NUMBER, a_set_of_books_id NUMBER,
									a_sales_type_info lsr_ilr_sales_sch_info,
                                    a_initial_direct_costs lsr_init_direct_cost_info_tab,
									a_fasb_type NUMBER)
  RETURN NUMBER;
  
  /*****************************************************************************
  * Function: f_build_sales_schedule
  * PURPOSE: Builds the sales-type schedule for the given payment terms and sales-type-specific information
  * PARAMETERS:
  *   a_ilr_id: The ID of the ILR being built
  *   a_revision: The revision of the ILR being built
  *   a_payment_terms: The payment terms to use in generating the schedule
  *   a_initial_direct_costs: The initial direct costs associated with the ILR
  *   a_sales_type_info: The sales-type-specific info to use in generating the schedule.
  *   a_rates_implicit: The rates to use in building the schedule
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  FUNCTION f_build_sales_schedule(a_ilr_id number,
                                  a_revision number,
                                  a_payment_info lsr_ilr_op_sch_pay_info_tab,
                                  a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                  a_sales_type_info lsr_ilr_sales_sch_info,
                                  a_rates_implicit t_lsr_rates_implicit_in_lease,
								  a_set_of_books_id NUMBER)
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED DETERMINISTIC;
  
  /*****************************************************************************
  * Function: f_build_sales_schedule
  * PURPOSE: Builds the sales-type schedule for the given payment terms and sales-type-specific information
  * PARAMETERS:
  *   a_ilr_id: The ID of the ILR being built
  *   a_revision: The revision of the ILR being built
  *   a_payment_terms: The payment terms to use in generating the schedule
  *   a_initial_direct_costs: The initial direct costs associated with the ILR
  *   a_sales_type_info: The sales-type-specific info to use in generating the schedule.
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  function f_build_sales_schedule(a_ilr_id number,
                                  a_revision number,
                                  a_payment_info lsr_ilr_op_sch_pay_info_tab,
                                  a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                  a_sales_type_info lsr_ilr_sales_sch_info,
                                  a_rates_implicit t_lsr_rates_implicit_in_lease,
                                  is_finance_type NUMBER,
								  a_set_of_books_id NUMBER)
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED DETERMINISTIC;

  /*****************************************************************************
  * Function: f_build_df_schedule
  * PURPOSE: Builds the direct-finance-type schedule for the given payment terms and
              sales-type (also used for direct finance) information
  * PARAMETERS:
  *   a_ilr_id: The ID of the ILR being built
  *   a_revision: The revision of the ILR being built
  *   a_payment_terms: The payment terms to use in generating the schedule
  *   a_initial_direct_costs: The initial direct costs associated with the ILR
  *   a_sales_type_info: The sales-type-specific info to use in generating the schedule.
  *   a_rates_implicit: The rates to use in building the schedule
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  function f_build_df_schedule( a_ilr_id number,
                                a_revision number,
                                a_payment_info lsr_ilr_op_sch_pay_info_tab,
                                a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                a_sales_type_info lsr_ilr_sales_sch_info,
                                a_rates_implicit t_lsr_rates_implicit_in_lease,
								a_set_of_books_id NUMBER)
  RETURN lsr_ilr_df_schedule_result_tab PIPELINED DETERMINISTIC;

  /*****************************************************************************
  * Function: f_get_sales_schedule
  * PURPOSE: Builds and returns the sales-type schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *   a_prelims: Sales Type Preliminary Info to use in schedule calculation
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  FUNCTION f_get_sales_schedule(a_ilr_id NUMBER,
                                a_revision NUMBER,
                                a_prelims t_lsr_ilr_sales_df_prelims,
								a_set_of_books_id NUMBER)
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED;

  /*****************************************************************************
  * Function: f_get_sales_schedule
  * PURPOSE: Builds and returns the sales-type schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  FUNCTION f_get_sales_schedule(a_ilr_id NUMBER,
                                a_revision NUMBER,
								a_set_of_books_id NUMBER)
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED;

  /*****************************************************************************
  * Function: f_get_df_schedule
  * PURPOSE: Builds and returns the direct-finance-type schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *   a_prelims: Sales Type Preliminary Info to use in schedule calculation
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  FUNCTION f_get_df_schedule( a_ilr_id NUMBER,
                              a_revision NUMBER,
                              a_prelims t_lsr_ilr_sales_df_prelims,
							  a_set_of_books_id NUMBER)
  RETURN lsr_ilr_df_schedule_result_tab PIPELINED;

  /*****************************************************************************
  * Function: f_get_df_schedule
  * PURPOSE: Builds and returns the direct-finance-type schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  FUNCTION f_get_df_schedule( a_ilr_id NUMBER,
                              a_revision NUMBER,
							  a_set_of_books_id NUMBER)
  RETURN lsr_ilr_df_schedule_result_tab PIPELINED;

  /*****************************************************************************
  * Function: f_get_op_sch_quash_exceptions
  * PURPOSE: Builds and returns the operating-type schedule for the given ILR/revision,
  *           logging exceptions and continuing instead of raising exceptions
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with schedule (pipelined)
  * NOTES: Logs exceptions thrown using p_log_kickouts and continues normally on exception
  *          CONSIDER THE IMPLICATIONS OF THIS BEFORE USING THIS FUNCTION
  ******************************************************************************/
  FUNCTION f_get_op_sch_quash_exceptions(a_ilr_id NUMBER, a_revision NUMBER, a_set_of_books_id NUMBER)
  RETURN lsr_ilr_op_sch_result_tab PIPELINED;

  /*****************************************************************************
  * Function: f_getsalessch_quash_exceptions
  * PURPOSE: Builds and returns the sales-type schedule for the given ILR/revision,
  *           logging exceptions and continuing instead of raising exceptions
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with schedule (pipelined)
  * NOTES: Logs exceptions thrown using p_log_kickouts and continues normally on exception
  *          CONSIDER THE IMPLICATIONS OF THIS BEFORE USING THIS FUNCTION
  ******************************************************************************/
  FUNCTION f_getsalessch_quash_exceptions(a_ilr_id NUMBER, a_revision NUMBER, a_set_of_books_id NUMBER)
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED;

  /*****************************************************************************
  * Function: f_get_df_sch_quash_exceptions
  * PURPOSE: Builds and returns the direct-finance-type schedule for the given ILR/revision,
  *           logging exceptions and continuing instead of raising exceptions
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *   a_set_of_books_id: The set of books id to check
  *
  * RETURNS: Table with schedule (pipelined)
  * NOTES: Logs exceptions thrown using p_log_kickouts and continues normally on exception
  *         CONSIDER THE IMPLICATIONS OF THIS BEFORE USING THIS FUNCTION
  ******************************************************************************/
  FUNCTION f_get_df_sch_quash_exceptions(a_ilr_id NUMBER, a_revision NUMBER, a_set_of_books_id NUMBER)
  RETURN lsr_ilr_df_schedule_result_tab PIPELINED;

END pkg_lessor_schedule;
/


CREATE OR REPLACE FORCE VIEW v_lsr_ilr_op_schedule_calc AS
SELECT  results.ilr_id,
        results.revision,
        results.set_of_books_id,
        cols.month,
        cols.interest_income_received,
        cols.interest_income_accrued,
        cols.interest_rental_recvd_spread,
        cols.begin_deferred_rev,
        cols.deferred_rev,
        cols.end_deferred_rev,
        cols.begin_receivable,
        cols.end_receivable,
        cols.begin_lt_receivable,
        cols.end_lt_receivable,
        cols.initial_direct_cost,
        cols.executory_accrual1,
        cols.executory_accrual2,
        cols.executory_accrual3,
        cols.executory_accrual4,
        cols.executory_accrual5,
        cols.executory_accrual6,
        cols.executory_accrual7,
        cols.executory_accrual8,
        cols.executory_accrual9,
        cols.executory_accrual10,
        cols.executory_paid1,
        cols.executory_paid2,
        cols.executory_paid3,
        cols.executory_paid4,
        cols.executory_paid5,
        cols.executory_paid6,
        cols.executory_paid7,
        cols.executory_paid8,
        cols.executory_paid9,
        cols.executory_paid10,
        cols.contingent_accrual1,
        cols.contingent_accrual2,
        cols.contingent_accrual3,
        cols.contingent_accrual4,
        cols.contingent_accrual5,
        cols.contingent_accrual6,
        cols.contingent_accrual7,
        cols.contingent_accrual8,
        cols.contingent_accrual9,
        cols.contingent_accrual10,
        cols.contingent_paid1,
        cols.contingent_paid2,
        cols.contingent_paid3,
        cols.contingent_paid4,
        cols.contingent_paid5,
        cols.contingent_paid6,
        cols.contingent_paid7,
        cols.contingent_paid8,
        cols.contingent_paid9,
        cols.contingent_paid10,
        t_lsr_ilr_schedule_all_rates( t_lsr_rates_implicit_in_lease(NULL, NULL), 
                                          t_lsr_rates_implicit_in_lease(NULL, NULL), 
                                          t_lsr_rates_implicit_in_lease(NULL, NULL)) as schedule_rates
FROM (SELECT  ilro.ilr_id,
              ilro.revision,
              fasb_sob.set_of_books_id,
              pkg_lessor_schedule.f_get_op_schedule(ilr_id,revision, fasb_sob.set_of_books_id) sch
      FROM lsr_ilr_options ilro
      JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
      JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
      WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION) ) = 'operating') results, TABLE ( results.sch ) (+) cols;


CREATE OR REPLACE FORCE VIEW v_lsr_ilr_sales_schedule_calc AS
SELECT  results.ilr_id,
        results.revision,
        results.set_of_books_id,
        cols.month,
        cols.principal_received,
        cols.interest_income_received,
        cols.interest_income_accrued,
        cols.principal_accrued,
        cols.begin_receivable,
        cols.end_receivable,
        cols.begin_lt_receivable,
        cols.end_lt_receivable,
        cols.initial_direct_cost,
        cols.executory_accrual1,
        cols.executory_accrual2,
        cols.executory_accrual3,
        cols.executory_accrual4,
        cols.executory_accrual5,
        cols.executory_accrual6,
        cols.executory_accrual7,
        cols.executory_accrual8,
        cols.executory_accrual9,
        cols.executory_accrual10,
        cols.executory_paid1,
        cols.executory_paid2,
        cols.executory_paid3,
        cols.executory_paid4,
        cols.executory_paid5,
        cols.executory_paid6,
        cols.executory_paid7,
        cols.executory_paid8,
        cols.executory_paid9,
        cols.executory_paid10,
        cols.contingent_accrual1,
        cols.contingent_accrual2,
        cols.contingent_accrual3,
        cols.contingent_accrual4,
        cols.contingent_accrual5,
        cols.contingent_accrual6,
        cols.contingent_accrual7,
        cols.contingent_accrual8,
        cols.contingent_accrual9,
        cols.contingent_accrual10,
        cols.contingent_paid1,
        cols.contingent_paid2,
        cols.contingent_paid3,
        cols.contingent_paid4,
        cols.contingent_paid5,
        cols.contingent_paid6,
        cols.contingent_paid7,
        cols.contingent_paid8,
        cols.contingent_paid9,
        cols.contingent_paid10,
        cols.begin_unguaranteed_residual,
        cols.int_on_unguaranteed_residual,
        cols.end_unguaranteed_residual,
        cols.begin_net_investment,
        cols.int_on_net_investment,
        cols.end_net_investment,
        cols.rate_implicit,
        0 as compounded_rate,
        cols.discount_rate,
        cols.rate_implicit_ni,
        0 as compounded_rate_ni,
        0 as discount_rate_ni,
        cols.begin_lease_receivable,
        cols.npv_lease_payments,
        cols.npv_guaranteed_residual,
        cols.npv_unguaranteed_residual,
        cols.selling_profit_loss,
        cols.cost_of_goods_sold,
        cols.schedule_rates
  FROM
    (
      SELECT
        ilro.ilr_id,
        ilro.revision,
        fasb_sob.set_of_books_id,
        pkg_lessor_schedule.f_get_sales_schedule(ilro.ilr_id,ilro.revision, fasb_sob.set_of_books_id) sch
      FROM
        lsr_ilr_options ilro
        JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
        JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
      WHERE
        lower(TRIM(fasb_cap_type.description) ) = 'sales type'
    ) results,
    TABLE ( results.sch ) (+) cols;

--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16384, 0, 2018, 2, 1, 0, 52908, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.1.0_maint_052908_lessor_03_pkg_view_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
	  