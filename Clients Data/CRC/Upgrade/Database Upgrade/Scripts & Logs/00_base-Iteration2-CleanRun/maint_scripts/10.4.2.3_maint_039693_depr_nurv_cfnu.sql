/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039693_depr_nurv_cfnu.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.3 08/12/2014 Brandon Beck
||============================================================================
*/

alter table CPR_DEPR_CALC_NURV_STG drop primary key drop index;

alter table CPR_DEPR_CALC_NURV_STG
   add constraint PK_CPR_DEPR_CALC_NURV_STG
       primary key (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, WORK_ORDER_NUMBER);

alter table CPR_DEPR_CALC_CFNU_STG drop primary key drop index;

alter table CPR_DEPR_CALC_CFNU_STG
   add constraint PK_CPR_DEPR_CALC_CFNU_STG
       primary key (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, WORK_ORDER_NUMBER);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1385, 0, 10, 4, 2, 3, 39693, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.3_maint_039693_depr_nurv_cfnu.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
