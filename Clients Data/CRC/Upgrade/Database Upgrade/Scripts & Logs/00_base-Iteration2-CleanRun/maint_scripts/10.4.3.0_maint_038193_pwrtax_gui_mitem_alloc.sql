/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038193_pwrtax_gui_mitem_alloc.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 07/21/2014 Andrew Scott        Base GUI workspaces for the M Item
||                                         allocation interface.
||============================================================================
*/

----
----  set up the new workspace to be accessible in powertax base gui
----

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'depr_input_mitem_alloc', 'M-Item Allocations', 'uo_tax_depr_act_wksp_mitem_alloc',
    'M-Item Allocations');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'depr_input_mitem_alloc'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'depr_input_mitem_alloc';

----
----  set up the dynamic filters.
----
insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
values
   (70, 'PowerTax - M Item Allocation', 'uo_ppbase_tab_filter_dynamic');

----make a new required, single select company filter.
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (171, 'Company', 'dw', 'company.company_id', '', 'dw_pp_company_filter', 'company_id', 'description', 'N', 1, 1, 0,
    0, 0);

----make a tax year filter that will be aliased in the selection and result grids
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (172, 'Tax Year', 'dw', 'm_item_filter_view.tax_year', '', 'dw_tax_year_unlock_by_version_filter', 'tax_year',
    'tax_year', 'D', 1, 1, 0, 0, 0);

----vintage by tax year filter that filters out the trunc of tax years not selected.
insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (173, 'Vintage', 'dw', 'vintage.vintage_id', '', 'dw_tax_vintage_trunc_years_filter', 'vintage_id', 'description',
    'N', 0, 0, 0, 0, 0);

----set up the mapping and restriction
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (70, 171);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (70, 172);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (70, 173);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (70, 110);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (70, 108);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (70, 99);
insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE)
values
   (56, 173, 172, 'tax_year_version.tax_year', '');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1271, 0, 10, 4, 3, 0, 38193, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038193_pwrtax_gui_mitem_alloc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;