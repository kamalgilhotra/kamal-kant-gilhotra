/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049478_taxrpr_configure_pre_ineligible_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 10/18/2017 Eric Berger    Loads in the new pretest explain
||                                      values for the ineligible tax uop
||                                      enhancement.
||============================================================================
*/

INSERT INTO repair_pretest_explain (id, description, long_description)
SELECT 0, 'Ineligible', 'The tax unit of property is marked as "Expense Ineligible"'
FROM dual
WHERE NOT EXISTS
(
  SELECT 1
  FROM repair_pretest_explain
  WHERE id = 0
);

INSERT INTO repair_lookup_qty_cost (id,Description)
SELECT 0, 'Ineligible'
FROM dual
WHERE NOT EXISTS
(
  SELECT 1
  FROM repair_lookup_qty_cost
  WHERE id = 0
);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13731, 0, 2018, 2, 0, 0, 49478, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_049478_taxrpr_configure_pre_ineligible_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;