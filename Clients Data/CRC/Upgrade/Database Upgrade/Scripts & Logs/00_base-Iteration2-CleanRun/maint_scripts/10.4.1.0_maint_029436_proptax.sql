/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029436_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   04/21/2013 Julia Breuer   Point Release
|| 10.4.2.0   10/22/2013 Andrew Scott   Maint 33371 : script fails on moving the authority limit
||                                      from the old approval tables (22,2) to the new workflow tables (22,8)
||                                      if the amount is too large.  Implementer likely put in 9.99 repeating to
||                                      to the limit, so this needs to be edited
|| 2015.2.0.1 05/01/2016 Brandon Borm   Fix Workflow data
||============================================================================
*/

--
-- Setup workflow approvals for Property Tax Payments
--
insert into PWRPLANT.APPROVAL_NOTIFICATION
   (NOTIFICATION_TYPE, COMPANY_ID, NOTIFICATION_MESSAGE, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP,
    USER_ID, DATA_SQL, ATTACHED_DATAWINDOW, USER_SQL, NOTIFICATION_SUBJECT)
values
   ('PROPTAX PAYMENT REJECTED MESSAGE', -1,
    'Payment ID: <col1> was rejected.  The payment is for prop tax company <col2>, state <col3>, and statement group <col4>.',
    '',
    'The email message body for Property Tax Payment rejection emails with ''<col#>'' fields being replaced by the corresponding column number returned by the data_sql field. Field ''<arg1>'' (payment_id) will be replaced by the actual value when the data_sql is run.',
    sysdate, user,
    'select sgp.payment_id, ptco.description, nvl( st.long_description, trim( st.state_id ) ), sg.description from pt_statement_group_payment sgp, pt_statement_group sg, pt_company ptco, state st where sgp.payment_id = <arg1> and sgp.statement_group_id = sg.statement_group_id and sg.prop_tax_company_id = ptco.prop_tax_company_id and sg.state_id = st.state_id',
    null, null, 'Property Tax Payment Rejected');

insert into PWRPLANT.APPROVAL_NOTIFICATION
   (NOTIFICATION_TYPE, COMPANY_ID, NOTIFICATION_MESSAGE, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP,
    USER_ID, DATA_SQL, ATTACHED_DATAWINDOW, USER_SQL, NOTIFICATION_SUBJECT)
values
   ('PROPTAX PAYMENT APPROVED MESSAGE', -1,
    'Payment ID: <col1> was approved.  The payment is for prop tax company <col2>, state <col3>, and statement group <col4>.',
    '',
    'The email message body for Property Tax Payment approved emails with ''<col#>'' fields being replaced by the corresponding column number returned by the data_sql field. Field ''<arg1>'' (payment_id) will be replaced by the actual value when the data_sql is run.',
    sysdate, user,
    'select sgp.payment_id, ptco.description, nvl( st.long_description, trim( st.state_id ) ), sg.description from pt_statement_group_payment sgp, pt_statement_group sg, pt_company ptco, state st where sgp.payment_id = <arg1> and sgp.statement_group_id = sg.statement_group_id and sg.prop_tax_company_id = ptco.prop_tax_company_id and sg.state_id = st.state_id',
    null, null, 'Property Tax Payment Approved');

insert into PWRPLANT.APPROVAL_NOTIFICATION
   (NOTIFICATION_TYPE, COMPANY_ID, NOTIFICATION_MESSAGE, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP,
    USER_ID, DATA_SQL, ATTACHED_DATAWINDOW, USER_SQL, NOTIFICATION_SUBJECT)
values
   ('PROPTAX PAYMENT APPROVAL MESSAGE', -1,
    'Payment ID: <col1> is ready for approval.  The payment is for prop tax company <col2>, state <col3>, and statement group <col4>.',
    '',
    'The email message body for Property Tax Payment approval emails with ''<col#>'' fields being replaced by the corresponding column number returned by the data_sql field. Field ''<arg1>'' (payment_id) will be replaced by the actual value when the data_sql is run.',
    sysdate, user,
    'select sgp.payment_id, ptco.description, nvl( st.long_description, trim( st.state_id ) ), sg.description from pt_statement_group_payment sgp, pt_statement_group sg, pt_company ptco, state st where sgp.payment_id = <arg1> and sgp.statement_group_id = sg.statement_group_id and sg.prop_tax_company_id = ptco.prop_tax_company_id and sg.state_id = st.state_id',
    null, null, 'Property Tax Payment Awaiting Approval');

insert into PWRPLANT.WORKFLOW_SUBSYSTEM
   (SUBSYSTEM, FIELD1_DESC, FIELD1_SQL, PENDING_NOTIFICATION_TYPE, APPROVED_NOTIFICATION_TYPE,
    REJECTED_NOTIFICATION_TYPE, SEND_SQL, APPROVE_SQL, REJECT_SQL, UNREJECT_SQL, UNSEND_SQL,
    UPDATE_WORKFLOW_TYPE_SQL, WORKFLOW_TYPE_FILTER, SEND_EMAILS, CASCADE_APPROVALS)
values
   ('proptax_payment_approval', 'Payment ID', '<<id_field1>>', 'PROPTAX PAYMENT APPROVAL MESSAGE',
    'PROPTAX PAYMENT APPROVED MESSAGE', 'PROPTAX PAYMENT REJECTED MESSAGE', null,
    'update pt_statement_group_payment set payment_status_id = 2, payment_approval_date = sysdate, payment_approver = user where payment_id = <<id_field1>>',
    'update pt_statement_group_payment set payment_status_id = 5, payment_rejected_date = sysdate, payment_rejector = user where payment_id = <<id_field1>>',
    'update pt_statement_group_payment set payment_status_id = 1, payment_rejected_date = null, payment_rejector = null where payment_id = <<id_field1>>',
    'update pt_statement_group_payment set payment_status_id = 6, voider = user, voided_date = sysdate where payment_id = <<id_field1>>',
    'update pt_statement_group set approval_type_id = (select workflow_type_id from workflow where subsystem = ''proptax_payment_approval'' and id_field1 = <<id_field1>>) where statement_group_id in (select statement_group_id from pt_statement_group_payment where payment_id = <<id_field1>>',
    'and workflow_type_id = (select sg.approval_type_id from pt_statement_group_payment sgp, pt_statement_group sg where sgp.statement_group_id = sg.statement_group_id and sgp.payment_id = <<id_field1>>)',
    0, 0);

insert into PWRPLANT.WORKFLOW_AMOUNT_SQL
   (SUBSYSTEM, BASE_SQL, sql)
values
   ('proptax_payment_approval', 1,
    'select nvl(sum(amount_paid),0) appr_amount from pt_statement_group_payment where payment_id = <<id_field1>>');

--
-- Setup workflow approvals for Property Tax Accruals
--
insert into PWRPLANT.APPROVAL_NOTIFICATION
   (NOTIFICATION_TYPE, COMPANY_ID, NOTIFICATION_MESSAGE, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP,
    USER_ID, DATA_SQL, ATTACHED_DATAWINDOW, USER_SQL, NOTIFICATION_SUBJECT)
values
   ('PROPTAX ACCRUAL REJECTED MESSAGE', -1,
    'Accrual journal entries for Prop Tax Company <col1> and month <col2> were rejected.', '',
    'The email message body for Property Tax Accrual rejection emails with ''<col#>'' fields being replaced by the corresponding column number returned by the data_sql field. Fields ''<arg1>'' (prop_tax_company_id) and ''<arg2>'' (month) will be replaced by the actual values when the data_sql is run.',
    sysdate, user,
    'select ptco.description, to_char( ap.month, ''mm/dd/yyyy'' ) from pt_accrual_pending ap, company co, pt_company ptco where ap.company_id = co.company_id and co.prop_tax_company_id = ptco.prop_tax_company_id and ptco.prop_tax_company_id = <arg1> and to_char( ap.month, ''mm/dd/yyyy'' ) = <arg2>',
    null, null, 'Property Tax Accrual Rejected');

insert into PWRPLANT.APPROVAL_NOTIFICATION
   (NOTIFICATION_TYPE, COMPANY_ID, NOTIFICATION_MESSAGE, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP,
    USER_ID, DATA_SQL, ATTACHED_DATAWINDOW, USER_SQL, NOTIFICATION_SUBJECT)
values
   ('PROPTAX ACCRUAL APPROVED MESSAGE', -1,
    'Accrual journal entries for Prop Tax Company <col1> and month <col2> were approved.', '',
    'The email message body for Property Tax Accrual approved emails with ''<col#>'' fields being replaced by the corresponding column number returned by the data_sql field. Fields ''<arg1>'' (prop_tax_company_id) and ''<arg2>'' (month) will be replaced by the actual values when the data_sql is run.',
    sysdate, user,
    'select ptco.description, to_char( ap.month, ''mm/dd/yyyy'' ) from pt_accrual_pending ap, company co, pt_company ptco where ap.company_id = co.company_id and co.prop_tax_company_id = ptco.prop_tax_company_id and ptco.prop_tax_company_id = <arg1> and to_char( ap.month, ''mm/dd/yyyy'' ) = <arg2>',
    null, null, 'Property Tax Accrual Approved');

insert into PWRPLANT.APPROVAL_NOTIFICATION
   (NOTIFICATION_TYPE, COMPANY_ID, NOTIFICATION_MESSAGE, DESCRIPTION, LONG_DESCRIPTION, TIME_STAMP,
    USER_ID, DATA_SQL, ATTACHED_DATAWINDOW, USER_SQL, NOTIFICATION_SUBJECT)
values
   ('PROPTAX ACCRUAL APPROVAL MESSAGE', -1,
    'Accrual journal entries for Prop Tax Company <col1> and Month <col2> are ready for approval.',
    '',
    'The email message body for Property Tax Accrual approval emails with ''<col#>'' fields being replaced by the corresponding column number returned by the data_sql field. Fields ''<arg1>'' (prop_tax_company_id) and ''<arg2>'' (month) will be replaced by the actual values when the data_sql is run.',
    sysdate, user,
    'select ptco.description, to_char( ap.month, ''mm/dd/yyyy'' ) from pt_accrual_pending ap, company co, pt_company ptco where ap.company_id = co.company_id and co.prop_tax_company_id = ptco.prop_tax_company_id and ptco.prop_tax_company_id = <arg1> and to_char( ap.month, ''mm/dd/yyyy'' ) = <arg2>',
    null, null, 'Property Tax Accrual Awaiting Approval');

insert into PWRPLANT.WORKFLOW_SUBSYSTEM
   (SUBSYSTEM, FIELD1_DESC, FIELD2_DESC, FIELD1_SQL, FIELD2_SQL, PENDING_NOTIFICATION_TYPE,
    APPROVED_NOTIFICATION_TYPE, REJECTED_NOTIFICATION_TYPE, SEND_SQL, APPROVE_SQL, REJECT_SQL,
    UNREJECT_SQL, UNSEND_SQL, UPDATE_WORKFLOW_TYPE_SQL, WORKFLOW_TYPE_FILTER, SEND_EMAILS,
    CASCADE_APPROVALS)
values
   ('proptax_accrual_approval', 'Prop Tax Company ID', 'Month', '<<id_field1>>', '<<id_field2>>',
    'PROPTAX ACCRUAL APPROVAL MESSAGE', 'PROPTAX ACCRUAL APPROVED MESSAGE',
    'PROPTAX ACCRUAL REJECTED MESSAGE', null,
    'update pt_accrual_pending set accrual_status_id = 2, final_approval_date = sysdate, final_approver = user where accrual_status_id = 1 and to_char( month, ''mm/dd/yyyy'' ) = ''<<id_field2>>'' and company_id in (select company_id from company where prop_tax_company_id = <<id_field1>>)',
    'update pt_accrual_pending set accrual_status_id = 3, rejected_date = sysdate, rejector = user where to_char( month, ''mm/dd/yyyy'' ) = ''<<id_field2>>'' and company_id in (select company_id from company where prop_tax_company_id = <<id_field1>>)',
    null, null, null,
    'and workflow_type_id = (select distinct ap.approval_type_id from pt_accrual_pending ap, company co where ap.company_id = co.company_id and co.prop_tax_company_id = <<id_field1>> and to_char( ap.month, ''mm/dd/yyyy'' ) = ''<<id_field2>>'')',
    0, 0);

insert into PWRPLANT.WORKFLOW_AMOUNT_SQL
   (SUBSYSTEM, BASE_SQL, sql)
values
   ('proptax_accrual_approval', 1,
    'select sum(nvl(amount, 0)) appr_amount from pt_accrual_pending ap, company co where ap.company_id = co.company_id and ap.debit_credit_indicator = 0 and ap.accrual_status_id = 1 and co.prop_tax_company_id = <<id_field1>> and to_char( ap.month, ''mm/dd/yyyy'' ) = ''<<id_field2>>''');

--
-- Update the "cascade approvals" field based on the current PTC option.  Then delete the system option.
--
update PWRPLANT.WORKFLOW_SUBSYSTEM
   set CASCADE_APPROVALS =
        (select DECODE(UPPER(trim(DECODE(trim(OPTION_VALUE),
                                         null,
                                         PP_DEFAULT_VALUE,
                                         '',
                                         PP_DEFAULT_VALUE,
                                         OPTION_VALUE))),
                       'YES',
                       1,
                       0)
           from PTC_SYSTEM_OPTIONS
          where SYSTEM_OPTION_ID = 'Payments - Approvals - Cascade Approvals')
 where SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval');

delete from PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES
 where SYSTEM_OPTION_ID = 'Payments - Approvals - Cascade Approvals';
delete from PWRPLANT.PTC_SYSTEM_OPTIONS
 where SYSTEM_OPTION_ID = 'Payments - Approvals - Cascade Approvals';

--
-- Add new columns to the Payment table to keep track of approvers/rejectors, now that we won't have the
-- pt_statement_payment_approval table anymore.  This will make searching & reporting easier.
--
alter table PWRPLANT.PT_STATEMENT_GROUP_PAYMENT add PAYMENT_REJECTED_DATE date;
alter table PWRPLANT.PT_STATEMENT_GROUP_PAYMENT add PAYMENT_REJECTOR      varchar2(18);
alter table PWRPLANT.PT_STATEMENT_GROUP_PAYMENT add PAYMENT_APPROVER      varchar2(18);

--
-- Update new columns on pt_statement_group_payment.
--
update PWRPLANT.PT_STATEMENT_GROUP_PAYMENT SGP
   set SGP.PAYMENT_APPROVER =
        (select COALESCE(SPA.AUTHORIZER10,
                         SPA.AUTHORIZER9,
                         SPA.AUTHORIZER8,
                         SPA.AUTHORIZER7,
                         SPA.AUTHORIZER6,
                         SPA.AUTHORIZER5,
                         SPA.AUTHORIZER4,
                         SPA.AUTHORIZER3,
                         SPA.AUTHORIZER2,
                         SPA.AUTHORIZER1)
           from PT_STATEMENT_PAYMENT_APPROVAL SPA
          where SPA.PAYMENT_ID = SGP.PAYMENT_ID);

update PWRPLANT.PT_STATEMENT_GROUP_PAYMENT SGP
   set (SGP.PAYMENT_REJECTOR, SGP.PAYMENT_REJECTED_DATE) =
        (select SPA.REJECTOR, SPA.REJECTED_DATE
           from PT_STATEMENT_PAYMENT_APPROVAL SPA
          where SPA.PAYMENT_ID = SGP.PAYMENT_ID);

--
-- Add a new column to the Accruals table to keep track of the final approver.
--
alter table PWRPLANT.PT_ACCRUAL_PENDING add FINAL_APPROVER varchar2(18);

--
-- Update new column on pt_accrual_pending.
--
update PWRPLANT.PT_ACCRUAL_PENDING
   set FINAL_APPROVER = COALESCE(AUTHORIZER10,
                                  AUTHORIZER9,
                                  AUTHORIZER8,
                                  AUTHORIZER7,
                                  AUTHORIZER6,
                                  AUTHORIZER5,
                                  AUTHORIZER4,
                                  AUTHORIZER3,
                                  AUTHORIZER2,
                                  AUTHORIZER1);
--
-- Convert existing Property Tax approvals to new workflow types.
--

-- Create a table to help with conversion of the types.
create table PWRPLANT.PT_APPROVAL_WORKFLOW_TYPE_CONV as
select APPROVAL_TYPE_ID,
       ((select nvl(max(WORKFLOW_TYPE_ID), 0) from WORKFLOW_TYPE) + ROWNUM) WORKFLOW_TYPE_ID
  from (select APPROVAL_TYPE_ID
          from PWRPLANT.APPROVAL
         where IS_PROPTAX = 1
         order by APPROVAL_TYPE_ID);

-- Create the workflow types from the approval table.
insert into PWRPLANT.WORKFLOW_TYPE
   (WORKFLOW_TYPE_ID, DESCRIPTION, SUBSYSTEM, ACTIVE, USE_LIMITS, SQL_APPROVAL_AMOUNT, BASE_SQL)
   select CONV.WORKFLOW_TYPE_ID WORKFLOW_TYPE_ID,
          APP.DESCRIPTION DESCRIPTION,
          'proptax_payment_approval,proptax_accrual_approval' SUBSYSTEM,
          1 ACTIVE,
          1 USE_LIMITS,
          'select nvl(sum(appr_amount),0) appr_amount from ' ||
          '(select nvl(sum(amount_paid),0) appr_amount from pt_statement_group_payment where payment_id = <<id_field1>> and ''<<subsystem>>'' = ''proptax_payment_approval'' ' ||
          'union all select nvl(sum(amount),0) appr_amount from pt_accrual_pending ap, company co where ap.company_id = co.company_id and ap.debit_credit_indicator = 0 and ap.accrual_status_id = 1 and co.prop_tax_company_id = <<id_field1>> and to_char( ap.month, ''mm/dd/yyyy'' ) = ''<<id_field2>>'' and ''<<subsystem>>'' = ''proptax_accrual_approval'' ) ' SQL_APPROVAL_AMOUNT,
          1 BASE_SQL
     from PWRPLANT.APPROVAL APP, PWRPLANT.PT_APPROVAL_WORKFLOW_TYPE_CONV CONV
    where APP.APPROVAL_TYPE_ID = CONV.APPROVAL_TYPE_ID;

-- Create a table to help with conversion of the rules.
create table PWRPLANT.PT_APPROVAL_WORKFLOW_RULE_CONV as
select APP.APPROVAL_TYPE_ID, APP.RULE_ORDER, APP.DESCRIPTION, APP.AUTHORITY_LIMIT
  from PWRPLANT.PT_APPROVAL_WORKFLOW_TYPE_CONV CONV,
       (select APPROVAL_TYPE_ID,
               1                RULE_ORDER,
               AUTH_LEVEL1      DESCRIPTION,
               DOLLAR_LIMIT1    AUTHORITY_LIMIT
          from PWRPLANT.APPROVAL
         where AUTH_LEVEL1 is not null
        union all
        select APPROVAL_TYPE_ID,
               2                RULE_ORDER,
               AUTH_LEVEL2      DESCRIPTION,
               DOLLAR_LIMIT2    AUTHORITY_LIMIT
          from PWRPLANT.APPROVAL
         where AUTH_LEVEL2 is not null
        union all
        select APPROVAL_TYPE_ID,
               3                RULE_ORDER,
               AUTH_LEVEL3      DESCRIPTION,
               DOLLAR_LIMIT3    AUTHORITY_LIMIT
          from PWRPLANT.APPROVAL
         where AUTH_LEVEL3 is not null
        union all
        select APPROVAL_TYPE_ID,
               4                RULE_ORDER,
               AUTH_LEVEL4      DESCRIPTION,
               DOLLAR_LIMIT4    AUTHORITY_LIMIT
          from PWRPLANT.APPROVAL
         where AUTH_LEVEL4 is not null
        union all
        select APPROVAL_TYPE_ID,
               5                RULE_ORDER,
               AUTH_LEVEL5      DESCRIPTION,
               DOLLAR_LIMIT5    AUTHORITY_LIMIT
          from PWRPLANT.APPROVAL
         where AUTH_LEVEL5 is not null
        union all
        select APPROVAL_TYPE_ID,
               6                RULE_ORDER,
               AUTH_LEVEL6      DESCRIPTION,
               DOLLAR_LIMIT6    AUTHORITY_LIMIT
          from PWRPLANT.APPROVAL
         where AUTH_LEVEL6 is not null
        union all
        select APPROVAL_TYPE_ID,
               7                RULE_ORDER,
               AUTH_LEVEL7      DESCRIPTION,
               DOLLAR_LIMIT7    AUTHORITY_LIMIT
          from PWRPLANT.APPROVAL
         where AUTH_LEVEL7 is not null
        union all
        select APPROVAL_TYPE_ID,
               8                RULE_ORDER,
               AUTH_LEVEL8      DESCRIPTION,
               DOLLAR_LIMIT8    AUTHORITY_LIMIT
          from PWRPLANT.APPROVAL
         where AUTH_LEVEL8 is not null
        union all
        select APPROVAL_TYPE_ID,
               9                RULE_ORDER,
               AUTH_LEVEL9      DESCRIPTION,
               DOLLAR_LIMIT9    AUTHORITY_LIMIT
          from PWRPLANT.APPROVAL
         where AUTH_LEVEL9 is not null
        union all
        select APPROVAL_TYPE_ID,
               10               RULE_ORDER,
               AUTH_LEVEL10     DESCRIPTION,
               DOLLAR_LIMIT10   AUTHORITY_LIMIT
          from PWRPLANT.APPROVAL
         where AUTH_LEVEL10 is not null) APP
 where CONV.APPROVAL_TYPE_ID = APP.APPROVAL_TYPE_ID;

----maint 33371.  force the conversion table to have an AUTHORITY_LIMIT of
---- 99999999999999.99 if authority limit is higher than this.
---- this will prevent a fail on the insert into workflow_rule and workflow_detail
---- because their amount fields are not a number(22,2), but a number(22,8).
update PWRPLANT.PT_APPROVAL_WORKFLOW_RULE_CONV
set AUTHORITY_LIMIT = 99999999999999.99
where AUTHORITY_LIMIT > 99999999999999.99;

-- Create the workflow rules from the approval table.
insert into PWRPLANT.WORKFLOW_RULE
   (WORKFLOW_RULE_ID, DESCRIPTION, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED, sql,
    GROUP_APPROVAL)
   select NVL(MAX_VIEW.MAX_ID, 0) + ROWNUM WORKFLOW_RULE_ID,
          RULE_CONV.DESCRIPTION DESCRIPTION,
          RULE_CONV.AUTHORITY_LIMIT AUTHORITY_LIMIT,
          null NUM_APPROVERS,
          1 NUM_REQUIRED,
          null sql,
          1 GROUP_APPROVAL
     from (select distinct RULE_CONV.DESCRIPTION, RULE_CONV.AUTHORITY_LIMIT
             from PWRPLANT.PT_APPROVAL_WORKFLOW_RULE_CONV RULE_CONV
            where not exists
            (select 'x'
                     from PWRPLANT.WORKFLOW_RULE WR
                    where WR.DESCRIPTION = RULE_CONV.DESCRIPTION
                      and NVL(WR.AUTHORITY_LIMIT, -90909) = NVL(RULE_CONV.AUTHORITY_LIMIT, -90909))
            order by DESCRIPTION) RULE_CONV,
          (select max(WORKFLOW_RULE_ID) MAX_ID from PWRPLANT.WORKFLOW_RULE) MAX_VIEW;

-- Relate the workflow types to the rules.
insert into PWRPLANT.WORKFLOW_TYPE_RULE
   (WORKFLOW_TYPE_ID, WORKFLOW_RULE_ID, RULE_ORDER)
   select TYPE_CONV.WORKFLOW_TYPE_ID WORKFLOW_TYPE_ID,
          WR.WORKFLOW_RULE_ID        WORKFLOW_RULE_ID,
          RULE_CONV.RULE_ORDER       RULE_ORDER
     from PWRPLANT.PT_APPROVAL_WORKFLOW_TYPE_CONV TYPE_CONV,
          PWRPLANT.PT_APPROVAL_WORKFLOW_RULE_CONV RULE_CONV,
          PWRPLANT.WORKFLOW_RULE                  WR
    where TYPE_CONV.APPROVAL_TYPE_ID = RULE_CONV.APPROVAL_TYPE_ID
      and RULE_CONV.DESCRIPTION = WR.DESCRIPTION
      and NVL(RULE_CONV.AUTHORITY_LIMIT, -90909) = NVL(WR.AUTHORITY_LIMIT, -90909);

-- Create a backup of approval_auth_level.
create table PWRPLANT.PT_APPROVAL_AUTH_LEVEL_BAK as
select *
  from PWRPLANT.APPROVAL_AUTH_LEVEL
 where APPROVAL_TYPE_ID in (select APPROVAL_TYPE_ID from PWRPLANT.APPROVAL where IS_PROPTAX = 1);

-- Update approval auth level to point to the new workflow types instead of approval types.
-- Set all approvers on Property Tax approvals to be default users.  This will mimic how the system worked previously.
update PWRPLANT.APPROVAL_AUTH_LEVEL AUTH
   set (AUTH.APPROVAL_TYPE_ID, AUTH.AUTH_LEVEL, AUTH.DEFAULT_USER) =
        (select CONV.WORKFLOW_TYPE_ID, WTR.WORKFLOW_RULE_ID, 1
           from PWRPLANT.PT_APPROVAL_WORKFLOW_TYPE_CONV CONV, PWRPLANT.WORKFLOW_TYPE_RULE WTR
          where CONV.WORKFLOW_TYPE_ID = WTR.WORKFLOW_TYPE_ID
            and CONV.APPROVAL_TYPE_ID = AUTH.APPROVAL_TYPE_ID
            and WTR.RULE_ORDER = AUTH.AUTH_LEVEL)
 where APPROVAL_TYPE_ID in (select APPROVAL_TYPE_ID from PWRPLANT.APPROVAL where IS_PROPTAX = 1);

--
-- Update approval_type_id on pt_statement_group, pt_company, and pt_accrual_pending.
--

-- First drop the foreign keys so that the tables no longer reference the approval table.
alter table PWRPLANT.PT_STATEMENT_GROUP drop constraint PT_STMT_GP_APPROVAL_TYPE_FK;
alter table PWRPLANT.PT_COMPANY         drop constraint PT_COMPANY_APP_FK;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop constraint PT_ACCRUAL_PENDING_APP_FK;

-- Update the tables with the new workflow_type_id.
update PWRPLANT.PT_STATEMENT_GROUP SG
   set SG.APPROVAL_TYPE_ID =
        (select CONV.WORKFLOW_TYPE_ID
           from PWRPLANT.PT_APPROVAL_WORKFLOW_TYPE_CONV CONV
          where CONV.APPROVAL_TYPE_ID = SG.APPROVAL_TYPE_ID)
 where SG.APPROVAL_TYPE_ID in (select APPROVAL_TYPE_ID from PWRPLANT.APPROVAL where IS_PROPTAX = 1);

update PWRPLANT.PT_COMPANY PTCO
   set PTCO.APPROVAL_TYPE_ID =
        (select CONV.WORKFLOW_TYPE_ID
           from PWRPLANT.PT_APPROVAL_WORKFLOW_TYPE_CONV CONV
          where CONV.APPROVAL_TYPE_ID = PTCO.APPROVAL_TYPE_ID)
 where PTCO.APPROVAL_TYPE_ID in
       (select APPROVAL_TYPE_ID from PWRPLANT.APPROVAL where IS_PROPTAX = 1);

update PWRPLANT.PT_ACCRUAL_PENDING AP
   set AP.APPROVAL_TYPE_ID =
        (select CONV.WORKFLOW_TYPE_ID
           from PWRPLANT.PT_APPROVAL_WORKFLOW_TYPE_CONV CONV
          where CONV.APPROVAL_TYPE_ID = AP.APPROVAL_TYPE_ID)
 where AP.APPROVAL_TYPE_ID in (select APPROVAL_TYPE_ID from PWRPLANT.APPROVAL where IS_PROPTAX = 1);

-- Create new foreign keys so that approval_type_id references the new workflow_type table.
alter table PWRPLANT.PT_STATEMENT_GROUP
   add constraint PT_STMT_GP_WORKFLOW_TYPE_FK
       foreign key (APPROVAL_TYPE_ID)
       references PWRPLANT.WORKFLOW_TYPE (WORKFLOW_TYPE_ID );

alter table PWRPLANT.PT_COMPANY
   add constraint PT_COMPANY_WRKFLOW_TYPE_FK
       foreign key (APPROVAL_TYPE_ID)
       references PWRPLANT.WORKFLOW_TYPE (WORKFLOW_TYPE_ID);

alter table PWRPLANT.PT_ACCRUAL_PENDING
   add constraint PT_ACCRUAL_PEND_WRKFLWTYPE_FK
       foreign key (APPROVAL_TYPE_ID)
       references PWRPLANT.WORKFLOW_TYPE (WORKFLOW_TYPE_ID);

--
-- Create workflows for existing payments and accrual journal entries.
--

-- First for payments.
insert into PWRPLANT.WORKFLOW
   (WORKFLOW_ID, WORKFLOW_TYPE_ID, WORKFLOW_TYPE_DESC, SUBSYSTEM, ID_FIELD1, APPROVAL_AMOUNT,
    APPROVAL_STATUS_ID, DATE_SENT, DATE_APPROVED, USE_LIMITS, SQL_APPROVAL_AMOUNT)
   select NVL(MAX_VIEW.MAX_ID, 0) + ROWNUM WORKFLOW_ID,
          SG.APPROVAL_TYPE_ID WORKFLOW_TYPE_ID,
          WT.DESCRIPTION WORKFLOW_TYPE_DESC,
          'proptax_payment_approval' SUBSYSTEM,
          TO_CHAR(SGP.PAYMENT_ID) ID_FIELD1,
          NVL(SGP.AMOUNT_PAID, 0) APPROVAL_AMOUNT,
          DECODE(SGP.PAYMENT_STATUS_ID, 1, 2, 2, 3, 3, 3, 4, 3, 5, 4, 6, 5, 1) APPROVAL_STATUS_ID,
          SGP.PAYMENT_ENTRY_DATE DATE_SENT,
          NVL(SGP.PAYMENT_REJECTED_DATE, SGP.PAYMENT_APPROVAL_DATE) DATE_APPROVED,
          1 USE_LIMITS,
          'select nvl(sum(appr_amount),0) appr_amount from ' ||
          '(select nvl(sum(amount_paid),0) appr_amount from pt_statement_group_payment where payment_id = <<id_field1>> and ''<<subsystem>>'' = ''proptax_payment_approval'' ' ||
          'union all select sum(nvl(amount, 0)) appr_amount from pt_accrual_pending ap, company co where ap.company_id = co.company_id and ap.debit_credit_indicator = 0 and ap.accrual_status_id = 1 and co.prop_tax_company_id = <<id_field1>> and to_char( ap.month, ''mm/dd/yyyy'' ) = ''<<id_field2>>'' ) ' SQL_APPROVAL_AMOUNT
     from PWRPLANT.PT_STATEMENT_GROUP_PAYMENT SGP,
          PWRPLANT.PT_STATEMENT_GROUP SG,
          PWRPLANT.WORKFLOW_TYPE WT,
          (select max(WORKFLOW_ID) MAX_ID from PWRPLANT.WORKFLOW) MAX_VIEW
    where SGP.STATEMENT_GROUP_ID = SG.STATEMENT_GROUP_ID
      and SG.APPROVAL_TYPE_ID = WT.WORKFLOW_TYPE_ID
      and not exists (select 'x'
             from PWRPLANT.WORKFLOW W
            where W.ID_FIELD1 = TO_CHAR(SGP.PAYMENT_ID)
              and W.SUBSYSTEM = 'proptax_payment_approval');

-- BGB - 04/29/2016 - Need to grab the max() of the DATE_SENT and DATE_APPROVED for the subquery below,
--                      as there is the possibility for there to be more than 1 timestamp and if so it'll break
--                      the WORKFLOW unique index.
--                      Additionally, we'll need to break this up according approved/pending first, then rejected,
--                      in case there are any rejected entries on the same company/month as an approved/pending entry.
-- Now for journal entries.
--insert into PWRPLANT.WORKFLOW
--   (WORKFLOW_ID, WORKFLOW_TYPE_ID, WORKFLOW_TYPE_DESC, SUBSYSTEM, ID_FIELD1, ID_FIELD2,
--    APPROVAL_AMOUNT, APPROVAL_STATUS_ID, DATE_SENT, DATE_APPROVED, USE_LIMITS, SQL_APPROVAL_AMOUNT)
--   select NVL(MAX_VIEW.MAX_ID, 0) + ROWNUM WORKFLOW_ID,
--          AP_VIEW.APPROVAL_TYPE_ID WORKFLOW_TYPE_ID,
--          WT.DESCRIPTION WORKFLOW_TYPE_DESC,
--          'proptax_accrual_approval' SUBSYSTEM,
--          TO_CHAR(AP_VIEW.PROP_TAX_COMPANY_ID) ID_FIELD1,
--          TO_CHAR(AP_VIEW.MONTH, 'mm/dd/yyyy') ID_FIELD2,
--          NVL(AP_VIEW.AMOUNT, 0) APPROVAL_AMOUNT,
--          AP_VIEW.APPROVAL_STATUS_ID APPROVAL_STATUS_ID,
--          AP_VIEW.DATE_SENT DATE_SENT,
--          AP_VIEW.DATE_APPROVED DATE_APPROVED,
--          1 USE_LIMITS,
--          'select nvl(sum(appr_amount),0) appr_amount from ' ||
--          '(select nvl(sum(amount_paid),0) appr_amount from pt_statement_group_payment where payment_id = <<id_field1>> and ''<<subsystem>>'' = ''proptax_payment_approval'' ' ||
--          'union all select nvl(sum(amount),0) appr_amount from pt_accrual_pending ap, company co where ap.company_id = co.company_id and ap.debit_credit_indicator = 0 and ap.accrual_status_id = 1 and co.prop_tax_company_id = <<id_field1>> and to_char( ap.month, ''mm/dd/yyyy'' ) = ''<<id_field2>>'' and ''<<subsystem>>'' = ''proptax_accrual_approval'' ) ' SQL_APPROVAL_AMOUNT
--     from (select AP.APPROVAL_TYPE_ID APPROVAL_TYPE_ID,
--                  CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
--                  AP.MONTH month,
--                  DECODE(AP.ACCRUAL_STATUS_ID, 1, 2, 2, 3, 3, 4, 1) APPROVAL_STATUS_ID,
--                  COALESCE(AP.REJECTED_DATE,
--                           AP.AUTHORIZED_DATE10,
--                           AP.AUTHORIZED_DATE9,
--                           AP.AUTHORIZED_DATE8,
--                           AP.AUTHORIZED_DATE7,
--                           AP.AUTHORIZED_DATE6,
--                           AP.AUTHORIZED_DATE5,
--                           AP.AUTHORIZED_DATE4,
--                           AP.AUTHORIZED_DATE3,
--                           AP.AUTHORIZED_DATE2,
--                           AP.AUTHORIZED_DATE1) DATE_SENT,
--                  NVL(AP.REJECTED_DATE, AP.FINAL_APPROVAL_DATE) DATE_APPROVED,
--                  sum(AP.AMOUNT) AMOUNT
--             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
--            where AP.COMPANY_ID = CO.COMPANY_ID
--            group by AP.APPROVAL_TYPE_ID,
--                     CO.PROP_TAX_COMPANY_ID,
--                     AP.MONTH,
--                     DECODE(AP.ACCRUAL_STATUS_ID, 1, 2, 2, 3, 3, 4, 1),
--                     COALESCE(AP.REJECTED_DATE,
--                              AP.AUTHORIZED_DATE10,
--                              AP.AUTHORIZED_DATE9,
--                              AP.AUTHORIZED_DATE8,
--                              AP.AUTHORIZED_DATE7,
--                              AP.AUTHORIZED_DATE6,
--                              AP.AUTHORIZED_DATE5,
--                              AP.AUTHORIZED_DATE4,
--                              AP.AUTHORIZED_DATE3,
--                              AP.AUTHORIZED_DATE2,
--                              AP.AUTHORIZED_DATE1),
--                     NVL(AP.REJECTED_DATE, AP.FINAL_APPROVAL_DATE)) AP_VIEW,
--          PWRPLANT.WORKFLOW_TYPE WT,
--          (select max(WORKFLOW_ID) MAX_ID from PWRPLANT.WORKFLOW) MAX_VIEW
--    where AP_VIEW.APPROVAL_TYPE_ID = WT.WORKFLOW_TYPE_ID
--      and not exists (select 'x'
--             from PWRPLANT.WORKFLOW W
--            where W.ID_FIELD1 = TO_CHAR(AP_VIEW.PROP_TAX_COMPANY_ID)
--              and W.ID_FIELD2 = TO_CHAR(AP_VIEW.MONTH, 'mm/dd/yyyy')
--              and W.SUBSYSTEM = 'proptax_accrual_approval');

-- Now for journal entries (pending and approved).
insert into PWRPLANT.WORKFLOW
   (WORKFLOW_ID, WORKFLOW_TYPE_ID, WORKFLOW_TYPE_DESC, SUBSYSTEM, ID_FIELD1, ID_FIELD2,
    APPROVAL_AMOUNT, APPROVAL_STATUS_ID, DATE_SENT, DATE_APPROVED, USE_LIMITS, SQL_APPROVAL_AMOUNT)
   select NVL(MAX_VIEW.MAX_ID, 0) + ROWNUM WORKFLOW_ID,
          AP_VIEW.APPROVAL_TYPE_ID WORKFLOW_TYPE_ID,
          WT.DESCRIPTION WORKFLOW_TYPE_DESC,
          'proptax_accrual_approval' SUBSYSTEM,
          TO_CHAR(AP_VIEW.PROP_TAX_COMPANY_ID) ID_FIELD1,
          TO_CHAR(AP_VIEW.MONTH, 'mm/dd/yyyy') ID_FIELD2,
          NVL(AP_VIEW.AMOUNT, 0) APPROVAL_AMOUNT,
          AP_VIEW.APPROVAL_STATUS_ID APPROVAL_STATUS_ID,
          AP_VIEW.DATE_SENT DATE_SENT,
          AP_VIEW.DATE_APPROVED DATE_APPROVED,
          1 USE_LIMITS,
          'select nvl(sum(appr_amount),0) appr_amount from ' ||
          '(select nvl(sum(amount_paid),0) appr_amount from pt_statement_group_payment where payment_id = <<id_field1>> and ''<<subsystem>>'' = ''proptax_payment_approval'' ' ||
          'union all select nvl(sum(amount),0) appr_amount from pt_accrual_pending ap, company co where ap.company_id = co.company_id and ap.debit_credit_indicator = 0 and ap.accrual_status_id = 1 and co.prop_tax_company_id = <<id_field1>> and to_char( ap.month, ''mm/dd/yyyy'' ) = ''<<id_field2>>'' and ''<<subsystem>>'' = ''proptax_accrual_approval'' ) ' SQL_APPROVAL_AMOUNT
     from (select AP.APPROVAL_TYPE_ID APPROVAL_TYPE_ID,
                  CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
                  AP.MONTH month,
                  DECODE(AP.ACCRUAL_STATUS_ID, 1, 2, 2, 3, 3, 4, 1) APPROVAL_STATUS_ID,
                  MAX(COALESCE(AP.REJECTED_DATE,
                           AP.AUTHORIZED_DATE10,
                           AP.AUTHORIZED_DATE9,
                           AP.AUTHORIZED_DATE8,
                           AP.AUTHORIZED_DATE7,
                           AP.AUTHORIZED_DATE6,
                           AP.AUTHORIZED_DATE5,
                           AP.AUTHORIZED_DATE4,
                           AP.AUTHORIZED_DATE3,
                           AP.AUTHORIZED_DATE2,
                           AP.AUTHORIZED_DATE1)) DATE_SENT,
                  MAX(NVL(AP.REJECTED_DATE, AP.FINAL_APPROVAL_DATE)) DATE_APPROVED,
                  sum(AP.AMOUNT) AMOUNT
             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.ACCRUAL_STATUS_ID IN (1,2) --Pending and Approved only
            group by AP.APPROVAL_TYPE_ID,
                     CO.PROP_TAX_COMPANY_ID,
                     AP.MONTH,
                     DECODE(AP.ACCRUAL_STATUS_ID, 1, 2, 2, 3, 3, 4, 1)
          ) AP_VIEW,
          PWRPLANT.WORKFLOW_TYPE WT,
          (select max(WORKFLOW_ID) MAX_ID from PWRPLANT.WORKFLOW) MAX_VIEW
    where AP_VIEW.APPROVAL_TYPE_ID = WT.WORKFLOW_TYPE_ID
      and not exists (select 'x'
             from PWRPLANT.WORKFLOW W
            where W.ID_FIELD1 = TO_CHAR(AP_VIEW.PROP_TAX_COMPANY_ID)
              and W.ID_FIELD2 = TO_CHAR(AP_VIEW.MONTH, 'mm/dd/yyyy')
              and W.SUBSYSTEM = 'proptax_accrual_approval');

-- Now for journal entries (rejected).
insert into PWRPLANT.WORKFLOW
   (WORKFLOW_ID, WORKFLOW_TYPE_ID, WORKFLOW_TYPE_DESC, SUBSYSTEM, ID_FIELD1, ID_FIELD2,
    APPROVAL_AMOUNT, APPROVAL_STATUS_ID, DATE_SENT, DATE_APPROVED, USE_LIMITS, SQL_APPROVAL_AMOUNT)
   select NVL(MAX_VIEW.MAX_ID, 0) + ROWNUM WORKFLOW_ID,
          AP_VIEW.APPROVAL_TYPE_ID WORKFLOW_TYPE_ID,
          WT.DESCRIPTION WORKFLOW_TYPE_DESC,
          'proptax_accrual_approval' SUBSYSTEM,
          TO_CHAR(AP_VIEW.PROP_TAX_COMPANY_ID) ID_FIELD1,
          TO_CHAR(AP_VIEW.MONTH, 'mm/dd/yyyy') ID_FIELD2,
          NVL(AP_VIEW.AMOUNT, 0) APPROVAL_AMOUNT,
          AP_VIEW.APPROVAL_STATUS_ID APPROVAL_STATUS_ID,
          AP_VIEW.DATE_SENT DATE_SENT,
          AP_VIEW.DATE_APPROVED DATE_APPROVED,
          1 USE_LIMITS,
          'select nvl(sum(appr_amount),0) appr_amount from ' ||
          '(select nvl(sum(amount_paid),0) appr_amount from pt_statement_group_payment where payment_id = <<id_field1>> and ''<<subsystem>>'' = ''proptax_payment_approval'' ' ||
          'union all select nvl(sum(amount),0) appr_amount from pt_accrual_pending ap, company co where ap.company_id = co.company_id and ap.debit_credit_indicator = 0 and ap.accrual_status_id = 1 and co.prop_tax_company_id = <<id_field1>> and to_char( ap.month, ''mm/dd/yyyy'' ) = ''<<id_field2>>'' and ''<<subsystem>>'' = ''proptax_accrual_approval'' ) ' SQL_APPROVAL_AMOUNT
     from (select AP.APPROVAL_TYPE_ID APPROVAL_TYPE_ID,
                  CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
                  AP.MONTH month,
                  DECODE(AP.ACCRUAL_STATUS_ID, 1, 2, 2, 3, 3, 4, 1) APPROVAL_STATUS_ID,
                  MAX(COALESCE(AP.REJECTED_DATE,
                           AP.AUTHORIZED_DATE10,
                           AP.AUTHORIZED_DATE9,
                           AP.AUTHORIZED_DATE8,
                           AP.AUTHORIZED_DATE7,
                           AP.AUTHORIZED_DATE6,
                           AP.AUTHORIZED_DATE5,
                           AP.AUTHORIZED_DATE4,
                           AP.AUTHORIZED_DATE3,
                           AP.AUTHORIZED_DATE2,
                           AP.AUTHORIZED_DATE1)) DATE_SENT,
                  MAX(NVL(AP.REJECTED_DATE, AP.FINAL_APPROVAL_DATE)) DATE_APPROVED,
                  sum(AP.AMOUNT) AMOUNT
             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.ACCRUAL_STATUS_ID = 3 --Rejected
            group by AP.APPROVAL_TYPE_ID,
                     CO.PROP_TAX_COMPANY_ID,
                     AP.MONTH,
                     DECODE(AP.ACCRUAL_STATUS_ID, 1, 2, 2, 3, 3, 4, 1)
          ) AP_VIEW,
          PWRPLANT.WORKFLOW_TYPE WT,
          (select max(WORKFLOW_ID) MAX_ID from PWRPLANT.WORKFLOW) MAX_VIEW
    where AP_VIEW.APPROVAL_TYPE_ID = WT.WORKFLOW_TYPE_ID
      and not exists (select 'x'
             from PWRPLANT.WORKFLOW W
            where W.ID_FIELD1 = TO_CHAR(AP_VIEW.PROP_TAX_COMPANY_ID)
              and W.ID_FIELD2 = TO_CHAR(AP_VIEW.MONTH, 'mm/dd/yyyy')
              and W.SUBSYSTEM = 'proptax_accrual_approval');

--
-- Create workflow detail records for existing approvers/approval dates and rejectors/rejected dates from pt_statement_payment_approval and pt_accrual_pending.
-- Set approval_status_id to null for now - we'll update later.
--

-- First for payments (approvers).
insert into PWRPLANT.WORKFLOW_DETAIL
   (WORKFLOW_ID, ID, WORKFLOW_RULE_ID, WORKFLOW_RULE_DESC, RULE_ORDER, USERS, DATE_APPROVED,
    USER_APPROVED, APPROVAL_STATUS_ID, REQUIRED, NOTES, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED,
    sql, ALLOW_EDIT, ORIG_AUTHORITY_LIMIT, IS_CUSTOM_LIMIT, GROUP_APPROVAL)
   select W.WORKFLOW_ID WORKFLOW_ID,
          NVL(MAX_VIEW.MAX_ID, 0) + RANK() OVER(partition by W.WORKFLOW_ID order by WTR.RULE_ORDER, SPA_VIEW.AUTHORIZER) ID,
          WR.WORKFLOW_RULE_ID WORKFLOW_RULE_ID,
          WR.DESCRIPTION WORKFLOW_RULE_DESC,
          WTR.RULE_ORDER RULE_ORDER,
          SPA_VIEW.AUTHORIZER USERS,
          SPA_VIEW.AUTHORIZED_DATE DATE_APPROVED,
          DECODE(SPA_VIEW.AUTHORIZED_DATE, null, null, SPA_VIEW.AUTHORIZER) USER_APPROVED,
          null APPROVAL_STATUS_ID,
          1 REQUIRED,
          null NOTES,
          WR.AUTHORITY_LIMIT AUTHORITY_LIMIT,
          null NUM_APPROVERS,
          1 NUM_REQUIRED,
          null sql,
          1 ALLOW_EDIT,
          WR.AUTHORITY_LIMIT ORIG_AUTHORITY_LIMIT,
          0 CUSTOM_LIMIT,
          1 GROUP_APPROVAL
     from PWRPLANT.WORKFLOW W,
          PWRPLANT.WORKFLOW_RULE WR,
          PWRPLANT.WORKFLOW_TYPE_RULE WTR,
          PWRPLANT.WORKFLOW_TYPE WT,
          (select PAYMENT_ID       PAYMENT_ID,
                  1                RULE_ORDER,
                  AUTHORIZER1      AUTHORIZER,
                  AUTHORIZED_DATE1 AUTHORIZED_DATE
             from PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL
            where AUTHORIZER1 is not null
              and AUTHORIZED_DATE1 is not null
           union
           select PAYMENT_ID       PAYMENT_ID,
                  2                RULE_ORDER,
                  AUTHORIZER2      AUTHORIZER,
                  AUTHORIZED_DATE2 AUTHORIZED_DATE
             from PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL
            where AUTHORIZER2 is not null
              and AUTHORIZED_DATE2 is not null
           union
           select PAYMENT_ID       PAYMENT_ID,
                  3                RULE_ORDER,
                  AUTHORIZER3      AUTHORIZER,
                  AUTHORIZED_DATE3 AUTHORIZED_DATE
             from PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL
            where AUTHORIZER3 is not null
              and AUTHORIZED_DATE3 is not null
           union
           select PAYMENT_ID       PAYMENT_ID,
                  4                RULE_ORDER,
                  AUTHORIZER4      AUTHORIZER,
                  AUTHORIZED_DATE4 AUTHORIZED_DATE
             from PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL
            where AUTHORIZER4 is not null
              and AUTHORIZED_DATE4 is not null
           union
           select PAYMENT_ID       PAYMENT_ID,
                  5                RULE_ORDER,
                  AUTHORIZER5      AUTHORIZER,
                  AUTHORIZED_DATE5 AUTHORIZED_DATE
             from PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL
            where AUTHORIZER5 is not null
              and AUTHORIZED_DATE5 is not null
           union
           select PAYMENT_ID       PAYMENT_ID,
                  6                RULE_ORDER,
                  AUTHORIZER6      AUTHORIZER,
                  AUTHORIZED_DATE6 AUTHORIZED_DATE
             from PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL
            where AUTHORIZER6 is not null
              and AUTHORIZED_DATE6 is not null
           union
           select PAYMENT_ID       PAYMENT_ID,
                  7                RULE_ORDER,
                  AUTHORIZER7      AUTHORIZER,
                  AUTHORIZED_DATE7 AUTHORIZED_DATE
             from PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL
            where AUTHORIZER7 is not null
              and AUTHORIZED_DATE7 is not null
           union
           select PAYMENT_ID       PAYMENT_ID,
                  8                RULE_ORDER,
                  AUTHORIZER8      AUTHORIZER,
                  AUTHORIZED_DATE8 AUTHORIZED_DATE
             from PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL
            where AUTHORIZER8 is not null
              and AUTHORIZED_DATE8 is not null
           union
           select PAYMENT_ID       PAYMENT_ID,
                  9                RULE_ORDER,
                  AUTHORIZER9      AUTHORIZER,
                  AUTHORIZED_DATE9 AUTHORIZED_DATE
             from PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL
            where AUTHORIZER9 is not null
              and AUTHORIZED_DATE9 is not null
           union
           select PAYMENT_ID        PAYMENT_ID,
                  10                RULE_ORDER,
                  AUTHORIZER10      AUTHORIZER,
                  AUTHORIZED_DATE10 AUTHORIZED_DATE
             from PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL
            where AUTHORIZER10 is not null
              and AUTHORIZED_DATE10 is not null) SPA_VIEW,
          (select WORKFLOW_ID WORKFLOW_ID, max(ID) MAX_ID
             from PWRPLANT.WORKFLOW_DETAIL
            group by WORKFLOW_ID) MAX_VIEW
    where WT.SUBSYSTEM like '%proptax_payment_approval%'
      and WT.WORKFLOW_TYPE_ID = WTR.WORKFLOW_TYPE_ID
      and WTR.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
      and WT.WORKFLOW_TYPE_ID = W.WORKFLOW_TYPE_ID
      and W.SUBSYSTEM = 'proptax_payment_approval'
      and W.ID_FIELD1 = TO_CHAR(SPA_VIEW.PAYMENT_ID)
      and WTR.RULE_ORDER = SPA_VIEW.RULE_ORDER
      and W.WORKFLOW_ID = MAX_VIEW.WORKFLOW_ID(+);

-- Now for payments (rejectors).
insert into PWRPLANT.WORKFLOW_DETAIL
   (WORKFLOW_ID, ID, WORKFLOW_RULE_ID, WORKFLOW_RULE_DESC, RULE_ORDER, USERS, DATE_APPROVED,
    USER_APPROVED, APPROVAL_STATUS_ID, REQUIRED, NOTES, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED,
    sql, ALLOW_EDIT, ORIG_AUTHORITY_LIMIT, IS_CUSTOM_LIMIT, GROUP_APPROVAL)
   select W.WORKFLOW_ID WORKFLOW_ID,
          NVL(MAX_VIEW.MAX_ID, 0) + RANK() OVER(partition by W.WORKFLOW_ID order by WTR.RULE_ORDER, SPA_VIEW.AUTHORIZER) ID,
          WR.WORKFLOW_RULE_ID WORKFLOW_RULE_ID,
          WR.DESCRIPTION WORKFLOW_RULE_DESC,
          WTR.RULE_ORDER RULE_ORDER,
          SPA_VIEW.AUTHORIZER USERS,
          SPA_VIEW.AUTHORIZED_DATE DATE_APPROVED,
          DECODE(SPA_VIEW.AUTHORIZED_DATE, null, null, SPA_VIEW.AUTHORIZER) USER_APPROVED,
          null APPROVAL_STATUS_ID,
          1 REQUIRED,
          null NOTES,
          WR.AUTHORITY_LIMIT AUTHORITY_LIMIT,
          null NUM_APPROVERS,
          1 NUM_REQUIRED,
          null sql,
          1 ALLOW_EDIT,
          WR.AUTHORITY_LIMIT ORIG_AUTHORITY_LIMIT,
          0 CUSTOM_LIMIT,
          1 GROUP_APPROVAL
     from PWRPLANT.WORKFLOW W,
          PWRPLANT.WORKFLOW_RULE WR,
          PWRPLANT.WORKFLOW_TYPE_RULE WTR,
          PWRPLANT.WORKFLOW_TYPE WT,
          (select PAYMENT_ID PAYMENT_ID,
                  DECODE(AUTHORIZED_DATE10,
                         null,
                         DECODE(AUTHORIZED_DATE9,
                                null,
                                DECODE(AUTHORIZED_DATE8,
                                       null,
                                       DECODE(AUTHORIZED_DATE7,
                                              null,
                                              DECODE(AUTHORIZED_DATE6,
                                                     null,
                                                     DECODE(AUTHORIZED_DATE5,
                                                            null,
                                                            DECODE(AUTHORIZED_DATE4,
                                                                   null,
                                                                   DECODE(AUTHORIZED_DATE3,
                                                                          null,
                                                                          DECODE(AUTHORIZED_DATE2,
                                                                                 null,
                                                                                 DECODE(AUTHORIZED_DATE1,
                                                                                        null,
                                                                                        1,
                                                                                        2),
                                                                                 3),
                                                                          4),
                                                                   5),
                                                            6),
                                                     7),
                                              8),
                                       9),
                                10),
                         11) RULE_ORDER,
                  REJECTOR AUTHORIZER,
                  REJECTED_DATE AUTHORIZED_DATE
             from PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL
            where REJECTOR is not null
              and REJECTED_DATE is not null) SPA_VIEW,
          (select WORKFLOW_ID WORKFLOW_ID, max(ID) MAX_ID
             from PWRPLANT.WORKFLOW_DETAIL
            group by WORKFLOW_ID) MAX_VIEW
    where WT.SUBSYSTEM like '%proptax_payment_approval%'
      and WT.WORKFLOW_TYPE_ID = WTR.WORKFLOW_TYPE_ID
      and WTR.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
      and WT.WORKFLOW_TYPE_ID = W.WORKFLOW_TYPE_ID
      and W.SUBSYSTEM = 'proptax_payment_approval'
      and W.ID_FIELD1 = TO_CHAR(SPA_VIEW.PAYMENT_ID)
      and WTR.RULE_ORDER = SPA_VIEW.RULE_ORDER
      and W.WORKFLOW_ID = MAX_VIEW.WORKFLOW_ID(+);

-- Now for journal entries (approvers).
-- BGB - 04/29/2016 - As above, we need to take the max approval timestamps as it's possible to have multiple for a
--                      given company/month.
--insert into PWRPLANT.WORKFLOW_DETAIL
--   (WORKFLOW_ID, ID, WORKFLOW_RULE_ID, WORKFLOW_RULE_DESC, RULE_ORDER, USERS, DATE_APPROVED,
--    USER_APPROVED, APPROVAL_STATUS_ID, REQUIRED, NOTES, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED,
--    sql, ALLOW_EDIT, ORIG_AUTHORITY_LIMIT, IS_CUSTOM_LIMIT, GROUP_APPROVAL)
--   select W.WORKFLOW_ID WORKFLOW_ID,
--          NVL(MAX_VIEW.MAX_ID, 0) + RANK() OVER(partition by W.WORKFLOW_ID order by WTR.RULE_ORDER, AP_VIEW.AUTHORIZER) ID,
--          WR.WORKFLOW_RULE_ID WORKFLOW_RULE_ID,
--          WR.DESCRIPTION WORKFLOW_RULE_DESC,
--          WTR.RULE_ORDER RULE_ORDER,
--          AP_VIEW.AUTHORIZER USERS,
--          AP_VIEW.AUTHORIZED_DATE DATE_APPROVED,
--          DECODE(AP_VIEW.AUTHORIZED_DATE, null, null, AP_VIEW.AUTHORIZER) USER_APPROVED,
--          null APPROVAL_STATUS_ID,
--          1 REQUIRED,
--          null NOTES,
--          WR.AUTHORITY_LIMIT AUTHORITY_LIMIT,
--          null NUM_APPROVERS,
--          1 NUM_REQUIRED,
--          null sql,
--          1 ALLOW_EDIT,
--          WR.AUTHORITY_LIMIT ORIG_AUTHORITY_LIMIT,
--          0 CUSTOM_LIMIT,
--          1 GROUP_APPROVAL
--     from PWRPLANT.WORKFLOW W,
--          PWRPLANT.WORKFLOW_RULE WR,
--          PWRPLANT.WORKFLOW_TYPE_RULE WTR,
--          PWRPLANT.WORKFLOW_TYPE WT,
--          (select distinct CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
--                           AP.MONTH               month,
--                           1                      RULE_ORDER,
--                           AP.AUTHORIZER1         AUTHORIZER,
--                           AP.AUTHORIZED_DATE1    AUTHORIZED_DATE
--             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
--            where AP.COMPANY_ID = CO.COMPANY_ID
--              and AP.AUTHORIZER1 is not null
--              and AP.AUTHORIZED_DATE1 is not null
--           union
--           select distinct CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
--                           AP.MONTH               month,
--                           2                      RULE_ORDER,
--                           AP.AUTHORIZER2         AUTHORIZER,
--                           AP.AUTHORIZED_DATE2    AUTHORIZED_DATE
--             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
--            where AP.COMPANY_ID = CO.COMPANY_ID
--              and AP.AUTHORIZER2 is not null
--              and AP.AUTHORIZED_DATE2 is not null
--           union
--           select distinct CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
--                           AP.MONTH               month,
--                           3                      RULE_ORDER,
--                           AP.AUTHORIZER3         AUTHORIZER,
--                           AP.AUTHORIZED_DATE3    AUTHORIZED_DATE
--             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
--            where AP.COMPANY_ID = CO.COMPANY_ID
--              and AP.AUTHORIZER3 is not null
--              and AP.AUTHORIZED_DATE3 is not null
--           union
--           select distinct CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
--                           AP.MONTH               month,
--                           4                      RULE_ORDER,
--                           AP.AUTHORIZER4         AUTHORIZER,
--                           AP.AUTHORIZED_DATE4    AUTHORIZED_DATE
--             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
--            where AP.COMPANY_ID = CO.COMPANY_ID
--              and AP.AUTHORIZER4 is not null
--              and AP.AUTHORIZED_DATE4 is not null
--           union
--           select distinct CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
--                           AP.MONTH               month,
--                           5                      RULE_ORDER,
--                           AP.AUTHORIZER5         AUTHORIZER,
--                           AP.AUTHORIZED_DATE5    AUTHORIZED_DATE
--             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
--            where AP.COMPANY_ID = CO.COMPANY_ID
--              and AP.AUTHORIZER5 is not null
--              and AP.AUTHORIZED_DATE5 is not null
--           union
--           select distinct CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
--                           AP.MONTH               month,
--                           6                      RULE_ORDER,
--                           AP.AUTHORIZER6         AUTHORIZER,
--                           AP.AUTHORIZED_DATE6    AUTHORIZED_DATE
--             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
--            where AP.COMPANY_ID = CO.COMPANY_ID
--              and AP.AUTHORIZER6 is not null
--              and AP.AUTHORIZED_DATE6 is not null
--           union
--           select distinct CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
--                           AP.MONTH               month,
--                           7                      RULE_ORDER,
--                           AP.AUTHORIZER7         AUTHORIZER,
--                           AP.AUTHORIZED_DATE7    AUTHORIZED_DATE
--             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
--            where AP.COMPANY_ID = CO.COMPANY_ID
--              and AP.AUTHORIZER7 is not null
--              and AP.AUTHORIZED_DATE7 is not null
--           union
--           select distinct CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
--                           AP.MONTH               month,
--                           8                      RULE_ORDER,
--                           AP.AUTHORIZER8         AUTHORIZER,
--                           AP.AUTHORIZED_DATE8    AUTHORIZED_DATE
--             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
--            where AP.COMPANY_ID = CO.COMPANY_ID
--              and AP.AUTHORIZER8 is not null
--              and AP.AUTHORIZED_DATE8 is not null
--           union
--           select distinct CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
--                           AP.MONTH               month,
--                           9                      RULE_ORDER,
--                           AP.AUTHORIZER9         AUTHORIZER,
--                           AP.AUTHORIZED_DATE9    AUTHORIZED_DATE
--             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
--            where AP.COMPANY_ID = CO.COMPANY_ID
--              and AP.AUTHORIZER9 is not null
--              and AP.AUTHORIZED_DATE9 is not null
--           union
--           select distinct CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
--                           AP.MONTH               month,
--                           10                     RULE_ORDER,
--                           AP.AUTHORIZER10        AUTHORIZER,
--                           AP.AUTHORIZED_DATE10   AUTHORIZED_DATE
--             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
--            where AP.COMPANY_ID = CO.COMPANY_ID
--              and AP.AUTHORIZER10 is not null
--              and AP.AUTHORIZED_DATE10 is not null) AP_VIEW,
--          (select WORKFLOW_ID WORKFLOW_ID, max(ID) MAX_ID
--             from PWRPLANT.WORKFLOW_DETAIL
--            group by WORKFLOW_ID) MAX_VIEW
--    where WT.SUBSYSTEM like '%proptax_accrual_approval%'
--      and WT.WORKFLOW_TYPE_ID = WTR.WORKFLOW_TYPE_ID
--      and WTR.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
--      and WT.WORKFLOW_TYPE_ID = W.WORKFLOW_TYPE_ID
--      and W.SUBSYSTEM = 'proptax_accrual_approval'
--      and W.ID_FIELD1 = TO_CHAR(AP_VIEW.PROP_TAX_COMPANY_ID)
--      and W.ID_FIELD2 = TO_CHAR(AP_VIEW.MONTH, 'mm/dd/yyyy')
--      and WTR.RULE_ORDER = AP_VIEW.RULE_ORDER
--      and W.WORKFLOW_ID = MAX_VIEW.WORKFLOW_ID(+);


insert into PWRPLANT.WORKFLOW_DETAIL
   (WORKFLOW_ID, ID, WORKFLOW_RULE_ID, WORKFLOW_RULE_DESC, RULE_ORDER, USERS, DATE_APPROVED,
    USER_APPROVED, APPROVAL_STATUS_ID, REQUIRED, NOTES, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED,
    sql, ALLOW_EDIT, ORIG_AUTHORITY_LIMIT, IS_CUSTOM_LIMIT, GROUP_APPROVAL)
   select W.WORKFLOW_ID WORKFLOW_ID,
          NVL(MAX_VIEW.MAX_ID, 0) + RANK() OVER(partition by W.WORKFLOW_ID order by WTR.RULE_ORDER, AP_VIEW.AUTHORIZER) ID,
          WR.WORKFLOW_RULE_ID WORKFLOW_RULE_ID,
          WR.DESCRIPTION WORKFLOW_RULE_DESC,
          WTR.RULE_ORDER RULE_ORDER,
          AP_VIEW.AUTHORIZER USERS,
          AP_VIEW.AUTHORIZED_DATE DATE_APPROVED,
          DECODE(AP_VIEW.AUTHORIZED_DATE, null, null, AP_VIEW.AUTHORIZER) USER_APPROVED,
          null APPROVAL_STATUS_ID,
          1 REQUIRED,
          null NOTES,
          WR.AUTHORITY_LIMIT AUTHORITY_LIMIT,
          null NUM_APPROVERS,
          1 NUM_REQUIRED,
          null sql,
          1 ALLOW_EDIT,
          WR.AUTHORITY_LIMIT ORIG_AUTHORITY_LIMIT,
          0 CUSTOM_LIMIT,
          1 GROUP_APPROVAL
     from PWRPLANT.WORKFLOW W,
          PWRPLANT.WORKFLOW_RULE WR,
          PWRPLANT.WORKFLOW_TYPE_RULE WTR,
          PWRPLANT.WORKFLOW_TYPE WT,
          (select CO.PROP_TAX_COMPANY_ID    PROP_TAX_COMPANY_ID,
                  AP.MONTH                  month,
                  1                         RULE_ORDER,
                  AP.AUTHORIZER1            AUTHORIZER,
                  MAX(AP.AUTHORIZED_DATE1)  AUTHORIZED_DATE
             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.AUTHORIZER1 is not null
              and AP.AUTHORIZED_DATE1 is not null
            group by CO.PROP_TAX_COMPANY_ID, AP.MONTH, AP.AUTHORIZER1
           union
           select CO.PROP_TAX_COMPANY_ID    PROP_TAX_COMPANY_ID,
                  AP.MONTH                  month,
                  2                         RULE_ORDER,
                  AP.AUTHORIZER2            AUTHORIZER,
                  max(AP.AUTHORIZED_DATE2)  AUTHORIZED_DATE
             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.AUTHORIZER2 is not null
              and AP.AUTHORIZED_DATE2 is not null
            group by CO.PROP_TAX_COMPANY_ID, AP.MONTH, AP.AUTHORIZER2
           union
           select CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
                  AP.MONTH                  month,
                  3                         RULE_ORDER,
                  AP.AUTHORIZER3            AUTHORIZER,
                  max(AP.AUTHORIZED_DATE3)  AUTHORIZED_DATE
             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.AUTHORIZER3 is not null
              and AP.AUTHORIZED_DATE3 is not null
            group by CO.PROP_TAX_COMPANY_ID, AP.MONTH, AP.AUTHORIZER3
           union
           select CO.PROP_TAX_COMPANY_ID    PROP_TAX_COMPANY_ID,
                  AP.MONTH                  month,
                  4                         RULE_ORDER,
                  AP.AUTHORIZER4            AUTHORIZER,
                  max(AP.AUTHORIZED_DATE4)  AUTHORIZED_DATE
             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.AUTHORIZER4 is not null
              and AP.AUTHORIZED_DATE4 is not null
            group by CO.PROP_TAX_COMPANY_ID, AP.MONTH, AP.AUTHORIZER4
           union
           select CO.PROP_TAX_COMPANY_ID    PROP_TAX_COMPANY_ID,
                  AP.MONTH                  month,
                  5                         RULE_ORDER,
                  AP.AUTHORIZER5            AUTHORIZER,
                  max(AP.AUTHORIZED_DATE5)  AUTHORIZED_DATE
             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.AUTHORIZER5 is not null
              and AP.AUTHORIZED_DATE5 is not null
            group by CO.PROP_TAX_COMPANY_ID, AP.MONTH, AP.AUTHORIZER5
           union
           select CO.PROP_TAX_COMPANY_ID    PROP_TAX_COMPANY_ID,
                  AP.MONTH                  month,
                  6                         RULE_ORDER,
                  AP.AUTHORIZER6            AUTHORIZER,
                  max(AP.AUTHORIZED_DATE6)  AUTHORIZED_DATE
             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.AUTHORIZER6 is not null
              and AP.AUTHORIZED_DATE6 is not null
            group by CO.PROP_TAX_COMPANY_ID, AP.MONTH, AP.AUTHORIZER6
           union
           select CO.PROP_TAX_COMPANY_ID    PROP_TAX_COMPANY_ID,
                  AP.MONTH                  month,
                  7                         RULE_ORDER,
                  AP.AUTHORIZER7            AUTHORIZER,
                  max(AP.AUTHORIZED_DATE7)  AUTHORIZED_DATE
             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.AUTHORIZER7 is not null
              and AP.AUTHORIZED_DATE7 is not null
            group by CO.PROP_TAX_COMPANY_ID, AP.MONTH, AP.AUTHORIZER7
           union
           select CO.PROP_TAX_COMPANY_ID    PROP_TAX_COMPANY_ID,
                  AP.MONTH                  month,
                  8                         RULE_ORDER,
                  AP.AUTHORIZER8            AUTHORIZER,
                  max(AP.AUTHORIZED_DATE8)  AUTHORIZED_DATE
             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.AUTHORIZER8 is not null
              and AP.AUTHORIZED_DATE8 is not null
            group by CO.PROP_TAX_COMPANY_ID, AP.MONTH, AP.AUTHORIZER8
           union
           select CO.PROP_TAX_COMPANY_ID    PROP_TAX_COMPANY_ID,
                  AP.MONTH                  month,
                  9                         RULE_ORDER,
                  AP.AUTHORIZER9            AUTHORIZER,
                  max(AP.AUTHORIZED_DATE9)  AUTHORIZED_DATE
             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.AUTHORIZER9 is not null
              and AP.AUTHORIZED_DATE9 is not null
            group by CO.PROP_TAX_COMPANY_ID, AP.MONTH, AP.AUTHORIZER9
           union
           select CO.PROP_TAX_COMPANY_ID    PROP_TAX_COMPANY_ID,
                  AP.MONTH                  month,
                  10                        RULE_ORDER,
                  AP.AUTHORIZER10           AUTHORIZER,
                  max(AP.AUTHORIZED_DATE10) AUTHORIZED_DATE
             from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.AUTHORIZER10 is not null
              and AP.AUTHORIZED_DATE10 is not null
            group by CO.PROP_TAX_COMPANY_ID, AP.MONTH, AP.AUTHORIZER10) AP_VIEW,
          (select WORKFLOW_ID WORKFLOW_ID, max(ID) MAX_ID
             from PWRPLANT.WORKFLOW_DETAIL
            group by WORKFLOW_ID) MAX_VIEW
    where WT.SUBSYSTEM like '%proptax_accrual_approval%'
      and WT.WORKFLOW_TYPE_ID = WTR.WORKFLOW_TYPE_ID
      and WTR.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
      and WT.WORKFLOW_TYPE_ID = W.WORKFLOW_TYPE_ID
      and W.SUBSYSTEM = 'proptax_accrual_approval'
      and W.ID_FIELD1 = TO_CHAR(AP_VIEW.PROP_TAX_COMPANY_ID)
      and W.ID_FIELD2 = TO_CHAR(AP_VIEW.MONTH, 'mm/dd/yyyy')
      and WTR.RULE_ORDER = AP_VIEW.RULE_ORDER
      and W.WORKFLOW_ID = MAX_VIEW.WORKFLOW_ID(+);

-- Now for journal entries (rejectors).
-- BGB - 04/29/2016 - As above, we need to take the max approval timestamps as it's possible to have multiple for a
--                      given company/month.
--
--insert into PWRPLANT.WORKFLOW_DETAIL
--   (WORKFLOW_ID, ID, WORKFLOW_RULE_ID, WORKFLOW_RULE_DESC, RULE_ORDER, USERS, DATE_APPROVED,
--    USER_APPROVED, APPROVAL_STATUS_ID, REQUIRED, NOTES, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED,
--    sql, ALLOW_EDIT, ORIG_AUTHORITY_LIMIT, IS_CUSTOM_LIMIT, GROUP_APPROVAL)
--   select W.WORKFLOW_ID WORKFLOW_ID,
--          NVL(MAX_VIEW.MAX_ID, 0) + RANK() OVER(partition by W.WORKFLOW_ID order by WTR.RULE_ORDER, AP_VIEW.AUTHORIZER) ID,
--          WR.WORKFLOW_RULE_ID WORKFLOW_RULE_ID,
--          WR.DESCRIPTION WORKFLOW_RULE_DESC,
--          WTR.RULE_ORDER RULE_ORDER,
--          AP_VIEW.AUTHORIZER USERS,
--          AP_VIEW.AUTHORIZED_DATE DATE_APPROVED,
--          DECODE(AP_VIEW.AUTHORIZED_DATE, null, null, AP_VIEW.AUTHORIZER) USER_APPROVED,
--          null APPROVAL_STATUS_ID,
--          1 REQUIRED,
--          null NOTES,
--          WR.AUTHORITY_LIMIT AUTHORITY_LIMIT,
--          null NUM_APPROVERS,
--          1 NUM_REQUIRED,
--          null sql,
--          1 ALLOW_EDIT,
--          WR.AUTHORITY_LIMIT ORIG_AUTHORITY_LIMIT,
--          0 CUSTOM_LIMIT,
--          1 GROUP_APPROVAL
--     from PWRPLANT.WORKFLOW W,
--          PWRPLANT.WORKFLOW_RULE WR,
--          PWRPLANT.WORKFLOW_TYPE_RULE WTR,
--          PWRPLANT.WORKFLOW_TYPE WT,
--          (select distinct CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
--                           AP.MONTH month,
--                           DECODE(AP.AUTHORIZED_DATE10,
--                                  null,
--                                  DECODE(AP.AUTHORIZED_DATE9,
--                                         null,
--                                         DECODE(AP.AUTHORIZED_DATE8,
--                                                null,
--                                                DECODE(AP.AUTHORIZED_DATE7,
--                                                       null,
--                                                       DECODE(AP.AUTHORIZED_DATE6,
--                                                              null,
--                                                              DECODE(AP.AUTHORIZED_DATE5,
--                                                                     null,
--                                                                     DECODE(AP.AUTHORIZED_DATE4,
--                                                                            null,
--                                                                            DECODE(AP.AUTHORIZED_DATE3,
--                                                                                   null,
--                                                                                   DECODE(AP.AUTHORIZED_DATE2,
--                                                                                          null,
--                                                                                          DECODE(AP.AUTHORIZED_DATE1,
--                                                                                                 null,
--                                                                                                 1,
--                                                                                                 2),
--                                                                                          3),
--                                                                                   4),
--                                                                            5),
--                                                                     6),
--                                                              7),
--                                                       8),
--                                                9),
--                                         10),
--                                  11) RULE_ORDER,
--                           AP.REJECTOR AUTHORIZER,
--                           AP.REJECTED_DATE AUTHORIZED_DATE
--             from PWRPLANT.PT_ACCRUAL_PENDING AP, COMPANY_SETUP CO
--            where AP.COMPANY_ID = CO.COMPANY_ID
--              and AP.REJECTOR is not null
--              and AP.REJECTED_DATE is not null) AP_VIEW,
--          (select WORKFLOW_ID WORKFLOW_ID, max(ID) MAX_ID
--             from PWRPLANT.WORKFLOW_DETAIL
--            group by WORKFLOW_ID) MAX_VIEW
--    where WT.SUBSYSTEM like '%proptax_accrual_approval%'
--      and WT.WORKFLOW_TYPE_ID = WTR.WORKFLOW_TYPE_ID
--      and WTR.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
--      and WT.WORKFLOW_TYPE_ID = W.WORKFLOW_TYPE_ID
--      and W.SUBSYSTEM = 'proptax_accrual_approval'
--      and W.ID_FIELD1 = TO_CHAR(AP_VIEW.PROP_TAX_COMPANY_ID)
--      and W.ID_FIELD2 = TO_CHAR(AP_VIEW.MONTH, 'mm/dd/yyyy')
--      and WTR.RULE_ORDER = AP_VIEW.RULE_ORDER
--      and W.WORKFLOW_ID = MAX_VIEW.WORKFLOW_ID(+);

insert into PWRPLANT.WORKFLOW_DETAIL
   (WORKFLOW_ID, ID, WORKFLOW_RULE_ID, WORKFLOW_RULE_DESC, RULE_ORDER, USERS, DATE_APPROVED,
    USER_APPROVED, APPROVAL_STATUS_ID, REQUIRED, NOTES, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED,
    sql, ALLOW_EDIT, ORIG_AUTHORITY_LIMIT, IS_CUSTOM_LIMIT, GROUP_APPROVAL)
   select W.WORKFLOW_ID WORKFLOW_ID,
          NVL(MAX_VIEW.MAX_ID, 0) + RANK() OVER(partition by W.WORKFLOW_ID order by WTR.RULE_ORDER, AP_VIEW.AUTHORIZER) ID,
          WR.WORKFLOW_RULE_ID WORKFLOW_RULE_ID,
          WR.DESCRIPTION WORKFLOW_RULE_DESC,
          WTR.RULE_ORDER RULE_ORDER,
          AP_VIEW.AUTHORIZER USERS,
          AP_VIEW.AUTHORIZED_DATE DATE_APPROVED,
          DECODE(AP_VIEW.AUTHORIZED_DATE, null, null, AP_VIEW.AUTHORIZER) USER_APPROVED,
          null APPROVAL_STATUS_ID,
          1 REQUIRED,
          null NOTES,
          WR.AUTHORITY_LIMIT AUTHORITY_LIMIT,
          null NUM_APPROVERS,
          1 NUM_REQUIRED,
          null sql,
          1 ALLOW_EDIT,
          WR.AUTHORITY_LIMIT ORIG_AUTHORITY_LIMIT,
          0 CUSTOM_LIMIT,
          1 GROUP_APPROVAL
     from PWRPLANT.WORKFLOW W,
          PWRPLANT.WORKFLOW_RULE WR,
          PWRPLANT.WORKFLOW_TYPE_RULE WTR,
          PWRPLANT.WORKFLOW_TYPE WT,
          (select CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
                  AP.MONTH month,
                  max(DECODE(AP.AUTHORIZED_DATE10,
                        null,
                        DECODE(AP.AUTHORIZED_DATE9,
                                null,
                                DECODE(AP.AUTHORIZED_DATE8,
                                      null,
                                      DECODE(AP.AUTHORIZED_DATE7,
                                              null,
                                              DECODE(AP.AUTHORIZED_DATE6,
                                                    null,
                                                    DECODE(AP.AUTHORIZED_DATE5,
                                                            null,
                                                            DECODE(AP.AUTHORIZED_DATE4,
                                                                  null,
                                                                  DECODE(AP.AUTHORIZED_DATE3,
                                                                          null,
                                                                          DECODE(AP.AUTHORIZED_DATE2,
                                                                                null,
                                                                                DECODE(AP.AUTHORIZED_DATE1,
                                                                                        null,
                                                                                        1,
                                                                                        2),
                                                                                3),
                                                                          4),
                                                                  5),
                                                            6),
                                                    7),
                                              8),
                                      9),
                                10),
                        11)) RULE_ORDER,
                  AP.REJECTOR AUTHORIZER,
                  max(AP.REJECTED_DATE) AUTHORIZED_DATE
             from PWRPLANT.PT_ACCRUAL_PENDING AP, COMPANY_SETUP CO
            where AP.COMPANY_ID = CO.COMPANY_ID
              and AP.REJECTOR is not null
              and AP.REJECTED_DATE is not null
            group by CO.PROP_TAX_COMPANY_ID,
                  AP.MONTH,
                  AP.REJECTOR
          ) AP_VIEW,
          (select WORKFLOW_ID WORKFLOW_ID, max(ID) MAX_ID
             from PWRPLANT.WORKFLOW_DETAIL
            group by WORKFLOW_ID) MAX_VIEW
    where WT.SUBSYSTEM like '%proptax_accrual_approval%'
      and WT.WORKFLOW_TYPE_ID = WTR.WORKFLOW_TYPE_ID
      and WTR.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
      and WT.WORKFLOW_TYPE_ID = W.WORKFLOW_TYPE_ID
      and W.SUBSYSTEM = 'proptax_accrual_approval'
      and W.ID_FIELD1 = TO_CHAR(AP_VIEW.PROP_TAX_COMPANY_ID)
      and W.ID_FIELD2 = TO_CHAR(AP_VIEW.MONTH, 'mm/dd/yyyy')
      and WTR.RULE_ORDER = AP_VIEW.RULE_ORDER
      and W.WORKFLOW_ID = MAX_VIEW.WORKFLOW_ID(+);

--
-- Now create workflow_detail records for pending approvers.  Just insert for rules where
-- that rule has not already been approved by someone.
--

-- Payments
insert into PWRPLANT.WORKFLOW_DETAIL
   (WORKFLOW_ID, ID, WORKFLOW_RULE_ID, WORKFLOW_RULE_DESC, RULE_ORDER, USERS, DATE_APPROVED,
    USER_APPROVED, APPROVAL_STATUS_ID, REQUIRED, NOTES, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED,
    sql, ALLOW_EDIT, ORIG_AUTHORITY_LIMIT, IS_CUSTOM_LIMIT, GROUP_APPROVAL)
   select W.WORKFLOW_ID WORKFLOW_ID,
          NVL(MAX_VIEW.MAX_ID, 0) + RANK() OVER(partition by W.WORKFLOW_ID order by WTR.RULE_ORDER, AAL.USERS) ID,
          WR.WORKFLOW_RULE_ID WORKFLOW_RULE_ID,
          WR.DESCRIPTION WORKFLOW_RULE_DESC,
          WTR.RULE_ORDER RULE_ORDER,
          AAL.USERS USERS,
          null DATE_APPROVED,
          null USER_APPROVED,
          null APPROVAL_STATUS_ID,
          1 REQUIRED,
          null NOTES,
          WR.AUTHORITY_LIMIT AUTHORITY_LIMIT,
          null NUM_APPROVERS,
          1 NUM_REQUIRED,
          null sql,
          1 ALLOW_EDIT,
          WR.AUTHORITY_LIMIT ORIG_AUTHORITY_LIMIT,
          0 CUSTOM_LIMIT,
          1 GROUP_APPROVAL
     from PWRPLANT.WORKFLOW W,
          PWRPLANT.WORKFLOW_RULE WR,
          PWRPLANT.WORKFLOW_TYPE_RULE WTR,
          PWRPLANT.WORKFLOW_TYPE WT,
          PWRPLANT.APPROVAL_AUTH_LEVEL AAL,
          (select WORKFLOW_ID WORKFLOW_ID, max(ID) MAX_ID
             from PWRPLANT.WORKFLOW_DETAIL
            group by WORKFLOW_ID) MAX_VIEW,
          (select SGP.PAYMENT_ID PAYMENT_ID,
                  NVL(LEAST(min(case
                                   when WR.AUTHORITY_LIMIT >= SGP.AMOUNT_PAID then
                                    WR.AUTHORITY_LIMIT
                                   else
                                    999999999999
                                end),
                            max(WR.AUTHORITY_LIMIT)),
                      999999999999) MAX_AUTHORITY_LIMIT
             from PWRPLANT.PT_STATEMENT_GROUP_PAYMENT SGP,
                  PWRPLANT.WORKFLOW                   W,
                  PWRPLANT.WORKFLOW_TYPE_RULE         WTR,
                  PWRPLANT.WORKFLOW_RULE              WR
            where TO_CHAR(SGP.PAYMENT_ID) = W.ID_FIELD1
              and W.SUBSYSTEM = 'proptax_payment_approval'
              and W.WORKFLOW_TYPE_ID = WTR.WORKFLOW_TYPE_ID
              and WTR.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
            group by SGP.PAYMENT_ID) LIMIT_VIEW
    where WT.SUBSYSTEM like '%proptax_payment_approval%'
      and WT.WORKFLOW_TYPE_ID = WTR.WORKFLOW_TYPE_ID
      and WTR.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
      and WT.WORKFLOW_TYPE_ID = W.WORKFLOW_TYPE_ID
      and W.SUBSYSTEM = 'proptax_payment_approval'
      and W.ID_FIELD1 = TO_CHAR(LIMIT_VIEW.PAYMENT_ID)
      and WT.WORKFLOW_TYPE_ID = AAL.APPROVAL_TYPE_ID
      and WR.WORKFLOW_RULE_ID = AAL.AUTH_LEVEL
      and WR.AUTHORITY_LIMIT <= LIMIT_VIEW.MAX_AUTHORITY_LIMIT
      and W.WORKFLOW_ID = MAX_VIEW.WORKFLOW_ID(+)
      and not exists (select 'x'
             from PWRPLANT.WORKFLOW_DETAIL WD
            where WD.WORKFLOW_ID = W.WORKFLOW_ID
              and WD.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
              and WD.DATE_APPROVED is not null)
      and not exists (select 'x'
             from PWRPLANT.WORKFLOW_DETAIL WD
            where WD.WORKFLOW_ID = W.WORKFLOW_ID
              and WD.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
              and WD.USERS = AAL.USERS);

-- Accruals
insert into PWRPLANT.WORKFLOW_DETAIL
   (WORKFLOW_ID, ID, WORKFLOW_RULE_ID, WORKFLOW_RULE_DESC, RULE_ORDER, USERS, DATE_APPROVED,
    USER_APPROVED, APPROVAL_STATUS_ID, REQUIRED, NOTES, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED,
    sql, ALLOW_EDIT, ORIG_AUTHORITY_LIMIT, IS_CUSTOM_LIMIT, GROUP_APPROVAL)
   select W.WORKFLOW_ID WORKFLOW_ID,
          NVL(MAX_VIEW.MAX_ID, 0) + RANK() OVER(partition by W.WORKFLOW_ID order by WTR.RULE_ORDER, AAL.USERS) ID,
          WR.WORKFLOW_RULE_ID WORKFLOW_RULE_ID,
          WR.DESCRIPTION WORKFLOW_RULE_DESC,
          WTR.RULE_ORDER RULE_ORDER,
          AAL.USERS USERS,
          null DATE_APPROVED,
          null USER_APPROVED,
          null APPROVAL_STATUS_ID,
          1 REQUIRED,
          null NOTES,
          WR.AUTHORITY_LIMIT AUTHORITY_LIMIT,
          null NUM_APPROVERS,
          1 NUM_REQUIRED,
          null sql,
          1 ALLOW_EDIT,
          WR.AUTHORITY_LIMIT ORIG_AUTHORITY_LIMIT,
          0 CUSTOM_LIMIT,
          1 GROUP_APPROVAL
     from PWRPLANT.WORKFLOW W,
          PWRPLANT.WORKFLOW_RULE WR,
          PWRPLANT.WORKFLOW_TYPE_RULE WTR,
          PWRPLANT.WORKFLOW_TYPE WT,
          PWRPLANT.APPROVAL_AUTH_LEVEL AAL,
          (select WORKFLOW_ID WORKFLOW_ID, max(ID) MAX_ID
             from PWRPLANT.WORKFLOW_DETAIL
            group by WORKFLOW_ID) MAX_VIEW,
          (select AP_VIEW.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
                  AP_VIEW.MONTH month,
                  NVL(LEAST(min(case
                                   when WR.AUTHORITY_LIMIT >= AP_VIEW.AMOUNT then
                                    WR.AUTHORITY_LIMIT
                                   else
                                    999999999999
                                end),
                            max(WR.AUTHORITY_LIMIT)),
                      999999999999) MAX_AUTHORITY_LIMIT
             from PWRPLANT.WORKFLOW W,
                  PWRPLANT.WORKFLOW_TYPE_RULE WTR,
                  PWRPLANT.WORKFLOW_RULE WR,
                  (select CO.PROP_TAX_COMPANY_ID PROP_TAX_COMPANY_ID,
                          AP.MONTH month,
                          sum(AP.AMOUNT) AMOUNT
                     from PWRPLANT.PT_ACCRUAL_PENDING AP, PWRPLANT.COMPANY_SETUP CO
                    where AP.COMPANY_ID = CO.COMPANY_ID
                    group by CO.PROP_TAX_COMPANY_ID, AP.MONTH) AP_VIEW
            where TO_CHAR(AP_VIEW.PROP_TAX_COMPANY_ID) = W.ID_FIELD1
              and TO_CHAR(AP_VIEW.MONTH, 'mm/dd/yyyy') = W.ID_FIELD2
              and W.SUBSYSTEM = 'proptax_accrual_approval'
              and W.WORKFLOW_TYPE_ID = WTR.WORKFLOW_TYPE_ID
              and WTR.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
            group by AP_VIEW.PROP_TAX_COMPANY_ID, AP_VIEW.MONTH) LIMIT_VIEW
    where WT.SUBSYSTEM like '%proptax_accrual_approval%'
      and WT.WORKFLOW_TYPE_ID = WTR.WORKFLOW_TYPE_ID
      and WTR.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
      and WT.WORKFLOW_TYPE_ID = W.WORKFLOW_TYPE_ID
      and W.SUBSYSTEM = 'proptax_accrual_approval'
      and W.ID_FIELD1 = TO_CHAR(LIMIT_VIEW.PROP_TAX_COMPANY_ID)
      and W.ID_FIELD2 = TO_CHAR(LIMIT_VIEW.MONTH, 'mm/dd/yyyy')
      and WT.WORKFLOW_TYPE_ID = AAL.APPROVAL_TYPE_ID
      and WR.WORKFLOW_RULE_ID = AAL.AUTH_LEVEL
      and WR.AUTHORITY_LIMIT <= LIMIT_VIEW.MAX_AUTHORITY_LIMIT
      and W.WORKFLOW_ID = MAX_VIEW.WORKFLOW_ID(+)
      and not exists (select 'x'
             from PWRPLANT.WORKFLOW_DETAIL WD
            where WD.WORKFLOW_ID = W.WORKFLOW_ID
              and WD.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
              and WD.DATE_APPROVED is not null)
      and not exists (select 'x'
             from PWRPLANT.WORKFLOW_DETAIL WD
            where WD.WORKFLOW_ID = W.WORKFLOW_ID
              and WD.WORKFLOW_RULE_ID = WR.WORKFLOW_RULE_ID
              and WD.USERS = AAL.USERS);

--
--   Update workflow_detail with the payment approval notes.  Just put the notes on the minimum ID (which should be the lowest level approver).
--
update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.NOTES =
        (select SPA.NOTES
           from PT_STATEMENT_PAYMENT_APPROVAL SPA, WORKFLOW W
          where W.WORKFLOW_ID = WD.WORKFLOW_ID
            and W.ID_FIELD1 = TO_CHAR(SPA.PAYMENT_ID))
 where exists
 (select 'x'
          from WORKFLOW W
         where W.SUBSYSTEM = 'proptax_payment_approval'
           and W.WORKFLOW_ID = WD.WORKFLOW_ID)
   and WD.ID = (select min(ID) from WORKFLOW_DETAIL INWD where INWD.WORKFLOW_ID = WD.WORKFLOW_ID);

--
-- Update approval_status_id on workflow_detail.
--

-- Workflow = Initiated - Mark all detail rows as Initiated
update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.APPROVAL_STATUS_ID = 1
 where WD.APPROVAL_STATUS_ID is null
   and exists
 (select 'x'
          from PWRPLANT.WORKFLOW W
         where W.APPROVAL_STATUS_ID = 1
           and W.WORKFLOW_ID = WD.WORKFLOW_ID
           and W.SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval'));

-- Workflow = Pending Approval
-- Detail rows where the date approved is not null for any record with that rule, mark as Approved
-- Detail rows where the date approved is null and it is the minimum rule order with a null status for the workflow (i.e., not approved), mark as Pending Approval
-- Otherwise, mark as Initiated
update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.APPROVAL_STATUS_ID = 3
 where WD.APPROVAL_STATUS_ID is null
   and exists (select 'x'
          from PWRPLANT.WORKFLOW_DETAIL INWD
         where INWD.WORKFLOW_ID = WD.WORKFLOW_ID
           and INWD.WORKFLOW_RULE_ID = WD.WORKFLOW_RULE_ID
           and INWD.DATE_APPROVED is not null)
   and exists
 (select 'x'
          from PWRPLANT.WORKFLOW W
         where W.APPROVAL_STATUS_ID = 2
           and W.WORKFLOW_ID = WD.WORKFLOW_ID
           and W.SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval'));

update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.APPROVAL_STATUS_ID = 2
 where WD.APPROVAL_STATUS_ID is null
   and WD.DATE_APPROVED is null
   and WD.RULE_ORDER = (select min(INWD.RULE_ORDER)
                          from PWRPLANT.WORKFLOW_DETAIL INWD
                         where INWD.WORKFLOW_ID = WD.WORKFLOW_ID
                           and INWD.APPROVAL_STATUS_ID is null)
   and exists
 (select 'x'
          from PWRPLANT.WORKFLOW W
         where W.APPROVAL_STATUS_ID = 2
           and W.WORKFLOW_ID = WD.WORKFLOW_ID
           and W.SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval'));

update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.APPROVAL_STATUS_ID = 1
 where WD.APPROVAL_STATUS_ID is null
   and exists
 (select 'x'
          from PWRPLANT.WORKFLOW W
         where W.APPROVAL_STATUS_ID = 2
           and W.WORKFLOW_ID = WD.WORKFLOW_ID
           and W.SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval'));

-- Workflow = Approved - Mark all detail rows as Approved
update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.APPROVAL_STATUS_ID = 3
 where WD.APPROVAL_STATUS_ID is null
   and exists
 (select 'x'
          from PWRPLANT.WORKFLOW W
         where W.APPROVAL_STATUS_ID = 3
           and W.WORKFLOW_ID = WD.WORKFLOW_ID
           and W.SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval'));

-- Workflow = Rejected
-- Detail rows where the approval date is the max approval date for the workflow, mark as Rejected
-- Detail rows where the approval date is not the max approval date, mark as Approved
-- Otherwise, mark as Inactive
update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.APPROVAL_STATUS_ID = 4
 where WD.APPROVAL_STATUS_ID is null
   and WD.DATE_APPROVED = (select max(INWD.DATE_APPROVED)
                             from PWRPLANT.WORKFLOW_DETAIL INWD
                            where INWD.WORKFLOW_ID = WD.WORKFLOW_ID
                              and INWD.DATE_APPROVED is not null)
   and exists
 (select 'x'
          from PWRPLANT.WORKFLOW W
         where W.APPROVAL_STATUS_ID = 4
           and W.WORKFLOW_ID = WD.WORKFLOW_ID
           and W.SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval'));

update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.APPROVAL_STATUS_ID = 3
 where WD.APPROVAL_STATUS_ID is null
   and WD.DATE_APPROVED is not null
   and exists
 (select 'x'
          from PWRPLANT.WORKFLOW W
         where W.APPROVAL_STATUS_ID = 4
           and W.WORKFLOW_ID = WD.WORKFLOW_ID
           and W.SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval'));

update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.APPROVAL_STATUS_ID = 5
 where WD.APPROVAL_STATUS_ID is null
   and exists
 (select 'x'
          from PWRPLANT.WORKFLOW W
         where W.APPROVAL_STATUS_ID = 4
           and W.WORKFLOW_ID = WD.WORKFLOW_ID
           and W.SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval'));

-- Workflow = Inactive
-- Detail rows with approval date populated for any record with that rule, mark as Approved
-- Detail rows with a null approval date and the minimum rule order, mark as Pending Approval
-- Otherwise, mark as Initiated
update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.APPROVAL_STATUS_ID = 3
 where WD.APPROVAL_STATUS_ID is null
   and exists (select 'x'
          from PWRPLANT.WORKFLOW_DETAIL INWD
         where INWD.WORKFLOW_ID = WD.WORKFLOW_ID
           and INWD.WORKFLOW_RULE_ID = WD.WORKFLOW_RULE_ID
           and INWD.DATE_APPROVED is not null)
   and exists
 (select 'x'
          from PWRPLANT.WORKFLOW W
         where W.APPROVAL_STATUS_ID = 5
           and W.WORKFLOW_ID = WD.WORKFLOW_ID
           and W.SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval'));

update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.APPROVAL_STATUS_ID = 2
 where WD.APPROVAL_STATUS_ID is null
   and WD.DATE_APPROVED is null
   and WD.RULE_ORDER = (select min(INWD.RULE_ORDER)
                          from PWRPLANT.WORKFLOW_DETAIL INWD
                         where INWD.WORKFLOW_ID = WD.WORKFLOW_ID
                           and INWD.DATE_APPROVED is null)
   and exists
 (select 'x'
          from PWRPLANT.WORKFLOW W
         where W.APPROVAL_STATUS_ID = 5
           and W.WORKFLOW_ID = WD.WORKFLOW_ID
           and W.SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval'));

update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.APPROVAL_STATUS_ID = 1
 where WD.APPROVAL_STATUS_ID is null
   and exists
 (select 'x'
          from PWRPLANT.WORKFLOW W
         where W.APPROVAL_STATUS_ID = 5
           and W.WORKFLOW_ID = WD.WORKFLOW_ID
           and W.SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval'));

-- Workflow = Unrejected - Mark all rows as Initiated
update PWRPLANT.WORKFLOW_DETAIL WD
   set WD.APPROVAL_STATUS_ID = 1
 where WD.APPROVAL_STATUS_ID is null
   and exists
 (select 'x'
          from PWRPLANT.WORKFLOW W
         where W.APPROVAL_STATUS_ID = 7
           and W.WORKFLOW_ID = WD.WORKFLOW_ID
           and W.SUBSYSTEM in ('proptax_payment_approval', 'proptax_accrual_approval'));

--
-- Create backups of pt_statement_payment_approval and pt_accrual_pending, in case anything in this script fails and we need to reference them.
--
create table PWRPLANT.PT_STATEMENT_PAYMENT_APP_BAK as select * from PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL;
create table PWRPLANT.PT_ACCRUAL_PENDING_BAK as select * from PWRPLANT.PT_ACCRUAL_PENDING;

--
-- Drop the pt_statement_payment_approval_table.  It has been replaced by workflow_detail.
--
SET SERVEROUTPUT ON

begin
   execute immediate 'DROP PUBLIC SYNONYM PT_STATEMENT_PAYMENT_APPROVAL';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('SYNONYM PT_STATEMENT_PAYMENT_APPROVAL DOESN''T EXIST or DROP PUBLIC SYNONYM PRIVILEGE not GRANTED.');
end;
/

drop table PWRPLANT.PT_STATEMENT_PAYMENT_APPROVAL;

--
-- Drop the authorizer/authorized_date columns on pt_accrual_pending.  They have been replaced by workflow_detail.
--
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZER1;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZED_DATE1;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZER2;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZED_DATE2;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZER3;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZED_DATE3;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZER4;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZED_DATE4;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZER5;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZED_DATE5;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZER6;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZED_DATE6;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZER7;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZED_DATE7;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZER8;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZED_DATE8;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZER9;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZED_DATE9;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZER10;
alter table PWRPLANT.PT_ACCRUAL_PENDING drop column AUTHORIZED_DATE10;

--
-- Drop the tables we used for conversion
--
drop table PWRPLANT.PT_APPROVAL_WORKFLOW_TYPE_CONV;
drop table PWRPLANT.PT_APPROVAL_WORKFLOW_RULE_CONV;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (354, 0, 10, 4, 1, 0, 29436, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029436_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
