 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044342_sys_02_ssp_versions_dml.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || --------    ---------- -------------- ----------------------------------------
 || 2015.1.2.0  08/06/2015 David Haupt    Populating a new table for use in SSP Version Validation
 ||============================================================================
 */

DELETE FROM pp_custom_pbd_versions
WHERE pbd_name = 'ssp_depr_calc_custom.pbd'
;

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppdepr_interface_custom.pbd',
  '2015.1.2.0'
FROM pp_processes
WHERE lower(trim(executable_file)) = 'ssp_depr_approval.exe'
;

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppdepr_interface_custom.pbd',
  '2015.1.2.0'
FROM pp_processes
WHERE lower(trim(executable_file)) = 'ssp_close_powerplant.exe'
;

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppprojct_custom.pbd',
  '2015.1.2.0'
FROM pp_processes
WHERE lower(trim(executable_file)) = 'ssp_wo_auto_nonunitize.exe non'
;

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppprojct_custom.pbd',
  '2015.1.2.0'
FROM pp_processes
WHERE lower(trim(executable_file)) = 'ssp_wo_auto_nonunitize.exe failed'
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2752, 0, 2015, 1, 2, 0, 044342, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.2.0_maint_044342_sys_02_ssp_versions_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;