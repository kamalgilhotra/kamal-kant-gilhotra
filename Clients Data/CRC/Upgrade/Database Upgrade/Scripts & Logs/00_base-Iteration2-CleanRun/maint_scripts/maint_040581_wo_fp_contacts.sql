/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_040581_wo_fp_contacts.sql
||==========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1.0.0 11/14/14   LAlston    	     Added NOTIFY_INITIATED_BY column
||==========================================================================================
*/

alter table WORK_ORDER_INITIATOR add NOTIFY_INITIATED_BY NUMBER(22, 0) DEFAULT 1 NOT NULL;
comment on column WORK_ORDER_INITIATOR.NOTIFY_INITIATED_BY IS 'Yes/no indicator  Whether to notify owner/iniator on approval or rejection of work order.';

