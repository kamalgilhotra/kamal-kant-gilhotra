/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051024_system_01_reset_pp_import_template_seq_value_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.3.0.0  04/30/2018 Andrew Hill       Reset nextval for pp_import_template_seq based on max value in pp_import_template
||============================================================================
*/

DECLARE
  l_sequence_value NUMBER;
  l_max_value number;
BEGIN
  EXECUTE IMMEDIATE 'SELECT pp_import_template_seq.NEXTVAL FROM dual' INTO l_sequence_value;
  
  execute immediate 'SELECT MAX(import_template_id) from pp_import_template' INTO l_max_value;
  
  -- Added check for 0 to avoid "ORA-04002: INCREMENT must be a non-zero integer"
   IF (l_max_value - l_sequence_value) <> 0
   THEN
      EXECUTE IMMEDIATE 'alter sequence pp_import_template_seq increment by ' || to_char(l_max_value - l_sequence_value);
   END IF;

   EXECUTE IMMEDIATE 'select pp_import_template_seq.nextval from dual' into l_sequence_value;

   EXECUTE IMMEDIATE 'alter sequence pp_import_template_seq increment by 1';
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4866, 0, 2017, 3, 0, 0, 51024, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051024_system_01_reset_pp_import_template_seq_value_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;