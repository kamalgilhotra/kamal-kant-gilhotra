/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030593_lease_mla_revision.sql
|| Description: Updates to allow revisions on MLA's
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/10/2013 Ryan Oliveria   Point Release
||============================================================================
*/

--* Default revision, change primary key on ls_lease_approval
update LS_LEASE_APPROVAL set REVISION = 1 where REVISION is null;

insert into LS_LEASE_APPROVAL
   (LEASE_ID, REVISION, TIME_STAMP, USER_ID, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
   select LEASE_ID,
          1,
          sysdate,
          user,
          (select WORKFLOW_TYPE_ID from WORKFLOW_TYPE where SUBSYSTEM = 'mla_approval'),
          1
     from LS_LEASE
    where not exists (select 1 from LS_LEASE_APPROVAL where LEASE_ID = LS_LEASE.LEASE_ID);

alter table LS_LEASE_APPROVAL drop primary key drop index;

alter table LS_LEASE_APPROVAL
   add constraint PK_LS_LEASE_APPROVAL
       primary key (LEASE_ID, REVISION)
       using index tablespace PWRPLANT_IDX;

--* Create ls_lease_options table (include primary and foreign keys)
create table LS_LEASE_OPTIONS
(
 LEASE_ID                number(22,0) not null,
 REVISION                number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 PURCHASE_OPTION_TYPE_ID number(22,0),
 PURCHASE_OPTION_AMT     number(22,2),
 RENEWAL_OPTION_TYPE_ID  number(22,0),
 CANCELABLE_TYPE_ID      number(22,0)
);

alter table LS_LEASE_OPTIONS
  add constraint PK_LS_LEASE_OPTIONS
    primary key (LEASE_ID, REVISION)
    using index tablespace PWRPLANT_IDX;

alter table LS_LEASE_OPTIONS
  add constraint FK_LS_LEASE_OPTIONS
    foreign key (LEASE_ID, REVISION)
    references LS_LEASE_APPROVAL (LEASE_ID, REVISION);

--* Populate ls_lease_options table
insert into LS_LEASE_OPTIONS
   (LEASE_ID, REVISION, TIME_STAMP, USER_ID, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT,
    RENEWAL_OPTION_TYPE_ID, CANCELABLE_TYPE_ID)
   select LEASE_ID,
          1,
          sysdate,
          user,
          PURCHASE_OPTION_TYPE_ID,
          PURCHASE_OPTION_AMT,
          RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID
     from LS_LEASE
    where not exists (select 1 from LS_LEASE_OPTIONS where LEASE_ID = LS_LEASE.LEASE_ID);

--* Drop revision and options columns from ls_lease
alter table LS_LEASE drop column PURCHASE_OPTION_TYPE_ID;
alter table LS_LEASE drop column PURCHASE_OPTION_AMT;
alter table LS_LEASE drop column RENEWAL_OPTION_TYPE_ID;
alter table LS_LEASE drop column CANCELABLE_TYPE_ID;

--* Add current_revision column on ls_lease
alter table LS_LEASE add CURRENT_REVISION number(22,0);

update LS_LEASE set CURRENT_REVISION = 1 where CURRENT_REVISION is null;

--* Add revision to lease children
alter table LS_LEASE_PAYMENT_TERM add REVISION number(22,0);

--* Default revision on lease children
update LS_LEASE_PAYMENT_TERM set REVISION = 1 where REVISION is null;

--* Drop foreign keys from lease children
alter table LS_LEASE_PAYMENT_TERM drop constraint FK_LEASEPYMTTERM_LEASE;

--* Drop primary keys from lease children
alter table LS_LEASE_PAYMENT_TERM drop primary key drop index;

--* Create primary key (lease_id, revision) on lease children
alter table LS_LEASE_PAYMENT_TERM
  add constraint PK_LS_LEASE_PAYMENT_TERM
      primary key (LEASE_ID, REVISION)
      using index tablespace PWRPLANT_IDX;

--* Create foreign key (lease_id, revision) on lease children
alter table LS_LEASE_PAYMENT_TERM
   add constraint FK_LEASEPYMTTERM_LEASE
       foreign key (LEASE_ID, REVISION)
       references LS_LEASE_APPROVAL (LEASE_ID, REVISION);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (459, 0, 10, 4, 1, 0, 30593, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030593_lease_mla_revision.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
