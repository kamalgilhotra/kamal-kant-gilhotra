/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_042694_pwrtax_new_idxs_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   02/11/2015 A Scott          Add new indexes from M Bradley to improve
||                                      performance. wrap in plsql to not kick out
||                                      if indexes cannot be added.  2nd index 
||                                      requested should already exist, but try in case 
||                                      it does not.
||============================================================================
*/

SET SERVEROUTPUT ON

------saving off drop statements to test re-runnability.
------drop index IDX_PTX_TY_VERSION;
------drop index TBT_FUNC_GROUP_IDX;

begin
   execute immediate 'create unique index IDX_PTX_TY_VERSION '||
                     'on TAX_YEAR_VERSION '||
                     '(VERSION_ID, TAX_YEAR) '||
                     'tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE('Index IDX_PTX_TY_VERSION added.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Unable to add IDX_PTX_TY_VERSION powertax index.  Continuing...');
end;
/


begin
   execute immediate 'create index TBT_FUNC_GROUP_IDX '||
                     'on TAX_BOOK_TRANSLATE '||
                     '(COMPANY_ID, BUS_SEGMENT_ID, UTILITY_ACCOUNT_ID, SUB_ACCOUNT_ID, TAX_LOCATION_ID, CLASS_CODE_ID, CLASS_CODE_VALUE, GL_ACCOUNT_ID, TAX_DISTINCTION_ID) '||
                     'tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE('Index TBT_FUNC_GROUP_IDX added.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Unable to add TBT_FUNC_GROUP_IDX powertax index.  Continuing...');
end;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2276, 0, 2015, 1, 0, 0, 42694, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042694_pwrtax_new_idxs_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;