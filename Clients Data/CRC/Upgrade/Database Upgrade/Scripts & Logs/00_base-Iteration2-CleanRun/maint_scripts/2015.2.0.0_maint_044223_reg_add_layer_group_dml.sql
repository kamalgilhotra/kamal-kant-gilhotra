/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_044223_reg_add_layer_group_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 07/21/2015 Anand R        Reg - Add layer group for reports
||============================================================================
*/

insert into pp_dynamic_filter (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values (280, 'Reg Layer Group', 'dw', 'incremental_group_id', 'incremental_adj_id in (select incremental_adj_id from reg_incremental_adj_group where [selected_values] )', 'dddw_reg_layer_group', 'incremental_group_id', 'description', 'N', 0, 0, null, null, 0, 0, 0);

insert into pp_dynamic_filter_mapping (PP_REPORT_FILTER_ID, FILTER_ID) values (83, 280);

insert into pp_dynamic_filter_restrictions (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION)
values (75, 192, 280, 'incremental_group_id ');

--Add new pp_report_filter for Rl - 104 report_number

insert into pp_reports_filter (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values (91, 'Reg Layer Group, Reg Layer', '', 'uo_ppbase_tab_filter_dynamic');

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values    (91, 192);

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values    (91, 280);

update pp_reports set pp_report_filter_id = 91 where upper(trim(report_number)) = 'RL - 104' ;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2697, 0, 2015, 2, 0, 0, 044223, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044223_reg_add_layer_group_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;