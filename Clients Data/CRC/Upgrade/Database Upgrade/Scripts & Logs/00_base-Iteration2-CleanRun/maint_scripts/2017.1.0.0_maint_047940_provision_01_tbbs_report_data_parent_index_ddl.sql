/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047940_provision_01_tbbs_report_data_parent_index_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 12/14/2017 Andrew Hill      Add index to improve report performance
||============================================================================
*/

CREATE INDEX tbbs_report_data_parent_idx ON tbbs_report_data_tmp (parent_line_id);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4070, 0, 2017, 1, 0, 0, 47940, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047940_provision_01_tbbs_report_data_parent_index_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;