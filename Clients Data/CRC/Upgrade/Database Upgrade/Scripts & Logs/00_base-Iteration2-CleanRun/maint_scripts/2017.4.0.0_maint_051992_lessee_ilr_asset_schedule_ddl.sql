/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051992_lessee_ilr_asset_schedule_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.4.0.0 07/19/2018 Anand R	       PP-51992 fix a join issue
||============================================================================
*/

CREATE OR REPLACE FORCE VIEW V_LS_ASSET_SCHEDULE_FX_VW AS
WITH cur
       AS ( SELECT ls_currency_type_id AS ls_cur_type,
                   currency_id,
                   currency_display_symbol,
                   iso_code,
                   CASE ls_currency_type_id
                     WHEN 1 THEN 1
                     ELSE NULL
                   END                 AS contract_approval_rate
            FROM   CURRENCY
                   cross join LS_LEASE_CURRENCY_TYPE ),
       open_month
       AS ( SELECT company_id,
                   Min( gl_posting_mo_yr ) open_month
            FROM   LS_PROCESS_CONTROL
            WHERE  open_next IS NULL
            GROUP  BY company_id ),
       calc_rate
       AS ( SELECT a.company_id,
                   a.contract_currency_id,
                   a.company_currency_id,
                   a.accounting_month,
                   a.exchange_date,
                   a.rate,
                   b.rate prev_rate
            FROM   LS_LEASE_CALCULATED_DATE_RATES a
                   left outer join LS_LEASE_CALCULATED_DATE_RATES b
                                ON a.company_id = b.company_id AND
                                   a.contract_currency_id = b.contract_currency_id AND
                                   a.accounting_month = Add_months( b.accounting_month, 1 )
            WHERE a.exchange_rate_type_id = 1 and b.exchange_rate_type_id = 1),
       cr_now
       AS ( SELECT currency_from,
                   currency_to,
                   rate
            FROM   ( SELECT currency_from,
                            currency_to,
                            rate,
                            Row_number( )
                              over(
                                PARTITION BY currency_from, currency_to
                                ORDER BY exchange_date DESC ) AS rn
                     FROM   CURRENCY_RATE_DEFAULT_DENSE
                     WHERE  Trunc( exchange_date, 'MONTH' ) <= Trunc( SYSDATE, 'MONTH' ) AND
                            exchange_rate_type_id = 1 )
            WHERE  rn = 1 ),
       calc_avg_rate
       AS ( SELECT a.company_id,
                   a.contract_currency_id,
                   a.company_currency_id,
                   a.accounting_month,
                   a.exchange_date,
                   a.rate,
                   b.rate prev_rate
            FROM   LS_LEASE_CALCULATED_DATE_RATES a
                   left outer join LS_LEASE_CALCULATED_DATE_RATES b
                                ON a.company_id = b.company_id AND
                                   a.contract_currency_id = b.contract_currency_id AND
                                   a.accounting_month = Add_months( b.accounting_month, 1 ) AND 
								   b.exchange_rate_type_id = 4
            WHERE a.exchange_rate_type_id = 4 ),
       war
       AS ( SELECT war.ilr_id,
                   war.revision,
                   war.set_of_books_id,
                   war.gross_weighted_avg_rate,
                   war.net_weighted_avg_rate,
                   lio.in_service_exchange_rate,
                   Trunc(Nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month
            FROM ls_ilr_weighted_avg_rates war
                 JOIN ls_ilr ilr ON war.ilr_id = ilr.ilr_id
                 JOIN ls_ilr_options lio ON lio.ilr_id = war.ilr_id AND lio.revision = war.revision
           ),
      ilr_dates AS (
        SELECT lio.ilr_id, lio.revision,
        Trunc(Nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month,
        lio.in_service_exchange_rate
        FROM ls_ilr_options lio
          JOIN ls_ilr ilr ON ilr.ilr_id = lio.ilr_id
          JOIN ls_ilr_approval lia ON lia.ilr_id = lio.ilr_id AND lia.revision = lio.revision
        WHERE lia.approval_status_id <> 1
      ),
      ilr_effective_rates AS (
        SELECT id2.ilr_id, id1.revision,
        id2.effective_month,
        Nvl(Add_Months(lead(id2.effective_month,1) OVER(PARTITION BY id2.ilr_id, id1.revision ORDER BY id2.effective_month), -1), To_Date('999912', 'yyyymm')) end_effective_month,
        id2.in_service_exchange_rate
        FROM ilr_dates id1
          JOIN ilr_dates id2 ON id1.ilr_id = id2.ilr_id AND id1.effective_month >= id2.effective_month
      )
  SELECT las.ilr_id                                                                                                                                               ilr_id,
         las.ls_asset_id                                                                                                                                          ls_asset_id,
         las.revision                                                                                                                                             revision,
         las.set_of_books_id                                                                                                                                      set_of_books_id,
         las.month                                                                                                                                                month,
         OPEN_MONTH.company_id                                                                                                                                    company_id,
         OPEN_MONTH.open_month                                                                                                                                    open_month,
         CUR.ls_cur_type                                                                                                                                          ls_cur_type,
         cr.exchange_date                                                                                                                                         exchange_date,
         CALC_RATE.exchange_date                                                                                                                                  prev_exchange_date,
         las.contract_currency_id                                                                                                                                 contract_currency_id,
         CUR.currency_id                                                                                                                                          currency_id,
         cr.rate                                                                                                                                                  rate,
         CALC_RATE.rate                                                                                                                                           calculated_rate,
         CALC_RATE.prev_rate                                                                                                                                      previous_calculated_rate,
         CALC_AVG_RATE.rate                                                                                                                                       calculated_average_rate,
         CALC_AVG_RATE.prev_rate                                                                                                                                  previous_calculated_avg_rate,
         Decode(lower(sc.CONTROL_VALUE), 'yes', CALC_AVG_RATE.rate, CALC_RATE.rate)                                                                               average_rate,
         las.effective_in_svc_rate,
         original_in_svc_rate,
         CUR.iso_code                                                                                                                                             iso_code,
         CUR.currency_display_symbol                                                                                                                              currency_display_symbol,
         las.residual_amount * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     residual_amount,
         las.term_penalty * Nvl( CALC_RATE.rate, cr.rate )                                                                                                        term_penalty,
         las.bpo_price * Nvl( CALC_RATE.rate, cr.rate )                                                                                                      bpo_price,
         las.beg_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, prior_war.gross_weighted_avg_rate), CR_NOW.rate )                                      beg_capital_cost,
         las.end_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, war.gross_weighted_avg_rate), CR_NOW.rate )                                            end_capital_cost,
         las.beg_obligation * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                                            beg_obligation,
         las.end_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 end_obligation,
         las.beg_lt_obligation * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                                         beg_lt_obligation,
         las.end_lt_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                              end_lt_obligation,
         las.principal_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate )                                       principal_remeasurement,
         las.beg_liability * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                                                  beg_liability,
         las.end_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  end_liability,
         las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                                               beg_lt_liability,
         las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                               end_lt_liability,
         las.liability_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate )                                       liability_remeasurement,
         las.interest_accrual      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  interest_accrual,
         las.principal_accrual     * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  principal_accrual,
         las.interest_paid         * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  interest_paid,
         las.principal_paid        * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  principal_paid,
         las.executory_accrual1    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual1,
         las.executory_accrual2    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual2,
         las.executory_accrual3    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual3,
         las.executory_accrual4    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual4,
         las.executory_accrual5    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual5,
         las.executory_accrual6    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual6,
         las.executory_accrual7    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual7,
         las.executory_accrual8    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual8,
         las.executory_accrual9    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual9,
         las.executory_accrual10   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual10,
         las.executory_paid1       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid1,
         las.executory_paid2       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid2,
         las.executory_paid3       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid3,
         las.executory_paid4       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid4,
         las.executory_paid5       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid5,
         las.executory_paid6       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid6,
         las.executory_paid7       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid7,
         las.executory_paid8       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid8,
         las.executory_paid9       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid9,
         las.executory_paid10      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid10,
         las.contingent_accrual1   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual1,
         las.contingent_accrual2   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual2,
         las.contingent_accrual3   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual3,
         las.contingent_accrual4   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual4,
         las.contingent_accrual5   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual5,
         las.contingent_accrual6   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual6,
         las.contingent_accrual7   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual7,
         las.contingent_accrual8   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual8,
         las.contingent_accrual9   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual9,
         las.contingent_accrual10  * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual10,
         las.contingent_paid1      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid1,
         las.contingent_paid2      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid2,
         las.contingent_paid3      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid3,
         las.contingent_paid4      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid4,
         las.contingent_paid5      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid5,
         las.contingent_paid6      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid6,
         las.contingent_paid7      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid7,
         las.contingent_paid8      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid8,
         las.contingent_paid9      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid9,
         las.contingent_paid10     * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid10,
         las.current_lease_cost    * Nvl( Nvl( CUR.contract_approval_rate, original_in_svc_rate), CR_NOW.rate ) current_lease_cost,
         las.beg_deferred_rent     * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                         beg_deferred_rent,
         las.deferred_rent         * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  deferred_rent,
         las.end_deferred_rent     * Nvl( CALC_RATE.rate, cr.rate )                                                                         end_deferred_rent,
         las.beg_st_deferred_rent  * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                              beg_st_deferred_rent,
         las.end_st_deferred_rent  * Nvl( CALC_RATE.rate, cr.rate )                                                                              end_st_deferred_rent,
         las.initial_direct_cost   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  initial_direct_cost,
         las.incentive_amount      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  incentive_amount,
         las.depr_expense          * Nvl( Nvl( CUR.contract_approval_rate, war.net_weighted_avg_rate ), CR_NOW.rate )                         depr_expense,
         las.begin_reserve         * Nvl( Nvl( CUR.contract_approval_rate, prior_war.net_weighted_avg_rate ), CR_NOW.rate )                         begin_reserve,
         las.end_reserve           * Nvl( Nvl( CUR.contract_approval_rate, war.net_weighted_avg_rate ), CR_NOW.rate )                         end_reserve,
         las.depr_exp_alloc_adjust * Nvl( Nvl( CUR.contract_approval_rate, war.net_weighted_avg_rate ), CR_NOW.rate )                         depr_exp_alloc_adjust,
         las.asset_description                                                                                                                   asset_description,
         las.leased_asset_number                                                                                                                 leased_asset_number,
         las.fmv * Nvl( Nvl( CUR.contract_approval_rate, original_in_svc_rate), CR_NOW.rate )                                                    fmv,
         las.is_om                                                                                                                               is_om,
         las.approved_revision                                                                                                                   approved_revision,
         las.lease_cap_type_id                                                                                                                   lease_cap_type_id,
         las.ls_asset_status_id                                                                                                                  ls_asset_status_id,
         las.retirement_date                                                                                                                     retirement_date,
         las.beg_prepaid_rent      * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                         beg_prepaid_rent,
         las.prepay_amortization   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  prepay_amortization,
         las.prepaid_rent          * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  prepaid_rent,
         las.end_prepaid_rent      * Nvl( CALC_RATE.rate, cr.rate )                                                                              end_prepaid_rent,
         Nvl(las.beg_net_rou_asset * Nvl( Nvl( CUR.contract_approval_rate, prior_war.net_weighted_avg_rate ), CR_NOW.rate ), 0)                  beg_net_rou_asset,
         Nvl(las.end_net_rou_asset * Nvl( Nvl( CUR.contract_approval_rate, war.net_weighted_avg_rate ), CR_NOW.rate ), 0)                        end_net_rou_asset,
         las.actual_residual_amount * Nvl( CALC_RATE.rate, cr.rate )                                                                             actual_residual_amount,
         las.guaranteed_residual_amount * Nvl( CALC_RATE.rate, cr.rate )                                                                         guaranteed_residual_amount,
         (las.fmv * las.estimated_residual) * Nvl( CALC_RATE.rate, cr.rate )                                                                     estimated_residual,
         nvl(las.rou_asset_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ),0)                    rou_asset_remeasurement,
         nvl(las.PARTIAL_TERM_GAIN_LOSS * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ),0)                     partial_term_gain_loss,
         las.beg_arrears_accrual * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                           beg_arrears_accrual,
         las.arrears_accrual * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))    arrears_accrual,
         las.end_arrears_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                                                end_arrears_accrual,
         nvl(las.EXECUTORY_ADJUST * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )),0)   executory_adjust,
         nvl(las.CONTINGENT_ADJUST * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )),0)  contingent_adjust,
         (
              (las.beg_liability * Nvl( CALC_RATE.prev_rate, cr.rate ))
              - (las.end_liability * Nvl( CALC_RATE.rate, cr.rate ))
              - Decode(lower(sc.CONTROL_VALUE), 'yes',las.interest_accrual * Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), las.interest_accrual * Nvl( CALC_RATE.rate, cr.rate ))
              + Decode(lower(sc.CONTROL_VALUE), 'yes',las.interest_paid * Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), las.interest_paid * Nvl( CALC_RATE.rate, cr.rate ))
              + Decode(lower(sc.CONTROL_VALUE), 'yes',las.principal_paid * Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), las.principal_paid * Nvl( CALC_RATE.rate, cr.rate ))
            ) gain_loss_fx,
         /* Short Term Curr Gain Loss = (beg_liability - beg_lt_liability) - (end_liability - end_lt_liability) - interest_accrual + interest_paid + + principal_paid */
            (
              ((las.beg_liability * Nvl( CALC_RATE.prev_rate, cr.rate )) - (las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate )))
              - ((las.end_liability * Nvl( CALC_RATE.rate, cr.rate )) - (las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )))
              - Decode(lower(sc.CONTROL_VALUE), 'yes',las.interest_accrual * Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), las.interest_accrual * Nvl( CALC_RATE.rate, cr.rate ))
              + Decode(lower(sc.CONTROL_VALUE), 'yes',las.interest_paid * Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), las.interest_paid * Nvl( CALC_RATE.rate, cr.rate ))
              + Decode(lower(sc.CONTROL_VALUE), 'yes',las.principal_paid * Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), las.principal_paid * Nvl( CALC_RATE.rate, cr.rate ))
            ) st_currency_gain_loss,
         /* Long Term Curr Gain Loss = ((beg_lt_liability - end_lt_liability) */
            (
                (las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate ))
              - (las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate ))
            ) lt_currency_gain_loss
  FROM   ( SELECT la.ilr_id,
                  las.ls_asset_id,
                  las.revision,
                  las.set_of_books_id,
                  las.month,
                  la.contract_currency_id,
                  las.residual_amount,
                  las.term_penalty,
                  las.bpo_price,
                  las.beg_capital_cost,
                  las.end_capital_cost,
                  las.beg_obligation,
                  las.end_obligation,
                  las.beg_lt_obligation,
                  las.end_lt_obligation,
                  las.principal_remeasurement,
                  las.beg_liability,
                  las.end_liability,
                  las.beg_lt_liability,
                  las.end_lt_liability,
                  las.liability_remeasurement,
                  las.interest_accrual,
                  las.principal_accrual,
                  las.interest_paid,
                  las.principal_paid,
                  las.executory_accrual1,
                  las.executory_accrual2,
                  las.executory_accrual3,
                  las.executory_accrual4,
                  las.executory_accrual5,
                  las.executory_accrual6,
                  las.executory_accrual7,
                  las.executory_accrual8,
                  las.executory_accrual9,
                  las.executory_accrual10,
                  las.executory_paid1,
                  las.executory_paid2,
                  las.executory_paid3,
                  las.executory_paid4,
                  las.executory_paid5,
                  las.executory_paid6,
                  las.executory_paid7,
                  las.executory_paid8,
                  las.executory_paid9,
                  las.executory_paid10,
                  las.contingent_accrual1,
                  las.contingent_accrual2,
                  las.contingent_accrual3,
                  las.contingent_accrual4,
                  las.contingent_accrual5,
                  las.contingent_accrual6,
                  las.contingent_accrual7,
                  las.contingent_accrual8,
                  las.contingent_accrual9,
                  las.contingent_accrual10,
                  las.contingent_paid1,
                  las.contingent_paid2,
                  las.contingent_paid3,
                  las.contingent_paid4,
                  las.contingent_paid5,
                  las.contingent_paid6,
                  las.contingent_paid7,
                  las.contingent_paid8,
                  las.contingent_paid9,
                  las.contingent_paid10,
                  las.current_lease_cost,
                  las.beg_deferred_rent,
                  las.deferred_rent,
                  las.end_deferred_rent,
                  las.beg_st_deferred_rent,
                  las.end_st_deferred_rent,
                  las.initial_direct_cost,
                  las.incentive_amount,
                  ldf.depr_expense,
                  ldf.begin_reserve,
                  ldf.end_reserve,
                  ldf.depr_exp_alloc_adjust,
                  la.company_id,
                  opt.in_service_exchange_rate,
                  la.description AS asset_description,
                  la.leased_asset_number,
                  la.fmv,
                  las.is_om,
                  la.approved_revision,
                  opt.lease_cap_type_id,
                  la.ls_asset_status_id,
                  la.retirement_date,
                  las.beg_prepaid_rent,
                  las.prepay_amortization,
                  las.prepaid_rent,
                  las.end_prepaid_rent,
                  las.beg_net_rou_asset,
                  las.end_net_rou_asset,
                  la.actual_residual_amount,
                  la.guaranteed_residual_amount,
                  la.estimated_residual,
                  las.rou_asset_remeasurement,
                  las.partial_term_gain_loss,
                  las.beg_arrears_accrual,
                  las.arrears_accrual,
                  las.end_arrears_accrual,
                  las.additional_rou_asset,
                  las.executory_adjust,
                  las.contingent_adjust,
                  ilr_effective_rates.in_service_exchange_rate AS effective_in_svc_rate,
                  First_Value(ilr_effective_rates.in_service_exchange_rate) OVER(PARTITION BY las.ls_asset_id, las.revision, las.set_of_books_id ORDER BY las.MONTH) AS original_in_svc_rate
           FROM   LS_ASSET_SCHEDULE las
                  JOIN LS_ASSET la ON las.ls_asset_id = la.ls_asset_id
                  JOIN LS_ILR_OPTIONS opt ON las.revision = opt.revision AND la.ilr_id = opt.ilr_id
                  left OUTER JOIN LS_DEPR_FORECAST ldf ON las.ls_asset_id = ldf.ls_asset_id
                                                        AND las.revision = ldf.revision
                                                        AND las.set_of_books_id = ldf.set_of_books_id
                                                        AND las.month = ldf.month
                  left OUTER JOIN ilr_effective_rates ON la.ilr_id = ilr_effective_rates.ilr_id
                                                      AND las.revision = ilr_effective_rates.revision
                                                      AND las.month BETWEEN ilr_effective_rates.effective_month AND ilr_effective_rates.end_effective_month
                   ) las
         inner join CURRENCY_SCHEMA cs
                 ON las.company_id = cs.company_id
         inner join cur
                 ON CUR.currency_id = CASE CUR.ls_cur_type
                                        WHEN 1 THEN las.contract_currency_id
                                        WHEN 2 THEN cs.currency_id
                                        ELSE NULL
                                      END
         inner join open_month
                 ON las.company_id = OPEN_MONTH.company_id
         inner join CURRENCY_RATE_DEFAULT_DENSE cr
                 ON CUR.currency_id = cr.currency_to AND
                    las.contract_currency_id = cr.currency_from AND
                    Trunc( cr.exchange_date, 'MONTH' ) = Trunc( las.month, 'MONTH' )
         inner join cr_now
                 ON CUR.currency_id = CR_NOW.currency_to AND
                    las.contract_currency_id = CR_NOW.currency_from
         left outer join calc_rate
                      ON las.contract_currency_id = CALC_RATE.contract_currency_id AND
                         CUR.currency_id = CALC_RATE.company_currency_id AND
                         las.company_id = CALC_RATE.company_id AND
                         las.month = CALC_RATE.accounting_month
         left outer join calc_avg_rate
                 ON las.contract_currency_id = calc_avg_rate.contract_currency_id AND
                    CUR.currency_id = calc_avg_rate.company_currency_id AND
                    las.company_id = calc_avg_rate.company_id AND
                    las.month = calc_avg_rate.accounting_month
         inner join pp_system_control_companies sc
                 ON open_month.company_id = sc.company_id AND
                    lower(trim(sc.control_name)) = 'lease mc: use average rates'
         left outer join war prior_war
                 ON las.ilr_id = prior_war.ilr_id  AND
                    las.revision = prior_war.revision AND
                    las.set_of_books_id = prior_war.set_of_books_id AND
                    add_months(las.month, -1) >= prior_war.effective_month
         left outer join war
                 ON las.ilr_id = war.ilr_id  AND
                    las.revision = war.revision AND
                    las.set_of_books_id = war.set_of_books_id AND
                    las.month >= war.effective_month
         inner join CURRENCY_RATE_DEFAULT_DENSE cr_avg
                 ON CUR.currency_id = cr_avg.currency_to AND
                    las.contract_currency_id = cr_avg.currency_from AND
                    Trunc( cr_avg.exchange_date, 'MONTH' ) = Trunc( las.month, 'MONTH' )
                    AND cr_avg.exchange_rate_type_id = Decode(lower(sc.CONTROL_VALUE), 'yes', 4, 1)
  WHERE  cs.currency_type_id = 1 AND
         cr.exchange_rate_type_id = 1;
		 
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8244, 0, 2017, 4, 0, 0, 51992, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051992_lessee_ilr_asset_schedule_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;