/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052599_lessor_03_lt_receivable_remseasuret_nonnull_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2018.1.0.0  11/15/2018 Anand Rajashekar make long term receivable remeasurement non nullable
||============================================================================
*/

alter table lsr_ilr_schedule
modify lt_receivable_remeasurement NOT NULL;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(11902, 0, 2018, 1, 0, 0, 52599, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052599_lessor_03_lt_receivable_remseasuret_nonnull_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 