/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_042819_cwip_cpiretro_01_ddl.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   3/10/2015   Sunjin Cone      CPI Retro Calc Input adjustments
||============================================================================
*/ 



create table CPI_RETRO_ADJUST_TYPE
(
CPI_RETRO_ADJUST_TYPE_ID number(22,0) not null, 
DESCRIPTION varchar2(35), 
TIME_STAMP date,
USER_ID varchar2(18)
);

alter table CPI_RETRO_ADJUST_TYPE
   add constraint PK_CPIRETRO_ADJTYPE
       primary key (CPI_RETRO_ADJUST_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  CPI_RETRO_ADJUST_TYPE is '(C) [04] A Standard table of the types of monthly input CPI adjustments used in CPI Retro Calculation.';
comment on column CPI_RETRO_ADJUST_TYPE.CPI_RETRO_ADJUST_TYPE_ID is 'System-assigned identifier of the CPI Retro Adjustment Type.  The IDs are standard to the base calculation code.'; 
comment on column CPI_RETRO_ADJUST_TYPE.DESCRIPTION is 'The description of CPI Retro Adjustment Type.';
comment on column CPI_RETRO_ADJUST_TYPE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column CPI_RETRO_ADJUST_TYPE.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table CPI_RETRO_INPUT
(
BATCH_ID number(22,0) not null, 
COMPANY_ID number(22,0) not null, 
MONTH date not null, 
WORK_ORDER_NUMBER  VARCHAR2(35) NOT NULL,
CPI_RETRO_ADJUST_TYPE_ID number(22,0) not null, 
AMOUNT number(22,2), 
WORK_ORDER_ID number(22,0), 
NOTES VARCHAR2(2000), 
TIME_STAMP date,
USER_ID varchar2(18)
);

alter table CPI_RETRO_INPUT
   add constraint PK_CPI_RETRO_INPUT
       primary key (BATCH_ID, COMPANY_ID, MONTH, WORK_ORDER_NUMBER, CPI_RETRO_ADJUST_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

alter table CPI_RETRO_INPUT ADD ( CONSTRAINT R_CPI_RETRO_INPUT1 FOREIGN KEY ( CPI_RETRO_ADJUST_TYPE_ID ) REFERENCES CPI_RETRO_ADJUST_TYPE) ;

comment on table  CPI_RETRO_INPUT is '(C) [04] Stores monthly CPI adjustment amounts used in CPI Retro Calculation.';
comment on column CPI_RETRO_INPUT.BATCH_ID is 'System-assigned identifier of the CPI Retro batch.';
comment on column CPI_RETRO_INPUT.MONTH is 'Month of the CPI Retro calculation.';
comment on column CPI_RETRO_INPUT.WORK_ORDER_ID is 'Work order of the CPI Retro calculation.';
comment on column CPI_RETRO_INPUT.CPI_RETRO_ADJUST_TYPE_ID is 'System-assigned identifier of the CPI Retro Adjustment Type.  The IDs are standard values referenced from the CPI_RETRO_ADJUST_TYPE table.'; 
comment on column CPI_RETRO_INPUT.AMOUNT is 'The amount of the adjustment.';
comment on column CPI_RETRO_INPUT.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column CPI_RETRO_INPUT.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table CPI_RETRO_BATCH_CONTROL add (moend_adjs_reviewed number(22,0));

comment on column CPI_RETRO_BATCH_CONTROL.MOEND_ADJS_REVIEWED is '1= Yes, the CPI input adjustments used for Month End CPI calculations have been reviewed.  The user can choose to copy or ignore those adjustments for CPI Retro calculation';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2362, 0, 2015, 1, 0, 0, 42819, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042819_cwip_cpiretro_01_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;