/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_033541_version-updates.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.2   10/21/2013 Lee Quinn      10.4.0.2 version
||============================================================================
*/

update PP_VERSION
   set PP_VERSION = 'Version 10.4.0.0',
       PP_PATCH   = 'Patch: 10.4.0.2',
       POWERTAX_VERSION = '10.4.0.2',
       DATE_INSTALLED = sysdate
where PP_VERSION_ID = 1
  and SUBSTR(REGEXP_REPLACE(PP_VERSION, '[^0-9]', ''), 1, 4) ||
             NVL(SUBSTR(REGEXP_REPLACE(PP_PATCH, '[^0-9]', ''),
                        LENGTH(REGEXP_REPLACE(PP_PATCH, '[^0-9]', '')),
                        1),
                 0) < 10410;

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
          select '10.4.0.2', 'automatedreview.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'batreporter.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'budget_allocations.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_allocations.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_balances.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_balances_bdg.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_batch_derivation.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_batch_derivation_bdg.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_budget_entry_calc_all.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_build_combos.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_build_deriver.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_delete_budget_allocations.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_financial_reports_build.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_flatten_structures.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_man_jrnl_reversals.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_posting.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_posting_bdg.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_sap_trueup.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_sum.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_sum_ab.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_to_commitments.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cwip_charge.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'populate_matlrec.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'ppmetricbatch.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'pptocr.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'ppverify.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'pp_integration.exe', '10.4.0.0' from dual
union all select '10.4.0.2', 'cr_derivation_trueup.exe', '10.4.0.0' from dual
minus select PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION from PP_VERSION_EXES;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (726, 0, 10, 4, 0, 2, 33541, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.2_maint_033541_version-updates.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
   SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
