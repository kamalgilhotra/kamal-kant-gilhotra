/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047987_lessor_lease_fk_deferrable_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date     Revised By      Reason for Change
|| ---------- -------- --------------  ------------------------------------
|| 2017.1.0.0 9/12/2017 Josh Sandler   Make the lease document to lease foreign key deferrable
||============================================================================
*/

ALTER TABLE lsr_lease_document
  DROP CONSTRAINT fk_lsr_lease_lease_document;

ALTER TABLE lsr_lease_document
ADD CONSTRAINT fk_lsr_lease_lease_document FOREIGN KEY (
  lease_id
) REFERENCES lsr_lease (
  lease_id
)
INITIALLY DEFERRED DEFERRABLE
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3700, 0, 2017, 1, 0, 0, 47987, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047987_lessor_lease_fk_deferrable_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;