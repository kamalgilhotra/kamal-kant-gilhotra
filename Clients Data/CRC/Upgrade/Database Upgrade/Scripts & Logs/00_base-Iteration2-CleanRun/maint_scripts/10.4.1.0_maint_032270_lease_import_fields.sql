/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032270_lease_import_fields.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   09/04/2013 Ryan Oliveria
||============================================================================
*/

delete from PP_IMPORT_TEMPLATE_FIELDS
 where COLUMN_NAME = 'muni_bo_sw'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_%');

--Make field ID's sequential
update PP_IMPORT_TEMPLATE_FIELDS A
   set A.FIELD_ID =
        (select count(1)
           from PP_IMPORT_TEMPLATE_FIELDS B
          where B.IMPORT_TEMPLATE_ID = A.IMPORT_TEMPLATE_ID
            and B.FIELD_ID <= A.FIELD_ID)
 where IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (600, 0, 10, 4, 1, 0, 32270, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032270_lease_import_fields.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;