/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008710_tax_exp.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/28/2011 Elhadj Bah     Point Release
||============================================================================
*/

-- ** Make required changes forthe CPR method
alter table REPAIR_WORK_ORDER_TEMP_ORIG add REPAIR_QTY_INCLUDE number(22,2) default 1;

alter table REPAIR_WORK_ORDER_TEMP add REPAIR_QTY_INCLUDE number(22,2) default 1;

alter table REPAIR_WORK_ORDER_SEGMENTS add REPAIR_QTY_INCLUDE number(22,2) default 1;
alter table REPAIR_WORK_ORDER_SEGMENTS drop primary key drop index;
alter table REPAIR_WORK_ORDER_SEGMENTS
   add constraint PK_REPAIR_WORK_ORDER_SEGMENTS
       primary key (REPAIR_SCHEMA_ID, COMPANY_ID, WORK_ORDER_NUMBER,
                    WORK_REQUEST, REPAIR_UNIT_CODE_ID, REPAIR_LOCATION_ID,
                    REPAIR_METHOD_ID, RETIREMENT_UNIT_ID, REPAIR_QTY_INCLUDE)
       using index tablespace PWRPLANT_IDX;

create table D1_REPAIR_WO_SEG_REPORTING as select * from REPAIR_WO_SEG_REPORTING;
drop table REPAIR_WO_SEG_REPORTING;
create table REPAIR_WO_SEG_REPORTING
(REPAIR_SCHEMA_ID              NUMBER(22,0),
 COMPANY_ID                    NUMBER(22,0),
 WORK_ORDER_NUMBER             VARCHAR2(35),
 REPAIR_UNIT_CODE_ID           NUMBER(22,0),
 REPAIR_LOCATION_ID            NUMBER(22,0),
 REPAIR_METHOD_ID              NUMBER(22,0),
 ADD_QUANTITY                  NUMBER(22,0),
 RETIRE_QUANTITY               NUMBER(22,0),
 ADD_COST                      NUMBER(22,2),
 RETIRE_COST                   NUMBER(22,2),
 MIN_VINTAGE                   NUMBER(22,0),
 MAX_VINTAGE                   NUMBER(22,0),
 PRE81_QUANTITY                NUMBER(22,0),
 POST80_QUANTITY               NUMBER(22,0),
 MY_CONDITION                  VARCHAR2(35),
 MY_EVALUATE                   NUMBER(1,0),
 MY_USE_VINTAGE                NUMBER(1,0),
 MY_ADD_TEST                   NUMBER(1,0),
 MY_RETIRE_TEST                NUMBER(1,0),
 MY_FACTOR                     NUMBER(22,8),
 MY_QTY_FACTOR                 NUMBER(22,8),
 MY_USE_RATIO                  NUMBER(1,0),
 MY_USE_REPLACE                NUMBER(1,0),
 MY_QTY_OR_DOLLARS             NUMBER(1,0),
 MY_ADD_AND_RETIRE_TEST        NUMBER(1,0),
 MY_PRE81_RETIREMENT_RATIO     NUMBER(22,8),
 MY_TEST_QUANTITY              NUMBER(22,2),
 MY_TEST_QUANTITY2             NUMBER(22,2),
 MY_ADD_THRESHOLD              NUMBER(22,0),
 MY_RETIRE_THRESHOLD           NUMBER(22,0),
 MY_UNIT_CODE_RATIO            NUMBER(22,8),
 MY_REPAIR_ELIGIBLE_AMOUNT     NUMBER(22,2),
 USER_ID                       VARCHAR2(18),
 TIME_STAMP                    DATE,
 WORK_REQUEST                  VARCHAR2(35),
 CPI_ADD_COST                  NUMBER(22,2),
 RETIREMENT_UNIT_ID            NUMBER(22,0) NOT NULL,
 MY_CPI_ELIGIBLE_AMOUNT        NUMBER(22,2),
 MY_EXPENSE_REPLACE            NUMBER(22,0),
 MY_RETIREMENT_ELIGIBLE_AMOUNT NUMBER(22,2),
 REPAIR_QTY_INCLUDE            NUMBER(22,2),
 BATCH_ID                      NUMBER(22,0)
);

-- ** Make required changes forthe WMIS method
alter table REPAIR_DETAIL_WMIS_FEED add REPAIR_QTY_INCLUDE number(22,2);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (57, 0, 10, 3, 3, 0, 8710, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008710_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
