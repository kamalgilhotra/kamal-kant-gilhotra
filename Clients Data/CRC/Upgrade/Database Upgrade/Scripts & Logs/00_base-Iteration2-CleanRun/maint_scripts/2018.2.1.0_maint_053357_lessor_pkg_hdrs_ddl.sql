/*
||============================================================================
|| Application: PowerPlan
|| Object Name: 2018.2.1.0_maint_053357_lessor_pkg_hdrs_ddl.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2018.2.1.0  4/23/2019  B.Beck         Package Headerss
||============================================================================
*/

CREATE OR REPLACE PACKAGE pkg_lessor_common AS
  G_PKG_VERSION varchar(35) := '2018.2.0.0';
  procedure p_startLog (a_process_description varchar2);
  
  -- function the return the process id for lessor calcs
  FUNCTION f_get_process_id(a_description pp_processes.description%TYPE)
    RETURN pp_processes.process_id%TYPE;
  
  --overlaoaded function with A_ORIG for the ORIGINATOR column on GL_TRANSACTION
   function F_BOOKJE(A_LSR_ILR_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_JE_METHOD_ID    in number,
                     A_AMOUNT_TYPE     in number,
                     A_REVERSAL_CONVENTION in number,
                     A_ORIG          in varchar2,
                     A_MSG           out varchar2) return number;

   -- Function to perform booking of LESSEE JEs (Multicurrencies)
   function F_BOOKJE(A_LSR_ILR_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
                        A_JE_METHOD_ID    in number,
                        A_AMOUNT_TYPE     in number,
                        A_REVERSAL_CONVENTION in number,
                        A_MSG           out varchar2) return number;


   -- Function to perform booking of LESSOR JEs (Multicurrencies)
   function F_MC_BOOKJE(A_LSR_ILR_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
                        A_JE_METHOD_ID    in number,
                        A_AMOUNT_TYPE     in number,
                        A_REVERSAL_CONVENTION in number,
                        A_EXCHANGE_RATE in number,
                        A_CURRENCY_FROM in number,
                        A_CURRENCY_TO   in number,
                        A_MSG           out varchar2) return number;

   --overlaoaded function with A_ORIG for the ORIGINATOR column on GL_TRANSACTION (Multicurrencies)
   function F_MC_BOOKJE(A_LSR_ILR_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
                        A_JE_METHOD_ID    in number,
                        A_AMOUNT_TYPE     in number,
                        A_REVERSAL_CONVENTION in number,
                        A_EXCHANGE_RATE in number,
                        A_CURRENCY_FROM in number,
                        A_CURRENCY_TO   in number,
                        A_ORIG          in varchar2,
                        A_MSG           out varchar2) return number;
    
  TYPE ilr_schedule_line IS RECORD (
    ilr_id          lsr_ilr_schedule_sales_direct.ilr_id%TYPE,
    set_of_books_id     lsr_ilr_schedule_sales_direct.set_of_books_id%TYPE,
    rownumber       INTEGER,
    company_id        lsr_ilr.company_id%type,
    month           lsr_ilr_schedule.month%type,
    principal_received    lsr_ilr_schedule_sales_direct.principal_received%TYPE,
    interest_income       lsr_ilr_schedule.interest_income_received%TYPE,
    interest_unguaranteed lsr_ilr_schedule_sales_direct.interest_unguaranteed_residual %TYPE,
    recognized_profit     lsr_ilr_schedule_direct_fin.recognized_profit %TYPE,
    initial_direct_cost   lsr_ilr_schedule.initial_direct_cost %TYPE,
    executory1        lsr_ilr_schedule.executory_paid1%TYPE,
    executory2        lsr_ilr_schedule.executory_paid2%TYPE,
    executory3        lsr_ilr_schedule.executory_paid3%TYPE,
    executory4        lsr_ilr_schedule.executory_paid4%TYPE,
    executory5        lsr_ilr_schedule.executory_paid5%TYPE,
    executory6        lsr_ilr_schedule.executory_paid6%TYPE,
    executory7        lsr_ilr_schedule.executory_paid7%TYPE,
    executory8        lsr_ilr_schedule.executory_paid8%TYPE,
    executory9        lsr_ilr_schedule.executory_paid9%TYPE,
    executory10       lsr_ilr_schedule.executory_paid10%TYPE,
    contingent1       lsr_ilr_schedule.contingent_paid1%TYPE,
    contingent2       lsr_ilr_schedule.contingent_paid2%TYPE,
    contingent3       lsr_ilr_schedule.contingent_paid3%TYPE,
    contingent4       lsr_ilr_schedule.contingent_paid4%TYPE,
    contingent5       lsr_ilr_schedule.contingent_paid5%TYPE,
    contingent6       lsr_ilr_schedule.contingent_paid6%TYPE,
    contingent7       lsr_ilr_schedule.contingent_paid7%TYPE,
    contingent8       lsr_ilr_schedule.contingent_paid8%TYPE,
    contingent9       lsr_ilr_schedule.contingent_paid9%TYPE,
    contingent10      lsr_ilr_schedule.contingent_paid10%TYPE,
    accrued_rent      lsr_ilr_schedule.accrued_rent%TYPE,
    deferred_rent     lsr_ilr_schedule.deferred_rent%TYPE
  );
  
  
  
  TYPE ilr_schedule_line_table IS TABLE OF ilr_schedule_line INDEX BY PLS_INTEGER;
  
  
  TYPE asset_rerecognition IS RECORD (
    ldg_asset_id        pend_transaction.ldg_asset_id%TYPE,
    ldg_activity_id     pend_transaction.ldg_activity_id%TYPE,
    ldg_depr_group_id   pend_transaction.ldg_depr_group_id%TYPE,
    books_schema_id     pend_transaction.books_schema_id%TYPE,
    retirement_unit_id    pend_transaction.retirement_unit_id%TYPE,
    utility_account_id    pend_transaction.utility_account_id%TYPE, 
    bus_segment_id      pend_transaction.bus_segment_id%TYPE, 
    func_class_id     pend_transaction.func_class_id%TYPE, 
    sub_account_id      pend_transaction.sub_account_id%TYPE, 
    asset_location_id   pend_transaction.asset_location_id%TYPE, 
    gl_account_id     pend_transaction.gl_account_id%TYPE, 
    company_id        pend_transaction.company_id%TYPE,
    gl_posting_mo_yr    pend_transaction.gl_posting_mo_yr%TYPE,
    subledger_indicator   pend_transaction.subledger_indicator%TYPE, 
    activity_code     pend_transaction.activity_code%TYPE, 
    work_order_number   pend_transaction.work_order_number%TYPE, 
    posting_quantity    pend_transaction.posting_quantity%TYPE,
    user_id1        pend_transaction.user_id1%TYPE,
    posting_amount      pend_transaction.posting_amount%TYPE,
    in_service_year     pend_transaction.in_service_year%TYPE,
    description       pend_transaction.description%TYPE,
    long_description    pend_transaction.long_description%TYPE,
    property_group_id   pend_transaction.property_group_id%TYPE,
    retire_method_id    pend_transaction.retire_method_id%TYPE,
    posting_status      pend_transaction.posting_status%TYPE,
    cost_of_removal     pend_transaction.cost_of_removal%TYPE,
    salvage_cash      pend_transaction.salvage_cash%TYPE,
    salvage_returns     pend_transaction.salvage_returns%TYPE,
    reserve         pend_transaction.reserve%TYPE,
    misc_description    pend_transaction.misc_description%TYPE,
    ferc_activity_code    pend_transaction.ferc_activity_code%TYPE,
    serial_number     pend_transaction.serial_number%TYPE,
    reserve_credits     pend_transaction.reserve_credits%TYPE,
    gain_loss_reversal    pend_transaction.gain_loss_reversal%TYPE,
    disposition_code    pend_transaction.disposition_code%TYPE,
    asset_activity_id   cpr_activity.asset_activity_id%TYPE,
    orig_posting_amount   pend_transaction.posting_amount%TYPE,
    set_of_books_id       set_of_books.set_of_books_id%TYPE
  );
  TYPE asset_rerecognition_table IS TABLE OF asset_rerecognition INDEX BY PLS_INTEGER;
  
  /*****************************************************************************
  * Procedure: p_rerecognize_cpr_assets
  * PURPOSE: Creates pending transactions for POST to pick up to re-recognize an asset after it had been
  *         de-recognized due to creating a Sales Type or Direct Finance Lease
  * PARAMETERS:
  *   a_asset_rerecognition_tbl: A table of ILR records to re-recognize
  *
  * RETURNS: NONE
  ******************************************************************************/
  PROCEDURE p_rerecognize_cpr_assets(a_asset_rerecognition_tbl pkg_lessor_common.asset_rerecognition_table); 
  
END pkg_lessor_common;
/

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (17342, 0, 2018, 2, 1, 0, 53357 , 'C:\PlasticWks\powerplant\sql\maint_scripts','2018.2.1.0_maint_053357_lessor_pkg_hdrs_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
  SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
