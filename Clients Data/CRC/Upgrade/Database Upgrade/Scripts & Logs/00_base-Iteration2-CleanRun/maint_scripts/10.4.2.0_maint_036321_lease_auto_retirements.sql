/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036321_lease_auto_retirements.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/17/2014 Kyle Peterson  Point Release
||============================================================================
*/

alter table LS_PROCESS_CONTROL rename column PAYMENT_INVOICE_RECON to AUTO_RETIREMENTS;

update LS_PROCESS_CONTROL set AUTO_RETIREMENTS = DECODE(OPEN_NEXT, null, null, sysdate);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (967, 0, 10, 4, 2, 0, 36321, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036321_lease_auto_retirements.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;