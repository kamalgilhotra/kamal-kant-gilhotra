/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045616_2_reg_stat_ledger_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2016.1.0.0 04/04/2016 Sarah Byers		Create RMS Statistic Ledger Data
||============================================================================
*/

-- REG_ACCT_TYPE
insert into reg_acct_type (
	reg_acct_type_id, description, long_description, sort_order)
values (
	12, 'Statistical Accounts', 'Accounts generated from Statistical Values', 12);

-- REG_SOURCE
insert into reg_source (
	reg_source_id, description, non_standard)
values (
	50, 'Statistic', 0);

-- REG_INTERFACE
insert into reg_interface (
	reg_interface_id, description, act_or_fcst_type, long_description, button_text)
values (
	50, 'Statistics', 1, 'Statistics', 'Run Statistics');

-- Workspaces
insert into ppbase_workspace (
	module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values (
	'REG', 'uo_reg_statistic', 'Regulatory Statistics', 'uo_reg_statistic', 'Regulatory Statistics', 1);

insert into ppbase_workspace (
	module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values (
	'REG', 'uo_reg_stat_ledger_ws', 'Reg Statistics Ledger - Input', 'uo_reg_stat_ledger_ws', 'Reg Statistics Ledger - Input', 1);

insert into ppbase_workspace (
	module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values (
	'REG', 'uo_reg_statistic_acct_ws', 'Reg Statistics Accounts', 'uo_reg_statistic_acct_ws', 'Reg Statistics Accounts', 1);

-- Menu Items
insert into ppbase_menu_items (
	module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values(
	'REG', 'STATISTIC', 2, 8, 'Statistics Setup', 'Statistics Setup', 'SETUP', null, 1);

insert into ppbase_menu_items (
	module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values(
	'REG', 'STAT_MAINT', 3, 1, 'Statistics', 'Statistics', 'STATISTIC', 'uo_reg_statistic', 1);

insert into ppbase_menu_items (
	module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values(
	'REG', 'STAT_LEDGER', 3, 2, 'Stat Ledger Input', 'Stat Ledger Input', 'STATISTIC', 'uo_reg_stat_ledger_ws', 1);

insert into ppbase_menu_items (
	module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values(
	'REG', 'STAT_IMPORT', 3, 3, 'Statistics Import', 'Statistics Import', 'STATISTIC', 'uo_reg_import_ws', 1);

insert into ppbase_menu_items (
	module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values(
	'REG', 'STAT_ACCT', 3, 4, 'Statistics Accounts', 'Statistics Accounts', 'STATISTIC', 'uo_reg_statistic_acct_ws', 1);
	

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3155, 0, 2016, 1, 0, 0, 045616, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045616_2_reg_stat_ledger_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;