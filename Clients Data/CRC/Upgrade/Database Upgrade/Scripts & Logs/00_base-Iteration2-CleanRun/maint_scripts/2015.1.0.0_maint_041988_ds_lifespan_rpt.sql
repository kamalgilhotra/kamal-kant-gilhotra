/*
||==========================================================================================
|| Application: PowerPlan
|| Module:      Depr Studies
|| File Name:   maint_041988_ds_lifespan_rpt.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     01/07/2015 Andrew Scott        Depr Studies users need access to the Life Span
||                                           Analysis report window created in 2012.
||==========================================================================================
*/

----place in a pl/sql block to keep the upgrade tool from erroring out.  This is a "nice to have",
----but should not blow up an upgrade.  implementors can always manipulate contents of pp interface
----for their client as needed.
begin

   delete from pp_interface
   where window = 'w_ds_trunc_analysis'
   and subsystem = 'Depreciation Study';

   insert into pp_interface
   (interface_id, company_id, description, subsystem, window, needed_for_closing)
   select interface_id, company_id, 'Life Span Analysis', 'Depreciation Study','w_ds_trunc_analysis',0
   from 
   (select min(company_id) company_id from company_setup),
   (select ( nvl(max(interface_id),0) - nvl(mod(max(interface_id),1000),0) + 1001 ) interface_id from pp_interface);

exception
   when others then
      DBMS_OUTPUT.PUT_LINE('No company available for the insert.  This may be okay.  Continuing.');
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2144, 0, 2015, 1, 0, 0, 41988, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041988_ds_lifespan_rpt.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;