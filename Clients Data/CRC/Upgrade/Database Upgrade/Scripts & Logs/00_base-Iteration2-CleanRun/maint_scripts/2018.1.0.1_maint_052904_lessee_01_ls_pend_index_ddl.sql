/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052904_lessee_01_ls_pend_index_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.1 12/26/2018 C.Yura         Add missing index to prevent deadlocks
||============================================================================
*/
CREATE INDEX PWRPLANT.LS_PEND_CLASS_CODE_PTID_IDX ON LS_PEND_CLASS_CODE (LS_PEND_TRANS_ID);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13342, 0, 2018, 1, 0, 1, 52904, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.1_maint_052904_lessee_01_ls_pend_index_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;