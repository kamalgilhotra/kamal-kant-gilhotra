/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009517_009518_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/17/2012 Julia Breuer   Point Release
||============================================================================
*/

alter table PT_LEDGER_TRANSFER
   add (BEG_BAL_PCT     number(22,8) default 0,
        ADDITIONS_PCT   number(22,8) default 0,
        RETIREMENTS_PCT number(22,8) default 0,
        TRANSFERS_PCT   number(22,8) default 0,
        END_BAL_PCT     number(22,8) default 0,
        CWIP_PCT        number(22,8) default 0,
        TAX_BASIS_PCT   number(22,8) default 0,
        RESERVE_PCT     number(22,8) default 0);

alter table PT_TEMP_LEDGER_TRANSFERS
   add (BEG_BAL_PCT     number(22,8) default 0,
        ADDITIONS_PCT   number(22,8) default 0,
        RETIREMENTS_PCT number(22,8) default 0,
        TRANSFERS_PCT   number(22,8) default 0,
        END_BAL_PCT     number(22,8) default 0,
        CWIP_PCT        number(22,8) default 0,
        TAX_BASIS_PCT   number(22,8) default 0,
        RESERVE_PCT     number(22,8) default 0);

alter table PT_PREALLO_TRANSFER
   add (BEG_BAL_PCT     number(22,8) default 0,
        ADDITIONS_PCT   number(22,8) default 0,
        RETIREMENTS_PCT number(22,8) default 0,
        TRANSFERS_PCT   number(22,8) default 0,
        END_BAL_PCT     number(22,8) default 0,
        CWIP_PCT        number(22,8) default 0);

alter table pt_temp_preallo_transfers
   add (BEG_BAL_PCT     number(22,8) default 0,
        ADDITIONS_PCT   number(22,8) default 0,
        RETIREMENTS_PCT number(22,8) default 0,
        TRANSFERS_PCT   number(22,8) default 0,
        END_BAL_PCT     number(22,8) default 0,
        CWIP_PCT        number(22,8) default 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (183, 0, 10, 3, 5, 0, 9517, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009517_009518_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
