/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036017_reg_multi_calc.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 02/10/2014  Sarah Byers
||============================================================================
*/

-- REG_MULTI_CALC_PARM
create table REG_MULTI_CALC_PARM
(
 PARAMETER_ID           number(22,0) not null,
 DESCRIPTION            varchar2(50) not null,
 TABLE_NAME             varchar2(30) not null,
 COLUMN_NAME            varchar2(30) not null,
 REG_FIN_MONITOR_KEY_ID number(22,0),
 USER_ID                varchar2(18),
 TIME_STAMP             date
);

alter table REG_MULTI_CALC_PARM
   add constraint PK_REG_MULTI_CALC_PARM
       primary key (PARAMETER_ID)
       using index tablespace PWRPLANT_IDX;

-- Foreign Keys
alter table REG_MULTI_CALC_PARM
   add constraint R_REG_MULTI_CALC_PARM1
       foreign key (REG_FIN_MONITOR_KEY_ID)
       references REG_FIN_MONITOR_KEY (REG_FIN_MONITOR_KEY_ID);

-- Comments
comment on table PWRPLANT.REG_MULTI_CALC_PARM is 'The Reg Multi Calc Parm table identifies the parameters available for use in the Case Multi-Calculation Automated functionality.';
comment on column PWRPLANT.REG_MULTI_CALC_PARM.PARAMETER_ID is 'System assigned identifier for the parameter.' ;
comment on column PWRPLANT.REG_MULTI_CALC_PARM.DESCRIPTION is 'Description of the parameter.' ;
comment on column PWRPLANT.REG_MULTI_CALC_PARM.TABLE_NAME is 'The table name of the case table that stores the result or value for the parameter.' ;
comment on column PWRPLANT.REG_MULTI_CALC_PARM.COLUMN_NAME is 'The column name of the field on the case table that stores the result or value for the parameter.' ;
comment on column PWRPLANT.REG_MULTI_CALC_PARM.REG_FIN_MONITOR_KEY_ID is 'System assigned identifier of the monitoring key that corresponds to the parameter.' ;
comment on column PWRPLANT.REG_MULTI_CALC_PARM.USER_ID is 'Standard system-assigned user id used for audit purposes.' ;
comment on column PWRPLANT.REG_MULTI_CALC_PARM.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.' ;

-- Delivered values
insert into REG_MULTI_CALC_PARM
   (PARAMETER_ID, DESCRIPTION, TABLE_NAME, COLUMN_NAME, REG_FIN_MONITOR_KEY_ID)
values
   (1, 'ROE %', 'reg_case_return_result', 'key_value', 1010);

insert into REG_MULTI_CALC_PARM
   (PARAMETER_ID, DESCRIPTION, TABLE_NAME, COLUMN_NAME, REG_FIN_MONITOR_KEY_ID)
values
   (2, 'Capital Structure Weighting %', 'reg_case_cost_of_capital', 'cap_structure_percent', null);

insert into REG_MULTI_CALC_PARM
   (PARAMETER_ID, DESCRIPTION, TABLE_NAME, COLUMN_NAME, REG_FIN_MONITOR_KEY_ID)
values
   (3, 'Rate Base (+/- % change)', 'reg_case_return_result', 'key_value', 199);

-- REG_CASE_MULTI_CALC_LABEL
create table REG_CASE_MULTI_CALC_LABEL
(
 REG_CASE_ID       number(22,0) not null,
 REG_LABEL_ID      number(22,0) not null,
 MONTH_YEAR        number(6,0) not null,
 PARAMETER_ID      number(22,0) not null,
 REG_RETURN_TAG_ID number(22,0) not null,
 DESCRIPTION       varchar2(254) not null,
 LOW_VALUE         number(22,8) not null,
 HIGH_VALUE        number(22,8) not null,
 INCREMENT_AMOUNT  number(22,8) not null,
 USER_ID           varchar2(18),
 TIME_STAMP        date
);

alter table REG_CASE_MULTI_CALC_LABEL
   add constraint PK_REG_CASE_MULTI_CALC_LABEL
       primary key (REG_CASE_ID, REG_LABEL_ID, MONTH_YEAR)
       using index tablespace PWRPLANT_IDX;

-- Foreign Keys
alter table REG_CASE_MULTI_CALC_LABEL
   add constraint R_REG_CASE_MULTI_CALC_LABEL1
       foreign key (REG_CASE_ID)
       references REG_CASE (REG_CASE_ID);

alter table REG_CASE_MULTI_CALC_LABEL
   add constraint R_REG_CASE_MULTI_CALC_LABEL2
       foreign key (PARAMETER_ID)
       references REG_MULTI_CALC_PARM (PARAMETER_ID);

alter table REG_CASE_MULTI_CALC_LABEL
   add constraint R_REG_CASE_MULTI_CALC_LABEL3
       foreign key (REG_CASE_ID, REG_RETURN_TAG_ID)
       references REG_CASE_MONITOR_CONTROL (REG_CASE_ID, REG_RETURN_TAG_ID);

-- Comments
comment on table PWRPLANT.REG_CASE_MULTI_CALC_LABEL is 'The Reg Case Multi Calc Label table saves references to user defined calculation parameters for each Multi-Calculation within a case.';
comment on column PWRPLANT.REG_CASE_MULTI_CALC_LABEL.REG_CASE_ID is 'System assigned identifier for the reg case.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_LABEL.REG_LABEL_ID is 'System assigned identifier for the calculation.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_LABEL.PARAMETER_ID is 'System assigned identifier for the parameter to be used in the calculation.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_LABEL.DESCRIPTION is 'The description of the calculation where the default is <return tag> <parameter> from <low> to <high> by <increment>, for example, "PSC ROE % from 9.50% to 11.00% by 0.25%".' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_LABEL.LOW_VALUE is 'The lowest value of the parameter in the range used for the calculation.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_LABEL.HIGH_VALUE is 'The highest value of the parameter in the range used for the calculation.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_LABEL.INCREMENT_AMOUNT is 'The increment used to move between the lowest and highest value for the calculation.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_LABEL.REG_RETURN_TAG_ID is 'System assigned identifier for the return tag (category + target) used for the calculation.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_LABEL.MONTH_YEAR is 'Month Year for the calculation in YYYYMM format.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_LABEL.USER_ID is 'Standard system-assigned user id used for audit purposes.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_LABEL.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.' ;

-- REG_CASE_MULTI_CALC_RESULT
create table REG_CASE_MULTI_CALC_RESULT
(
 REG_CASE_ID        number(22,0) not null,
 REG_RETURN_TAG_ID  number(22,0) not null,
 REG_LABEL_ID       number(22,0) not null,
 MONTH_YEAR         number(6,0) not null,
 INCREMENT_VALUE    number(22,8) not null,
 NET_INVESTMENT     number(22,2) not null,
 OPERATING_REVENUES number(22,2) not null,
 OPERATING_EXPENSES number(22,2) not null,
 INTEREST_EXPENSE   number(22,2) not null,
 INCOME_TAXES       number(22,2) not null,
 NET_INCOME         number(22,2) not null,
 COMMON_EQUITY      number(22,2) not null,
 EQUITY_RETURN      number(22,8) not null,
 USER_ID            varchar2(18),
 TIME_STAMP         date
);

alter table REG_CASE_MULTI_CALC_RESULT
   add constraint PK_REG_CASE_MULTI_CALC_RESULT
       primary key (REG_CASE_ID, REG_RETURN_TAG_ID, REG_LABEL_ID, MONTH_YEAR, INCREMENT_VALUE)
       using index tablespace PWRPLANT_IDX;

-- Foreign Keys
alter table REG_CASE_MULTI_CALC_RESULT
   add constraint R_REG_CASE_MULTI_CALC_RESULT1
       foreign key (REG_CASE_ID)
       references REG_CASE (REG_CASE_ID);

alter table REG_CASE_MULTI_CALC_RESULT
   add constraint R_REG_CASE_MULTI_CALC_RESULT2
       foreign key (REG_CASE_ID, REG_LABEL_ID, MONTH_YEAR)
       references REG_CASE_MULTI_CALC_LABEL (REG_CASE_ID, REG_LABEL_ID, MONTH_YEAR);

alter table REG_CASE_MULTI_CALC_RESULT
   add constraint R_REG_CASE_MULTI_CALC_RESULT3
       foreign key (REG_CASE_ID, REG_RETURN_TAG_ID)
       references REG_CASE_MONITOR_CONTROL (REG_CASE_ID, REG_RETURN_TAG_ID);

-- Comments
comment on table PWRPLANT.REG_CASE_MULTI_CALC_RESULT is 'The Reg Case Multi Calc Result table stores results by label, monitoring key, month year, and increment for each Multi-Calculation within a case.';
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.REG_CASE_ID is 'System assigned identifier for the reg case.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.REG_RETURN_TAG_ID is 'System assigned identifier for the return tag (category + target) used for the calculation.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.REG_LABEL_ID is 'System assigned identifier for the calculation.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.MONTH_YEAR is 'Month Year for the calculation in YYYYMM format.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.INCREMENT_VALUE is 'The incremental value used for the parameter to generate the result.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.NET_INVESTMENT is 'The resultant Net Investment amount shown in dollars.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.OPERATING_REVENUES is 'The resultant Operating Revenues amount shown in dollars.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.OPERATING_EXPENSES is 'The resultant Operating Expenses amount shown in dollars.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.INTEREST_EXPENSE is 'The resultant Interest Expense amount shown in dollars.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.INCOME_TAXES is 'The resultant Income Taxes amount shown in dollars.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.NET_INCOME is 'The resultant Net Income amount shown in dollars.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.COMMON_EQUITY is 'The resultant Common Equity amount shown in dollars.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.EQUITY_RETURN is 'The resultant Return on Equity Percentage.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.USER_ID is 'Standard system-assigned user id used for audit purposes.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_RESULT.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.' ;

-- REG_CASE_MULTI_CALC_CAP_STRUCT
create table REG_CASE_MULTI_CALC_CAP_STRUCT
(
 REG_CASE_ID     number(22,0) not null,
 REG_LABEL_ID    number(22,0) not null,
 MONTH_YEAR      number(6,0)  not null,
 PRIMARY_CS_ID   number(22,0) not null,
 SECONDARY_CS_ID number(22,0) not null,
 USER_ID         varchar2(18),
 TIME_STAMP      date
);

alter table REG_CASE_MULTI_CALC_CAP_STRUCT
   add constraint PK_REG_CASE_MC_CAP_STRUCT
       primary key (REG_CASE_ID, REG_LABEL_ID, MONTH_YEAR)
       using index tablespace PWRPLANT_IDX;

-- Foreign Keys
alter table REG_CASE_MULTI_CALC_CAP_STRUCT
   add constraint R_REG_CASE_MC_CAP_STRUCT1
       foreign key (REG_CASE_ID)
       references REG_CASE (REG_CASE_ID);

alter table REG_CASE_MULTI_CALC_CAP_STRUCT
   add constraint R_REG_CASE_MC_CAP_STRUCT2
       foreign key (REG_CASE_ID, REG_LABEL_ID, MONTH_YEAR)
       references REG_CASE_MULTI_CALC_LABEL (REG_CASE_ID, REG_LABEL_ID, MONTH_YEAR);

alter table REG_CASE_MULTI_CALC_CAP_STRUCT
   add constraint R_REG_CASE_MC_CAP_STRUCT3
       foreign key (PRIMARY_CS_ID)
       references REG_CAPITAL_STRUCTURE_CONTROL (CAPITAL_STRUCTURE_ID);

alter table REG_CASE_MULTI_CALC_CAP_STRUCT
   add constraint R_REG_CASE_MC_CAP_STRUCT4
       foreign key (SECONDARY_CS_ID)
       references REG_CAPITAL_STRUCTURE_CONTROL (CAPITAL_STRUCTURE_ID);

-- Comments
comment on table PWRPLANT.REG_CASE_MULTI_CALC_CAP_STRUCT is 'The Reg Case Multi Calc Cap Struct table saves references to the Capital Structure items to be adjusted when the parameter is Capital Structure Weighting for a Multi-Calculation within a case.';
comment on column PWRPLANT.REG_CASE_MULTI_CALC_CAP_STRUCT.REG_CASE_ID is 'System assigned identifier for the reg case.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_CAP_STRUCT.REG_LABEL_ID is 'System assigned identifier for the calculation.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_CAP_STRUCT.PRIMARY_CS_ID is 'System assigned identifier for the first capital structure item to be adjusted.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_CAP_STRUCT.SECONDARY_CS_ID is 'System assigned identifier for the second capital structure item to be adjusted.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_CAP_STRUCT.USER_ID is 'Standard system-assigned user id used for audit purposes.' ;
comment on column PWRPLANT.REG_CASE_MULTI_CALC_CAP_STRUCT.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.' ;

-- Add workspace
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_multi_calc_ws', 'Case Multi-Calculation', 'uo_reg_multi_calc_ws',
    'Case Multi-Calculation');

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 16
 where MODULE = 'REG'
   and MENU_IDENTIFIER = 'SELECT_CASE';

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER,
    WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('REG', 'MULTI_CALC', 2, 15, 'Case Multi-Calculation', 'Case Multi-Calculation', 'RATE_CASES',
    'uo_reg_multi_calc_ws', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1016, 0, 10, 4, 2, 0, 36017, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036017_reg_multi_calc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;