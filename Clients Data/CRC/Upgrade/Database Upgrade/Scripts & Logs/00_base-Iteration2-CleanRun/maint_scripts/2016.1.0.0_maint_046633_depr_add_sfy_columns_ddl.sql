SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_046633_depr_add_sfy_columns_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 10/12/2016 Charlie Shilling add SFY columns to depr_calc_stg and depr_calc_stg_arc
||============================================================================
*/

--This is a PLSQL Script with functions to add and drop columns from tables in the DB
declare
   procedure ADD_COLUMN(V_TABLE_NAME varchar2,
                        V_COL_NAME   varchar2,
                        V_DATATYPE   varchar2,
                        V_COMMENT    varchar2) is
   begin
      begin
         execute immediate 'alter table ' || V_TABLE_NAME || ' add ' || V_COL_NAME || ' ' ||
                           V_DATATYPE;
         DBMS_OUTPUT.PUT_LINE('Sucessfully added column ' || V_COL_NAME || ' to table ' ||
                              V_TABLE_NAME || '.');
      exception
         when others then
            if sqlcode = -1430 then
               --1430 is "column being added already exists in table", so we are good here
               DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' already exists on table ' ||
                                    V_TABLE_NAME || '. No action necessasry.');
            else
               RAISE_APPLICATION_ERROR(-20001,
                                       'Could not add column ' || V_COL_NAME || ' to ' ||
                                       V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
            end if;
      end;

      begin
         execute immediate 'comment on column ' || V_TABLE_NAME || '.' || V_COL_NAME || ' is ''' ||
                           V_COMMENT || '''';
         DBMS_OUTPUT.PUT_LINE('	Sucessfully added the comment to column ' || V_COL_NAME ||
                              ' to table ' || V_TABLE_NAME || '.');
      exception
         when others then
            RAISE_APPLICATION_ERROR(-20002,
                                    'Could not add comment to column ' || V_COL_NAME || ' to ' ||
                                    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
      end;
   end ADD_COLUMN;
begin
   --add sinking_fund_base
   ADD_COLUMN('depr_calc_stg',
              'sinking_fund_base',
              'number(22,2) default 0',
              'Calculation column used to determine the sinking fund base amount for SFY depr_groups.');
   ADD_COLUMN('depr_calc_stg_arc',
              'sinking_fund_base',
              'number(22,2) default 0',
              'Calculation column used to determine the sinking fund base amount for SFY depr_groups.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'sinking_fund_base',
              'number(22,2) default 0',
              'Calculation column used to determine the sinking fund base amount for SFY depr_groups.');

   --add sinking_fund_provision
   ADD_COLUMN('depr_calc_stg',
              'sinking_fund_provision',
              'number(22,2) default 0',
              'Calculation column used to determine the sinking fund provision amount for SFY depr_groups.');
   ADD_COLUMN('depr_calc_stg_arc',
              'sinking_fund_provision',
              'number(22,2) default 0',
              'Calculation column used to determine the sinking fund provision amount for SFY depr_groups.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'sinking_fund_provision',
              'number(22,2) default 0',
              'Calculation column used to determine the sinking fund provision amount for SFY depr_groups.');

   --add sinking_fund_depr_exp
   ADD_COLUMN('depr_calc_stg',
              'sinking_fund_depr_exp',
              'number(22,2) default 0',
              'Used to store the original calculated sinking fund expense for audit purposes since the finAL deprecation_expense column = original expense + SINKING_FUND_PROVISION.');
   ADD_COLUMN('depr_calc_stg_arc',
              'sinking_fund_depr_exp',
              'number(22,2) default 0',
              'Used to store the original calculated sinking fund expense for audit purposes since the finAL deprecation_expense column = original expense + SINKING_FUND_PROVISION.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'sinking_fund_depr_exp',
              'number(22,2) default 0',
              'Used to store the original calculated sinking fund expense for audit purposes since the finAL deprecation_expense column = original expense + SINKING_FUND_PROVISION.');
end;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3314, 0, 2016, 1, 0, 0, 046633, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046633_depr_add_sfy_columns_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;