/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049515_lessor_05_drop_cols_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/14/2017 Anand R        drop two columns from lsr_ilr_options table
||============================================================================
*/

alter table lsr_ilr_options 
	drop column initial_direct_cost;
   
alter table lsr_ilr_options 
	drop column initial_lease_cost;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3949, 0, 2017, 1, 0, 0, 49515, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049515_lessor_05_drop_cols_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
