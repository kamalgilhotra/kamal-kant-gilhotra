/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046116_prov_tbbs_line_item_data_cleanup_01_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version	  Date	     Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0 09/22/2016 Jared Watkins  Add missing descriptions to tbbs_line_item
||                                        and tbbs_treatment_type
||============================================================================
*/
update tbbs_line_item set description = 'Line Item '||line_item_id
where description is null;

update tbbs_treatment_type set description = 'Treatment '||treatment_type_id
where description is null;

update tbbs_treatment_type a set description = description||' '||treatment_type_id
where treatment_type_id <> (select min(treatment_type_id)
                            from tbbs_treatment_type b
                            where a.description = b.description
                            group by b.description
                            having count(b.description) > 1);

update tbbs_treatment_type set treatment_sql = 'SET book_elim = -1 * (coalesce(book_amount, 0) + coalesce(book_rc, 0) + coalesce(book_adj,0))'
where treatment_type_id = 2 and description = 'Eliminate Deferred Tax Accounts';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3292, 0, 2016, 1, 0, 0, 046116, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046116_prov_tbbs_line_item_data_cleanup_01_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;