/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036057_pwrtax_incr_frcst.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- ------------------------------------
|| 10.4.2.0 03/12/2014 Andrew Scott        Table creations for incremental
||                                         forecasting logic provided by
||                                         powertax.
||============================================================================
*/


/****
Table definitions of tables to use in the creation of incremental forecast tables
for tax, pulled from masterdb:

TAX_DEPRECIATION_FP             � copied from TAX_DEPRECIATION
TAX_BOOK_RECONCILE_FP           � copied from TAX_BOOK_RECONCILE
BASIS_AMOUNTS_FP                � copied from BASIS_AMOUNTS
DEFERRED_INCOME_TAX_FP          � copied from DEFERRED_INCOME_TAX
UTILITY_ACCOUNT_DEPRECIATION_FP � copied from UTILITY_ACCOUNT_DEPRECIATION
TAX_FCST_BUDGET_ADDS_FP         � copied from TAX_FCST_BUDGET_ADDS
****/


create table TAX_DEPRECIATION_FP
(
 TAX_BOOK_ID                    number(22,0) not null,
 TAX_YEAR                       number(24,2) not null,
 TAX_RECORD_ID                  number(22,0) not null,
 FUNDING_WO_ID                  number(22,0) not null,
 REVISION                       number(22,0) not null,
 TIME_STAMP                     date,
 USER_ID                        varchar2(18),
 TYPE_OF_PROPERTY_ID            number(22,0),
 TAX_LAW_ID                     number(22,0),
 CONVENTION_ID                  number(22,0),
 EXTRAORDINARY_CONVENTION       number(22,0),
 TAX_RATE_ID                    number(22,0) not null,
 REMAINING_LIFE_INDICATOR       number(22,0),
 TAX_LIMIT_ID                   number(22,0),
 SUMMARY_4562_ID                number(22,0),
 LISTED_PROPERTY_IND            number(22,0),
 RECOVERY_PERIOD_ID             number(22,0),
 TAX_CREDIT_ID                  number(22,0),
 DEFERRED_TAX_SCHEMA_ID         number(22,0) default 0,
 BOOK_BALANCE                   number(22,2) default 0,
 TAX_BALANCE                    number(22,2) default 0,
 REMAINING_LIFE                 number(22,8) default 0,
 ACCUM_RESERVE                  number(22,2) default 0,
 SL_RESERVE                     number(22,2) default 0,
 DEPRECIABLE_BASE               number(22,2) default 0,
 FIXED_DEPRECIABLE_BASE         number(22,2) default 0,
 ACTUAL_SALVAGE                 number(22,2) default 0,
 ESTIMATED_SALVAGE              number(22,2) default 0,
 ACCUM_SALVAGE                  number(22,2) default 0,
 ADDITIONS                      number(22,2) default 0,
 TRANSFERS                      number(22,2) default 0,
 ADJUSTMENTS                    number(22,2) default 0,
 RETIREMENTS                    number(22,2) default 0,
 EXTRAORDINARY_RETIRES          number(22,2) default 0,
 ACCUM_ORDINARY_RETIRES         number(22,2) default 0,
 DEPRECIATION                   number(22,2) default 0,
 COST_OF_REMOVAL                number(22,2) default 0,
 COR_EXPENSE                    number(22,2) default 0,
 GAIN_LOSS                      number(22,2) default 0,
 CAPITAL_GAIN_LOSS              number(22,2) default 0,
 EST_SALVAGE_PCT                number(22,8) default 0,
 BOOK_BALANCE_END               number(22,2) default 0,
 TAX_BALANCE_END                number(22,2) default 0,
 ACCUM_RESERVE_END              number(22,2) default 0,
 ACCUM_SALVAGE_END              number(22,2) default 0,
 ACCUM_ORDIN_RETIRES_END        number(22,2) default 0,
 SL_RESERVE_END                 number(22,2) default 0,
 RETIRE_INVOL_CONV              number(22,2) default 0,
 SALVAGE_INVOL_CONV             number(22,2) default 0,
 SALVAGE_EXTRAORD               number(22,2) default 0,
 CALC_DEPRECIATION              number(22,2) default 0,
 OVER_ADJ_DEPRECIATION          number(22,2) default 0,
 RETIRE_RES_IMPACT              number(22,2) default 0,
 TRANSFER_RES_IMPACT            number(22,2) default 0,
 SALVAGE_RES_IMPACT             number(22,2) default 0,
 COR_RES_IMPACT                 number(22,2) default 0,
 ADJUSTED_RETIRE_BASIS          number(22,2) default 0,
 RESERVE_AT_SWITCH              number(22,2) default 0,
 QUANTITY                       number(22,0) default 0,
 CAPITALIZED_DEPR               number(22,8) default 0,
 RESERVE_AT_SWITCH_END          number(22,2) default 0,
 NUMBER_MONTHS_BEG              number(22,2) default 0,
 NUMBER_MONTHS_END              number(22,2) default 0,
 EX_RETIRE_RES_IMPACT           number(22,2) default 0,
 EX_GAIN_LOSS                   number(22,2) default 0,
 QUANTITY_END                   number(22,2) default 0,
 ESTIMATED_SALVAGE_END          number(22,2) default 0,
 JOB_CREATION_AMOUNT            number(22,2) default 0,
 BOOK_BALANCE_ADJUST            number(22,2) default 0,
 ACCUM_RESERVE_ADJUST           number(22,2) default 0,
 DEPRECIABLE_BASE_ADJUST        number(22,2) default 0,
 DEPRECIATION_ADJUST            number(22,2) default 0,
 GAIN_LOSS_ADJUST               number(22,2) default 0,
 CAP_GAIN_LOSS_ADJUST           number(22,2) default 0,
 BOOK_BALANCE_ADJUST_METHOD     number(22,0) default 1,
 ACCUM_RESERVE_ADJUST_METHOD    number(22,0) default 1,
 DEPRECIABLE_BASE_ADJUST_METHOD number(22,0) default 1,
 DEPRECIATION_ADJUST_METHOD     number(22,0) default 1
);

alter table TAX_DEPRECIATION_FP
   add constraint TAX_DEPRECIATION_FP_PK
       primary key (TAX_BOOK_ID,
                    TAX_YEAR,
                    TAX_RECORD_ID,
                    FUNDING_WO_ID,
                    REVISION)
       using index tablespace PWRPLANT_IDX;

alter table TAX_DEPRECIATION_FP add constraint TAX_DEPRECIATION_FP_CONV_FK    foreign key (CONVENTION_ID)            references TAX_CONVENTION;
alter table TAX_DEPRECIATION_FP add constraint TAX_DEPRECIATION_FP_DEFSC_FK   foreign key (DEFERRED_TAX_SCHEMA_ID)   references DEFERRED_TAX_SCHEMA;
alter table TAX_DEPRECIATION_FP add constraint TAX_DEPRECIATION_FP_RECPRD_FK  foreign key (RECOVERY_PERIOD_ID)       references RECOVERY_PERIOD;
alter table TAX_DEPRECIATION_FP add constraint TAX_DEPRECIATION_FP_SUM4562_FK foreign key (SUMMARY_4562_ID)          references SUMMARY_4562;
alter table TAX_DEPRECIATION_FP add constraint TAX_DEPRECIATION_FP_TAXLAW_FK  foreign key (TAX_LAW_ID)               references TAX_LAW;
alter table TAX_DEPRECIATION_FP add constraint TAX_DEPRECIATION_FP_TXLMT_FK   foreign key (TAX_LIMIT_ID)             references TAX_LIMIT;
alter table TAX_DEPRECIATION_FP add constraint TAX_DEPRECIATION_FP_TAXRATE_FK foreign key (TAX_RATE_ID)              references TAX_RATE_CONTROL;
alter table TAX_DEPRECIATION_FP add constraint TAX_DEPRECIATION_FP_TAXRECD_FK foreign key (TAX_RECORD_ID)            references TAX_RECORD_CONTROL;
alter table TAX_DEPRECIATION_FP add constraint TAX_DEPRECIATION_FP_TB_FK      foreign key (TAX_BOOK_ID)              references TAX_BOOK;
alter table TAX_DEPRECIATION_FP add constraint TAX_DEPRECIATION_FP_TPROP_FK   foreign key (TYPE_OF_PROPERTY_ID)      references TAX_TYPE_OF_PROPERTY;
alter table TAX_DEPRECIATION_FP add constraint TAX_DEPRECIATION_FP_XCONV_FK   foreign key (EXTRAORDINARY_CONVENTION) references TAX_CONVENTION;

create index TAX_DEPRECIATION_FP_TRC_IDX
   on TAX_DEPRECIATION_FP (TAX_RECORD_ID)
      tablespace PWRPLANT_IDX;


create table TAX_BOOK_RECONCILE_FP
(
 TAX_INCLUDE_ID            number(22,0) not null,
 TAX_YEAR                  number(24,2) not null,
 TAX_RECORD_ID             number(22,0) not null,
 RECONCILE_ITEM_ID         number(22,0) not null,
 FUNDING_WO_ID             number(22,0) not null,
 REVISION                  number(22,0) not null,
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 BASIS_AMOUNT_BEG          number(22,2) default 0,
 BASIS_AMOUNT_END          number(22,2) default 0,
 BASIS_AMOUNT_ACTIVITY     number(22,2) default 0,
 BASIS_AMOUNT_INPUT_RETIRE number(22,2) default 0,
 BASIS_AMOUNT_TRANSFER     number(22,2) default 0
);

alter table TAX_BOOK_RECONCILE_FP
   add constraint PK_TAX_BOOK_RECONCILE_FP
       primary key (TAX_INCLUDE_ID,
                    TAX_YEAR,
                    TAX_RECORD_ID,
                    RECONCILE_ITEM_ID,
                    FUNDING_WO_ID,
                    REVISION)
       using index tablespace PWRPLANT_IDX;

alter table TAX_BOOK_RECONCILE_FP add constraint R_TAX_BOOK_RECONCILE_FP1 foreign key (RECONCILE_ITEM_ID) references TAX_RECONCILE_ITEM;
alter table TAX_BOOK_RECONCILE_FP add constraint R_TAX_BOOK_RECONCILE_FP2 foreign key (TAX_RECORD_ID)     references TAX_RECORD_CONTROL;
alter table TAX_BOOK_RECONCILE_FP add constraint R_TAX_BOOK_RECONCILE_FP3 foreign key (TAX_INCLUDE_ID)    references TAX_INCLUDE;


create table BASIS_AMOUNTS_FP
(
 TAX_INCLUDE_ID       number(22,0) not null,
 TAX_RECORD_ID        number(22,0) not null,
 TAX_YEAR             number(24,2) not null,
 TAX_ACTIVITY_CODE_ID number(22,0) not null,
 FUNDING_WO_ID        number(22,0) not null,
 REVISION             number(22,0) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 AMOUNT               number(22,2) default 0,
 TAX_SUMMARY_ID       number(22,0)
);

alter table BASIS_AMOUNTS_FP
   add constraint PK_BASIS_AMOUNTS_FP
       primary key (TAX_INCLUDE_ID,
                    TAX_RECORD_ID,
                    TAX_YEAR,
                    TAX_ACTIVITY_CODE_ID,
                    FUNDING_WO_ID,
                    REVISION)
       using index tablespace PWRPLANT_IDX;

alter table BASIS_AMOUNTS_FP add constraint R_BASIS_AMOUNTS_FP1 foreign key (TAX_SUMMARY_ID)       references TAX_SUMMARY_CONTROL;
alter table BASIS_AMOUNTS_FP add constraint R_BASIS_AMOUNTS_FP2 foreign key (TAX_ACTIVITY_CODE_ID) references TAX_ACTIVITY_CODE;
alter table BASIS_AMOUNTS_FP add constraint R_BASIS_AMOUNTS_FP3 foreign key (TAX_INCLUDE_ID)       references TAX_INCLUDE;
alter table BASIS_AMOUNTS_FP add constraint R_BASIS_AMOUNTS_FP4 foreign key (TAX_RECORD_ID)        references TAX_RECORD_CONTROL;

create table DEFERRED_INCOME_TAX_FP
(
 TAX_RECORD_ID                 number(22,0) default 0 not null,
 TAX_YEAR                      number(24,2) default 0 not null,
 TAX_MONTH                     number(22,0) default 0 not null,
 NORMALIZATION_ID              number(22,0) default 0 not null,
 FUNDING_WO_ID                 number(22,0) not null,
 REVISION                      number(22,0) not null,
 TIME_SLICE_ID                 number(22,0) default 1,
 TIME_STAMP                    date default sysdate,
 USER_ID                       varchar2(18),
 DEF_INCOME_TAX_BALANCE_BEG    number(22,2) default 0,
 DEF_INCOME_TAX_BALANCE_END    number(22,2) default 0,
 NORM_DIFF_BALANCE_BEG         number(22,2) default 0,
 NORM_DIFF_BALANCE_END         number(22,2) default 0,
 DEF_INCOME_TAX_PROVISION      number(22,2) default 0,
 DEF_INCOME_TAX_REVERSAL       number(22,2) default 0,
 ARAM_RATE                     number(22,8) default 0,
 ARAM_RATE_END                 number(22,8) default 0,
 LIFE                          number(22,2) default 0,
 DEF_INCOME_TAX_RETIRE         number(22,2) default 0,
 DEF_INCOME_TAX_ADJUST         number(22,2) default 0,
 DEF_INCOME_TAX_GAIN_LOSS      number(22,2) default 0,
 GAIN_LOSS_DEF_TAX_BALANCE     number(22,2) default 0,
 GAIN_LOSS_DEF_TAX_BALANCE_END number(22,2) default 0,
 BASIS_DIFF_ADD_RET            number(22,2) default 0,
 INPUT_AMORTIZATION            number(22,2) default 0,
 BASIS_DIFF_RETIRE_REVERSAL    number(22,2) default 0
);

alter table DEFERRED_INCOME_TAX_FP
   add constraint PK_DEFERRED_INCOME_TAX_FP
       primary key (TAX_RECORD_ID,
                    TAX_YEAR,
                    TAX_MONTH,
                    NORMALIZATION_ID,
                    FUNDING_WO_ID,
                    REVISION)
       using index tablespace PWRPLANT_IDX;

alter table DEFERRED_INCOME_TAX_FP add constraint R_DEFERRED_INCOME_TAX_FP1 foreign key (NORMALIZATION_ID) references NORMALIZATION_SCHEMA;
alter table DEFERRED_INCOME_TAX_FP add constraint R_DEFERRED_INCOME_TAX_FP2 foreign key (TAX_RECORD_ID)    references TAX_RECORD_CONTROL;

CREATE TABLE UTILITY_ACCT_DEPRECIATION_FP
(
 UTILITY_ACCOUNT_DEPR_ID     number(22,0) not null,
 FUNDING_WO_ID               number(22,0) not null,
 REVISION                    number(22,0) not null,
 TIME_STAMP                  date,
 USER_ID                     varchar2(18),
 TAX_UTILITY_ACCOUNT_ID      number(22,0),
 TAX_YEAR                    number(24,2),
 BOOK_DEPR                   number(22,2) default 0,
 LIFE_RATE                   number(22,8) default 0,
 SALVAGE_RATE                number(22,8) default 0,
 COR_RATE                    number(22,8) default 0,
 ACTUAL_COST_OF_REMOVAL      number(22,2) default 0,
 ACTUAL_CASH_SALVAGE         number(22,2) default 0,
 ACTUAL_SALVAGE_RETURNS      number(22,2) default 0,
 GAIN_LOSS                   number(22,2) default 0,
 BEG_BALANCE                 number(22,2) default 0,
 RESERVE_PRE81               number(22,2) default 0,
 RESERVE_POST80              number(22,2) default 0,
 COR_RESERVE                 number(22,2) default 0,
 RWIP_COR_CHANGE             number(22,2) default 0,
 RWIP_COR_END                number(22,2) default 0,
 RWIP_COR_START              number(22,2) default 0,
 RWIP_RESERVE_CREDITS_CHANGE number(22,2) default 0,
 RWIP_RESERVE_CREDITS_END    number(22,2) default 0,
 RWIP_RESERVE_CREDITS_START  number(22,2) default 0,
 RWIP_SALVAGE_CHANGE         number(22,2) default 0,
 RWIP_SALVAGE_END            number(22,2) default 0,
 RWIP_SALVAGE_START          number(22,2) default 0,
 ACTUAL_RESERVE_CREDITS      number(22,2) default 0
);

alter table UTILITY_ACCT_DEPRECIATION_FP
   add constraint PK_UTILITY_ACCT_DEPRECIATION
       primary key (UTILITY_ACCOUNT_DEPR_ID,
                    FUNDING_WO_ID,
                    REVISION)
       using index tablespace PWRPLANT_IDX;

create table TAX_FCST_BUDGET_ADDS_FP
(
 TRANS_ID             number(22,0) not null,
 FUNDING_WO_ID        number(22,0) not null,
 REVISION             number(22,0) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 COMPANY_ID           number(22,0),
 FCST_DEPR_VERSION_ID number(22,0),
 FCST_DEPR_GROUP_ID   number(22,0),
 TAX_CLASS_ID         number(22,0),
 MONTH_NUMBER         number(22,0),
 AMOUNT               number(22,2),
 BASIS_1              number(22,2) default 0,
 BASIS_2              number(22,2) default 0,
 BASIS_3              number(22,2) default 0,
 BASIS_4              number(22,2) default 0,
 BASIS_5              number(22,2) default 0,
 BASIS_6              number(22,2) default 0,
 BASIS_7              number(22,2) default 0,
 BASIS_8              number(22,2) default 0,
 BASIS_9              number(22,2) default 0,
 BASIS_10             number(22,2) default 0,
 BASIS_11             number(22,2) default 0,
 BASIS_12             number(22,2) default 0,
 BASIS_13             number(22,2) default 0,
 BASIS_14             number(22,2) default 0,
 BASIS_15             number(22,2) default 0,
 BASIS_16             number(22,2) default 0,
 BASIS_17             number(22,2) default 0,
 BASIS_18             number(22,2) default 0,
 BASIS_19             number(22,2) default 0,
 BASIS_20             number(22,2) default 0,
 BASIS_21             number(22,2) default 0,
 BASIS_22             number(22,2) default 0,
 BASIS_23             number(22,2) default 0,
 BASIS_24             number(22,2) default 0,
 BASIS_25             number(22,2) default 0,
 BASIS_26             number(22,2) default 0,
 BASIS_27             number(22,2) default 0,
 BASIS_28             number(22,2) default 0,
 BASIS_29             number(22,2) default 0,
 BASIS_30             number(22,2) default 0,
 BASIS_31             number(22,2) default 0,
 BASIS_32             number(22,2) default 0,
 BASIS_33             number(22,2) default 0,
 BASIS_34             number(22,2) default 0,
 BASIS_35             number(22,2) default 0,
 BASIS_36             number(22,2) default 0,
 BASIS_37             number(22,2) default 0,
 BASIS_38             number(22,2) default 0,
 BASIS_39             number(22,2) default 0,
 BASIS_40             number(22,2) default 0,
 BASIS_41             number(22,2) default 0,
 BASIS_42             number(22,2) default 0,
 BASIS_43             number(22,2) default 0,
 BASIS_44             number(22,2) default 0,
 BASIS_45             number(22,2) default 0,
 BASIS_46             number(22,2) default 0,
 BASIS_47             number(22,2) default 0,
 BASIS_48             number(22,2) default 0,
 BASIS_49             number(22,2) default 0,
 BASIS_50             number(22,2) default 0,
 BASIS_51             number(22,2) default 0,
 BASIS_52             number(22,2) default 0,
 BASIS_53             number(22,2) default 0,
 BASIS_54             number(22,2) default 0,
 BASIS_55             number(22,2) default 0,
 BASIS_56             number(22,2) default 0,
 BASIS_57             number(22,2) default 0,
 BASIS_58             number(22,2) default 0,
 BASIS_59             number(22,2) default 0,
 BASIS_60             number(22,2) default 0,
 BASIS_61             number(22,2) default 0,
 BASIS_62             number(22,2) default 0,
 BASIS_63             number(22,2) default 0,
 BASIS_64             number(22,2) default 0,
 BASIS_65             number(22,2) default 0,
 BASIS_66             number(22,2) default 0,
 BASIS_67             number(22,2) default 0,
 BASIS_68             number(22,2) default 0,
 BASIS_69             number(22,2) default 0,
 BASIS_70             number(22,2) default 0,
 ERROR_MESSAGE        varchar2(4000),
 TAX_RECORD_ID        number(22,0)
);

alter table TAX_FCST_BUDGET_ADDS_FP
   add constraint TAX_FCST_BUDGET_ADDS_FP_PK
       primary key (TRANS_ID,
                    FUNDING_WO_ID,
                    REVISION)
       using index tablespace PWRPLANT_IDX;


comment on table TAX_DEPRECIATION_FP
   is '(C) [09] The Tax Depreciation FP table contains a record for each tax asset being depreciated in the PowerTax system for Incremental Forecasting by the Budget module.';

comment on column TAX_DEPRECIATION_FP.TAX_BOOK_ID
   is 'System-assigned identifier for each set of tax books  a utility maintains.';
comment on column TAX_DEPRECIATION_FP.TAX_YEAR
   is 'Calendar tax year associated with the tax record''s  depreciation calculation and stored balances.  The fraction is a sequential number for dealing with short tax years.';
comment on column TAX_DEPRECIATION_FP.TAX_RECORD_ID
   is 'System-assigned key that identifies an individual tax asset on the tax depreciation (and related) tables. Tax record id records the unique combination of version, tax class, vintage,  in service month (optional), and tax location (also optional).  Each tax asset record on the depreciation table is fully described  by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';
comment on column TAX_DEPRECIATION_FP.FUNDING_WO_ID
   is 'The funding project used in the incremental forecasting calc.';
comment on column TAX_DEPRECIATION_FP.REVISION
   is 'The work order revision used in the incremental forecasting calc.';
comment on column TAX_DEPRECIATION_FP.TIME_STAMP
   is 'Standard System-assigned timestamp used for audit purposes.';
comment on column TAX_DEPRECIATION_FP.USER_ID
   is 'Standard System-assigned user id used for audit purposes.';
comment on column TAX_DEPRECIATION_FP.TYPE_OF_PROPERTY_ID
   is 'Optional System-assigned identifier to tax type of property or external system reference, such as for ''Corp Tax''.';
comment on column TAX_DEPRECIATION_FP.TAX_LAW_ID
   is 'Contains the System-assigned identifiers  for the distinctions in historic tax laws that are needed for reporting and IRS form filings.';
comment on column TAX_DEPRECIATION_FP.CONVENTION_ID
   is 'System-defined identifier for a set of  rules and methodologies regarding the calculation of depreciation  and the development of a depreciable base.';
comment on column TAX_DEPRECIATION_FP.EXTRAORDINARY_CONVENTION
   is 'System-defined identifier for a set of  rules and methodologies regarding the calculation of depreciation  and the development of a depreciable base.';
comment on column TAX_DEPRECIATION_FP.TAX_RATE_ID
   is 'System-defined identifier for  the unique set of tax depreciation rates, for all sets of tax books, which  will be needed to calculate tax depreciation.';
comment on column TAX_DEPRECIATION_FP.REMAINING_LIFE_INDICATOR
   is 'A Yes/No indicator describing whether a remaining life calculation is desired.  The PowerTax logic will automatically calculate the remaining life rate  and maintain the remaining life itself on the tax depreciation table';
comment on column TAX_DEPRECIATION_FP.TAX_LIMIT_ID
   is 'System-assigned identifier to a depreciation limit, such as for luxury autos".';
comment on column TAX_DEPRECIATION_FP.SUMMARY_4562_ID
   is 'System-assigned identifier';
comment on column TAX_DEPRECIATION_FP.LISTED_PROPERTY_IND
   is 'Indicator for listed property to show on the 4562 report. 0 or Null=No. 1 = Yes"';
comment on column TAX_DEPRECIATION_FP.RECOVERY_PERIOD_ID
   is 'System-assigned identifier to a row on the "Summary 4562" table for summarization on the 4562 report.';
comment on column TAX_DEPRECIATION_FP.TAX_CREDIT_ID
   is 'System-assigned identifier of a "tax limit."  The limits table can contain limits (e.g., luxury autos), credits, or bonus depreciation (e.g., 30% JCA 2002).';
comment on column TAX_DEPRECIATION_FP.DEFERRED_TAX_SCHEMA_ID
   is 'System-assigned identifier of a particular deferred tax schema, that indicates which normalization schemas apply to this tax record.  This is on the Federal books only.';
comment on column TAX_DEPRECIATION_FP.BOOK_BALANCE
   is 'The beginning book basis balance of this tax asset record in dollars.';
comment on column TAX_DEPRECIATION_FP.TAX_BALANCE
   is 'The beginning tax basis balance of this tax asset record in dollars, not reduced for ordinary (ADR) retirements.';
comment on column TAX_DEPRECIATION_FP.REMAINING_LIFE
   is 'If this tax asset record is to utilize a remaining life technique, this  data item will hold the calculated remaining life.';
comment on column TAX_DEPRECIATION_FP.ACCUM_RESERVE
   is 'The beginning accumulated tax depreciation reserve balance of this tax asset record.';
comment on column TAX_DEPRECIATION_FP.SL_RESERVE
   is 'For "remaining life plan" rates this is the beginning tax basis minus the beginning straight line reserve, i.e., the un-recovered straight line reserve in dollars.';
comment on column TAX_DEPRECIATION_FP.DEPRECIABLE_BASE
   is 'The calculated depreciable base to which the tax depreciation rates are  applied in order to calculate tax depreciation.';
comment on column TAX_DEPRECIATION_FP.FIXED_DEPRECIABLE_BASE
   is 'The locked-in depreciable base to which the tax depreciation rates are  applied in order to calculate tax depreciation, which ignores retirements  and other adjustments.';
comment on column TAX_DEPRECIATION_FP.ACTUAL_SALVAGE
   is 'Current year actual salvage proceeds associated with ordinary (normal/retirements) in dollars.';
comment on column TAX_DEPRECIATION_FP.ESTIMATED_SALVAGE
   is '(Currently not used)';
comment on column TAX_DEPRECIATION_FP.ACCUM_SALVAGE
   is 'Accumulated salvage proceeds included in the beginning depreciation reserve in dollars.';
comment on column TAX_DEPRECIATION_FP.ADDITIONS
   is 'Current year tax basis additions in dollars.';
comment on column TAX_DEPRECIATION_FP.TRANSFERS
   is 'Current year net tax basis transfers in dollars.';
comment on column TAX_DEPRECIATION_FP.ADJUSTMENTS
   is 'Current year net tax basis adjustments in dollars.';
comment on column TAX_DEPRECIATION_FP.RETIREMENTS
   is 'Current year tax basis, normal or ordinary, retirement or dispositions in dollars.';
comment on column TAX_DEPRECIATION_FP.EXTRAORDINARY_RETIRES
   is 'Current year tax basis, extraordinary or abnormal, retirements or dispositions in dollars.';
comment on column TAX_DEPRECIATION_FP.ACCUM_ORDINARY_RETIRES
   is 'Accumulated retirements not reducing the depreciable base at the beginning of the period in dollars.';
comment on column TAX_DEPRECIATION_FP.DEPRECIATION
   is 'This is the sum of calc_depreciation, over_adj_depreciation, and any input depreciation adjustment (depreciation_adjust).';
comment on column TAX_DEPRECIATION_FP.COST_OF_REMOVAL
   is 'Current year cost of removal dollars, whether directly expensed, in gain/loss, or applied to the reserve.';
comment on column TAX_DEPRECIATION_FP.COR_EXPENSE
   is 'Current year cost of removal dollars that are directly expensed.';
comment on column TAX_DEPRECIATION_FP.GAIN_LOSS
   is 'Current year (ordinary and capital) gain (+) or loss (-) in dollars including the (extraordinary) gain/loss.';
comment on column TAX_DEPRECIATION_FP.CAPITAL_GAIN_LOSS
   is 'Current year capital gain (+) or loss (-) in dollars (ordinary and extraordinary).';
comment on column TAX_DEPRECIATION_FP.EST_SALVAGE_PCT
   is 'Estimated salvage percent entered as a decimal.  (It is after reduction for the ADR exclusion).';
comment on column TAX_DEPRECIATION_FP.BOOK_BALANCE_END
   is 'The ending book basis balance of this tax asset record in dollars.';
comment on column TAX_DEPRECIATION_FP.TAX_BALANCE_END
   is 'The ending tax basis balance of this tax asset record in dollars, not reduced for ordinary (ADR) retirements.';
comment on column TAX_DEPRECIATION_FP.ACCUM_RESERVE_END
   is 'The ending accumulated tax depreciation reserve balance in dollars.';
comment on column TAX_DEPRECIATION_FP.ACCUM_SALVAGE_END
   is 'Accumulated salvage proceeds included in the ending depreciation reserve in dollars.  Current period allowed tax depreciation after adjustment but before reduction for the amount capitalized if any.';
comment on column TAX_DEPRECIATION_FP.ACCUM_ORDIN_RETIRES_END
   is 'Accumulated retirements not reducing the depreciable base at the end of the period in dollars.';
comment on column TAX_DEPRECIATION_FP.SL_RESERVE_END
   is 'For "remaining life plan" rates, this is the ending tax basis minus the ending straight line reserve, i.e., the ending un-recovered straight line reserve in dollars.';
comment on column TAX_DEPRECIATION_FP.RETIRE_INVOL_CONV
   is 'Currently not used.';
comment on column TAX_DEPRECIATION_FP.SALVAGE_INVOL_CONV
   is 'Currently not used.';
comment on column TAX_DEPRECIATION_FP.SALVAGE_EXTRAORD
   is 'Current salvage associated with extraordinary retirement in dollars.';
comment on column TAX_DEPRECIATION_FP.CALC_DEPRECIATION
   is 'Calculated current period depreciation in dollars before over depreciation and other adjustments.';
comment on column TAX_DEPRECIATION_FP.OVER_ADJ_DEPRECIATION
   is 'Current depreciation adjustments in dollars calculated because of reserve limits.';
comment on column TAX_DEPRECIATION_FP.RETIRE_RES_IMPACT
   is 'Current impact of ordinary retirements on the reserve for depreciation in dollars.';
comment on column TAX_DEPRECIATION_FP.TRANSFER_RES_IMPACT
   is 'Current impact of all transfers on the reserve for depreciation in dollars.';
comment on column TAX_DEPRECIATION_FP.SALVAGE_RES_IMPACT
   is 'Current impact of all salvage on the reserve for depreciation in dollars.';
comment on column TAX_DEPRECIATION_FP.COR_RES_IMPACT
   is 'Current impact of all cost of removal on the reserve for depreciation in dollars.';
comment on column TAX_DEPRECIATION_FP.ADJUSTED_RETIRE_BASIS
   is '(Not currently used)';
comment on column TAX_DEPRECIATION_FP.RESERVE_AT_SWITCH
   is 'Reserve at switch is the reserve in dollars at the point of switch from a net method to a gross method.  It is subsequently adjusted for retirements impacting the depreciable base (e.g., extraordinary retirements).  This variable is the beginning of the year value.';
comment on column TAX_DEPRECIATION_FP.QUANTITY
   is 'Quantity is the quantity of, for example, luxury auto.  This is only used when applying limit processing.';
comment on column TAX_DEPRECIATION_FP.CAPITALIZED_DEPR
   is 'Input decimal rate for capitalizing a percentage of the depreciation calculated (for example, transportation equipment).';
comment on column TAX_DEPRECIATION_FP.RESERVE_AT_SWITCH_END
   is 'End of the year reserve at switch.';
comment on column TAX_DEPRECIATION_FP.NUMBER_MONTHS_BEG
   is 'This is the cumulative number of months in the tax years up to the current tax year.  (Note that no ''half year'' convention is used in the first year, e.g. it is ''12'' where the vintage equals the tax year).  This is needed to calculate the impacts of short tax years.';
comment on column TAX_DEPRECIATION_FP.NUMBER_MONTHS_END
   is 'This is the cumulative number of months in the tax years including the current tax year.  )Note that no ''half year'' convention is used in the first year).  This is needed to calculate the impacts of short tax years.';
comment on column TAX_DEPRECIATION_FP.EX_RETIRE_RES_IMPACT
   is 'Current impact of extraordinary retirements on the reserve for depreciation in dollars.';
comment on column TAX_DEPRECIATION_FP.EX_GAIN_LOSS
   is 'Current year extraordinary gain (+) loss (-) in dollars, i.e. gain/loss related to an extraordinary retirement.';
comment on column TAX_DEPRECIATION_FP.QUANTITY_END
   is 'Ending quantity of , for example, luxury autos.  This is used only when applying limit processing.';
comment on column TAX_DEPRECIATION_FP.ESTIMATED_SALVAGE_END
   is 'Dollar amount of estimated salvage at the end of the period.';
comment on column TAX_DEPRECIATION_FP.JOB_CREATION_AMOUNT
   is 'Amount of the bonus depreciation (under the Job Creation Act of 2002, the Job Growth Act of 2003, etc) taken during the year.';
comment on column TAX_DEPRECIATION_FP.BOOK_BALANCE_ADJUST
   is 'Adjustment to the book balance in dollars.';
comment on column TAX_DEPRECIATION_FP.ACCUM_RESERVE_ADJUST
   is 'User-input  dollar amount adjustment to accumulated tax depreciation reserve balance of the tax asset.';
comment on column TAX_DEPRECIATION_FP.DEPRECIABLE_BASE_ADJUST
   is 'User-input dollar amount adjustment to depreciable base of the tax asset.';
comment on column TAX_DEPRECIATION_FP.DEPRECIATION_ADJUST
   is 'User-input dollar amount adjustment to current year depreciation expense for the tax asset.';
comment on column TAX_DEPRECIATION_FP.GAIN_LOSS_ADJUST
   is 'User-input dollar amount adjustment to ordinary gain/loss.';
comment on column TAX_DEPRECIATION_FP.CAP_GAIN_LOSS_ADJUST
   is 'User-input dollar amount adjustment to capital gain/loss.';
comment on column TAX_DEPRECIATION_FP.BOOK_BALANCE_ADJUST_METHOD
   is 'System-assigned identifier to an adjust convention (beginning, average, or end) for input adjustments to the book balance.';
comment on column TAX_DEPRECIATION_FP.ACCUM_RESERVE_ADJUST_METHOD
   is 'System-assigned identifier to an adjust convention (beginning, average, or end) for input adjustments to the accumulated reserve depreciation reserve.';
comment on column TAX_DEPRECIATION_FP.DEPRECIABLE_BASE_ADJUST_METHOD
   is 'System-assigned identifier to an adjust convention (beginning, average, or end) for input adjustments to the depreciable base.';
comment on column TAX_DEPRECIATION_FP.DEPRECIATION_ADJUST_METHOD
   is 'Not used.';


comment on table TAX_BOOK_RECONCILE_FP
   is '(C)  [09] {A} The Tax Book Reconcile FP data table contains the reconciliation amounts between book basis assets and tax basis assets for each tax account and for each tax set of books for Incremental Forecasting by the Budget module.';

comment on column TAX_BOOK_RECONCILE_FP.TAX_INCLUDE_ID
   is 'System-assigned identifier to a Tax Include type that specifies the set of tax books to which this reconciling item applies.';
comment on column TAX_BOOK_RECONCILE_FP.TAX_YEAR
   is 'Tax filing year (as opposed to the vintage).  The fractions are sequential numbers used to deal with short tax years.';
comment on column TAX_BOOK_RECONCILE_FP.TAX_RECORD_ID
   is 'System-assigned key that identifies an individual  tax asset on the tax depreciation (and related) tables. Tax record id records the unique combination of version, tax class, vintage, in service month (optional), and tax location (also optional).  Each tax asset record on the depreciation table is fully described by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';
comment on column TAX_BOOK_RECONCILE_FP.RECONCILE_ITEM_ID
   is 'System-assigned identifier for a book-to-tax reconciling item.';
comment on column TAX_BOOK_RECONCILE_FP.FUNDING_WO_ID
   is 'The funding project used in the incremental forecasting calc.';
comment on column TAX_BOOK_RECONCILE_FP.REVISION
   is 'The work order revision used in the incremental forecasting calc.';
comment on column TAX_BOOK_RECONCILE_FP.TIME_STAMP
   is 'Standard System-assigned timestamp used for audit purposes.';
comment on column TAX_BOOK_RECONCILE_FP.USER_ID
   is 'Standard System-assigned user id used for audit purposes.';
comment on column TAX_BOOK_RECONCILE_FP.BASIS_AMOUNT_BEG
   is 'Beginning book-to-tax basis difference in dollars.  (Note that AFUDC will be negative, CIAC positive, etc.).';
comment on column TAX_BOOK_RECONCILE_FP.BASIS_AMOUNT_END
   is 'Ending book-to-tax basis difference in dollars.';
comment on column TAX_BOOK_RECONCILE_FP.BASIS_AMOUNT_ACTIVITY
   is 'Changes in book-to-tax basis differences in dollars, generally for additions since this is used as an originating difference for deferred taxes.  Note that it does not reconcile basis_amount_beg with basis_amount_end, since pro-rated retirement differences are excluded as are input amounts for retirements in basis_amount_input_retire.';
comment on column TAX_BOOK_RECONCILE_FP.BASIS_AMOUNT_INPUT_RETIRE
   is 'Input change in basis differences associated with a retirement in dollars.  Normally basis differences associated with retirements are automatically calculated by the system.  In order to input these, the input_retire_ind on tax reconcile_item must be set to ''1''.';
comment on column TAX_BOOK_RECONCILE_FP.BASIS_AMOUNT_TRANSFER
   is 'Change in basis differences due to a retirement.';


comment on table BASIS_AMOUNTS_FP
   is '(C) [09] {A} Basis Amounts FP holds the current book activity (additions, retirements, etc.) with the charges summarized up to ''Tax Summary'' (e.g., Directs, AFUDC, CPI, etc.) and classified for tax for Incremental Forecasting by the Budget module.';

comment on column BASIS_AMOUNTS_FP.TAX_INCLUDE_ID
   is 'The record id from the Tax Include table indicating which tax books (e.g., All, Federal, State, etc.) the transaction pertains to the default is all).';
comment on column BASIS_AMOUNTS_FP.TAX_RECORD_ID
   is 'System-assigned key that identifies an individual tax asset on the tax depreciation (and related) tables.  Tax record id records the unique combination of version, tax class, vintage, in service month (optional), and tax location (also optional).  Each tax asset record on the depreciation table is fully described by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';
comment on column BASIS_AMOUNTS_FP.TAX_YEAR
   is 'Filing year (as opposed to vintage year).';
comment on column BASIS_AMOUNTS_FP.FUNDING_WO_ID
   is 'The funding project used in the incremental forecasting calc.';
comment on column BASIS_AMOUNTS_FP.REVISION
   is 'The work order revision used in the incremental forecasting calc.';
comment on column BASIS_AMOUNTS_FP.TAX_ACTIVITY_CODE_ID
   is 'System-assigned identifier for the Tax Activities defined by the PowerPlan application � Additions, Ordinary Retirements, Extraordinary Retirements, Transfer.';
comment on column BASIS_AMOUNTS_FP.TIME_STAMP
   is 'Standard system-assigned timestamp used for audit purposes.';
comment on column BASIS_AMOUNTS_FP.USER_ID
   is 'Standard system-assigned user id used for audit purposes.';
comment on column BASIS_AMOUNTS_FP.AMOUNT
   is 'The ''book'' amount of the transaction in dollars.';
comment on column BASIS_AMOUNTS_FP.TAX_SUMMARY_ID
   is 'Summary of charge type for grouping dollar amounts for income tax processing, specifically for book-to-tax basis reconciliation. (Not used.)';


comment on table DEFERRED_INCOME_TAX_FP
   is '(C)[09] {A} The Deferred Income Tax FP data table maintains the deferred income tax balances, normalization balances, current deferred provision, and ARAM rates.  Used for Incremental Forecasting by the Budget module.';

comment on column DEFERRED_INCOME_TAX_FP.TAX_RECORD_ID
   is 'System-assigned key that identifies an individual tax asset on the tax depreciation (and related) tables.  Tax record id records the unique combination of version, tax class, vintage, in service month (optional), and tax location (also optional).  Each tax asset record on the depreciation table is fully described by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';
comment on column DEFERRED_INCOME_TAX_FP.TAX_YEAR
   is 'Filing or calendar year (as opposed to the vintage year).';
comment on column DEFERRED_INCOME_TAX_FP.TAX_MONTH
   is 'In-service month. (Not used).';
comment on column DEFERRED_INCOME_TAX_FP.NORMALIZATION_ID
   is 'System-assigned key to the normalization schema, which indicates the ''to - from'' depreciation.';
comment on column DEFERRED_INCOME_TAX_FP.TIME_SLICE_ID
   is 'System-assigned identifier of the time slice that controls the time periodicity for calculating and maintaining deferred taxes for each tax year.';
comment on column DEFERRED_INCOME_TAX_FP.FUNDING_WO_ID
   is 'The funding project used in the incremental forecasting calc.';
comment on column DEFERRED_INCOME_TAX_FP.REVISION
   is 'The work order revision used in the incremental forecasting calc.';
comment on column DEFERRED_INCOME_TAX_FP.TIME_STAMP
   is 'Standard system-assigned timestamp used for audit purposes.';
comment on column DEFERRED_INCOME_TAX_FP.USER_ID
   is 'Standard system-assigned user_id used for audit purposes.';
comment on column DEFERRED_INCOME_TAX_FP.DEF_INCOME_TAX_BALANCE_BEG
   is 'Beginning (of the year or month) accumulated deferred tax balance in dollars.';
comment on column DEFERRED_INCOME_TAX_FP.DEF_INCOME_TAX_BALANCE_END
   is 'Ending (of the year or month) accumulated deferred tax balance in dollars.';
comment on column DEFERRED_INCOME_TAX_FP.NORM_DIFF_BALANCE_BEG
   is 'Accumulated tax less book difference at the beginning of the period (month or year) in dollars.';
comment on column DEFERRED_INCOME_TAX_FP.NORM_DIFF_BALANCE_END
   is 'Accumulated tax less book difference at the end of the period (month or year) in dollars.';
comment on column DEFERRED_INCOME_TAX_FP.DEF_INCOME_TAX_PROVISION
   is '''Current'' deferred income tax provision in dollars.  (May be debit or credit, generally a debit.)';
comment on column DEFERRED_INCOME_TAX_FP.DEF_INCOME_TAX_REVERSAL
   is 'Current period reversal of deferred income taxes previously provided.';
comment on column DEFERRED_INCOME_TAX_FP.ARAM_RATE
   is 'Average Rate Assumption Method (ARAM) rate used to turn around deferred taxes (weighted average rate) from prior period.';
comment on column DEFERRED_INCOME_TAX_FP.ARAM_RATE_END
   is 'Average Rate Assumption Method (ARAM) rate actually used during the period, calculated.';
comment on column DEFERRED_INCOME_TAX_FP.LIFE
   is 'Life (in years) for amortizing basis deferred taxes if the amortization type is ''overhead'' on the normalization schema table and actual book depreciation is not allocated.';
comment on column DEFERRED_INCOME_TAX_FP.DEF_INCOME_TAX_RETIRE
   is 'No longer used.';
comment on column DEFERRED_INCOME_TAX_FP.DEF_INCOME_TAX_ADJUST
   is 'User input deferred tax adjustment in dollars.';
comment on column DEFERRED_INCOME_TAX_FP.DEF_INCOME_TAX_GAIN_LOSS
   is '(Not Used)';
comment on column DEFERRED_INCOME_TAX_FP.GAIN_LOSS_DEF_TAX_BALANCE
   is '(Not Used)';
comment on column DEFERRED_INCOME_TAX_FP.GAIN_LOSS_DEF_TAX_BALANCE_END
   is '(Not Used)';
comment on column DEFERRED_INCOME_TAX_FP.BASIS_DIFF_ADD_RET
   is 'Change in a basis difference (for deferral) due to additions or retirements in dollars calculated by the system.';
comment on column DEFERRED_INCOME_TAX_FP.INPUT_AMORTIZATION
   is 'Amount of book depreciation allocated to the basis differences.';
comment on column DEFERRED_INCOME_TAX_FP.BASIS_DIFF_RETIRE_REVERSAL
   is 'A switch determining the treatment of the basis deferred taxes on a retirement. 0'' = adjust the basis deferred.  This is the default.  1'' = don''t correspondingly adjust the deferred taxes.';

comment on table UTILITY_ACCT_DEPRECIATION_FP
   is '(C)  [01] [09] The Utility Account Depreciation FP table contains the book depreciation data being transferred from PowerPlan to PowerTax for Incremental Forecasting by the Budget module.';

comment on column UTILITY_ACCT_DEPRECIATION_FP.UTILITY_ACCOUNT_DEPR_ID
   is 'System-assigned identifier of a particular row (tax utility account and tax years) where tax utility account is a depreciation group being mapped.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.FUNDING_WO_ID
   is 'The funding project used in the incremental forecasting calc.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.REVISION
   is 'The work order revision used in the incremental forecasting calc.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.TIME_STAMP
   is 'Standard System-assigned timestamp used for audit purposes.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.USER_ID
   is 'Standard System-assigned user id used for audit purposes.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.TAX_UTILITY_ACCOUNT_ID
   is 'System-assigned identifier of a particular tax utility account (depreciation group).';
comment on column UTILITY_ACCT_DEPRECIATION_FP.TAX_YEAR
   is 'Tax year being processed.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.BOOK_DEPR
   is 'Dollar amount of book depreciation, depreciation adjustments, reserve adjustments, and transfers.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.LIFE_RATE
   is 'Life component of the book depreciation rate.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.SALVAGE_RATE
   is 'Salvage component of the book depreciation rate.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.COR_RATE
   is 'Cost of removal component of the book depreciation rate.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.ACTUAL_COST_OF_REMOVAL
   is 'Cost of removal incurred in dollars (negative).';
comment on column UTILITY_ACCT_DEPRECIATION_FP.ACTUAL_CASH_SALVAGE
   is 'Salvage cash proceeds in dollars (positive).';
comment on column UTILITY_ACCT_DEPRECIATION_FP.ACTUAL_SALVAGE_RETURNS
   is 'Salvage returns to store in dollars (positive).';
comment on column UTILITY_ACCT_DEPRECIATION_FP.GAIN_LOSS
   is 'Book gain (negative)/loss (positive).';
comment on column UTILITY_ACCT_DEPRECIATION_FP.BEG_BALANCE
   is 'Beginning book reserve balance in dollars.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.RESERVE_PRE81
   is 'Beginning pre 1981 beginning book reserve in dollars.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.RESERVE_POST80
   is 'Beginning post 1981 beginning book reserve in dollars.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.COR_RESERVE
   is 'Dollar amount of Cost of Removal expense provision, depreciation adjustments, reserve adjustments, and transfers.  Amount not included in book_depr column.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.RWIP_COR_CHANGE
   is 'Change (end less beginning) in cost of removal dollars in RWIP.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.RWIP_COR_END
   is 'End of the period cost of removal dollars in RWIP.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.RWIP_COR_START
   is 'Beginning of the period cost of removal dollars in RWIP.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.RWIP_RESERVE_CREDITS_CHANGE
   is 'Change (end less beginning) in other credits in RWIP.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.RWIP_RESERVE_CREDITS_END
   is 'End of the period credit dollars in RWIP.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.RWIP_RESERVE_CREDITS_START
   is 'Beginning of the period credit dollars in RWIP.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.RWIP_SALVAGE_CHANGE
   is 'Change (end less beginning) in salvage dollars in RWIP.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.RWIP_SALVAGE_END
   is 'End of the period salvage dollars in RWIP.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.RWIP_SALVAGE_START
   is 'Beginning of the period salvage dollars in RWIP.';
comment on column UTILITY_ACCT_DEPRECIATION_FP.ACTUAL_RESERVE_CREDITS
   is 'As part of maint 33359, we are splitting salvage into three columns from two columns';


comment on table TAX_FCST_BUDGET_ADDS_FP
   is '(C)  [09] The Tax Forecast Budget Adds FP table is produced by the Budget Module for use in PowerTax for incremental forecasting';

comment on column TAX_FCST_BUDGET_ADDS_FP.TRANS_ID
   is 'System assigned identifier of a transaction on this table.';
comment on column TAX_FCST_BUDGET_ADDS_FP.FUNDING_WO_ID
   is 'The funding project used in the incremental forecasting calc.';
comment on column TAX_FCST_BUDGET_ADDS_FP.REVISION
   is 'The work order revision used in the incremental forecasting calc.';
comment on column TAX_FCST_BUDGET_ADDS_FP.TIME_STAMP
   is 'Standard System-assigned timestamp used for audit purposes.';
comment on column TAX_FCST_BUDGET_ADDS_FP.USER_ID
   is 'Standard System-assigned user id used for audit purposes.';
comment on column TAX_FCST_BUDGET_ADDS_FP.COMPANY_ID
   is 'System-assigned identifier of a particular company.';
comment on column TAX_FCST_BUDGET_ADDS_FP.FCST_DEPR_VERSION_ID
   is 'System assigned identifier of a book forecast depreciation version.';
comment on column TAX_FCST_BUDGET_ADDS_FP.FCST_DEPR_GROUP_ID
   is 'System assigned identifier of a book forecast depreciation group.';
comment on column TAX_FCST_BUDGET_ADDS_FP.TAX_CLASS_ID
   is 'System-assigned identifier of a particular tax class.';
comment on column TAX_FCST_BUDGET_ADDS_FP.MONTH_NUMBER
   is 'Month of the forecast additions in YYYYMM format.';
comment on column TAX_FCST_BUDGET_ADDS_FP.AMOUNT
   is 'Book amount of the addition forecast.';
comment on column TAX_FCST_BUDGET_ADDS_FP.BASIS_1
   is 'Forecast basis 1 in dollars.';
comment on column TAX_FCST_BUDGET_ADDS_FP.BASIS_70
   is 'Forecast basis 70 in dollars.';
comment on column TAX_FCST_BUDGET_ADDS_FP.ERROR_MESSAGE
   is 'Error message used by the processing.';
comment on column TAX_FCST_BUDGET_ADDS_FP.TAX_RECORD_ID
   is 'System assigned identifier of a tax record.';



----
----  new columns needed for incremental forecasting run fast packages (next scripts).
----
alter table TAX_JOB_PARAMS
   add (FUNDING_WO_ID number(22,0),
        REVISION      number(22,0));


comment on column TAX_JOB_PARAMS.FUNDING_WO_ID
   is 'Funding wo id needed for submission of incremental forecast tax packages.';
comment on column TAX_JOB_PARAMS.REVISION
   is 'Revision needed for submission of incremental forecast tax packages.';

----
----  create a temp table for the allocation of the book depr
----

create global temporary table TAX_TEMP_INCFCST_DEPR_ALLOC
(
 TAX_RECORD_ID     number(22,0) not null,
 TAX_YEAR          number(22,2) not null,
 TAX_INCLUDE_ID    number(22,0),
 RECONCILE_ITEM_ID number(22,0),
 TARGET            varchar2(35) not null,
 ORIG_AMOUNT       number(22,2) default 0,
 TOTAL_AMOUNT      number(22,2) default 0,
 DEPR_TO_SPREAD    number(22,2) default 0,
 ALLOCATED_AMOUNT  number(22,2) default 0
) on commit preserve rows;

create unique index TAX_TEMP_INCFCST_DEPR_ALLOC_PK
   on TAX_TEMP_INCFCST_DEPR_ALLOC (TAX_RECORD_ID, TAX_YEAR, TAX_INCLUDE_ID, RECONCILE_ITEM_ID, TARGET);

comment on table TAX_TEMP_INCFCST_DEPR_ALLOC
   is '(T)  [09] The Tax Temp Incremental Forecast Depr Alloc table is used for the allocation of book depreciation during the incremental forecast processing.';

comment on column TAX_TEMP_INCFCST_DEPR_ALLOC.TAX_RECORD_ID
   is 'System-assigned key that identifies an individual  tax asset on the tax depreciation (and related) tables. Tax record id records the unique combination of version, tax class, vintage, in service month (optional), and tax location (also optional).  Each tax asset record on the depreciation table is fully described by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';
comment on column TAX_TEMP_INCFCST_DEPR_ALLOC.TAX_YEAR
   is 'Tax filing year (as opposed to the vintage).  The fractions are sequential numbers used to deal with short tax years.';
comment on column TAX_TEMP_INCFCST_DEPR_ALLOC.TAX_INCLUDE_ID
   is 'System-assigned identifier to a Tax Include type that specifies the set of tax books to which this reconciling item applies.';
comment on column TAX_TEMP_INCFCST_DEPR_ALLOC.RECONCILE_ITEM_ID
   is 'System-assigned identifier for a book-to-tax reconciling item.';
comment on column TAX_TEMP_INCFCST_DEPR_ALLOC.TARGET
   is 'The eventual target of the allocated depreciation.  This will either be "TD" for tax depreciation and "TBR" for tax book reconcile.';
comment on column TAX_TEMP_INCFCST_DEPR_ALLOC.ORIG_AMOUNT
   is 'The original amount pulled from the table used to allocate the book depreciation.';
comment on column TAX_TEMP_INCFCST_DEPR_ALLOC.TOTAL_AMOUNT
   is 'The sum of the original amounts.';
comment on column TAX_TEMP_INCFCST_DEPR_ALLOC.DEPR_TO_SPREAD
   is 'The extracted book depreciation amount to allocate.';
comment on column TAX_TEMP_INCFCST_DEPR_ALLOC.ALLOCATED_AMOUNT
   is 'The final allocated book depreciation amount.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1066, 0, 10, 4, 2, 0, 36057, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036057_pwrtax_incr_frcst.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;