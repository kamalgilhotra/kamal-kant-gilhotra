/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_043528_pcm_simple_grid_misc_search_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2015.1     04/09/2015 Anand R        modify sql for simple grid Misc tab search
||============================================================================
*/

update pp_misc_search set sqls= ' and work_order_control.work_order_id in (select work_order_id from work_order_account where [f_parseArrayBy254] )'
where upper(type) = 'PROJECT' and upper(label) = 'AFUDC TYPE' ;

update pp_misc_search set sqls= ' and work_order_control.work_order_id in (select work_order_id from work_order_account where [f_parseArrayBy254] )'
where upper(type) = 'PROJECT' and upper(label) = 'AGREEMENT' ;

update pp_misc_search set sqls= ' and work_order_control.work_order_id in (select work_order_id from work_order_account where [f_parseArrayBy254] )'
where upper(type) = 'PROJECT' and upper(label) = 'ALLOCATION METHOD' ;

update pp_misc_search set sqls= ' and work_order_control.work_order_id in (select work_order_id from work_order_account where [f_parseArrayBy254] )'
where upper(type) = 'PROJECT' and upper(label) = 'CLOSING OPTION' ;

update pp_misc_search set sqls= ' and work_order_control.work_order_id in (select work_order_id from work_order_account where [f_parseArrayBy254] )'
where upper(type) = 'PROJECT' and upper(label) = 'ELIGIBLE FOR AFUDC' ;

update pp_misc_search set sqls= ' and work_order_control.work_order_id in (select work_order_id from work_order_account where [f_parseArrayBy254] )'
where upper(type) = 'PROJECT' and upper(label) = 'ELIGIBLE FOR CPI' ;

update pp_misc_search set sqls= ' and work_order_control.work_order_id in (select work_order_id from work_order_account where [f_parseArrayBy254] )'
where upper(type) = 'PROJECT' and upper(label) = 'FUNCTIONAL CLASS' ;

update pp_misc_search set sqls= ' and work_order_control.work_order_id in (select work_order_id from work_order_account where [f_parseArrayBy254] )'
where upper(type) = 'PROJECT' and upper(label) = 'REIMBURSABLE' ;

update pp_misc_search set sqls= ' and work_order_control.asset_location_id in (select asset_location_id from asset_location where [f_parseArrayBy254] )'
where upper(type) = 'PROJECT' and upper(label) = 'TOWN' ;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2487, 0, 2015, 1, 0, 0, 43528, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043528_pcm_simple_grid_misc_search_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;