 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048160_2_reg_alloc_dyn_convert_dml.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 06/27/2017 Sarah Byers    Convert reg_acct_id to reg_alloc_acct_id for dynamic allocator basis
 ||============================================================================
 */ 

-- convert jur template basis
merge INTO reg_jur_allocator_dyn y
USING (
        WITH alloc_steps AS (
          SELECT /*+ MATERIALIZE */ *
            FROM reg_jur_alloc_steps),
            target_forward AS (
          SELECT /*+ MATERIALIZE */ reg_jur_template_id, reg_alloc_category_id, reg_alloc_target_id, carry_forward_dollars
            FROM reg_jur_target_forward
          UNION ALL
          SELECT reg_jur_template_id, 0, 0, 1
            FROM reg_jur_template),
            alloc_acct AS (
          SELECT /*+ MATERIALIZE */ *
            FROM reg_jur_alloc_account  ),
            allocator_dyn AS (
          SELECT /*+ MATERIALIZE */ *
            FROM reg_jur_allocator_dyn
           WHERE reg_acct_id IS NOT NULL
             AND reg_alloc_target_id IS NOT NULL
            )
        SELECT /*+ NO_MERGE */ d.reg_jur_template_id, d.reg_allocator_id, d.Row_Number, a.reg_alloc_acct_id
          FROM allocator_dyn d, alloc_acct a, target_forward t, alloc_steps p
        WHERE d.reg_jur_template_id = a.reg_jur_template_id
          AND d.reg_jur_template_id = t.reg_jur_template_id
          AND d.reg_jur_template_id = p.reg_jur_template_id
          AND Nvl(t.carry_forward_dollars,0) = 1
          AND p.reg_alloc_category_id = t.reg_alloc_category_id
          AND d.reg_acct_id = a.reg_acct_id
          AND a.reg_alloc_target_id = t.reg_alloc_target_id
          AND a.reg_alloc_category_id = p.reg_alloc_category_id
          ) z
ON (y.reg_jur_template_id = z.reg_jur_template_id
    AND y.reg_allocator_id = z.reg_allocator_id
    AND y.Row_Number = z.Row_Number)
WHEN matched THEN
 UPDATE SET y.reg_alloc_acct_id = z.reg_alloc_acct_id;

INSERT INTO reg_jur_allocator_dyn (
  reg_jur_template_id, reg_allocator_id, Row_Number, reg_acct_type_id, sub_acct_type_id, reg_acct_id, reg_alloc_target_id, Sign, reg_alloc_acct_id)
  WITH alloc_steps AS (
    SELECT /*+ MATERIALIZE */ *
      FROM reg_jur_alloc_steps    ),
      target_forward AS (
    SELECT /*+ MATERIALIZE */ reg_jur_template_id, reg_alloc_category_id, reg_alloc_target_id, carry_forward_dollars
      FROM reg_jur_target_forward
    UNION ALL
    SELECT reg_jur_template_id, 0, 0, 1
      FROM reg_jur_template),
      alloc_acct AS (
    SELECT /*+ MATERIALIZE */ *
      FROM reg_jur_alloc_account  ),
      allocator_dyn AS (
    SELECT /*+ MATERIALIZE */ *
      FROM reg_jur_allocator_dyn 
      WHERE reg_acct_id IS NOT NULL
        AND reg_alloc_target_id IS NULL ),
      allocator_list as (
    select /*+ MATERIALIZE */ *
      from reg_allocator),
      max_row_number AS (
    SELECT /*+ MATERIALIZE */ reg_jur_template_id, reg_allocator_id, Max(Row_Number) max_rn
      FROM reg_jur_allocator_dyn
      GROUP BY reg_jur_template_id, reg_allocator_id)
  SELECT /*+ NO_MERGE */
         d.reg_jur_template_id, d.reg_allocator_id, m.max_rn + Row_Number() OVER (PARTITION BY d.reg_allocator_id ORDER BY d.Row_Number) new_rn,
         d.reg_acct_type_id, d.sub_acct_type_id, a.reg_acct_id, NULL, d.Sign, a.reg_alloc_acct_id
    FROM allocator_dyn d, alloc_acct a, target_forward t, alloc_steps p, max_row_number m
  WHERE d.reg_jur_template_id = a.reg_jur_template_id
    AND d.reg_jur_template_id = t.reg_jur_template_id
    AND d.reg_jur_template_id = p.reg_jur_template_id
    AND d.reg_jur_template_id = m.reg_jur_template_id
    AND d.reg_acct_id = a.reg_acct_id
    AND a.reg_alloc_target_id = t.reg_alloc_target_id
    AND Nvl(t.carry_forward_dollars,0) = 1
    AND p.reg_alloc_category_id = t.reg_alloc_category_id
    AND p.reg_alloc_category_id = a.reg_alloc_category_id
    AND d.reg_allocator_id = m.reg_allocator_id
    and d.reg_allocator_id not in (select related_reg_alloc_id from reg_allocator where related_reg_alloc_id is not null);

DELETE FROM reg_jur_allocator_dyn
 WHERE reg_acct_id IS NOT NULL
   AND reg_alloc_target_id IS NULL
   AND reg_alloc_acct_id IS NULL
   and reg_allocator_id not in (select related_reg_alloc_id from reg_allocator where related_reg_alloc_id is not null);

-- convert case basis
merge INTO reg_case_allocator_dyn y
USING (
        WITH alloc_steps AS (
          SELECT /*+ MATERIALIZE */ *
            FROM reg_case_alloc_steps),
            target_forward AS (
          SELECT /*+ MATERIALIZE */ reg_case_id, reg_alloc_category_id, reg_alloc_target_id, carry_forward_dollars
            FROM reg_case_target_forward
          UNION ALL
          SELECT reg_case_id, 0, 0, 1
            FROM reg_case),
            alloc_acct AS (
          SELECT /*+ MATERIALIZE */ *
            FROM reg_case_alloc_account  ),
            allocator_dyn AS (
          SELECT /*+ MATERIALIZE */ *
            FROM reg_case_allocator_dyn
           WHERE reg_acct_id IS NOT NULL
             AND reg_alloc_target_id IS NOT NULL
            )
        SELECT /*+ NO_MERGE */ d.reg_case_id, d.reg_allocator_id, d.Row_Number, a.reg_alloc_acct_id
          FROM allocator_dyn d, alloc_acct a, target_forward t, alloc_steps p
        WHERE d.reg_case_id = a.reg_case_id
          AND d.reg_case_id = t.reg_case_id
          AND d.reg_case_id = p.reg_case_id
          AND Nvl(t.carry_forward_dollars,0) = 1
          AND p.reg_alloc_category_id = t.reg_alloc_category_id
          AND d.reg_acct_id = a.reg_acct_id
          AND a.reg_alloc_target_id = t.reg_alloc_target_id
          AND a.reg_alloc_category_id = p.reg_alloc_category_id
          ) z
ON (y.reg_case_id = z.reg_case_id
    AND y.reg_allocator_id = z.reg_allocator_id
    AND y.Row_Number = z.Row_Number)
WHEN matched THEN
 UPDATE SET y.reg_alloc_acct_id = z.reg_alloc_acct_id;

INSERT INTO reg_case_allocator_dyn (
  reg_case_id, reg_allocator_id, Row_Number, reg_acct_type_id, sub_acct_type_id, reg_acct_id, reg_alloc_target_id, Sign, reg_alloc_acct_id)
  WITH alloc_steps AS (
    SELECT /*+ MATERIALIZE */ *
      FROM reg_case_alloc_steps    ),
      target_forward AS (
    SELECT /*+ MATERIALIZE */ reg_case_id, reg_alloc_category_id, reg_alloc_target_id, carry_forward_dollars
      FROM reg_case_target_forward
    UNION ALL
    SELECT reg_case_id, 0, 0, 1
      FROM reg_case),
      alloc_acct AS (
    SELECT /*+ MATERIALIZE */ *
      FROM reg_case_alloc_account  ),
      allocator_dyn AS (
    SELECT /*+ MATERIALIZE */ *
      FROM reg_case_allocator_dyn
      WHERE reg_acct_id IS NOT NULL
        AND reg_alloc_target_id IS NULL ),
      allocator_list as (
    select /*+ MATERIALIZE */ *
      from reg_allocator),
      max_row_number AS (
    SELECT /*+ MATERIALIZE */ reg_case_id, reg_allocator_id, Max(Row_Number) max_rn
      FROM reg_case_allocator_dyn
      GROUP BY reg_case_id, reg_allocator_id)
  SELECT /*+ NO_MERGE */
         d.reg_case_id, d.reg_allocator_id, m.max_rn + Row_Number() OVER (PARTITION BY d.reg_allocator_id ORDER BY d.Row_Number) new_rn,
         d.reg_acct_type_id, d.sub_acct_type_id, a.reg_acct_id, NULL, d.Sign, a.reg_alloc_acct_id
    FROM allocator_dyn d, alloc_acct a, target_forward t, alloc_steps p, max_row_number m
  WHERE d.reg_case_id = a.reg_case_id
    AND d.reg_case_id = t.reg_case_id
    AND d.reg_case_id = p.reg_case_id
    AND d.reg_case_id = m.reg_case_id
    AND d.reg_acct_id = a.reg_acct_id
    AND a.reg_alloc_target_id = t.reg_alloc_target_id
    AND Nvl(t.carry_forward_dollars,0) = 1
    AND p.reg_alloc_category_id = t.reg_alloc_category_id
    AND p.reg_alloc_category_id = a.reg_alloc_category_id
    AND d.reg_allocator_id = m.reg_allocator_id
    and d.reg_allocator_id not in (select related_reg_alloc_id from allocator_list where related_reg_alloc_id is not null);

DELETE FROM reg_case_allocator_dyn
 WHERE reg_acct_id IS NOT NULL
   AND reg_alloc_target_id IS NULL
   AND reg_alloc_acct_id IS NULL
   and reg_allocator_id not in (select related_reg_alloc_id from reg_allocator where related_reg_alloc_id is not null);
   
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3548, 0, 2017, 1, 0, 0, 48160, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048160_2_reg_alloc_dyn_convert_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;   