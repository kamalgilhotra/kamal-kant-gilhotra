/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050185_lessor_02_pend_trans_object_rename_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/16/2018 Anand R        Rename workspace_uo_name for Lessor Pend Transaction to open lease pend trans window
||============================================================================
*/

update ppbase_workspace
set workspace_uo_name = 'w_ls_trans_manage'
where module = 'LESSOR'
and workspace_identifier = 'admin_lessor_pend_transaction' ;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4207, 0, 2017, 3, 0, 0, 50185, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050185_lessor_02_pend_trans_object_rename_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;