--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042975_pcm_bdg_afc_fp_rpt03_aar_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1.0.0 03/02/2015 Anand R          Budget - AFUDC FP Report Conversion.
--||============================================================================
--*/

---- New report filter for Budget FP reports
insert into pp_reports_filter
(pp_report_filter_id, description, table_name, filter_uo_name)
values
(89, 'PCM - Rpt Budget FP 2', '', 'uo_ppbase_tab_filter_dynamic');

insert into pp_reports_filter
(pp_report_filter_id, description, table_name, filter_uo_name)
values
(90, 'PCM - Rpt Budget FP 04', '', 'uo_ppbase_tab_filter_dynamic');


---- New Dynamic Filters
---- Estimate Charge Type
insert into pp_dynamic_filter
(filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required)
values
(265, 'Estimate Charge Type', 'dw', 'amounts.est_chg_type_id', '', 'dw_pcm_est_charge_type_filter', 'est_chg_type_id', 'description', 'N', 0);
---- Expenditure Type
insert into pp_dynamic_filter
(filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required)
values
(267, 'Expenditure Type', 'dw', 'amounts.expenditure_type_id', '', 'dw_pcm_expenditure_type_filter', 'expenditure_type_id', 'description', 'N', 0);
---- Budget Summary
insert into pp_dynamic_filter
(filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required)
values
(268, 'Budget Summary', 'dw', 'budget.budget_summary_id', 'work_order_control.budget_id in (select budget.budget_id from budget where [selected_values] )', 'dw_pp_budget_summary_filter', 'budget_summary_id', 'description', 'N', 0);
---- Budget Organization
insert into pp_dynamic_filter
(filter_id, label, input_type, sqls_column_expression, sqls_where_clause, dw, dw_id, dw_description, dw_id_datatype, required)
values
(269, 'Budget Organization', 'dw', 'budget.budget_organization_id', 'work_order_control.budget_id in (select budget.budget_id from budget where [selected_values] )', 'dw_pp_budget_org_filter', 'budget_organization_id', 'description', 'N', 0);


insert into pp_dynamic_filter (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values (266, 'Dynamic Subtotal', 'dw', null, null, 'dw_subtotal_select', 'description', 'description', 'C', 1, null, null, null, 'PWRPLANT', to_date('27-02-2015 14:05:22', 'dd-mm-yyyy hh24:mi:ss'), 1, null, null);

insert into pp_dynamic_filter (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values (270, 'Dynamic Subtotal', 'dw', null, null, 'dw_wo_subtotal_select', 'description', 'description', 'C', 1, null, null, null, 'PWRPLANT', to_date('02-03-2015 12:14:16', 'dd-mm-yyyy hh24:mi:ss'), 1, null, null);

insert into pp_dynamic_filter (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values (271, 'Dynamic Subtotal', 'dw', null, null, 'dw_pcm_subtotalnew_fp_select', 'subtotal_value', 'subtotal_value', 'C', 0, null, null, null, 'PWRPLANT', to_date('02-03-2015 12:14:16', 'dd-mm-yyyy hh24:mi:ss'), 1, null, null);

insert into pp_dynamic_filter (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values (272, 'Dynamic Subtotal', 'dw', null, null, 'dw_pcm_subtotalnew_wo_select', 'subtotal_value', 'subtotal_value', 'C', 0, null, null, null, 'PWRPLANT', to_date('02-03-2015 12:14:16', 'dd-mm-yyyy hh24:mi:ss'), 1, null, null);

insert into pp_dynamic_filter (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values (273, 'Dynamic Subtotal', 'dw', null, null, 'dw_pcm_subtotalnew_bi_select', 'subtotal_value', 'subtotal_value', 'C', 0, null, null, null, 'PWRPLANT', to_date('02-03-2015 12:14:16', 'dd-mm-yyyy hh24:mi:ss'), 1, null, null);


--------Link the Dynamic Filters to the Report Filter
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 24 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 213 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 27 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 28 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 219 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 32 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 34 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 33 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 238 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 242 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 243 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 244 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 245 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 247 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 29 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 30 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 31 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 268 );
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 269 );

insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (88, 266);
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (89, 271);

insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (90, 3);
insert into pp_dynamic_filter_mapping (pp_report_filter_id, filter_id) values (90, 264);

----need to turn off multi-threading since they join into temp work order.
insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, TURN_OFF_MULTI_THREAD)
values
   (300004, 'FP Actuals',
    'Funding Project Actuals - Actual charges within a user-specified span of time',
    'dw_pcm_rpt_fp_bdg_1000',
    'SUBTOTALNEW-FP, BV NEEDS TEMP_WORK_ORDER, FISCAL_YEAR, SELECT DENOMINATION',
    'PCM - FP - BDG - 1000', 6, 35, 2, 89, 1, 3, 1);

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, TURN_OFF_MULTI_THREAD)
values
   (300005, 'FP Actuals',
    'Funding Project Actuals - Actual charges within a user-specified span of time',
    'dw_pcm_rpt_fp_bdg_1000',
    'SUBTOTALNEW-FP, FISCAL_YEAR, SELECT DENOMINATION',
    'PCM - FP - BDG - 1000', 6, 36, 2, 89, 1, 3, 1);

update pp_reports
set pp_report_filter_id = 90
where report_id = 300002 ;


commit;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2331, 0, 2015, 1, 0, 0, 042975, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042975_pcm_bdg_afc_fp_rpt03_aar_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;