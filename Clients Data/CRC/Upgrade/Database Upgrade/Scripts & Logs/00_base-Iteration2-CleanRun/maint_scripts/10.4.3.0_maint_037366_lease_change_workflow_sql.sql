/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037366_lease_change_workflow_sql.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 04/14/2014 Kyle Peterson
||============================================================================
*/

update WORKFLOW_SUBSYSTEM
   set SEND_SQL = 'select PKG_LEASE_CALC.F_SEND_ILR(<<id_field1>>, <<id_field2>>) from dual',
       APPROVE_SQL = 'select PKG_LEASE_CALC.F_APPROVE_ILR(<<id_field1>>, <<id_field2>>) from dual',
       REJECT_SQL = 'select PKG_LEASE_CALC.F_REJECT_ILR(<<id_field1>>, <<id_field2>>) from dual',
       UNREJECT_SQL = 'select PKG_LEASE_CALC.F_UNREJECT_ILR(<<id_field1>>, <<id_field2>>) from dual',
       UNSEND_SQL = 'select PKG_LEASE_CALC.F_UNSEND_ILR(<<id_field1>>, <<id_field2>>) from dual',
       UPDATE_WORKFLOW_TYPE_SQL = 'select PKG_LEASE_CALC.F_UPDATE_WORKFLOW_ILR(<<id_field1>>, <<id_field2>>) from dual'
 where UPPER(SUBSYSTEM) = 'ILR_APPROVAL';

update WORKFLOW_SUBSYSTEM
   set SEND_SQL = 'select PKG_LEASE_CALC.F_SEND_MLA(<<id_field1>>, <<id_field2>>) from dual',
       APPROVE_SQL = 'select PKG_LEASE_CALC.F_APPROVE_MLA(<<id_field1>>, <<id_field2>>) from dual',
       REJECT_SQL = 'select PKG_LEASE_CALC.F_REJECT_MLA(<<id_field1>>, <<id_field2>>) from dual',
       UNREJECT_SQL = 'select PKG_LEASE_CALC.F_UNREJECT_MLA(<<id_field1>>, <<id_field2>>) from dual',
       UNSEND_SQL = 'select PKG_LEASE_CALC.F_UNSEND_MLA(<<id_field1>>, <<id_field2>>) from dual',
       UPDATE_WORKFLOW_TYPE_SQL = 'select PKG_LEASE_CALC.F_UPDATE_WORKFLOW_MLA(<<id_field1>>, <<id_field2>>) from dual'
 where UPPER(SUBSYSTEM) = 'MLA_APPROVAL';

update WORKFLOW_SUBSYSTEM
   set SEND_SQL = 'select PKG_LEASE_CALC.F_SEND_PAYMENT(<<id_field1>>) from dual',
       APPROVE_SQL = 'select PKG_LEASE_CALC.F_APPROVE_PAYMENT(<<id_field1>>) from dual',
       REJECT_SQL = 'select PKG_LEASE_CALC.F_REJECT_PAYMENT(<<id_field1>>) from dual',
       UNREJECT_SQL = 'select PKG_LEASE_CALC.F_UNREJECT_PAYMENT(<<id_field1>>) from dual',
       UNSEND_SQL = 'select PKG_LEASE_CALC.F_UNSEND_PAYMENT(<<id_field1>>) from dual',
       UPDATE_WORKFLOW_TYPE_SQL = 'select PKG_LEASE_CALC.F_UPDATE_WORKFLOW_PAYMENT(<<id_field1>>) from dual'
 where UPPER(SUBSYSTEM) = 'LS_PAYMENT_APPROVAL';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1110, 0, 10, 4, 3, 0, 37366, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037366_lease_change_workflow_sql.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
