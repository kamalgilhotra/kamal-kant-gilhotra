/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044025_lease_import_fixes_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/15/2015 Will Davis     Fixed various import issues
||============================================================================
*/

insert into pp_import_column(import_type_id, column_name, description, is_required, processing_order, column_type, is_on_table)
select
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_component') AS import_type_id,
   'ilr_id' AS column_name,
   'ILR ID' AS description,
   0 AS is_required,
   1 AS processing_order,
   'number(22,0)' AS column_type,
   1 AS is_on_table
from dual;

insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name)
select
pit.import_template_id, 15, pit.import_type_id, 'ilr_id'
from pp_import_template pit
where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_component')
;

insert into pp_import_lookup(import_lookup_id, description, long_description, column_name, lookup_sql,
   is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql,
   derived_autocreate_yn)
select
   (select max(import_lookup_id)+1 from pp_import_lookup where import_lookup_id between 2501 and 3000) import_lookup_id,
   'LS Asset.Leased Asset Number' AS description,
   'LS Asset.Leased Asset Number with ILR ID' AS long_Description,
   'ls_asset_id' AS column_name,
   '( select b.ls_asset_id from ls_asset b where upper( trim( <importfield> ) ) = upper( trim( b.leased_asset_number ) ) and <importtable>.ilr_id = b.ilr_id )' AS lookup_sql,
   0 AS is_derived,
   'ls_asset' AS lookup_table_name,
   'leased_asset_number' AS lookup_column_name,
   'ilr_id' AS lookup_constraining_columns,
   'select leased_asset_number from ls_asset where ls_asset_status_id<=3' AS lookup_values_alternate_sql,
   null AS derived_autocreate_yn
from dual
where not exists(
   select 1
   from pp_import_lookup
   where lookup_column_name = 'leased_asset_number'
   and lookup_constraining_columns = 'ilr_id')
;

update pp_import_template_fields
set import_lookup_id = (
   select import_lookup_id
   from pp_import_lookup
   where lookup_column_name = 'leased_asset_number'
   and lookup_constraining_columns = 'ilr_id')
where import_type_id = 255
and column_name = 'ls_asset_id'
;

insert into pp_import_lookup(import_lookup_id, description, long_description, column_name, lookup_sql,
   is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql,
   derived_autocreate_yn)
select
   (select max(import_lookup_id)+1 from pp_import_lookup where import_lookup_id between 2501 and 3000) import_lookup_id,
   'Class Code.Description (Lease)' AS description,
   'The passed in value corresponds to the Class Code: Description field.  Translate to the Class Code ID using the Description column on the Class Code table.' AS long_Description,
   'class_code_id' AS column_name,
   '( select cc.class_code_id from class_code cc where upper( trim( <importfield> ) ) = upper( trim( cc.description ) ) and (cc.mla_indicator = 1 or cc.ilr_indicator = 1 or cc.asset_indicator = 1))' AS lookup_sql,
   0 AS is_derived,
   'class_code' AS lookup_table_name,
   'description' AS lookup_column_name,
   null AS lookup_constraining_columns,
   'where class_code.mla_indicator = 1 or class_code.ilr_indicator = 1 or class_code.asset_indicator = 1' AS lookup_values_alternate_sql,
   null AS derived_autocreate_yn
from dual;

update pp_import_template_fields
set import_lookup_id = (
   select import_lookup_id
   from pp_import_lookup
   where description = 'Class Code.Description (Lease)')
where import_type_id in (select import_type_id from pp_import_type where lower(import_table_name) like 'ls%')
and column_name like 'class_code_id%';


update pp_import_lookup
set long_description = 'LS ASSET.LEASED ASSET NUMBER with ASSET ID',
lookup_sql = '( select case when ls_asset_override is not null then
 (select to_char(c.ls_asset_id) from
 ls_asset c where ls_asset_override = to_char(c.ls_asset_id) )
   else (select to_char(b.ls_asset_id) end from
   ls_asset b where upper( trim( <importfield> ) ) = upper( trim( b.leased_asset_number ) ) and <importtable>.company_id = b.company_id) end from dual where 1=1)',
lookup_constraining_columns = 'company_id'
where upper(long_description) = 'LS ASSET.LEASED ASSET NUMBER WITH ILR ID';

update pp_import_template_fields
set import_lookup_id = (select import_lookup_id from pp_import_lookup where long_description = 'LS ASSET.LEASED ASSET NUMBER with ASSET ID')
where column_name = 'ls_asset_id'
and import_type_id in (255, 261, 262);

insert into pp_import_column(import_type_id, column_name, description, is_required, processing_order, column_type, is_on_table)
select import_type_id, 'ls_asset_override', 'LS Asset ID Override', 0, 2, 'varchar2(254)', 1
from pp_import_type
where import_type_id in (255, 261, 262);

update pp_import_template_fields
set column_name = 'ls_asset_override'
where column_name = 'ilr_id'
and import_type_id = 255;

insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
select pit.import_template_id , max(field_id) + 1, pit.import_type_id, 'ls_asset_override', null
from pp_import_template_fields pit
where pit.import_type_id in (261,262)
group by pit.import_template_id, pit.import_type_id;

delete from pp_import_column
where column_name = 'ilr_id'
and import_type_id = 255;


update pp_Import_type z set archive_additional_columns = (
	select lower(listagg(column_name, ', ') within group (order by column_id, import_type_id))
	from all_tab_cols a, pp_import_type pp
	where pp.import_table_name = lower(a.table_name)
	and import_type_id between 250 and 262
	and (a.column_name, import_type_id) not in (
		select upper(column_name), import_type_id
		from pp_import_column
		where import_type_id between 250 and 262
		union all
		select  nvl(upper(import_column_name),'-1'), import_type_id
		from pp_import_column
		where import_type_id between 250 and 262)
	and lower(column_name) not in ('import_run_id','line_id','time_stamp','user_id','loaded','is_modified','error_message')
	and import_type_id = z.import_type_id
	group by import_Type_id, table_name)
where import_type_id between 250 and 262;

insert into ls_funding_status
(funding_status_id, description, long_description)
values
(5, 'Retired', 'The ILR has been retired');

update ls_ilr
set funding_status_id = 5
where ilr_status_id = 3;

update ppbase_actions_windows set action_text = 'Delete Revision'
where action_text = 'Delete';

update ls_import_asset set estimated_residual_tmp = estimated_residual;
update ls_import_asset_archive set estimated_residual_tmp = estimated_residual;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2613, 0, 2015, 2, 0, 0, 044025, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044025_lease_import_fixes_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;