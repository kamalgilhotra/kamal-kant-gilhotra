/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_052074_lessee_01_change_lessee_2015_report_time_option_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 07/24/2018 David Conway     Change Report Time option for Lessee - 2015 consolidated
||============================================================================
*/

update pp_reports set pp_report_time_option_id = 1 where report_number = 'Lessee - 2015 - Consolidated';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8683, 0, 2017, 4, 0, 0, 52074, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_052074_lessee_01_change_lessee_2015_report_time_option_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;