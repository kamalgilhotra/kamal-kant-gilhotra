/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009449_jes_on_payment.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   04/02/2012 Brandon Beck   Point Release
||============================================================================
*/

insert into REIMB_JE_TIMING (REIMB_JE_TIMING_ID, DESCRIPTION) values (12, 'Payment');

alter table REIMB_GL_TRANSACTION
   add IS_REVERSAL number(22, 0) default 0;

alter table REIMB_GL_TRANSACTION
   add IS_PAYMENT number(22, 0) default 0;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (127, 0, 10, 3, 5, 0, 9449, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009449_jes_on_payment.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
