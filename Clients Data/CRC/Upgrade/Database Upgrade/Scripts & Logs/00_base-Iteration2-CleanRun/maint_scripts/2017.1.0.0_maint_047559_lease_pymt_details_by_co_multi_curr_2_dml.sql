/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047559_lease_pymt_details_by_co_multi_curr_2_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 05/22/2017 Shane Ward   Create new view for Mutli Currency Lease Taxes
||============================================================================
*/

UPDATE pp_any_query_criteria SET sql = 'select to_char(lpl.gl_posting_mo_yr, ''yyyymm'') as monthnum,
co.company_id,
co.description as company_description,
lct.description lease_cap_type,
nvl(sum(decode(lpl.payment_type_id, 1, lpl.amount, 0)), 0) principal_payment,
nvl(sum(decode(lpl.payment_type_id, 2, lpl.amount, 0)), 0) interest_payment,
nvl(sum(decode(lpl.payment_type_id, 2, lpl.adjustment_amount, 0)), 0) interest_adjustment,
nvl(sum(decode(lpl.payment_type_id, 3, lpl.amount, 0)), 0) executory_payment1,
nvl(sum(decode(lpl.payment_type_id, 3, lpl.adjustment_amount, 0)), 0) executory_adjustment1,
nvl(sum(decode(lpl.payment_type_id, 4, lpl.amount, 0)), 0) executory_payment2,
nvl(sum(decode(lpl.payment_type_id, 4, lpl.adjustment_amount, 0)), 0) executory_adjustment2,
nvl(sum(decode(lpl.payment_type_id, 5, lpl.amount, 0)), 0) executory_payment3,
nvl(sum(decode(lpl.payment_type_id, 5, lpl.adjustment_amount, 0)), 0) executory_adjustment3,
nvl(sum(decode(lpl.payment_type_id, 6, lpl.amount, 0)), 0) executory_payment4,
nvl(sum(decode(lpl.payment_type_id, 6, lpl.adjustment_amount, 0)), 0) executory_adjustment4,
nvl(sum(decode(lpl.payment_type_id, 7, lpl.amount, 0)), 0) executory_payment5,
nvl(sum(decode(lpl.payment_type_id, 7, lpl.adjustment_amount, 0)), 0) executory_adjustment5,
nvl(sum(decode(lpl.payment_type_id, 8, lpl.amount, 0)), 0) executory_payment6,
nvl(sum(decode(lpl.payment_type_id, 8, lpl.adjustment_amount, 0)), 0) executory_adjustment6,
nvl(sum(decode(lpl.payment_type_id, 9, lpl.amount, 0)), 0) executory_payment7,
nvl(sum(decode(lpl.payment_type_id, 9, lpl.adjustment_amount, 0)), 0) executory_adjustment7,
nvl(sum(decode(lpl.payment_type_id, 10, lpl.amount, 0)), 0) executory_payment8,
nvl(sum(decode(lpl.payment_type_id, 10, lpl.adjustment_amount, 0)), 0) executory_adjustment8,
nvl(sum(decode(lpl.payment_type_id, 11, lpl.amount, 0)), 0) executory_payment9,
nvl(sum(decode(lpl.payment_type_id, 11, lpl.adjustment_amount, 0)), 0) executory_adjustment9,
nvl(sum(decode(lpl.payment_type_id, 12, lpl.amount, 0)), 0) executory_payment10,
nvl(sum(decode(lpl.payment_type_id, 12, lpl.adjustment_amount, 0)), 0) executory_adjustment10,
nvl(sum(decode(lpl.payment_type_id, 13, lpl.amount, 0)), 0) contingent_payment1,
nvl(sum(decode(lpl.payment_type_id, 13, lpl.adjustment_amount, 0)), 0) contingent_adjustment1,
nvl(sum(decode(lpl.payment_type_id, 14, lpl.amount, 0)), 0) contingent_payment2,
nvl(sum(decode(lpl.payment_type_id, 14, lpl.adjustment_amount, 0)), 0) contingent_adjustment2,
nvl(sum(decode(lpl.payment_type_id, 15, lpl.amount, 0)), 0) contingent_payment3,
nvl(sum(decode(lpl.payment_type_id, 15, lpl.adjustment_amount, 0)), 0) contingent_adjustment3,
nvl(sum(decode(lpl.payment_type_id, 16, lpl.amount, 0)), 0) contingent_payment4,
nvl(sum(decode(lpl.payment_type_id, 16, lpl.adjustment_amount, 0)), 0) contingent_adjustment4,
nvl(sum(decode(lpl.payment_type_id, 17, lpl.amount, 0)), 0) contingent_payment5,
nvl(sum(decode(lpl.payment_type_id, 17, lpl.adjustment_amount, 0)), 0) contingent_adjustment5,
nvl(sum(decode(lpl.payment_type_id, 18, lpl.amount, 0)), 0) contingent_payment6,
nvl(sum(decode(lpl.payment_type_id, 18, lpl.adjustment_amount, 0)), 0) contingent_adjustment6,
nvl(sum(decode(lpl.payment_type_id, 19, lpl.amount, 0)), 0) contingent_payment7,
nvl(sum(decode(lpl.payment_type_id, 19, lpl.adjustment_amount, 0)), 0) contingent_adjustment7,
nvl(sum(decode(lpl.payment_type_id, 20, lpl.amount, 0)), 0) contingent_payment8,
nvl(sum(decode(lpl.payment_type_id, 20, lpl.adjustment_amount, 0)), 0) contingent_adjustment8,
nvl(sum(decode(lpl.payment_type_id, 21, lpl.amount, 0)), 0) contingent_payment9,
nvl(sum(decode(lpl.payment_type_id, 21, lpl.adjustment_amount, 0)), 0) contingent_adjustment9,',
   sql2 = 'nvl(sum(decode(lpl.payment_type_id, 22, lpl.amount, 0)), 0) contingent_payment10,
nvl(sum(decode(lpl.payment_type_id, 22, lpl.adjustment_amount, 0)), 0) contingent_adjustment10,
nvl(sum(decode(lpl.payment_type_id, 23, lpl.amount, 0)), 0) termination_penalty,
nvl(sum(decode(lpl.payment_type_id, 24, lpl.amount, 0)), 0) sales_proceeds,
sum(nvl(tax_1, 0)) as tax_1,
sum(nvl(tax_1_adjustment, 0)) as tax_1_adjustment,
sum(nvl(tax_2, 0)) as tax_2,
sum(nvl(tax_2_adjustment, 0)) as tax_2_adjustment,
sum(nvl(tax_3, 0)) as tax_3,
sum(nvl(tax_3_adjustment, 0)) as tax_3_adjustment,
sum(nvl(tax_4, 0)) as tax_4,
sum(nvl(tax_4_adjustment, 0)) as tax_4_adjustment,
sum(nvl(tax_5, 0)) as tax_5,
sum(nvl(tax_5_adjustment, 0)) as tax_5_adjustment,
sum(nvl(tax_6, 0)) as tax_6,
sum(nvl(tax_6_adjustment, 0)) as tax_6_adjustment,
sum(nvl(tax_7, 0)) as tax_7,
sum(nvl(tax_7_adjustment, 0)) as tax_7_adjustment,
(select initcap(filter_value)
from pp_any_required_filter
where upper(trim(column_name)) =
''CURRENCY TYPE'') currency_type,
lpl.iso_code currency,
lpl.currency_display_symbol currency_symbol
from ls_asset la,
company co,
v_ls_payment_line_fx lpl,
asset_location al,
ls_ilr ilr,
ls_lease ll,
ls_ilr_options ilro,
ls_lease_cap_type lct,
(select lmt.ls_asset_id,
nvl(sum(decode(tax_local_id, 1, amount, 0)), 0) as tax_1,
nvl(sum(decode(tax_local_id, 1, adjustment_amount, 0)), 0) as tax_1_adjustment,
nvl(sum(decode(tax_local_id, 2, amount, 0)), 0) as tax_2,
nvl(sum(decode(tax_local_id, 2, adjustment_amount, 0)), 0) as tax_2_adjustment,
nvl(sum(decode(tax_local_id, 3, amount, 0)), 0) as tax_3,
nvl(sum(decode(tax_local_id, 3, adjustment_amount, 0)), 0) as tax_3_adjustment,
nvl(sum(decode(tax_local_id, 4, amount, 0)), 0) as tax_4,
nvl(sum(decode(tax_local_id, 4, adjustment_amount, 0)), 0) as tax_4_adjustment,
nvl(sum(decode(tax_local_id, 5, amount, 0)), 0) as tax_5,
nvl(sum(decode(tax_local_id, 5, adjustment_amount, 0)), 0) as tax_5_adjustment,
nvl(sum(decode(tax_local_id, 6, amount, 0)), 0) as tax_6,
nvl(sum(decode(tax_local_id, 6, adjustment_amount, 0)), 0) as tax_6_adjustment,
nvl(sum(decode(tax_local_id, 7, amount, 0)), 0) as tax_7,
nvl(sum(decode(tax_local_id, 7, adjustment_amount, 0)), 0) as tax_7_adjustment,
contract_currency_id,
company_currency_id,
ls_cur_type
from v_ls_monthly_tax_fx_vw lmt
where to_char(gl_posting_mo_yr, ''yyyymm'') in
(select filter_value
from pp_any_required_filter
where upper(trim(column_name)) = ''MONTHNUM'')
and lmt.accrual = 0
AND lmt.ls_cur_type =
(SELECT ls_currency_type_id
FROM ls_lease_currency_type
WHERE Upper(description) =
Upper((select filter_value
from pp_any_required_filter
where upper(trim(column_name)) =
''CURRENCY TYPE'')))
and to_char(lmt.company_id) in
(select filter_value
from pp_any_required_filter
where upper(trim(column_name)) = ''COMPANY ID'')
group by lmt.ls_asset_id,
contract_currency_id,
company_currency_id,
ls_cur_type) taxes
where la.company_id = co.company_id
and to_char(la.company_id) in
(select filter_value
from pp_any_required_filter
where upper(trim(column_name)) = ''COMPANY ID'')
and to_char(lpl.gl_posting_mo_yr, ''yyyymm'') in
(select filter_value
from pp_any_required_filter
where upper(trim(column_name)) = ''MONTHNUM'')
and lpl.ls_asset_id = la.ls_asset_id
and la.asset_location_id = al.asset_location_id
and la.ilr_id = ilr.ilr_id
and ilr.lease_id = ll.lease_id
and la.ilr_id = ilro.ilr_id
and la.approved_revision = ilro.revision
and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
and la.ls_asset_id = taxes.ls_asset_id(+)
AND lpl.ls_cur_type =
(SELECT ls_currency_type_id
FROM ls_lease_currency_type
WHERE Upper(description) =
Upper((select filter_value
        from pp_any_required_filter
      where upper(trim(column_name)) = ''CURRENCY TYPE'')))
group by lpl.gl_posting_mo_yr,
co.company_id,
co.description,
lct.description,
lpl.ls_cur_type,
lpl.iso_code,
lpl.currency_display_symbol'
WHERE description = 'Lease Payment Detail by Company';


--Currency Type
INSERT INTO pp_any_query_criteria_fields (id, detail_field, column_order, amount_field, include_in_select_criteria, column_Header,
column_width, display_field, display_table, column_type,
quantity_field, data_field, required_filter, required_one_mult, hide_from_results, hide_from_filters)
SELECT id, 'currency_type', 64,  0,1, 'Currency Type', 300, 'description', 'ls_lease_currency_type', 'VARCHAR2', 0, 'ls_currency_type_id', 1, 2, 1, 0
FROM pp_any_query_criteria WHERE description = 'Lease Payment Detail by Company'
;

--Currency Symbol
INSERT INTO pp_any_query_criteria_fields (id, detail_field, column_order, amount_field, include_in_select_criteria, column_Header,
column_width, display_field, display_table, column_type,
quantity_field, data_field, required_filter, required_one_mult, hide_from_results, hide_from_filters)
SELECT id, 'currency_symbol', 65,  0, 0, 'Currency Symbol', 300, null, null, 'VARCHAR2', 0, null, null, null, 1, 1
FROM pp_any_query_criteria WHERE description = 'Lease Payment Detail by Company'
;
--Currency
INSERT INTO pp_any_query_criteria_fields (id, detail_field, column_order, amount_field, include_in_select_criteria, column_Header,
column_width, display_field, display_table, column_type,
quantity_field, data_field, required_filter, required_one_mult, hide_from_results, hide_from_filters)
SELECT id, 'currency', 66,  0, 0, 'Currency', 300, null, null, 'VARCHAR2', 0, null, null, null, 0, 1
FROM pp_any_query_criteria WHERE description = 'Lease Payment Detail by Company'
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3515, 0, 2017, 1, 0, 0, 47559, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047559_lease_pymt_details_by_co_multi_curr_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	