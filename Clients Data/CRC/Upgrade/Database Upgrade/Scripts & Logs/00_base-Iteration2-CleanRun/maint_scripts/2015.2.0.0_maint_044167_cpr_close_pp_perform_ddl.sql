/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044167_cpr_close_pp_perform_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2 06/19/2015 Daniel Mendel		Add index to speed up Close PowerPlant
||============================================================================
*/

create index arc_gl_transaction_idx on arc_gl_transaction(gl_trans_id) tablespace PWRPLANT_IDX;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2621, 0, 2015, 2, 0, 0, 044167, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044167_cpr_close_pp_perform_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;