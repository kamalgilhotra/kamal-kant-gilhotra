/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_048330_lease_10_summ_deferred_rent_ddl.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2017.1.0.0 07/31/2017 Jared Schwantz     	Adding summary columns for deferred rent
||========================================================================================
*/

alter table ls_summary_forecast add beg_deferred_rent number(22,2);

alter table ls_summary_forecast add deferred_rent number(22,2);

alter table ls_summary_forecast add end_deferred_rent number(22,2);

alter table ls_summary_forecast add beg_st_deferred_rent number(22,2);

alter table ls_summary_forecast add end_st_deferred_rent number(22,2);

COMMENT ON COLUMN ls_summary_forecast.beg_deferred_rent IS 'The beginning deferred rent amount for the period.';

COMMENT ON COLUMN ls_summary_forecast.deferred_rent IS 'The deferred rent amount for the period.';

COMMENT ON COLUMN ls_summary_forecast.end_deferred_rent IS 'The ending deferred rent amount for the period.';

COMMENT ON COLUMN ls_summary_forecast.beg_st_deferred_rent IS 'The beginning short term deferred rent amount for the period.';

COMMENT ON COLUMN ls_summary_forecast.end_st_deferred_rent IS 'The ending short term deferred rent amount for the period.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3616, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_10_summ_deferred_rent_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;