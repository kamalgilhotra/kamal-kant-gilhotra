/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050388_lessor_03_add_ilr_to_multicurrency_audit_table_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 03/16/2018 Jared Watkins     Add ILR ID to the Lease Multicurrency Audit table
||============================================================================
*/

alter table ls_mc_gl_transaction_audit
add ilr_id number(22,0) null;

comment on column ls_mc_gl_transaction_audit.ilr_id is 'The unique identifier for the ILR having GL Transactions created.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4216, 0, 2017, 3, 0, 0, 50388, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050388_lessor_03_add_ilr_to_multicurrency_audit_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;