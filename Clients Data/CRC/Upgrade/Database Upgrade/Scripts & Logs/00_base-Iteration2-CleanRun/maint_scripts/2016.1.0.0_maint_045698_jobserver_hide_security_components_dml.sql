/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045698_jobserver_hide_security_components_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| --------    ---------- -------------- ----------------------------------------
|| 2016.1.0.0  05/19/2016 David Haupt    Removing security options for non-secured modules
||============================================================================
*/

UPDATE pp_web_modules
SET classification = 'DISABLED'
WHERE module_name IN ('Analytics', 'PCMAnalytics', 'Charting')
;

--add service jobs to allowed Job Flow jobs
INSERT INTO pp_web_security_perm_control
(objects, module_id, component_id, SECTION, name)
VALUES
('IntegrationManager.JobFlows.AddCloneServiceJobs', 9, 7, 'Add/Clone', 'Service Jobs')
;

INSERT INTO pp_web_security_perm_control
(objects, module_id, component_id, SECTION, name)
VALUES
('IntegrationManager.JobFlows.EditServiceJobs', 9, 7, 'Edit', 'Service Jobs')
;

INSERT INTO pp_web_security_perm_control
(objects, module_id, component_id, SECTION, name)
VALUES
('IntegrationManager.JobFlows.DeleteServiceJobs', 9, 7, 'Delete', 'Service Jobs')
;

--add service jobs to allowed Schedule jobs
INSERT INTO pp_web_security_perm_control
(objects, module_id, pseudo_component, SECTION, name)
VALUES
('JobService.Workflow.Schedule.Service', 9, 'Schedule', 'Add/Clone', 'Service Jobs')
;

INSERT INTO pp_web_security_perm_control
(objects, module_id, pseudo_component, SECTION, name)
VALUES
('JobService.Workflow.Reschedule.Service', 9, 'Schedule', 'Edit', 'Service Jobs')
;

INSERT INTO pp_web_security_perm_control
(objects, module_id, pseudo_component, SECTION, name)
VALUES
('JobService.Workflow.Delete.Service', 9, 'Schedule', 'Delete', 'Service Jobs')
;

--remove input and notification jobs from allowed Schedule jobs
DELETE FROM pp_web_security_perm_control
WHERE module_id = 9
AND pseudo_component = 'Schedule'
AND name IN ('Input Jobs', 'Notification')
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3193, 0, 2016, 1, 0, 0, 45698, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045698_jobserver_hide_security_components_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;