/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009773_taxrpr_general_actuals2.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   08/22/2013 Alex P.         Point Release
||============================================================================
*/

alter table WORK_ORDER_TAX_STATUS add TESTED_ACTUALS number(1,0);

alter table WO_EST_PROCESSING_TEMP drop column WORK_ORDER_NUMBER;
alter table WO_EST_PROCESSING_TEMP drop column COMPANY_ID;

delete PP_PROCESSES_MESSAGES
 where PROCESS_ID in (select PROCESS_ID from PP_PROCESSES where DESCRIPTION = 'Tax Repairs Status Update');

delete PP_PROCESSES_OCCURRENCES
 where PROCESS_ID in (select PROCESS_ID from PP_PROCESSES where DESCRIPTION = 'Tax Repairs Status Update');

delete PP_PROCESSES where DESCRIPTION = 'Tax Repairs Status Update';

comment on column WORK_ORDER_TAX_STATUS.TESTED_ACTUALS is 'A binary flag indicating whether actuals were used for tax status testing. 1 = ACTUALS, 0 or null = ESTIMATES.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (528, 0, 10, 4, 1, 0, 9773, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_009773_taxrpr_general_actuals2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
