/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035520_system_dynfilter.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014 Alex P.        Point Release
||============================================================================
*/

alter table PP_DYNAMIC_FILTER add INVISIBLE number(1, 0);
comment on column PP_DYNAMIC_FILTER.INVISIBLE is 'Binary flag indicating whether the filter is invisible. 1 = invisible, 0 or NULL (default) = visible';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (894, 0, 10, 4, 2, 0, 35520, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035520_system_dynfilter.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;