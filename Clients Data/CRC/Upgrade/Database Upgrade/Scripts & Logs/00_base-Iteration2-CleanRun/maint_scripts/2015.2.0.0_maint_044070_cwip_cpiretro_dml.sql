/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044070_cwip_cpiretro.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 07/1/2015 Sunjin Cone	New
||============================================================================
*/

INSERT INTO cpi_retro_adjust_type (CPI_RETRO_ADJUST_TYPE_ID, DESCRIPTION)
VALUES (3, 'CPI Should Be $0.00'); 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2692, 0, 2015, 2, 0, 0, 044070, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044070_cwip_cpiretro_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;