/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007264_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date             Revised By           Reason for Change
|| ----------  ---------------  -------------------  -------------------------
|| 10.4.2.0    12/18/2013       Andrew Scott         Remove escalate_reserve_yn
||                                                   and any of its references.
||============================================================================
*/

alter table PROPERTY_TAX_TYPE_DATA drop column ESCALATE_RESERVE_YN;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (817, 0, 10, 4, 2, 0, 7264, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_007264_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;