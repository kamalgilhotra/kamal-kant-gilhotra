/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029851_projects_table_storage.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/29/2013 Lee Quinn      Point Release
||============================================================================
*/

alter table RWIP_CLOSEOUT_STAG_AL
   move tablespace PWRPLANT
      storage (initial 64K next 64K)
               pctfree 40
               initrans 4
               maxtrans 255;

alter table RWIP_CLOSEOUT_STAGING
   move tablespace PWRPLANT
      storage (initial 64K next 64K)
               pctfree 20
               initrans 4
               maxtrans 255;

create index RWIP_CLOSEOUT_STAG_AL_PERF
   on RWIP_CLOSEOUT_STAG_AL (GL_POST_MONTH_NUMBER, COMPANY_ID)
      storage(initial 64K next 64K)
              pctfree 20
              tablespace PWRPLANT_IDX;

create index RWIP_CLOSEOUT_STAGING_PERF
   on RWIP_CLOSEOUT_STAGING (CHARGE_ID, DEPR_ACTIVITY_CODE_ID)
      storage (initial 64K next 64K)
               pctfree 20 tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (368, 0, 10, 4, 0, 0, 29851, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029851_projects_table_storage.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
