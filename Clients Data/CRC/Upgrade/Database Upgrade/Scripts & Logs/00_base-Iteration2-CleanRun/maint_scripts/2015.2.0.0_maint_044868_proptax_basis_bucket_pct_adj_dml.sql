/*
||==========================================================================================
|| Application: PowerPlan
|| Module: Property Tax
|| File Name:   maint_044868_proptax_basis_bucket_pct_adj_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.2    09/01/2015  Graham Miller    Add ability to adjust basis buckets by a percentage during extraction
||==========================================================================================
*/


--Update all basis bucket adjustments to have a adjustment percent of 100%
Update prop_tax_type_prop_tax_adjust
set auto_adjust_percent = 1 where auto_adjust_percent is null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2839, 0, 2015, 2, 0, 0, 044868, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044868_proptax_basis_bucket_pct_adj_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;