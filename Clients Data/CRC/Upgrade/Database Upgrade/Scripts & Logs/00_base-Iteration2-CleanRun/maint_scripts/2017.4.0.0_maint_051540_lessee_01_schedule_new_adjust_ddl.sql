/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_051540_lessee_01_schedule_new_adjust_ddl.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- --------------------------------------
 || 2017.4.0   06/06/2018   K. Powers      PP-51540 - Add adjustment columns to 
 ||                                        schedule tables
 ||============================================================================
 */ 

 ALTER TABLE LS_ASSET_SCHEDULE ADD (EXECUTORY_ADJUST NUMBER(22,2));
 ALTER TABLE LS_ASSET_SCHEDULE ADD (CONTINGENT_ADJUST NUMBER(22,2));
 
 ALTER TABLE LS_ILR_SCHEDULE ADD (EXECUTORY_ADJUST NUMBER(22,2));
 ALTER TABLE LS_ILR_SCHEDULE ADD (CONTINGENT_ADJUST NUMBER(22,2));
 
 COMMENT ON COLUMN "PWRPLANT"."LS_ASSET_SCHEDULE"."EXECUTORY_ADJUST" IS 'Summated value of all Executory Adjusted values, by Asset';
 COMMENT ON COLUMN "PWRPLANT"."LS_ASSET_SCHEDULE"."CONTINGENT_ADJUST" IS 'Summated value of all Contingent Adjusted values, by Asset';
 
 COMMENT ON COLUMN "PWRPLANT"."LS_ILR_SCHEDULE"."EXECUTORY_ADJUST" IS 'Summated value of all Executory Adjusted values, by ILR';
 COMMENT ON COLUMN "PWRPLANT"."LS_ILR_SCHEDULE"."CONTINGENT_ADJUST" IS 'Summated value of all Contingent Adjusted values, by ILR';
 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6827, 0, 2017, 4, 0, 0, 51540, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051540_lessee_01_schedule_new_adjust_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;


