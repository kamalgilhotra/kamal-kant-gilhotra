/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052146_lessee_02_ifrs_sob_remeasure_config_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2018.1.0.0  09/05/2018 Shane "C" Ward    New system control and column on rent 
||											bucket admin for IFRS Set of Books Remeasurements
||============================================================================
*/

--New System Control for IFRS Set of Books Remeasurement
INSERT INTO pp_system_control (control_id, control_name, control_value, description, long_description, company_id)
SELECT Max(control_id) + 1 , 'IFRS Set of Books Remeasurement', 'no', 'dw_yes_no;1', '''Yes'' states the companies selected, are using an IFRS set of books remeasurement process in their accounting', -1
FROM pp_system_control
WHERE NOT EXISTS (SELECT 1 FROM pp_system_control WHERE Upper(control_name) = 'CALCULATE IFRS SET OF BOOKS REMEASUREMENT') ;

--New Column on Rent bucket table admin for the new flag
UPDATE powerplant_columns SET column_rank = column_rank+1 WHERE column_rank > 8 AND Lower(table_name) = 'ls_rent_bucket_admin';

INSERT INTO powerplant_columns (column_name, table_name, dropdown_name, pp_edit_type_id, description, column_rank, read_only)
SELECT 'incl_in_ifrs_remeasure', 'ls_rent_bucket_admin', 'status_code', 'p', 'Include in IFRS Remeasurement', 9, 0
FROM dual WHERE 'incl_in_ifr_remeasure' NOT IN (SELECT column_name FROM powerplant_columns WHERE Lower(table_name) = 'ls_rent_bucket_admin');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9523, 0, 2018, 1, 0, 0, 52146, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052146_lessee_02_ifrs_sob_remeasure_config_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	