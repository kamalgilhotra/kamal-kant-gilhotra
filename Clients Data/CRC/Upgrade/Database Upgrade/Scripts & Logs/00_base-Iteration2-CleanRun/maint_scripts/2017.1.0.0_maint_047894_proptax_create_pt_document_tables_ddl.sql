/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047894_proptax_create_pt_document_tables_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 05/07/2017 Josh Sandler   Create new tables supporting PropertyTax Bill/Statement Group attachment functionality
||============================================================================
*/

CREATE TABLE pt_statement_document (
  statement_id          NUMBER(22,0) NOT NULL,
  statement_year_id     NUMBER(22,0) NOT NULL,
  document_id	          NUMBER(22,0) NOT NULL,
  document_data       	BLOB,
  notes                 VARCHAR2(2000 BYTE),
  file_name	            VARCHAR2(254),
  file_path	            VARCHAR2(2000),
  filesize	            NUMBER(22,0),
  original_attach_date	DATE,
  user_id	              VARCHAR2(18),
  time_stamp	          DATE
)
/

ALTER TABLE pt_statement_document
  ADD CONSTRAINT pk_pt_statement_document PRIMARY KEY (
    statement_id,
    statement_year_id,
    document_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE pt_statement_document
  ADD CONSTRAINT fk_pt_statement_doc FOREIGN KEY (
    statement_id
  ) REFERENCES pt_statement (
    statement_id
  )
/

ALTER TABLE pt_statement_document
  ADD CONSTRAINT fk_pt_statement_year_doc FOREIGN KEY (
    statement_year_id
  ) REFERENCES pt_statement_year (
    statement_year_id
  )
/

COMMENT ON TABLE pt_statement_document IS '(O)  [07]
The table stores uploaded bill images for individual bills.';

COMMENT ON COLUMN pt_statement_document.statement_id IS 'System-assigned identifier of a particular statement (i.e., bill).';
COMMENT ON COLUMN pt_statement_document.statement_year_id IS 'System-assigned identifier of a particular statement year.';
COMMENT ON COLUMN pt_statement_document.document_id IS 'System-assigned identifier of a particular document.';
COMMENT ON COLUMN pt_statement_document.document_data IS 'A blob that stores the actual document';
COMMENT ON COLUMN pt_statement_document.notes IS 'Notes about the document.';
COMMENT ON COLUMN pt_statement_document.file_name IS 'The name of the file when it was imported.';
COMMENT ON COLUMN pt_statement_document.file_path IS 'The file path where the file was uploaded from.';
COMMENT ON COLUMN pt_statement_document.filesize IS 'The size of the uploaded file.';
COMMENT ON COLUMN pt_statement_document.original_attach_date IS 'Time stamp when file was uploaded.';
COMMENT ON COLUMN pt_statement_document.time_stamp IS 'System-generated time stamp used for audit purposes.';
COMMENT ON COLUMN pt_statement_document.user_id IS 'System-generated user_id used for audit purposes.';


CREATE TABLE pt_statement_group_document (
  statement_group_id    NUMBER(22,0) NOT NULL,
  statement_year_id     NUMBER(22,0) NOT NULL,
  document_id	          NUMBER(22,0) NOT NULL,
  document_data       	BLOB,
  notes                 VARCHAR2(2000 BYTE),
  file_name	            VARCHAR2(254),
  file_path	            VARCHAR2(2000),
  filesize	            NUMBER(22,0),
  original_attach_date	DATE,
  user_id	              VARCHAR2(18),
  time_stamp	          DATE
)
/

ALTER TABLE pt_statement_group_document
  ADD CONSTRAINT pk_pt_statement_group_document PRIMARY KEY (
    statement_group_id,
    statement_year_id,
    document_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE pt_statement_group_document
  ADD CONSTRAINT fk_pt_statement_group_doc FOREIGN KEY (
    statement_group_id
  ) REFERENCES pt_statement_group (
    statement_group_id
  )
/

ALTER TABLE pt_statement_group_document
  ADD CONSTRAINT fk_pt_statement_group_year_doc FOREIGN KEY (
    statement_year_id
  ) REFERENCES pt_statement_year (
    statement_year_id
  )
/

COMMENT ON TABLE pt_statement_group_document IS '(O)  [07]
The table stores uploaded bill images for statement groups.';

COMMENT ON COLUMN pt_statement_group_document.statement_group_id IS 'System-assigned identifier of a statement group.';
COMMENT ON COLUMN pt_statement_group_document.statement_year_id IS 'System-assigned identifier of a particular statement year.';
COMMENT ON COLUMN pt_statement_group_document.document_id IS 'System-assigned identifier of a particular document.';
COMMENT ON COLUMN pt_statement_group_document.document_data IS 'A blob that stores the actual document';
COMMENT ON COLUMN pt_statement_group_document.notes IS 'Notes about the document.';
COMMENT ON COLUMN pt_statement_group_document.file_name IS 'The name of the file when it was imported.';
COMMENT ON COLUMN pt_statement_group_document.file_path IS 'The file path where the file was uploaded from.';
COMMENT ON COLUMN pt_statement_group_document.filesize IS 'The size of the uploaded file.';
COMMENT ON COLUMN pt_statement_group_document.original_attach_date IS 'Time stamp when file was uploaded.';
COMMENT ON COLUMN pt_statement_group_document.time_stamp IS 'System-generated time stamp used for audit purposes.';
COMMENT ON COLUMN pt_statement_group_document.user_id IS 'System-generated user_id used for audit purposes.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3484, 0, 2017, 1, 0, 0, 47894, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047894_proptax_create_pt_document_tables_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;