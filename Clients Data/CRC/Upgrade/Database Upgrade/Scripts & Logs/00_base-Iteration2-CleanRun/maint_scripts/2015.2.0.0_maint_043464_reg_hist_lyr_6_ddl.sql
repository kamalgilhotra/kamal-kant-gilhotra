/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043464_reg_hist_lyr_6_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/09/2015 Andrew Scott   drop fp id and revision from stg and arc tbls
||============================================================================
*/

alter table DEPR_CALC_STG drop column FUNDING_WO_ID;
alter table DEPR_CALC_STG drop column REVISION;

alter table CPR_DEPR_CALC_STG drop column FUNDING_WO_ID;
alter table CPR_DEPR_CALC_STG drop column REVISION;

alter table DEPR_CALC_STG_ARC drop column FUNDING_WO_ID;
alter table DEPR_CALC_STG_ARC drop column REVISION;

alter table FCST_DEPR_CALC_STG_ARC drop column FUNDING_WO_ID;
alter table FCST_DEPR_CALC_STG_ARC drop column REVISION;

alter table FCST_DEPR_LEDGER_BLEND_STG_ARC drop column FUNDING_WO_ID;
alter table FCST_DEPR_LEDGER_BLEND_STG_ARC drop column REVISION;

alter table DEPR_LEDGER_BLENDING_STG drop column FUNDING_WO_ID;
alter table DEPR_LEDGER_BLENDING_STG drop column REVISION;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2587, 0, 2015, 2, 0, 0, 043464, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043464_reg_hist_lyr_6_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;