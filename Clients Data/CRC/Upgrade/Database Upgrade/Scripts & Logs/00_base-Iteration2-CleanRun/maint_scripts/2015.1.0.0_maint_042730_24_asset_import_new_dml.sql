/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_24_asset_import_new_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/


insert into pp_import_column(import_type_id, column_name, description, import_column_name, is_required,
   processing_order, column_type, is_on_table)
select
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_asset') AS import_type_id,
   'used_yn_sw' AS column_name,
   'Used Yes/No Switch' AS description,
   null AS import_column_name,
   0 AS is_required,
   1 AS processing_order,
   'varchar2(35)' AS column_type,
   1 AS is_on_table
from dual
union
select
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_asset') AS import_type_id,
   'gl_account_id' AS column_name,
   'GL Account' AS description,
   'gl_account_xlate' AS import_column_name,
   1 AS is_required,
   1 AS processing_order,
   'number(22,0)' AS column_type,
   1 AS is_on_table
from dual
union
select
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_asset') AS import_type_id,
   'property_tax_date' AS column_name,
   'Property Tax Date' AS description,
   null AS import_column_name,
   0 AS is_required,
   1 AS processing_order,
   'varchar2(254)' AS column_type,
   1 AS is_on_table
from dual
union
select
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_asset') AS import_type_id,
   'property_tax_amount' AS column_name,
   'Property Tax Amount' AS description,
   null AS import_column_name,
   0 AS is_required,
   1 AS processing_order,
   'number(22,2)' AS column_type,
   1 AS is_on_table
from dual
;

declare 
	max_field_id pp_import_template_fields.field_id%type;
begin 
	select nvl(max(field_id),0) 
	into max_field_id
	from pp_import_template_fields pitf
	where pitf.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_asset');
 
	insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
	select 
	pit.import_template_id, max_field_id + 1, pit.import_type_id, 'used_yn_sw', null
	from pp_import_template pit
	where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_asset');

	insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
	select 
	pit.import_template_id, max_field_id + 2, pit.import_type_id, 'gl_account_id',
	(select import_lookup_id from pp_import_lookup where lookup_table_name = 'gl_account' and lookup_column_name = 'external_account_code'
	   and lookup_constraining_columns is null)
	from pp_import_template pit
	where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_asset');

	insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
	select 
	pit.import_template_id, max_field_id + 3, pit.import_type_id, 'property_tax_date', null
	from pp_import_template pit
	where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_asset');

	insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
	select 
	pit.import_template_id, max_field_id + 4, pit.import_type_id, 'property_tax_amount', null
	from pp_import_template pit
	where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_asset');
end;
/
	
update pp_import_lookup set description = 'Invoice ILR Number',
   long_description = 'Invoice ILR Number'
where column_name = 'ilr_id'
and lookup_column_name = 'ilr_number'
and import_lookup_id in (select import_lookup_id from pp_Import_template_fields)
;

insert into pp_import_lookup(import_lookup_id, description, long_description, column_name, lookup_sql,
   is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql,
   derived_autocreate_yn)
select
   (select max(import_lookup_id)+1 from pp_import_lookup) import_lookup_id,
   'LS ILR.ILR Number' AS description,
   'LS ILR.ILR Number' AS long_Description,
   'ilr_id' AS column_name,
   replace(lookup_sql,'and <importtable>.lease_id = b.lease_id','') AS lookup_sql,
   is_derived,
   lookup_table_name,
   lookup_column_name,
   replace(lookup_constraining_columns,', lease_id',''),
   lookup_values_alternate_sql,
   derived_autocreate_yn
from pp_import_lookup
where column_name = 'ilr_id'
and lookup_column_name = 'ilr_number'
and import_lookup_id in (select import_lookup_id from pp_Import_template_fields)
;

update pp_import_template_fields set import_lookup_id = (
   select import_lookup_id
   from pp_Import_lookup
   where lookup_column_name = 'ilr_number'
   and lookup_constraining_columns not like '%lease_id%')
where column_name = 'ilr_id'
and import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_asset')
;

commit;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2426, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_24_asset_import_new_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;