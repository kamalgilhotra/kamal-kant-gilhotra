
 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044747_prov_20152_ddl.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2 09/09/2015 	Jarrett Skov   Adds signoff_ind column to tax_accrual_m_item
 ||============================================================================
 */ 

-- add Signoff Column
alter table tax_accrual_m_item
add signoff_ind number(22,0) default 0 NOT null;

comment on column TAX_ACCRUAL_M_ITEM.SIGNOFF_IND
is 'Indicator that displays if an individual M Item has been signed off in a given month. 1 = signed off, 2 = unsigned. This has no impact on processing, only to help users track their M Items during close';

 -- Create trigger that un-signs the M item if it's amount_act value ever changes.
create or replace trigger tax_accrual_m_signoff
before update of amount_act
on tax_accrual_m_item
for each row
begin
if :OLD.amount_act <> :NEW.amount_act AND :OLD.signoff_ind = 1
then
	:NEW.signoff_ind := 0;
end if;
end;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2854, 0, 2015, 2, 0, 0, 044747, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044747_prov_20152_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;