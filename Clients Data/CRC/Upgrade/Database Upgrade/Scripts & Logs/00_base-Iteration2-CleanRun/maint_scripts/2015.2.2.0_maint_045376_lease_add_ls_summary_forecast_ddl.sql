/*
||==========================================================================================
|| Application: PowerPlan
|| File Name:   maint_045376_lease_add_ls_summary_forecast_ddl.sql
||==========================================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By           Reason for Change
|| ---------- ---------- -------------------  -----------------------------------------------
|| 2016.1     02/04/2016 Khamchanh Sisouphanh Add Table for Summary Forecast
|| 2015.2.2.0 04/14/2016 David Haupt		 Backporting to 2015.2.2
||==========================================================================================
*/

CREATE TABLE ls_summary_forecast (
  company_id			NUMBER(22,0) 	NOT NULL,
  revision				NUMBER(22,0) 	NOT NULL,
  set_of_books_id		NUMBER(22,0) 	NOT NULL,
  month					DATE			NOT NULL,
  user_id				VARCHAR2(18)	NULL,
  time_stamp			DATE			NULL,  
  type_a_expense		NUMBER(22,0) 	NOT NULL,
  type_b_expense 		NUMBER(22,0) 	NOT NULL,
  residual_amount      	NUMBER(22,2) 	NULL,
  term_penalty         	NUMBER(22,2)	NULL,
  bpo_price            	NUMBER(22,2) 	NULL,
  beg_capital_cost     	NUMBER(22,2) 	NULL,
  end_capital_cost     	NUMBER(22,2) 	NULL,
  beg_obligation       	NUMBER(22,2) 	NULL,
  end_obligation       	NUMBER(22,2) 	NULL,
  beg_lt_obligation    	NUMBER(22,2) 	NULL,
  end_lt_obligation    	NUMBER(22,2) 	NULL,
  interest_accrual     	NUMBER(22,2) 	NULL,
  principal_accrual    	NUMBER(22,2) 	NULL,
  interest_paid        	NUMBER(22,2) 	NULL,
  principal_paid       	NUMBER(22,2) 	NULL,
  executory_accrual1   	NUMBER(22,2) 	NULL,
  executory_accrual2   	NUMBER(22,2) 	NULL,
  executory_accrual3   	NUMBER(22,2) 	NULL,
  executory_accrual4   	NUMBER(22,2) 	NULL,
  executory_accrual5   	NUMBER(22,2) 	NULL,
  executory_accrual6   	NUMBER(22,2) 	NULL,
  executory_accrual7   	NUMBER(22,2) 	NULL,
  executory_accrual8   	NUMBER(22,2) 	NULL,
  executory_accrual9   	NUMBER(22,2) 	NULL,
  executory_accrual10  	NUMBER(22,2) 	NULL,
  executory_paid1      	NUMBER(22,2) 	NULL,
  executory_paid2      	NUMBER(22,2) 	NULL,
  executory_paid3      	NUMBER(22,2) 	NULL,
  executory_paid4      	NUMBER(22,2) 	NULL,
  executory_paid5      	NUMBER(22,2) 	NULL,
  executory_paid6      	NUMBER(22,2) 	NULL,
  executory_paid7      	NUMBER(22,2) 	NULL,
  executory_paid8      	NUMBER(22,2) 	NULL,
  executory_paid9      	NUMBER(22,2) 	NULL,
  executory_paid10     	NUMBER(22,2) 	NULL,
  contingent_accrual1  	NUMBER(22,2) 	NULL,
  contingent_accrual2  	NUMBER(22,2) 	NULL,
  contingent_accrual3  	NUMBER(22,2) 	NULL,
  contingent_accrual4  	NUMBER(22,2) 	NULL,
  contingent_accrual5  	NUMBER(22,2) 	NULL,
  contingent_accrual6  	NUMBER(22,2) 	NULL,
  contingent_accrual7  	NUMBER(22,2) 	NULL,
  contingent_accrual8  	NUMBER(22,2) 	NULL,
  contingent_accrual9  	NUMBER(22,2) 	NULL,
  contingent_accrual10 	NUMBER(22,2) 	NULL,
  contingent_paid1     	NUMBER(22,2) 	NULL,
  contingent_paid2     	NUMBER(22,2) 	NULL,
  contingent_paid3     	NUMBER(22,2) 	NULL,
  contingent_paid4     	NUMBER(22,2) 	NULL,
  contingent_paid5     	NUMBER(22,2) 	NULL,
  contingent_paid6     	NUMBER(22,2) 	NULL,
  contingent_paid7     	NUMBER(22,2) 	NULL,
  contingent_paid8     	NUMBER(22,2) 	NULL,
  contingent_paid9     	NUMBER(22,2) 	NULL,
  contingent_paid10    	NUMBER(22,2) 	NULL,
  current_lease_cost   	NUMBER(22,2) 	NULL);

alter table ls_summary_forecast add (
constraint pk_ls_summary_forecast primary key (company_id,revision, set_of_books_id, month) using index tablespace pwrplant_idx);

COMMENT ON TABLE ls_summary_forecast is '(S) [06] A reporting table to show the financial outputs of lessee forecasting';
COMMENT ON COLUMN ls_summary_forecast.type_a_expense is 'Total expense incurred for FASB Type A leases';
COMMENT ON COLUMN ls_summary_forecast.type_b_expense is 'Total expense incurred for FASB Type B leases';
COMMENT ON COLUMN ls_summary_forecast.company_id is 'The system assigned identifier for a company within PowerPlant';
COMMENT ON COLUMN ls_summary_forecast.revision IS 'The revision.';
COMMENT ON COLUMN ls_summary_forecast.set_of_books_id IS 'The internal set of books id within PowerPlant.';
COMMENT ON COLUMN ls_summary_forecast.month IS 'The month being processed.';
COMMENT ON COLUMN ls_summary_forecast.residual_amount IS 'The guaranteed residual amount.';
COMMENT ON COLUMN ls_summary_forecast.term_penalty IS 'The termination penalty amount.';
COMMENT ON COLUMN ls_summary_forecast.bpo_price IS 'The bargain purchase amount.';
COMMENT ON COLUMN ls_summary_forecast.beg_capital_cost IS 'The capital amount.';
COMMENT ON COLUMN ls_summary_forecast.end_capital_cost IS 'The capital amount.';
COMMENT ON COLUMN ls_summary_forecast.beg_obligation IS 'The beginning obligation for the period.';
COMMENT ON COLUMN ls_summary_forecast.end_obligation IS 'The ending obligation for the period.';
COMMENT ON COLUMN ls_summary_forecast.beg_lt_obligation IS 'The beginning long term obligation for the period.';
COMMENT ON COLUMN ls_summary_forecast.end_lt_obligation IS 'The ending long term obligation for the period.';
COMMENT ON COLUMN ls_summary_forecast.interest_accrual IS 'The amount of interest accrued in this period .';
COMMENT ON COLUMN ls_summary_forecast.principal_accrual IS 'The amount of principal accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.interest_paid IS 'The amount of interest paid in this period .';
COMMENT ON COLUMN ls_summary_forecast.principal_paid IS 'The amount of principal paid in this period. This will cause a reduction in the obligation.';
COMMENT ON COLUMN ls_summary_forecast.executory_accrual1 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.executory_accrual2 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.executory_accrual3 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.executory_accrual4 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.executory_accrual5 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.executory_accrual6 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.executory_accrual7 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.executory_accrual8 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.executory_accrual9 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.executory_accrual10 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.executory_paid1 IS 'The executory paid amount associated with the executory bucket number 1.';
COMMENT ON COLUMN ls_summary_forecast.executory_paid2 IS 'The executory paid amount associated with the executory bucket number 2.';
COMMENT ON COLUMN ls_summary_forecast.executory_paid3 IS 'The executory paid amount associated with the executory bucket number 3.';
COMMENT ON COLUMN ls_summary_forecast.executory_paid4 IS 'The executory paid amount associated with the executory bucket number 4.';
COMMENT ON COLUMN ls_summary_forecast.executory_paid5 IS 'The executory paid amount associated with the executory bucket number 5.';
COMMENT ON COLUMN ls_summary_forecast.executory_paid6 IS 'The executory paid amount associated with the executory bucket number 6.';
COMMENT ON COLUMN ls_summary_forecast.executory_paid7 IS 'The executory paid amount associated with the executory bucket number 7.';
COMMENT ON COLUMN ls_summary_forecast.executory_paid8 IS 'The executory paid amount associated with the executory bucket number 8.';
COMMENT ON COLUMN ls_summary_forecast.executory_paid9 IS 'The executory paid amount associated with the executory bucket number 9.';
COMMENT ON COLUMN ls_summary_forecast.executory_paid10 IS 'The executory paid amount associated with the executory bucket number 10.';
COMMENT ON COLUMN ls_summary_forecast.contingent_accrual1 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.contingent_accrual2 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.contingent_accrual3 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.contingent_accrual4 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.contingent_accrual5 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.contingent_accrual6 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.contingent_accrual7 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.contingent_accrual8 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.contingent_accrual9 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.contingent_accrual10 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_summary_forecast.contingent_paid1 IS 'The contingent paid amount associated with the contingent bucket number 1.';
COMMENT ON COLUMN ls_summary_forecast.contingent_paid2 IS 'The contingent paid amount associated with the contingent bucket number 2.';
COMMENT ON COLUMN ls_summary_forecast.contingent_paid3 IS 'The contingent paid amount associated with the contingent bucket number 3.';
COMMENT ON COLUMN ls_summary_forecast.contingent_paid4 IS 'The contingent paid amount associated with the contingent bucket number 4.';
COMMENT ON COLUMN ls_summary_forecast.contingent_paid5 IS 'The contingent paid amount associated with the contingent bucket number 5.';
COMMENT ON COLUMN ls_summary_forecast.contingent_paid6 IS 'The contingent paid amount associated with the contingent bucket number 6.';
COMMENT ON COLUMN ls_summary_forecast.contingent_paid7 IS 'The contingent paid amount associated with the contingent bucket number 7.';
COMMENT ON COLUMN ls_summary_forecast.contingent_paid8 IS 'The contingent paid amount associated with the contingent bucket number 8.';
COMMENT ON COLUMN ls_summary_forecast.contingent_paid9 IS 'The contingent paid amount associated with the contingent bucket number 9.';
COMMENT ON COLUMN ls_summary_forecast.contingent_paid10 IS 'The contingent paid amount associated with the contingent bucket number 10.';
COMMENT ON COLUMN ls_summary_forecast.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_summary_forecast.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_summary_forecast.current_lease_cost IS 'The total amount funded on the leased asset in the given month';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3040, 0, 2015, 2, 2, 0, 045376, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045376_lease_add_ls_summary_forecast_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;