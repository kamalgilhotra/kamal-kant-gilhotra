/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049515_lessor_03_create_ilr_idc_table_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/13/2017 Anand R        Add new table to associate IDC to ILR
||============================================================================
*/

create table lsr_ilr_initial_direct_cost (
  ilr_initial_direct_cost_id     NUMBER(22,0),
  ilr_id       NUMBER(22,0) not null,
  revision          NUMBER(22,0) not null,
  idc_group_id      NUMBER(22,0) not null,
  user_id           VARCHAR2(18),
  time_stamp        DATE,
  date_incurred     DATE not null,
  amount            NUMBER(22,2) default 0.0,
  description       VARCHAR2(254)
);

alter table lsr_ilr_initial_direct_cost
  add constraint lsr_ilr_idc_pk primary key (ilr_initial_direct_cost_id)
  using index tablespace PWRPLANT_IDX;
  
alter table lsr_ilr_initial_direct_cost
  add constraint lsr_ilr_idc_fk1 foreign key (ilr_id, revision)
  references lsr_ilr_approval(ilr_id, revision) ;
  
alter table lsr_ilr_initial_direct_cost
  add constraint lsr_ilr_idc foreign key (idc_group_id)
  references lsr_initial_direct_cost_group(idc_group_id) ;
  
-- Comments
comment on table lsr_ilr_initial_direct_cost is '(S) [] The table stores the Indirect Cost groups associated to an ILR.';
comment on column lsr_ilr_initial_direct_cost.ilr_initial_direct_cost_id is 'System-assigned identifier of a Lessor ILR IDC.';
comment on column lsr_ilr_initial_direct_cost.ilr_id is 'System-assigned identifier of a Lessor ILR.';
comment on column lsr_ilr_initial_direct_cost.revision is 'System-assigned identifier of a Lessor ILR revision.';
comment on column lsr_ilr_initial_direct_cost.idc_group_id is 'System-assigned identifier of an indirect cost group.';
comment on column lsr_ilr_initial_direct_cost.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column lsr_ilr_initial_direct_cost.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column lsr_ilr_initial_direct_cost.date_incurred is 'The date on which the cost incurred.';
comment on column lsr_ilr_initial_direct_cost.amount is 'Cost incurred';
comment on column lsr_ilr_initial_direct_cost.description is 'Description of the ILR indirect cost group assoiation.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3935, 0, 2017, 1, 0, 0, 49515, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049515_lessor_03_create_ilr_idc_table_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
