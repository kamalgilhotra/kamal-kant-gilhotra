/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035704_sys_pp_client_extensions.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/15/2014 Brandon Beck   Point Release
||============================================================================
*/

create table PP_CLIENT_EXTENSIONS
(
 ID            number(22,0) not null,
 FUNCTION_NAME varchar2(35),
 IS_ACTIVE     number(1,0),
 USER_ID       varchar2(18),
 TIME_STAMP    date
);

alter table PP_CLIENT_EXTENSIONS
   add constraint PP_CLIENT_EXTENSIONS_PK
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_CLIENT_EXTENSIONS
   add constraint PP_CLIENT_EXTENSIONS_FK
       foreign key (IS_ACTIVE)
       references YES_NO (YES_NO_ID);

comment on table PP_CLIENT_EXTENSIONS is 'A table to track what client extensions are implemented and which ones are active.';

comment on column PP_CLIENT_EXTENSIONS.ID is 'A unique id';
comment on column PP_CLIENT_EXTENSIONS.FUNCTION_NAME is 'The name of the extension function that is implemented';
comment on column PP_CLIENT_EXTENSIONS.IS_ACTIVE is 'A field to track whether or not the extension function is active';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (861, 0, 10, 4, 2, 0, 35704, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035704_sys_pp_client_extensions.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

