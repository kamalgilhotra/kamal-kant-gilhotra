/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038995_sys_PEND_BASIS_INSERT_PEND_SOB.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 09/05/2013 Charlie Shilling Maint-29713
||============================================================================
*/

create or replace trigger PEND_BASIS_INSERT_PEND_SOB
   after insert on PEND_BASIS
   for each row
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_BASIS_INSERT_PEND_SOB
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/05/2013 Charlie Shilling
   || 10.4.0.0 02/11/2013 Charlie Shilling Maint-29286
   || 10.4.0.0 03/12/2013 Charlie Shilling Maint-29411
   || 10.4.2.0 09/30/2013 Charlie Shilling Maint-29713
   ||============================================================================
   */

declare
   V_LDG_ASSET_ID      PEND_TRANSACTION.LDG_ASSET_ID%type;
   V_GL_POSTING_MO_YR  PEND_TRANSACTION.GL_POSTING_MO_YR%type;
   V_ACTIVITY_CODE     PEND_TRANSACTION.ACTIVITY_CODE%type;
   V_POSTING_AMOUNT    PEND_TRANSACTION.POSTING_AMOUNT%type;
   V_POSTING_QUANTITY  PEND_TRANSACTION.POSTING_QUANTITY%type;
   V_WORK_ORDER_NUMBER PEND_TRANSACTION.WORK_ORDER_NUMBER%type;
   V_COMPANY_ID        PEND_TRANSACTION.COMPANY_ID%type;
   V_RESERVE           PEND_TRANSACTION.RESERVE%type;
   V_GAIN_LOSS         PEND_TRANSACTION.GAIN_LOSS%type;
   V_SUBLEDGER         PEND_TRANSACTION.SUBLEDGER_INDICATOR%type;
begin
   select LDG_ASSET_ID,
          GL_POSTING_MO_YR,
          ACTIVITY_CODE,
          POSTING_AMOUNT,
          POSTING_QUANTITY,
          WORK_ORDER_NUMBER,
          COMPANY_ID,
          RESERVE, 0, SUBLEDGER_INDICATOR
     into V_LDG_ASSET_ID,
          V_GL_POSTING_MO_YR,
          V_ACTIVITY_CODE,
          V_POSTING_AMOUNT,
          V_POSTING_QUANTITY,
          V_WORK_ORDER_NUMBER,
          V_COMPANY_ID,
          V_RESERVE, V_GAIN_LOSS, V_SUBLEDGER
     from PEND_TRANSACTION
    where PEND_TRANS_ID = :NEW.PEND_TRANS_ID;

   if V_ACTIVITY_CODE = 'UTRT' then
      select UTRF.LDG_ASSET_ID
        into V_LDG_ASSET_ID
        from PEND_TRANSACTION UTRF, PEND_TRANSACTION UTRT
       where UTRT.LDG_ASSET_ID = UTRF.PEND_TRANS_ID
         and UTRT.PEND_TRANS_ID = :NEW.PEND_TRANS_ID;
   end if;

   --call functions to calculate gain_loss_reversal and reserve, and update
   -- pend_transaction
   if v_subledger <> 100 then
      V_RESERVE := GET_PEND_SOB_RESERVE(V_LDG_ASSET_ID,
                                                       1,
                                                       V_GL_POSTING_MO_YR,
                                                       V_ACTIVITY_CODE,
                                                       V_POSTING_AMOUNT,
                                                       V_POSTING_QUANTITY);
      V_GAIN_LOSS := GET_PEND_SOB_GL_REVERSAL(V_LDG_ASSET_ID,
                                                           1,
                                                           V_ACTIVITY_CODE,
                                                           V_WORK_ORDER_NUMBER,
                                                           V_COMPANY_ID);
   end if;

   P_UPDATE_PEND_TRANS_RES_GL_REV(:NEW.PEND_TRANS_ID, V_RESERVE, V_GAIN_LOSS);

   --update adjusted columns using the original value from pend_transaction
   --This is only done in the insert trigger.
   update PEND_TRANSACTION
      set (ADJUSTED_COST_OF_REMOVAL,
            ADJUSTED_SALVAGE_CASH,
            ADJUSTED_SALVAGE_RETURNS,
            ADJUSTED_RESERVE_CREDITS,
            ADJUSTED_RESERVE) =
           (select A.COST_OF_REMOVAL * B.PERCENT_REMOVAL_SALVAGE,
                   A.SALVAGE_CASH * B.PERCENT_REMOVAL_SALVAGE,
                   A.SALVAGE_RETURNS * B.PERCENT_REMOVAL_SALVAGE,
                   A.RESERVE_CREDITS * B.PERCENT_REMOVAL_SALVAGE,
                   A.RESERVE
              from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
             where A.COMPANY_ID = B.COMPANY_ID
               and B.SET_OF_BOOKS_ID = 1
               and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID)
    where PEND_TRANS_ID = :NEW.PEND_TRANS_ID;

   --CBS - gain loss calc was removed from here and will now be done by a table trigger (instead of row trigger)
   --       fired after the pend_basis insert
   insert into PEND_TRANSACTION_SET_OF_BOOKS
      (PEND_TRANS_ID, SET_OF_BOOKS_ID, ACTIVITY_CODE, ADJUSTED_COST_OF_REMOVAL,
       ADJUSTED_SALVAGE_CASH, ADJUSTED_SALVAGE_RETURNS, GAIN_LOSS, RESERVE, ADJUSTED_RESERVE,
       ADJUSTED_RESERVE_CREDITS, REPLACEMENT_AMOUNT, GAIN_LOSS_REVERSAL, POSTING_AMOUNT,
       IMPAIRMENT_EXPENSE_AMOUNT)
      select PEND_TRANS_ID,
             SET_OF_BOOKS_ID,
             ACTIVITY_CODE,
             COST_OF_REMOVAL,
             SALVAGE_CASH,
             SALVAGE_RETURNS,
             0 GAIN_LOSS,
             RESERVE,
             RESERVE,
             RESERVE_CREDITS,
             REPLACEMENT_AMOUNT,
             GAIN_LOSS_REVERSAL,
             POSTING_AMOUNT,
             IMPAIRMENT_EXPENSE_AMOUNT
        from (select PEND_TRANS_ID,
                     SET_OF_BOOKS_ID,
                     ACTIVITY_CODE,
                     COST_OF_REMOVAL,
                     SALVAGE_CASH,
                     SALVAGE_RETURNS,
                     GET_PEND_SOB_RESERVE_HELPER(PEND_TRANS_ID,
                                                 LDG_ASSET_ID,
                                                 SET_OF_BOOKS_ID,
                                                 GL_POSTING_MO_YR,
                                                 ACTIVITY_CODE,
                                                 POSTING_AMOUNT,
                                                 POSTING_QUANTITY) RESERVE,
                     RESERVE_CREDITS,
                     REPLACEMENT_AMOUNT,
                     GET_PEND_SOB_GL_REVERSAL(LDG_ASSET_ID,
                                              SET_OF_BOOKS_ID,
                                              ACTIVITY_CODE,
                                              WORK_ORDER_NUMBER,
                                              COMPANY_ID) GAIN_LOSS_REVERSAL,
                     POSTING_AMOUNT,
                     IMPAIRMENT_EXPENSE_AMOUNT
                from (select PEND_TRANS_ID,
                             SET_OF_BOOKS_ID,
                             WORK_ORDER_NUMBER, --need to carry forward to use in get_pend_sob_gl_reversal function
                             COMPANY_ID, --need to carry forward to use in get_pend_sob_gl_reversal function
                             LDG_ASSET_ID,
                             GL_POSTING_MO_YR,
                             GET_PEND_SOB_ACTV_CODE(LDG_ASSET_ID,
                                                    SET_OF_BOOKS_ID,
                                                    GL_POSTING_MO_YR,
                                                    ACTIVITY_CODE,
                                                    POSTING_AMOUNT,
                                                    GAIN_LOSS_REVERSAL) ACTIVITY_CODE,
                             COST_OF_REMOVAL,
                             SALVAGE_CASH,
                             SALVAGE_RETURNS,
                             RESERVE_CREDITS,
                             REPLACEMENT_AMOUNT,
                             GAIN_LOSS_REVERSAL,
                             POSTING_AMOUNT,
                             POSTING_QUANTITY,
                             IMPAIRMENT_EXPENSE_AMOUNT
                        from (select A.PEND_TRANS_ID,
                                     B.SET_OF_BOOKS_ID,
                                     A.WORK_ORDER_NUMBER, --need to carry forward to use in get_pend_sob_gl_reversal function
                                     A.COMPANY_ID, --need to carry forward to use in get_pend_sob_gl_reversal function
                                     A.LDG_ASSET_ID,
                                     A.GL_POSTING_MO_YR,
                                     A.ACTIVITY_CODE,
                                     NVL(COST_OF_REMOVAL * B.PERCENT_REMOVAL_SALVAGE, 0) COST_OF_REMOVAL,
                                     NVL(SALVAGE_CASH * B.PERCENT_REMOVAL_SALVAGE, 0) SALVAGE_CASH,
                                     NVL(SALVAGE_RETURNS * B.PERCENT_REMOVAL_SALVAGE, 0) SALVAGE_RETURNS,
                                     NVL(RESERVE_CREDITS * B.PERCENT_REMOVAL_SALVAGE, 0) RESERVE_CREDITS,
                                     NVL(REPLACEMENT_AMOUNT, 0) REPLACEMENT_AMOUNT,
                                     NVL(GAIN_LOSS_REVERSAL, 0) GAIN_LOSS_REVERSAL,
                                     (select NVL(:NEW.BASIS_1, 0) * BASIS_1_INDICATOR +
                                             NVL(:NEW.BASIS_2, 0) * BASIS_2_INDICATOR +
                                             NVL(:NEW.BASIS_3, 0) * BASIS_3_INDICATOR +
                                             NVL(:NEW.BASIS_4, 0) * BASIS_4_INDICATOR +
                                             NVL(:NEW.BASIS_5, 0) * BASIS_5_INDICATOR +
                                             NVL(:NEW.BASIS_6, 0) * BASIS_6_INDICATOR +
                                             NVL(:NEW.BASIS_7, 0) * BASIS_7_INDICATOR +
                                             NVL(:NEW.BASIS_8, 0) * BASIS_8_INDICATOR +
                                             NVL(:NEW.BASIS_9, 0) * BASIS_9_INDICATOR +
                                             NVL(:NEW.BASIS_10, 0) * BASIS_10_INDICATOR +
                                             NVL(:NEW.BASIS_11, 0) * BASIS_11_INDICATOR +
                                             NVL(:NEW.BASIS_12, 0) * BASIS_12_INDICATOR +
                                             NVL(:NEW.BASIS_13, 0) * BASIS_13_INDICATOR +
                                             NVL(:NEW.BASIS_14, 0) * BASIS_14_INDICATOR +
                                             NVL(:NEW.BASIS_15, 0) * BASIS_15_INDICATOR +
                                             NVL(:NEW.BASIS_16, 0) * BASIS_16_INDICATOR +
                                             NVL(:NEW.BASIS_17, 0) * BASIS_17_INDICATOR +
                                             NVL(:NEW.BASIS_18, 0) * BASIS_18_INDICATOR +
                                             NVL(:NEW.BASIS_19, 0) * BASIS_19_INDICATOR +
                                             NVL(:NEW.BASIS_20, 0) * BASIS_20_INDICATOR +
                                             NVL(:NEW.BASIS_21, 0) * BASIS_21_INDICATOR +
                                             NVL(:NEW.BASIS_22, 0) * BASIS_22_INDICATOR +
                                             NVL(:NEW.BASIS_23, 0) * BASIS_23_INDICATOR +
                                             NVL(:NEW.BASIS_24, 0) * BASIS_24_INDICATOR +
                                             NVL(:NEW.BASIS_25, 0) * BASIS_25_INDICATOR +
                                             NVL(:NEW.BASIS_26, 0) * BASIS_26_INDICATOR +
                                             NVL(:NEW.BASIS_27, 0) * BASIS_27_INDICATOR +
                                             NVL(:NEW.BASIS_28, 0) * BASIS_28_INDICATOR +
                                             NVL(:NEW.BASIS_29, 0) * BASIS_29_INDICATOR +
                                             NVL(:NEW.BASIS_30, 0) * BASIS_30_INDICATOR +
                                             NVL(:NEW.BASIS_31, 0) * BASIS_31_INDICATOR +
                                             NVL(:NEW.BASIS_32, 0) * BASIS_32_INDICATOR +
                                             NVL(:NEW.BASIS_33, 0) * BASIS_33_INDICATOR +
                                             NVL(:NEW.BASIS_34, 0) * BASIS_34_INDICATOR +
                                             NVL(:NEW.BASIS_35, 0) * BASIS_35_INDICATOR +
                                             NVL(:NEW.BASIS_36, 0) * BASIS_36_INDICATOR +
                                             NVL(:NEW.BASIS_37, 0) * BASIS_37_INDICATOR +
                                             NVL(:NEW.BASIS_38, 0) * BASIS_38_INDICATOR +
                                             NVL(:NEW.BASIS_39, 0) * BASIS_39_INDICATOR +
                                             NVL(:NEW.BASIS_40, 0) * BASIS_40_INDICATOR +
                                             NVL(:NEW.BASIS_41, 0) * BASIS_41_INDICATOR +
                                             NVL(:NEW.BASIS_42, 0) * BASIS_42_INDICATOR +
                                             NVL(:NEW.BASIS_43, 0) * BASIS_43_INDICATOR +
                                             NVL(:NEW.BASIS_44, 0) * BASIS_44_INDICATOR +
                                             NVL(:NEW.BASIS_45, 0) * BASIS_45_INDICATOR +
                                             NVL(:NEW.BASIS_46, 0) * BASIS_46_INDICATOR +
                                             NVL(:NEW.BASIS_47, 0) * BASIS_47_INDICATOR +
                                             NVL(:NEW.BASIS_48, 0) * BASIS_48_INDICATOR +
                                             NVL(:NEW.BASIS_49, 0) * BASIS_49_INDICATOR +
                                             NVL(:NEW.BASIS_50, 0) * BASIS_50_INDICATOR +
                                             NVL(:NEW.BASIS_51, 0) * BASIS_51_INDICATOR +
                                             NVL(:NEW.BASIS_52, 0) * BASIS_52_INDICATOR +
                                             NVL(:NEW.BASIS_53, 0) * BASIS_53_INDICATOR +
                                             NVL(:NEW.BASIS_54, 0) * BASIS_54_INDICATOR +
                                             NVL(:NEW.BASIS_55, 0) * BASIS_55_INDICATOR +
                                             NVL(:NEW.BASIS_56, 0) * BASIS_56_INDICATOR +
                                             NVL(:NEW.BASIS_57, 0) * BASIS_57_INDICATOR +
                                             NVL(:NEW.BASIS_58, 0) * BASIS_58_INDICATOR +
                                             NVL(:NEW.BASIS_59, 0) * BASIS_59_INDICATOR +
                                             NVL(:NEW.BASIS_60, 0) * BASIS_60_INDICATOR +
                                             NVL(:NEW.BASIS_61, 0) * BASIS_61_INDICATOR +
                                             NVL(:NEW.BASIS_62, 0) * BASIS_62_INDICATOR +
                                             NVL(:NEW.BASIS_63, 0) * BASIS_63_INDICATOR +
                                             NVL(:NEW.BASIS_64, 0) * BASIS_64_INDICATOR +
                                             NVL(:NEW.BASIS_65, 0) * BASIS_65_INDICATOR +
                                             NVL(:NEW.BASIS_66, 0) * BASIS_66_INDICATOR +
                                             NVL(:NEW.BASIS_67, 0) * BASIS_67_INDICATOR +
                                             NVL(:NEW.BASIS_68, 0) * BASIS_68_INDICATOR +
                                             NVL(:NEW.BASIS_69, 0) * BASIS_69_INDICATOR +
                                             NVL(:NEW.BASIS_70, 0) * BASIS_70_INDICATOR
                                        from SET_OF_BOOKS
                                       where SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID) POSTING_AMOUNT,
                                     NVL(POSTING_QUANTITY, 0) POSTING_QUANTITY,
                                     DECODE(trim(ACTIVITY_CODE),
                                            'IMPA',
                                            (A.IMPAIRMENT_EXPENSE_AMOUNT *
                                            DECODE(B.SET_OF_BOOKS_ID,
                                                    (select max(SET_OF_BOOKS_ID)
                                                       from CPR_IMPAIRMENT_EVENT
                                                      where IMPAIRMENT_ID =
                                                            (select TO_NUMBER(SUBSTR(WORK_ORDER_NUMBER,
                                                                                     4,
                                                                                     100))
                                                               from PEND_TRANSACTION
                                                              where PEND_TRANS_ID = :NEW.PEND_TRANS_ID)),
                                                    1,
                                                    0)),
                                            0) IMPAIRMENT_EXPENSE_AMOUNT
                                from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
                               where A.COMPANY_ID = B.COMPANY_ID
                                 and B.SET_OF_BOOKS_ID <> 1
                                 and B.INCLUDE_INDICATOR = 1
                                 and LOWER(trim(DECODE(A.FERC_ACTIVITY_CODE,
                                                       2,
                                                       A.DESCRIPTION,
                                                       'not unretire'))) <> 'unretire'
                                 and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
                                 and (PEND_TRANS_ID, SET_OF_BOOKS_ID) in
                                     (select A.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
                                        from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
                                       where A.COMPANY_ID = B.COMPANY_ID
                                         and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
                                      minus
                                      select PEND_TRANS_ID, SET_OF_BOOKS_ID
                                        from PEND_TRANSACTION_SET_OF_BOOKS
                                       where PEND_TRANS_ID = :NEW.PEND_TRANS_ID))));
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1298, 0, 10, 4, 2, 2, 38995, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.2_maint_038995_sys_PEND_BASIS_INSERT_PEND_SOB.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
