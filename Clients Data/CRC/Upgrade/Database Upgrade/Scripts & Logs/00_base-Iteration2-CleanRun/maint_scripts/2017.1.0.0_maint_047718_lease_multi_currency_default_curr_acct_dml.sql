/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047718_lease_multi_currency_default_curr_acct_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 07/13/2017 Anand R          add defaults to currency gain loss accounts and create JE codes
||============================================================================
*/
		   
update ls_ilr_group
set CURR_GAIN_LOSS_ACCT_ID = ST_OBLIG_ACCOUNT_ID,
    CURR_GAIN_LOSS_OFFSET_ACCT_ID = ST_OBLIG_ACCOUNT_ID ;
		   
--STANDARD_JOURNAL_ENTRIES
insert into STANDARD_JOURNAL_ENTRIES
   (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
   select max(A.JE_ID) + 1,
          'LEASECURGL',
          'LEASECURGL',
          'Lease Currency Journals',
          'Lease Currency Journals'
     from STANDARD_JOURNAL_ENTRIES A;

insert into GL_JE_CONTROL
   (PROCESS_ID, JE_ID, DR_TABLE, DR_COLUMN, CR_TABLE, CR_COLUMN)
   select S.GL_JE_CODE, S.JE_ID, 'NONE', 'NONE', 'NONE', 'NONE'
     from STANDARD_JOURNAL_ENTRIES S
    where trim(S.GL_JE_CODE) in ('LEASECURGL');	 

	
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3577, 0, 2017, 1, 0, 0, 47718, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047718_lease_multi_currency_default_curr_acct_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;