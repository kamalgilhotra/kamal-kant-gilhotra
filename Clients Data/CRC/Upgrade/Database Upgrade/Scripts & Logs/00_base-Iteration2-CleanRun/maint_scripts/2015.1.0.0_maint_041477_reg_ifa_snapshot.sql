/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_041477_reg_ifa_snapshot.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Created By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   12/01/2015 Shane Ward	 	Add IFA Snapshot
||============================================================================
*/

/**********************
** Rearrange Menu Item
**********************/
UPDATE ppbase_menu_items
	 SET item_order = 6
 WHERE parent_menu_identifier = 'IFA_TOP'
	 AND item_order = 5;

UPDATE ppbase_menu_items
	 SET item_order = 5
 WHERE parent_menu_identifier = 'IFA_TOP'
	 AND item_order = 4;

UPDATE ppbase_menu_items
	 SET item_order = 4
 WHERE parent_menu_identifier = 'IFA_TOP'
	 AND item_order = 3;

/**********************
** New Menu Item
**********************/
INSERT INTO ppbase_workspace
	(MODULE, workspace_identifier, label, workspace_uo_name, object_type_id)
VALUES
	('REG',
	 'uo_reg_inc_fp_snap_ws',
	 'IFA - FP Snapshot',
	 'uo_reg_inc_fp_snap_ws',
	 1);

INSERT INTO ppbase_menu_items
	(MODULE,
	 menu_identifier,
	 menu_level,
	 item_order,
	 label,
	 minihelp,
	 parent_menu_identifier,
	 workspace_identifier,
	 enable_yn)
VALUES
	('REG',
	 'IFA_SNAP',
	 3,
	 3,
	 'IFA - FP Snapshot',
	 'IFA - FP Snapshot',
	 'IFA_TOP',
	 'uo_reg_inc_fp_snap_ws',
	 1);

/**********************
** New Adj Type
**********************/
INSERT INTO reg_incremental_adjust_type
	(incremental_adj_type_id, description, long_description)
VALUES
	(4, 'Revision Snapshot', 'Funding Project + Revision Snapshot');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2074, 0, 2015, 1, 0, 0, 41477, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041477_reg_ifa_snapshot.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;