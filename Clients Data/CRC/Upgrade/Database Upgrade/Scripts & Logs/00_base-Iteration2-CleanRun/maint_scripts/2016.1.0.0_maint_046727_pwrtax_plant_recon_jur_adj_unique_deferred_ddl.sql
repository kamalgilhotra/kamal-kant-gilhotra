/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_046727_pwrtax_plant_recon_jur_adj_unique_deferred_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 10/27/2016 Jared Watkins  Set the unique constraint to deferred since users cannot modify it
||============================================================================
*/

set constraint tax_plant_recon_form_adj_ui DEFERRED;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3330, 0, 2016, 1, 0, 0, 046727, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046727_pwrtax_plant_recon_jur_adj_unique_deferred_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;