/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043309_cpr_ssp_cpr_allow_concurrent_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 04/07/2015 Sarah Byers		Set ALLOW_CONCURRENT for Server Side Processes
||============================================================================
*/

update pp_processes 
	set ALLOW_CONCURRENT = 1 
 where lower(executable_file) in (
	'ssp_cpr_new_month.exe',
	'ssp_cpr_close_month.exe',
	'ssp_aro_calc.exe',
	'ssp_aro_approve.exe',
	'ssp_depr_calc.exe',
	'ssp_depr_approval.exe',
	'ssp_release_je.exe',
	'ssp_gl_recon.exe',
	'ssp_close_powerplant.exe',
	'ssp_cpr_balance_pp.exe');




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2477, 0, 2015, 1, 0, 0, 043309, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043309_cpr_ssp_cpr_allow_concurrent_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;