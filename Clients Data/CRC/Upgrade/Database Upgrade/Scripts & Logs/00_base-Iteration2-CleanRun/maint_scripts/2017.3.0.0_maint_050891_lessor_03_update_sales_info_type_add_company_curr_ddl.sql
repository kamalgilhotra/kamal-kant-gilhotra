/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050891_lessor_03_update_sales_info_type_add_company_curr_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 04/17/2018 Andrew Hill    Remove compounded rates from schedule result types
||============================================================================
*/

drop type T_LSR_ILR_SALES_DF_PRELIMS;

drop type LSR_ILR_SALES_SCH_INFO_TAB;

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_INFO AUTHID CURRENT_USER AS OBJECT (carrying_cost NUMBER,
                                                                              carrying_cost_company_curr number,
                                                                              fair_market_value NUMBER,
                                                                              fair_market_value_company_curr number,
                                                                              guaranteed_residual NUMBER,
                                                                              estimated_residual NUMBER,
                                                                              days_in_year NUMBER,
                                                                              purchase_option_amount NUMBER,
                                                                              termination_amount NUMBER);
/

create or replace type LSR_ILR_SALES_SCH_INFO_TAB as TABLE OF LSR_ILR_SALES_SCH_INFO;
/

create or replace TYPE t_lsr_ilr_sales_df_prelims AUTHID current_user AS OBJECT ( payment_info lsr_ilr_op_sch_pay_info_tab,
                                                              rates t_lsr_ilr_schedule_all_rates,
                                                              initial_direct_costs lsr_init_direct_cost_info_tab,
                                                              sales_type_info lsr_ilr_sales_sch_info);
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4630, 0, 2017, 3, 0, 0, 50891, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050891_lessor_03_update_sales_info_type_add_company_curr_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;