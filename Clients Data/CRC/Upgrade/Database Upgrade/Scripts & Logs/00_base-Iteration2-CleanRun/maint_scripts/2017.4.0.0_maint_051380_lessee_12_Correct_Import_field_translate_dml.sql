/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051380_lessee_12_Correct_Import_field_translate_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 5/30/2018  Alex Healey    Update import fields to xlate and archive properly
||============================================================================
*/ 

UPDATE pp_import_lookup
SET lookup_sql = '(SELECT DISTINCT (b.purchase_option_type_id) FROM    ls_purchase_option_type b WHERE Upper(Trim(<importfield>)) = upper( trim(b.description)))'
WHERE import_lookup_id = 1117;



UPDATE pp_import_type
SET archive_Table_name = 'ls_import_ilr_purchase_opt_arc'
WHERE import_type_id = 268;


update pp_import_column
set is_required = 0
where import_type_id = 268
and lower(column_name) = 'ilr_purchase_option_id';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6203, 0, 2017, 4, 0, 0, 51380, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051380_lessee_12_Correct_Import_field_translate_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;