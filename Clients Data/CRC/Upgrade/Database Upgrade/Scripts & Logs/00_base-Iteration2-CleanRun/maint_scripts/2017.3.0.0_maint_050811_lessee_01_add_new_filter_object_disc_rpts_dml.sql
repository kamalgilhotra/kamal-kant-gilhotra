/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050811_lessee_01_add_new_filter_object_disc_rpts_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 3/29/2018  Alex Healey    Add new filter object to be used in the lessee disclosure reports
||============================================================================
*/

INSERT INTO PP_REPORTS_FILTER
            (pp_report_filter_id,
             description,
             table_name,
             filter_uo_name)
VALUES      (103,
             'Lessee - Disclosure',
             NULL,
             'uo_ls_selecttabs_disclosure'); 
			 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4244, 0, 2017, 3, 0, 0, 50811, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050811_lessee_01_add_new_filter_object_disc_rpts_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
