/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046351_budget_mypp_subs_appr_count_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2016.1.0 09/06/2016 Daniel Mendel  Fix the count of pending subsitution approvals in mypp
 ||============================================================================
 */

update pp_mypp_approval_control 
set dw = 'dw_budget_sub_workflow_review' 
where label_one = 'Substitution to Review';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3276, 0, 2016, 1, 0, 0, 046351, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046351_budget_mypp_subs_appr_count_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;