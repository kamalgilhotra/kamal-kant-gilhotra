/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_03_lease_group_rates_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

delete from pp_table_groups where table_name = 'ls_lease_rate_group';
delete from powerplant_columns where table_name = 'ls_lease_rate_group';
delete from powerplant_tables where table_name = 'ls_lease_rate_group';


INSERT INTO powerplant_tables
 (TABLE_NAME,PP_TABLE_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,SUBSYSTEM_SCREEN,SELECT_WINDOW,SUBSYSTEM_DISPLAY,SUBSYSTEM,DELIVERY,NOTES,ASSET_MANAGEMENT,BUDGET,CHARGE_REPOSITORY,CWIP_ACCOUNTING,DEPR_STUDIES,LEASE,PROPERTY_TAX,POWERTAX_PROVISION,POWERTAX,SYSTEM,UNITIZATION,WORK_ORDER_MANAGEMENT,CLIENT,WHERE_CLAUSE)
 VALUES('ls_lease_rate_group','s','Lease Rate Groups','Contains adjustments to interest rates based on the rate_group_id defined on the ls_ilr table. Each lease may have multiple rate groups associated with it.','always','','lessee','','','','','','','','','','','','','','','','','');

INSERT INTO powerplant_columns
 (COLUMN_NAME,TABLE_NAME,DROPDOWN_NAME,PP_EDIT_TYPE_ID,HELP_INDEX,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,SHORT_LONG_DESCRIPTION,EDIT_MASK,DROPDOWN_PERCENT,DROPDOWN_RESTRICT,RELATED_TYPE,RELATED_TABLE,READ_ONLY,DEFAULT_VALUE)
 VALUES('effective_date','ls_lease_rate_group','','e',null,'effective date','',3,'','',null,'','','',null,'');
INSERT INTO powerplant_columns
 (COLUMN_NAME,TABLE_NAME,DROPDOWN_NAME,PP_EDIT_TYPE_ID,HELP_INDEX,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,SHORT_LONG_DESCRIPTION,EDIT_MASK,DROPDOWN_PERCENT,DROPDOWN_RESTRICT,RELATED_TYPE,RELATED_TABLE,READ_ONLY,DEFAULT_VALUE)
 VALUES('lease_id','ls_lease_rate_group','','p',null,'lease id','',1,'','',null,'','','',null,'');
INSERT INTO powerplant_columns
 (COLUMN_NAME,TABLE_NAME,DROPDOWN_NAME,PP_EDIT_TYPE_ID,HELP_INDEX,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,SHORT_LONG_DESCRIPTION,EDIT_MASK,DROPDOWN_PERCENT,DROPDOWN_RESTRICT,RELATED_TYPE,RELATED_TABLE,READ_ONLY,DEFAULT_VALUE)
 VALUES('rate','ls_lease_rate_group','','e',null,'rate','',4,'','',null,'','','',null,'');
INSERT INTO powerplant_columns
 (COLUMN_NAME,TABLE_NAME,DROPDOWN_NAME,PP_EDIT_TYPE_ID,HELP_INDEX,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,SHORT_LONG_DESCRIPTION,EDIT_MASK,DROPDOWN_PERCENT,DROPDOWN_RESTRICT,RELATED_TYPE,RELATED_TABLE,READ_ONLY,DEFAULT_VALUE)
 VALUES('rate_group_id','ls_lease_rate_group','','e',null,'rate group id','',2,'','',null,'','','',null,'');
INSERT INTO powerplant_columns
 (COLUMN_NAME,TABLE_NAME,DROPDOWN_NAME,PP_EDIT_TYPE_ID,HELP_INDEX,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,SHORT_LONG_DESCRIPTION,EDIT_MASK,DROPDOWN_PERCENT,DROPDOWN_RESTRICT,RELATED_TYPE,RELATED_TABLE,READ_ONLY,DEFAULT_VALUE)
 VALUES('time_stamp','ls_lease_rate_group','','e',null,'time stamp','',100,'','',null,'','','',null,'');
INSERT INTO powerplant_columns
 (COLUMN_NAME,TABLE_NAME,DROPDOWN_NAME,PP_EDIT_TYPE_ID,HELP_INDEX,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,SHORT_LONG_DESCRIPTION,EDIT_MASK,DROPDOWN_PERCENT,DROPDOWN_RESTRICT,RELATED_TYPE,RELATED_TABLE,READ_ONLY,DEFAULT_VALUE)
 VALUES('user_id','ls_lease_rate_group','','e',null,'user id','',101,'','',null,'','','',null,'');

commit;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2416, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_03_lease_group_rates_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;