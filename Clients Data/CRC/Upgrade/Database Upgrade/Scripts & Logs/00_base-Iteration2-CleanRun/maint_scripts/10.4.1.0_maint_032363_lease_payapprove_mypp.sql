/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032363_lease_payapprove_mypp.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By      Reason for Change
|| -------- ---------- --------------- ---------------------------------------
|| 10.4.1.0 09/12/2013 Matthew Mikulka Point Release
||============================================================================
*/

--Fix Payment Approvals from MyPOWERPLANT
--Update pp_mypp_task_step to validate the correct fields for the first step
update PP_MYPP_TASK_STEP
   set PATH = '', EVENT = 'opensheet'
 where TASK_ID = (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lease Payment Approval');
commit;

--Insert 2nd step (clicked event) into pp_mypp_task_step
insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, TIME_STAMP, USER_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
values
   ((select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lease Payment Approval'), 2, sysdate,
    'PWRPLANT', 'w_ls_center_main', 'dw_approve_payments', 'clicked', '');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (613, 0, 10, 4, 1, 0, 32363, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032363_lease_payapprove_mypp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;