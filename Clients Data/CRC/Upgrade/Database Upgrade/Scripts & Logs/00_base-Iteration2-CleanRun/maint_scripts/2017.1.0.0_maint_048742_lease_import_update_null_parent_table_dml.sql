/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048742_lease_import_update_null_parent_table_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.1.0.0 08/21/2017 Johnny Sisouphanh Update null parent table column
||============================================================================
*/


UPDATE pp_import_column
   SET parent_table = 'ls_funding_status'
 WHERE import_type_id IN
       (SELECT import_type_id
          FROM pp_import_type
         WHERE description = 'Add: ILR''s ')
   AND column_name = 'funding_status_id'
   AND parent_table IS NULL;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3665, 0, 2017, 1, 0, 0, 48742, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048742_lease_import_update_null_parent_table_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;