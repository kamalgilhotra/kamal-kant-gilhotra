/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042604_pcm_fcst_options_details_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/02/2015 Sarah Byers      New table to support N level of details
||============================================================================
*/
-- Populate the new table with the existing values from each of the templates
-- Level 1
insert into wo_est_fcst_options_details (
	users, template_name, template_level, attribute_name, detail_level, subtotal)
select users, template_name, template_level, detail_level1, 1, nvl(subtotal_detail_1,0)
  from wo_est_forecast_options
 where detail_level1 <> 'N/A';

-- Level 2
insert into wo_est_fcst_options_details (
	users, template_name, template_level, attribute_name, detail_level, subtotal)
select users, template_name, template_level, detail_level2, 2, nvl(subtotal_detail_2,0)
  from wo_est_forecast_options
 where detail_level2 <> 'N/A';

-- Level 3
insert into wo_est_fcst_options_details (
	users, template_name, template_level, attribute_name, detail_level, subtotal)
select users, template_name, template_level, detail_level3, 3, nvl(subtotal_detail_3,0)
  from wo_est_forecast_options
 where detail_level3 <> 'N/A';

-- Level 4
insert into wo_est_fcst_options_details (
	users, template_name, template_level, attribute_name, detail_level, subtotal)
select users, template_name, template_level, detail_level4, 4, nvl(subtotal_detail_4,0)
  from wo_est_forecast_options
 where detail_level4 <> 'N/A';

-- Level 5
insert into wo_est_fcst_options_details (
	users, template_name, template_level, attribute_name, detail_level, subtotal)
select users, template_name, template_level, detail_level5, 5, nvl(subtotal_detail_5,0)
  from wo_est_forecast_options
 where detail_level5 <> 'N/A';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2239, 0, 2015, 1, 0, 0, 042604, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042604_pcm_fcst_options_details_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;