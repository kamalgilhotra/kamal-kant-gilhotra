/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029565_cr_bdg_small_fonts.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   03/28/2013 Marc Zawko       Point Release
||============================================================================
*/

alter table CR_BUDGET_TEMPLATES add SMALL_FONTS number(22,0);

update CR_BUDGET_TEMPLATES set SMALL_FONTS = 0 where SMALL_FONTS is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (340, 0, 10, 4, 1, 0, 29565, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029565_cr_bdg_small_fonts.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;