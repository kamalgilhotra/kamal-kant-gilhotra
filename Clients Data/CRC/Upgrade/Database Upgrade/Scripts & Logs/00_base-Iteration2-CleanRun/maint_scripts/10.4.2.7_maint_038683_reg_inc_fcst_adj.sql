/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038683_reg_inc_fcst_adj.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/01/2014 Kyle Peterson
||============================================================================
*/

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = null
 where MODULE = 'REG'
   and MENU_IDENTIFIER = 'IFA_CREATE';

update PPBASE_WORKSPACE
   set WORKSPACE_IDENTIFIER = 'uo_reg_inc_ifa_fp_review', LABEL = 'IFA - FP Review',
       WORKSPACE_UO_NAME = 'uo_reg_inc_review_fp_ifa', MINIHELP = 'IFA - FP Review'
 where WORKSPACE_IDENTIFIER = 'uo_reg_inc_create_ifa';

update PPBASE_MENU_ITEMS
   set LABEL = 'IFA - FP Review', MINIHELP = 'IFA - FP Review', WORKSPACE_IDENTIFIER = 'uo_reg_inc_ifa_fp_review',
       MENU_IDENTIFIER = 'IFA_REVIEW'
 where MODULE = 'REG'
   and MENU_IDENTIFIER = 'IFA_CREATE';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1295, 0, 10, 4, 2, 7, 38683, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038683_reg_inc_fcst_adj.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;