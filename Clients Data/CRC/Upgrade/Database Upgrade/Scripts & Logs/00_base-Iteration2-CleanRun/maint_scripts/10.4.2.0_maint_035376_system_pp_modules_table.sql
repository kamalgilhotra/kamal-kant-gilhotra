/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035376_system_pp_modules_table.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/23/2014 Ron Ferentini  Point Release
||============================================================================
*/

alter table PP_MODULES add VERSION varchar2(35);

comment on column PP_MODULES.VERSION is 'Current code version for a module.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (899, 0, 10, 4, 2, 0, 35376, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035376_system_pp_modules_table.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;