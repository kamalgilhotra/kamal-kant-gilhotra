/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009387_pp_integration.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   04/12/2012 Joseph King    Point Release
||============================================================================
*/

create table PP_INTEGRATION
(
 BATCH      VARCHAR2(100) NOT NULL,
 PRIORITY   NUMBER(22,0) NOT NULL,
 COMPONENT  VARCHAR2(100) NOT NULL,
 ARGUMENT1  VARCHAR2(100),
 ARGUMENT2  VARCHAR2(100),
 ARGUMENT3  VARCHAR2(100),
 ARGUMENT4  VARCHAR2(100),
 ARGUMENT5  VARCHAR2(100),
 ARGUMENT6  VARCHAR2(100),
 ARGUMENT7  VARCHAR2(100),
 ARGUMENT8  VARCHAR2(100),
 ARGUMENT9  VARCHAR2(100),
 ARGUMENT10 VARCHAR2(100),
 USER_ID    VARCHAR2(18),
 TIME_STAMP DATE
);

alter table PP_INTEGRATION
   add constraint PP_INTEGRATION_PK
       primary key (BATCH, PRIORITY)
       using index tablespace PWRPLANT_IDX;

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION, EXECUTABLE_FILE, VERSION, ALLOW_CONCURRENT)
   select (select NVL(max(PROCESS_ID), 0) + 1 from PP_PROCESSES),
          'PP Integration',
          'PP Integration',
          'pp_integration.exe',
          '10.3.4.0',
          0
     from DUAL
    where not exists
    (select 1 from PP_PROCESSES where LOWER(trim(EXECUTABLE_FILE)) = 'pp_integration.exe');

alter table PP_INTEGRATION add STATUS number(22);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (115, 0, 10, 3, 4, 0, 9387, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009387_pp_integration.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
