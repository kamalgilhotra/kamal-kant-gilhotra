/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052460_lessee_02_add_impair_acct_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/08/2018 K Powers       Add impairment accounts
||============================================================================
*/ 

-- add to ILR import

INSERT INTO PP_IMPORT_COLUMN
(IMPORT_TYPE_ID,COLUMN_NAME,DESCRIPTION,IMPORT_COLUMN_NAME,IS_REQUIRED,PROCESSING_ORDER,
COLUMN_TYPE,PARENT_TABLE,IS_ON_TABLE,PARENT_TABLE_PK_COLUMN)
VALUES
(252,'impair_expense_account_id','Impairment Expense Account','impair_expense_acct_xlate',0,2,
'number(22,0)','gl_account',1,'gl_account_id');

INSERT INTO PP_IMPORT_COLUMN
(IMPORT_TYPE_ID,COLUMN_NAME,DESCRIPTION,IMPORT_COLUMN_NAME,IS_REQUIRED,PROCESSING_ORDER,
COLUMN_TYPE,PARENT_TABLE,IS_ON_TABLE,PARENT_TABLE_PK_COLUMN)
VALUES
(252,'impair_reversal_account_id','Impairment Reversal Account','impair_reversal_acct_xlate',0,2,
'number(22,0)','gl_account',1,'gl_account_id');

INSERT INTO PP_IMPORT_COLUMN
(IMPORT_TYPE_ID,COLUMN_NAME,DESCRIPTION,IMPORT_COLUMN_NAME,IS_REQUIRED,PROCESSING_ORDER,
COLUMN_TYPE,PARENT_TABLE,IS_ON_TABLE,PARENT_TABLE_PK_COLUMN)
VALUES
(252,'impair_accum_amort_account_id','Impairment Accum Amort Account','impair_accum_amort_acct_xlate',0,2,
'number(22,0)','gl_account',1,'gl_account_id');

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'impair_expense_account_id', 601);

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'impair_expense_account_id', 602);

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'impair_expense_account_id', 603);
  
INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'impair_reversal_account_id', 601);

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'impair_reversal_account_id', 602);

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'impair_reversal_account_id', 603);
  
  INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'impair_accum_amort_account_id', 601);

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'impair_accum_amort_account_id', 602);

INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
VALUES
  (252, 'impair_accum_amort_account_id', 603);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (10243, 0, 2018, 1, 0, 0, 52460, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052460_lessee_02_add_impair_acct_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;