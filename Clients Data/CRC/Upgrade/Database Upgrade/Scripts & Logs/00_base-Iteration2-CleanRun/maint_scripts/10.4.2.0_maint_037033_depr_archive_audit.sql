SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037033_depr_archive_audit.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 03/11/2014 Kyle Peterson
||============================================================================
*/

declare
   procedure ADD_COLUMN(V_TABLE_NAME varchar2,
                        V_COL_NAME   varchar2,
                        V_DATATYPE   varchar2,
                        V_COMMENT    varchar2) is
   begin
      begin
         execute immediate 'alter table ' || V_TABLE_NAME || ' add ' || V_COL_NAME || ' ' ||
                           V_DATATYPE;
         DBMS_OUTPUT.PUT_LINE('Sucessfully added column ' || V_COL_NAME || ' to table ' ||
                              V_TABLE_NAME || '.');
      exception
         when others then
            if sqlcode = -1430 then
               --1430 is "column being added already exists in table", so we are good here
               DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' already exists on table ' ||
                                    V_TABLE_NAME || '. No action necessasry.');
            else
               RAISE_APPLICATION_ERROR(-20001,
                                       'Could not add column ' || V_COL_NAME || ' to ' ||
                                       V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
            end if;
      end;

      begin
         execute immediate 'comment on column ' || V_TABLE_NAME || '.' || V_COL_NAME || ' is ''' ||
                           V_COMMENT || '''';
         DBMS_OUTPUT.PUT_LINE('  Sucessfully added the comment to column ' || V_COL_NAME ||
                              ' to table ' || V_TABLE_NAME || '.');
      exception
         when others then
            RAISE_APPLICATION_ERROR(-20002,
                                    'Could not add comment to column ' || V_COL_NAME || ' to ' ||
                                    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
      end;
   end ADD_COLUMN;
begin

   ADD_COLUMN('CPR_DEPR_CALC_STG_ARC',
              'TIME_STAMP',
              'date',
              'Standard system-assigned timestamp used for audit purposes.');

   ADD_COLUMN('CPR_DEPR_CALC_STG_ARC',
              'USER_ID',
              'varchar2(18)',
              'Standard system-assigned user id used for audit purposes.');

   ADD_COLUMN('DEPR_CALC_STG_ARC',
              'TIME_STAMP',
              'date',
              'Standard system-assigned timestamp used for audit purposes.');

   ADD_COLUMN('DEPR_CALC_STG_ARC',
              'USER_ID',
              'varchar2(18)',
              'Standard system-assigned user id used for audit purposes.');

   ADD_COLUMN('DEPR_LEDGER_BLENDING_STG_ARC',
              'TIME_STAMP',
              'date',
              'Standard system-assigned timestamp used for audit purposes.');

   ADD_COLUMN('DEPR_LEDGER_BLENDING_STG_ARC',
              'USER_ID',
              'varchar2(18)',
              'Standard system-assigned user id used for audit purposes.');

   ADD_COLUMN('DEPR_CALC_COMBINED_ARC',
              'TIME_STAMP',
              'date',
              'Standard system-assigned timestamp used for audit purposes.');

   ADD_COLUMN('DEPR_CALC_COMBINED_ARC',
              'USER_ID',
              'varchar2(18)',
              'Standard system-assigned user id used for audit purposes.');

   ADD_COLUMN('DEPR_CALC_AMORT_ARC',
              'TIME_STAMP',
              'date',
              'Standard system-assigned timestamp used for audit purposes.');

   ADD_COLUMN('DEPR_CALC_AMORT_ARC',
              'USER_ID',
              'varchar2(18)',
              'Standard system-assigned user id used for audit purposes.');

   ADD_COLUMN('FCST_DEPR_CALC_STG_ARC',
              'TIME_STAMP',
              'date',
              'Standard system-assigned timestamp used for audit purposes.');

   ADD_COLUMN('FCST_DEPR_CALC_STG_ARC',
              'USER_ID',
              'varchar2(18)',
              'Standard system-assigned user id used for audit purposes.');

   ADD_COLUMN('FCST_DEPR_CALC_AMORT_ARC',
              'TIME_STAMP',
              'date',
              'Standard system-assigned timestamp used for audit purposes.');

   ADD_COLUMN('FCST_DEPR_CALC_AMORT_ARC',
              'USER_ID',
              'varchar2(18)',
              'Standard system-assigned user id used for audit purposes.');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1025, 0, 10, 4, 2, 0, 37033, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037033_depr_archive_audit.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;