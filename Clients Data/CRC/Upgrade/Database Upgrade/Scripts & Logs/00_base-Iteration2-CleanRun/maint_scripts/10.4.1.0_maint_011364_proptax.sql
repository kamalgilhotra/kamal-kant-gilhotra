/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011364_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   11/05/2012 Julia Breuer   Point Release
||============================================================================
*/

insert into PWRPLANT.PTC_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Accrual Center - Accrual Search - Default Grid View', sysdate, user, 'The default selection for the way to view accrual charges in the grid - i.e., by Summary, by Summary / Authority, or by Detail.  This specifies the initial selection when the workspace is opened.', 0, 'Summary / Authority', null, 1 );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Accrual Center - Accrual Search - Initial Row Selection Status', sysdate, user, 'Whether row selection in the grid on the Accrual Search workspace is initially enabled or disabled.  This only controls the default value on opening the workspace.  The row selection capabilities can then be changed using the grid''s right click menu.', 0, 'Disabled', null, 1 );

insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Accrual Search - Default Grid View', 'Detail', sysdate, user );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Accrual Search - Default Grid View', 'Summary / Authority', sysdate, user );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Accrual Search - Default Grid View', 'Summary', sysdate, user );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Accrual Search - Initial Row Selection Status', 'Disabled', sysdate, user );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Accrual Search - Initial Row Selection Status', 'Enabled', sysdate, user );

insert into PWRPLANT.PTC_SYSTEM_OPTIONS_CENTERS ( SYSTEM_OPTION_ID, CENTER_NAME, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Accrual Search - Default Grid View', 'accrual', sysdate, user );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_CENTERS ( SYSTEM_OPTION_ID, CENTER_NAME, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Accrual Search - Initial Row Selection Status', 'accrual', sysdate, user );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (244, 0, 10, 4, 1, 0, 11364, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_011364_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
