/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048943_2_lessor_01_create_sales_schedule_types_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/05/2017 A. Hill          Add types for lessor sales type schedule calc
||============================================================================
*/

CREATE OR REPLACE TYPE lsr_ilr_sales_sch_result IS OBJECT(MONTH DATE,
                                                          principal_received NUMBER(22,2),
                                                          interest_income_received 		NUMBER(22,2),
                                                          interest_income_accrued 		NUMBER(22,2),
                                                          principal_accrued NUMBER(22,2),
                                                          begin_receivable 				NUMBER(22,2),
                                                          end_receivable 					NUMBER(22,2),
                                                          begin_lt_receivable NUMBER(22,2),
                                                          end_lt_receivable NUMBER(22,2),
                                                          executory_accrual1           NUMBER(22,2),
                                                          executory_accrual2           NUMBER(22,2),
                                                          executory_accrual3           NUMBER(22,2),
                                                          executory_accrual4           NUMBER(22,2),
                                                          executory_accrual5           NUMBER(22,2),
                                                          executory_accrual6           NUMBER(22,2),
                                                          executory_accrual7           NUMBER(22,2),
                                                          executory_accrual8           NUMBER(22,2),
                                                          executory_accrual9           NUMBER(22,2),
                                                          executory_accrual10          NUMBER(22,2),
                                                          executory_paid1              NUMBER(22,2),
                                                          executory_paid2              NUMBER(22,2),
                                                          executory_paid3              NUMBER(22,2),
                                                          executory_paid4              NUMBER(22,2),
                                                          executory_paid5              NUMBER(22,2),
                                                          executory_paid6              NUMBER(22,2),
                                                          executory_paid7              NUMBER(22,2),
                                                          executory_paid8              NUMBER(22,2),
                                                          executory_paid9              NUMBER(22,2),
                                                          executory_paid10             NUMBER(22,2),
                                                          contingent_accrual1          NUMBER(22,2),
                                                          contingent_accrual2          NUMBER(22,2),
                                                          contingent_accrual3          NUMBER(22,2),
                                                          contingent_accrual4          NUMBER(22,2),
                                                          contingent_accrual5          NUMBER(22,2),
                                                          contingent_accrual6          NUMBER(22,2),
                                                          contingent_accrual7          NUMBER(22,2),
                                                          contingent_accrual8          NUMBER(22,2),
                                                          contingent_accrual9          NUMBER(22,2),
                                                          contingent_accrual10         NUMBER(22,2),
                                                          contingent_paid1             NUMBER(22,2),
                                                          contingent_paid2             NUMBER(22,2),
                                                          contingent_paid3             NUMBER(22,2),
                                                          contingent_paid4             NUMBER(22,2),
                                                          contingent_paid5             NUMBER(22,2),
                                                          contingent_paid6             NUMBER(22,2),
                                                          contingent_paid7             NUMBER(22,2),
                                                          contingent_paid8             NUMBER(22,2),
                                                          contingent_paid9             NUMBER(22,2),
                                                          contingent_paid10            NUMBER(22,2),
                                                          begin_unguaranteed_residual NUMBER(22,2),
                                                          int_on_unguaranteed_residual NUMBER(22,2),
                                                          end_unguaranteed_residual NUMBER(22,2),
                                                          begin_net_investment NUMBER(22,2),
                                                          int_on_net_investment NUMBER(22,2),
                                                          end_net_investment NUMBER(22,2),
                                                          initial_rate NUMBER(22,8),
                                                          rate NUMBER(22,8),
                                                          begin_lease_receivable number(22,2),
                                                          npv_lease_payments NUMBER(22,2),
                                                          npv_guaranteed_residual NUMBER(22,2),
                                                          npv_unguaranteed_residual NUMBER(22,2),
                                                          selling_profit_loss NUMBER(22,2),
                                                          cost_of_goods_sold number(22,2));
/

CREATE OR REPLACE TYPE lsr_ilr_sales_sch_result_tab IS TABLE OF lsr_ilr_sales_sch_result;
/

CREATE OR REPLACE TYPE lsr_bucket_amount AS OBJECT (bucket_name VARCHAR2(254),
                                                    amount NUMBER);
/

CREATE OR REPLACE TYPE lsr_bucket_amount_tab AS TABLE OF lsr_bucket_amount;
/

create or replace TYPE lsr_bucket_result AS OBJECT (MONTH DATE,
                                                    executory_accrued_1 NUMBER(22,2),
                                                    executory_accrued_2 NUMBER(22,2),
                                                    executory_accrued_3 NUMBER(22,2),
                                                    executory_accrued_4 NUMBER(22,2),
                                                    executory_accrued_5 NUMBER(22,2),
                                                    executory_accrued_6 NUMBER(22,2),
                                                    executory_accrued_7 NUMBER(22,2),
                                                    executory_accrued_8 NUMBER(22,2),
                                                    executory_accrued_9 NUMBER(22,2),
                                                    executory_accrued_10 NUMBER(22,2),
                                                    executory_received_1 NUMBER(22,2),
                                                    executory_received_2 NUMBER(22,2),
                                                    executory_received_3 NUMBER(22,2),
                                                    executory_received_4 NUMBER(22,2),
                                                    executory_received_5 NUMBER(22,2),
                                                    executory_received_6 NUMBER(22,2),
                                                    executory_received_7 NUMBER(22,2),
                                                    executory_received_8 NUMBER(22,2),
                                                    executory_received_9 NUMBER(22,2),
                                                    executory_received_10 NUMBER(22,2),
                                                    contingent_accrued_1 NUMBER(22,2),
                                                    contingent_accrued_2 NUMBER(22,2),
                                                    contingent_accrued_3 NUMBER(22,2),
                                                    contingent_accrued_4 NUMBER(22,2),
                                                    contingent_accrued_5 NUMBER(22,2),
                                                    contingent_accrued_6 NUMBER(22,2),
                                                    contingent_accrued_7 NUMBER(22,2),
                                                    contingent_accrued_8 NUMBER(22,2),
                                                    contingent_accrued_9 NUMBER(22,2),
                                                    contingent_accrued_10 NUMBER(22,2),
                                                    contingent_received_1 NUMBER(22,2),
                                                    contingent_received_2 NUMBER(22,2),
                                                    contingent_received_3 NUMBER(22,2),
                                                    contingent_received_4 NUMBER(22,2),
                                                    contingent_received_5 NUMBER(22,2),
                                                    contingent_received_6 NUMBER(22,2),
                                                    contingent_received_7 NUMBER(22,2),
                                                    contingent_received_8 NUMBER(22,2),
                                                    contingent_received_9 NUMBER(22,2),
                                                    contingent_received_10 NUMBER(22,2));
/

CREATE OR REPLACE TYPE lsr_bucket_result_tab AS TABLE OF lsr_bucket_result;
/

CREATE OR REPLACE TYPE  lsr_schedule_payment_def AS OBJECT (payment_month_frequency NUMBER,
                                                            is_prepay number(1,0),
                                                            payment_group NUMBER, --Group months based on date payment takes place
                                                            MONTH DATE, 
                                                            number_of_terms NUMBER,
                                                            iter NUMBER,
                                                            payment_amount NUMBER);
/

create or replace type lsr_schedule_payment_def_tab as table of lsr_schedule_payment_def;
/

CREATE OR REPLACE TYPE lsr_ilr_sales_sch_res_full IS OBJECT(ilr_id NUMBER,
                                                            revision NUMBER,
                                                            set_of_books_id NUMBER,
                                                            MONTH DATE,
                                                            principal_received NUMBER(22,2),
                                                            interest_income_received 		NUMBER(22,2),
                                                            interest_income_accrued 		NUMBER(22,2),
                                                            principal_accrued NUMBER(22,2),
                                                            begin_receivable 				NUMBER(22,2),
                                                            end_receivable 					NUMBER(22,2),
                                                            begin_lt_receivable NUMBER(22,2),
                                                            end_lt_receivable NUMBER(22,2),
                                                            executory_accrual1           NUMBER(22,2),
                                                            executory_accrual2           NUMBER(22,2),
                                                            executory_accrual3           NUMBER(22,2),
                                                            executory_accrual4           NUMBER(22,2),
                                                            executory_accrual5           NUMBER(22,2),
                                                            executory_accrual6           NUMBER(22,2),
                                                            executory_accrual7           NUMBER(22,2),
                                                            executory_accrual8           NUMBER(22,2),
                                                            executory_accrual9           NUMBER(22,2),
                                                            executory_accrual10          NUMBER(22,2),
                                                            executory_paid1              NUMBER(22,2),
                                                            executory_paid2              NUMBER(22,2),
                                                            executory_paid3              NUMBER(22,2),
                                                            executory_paid4              NUMBER(22,2),
                                                            executory_paid5              NUMBER(22,2),
                                                            executory_paid6              NUMBER(22,2),
                                                            executory_paid7              NUMBER(22,2),
                                                            executory_paid8              NUMBER(22,2),
                                                            executory_paid9              NUMBER(22,2),
                                                            executory_paid10             NUMBER(22,2),
                                                            contingent_accrual1          NUMBER(22,2),
                                                            contingent_accrual2          NUMBER(22,2),
                                                            contingent_accrual3          NUMBER(22,2),
                                                            contingent_accrual4          NUMBER(22,2),
                                                            contingent_accrual5          NUMBER(22,2),
                                                            contingent_accrual6          NUMBER(22,2),
                                                            contingent_accrual7          NUMBER(22,2),
                                                            contingent_accrual8          NUMBER(22,2),
                                                            contingent_accrual9          NUMBER(22,2),
                                                            contingent_accrual10         NUMBER(22,2),
                                                            contingent_paid1             NUMBER(22,2),
                                                            contingent_paid2             NUMBER(22,2),
                                                            contingent_paid3             NUMBER(22,2),
                                                            contingent_paid4             NUMBER(22,2),
                                                            contingent_paid5             NUMBER(22,2),
                                                            contingent_paid6             NUMBER(22,2),
                                                            contingent_paid7             NUMBER(22,2),
                                                            contingent_paid8             NUMBER(22,2),
                                                            contingent_paid9             NUMBER(22,2),
                                                            contingent_paid10            NUMBER(22,2),
                                                            current_lease_cost number(22,2),
                                                            begin_unguaranteed_residual NUMBER(22,2),
                                                            int_on_unguaranteed_residual NUMBER(22,2),
                                                            end_unguaranteed_residual NUMBER(22,2),
                                                            begin_net_investment NUMBER(22,2),
                                                            int_on_net_investment NUMBER(22,2),
                                                            end_net_investment NUMBER(22,2),
                                                            initial_rate NUMBER(22,8),
                                                            rate NUMBER(22,8),
                                                            begin_lease_receivable number(22,2),
                                                            npv_lease_payments NUMBER(22,2),
                                                            npv_guaranteed_residual NUMBER(22,2),
                                                            npv_unguaranteed_residual NUMBER(22,2),
                                                            selling_profit_loss NUMBER(22,2),
                                                            cost_of_goods_sold number(22,2)
);
/

CREATE OR REPLACE TYPE lsr_ilr_sales_sch_res_full_tab as table of lsr_ilr_sales_sch_res_full;
/

create or replace TYPE lsr_ilr_sales_sch_info AS OBJECT ( carrying_cost NUMBER,
                                                          discount_rate NUMBER,
                                                          guaranteed_residual NUMBER,
                                                          estimated_residual number,
                                                          days_in_year NUMBER,
                                                          purchase_option_amount NUMBER,
                                                          termination_amount NUMBER);
/

create or replace type lsr_ilr_sales_sch_info_tab as table of lsr_ilr_sales_sch_info;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3763, 0, 2017, 1, 0, 0, 48943, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048943_2_lessor_01_create_sales_schedule_types_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;