/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045131_reimb_void_bill_id_ddl.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/27/2015 Jared Watkins  Add column to support voiding reimb bills
||============================================================================
*/

alter table reimb_bill_support_cr 
add voided_bill_number_id number(22,0);

comment on column REIMB_BILL_SUPPORT_CR.VOIDED_BILL_NUMBER_ID is 'The ID of the voided reimb bill associated with the given row.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2943, 0, 2015, 2, 0, 0, 45131, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045131_reimb_void_bill_id_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;