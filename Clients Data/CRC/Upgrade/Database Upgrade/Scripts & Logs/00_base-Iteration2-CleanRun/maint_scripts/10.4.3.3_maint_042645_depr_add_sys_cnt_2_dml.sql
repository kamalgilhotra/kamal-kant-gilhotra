/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042645_depr_add_sys_cnt_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.3.3   02/18/2015 Daniel Motter    Add system control and some config to new tables
||============================================================================
*/

--Add system control if it doesn't already exist, default it to 'NO'
INSERT INTO pp_system_control_company (control_id, control_name, control_value, description, long_description, company_id)
SELECT (SELECT max(control_id) + 1 FROM pp_system_control_company), 'Detailed Budget Closings', 'NO', 'dw_yes_no;1', 'Drives logic of level of detail for closings from budget to depr forecast.', -1
  FROM DUAL
 WHERE NOT EXISTS (SELECT 1 FROM pp_system_control_company WHERE control_name = 'Detailed Budget Closings');

--Add config to new tables
INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('budget company_id', 'work_order_control.company_id', sysdate, user);

INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('budget bus_segment_id', 'work_order_control.bus_segment_id', sysdate, user);
	
INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('budget major_location_id', 'work_order_control.major_location_id', sysdate, user);

INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('budget job_task_id', 'wo_est_monthly.job_task_id', sysdate, user);

INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('budget vintage', 'null', sysdate, user);

INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('budget book_summary_id', 'estimate_charge_type.book_summary_id', sysdate, user);

INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('actual company_id', 'work_order_control.company_id', sysdate, user);

INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('actual bus_segment_id', 'work_order_control.bus_segment_id', sysdate, user);

INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('actual major_location_id', 'work_order_control.major_location_id', sysdate, user);

INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('actual job_task_id', 'null', sysdate, user);

INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('actual vintage', 'null', sysdate, user);

INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('actual book_summary_id', 'charge_type.book_summary_id', sysdate, user);

INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('budget budget_plant_class_id', 'null', sysdate, user);

INSERT INTO budget_closings_values (value, sql, time_stamp, user_id) VALUES
('actual budget_plant_class_id', 'null', sysdate, user);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2318, 0, 10, 4, 3, 3, 42645, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.3_maint_042645_depr_add_sys_cnt_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;