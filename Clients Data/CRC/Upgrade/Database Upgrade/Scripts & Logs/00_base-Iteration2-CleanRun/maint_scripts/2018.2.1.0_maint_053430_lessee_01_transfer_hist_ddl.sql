/*
||============================================================================
|| Application: PowerPlan
|| File Name:   2018.2.1.0_maint_053430_lessee_01_transfer_hist_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.1.0 04/29/2019 Shane "C" Ward    Add columns on Lease Asset Transfer for tracking to and froms
||============================================================================
*/

ALTER TABLE ls_asset_transfer_history ADD from_ilr_id NUMBER(22,0);
ALTER TABLE ls_asset_transfer_history ADD from_revision NUMBER(22,0);
ALTER TABLE ls_asset_transfer_history ADD to_ilr_id NUMBER(22,0);
ALTER TABLE ls_asset_transfer_history ADD to_revision NUMBER(22,0);

comment on column ls_asset_transfer_history.from_ilr_id is 'ILR Id of the Transferred FROM ILR for the Asset';
comment on column ls_asset_transfer_history.from_revision is 'Revision of the Transferred FROM ILR for the Asset';
comment on column ls_asset_transfer_history.to_ilr_id is 'ILR of the Transferred TO ILR for the Asset';
comment on column ls_asset_transfer_history.to_revision is 'Revision of the Transferred TO ILR for the Asset';

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, 
     SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (17682, 0, 2018, 2, 1, 0, 53430 , 'C:\BitBucketRepos\classic_pb\scripts\00_base\maint_scripts',
     '2018.2.1.0_maint_053430_lessee_01_transfer_hist_ddl.sql', 1, 
     SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), 
     SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;