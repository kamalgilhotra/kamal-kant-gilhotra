/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_038693_cwip_retire.sql
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ---------------------------------------------
|| 10.4.3.0 07/15/2014 Sunjin Cone
||=================================================================================
*/

comment on column WO_ESTIMATE.RETIRE_VINTAGE is 'Used to identify specific vintage retirements for mass property.';
comment on column UNITIZED_WORK_ORDER.RETIRE_VINTAGE is 'Used to identify specific vintage retirements for mass property.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1260, 0, 10, 4, 3, 0, 38693, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038693_cwip_retire.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
