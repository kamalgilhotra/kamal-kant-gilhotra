SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037615_lease.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 04/08/2014 Lee Quinn
||============================================================================
*/

begin
   execute immediate 'drop package LS_TEST_ILR_SCHEDULE';
   DBMS_OUTPUT.PUT_LINE('Package LS_TEST_ILR_SCHEDULE dropped.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Package LS_TEST_ILR_SCHEDULE did not exist.');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1103, 0, 10, 4, 2, 0, 37615, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037615_lease.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
