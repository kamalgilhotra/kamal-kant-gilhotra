/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045259_sys_ssp_budget_pbd_version_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| --------    ---------- -------------- ----------------------------------------
|| 2015.2.0.0  12/07/2015 Sarah Byers    Add the rest of the processes for SSP Budget to pp_custom_pbd_versions
||============================================================================
*/

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'ppprojct_custom.pbd',
		 '2015.2.0.0'
  from pp_processes p
 where lower(trim(executable_file)) like 'ssp_budget.exe%'
	and not exists (
	select 1 from pp_custom_pbd_versions v
	 where v.process_id = p.process_id
		and v.pbd_name = 'ppprojct_custom.pbd');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2991, 0, 2015, 2, 0, 0, 45259, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045259_sys_ssp_budget_pbd_version_dml.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
   SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
