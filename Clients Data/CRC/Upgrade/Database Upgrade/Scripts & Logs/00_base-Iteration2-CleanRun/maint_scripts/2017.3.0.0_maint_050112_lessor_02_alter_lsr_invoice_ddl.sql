/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050112_lessor_02_alter_lsr_invoice_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 03/09/2018 Johnny Sisouphanh Add new column Recognized Profit
||============================================================================
*/

alter table LSR_INVOICE add INVOICE_RECOGNIZED_PROFIT number(22,2);
   
comment on column LSR_INVOICE."INVOICE_RECOGNIZED_PROFIT" is 'The profit recognized in the period.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4175, 0, 2017, 3, 0, 0, 50112, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050112_lessor_02_alter_lsr_invoice_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

