/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_038156_reg_fcst_depr_set_of_books.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.6 05/15/2014 Shane "C" Ward Point Release
||============================================================================
*/

create table REG_FCST_DEPR_SET_OF_BOOKS
(
 FORECAST_VERSION_ID number(22,0) not null,
 SET_OF_BOOKS_ID     number(22,0) not null,
 USER_ID             varchar2(18) null,
 TIME_STAMP          date         null
);

alter table REG_FCST_DEPR_SET_OF_BOOKS
   add constraint PK_REG_FCST_DEPR_SET_OF_BOOKS
       primary key (FORECAST_VERSION_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_FCST_DEPR_SET_OF_BOOKS
   add constraint R_REG_FCST_DEPR_SET_OF_BOOKS1
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_FCST_DEPR_SET_OF_BOOKS
   add constraint R_REG_FCST_DEPR_SET_OF_BOOKS2
       foreign key (SET_OF_BOOKS_ID)
       references SET_OF_BOOKS (SET_OF_BOOKS_ID);

COMMENT ON TABLE REG_FCST_DEPR_SET_OF_BOOKS is '(  )  [  ] THE REG HIST DEPR SET OF BOOKS TABLE RELATES A FORECAST VERSION TO THE SET OF BOOKS THAT SHOULD BE USED WHEN LOADING FROM DEPR LEDGER.';

COMMENT ON COLUMN REG_FCST_DEPR_SET_OF_BOOKS.FORECAST_VERSION_ID is 'SYSTEM ASSIGNED IDENTIFIER OF A FORECAST VERSION.';
COMMENT ON COLUMN REG_FCST_DEPR_SET_OF_BOOKS.SET_OF_BOOKS_ID is 'SYSTEM ASSIGNED IDENTIFIER OF A SET OF BOOKS.';
COMMENT ON COLUMN REG_FCST_DEPR_SET_OF_BOOKS.USER_ID is 'SYSTEM ASSIGNED USER ID USED FOR AUDIT PURPOSES';
COMMENT ON COLUMN REG_FCST_DEPR_SET_OF_BOOKS.TIME_STAMP is 'SYSTEM ASSIGNED TIME STAMP USED FOR AUDIT PURPOSES.';

create or replace view REG_FCST_DEPR_DRILLBACK_VIEW
(
 CO_DESC,
 COMPANY_ID,
 DESCRIPTION,
 DEPR_GROUP_ID,
 FORECAST_DEPR_VERSION_ID,
 FORECAST_DEPR_VERSION,
 REG_ACCT_DESC,
 REG_ACCT_ID,
 GL_POST_MO_YR,
 BEGIN_BALANCE,
 ADDITIONS,
 RETIREMENTS,
 TRANSFERS_IN,
 TRANSFERS_OUT,
 ADJUSTMENTS,
 END_BALANCE,
 BEGIN_RESERVE,
 DEPRECIATION_EXPENSE,
 DEPR_EXP_ADJUST,
 DEPR_EXP_ALLOC_ADJUST,
 SALVAGE_EXPENSE,
 SALVAGE_EXP_ADJUST,
 SALVAGE_EXP_ALLOC_ADJUST,
 SALVAGE_RETURNS,
 SALVAGE_CASH,
 RESERVE_CREDITS,
 RESERVE_ADJUSTMENTS,
 RESERVE_TRAN_IN,
 RESERVE_TRAN_OUT,
 GAIN_LOSS,
 END_RESERVE,
 COR_BEG_RESERVE,
 COR_EXPENSE,
 COR_EXP_ADJUST,
 COR_EXP_ALLOC_ADJUST,
 COST_OF_REMOVAL,
 COR_RES_TRAN_IN,
 COR_RES_TRAN_OUT,
 COR_RES_ADJUST,
 COR_END_RESERVE,
 RWIP_COST_OF_REMOVAL,
 RWIP_SALVAGE_CASH,
 RWIP_SALVAGE_RETURNS,
 RWIP_RESERVE_CREDITS
) as
(
select REG.CO_DESC,
       REG.COMPANY_ID,
       REG.DESCRIPTION,
       REG.FCST_DEPR_GROUP_ID,
       REG.FORECAST_DEPR_VERSION_ID,
       REG.FORECAST_DEPR_VERSION,
       REG.REG_ACCT_DESC,
       REG.REG_ACCT_ID,
       GL_POST_MO_YR,
       BEGIN_BALANCE,
       ADDITIONS,
       RETIREMENTS,
       TRANSFERS_IN,
       TRANSFERS_OUT,
       ADJUSTMENTS,
       END_BALANCE,
       BEGIN_RESERVE,
       DEPRECIATION_EXPENSE,
       DEPR_EXP_ADJUST,
       DEPR_EXP_ALLOC_ADJUST,
       SALVAGE_EXPENSE,
       SALVAGE_EXP_ADJUST,
       SALVAGE_EXP_ALLOC_ADJUST,
       SALVAGE_RETURNS,
       SALVAGE_CASH,
       RESERVE_CREDITS,
       RESERVE_ADJUSTMENTS,
       RESERVE_TRAN_IN,
       RESERVE_TRAN_OUT,
       GAIN_LOSS,
       END_RESERVE,
       COR_BEG_RESERVE,
       COR_EXPENSE,
       COR_EXP_ADJUST,
       COR_EXP_ALLOC_ADJUST,
       COST_OF_REMOVAL,
       COR_RES_TRAN_IN,
       COR_RES_TRAN_OUT,
       COR_RES_ADJUST,
       COR_END_RESERVE,
       RWIP_COST_OF_REMOVAL,
       RWIP_SALVAGE_CASH,
       RWIP_SALVAGE_RETURNS,
       RWIP_RESERVE_CREDITS
  from FCST_DEPR_LEDGER DL,
       (select distinct CO.DESCRIPTION           CO_DESC,
                        CO.COMPANY_ID,
                        DG.DESCRIPTION,
                        DG.FCST_DEPR_GROUP_ID,
                        RAM.DESCRIPTION          REG_ACCT_DESC,
                        RAM.REG_ACCT_ID,
                        FDV.FCST_DEPR_VERSION_ID FORECAST_DEPR_VERSION_ID,
                        FDV.LONG_DESCRIPTION     FORECAST_DEPR_VERSION
          from REG_ACCT_MASTER            RAM,
               REG_FORECAST_LEDGER        RFL,
               REG_FCST_DEPR_FAM_MEM_MAP  RFMM,
               FCST_DEPR_GROUP_VERSION    DG,
               REG_COMPANY                RC,
               COMPANY_SETUP              CO,
               REG_FCST_DEPR_SET_OF_BOOKS RDSOB,
               FCST_DEPR_VERSION          FDV,
               CR_DD_REQUIRED_FILTER      FV,
               CR_DD_REQUIRED_FILTER      FC
         where RAM.REG_SOURCE_ID = 1
           and RAM.REG_FAMILY_ID = RFMM.REG_FAMILY_ID
           and RAM.REG_MEMBER_ID = RFMM.REG_MEMBER_ID
           and RFMM.FORECAST_VERSION_ID = TO_NUMBER(FV.FILTER_STRING)
           and UPPER(FV.COLUMN_NAME) = 'FORECAST_VERSION_ID'
           and DG.FCST_DEPR_GROUP_ID = RFMM.FCST_DEPR_GROUP_ID
           and RFL.REG_COMPANY_ID = RC.REG_COMPANY_ID
           and RC.REG_COMPANY_ID = CO.REG_COMPANY_ID
           and RAM.REG_ACCT_ID = RFL.REG_ACCT_ID
           and RDSOB.FORECAST_VERSION_ID = RFMM.FORECAST_VERSION_ID
           and FDV.FCST_DEPR_VERSION_ID = DG.FCST_DEPR_VERSION_ID
           and RAM.REG_ACCT_ID in (select TO_NUMBER(FILTER_STRING)
                                     from CR_DD_REQUIRED_FILTER
                                    where UPPER(COLUMN_NAME) = 'REG_ACCT_ID')
           and RC.REG_COMPANY_ID = TO_NUMBER(FC.FILTER_STRING)
           and UPPER(FC.COLUMN_NAME) = 'REG_COMPANY_ID') REG,
       CR_DD_REQUIRED_FILTER MN
 where DL.FCST_DEPR_GROUP_ID = REG.FCST_DEPR_GROUP_ID
   and DL.GL_POST_MO_YR = TO_DATE(MN.FILTER_STRING, 'yyyymm')
   and UPPER(MN.COLUMN_NAME) = 'GL_MONTH');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1144, 0, 10, 4, 2, 6, 38156, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_038156_reg_fcst_depr_set_of_books.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;