/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_034964_reg_tax_comp_member_map.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/25/2014 Sarah Byers
||============================================================================
*/

create table REG_TAX_COMP_MEMBER_MAP
(
 HISTORIC_VERSION_ID number(22,0) not null,
 REG_COMPONENT_ID    number(22,0) not null,
 REG_FAMILY_ID       number(22,0) not null,
 REG_MEMBER_ID       number(22,0) not null,
 USER_ID             varchar2(18),
 TIME_STAMP          date
);

alter table REG_TAX_COMP_MEMBER_MAP
   add constraint PK_REG_TAX_COMP_MEMBER_MAP
       primary key (HISTORIC_VERSION_ID, REG_COMPONENT_ID, REG_FAMILY_ID, REG_MEMBER_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_TAX_COMP_MEMBER_MAP
   add constraint R_REG_TAX_COMP_MEMBER_MAP1
       foreign key (HISTORIC_VERSION_ID)
       references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);

alter table REG_TAX_COMP_MEMBER_MAP
   add constraint R_REG_TAX_COMP_MEMBER_MAP2
       foreign key (REG_COMPONENT_ID, REG_FAMILY_ID)
       references REG_TAX_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);

alter table REG_TAX_COMP_MEMBER_MAP
   add constraint R_REG_TAX_COMP_MEMBER_MAP3
       foreign key (REG_MEMBER_ID, REG_FAMILY_ID)
       references REG_TAX_FAMILY_MEMBER (REG_MEMBER_ID, REG_FAMILY_ID);

comment on table REG_TAX_COMP_MEMBER_MAP is '(  )  [  ] The Reg Tax Comp Member Map Table stores the relationship between the system create tax components and user created family members.  This may be 1-to-1 or many-to-1, but not 1-to-many.';
comment on column REG_TAX_COMP_MEMBER_MAP.HISTORIC_VERSION_ID is 'System-Assigned identifier of the historic ledger (version)';
comment on column REG_TAX_COMP_MEMBER_MAP.REG_COMPONENT_ID is 'System-Assigned identifier of a family component for PowerTax.';
comment on column REG_TAX_COMP_MEMBER_MAP.REG_FAMILY_ID is 'System-Assigned identifier of A reg family for PowerTax.';
comment on column REG_TAX_COMP_MEMBER_MAP.REG_MEMBER_ID is 'System-Assigned identifier of a family member for PowerTax.';
comment on column REG_TAX_COMP_MEMBER_MAP.USER_ID is 'Standard System-Assigned user id used for audit purposes.';
comment on column REG_TAX_COMP_MEMBER_MAP.TIME_STAMP is 'Standard System-Assigned timestamp used for audit purposes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (990, 0, 10, 4, 2, 0, 34964, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034964_reg_tax_comp_member_map.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;