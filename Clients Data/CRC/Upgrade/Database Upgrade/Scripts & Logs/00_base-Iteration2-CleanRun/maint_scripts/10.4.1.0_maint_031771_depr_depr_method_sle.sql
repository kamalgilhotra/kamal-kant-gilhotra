/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031771_depr_depr_method_sle.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   09/04/2013 Brandon Beck    Point Release
||============================================================================
*/

create table DEPR_METHOD_SLE
(
 ASSET_ID         number(22,0),
 SET_OF_BOOKS_ID  number(22,0),
 GL_POSTING_MO_YR date,
 DEPR_EXPENSE     number(22,2),
 TIME_STAMP       date,
 USER_ID          varchar2(18)
);

alter table DEPR_METHOD_SLE
   add constraint DEPR_METHOD_SLE_PK
       primary key (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (579, 0, 10, 4, 1, 0, 31771, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031771_depr_depr_method_sle.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
