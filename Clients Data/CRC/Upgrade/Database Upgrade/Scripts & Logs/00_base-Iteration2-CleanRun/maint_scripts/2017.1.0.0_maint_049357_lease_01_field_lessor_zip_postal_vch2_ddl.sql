/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049357_lease_01_field_lessor_zip_postal_vch2_ddl.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/

alter table LS_LESSOR add POSTAL_OLD varchar2(4);
update LS_LESSOR set POSTAL_OLD = POSTAL;
update LS_LESSOR set POSTAL = NULL;
alter table LS_LESSOR modify "POSTAL" varchar2(10);
update LS_LESSOR set POSTAL = POSTAL_OLD;
alter table LS_LESSOR drop column POSTAL_OLD;

alter table LS_LESSOR add "ZIP_OLD" varchar2(5);
update LS_LESSOR set ZIP_OLD = ZIP;
update LS_LESSOR set ZIP = NULL;
alter table LS_LESSOR modify "ZIP" varchar2(10);
update LS_LESSOR set ZIP = ZIP_OLD;
alter table LS_LESSOR drop column ZIP_OLD;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4026, 0, 2017, 1, 0, 0, 49357, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049357_lease_01_field_lessor_zip_postal_vch2_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;