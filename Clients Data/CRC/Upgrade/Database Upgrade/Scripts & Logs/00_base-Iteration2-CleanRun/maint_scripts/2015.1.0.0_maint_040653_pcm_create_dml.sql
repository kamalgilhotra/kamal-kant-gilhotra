 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_040653_pcm_create_dml.sql
|| Description: PCM Create Workspaces
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1   01/20/2014 Alex P.        New Create workspaces
||============================================================================
*/

update ppbase_workspace
set workspace_uo_name = 'uo_pcm_create_wksp'
where module = 'pcm' and workspace_identifier = 'fp_create';

update ppbase_workspace
set workspace_uo_name = 'uo_pcm_create_wksp'
where module = 'pcm' and workspace_identifier = 'wo_create';

insert into ppbase_workspace( module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values( 'pcm', 'fp_create_detail', 'Create (FP) - Details', 'uo_pcm_create_wksp_detail', 'Create Details (FP)', 1 );

insert into ppbase_workspace( module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values( 'pcm', 'wo_create_detail', 'Create (WO) - Details', 'uo_pcm_create_wksp_detail', 'Create Details (WO)', 1 );



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2200, 0, 2015, 1, 0, 0, 040653, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040653_pcm_create_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;