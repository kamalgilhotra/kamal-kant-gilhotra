/*
||==========================================================================================
|| Application: PowerPlan
|| File Name:   maint_043162_fcst_depr_version_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     03/30/2015 Aaron Smith    	 Reciprocal weighting option for avg end of life
||==========================================================================================
*/

ALTER TABLE FCST_DEPR_VERSION
       ADD (COMPOSITE_EOL_WEIGHTING VARCHAR2(10) DEFAULT 'DIRECT');

	   
comment on column FCST_DEPR_VERSION.COMPOSITE_EOL_WEIGHTING is 'Controls if the End of Life value for composite forecast depreciation methods is calculated by direct weighting (Plant x RL) or Reciprocal weighting (Net Plant / RL).';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2469, 0, 2015, 1, 0, 0, 43162, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043162_fcst_depr_version_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;