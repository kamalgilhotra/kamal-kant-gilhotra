/*
||=============================================================================
|| Application: PowerPlan
|| File Name:   maint_051479_lessor_02_class_code_dml.sql
||=============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  06/06/2018 David Conway     Populate Lessor indicators on class_code
||============================================================================
*/

/* make entries to powerplant_columns for new columns, from old columns */
insert into powerplant_columns ( column_name, table_name, time_stamp, user_id, dropdown_name, pp_edit_type_id, description, read_only, column_rank )
select 'lsr_' || a.column_name, a.table_name, sysdate, user, a.dropdown_name, a.pp_edit_type_id, 'Lessor ' || a.description, a.read_only, a.column_rank
from powerplant_columns a
where a.column_name in ('ilr_indicator', 'mla_indicator')
  and a.table_name = 'class_code'
  and not exists( select 1 from powerplant_columns b where b.table_name = a.table_name and b.column_name = 'lsr_' || a.column_name);

/* populate new lsr-specific columns from old columns */
update class_code
set lsr_mla_indicator = mla_indicator
  , lsr_ilr_indicator = ilr_indicator
where mla_indicator is not null
   or ilr_indicator is not null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6723, 0, 2017, 3, 0, 1, 51479, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051479_lessor_02_class_code_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;