/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052420_lessee_01_add_unaccrued_int_reclass_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.3  10/09/2018 Sarah Byers      Add new reclass and remeasurement columns to the asset schedule
||============================================================================
*/

alter table ls_asset_schedule add (
unaccrued_interest_reclass number(22,2),
unaccrued_interest_remeasure number(22,2),
st_obligation_remeasurement number(22,2),
lt_obligation_remeasurement number(22,2),
obligation_reclass number(22,2));

comment on column ls_asset_schedule.unaccrued_interest_reclass is 'The reclassification of unaccrued interest from short to long term.';
comment on column ls_asset_schedule.unaccrued_interest_remeasure is 'The unaccrued interest amount as a result of a remeasurement.';
comment on column ls_asset_schedule.st_obligation_remeasurement is 'The short term obligation remeasurement amount.';
comment on column ls_asset_schedule.lt_obligation_remeasurement is 'The long term obligation remeasurement amount.';
comment on column ls_asset_schedule.obligation_reclass is 'The reclassification of obligation from short to long term.';

alter table ls_ilr_asset_schedule_stg add (
unaccrued_interest_reclass number(22,2),
unaccrued_interest_remeasure number(22,2),
st_obligation_remeasurement number(22,2),
lt_obligation_remeasurement number(22,2),
obligation_reclass number(22,2));

comment on column ls_ilr_asset_schedule_stg.unaccrued_interest_reclass is 'The reclassification of unaccrued interest from short to long term.';
comment on column ls_ilr_asset_schedule_stg.unaccrued_interest_remeasure is 'The unaccrued interest amount as a result of a remeasurement.';
comment on column ls_ilr_asset_schedule_stg.st_obligation_remeasurement is 'The short term obligation remeasurement amount.';
comment on column ls_ilr_asset_schedule_stg.lt_obligation_remeasurement is 'The long term obligation remeasurement amount.';
comment on column ls_ilr_asset_schedule_stg.obligation_reclass is 'The reclassification of obligation from short to long term.';

alter table ls_ilr_asset_schedule_calc_stg add (
unaccrued_interest_reclass number(22,2),
unaccrued_interest_remeasure number(22,2),
st_obligation_remeasurement number(22,2),
lt_obligation_remeasurement number(22,2),
obligation_reclass number(22,2));

comment on column ls_ilr_asset_schedule_calc_stg.unaccrued_interest_reclass is 'The reclassification of unaccrued interest from short to long term.';
comment on column ls_ilr_asset_schedule_calc_stg.unaccrued_interest_remeasure is 'The unaccrued interest amount as a result of a remeasurement.';
comment on column ls_ilr_asset_schedule_calc_stg.st_obligation_remeasurement is 'The short term obligation remeasurement amount.';
comment on column ls_ilr_asset_schedule_calc_stg.lt_obligation_remeasurement is 'The long term obligation remeasurement amount.';
comment on column ls_ilr_asset_schedule_calc_stg.obligation_reclass is 'The reclassification of obligation from short to long term.';

alter table ls_ilr_schedule add (
unaccrued_interest_reclass number(22,2),
unaccrued_interest_remeasure number(22,2),
st_obligation_remeasurement number(22,2),
lt_obligation_remeasurement number(22,2),
obligation_reclass number(22,2));

comment on column ls_ilr_schedule.unaccrued_interest_reclass is 'The reclassification of unaccrued interest from short to long term.';
comment on column ls_ilr_schedule.unaccrued_interest_remeasure is 'The unaccrued interest amount as a result of a remeasurement.';
comment on column ls_ilr_schedule.st_obligation_remeasurement is 'The short term obligation remeasurement amount.';
comment on column ls_ilr_schedule.lt_obligation_remeasurement is 'The long term obligation remeasurement amount.';
comment on column ls_ilr_schedule.obligation_reclass is 'The reclassification of obligation from short to long term.';

-- Rebuild V_LS_ASSET_SCHEDULE_FX_VW to consider new fields in currency gain/loss calc
CREATE OR REPLACE FORCE VIEW V_LS_ASSET_SCHEDULE_FX_VW AS
WITH cur
       AS ( SELECT ls_currency_type_id AS ls_cur_type,
                   currency_id,
                   currency_display_symbol,
                   iso_code,
                   CASE ls_currency_type_id
                     WHEN 1 THEN 1
                     ELSE NULL
                   END                 AS contract_approval_rate
            FROM   CURRENCY
                   cross join LS_LEASE_CURRENCY_TYPE ),
       open_month
       AS ( SELECT  company_id,
                   Min( gl_posting_mo_yr ) open_month
            FROM   LS_PROCESS_CONTROL
            WHERE  open_next IS NULL
            GROUP  BY company_id ),
       calc_rate
       AS ( SELECT  Nvl(a.company_id, b.company_id) company_id,
                   Nvl(a.contract_currency_id, b.contract_currency_id) contract_currency_id,
                   Nvl(a.company_currency_id, b.company_currency_id) company_currency_id,
                   Nvl(a.accounting_month, Add_Months(b.accounting_month,1)) accounting_month,
                   nvl(a.exchange_rate_type_id, b.exchange_rate_type_id) exchange_rate_type_id,
                   Nvl(a.exchange_date, b.exchange_date) exchange_date,
                   a.rate,
                   b.rate prev_rate
            FROM ls_lease_calculated_date_rates a
              FULL OUTER JOIN ls_lease_calculated_date_rates b ON a.company_id = b.company_id
              AND a.contract_currency_id = b.contract_currency_id
              AND a.accounting_month = add_months(b.accounting_month,1)
              and b.exchange_rate_type_id = a.exchange_rate_type_id),
       cr_now
       AS ( SELECT  currency_from,
                   currency_to,
                   rate
            FROM   ( SELECT currency_from,
                            currency_to,
                            rate,
                            Row_number( )
                              over(
                                PARTITION BY currency_from, currency_to
                                ORDER BY exchange_date DESC ) AS rn
                     FROM   CURRENCY_RATE_DEFAULT_DENSE
                     WHERE  Trunc( exchange_date, 'MONTH' ) <= Trunc( SYSDATE, 'MONTH' ) AND
                            exchange_rate_type_id = 1 )
            WHERE  rn = 1 ),
       war
       AS ( SELECT  war.ilr_id,
                   war.revision,
                   war.set_of_books_id,
                   war.gross_weighted_avg_rate,
                   war.net_weighted_avg_rate,
                   lio.in_service_exchange_rate,
                   Trunc(Nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month
            FROM ls_ilr_weighted_avg_rates war
                 JOIN ls_ilr ilr ON war.ilr_id = ilr.ilr_id
                 JOIN ls_ilr_options lio ON lio.ilr_id = war.ilr_id AND lio.revision = war.revision
           union
           select ilr.ilr_id,
                  lio.revision,
                  sob.set_of_books_id,
                  lio.in_service_exchange_rate,
                  lio.in_service_exchange_rate,
                  lio.in_service_exchange_rate,
                  Trunc(Nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month
             from ls_ilr ilr
             join ls_ilr_options lio on lio.ilr_id = ilr.ilr_id
             cross join set_of_books sob
             where (ilr.ilr_id, lio.revision) not in (select ilr_id, revision from ls_ilr_weighted_avg_rates)
           ),
      ilr_dates AS (
        SELECT  lio.ilr_id, lio.revision,
        Trunc(Nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month,
        lio.in_service_exchange_rate
        FROM ls_ilr_options lio
          JOIN ls_ilr ilr ON ilr.ilr_id = lio.ilr_id
      ),
      orig_in_svc AS (
        select ilr_id, revision, effective_month, in_service_exchange_rate
          from ilr_dates
         where(ilr_id, revision) in ( 
          select ilr_id, min(revision) over (partition by ilr_id order by approval_date) revision 
            from ls_ilr_approval
           where approval_status_id <> 1)
       )
  SELECT las.ilr_id                                                                                                                                               ilr_id,
         las.ls_asset_id                                                                                                                                          ls_asset_id,
         las.revision                                                                                                                                             revision,
         las.set_of_books_id                                                                                                                                      set_of_books_id,
         las.month                                                                                                                                                month,
         OPEN_MONTH.company_id                                                                                                                                    company_id,
         OPEN_MONTH.open_month                                                                                                                                    open_month,
         CUR.ls_cur_type                                                                                                                                          ls_cur_type,
         cr.exchange_date                                                                                                                                         exchange_date,
         CALC_RATE.exchange_date                                                                                                                                  prev_exchange_date,
         las.contract_currency_id                                                                                                                                 contract_currency_id,
         CUR.currency_id                                                                                                                                          currency_id,
         cr.rate                                                                                                                                                  rate,
         CALC_RATE.rate                                                                                                                                           calculated_rate,
         CALC_RATE.prev_rate                                                                                                                                      previous_calculated_rate,
         CALC_AVG_RATE.rate                                                                                                                                       calculated_average_rate,
         CALC_AVG_RATE.prev_rate                                                                                                                                  previous_calculated_avg_rate,
         Decode(lower(sc.CONTROL_VALUE), 'yes', CALC_AVG_RATE.rate, CALC_RATE.rate)                                                                               average_rate,
         las.effective_in_svc_rate,
         original_in_svc_rate,
         CUR.iso_code                                                                                                                                             iso_code,
         CUR.currency_display_symbol                                                                                                                              currency_display_symbol,
         las.residual_amount * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     residual_amount,
         las.term_penalty * Nvl( CALC_RATE.rate, cr.rate )                                                                                                        term_penalty,
         las.bpo_price * Nvl( CALC_RATE.rate, cr.rate )                                                                                                      bpo_price,
         las.beg_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, war2.gross_weighted_avg_rate), CR_NOW.rate )                                      beg_capital_cost,
         las.end_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, case when las.month < nvl(las.remeasurement_date, las.month) 
                                                                          then war2.gross_weighted_avg_rate
                                                                          else war.gross_weighted_avg_rate
                                                                     end), CR_NOW.rate )                                            end_capital_cost,
         las.beg_obligation * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                                            beg_obligation,
         las.end_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 end_obligation,
         las.beg_lt_obligation * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                                         beg_lt_obligation,
         las.end_lt_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                              end_lt_obligation,
         las.principal_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate )                                       principal_remeasurement,
         las.beg_liability * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                                                  beg_liability,
         las.end_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  end_liability,
         las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                                               beg_lt_liability,
         las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                               end_lt_liability,
         las.liability_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate )                                       liability_remeasurement,
         las.interest_accrual      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  interest_accrual,
         las.principal_accrual     * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  principal_accrual,
         las.interest_paid         * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  interest_paid,
         las.principal_paid        * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  principal_paid,
         las.executory_accrual1    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual1,
         las.executory_accrual2    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual2,
         las.executory_accrual3    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual3,
         las.executory_accrual4    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual4,
         las.executory_accrual5    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual5,
         las.executory_accrual6    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual6,
         las.executory_accrual7    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual7,
         las.executory_accrual8    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual8,
         las.executory_accrual9    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual9,
         las.executory_accrual10   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual10,
         las.executory_paid1       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid1,
         las.executory_paid2       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid2,
         las.executory_paid3       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid3,
         las.executory_paid4       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid4,
         las.executory_paid5       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid5,
         las.executory_paid6       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid6,
         las.executory_paid7       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid7,
         las.executory_paid8       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid8,
         las.executory_paid9       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid9,
         las.executory_paid10      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid10,
         las.contingent_accrual1   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual1,
         las.contingent_accrual2   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual2,
         las.contingent_accrual3   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual3,
         las.contingent_accrual4   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual4,
         las.contingent_accrual5   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual5,
         las.contingent_accrual6   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual6,
         las.contingent_accrual7   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual7,
         las.contingent_accrual8   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual8,
         las.contingent_accrual9   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual9,
         las.contingent_accrual10  * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual10,
         las.contingent_paid1      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid1,
         las.contingent_paid2      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid2,
         las.contingent_paid3      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid3,
         las.contingent_paid4      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid4,
         las.contingent_paid5      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid5,
         las.contingent_paid6      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid6,
         las.contingent_paid7      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid7,
         las.contingent_paid8      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid8,
         las.contingent_paid9      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid9,
         las.contingent_paid10     * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid10,
         las.current_lease_cost    * Nvl( Nvl( CUR.contract_approval_rate, original_in_svc_rate), CR_NOW.rate ) current_lease_cost,
         las.beg_deferred_rent     * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                         beg_deferred_rent,
         las.deferred_rent         * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  deferred_rent,
         las.end_deferred_rent     * Nvl( CALC_RATE.rate, cr.rate )                                                                         end_deferred_rent,
         las.beg_st_deferred_rent  * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                              beg_st_deferred_rent,
         las.end_st_deferred_rent  * Nvl( CALC_RATE.rate, cr.rate )                                                                              end_st_deferred_rent,
         las.initial_direct_cost   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  initial_direct_cost,
         las.incentive_amount      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  incentive_amount,
         las.depr_expense          * Nvl( Nvl( CUR.contract_approval_rate, case when las.month < nvl(las.remeasurement_date, las.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate )                         depr_expense,
         las.begin_reserve         * Nvl( Nvl( CUR.contract_approval_rate, war2.net_weighted_avg_rate), CR_NOW.rate )                         begin_reserve,
         las.end_reserve           * Nvl( Nvl( CUR.contract_approval_rate, case when las.month < nvl(las.remeasurement_date, las.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate )                         end_reserve,
         las.depr_exp_alloc_adjust * Nvl( Nvl( CUR.contract_approval_rate, case when las.month < nvl(las.remeasurement_date, las.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate )                         depr_exp_alloc_adjust,
         las.asset_description                                                                                                                   asset_description,
         las.leased_asset_number                                                                                                                 leased_asset_number,
         las.fmv * Nvl( Nvl( CUR.contract_approval_rate, original_in_svc_rate), CR_NOW.rate )                                                    fmv,
         las.is_om                                                                                                                               is_om,
         las.approved_revision                                                                                                                   approved_revision,
         las.lease_cap_type_id                                                                                                                   lease_cap_type_id,
         las.ls_asset_status_id                                                                                                                  ls_asset_status_id,
         las.retirement_date                                                                                                                     retirement_date,
         las.beg_prepaid_rent      * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                         beg_prepaid_rent,
         las.prepay_amortization   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  prepay_amortization,
         las.prepaid_rent          * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  prepaid_rent,
         las.end_prepaid_rent      * Nvl( CALC_RATE.rate, cr.rate )                                                                              end_prepaid_rent,
         Nvl(las.beg_net_rou_asset * Nvl( Nvl( CUR.contract_approval_rate, war2.net_weighted_avg_rate), CR_NOW.rate ), 0)                  beg_net_rou_asset,
         Nvl(las.end_net_rou_asset * Nvl( Nvl( CUR.contract_approval_rate, case when las.month < nvl(las.remeasurement_date, las.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate ), 0)                        end_net_rou_asset,
         las.actual_residual_amount * Nvl( CALC_RATE.rate, cr.rate )                                                                             actual_residual_amount,
         las.guaranteed_residual_amount * Nvl( CALC_RATE.rate, cr.rate )                                                                         guaranteed_residual_amount,
         (las.fmv * las.estimated_residual) * Nvl( CALC_RATE.rate, cr.rate )                                                                     estimated_residual,
         nvl(las.rou_asset_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ),0)                    rou_asset_remeasurement,
         nvl(las.PARTIAL_TERM_GAIN_LOSS * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ),0)                     partial_term_gain_loss,
         las.beg_arrears_accrual * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                           beg_arrears_accrual,
         las.arrears_accrual * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))    arrears_accrual,
         las.end_arrears_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                                                end_arrears_accrual,
         nvl(las.EXECUTORY_ADJUST * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )),0)   executory_adjust,
         nvl(las.CONTINGENT_ADJUST * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )),0)  contingent_adjust,
        case when las.is_om = 1 then
          0
        else
          (
            ((las.end_liability * Nvl( CALC_RATE.rate, cr.rate )) - (las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )))
            - ((las.beg_liability * Nvl( CALC_RATE.prev_rate, cr.rate )) - (las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate )))
            - (las.interest_accrual *  Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            + (las.interest_paid * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            + (las.principal_paid * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            - (las.obligation_reclass * Nvl( CALC_RATE.rate, cr.rate ))
            - (las.st_obligation_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ))
            + (las.unaccrued_interest_reclass * Nvl( CALC_RATE.rate, cr.rate ))
            + (las.unaccrued_interest_remeasure * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ))
          )
        end st_currency_gain_loss,
        case when las.is_om = 1 then
          0
        else
          (
              (las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate ))
            - (las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate ))
            + ((las.beg_lt_liability - las.end_lt_liability) * NVL( CALC_RATE.rate, cr.rate))
          )
        end lt_currency_gain_loss,
        case when las.is_om = 1 then
          0
        else
          (
            ((las.end_liability * Nvl( CALC_RATE.rate, cr.rate )) - (las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )))
            - ((las.beg_liability * Nvl( CALC_RATE.prev_rate, cr.rate )) - (las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate )))
            - (las.interest_accrual *  Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            + (las.interest_paid * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            + (las.principal_paid * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            - (las.obligation_reclass * Nvl( CALC_RATE.rate, cr.rate ))
            - (las.st_obligation_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ))
            + (las.unaccrued_interest_reclass * Nvl( CALC_RATE.rate, cr.rate ))
            + (las.unaccrued_interest_remeasure * Nvl( Nvl( CUR.contract_approval_rate, las.effective_in_svc_rate), CR_NOW.rate ))
          ) +
          (
              (las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate ))
            - (las.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate ))
            + ((las.beg_lt_liability - las.end_lt_liability) * NVL( CALC_RATE.rate, cr.rate))
          )
        end gain_loss_fx,
        las.obligation_reclass * Nvl( CALC_RATE.rate, cr.rate ) obligation_reclass,
        las.unaccrued_interest_reclass * Nvl( CALC_RATE.rate, cr.rate ) unaccrued_interest_reclass
  FROM   ( SELECT la.ilr_id,
                  las.ls_asset_id,
                  las.revision,
                  las.set_of_books_id,
                  las.month,
                  la.contract_currency_id,
                  las.residual_amount,
                  las.term_penalty,
                  las.bpo_price,
                  las.beg_capital_cost,
                  las.end_capital_cost,
                  las.beg_obligation,
                  las.end_obligation,
                  las.beg_lt_obligation,
                  las.end_lt_obligation,
                  las.principal_remeasurement,
                  las.beg_liability,
                  las.end_liability,
                  las.beg_lt_liability,
                  las.end_lt_liability,
                  las.liability_remeasurement,
                  las.interest_accrual,
                  las.principal_accrual,
                  las.interest_paid,
                  las.principal_paid,
                  las.executory_accrual1,
                  las.executory_accrual2,
                  las.executory_accrual3,
                  las.executory_accrual4,
                  las.executory_accrual5,
                  las.executory_accrual6,
                  las.executory_accrual7,
                  las.executory_accrual8,
                  las.executory_accrual9,
                  las.executory_accrual10,
                  las.executory_paid1,
                  las.executory_paid2,
                  las.executory_paid3,
                  las.executory_paid4,
                  las.executory_paid5,
                  las.executory_paid6,
                  las.executory_paid7,
                  las.executory_paid8,
                  las.executory_paid9,
                  las.executory_paid10,
                  las.contingent_accrual1,
                  las.contingent_accrual2,
                  las.contingent_accrual3,
                  las.contingent_accrual4,
                  las.contingent_accrual5,
                  las.contingent_accrual6,
                  las.contingent_accrual7,
                  las.contingent_accrual8,
                  las.contingent_accrual9,
                  las.contingent_accrual10,
                  las.contingent_paid1,
                  las.contingent_paid2,
                  las.contingent_paid3,
                  las.contingent_paid4,
                  las.contingent_paid5,
                  las.contingent_paid6,
                  las.contingent_paid7,
                  las.contingent_paid8,
                  las.contingent_paid9,
                  las.contingent_paid10,
                  las.current_lease_cost,
                  las.beg_deferred_rent,
                  las.deferred_rent,
                  las.end_deferred_rent,
                  las.beg_st_deferred_rent,
                  las.end_st_deferred_rent,
                  las.initial_direct_cost,
                  las.incentive_amount,
                  ldf.depr_expense,
                  ldf.begin_reserve,
                  ldf.end_reserve,
                  ldf.depr_exp_alloc_adjust,
                  la.company_id,
                  opt.in_service_exchange_rate,
                  la.description AS asset_description,
                  la.leased_asset_number,
                  la.fmv,
                  las.is_om,
                  la.approved_revision,
                  opt.lease_cap_type_id,
                  la.ls_asset_status_id,
                  la.retirement_date,
                  las.beg_prepaid_rent,
                  las.prepay_amortization,
                  las.prepaid_rent,
                  las.end_prepaid_rent,
                  las.beg_net_rou_asset,
                  las.end_net_rou_asset,
                  la.actual_residual_amount,
                  la.guaranteed_residual_amount,
                  la.estimated_residual,
                  las.rou_asset_remeasurement,
                  las.partial_term_gain_loss,
                  las.beg_arrears_accrual,
                  las.arrears_accrual,
                  las.end_arrears_accrual,
                  las.additional_rou_asset,
                  las.executory_adjust,
                  las.contingent_adjust,
                  ilr_dates.in_service_exchange_rate AS effective_in_svc_rate,
                  orig_in_svc.in_service_exchange_rate AS original_in_svc_rate,
                  opt.remeasurement_date remeasurement_date,
                  nvl(las.obligation_reclass,0) obligation_reclass,
                  nvl(las.st_obligation_remeasurement,0) st_obligation_remeasurement,
                  nvl(las.unaccrued_interest_reclass,0) unaccrued_interest_reclass,
                  nvl(las.unaccrued_interest_remeasure,0) unaccrued_interest_remeasure
           FROM   LS_ASSET_SCHEDULE las
                  JOIN LS_ASSET la ON las.ls_asset_id = la.ls_asset_id
                  JOIN LS_ILR_OPTIONS opt ON las.revision = opt.revision AND la.ilr_id = opt.ilr_id
                  left OUTER JOIN LS_DEPR_FORECAST ldf ON las.ls_asset_id = ldf.ls_asset_id
                                                        AND las.revision = ldf.revision
                                                        AND las.set_of_books_id = ldf.set_of_books_id
                                                        AND las.month = ldf.month
                  JOIN ilr_dates ON la.ilr_id = ilr_dates.ilr_id
                                                      AND las.revision = ilr_dates.revision
                  left outer join orig_in_svc on la.ilr_id = orig_in_svc.ilr_id and las.revision = orig_in_svc.revision
                   ) las
         inner join CURRENCY_SCHEMA cs
                 ON las.company_id = cs.company_id
         inner join cur
                 ON CUR.currency_id = CASE CUR.ls_cur_type
                                        WHEN 1 THEN las.contract_currency_id
                                        WHEN 2 THEN cs.currency_id
                                        ELSE NULL
                                      END
         inner join open_month
                 ON las.company_id = OPEN_MONTH.company_id
         inner join CURRENCY_RATE_DEFAULT_DENSE cr
                 ON CUR.currency_id = cr.currency_to AND
                    las.contract_currency_id = cr.currency_from AND
                    Trunc( cr.exchange_date, 'MONTH' ) = Trunc( las.month, 'MONTH' )
         inner join cr_now
                 ON CUR.currency_id = CR_NOW.currency_to AND
                    las.contract_currency_id = CR_NOW.currency_from
         left outer join calc_rate
                      ON las.contract_currency_id = CALC_RATE.contract_currency_id AND
                         CUR.currency_id = CALC_RATE.company_currency_id AND
                         las.company_id = CALC_RATE.company_id AND
                         las.month = CALC_RATE.accounting_month
                         and calc_rate.exchange_rate_type_id = 1
         left outer join calc_rate calc_avg_rate
                 ON las.contract_currency_id = calc_avg_rate.contract_currency_id AND
                    CUR.currency_id = calc_avg_rate.company_currency_id AND
                    las.company_id = calc_avg_rate.company_id AND
                    las.month = calc_avg_rate.accounting_month 
                    and calc_avg_rate.exchange_rate_type_id = 4
         inner join pp_system_control_companies sc
                 ON open_month.company_id = sc.company_id AND
                    lower(trim(sc.control_name)) = 'lease mc: use average rates'
         join war war2 on las.ilr_id = war2.ilr_id
                      and las.set_of_books_id = war2.set_of_books_id
                      and las.month >= war2.effective_month
                      and war2.revision = case when las.month <= nvl(las.remeasurement_date, las.month)
                                              then case when las.revision - 1 <= 0 then 1 else las.revision - 1 end
                                              else las.revision
                                              end	
         join war   on las.ilr_id = war.ilr_id
                   and las.set_of_books_id = war.set_of_books_id
                   and las.revision = war.revision
         inner join CURRENCY_RATE_DEFAULT_DENSE cr_avg
                 ON CUR.currency_id = cr_avg.currency_to AND
                    las.contract_currency_id = cr_avg.currency_from AND
                    Trunc( cr_avg.exchange_date, 'MONTH' ) = Trunc( las.month, 'MONTH' )
                    AND cr_avg.exchange_rate_type_id = Decode(lower(sc.CONTROL_VALUE), 'yes', 4, 1)
  WHERE  cs.currency_type_id = 1 AND
         cr.exchange_rate_type_id = 1 ;

-- Rebuild V_LS_ILR_SCHEDULE_FX_VW to consider new fields in currency gain/loss calc
CREATE OR REPLACE FORCE VIEW V_LS_ILR_SCHEDULE_FX_VW AS
WITH cur
      AS ( SELECT ls_currency_type_id AS ls_cur_type,
                  currency_id,
                  currency_display_symbol,
                  iso_code,
                  CASE ls_currency_type_id
                    WHEN 1 THEN 1
                    ELSE NULL
                  END                 AS contract_approval_rate
          FROM   CURRENCY
                  cross join LS_LEASE_CURRENCY_TYPE ),
      open_month
      AS ( SELECT company_id,
                  Min( gl_posting_mo_yr ) open_month
          FROM   LS_PROCESS_CONTROL
          WHERE  open_next IS NULL
          GROUP  BY company_id ),
      calc_rate
      AS (  SELECT Nvl(a.company_id, b.company_id) company_id,
                   Nvl(a.contract_currency_id, b.contract_currency_id) contract_currency_id,
                   Nvl(a.company_currency_id, b.company_currency_id) company_currency_id,
                   Nvl(a.accounting_month, Add_Months(b.accounting_month,1)) accounting_month,
                   nvl(a.exchange_rate_type_id, b.exchange_rate_type_id) exchange_rate_type_id,
                   Nvl(a.exchange_date, b.exchange_date) exchange_date,
                   a.rate,
                   b.rate prev_rate
            FROM ls_lease_calculated_date_rates a
              FULL OUTER JOIN ls_lease_calculated_date_rates b ON a.company_id = b.company_id
              AND a.contract_currency_id = b.contract_currency_id
              AND a.accounting_month = add_months(b.accounting_month,1)
              and b.exchange_rate_type_id = a.exchange_rate_type_id),
      cr_now
      AS ( SELECT currency_from,
                  currency_to,
                  rate
          FROM   ( SELECT currency_from,
                          currency_to,
                          rate,
                          Row_number( )
                            over(
                              PARTITION BY currency_from, currency_to
                              ORDER BY exchange_date DESC ) AS rn
                    FROM   CURRENCY_RATE_DEFAULT_DENSE
                    WHERE  Trunc( exchange_date, 'MONTH' ) <= Trunc( SYSDATE, 'MONTH' ) AND
                          exchange_rate_type_id = 1 )
          WHERE  rn = 1 ),
      war
      AS ( SELECT war.ilr_id,
                  war.revision,
                  war.set_of_books_id,
                  war.gross_weighted_avg_rate,
                  war.net_weighted_avg_rate,
                  lio.in_service_exchange_rate,
                  Trunc(Nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month
          FROM ls_ilr_weighted_avg_rates war
                JOIN ls_ilr ilr ON war.ilr_id = ilr.ilr_id
                JOIN ls_ilr_options lio ON lio.ilr_id = war.ilr_id AND lio.revision = war.revision
           union
           select ilr.ilr_id,
                  lio.revision,
                  sob.set_of_books_id,
                  lio.in_service_exchange_rate,
                  lio.in_service_exchange_rate,
                  lio.in_service_exchange_rate,
                  Trunc(Nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month
             from ls_ilr ilr
             join ls_ilr_options lio on lio.ilr_id = ilr.ilr_id
             cross join set_of_books sob
             where (ilr.ilr_id, lio.revision) not in (select ilr_id, revision from ls_ilr_weighted_avg_rates)
          ),
      ilr_dates AS (
        SELECT  lio.ilr_id, lio.revision,
        Trunc(Nvl(lio.remeasurement_date, ilr.est_in_svc_date), 'month') effective_month,
        lio.in_service_exchange_rate
        FROM ls_ilr_options lio
          JOIN ls_ilr ilr ON ilr.ilr_id = lio.ilr_id
      ),
      orig_in_svc AS (
        select ilr_id, revision, effective_month, in_service_exchange_rate
          from ilr_dates
         where(ilr_id, revision) in ( 
          select ilr_id, min(revision) over (partition by ilr_id order by approval_date) revision 
            from ls_ilr_approval
           where approval_status_id <> 1)
       )
SELECT lis.ilr_id                                                                                                      ilr_id,
        lis.ilr_number                                                                                                  ilr_number,
        lease.lease_id                                                                                                  lease_id,
        lease.lease_number                                                                                              lease_number,
        lis.current_revision                                                                                            current_revision,
        lis.revision                                                                                                    revision,
        lis.set_of_books_id                                                                                             set_of_books_id,
        lis.month                                                                                                       month,
        OPEN_MONTH.company_id                                                                                           company_id,
        OPEN_MONTH.open_month                                                                                           open_month,
        CUR.ls_cur_type                                                                                                 AS ls_cur_type,
        cr.exchange_date                                                                                                exchange_date,
        CALC_RATE.exchange_date                                                                                         prev_exchange_date,
        lease.contract_currency_id                                                                                      contract_currency_id,
        CUR.currency_id                                                                                                 display_currency_id,
        cr.rate                                                                                                         rate,
        CALC_RATE.rate                                                                                                  calculated_rate,
        CALC_RATE.prev_rate                                                                                             previous_calculated_rate,
        CALC_AVG_RATE.rate                                                                                              calculated_average_rate,
        CALC_AVG_RATE.prev_rate                                                                                         previous_calculated_avg_rate,
    Decode(lower(sc.CONTROL_VALUE), 'yes', CALC_AVG_RATE.rate, CALC_RATE.rate)                                      average_rate,
        effective_in_svc_rate,
        original_in_svc_rate,
        CUR.iso_code                                                                                                    iso_code,
        CUR.currency_display_symbol                                                                                     currency_display_symbol,
        lis.is_om                                                                                                       is_om,
        lis.purchase_option_amt * Nvl( CALC_RATE.rate, cr.rate )                                                        purchase_option_amt,
        lis.termination_amt * Nvl( CALC_RATE.rate, cr.rate )                                                            termination_amt,
        lis.net_present_value * Nvl( CALC_RATE.rate, cr.rate )                                                          net_present_value,
        lis.capital_cost * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )          capital_cost,
        lis.beg_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, war2.gross_weighted_avg_rate), CR_NOW.rate )  beg_capital_cost,
        lis.end_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, case when lis.month < nvl(lis.remeasurement_date, lis.month) 
                                                                          then war2.gross_weighted_avg_rate
                                                                          else war.gross_weighted_avg_rate
                                                                     end), CR_NOW.rate )        end_capital_cost,
        lis.beg_obligation * Nvl( CALC_RATE.prev_rate, cr.rate )                                                        beg_obligation,
        lis.end_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                             end_obligation,
        lis.beg_lt_obligation * Nvl( CALC_RATE.prev_rate, cr.rate )                                                     beg_lt_obligation,
        lis.end_lt_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                          end_lt_obligation,
        lis.principal_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, effective_in_svc_rate), CR_NOW.rate )       principal_remeasurement,
        lis.beg_liability * Nvl( CALC_RATE.prev_rate, cr.rate )                                                              beg_liability,
        lis.end_liability * Nvl( CALC_RATE.rate, cr.rate )                                                              end_liability,
        lis.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate )                                                           beg_lt_liability,
        lis.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )                                                           end_lt_liability,
        lis.liability_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, effective_in_svc_rate), CR_NOW.rate )       liability_remeasurement,
        lis.interest_accrual      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  interest_accrual,
        lis.principal_accrual     * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  principal_accrual,
        lis.interest_paid         * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  interest_paid,
        lis.principal_paid        * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  principal_paid,
        lis.executory_accrual1    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual1,
        lis.executory_accrual2    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual2,
        lis.executory_accrual3    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual3,
        lis.executory_accrual4    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual4,
        lis.executory_accrual5    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual5,
        lis.executory_accrual6    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual6,
        lis.executory_accrual7    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual7,
        lis.executory_accrual8    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual8,
        lis.executory_accrual9    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual9,
        lis.executory_accrual10   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_accrual10,
        lis.executory_paid1       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid1,
        lis.executory_paid2       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid2,
        lis.executory_paid3       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid3,
        lis.executory_paid4       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid4,
        lis.executory_paid5       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid5,
        lis.executory_paid6       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid6,
        lis.executory_paid7       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid7,
        lis.executory_paid8       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid8,
        lis.executory_paid9       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid9,
        lis.executory_paid10      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  executory_paid10,
        lis.contingent_accrual1   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual1,
        lis.contingent_accrual2   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual2,
        lis.contingent_accrual3   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual3,
        lis.contingent_accrual4   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual4,
        lis.contingent_accrual5   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual5,
        lis.contingent_accrual6   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual6,
        lis.contingent_accrual7   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual7,
        lis.contingent_accrual8   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual8,
        lis.contingent_accrual9   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual9,
        lis.contingent_accrual10  * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_accrual10,
        lis.contingent_paid1      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid1,
        lis.contingent_paid2      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid2,
        lis.contingent_paid3      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid3,
        lis.contingent_paid4      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid4,
        lis.contingent_paid5      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid5,
        lis.contingent_paid6      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid6,
        lis.contingent_paid7      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid7,
        lis.contingent_paid8      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid8,
        lis.contingent_paid9      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid9,
        lis.contingent_paid10     * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  contingent_paid10,
        lis.current_lease_cost    * Nvl( Nvl( CUR.contract_approval_rate, original_in_svc_rate), CR_NOW.rate )  current_lease_cost,
        lis.beg_deferred_rent     * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                         beg_deferred_rent,
        lis.deferred_rent         * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  deferred_rent,
        lis.end_deferred_rent     * Nvl( CALC_RATE.rate, cr.rate )                                                                         end_deferred_rent,
        lis.beg_st_deferred_rent  * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                              beg_st_deferred_rent,
        lis.end_st_deferred_rent  * Nvl( CALC_RATE.rate, cr.rate )                                                                              end_st_deferred_rent,
        lis.initial_direct_cost   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  initial_direct_cost,
        lis.incentive_amount      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  incentive_amount,
        lis.depr_expense          * Nvl( Nvl( CUR.contract_approval_rate, case when lis.month < nvl(lis.remeasurement_date, lis.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate )                         depr_expense,
        lis.begin_reserve         * Nvl( Nvl( CUR.contract_approval_rate, war2.net_weighted_avg_rate), CR_NOW.rate )                         begin_reserve,
        lis.end_reserve           * Nvl( Nvl( CUR.contract_approval_rate, case when lis.month < nvl(lis.remeasurement_date, lis.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate )                         end_reserve,
        lis.depr_exp_alloc_adjust * Nvl( Nvl( CUR.contract_approval_rate, case when lis.month < nvl(lis.remeasurement_date, lis.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate )                         depr_exp_alloc_adjust,
        lis.beg_prepaid_rent      * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                         beg_prepaid_rent,
        lis.prepay_amortization   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  prepay_amortization,
        lis.prepaid_rent          * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  prepaid_rent,
        lis.end_prepaid_rent      * Nvl( CALC_RATE.rate, cr.rate )                                                                              end_prepaid_rent,
        lis.beg_net_rou_asset     * Nvl( Nvl( CUR.contract_approval_rate, war2.net_weighted_avg_rate), CR_NOW.rate )                      beg_net_rou_asset,
        lis.end_net_rou_asset     * Nvl( Nvl( CUR.contract_approval_rate, case when lis.month < nvl(lis.remeasurement_date, lis.month) 
                                                                               then war2.net_weighted_avg_rate
                                                                               else war.net_weighted_avg_rate
                                                                          end ), CR_NOW.rate )                            end_net_rou_asset,
        lis.rou_asset_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, effective_in_svc_rate), CR_NOW.rate )                           rou_asset_remeasurement,
        lis.partial_term_gain_loss  * Nvl( Nvl( CUR.contract_approval_rate, effective_in_svc_rate), CR_NOW.rate )                           partial_term_gain_loss,
        lis.beg_arrears_accrual   * Nvl( CALC_RATE.prev_rate, cr.rate )                                                                         beg_arrears_accrual,
        lis.arrears_accrual       * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))  arrears_accrual,
        lis.end_arrears_accrual   * Nvl( CALC_RATE.rate, cr.rate )                                                                              end_arrears_accrual,
        lis.additional_rou_asset  * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )                         additional_rou_asset,
        lis.executory_adjust      * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))   executory_adjust,
        lis.contingent_adjust     * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate ))   contingent_adjust,
        case when lis.is_om = 1 then
          0
        else
          (
            ((lis.end_liability     * Nvl( CALC_RATE.rate, cr.rate )) - (lis.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )))
            - ((lis.beg_liability   * Nvl( CALC_RATE.prev_rate, cr.rate )) - (lis.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate )))
            - (lis.interest_accrual * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            + (lis.interest_paid    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            + (lis.principal_paid   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            - (lis.obligation_reclass * Nvl( CALC_RATE.rate, cr.rate ))
            - (lis.st_obligation_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, lis.effective_in_svc_rate), CR_NOW.rate ))
            + (lis.unaccrued_interest_reclass * Nvl( CALC_RATE.rate, cr.rate ))
            + (lis.unaccrued_interest_remeasure * Nvl( Nvl( CUR.contract_approval_rate, lis.effective_in_svc_rate), CR_NOW.rate ))
          )
        end st_currency_gain_loss,
        case when lis.is_om = 1 then
          0
        else
          (
              (lis.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate ))
            - (lis.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate ))
            + ((lis.beg_lt_liability - lis.end_lt_liability) * NVL( CALC_RATE.rate, cr.rate))
          )
        end lt_currency_gain_loss,
        case when lis.is_om = 1 then
          0
        else
          (
            ((lis.end_liability     * Nvl( CALC_RATE.rate, cr.rate )) - (lis.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )))
            - ((lis.beg_liability   * Nvl( CALC_RATE.prev_rate, cr.rate )) - (lis.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate )))
            - (lis.interest_accrual * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            + (lis.interest_paid    * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            + (lis.principal_paid   * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), Nvl( CALC_RATE.rate, cr.rate )))
            - (lis.obligation_reclass * Nvl( CALC_RATE.rate, cr.rate ))
            - (lis.st_obligation_remeasurement * Nvl( Nvl( CUR.contract_approval_rate, lis.effective_in_svc_rate), CR_NOW.rate ))
            + (lis.unaccrued_interest_reclass * Nvl( CALC_RATE.rate, cr.rate ))
            + (lis.unaccrued_interest_remeasure * Nvl( Nvl( CUR.contract_approval_rate, lis.effective_in_svc_rate), CR_NOW.rate ))
          ) +
          (
              (lis.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate ))
            - (lis.beg_lt_liability * Nvl( CALC_RATE.prev_rate, cr.rate ))
            + ((lis.beg_lt_liability - lis.end_lt_liability) * NVL( CALC_RATE.rate, cr.rate))
          )
        end gain_loss_fx,
        lis.obligation_reclass * Nvl( CALC_RATE.rate, cr.rate ) obligation_reclass,
        lis.unaccrued_interest_reclass * Nvl( CALC_RATE.rate, cr.rate ) unaccrued_interest_reclass
FROM   ( SELECT a.ilr_id,
                a.ilr_number,
                a.current_revision,
                a.revision,
                a.set_of_books_id,
                a.month,
                a.beg_capital_cost,
                a.end_capital_cost,
                a.beg_obligation,
                a.end_obligation,
                a.beg_lt_obligation,
                a.end_lt_obligation,
                a.principal_remeasurement,
                a.beg_liability,
                a.end_liability,
                a.beg_lt_liability,
                a.end_lt_liability,
                a.liability_remeasurement,
                a.interest_accrual,
                a.principal_accrual,
                a.interest_paid,
                a.principal_paid,
                a.executory_accrual1,
                a.executory_accrual2,
                a.executory_accrual3,
                a.executory_accrual4,
                a.executory_accrual5,
                a.executory_accrual6,
                a.executory_accrual7,
                a.executory_accrual8,
                a.executory_accrual9,
                a.executory_accrual10,
                a.executory_paid1,
                a.executory_paid2,
                a.executory_paid3,
                a.executory_paid4,
                a.executory_paid5,
                a.executory_paid6,
                a.executory_paid7,
                a.executory_paid8,
                a.executory_paid9,
                a.executory_paid10,
                a.contingent_accrual1,
                a.contingent_accrual2,
                a.contingent_accrual3,
                a.contingent_accrual4,
                a.contingent_accrual5,
                a.contingent_accrual6,
                a.contingent_accrual7,
                a.contingent_accrual8,
                a.contingent_accrual9,
                a.contingent_accrual10,
                a.contingent_paid1,
                a.contingent_paid2,
                a.contingent_paid3,
                a.contingent_paid4,
                a.contingent_paid5,
                a.contingent_paid6,
                a.contingent_paid7,
                a.contingent_paid8,
                a.contingent_paid9,
                a.contingent_paid10,
                a.current_lease_cost,
                a.beg_deferred_rent,
                a.deferred_rent,
                a.end_deferred_rent,
                a.beg_st_deferred_rent,
                a.end_st_deferred_rent,
                a.initial_direct_cost,
                a.incentive_amount,
                depr.depr_expense,
                depr.begin_reserve,
                depr.end_reserve,
                depr.depr_exp_alloc_adjust,
                a.lease_id,
                a.company_id,
                a.in_service_exchange_rate,
                a.purchase_option_amt,
                a.termination_amt,
                a.net_present_value,
                a.capital_cost,
                a.is_om,
                a.beg_prepaid_rent,
                a.prepay_amortization,
                a.prepaid_rent,
                a.end_prepaid_rent,
                a.beg_net_rou_asset,
                a.end_net_rou_asset ,
                a.rou_asset_remeasurement,
                a.partial_term_gain_loss,
                a.beg_arrears_accrual,
                a.arrears_accrual,
                a.end_arrears_accrual,
                a.additional_rou_asset,
                a.executory_adjust,
                a.contingent_adjust,
                a.effective_in_svc_rate,
                a.original_in_svc_rate,
                a.remeasurement_date,
                a.obligation_reclass,
                a.st_obligation_remeasurement,
                a.unaccrued_interest_reclass,
                a.unaccrued_interest_remeasure
          FROM   ( SELECT lis.ilr_id,
                          ilr.ilr_number,
                          ilr.current_revision,
                          lis.revision,
                          lis.set_of_books_id,
                          lis.month,
                          lis.beg_capital_cost,
                          lis.end_capital_cost,
                          lis.beg_obligation,
                          lis.end_obligation,
                          lis.beg_lt_obligation,
                          lis.end_lt_obligation,
                          lis.principal_remeasurement,
                          lis.beg_liability,
                          lis.end_liability,
                          lis.beg_lt_liability,
                          lis.end_lt_liability,
                          lis.liability_remeasurement,
                          lis.interest_accrual,
                          lis.principal_accrual,
                          lis.interest_paid,
                          lis.principal_paid,
                          lis.executory_accrual1,
                          lis.executory_accrual2,
                          lis.executory_accrual3,
                          lis.executory_accrual4,
                          lis.executory_accrual5,
                          lis.executory_accrual6,
                          lis.executory_accrual7,
                          lis.executory_accrual8,
                          lis.executory_accrual9,
                          lis.executory_accrual10,
                          lis.executory_paid1,
                          lis.executory_paid2,
                          lis.executory_paid3,
                          lis.executory_paid4,
                          lis.executory_paid5,
                          lis.executory_paid6,
                          lis.executory_paid7,
                          lis.executory_paid8,
                          lis.executory_paid9,
                          lis.executory_paid10,
                          lis.contingent_accrual1,
                          lis.contingent_accrual2,
                          lis.contingent_accrual3,
                          lis.contingent_accrual4,
                          lis.contingent_accrual5,
                          lis.contingent_accrual6,
                          lis.contingent_accrual7,
                          lis.contingent_accrual8,
                          lis.contingent_accrual9,
                          lis.contingent_accrual10,
                          lis.contingent_paid1,
                          lis.contingent_paid2,
                          lis.contingent_paid3,
                          lis.contingent_paid4,
                          lis.contingent_paid5,
                          lis.contingent_paid6,
                          lis.contingent_paid7,
                          lis.contingent_paid8,
                          lis.contingent_paid9,
                          lis.contingent_paid10,
                          lis.current_lease_cost,
                          lis.beg_deferred_rent,
                          lis.deferred_rent,
                          lis.end_deferred_rent,
                          lis.beg_st_deferred_rent,
                          lis.end_st_deferred_rent,
                          lis.initial_direct_cost,
                          lis.incentive_amount,
                          ilr.lease_id,
                          ilr.company_id,
                          opt.in_service_exchange_rate,
                          opt.purchase_option_amt,
                          opt.termination_amt,
                          liasob.net_present_value,
                          liasob.capital_cost,
                          lis.is_om,
                          lis.beg_prepaid_rent,
                          lis.prepay_amortization,
                          lis.prepaid_rent,
                          lis.end_prepaid_rent,
                          lis.beg_net_rou_asset,
                          lis.end_net_rou_asset ,
                          lis.rou_asset_remeasurement,
                          lis.partial_term_gain_loss,
                          lis.beg_arrears_accrual,
                          lis.arrears_accrual,
                          lis.end_arrears_accrual,
                          lis.additional_rou_asset,
                          lis.executory_adjust,
                          lis.contingent_adjust,
                          ilr_dates.in_service_exchange_rate AS effective_in_svc_rate,
                          orig_in_svc.in_service_exchange_rate AS original_in_svc_rate,
                          opt.remeasurement_date remeasurement_date,
                          nvl(lis.obligation_reclass,0) obligation_reclass,
                          nvl(lis.st_obligation_remeasurement,0) st_obligation_remeasurement,
                          nvl(lis.unaccrued_interest_reclass,0) unaccrued_interest_reclass,
                          nvl(lis.unaccrued_interest_remeasure,0) unaccrued_interest_remeasure
                  FROM   LS_ILR_SCHEDULE lis
                          JOIN LS_ILR_OPTIONS opt ON lis.ilr_id = opt.ilr_id AND lis.revision = opt.revision
                          JOIN LS_ILR_AMOUNTS_SET_OF_BOOKS liasob ON lis.ilr_id = liasob.ilr_id AND lis.revision = liasob.revision AND lis.set_of_books_id = liasob.set_of_books_id
                          JOIN LS_ILR ilr ON lis.ilr_id = ilr.ilr_id
                          JOIN LS_LEASE lease ON ilr.lease_id = lease.lease_id
                          JOIN ilr_dates ON lis.ilr_id = ilr_dates.ilr_id AND lis.revision = ilr_dates.revision
                      left outer join orig_in_svc on lis.ilr_id = orig_in_svc.ilr_id and lis.revision = orig_in_svc.revision
                          ) a
                left outer join ( SELECT la.ilr_id,
                                          ldf.revision,
                                          ldf.set_of_books_id,
                                          ldf.month,
                                          SUM( ldf.depr_expense )          AS depr_expense,
                                          SUM( ldf.begin_reserve )         AS begin_reserve,
                                          SUM( ldf.end_reserve )           AS end_reserve,
                                          SUM( ldf.depr_exp_alloc_adjust ) AS depr_exp_alloc_adjust
                                  FROM   LS_ASSET la,
                                          LS_DEPR_FORECAST ldf
                                  WHERE  la.ls_asset_id = ldf.ls_asset_id
                                  GROUP  BY la.ilr_id,ldf.revision,ldf.set_of_books_id,ldf.month ) depr
                              ON a.ilr_id = depr.ilr_id AND
                                a.revision = depr.revision AND
                                a.set_of_books_id = depr.set_of_books_id AND
                                a.month = depr.month ) lis
        inner join LS_LEASE lease
                ON lis.lease_id = lease.lease_id
        inner join CURRENCY_SCHEMA cs
                ON lis.company_id = cs.company_id
        inner join cur
                ON CUR.currency_id = CASE CUR.ls_cur_type
                                      WHEN 1 THEN lease.contract_currency_id
                                      WHEN 2 THEN cs.currency_id
                                      ELSE NULL
                                    END
        inner join open_month
                ON lis.company_id = OPEN_MONTH.company_id
        inner join CURRENCY_RATE_DEFAULT_DENSE cr
                ON CUR.currency_id = cr.currency_to AND
                  lease.contract_currency_id = cr.currency_from AND
                  Trunc( cr.exchange_date, 'MONTH' ) = Trunc( lis.month, 'MONTH' )
        inner join cr_now
                ON CUR.currency_id = CR_NOW.currency_to AND
                  lease.contract_currency_id = CR_NOW.currency_from
        left outer join calc_rate
                    ON lease.contract_currency_id = CALC_RATE.contract_currency_id AND
                        CUR.currency_id = CALC_RATE.company_currency_id AND
                        lis.company_id = CALC_RATE.company_id AND
                        lis.month = CALC_RATE.accounting_month
                        and calc_rate.exchange_rate_type_id = 1
        left outer join calc_rate calc_avg_rate
                    ON lease.contract_currency_id = calc_avg_rate.contract_currency_id AND
                        CUR.currency_id = calc_avg_rate.company_currency_id AND
                        lis.company_id = calc_avg_rate.company_id AND
                        lis.month = calc_avg_rate.accounting_month
                        and calc_avg_rate.exchange_rate_type_id = 4
        inner join pp_system_control_companies sc
                  ON open_month.company_id = sc.company_id AND
                     lower(trim(sc.control_name)) = 'lease mc: use average rates'
        join war war2 on lis.ilr_id = war2.ilr_id
                     and lis.set_of_books_id = war2.set_of_books_id
                     and lis.month >= war2.effective_month
                     and war2.revision = case when lis.month <= nvl(lis.remeasurement_date, lis.month)
                                              then case when lis.revision - 1 <= 0 then 1 else lis.revision - 1 end
                                              else lis.revision
                                              end	
        join war    on lis.ilr_id = war.ilr_id
                   and lis.set_of_books_id = war.set_of_books_id
                   and lis.revision = war.revision
        inner join CURRENCY_RATE_DEFAULT_DENSE cr_avg
                ON CUR.currency_id = cr_avg.currency_to AND
                  lease.contract_currency_id = cr_avg.currency_from AND
                  Trunc( cr_avg.exchange_date, 'MONTH' ) = Trunc( lis.month, 'MONTH' )
                  AND cr_avg.exchange_rate_type_id = Decode(lower(sc.CONTROL_VALUE), 'yes', 4, 1)
WHERE  cs.currency_type_id = 1 AND
        cr.exchange_rate_type_id = 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(10222, 0, 2017, 4, 0, 3, 52420, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052420_lessee_01_add_unaccrued_int_reclass_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;