/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036411_reg_REG_CASE_REV_REQ_COMPARE_V.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 03/11/2014 Ryan Oliveria
||============================================================================
*/

create or replace view REG_CASE_REV_REQ_COMPARE_V
as
select C.REG_CHART_GROUP_ID CHART_GROUP_ID,
       C.TEST_YEAR TEST_YEAR,
       C.REG_CASE_ID REG_CASE_ID,
       L.DESCRIPTION SERIES_LABEL,
       RC.CASE_NAME CASE_NAME,
       NVL(RCR.RATE_BASE, 0) RATE_BASE,
       0 ALLOWED_RATE,
       NVL(RCR.NOI_ALLOWED, 0) RETURN_ON_RATE_BASE,
       NVL(RCR.TAXES, 0) TAXES,
       NVL(RCR.DEPRECIATION, 0) DEPR_AND_AMORT,
       NVL(RCR.OPERATING_EXPENSE, 0) O_AND_M,
       0 FUEL_PURCH_POWER,
       0 OTHER_EXPENSES,
       NVL(RCR.OVERALL_RETURN, 0) OVERALL_RETURN,
       RCR.CASE_YEAR_END
  from REG_CHART_GROUP          G,
       REG_CHART_GROUP_LABELS   L,
       REG_CHART_GROUP_CASES    C,
       REG_CASE                 RC,
       REG_CASE_REV_REQ_RESULTS RCR
 where G.REG_CHART_GROUP_ID = L.REG_CHART_GROUP_ID
   and L.REG_CHART_GROUP_ID = C.REG_CHART_GROUP_ID
   and L.SERIES_LABEL_ID = C.SERIES_LABEL_ID
   and C.REG_CASE_ID = RC.REG_CASE_ID
   and RCR.REG_CASE_ID = RC.REG_CASE_ID
   and RCR.CASE_YEAR_END = C.TEST_YEAR;

 --**************************
 -- Log the run of the script
 --**************************

 insert into PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
     SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
 values
    (1031, 0, 10, 4, 2, 0, 36411, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036411_reg_REG_CASE_REV_REQ_COMPARE_V.sql', 1,
     SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
     SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;