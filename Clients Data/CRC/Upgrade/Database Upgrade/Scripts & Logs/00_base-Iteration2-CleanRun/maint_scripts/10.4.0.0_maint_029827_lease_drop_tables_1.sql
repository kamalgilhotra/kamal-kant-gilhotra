SET SERVEROUTPUT ON
SET LINESIZE 120

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029827_lease_drop_tables_1.sql
|| Description: Drop all Lease tables
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/12/2013 Lee Quinn      Created
||============================================================================
*/

declare
   TABLE_DOES_NOT_EXIST exception;
   pragma exception_init(TABLE_DOES_NOT_EXIST, -00942);

   LOOPING_CHAIN_OF_SYNONYMS exception;
   pragma exception_init(LOOPING_CHAIN_OF_SYNONYMS, -01775);

   type CV_TYP is ref cursor;
   CV CV_TYP;

   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   L_COUNT             number := 1;
   LB_LEASE_BEING_USED boolean := true;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   begin
      open CV for 'select count(*) from LS_ASSET where ROWNUM < 10';
      fetch CV
         into L_COUNT;
      close CV;

   if L_COUNT = 0 then
      LB_LEASE_BEING_USED := false;
   end if;

   exception
      when TABLE_DOES_NOT_EXIST then
         LB_LEASE_BEING_USED := false;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'LS_ASSET table Doesn''t exist so Lease Module can be refreshed.');
         LB_LEASE_BEING_USED := false;
      when LOOPING_CHAIN_OF_SYNONYMS then
         LB_LEASE_BEING_USED := false;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Looping Chain of Synonyms - LS_ASSET table Doesn''t exist so the Lease Module can be refreshed.');
         LB_LEASE_BEING_USED := false;
   end;

   if LB_LEASE_BEING_USED then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Lease is being used so no Tables will be dropped.');
   else

      for CONSTRAINT_SQL in (select 'alter table ' || TABLE_NAME || ' drop constraint ' ||
                                    CONSTRAINT_NAME SQL_LINE
                               from ALL_CONSTRAINTS
                              where OWNER = 'PWRPLANT'
                                and CONSTRAINT_TYPE = 'R'
                                and TABLE_NAME like 'LS\_%' escape '\'
                              order by TABLE_NAME)

      loop
         DBMS_OUTPUT.PUT_LINE(PPCSQL || CONSTRAINT_SQL.SQL_LINE || ';');
         begin
            execute immediate CONSTRAINT_SQL.SQL_LINE;
         end;
      end loop;

      for TABLE_SQL in (select 'drop table ' || TABLE_NAME SQL_LINE
                          from ALL_TABLES
                         where OWNER = 'PWRPLANT'
                           and TABLE_NAME like 'LS\_%' escape '\'
                         order by TABLE_NAME)

      loop
         DBMS_OUTPUT.PUT_LINE(PPCSQL || TABLE_SQL.SQL_LINE || ';');
         begin
            execute immediate TABLE_SQL.SQL_LINE;
         end;
      end loop;
   end if;
end;
/

/*
|| END Drop Lease Tables
*/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (357, 0, 10, 4, 0, 0, 29827, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029827_lease_drop_tables_1.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
