/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032218_lease_missing_column.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/05/2013 Kyle Peterson  Point Release
||============================================================================
*/

alter table LS_IMPORT_LEASE_ARCHIVE
   add (AUTO_APPROVE       number(22,0),
        AUTO_APPROVE_XLATE varchar2(35));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (587, 0, 10, 4, 1, 0, 32218, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032218_lease_missing_column.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
