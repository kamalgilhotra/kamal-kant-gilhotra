/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_053351_lessee_03_tax_query_changes_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 04/08/2019 Sarah Byers      Make updates based on ls_monthly_tax table changes
||============================================================================
*/

DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Payment Detail by Company';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'SELECT To_char(lpl.gl_posting_mo_yr,''yyyymm'')     AS monthnum,
       co.company_id,
       co.description                              AS company_description,
       lct.description                             lease_cap_type,
       Nvl(SUM(Decode(lpl.payment_type_id,1,lpl.amount,
                                           0)),0) principal_payment,
       Nvl(SUM(Decode(lpl.payment_type_id,2,lpl.amount,
                                           0)),0) interest_payment,
       Nvl(SUM(Decode(lpl.payment_type_id,2,lpl.adjustment_amount,
                                           0)),0) interest_adjustment,
       Nvl(SUM(Decode(lpl.payment_type_id,3,lpl.amount,
                                           0)),0) executory_payment1,
       Nvl(SUM(Decode(lpl.payment_type_id,3,lpl.adjustment_amount,
                                           0)),0) executory_adjustment1,
       Nvl(SUM(Decode(lpl.payment_type_id,4,lpl.amount,
                                           0)),0) executory_payment2,
       Nvl(SUM(Decode(lpl.payment_type_id,4,lpl.adjustment_amount,
                                           0)),0) executory_adjustment2,
       Nvl(SUM(Decode(lpl.payment_type_id,5,lpl.amount,
                                           0)),0) executory_payment3,
       Nvl(SUM(Decode(lpl.payment_type_id,5,lpl.adjustment_amount,
                                           0)),0) executory_adjustment3,
       Nvl(SUM(Decode(lpl.payment_type_id,6,lpl.amount,
                                           0)),0) executory_payment4,
       Nvl(SUM(Decode(lpl.payment_type_id,6,lpl.adjustment_amount,
                                           0)),0) executory_adjustment4,
       Nvl(SUM(Decode(lpl.payment_type_id,7,lpl.amount,
                                           0)),0) executory_payment5,
       Nvl(SUM(Decode(lpl.payment_type_id,7,lpl.adjustment_amount,
                                           0)),0) executory_adjustment5,
       Nvl(SUM(Decode(lpl.payment_type_id,8,lpl.amount,
                                           0)),0) executory_payment6,
       Nvl(SUM(Decode(lpl.payment_type_id,8,lpl.adjustment_amount,
                                           0)),0) executory_adjustment6,
       Nvl(SUM(Decode(lpl.payment_type_id,9,lpl.amount,
                                           0)),0) executory_payment7,
       Nvl(SUM(Decode(lpl.payment_type_id,9,lpl.adjustment_amount,
                                           0)),0) executory_adjustment7,
       Nvl(SUM(Decode(lpl.payment_type_id,10,lpl.amount,
                                           0)),0) executory_payment8,
       Nvl(SUM(Decode(lpl.payment_type_id,10,lpl.adjustment_amount,
                                           0)),0) executory_adjustment8,
       Nvl(SUM(Decode(lpl.payment_type_id,11,lpl.amount,
                                           0)),0) executory_payment9,
       Nvl(SUM(Decode(lpl.payment_type_id,11,lpl.adjustment_amount,
                                           0)),0) executory_adjustment9,
       Nvl(SUM(Decode(lpl.payment_type_id,12,lpl.amount,
                                           0)),0) executory_payment10,
       Nvl(SUM(Decode(lpl.payment_type_id,12,lpl.adjustment_amount,
                                           0)),0) executory_adjustment10,
       Nvl(SUM(Decode(lpl.payment_type_id,13,lpl.amount,
                                           0)),0) contingent_payment1,
       Nvl(SUM(Decode(lpl.payment_type_id,13,lpl.adjustment_amount,
                                           0)),0) contingent_adjustment1,
       Nvl(SUM(Decode(lpl.payment_type_id,14,lpl.amount,
                                           0)),0) contingent_payment2,
       Nvl(SUM(Decode(lpl.payment_type_id,14,lpl.adjustment_amount,
                                           0)),0) contingent_adjustment2,
       Nvl(SUM(Decode(lpl.payment_type_id,15,lpl.amount,
                                           0)),0) contingent_payment3,
       Nvl(SUM(Decode(lpl.payment_type_id,15,lpl.adjustment_amount,
                                           0)),0) contingent_adjustment3,
       Nvl(SUM(Decode(lpl.payment_type_id,16,lpl.amount,
                                           0)),0) contingent_payment4,
       Nvl(SUM(Decode(lpl.payment_type_id,16,lpl.adjustment_amount,
                                           0)),0) contingent_adjustment4,
       Nvl(SUM(Decode(lpl.payment_type_id,17,lpl.amount,
                                           0)),0) contingent_payment5,
       Nvl(SUM(Decode(lpl.payment_type_id,17,lpl.adjustment_amount,
                                           0)),0) contingent_adjustment5,
       Nvl(SUM(Decode(lpl.payment_type_id,18,lpl.amount,
                                           0)),0) contingent_payment6,
       Nvl(SUM(Decode(lpl.payment_type_id,18,lpl.adjustment_amount,
                                           0)),0) contingent_adjustment6,
       Nvl(SUM(Decode(lpl.payment_type_id,19,lpl.amount,
                                           0)),0) contingent_payment7,
       Nvl(SUM(Decode(lpl.payment_type_id,19,lpl.adjustment_amount,
                                           0)),0) contingent_adjustment7,
       Nvl(SUM(Decode(lpl.payment_type_id,20,lpl.amount,
                                           0)),0) contingent_payment8,
       Nvl(SUM(Decode(lpl.payment_type_id,20,lpl.adjustment_amount,
                                           0)),0) contingent_adjustment8,
       Nvl(SUM(Decode(lpl.payment_type_id,21,lpl.amount,
                                           0)),0) contingent_payment9,
       Nvl(SUM(Decode(lpl.payment_type_id,21,lpl.adjustment_amount,
                                           0)),0) contingent_adjustment9,
       Nvl(SUM(Decode(lpl.payment_type_id,22,lpl.amount,
                                           0)),0) contingent_payment10,
       Nvl(SUM(Decode(lpl.payment_type_id,22,lpl.adjustment_amount,
                                           0)),0) contingent_adjustment10,
       Nvl(SUM(Decode(lpl.payment_type_id,23,lpl.amount,
                                           0)),0) termination_penalty,
       Nvl(SUM(Decode(lpl.payment_type_id,24,lpl.amount,
                                           0)),0) sales_proceeds,
       SUM(Nvl(tax_1,0))                          AS tax_1,
       SUM(Nvl(tax_1_adjustment,0))               AS tax_1_adjustment,
       SUM(Nvl(tax_2,0))                          AS tax_2,
       SUM(Nvl(tax_2_adjustment,0))               AS tax_2_adjustment,
       SUM(Nvl(tax_3,0))                          AS tax_3,
       SUM(Nvl(tax_3_adjustment,0))               AS tax_3_adjustment,
       SUM(Nvl(tax_4,0))                          AS tax_4,
       SUM(Nvl(tax_4_adjustment,0))               AS tax_4_adjustment,
       SUM(Nvl(tax_5,0))                          AS tax_5,
       SUM(Nvl(tax_5_adjustment,0))               AS tax_5_adjustment,
       SUM(Nvl(tax_6,0))                          AS tax_6,
       SUM(Nvl(tax_6_adjustment,0))               AS tax_6_adjustment,
       SUM(Nvl(tax_7,0))                          AS tax_7,
       SUM(Nvl(tax_7_adjustment,0))               AS tax_7_adjustment,
       ls_cur_type.description                     currency_type,
       lpl.iso_code                                currency,
       lpl.currency_display_symbol                 currency_symbol
FROM   ls_asset la
       join company co
         ON la.company_id = co.company_id
       join v_ls_payment_line_fx lpl
         ON lpl.ls_asset_id = la.ls_asset_id
       join asset_location al
         ON la.asset_location_id = al.asset_location_id
       join ls_ilr ilr
         ON la.ilr_id = ilr.ilr_id
       join ls_lease ll
         ON ilr.lease_id = ll.lease_id
       join ls_ilr_options ilro
         ON la.ilr_id = ilro.ilr_id
            AND la.approved_revision = ilro.revision
       join ls_lease_cap_type lct
         ON ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
       join ls_lease_currency_type ls_cur_type
         ON lpl.ls_cur_type = ls_cur_type.ls_currency_type_id
       left join (SELECT lmt.ls_asset_id,
                         Nvl(SUM(Decode(tax_local_id,1,payment_amount,
                                                      0)),0) AS tax_1,
                         Nvl(SUM(Decode(tax_local_id,1,adjustment_amount,
                                                      0)),0) AS
                         tax_1_adjustment,
                         Nvl(SUM(Decode(tax_local_id,2,payment_amount,
                                                      0)),0) AS tax_2,
                         Nvl(SUM(Decode(tax_local_id,2,adjustment_amount,
                                                      0)),0) AS
                         tax_2_adjustment,
                         Nvl(SUM(Decode(tax_local_id,3,payment_amount,
                                                      0)),0) AS tax_3,
                         Nvl(SUM(Decode(tax_local_id,3,adjustment_amount,
                                                      0)),0) AS
                         tax_3_adjustment,
                         Nvl(SUM(Decode(tax_local_id,4,payment_amount,
                                                      0)),0) AS tax_4,
                         Nvl(SUM(Decode(tax_local_id,4,adjustment_amount,
                                                      0)),0) AS
                         tax_4_adjustment,
                         Nvl(SUM(Decode(tax_local_id,5,payment_amount,
                                                      0)),0) AS tax_5,
                         Nvl(SUM(Decode(tax_local_id,5,adjustment_amount,
                                                      0)),0) AS
                         tax_5_adjustment,
                         Nvl(SUM(Decode(tax_local_id,6,payment_amount,
                                                      0)),0) AS tax_6,
                         Nvl(SUM(Decode(tax_local_id,6,adjustment_amount,
                                                      0)),0) AS
                         tax_6_adjustment,
                         Nvl(SUM(Decode(tax_local_id,7,payment_amount,
                                                      0)),0) AS tax_7,
                         Nvl(SUM(Decode(tax_local_id,7,adjustment_amount,
                                                      0)),0) AS
                         tax_7_adjustment,
                         contract_currency_id,
                         company_currency_id,
                         ls_cur_type
                  FROM   v_ls_monthly_tax_fx_vw lmt
                  WHERE  To_char(gl_posting_mo_yr,''yyyymm'') IN (SELECT
                         filter_value
                                                                 FROM
                         pp_any_required_filter
                                                                 WHERE
                                 Upper(Trim(column_name)) = ''MONTHNUM'')
                         AND To_char(lmt.company_id) IN (SELECT filter_value
                                                         FROM
                             pp_any_required_filter
                                                         WHERE
                             Upper(Trim(column_name)) = ''COMPANY ID'')
                  GROUP  BY lmt.ls_asset_id,
                            contract_currency_id,
                            company_currency_id,
                            ls_cur_type) taxes
              ON la.ls_asset_id = taxes.ls_asset_id
                 AND ls_cur_type.ls_currency_type_id = taxes.ls_cur_type
WHERE  To_char(la.company_id) IN (SELECT filter_value
                                  FROM   pp_any_required_filter
                                  WHERE  Upper(Trim(column_name)) = ''COMPANY ID''
                                 )
       AND To_char(lpl.gl_posting_mo_yr,''yyyymm'') IN (SELECT filter_value
                                                       FROM
           pp_any_required_filter
                                                       WHERE
               Upper(Trim(column_name)) = ''MONTHNUM'')
       AND Upper(Trim(ls_cur_type.description)) IN (SELECT filter_value
                                                    FROM
           pp_any_required_filter
                                                    WHERE
               Upper(Trim(column_name)) = ''CURRENCY TYPE'')
GROUP  BY lpl.gl_posting_mo_yr,
          co.company_id,
          co.description,
          lct.description,
          ls_cur_type.description,
          lpl.iso_code,
          lpl.currency_display_symbol    ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'company_description',
        3,
        0,
        1,
        '',
        'Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'company_id',
        2,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        1,
        2,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment1',
        31,
        1,
        1,
        '',
        'Contingent Adjustment1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment10',
        49,
        1,
        1,
        '',
        'Contingent Adjustment10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment2',
        33,
        1,
        1,
        '',
        'Contingent Adjustment2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment3',
        35,
        1,
        1,
        '',
        'Contingent Adjustment3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment4',
        37,
        1,
        1,
        '',
        'Contingent Adjustment4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment5',
        39,
        1,
        1,
        '',
        'Contingent Adjustment5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment6',
        41,
        1,
        1,
        '',
        'Contingent Adjustment6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment7',
        43,
        1,
        1,
        '',
        'Contingent Adjustment7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment8',
        45,
        1,
        1,
        '',
        'Contingent Adjustment8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment9',
        47,
        1,
        1,
        '',
        'Contingent Adjustment9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment1',
        30,
        1,
        1,
        '',
        'Contingent Payment1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment10',
        48,
        1,
        1,
        '',
        'Contingent Payment10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment2',
        32,
        1,
        1,
        '',
        'Contingent Payment2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment3',
        34,
        1,
        1,
        '',
        'Contingent Payment3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment4',
        36,
        1,
        1,
        '',
        'Contingent Payment4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment5',
        38,
        1,
        1,
        '',
        'Contingent Payment5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment6',
        40,
        1,
        1,
        '',
        'Contingent Payment6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment7',
        42,
        1,
        1,
        '',
        'Contingent Payment7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment8',
        44,
        1,
        1,
        '',
        'Contingent Payment8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment9',
        46,
        1,
        1,
        '',
        'Contingent Payment9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'currency',
        5,
        0,
        0,
        '',
        'Currency',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        1

FROM dual

UNION ALL

SELECT  l_id,
        'currency_symbol',
        67,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        1,
        1

FROM dual

UNION ALL

SELECT  l_id,
        'currency_type',
        6,
        0,
        1,
        '',
        'Currency Type',
        300,
        'description',
        'ls_lease_currency_type',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment1',
        11,
        1,
        1,
        '',
        'Executory Adjustment1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment10',
        29,
        1,
        1,
        '',
        'Executory Adjustment10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment2',
        13,
        1,
        1,
        '',
        'Executory Adjustment2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment3',
        15,
        1,
        1,
        '',
        'Executory Adjustment3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment4',
        17,
        1,
        1,
        '',
        'Executory Adjustment4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment5',
        19,
        1,
        1,
        '',
        'Executory Adjustment5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment6',
        21,
        1,
        1,
        '',
        'Executory Adjustment6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment7',
        23,
        1,
        1,
        '',
        'Executory Adjustment7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment8',
        25,
        1,
        1,
        '',
        'Executory Adjustment8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment9',
        27,
        1,
        1,
        '',
        'Executory Adjustment9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment1',
        10,
        1,
        1,
        '',
        'Executory Payment1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment10',
        28,
        1,
        1,
        '',
        'Executory Payment10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment2',
        12,
        1,
        1,
        '',
        'Executory Payment2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment3',
        14,
        1,
        1,
        '',
        'Executory Payment3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment4',
        16,
        1,
        1,
        '',
        'Executory Payment4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment5',
        18,
        1,
        1,
        '',
        'Executory Payment5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment6',
        20,
        1,
        1,
        '',
        'Executory Payment6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment7',
        22,
        1,
        1,
        '',
        'Executory Payment7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment8',
        24,
        1,
        1,
        '',
        'Executory Payment8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment9',
        26,
        1,
        1,
        '',
        'Executory Payment9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_adjustment',
        9,
        1,
        1,
        '',
        'Interest Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_payment',
        8,
        1,
        1,
        '',
        'Interest Payment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_cap_type',
        4,
        0,
        1,
        '',
        'Lease Cap Type',
        300,
        'description',
        'ls_lease_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'monthnum',
        1,
        0,
        1,
        '',
        'Monthnum',
        300,
        'month_number',
        '(select month_number from pp_calendar)',
        'VARCHAR2',
        0,
        'month_number',
        1,
        2,
        'the_sort',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'principal_payment',
        7,
        1,
        1,
        '',
        'Principal Payment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'sales_proceeds',
        51,
        1,
        1,
        '',
        'Sales Proceeds',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_1',
        52,
        1,
        1,
        '',
        'Tax 1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_1_adjustment',
        53,
        1,
        1,
        '',
        'Tax 1 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_2',
        54,
        1,
        1,
        '',
        'Tax 2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_2_adjustment',
        55,
        1,
        1,
        '',
        'Tax 2 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_3',
        56,
        1,
        1,
        '',
        'Tax 3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_3_adjustment',
        57,
        1,
        1,
        '',
        'Tax 3 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_4',
        58,
        1,
        1,
        '',
        'Tax 4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_4_adjustment',
        59,
        1,
        1,
        '',
        'Tax 4 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_5',
        60,
        1,
        1,
        '',
        'Tax 5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_5_adjustment',
        61,
        1,
        1,
        '',
        'Tax 5 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_6',
        62,
        1,
        1,
        '',
        'Tax 6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_6_adjustment',
        63,
        1,
        1,
        '',
        'Tax 6 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_7',
        64,
        1,
        1,
        '',
        'Tax 7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_7_adjustment',
        65,
        1,
        1,
        '',
        'Tax 7 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'termination_penalty',
        50,
        1,
        1,
        '',
        'Termination Penalty',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual
;

END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16704, 0, 2018, 2, 0, 0, 53351, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053351_lessee_03_tax_query_changes_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;