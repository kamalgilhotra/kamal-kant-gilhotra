SET SERVEROUTPUT ON;

/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044550_cpr_drop_pend_transaction_sob_trigger_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 08/07/2015 Charlie Shilling maint-44550
||============================================================================
*/

--this is a standard time_stamp/user_id trigger that was mistakenly modified by
--another script, so I will drop it here and let the standard user_id/time_stamp
--script included in every release recreate it.
DECLARE
	trigger_not_found 	EXCEPTION;
	PRAGMA EXCEPTION_INIT(trigger_not_found, -4080);
BEGIN
	EXECUTE IMMEDIATE 'DROP TRIGGER pend_transaction_set_of_books';
	Dbms_Output.put_line('PEND_TRANSACTION_SET_OF_BOOKS trigger dropped.');
EXCEPTION
	WHEN trigger_not_found THEN
		Dbms_Output.put_line('PEND_TRANSACTION_SET_OF_BOOKS trigger does not exist. No action necessary.');
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2759, 0, 2015, 2, 0, 0, 044550, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044550_cpr_drop_pend_transaction_sob_trigger_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;