/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031778_taxrpr_wmis.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------
|| 10.4.2.0 11/25/2013 Alex Pivoshenko      Tax Repairs : WMIS logic changes
||============================================================================
*/

alter table REPAIR_DETAIL_WMIS_FEED
   drop (ASSET_ID,
         WORK_ORDER_TYPE_ID,
         CLOSING_OPTION_ID,
         FERC_PLT_ACCT_ID,
         ENG_IN_SERVICE_YEAR,
         CPI_COST,
         PEND_TRANS_ID);

alter table REPAIR_DETAIL_WMIS_FEED
   modify (WORK_REQUEST       not null,
           WORK_ORDER_NUMBER  not null,
           MONTH_NUMBER       not null,
           FERC_ACTIVITY_CODE not null,
           QUANTITY           not null,
           COMPANY_ID         not null,
           UTILITY_ACCOUNT_ID not null,
           BUS_SEGMENT_ID     not null,
           RETIREMENT_UNIT_ID not null);

drop table REPAIR_DETAIL_WMS_FEED_TEMP;

alter table REPAIR_WORK_ORDER_SEGMENTS drop column MY_PRE81_RETIREMENT_RATIO;

alter table REPAIR_WO_SEG_REPORTING drop column MY_PRE81_RETIREMENT_RATIO;

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REPAIRS', 'menu_wksp_debug', null, null, 'Debug Portal', 'uo_rpr_wksp_debug', 'Debug Portal');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('REPAIRS', 'menu_wksp_debug', null, null, 1, 8, 'Debug', 'Debug Portal', null,
    'menu_wksp_debug', 0);

create table REPAIR_TABLES
(
 TABLE_NAME varchar2(35) not null,
 USAGE_FLAG varchar2(35) not null,
 ORDER_IDX  number(22,0) not null
);

alter table REPAIR_TABLES
   add constraint PK_REPAIR_TABLES
       primary key (TABLE_NAME, USAGE_FLAG);

insert into REPAIR_TABLES( TABLE_NAME, USAGE_FLAG, ORDER_IDX )
select 'REPAIR_WORK_ORDER_TEMP_ORIG', 'CPR', 1 from dual union
select 'REPAIR_WORK_ORDER_TEMP', 'CPR', 2 from dual   union
select 'REPAIR_WORK_ORDER_SEGMENTS', 'CPR', 3 from dual  union
select 'REPAIR_MAJOR_UNIT_PCT', 'CPR', 4 from dual union
select 'REPAIR_WO_SEG_REPORTING', 'CPR', 5 from dual  union
select 'REPAIR_AMOUNT_ALLOCATE', 'CPR', 6 from dual   union
select 'REPAIR_AMT_ALLOC_REPORTING', 'CPR', 7 from dual  union
select 'REPAIR_CALC_PRA_REVERSE', 'CPR', 9 from dual  union
select 'REPAIR_CALC_ASSET', 'CPR', 8 from dual  union
select 'REPAIR_BATCH_CONTROL', 'CPR', 10 from dual union
select 'REPAIR_DETAIL_WMIS_FEED', 'WMIS', 1 from dual union
select 'REPAIR_WORK_ORDER_TEMP', 'WMIS', 3 from dual  union
select 'REPAIR_WORK_ORDER_SEGMENTS', 'WMIS', 2 from dual union
select 'REPAIR_MAJOR_UNIT_PCT', 'WMIS', 4 from dual   union
select 'REPAIR_WO_SEG_REPORTING', 'WMIS', 5 from dual union
select 'REPAIR_AMOUNT_ALLOCATE', 'WMIS', 6 from dual  union
select 'REPAIR_AMT_ALLOC_REPORTING', 'WMIS', 7 from dual union
select 'REPAIR_CALC_ASSET', 'WMIS', 8 from dual union
select 'REPAIR_BATCH_CONTROL', 'WMIS', 9 from dual union
select 'REPAIR_WORK_ORDER_TEMP_ORIG', 'BLANKET', 1 from dual   union
select 'REPAIR_WORK_ORDER_TEMP', 'BLANKET', 2 from dual  union
select 'REPAIR_BLANKET_PROCESSING', 'BLANKET', 3 from dual  union
select 'REPAIR_BLANKET_PROCESS_REPORT', 'BLANKET', 4 from dual union
select 'REPAIR_BLANKET_RESULTS', 'BLANKET', 5 from dual  union
select 'REPAIR_BLKT_RESULTS_REPORTING', 'BLANKET', 6 from dual union
select 'REPAIR_AMOUNT_ALLOCATE', 'BLANKET', 7 from dual  union
select 'REPAIR_AMT_ALLOC_REPORTING', 'BLANKET', 8 from dual union
select 'REPAIR_CALC_ASSET', 'BLANKET', 9 from dual;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (774, 0, 10, 4, 2, 0, 31778, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_031778_taxrpr_wmis.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
