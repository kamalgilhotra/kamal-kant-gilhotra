/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041883_prov_etr_grid_labels_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 12/17/2014 Blake Andrews    Labels were reversed when running
||                                        report by month
||============================================================================
*/

update tax_accrual_rep_cons_rows
set label_value = 'State Tax Expense'
where rep_cons_type_id in (32,33)
	and row_value = 'comp_dtl_state_taxes_tt_total';

update tax_accrual_rep_cons_rows
set label_value = 'Fed Benefit of State Tax Deduction'
where rep_cons_type_id in (32,33)
	and row_value = 'comp_dtl_fed_deduct_tt_total';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2119, 0, 2015, 1, 0, 0, 041883, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041883_prov_etr_grid_labels_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;