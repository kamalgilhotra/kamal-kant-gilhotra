 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048299_depr_draf_sys_control_dml.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.2   06/19/2017  A Smith       Initial script adding new system control
 ||============================================================================
 */ 

-- update long description if control exists with different description
update pp_system_control_company
set long_description = 'Defines the number of processing batches to split depr groups into for theoretical reserve allocation. If 0 or null (blank), Close PowerPlant will continue to process all groups in a single batch. If non-zero, Close PowerPlant will divide the groups into that many batches to reduce volume being processed at once which can improve performance. The Default is 0.'
where control_id = 
(select control_id from pp_system_control_company where upper(trim(control_name)) = 'CPR CLOSE MONTH LOOP ALLO RES')
and long_description <> 'Defines the number of processing batches to split depr groups into for theoretical reserve allocation. If 0 or null (blank), Close PowerPlant will continue to process all groups in a single batch. If non-zero, Close PowerPlant will divide the groups into that many batches to reduce volume being processed at one time which can improve performance. The Default is 0.'
;

-- add if it doesn't exist
insert into pp_system_control_company (control_id, control_name, control_value, company_id, long_description)
select new_id, 'CPR CLOSE MONTH LOOP ALLO RES', 0, -1, 
'Defines the number of processing batches to split depr groups into for theoretical reserve allocation. If 0 or null (blank), Close PowerPlant will continue to process all groups in a 
single batch, If non-zero, Close PowerPlant will divide the groups into that many batches to reduce volume being processed at once which can improve performance. The Default is 0.'
from (select  max(control_id) +1 new_id 
     from pp_system_control_company
     where not exists 
     (select 1 from pp_system_control_company where upper(trim(control_name)) = 'CPR CLOSE MONTH LOOP ALLO RES')
     ) 
where new_id is not null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3532, 0, 2016, 1, 2, 0, 48299, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.2.0_maint_048299_depr_draf_sys_control_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
