/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010723_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/05/2012 Chris Mardis   Point Release
||============================================================================
*/

alter table ORIGINAL_COST_RETIREMENT     add NEW_ESTIMATE_ID number(22);
alter table ORIGINAL_COST_RETIREMENT_STG add NEW_ESTIMATE_ID number(22);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (195, 0, 10, 3, 5, 0, 10723, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010723_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;