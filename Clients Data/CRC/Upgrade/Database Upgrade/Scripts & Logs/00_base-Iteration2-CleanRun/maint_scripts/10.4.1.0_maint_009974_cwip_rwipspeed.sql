/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009974_cwip_rwipspeed.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/31/2013 Sunjin Cone
||============================================================================
*/

delete from PP_SYSTEM_CONTROL_COMPANY where UPPER(CONTROL_NAME) = 'RWIP CLOSEOUT ARCHIVE';

drop index RWIP_CLOSEOUT_STAG_AL_PERF;

create index RWIP_CLOSEOUT_STAG_AL_DGID
   on RWIP_CLOSEOUT_STAG_AL (DEPR_GROUP_ID)
      tablespace PWRPLANT_IDX;

alter table RWIP_CLOSEOUT_STAGING parallel 4;

alter table RWIP_CLOSEOUT_STAG_AL parallel 4;

alter table RWIP_CLOSEOUT_REVISION add RWIP_TYPE_ID number(22,0);
comment on column RWIP_CLOSEOUT_REVISION.RWIP_TYPE_ID is 'System-assigned identifier of a particular RWIP Type';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (490, 0, 10, 4, 1, 0, 9974, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_009974_cwip_rwipspeed.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;