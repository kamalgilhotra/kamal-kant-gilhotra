 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_042210_pcm_authorizations_ddl.sql
|| Description: Add pct_complete field to work_order_approval
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1   01/18/2015 Chris Mardis   New Functionality
||============================================================================
*/

alter table work_order_approval add pct_complete number(22,8);
comment on column work_order_approval.pct_complete is 'A value between 0 and 1 that signifies the percent of work completed on this work order or funding project.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2191, 0, 2015, 1, 0, 0, 042210, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042210_pcm_authorizations_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;