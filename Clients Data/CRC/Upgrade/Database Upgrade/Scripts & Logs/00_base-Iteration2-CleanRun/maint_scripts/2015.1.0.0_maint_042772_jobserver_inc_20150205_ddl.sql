/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_042772_jobserver_inc_20150205_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     03/03/2014 Paul Cordero    	 Creating new table
||==========================================================================================
*/

set serveroutput on;

declare 
  doesTableColumnExistAs number := 0;
begin
	begin
	   
		select COUNT(*) INTO doesTableColumnExistAs
		from ALL_TAB_COLUMNS
		where DATA_TYPE = upper('VARCHAR2')
		  and DATA_LENGTH = 4000
		  and COLUMN_NAME = upper('RESULT')
		  and TABLE_NAME = upper('PP_JOB_EXECUTION_JOB') 
		  and OWNER = upper('PWRPLANT');   	   
          
		if doesTableColumnExistAs = 1 then
			begin
				dbms_output.put_line('Changing column RESULT in PP_JOB_EXECUTION_JOB table to varchar(50)');
	
				-- CREATE TEMP CLOB COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
				ADD 
				(
				"RESULT_TMP" CLOB
				)';

				-- COPY DATA FROM RESULT TO TEMP COLUMN
				execute immediate 'UPDATE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
				SET  RESULT_TMP = RESULT';
				execute immediate 'COMMIT';				

				-- DROP 'RESULT' COLUMN
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
				DROP COLUMN RESULT';

				-- RENAME TEMP COLUMN TO 'RESULT'
				execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
				RENAME COLUMN RESULT_TMP TO RESULT';			  

			end;
		  else
			begin
			  dbms_output.put_line('RESULT in PP_JOB_EXECUTION_JOB table is varchar(50)');
			end;
		end if;
	end;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2345, 0, 2015, 1, 0, 0, 042772, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042772_jobserver_inc_20150205_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;