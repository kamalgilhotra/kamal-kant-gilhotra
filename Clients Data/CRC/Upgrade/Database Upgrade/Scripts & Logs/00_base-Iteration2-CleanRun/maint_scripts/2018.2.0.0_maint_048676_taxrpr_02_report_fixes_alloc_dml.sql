/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048676_taxrpr_02_report_fixes_alloc_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 08/01/2017 Eric Berger    Updating the datawindow values in
||                                      reporting tables to reflect the new
||                                      tax repairs standard.
||============================================================================
*/

--ALLOC 4010
UPDATE pp_reports
SET datawindow = 'dw_rpr_4010'
WHERE report_number = 'ALLOC - 4010';

--ALLOC 4020
UPDATE pp_reports
SET datawindow = 'dw_rpr_4020'
WHERE report_number = 'ALLOC - 4020';

--insert new reports:

--4030
insert into pp_reports
    (REPORT_ID,
     DESCRIPTION,
     LONG_DESCRIPTION,
     SUBSYSTEM,
     DATAWINDOW,
     SPECIAL_NOTE,
     REPORT_TYPE,
     TIME_OPTION,
     REPORT_NUMBER,
     INPUT_WINDOW,
     FILTER_OPTION,
     STATUS,
     PP_REPORT_SUBSYSTEM_ID,
     REPORT_TYPE_ID,
     PP_REPORT_TIME_OPTION_ID,
     PP_REPORT_FILTER_ID,
     PP_REPORT_STATUS_ID,
     PP_REPORT_ENVIR_ID,
     DOCUMENTATION,
     USER_COMMENT,
     LAST_APPROVED_DATE,
     PP_REPORT_NUMBER,
     OLD_REPORT_NUMBER,
     DYNAMIC_DW,
     TURN_OFF_MULTI_THREAD)
select (select max(report_id) + 1 from pp_reports),
       'Alloc: Run Results Retirements',
       'Blanket Network Repairs:  Part 2 of 2 Process Run Results for Blanket Work Orders using Pro-Rata or Proporional.  Display dollar values for each work order and repair unit.',
       null,
       'dw_rpr_4030',
       null,
       null,
       null,
       'ALLOC - 4030',
       null,
       null,
       null,
       13,
       104,
       42,
       32,
       1,
       3,
       null,
       null,
       null,
       'ALLOC - 4030',
       null,
       0,
       1 from dual
       WHERE NOT EXISTS (
       SELECT 1 FROM pp_reports
       WHERE datawindow = 'dw_rpr_4030')
       ;



--4040
insert into pp_reports
  (REPORT_ID,
   DESCRIPTION,
   LONG_DESCRIPTION,
   SUBSYSTEM,
   DATAWINDOW,
   SPECIAL_NOTE,
   REPORT_TYPE,
   TIME_OPTION,
   REPORT_NUMBER,
   INPUT_WINDOW,
   FILTER_OPTION,
   STATUS,
   PP_REPORT_SUBSYSTEM_ID,
   REPORT_TYPE_ID,
   PP_REPORT_TIME_OPTION_ID,
   PP_REPORT_FILTER_ID,
   PP_REPORT_STATUS_ID,
   PP_REPORT_ENVIR_ID,
   DOCUMENTATION,
   USER_COMMENT,
   LAST_APPROVED_DATE,
   PP_REPORT_NUMBER,
   OLD_REPORT_NUMBER,
   DYNAMIC_DW,
   TURN_OFF_MULTI_THREAD)
SELECT
  (select max(report_id) + 1 from pp_reports),
   'Alloc: Run Results',
   'Blanket Network Repairs:  Combined Process Run Results for Blanket Work Orders using Pro-Rata or Proporional.  Display dollar values for each work order and repair unit.',
   null,
   'dw_rpr_4040',
   null,
   null,
   null,
   'ALLOC - 4040',
   null,
   null,
   null,
   13,
   104,
   42,
   32,
   1,
   3,
   null,
   null,
   null,
   'ALLOC - 4040',
   null,
   0,
   1
   FROM dual
   WHERE NOT EXISTS (
       SELECT 1 FROM pp_reports
       WHERE datawindow = 'dw_rpr_4040')
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13729, 0, 2018, 2, 0, 0, 48676, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_048676_taxrpr_02_report_fixes_alloc_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;