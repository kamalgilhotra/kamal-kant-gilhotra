/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_050124_lesee_import_asset_est_residual_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.1.0.0  12/07/2017 Shane "C" Ward Remove unecessary xlate for estimated residual amount
||============================================================================
*/

UPDATE PP_IMPORT_COLUMN
SET    parent_table = 'ls_import_asset'
WHERE  column_name = 'estimated_residual' AND
       import_type_id IN ( 253 ); 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4065, 0, 2017, 1, 0, 0, 50124, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_050124_lessee_import_asset_est_residual_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;