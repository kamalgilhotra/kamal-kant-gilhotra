--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042909_proptax_recon_rpts_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     03/06/2015 B Borm, A Scott  New estimate reconciliation reports
--||============================================================================
--*/

set define off

insert into pp_reports ( report_id, time_stamp, user_id, description, long_description, datawindow, report_number, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, dynamic_dw ) values( 503400, sysdate, user, 'Recon: Liab (Bills) by Tax District', 'Recon of tax liability calculated from prior year bills by tax district. Includes bill amount, calculated tax liability, difference, and percent difference. The Current Case is where liability is calculated; the Prior Case is the final case of the bills.', 'dw_ptr_asmt_liab_pr_recon_by_st_cty_td', 'PropTax - 3400', 10, 206, 19, 8, 2, 3, 0);
insert into pp_reports ( report_id, time_stamp, user_id, description, long_description, datawindow, report_number, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, dynamic_dw ) values( 503401, sysdate, user, 'Recon: Liab (Bills) by Dist & Prcl', 'Recon of tax liability calculated from prior year bills by tax district & parcel. Includes bill amount, calculated tax liability, & difference.  The Current Case is where liability is calculated; the Prior Case is the final case of the bills.', 'dw_ptr_asmt_liab_pr_recon_by_st_cty_td_pr', 'PropTax - 3401', 10, 206, 19, 8, 2, 3, 0);
insert into pp_reports ( report_id, time_stamp, user_id, description, long_description, datawindow, report_number, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, dynamic_dw ) values( 503402, sysdate, user, 'Recon: Liab (Rates) by Tax District', 'Recon of tax liability calculated from assessments and rates by tax district, parcel, tax authority and levy class. Includes assessment, levy rate, calculated liability, tax liability, & difference. ', 'dw_ptr_asmt_liab_ar_recon_by_st_cty_td', 'PropTax - 3402', 10, 206, 18, 7, 2, 3, 0);
insert into pp_reports ( report_id, time_stamp, user_id, description, long_description, datawindow, report_number, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, dynamic_dw ) values( 503403, sysdate, user, 'Recon: Liab (Rates) by Dist & Prcl', 'Recon of tax liability calculated from assessments and rates by tax district, parcel, tax authority and levy class. Includes assessment, levy rate, calculated liability, tax liability, & difference. ', 'dw_ptr_asmt_liab_ar_recon_by_st_cty_td_pr', 'PropTax - 3403', 10, 206, 18, 7, 2, 3, 0);
insert into pp_reports ( report_id, time_stamp, user_id, description, long_description, datawindow, report_number, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, dynamic_dw ) values( 504200, sysdate, user, 'Compare: Liab & Tax by Tax District', 'Comparison of calc tax liability to billed amount by tax dist, tax auth & levy class. Includes bill tax, taxable value, levy rate, calc liability, & difference. The Current Case is where liability is calculated; the Prior Case is the bill final case.', 'dw_ptr_asmt_liab_compare_by_st_cty_td', 'PropTax - 4200', 10, 207, 19, 8, 2, 3, 0);
insert into pp_reports ( report_id, time_stamp, user_id, description, long_description, datawindow, report_number, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, dynamic_dw ) values( 504201, sysdate, user, 'Compare: Liab & Tax by Dist & Prcl', 'Comparison of calc tax liability to billed amount by tax dist, parcel, tax auth & levy class. Includes bill tax, taxable value, levy rate, calc liability, & difference. The Current Case is the estimate case; the Prior Case is the bill final case.', 'dw_ptr_asmt_liab_compare_by_st_cty_td_pr', 'PropTax - 4201', 10, 207, 19, 8, 2, 3, 0);

set define on

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2356, 0, 2015, 1, 0, 0, 42909, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042909_proptax_recon_rpts_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;