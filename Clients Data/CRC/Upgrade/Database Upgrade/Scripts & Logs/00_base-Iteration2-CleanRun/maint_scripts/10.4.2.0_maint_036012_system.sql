/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036012_system.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/27/2014 Chris Mardis
||============================================================================
*/

create table PP_ALERT_PROCESSES
(
 ALERT        varchar2(100) not null,
 SERVER_ALERT varchar2(100),
 PROCESS_NAME varchar2(100),
 ALERT_TYPE   varchar2(20)
);

comment on table PP_ALERT_PROCESSES is '(S)[10] This table holds alert-process pairings used by the listening service to prompt the user for information during PL/SQL function calls.';
comment on column PP_ALERT_PROCESSES.ALERT is 'The primary key of the alert pair.';
comment on column PP_ALERT_PROCESSES.SERVER_ALERT is 'The server alert value of pair.';
comment on column PP_ALERT_PROCESSES.PROCESS_NAME is 'The name of the process the alert is monitoring.';
comment on column PP_ALERT_PROCESSES.ALERT_TYPE is 'Determines the type of alert that gets displayed (messagebox, statusbox, etc)';

alter table PP_ALERT_PROCESSES add DESCRIPTION varchar2(100);

alter table PP_ALERT_PROCESSES
   add constraint PP_ALTER_PROCESSES_PK
       primary key (ALERT)
       using index tablespace PWRPLANT_IDX;

create table PP_ALERT_PROCESS_LOG
(
 PROCESS   varchar2(100),
 ALERT     varchar2(100),
 SESSIONID varchar2(35),
 LOG_DATE  date,
 STATUS    varchar2(10)
);

comment on table PP_ALERT_PROCESS_LOG is '(S)[10] This table holds the logs data for the pp_alert_processes table.';
comment on column PP_ALERT_PROCESS_LOG.PROCESS is 'The name of the process the alert is monitoring.';
comment on column PP_ALERT_PROCESS_LOG.ALERT is 'The name of the alert corresponding to this log.';
comment on column PP_ALERT_PROCESS_LOG.SESSIONID is 'The id of the active session.';
comment on column PP_ALERT_PROCESS_LOG.LOG_DATE is 'The date of the log.';
comment on column PP_ALERT_PROCESS_LOG.STATUS is 'Indicator of the status of the listener. Example values are "Waiting" and "ACK"';

insert into PP_ALERT_PROCESSES
   (ALERT, SERVER_ALERT, PROCESS_NAME, ALERT_TYPE, DESCRIPTION)
values
   ('alert_test', 'server_alert_test', 'alert_test', null, 'Alert Test');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (918, 0, 10, 4, 2, 0, 36012, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036012_system.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;