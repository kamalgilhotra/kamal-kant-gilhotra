/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_050900_lessee_01_add_interest_catchup_sys_control_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2017.4.0.0 04/26/2018 Charlie Shilling Add new System Control for Lessee
||============================================================================
*/

insert into pp_system_control_company(control_id, control_name, control_value, description, long_description, company_id)
SELECT *
FROM (select
   		max(control_id)+1 AS control_id,
   		'Disable Lease Interest True-up JEs' AS control_name,
   		'no' AS control_value,
   		'dw_yes_no;1' AS description,
   		'Catch Up Interest Accrued and Interest Paid on Lease Backdated Addition' AS long_description,
   		-1 AS company_id
	from pp_system_control_company
)
where not exists(
   select 1
   from pp_system_control_company
   where control_name = 'Disable Lease Interest True-up JEs');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5043, 0, 2017, 4, 0, 0, 50900, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050900_lessee_01_add_interest_catchup_sys_control_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;