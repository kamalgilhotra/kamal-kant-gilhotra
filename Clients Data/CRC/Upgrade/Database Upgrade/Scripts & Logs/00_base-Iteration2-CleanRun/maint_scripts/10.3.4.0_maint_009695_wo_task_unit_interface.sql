/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009695_wo_task_unit_interface.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   04/12/2012 Chris Mardis   Point Release
||============================================================================
*/

--
-- CHANGES TO WORK_ORDER_TYPE
--

alter table WORK_ORDER_TYPE add EXTERNAL_WORK_ORDER_TYPE varchar2(254);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION,
    READ_ONLY)
   select 'external_work_order_type',
          'work_order_type',
          'e',
          'External Work Order Type',
          NVL(max(COLUMN_RANK), 0) + 1,
          '1 Descriptive',
          0
     from POWERPLANT_COLUMNS
    where TABLE_NAME = 'work_order_type';

--
-- CHANGES TO WO_INTERFACE_STAGING
--
alter table WO_INTERFACE_STAGING add BATCH_ID               varchar2(60);
-- This was done on the create in script 361_changedb_10.2.0.1_Budget_upgrade.sql
--alter table WO_INTERFACE_STAGING add BACKGROUND_INFORMATION varchar2(2000);
--alter table WO_INTERFACE_STAGING add REIMBURSABLE_TYPE_ID   number(22,0);
alter table WO_INTERFACE_STAGING add EXT_COMPANY            varchar2(35);
alter table WO_INTERFACE_STAGING add EXT_WORK_ORDER_TYPE    varchar2(254);
alter table WO_INTERFACE_STAGING add EXT_MAJOR_LOCATION     varchar2(254);
alter table WO_INTERFACE_STAGING add BUDGET_NUMBER          varchar2(35);
alter table WO_INTERFACE_STAGING add BUDGET_COMPANY_ID      number(22);
alter table WO_INTERFACE_STAGING add EXT_DEPARTMENT         varchar2(254);
alter table WO_INTERFACE_STAGING add EXT_WORK_ORDER_GROUP   varchar2(254);
alter table WO_INTERFACE_STAGING add EXT_ASSET_LOCATION     varchar2(254);
alter table WO_INTERFACE_STAGING add SEQ_ID                 number(22);

alter table WO_INTERFACE_STAGING modify WORK_ORDER_TYPE_ID   number(22) null;
alter table WO_INTERFACE_STAGING modify BUDGET_VERSION_ID    number(22) null;
alter table WO_INTERFACE_STAGING modify WORK_ORDER_NUMBER    varchar2(35) not null;
alter table WO_INTERFACE_STAGING modify FUNDING_WO_INDICATOR number(22) not null;

--
-- CHANGES TO WO_INTERFACE_STAGING_ARC
--
alter table WO_INTERFACE_STAGING_ARC add BATCH_ID               varchar2(60);
alter table WO_INTERFACE_STAGING_ARC add BACKGROUND_INFORMATION varchar2(2000);
alter table WO_INTERFACE_STAGING_ARC add REIMBURSABLE_TYPE_ID   number(22,0);
alter table WO_INTERFACE_STAGING_ARC add EXT_COMPANY            varchar2(35);
alter table WO_INTERFACE_STAGING_ARC add EXT_WORK_ORDER_TYPE    varchar2(254);
alter table WO_INTERFACE_STAGING_ARC add EXT_MAJOR_LOCATION     varchar2(254);
alter table WO_INTERFACE_STAGING_ARC add BUDGET_NUMBER          varchar2(35);
alter table WO_INTERFACE_STAGING_ARC add BUDGET_COMPANY_ID      number(22);
alter table WO_INTERFACE_STAGING_ARC add EXT_DEPARTMENT         varchar2(254);
alter table WO_INTERFACE_STAGING_ARC add EXT_WORK_ORDER_GROUP   varchar2(254);
alter table WO_INTERFACE_STAGING_ARC add EXT_ASSET_LOCATION     varchar2(254);
alter table WO_INTERFACE_STAGING_ARC add SEQ_ID                 number(22);

alter table WO_INTERFACE_STAGING_ARC modify WORK_ORDER_TYPE_ID number(22) null;
alter table WO_INTERFACE_STAGING_ARC modify BUDGET_VERSION_ID  number(22) null;
alter table WO_INTERFACE_STAGING_ARC modify WORK_ORDER_NUMBER  varchar2(35) not null;

--
-- CHANGES TO JOB_TASK_INTERFACE_STAGING
--
alter table JOB_TASK_INTERFACE_STAGING add BATCH_ID    varchar2(60);
alter table JOB_TASK_INTERFACE_STAGING add EXT_COMPANY varchar2(35);
alter table JOB_TASK_INTERFACE_STAGING add SEQ_ID      number(22);

--
-- CHANGES TO JOB_TASK_INTERFACE_STAGING_ARC
--
alter table JOB_TASK_INTERFACE_STAGING_ARC add BATCH_ID    varchar2(60);
alter table JOB_TASK_INTERFACE_STAGING_ARC add EXT_COMPANY varchar2(35);
alter table JOB_TASK_INTERFACE_STAGING_ARC add SEQ_ID      number(22);

--
-- CHANGES TO WO_INTERFACE_UNIT
--
--** drop columns from wo_interface_unit staging table
alter table WO_INTERFACE_UNIT drop column BATCH_UNIT_ITEM_ID;

--** add columns to wo_interface_unit staging table
alter table WO_INTERFACE_UNIT
   add (UNIT_DESC        varchar2(35) null,
        UNIT_LONG_DESC   varchar2(254) null,
        PERCENT          number(22,8) null,
        FIELD_1          varchar2(35) null,
        FIELD_2          varchar2(35) null,
        FIELD_3          varchar2(35) null,
        FIELD_4          varchar2(35) null,
        FIELD_5          varchar2(35) null,
        COMPANY_ID       number(22) null);

alter table WO_INTERFACE_UNIT modify WORK_ORDER_NUMBER   varchar2(35) not null;
alter table WO_INTERFACE_UNIT modify EXPENDITURE_TYPE_ID number(22) null;
alter table WO_INTERFACE_UNIT modify EST_CHG_TYPE_ID     number(22) null;
alter table WO_INTERFACE_UNIT modify HOURS               number(22,2) null;
alter table WO_INTERFACE_UNIT modify AMOUNT              number(22,2) null;

alter table WO_INTERFACE_UNIT add BATCH_ID              varchar2(60);
alter table WO_INTERFACE_UNIT add EXT_COMPANY           varchar2(35);
alter table WO_INTERFACE_UNIT add FUNDING_WO_INDICATOR  number(22);
alter table WO_INTERFACE_UNIT add EXT_EXPENDITURE_TYPE  varchar2(254);
alter table WO_INTERFACE_UNIT add EXT_EST_CHG_TYPE      varchar2(254);
alter table WO_INTERFACE_UNIT add EXT_STCK_KEEP_UNIT    varchar2(254);
alter table WO_INTERFACE_UNIT add EXT_WO_EST_TRANS_TYPE varchar2(35);
alter table WO_INTERFACE_UNIT add EXT_DEPARTMENT        varchar2(254);
alter table WO_INTERFACE_UNIT add EXT_JOB_TASK          varchar2(254);
alter table WO_INTERFACE_UNIT add EXT_BUS_SEGMENT       varchar2(254);
alter table WO_INTERFACE_UNIT add EXT_UTILITY_ACCOUNT   varchar2(254);
alter table WO_INTERFACE_UNIT add EXT_SUB_ACCOUNT       varchar2(254);
alter table WO_INTERFACE_UNIT add EXT_PROPERTY_GROUP    varchar2(254);
alter table WO_INTERFACE_UNIT add EXT_RETIREMENT_UNIT   varchar2(254);
alter table WO_INTERFACE_UNIT add EXT_ASSET_LOCATION    varchar2(254);
alter table WO_INTERFACE_UNIT add PROCESS_LEVEL         varchar2(35);

--
-- CHANGES TO WO_INTERFACE_UNIT_ARC
--
--** drop columns from wo_interface_unit_arc staging table
alter table WO_INTERFACE_UNIT_ARC drop column BATCH_UNIT_ITEM_ID;

--** add columns to wo_interface_unit_arc staging table
alter table WO_INTERFACE_UNIT_ARC
   add (UNIT_DESC        varchar2(35) null,
        UNIT_LONG_DESC   varchar2(254) null,
        PERCENT          number(22,8) null,
        FIELD_1          varchar2(35) null,
        FIELD_2          varchar2(35) null,
        FIELD_3          varchar2(35) null,
        FIELD_4          varchar2(35) null,
        FIELD_5          varchar2(35) null,
        COMPANY_ID       number(22) null);

alter table WO_INTERFACE_UNIT_ARC add BATCH_ID              varchar2(60);
alter table WO_INTERFACE_UNIT_ARC add EXT_COMPANY           varchar2(35);
alter table WO_INTERFACE_UNIT_ARC add FUNDING_WO_INDICATOR  number(22);
alter table WO_INTERFACE_UNIT_ARC add EXT_EXPENDITURE_TYPE  varchar2(254);
alter table WO_INTERFACE_UNIT_ARC add EXT_EST_CHG_TYPE      varchar2(254);
alter table WO_INTERFACE_UNIT_ARC add EXT_STCK_KEEP_UNIT    varchar2(254);
alter table WO_INTERFACE_UNIT_ARC add EXT_WO_EST_TRANS_TYPE varchar2(35);
alter table WO_INTERFACE_UNIT_ARC add EXT_DEPARTMENT        varchar2(254);
alter table WO_INTERFACE_UNIT_ARC add EXT_JOB_TASK          varchar2(254);
alter table WO_INTERFACE_UNIT_ARC add EXT_BUS_SEGMENT       varchar2(254);
alter table WO_INTERFACE_UNIT_ARC add EXT_UTILITY_ACCOUNT   varchar2(254);
alter table WO_INTERFACE_UNIT_ARC add EXT_SUB_ACCOUNT       varchar2(254);
alter table WO_INTERFACE_UNIT_ARC add EXT_PROPERTY_GROUP    varchar2(254);
alter table WO_INTERFACE_UNIT_ARC add EXT_RETIREMENT_UNIT   varchar2(254);
alter table WO_INTERFACE_UNIT_ARC add EXT_ASSET_LOCATION    varchar2(254);
alter table WO_INTERFACE_UNIT_ARC add PROCESS_LEVEL         varchar2(35);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (117, 0, 10, 3, 4, 0, 9695, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009695_wo_task_unit_interface.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
