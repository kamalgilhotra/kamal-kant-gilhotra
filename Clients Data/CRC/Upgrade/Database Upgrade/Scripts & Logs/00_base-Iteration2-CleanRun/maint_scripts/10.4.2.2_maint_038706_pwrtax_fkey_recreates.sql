SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038706_pwrtax_fkey_recreates.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.2 06/26/2014 Andrew Scott        The alter package fix related to this issue may
||                                         have previously been the reason foreign
||                                         keys were dropped on tax tables.  This script
||                                         recreates them but will not error out if they
||                                         already exist.
||============================================================================
*/

begin
   execute immediate 'alter table TAX_PACKAGE_CONTROL add constraint TAX_PACKAGE_CONTROL_CO_FK foreign key (COMPANY_ID) references COMPANY_SETUP (COMPANY_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_PACKAGE_CONTROL add constraint TAX_PACKAGE_CONTROL_SCHF_FK foreign key (TAX_DEPR_SCHEMA_ID_FROM) references TAX_DEPR_SCHEMA (TAX_DEPR_SCHEMA_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_PACKAGE_CONTROL add constraint TAX_PACKAGE_CONTROL_SCHT_FK foreign key (TAX_DEPR_SCHEMA_ID_TO) references TAX_DEPR_SCHEMA (TAX_DEPR_SCHEMA_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_PACKAGE_CONTROL add constraint TAX_PACKAGE_CONTROL_TC_FK foreign key (TAX_CLASS_ID) references TAX_CLASS (TAX_CLASS_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_PACKAGE_CONTROL add constraint TAX_PACKAGE_CONTROL_TE_FK foreign key (TAX_EVENT_ID) references TAX_EVENT (TAX_EVENT_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_PACKAGE_CONTROL add constraint TAX_PACKAGE_CONTROL_TL_FK foreign key (TAX_LAYER_ID) references TAX_LAYER (TAX_LAYER_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_PACKAGE_CONTROL add constraint TAX_PACKAGE_CONTROL_V_FK foreign key (DEF_GAIN_VINTAGE_ID) references VINTAGE (VINTAGE_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION add constraint TAX_DEPRECIATION_CONV_FK foreign key (CONVENTION_ID) references TAX_CONVENTION (CONVENTION_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION add constraint TAX_DEPRECIATION_DEFSCHMA_FK foreign key (DEFERRED_TAX_SCHEMA_ID) references DEFERRED_TAX_SCHEMA (DEFERRED_TAX_SCHEMA_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION add constraint TAX_DEPRECIATION_RECPRD_FK foreign key (RECOVERY_PERIOD_ID) references RECOVERY_PERIOD (RECOVERY_PERIOD_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION add constraint TAX_DEPRECIATION_SUM4562_FK foreign key (SUMMARY_4562_ID) references SUMMARY_4562 (SUMMARY_4562_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION add constraint TAX_DEPRECIATION_TAXLAW_FK foreign key (TAX_LAW_ID) references TAX_LAW (TAX_LAW_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION add constraint TAX_DEPRECIATION_TAXLIMIT_FK foreign key (TAX_LIMIT_ID) references TAX_LIMIT (TAX_LIMIT_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION add constraint TAX_DEPRECIATION_TAXRATE_FK foreign key (TAX_RATE_ID) references TAX_RATE_CONTROL (TAX_RATE_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION add constraint TAX_DEPRECIATION_TAXRECD_FK foreign key (TAX_RECORD_ID) references TAX_RECORD_CONTROL (TAX_RECORD_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION add constraint TAX_DEPRECIATION_TB_FK foreign key (TAX_BOOK_ID) references TAX_BOOK (TAX_BOOK_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION add constraint TAX_DEPRECIATION_TYPEPROP_FK foreign key (TYPE_OF_PROPERTY_ID) references TAX_TYPE_OF_PROPERTY (TYPE_OF_PROPERTY_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION add constraint TAX_DEPRECIATION_XTRACONV_FK foreign key (EXTRAORDINARY_CONVENTION) references TAX_CONVENTION (CONVENTION_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table BASIS_AMOUNTS add constraint BASIS_AMOUNTS_TAX_ACT_CODE_FK foreign key (TAX_ACTIVITY_CODE_ID) references TAX_ACTIVITY_CODE (TAX_ACTIVITY_CODE_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table BASIS_AMOUNTS add constraint BASIS_AMOUNTS_TAX_INCLUDE_FK foreign key (TAX_INCLUDE_ID) references TAX_INCLUDE (TAX_INCLUDE_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table BASIS_AMOUNTS add constraint BASIS_AMOUNTS_TAX_RECORD_FK foreign key (TAX_RECORD_ID) references TAX_RECORD_CONTROL (TAX_RECORD_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table BASIS_AMOUNTS add constraint BASIS_AMOUNTS_TAX_SUMMARY_FK foreign key (TAX_SUMMARY_ID) references TAX_SUMMARY_CONTROL (TAX_SUMMARY_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table DEFERRED_INCOME_TAX add constraint DEFERRED_INCOME_TAX_NORM_FK foreign key (NORMALIZATION_ID) references NORMALIZATION_SCHEMA (NORMALIZATION_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table DEFERRED_INCOME_TAX add constraint DEFERRED_INCOME_TAX_RECD_FK foreign key (TAX_RECORD_ID) references TAX_RECORD_CONTROL (TAX_RECORD_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_BOOK_RECONCILE add constraint TAX_BOOK_RECONCILE_T_INCLD_FK foreign key (TAX_INCLUDE_ID) references TAX_INCLUDE (TAX_INCLUDE_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_BOOK_RECONCILE add constraint TAX_BOOK_RECONCILE_TAX_RCON_FK foreign key (RECONCILE_ITEM_ID) references TAX_RECONCILE_ITEM (RECONCILE_ITEM_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_BOOK_RECONCILE add constraint TAX_BOOK_RECONCILE_TAX_RECD_FK foreign key (TAX_RECORD_ID) references TAX_RECORD_CONTROL (TAX_RECORD_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_BOOK_RECONCILE_TRANSFER add constraint TAX_BOOK_RECON_XFER_T_INCLD_FK foreign key (TAX_INCLUDE_ID) references TAX_INCLUDE (TAX_INCLUDE_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_BOOK_RECONCILE_TRANSFER add constraint TAX_BOOK_RECON_XFER_TAX_RCN_FK foreign key (RECONCILE_ITEM_ID) references TAX_RECONCILE_ITEM (RECONCILE_ITEM_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_BOOK_RECONCILE_TRANSFER add constraint TAX_BOOK_RECON_XFER_T_RECD_FK foreign key (TAX_RECORD_ID) references TAX_RECORD_CONTROL (TAX_RECORD_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_BOOK_RECONCILE_TRANSFER add constraint TAX_BOOK_RECON_XFER_T_XFER_FK foreign key (TAX_TRANSFER_ID) references TAX_TRANSFER_CONTROL (TAX_TRANSFER_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION_TRANSFER add constraint TAX_DEPR_XFER_TAX_BOOK_FK foreign key (TAX_BOOK_ID) references TAX_BOOK (TAX_BOOK_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION_TRANSFER add constraint TAX_DEPR_XFER_TAX_RECD_FK foreign key (TAX_RECORD_ID) references TAX_RECORD_CONTROL (TAX_RECORD_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_DEPRECIATION_TRANSFER add constraint TAX_DEPR_XFER_TAX_XFER_FK foreign key (TAX_TRANSFER_ID) references TAX_TRANSFER_CONTROL (TAX_TRANSFER_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_RECORD_CONTROL add constraint TAX_RECORD_CONTROL_COMPANY_FK foreign key (COMPANY_ID) references COMPANY_SETUP (COMPANY_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_RECORD_CONTROL add constraint TAX_RECORD_CONTROL_TAX_CLS_FK foreign key (TAX_CLASS_ID) references TAX_CLASS (TAX_CLASS_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_RECORD_CONTROL add constraint TAX_RECORD_CONTROL_TAX_LOC_FK foreign key (TAX_LOCATION_ID) references TAX_LOCATION (TAX_LOCATION_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_RECORD_CONTROL add constraint TAX_RECORD_CONTROL_VERSION_FK foreign key (VERSION_ID) references VERSION (VERSION_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_RECORD_CONTROL add constraint TAX_RECORD_CONTROL_VINTAGE_FK foreign key (VINTAGE_ID) references VINTAGE (VINTAGE_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_RECORD_CONTROL add constraint TAX_RECORD_CONTROL_LAYER_FK foreign key (TAX_LAYER_ID) references TAX_LAYER (TAX_LAYER_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_RECORD_DOCUMENT add constraint TAX_RECORD_DOCUMENT_T_RECD_FK foreign key (TAX_RECORD_ID) references TAX_RECORD_CONTROL (TAX_RECORD_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_TRANSFER_CONTROL add constraint TAX_TRANSFER_CONTROL_T_RECD_FK foreign key (FROM_TRID) references TAX_RECORD_CONTROL (TAX_RECORD_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table TAX_YEAR_VERSION add constraint TAX_YEAR_VERSION_VERSION_FK foreign key (VERSION_ID) references VERSION (VERSION_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  Continuing...');
end;
/

begin
   execute immediate 'alter table VERSION add constraint VERSION_VERSION_TYPE_FK foreign key (VERSION_TYPE_ID) references VERSION_TYPE (VERSION_TYPE_ID)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign key already exists.  End of script.');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1252, 0, 10, 4, 2, 2, 38706, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.2_maint_038706_pwrtax_fkey_recreates.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
