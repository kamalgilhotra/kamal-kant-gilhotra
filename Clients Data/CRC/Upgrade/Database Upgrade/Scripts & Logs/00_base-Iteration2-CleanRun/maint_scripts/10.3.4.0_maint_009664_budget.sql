/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009664_budget.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   04/10/2012 Chris Mardis   Point Release
||============================================================================
*/

create table PP_REPORTS_DENOMINATION
(
 DENOMINATOR_ID number(22) not null,
 DESCRIPTION    varchar2(35),
 USER_ID        varchar2(18),
 TIME_STAMP     date
);

alter table PP_REPORTS_DENOMINATION
   add constraint PP_REPORTS_DENOMINATION_PK
       primary key (DENOMINATOR_ID)
       using index tablespace PWRPLANT_IDX;

insert into PP_REPORTS_DENOMINATION (DENOMINATOR_ID, DESCRIPTION) values (1, '$''s');
insert into PP_REPORTS_DENOMINATION (DENOMINATOR_ID, DESCRIPTION) values (1000, '$000''s');
insert into PP_REPORTS_DENOMINATION (DENOMINATOR_ID, DESCRIPTION) values (1000000, '$000,000''s');

create global temporary table TEMP_DENOMINATION
(
 DENOMINATOR_ID number(22)
) on commit preserve rows;

alter table TEMP_DENOMINATION
   add constraint TEMP_DENOMINATION_PK
       primary key (DENOMINATOR_ID);

update PP_REPORTS
   set SPECIAL_NOTE = SPECIAL_NOTE || ', SELECT DENOMINATION'
 where (DATAWINDOW like 'dw_fp_bdg_%' or DATAWINDOW like 'dw_bdg_bi_%');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (112, 0, 10, 3, 4, 0, 9664, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009664_budget.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
