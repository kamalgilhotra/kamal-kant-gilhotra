/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045159_sys_add_comments_ddl.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/29/2015 Andrew Scott   Add missing table comments for tables, cols
||                                      added since 2015.1.
||============================================================================
*/

comment on table ls_forecast_ilr_master is 'The LS Forecast ILR Master table contains the master data for ILRs being converted on the forecast version.';
comment on column ls_forecast_ilr_master.revision is 'System assigned identifier of the lease forecast version.';
comment on column ls_forecast_ilr_master.ilr_id is 'System assigned identifier of the ILR.';
comment on column ls_forecast_ilr_master.from_cap_type_id is 'System assigned identifier of the Capital/Operating treatment of the lease.';
comment on column ls_forecast_ilr_master.to_cap_type_id is 'System assigned identifier of the Capital/Operating treatment... ';
comment on column ls_forecast_ilr_master.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_forecast_ilr_master.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

comment on table ls_forecast_default_cap_types is 'The LS Forecast Default Cap Types table determins the default Cap Type to which each ILR will transition for a forecast version.';
comment on column ls_forecast_default_cap_types.revision is 'System assigned identifier of the lease forecast version.';
comment on column ls_forecast_default_cap_types.from_cap_type_id is 'System assigned identifier of the Capital/Operating treatment of the lease; this field holds the existing value so that it can be converted to another value in the forecast.';
comment on column ls_forecast_default_cap_types.to_cap_type_id is 'System assigned identifier of the Capital/Operating treatment of the lease; this field contains the default conversion value when transitioning records from existing capital types to new capital types that correspond to the new lease accounting standards.';
comment on column ls_forecast_default_cap_types.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_forecast_default_cap_types.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN rwip_closeout_stag_woe.asset_id IS 'Asset ID from the work order estimate, if applicable. When this is populated, the depreciation group for the RWIP allocation will be founding using the GL Account from the asset header rather than the Unitized GL Account from the work order.';

comment on column pt_import_preallo.ld_description_1 is 'Description 1 on the ledger detail record.';
comment on column pt_import_preallo.ld_description_2 is 'Description 2 on the ledger detail record.';
comment on column pt_import_preallo.ld_description_3 is 'Description 3 on the ledger detail record.';
comment on column pt_import_preallo.ld_unit_number is 'Unit number on the ledger detail record.';
comment on column pt_import_preallo.ld_serial_number is 'Serial number on the ledger detail record.';
comment on column pt_import_preallo.ld_make is 'Make on the ledger detail record.';
comment on column pt_import_preallo.ld_model is 'Model on the ledger detail record.';
comment on column pt_import_preallo.ld_model_year is 'Model year on the ledger detail record.';
comment on column pt_import_preallo.ld_body_type is 'Body type on the ledger detail record.';
comment on column pt_import_preallo.ld_fuel_type is 'Fuel type on the ledger detail record.';
comment on column pt_import_preallo.ld_gross_weight is 'Gross weight on the ledger detail record.';
comment on column pt_import_preallo.ld_license_number is 'License number on the ledger detail record.';
comment on column pt_import_preallo.ld_license_weight is 'License weight on the ledger detail record.';
comment on column pt_import_preallo.ld_license_expiration is 'License expiration on the ledger detail record.';
comment on column pt_import_preallo.ld_key_number is 'Key number on the ledger detail record.';
comment on column pt_import_preallo.ld_primary_driver is 'Primary driver on the ledger detail record.';
comment on column pt_import_preallo.ld_group_code is 'Group code on the ledger detail record.';
comment on column pt_import_preallo.ld_length is 'Length on the ledger detail record.';
comment on column pt_import_preallo.ld_width is 'Width on the ledger detail record.';
comment on column pt_import_preallo.ld_height is 'Height on the ledger detail record.';
comment on column pt_import_preallo.ld_construction_type is 'Construction type on the ledger detail record.';
comment on column pt_import_preallo.ld_stories is 'Stories on the ledger detail record.';
comment on column pt_import_preallo.ld_source_system is 'Source system on the ledger detail record.';
comment on column pt_import_preallo.ld_external_code is 'External code on the ledger detail record.';
comment on column pt_import_preallo.ld_lease_number is 'Lease number on the ledger detail record.';
comment on column pt_import_preallo.ld_lease_start is 'Lease start on the ledger detail record.';
comment on column pt_import_preallo.ld_lease_end is 'Lease end on the ledger detail record.';
comment on column pt_import_preallo.ld_notes is 'User input freeform notes for the ledger detail record.';
comment on column pt_import_preallo.ledger_detail_id is 'System-assigned identifier of a particular ledger detail item.';
comment on column pt_import_preallo.ledger_detail_xlate is 'Value from the import file translated to obtain the ledger_detail_id.';

comment on column INCREMENTAL_HIST_LAYER.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column INCREMENTAL_HIST_LAYER.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column INCREMENTAL_HIST_LAYER_ASSETS.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column INCREMENTAL_HIST_LAYER_ASSETS.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column INCREMENTAL_HIST_LAYER_DATA.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column INCREMENTAL_HIST_LAYER_DATA.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_INT_FILE_EXPORT.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_INT_FILE_EXPORT.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column PP_INT_FILE_EXPORT_COL.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_INT_FILE_EXPORT_COL.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column PP_INT_FILE_IMPORT.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_INT_FILE_IMPORT.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column PP_INT_FILE_IMPORT_COL.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_INT_FILE_IMPORT_COL.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column PP_INT_XML_EXPORT.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_INT_XML_EXPORT.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column PP_INT_XML_EXPORT_COL.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_INT_XML_EXPORT_COL.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column PP_INT_XML_IMPORT.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_INT_XML_IMPORT.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column PP_INT_XML_IMPORT_COL.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PP_INT_XML_IMPORT_COL.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_INC_HIST_LAYER_TRANS.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column REG_INC_HIST_LAYER_TRANS.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

comment on column TAX_DEPR_TRANSFER_AUDIT.vintage is 'Vintage of the tax record.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2952, 0, 2015, 2, 0, 0, 45159, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045159_sys_add_comments_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
