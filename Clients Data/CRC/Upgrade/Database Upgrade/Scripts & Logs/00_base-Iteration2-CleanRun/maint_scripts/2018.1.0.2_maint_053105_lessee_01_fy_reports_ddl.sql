/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053105_lessee_01_fy_reports_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.1.0.2 02/21/2019 C. Yura        add new field for fixed/fiscal year mat analysis report
||============================================================================
*/


BEGIN
   EXECUTE IMMEDIATE 'alter table ls_ilr_npv_fx_temp add in_year_fy number(22,0)';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/

comment on column ls_ilr_npv_fx_temp.in_year_fy is 'Year to display amount on Lessee 2004a report';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15302, 0, 2018, 1, 0, 2, 53105, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.2_maint_053105_lessee_01_fy_reports_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;