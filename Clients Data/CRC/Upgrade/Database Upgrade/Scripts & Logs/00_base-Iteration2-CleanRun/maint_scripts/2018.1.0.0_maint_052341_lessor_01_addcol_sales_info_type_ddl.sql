/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052341_lessor_01_addcol_sales_info_type_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 11/05/2018 Anand R        PP-52341 Add remeasure_month_fixed_payment to sales info type object
||============================================================================
*/

drop type LSR_ILR_SALES_SCH_INFO_TAB
/
drop type T_LSR_ILR_SALES_DF_PRELIMS
/

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_INFO AS OBJECT (carrying_cost NUMBER,
                                                         carrying_cost_company_curr number,
                                                         fair_market_value NUMBER,
                                                         fair_market_value_company_curr number,
                                                         guaranteed_residual NUMBER,
                                                         estimated_residual NUMBER,
                                                         days_in_year NUMBER,
                                                         purchase_option_amount NUMBER,
                                                         termination_amount NUMBER,
                                                         remeasurement_date date,
                                                         investment_amount number,
                                                         original_profit_loss number,
                                                         new_beg_receivable number,
                                                         prior_end_unguaran_residual number,
                                                         remeasure_month_fixed_payment number)
/                      
                                                         
CREATE OR REPLACE TYPE T_LSR_ILR_SALES_DF_PRELIMS AS OBJECT ( payment_info lsr_ilr_op_sch_pay_info_tab,
                                                              rates t_lsr_ilr_schedule_all_rates,
                                                              initial_direct_costs lsr_init_direct_cost_info_tab,
                                                              sales_type_info lsr_ilr_sales_sch_info)
/                        
                                                         
create type LSR_ILR_SALES_SCH_INFO_TAB as table of LSR_ILR_SALES_SCH_INFO
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(11202, 0, 2018, 1, 0, 0, 52341, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052341_lessor_01_addcol_sales_info_type_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 