/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038215_pwrtax_rpts_conv6.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 08/15/2014 Anand Rajashekar    script changes to for report conversion
||============================================================================
*/

--Report 216 - 402017

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (80, TO_DATE('12-AUG-14', 'DD-MON-RR'), 'PWRPLANT', 'PowerTax - Deferred Tax', null,
    'uo_tax_selecttabs_rpts_rollup_cons');

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (402017, 'PWRPLANT', TO_DATE('13-AUG-14', 'DD-MON-RR'), 'Deferred Tax Recovery by Type', ' ', 'PowerTax',
    'dw_tax_rpt_def_tax_recov_type', 'dfit', 'PowerTax_Rollup', null, 'PwrTax - 216', null, 'DETAIL', null, 1, 129, 52,
    80, 1, 3, null, null, null, null, null, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (189, 'Normalization Schema', 'dw', null, null, 'dw_tax_normalization_schema_filter', 'normalization_id',
    'description', 'N', 0, 0, null, null, 'PWRPLANT', TO_DATE('14-AUG-14', 'DD-MON-RR'), 1, 0, 0);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (80, 102, 'PWRPLANT', TO_DATE('12-AUG-14', 'DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (80, 103, 'PWRPLANT', TO_DATE('12-AUG-14', 'DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (80, 157, 'PWRPLANT', TO_DATE('12-AUG-14', 'DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (80, 189, 'PWRPLANT', TO_DATE('14-AUG-14', 'DD-MON-RR'));

--Report 37 - 400006

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (400006, 'PWRPLANT', TO_DATE('11-JUL-14', 'DD-MON-RR'), 'Gain/Loss Compare Books 1',
    'Gain/Loss Compare Books 1. For Preference Items  By Vintage and Tax Class', 'PowerTax',
    'dw_tax_rpt_gain_loss_comp_rep1', 'TAX_BOOK_COMPARE                   ', ' ', null, 'PwrTax - 37', null, null, null,
    1, 29, 52, 68, 1, 3, null, null, null, null, null, 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1360, 0, 10, 4, 3, 0, 38215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038215_pwrtax_rpts_conv6.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
