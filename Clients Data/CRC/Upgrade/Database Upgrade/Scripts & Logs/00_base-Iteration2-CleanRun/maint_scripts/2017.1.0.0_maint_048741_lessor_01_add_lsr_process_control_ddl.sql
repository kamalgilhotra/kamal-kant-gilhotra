/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048741_lessor_01_add_lsr_process_control_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/19/2017 Jared Watkins    Create the Lessor Month End Process Control table
||============================================================================
*/

CREATE TABLE lsr_process_control (
  company_id        number(22,0)  not null,
  gl_posting_mo_yr  date          not null,
  time_stamp         date          null,
  user_id           varchar2(18)  null,
  open_next         date          null
);

ALTER TABLE lsr_process_control
ADD CONSTRAINT pk_lsr_process_control 
PRIMARY KEY (
  company_id,
  gl_posting_mo_yr
)
USING INDEX TABLESPACE pwrplant_idx;

ALTER TABLE lsr_process_control
ADD CONSTRAINT fk_lsr_process_company 
FOREIGN KEY (company_id) 
REFERENCES company_setup (company_id);

COMMENT ON TABLE lsr_process_control IS '(C)[06] The Lessor Process Control table supports navigation and control of monthly closing processes for Lessors. Also, the historical records provide an auditable record of closing routines.';

COMMENT ON COLUMN lsr_process_control.company_id IS 'System-assigned identifier of a particular company.';
COMMENT ON COLUMN lsr_process_control.gl_posting_mo_yr IS 'GL accounting period.';
COMMENT ON COLUMN lsr_process_control.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_process_control.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_process_control.open_next IS 'A timestamp indicating when the next month has been openned for a given company and month.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3722, 0, 2017, 1, 0, 0, 48741, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048741_lessor_01_add_lsr_process_control_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;