/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050181_lessor_05_remove_idc_import_column_from_ilr_import_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 04/05/2018 Andrew Hill       Remove IDC Column from ILR Import
||============================================================================
*/

ALTER TABLE lsr_import_ilr DROP COLUMN init_direct_cost_amt;
ALTER TABLE lsr_import_ilr_archive DROP COLUMN init_direct_cost_amt;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4280, 0, 2017, 3, 0, 0, 50181, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050181_lessor_05_remove_idc_import_column_from_ilr_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 