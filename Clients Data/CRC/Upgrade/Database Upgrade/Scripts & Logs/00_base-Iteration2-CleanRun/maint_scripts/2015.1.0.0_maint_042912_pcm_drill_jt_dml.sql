/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042912_pcm_drill_jt_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/18/2015 Alex P.         Add drill JT links to JT tree workspaces
||============================================================================
*/

update ppbase_workspace_links
set link_order = 4
where module = 'pcm'
  and workspace_identifier in ('fp_maint_job_tasks', 'wo_maint_job_tasks')
  and link_order = 3;
  
insert into ppbase_workspace_links( module, workspace_identifier, link_order, linked_window, linked_window_label, linked_window_opensheet )
values( 'pcm', 'fp_maint_job_tasks', 3, 'w_task_select_tabs', 'Search (Tasks)', 1);

insert into ppbase_workspace_links( module, workspace_identifier, link_order, linked_window, linked_window_label, linked_window_opensheet )
values( 'pcm', 'wo_maint_job_tasks', 3, 'w_task_select_tabs', 'Search (Tasks)', 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2309, 0, 2015, 1, 0, 0, 042912, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042912_pcm_drill_jt_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;