/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_043084_cwip_ocr_02_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   3/3/2015   Sunjin Cone      New tool for deleting WO OCR
||============================================================================
*/ 
 
insert into PP_CONVERSION_WINDOWS( WINDOW_ID ,
          DESCRIPTION ,
           LONG_DESCRIPTION ,
           WINDOW_NAME     )
select max(window_id) + 1, 'Delete WO OCR', 'Delete Original Cost Retirement Records in Proj Mang', 'w_wo_ocr_delete_list'
from PP_CONVERSION_WINDOWS;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2339, 0, 2015, 1, 0, 0, 43084, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043084_cwip_ocr_02_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;