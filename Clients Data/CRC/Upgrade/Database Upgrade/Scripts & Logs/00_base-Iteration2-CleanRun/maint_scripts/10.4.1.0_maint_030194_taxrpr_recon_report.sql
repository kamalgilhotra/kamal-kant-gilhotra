SET DEFINE OFF

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030194_taxrpr_recon_report.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ---------------------------------
|| 10.4.1.0   08/09/2013 Andrew Scott        New reconcilation report for Tax Repairs
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, TIME_OPTION, REPORT_NUMBER,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID)
   select MAX_ID + 1,
          'Repairs: Network Run Detail Results',
          'T&D Network Repairs:  Process Run Detailed Results In-Service Analysis by Batch',
          'dw_rpr_rpt_netwk_process_details',
          '40',
          'UOP - 2271',
          13,
          102,
          41,
          37,
          1,
          3
     from (select max(REPORT_ID) MAX_ID from PP_REPORTS);


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (505, 0, 10, 4, 1, 0, 30194, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030194_taxrpr_recon_report.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON