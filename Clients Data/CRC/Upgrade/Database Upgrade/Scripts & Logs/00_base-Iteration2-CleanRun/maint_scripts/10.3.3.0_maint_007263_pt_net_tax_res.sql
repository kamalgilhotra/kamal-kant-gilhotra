/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007263_pt_net_tax_res.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/28/2011 Andrew Scott   Point Release
||============================================================================
*/

----
----  Maint 7263 - New reserve method needed for net tax basis calculated reserve.
----
insert into PT_RESERVE_METHOD
   (RESERVE_METHOD_ID, DESCRIPTION)
values
   (6, 'Net Tax Calculated Reserve');

--**************************
-- Log the run of the script
--**************************
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (20, 0, 10, 3, 3, 0, 7263, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_007263_pt_net_tax_res.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
