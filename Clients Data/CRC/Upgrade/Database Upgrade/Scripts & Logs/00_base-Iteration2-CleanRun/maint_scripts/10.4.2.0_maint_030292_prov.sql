/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030292_prov.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/08/2014 Nathan Hollis
||============================================================================
*/

update PP_REPORTS set SUBSYSTEM = 'Tax Accruala' where REPORT_ID = 54530;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (845, 0, 10, 4, 2, 0, 30292, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_030292_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;