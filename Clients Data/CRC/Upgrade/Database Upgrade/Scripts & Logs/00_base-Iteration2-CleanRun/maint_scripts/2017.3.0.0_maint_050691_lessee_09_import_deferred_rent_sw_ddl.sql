/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050691_lessee_09_import_deferred_rent_sw_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 04/20/2018 Josh Sandler 	  Add deferred rent switch to import
||============================================================================
*/

alter table ls_import_ilr
add (deferred_rent_sw_xlate varchar2(35), deferred_rent_sw number(1));

COMMENT ON COLUMN ls_import_ilr.deferred_rent_sw_xlate IS 'Translation field for determining the prepaid/deferred switch.';
COMMENT ON COLUMN ls_import_ilr.deferred_rent_sw IS 'The prepaid/deferred switch.';



alter table ls_import_ilr_archive
add (deferred_rent_sw_xlate varchar2(35), deferred_rent_sw number(1));

COMMENT ON COLUMN ls_import_ilr_archive.deferred_rent_sw_xlate IS 'Translation field for determining the prepaid/deferred switch.';
COMMENT ON COLUMN ls_import_ilr_archive.deferred_rent_sw IS 'The prepaid/deferred switch.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4544, 0, 2017, 3, 0, 0, 50691, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050691_lessee_09_import_deferred_rent_sw_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;