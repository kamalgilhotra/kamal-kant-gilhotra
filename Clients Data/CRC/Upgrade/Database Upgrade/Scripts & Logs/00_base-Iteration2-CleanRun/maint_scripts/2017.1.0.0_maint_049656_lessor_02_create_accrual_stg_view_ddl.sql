/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049656_lessor_02_create_accrual_stg_view_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/13/2017 Josh Sandler     Create v_lsr_monthly_accr_stg_fx_vw
||============================================================================
*/

CREATE OR REPLACE VIEW v_lsr_monthly_accr_stg_fx_vw (
	accrual_id,
	accrual_type_id,
	ilr_id,
	gl_posting_mo_yr,
	set_of_books_id,
	amount,
	ls_cur_type,
	exchange_date,
	contract_currency_id,
	display_currency_id,
	company_currency_id,
	iso_code,
	currency_display_symbol
) AS
WITH
cur AS (
  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
    contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
  FROM currency contract_cur
  UNION
  SELECT 2, company_cur.currency_id,
    company_cur.currency_display_symbol, company_cur.iso_code, 0
  FROM currency company_cur
)
SELECT
acr.accrual_id,
acr.accrual_type_id,
acr.ilr_id,
gl_posting_mo_yr,
acr.set_of_books_id,
acr.amount * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) amount,
cur.ls_cur_type ls_cur_type,
cr.exchange_date,
lease.contract_currency_id,
cur.currency_id display_currency_id,
cs.currency_id company_currency_id,
cur.iso_code,
cur.currency_display_symbol
FROM lsr_monthly_accrual_stg acr
INNER JOIN lsr_ilr ilr
  on acr.ilr_id = ilr.ilr_id
INNER JOIN currency_schema cs
  ON ilr.company_id = cs.company_id
INNER JOIN lsr_lease lease
  ON ilr.lease_id = lease.lease_id
INNER JOIN cur
  ON (cur.ls_cur_type = 1 AND
      cur.currency_id = lease.contract_currency_id)
  OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
LEFT OUTER JOIN ls_lease_calculated_date_rates cr
  ON lease.contract_currency_id = cr.contract_currency_id
  AND cur.currency_id = cr.company_currency_id
  AND ilr.company_id = cr.company_id
  AND acr.gl_posting_mo_yr = cr.accounting_month
WHERE Nvl(cr.exchange_rate_type_id, 1) = 1
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3952, 0, 2017, 1, 0, 0, 49656, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049656_lessor_02_create_accrual_stg_view_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;