/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049514_lessor_02_create_invoice_tables_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/26/2017 JSisouphanh      Create invoice tables
||
||============================================================================
*/

create table LSR_INVOICE
(
 INVOICE_ID         number(22,0) not null,
 INVOICE_NUMBER     varchar2(35), 
 ILR_ID             number(22,0),
 GL_POSTING_MO_YR   date,
 INVOICE_AMOUNT     number(22,2),
 INVOICE_INTEREST   number(22,2),
 INVOICE_EXECUTORY  number(22,2),
 INVOICE_CONTINGENT number(22,2),
 TIME_STAMP         date,
 USER_ID            varchar2(18)
);

alter table LSR_INVOICE
   add constraint PK_LSR_INVOICE
       primary key (INVOICE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LSR_INVOICE
   add constraint FK_LSR_INVOICE_ILR
       foreign key (ILR_ID)
       references LSR_ILR(ILR_ID);

comment on table LSR_INVOICE is '(S) [06] The LSR Invoice table holds lessee invoice records. ';
comment on column LSR_INVOICE."INVOICE_ID" is 'A system generated unique identifier of a particular invoice.';
comment on column LSR_INVOICE."INVOICE_NUMBER" is 'A user assigned identifier of a particular invoice.';
comment on column LSR_INVOICE."ILR_ID" is 'System-assigned identifier of a specific Individual Lease Record.';
comment on column LSR_INVOICE."GL_POSTING_MO_YR" is 'The accounting month for the invoice.';
comment on column LSR_INVOICE."INVOICE_AMOUNT" is 'The total dollar amount of the invoice.';
comment on column LSR_INVOICE."INVOICE_INTEREST" is 'The amount of the invoice payment allocated interest.';
comment on column LSR_INVOICE."INVOICE_EXECUTORY" is 'The amount of the invoice payment allocated to executory costs.';
comment on column LSR_INVOICE."INVOICE_CONTINGENT" is 'The amount of the invoice payment allocated to contingent costs.';
comment on column LSR_INVOICE."TIME_STAMP" is 'Standard system-assigned timestamp used for audit purposes.';
comment on column LSR_INVOICE."USER_ID" is 'Standard system-assigned user id used for audit purposes.';

create sequence LSR_INVOICE_SEQ
   minvalue 1
   start with 1
   increment by 1
   cache 20;

create table LSR_INVOICE_LINE
(
 INVOICE_ID          number(22,0) not null,
 INVOICE_LINE_NUMBER number(22,0) not null,
 INVOICE_TYPE_ID     number(22,0) not null,
 AMOUNT              number(22,2),
 ADJUSTMENT_AMOUNT   number(22,2), 
 DESCRIPTION         varchar2(35),
 NOTES               varchar2(4000),
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);

alter table LSR_INVOICE_LINE
   add constraint PK_LSR_INVOICE_LINE
       primary key (INVOICE_ID, INVOICE_LINE_NUMBER, INVOICE_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LSR_INVOICE_LINE
   add constraint FK_LSR_INVOICE
       foreign key (INVOICE_ID)
       references LSR_INVOICE(INVOICE_ID);

alter table LSR_INVOICE_LINE
   add constraint FK_LSR_INVOICE_LINE_PAY_TYPE
       foreign key (INVOICE_TYPE_ID)
       references LS_PAYMENT_TYPE(PAYMENT_TYPE_ID);
	   
comment on table LSR_INVOICE_LINE is '(S) [06] The LSR Invoice Line table contains line level information for a given invoice. ';
comment on column LSR_INVOICE_LINE."INVOICE_ID" is 'A system generated identifier of a particular invoice.';
comment on column LSR_INVOICE_LINE."INVOICE_LINE_NUMBER" is 'A unique identifying number for a particular invoice line for an invoice.';
comment on column LSR_INVOICE_LINE."INVOICE_TYPE_ID" is 'The type of payment for an invoice line (interest, executory, principal etc).';
comment on column LSR_INVOICE_LINE."AMOUNT" is 'The dollar amount for the invoice line.';
comment on column LSR_INVOICE_LINE."ADJUSTMENT_AMOUNT" is 'The adjusted dollar amount for the invoice line.';
comment on column LSR_INVOICE_LINE."DESCRIPTION" is 'A description for the invoice line.';
comment on column LSR_INVOICE_LINE."NOTES" is 'A column for notes about a particular invoice line.';
comment on column LSR_INVOICE_LINE."TIME_STAMP" is 'Standard system-assigned timestamp used for audit purposes.';
comment on column LSR_INVOICE_LINE."USER_ID" is 'Standard system-assigned user id used for audit purposes.';

create table LSR_INVOICE_APPROVAL
(
 INVOICE_ID         number(22) not null,
 APPROVAL_TYPE_ID   number(22),
 APPROVAL_STATUS_ID number(22),
 APPROVER           varchar2(35),
 APPROVAL_DATE      date,
 REJECTED           number(22),
 INVOICE_APPROVAL_FLAG number(22),
 TIME_STAMP         date,
 USER_ID            varchar2(35)
);

alter table LSR_INVOICE_APPROVAL
  add constraint PK_LSR_INVOICE_APPROVAL
      primary key (INVOICE_ID)
      using index tablespace PWRPLANT_IDX;

alter table LSR_INVOICE_APPROVAL
  add constraint FK_LSR_INVOICE_APPROVAL
      foreign key (INVOICE_ID)
        references LSR_INVOICE (INVOICE_ID);

alter table LSR_INVOICE_APPROVAL
  add constraint FK_INVOICE_APPR_STATUS
      foreign key (APPROVAL_STATUS_ID)
        references APPROVAL_STATUS (APPROVAL_STATUS_ID);

comment on table LSR_INVOICE_APPROVAL is '(S) [06] The LSR Payment Approval table holds invoice approval statuses. ';		
comment on column LSR_INVOICE_APPROVAL."INVOICE_ID" is 'A system generated identifier of a particular invoice.';
comment on column LSR_INVOICE_APPROVAL."APPROVAL_TYPE_ID" is 'The approval type.';
comment on column LSR_INVOICE_APPROVAL."APPROVAL_STATUS_ID" is 'Whether or not the revision is initiated, pending approval, approved, or rejected.';
comment on column LSR_INVOICE_APPROVAL."APPROVER" is 'User or group approving.';
comment on column LSR_INVOICE_APPROVAL."APPROVAL_DATE" is 'Date approver approved.';
comment on column LSR_INVOICE_APPROVAL."REJECTED" is 'If the approver rejects the revision.';
comment on column LSR_INVOICE_APPROVAL."INVOICE_APPROVAL_FLAG" is 'If the approver rejects the revision.';
comment on column LSR_INVOICE_APPROVAL."TIME_STAMP" is 'Standard system-assigned timestamp used for audit purposes.';
comment on column LSR_INVOICE_APPROVAL."USER_ID" is 'Standard system-assigned user id used for audit purposes.';

comment on table LS_PAYMENT_TYPE is '(S) [06] Table used to match payment and invoice descriptions to their type IDs. Used for both payments and invoices. Payment Type ID 3-12 is Executory, 13-22 is Contingent.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3878, 0, 2017, 1, 0, 0, 49514, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049514_lessor_02_create_invoice_tables_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
