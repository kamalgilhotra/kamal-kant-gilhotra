/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011596_cr.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   12/28/2012 Joseph King    Point Release
||============================================================================
*/

-- * New PP_PROCESSES Record for cr_balances_bdg.exe
insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION, EXECUTABLE_FILE, VERSION, ALLOW_CONCURRENT,
    SYSTEM_LOCK_ENABLED, SYSTEM_LOCK, PREREQUISITE, PRIMARY_TABLES, FUNCTION_SUMMARY,
    TECHNICAL_SUMMARY)
   select NVL(max(PROCESS_ID), 0) + 1,
          'CR - Balances BDG',
          'CR - Balances BDG',
          'cr_balances_bdg.exe',
          '10.4.0.0',
          0,
          0,
          0,
          'CR Balances must be initialized and build up through the month that budgets have been updated.  If CR Balances is not built, this interface will not run.',
          'CR_BUDGET_DATA, CR_BALANCES_BDG, CR_BALANCES',
          'Maintains the Budget Balances table for the selected budget versions.',
          'Maintains the Budget Balances table by rolling forward the prior month (from cr_balances_bdg or cr_balances -- only for the first actual month) and then incrementally adding the new activity from CR_BALANCES.  Year-end processing for income statement accounts is applied in the event that retained earnings entries are not booked.'
     from PP_PROCESSES;

-- * control for the budget versions to process
insert into CR_ALLOC_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION)
   select *
     from (select max(CONTROL_ID) + 1,
                  'CR Balances BDG - Budget Version(s)',
                  '',
                  'The list of budget versions to be maintained in cr_balances_bdg.exe as a comma seperated list.'
             from CR_ALLOC_SYSTEM_CONTROL)
    where not exists
    (select 1
             from CR_ALLOC_SYSTEM_CONTROL
            where UPPER(trim(CONTROL_NAME)) = 'CR BALANCES BDG - BUDGET VERSION(S)');

-- * new table for budget year end criteria -- will by default use the same criteria that exist on the actual side
create table CR_BALANCES_BDG_YE_CLOSE as select * from CR_BALANCES_YE_CLOSE;

alter table CR_BALANCES_BDG_YE_CLOSE
   add constraint CR_BALANCES_BDG_YE_CLOSE_PK
       primary key (ROW_ID)
       using index tablespace PWRPLANT_IDX;

comment on table CR_BALANCES_BDG_YE_CLOSE is '(O) [03]
The CR Balances BDG YE Close table is used to define the income statement criteria when building the CR Balances BDG after year end.  The transactions to exclude when rolling the balances forward at the beginning of the year are defined in this table.';
comment on column CR_BALANCES_BDG_YE_CLOSE.ROW_ID is 'Unique id number for each row in a source criterion.';
comment on column CR_BALANCES_BDG_YE_CLOSE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column CR_BALANCES_BDG_YE_CLOSE.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column CR_BALANCES_BDG_YE_CLOSE.LEFT_PAREN is 'A left parenthesis ''('', if one is used in the source criteria.';
comment on column CR_BALANCES_BDG_YE_CLOSE.COLUMN_NAME is 'The accounting key element for the source criteria.';
comment on column CR_BALANCES_BDG_YE_CLOSE.OPERATOR is 'A logical operator (=, <>, in, not in, between, not between, etc).';
comment on column CR_BALANCES_BDG_YE_CLOSE.VALUE1 is 'The source criteria value, applied against the column name using the operator.';
comment on column CR_BALANCES_BDG_YE_CLOSE.BETWEEN_AND is '"And" if using "between" or "not between" as the operator.';
comment on column CR_BALANCES_BDG_YE_CLOSE.VALUE2 is 'The second value of the source criteria if using "between" or "not between" as the operator.';
comment on column CR_BALANCES_BDG_YE_CLOSE.AND_OR is '"And" or "or" identifying the relationship between multiple lines in the source criteria.';
comment on column CR_BALANCES_BDG_YE_CLOSE.RIGHT_PAREN is 'A right parenthesis '')'', if one is used in the source criteria.';
comment on column CR_BALANCES_BDG_YE_CLOSE.STRUCTURE_ID is 'The structure_id if value1 was selected from a structure.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (269, 0, 10, 4, 0, 0, 11596, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011596_cr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;