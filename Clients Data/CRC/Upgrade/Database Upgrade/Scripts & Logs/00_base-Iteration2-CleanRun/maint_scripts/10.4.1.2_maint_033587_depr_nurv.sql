/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033587_depr_nurv.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.2   10/25/2013 Kyle Peterson
||============================================================================
*/

alter table CPR_DEPR_CALC_STG add NURV_ADJ number(22,2);

comment on column CPR_DEPR_CALC_STG.NURV_ADJ is 'The NURV adjustment for the asset.';

create global temporary table CPR_DEPR_CALC_NURV_STG
(
 ASSET_ID          number(22,0) not null,
 SET_OF_BOOKS_ID   number(22,0) not null,
 GL_POSTING_MO_YR  date not null,
 WORK_ORDER_NUMBER varchar2(35),
 COST              number(22,2)
) on commit preserve rows;

alter table CPR_DEPR_CALC_NURV_STG
   add constraint PK_CPR_DEPR_CALC_NURV_STG
       primary key (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR);

comment on table CPR_DEPR_CALC_NURV_STG is '(T)  [01] A temp table to calculate non-unitized reversals for depreciation.';
comment on column CPR_DEPR_CALC_NURV_STG.ASSET_ID is 'An internal ID to the CPR.';
comment on column CPR_DEPR_CALC_NURV_STG.SET_OF_BOOKS_ID is 'The set of books.';
comment on column CPR_DEPR_CALC_NURV_STG.GL_POSTING_MO_YR is 'The accounting month.';
comment on column CPR_DEPR_CALC_NURV_STG.WORK_ORDER_NUMBER is 'The work order number of the NURV activity.';
comment on column CPR_DEPR_CALC_NURV_STG.COST is 'The cost of the NURV activity on the CPR.';

create global temporary table CPR_DEPR_CALC_CFNU_STG
(
 ASSET_ID          number(22,0),
 SET_OF_BOOKS_ID   number(22,0),
 GL_POSTING_MO_YR  date,
 WORK_ORDER_NUMBER varchar2(35),
 COST              number(22,2),
 ADJ               number(22,2)
) on commit preserve rows;

alter table CPR_DEPR_CALC_CFNU_STG
   add constraint PK_CPR_DEPR_CALC_CFNU_STG
       primary key (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR);

comment on table CPR_DEPR_CALC_CFNU_STG is '(T)  [01] A temp table to calculate non-unitized reversals for depreciation.';
comment on column CPR_DEPR_CALC_CFNU_STG.ASSET_ID is 'An internal ID to the CPR.';
comment on column CPR_DEPR_CALC_CFNU_STG.SET_OF_BOOKS_ID is 'The set of books.';
comment on column CPR_DEPR_CALC_CFNU_STG.GL_POSTING_MO_YR is 'The accounting month.';
comment on column CPR_DEPR_CALC_CFNU_STG.WORK_ORDER_NUMBER is 'The work order number of the CFNU activity.';
comment on column CPR_DEPR_CALC_CFNU_STG.COST is 'The cost of the asset on the CPR.';
comment on column CPR_DEPR_CALC_CFNU_STG.ADJ is 'The NURV adjustment for the assets.';

alter table CPR_DEPR_CALC_STG_ARC add NURV_ADJ number(22,2);

comment on column CPR_DEPR_CALC_STG_ARC.NURV_ADJ is 'The NURV adjustment for the asset.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (710, 0, 10, 4, 1, 2, 33587, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_033587_depr_nurv.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;