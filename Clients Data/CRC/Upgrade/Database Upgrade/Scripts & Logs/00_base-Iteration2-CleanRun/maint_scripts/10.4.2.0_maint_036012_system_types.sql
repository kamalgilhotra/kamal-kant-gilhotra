/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036012_system_types.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/27/2014 Chris Mardis
||============================================================================
*/

create type date_sort_table_t is table of date;
/
create type number_sort_table_t is table of number;
/
create type timestamp_sort_table_t is table of timestamp;
/
create type varchar2_sort_table_t is table of varchar(32767);
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (923, 0, 10, 4, 2, 0, 36012, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036012_system_types.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;