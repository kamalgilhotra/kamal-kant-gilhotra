/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039025_lease_create_recon_type_table_1.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 07/30/2014 Daniel Motter    New table holds payment recon types
||============================================================================
*/

/* Create reconcile type table */
create table LS_RECONCILE_TYPE
(
 LS_RECONCILE_TYPE_ID  NUMBER(22,0),
 DESCRIPTION           VARCHAR2(35),
 USER_ID               VARCHAR2(18),
 TIME_STAMP            DATE
);

alter table LS_RECONCILE_TYPE
   add constraint PK_LS_RECONCILE_TYPE
       primary key (LS_RECONCILE_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1302, 0, 10, 4, 3, 0, 39025, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039025_lease_create_recon_type_table_1.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
