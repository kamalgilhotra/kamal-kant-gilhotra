/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044124_reg_fix_wksp_label_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/16/2015 Sarah Byers    Fix wksp label for Create Layer Depr Adj
||============================================================================
*/

update ppbase_workspace
	set label = 'Create - Depr Adjustment',
		 minihelp = 'Create Layer - Depr Adjustment'
 where workspace_identifier = 'uo_reg_fcst_depr_inc_adj_ws';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2599, 0, 2015, 2, 0, 0, 044124, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044124_reg_fix_wksp_label_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;