/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051580_lessee_02_add_payment_escalations_import_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/18/2018 Josh Sandler     Add escalations to ILR Import
||============================================================================
*/

insert into pp_import_lookup(import_lookup_id, description, long_description,
  column_name, lookup_sql,
  is_derived, lookup_table_name, lookup_column_name)
values(1118, 'LS_VARIABLE_PAYMENT.description', 'The passed in value corresponds to Variable Payment description field for escalation type VPs',
  'variable_payment_id', '( select b.variable_payment_id from ls_variable_payment b where escalation_sw = 1 and upper( trim( <importfield> ) ) = upper( trim( b.description ) ) )',
  0, 'ls_variable_payment', 'description')
;

insert into pp_import_lookup(import_lookup_id, description, long_description,
  column_name, lookup_sql,
  is_derived, lookup_table_name, lookup_column_name)
values(1119, 'LS_VARIABLE_PAYMENT.long_description', 'The passed in value corresponds to Variable Payment long description field for escalation type VPs',
  'variable_payment_id', '( select b.variable_payment_id from ls_variable_payment b where escalation_sw = 1 and upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )',
  0, 'ls_variable_payment', 'long_description')
;

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (252,
             'escalation',
             'Escalation',
             'escalation_xlate',
             0,
             1,
             'number(22)',
             'ls_variable_payment',
             1,
             'variable_payment_id');

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (252,
             'escalation',
             1118);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (252,
             'escalation',
             1119);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (252,
             'escalation_freq_id',
             'Escalation Frequency',
             'escalation_freq_xlate',
             0,
             1,
             'number(22)',
             'ls_payment_freq',
             1,
             'payment_freq_id');

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (252,
             'escalation_freq_id',
             1047);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (252,
             'escalation_freq_id',
             1064);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             is_required,
             processing_order,
             column_type,
             is_on_table)
VALUES      (252,
             'escalation_pct',
             'Escalation Percent',
             0,
             1,
             'number(22,12)',
             1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6763, 0, 2017, 4, 0, 0, 51580, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051580_lessee_02_add_payment_escalations_import_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;