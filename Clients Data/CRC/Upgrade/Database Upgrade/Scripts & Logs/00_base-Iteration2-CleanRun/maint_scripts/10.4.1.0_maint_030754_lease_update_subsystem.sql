/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030754_lease_update_subsystem.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   07/12/2013 Matthew Mikulka Point Release
||============================================================================
*/

--Update WORKFLOW_SUBSYSTEM id_field2

update WORKFLOW_SUBSYSTEM
   set FIELD2_SQL = '<<id_field2>>'
 where SUBSYSTEM in ('mla_approval', 'ilr_approval');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (478, 0, 10, 4, 1, 0, 30754, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030754_lease_update_subsystem.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
