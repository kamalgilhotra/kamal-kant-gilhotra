 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048897_lessor_02_mla_approval_dml.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 10/02/2017 Shane "C" Ward  New Workflows for Lessor, update Lessee Workflows
 ||
 ||============================================================================
 */
 
 declare
  PPCMSG varchar2(10) := 'PPC-MSG> ';
  PPCERR varchar2(10) := 'PPC-ERR> ';
  PPCSQL varchar2(10) := 'PPC-SQL> ';
  PPCORA varchar2(10) := 'PPC-ORA' || '-> ';

  LL_BAD_COUNT number;
  -- change value of LB_SKIP_CHECK to TRUE if you determine there is data that is OK but will always
  -- show up as an exception.
  LB_SKIP_CHECK boolean := false;
begin
  DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

	if not LB_SKIP_CHECK then
		--Find workflows with ilr_approval, mla_approval, or both and check that they have lengths less than the max without our new additions
		select count(1) into ll_bad_count from 
			(select 1 from workflow_type where (subsystem like '%mla_approval%' and length(subsystem) > 53)
											or	(subsystem like '%ilr_approval%' and length(subsystem) > 53)
											or (subsystem like '%ilr_approval%' and subsystem like '%mla_approval%' and length (subsystem) > 46));
		
		if LL_BAD_COUNT > 0 then
		  DBMS_OUTPUT.PUT_LINE(PPCORA || 'ORA' ||
							  '-02000 - Contact PPC support for help.  Read message below.');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  Please edit the script and read the comments at ');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  the bottom to see how to evaluate the data issues.');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  To skip checking this in the future.');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  change this line: LB_SKIP_CHECK boolean := false;');
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || '  to:               LB_SKIP_CHECK boolean := true;');

		  RAISE_APPLICATION_ERROR(-20000, 'Workflow Type issue, contact PPC support for help.');
		else
		  DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Workflow Type Setup is OK.');
		end if;
	else
		--No Validations
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Workflow Type check not performed.');
	end IF;
 
	--Update Lessee Workflows so they don't catch the new lessor approvals
	UPDATE workflow_type 
	SET subsystem = REPLACE(subsystem, 'mla_approval', 'lessee_mla_approval')
	WHERE Lower(subsystem) LIKE '%mla_approval%'
	;

	UPDATE workflow_type
	SET subsystem = REPLACE(subsystem, 'ilr_approval', 'lessee_ilr_approval')
	WHERE Lower(subsystem) LIKE '%ilr_approval%';

	INSERT INTO WORKFLOW_SUBSYSTEM
				(subsystem,
				 field1_desc,
				 field2_desc,
				 field1_sql,
				 field2_sql,
				 pending_notification_type,
				 approved_notification_type,
				 rejected_notification_type,
				 send_sql,
				 approve_sql,
				 reject_sql,
				 unreject_sql,
				 unsend_sql,
				 update_workflow_type_sql,
				 send_emails)
	VALUES      ('lessor_mla_approval',
				 'MLA Number',
				 'Revision',
				 'select lease_number from lsr_lease where lease_id = <<id_field1>>',
				 '<<id_field2>>',
				 'MLA APPROVAL MESSAGE',
				 'MLA APPROVED NOTICE MESSAGE',
				 'MLA REJECTION MESSAGE',
				 'select PKG_LESSOR_APPROVAL.F_SEND_MLA(<<id_field1>>, <<id_field2>>) from dual',
				 'select PKG_LESSOR_APPROVAL.F_APPROVE_MLA(<<id_field1>>, <<id_field2>>) from dual',
				 'select PKG_LESSOR_APPROVAL.F_REJECT_MLA(<<id_field1>>, <<id_field2>>) from dual',
				 'select PKG_LESSOR_APPROVAL.F_UNREJECT_MLA(<<id_field1>>, <<id_field2>>) from dual',
				 'select PKG_LESSOR_APPROVAL.F_UNSEND_MLA(<<id_field1>>, <<id_field2>>) from dual',
				 'select PKG_LESSOR_APPROVAL.F_UPDATE_WORKFLOW_MLA(<<id_field1>>, <<id_field2>>) from dual',
				 1);

end;
/
				 
/*
THIS VALIDATION MUST PASS. Without this script Lease Workflows will not work.

As part of the new Lessor Subsystem for Lease, we have updated the subsystem that PowerPlan will 
search for for Lessee Approvals to be specific to Lessee (lessee_xxx_approval instead of xxx_approval)
The Subsystem column on workflow_type allows for multiple subsystems to be assigned to a single workflow. 
The validation checks the length of currently set up workflows in WORKFLOW_TYPE to be < 54 since we are 
adding 7 characters to the original descriptions

Breaking out Consolidated workflows in workflow_type will allow this script to run.
*/
				 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
  (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
  (3754, 0, 2017, 1, 0, 0, 48897, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048897_lessor_02_mla_approval_dml.sql', 1,
  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
  