/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_042691_pwrtax_drop_arc_layer.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 10.4.3.3 02/05/2015 A Scott          drop tax layer id from arc tax record control table
||                                      (but it may not exist, so don't error).
||============================================================================
*/

SET SERVEROUTPUT ON

begin
   execute immediate 'alter table arc_tax_record_control drop column tax_layer_id';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('tax_layer_id does not exist on arc_tax_record_control. Continuing...');
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2251, 0, 10, 4, 3, 3, 42691, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.3_maint_042691_pwrtax_drop_arc_layer.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;