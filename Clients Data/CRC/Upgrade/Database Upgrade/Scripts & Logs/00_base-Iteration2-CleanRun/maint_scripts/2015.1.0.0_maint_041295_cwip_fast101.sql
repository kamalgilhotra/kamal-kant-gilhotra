/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041295_cwip_fast101.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 2015.1    11/17/2014 Sunjin Cone      Add more columns to Archive tables
||============================================================================
*/

alter table UNITIZE_WO_LIST_ARC add HAS_GAIN_LOSS_OCR      number(22,0);
alter table UNITIZE_WO_LIST_ARC add HAS_ELIG_ADDS          number(22,0);
alter table UNITIZE_WO_LIST_ARC add HAS_ELIG_RWIP          number(22,0); 
alter table UNITIZE_WO_LIST_ARC add MIN_RWIP_BATCH_UNIT_ID number(22,0);  
alter table UNITIZE_WO_LIST_ARC add WOA_FUNC_CLASS_ID      number (22,0);
alter table UNITIZE_WO_LIST_ARC add HAS_NET_ZERO_ADDS      number (22,0);
alter table UNITIZE_WO_LIST_ARC add HAS_NET_ZERO_RWIP      number (22,0);
alter table UNITIZE_WO_LIST_ARC add CREATE_NET_ZERO_ADD    number (22,0);
alter table UNITIZE_WO_LIST_ARC add CREATE_NET_ZERO_RWIP   number (22,0);
alter table UNITIZE_WO_LIST_ARC add NET_ZERO_UNIT_ASSET_ID number (22,0); 
alter table UNITIZE_WO_LIST_ARC add DB_SESSION_ID   number (22,0);
alter table UNITIZE_WO_LIST_ARC add CLOSING_TYPE  varchar2 (35);

comment on column UNITIZE_WO_LIST_ARC.HAS_GAIN_LOSS_OCR is 'This field has a 1 value if the work order has Original Cost Retirement with URGL or SAGL needing to be processed for WO Retirements during Unitization.';
comment on column UNITIZE_WO_LIST_ARC.HAS_ELIG_ADDS is 'This field has a 1 value if the work order has eligible Addition charges for Unitization.';
comment on column UNITIZE_WO_LIST_ARC.HAS_ELIG_RWIP is 'This field has a 1 value if the work order has eligible RWIP charges for Unitization.';
comment on column UNITIZE_WO_LIST_ARC.MIN_RWIP_BATCH_UNIT_ID is 'This field has the min(BATCH_UNIT_ITEM_ID) from CHARGE_GROUP_CONTROL for the first time RWIP charges unitized for the work order.';
comment on column UNITIZE_WO_LIST_ARC.WOA_FUNC_CLASS_ID is 'System-assigned optional identifier of a unique functional class assigned on the Work Order.';
comment on column UNITIZE_WO_LIST_ARC.HAS_NET_ZERO_ADDS is 'System-assigns a 1 value during net $0 unitization where 1 indicates net $0 Additions exist.';
comment on column UNITIZE_WO_LIST_ARC.HAS_NET_ZERO_RWIP is 'System-assigns a 1 value during net $0 unitization where 1 indicates net $0 RWIP exist.';
comment on column UNITIZE_WO_LIST_ARC.CREATE_NET_ZERO_ADD is 'System-assigns a 1 value during net $0 unitization where 1 indicates net $0 Additions exists and a $0 Unit Item needs to be created.';
comment on column UNITIZE_WO_LIST_ARC.CREATE_NET_ZERO_RWIP is 'System-assigns a 1 value during net $0 unitization where 1 indicates net $0 RWIP exists and a $0 Unit Item needs to be created.';
comment on column UNITIZE_WO_LIST_ARC.NET_ZERO_UNIT_ASSET_ID is 'The ASSET_ID used for the Unit Item created for net $0 unitization.'; 
comment on column UNITIZE_WO_LIST_ARC.DB_SESSION_ID is 'The database sessionid.'; 
comment on column UNITIZE_WO_LIST_ARC.CLOSING_TYPE is 'The type of filter used to start Auto Unitization:  BLANKETS, NON-BLANKETS, ALL, SINGLE WO'; 

alter table UNITIZE_ALLOC_CGC_INSERTS_TEMP add RATIO_USED varchar2 (35);
comment on column UNITIZE_ALLOC_CGC_INSERTS_TEMP.RATIO_USED is 'The ratio used from UNITIZE_RATIO_UNITS_TEMP table to generate the allocation charge groups: alloc_ratio_by_sub; alloc_ratio_by_ut; alloc_ratio (used for allocate remaining)'; 

alter table UNITIZE_ALLOC_CGC_INSERTS_ARC add RATIO_USED varchar2 (35);
comment on column UNITIZE_ALLOC_CGC_INSERTS_ARC.RATIO_USED is 'The ratio used from UNITIZE_RATIO_UNITS_TEMP table to generate the allocation charge groups: alloc_ratio_by_sub; alloc_ratio_by_ut; alloc_ratio (used for allocate remaining)'; 



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2044, 0, 2015, 1, 0, 0, 041295, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041295_cwip_fast101.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;