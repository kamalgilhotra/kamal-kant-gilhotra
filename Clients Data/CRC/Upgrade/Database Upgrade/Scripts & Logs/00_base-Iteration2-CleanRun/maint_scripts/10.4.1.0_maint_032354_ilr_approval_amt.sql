/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032354_ilr_approval_amt.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/12/2013 Ryan Oliveria  Point Release
||============================================================================
*/

update WORKFLOW_TYPE
   set SQL_APPROVAL_AMOUNT = 'select nvl(max(beg_obligation),0) from ls_ilr_schedule
where ilr_id = <<id_field1>> and revision = <<id_field2>>
and month = (select min(month) from ls_ilr_schedule
where ilr_id = <<id_field1>> and revision = <<id_field2>>)'
 where WORKFLOW_TYPE_ID = 10;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (609, 0, 10, 4, 1, 0, 32354, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032354_ilr_approval_amt.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;