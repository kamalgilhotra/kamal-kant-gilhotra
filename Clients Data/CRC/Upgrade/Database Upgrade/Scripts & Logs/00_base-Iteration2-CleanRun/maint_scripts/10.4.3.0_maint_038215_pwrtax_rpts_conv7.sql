/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038215_pwrtax_rpts_conv7.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 08/18/2014 Anand Rajashekar    script changes to for report conversion
||============================================================================
*/

--Report 248
insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (82, TO_DATE('18-AUG-14', 'DD-MON-RR'), 'PWRPLANT', 'PowerTax - Deferred Tax', null,
    'uo_tax_selecttabs_rpts_rollup_cons');

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (402020, 'PWRPLANT', TO_DATE('18-AUG-14', 'DD-MON-RR'), 'FAS109 Beginning Balance Check', ' ', 'PowerTax',
    'dw_tax_rpt_fas109_beg_total', 'dfit', 'PowerTax_Rollup', null, 'PwrTax - 248', null, 'DETAIL', null, 1, 129, 52, 82,
    1, 3, null, null, null, null, null, 0);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (82, 101, 'PWRPLANT', TO_DATE('18-AUG-14', 'DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (82, 102, 'PWRPLANT', TO_DATE('18-AUG-14', 'DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (82, 103, 'PWRPLANT', TO_DATE('18-AUG-14', 'DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (82, 110, 'PWRPLANT', TO_DATE('18-AUG-14', 'DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (82, 189, 'PWRPLANT', TO_DATE('18-AUG-14', 'DD-MON-RR'));


--Report 257
insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (81, TO_DATE('17-AUG-14', 'DD-MON-RR'), 'PWRPLANT', 'PowerTax - Deferred Tax', null,
    'uo_tax_selecttabs_rpts_rollup_cons');

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (402023, 'PWRPLANT', TO_DATE('17-AUG-14', 'DD-MON-RR'), 'FAS109  by type', ' ', 'PowerTax',
    'dw_tax_rpt_dfit_fas109_type', 'dfit', 'PowerTax_Rollup', null, 'PwrTax - 257', null, 'DETAIL', null, 1, 129, 52, 81,
    1, 3, null, null, null, null, null, 0);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (81, 101, 'PWRPLANT', TO_DATE('17-AUG-14', 'DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (81, 102, 'PWRPLANT', TO_DATE('17-AUG-14', 'DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (81, 103, 'PWRPLANT', TO_DATE('17-AUG-14', 'DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (81, 106, 'PWRPLANT', TO_DATE('17-AUG-14', 'DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (81, 110, 'PWRPLANT', TO_DATE('18-AUG-14', 'DD-MON-RR'));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1361, 0, 10, 4, 3, 0, 38215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038215_pwrtax_rpts_conv7.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
