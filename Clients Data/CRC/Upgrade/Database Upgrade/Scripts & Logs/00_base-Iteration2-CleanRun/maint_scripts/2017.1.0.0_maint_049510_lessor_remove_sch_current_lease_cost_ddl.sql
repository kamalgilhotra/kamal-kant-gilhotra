/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049510_lessor_remove_sch_current_lease_cost_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/31/2017 Andrew Hill      Remove current lease cost from lsr_ilr_schedule
||============================================================================
*/

ALTER TABLE lsr_ilr_schedule DROP COLUMN current_lease_cost;

DROP MATERIALIZED VIEW mv_lsr_ilr_mc_schedule_amounts;

CREATE MATERIALIZED VIEW mv_lsr_ilr_mc_schedule_amounts
BUILD IMMEDIATE REFRESH FAST ON COMMIT AS 
SELECT  schedule.ilr_id,
        ilr.ilr_number,
        ilr.current_revision,
        schedule.revision,
        schedule.set_of_books_id,
        schedule.month,
        schedule.interest_income_received,
        schedule.interest_income_accrued,
        schedule.beg_deferred_rev,
        schedule.deferred_rev_activity,
        schedule.end_deferred_rev,
        schedule.beg_receivable,
        schedule.end_receivable,
        schedule.executory_accrual1,
        schedule.executory_accrual2,
        schedule.executory_accrual3,
        schedule.executory_accrual4,
        schedule.executory_accrual5,
        schedule.executory_accrual6,
        schedule.executory_accrual7,
        schedule.executory_accrual8,
        schedule.executory_accrual9,
        schedule.executory_accrual10,
        schedule.executory_paid1,
        schedule.executory_paid2,
        schedule.executory_paid3,
        schedule.executory_paid4,
        schedule.executory_paid5,
        schedule.executory_paid6,
        schedule.executory_paid7,
        schedule.executory_paid8,
        schedule.executory_paid9,
        schedule.executory_paid10,
        schedule.contingent_accrual1,
        schedule.contingent_accrual2,
        schedule.contingent_accrual3,
        schedule.contingent_accrual4,
        schedule.contingent_accrual5,
        schedule.contingent_accrual6,
        schedule.contingent_accrual7,
        schedule.contingent_accrual8,
        schedule.contingent_accrual9,
        schedule.contingent_accrual10,
        schedule.contingent_paid1,
        schedule.contingent_paid2,
        schedule.contingent_paid3,
        schedule.contingent_paid4,
        schedule.contingent_paid5,
        schedule.contingent_paid6,
        schedule.contingent_paid7,
        schedule.contingent_paid8,
        schedule.contingent_paid9,
        schedule.contingent_paid10,
        st_schedule.principal_received,
        st_schedule.principal_accrued,
        st_schedule.beg_long_term_receivable,
        st_schedule.end_long_term_receivable,
        st_schedule.beg_unguaranteed_residual,
        st_schedule.interest_unguaranteed_residual,
        st_schedule.ending_unguaranteed_residual,
        st_schedule.beg_net_investment,
        st_schedule.interest_net_investment,
        st_schedule.ending_net_investment,
        ilr.lease_id,
        ilr.company_id,
        options.in_service_exchange_rate,
        options.purchase_option_amt,
        options.termination_amt,
        schedule.ROWID AS lisrowid,
        options.ROWID AS optrowid,
        ilr.ROWID AS ilrrowid,
        lease.rowid as leaserowid,
        st_schedule.ROWID AS salesrowid
FROM  lsr_ilr_schedule schedule,
      lsr_ilr_options options,
      lsr_ilr ilr,
      lsr_lease lease,
      lsr_ilr_schedule_sales_direct st_schedule
WHERE schedule.ilr_id = options.ilr_id
AND schedule.revision = options.revision
AND schedule.ilr_id = ilr.ilr_id
AND ilr.lease_id = lease.lease_id
AND st_schedule.ilr_id (+) = schedule.ilr_id
AND st_schedule.revision (+) = schedule.revision
AND st_schedule.month (+) = schedule.month
AND st_schedule.set_of_books_id (+) = schedule.set_of_books_id;

CREATE INDEX mv_lsr_ilr_mc_sch_amt_idx ON mv_lsr_ilr_mc_schedule_amounts(ilr_id, revision, MONTH, set_of_books_id) tablespace pwrplant_idx;
CREATE INDEX mv_lsr_ilr_mc_sch_amt_mth_idx ON mv_lsr_ilr_mc_schedule_amounts(MONTH) TABLESPACE pwrplant_idx;
CREATE INDEX mv_lsr_ilr_mc_sch_amt_cl_idx ON mv_lsr_ilr_mc_schedule_amounts(company_id, lease_id) TABLESPACE pwrplant_idx;
CREATE INDEX mv_lsr_ilr_mc_sch_amt_ls_idx ON mv_lsr_ilr_mc_schedule_amounts(lease_id) TABLESPACE pwrplant_idx;
CREATE INDEX mv_lsr_ilr_mc_sch_lisrid_idx ON mv_lsr_ilr_mc_schedule_amounts(lisrowid) TABLESPACE pwrplant_idx;
CREATE INDEX mv_lsr_ilr_mc_sch_optrid_idx ON mv_lsr_ilr_mc_schedule_amounts(optrowid) TABLESPACE pwrplant_idx;
CREATE INDEX mv_lsr_ilr_mc_sch_ilrrid_idx ON mv_lsr_ilr_mc_schedule_amounts(ilrrowid) TABLESPACE pwrplant_idx;
CREATE INDEX mv_lsr_ilr_mc_sch_lsrid_idx ON mv_lsr_ilr_mc_schedule_amounts(leaserowid) TABLESPACE pwrplant_idx;
create index mv_lsr_ilr_mc_sch_slsrid_idx on mv_lsr_ilr_mc_schedule_amounts(salesrowid) tablespace pwrplant_idx;

CREATE OR REPLACE FORCE VIEW v_lsr_ilr_mc_schedule AS 
WITH cur
AS (SELECT  ls_currency_type_id AS ls_cur_type,
            currency_id,
            currency_display_symbol,
            iso_code,
            CASE ls_currency_type_id
              WHEN 1 THEN 1
              ELSE NULL
            END AS contract_approval_rate
    FROM currency
    CROSS JOIN ls_lease_currency_type),
open_month
AS (SELECT  company_id,
            MIN(gl_posting_mo_yr) open_month
    FROM lsr_process_control
    WHERE open_next IS NULL
    GROUP BY company_id),
calc_rate 
AS (SELECT  a.company_id,
            a.contract_currency_id,
            A.company_currency_id,
            a.accounting_month,
            a.exchange_date,
            a.rate,
            b.rate prev_rate
    FROM ls_lease_calculated_date_rates a
    LEFT OUTER JOIN ls_lease_calculated_date_rates b ON a.company_id = b.company_id
                                                      AND a.contract_currency_id = b.contract_currency_id
                                                      AND A.accounting_month = add_months(b.accounting_month,1)),
rate_now
AS (SELECT  currency_from,
            currency_to,
            rate
    FROM (SELECT  currency_from,
                  currency_to,
                  rate,
                  row_number() OVER(PARTITION BY currency_from, currency_to
                                    ORDER BY exchange_date DESC) AS rn
          FROM currency_rate_default_dense 
          WHERE TRUNC(exchange_date,'MONTH') <= TRUNC(sysdate,'MONTH')
          AND exchange_rate_type_id = 1)
    WHERE rn = 1) 
SELECT schedule.ilr_id ilr_id,
       schedule.ilr_number,
       lease.lease_id,
       lease.lease_number,
       schedule.current_revision,
       schedule.revision revision,
       schedule.set_of_books_id set_of_books_id,
       schedule.month month,
       open_month.company_id,
       open_month.open_month,
       cur.ls_cur_type AS ls_cur_type,
       rates.exchange_date,
       calc_rate.exchange_date prev_exchange_date,
       lease.contract_currency_id,
       cur.currency_id display_currency_id,
       rates.rate,
       calc_rate.rate calculated_rate,
       calc_rate.prev_rate previous_calculated_rate,
       cur.iso_code,
       cur.currency_display_symbol,
       schedule.interest_income_received * nvl(calc_rate.rate, rates.rate) interest_income_received,
       schedule.interest_income_accrued * nvl(calc_rate.rate, rates.rate) interest_income_accrued,
       schedule.beg_deferred_rev * nvl(calc_rate.rate, rates.rate) beg_deferred_rev,
       schedule.deferred_rev_activity * nvl(calc_rate.rate, rates.rate) deferred_rev_activity,
       schedule.end_deferred_rev * nvl(calc_rate.rate, rates.rate) end_deferred_rev,
       schedule.beg_receivable * nvl(calc_rate.rate, rates.rate) beg_receivable,
       schedule.end_receivable * nvl(calc_rate.rate, rates.rate) end_receivable,
       schedule.executory_accrual1 * nvl(calc_rate.rate, rates.rate) executory_accrual1,
       schedule.executory_accrual2 * nvl(calc_rate.rate, rates.rate) executory_accrual2,
       schedule.executory_accrual3 * nvl(calc_rate.rate, rates.rate) executory_accrual3,
       schedule.executory_accrual4 * nvl(calc_rate.rate, rates.rate) executory_accrual4,
       schedule.executory_accrual5 * nvl(calc_rate.rate, rates.rate) executory_accrual5,
       schedule.executory_accrual6 * nvl(calc_rate.rate, rates.rate) executory_accrual6,
       schedule.executory_accrual7 * nvl(calc_rate.rate, rates.rate) executory_accrual7,
       schedule.executory_accrual8 * nvl(calc_rate.rate, rates.rate) executory_accrual8,
       schedule.executory_accrual9 * nvl(calc_rate.rate, rates.rate) executory_accrual9,
       schedule.executory_accrual10 * nvl(calc_rate.rate, rates.rate) executory_accrual10,
       schedule.executory_paid1 * nvl(calc_rate.rate, rates.rate) executory_paid1,
       schedule.executory_paid2 * nvl(calc_rate.rate, rates.rate) executory_paid2,
       schedule.executory_paid3 * nvl(calc_rate.rate, rates.rate) executory_paid3,
       schedule.executory_paid4 * nvl(calc_rate.rate, rates.rate) executory_paid4,
       schedule.executory_paid5 * nvl(calc_rate.rate, rates.rate) executory_paid5,
       schedule.executory_paid6 * nvl(calc_rate.rate, rates.rate) executory_paid6,
       schedule.executory_paid7 * nvl(calc_rate.rate, rates.rate) executory_paid7,
       schedule.executory_paid8 * nvl(calc_rate.rate, rates.rate) executory_paid8,
       schedule.executory_paid9 * nvl(calc_rate.rate, rates.rate) executory_paid9,
       schedule.executory_paid10 * nvl(calc_rate.rate, rates.rate) executory_paid10,
       schedule.contingent_accrual1 * nvl(calc_rate.rate, rates.rate) contingent_accrual1,
       schedule.contingent_accrual2 * nvl(calc_rate.rate, rates.rate) contingent_accrual2,
       schedule.contingent_accrual3 * nvl(calc_rate.rate, rates.rate) contingent_accrual3,
       schedule.contingent_accrual4 * nvl(calc_rate.rate, rates.rate) contingent_accrual4,
       schedule.contingent_accrual5 * nvl(calc_rate.rate, rates.rate) contingent_accrual5,
       schedule.contingent_accrual6 * nvl(calc_rate.rate, rates.rate) contingent_accrual6,
       schedule.contingent_accrual7 * nvl(calc_rate.rate, rates.rate) contingent_accrual7,
       schedule.contingent_accrual8 * nvl(calc_rate.rate, rates.rate) contingent_accrual8,
       schedule.contingent_accrual9 * nvl(calc_rate.rate,rates.rate) contingent_accrual9,
       schedule.contingent_accrual10 * nvl(calc_rate.rate, rates.rate) contingent_accrual10,
       schedule.contingent_paid1 * nvl(calc_rate.rate, rates.rate) contingent_paid1,
       schedule.contingent_paid2 * nvl(calc_rate.rate, rates.rate) contingent_paid2,
       schedule.contingent_paid3 * nvl(calc_rate.rate, rates.rate) contingent_paid3,
       schedule.contingent_paid4 * nvl(calc_rate.rate, rates.rate) contingent_paid4,
       schedule.contingent_paid5 * nvl(calc_rate.rate, rates.rate) contingent_paid5,
       schedule.contingent_paid6 * nvl(calc_rate.rate, rates.rate) contingent_paid6,
       schedule.contingent_paid7 * nvl(calc_rate.rate, rates.rate) contingent_paid7,
       schedule.contingent_paid8 * nvl(calc_rate.rate, rates.rate) contingent_paid8,
       schedule.contingent_paid9 * nvl(calc_rate.rate, rates.rate) contingent_paid9,
       schedule.contingent_paid10 * nvl(calc_rate.rate, rates.rate) contingent_paid10,
       schedule.principal_received * nvl(calc_rate.rate, rates.rate) principal_received,
       schedule.principal_accrued * nvl(calc_rate.rate,rates.rate) principal_accrued,
       schedule.beg_long_term_receivable * nvl(calc_rate.rate, rates.rate) beg_long_term_receivable,
       schedule.end_long_term_receivable * nvl(calc_rate.rate, rates.rate) end_long_term_receivable,
       schedule.beg_unguaranteed_residual * nvl(calc_rate.rate, rates.rate) beg_unguaranteed_residual,
       schedule.interest_unguaranteed_residual * nvl(calc_rate.rate, rates.rate) interest_unguaranteed_residual,
       schedule.ending_unguaranteed_residual * nvl(calc_rate.rate, rates.rate) ending_unguaranteed_residual,
       schedule.beg_net_investment * nvl(calc_rate.rate, rates.rate) beg_net_investment,
       schedule.interest_net_investment * nvl(calc_rate.rate, rates.rate) interest_net_investment,
       schedule.ending_net_investment * nvl(calc_rate.rate, rates.rate) ending_net_investment,
       schedule.beg_receivable * ( nvl(calc_rate.rate,0) - nvl(calc_rate.prev_rate,0) ) gain_loss_fx
FROM mv_lsr_ilr_mc_schedule_amounts schedule
INNER JOIN lsr_lease lease ON schedule.lease_id = lease.lease_id
INNER JOIN currency_schema cs ON schedule.company_id = cs.company_id
INNER JOIN cur ON cur.currency_id = CASE cur.ls_cur_type
                                      WHEN 1 THEN lease.contract_currency_id
                                      WHEN 2 THEN cs.currency_id
                                    END
INNER JOIN open_month ON schedule.company_id = open_month.company_id
INNER JOIN currency_rate_default_dense rates ON cur.currency_id = rates.currency_to 
                                              AND lease.contract_currency_id = rates.currency_from
                                              AND trunc(rates.exchange_date,'MONTH') = trunc(schedule.month,'MONTH')
INNER JOIN rate_now ON cur.currency_id = rate_now.currency_to AND lease.contract_currency_id = rate_now.currency_from
LEFT OUTER JOIN calc_rate ON lease.contract_currency_id = calc_rate.contract_currency_id
                          AND cur.currency_id = calc_rate.company_currency_id
                          AND schedule.company_id = calc_rate.company_id
                          AND schedule.month = calc_rate.accounting_month
WHERE cs.currency_type_id = 1
AND rates.exchange_rate_type_id = 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3847, 0, 2017, 1, 0, 0, 49510, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049510_lessor_remove_sch_current_lease_cost_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));