/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051953_lessee_01_default_deferred_rent_sw_to_on_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 07/11/2018 Crystal Yura 	  Default Deferred/Prepaid Rent option to On for records imported as null in 2017.3
||============================================================================
*/

UPDATE ls_ilr_options
SET deferred_rent_sw = 1
WHERE deferred_rent_sw IS NULL;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7922, 0, 2017, 4, 0, 0, 51593, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051953_lessee_01_default_deferred_rent_sw_to_on_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	