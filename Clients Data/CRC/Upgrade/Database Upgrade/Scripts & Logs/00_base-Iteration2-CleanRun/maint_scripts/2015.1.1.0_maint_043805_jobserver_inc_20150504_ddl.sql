/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_043805_jobserver_inc_20150504_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1.1   05/04/2014 Paul Cordero    	 Adding new column 'STATUS' for 'PP_JOB_QUEUE' table
||==========================================================================================
*/

set serveroutput on;

declare 
  doesTableColumnExist number := 0;
  begin
       begin
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('STATUS', 'PP_JOB_QUEUE','PWRPLANT');
          
          if doesTableColumnExist = 0 then
            begin
              dbms_output.put_line('Creating column STATUS for PP_JOB_QUEUE table');
                            
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_QUEUE" 
              ADD 
              (
                "STATUS" VARCHAR2(50 BYTE) DEFAULT ''Queued'' CONSTRAINT PP_JOB_QUEUE07 NOT NULL ENABLE
              )';    

              execute immediate 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."STATUS" IS ''Column to denote current status of queued item.''';
			  
            end;
          else
            begin
              dbms_output.put_line('Column STATUS for PP_JOB_QUEUE table exists');
            end;
          end if;		  
		  
	   end;
  end;
 /


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2571, 0, 2015, 1, 1, 0, 043805, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.1.0_maint_043805_jobserver_inc_20150504_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;