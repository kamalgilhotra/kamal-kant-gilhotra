/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047025_lease_add_asset_level_formula_col_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 03/22/2017 Charlie Shilling add new column to denote if this formula is asset or ilr level. 
||============================================================================
*/
ALTER TABLE ls_variable_payment
ADD asset_level_formula NUMBER(1,0) NULL;

COMMENT ON COLUMN ls_variable_payment.asset_level_formula IS 'A true/false flag to denote whether this formula applies to the asset level or ILR. 0 = ILR Level; 1 = Asset Level';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3403, 0, 2017, 1, 0, 0, 47025, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047025_lease_add_asset_level_formula_col_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	