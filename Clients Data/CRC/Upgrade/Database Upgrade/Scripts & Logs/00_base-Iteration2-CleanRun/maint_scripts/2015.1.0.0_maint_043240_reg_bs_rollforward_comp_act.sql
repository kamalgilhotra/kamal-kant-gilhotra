/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043240_reg_bs_rollforward_comp_act.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/31/2015 Shane Ward       Update Balancesheet rollforward queries
||                                          to use reg_component_activity
||============================================================================
*/

UPDATE cr_dd_sources_criteria
SET SQL = 'select l.long_description historic_ledger,
			 c.description reg_company,
			 m.description reg_account,
			 t.description reg_acct_type,
			 s.description sub_acct_type,
			 f.description component,
			 decode(a.reg_activity_id,
							-1,
							''Begin Balance'',
							0,
							''End Balance'',
							-2,
							''Regulatory Ledger Adjustment'',
							d.description) activity,
			 a.gl_month,
			 a.act_amount amount
	from (select reg_company_id,
							 reg_acct_id,
							 historic_version_id,
							 gl_month,
							 reg_component_id,
							 reg_activity_id,
							 act_amount,
							 reg_activity_id sort_order
					from reg_history_activity
          WHERE reg_source_id = 1
				union
				select x.reg_company_id,
							 x.reg_acct_id,
							 x.historic_version_id,
							 to_number(to_char(add_months(to_date(to_char(x.gl_month),
																										''YYYYMM''),
																						1),
																 ''YYYYMM'')),
							 y.reg_component_id,
							 -1,
							 x.act_amount,
							 0
					from reg_history_ledger x, reg_acct_master y
				 where x.reg_source_id = 1
					 and x.reg_acct_id = y.reg_acct_id
				union
				select x.reg_company_id,
							 x.reg_acct_id,
							 x.historic_version_id,
							 x.gl_month,
							 y.reg_component_id,
							 -2,
							 nvl(x.adj_amount, 0),
							 900
					from reg_history_ledger x, reg_acct_master y
				 where x.reg_source_id = 1
					 and x.reg_acct_id = y.reg_acct_id
				union
				select x.reg_company_id,
							 x.reg_acct_id,
							 x.historic_version_id,
							 x.gl_month,
							 y.reg_component_id,
							 0,
							 x.act_amount,
							 1000
					from reg_history_ledger x, reg_acct_master y
				 where x.reg_source_id = 1
					 and x.reg_acct_id = y.reg_acct_id) a,
			 reg_acct_master m,
			 reg_acct_type t,
			 reg_sub_acct_type s,
			 (select distinct reg_company_id, description from reg_company_sv) c,
			 reg_component_activity d,
			 reg_historic_version l,
			 reg_depr_family_components f
 where a.reg_company_id = c.reg_company_id
	 and a.reg_acct_id = m.reg_acct_id
   and d.reg_family_id = 2
	 and m.reg_acct_type_default = t.reg_acct_type_id
	 and m.reg_acct_type_default = s.reg_acct_type_id
	 and m.sub_acct_type_id = s.sub_acct_type_id
	 and a.historic_version_id = l.historic_version_id
	 and a.reg_component_id = d.reg_component_id(+)
	 and a.reg_activity_id = d.reg_activity_id(+)
	 and a.reg_component_id = f.reg_component_id'
WHERE Lower(description) = 'balance sheet roll-forward detail';

UPDATE cr_dd_sources_criteria
SET SQL = 'SELECT l.long_description historic_ledger,
			 c.description reg_company,
			 m.description reg_account,
			 t.description reg_acct_type,
			 s.description sub_acct_type,
			 f.description component,
			 decode(a.reg_activity_id,
							-1,
							''Begin Balance'',
							0,
							''End Balance'',
							-2,
							''Regulatory Ledger Adjustment'',
							d.description) activity,
			 a.act_amount amount,
			 sm.filter_string start_month,
			 em.filter_string end_month
	FROM (SELECT reg_company_id,
							 reg_acct_id,
							 historic_version_id,
							 reg_component_id,
							 reg_activity_id,
							 Sum(act_amount) act_amount,
							 reg_activity_id sort_order
					FROM reg_history_activity,
							 cr_dd_required_filter sm,
							 cr_dd_required_filter em
				 WHERE gl_month BETWEEN sm.filter_string AND em.filter_string
					 AND Upper(sm.column_name) = ''START_MONTH''
					 AND Upper(em.column_name) = ''END_MONTH''
           AND reg_source_id = 1
				 GROUP BY reg_company_id,
									reg_acct_id,
									historic_version_id,
									reg_component_id,
									reg_activity_id
				UNION
				SELECT x.reg_company_id,
							 x.reg_acct_id,
							 x.historic_version_id,
							 y.reg_component_id,
							 -1,
							 x.act_amount,
							 0
					FROM reg_history_ledger    x,
							 reg_acct_master       y,
							 cr_dd_required_filter sm
				 WHERE x.reg_source_id = 1
					 and x.reg_acct_id = y.reg_acct_id
					 AND gl_month = To_Number(To_Char(Add_Months(To_Date(sm.filter_string,
																															 ''yyyymm''),
																											 -1),
																						''yyyymm''))
					 AND Upper(sm.column_name) = ''START_MONTH''
				UNION
				SELECT x.reg_company_id,
							 x.reg_acct_id,
							 x.historic_version_id,
							 y.reg_component_id,
							 -2,
							 sum(nvl(x.adj_amount, 0)),
							 900
					FROM reg_history_ledger    x,
							 reg_acct_master       y,
							 cr_dd_required_filter sm,
							 cr_dd_required_filter em
				 WHERE x.reg_source_id = 1
					 and x.reg_acct_id = y.reg_acct_id
					 AND gl_month BETWEEN sm.filter_string AND em.filter_string
					 AND Upper(sm.column_name) = ''START_MONTH''
					 AND Upper(em.column_name) = ''END_MONTH''
				 GROUP BY x.reg_company_id,
									x.reg_acct_id,
									x.historic_version_id,
									y.reg_component_id
				UNION
				SELECT x.reg_company_id,
							 x.reg_acct_id,
							 x.historic_version_id,
							 y.reg_component_id,
							 0,
							 x.act_amount,
							 1000
					FROM reg_history_ledger    x,
							 reg_acct_master       y,
							 cr_dd_required_filter sm,
							 cr_dd_required_filter em
				 WHERE x.reg_source_id = 1
					 and x.reg_acct_id = y.reg_acct_id
					 AND gl_month = em.filter_string
					 AND Upper(em.column_name) = ''END_MONTH'') a,
			 reg_acct_master m,
			 reg_acct_type t,
			 reg_sub_acct_type s,
			 (select distinct reg_company_id, description from reg_company_sv) c,
			 reg_component_activity d,
			 reg_historic_version l,
			 reg_depr_family_components f,
			 cr_dd_required_filter sm,
			 cr_dd_required_filter em
 WHERE a.reg_company_id = c.reg_company_id
	 and a.reg_acct_id = m.reg_acct_id
	 and m.reg_acct_type_default = t.reg_acct_type_id
	 and m.reg_acct_type_default = s.reg_acct_type_id
	 and m.sub_acct_type_id = s.sub_acct_type_id
	 and a.historic_version_id = l.historic_version_id
	 and a.reg_component_id = d.reg_component_id(+)
	 and a.reg_activity_id = d.reg_activity_id(+)
	 and a.reg_component_id = f.reg_component_id
	 AND Upper(sm.column_name) = ''START_MONTH''
	 AND Upper(em.column_name) = ''END_MONTH'''
WHERE Lower(description) = 'balance sheet roll-forward timespan';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2470, 0, 2015, 1, 0, 0, 43240, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043240_reg_bs_rollforward_comp_act.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;