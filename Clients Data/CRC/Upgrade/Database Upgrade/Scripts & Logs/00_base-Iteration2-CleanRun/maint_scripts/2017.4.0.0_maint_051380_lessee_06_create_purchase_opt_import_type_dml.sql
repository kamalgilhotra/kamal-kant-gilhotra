/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051380_lessee_06_create_purchase_opt_import_type_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 5/29/2018  Alex Healey    Create new import type for purchase options on ILR import
||============================================================================
*/

insert into pp_import_type
(import_type_id,
description,
long_description,
import_table_name,
archive_table_name,
allow_updates_on_add,
delegate_object_name)
values
(268,
 'Add: ILR Purchase Options',
 'Lessee ILR Purchase Options',
 'ls_import_ilr_purchase_opt',
 'ls_import_ilr_purchase_opt_archive',
 1,
 'nvo_ls_logic_import'
);


insert into pp_import_type_subsystem
(import_type_id,
 import_subsystem_id
)
values
(268,
 8
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6082, 0, 2017, 4, 0, 0, 51380, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051380_lessee_06_create_purchase_opt_import_type_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;