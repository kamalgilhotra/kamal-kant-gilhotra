/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_044472_pwrtax_add_menu_items_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/22/2015 Andrew Hill    Add Tax Class to the “Standard Table Setup” list
//                                      in the PowerTax ppbase menu
||============================================================================
*/
INSERT INTO ppbase_workspace( module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
VALUES ('powertax', 'depr_table_tax_class', 'Tax Class', 'w_tax_class_input', 'Tax Class', 2);

INSERT INTO ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
VALUES ('powertax', 'depr_table_tax_class', 3, 6, 'Tax Class', NULL,'depr_table', 'depr_table_tax_class', 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2708, 0, 2015, 2, 0, 0, 044472, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044472_pwrtax_add_menu_items_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;