/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048897_lessor_01_mla_approval_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 10/02/2017 Shane "C" Ward  Add columns for MLA Approval workflow Lessor
||
||============================================================================
*/

--Columns for Lessor MLA Approval
ALTER TABLE lsr_lease_approval ADD approval_type_id NUMBER(22,0);
ALTER TABLE lsr_lease_approval ADD approval_status_id NUMBER(22,0);
ALTER TABLE lsr_lease_approval ADD approver varchar2(18);
ALTER TABLE lsr_lease_approval ADD approval_date DATE;
ALTER TABLE lsr_lease_approval ADD rejected number(1,0);
ALTER TABLE lsr_lease_approval ADD revision_desc number(35);
ALTER TABLE lsr_lease_approval ADD revision_long_desc varchar2(254);

COMMENT ON COLUMN lsr_lease_approval.approval_type_id IS 'The approval type.';
COMMENT ON COLUMN lsr_lease_approval.approval_status_id IS 'Whether or not the revision is initiated, pending approval, approved, or rejected.';
COMMENT ON COLUMN lsr_lease_approval.approver IS 'User or group approving.';
COMMENT ON COLUMN lsr_lease_approval.approval_date IS 'Date approver approved.';
COMMENT ON COLUMN lsr_lease_approval.rejected IS 'If the approver rejects the revision.';
COMMENT ON COLUMN lsr_lease_approval.revision_desc IS 'A description for the revision.';
COMMENT ON COLUMN lsr_lease_approval.revision_long_desc IS 'A longer description for the revision.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
  (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
  (3753, 0, 2017, 1, 0, 0, 48897, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048897_lessor_01_mla_approval_ddl.sql', 1,
  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;