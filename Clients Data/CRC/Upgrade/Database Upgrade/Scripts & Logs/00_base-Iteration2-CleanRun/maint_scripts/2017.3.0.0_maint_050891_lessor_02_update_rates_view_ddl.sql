/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050891_lessor_02_update_rates_view_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 04/19/2018 Andrew Hill    Remove compounded rates from rates view
||============================================================================
*/

CREATE OR REPLACE VIEW v_lsr_ilr_rates_compounded AS
SELECT  lsr_ilr_rates.ilr_id,
        lsr_ilr_rates.revision,
        lsr_ilr_rates.rate_type_id,
        lsr_ilr_rate_types.DESCRIPTION AS rate_type_description,
        pkg_lessor_schedule.f_annual_to_implicit_rate(lsr_ilr_rates.rate) AS rate_implicit,
        rate AS annual_discount_rate
FROM lsr_ilr_rates
JOIN lsr_ilr_rate_types ON lsr_ilr_rates.rate_type_id = lsr_ilr_rate_types.rate_type_id;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4629, 0, 2017, 3, 0, 0, 50891, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050891_lessor_02_update_rates_view_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;