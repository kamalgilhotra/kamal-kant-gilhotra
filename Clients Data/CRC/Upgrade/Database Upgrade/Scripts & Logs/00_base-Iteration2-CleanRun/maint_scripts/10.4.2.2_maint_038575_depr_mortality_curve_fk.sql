/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038575_depr_mortality_curve_fk.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------- ----------------------------------------------
|| 10.4.2.2 06/16/2014 Kyle Peterson
||========================================================================================
*/

update DEPR_METHOD_RATES
   set MORTALITY_CURVE_ID = null
 where MORTALITY_CURVE_ID not in (select MORTALITY_CURVE_ID from MORTALITY_CURVE);

alter table DEPR_METHOD_RATES
   add constraint R_DEPR_METHOD_RATES4
       foreign key (MORTALITY_CURVE_ID)
       references MORTALITY_CURVE(MORTALITY_CURVE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1248, 0, 10, 4, 2, 2, 38575, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.2_maint_038575_depr_mortality_curve_fk.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
