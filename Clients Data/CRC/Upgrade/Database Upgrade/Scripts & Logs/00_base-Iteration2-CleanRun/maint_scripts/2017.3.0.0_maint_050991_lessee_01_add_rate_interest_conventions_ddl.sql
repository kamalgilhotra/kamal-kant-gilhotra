/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050991_lessee_01_add_rate_interest_conventions_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 04/23/2018 Josh Sandler 	  Add rate convention and interst convention
||============================================================================
*/

CREATE TABLE ls_rate_convention
  (
     rate_convention_id NUMBER(22, 0) NOT NULL,
     description     VARCHAR2(35) NOT NULL,
     user_id         VARCHAR2(18) NULL,
     time_stamp      DATE NULL
  );

ALTER TABLE ls_rate_convention
  ADD CONSTRAINT pk_ls_rate_convention PRIMARY KEY ( rate_convention_id ) USING INDEX TABLESPACE pwrplant_idx;

COMMENT ON COLUMN ls_rate_convention.rate_convention_id IS 'The internal lease rate convention id within PowerPlant.';
COMMENT ON COLUMN ls_rate_convention.description IS 'A description of the rate convention.';
COMMENT ON COLUMN ls_rate_convention.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_rate_convention.user_id IS 'Standard System-assigned user id used for audit purposes.';


CREATE TABLE ls_interest_convention
  (
     interest_convention_id NUMBER(22, 0) NOT NULL,
     description     VARCHAR2(35) NOT NULL,
     user_id         VARCHAR2(18) NULL,
     time_stamp      DATE NULL
  );

ALTER TABLE ls_interest_convention
  ADD CONSTRAINT pk_ls_interest_convention PRIMARY KEY ( interest_convention_id ) USING INDEX TABLESPACE pwrplant_idx;

COMMENT ON COLUMN ls_interest_convention.interest_convention_id IS 'The internal lease interest convention id within PowerPlant.';
COMMENT ON COLUMN ls_interest_convention.description IS 'A description of the interest convention.';
COMMENT ON COLUMN ls_interest_convention.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_interest_convention.user_id IS 'Standard System-assigned user id used for audit purposes.';



ALTER TABLE ls_lease_cap_type
  ADD (rate_convention_id      NUMBER(1),
       interest_convention_id  NUMBER(1)
      );

ALTER TABLE ls_lease_cap_type
  ADD CONSTRAINT fk_lct_rate FOREIGN KEY (
    rate_convention_id
  ) REFERENCES ls_rate_convention (
    rate_convention_id
  )
;

ALTER TABLE ls_lease_cap_type
  ADD CONSTRAINT fk_lct_interest FOREIGN KEY (
    interest_convention_id
  ) REFERENCES ls_interest_convention (
    interest_convention_id
  )
;

COMMENT ON COLUMN ls_lease_cap_type.rate_convention_id IS 'The rate convention.';
COMMENT ON COLUMN ls_lease_cap_type.interest_convention_id IS 'The interest convention.';




ALTER TABLE LS_ILR_STG
  ADD interest_convention_id NUMBER(1, 0);

COMMENT ON COLUMN LS_ILR_STG.interest_convention_id IS 'The interest convention used for calculating interest.';

ALTER TABLE LS_ILR_SCHEDULE_STG
  ADD interest_convention_id NUMBER(1, 0);

COMMENT ON COLUMN LS_ILR_SCHEDULE_STG.interest_convention_id IS 'The interest convention used for calculating interest.';

ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD interest_convention_id NUMBER(1, 0);

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.interest_convention_id IS 'The interest convention used for calculating interest.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4622, 0, 2017, 3, 0, 0, 50991, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050991_lessee_01_add_rate_interest_conventions_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;