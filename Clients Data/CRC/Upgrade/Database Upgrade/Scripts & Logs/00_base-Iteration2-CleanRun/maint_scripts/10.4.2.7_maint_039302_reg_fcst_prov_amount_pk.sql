SET DEFINE OFF

/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_039302_reg_fcst_prov_amount_pk.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.2.7 07/31/2014 Ryan Oliveria       Need company on split prov amount PK
||========================================================================================
*/

alter table REG_TA_SPLIT_PROV_AMOUNT drop primary key drop index;

alter table REG_TA_SPLIT_PROV_AMOUNT
   add constraint PK_REG_TA_SPLIT_PROV_AMOUNT
       primary key (REG_COMPANY_ID, HISTORIC_VERSION_ID, REG_ACCT_ID, GL_MONTH, FORECAST_VERSION_ID)
       using index tablespace PWRPLANT_IDX;


delete from PPBASE_MENU_ITEMS
 where MODULE = 'REG'
   and PARENT_MENU_IDENTIFIER = 'FORE_INTEGRATION';

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'FCST_TAX_PROV_SETUP', 3, 5, 'Fcst Tax Prov - Setup', 'Tax Provision - Integration Setup',
    'FORE_INTEGRATION', 'uo_reg_fcst_ta_setup', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'FORE_POWERTAX', 3, 8, 'Fcst PowerTax', 'Fcst PowerTax', 'FORE_INTEGRATION', 'uo_reg_fcst_tax_int', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'FCST_DEF_TAX', 3, 6, 'Fcst Tax Prov - Timing Diff/Def',
    'Tax Provision - Timing Differences/Deferreds Integration', 'FORE_INTEGRATION', 'uo_reg_fcst_ta_def_int', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'FCST_TAX_PROV', 3, 7, 'Fcst Tax Prov - Other Tax Items', 'Tax Provision - Other Tax Items Integration',
    'FORE_INTEGRATION', 'uo_reg_fcst_prov_int', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'FORE_RECON_MAINT', 3, 1, 'Fcst Recon Maintenance', 'Reconciliation Maintenance', 'FORE_INTEGRATION',
    'uo_reg_fcst_recon_setup', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'FORE_VERSION', 3, 9, 'Forecast Ledgers', 'Forecast Ledger Maintenance', 'FORE_INTEGRATION',
    'uo_reg_fcst_version', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'FORE_CAPITAL', 3, 4, 'Capital Budget', 'Capital Budget Integration', 'FORE_INTEGRATION',
    'uo_reg_fcst_cap_bdg_int', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'FORE_OM', 3, 3, 'Dept Budget (O&M)', 'Departmental Budget (O&M) Integration', 'FORE_INTEGRATION',
    'uo_reg_fcst_cr_int_grid_map', 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'FORE_DEPR', 3, 2, 'Fcst Assets/Depreciation', 'Forecast Assets/Depreciation Integration',
    'FORE_INTEGRATION', 'uo_reg_fcst_depr_int', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1329, 0, 10, 4, 2, 7, 39302, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039302_reg_fcst_prov_amount_pk.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON