/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031522_taxrpr_asset_act_post.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date        Revised By          Reason for Change
|| -------- ----------  ------------------- ----------------------------------
|| 10.4.1.0 08/15/2013  Andrew Scott        Change to logic for how next asset activity is pulled during post to cpr
||============================================================================
*/

drop table REPAIR_POST_ACTIVITY_STG;

----drop table REPAIR_CALC_ASSET_STG;

create table REPAIR_CALC_ASSET_STG
(
 ASSET_ID           number(22,0) not null,
 ASSET_ACTIVITY_ID  number(22,0) not null,
 BATCH_ID           number(22,0) not null,
 BOOK_SUMMARY_ID    number(22,0) not null,
 WORK_REQUEST       varchar2(35) default 0 not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 INSERT_ACTIVITY_ID number(22,0) not null,
 COMPANY_ID         number(22,0),
 REPAIR_SCHEMA_ID   number(22,0),
 AMOUNT             number(22,2),
 ADJUSTMENT         number(22,2),
 FERC_ACTIVITY_CODE number(22,0) default 1
);

alter table REPAIR_CALC_ASSET_STG
   add constraint PK_REPAIR_CALC_ASSET_STG
       primary key (ASSET_ID, ASSET_ACTIVITY_ID, BATCH_ID, BOOK_SUMMARY_ID, WORK_REQUEST)
       using index tablespace PWRPLANT_IDX;

create unique index REPAIR_CALC_ASSET_STG_UIDX
   on REPAIR_CALC_ASSET_STG (ASSET_ID, INSERT_ACTIVITY_ID)
      tablespace PWRPLANT_IDX;

comment on table REPAIR_CALC_ASSET_STG is '(C) [18] The Repair Calc Asset Stg is a staging table used to calculate the next asset activity id for inserting into new activity into the CPR.';
comment on column REPAIR_CALC_ASSET_STG.ASSET_ID is 'Part of the primary key of Repair Calc Asset.  Needed to join back to Repair Calc Asset with the new asset activity id.';
comment on column REPAIR_CALC_ASSET_STG.ASSET_ACTIVITY_ID is 'Part of the primary key of Repair Calc Asset.  Needed to join back to Repair Calc Asset with the new asset activity id.';
comment on column REPAIR_CALC_ASSET_STG.BATCH_ID is 'Part of the primary key of Repair Calc Asset.  Needed to join back to Repair Calc Asset with the new asset activity id.';
comment on column REPAIR_CALC_ASSET_STG.BOOK_SUMMARY_ID is 'Part of the primary key of Repair Calc Asset.  Needed to join back to Repair Calc Asset with the new asset activity id.';
comment on column REPAIR_CALC_ASSET_STG.WORK_REQUEST is 'Part of the primary key of Repair Calc Asset.  Needed to join back to Repair Calc Asset with the new asset activity id.';
comment on column REPAIR_CALC_ASSET_STG.TIME_STAMP is 'Standard system-assigned user id used for audit purposes.';
comment on column REPAIR_CALC_ASSET_STG.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column REPAIR_CALC_ASSET_STG.INSERT_ACTIVITY_ID is 'The calculated next asset activity id to be used for inserting into the CPR Activity table.';
comment on column REPAIR_CALC_ASSET_STG.COMPANY_ID is 'Pulled from Repair Calc Asset.  Need to pull in for staging before inserting into CPR Activity table.';
comment on column REPAIR_CALC_ASSET_STG.REPAIR_SCHEMA_ID is 'Pulled from Repair Calc Asset.  Need to pull in for staging before inserting into CPR Activity table.';
comment on column REPAIR_CALC_ASSET_STG.AMOUNT is 'Pulled from Repair Calc Asset.  Need to pull in for staging before inserting into CPR Activity table.';
comment on column REPAIR_CALC_ASSET_STG.ADJUSTMENT is 'Pulled from Repair Calc Asset.  Need to pull in for staging before inserting into CPR Activity table.';
comment on column REPAIR_CALC_ASSET_STG.FERC_ACTIVITY_CODE is 'Pulled from Repair Calc Asset.  Need to pull in for staging before inserting into CPR Activity table.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (542, 0, 10, 4, 1, 0, 31522, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031522_taxrpr_asset_act_post.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;