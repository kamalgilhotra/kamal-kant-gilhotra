/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045434_lease_allow_concurrent_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 02/24/2016 Will Davis			 Allow lessee calculations to run 
|| 											 concurrently
||============================================================================
*/ 

update pp_processes
set allow_concurrent = 1
where upper(trim(description)) = 'LESSEE CALCULATIONS';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3066, 0, 2016, 1, 0, 0, 045434, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045434_lease_allow_concurrent_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;