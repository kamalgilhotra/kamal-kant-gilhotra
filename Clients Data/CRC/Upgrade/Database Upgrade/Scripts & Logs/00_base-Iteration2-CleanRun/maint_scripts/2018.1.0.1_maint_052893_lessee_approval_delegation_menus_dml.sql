/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052893_lessee_approval_delegation_menus_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.1.0.1 12/19/2018 Daniel Mendel    Add Approval Delegation menu items
||============================================================================
*/

insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, object_type_id)
select 'LESSEE', 'approval_delegation', 'Delegation', 'uo_ls_delegation_wksp', 1 from dual union
select 'LESSOR', 'approval_delegation', 'Delegation', 'uo_ls_delegation_wksp', 1 from dual;

insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn)
select 'LESSEE', 'approval_delegation', 2, 4, 'Delegation', 'menu_wksp_approval', 'approval_delegation', 1 from dual union
select 'LESSOR', 'approval_delegation', 2, 4, 'Delegation', 'menu_wksp_approval', 'approval_delegation', 1 from dual;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13285, 0, 2018, 1, 0, 1, 52893, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.1_maint_052893_lessee_approval_delegation_menus_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;