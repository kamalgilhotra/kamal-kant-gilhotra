/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_010940_tax_rpr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   05/10/2013 Alex P.         Point Release
||============================================================================
*/

create unique index IDX_RLR_DESCRIPTION
   on REPAIR_LOC_ROLLUP (DESCRIPTION)
      tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (387, 0, 10, 4, 1, 0, 10940, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010940_tax_rpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
