/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040746_depr_PKG_DEPR_IND_CALC.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 08/12/2014 Brandon Beck
||============================================================================
*/

create or replace package PKG_DEPR_IND_CALC as
	--**********************************************
	--
	--	THIS PACKAGE HOUSES THE CPR DEPR CALCULATIONS
	--	RECORDS SHOULD BE STAGED PRIOR TO THIS PACKAGE
	--	RESULTS SHOULD BE HANDLED AFTER THIS PACKAGE
	--
	--	The package handles:
	--		106 to 101 adjustments: 		F_NURV
	--		Trueup for late in service:		F_TRUEUP
	--		Calculating Init and rem life:	F_CALCREMLIFE
	--		NET / GROSS determination: 		F_CALCBASEANDRATE
	--		Mid Period Convention: 			F_CALCBASEANDRATE
	--
	--	Deprecation Calculations for the following methods:
	--		SL (Straight Line)
	--		SLEOL (Straight Line to End Of Life)
	--		SLE (Straight Line Expense for Leased Assets)
	--		SOMD (Sum of Months Digits)
	--		UOPR (Unit of Production Rates)
	--		RATE (Rates)
	--		DB150 (Declining Balance 1.5)
	--		DB200 (Declining Balance 2)
	--		FERC (FERC depreciation for Leased Assets: Depr Expense = Principal Accrued)
	--		SCH (Schedule: Different rates for different years with spread factors to the given months)
	--
	--**********************************************
	
	--
	--	MAIN Depr Calc Call
	--
	procedure P_DEPRCALC
	(
		A_MONTHS		PKG_PP_COMMON.DATE_TABTYPE,
		A_VERSION_ID	FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE
	);
	
	--
	--	WRAPPERS to CALL depreciation with different arguments
	--
	procedure P_DEPRCALC_LEASE;
	
	procedure P_DEPRCALC
	(
		A_MONTHS	PKG_PP_COMMON.DATE_TABTYPE
	);

	procedure P_DEPRCALC
	(
		A_MONTH		date
	);

	procedure P_DEPRCALC
	(
		A_MONTH			date,
		A_VERSION_ID	FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE
	);
	--
	--	END WRAPPERS
	--
end PKG_DEPR_IND_CALC;
/


create or replace package body PKG_DEPR_IND_CALC as
	--**************************************************************
	--       VARIABLES
	--**************************************************************
	G_CURRMONTH		date;

	G_VERSION_ID	FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE;
	G_MONTHS		PKG_PP_COMMON.DATE_TABTYPE;

	
		--
	--	ARCHIVE
	--
	procedure P_ARCHIVE
	is
	begin
	
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.P_ARCHIVE');
		
		--This only archives subledger -100 rows so it's fine to have it run for all actuals calcs
		if G_VERSION_ID is null then
			PKG_PP_CPR_DEPR.P_ARCHIVELEASEDEPR;
		end if;
		
		-- archive
		delete from cpr_depr_calc_stg_arc a
		where exists (
			select 1
			from cpr_depr_calc_stg b
			where b.asset_id = a.asset_id
			and b.set_of_books_id = a.set_of_books_id
			and b.gl_posting_mo_yr = a.gl_posting_mo_yr
		);

		-- archive
		insert into cpr_depr_calc_stg_arc
		(ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, TIME_STAMP, USER_ID, INIT_LIFE, 
		REMAINING_LIFE, ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS, 
		TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH, SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, 
		COST_OF_REMOVAL, RESERVE_TRANS_IN, RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, 
		GAIN_LOSS, DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE, 
		YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE, PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB, MONTH_RATE, 
		COMPANY_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID, DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, 
		TRUE_UP_CPR_DEPR, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT, 
		IMPAIRMENT_EXPENSE_AMOUNT, DEPRECIATION_INDICATOR, END_OF_LIFE, ENG_IN_SERVICE_YEAR, ESTIMATED_PRODUCTION, 
		HAS_CFNU, HAS_NURV, HAS_NURV_LAST_MONTH, NET_GROSS, OVER_DEPR_CHECK, PRODUCTION, RATE, EXISTS_LAST_MONTH, 
		EXISTS_TWO_MONTHS, EXISTS_ARO, EFFECTIVE_DATE, TRF_WEIGHT, DEPR_CALC_MESSAGE, 
		SUBLEDGER_TYPE_ID, DEPR_CALC_STATUS, NET_TRF, NET_RES_TRF, NET_IMP_AMT, ACTIVITY, BEG_RES_AMT, 
		ACTIVITY_3, DEPR_METHOD_SLE_EXP, BEG_BAL_AMT, SCH_RATE, SCH_FACTOR, MIN_MPC, TRUEUP_ADJ, NURV_ADJ, 
		OVER_DEPR_ADJ, sch_rate_used)
		select ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, TIME_STAMP, USER_ID, INIT_LIFE, REMAINING_LIFE, ESTIMATED_SALVAGE, BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, ASSET_DOLLARS, BEG_RESERVE_MONTH, SALVAGE_DOLLARS, RESERVE_ADJUSTMENT, COST_OF_REMOVAL, RESERVE_TRANS_IN, RESERVE_TRANS_OUT, DEPR_EXP_ADJUST, OTHER_CREDITS_AND_ADJUST, GAIN_LOSS, DEPRECIATION_BASE, CURR_DEPR_EXPENSE, DEPR_RESERVE, BEG_RESERVE_YEAR, YTD_DEPR_EXPENSE, YTD_DEPR_EXP_ADJUST, PRIOR_YTD_DEPR_EXPENSE, PRIOR_YTD_DEPR_EXP_ADJUST, ACCT_DISTRIB, MONTH_RATE, COMPANY_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, DEPR_GROUP_ID, DEPR_EXP_ALLOC_ADJUST, DEPR_METHOD_ID, TRUE_UP_CPR_DEPR, SALVAGE_EXPENSE, SALVAGE_EXP_ADJUST, SALVAGE_EXP_ALLOC_ADJUST, IMPAIRMENT_ASSET_AMOUNT, IMPAIRMENT_EXPENSE_AMOUNT, DEPRECIATION_INDICATOR, END_OF_LIFE, ENG_IN_SERVICE_YEAR, ESTIMATED_PRODUCTION, HAS_CFNU, HAS_NURV, HAS_NURV_LAST_MONTH, NET_GROSS, OVER_DEPR_CHECK, PRODUCTION, RATE, EXISTS_LAST_MONTH, EXISTS_TWO_MONTHS, EXISTS_ARO, EFFECTIVE_DATE, TRF_WEIGHT, DEPR_CALC_MESSAGE, SUBLEDGER_TYPE_ID, DEPR_CALC_STATUS, NET_TRF, NET_RES_TRF, NET_IMP_AMT, ACTIVITY, BEG_RES_AMT, ACTIVITY_3, DEPR_METHOD_SLE_EXP, BEG_BAL_AMT, SCH_RATE, SCH_FACTOR, MIN_MPC, 
		TRUEUP_ADJ, NURV_ADJ, OVER_DEPR_ADJ, sch_rate_used
		from cpr_depr_calc_stg;
		
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_ARCHIVE;

	--**************************************************************************
	--                            F_CALCBASEANDRATE
	--**************************************************************************
	procedure P_CALCBASEANDRATE(A_MONTH date)
	is
		L_STATUS varchar2(255);
	begin
	
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.F_CALCBASEANDRATE');
		
		merge into CPR_DEPR_CALC_STG A
		using
		(
			select *
			from CPR_DEPR_CALC_STG B
			where B.DEPR_CALC_STATUS = 2
			and B.GL_POSTING_MO_YR = A_MONTH
			MODEL
			partition by(ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR)
			DIMENSION by(MID_PERIOD_METHOD)
			MEASURES(
				MONTH_RATE, DEPRECIATION_BASE,
				CURR_DEPR_EXPENSE, REMAINING_LIFE,
				NET_GROSS, TRUE_UP_CPR_DEPR,
				MID_PERIOD_CONV, trunc( ENG_IN_SERVICE_YEAR, 'MM' ) as ENG_IN_SERVICE_YEAR,
				PRODUCTION, ESTIMATED_PRODUCTION,
				RATE, INIT_LIFE,
				BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST,
				RETIREMENTS, TRANSFERS_IN,
				TRANSFERS_OUT, IMPAIRMENT_ASSET_AMOUNT,
				ESTIMATED_SALVAGE, BEG_RESERVE_MONTH,
				DEPR_EXP_ALLOC_ADJUST, DEPR_EXP_ADJUST,
				SALVAGE_DOLLARS, COST_OF_REMOVAL,
				OTHER_CREDITS_AND_ADJUST, GAIN_LOSS,
				RESERVE_TRANS_IN, RESERVE_TRANS_OUT,
				RESERVE_ADJUSTMENT, IMPAIRMENT_EXPENSE_AMOUNT,
				TRF_WEIGHT, EXISTS_LAST_MONTH,
				HAS_CFNU, DEPR_METHOD_SLE_EXP,
				NET_TRF, NET_RES_TRF,
				NET_IMP_AMT, ACTIVITY,
				BEG_RES_AMT, ACTIVITY_3,
				BEG_BAL_AMT, SCH_RATE,
				NURV_ADJ, IMPAIRMENT_ASSET_ACTIVITY_SALV,
				IMPAIRMENT_ASSET_BEGIN_BALANCE, MIN_MPC
			)
			RULES update
			(
				NET_TRF[any] = TRANSFERS_IN[CV()] + TRANSFERS_OUT[CV()],
				NET_RES_TRF[any] = RESERVE_TRANS_IN[CV()] + RESERVE_TRANS_OUT[CV()],
				NET_IMP_AMT[any] = IMPAIRMENT_EXPENSE_AMOUNT[CV()] + IMPAIRMENT_ASSET_AMOUNT[CV()],
				ACTIVITY[any] = NET_ADDS_AND_ADJUST[CV()] + RETIREMENTS[CV()] 
						+ IMPAIRMENT_ASSET_AMOUNT[CV()],
				-- check to see if activity includes net trfs
				ACTIVITY [any] =
						case when TRF_WEIGHT[CV()] = 0 
						then ACTIVITY[CV()] + NET_TRF[CV()]
						else ACTIVITY[CV()]
						end,
				BEG_BAL_AMT[any] =
						case when TRF_WEIGHT[CV()] = 0
						then BEG_ASSET_DOLLARS[CV()]
						else BEG_ASSET_DOLLARS[CV()] + NET_TRF[CV()]
						end,
				BEG_RES_AMT[any] =
						case when TRF_WEIGHT[CV()] = 0
						then BEG_RESERVE_MONTH[CV()] + DEPR_EXP_ALLOC_ADJUST[CV()]
						else BEG_RESERVE_MONTH[CV()] + DEPR_EXP_ALLOC_ADJUST[CV()] 
							+ NET_RES_TRF[CV()]
						end,
				ACTIVITY_3[any] = RETIREMENTS[CV()] + DEPR_EXP_ADJUST[CV()]
						+ SALVAGE_DOLLARS[CV()] + COST_OF_REMOVAL[CV()]
						+ OTHER_CREDITS_AND_ADJUST[CV()] + GAIN_LOSS[CV()]
						+ RESERVE_ADJUSTMENT[CV()] + NURV_ADJ[CV()],
				-- check to see if activity includes net trfs
				ACTIVITY_3[any] = 
						case when TRF_WEIGHT[CV()] = 0
						then ACTIVITY_3[CV()] + NET_RES_TRF[CV()]
						else ACTIVITY_3[CV()]
						end,
				--Rate calcs
				MONTH_RATE[MID_PERIOD_METHOD in ('sleol', 'sl')] = 
						case when REMAINING_LIFE[CV()] >= 1
						then 1 / REMAINING_LIFE[CV()]
						else 1
						end,
				MONTH_RATE[MID_PERIOD_METHOD in ('somd')] =
						case when REMAINING_LIFE[CV()] >= 1
						then REMAINING_LIFE[CV()] / ((INIT_LIFE[CV()] * (INIT_LIFE[CV()] + 1)) / 2)
						else 1
						end,
				--RO: Added nvl because estimated_production can be null
				MONTH_RATE[MID_PERIOD_METHOD in ('uopr')] = 
						case when ESTIMATED_PRODUCTION[CV()] = 0
						then 0
						when PRODUCTION[CV()] > ESTIMATED_PRODUCTION[CV()]
						then 1
						else PRODUCTION[CV()] / ESTIMATED_PRODUCTION[CV()]
						end,
				MONTH_RATE[MID_PERIOD_METHOD in ('rate')] 	= RATE[CV()] / 12,
				MONTH_RATE[MID_PERIOD_METHOD in ('db200')]	= 
						case when REMAINING_LIFE[CV()] > 0
						then least(1, greatest(2 / INIT_LIFE[CV()], 1 / REMAINING_LIFE[CV()]))
						else 1
						end,
				MONTH_RATE[MID_PERIOD_METHOD in ('db150')] = 
						case when REMAINING_LIFE[CV()] > 0
						then least(1, greatest(1.5 / INIT_LIFE[CV()], 1 / REMAINING_LIFE[CV()]))
						else 1
						end,
				MONTH_RATE[MID_PERIOD_METHOD in ('sle', 'ferc')] = 1,
				MONTH_RATE[MID_PERIOD_METHOD in ('sch')] = nvl(SCH_RATE[CV()], 0),

				--Base calcs
				
				-- SET MID_PERIOD_CONV TO BE A 1 UNLESS one of the following:
				--		The initial life is the same as the remaining life (ie first month)
				--		Asset exists last month and the trueup option is not set to trueup every month 
				-- RATE AND UOPR GROSS uses real MPC if GROSS
				MID_PERIOD_CONV[MID_PERIOD_METHOD not in ('rate', 'uopr')] = 
						-- AND the mid period convention is a 0 but the eng in service is earlier than
						-- current month, then the activity should really be treated as a prior period.  So take into account.
						case when MID_PERIOD_CONV[CV()] = 0
						and MIN_MPC[CV()] = -1 
						and EXISTS_LAST_MONTH[CV()] = 0
						and ENG_IN_SERVICE_YEAR[CV()] <= ADD_MONTHS(CV(GL_POSTING_MO_YR), -1)
						then 1
						when (REMAINING_LIFE[CV()] = INIT_LIFE[CV()])
							or (TRUE_UP_CPR_DEPR[CV()] != 1 and EXISTS_LAST_MONTH[CV()] = 1)
						then MID_PERIOD_CONV[CV()]
						else 1
						end,
				MID_PERIOD_CONV[MID_PERIOD_METHOD in ('rate', 'uopr')] = 
						-- IF I am not doing trueup:
						-- AND the mid period convention is a 0 but the eng in service is earlier than
						-- current month, then the activity should really be treated as a prior period.  So take into account.
						case when MID_PERIOD_CONV[CV()] = 0
						and MIN_MPC[CV()] = -1 
						and EXISTS_LAST_MONTH[CV()] = 0
						and ENG_IN_SERVICE_YEAR[CV()] <= ADD_MONTHS(CV(GL_POSTING_MO_YR), -1)
						and NET_GROSS[CV()] = 1
						then 1
						when ( NET_GROSS[CV()] = 2 and TRUE_UP_CPR_DEPR[CV()] != 1 )
							or (REMAINING_LIFE[CV()] = INIT_LIFE[CV()])
							or (TRUE_UP_CPR_DEPR[CV()] != 1 and EXISTS_LAST_MONTH[CV()] = 1)
						then MID_PERIOD_CONV[CV()]
						else 1
						end,
						
				-- certain methods are ALWAYS based on net
				NET_GROSS[MID_PERIOD_METHOD in ('sleol', 'sl', 'somd', 'db200', 'db150')] = 1,

				-- certain methods are ALWAYS based on gross
				-- SCH takes into account salvage on GROSS method.
				-- FERC and SLE do not take into account salvage
				NET_GROSS[MID_PERIOD_METHOD in ('sle', 'ferc', 'sch')] = 2,

				-- if GROSS and METHOD is not SCH, then estimate salvage should be 0 since not taken into account
				-- IE RATE, UOPR, SLE, FERC
				ESTIMATED_SALVAGE[MID_PERIOD_METHOD not in ('sch')] =
						case when NET_GROSS[CV()] = 2 
						then 0
						else ESTIMATED_SALVAGE[CV()]
						end,


				DEPRECIATION_BASE[ANY] =
					--if net then use this formula
					case when NET_GROSS[CV()] = 1 then
						case when
							(BEG_ASSET_DOLLARS[CV()] + ACTIVITY[CV()]) 
							* (1 - ESTIMATED_SALVAGE[CV()])
							+ ((IMPAIRMENT_ASSET_ACTIVITY_SALV[CV()] 
								+ IMPAIRMENT_ASSET_BEGIN_BALANCE[CV()]) * ESTIMATED_SALVAGE[CV()])
							- (BEG_RES_AMT[CV()] + ACTIVITY_3[CV()] + 
								NET_RES_TRF[CV()] + NET_IMP_AMT[CV()]) = 0 
						then 0
						else
							(BEG_ASSET_DOLLARS[CV()] + (ACTIVITY[CV()] * MID_PERIOD_CONV[CV()]))
							* (1 - ESTIMATED_SALVAGE[CV()])
							+ ((IMPAIRMENT_ASSET_ACTIVITY_SALV[CV()]
								+ IMPAIRMENT_ASSET_BEGIN_BALANCE[CV()]) * ESTIMATED_SALVAGE[CV()])
							- (BEG_RES_AMT[CV()] + NET_IMP_AMT[CV()] 
								+ ((ACTIVITY_3[CV()] + NET_RES_TRF[CV()]) * MID_PERIOD_CONV[CV()])
								+ (NURV_ADJ[CV()] * ( 1 - MID_PERIOD_CONV[CV()]))
								)
						end
					else -- GROSS (SCH, UOPR, RATE, SLE, FERC)
						-- EXCEPT FOR SCH, estimated_salvage is set to be 0, so has no impact on result
						case when (MID_PERIOD_CONV[CV()] = 0)
							and (EXISTS_LAST_MONTH[CV()] != 1)
							and (HAS_CFNU[CV()] = 1)
							and (ENG_IN_SERVICE_YEAR[CV()] <= ADD_MONTHS(CV(GL_POSTING_MO_YR), -1)) 
						then BEG_ASSET_DOLLARS[CV()] + ACTIVITY[CV()]
						when (BEG_ASSET_DOLLARS[CV()] + ACTIVITY[CV()]) = 0
						then 0
						else (BEG_ASSET_DOLLARS[CV()] + (ACTIVITY[CV()] * MID_PERIOD_CONV[CV()])) 
							* (1 - ESTIMATED_SALVAGE[CV()])
							+ ((IMPAIRMENT_ASSET_ACTIVITY_SALV[CV()]
								+ IMPAIRMENT_ASSET_BEGIN_BALANCE[CV()]) * ESTIMATED_SALVAGE[CV()])
						end
					end,

				-- rate for SLE and FERC is EXPENSE / BASE
				MONTH_RATE[MID_PERIOD_METHOD in ('sle', 'ferc')] = 
						case when DEPRECIATION_BASE[CV()] = 0 then 1
						else DEPR_METHOD_SLE_EXP[CV()] / DEPRECIATION_BASE[CV()] end,

				--Expense calc for all methods
				CURR_DEPR_EXPENSE[MID_PERIOD_METHOD not in ('sle', 'ferc')] = 
						DEPRECIATION_BASE[CV()] * MONTH_RATE[CV()],
				
				-- SET THE lease depr expense
				CURR_DEPR_EXPENSE[MID_PERIOD_METHOD in ('sle', 'ferc')] = 
					case when DEPRECIATION_BASE[CV()] = 0 then 0
					else DEPR_METHOD_SLE_EXP[CV()] end
			)
		) B
		on (A.ASSET_ID = B.ASSET_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.GL_POSTING_MO_YR = B.GL_POSTING_MO_YR)
		when matched then
		update
		set DEPRECIATION_BASE = B.DEPRECIATION_BASE, MONTH_RATE = B.MONTH_RATE,
			CURR_DEPR_EXPENSE = B.CURR_DEPR_EXPENSE, NET_GROSS = B.NET_GROSS,
			ACTIVITY = B.ACTIVITY,
			ACTIVITY_3 = B.ACTIVITY_3, BEG_BAL_AMT = B.BEG_BAL_AMT,
			BEG_RES_AMT = B.BEG_RES_AMT, NET_TRF = B.NET_TRF,
			NET_RES_TRF = B.NET_RES_TRF,
			NET_IMP_AMT = B.NET_IMP_AMT,
			MID_PERIOD_CONV = B.MID_PERIOD_CONV,
			DEPR_CALC_MESSAGE = 'Staged';
	
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			P_ARCHIVE();
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_CALCBASEANDRATE;


	--**************************************************************************
	--                            F_CALCREMLIFE
	--**************************************************************************
	procedure P_CALCREMLIFE 
	is
		L_STATUS varchar2(255);
	begin
		--set init life and remaining life for SLEOL for first month
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.P_CALCREMLIFE');
		
		update cpr_depr_calc_stg a
		set init_life =
				nvl2( a.end_of_life,
					greatest(1, least(a.init_life, 
									months_between(to_date(a.end_of_life,'yyyymm'), 
										to_date(to_char(a.eng_in_service_year,'yyyymm'),'yyyymm')) + 1 
								)
							),
					a.init_life
				), 
			remaining_life =
				nvl2( a.end_of_life,
					greatest(1, least(a.init_life, 
									months_between(to_date(a.end_of_life,'yyyymm'), 
										to_date(to_char(a.eng_in_service_year,'yyyymm'),'yyyymm')) + 1
								)
							),
					a.init_life
				)
		where EXISTS_LAST_MONTH = 0
		and EXISTS_ARO = 0
		and a.mid_period_method = 'sleol'
		;

		--Set the remaining life on DEPR stg for first months
		update cpr_depr_calc_stg a
		set remaining_life =
				greatest(0,
						least(a.init_life,
							a.init_life + months_between(to_date(to_char(a.eng_in_service_year,'yyyymm'),'yyyymm'), 
														a.gl_posting_mo_yr)
										+ (1 - a.mid_period_conv)
						)
				)
		where EXISTS_LAST_MONTH = 0
		and EXISTS_ARO = 0
		and a.mid_period_method <> 'custom'
		and true_up_cpr_depr in (1, 2)
		and mid_period_conv > min_mpc
		and remaining_life = init_life
		;

		--Set the remaining life on DEPR stg for existing assets with 0 mid period conv
		update cpr_depr_calc_stg a
		set remaining_life =
				greatest(0,
						least(a.init_life,
							a.init_life + months_between(to_date(to_char(a.eng_in_service_year,'yyyymm'),'yyyymm'), 
														a.gl_posting_mo_yr)
										+ (1 - a.mid_period_conv)
						)
				)
		where EXISTS_LAST_MONTH = 1
		and EXISTS_TWO_MONTHS = 0
		and EXISTS_ARO = 0
		and a.mid_period_method <> 'custom'
		and true_up_cpr_depr in (1, 2)
		and mid_period_conv = 0
		and remaining_life = init_life
		;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			P_ARCHIVE();
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_CALCREMLIFE;
	
	--**************************************************************************
	--                            F_TRUEUP
	--	TRUEUP_CPR_DEPR: 1 means cumulative (attempt to trueup every month)
	--					2 means convention (only trueup first month)
	--**************************************************************************
	procedure P_TRUEUP
	is
		L_STATUS varchar2(255);
	begin
		
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.F_TRUEUP');
		
		-- DO NOT TRUEUP records that have NURV ADJS
		merge into CPR_DEPR_CALC_STG A
		using
		(
			select *
			from
			(
				-- TRUEUP records each month if trueup cpr depr option is cumulative (1)
				--	OR trueup cpr depr is convention (2) and it does not exist last month
				select *
				from CPR_DEPR_CALC_STG B
				where B.DEPR_CALC_STATUS = 2
				and ( (exists_last_month = 0 and true_up_cpr_depr = 2)
					or (true_up_cpr_depr = 1) )
				and mid_period_method not in ('sch', 'sle', 'ferc', 'custom')
				and exists_aro = 0
				and has_nurv = 0
				and INIT_LIFE > 0
				and mid_period_conv > min_mpc
				union all
				-- TRUEUP Records in second month if the mid period convention = 0
				--		AND in the second month if the trueup cpr depr option is convention
				select *
				from CPR_DEPR_CALC_STG
				where DEPR_CALC_STATUS = 2
				and true_up_cpr_depr = 2
				and exists_last_month = 1
				and exists_two_months = 0
				and INIT_LIFE > 0
				and mid_period_method not in ('sch', 'sle', 'ferc', 'custom')
				and exists_aro = 0
				and has_nurv_last_month = 0
				and mid_period_conv = 0
				union all
				-- SCH Always gets included in trueup logic
				select *
				from CPR_DEPR_CALC_STG
				where DEPR_CALC_STATUS = 2
				and INIT_LIFE > 0
				and mid_period_method = 'sch'
				union all
				-- SLE and FERC
				select *
				from CPR_DEPR_CALC_STG
				where DEPR_CALC_STATUS = 2
				and ( (exists_last_month = 0 and true_up_cpr_depr = 2)
					or (true_up_cpr_depr = 1) )
				and INIT_LIFE > 0
				and mid_period_method in ('sle', 'ferc')
			)
			MODEL
			reference DEPR_METHOD_SLE on (select ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR,
										sum(DEPR_EXPENSE) 
											over (partition by ASSET_ID, SET_OF_BOOKS_ID order by GL_POSTING_MO_YR)
										- DEPR_EXPENSE as TOTAL_DEPR
										from DEPR_METHOD_SLE)
				DIMENSION BY (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR)
				MEASURES (TOTAL_DEPR)
			MAIN main_model
			PARTITION BY (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR)
			DIMENSION by (MID_PERIOD_METHOD)
			MEASURES
			(
				MONTH_RATE, DEPRECIATION_BASE,
				CURR_DEPR_EXPENSE, IMPAIRMENT_ASSET_ACTIVITY_SALV,
				IMPAIRMENT_ASSET_BEGIN_BALANCE, REMAINING_LIFE,
				NET_GROSS, TRUE_UP_CPR_DEPR,
				MID_PERIOD_CONV, ENG_IN_SERVICE_YEAR,
				PRODUCTION, ESTIMATED_PRODUCTION,
				RATE, INIT_LIFE,
				BEG_ASSET_DOLLARS, NET_ADDS_AND_ADJUST,
				RETIREMENTS, TRANSFERS_IN,
				TRANSFERS_OUT, IMPAIRMENT_ASSET_AMOUNT,
				ESTIMATED_SALVAGE, BEG_RESERVE_MONTH,
				DEPR_EXP_ALLOC_ADJUST, DEPR_EXP_ADJUST,
				SALVAGE_DOLLARS, COST_OF_REMOVAL,
				OTHER_CREDITS_AND_ADJUST, GAIN_LOSS,
				RESERVE_TRANS_IN, RESERVE_TRANS_OUT,
				RESERVE_ADJUSTMENT, IMPAIRMENT_EXPENSE_AMOUNT,
				TRF_WEIGHT, EXISTS_LAST_MONTH, EXISTS_TWO_MONTHS,
				HAS_CFNU, DEPR_METHOD_SLE_EXP,
				NET_TRF, NET_RES_TRF,
				NET_IMP_AMT, ACTIVITY,
				BEG_RES_AMT, ACTIVITY_3,
				BEG_BAL_AMT, SCH_RATE_USED,
				TRUEUP_ADJ, 0 as TRUEUP_BASE, 0 as RATE_USED, 0 as RESERVE_TOBE,
				0 as STRAIGHT_LINE_RATE, 0 as FIRST_HALF_RATE, 0 as DEPR_USED_FIRST_HALF, 0 as DB_FACTOR
			)
			RULES update
			(
				ESTIMATED_SALVAGE[MID_PERIOD_METHOD in ('rate', 'uopr')] =
						case when NET_GROSS[CV()] = 2 
						then 0
						else ESTIMATED_SALVAGE[CV()]
						end,
			
				-- generally the formula is
				-- GROSS amount times the rate times the "used life" - reserve				
				-- GET THE GROSS PLANT BALANCE
				TRUEUP_BASE[any] = 
						(BEG_ASSET_DOLLARS[CV()] + ACTIVITY[CV()]) * (1 - ESTIMATED_SALVAGE[CV()])
						+ ((IMPAIRMENT_ASSET_ACTIVITY_SALV[CV()] + IMPAIRMENT_ASSET_BEGIN_BALANCE[CV()])
							* ESTIMATED_SALVAGE[CV()]),
				
				-- START SETTING THE RATE_USED for each method
				RATE_USED[MID_PERIOD_METHOD in ('sl', 'sleol')] =
						(INIT_LIFE[CV()]-REMAINING_LIFE[CV()]) / INIT_LIFE[CV()],
				RATE_USED['sch'] = SCH_RATE_USED[CV()],
				RATE_USED['somd'] = 1 - (REMAINING_LIFE[CV()] * (REMAINING_LIFE[CV()]+1) 
										/ (INIT_LIFE[CV()] * (INIT_LIFE[CV()]+1))),
								
				-- RATE_USED FOR rate and uopr is dependent on whether or not net vs gross
				RATE_USED[MID_PERIOD_METHOD in ('rate', 'uopr')] =
					case when NET_GROSS[CV()] = 2
					then MONTH_RATE[CV()] * (INIT_LIFE[CV()]-REMAINING_LIFE[CV()])
					else 1 - power((1 - MONTH_RATE[CV()]), (INIT_LIFE[CV()]-REMAINING_LIFE[CV()]))
					end,
						
				--
				--	DB200 and DB150
				--	THE example below is based on DB200.  The factor of 1.5 or 2 will be used in the actual calculation
				-- IF remaining life is greater than half the initial life then the rate used is
				--		(1 - (2/INIT_LIFE)) ^ (INIT_LIFE - REMAINING_LIFE)
				--	OTHERWISE the rate to use must take into account the "first half" rate and add to it
				--		the second half rate used (which is straight lined)
				--		IE: RATE_FIRST_HALF =  (1 - (2/INIT_LIFE)) ^ (INIT_LIFE - (INIT_LIFE/2))
				--			STRAIGHT_LINE_RATE = (1 - (REMAINING_LIFE / (INIT_LIFE/2) ) )
				--			DEPR_USED_SECOND_HALF = STRAIGHT_LINE_RATE * (GROSS_PLANT - DEPR_USED_FIRST_HALF)
				--			RATE_SECOND_HALF = DEPR_USED_SECOND_HALF / GROSS_PLANT
				--		TOTAL_RATE = RATE_FIRST_HALF + RATE_SECOND_HALF
				DB_FACTOR['db200'] = 2,
				DB_FACTOR['db150'] = 1.5,
				FIRST_HALF_RATE[MID_PERIOD_METHOD in ('db200', 'db150')] = 
						1 - power((1 - (DB_FACTOR[CV()]/INIT_LIFE[CV()])),(INIT_LIFE[CV()]-trunc(INIT_LIFE[CV()]/DB_FACTOR[CV()],0))),
				STRAIGHT_LINE_RATE[MID_PERIOD_METHOD in ('db200', 'db150')] = 
						1 - (REMAINING_LIFE[CV()] / trunc(INIT_LIFE[CV()]/DB_FACTOR[CV()],0)),
				DEPR_USED_FIRST_HALF[MID_PERIOD_METHOD in ('db200', 'db150')] = TRUEUP_BASE[CV()] * FIRST_HALF_RATE[CV()],
				RATE_USED[MID_PERIOD_METHOD in ('db200', 'db150')] = 
						case when sign(REMAINING_LIFE[CV()] - (INIT_LIFE[CV()]/2)) = 1
						then (1 - power((1-(DB_FACTOR[CV()]/INIT_LIFE[CV()])), INIT_LIFE[CV()] - REMAINING_LIFE[CV()]))
						else FIRST_HALF_RATE[CV()]
							+ (STRAIGHT_LINE_RATE[CV()] * (TRUEUP_BASE[CV()] - DEPR_USED_FIRST_HALF[CV()]) / TRUEUP_BASE[CV()])
						end,
				
				
				
				RESERVE_TOBE[MID_PERIOD_METHOD in ('sle', 'ferc')] = 
					nvl(DEPR_METHOD_SLE.TOTAL_DEPR[CV(ASSET_ID), CV(SET_OF_BOOKS_ID), CV(GL_POSTING_MO_YR) ], 0),
				
				-- the total reserve that should be is base times rate
				RESERVE_TOBE[MID_PERIOD_METHOD not in ('sle', 'ferc')] = TRUEUP_BASE[CV()] * RATE_USED[CV()],

				-- adjustment is reserve to be minus the (total reserve from prior months plus current month activity)
				TRUEUP_ADJ[any] = RESERVE_TOBE[CV()] - BEG_RESERVE_MONTH[CV()] - ACTIVITY_3[CV()]
			)
		) B
		on (A.ASSET_ID = B.ASSET_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.GL_POSTING_MO_YR = B.GL_POSTING_MO_YR)
		when matched then
		update set
			TRUEUP_ADJ = B.TRUEUP_ADJ,
			DEPR_CALC_MESSAGE = 'True Up adjustment calculated.';

		-- if the remaining life is zero then always trueup the expense so fully depreciated
		-- also set the retirements to be the ending asset balance
		update CPR_DEPR_CALC_STG
		set TRUEUP_ADJ = DEPRECIATION_BASE, 
			RETIREMENTS = case when G_VERSION_ID is null
						then RETIREMENTS 
						else -1 * (BEG_ASSET_DOLLARS + NET_ADDS_AND_ADJUST + TRANSFERS_IN + TRANSFERS_OUT + IMPAIRMENT_ASSET_AMOUNT)
						end
		where DEPR_CALC_STATUS = 2
		and remaining_life = 0
		and net_gross = 1
		and mid_period_method <> 'sch';

		-- decrease the depreciable base by the amount of the trueup adj since the expense should have
		-- occurred in prior months.  Only update for NET methods since GROSS should not be based on reserve
		update CPR_DEPR_CALC_STG
		set DEPRECIATION_BASE = DEPRECIATION_BASE - TRUEUP_ADJ
		where nvl(TRUEUP_ADJ,0) <> 0
		and DEPR_CALC_STATUS = 2
		and NET_GROSS = 1;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			P_ARCHIVE();
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_TRUEUP;
	
	
	
		--**************************************************************************
	--                            P_OVER_DEPR
	--	over depr for rate and uopr
	--**************************************************************************
	procedure P_OVER_DEPR(A_MONTH date)
	is
		L_STATUS varchar2(255);
	begin
		
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.P_OVER_DEPR');

		-- decrease the depreciable base by the amount of the trueup adj since the expense should have
		-- occurred in prior months.  Only update for NET methods since GROSS should not be based on reserve
		-- CAP the over depr adjustment to curr_depr_expense.
		update CPR_DEPR_CALC_STG
		set OVER_DEPR_ADJ = 
				-1 * sign(asset_dollars) *
				least(
					abs((ASSET_DOLLARS * (1 - ESTIMATED_SALVAGE)
					+ ( IMPAIRMENT_ASSET_ACTIVITY_SALV + IMPAIRMENT_ASSET_BEGIN_BALANCE ) * ESTIMATED_SALVAGE
					) - DEPR_RESERVE),
					abs(CURR_DEPR_EXPENSE))
		where mid_period_method in ('rate', 'uopr')
		and NET_GROSS = 2
		and OVER_DEPR_CHECK = 1
		and abs(ASSET_DOLLARS) * (1 - ESTIMATED_SALVAGE) < abs(DEPR_RESERVE)
		and sign(ASSET_DOLLARS) = sign(DEPR_RESERVE)
		and GL_POSTING_MO_YR = A_MONTH
		;
		
		update CPR_DEPR_CALC_STG
		set DEPR_EXP_ALLOC_ADJUST = DEPR_EXP_ALLOC_ADJUST + OVER_DEPR_ADJ,
			DEPR_RESERVE = DEPR_RESERVE + OVER_DEPR_ADJ
		where mid_period_method in ('rate', 'uopr')
		and OVER_DEPR_ADJ <> 0
		and GL_POSTING_MO_YR = A_MONTH
		;		

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			P_ARCHIVE();
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_OVER_DEPR;

	--**************************************************************************
	--                            P_SETDEPREXPENSE
	--**************************************************************************
	procedure P_SETDEPREXPENSE(A_MONTH date)
	is
		L_STATUS varchar2(255);
	begin
	
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.P_SETDEPREXPENSE');
		
		-- do not reset the current depr expense for SLE or FERC.
		update CPR_DEPR_CALC_STG
		set CURR_DEPR_EXPENSE = case when mid_period_method in ('sle', 'ferc') then CURR_DEPR_EXPENSE else DEPRECIATION_BASE * MONTH_RATE end,
			DEPR_RESERVE = BEG_RESERVE_MONTH + RETIREMENTS + TRUEUP_ADJ + NURV_ADJ + DEPR_EXP_ADJUST
						+ SALVAGE_DOLLARS + COST_OF_REMOVAL + OTHER_CREDITS_AND_ADJUST + GAIN_LOSS
						+ RESERVE_TRANS_IN + RESERVE_TRANS_OUT + 
						(case when mid_period_method in ('sle', 'ferc') then CURR_DEPR_EXPENSE else DEPRECIATION_BASE * MONTH_RATE end)
						+ RESERVE_ADJUSTMENT + IMPAIRMENT_ASSET_AMOUNT + IMPAIRMENT_EXPENSE_AMOUNT,
			ASSET_DOLLARS = BEG_ASSET_DOLLARS + NET_ADDS_AND_ADJUST + RETIREMENTS
						+ TRANSFERS_IN + TRANSFERS_OUT + IMPAIRMENT_ASSET_AMOUNT,
			DEPR_CALC_STATUS = 9,
			DEPR_CALC_MESSAGE = 'Expense set.'
		where DEPR_CALC_STATUS = 2
		and GL_POSTING_MO_YR = A_MONTH
		;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			P_ARCHIVE();
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_SETDEPREXPENSE;
	
	--**************************************************************************
	--                            P_ROLLFWD
	--**************************************************************************
	procedure P_ROLLFWD(A_MONTH date)
	is
		L_STATUS varchar2(255);
		
		type retiredRec is record(
		  asset_id cpr_depr_calc_stg.asset_id%type,
		  set_of_books_id cpr_depr_calc_stg.set_of_books_id%type,
		  gl_posting_mo_yr cpr_depr_calc_stg.gl_posting_mo_yr%type);
		type retiredTable is table of retiredRec index by pls_integer;
		
		L_retired retiredTable;
	begin
	
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.P_ROLLFWD: REMOVE FULLY RET');
		PKG_PP_LOG.P_WRITE_MESSAGE('Rolling balances forward');
		
		select stg.asset_id, stg.set_of_books_id, stg.gl_posting_mo_yr
		bulk collect
		into L_retired
		from cpr_depr_calc_stg stg
		where gl_posting_mo_yr > A_MONTH
		and (stg.asset_id, stg.set_of_books_id) in
		(
			select stg2.asset_id, stg2.set_of_books_id
			from cpr_depr_calc_stg stg2
			where stg2.remaining_life = 0
			and stg2.asset_dollars = 0 
			and stg2.depr_reserve = 0
			and stg2.gl_posting_mo_yr = A_MONTH
		);
		
		if L_retired.COUNT > 0 then
			FORALL ndx IN INDICES OF L_retired
				delete from CPR_DEPR_CALC_STG
				where gl_posting_mo_yr = L_retired( ndx ).gl_posting_mo_yr
				and asset_id = L_retired( ndx ).asset_id
				and set_of_books_id = L_retired( ndx ).set_of_books_id
				;
		end if;
		
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.P_ROLLFWD: ROLLFWD');
		update CPR_DEPR_CALC_STG A
		   set (BEG_RESERVE_MONTH,
				 BEG_ASSET_DOLLARS,
				 YTD_DEPR_EXPENSE,
				 YTD_DEPR_EXP_ADJUST,
				 BEG_RESERVE_YEAR,
				 REMAINING_LIFE,
				 IMPAIRMENT_ASSET_BEGIN_BALANCE) =
				(select NVL(B.DEPR_RESERVE, 0),
						NVL(B.ASSET_DOLLARS, 0),
						DECODE(TO_NUMBER(TO_CHAR(B.GL_POSTING_MO_YR, 'MM')),
							   12,
							   0,
							   NVL(B.YTD_DEPR_EXPENSE, 0) + B.CURR_DEPR_EXPENSE),
						DECODE(TO_NUMBER(TO_CHAR(B.GL_POSTING_MO_YR, 'MM')),
							   12,
							   0,
							   NVL(B.YTD_DEPR_EXP_ADJUST, 0)+ + B.DEPR_EXP_ADJUST),
						DECODE(TO_NUMBER(TO_CHAR(B.GL_POSTING_MO_YR, 'MM')),
							   12,
							   NVL(B.DEPR_RESERVE, 0),
							   NVL(B.BEG_RESERVE_YEAR, 0)),
						GREATEST(B.REMAINING_LIFE -
								 DECODE(NVL(D.INIT_LIFE, -1),
										-1,
										DECODE(B.INIT_LIFE, B.REMAINING_LIFE, B.MID_PERIOD_CONV, 1),
										1),
								 0),
						B.IMPAIRMENT_ASSET_BEGIN_BALANCE + B.IMPAIRMENT_ASSET_ACTIVITY_SALV
				   from CPR_DEPR_CALC_STG B, CPR_DEPR_CALC_STG D
				  where A.ASSET_ID = B.ASSET_ID
					and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID
					and B.GL_POSTING_MO_YR = ADD_MONTHS(A.GL_POSTING_MO_YR, -1)
					and B.ASSET_ID = D.ASSET_ID(+)
					and B.SET_OF_BOOKS_ID = D.SET_OF_BOOKS_ID(+)
					and ADD_MONTHS(B.GL_POSTING_MO_YR, -1) = D.GL_POSTING_MO_YR(+)
					and B.GL_POSTING_MO_YR = A_MONTH)
			where A.GL_POSTING_MO_YR = add_months(A_MONTH,1);


		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			P_ARCHIVE();
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_ROLLFWD;	
	
	--**************************************************************************
	--                            P_NURV
	--**************************************************************************
	--
	-- This function does non-unitized reversals
	-- PREREQS: The staging table cpr_depr_calc_stg is already populated by some other means
	-- ALLOWING: this function makes the adjustments on DEPR_EXP_ALLOC_ADJUST for non-unitzed reversals
	--
	procedure P_NURV
	is
		L_STATUS varchar2(255);
	begin
	
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.P_NURV');
		
		--Adjust reserve down to 0 for 106 assets
		update CPR_DEPR_CALC_STG
		set NURV_ADJ = 
			case when BEG_ASSET_DOLLARS = 0 then
				-(BEG_RESERVE_MONTH + RETIREMENTS + DEPR_EXP_ADJUST + SALVAGE_DOLLARS +
					COST_OF_REMOVAL + OTHER_CREDITS_AND_ADJUST + GAIN_LOSS + RESERVE_TRANS_IN +
					RESERVE_TRANS_OUT + RESERVE_ADJUSTMENT + IMPAIRMENT_EXPENSE_AMOUNT +
					IMPAIRMENT_ASSET_AMOUNT)
			else
				(NET_ADDS_AND_ADJUST / BEG_ASSET_DOLLARS) *
				(BEG_RESERVE_MONTH + RETIREMENTS + DEPR_EXP_ADJUST + SALVAGE_DOLLARS +
					COST_OF_REMOVAL + OTHER_CREDITS_AND_ADJUST + GAIN_LOSS + RESERVE_TRANS_IN +
					RESERVE_TRANS_OUT + RESERVE_ADJUSTMENT + IMPAIRMENT_EXPENSE_AMOUNT +
					IMPAIRMENT_ASSET_AMOUNT)
			end
		where EXISTS_ARO = 0
		and ((HAS_NURV = 1 and MID_PERIOD_CONV > MIN_MPC)
			or (HAS_NURV_LAST_MONTH = 1 and MID_PERIOD_CONV = 0))
		and MID_PERIOD_METHOD not in ('custom', 'sch');

		--Get our 106 Assets
		insert into CPR_DEPR_CALC_NURV_STG
		(ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, WORK_ORDER_NUMBER, COST)
		(
			select distinct CDS.ASSET_ID,
				CDS.SET_OF_BOOKS_ID,
				CDS.GL_POSTING_MO_YR,
				CA.WORK_ORDER_NUMBER,
				CDS.NURV_ADJ
			from CPR_DEPR_CALC_STG CDS, CPR_ACTIVITY CA
			where CDS.ASSET_ID = CA.ASSET_ID
			and cds.MID_PERIOD_METHOD not in ('custom', 'sch')
			and ((CDS.GL_POSTING_MO_YR = CA.GL_POSTING_MO_YR and CDS.HAS_NURV = 1)
				or (CDS.GL_POSTING_MO_YR = add_months(CA.GL_POSTING_MO_YR, 1) and CDS.HAS_NURV_LAST_MONTH = 1))
		);

		--Get our 101 Assets
				--Get our 101 Assets
		insert into CPR_DEPR_CALC_CFNU_STG
		(ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, WORK_ORDER_NUMBER, COST)
		(
			select CDS.ASSET_ID,
			CDS.SET_OF_BOOKS_ID,
			CDS.GL_POSTING_MO_YR,
			CA.WORK_ORDER_NUMBER,
			sum(ca.activity_cost)
			from CPR_DEPR_CALC_STG CDS, CPR_ACTIVITY CA
			where CDS.ASSET_ID = CA.ASSET_ID
			and cds.MID_PERIOD_METHOD not in ('custom', 'sch')
			and (CDS.GL_POSTING_MO_YR = CA.GL_POSTING_MO_YR 
				or CDS.GL_POSTING_MO_YR = add_months(CA.GL_POSTING_MO_YR, 1))
			and CDS.HAS_CFNU = 1
			and trim(ca.activity_code) = 'CFNU'
			and CA.WORK_ORDER_NUMBER in
			(
				select CDN.WORK_ORDER_NUMBER
				from CPR_DEPR_CALC_NURV_STG CDN
			)
			group by CDS.ASSET_ID,
				CDS.SET_OF_BOOKS_ID,
				CDS.GL_POSTING_MO_YR,
				CA.WORK_ORDER_NUMBER
		);

		--Calculate the NURV Adjustment. This includes a correction for rounding.
		update CPR_DEPR_CALC_CFNU_STG C2
		set ADJ =
		(
			select NN.AMOUNT
			from
			(
				with THE_SPREAD as
				(
					select N.SET_OF_BOOKS_ID,
						N.WORK_ORDER_NUMBER,
						C.ASSET_ID,
						-1 * sum(N.COST) * nvl(RATIO_TO_REPORT(C.COST) OVER(partition by N.WORK_ORDER_NUMBER, N.SET_OF_BOOKS_ID), 0) AMOUNT,
						sum(N.COST) WO_COST,
						C.COST,
						RATIO_TO_REPORT(C.COST) OVER(partition by N.WORK_ORDER_NUMBER, N.SET_OF_BOOKS_ID)
					from CPR_DEPR_CALC_CFNU_STG C, CPR_DEPR_CALC_NURV_STG N
					where C.SET_OF_BOOKS_ID = N.SET_OF_BOOKS_ID
					and C.WORK_ORDER_NUMBER = N.WORK_ORDER_NUMBER
					group by N.SET_OF_BOOKS_ID, N.WORK_ORDER_NUMBER, C.ASSET_ID, C.COST
				)
				select THE_SPREAD.AMOUNT
						- DECODE(ROW_NUMBER() OVER(partition by THE_SPREAD.WORK_ORDER_NUMBER, THE_SPREAD.SET_OF_BOOKS_ID 
													order by abs(THE_SPREAD.AMOUNT) desc, THE_SPREAD.ASSET_ID desc),
								1, THE_SPREAD.WO_COST + sum(ROUND(THE_SPREAD.AMOUNT, 2))
														OVER(partition by THE_SPREAD.WORK_ORDER_NUMBER, THE_SPREAD.SET_OF_BOOKS_ID),
								0) as AMOUNT,
						THE_SPREAD.SET_OF_BOOKS_ID,
						THE_SPREAD.WORK_ORDER_NUMBER,
						THE_SPREAD.ASSET_ID,
						THE_SPREAD.WO_COST
				from THE_SPREAD
			) NN, CPR_DEPR_CALC_CFNU_STG C
			where C.SET_OF_BOOKS_ID = C2.SET_OF_BOOKS_ID
			and C.ASSET_ID = C2.ASSET_ID
			and C.WORK_ORDER_NUMBER = C2.WORK_ORDER_NUMBER
			and NN.ASSET_ID = C.ASSET_ID
			and NN.SET_OF_BOOKS_ID = C.SET_OF_BOOKS_ID
			and NN.WORK_ORDER_NUMBER = C.WORK_ORDER_NUMBER
		);

		--Move the NURV Adjustment from the CFNU table to the main calc table.
		--Also update the remaining life on the 101 Assets to have as much expired life as the 106 Assets.
		update CPR_DEPR_CALC_STG C1
		set (C1.NURV_ADJ, C1.REMAINING_LIFE) =
		(
			select nvl(ADJ, 0), greatest( C1.INIT_LIFE - WO_EXP_LIFE, 0 )
			from
			(
				with LIFE106 as
				(
					select CN.WORK_ORDER_NUMBER, C3.SET_OF_BOOKS_ID,
						min(GREATEST(0, C3.INIT_LIFE - C3.REMAINING_LIFE)) WO_EXP_LIFE
					from CPR_DEPR_CALC_STG C3, CPR_DEPR_CALC_NURV_STG CN
					where CN.ASSET_ID = C3.ASSET_ID
					group by CN.WORK_ORDER_NUMBER, C3.SET_OF_BOOKS_ID
				)
				select CFNU.ADJ, LIFE106.WO_EXP_LIFE,
					CFNU.ASSET_ID, CFNU.SET_OF_BOOKS_ID,
					CFNU.GL_POSTING_MO_YR
				from LIFE106, CPR_DEPR_CALC_CFNU_STG CFNU, CPR_DEPR_CALC_STG C2
				where LIFE106.WORK_ORDER_NUMBER = CFNU.WORK_ORDER_NUMBER
				and CFNU.ASSET_ID = C2.ASSET_ID
				and CFNU.SET_OF_BOOKS_ID = C2.SET_OF_BOOKS_ID
				and LIFE106.SET_OF_BOOKS_ID = C2.SET_OF_BOOKS_ID
				and CFNU.GL_POSTING_MO_YR = C2.GL_POSTING_MO_YR
			) WO_ARG
			where WO_ARG.GL_POSTING_MO_YR = C1.GL_POSTING_MO_YR
			and WO_ARG.ASSET_ID = C1.ASSET_ID
			and WO_ARG.SET_OF_BOOKS_ID = C1.SET_OF_BOOKS_ID
		)
		where exists
		(
			select 1
			from CPR_DEPR_CALC_CFNU_STG CDN
			where CDN.ASSET_ID = C1.ASSET_ID
			and CDN.SET_OF_BOOKS_ID = C1.SET_OF_BOOKS_ID
			and C1.GL_POSTING_MO_YR = CDN.GL_POSTING_MO_YR
		);

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			P_ARCHIVE();
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_NURV;

	--**************************************************************************
	--                            P_PREPDEPRCALC
	--**************************************************************************
	procedure P_PREPDEPRCALC is
		L_STATUS varchar2(255);
	begin
	
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.P_PREPDEPRCALC');
		
		
		--we know all we need to know to set remaining life before calc'ing, so keep it outside the loop
		P_CALCREMLIFE;
		
		for i in G_MONTHS.first .. G_MONTHS.last loop
			PKG_PP_LOG.P_WRITE_MESSAGE('Starting individual depr calculation for: '||to_char(G_MONTHS(i)));
			-- only calc NURV for actuals
			if G_VERSION_ID is null then
				P_NURV;
			end if;

			P_CALCBASEANDRATE(G_MONTHS(i));

			-- only calc trueup for actuals
			if G_VERSION_ID is null then
				P_TRUEUP;
			end if;

			P_SETDEPREXPENSE(G_MONTHS(i));
			P_OVER_DEPR(G_MONTHS(i));
			
			if i <> G_MONTHS.last then 
				P_ROLLFWD(G_MONTHS(i));
			end if;
		end loop;
		
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			P_ARCHIVE();
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_PREPDEPRCALC;


	--**************************************************************************
	--                            P_BACKFILL
	--**************************************************************************
	procedure P_BACKFILL
	is
		L_STATUS varchar2(254);
	begin
	
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.P_BACKFILL');
		
		--has_cfnu
		update CPR_DEPR_CALC_STG Z
		set HAS_CFNU = 1
		where exists
		(
			select 1
			from CPR_ACTIVITY Y
			where Y.ASSET_ID = Z.ASSET_ID
			and Y.GL_POSTING_MO_YR between add_months(Z.GL_POSTING_MO_YR, -1) and Z.GL_POSTING_MO_YR
			and trim(ACTIVITY_CODE) = 'CFNU'
		);

		--has_nurv
		update CPR_DEPR_CALC_STG Z
		set HAS_NURV = 1
		where exists
		(
			select 1
			from CPR_ACTIVITY Y
			where Y.ASSET_ID = Z.ASSET_ID
			and Y.GL_POSTING_MO_YR = Z.GL_POSTING_MO_YR
			and trim(ACTIVITY_CODE) = 'NURV'
		);

		--has_nurv_last_month
		update CPR_DEPR_CALC_STG Z
		set HAS_NURV_LAST_MONTH = 1
		where exists
		(
			select 1
			from CPR_ACTIVITY Y
			where Y.ASSET_ID = Z.ASSET_ID
			and Y.GL_POSTING_MO_YR = ADD_MONTHS(Z.GL_POSTING_MO_YR, -1)
			and trim(ACTIVITY_CODE) = 'NURV'
		);

		--estimated_production
		--production
		update CPR_DEPR_CALC_STG A
		set (ESTIMATED_PRODUCTION, PRODUCTION) =
		(
			select ESTIMATED_PRODUCTION, PRODUCTION
			from DEPR_METHOD_UOP UOP
			where UOP.DEPR_METHOD_ID = A.DEPR_METHOD_ID
			and UOP.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
			and UOP.GL_POST_MO_YR = A.GL_POSTING_MO_YR
		)
		where exists
		(
			select 1
			from DEPR_METHOD_UOP UOP
			where UOP.DEPR_METHOD_ID = A.DEPR_METHOD_ID
			and UOP.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
			and UOP.GL_POST_MO_YR = A.GL_POSTING_MO_YR
		)
		and A.MID_PERIOD_METHOD = 'uopr';

		--exists_last_month
		update CPR_DEPR_CALC_STG Z
		set EXISTS_LAST_MONTH = 1
		where exists
		(
			select 1
			from CPR_DEPR Y
			where Y.ASSET_ID = Z.ASSET_ID
			and Y.SET_OF_BOOKS_ID = Z.SET_OF_BOOKS_ID
			and Y.GL_POSTING_MO_YR = ADD_MONTHS(Z.GL_POSTING_MO_YR, -1)
		);

		--exists_two_months
		update CPR_DEPR_CALC_STG Z
		set EXISTS_TWO_MONTHS = 1
		where exists
		(
			select 1
			from CPR_DEPR Y
			where Y.ASSET_ID = Z.ASSET_ID
			and Y.SET_OF_BOOKS_ID = Z.SET_OF_BOOKS_ID
			and Y.GL_POSTING_MO_YR = ADD_MONTHS(Z.GL_POSTING_MO_YR, -2)
		);

		--exists_aro
		update CPR_DEPR_CALC_STG Y
		set EXISTS_ARO = 1
		where exists (select 1 from ARO where ARO.ASSET_ID = Y.ASSET_ID);

		--depr_method_sle_exp
		update CPR_DEPR_CALC_STG STG
		set DEPR_METHOD_SLE_EXP =
		(
			select DMS.DEPR_EXPENSE
			from DEPR_METHOD_SLE DMS
			where STG.ASSET_ID = DMS.ASSET_ID
			and STG.SET_OF_BOOKS_ID = DMS.SET_OF_BOOKS_ID
			and STG.GL_POSTING_MO_YR = DMS.GL_POSTING_MO_YR
		)
		where exists
		(
			select 1
			from DEPR_METHOD_SLE DMS
			where STG.ASSET_ID = DMS.ASSET_ID
			and STG.SET_OF_BOOKS_ID = DMS.SET_OF_BOOKS_ID
			and STG.GL_POSTING_MO_YR = DMS.GL_POSTING_MO_YR
		)
		and STG.MID_PERIOD_METHOD in ('sle', 'ferc');



		--stuff for sch
		--get the yearly rate, divide by 12 to make it monthly, and multiply by the factor
		-- THIS NEEDS TO TAKE INTO ACCOUNT FISCAL CALENDARS....
		update CPR_DEPR_CALC_STG STG
		set (STG.SCH_RATE, STG.SCH_RATE_USED) =
		(
			select NVL(S.RATE , 0) *
					case TO_CHAR(STG.GL_POSTING_MO_YR, 'mm')
					when '01' then SF.FACTOR_1
					when '02' then SF.FACTOR_2
					when '03' then SF.FACTOR_3
					when '04' then SF.FACTOR_4
					when '05' then SF.FACTOR_5
					when '06' then SF.FACTOR_6
					when '07' then SF.FACTOR_7
					when '08' then SF.FACTOR_8
					when '09' then SF.FACTOR_9
					when '10' then SF.FACTOR_10
					when '11' then SF.FACTOR_11
					when '12' then SF.FACTOR_12
					end,
				S.TOTAL_RATE -
					NVL(S.RATE , 0) * case TO_CHAR(STG.GL_POSTING_MO_YR, 'mm')
					when '01' then SF.FACTOR_1 + SF.FACTOR_2 + SF.FACTOR_3 +
					SF.FACTOR_4 + SF.FACTOR_5 +
					SF.FACTOR_6 + SF.FACTOR_7 +
					SF.FACTOR_8 + SF.FACTOR_9 +
					SF.FACTOR_10 + SF.FACTOR_11 +
					SF.FACTOR_12
					when '02' then SF.FACTOR_2 + SF.FACTOR_3 +
					SF.FACTOR_4 + SF.FACTOR_5 +
					SF.FACTOR_6 + SF.FACTOR_7 +
					SF.FACTOR_8 + SF.FACTOR_9 +
					SF.FACTOR_10 + SF.FACTOR_11 +
					SF.FACTOR_12
					when '03' then SF.FACTOR_3 +
					SF.FACTOR_4 + SF.FACTOR_5 +
					SF.FACTOR_6 + SF.FACTOR_7 +
					SF.FACTOR_8 + SF.FACTOR_9 +
					SF.FACTOR_10 + SF.FACTOR_11 +
					SF.FACTOR_12
					when '04' then SF.FACTOR_4 + SF.FACTOR_5 +
					SF.FACTOR_6 + SF.FACTOR_7 +
					SF.FACTOR_8 + SF.FACTOR_9 +
					SF.FACTOR_10 + SF.FACTOR_11 +
					SF.FACTOR_12
					when '05' then SF.FACTOR_5 +
					SF.FACTOR_6 + SF.FACTOR_7 +
					SF.FACTOR_8 + SF.FACTOR_9 +
					SF.FACTOR_10 + SF.FACTOR_11 +
					SF.FACTOR_12
					when '06' then SF.FACTOR_6 + SF.FACTOR_7 +
					SF.FACTOR_8 + SF.FACTOR_9 +
					SF.FACTOR_10 + SF.FACTOR_11 +
					SF.FACTOR_12
					when '07' then SF.FACTOR_7 +
					SF.FACTOR_8 + SF.FACTOR_9 +
					SF.FACTOR_10 + SF.FACTOR_11 +
					SF.FACTOR_12
					when '08' then SF.FACTOR_8 + SF.FACTOR_9 +
					SF.FACTOR_10 + SF.FACTOR_11 +
					SF.FACTOR_12
					when '09' then SF.FACTOR_9 +
					SF.FACTOR_10 + SF.FACTOR_11 +
					SF.FACTOR_12
					when '10' then SF.FACTOR_10 + SF.FACTOR_11 +
					SF.FACTOR_12
					when '11' then SF.FACTOR_11 +
					SF.FACTOR_12
					when '12' then SF.FACTOR_12
					end
			from DEPR_METHOD_SCHEDULE S, SPREAD_FACTOR SF, DEPR_GROUP DG
			where SF.FACTOR_ID = DG.FACTOR_ID
			and DG.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
			and S.DEPR_METHOD_ID = STG.DEPR_METHOD_ID
			and S.YEAR = TO_NUMBER(TO_CHAR(STG.GL_POSTING_MO_YR, 'yyyy')) - TO_NUMBER(TO_CHAR(stg.ENG_IN_SERVICE_YEAR, 'yyyy')) + 1
		)
		where exists
		(
			select 1
			from DEPR_METHOD_SCHEDULE S, SPREAD_FACTOR SF, DEPR_GROUP DG
			where SF.FACTOR_ID = DG.FACTOR_ID
			and DG.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
			and S.DEPR_METHOD_ID = STG.DEPR_METHOD_ID
			and S.YEAR = TO_NUMBER(TO_CHAR(STG.GL_POSTING_MO_YR, 'yyyy')) - TO_NUMBER(TO_CHAR(stg.ENG_IN_SERVICE_YEAR, 'yyyy')) + 1
		)
		and STG.MID_PERIOD_METHOD = 'sch';
		
		update CPR_DEPR_CALC_STG STG
		set STG.SCH_RATE = nvl(STG.SCH_RATE, 0),
			STG.SCH_RATE_USED = nvl(STG.SCH_RATE_USED, 1)
		where STG.MID_PERIOD_METHOD = 'sch';

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			P_ARCHIVE();
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_BACKFILL;

	--**************************************************************************
	--                            P_BACKFILL_FCST
	--**************************************************************************
	procedure P_BACKFILL_FCST
	is
		L_STATUS varchar2(254);
	begin
	
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.P_BACKFILL_FCST');
		
		--estimated_production
		--production
		update CPR_DEPR_CALC_STG A
		set (ESTIMATED_PRODUCTION, PRODUCTION) =
		(
			select ESTIMATED_PRODUCTION, PRODUCTION
			from FCST_DEPR_METHOD_UOP UOP
			where UOP.FCST_DEPR_METHOD_ID = A.DEPR_METHOD_ID
			and UOP.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
			and UOP.GL_POST_MO_YR = A.GL_POSTING_MO_YR
			and UOP.FCST_DEPR_VERSION_ID = G_VERSION_ID
		)
		where exists
		(
			select 1
			from FCST_DEPR_METHOD_UOP UOP
			where UOP.FCST_DEPR_METHOD_ID = A.DEPR_METHOD_ID
			and UOP.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
			and UOP.GL_POST_MO_YR = A.GL_POSTING_MO_YR
			and UOP.FCST_DEPR_VERSION_ID = G_VERSION_ID
		)
		and A.MID_PERIOD_METHOD = 'uopr';

		--exists_aro
		update CPR_DEPR_CALC_STG Y
		set EXISTS_ARO = 1
		where exists (select 1 from ARO where ARO.ASSET_ID = Y.ASSET_ID);

		--depr_method_sle_exp, how does one FCST lease expense? I think this is ok, but I'm unsure
		update CPR_DEPR_CALC_STG STG
		set DEPR_METHOD_SLE_EXP =
		(
			select DMS.DEPR_EXPENSE
			from DEPR_METHOD_SLE DMS
			where STG.ASSET_ID = DMS.ASSET_ID
			and STG.SET_OF_BOOKS_ID = DMS.SET_OF_BOOKS_ID
			and STG.GL_POSTING_MO_YR = DMS.GL_POSTING_MO_YR
		)
		where exists
		(
			select 1
			from DEPR_METHOD_SLE DMS
			where STG.ASSET_ID = DMS.ASSET_ID
			and STG.SET_OF_BOOKS_ID = DMS.SET_OF_BOOKS_ID
			and STG.GL_POSTING_MO_YR = DMS.GL_POSTING_MO_YR
		)
		and STG.MID_PERIOD_METHOD in ('sle', 'ferc');

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			P_ARCHIVE();
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_BACKFILL_FCST;
	
	--**************************************************************************
	--                     P_DEPRCALC
	--**************************************************************************
	--
	--  Start a depr calc for an array of companies and an array of months
	-- Start Logging
	-- Load Global Temp Staging table
	-- Calculate Deprecation
	-- Backfill results to depr ledger
	-- End logging
	--
	procedure P_DEPRCALC
	(
		A_MONTHS		PKG_PP_COMMON.DATE_TABTYPE,
		A_VERSION_ID	FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE
	)
	is
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_DEPR_IND_CALC.P_DEPRCALC');
		-- set global variables
		G_VERSION_ID := A_VERSION_ID;
		G_MONTHS := A_MONTHS;

		if G_VERSION_ID is not null then
			P_BACKFILL_FCST;
		else
			P_BACKFILL;
		end if;
		
		P_PREPDEPRCALC();
		P_ARCHIVE();

		--
		--  END LOGGING
		--
		PKG_PP_LOG.P_WRITE_MESSAGE('DONE');
		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			P_ARCHIVE();
			PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
	end P_DEPRCALC;
	
	--**************************************************************************
	--                     P_DEPRCALC
	--**************************************************************************
	--	WRAPPER
	--
	procedure P_DEPRCALC
	(
		A_MONTHS	PKG_PP_COMMON.DATE_TABTYPE
	)
	is
		L_VERSION	FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE;
	begin
		--don't register wrappers with the call stack
		L_VERSION	:= null;
		
		P_DEPRCALC(A_MONTHS, L_VERSION);
	end P_DEPRCALC;
	
	--**************************************************************************
	--                     P_DEPRCALC
	--**************************************************************************
	--
	-- WRAPPER for the start depr calc to allow single month depr calculations
	--
	procedure P_DEPRCALC
	(
		A_MONTH		date
	) 
	is
		L_MONTHS	PKG_PP_COMMON.DATE_TABTYPE;
		L_VERSION	FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE;
	begin
		--don't register wrappers with the call stack
		L_MONTHS(1) := A_MONTH;
		L_VERSION	:= null;
		
		P_DEPRCALC(L_MONTHS, L_VERSION);
		
	end P_DEPRCALC;

	--**************************************************************************
	--                            P_FCSTDEPR
	--**************************************************************************
	--
	-- WRAPPER for the fcst depr calc to allow single month depr calculations
	--
	procedure P_DEPRCALC
	(
		A_MONTH			date,
		A_VERSION_ID	FCST_DEPR_VERSION.FCST_DEPR_VERSION_ID%TYPE
	)
	is
		L_MONTHS PKG_PP_COMMON.DATE_TABTYPE;
	begin
		--don't register wrappers with the callstack
		L_MONTHS(1) := A_MONTH;
		P_DEPRCALC(L_MONTHS, A_VERSION_ID);
	end P_DEPRCALC;
	
	--
	-- WRAPPER for the fcst lease depr calc
	--
	procedure P_DEPRCALC_LEASE
	is
		L_MONTHS PKG_PP_COMMON.DATE_TABTYPE;
	begin
		select distinct GL_POSTING_MO_YR
		bulk collect
		into L_MONTHS
		from CPR_DEPR_CALC_STG
		order by GL_POSTING_MO_YR
		;
		
		G_VERSION_ID := null;
		G_MONTHS := L_MONTHS;
	
		P_PREPDEPRCALC();
	end P_DEPRCALC_LEASE;
	
end PKG_DEPR_IND_CALC;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1560, 0, 10, 4, 3, 0, 40746, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040746_depr_PKG_DEPR_IND_CALC.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;