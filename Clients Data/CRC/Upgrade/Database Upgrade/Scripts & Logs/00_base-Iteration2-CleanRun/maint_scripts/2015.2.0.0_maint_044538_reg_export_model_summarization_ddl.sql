/*
||=====================================================================================
|| Application: PowerPlan
|| File Name: maint_044538_reg_export_model_summarization_ddl.sql
||=====================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||=====================================================================================
|| Version     Date        Revised By      Reason for Change
|| ----------  ----------  --------------  --------------------------------------------
|| 2015.2.0.0  08/05/2015  Luke Warren     Regulatory Export Model Summarization Tables
||=====================================================================================
*/

-- Create table
create table REG_EXPORT_SUMMARY
(
  REG_CASE_ID             number(22) not null,
  CASE_YEAR               number(22) not null,
  REG_ACCT_TYPE_ID        number(22),
  SUB_ACCT_TYPE_ID        number(22),
  REG_FIN_MONITOR_SUMM_ID number(22),
  USER_ID                 varchar2(18),
  TIME_STAMP              date
);

comment on table REG_EXPORT_SUMMARY is '(S) [19] Regulatory Export Summarization Preferences for a Case by an Analysis Summary Key or a Sub Account Type.';

comment on column REG_EXPORT_SUMMARY.REG_CASE_ID is 'System Assigned Identifier for the Regulatory Case.';
comment on column REG_EXPORT_SUMMARY.CASE_YEAR is 'Last Month of the Case Year in YYYYMM Format.';
comment on column REG_EXPORT_SUMMARY.REG_ACCT_TYPE_ID is 'System Assigned Identifer for a Regulatory Account Type that is used as part of the unique key required to identify a Sub Account Type for a Case.';
comment on column REG_EXPORT_SUMMARY.SUB_ACCT_TYPE_ID is 'System Assigned Identifer valid for a particular Reg Account Type that is used to filter the Regulatory Export by Sub Account Type.';
comment on column REG_EXPORT_SUMMARY.REG_FIN_MONITOR_SUMM_ID is 'System Assigned Identifier for an Analysis (Monitor) Summary that is used to filter the Regulatory Export by Analysis Summary.';
comment on column REG_EXPORT_SUMMARY.USER_ID is 'Standard System-assigned user id used for audit purposes.';
comment on column REG_EXPORT_SUMMARY.TIME_STAMP is 'Standard System-assigned time stamp used for audit purposes.';

-- Create indexes 
create unique index IDX_RES_DETAILS on REG_EXPORT_SUMMARY (REG_CASE_ID, CASE_YEAR, REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID, REG_FIN_MONITOR_SUMM_ID);

-- Create foreign key constraints 
alter table REG_EXPORT_SUMMARY
  add constraint FK_RES_REG_CASE_ID foreign key (REG_CASE_ID)
  references REG_CASE (REG_CASE_ID);
alter table REG_EXPORT_SUMMARY
  add constraint FK_RES_REG_FIN_MONITOR_SUMM foreign key (REG_FIN_MONITOR_SUMM_ID)
  references REG_FIN_MONITOR_SUMMARY (REG_FIN_MONITOR_SUMM_ID);
alter table REG_EXPORT_SUMMARY
  add constraint FK_RES_SUB_ACCT_TYPE foreign key (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID)
  references REG_SUB_ACCT_TYPE (REG_ACCT_TYPE_ID, SUB_ACCT_TYPE_ID);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2746, 0, 2015, 2, 0, 0, 044538, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044538_reg_export_model_summarization_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;