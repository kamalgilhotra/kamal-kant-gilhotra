/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051379_lessee_05_import_configuration_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/5/2018 Levine - configure import for ls_asset_uop
||============================================================================
*/

DECLARE
  TEMPLATE_ID NUMBER;
BEGIN
 
  insert into pp_import_type (import_type_id, description, long_description, import_table_name, archive_table_name, allow_updates_on_add, delegate_object_name)
  values (270,'Add: LS Asset UOP', 'LS Asset UOP','LS_IMPORT_ASSET_UOP','LS_IMPORT_ASSET_UOP_ARCHIVE', 1, 'nvo_ls_logic_import');

  insert into pp_import_type_subsystem (import_type_id,import_subsystem_id)
  values (270,8);

  insert into pp_import_column (
       import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, is_on_table)
  values (
       270, 'company_id', 'Company', 'company_xlate', 1, 1, 'VARCHAR2(254)', 1);
  
  insert into pp_import_column (
       import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, is_on_table)
  values (
       270, 'ls_asset_id', 'Asset', 'ls_asset_xlate', 1, 1, 'VARCHAR2(254)', 1);

  insert into pp_import_column (
       import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, is_on_table)
  values (
       270, 'gl_posting_mo_yr', 'GL Posting Month Year', NULL, 1, 1, 'VARCHAR2(254)', 1);

  insert into pp_import_column (
       import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, is_on_table)
  values (
       270, 'estimated_production', 'Estimated Production', NULL, 1, 1, 'NUMBER(22,0)', 1);

  insert into pp_import_column (     
       import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, is_on_table)
  values (
       270, 'production', 'Production', NULL, 1, 1, 'NUMBER(22,0)', 1);
  
  -- Look up the company based on the description
  insert into pp_import_column_lookup (
         import_type_id, column_name, import_lookup_id)
  values (
         270, 'company_id', 1107);
  
  -- Look up the leased asset based on the asset number which also requires company     
  insert into pp_import_column_lookup ( 
         import_type_id, column_name, import_lookup_id)
  values (
         270, 'ls_asset_id', 1052);
  
  SELECT pp_import_template_seq.nextval INTO TEMPLATE_ID FROM DUAL;

  insert into pp_import_template (import_template_id, import_type_id, description, long_description, do_update_with_add, is_autocreate_template)
  values (TEMPLATE_ID, 270, 'LS Asset Units of Production', 'LS Asset Units of Production',1,0);

  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id) 
  values (TEMPLATE_ID, 1, 270, 'company_id', 1107);
  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id) 
  values (TEMPLATE_ID, 2, 270, 'ls_asset_id', 1052);
  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name) 
  values (TEMPLATE_ID, 3, 270, 'gl_posting_mo_yr');
  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name) 
  values (TEMPLATE_ID, 4, 270, 'estimated_production');
  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name) 
  values (TEMPLATE_ID, 5, 270, 'production');
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6682, 0, 2017, 4, 0, 0, 51379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051379_lessee_05_import_configuration_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;