/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034016_taxrpr_perse_capital_pct.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/05/2014 Alex P.
||============================================================================
*/

-- This functionality is no longer used in Tax Repairs
delete from WO_DOC_JUSTIFICATION_RULES
 where DESCRIPTION = 'Per Se Capital Percentages'
   and CATEGORY = 'Tax Status';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (823, 0, 10, 4, 2, 0, 34016, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034016_taxrpr_perse_capital_pct.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;