/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051515_lessee_01_remove_set_of_books_from_report_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  05/31/2018 David Conway     Add new lease disclosure lease classification report.
||============================================================================
*/

UPDATE PP_REPORTS
SET pp_report_time_option_id = 1
WHERE REPORT_NUMBER = 'Lessee - 2015';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6204, 0, 2017, 4, 0, 0, 51515, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051515_lessee_01_remove_set_of_books_from_report_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
