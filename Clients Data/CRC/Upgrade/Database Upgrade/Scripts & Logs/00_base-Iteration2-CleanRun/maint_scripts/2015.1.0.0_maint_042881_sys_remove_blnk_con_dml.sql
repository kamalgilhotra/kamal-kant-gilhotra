/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042881_sys_remove_blnk_con_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/24/2015 Daniel Mendel      Remove system control WOEST - REQUIRE DEPT FOR BLANKETS
||============================================================================
*/

DELETE FROM pp_system_control_company WHERE upper(control_name) = 'WOEST - REQUIRE DEPT FOR BLANKETS';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2445, 0, 2015, 1, 0, 0, 42881, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042881_sys_remove_blnk_con_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;