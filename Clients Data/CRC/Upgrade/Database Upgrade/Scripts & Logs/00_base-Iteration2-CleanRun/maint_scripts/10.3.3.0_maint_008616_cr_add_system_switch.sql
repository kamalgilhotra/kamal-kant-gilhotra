/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008616_cr_add_system_switch.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/30/2011 Sarah Byers    Point Release
||============================================================================
*/

insert into CR_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION)
values
   (267, 'CR Posting: Use Standard SAP', 'No',
    '"Yes" means that the Standard SAP code will be used by CR Posting instead of the traditional logic.  The default is "No".');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (64, 0, 10, 3, 3, 0, 8616, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008616_cr_add_system_switch.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

