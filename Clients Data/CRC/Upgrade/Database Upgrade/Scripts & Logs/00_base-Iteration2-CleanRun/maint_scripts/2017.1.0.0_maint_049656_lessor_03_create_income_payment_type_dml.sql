/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049656_lessor_03_create_income_payment_type_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/14/2017 Josh Sandler     Create Income payment type
||============================================================================
*/

INSERT INTO ls_payment_type
	(payment_type_id, description)
VALUES
	(25, 'Income')
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3953, 0, 2017, 1, 0, 0, 49656, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049656_lessor_03_create_income_payment_type_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;