/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052988_pwrtax_01_six_step_perf_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 02/07/2019 K Powers	  PowerTax Six Step performance fix
||============================================================================
*/

insert into ppbase_system_options 
(system_option_id, long_Description, system_only, pp_default_Value, option_Value, is_base_option, allow_Company_override)
values
('Deferred - Book Depr Allocate Threshold', 
'Defines the threshold to which the book depreciation allocation uses to allocate book depreciation. For example, if set to 10, the allocation will get within 10 dollars of book depreciation for the allocaiton group.',
 0, 
 '10', 
 null, 
 1, 
 1);
 
insert into ppbase_system_options_module 
(system_option_id, module) 
values ('Deferred - Book Depr Allocate Threshold', 
'powertax');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14782, 0, 2018, 2, 0, 0, 52988, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052988_pwrtax_01_six_step_perf_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;


