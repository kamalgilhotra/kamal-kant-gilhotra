/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_043985_jobserver_optional_path_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.2     07/06/2015 Chad Theilman    	 Modifying table 'PP_JOB_EXECUTABLE'
||==========================================================================================
*/

SET serveroutput ON size 30000;

declare 
  doesTableColumnExist number := 0;
  begin
       begin
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('OPTIONAL_FILE_PATH', 'PP_JOB_EXECUTABLE','PWRPLANT');
          
          if doesTableColumnExist = 0 then
            begin
              dbms_output.put_line('Creating column OPTIONAL_FILE_PATH for PP_JOB_EXECUTABLE table');
                            
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTABLE" 
              ADD 
              (
                "OPTIONAL_FILE_PATH" VARCHAR2(250)
              )';    

              execute immediate 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTABLE"."OPTIONAL_FILE_PATH" IS ''An alternative path for JobServer to run an executable From.''';
			  
            end;
          else
            begin
              dbms_output.put_line('Column OPTIONAL_FILE_PATH for PP_JOB_EXECUTABLE table exists');
            end;
          end if;
       end;
  end;
 /
 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2684, 0, 2015, 2, 0, 0, 043985, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043985_jobserver_optional_path_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;