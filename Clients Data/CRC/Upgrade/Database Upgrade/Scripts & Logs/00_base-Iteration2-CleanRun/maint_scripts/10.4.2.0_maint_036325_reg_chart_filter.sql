/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036325_reg_chart_filter.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/17/2014 Ryan Oliveria
||============================================================================
*/

--drop table REG_CHART_FILTER;

create table REG_CHART_FILTER
(
 CHART_ID       number(22,0),
 REQUESTED_USER varchar2(18),
 COLUMN_NAME    varchar2(254),
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 COLUMN_VALUE   varchar2(254)
);

alter table REG_CHART_FILTER
   add constraint PK_REG_CHART_FILTER
       primary key (CHART_ID, REQUESTED_USER, COLUMN_NAME)
       using index tablespace PWRPLANT_IDX;

alter table REG_CHART_FILTER
   add constraint FK_REG_CHART_FILTER_1
       foreign key (CHART_ID)
       references REG_CHART (CHART_ID);

comment on table REG_CHART_FILTER is 'A table to hold filter values for reg charts on the web that are viewed through PowerBuilder.';
comment on column REG_CHART_FILTER.CHART_ID is 'A reference to the REG_CHART that is being filtered.';
comment on column REG_CHART_FILTER.REQUESTED_USER is 'The username of the user that will be viewing the chart with this filter.';
comment on column REG_CHART_FILTER.COLUMN_NAME is 'The name of the column that is being filtered.';
comment on column REG_CHART_FILTER.COLUMN_VALUE is 'The value of the filter.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (966, 0, 10, 4, 2, 0, 36325, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036325_reg_chart_filter.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;