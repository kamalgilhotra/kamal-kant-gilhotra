/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049654_lessor_02_add_set_of_books_to_invoice_line_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/07/2017 Charlie Shilling need to add set of books to table (PK in later script)
||============================================================================
*/
ALTER TABLE lsr_invoice_line
ADD set_of_books_id NUMBER(22,0);

COMMENT ON COLUMN lsr_invoice_line.set_of_books_id IS 'System-assigned identifier of a unique set of books maintained by the company in PowerPlan.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3900, 0, 2017, 1, 0, 0, 49654, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049654_lessor_02_add_set_of_books_to_invoice_line_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	sys_context('USERENV', 'SERVICE_NAME'));
COMMIT;