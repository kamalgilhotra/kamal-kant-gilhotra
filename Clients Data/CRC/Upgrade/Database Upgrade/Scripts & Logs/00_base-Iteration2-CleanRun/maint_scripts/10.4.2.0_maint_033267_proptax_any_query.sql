/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_033267_proptax_any_query.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- ------------------------------------
|| 10.4.2.0 11/11/2013 Andrew Scott        Replace old query tool with base Any Query window
||============================================================================
*/

update PPBASE_WORKSPACE
   set LABEL = 'Queries', MINIHELP = 'Queries (launched in external window)'
 where LOWER(trim(MODULE)) = 'proptax'
   and WORKSPACE_UO_NAME = 'uo_ptc_rptcntr_wksp_query';

update PPBASE_MENU_ITEMS
   set LABEL = 'Queries', MINIHELP = 'Queries (launched in external window)'
 where LOWER(trim(MODULE)) = 'proptax'
   and WORKSPACE_IDENTIFIER = 'reporting_query';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (750, 0, 10, 4, 2, 0, 33267, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033267_proptax_any_query.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
