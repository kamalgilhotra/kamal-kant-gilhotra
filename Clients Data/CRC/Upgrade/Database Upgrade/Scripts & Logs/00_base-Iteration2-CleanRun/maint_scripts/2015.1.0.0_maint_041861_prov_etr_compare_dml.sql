/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041861_prov_etr_compare_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 12/23/2014 Blake Andrews    Create new ETR Comparison report
||============================================================================
*/

--Make sure this is re-runnable in case we have to local patch a client with it
delete from pp_reports where datawindow = 'dw_tax_accrual_etr_footnote_compare';
delete from tax_accrual_rep_special_crit where special_note_id = 78;
delete from tax_accrual_rep_special_note where description = 'ETR_FOOTNOTE_COMPARE';
delete from tax_accrual_report_option_dtl where report_option_id = 36;
delete from tax_accrual_report_option where report_option_id = 36;
delete from tax_accrual_rep_rollup_dtl where rep_rollup_group_id = 13;
delete from tax_accrual_rep_rollup_group where rep_rollup_group_id = 13;
delete from tax_accrual_rep_cons_cols where rep_cons_type_id = 45;
delete from tax_accrual_rep_cons_rows where rep_cons_type_id = 45;
delete from tax_accrual_rep_cons_type where rep_cons_type_id = 45;

update pp_reports
set report_id =	(	select max(report_id)
							from pp_reports
						) + 1
where report_id = 54514;

update pp_reports
set report_id =	(	select max(report_id)
							from pp_reports
						) + 1
where report_id = 54517;

--54517 replaces the 54520 Report
update pp_reports
set report_id =	(	select max(report_id)
							from pp_reports
						) + 1
where report_id = 54520;

update pp_reports
set subsystem = 'Tax Accruala'
where datawindow = 'dw_tax_accrual_etr_report_compare';
--End remove the 54520 report

insert into pp_reports
(	report_id, 
	description,
	long_description,
	subsystem,
	datawindow,
	special_note,
	report_number
)
values
(	54517,
	'ETR Footnote - Compare',
	'Comparison version of the ETR footnote (54511), comparing two time periods for the same consolidated group.',
	'Tax Accrual',
	'dw_tax_accrual_etr_footnote_compare',
	'ETR_FOOTNOTE_COMPARE',
	'Tax Accrual - 54517'
);

insert into tax_accrual_report_option
(	report_option_id,
	description
)
values
(	36,
	'GRID COMPARE - ETR'
);

insert into tax_accrual_rep_rollup_group
(	rep_rollup_group_id,
	description
)
values
(	13,
	'M Rollup and M Partial'
);

insert into tax_accrual_rep_rollup_dtl
(	rep_rollup_group_id,
	rep_rollup_id
)
values
(	13,
	1
);

insert into tax_accrual_rep_rollup_dtl
(	rep_rollup_group_id,
	rep_rollup_id
)
values
(	13,
	8
);

insert into tax_accrual_rep_special_note
(	special_note_id,
	description,
	report_option_id,
	month_required,
	compare_case,
	m_rollup_required,
	calc_rates,
	report_type,
	dw_parameter_list,
	je_temp_table_ind,
	ns2fas109_temp_table_ind,
	rep_rollup_group_id,
	default_acct_rollup,
	rep_cons_category_id
)
values
(	78,
	'ETR_FOOTNOTE_COMPARE',
	36,
	0,
	1,
	0,
	0,
	0,
	5,
	0,
	0,
	13,
	null,
	8
);

insert into tax_accrual_report_option_dtl
(	report_option_id,
	report_object_id
)
select	36,
			report_object_id
from tax_accrual_report_object
where report_object_id in	(	3, --Tabpage GL Account
										44, 21, --Dampening Type
										43, 20, --Deferred Type
										55, 56, --JE Type
										40, 18, 60, 59, 58, 57, --GL Month objects on General Tab
										41,19, --Entity objects on General tab
										59, --Consolidating Type
										33, 12, --M Rollup Group
										15, 51, --M Item
										34, --CB Select Rollup
										38 --CB Filter
									);

insert into tax_accrual_rep_special_crit
(	special_note_id,
	special_crit_id,
	description,
	long_description
)
values
(	78,
	5,
	'NO_STATE_IN_DISCRETE',
	'Roll State Discretes in Total State'
);

insert into tax_accrual_rep_cons_type
(	rep_cons_type_id,
	description,
	datawindow,
	sort_spec,
	rep_cons_category_id,
	balances_ind
)
values
(	45,
	'ETR Footnote Compare',
	'dw_tax_accrual_etr_footnote_compare',
	'report_section, rollup_description, detail_description',
	8,
	2
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	45,
	'compare_id',
	1,
	'number',
	'column_header',
	0
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	45,
	'detail_description',
	1,
	'string',
	'detail_description',
	1
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	45,
	'rollup_description',
	1,
	'string',
	'rollup_description',
	2
);

insert into tax_accrual_rep_cons_cols
(	rep_cons_type_id,
	cons_colname,
	rank,
	cons_coltype,
	cons_descr_colname,
	group_id
)
values
(	45,
	'report_section',
	1,
	'number',
	'report_section',
	3
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	1, --row_id
	'H', --row_type
	-1, --group_id
	1, --rank
	1, --color_id
	'TEXT', --value_type
	1, --is_report_title
	'Tax Footnote ETR Comparison', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	2, --row_id
	'H', --row_type
	-1, --group_id
	2, --rank
	1, --color_id
	'USER_TITLE', --value_type
	0, --is_report_title
	null, --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	3, --row_id
	'H', --row_type
	-1, --group_id
	3, --rank
	1, --color_id
	'COL_VALUE_TEXT', --value_type
	0, --is_report_title
	'oper_ind_label', --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	4, --row_id
	'H', --row_type
	-1, --group_id
	4, --rank
	1, --color_id
	'FILLER', --value_type
	0, --is_report_title
	null, --row_value
	null, --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	5, --row_id
	'H', --row_type
	0, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'ptbi', --row_value
	'TEXT', --label_value_type
	'Book Income', --label_value
	'T', --col_format
	null, --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	6, --row_id
	'H', --row_type
	0, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'tax_items	', --row_value
	'TEXT', --label_value_type
	'Tax Items', --label_value
	'T', --col_format
	null, --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	7, --row_id
	'H', --row_type
	0, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_h1_bibit', --row_value
	'TEXT', --label_value_type
	'Book Income Before Income Taxes (Adjusted for Tax Items)', --label_value
	'T', --col_format
	null, --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	8, --row_id
	'H', --row_type
	0, --group_id
	4, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'FILLER', --label_value_type
	null, --label_value
	'T', --col_format
	null, --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	9, --row_id
	'D', --row_type
	1, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tt', --row_value
	'COL_VALUE_TEXT', --label_value_type
	'detail_description', --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	10, --row_id
	'D', --row_type
	1, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'state_expense', --row_value
	'TEXT', --label_value_type
	'State Tax Expense', --label_value
	'T', --col_format
	'state_expense_display', --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	11, --row_id
	'D', --row_type
	1, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'deduct_fed', --row_value
	'TEXT', --label_value_type
	'Fed Benefit of State Tax Deduction', --label_value
	'T', --col_format
	'state_expense_display', --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	12, --row_id
	'F', --row_type
	2, --group_id
	1, --rank
	1, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'COL_VALUE_TEXT', --label_value_type
	'rollup_description', --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	13, --row_id
	'F', --row_type
	2, --group_id
	2, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'FILLER', --label_value_type
	null, --label_value
	'T', --col_format
	'display_detail_row', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	14, --row_id
	'H', --row_type
	2, --group_id
	1, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'COL_VALUE_TEXT', --label_value_type
	'rollup_description', --label_value
	'T', --col_format
	'if(report_section=10,0,display_detail_row)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	15, --row_id
	'H', --row_type
	2, --group_id
	2, --rank
	0, --color_id
	'TEXT', --value_type
	0, --is_report_title
	null, --row_value
	'TEXT', --label_value_type
	' ', --label_value
	'T', --col_format
	'if(report_section=10,1,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	16, --row_id
	'F', --row_type
	3, --group_id
	1, --rank
	1, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tte_b4_discretes_tt', --row_value
	'TEXT', --label_value_type
	'Tax Expense (Benefit) Before Discrete Items', --label_value
	'T', --col_format
	'if(report_section=40,1,0)', --display_ind_field
	null --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	17, --row_id
	'F', --row_type
	99, --group_id
	1, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'curtax_not_expense_fed', --row_value
	'TEXT', --label_value_type
	'Current Tax Not Booked to Expense', --label_value
	'T', --col_format
	'if(report_section=40,show_curtax_not_expense_cons,0)', --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	18, --row_id
	'F', --row_type
	99, --group_id
	2, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_dtl_tte_after_discretes_tt', --row_value
	'TEXT', --label_value_type
	'Total Tax Expense (Benefit) With Discrete Items', --label_value
	'T', --col_format
	null, --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	19, --row_id
	'F', --row_type
	99, --group_id
	3, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'total_tax_expense', --row_value
	'TEXT', --label_value_type
	'Tax Expense Booked', --label_value
	'T', --col_format
	null, --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	20, --row_id
	'F', --row_type
	99, --group_id
	4, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'comp_tt_difference', --row_value
	'TEXT', --label_value_type
	'Difference', --label_value
	'T', --col_format
	null, --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	21, --row_id
	'F', --row_type
	99, --group_id
	5, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'total_co_etr', --row_value
	'TEXT', --label_value_type
	'ETR Percentage', --label_value
	'P', --col_format
	null, --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

insert into tax_accrual_rep_cons_rows
(	rep_cons_type_id,
	row_id,
	row_type,
	group_id,
	rank,
	color_id,
	value_type,
	is_report_title,
	row_value,
	label_value_type,
	label_value,
	col_format,
	display_ind_field,
	total_col_fieldname
)
values
(	45, --rep_cons_type_id
	22, --row_id
	'F', --row_type
	99, --group_id
	6, --rank
	0, --color_id
	'COL_VALUE_NUM', --value_type
	0, --is_report_title
	'all_co_etr_total', --row_value
	'TEXT', --label_value_type
	'Total ETR (Both Periods)', --label_value
	'P', --col_format
	null, --display_ind_field
	'SPREADSHEETDIFF(A-B)' --total_col_fieldname
);

commit;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2137, 0, 2015, 1, 0, 0, 041861, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041861_prov_etr_compare_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;