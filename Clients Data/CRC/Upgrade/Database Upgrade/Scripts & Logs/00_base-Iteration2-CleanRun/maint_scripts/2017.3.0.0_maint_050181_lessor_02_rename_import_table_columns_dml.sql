/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050181_lessor_02_rename_import_table_columns_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 04/05/2018 Andrew Hill       Rename ILR Import Columns for Override Rates
||============================================================================
*/

DECLARE
  l_id number;
BEGIN
  FOR FIELD IN (SELECT import_template_id, field_id
                FROM pp_import_template_fields
                WHERE LOWER(TRIM(column_name)) = 'direct_fin_fmv_rate'
                AND import_type_id = 502)
  LOOP
    DELETE FROM pp_import_template_fields
    WHERE import_template_id = FIELD.import_template_id
    AND field_id = field.field_id;
    
    UPDATE pp_import_template_fields
    SET field_id = field_id - 1
    WHERE import_template_id = FIELD.import_template_id
    AND field_id > FIELD.field_id;
  END LOOP;
END;
/

UPDATE pp_import_template_fields
SET column_name = 'sales_type_disc_rate_override'
WHERE lower(trim(column_name)) = 'sales_type_disc_rate'
and import_type_id = 502;

UPDATE pp_import_template_fields
SET column_name = 'direct_fin_disc_rate_override'
WHERE lower(trim(column_name)) = 'direct_fin_disc_rate'
and import_type_id = 502;

UPDATE pp_import_template_fields
SET column_name = 'direct_fin_int_rate_override'
WHERE lower(trim(column_name)) = 'direct_fin_int_rate'
and import_type_id = 502;

UPDATE pp_import_column
SET column_name = 'sales_type_disc_rate_override',
    DESCRIPTION = 'Override ST Discount Rate'
WHERE lower(trim(column_name)) = 'sales_type_disc_rate'
and import_type_id = 502;

UPDATE pp_import_column
SET column_name = 'direct_fin_disc_rate_override',
    DESCRIPTION = 'Override DF Discount Rate'
WHERE lower(trim(column_name)) = 'direct_fin_disc_rate'
and import_type_id = 502;

UPDATE pp_import_column
SET column_name = 'direct_fin_int_rate_override',
    DESCRIPTION = 'Override DF Int on Net Inv Rate'
WHERE lower(trim(column_name)) = 'direct_fin_int_rate'
and import_type_id = 502;

DELETE FROM pp_import_column
WHERE LOWER(TRIM(column_name)) = 'direct_fin_fmv_rate'
AND import_type_id = 502;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4277, 0, 2017, 3, 0, 0, 50181, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050181_lessor_02_rename_import_table_columns_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 
