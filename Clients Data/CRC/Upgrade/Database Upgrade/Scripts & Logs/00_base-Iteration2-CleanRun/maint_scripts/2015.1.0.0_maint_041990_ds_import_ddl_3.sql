/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_041990_ds_import_ddl_3.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   01/19/2015 A Scott          Import tool setup for depr studies.
||                                      Changes made based on QA, including
||                                      missing creation activity.
||============================================================================
*/

alter table ds_import_reserve
add 
(
  sum_factor                NUMBER(22,8),
  final_amount              NUMBER(22,2)
);

comment on column ds_import_reserve.sum_factor is 'Sum factor populated from the analysis transaction code.';
comment on column ds_import_reserve.final_amount is 'Amount imported from file times the sum factor field from the corresponding analysis transaction code.';


alter table ds_import_reserve_arc
add 
(
  sum_factor                NUMBER(22,8),
  final_amount              NUMBER(22,2)
);

comment on column ds_import_reserve_arc.sum_factor is 'Sum factor populated from the analysis transaction code.';
comment on column ds_import_reserve_arc.final_amount is 'Amount imported from file times the sum factor field from the corresponding analysis transaction code.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2198, 0, 2015, 1, 0, 0, 41990, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041990_ds_import_ddl_3.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;