/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_051540_lessee_04_query_pymt_adj_MLA_DML.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- --------------------------------------
 || 2017.4.0   06/11/2018   K. Powers   Add Exec and Cont Payment Adj fields 
 ||============================================================================
 */ 

DECLARE

  query_id number;
  exec_last_col number;
  cont_last_col number;

BEGIN

  delete from pp_any_query_criteria_fields 
  where detail_field in ('executory_adjust','contingent_adjust');
  
  select id into query_id from pp_any_query_criteria 
  WHERE description = 'Lease Asset Schedule By MLA';
  
  select column_order into exec_last_col 
  from pp_any_query_criteria_fields  
  WHERE id = query_id 
  AND detail_field = 'executory_paid10';
  
  update pp_any_query_criteria_fields 
  set column_order = column_order + 1
  where column_order > exec_last_col;

  select column_order into cont_last_col 
  from pp_any_query_criteria_fields  
  WHERE id = query_id 
  AND detail_field = 'contingent_paid10';
  
  update pp_any_query_criteria_fields 
  set column_order = column_order + 1
  where column_order > cont_last_col;

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     TIME_STAMP,
     USER_ID,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (query_id,
     'executory_adjust',
     exec_last_col + 1,
     to_date(sysdate, 'yyyy-mm-dd hh24:mi:ss'),
     'PWRPLANT',
     1,
     1,
     null,
     'Executory Adjust',
     300,
     null,
     null,
     'NUMBER',
     null,
     0,
     null,
     null,
     null);
     
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     TIME_STAMP,
     USER_ID,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (query_id,
     'contingent_adjust',
     cont_last_col + 1,
     to_date(sysdate, 'yyyy-mm-dd hh24:mi:ss'),
     'PWRPLANT',
     1,
     1,
     null,
     'Contingent Adjust',
      300,
     null,
     null,
     'NUMBER',
     null,
     0,
     null,
     null,
     null);

   update pp_any_query_criteria
     set SQL = 'SELECT la.ls_asset_id,la.leased_asset_number,co.company_id,co.description AS company_description,
                ilr.ilr_id,ilr.ilr_number,ll.lease_id,ll.lease_number,lct.description AS lease_cap_type,al.long_description AS location,
                las.REVISION,las.SET_OF_BOOKS_ID,TO_CHAR(las.MONTH, ''yyyymm'') AS monthnum,lcurt.DESCRIPTION AS currency_type,
                las.iso_code currency,las.currency_display_symbol,las.BEG_CAPITAL_COST,las.END_CAPITAL_COST,las.BEG_OBLIGATION,
                las.END_OBLIGATION,las.BEG_LT_OBLIGATION,las.END_LT_OBLIGATION,las.INTEREST_ACCRUAL,las.PRINCIPAL_ACCRUAL,
                las.INTEREST_PAID,las.PRINCIPAL_PAID,las.EXECUTORY_ACCRUAL1,las.EXECUTORY_ACCRUAL2,las.EXECUTORY_ACCRUAL3,
                las.EXECUTORY_ACCRUAL4,las.EXECUTORY_ACCRUAL5,las.EXECUTORY_ACCRUAL6,las.EXECUTORY_ACCRUAL7,las.EXECUTORY_ACCRUAL8,
                las.EXECUTORY_ACCRUAL9,las.EXECUTORY_ACCRUAL10,las.EXECUTORY_PAID1,las.EXECUTORY_PAID2,las.EXECUTORY_PAID3,
                las.EXECUTORY_PAID4,las.EXECUTORY_PAID5,las.EXECUTORY_PAID6,las.EXECUTORY_PAID7,las.EXECUTORY_PAID8,
                las.EXECUTORY_PAID9,las.EXECUTORY_PAID10,las.EXECUTORY_ADJUST,las.CONTINGENT_ACCRUAL1,las.CONTINGENT_ACCRUAL2,las.CONTINGENT_ACCRUAL3,
                las.CONTINGENT_ACCRUAL4,las.CONTINGENT_ACCRUAL5,las.CONTINGENT_ACCRUAL6,las.CONTINGENT_ACCRUAL7,
                las.CONTINGENT_ACCRUAL8,las.CONTINGENT_ACCRUAL9,las.CONTINGENT_ACCRUAL10,las.CONTINGENT_PAID1,
                las.CONTINGENT_PAID2,las.CONTINGENT_PAID3,las.CONTINGENT_PAID4,las.CONTINGENT_PAID5,las.CONTINGENT_PAID6,
                las.CONTINGENT_PAID7,las.CONTINGENT_PAID8,las.CONTINGENT_PAID9,las.CONTINGENT_PAID10,las.CONTINGENT_ADJUST,las.IS_OM,
                las.CURRENT_LEASE_COST,las.RESIDUAL_AMOUNT,las.TERM_PENALTY,las.BPO_PRICE,las.BEG_DEFERRED_RENT,
                las.DEFERRED_RENT,las.END_DEFERRED_RENT,las.BEG_LIABILITY,las.END_LIABILITY,las.BEG_LT_LIABILITY,
                las.END_LT_LIABILITY 
              FROM ls_asset la,ls_ilr ilr,ls_lease ll,company co,asset_location al,
                ls_lease_cap_type lct,ls_ilr_options ilro,v_ls_asset_schedule_fx_vw las,ls_lease_currency_type lcurt
              WHERE la.ilr_id             = ilr.ilr_id
              AND ilr.lease_id            = ll.lease_id
              AND ilr.ilr_id              = ilro.ilr_id
              AND ilr.current_revision    = ilro.revision
              AND ilro.lease_cap_type_id  = lct.ls_lease_cap_type_id
              AND la.asset_location_id    = al.asset_location_id
              AND la.company_id           = co.company_id
              AND las.ls_asset_id         = la.ls_asset_id
              AND las.ls_cur_type         = lcurt.ls_currency_type_id
              AND las.revision            = la.approved_revision
              AND upper(ll.lease_number) IN
                (SELECT upper(filter_value)
                FROM pp_any_required_filter
                WHERE upper(trim(column_name)) = ''LEASE NUMBER''
                )
              AND UPPER(TRIM(lcurt.DESCRIPTION)) IN
                (SELECT UPPER(filter_value)
                FROM pp_any_required_filter
                WHERE Upper(Trim(column_name)) = ''CURRENCY TYPE'')'
                where id=query_id;
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6830, 0, 2017, 4, 0, 0, 51540, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051540_lessee_04_query_pymt_adj_MLA_DML.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

