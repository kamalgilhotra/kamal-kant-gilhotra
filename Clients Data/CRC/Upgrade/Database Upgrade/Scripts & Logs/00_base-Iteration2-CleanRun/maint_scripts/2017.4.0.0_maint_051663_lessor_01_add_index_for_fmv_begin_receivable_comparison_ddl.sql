/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051663_lessor_01_add_index_for_fmv_begin_receivable_comparison_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 06/07/2018 Andrew Hill    Add index to improve performance of fmv/begin receivable comparison
||============================================================================
*/

create index lsr_ilr_sch_fmv_begrec_tst_idx on lsr_ilr_schedule(ilr_id, revision, set_of_books_id, month, beg_receivable) tablespace pwrplant_idx;

--***********************************************
--Log the run of the script PP_SCHEMA_CGANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6462, 0, 2017, 4, 0, 0, 51663, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051663_lessor_01_add_index_for_fmv_begin_receivable_comparison_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;