/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_27_lease_import_new_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

alter table ls_import_lease
add (lease_end_date varchar2(254), days_in_month_sw varchar2(35))
;

alter table ls_import_lease_archive
add (lease_end_date varchar2(254), days_in_month_sw varchar2(35))
;

COMMENT ON COLUMN ls_import_lease.lease_end_date IS 'End date for any associated ILR payment terms to be within.';
COMMENT ON COLUMN ls_import_lease.days_in_month_sw IS 'Switch indicating number of days in month used for interim interest. 0 is 30 days, 1 is actual days in month';

COMMENT ON COLUMN ls_import_lease_archive.lease_end_date IS 'End date for any associated ILR payment terms to be within.';
COMMENT ON COLUMN ls_import_lease_archive.days_in_month_sw IS 'Switch indicating number of days in month used for interim interest. 0 is 30 days, 1 is actual days in month';





--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2423, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_27_lease_import_new_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;