/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_041643_lease_float_rates_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.2 01/18/2015 		B.Beck			Load ls ilr floating rates into table maintenance
||==========================================================================================
*/

insert into powerplant_tables
(
table_name, pp_table_type_id, description,
subsystem_screen, subsystem_display, subsystem
)
values
(
'ls_ilr_group_floating_rates', 's', 'Lease Floating Rates',
'lessee', 'lessee', 'lessee'
);

insert into powerplant_columns
(
column_name, table_name, dropdown_name, pp_edit_type_id,
description, column_rank, read_only
)
values
(
'lease_id', 'ls_ilr_group_floating_rates', 'ls_lease', 'p',
'Lease', 1, 0
);

insert into powerplant_columns
(
column_name, table_name, dropdown_name, pp_edit_type_id,
description, column_rank, read_only
)
values
(
'ilr_group_id', 'ls_ilr_group_floating_rates', 'ls_ilr_group_search', 'p',
'ILR Group', 2, 0
);

insert into powerplant_columns
(
column_name, table_name, dropdown_name, pp_edit_type_id,
description, column_rank, read_only
)
values
(
'effective_date', 'ls_ilr_group_floating_rates', null, 'e',
'Effective Date', 3, 0
);

insert into powerplant_columns
(
column_name, table_name, dropdown_name, pp_edit_type_id,
description, column_rank, read_only
)
values
(
'rate', 'ls_ilr_group_floating_rates', null, 'e',
'Rate', 4, 0
);

insert into powerplant_columns
(
column_name, table_name, dropdown_name, pp_edit_type_id,
description, column_rank, read_only
)
values
(
'time_stamp', 'ls_ilr_group_floating_rates', null, 'e',
'Time Stamp', 98, 0
);

insert into powerplant_columns
(
column_name, table_name, dropdown_name, pp_edit_type_id,
description, column_rank, read_only
)
values
(
'user_id', 'ls_ilr_group_floating_rates', null, 'e',
'User', 99, 0
);



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2224, 0, 10, 4, 3, 2, 041643, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041643_lease_float_rates_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;