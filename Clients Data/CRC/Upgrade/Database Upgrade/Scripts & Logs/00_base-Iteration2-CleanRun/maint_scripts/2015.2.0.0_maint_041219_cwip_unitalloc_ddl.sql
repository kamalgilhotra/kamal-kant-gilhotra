/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_041219_cwip_unitalloc_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2     08/5/2015   Sunjin Cone	  Add unique index to Unit_Alloc_Basis
||============================================================================
*/

ALTER TABLE unit_alloc_basis
ADD CONSTRAINT unitalloc_estchg UNIQUE (alloc_id, est_chg_type_id, charge_type_id)
USING INDEX TABLESPACE PWRPLANT_IDX;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2751, 0, 2015, 2, 0, 0, 041219, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_041219_cwip_unitalloc_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;