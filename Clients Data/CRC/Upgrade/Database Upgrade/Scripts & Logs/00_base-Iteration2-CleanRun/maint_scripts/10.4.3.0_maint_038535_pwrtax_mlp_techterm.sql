/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038535_pwrtax_mlp_techterm.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 07/31/2014 Anand Rajashekar    Asset Technical Termination
||========================================================================================
*/

--Add Tax Event

insert into TAX_EVENT
   (TAX_EVENT_ID, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (4, TO_DATE('30-JUL-14', 'DD-MON-RR'), 'PWRPLANT', 'Technical Termination');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1310, 0, 10, 4, 3, 0, 38535, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038535_pwrtax_mlp_techterm.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;