/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034642_lease_ILR_account_imports.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/05/2014 Kyle Peterson
||============================================================================
*/

alter table LS_IMPORT_ILR
   add (INT_ACCRUAL_ACCOUNT_XLATE  varchar2(254),
        INT_ACCRUAL_ACCOUNT_ID     number(22,0),
        INT_EXPENSE_ACCOUNT_XLATE  varchar2(254),
        INT_EXPENSE_ACCOUNT_ID     number(22,0),
        EXEC_ACCRUAL_ACCOUNT_XLATE varchar2(254),
        EXEC_ACCRUAL_ACCOUNT_ID    number(22,0),
        EXEC_EXPENSE_ACCOUNT_XLATE varchar2(254),
        EXEC_EXPENSE_ACCOUNT_ID    number(22,0),
        CONT_ACCRUAL_ACCOUNT_XLATE varchar2(254),
        CONT_ACCRUAL_ACCOUNT_ID    number(22,0),
        CONT_EXPENSE_ACCOUNT_XLATE varchar2(254),
        CONT_EXPENSE_ACCOUNT_ID    number(22,0),
        CAP_ASSET_ACCOUNT_XLATE    varchar2(254),
        CAP_ASSET_ACCOUNT_ID       number(22,0),
        ST_OBLIG_ACCOUNT_XLATE     varchar2(254),
        ST_OBLIG_ACCOUNT_ID        number(22,0),
        LT_OBLIG_ACCOUNT_XLATE     varchar2(254),
        LT_OBLIG_ACCOUNT_ID        number(22,0),
        AP_ACCOUNT_XLATE           varchar2(254),
        AP_ACCOUNT_ID              number(22,0));

comment on column LS_IMPORT_ILR."INT_ACCRUAL_ACCOUNT_XLATE" is 'Translation field for determining interest accrual account.';
comment on column LS_IMPORT_ILR."INT_ACCRUAL_ACCOUNT_ID" is 'The interest accrual account.';
comment on column LS_IMPORT_ILR."INT_EXPENSE_ACCOUNT_XLATE" is 'Translation field for determining interest expense account.';
comment on column LS_IMPORT_ILR."INT_EXPENSE_ACCOUNT_ID" is 'The interest expense account.';
comment on column LS_IMPORT_ILR."EXEC_ACCRUAL_ACCOUNT_XLATE" is 'Translation field for determining executory accrual account.';
comment on column LS_IMPORT_ILR."EXEC_ACCRUAL_ACCOUNT_ID" is 'The executory accrual account.';
comment on column LS_IMPORT_ILR."EXEC_EXPENSE_ACCOUNT_XLATE" is 'Translation field for determining executory expense account.';
comment on column LS_IMPORT_ILR."EXEC_EXPENSE_ACCOUNT_ID" is 'The executory expense account.';
comment on column LS_IMPORT_ILR."CONT_ACCRUAL_ACCOUNT_XLATE" is 'Translation field for determining contingent accrual account.';
comment on column LS_IMPORT_ILR."CONT_ACCRUAL_ACCOUNT_ID" is 'The contingent accrual account.';
comment on column LS_IMPORT_ILR."CONT_EXPENSE_ACCOUNT_XLATE" is 'Translation field for determining contingent expense account.';
comment on column LS_IMPORT_ILR."CONT_EXPENSE_ACCOUNT_ID" is 'The contingent expense account.';
comment on column LS_IMPORT_ILR."CAP_ASSET_ACCOUNT_XLATE" is 'Translation field for determining capital asset account.';
comment on column LS_IMPORT_ILR."CAP_ASSET_ACCOUNT_ID" is 'The capital asset account.';
comment on column LS_IMPORT_ILR."ST_OBLIG_ACCOUNT_XLATE" is 'Translation field for determining short-term obligation account.';
comment on column LS_IMPORT_ILR."ST_OBLIG_ACCOUNT_ID" is 'The short-term obligation account.';
comment on column LS_IMPORT_ILR."LT_OBLIG_ACCOUNT_XLATE" is 'Translation field for determining long-term obligation account.';
comment on column LS_IMPORT_ILR."LT_OBLIG_ACCOUNT_ID" is 'The long-term obligation account.';
comment on column LS_IMPORT_ILR."AP_ACCOUNT_XLATE" is 'Translation field for determining AP account.';
comment on column LS_IMPORT_ILR."AP_ACCOUNT_ID" is 'The AP account.';

alter table LS_IMPORT_ILR_ARCHIVE
   add (INT_ACCRUAL_ACCOUNT_XLATE  varchar2(254),
        INT_ACCRUAL_ACCOUNT_ID     number(22,0),
        INT_EXPENSE_ACCOUNT_XLATE  varchar2(254),
        INT_EXPENSE_ACCOUNT_ID     number(22,0),
        EXEC_ACCRUAL_ACCOUNT_XLATE varchar2(254),
        EXEC_ACCRUAL_ACCOUNT_ID    number(22,0),
        EXEC_EXPENSE_ACCOUNT_XLATE varchar2(254),
        EXEC_EXPENSE_ACCOUNT_ID    number(22,0),
        CONT_ACCRUAL_ACCOUNT_XLATE varchar2(254),
        CONT_ACCRUAL_ACCOUNT_ID    number(22,0),
        CONT_EXPENSE_ACCOUNT_XLATE varchar2(254),
        CONT_EXPENSE_ACCOUNT_ID    number(22,0),
        CAP_ASSET_ACCOUNT_XLATE    varchar2(254),
        CAP_ASSET_ACCOUNT_ID       number(22,0),
        ST_OBLIG_ACCOUNT_XLATE     varchar2(254),
        ST_OBLIG_ACCOUNT_ID        number(22,0),
        LT_OBLIG_ACCOUNT_XLATE     varchar2(254),
        LT_OBLIG_ACCOUNT_ID        number(22,0),
        AP_ACCOUNT_XLATE           varchar2(254),
        AP_ACCOUNT_ID              number(22,0));

comment on column LS_IMPORT_ILR_ARCHIVE."INT_ACCRUAL_ACCOUNT_XLATE" is 'Translation field for determining interest accrual account.';
comment on column LS_IMPORT_ILR_ARCHIVE."INT_ACCRUAL_ACCOUNT_ID" is 'The interest accrual account.';
comment on column LS_IMPORT_ILR_ARCHIVE."INT_EXPENSE_ACCOUNT_XLATE" is 'Translation field for determining interest expense account.';
comment on column LS_IMPORT_ILR_ARCHIVE."INT_EXPENSE_ACCOUNT_ID" is 'The interest expense account.';
comment on column LS_IMPORT_ILR_ARCHIVE."EXEC_ACCRUAL_ACCOUNT_XLATE" is 'Translation field for determining executory accrual account.';
comment on column LS_IMPORT_ILR_ARCHIVE."EXEC_ACCRUAL_ACCOUNT_ID" is 'The executory accrual account.';
comment on column LS_IMPORT_ILR_ARCHIVE."EXEC_EXPENSE_ACCOUNT_XLATE" is 'Translation field for determining executory expense account.';
comment on column LS_IMPORT_ILR_ARCHIVE."EXEC_EXPENSE_ACCOUNT_ID" is 'The executory expense account.';
comment on column LS_IMPORT_ILR_ARCHIVE."CONT_ACCRUAL_ACCOUNT_XLATE" is 'Translation field for determining contingent accrual account.';
comment on column LS_IMPORT_ILR_ARCHIVE."CONT_ACCRUAL_ACCOUNT_ID" is 'The contingent accrual account.';
comment on column LS_IMPORT_ILR_ARCHIVE."CONT_EXPENSE_ACCOUNT_XLATE" is 'Translation field for determining contingent expense account.';
comment on column LS_IMPORT_ILR_ARCHIVE."CONT_EXPENSE_ACCOUNT_ID" is 'The contingent expense account.';
comment on column LS_IMPORT_ILR_ARCHIVE."CAP_ASSET_ACCOUNT_XLATE" is 'Translation field for determining capital asset account.';
comment on column LS_IMPORT_ILR_ARCHIVE."CAP_ASSET_ACCOUNT_ID" is 'The capital asset account.';
comment on column LS_IMPORT_ILR_ARCHIVE."ST_OBLIG_ACCOUNT_XLATE" is 'Translation field for determining short-term obligation account.';
comment on column LS_IMPORT_ILR_ARCHIVE."ST_OBLIG_ACCOUNT_ID" is 'The short-term obligation account.';
comment on column LS_IMPORT_ILR_ARCHIVE."LT_OBLIG_ACCOUNT_XLATE" is 'Translation field for determining long-term obligation account.';
comment on column LS_IMPORT_ILR_ARCHIVE."LT_OBLIG_ACCOUNT_ID" is 'The long-term obligation account.';
comment on column LS_IMPORT_ILR_ARCHIVE."AP_ACCOUNT_XLATE" is 'Translation field for determining AP account.';
comment on column LS_IMPORT_ILR_ARCHIVE."AP_ACCOUNT_ID" is 'The AP account.';

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'int_accrual_account_id',
           'Interest Accrual Account',
           'int_accrual_account_xlate',
           0,
           2,
           'number(22,0)',
           'gl_account',
           1,
           'gl_account_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: ILR%');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'int_expense_account_id',
           'Interest Expense Account',
           'int_expense_account_xlate',
           0,
           2,
           'number(22,0)',
           'gl_account',
           1,
           'gl_account_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: ILR%');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'exec_accrual_account_id',
           'Executory Accrual Account',
           'exec_accrual_account_xlate',
           0,
           2,
           'number(22,0)',
           'gl_account',
           1,
           'gl_account_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: ILR%');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'exec_expense_account_id',
           'Executory Expense Account',
           'exec_expense_account_xlate',
           0,
           2,
           'number(22,0)',
           'gl_account',
           1,
           'gl_account_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: ILR%');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'cont_accrual_account_id',
           'Contingent Accrual Account',
           'cont_accrual_account_xlate',
           0,
           2,
           'number(22,0)',
           'gl_account',
           1,
           'gl_account_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: ILR%');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'cont_expense_account_id',
           'Contingent Expense Account',
           'cont_expense_account_xlate',
           0,
           2,
           'number(22,0)',
           'gl_account',
           1,
           'gl_account_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: ILR%');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'cap_asset_account_id',
           'Capital Asset Account',
           'cap_asset_account_xlate',
           0,
           2,
           'number(22,0)',
           'gl_account',
           1,
           'gl_account_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: ILR%');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'st_oblig_account_id',
           'Short-term Obligation Account',
           'st_oblig_account_xlate',
           0,
           2,
           'number(22,0)',
           'gl_account',
           1,
           'gl_account_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: ILR%');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'lt_oblig_account_id',
           'Long-term Obligation Account',
           'lt_oblig_account_xlate',
           0,
           2,
           'number(22,0)',
           'gl_account',
           1,
           'gl_account_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: ILR%');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'ap_account_id',
           'AP Account',
           'ap_account_xlate',
           0,
           2,
           'number(22,0)',
           'gl_account',
           1,
           'gl_account_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: ILR%');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'int_accrual_account_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: ILR%'
    and (PPIL.DESCRIPTION like 'GL Account.External Account Code (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Long Description (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Description (for given company)'));

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'int_expense_account_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: ILR%'
    and (PPIL.DESCRIPTION like 'GL Account.External Account Code (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Long Description (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Description (for given company)'));

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'exec_accrual_account_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: ILR%'
    and (PPIL.DESCRIPTION like 'GL Account.External Account Code (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Long Description (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Description (for given company)'));

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'exec_expense_account_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: ILR%'
    and (PPIL.DESCRIPTION like 'GL Account.External Account Code (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Long Description (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Description (for given company)'));

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'cont_accrual_account_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: ILR%'
    and (PPIL.DESCRIPTION like 'GL Account.External Account Code (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Long Description (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Description (for given company)'));

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'cont_expense_account_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: ILR%'
    and (PPIL.DESCRIPTION like 'GL Account.External Account Code (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Long Description (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Description (for given company)'));

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'cap_asset_account_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: ILR%'
    and (PPIL.DESCRIPTION like 'GL Account.External Account Code (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Long Description (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Description (for given company)'));

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'st_oblig_account_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: ILR%'
    and (PPIL.DESCRIPTION like 'GL Account.External Account Code (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Long Description (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Description (for given company)'));

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'lt_oblig_account_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: ILR%'
    and (PPIL.DESCRIPTION like 'GL Account.External Account Code (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Long Description (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Description (for given company)'));

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'ap_account_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: ILR%'
    and (PPIL.DESCRIPTION like 'GL Account.External Account Code (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Long Description (for given company)'
    or PPIL.DESCRIPTION like 'GL Account.Description (for given company)'));

update PP_IMPORT_COLUMN
   set IS_REQUIRED = 0
 where COLUMN_NAME in ('muni_bo_sw','est_executory_cost','contingent_amount');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (828, 0, 10, 4, 2, 0, 34642, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034642_lease_ILR_account_imports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;