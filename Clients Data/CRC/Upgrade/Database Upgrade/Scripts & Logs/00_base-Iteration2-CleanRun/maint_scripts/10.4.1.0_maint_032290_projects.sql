/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032290_projects.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   09/09/2013 Chris Mardis
||============================================================================
*/

alter table WO_EST_RATE_DEFAULT add WORK_ORDER_TYPE_ID number(22);

comment on column WO_EST_RATE_DEFAULT.WORK_ORDER_TYPE_ID is 'System-assigned identifier for the Work Order Type';

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    COLUMN_RANK)
values
   ('work_order_type_id', 'wo_est_rate_default', 'work_order_type', 'p', 'Work Order Type',
    'Work Order Type', 15);

alter table WO_EST_RATE_FILTER add WORK_ORDER_ID      number(22);
alter table WO_EST_RATE_FILTER add WORK_ORDER_TYPE_ID number(22);

comment on column WO_EST_RATE_FILTER.WORK_ORDER_ID is 'System-assigned identifier for the Work Order';
comment on column WO_EST_RATE_FILTER.WORK_ORDER_TYPE_ID is 'System-assigned identifier for the Work Order Type';

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    COLUMN_RANK)
values
   ('work_order_id', 'wo_est_rate_filter', null, 'e', 'Work Order Id', 'Work Order Id', 12);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    COLUMN_RANK)
values
   ('work_order_type_id', 'wo_est_rate_filter', 'work_order_type', 'p', 'Work Order Type',
    'Work Order Type', 15);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (603, 0, 10, 4, 1, 0, 32290, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032290_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;