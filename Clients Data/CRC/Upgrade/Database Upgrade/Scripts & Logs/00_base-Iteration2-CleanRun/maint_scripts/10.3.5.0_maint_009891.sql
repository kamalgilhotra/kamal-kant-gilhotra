/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009891.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/11/2012 Sunjin Cone    Point Release
||============================================================================
*/

insert into PP_WO_EST_CUSTOMIZE
   (COMPANY_ID, TAB, COLUMN_NAME, DISPLAY, COLUMN_ORDER, COLUMN_WIDTH)
   select COMPANY_ID, TAB, 'retire_vintage', DISPLAY, COLUMN_ORDER + 1, COLUMN_WIDTH
     from PP_WO_EST_CUSTOMIZE
    where COLUMN_NAME = 'serial_number'
      and TAB = 1;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (181, 0, 10, 3, 5, 0, 9891, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009891.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
