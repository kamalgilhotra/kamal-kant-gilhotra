/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053038_pwrtax_fix_report_257a_description_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 02/04/2019 David Conway   DML to change description on report 257a to FAS109 by type provision & reversal
|| 2018.2.0.0 02/04/2019 David Conway   Modified criteria to use report_number and subsystem instead of report_id.
||============================================================================
*/

update pp_reports
set description = 'FAS109 by type provision &' || ' reversal'
where report_number = 'PwrTax - 257a'
  and subsystem = 'PowerTax';

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14590, 0, 2018, 2, 0, 0, 53038, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053038_pwrtax_fix_report_257a_description_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
