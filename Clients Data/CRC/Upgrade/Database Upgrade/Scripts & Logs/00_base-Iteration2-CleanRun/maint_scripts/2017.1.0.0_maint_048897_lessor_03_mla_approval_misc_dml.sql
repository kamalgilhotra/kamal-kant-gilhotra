 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048897_lessor_03_mla_approval_misc_dml.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 10/02/2017 Shane "C" Ward  New Workflows for Lessor, update Lessee Workflows
 ||
 ||============================================================================
 */
UPDATE workflow_subsystem SET subsystem = 'lessee_mla_approval' WHERE subsystem = 'mla_approval';
UPDATE workflow_subsystem SET subsystem = 'lessee_ilr_approval' WHERE subsystem = 'ilr_approval';

INSERT INTO WORKFLOW_SUBSYSTEM
            (subsystem,
             field1_desc,
             field2_desc,
             field1_sql,
             field2_sql,
             pending_notification_type,
             approved_notification_type,
             rejected_notification_type,
             send_sql,
             approve_sql,
             reject_sql,
             unreject_sql,
             unsend_sql,
             update_workflow_type_sql,
             send_emails)
VALUES      ('lessor_ilr_approval',
             'ILR Number',
             'Revision',
             'select ilr_number from ls_ilr where ilr_id = <<id_field1>>',
             '<<id_field2>>',
             'ILR APPROVAL MESSAGE',
             'ILR APPROVED NOTICE MESSAGE',
             'ILR REJECTION MESSAGE',
             'select PKG_LESSOR_APPROVAL.F_SEND_ILR(<<id_field1>>, <<id_field2>>) from dual',
             'select PKG_LESSOR_APPROVAL.F_APPROVE_ILR(<<id_field1>>, <<id_field2>>) from dual',
             'select PKG_LESSOR_APPROVAL.F_REJECT_ILR(<<id_field1>>, <<id_field2>>) from dual',
             'select PKG_LESSOR_APPROVAL.F_UNREJECT_ILR(<<id_field1>>, <<id_field2>>) from dual',
             'select PKG_LESSOR_APPROVAL.F_UNSEND_ILR(<<id_field1>>, <<id_field2>>) from dual',
             'select PKG_LESSOR_APPROVAL.F_UPDATE_WORKFLOW_ilr(<<id_field1>>, <<id_field2>>) from dual',
             1);

			 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3759, 0, 2017, 1, 0, 0, 48897, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048897_lessor_03_mla_approval_misc_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;			 
			 