/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050372_lessor_05_update_trans_type_descriptions_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 04/12/2018 Jared Watkins  Update all of the Lessor Trans Types we added to match existing format
||============================================================================
*/

merge into je_trans_type a
using (select 4001 trans_type, '4001 - Lessor PP&'||'E Adjustment Credit' description from dual union all
       select 4002, '4002 - Lessor Short Term Receivable Debit' from dual union all
       select 4003, '4003 - Lessor Long Term Receivable Debit' from dual union all
       select 4004, '4004 - Lessor Unguaranteed Residual Debit' from dual union all
       select 4005, '4005 - Lessor Selling Profit Credit' from dual union all
       select 4006, '4006 - Lessor Selling Loss Debit' from dual union all
       select 4007, '4007 - Lessor Incurred Lease Costs Credit' from dual union all
       select 4008, '4008 - Lessor Initial Direct Cost Expense Debit' from dual union all
       select 4010, '4010 - Lessor Monthly Interest Accrual Debit' from dual union all
       select 4011, '4011 - Lessor Monthly Interest Accrual Credit' from dual union all
       select 4012, '4012 - Lessor Monthly Executory Accrual Debit' from dual union all
       select 4013, '4013 - Lessor Monthly Executory Accrual Credit' from dual union all
       select 4014, '4014 - Lessor Monthly Contingent Accrual Debit' from dual union all
       select 4015, '4015 - Lessor Monthly Contingent Accrual Credit' from dual union all
       select 4016, '4016 - Lessor Unguaranteed Residual Accretion Debit' from dual union all
       select 4017, '4017 - Lessor Unguaranteed Residual Accretion Credit' from dual union all
       select 4018, '4018 - Lessor Accounts Receivable Debit' from dual union all
       select 4019, '4019 - Lessor Interest Invoice Credit' from dual union all
       select 4020, '4020 - Lessor Executory Invoice Credit' from dual union all
       select 4021, '4021 - Lessor Contingent Invoice Credit' from dual union all
       select 4022, '4022 - Lessor ST Receivable Credit' from dual union all
       select 4027, '4027 - Lessor ST Receivable Reclass Debit' from dual union all
       select 4028, '4028 - Lessor LT Receivable Reclass Credit' from dual union all
       select 4032, '4032 - Lessor Initial Direct Cost Amortization Debit' from dual union all
       select 4033, '4033 - Lessor Initial Direct Cost Amortization Credit' from dual union all
       select 4051, '4051 - Lessor Currency Gain/Loss Debit' from dual union all
       select 4052, '4052 - Lessor Currency Gain/Loss Credit' from dual union all
       select 4055, '4055 - Lessor Deferred Selling Profit Credit' from dual union all
       select 4056, '4056 - Lessor Deferred Selling Profit Debit' from dual union all
       select 4057, '4057 - Lessor Recognize Selling Profit Credit' from dual) x
on (a.trans_type = x.trans_type)
when matched then update set a.description = x.description
when not matched then insert (a.trans_type, a.description) values (x.trans_type, x.description);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4309, 0, 2017, 3, 0, 0, 50372, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050372_lessor_05_update_trans_type_descriptions_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;