/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043345_cap_bud_proposed_version_type_dml_1.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.1     03/23/2015 Chris Mardis   Point Release
||============================================================================
*/

insert into budget_version_status (budget_version_status_id, description, long_description)
values (6, 'Proposed Budget Version', 'This is the proposed annual budget (not yet approved) reflecting the latest proposed annual budget that will be presented to the Board');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2392, 0, 2015, 1, 0, 0, 43345, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043345_cap_bud_proposed_version_type_dml_1.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;