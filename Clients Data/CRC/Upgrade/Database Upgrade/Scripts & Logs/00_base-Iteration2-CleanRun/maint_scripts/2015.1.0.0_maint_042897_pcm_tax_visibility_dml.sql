/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042897_pcm_tax_visibility_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1 	  2/12/2015  Ryan Oliveria       Convert Tax Status system control to menu item
||==========================================================================================
*/

update PPBASE_MENU_ITEMS
   set ENABLE_YN = decode((select lower(trim(CONTROL_VALUE)) from PP_SYSTEM_CONTROL where upper(trim(CONTROL_NAME)) = 'TAX EXPENSE USER OBJECT'), 'no', 0, 1)
 where MODULE = 'pcm'
   and MENU_IDENTIFIER = 'fp_maint_tax_status';

update PPBASE_MENU_ITEMS
   set ENABLE_YN = decode((select lower(trim(CONTROL_VALUE)) from PP_SYSTEM_CONTROL where upper(trim(CONTROL_NAME)) = 'TAX EXPENSE USER OBJECT - WO'), 'no', 0, 1)
 where MODULE = 'pcm'
   and MENU_IDENTIFIER = 'wo_maint_tax_status';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2290, 0, 2015, 1, 0, 0, 042897, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042897_pcm_tax_visibility_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;