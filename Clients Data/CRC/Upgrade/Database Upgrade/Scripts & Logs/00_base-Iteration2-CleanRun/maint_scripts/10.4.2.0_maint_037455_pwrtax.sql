/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037455_pwrtax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 04/03/2014 Nathan Hollis
||============================================================================
*/

------------------------------------------------------------------------------------
----FIX 30412
------------------------------------------------------------------------------------
update POWERPLANT_TABLES
   set (LONG_DESCRIPTION, SUBSYSTEM_DISPLAY, SUBSYSTEM_SCREEN, SUBSYSTEM) =
        (select 'The Tax Repairs Add Expense data table holds by tax years how many months forward to look and how many months backwards.',
                'tax_accrual',
                'tax',
                'tax'
           from DUAL)
 where LOWER(TABLE_NAME) = 'tax_repairs_add_expense';

insert into PP_TABLE_GROUPS
   (TABLE_NAME, GROUPS)
   select 'tax_repairs_add_expense', 'system'
     from DUAL
    where not exists (select 'X'
             from PP_TABLE_GROUPS
            where LOWER(TABLE_NAME) = 'tax_repairs_add_expense'
              and LOWER(GROUPS) = 'system')
      and exists (select 'X' from PP_TABLE_GROUPS where LOWER(GROUPS) = 'system');

insert into PP_TABLE_GROUPS
   (TABLE_NAME, GROUPS)
   select 'tax_repairs_add_expense', 'tax_power'
     from DUAL
    where not exists (select 'X'
             from PP_TABLE_GROUPS
            where LOWER(TABLE_NAME) = 'tax_repairs_add_expense'
              and LOWER(GROUPS) = 'tax_power')
      and exists (select 'X' from PP_TABLE_GROUPS where LOWER(GROUPS) = 'tax_power');

insert into PP_TABLE_GROUPS
   (TABLE_NAME, GROUPS)
   select 'tax_repairs_add_expense', 'property tax'
     from DUAL
    where not exists (select 'X'
             from PP_TABLE_GROUPS
            where LOWER(TABLE_NAME) = 'tax_repairs_add_expense'
              and LOWER(GROUPS) = 'property tax')
      and exists (select 'X' from PP_TABLE_GROUPS where LOWER(GROUPS) = 'property tax');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1093, 0, 10, 4, 2, 0, 37455, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037455_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
