/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034349_sys_PKG_PP_GET_VALUE_ASYNC.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 04/12/2014 Stephen Motter
||============================================================================
*/

create or replace package PKG_PP_GET_VALUE_ASYNC is
  --||============================================================================
  --|| Application: PowerPlan
  --|| Object Name: pkg_pp_get_value_async
  --|| Description:
  --||============================================================================
  --|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
  --||============================================================================
  --|| Version  Date       Revised By        Reason for Change
  --|| -------- ---------- ----------------- -------------------------------------
  --|| 1.0      12/12/2013 Stephen Motter      Original Version
  --||============================================================================

  function F_GET_VARCHAR2(A_ARG varchar2) return varchar2;

  function F_GET_NUMBER(A_ARG varchar2) return number;

  function F_GET_DATE(A_ARG varchar2) return date;

end PKG_PP_GET_VALUE_ASYNC;
/

create or replace package body PKG_PP_GET_VALUE_ASYNC is
  -- =============================================================================
  --  Function F_GET_VARCHAR2
  -- =============================================================================
  function F_GET_VARCHAR2(A_ARG varchar2) return varchar2 as
    pragma autonomous_transaction;
    RETURN_VAL varchar2(4000);
  begin
    execute immediate A_ARG
      into RETURN_VAL;
    return RETURN_VAL;
  exception
    when others then
      return null;
  end;
  -- =============================================================================
  --  Function F_GET_NUMBER
  -- =============================================================================
  function F_GET_NUMBER(A_ARG varchar2) return number as
    pragma autonomous_transaction;
    RETURN_VAL number;
  begin
    execute immediate A_ARG
      into RETURN_VAL;
    return RETURN_VAL;
  exception
    when others then
      return null;
  end;
  -- =============================================================================
  --  Function F_GET_DATE
  -- =============================================================================
  function F_GET_DATE(A_ARG varchar2) return date as
    pragma autonomous_transaction;
    RETURN_VAL date;
  begin
    execute immediate A_ARG
      into RETURN_VAL;
    return RETURN_VAL;
  exception
    when others then
      return null;
  end;
end PKG_PP_GET_VALUE_ASYNC;
/

alter table WO_VALIDATION_RUN add RETURN_CODE number default 1;
comment on column WO_VALIDATION_RUN.RETURN_CODE is 'This column indicates whether the modify or sql is supposed to be run only when the validation returns a particular value.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1029, 0, 10, 4, 2, 0, 34349, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034349_sys_PKG_PP_GET_VALUE_ASYNC.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;