 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046114_pwrtax_report_sys_control_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2015.2.3 08/12/2016   Rob Burns      Initial script setting the default system control for report name to a space instead of USA Power Corp
 ||============================================================================
 */ 

update pp_system_control_company 
set control_value = ' '
where control_name = 'Report Company Name'
and control_value like '%USA Power Corp%';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3254, 0, 2016, 1, 0, 0, 046114, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046114_pwrtax_report_sys_control_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;