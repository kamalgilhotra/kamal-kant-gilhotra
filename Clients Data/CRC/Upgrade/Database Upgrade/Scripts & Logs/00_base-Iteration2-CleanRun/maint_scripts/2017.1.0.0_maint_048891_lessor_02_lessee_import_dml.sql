/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048891_lessor_02_lessee_import_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/29/2017 Shane "C" Ward		Set up Lessor Lessee Import Tables
||============================================================================
*/
--Set up Lessor Import Subsystem
INSERT INTO PP_IMPORT_SUBSYSTEM
            (import_subsystem_id,
             description,
             long_description)
VALUES      (14,
             'Lessor',
             'Lessor');

--Setup Lessor Lessee import type
INSERT INTO PP_IMPORT_TYPE
            (import_type_id,
             description,
             long_description,
             import_table_name,
             archive_table_name,
             allow_updates_on_add,
             delegate_object_name,
             archive_additional_columns)
VALUES      (500,
             'Add: Lessees',
             'Lessor Lessees',
             'lsr_import_lessee',
             'lsr_import_lessee_archive',
             1,
             'nvo_lsr_logic_import',
             'lessee_id');

--Associate Import type with Subsystem
INSERT INTO PP_IMPORT_TYPE_SUBSYSTEM
            (import_type_id,
             import_subsystem_id)
VALUES      (500,
             14);

--Columns for use in Import template
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'address1',
             'Address1',
             NULL,
             0,
             1,
             'varchar2(35)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'address2',
             'Address2',
             NULL,
             0,
             1,
             'varchar2(35)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'address3',
             'Address3',
             NULL,
             0,
             1,
             'varchar2(35)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'address4',
             'Address4',
             NULL,
             0,
             1,
             'varchar2(35)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'city',
             'City',
             NULL,
             0,
             1,
             'varchar2(35)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'country_id',
             'Country ID',
             'country_xlate',
             0,
             1,
             'char(18)',
             'country',
             1,
             'country_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'county_id',
             'County ID',
             'county_xlate',
             0,
             1,
             'char(18)',
             'county',
             1,
             'county_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'description',
             'Description',
             NULL,
             1,
             1,
             'varchar2(35)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'extension',
             'Extension',
             NULL,
             0,
             1,
             'varchar2(8)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'external_lessee_number',
             'External Lessee Number',
             NULL,
             1,
             1,
             'varchar2(35)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'fax_number',
             'Fax Number',
             NULL,
             0,
             1,
             'varchar2(18)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'is_modified',
             'Is Modified',
             NULL,
             0,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'lease_group_id',
             'Lease Group ID',
             'lease_group_xlate',
             1,
             1,
             'varchar2(35)',
             'ls_lease_group',
             1,
             'lease_group_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'loaded',
             'Loaded',
             NULL,
             0,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'long_description',
             'Long Description',
             NULL,
             1,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'phone_number',
             'Phone Number',
             NULL,
             0,
             1,
             'varchar2(18)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'postal',
             'Postal',
             NULL,
             0,
             1,
             'number(4)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'site_code',
             'Site Code',
             NULL,
             0,
             1,
             'varchar2(50)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'state_id',
             'State ID',
             'state_xlate',
             0,
             1,
             'char(18)',
             'state',
             1,
             'state_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'unique_lessee_identifier',
             'Unique Lessee Identifier',
             NULL,
             1,
             1,
             'varchar2(35)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (500,
             'zip',
             'Zip',
             NULL,
             0,
             1,
             'number(5,0)',
             NULL,
             1,
             NULL);

--Insert lookups for columns
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (250,
             'country_id',
             1031);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (500,
             'country_id',
             1031);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (500,
             'state_id',
             1);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (500,
             'lease_group_id',
             1026); 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3747, 0, 2017, 1, 0, 0, 48891, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048891_lessor_02_lessee_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;