/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_037539_sys_std_jrnl_entries.sql
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ---------------------------------------------
|| 10.4.2.0 04/03/2014 Brandon Beck
||=================================================================================
*/

update STANDARD_JOURNAL_ENTRIES
   set GL_JE_CODE = trim(GL_JE_CODE), EXTERNAL_JE_CODE = trim(EXTERNAL_JE_CODE)
 where GL_JE_CODE <> trim(GL_JE_CODE);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1090, 0, 10, 4, 2, 0, 37539, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037539_sys_std_jrnl_entries.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;