/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_043174_budgetcap_add_col.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------------------
|| 2015.1   03/16/2015 Daniel Motter        Add column to fcst_gl_transaction
||========================================================================================
*/

declare
begin
  execute immediate 'alter table fcst_gl_transaction add je_method_id number(22,0) null';
  execute immediate 'comment on column fcst_gl_transaction.je_method_id is ''JE Method used to generate entries.''';
exception 
  when others then 
    DBMS_OUTPUT.PUT_LINE('Column je_method_id already exists on table fcst_gl_transaction.');
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2379, 0, 2015, 1, 0, 0, 43174, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043174_budgetcap_add_col.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;