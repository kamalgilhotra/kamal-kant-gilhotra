/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_040449_depr_blending.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------- ----------------------------------------------
|| 10.4.3.0 10/22/2014
||========================================================================================
*/

create global temporary table DEPR_LEDGER_BLENDING_TEMP
(
 DEPR_METHOD_ID         number(22,0),
 TARGET_SET_OF_BOOKS_ID number(22,0),
 EFFECTIVE_DATE         date,
 SOURCE_SET_OF_BOOKS_ID number(22,0),
 SOURCE_PERCENT         number(22,8),
 PRIORITY               number(22,0)
) on commit preserve rows;

comment on table DEPR_LEDGER_BLENDING_TEMP
is 'A temporary table to hold the depreciation blending percent used in calculation of depreciation';

comment on column DEPR_LEDGER_BLENDING_TEMP.DEPR_METHOD_ID
is 'The depreciation method that uses the blended percent';

comment on column DEPR_LEDGER_BLENDING_TEMP.TARGET_SET_OF_BOOKS_ID
is 'The target set of books (The set of books that will get blended)';

comment on column DEPR_LEDGER_BLENDING_TEMP.EFFECTIVE_DATE
is 'The effective date for the blended percent';

comment on column DEPR_LEDGER_BLENDING_TEMP.SOURCE_SET_OF_BOOKS_ID
is 'The source set of books (One of the set of books that will get used in the blended results)';

comment on column DEPR_LEDGER_BLENDING_TEMP.SOURCE_PERCENT
is 'The percent of the source set of books to use in the blended result';

comment on column DEPR_LEDGER_BLENDING_TEMP.PRIORITY
is 'The priority for the target set of books blending';


create index NDX_DEPR_BLND_TEMP
   on DEPR_LEDGER_BLENDING_TEMP (DEPR_METHOD_ID, EFFECTIVE_DATE);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1551, 0, 10, 4, 3, 0, 40449, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040449_depr_blending.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;