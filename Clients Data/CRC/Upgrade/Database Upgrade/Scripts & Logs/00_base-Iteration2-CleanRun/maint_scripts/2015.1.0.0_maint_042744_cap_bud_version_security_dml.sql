/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042744_cap_bud_version_security_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 3/14/2015  Chris Mardis     Capital Budget Version Security
||============================================================================
*/

insert into budget_version_security_all (budget_version_id)
select budget_version_id
from budget_version_control;
commit;

insert into budget_version_sec_all_arch (budget_version_id)
select budget_version_id
from budget_version_arch;
commit;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2378, 0, 2015, 1, 0, 0, 42744, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042744_cap_bud_version_security_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;