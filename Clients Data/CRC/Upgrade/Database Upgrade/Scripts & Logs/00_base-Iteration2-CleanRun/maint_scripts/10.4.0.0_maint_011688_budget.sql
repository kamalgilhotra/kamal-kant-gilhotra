SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011688_budget.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   01/08/2013 Lee Quinn      Point Release
||============================================================================
*/

-- Drop the foreign key on WORK_ORDER_TYPE > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE
declare
   LV_EXISTS boolean := false;

begin
   for CSR_ALTER_SQL in (select 'alter table ' || TABLE_NAME || ' drop constraint ' ||
                                CONSTRAINT_NAME ALTER_SQL
                           from ALL_CONSTRAINTS
                          where OWNER = 'PWRPLANT'
                            and TABLE_NAME = 'WORK_ORDER_TYPE'
                            and R_CONSTRAINT_NAME in
                                (select CONSTRAINT_NAME
                                   from ALL_CONSTRAINTS
                                  where OWNER = 'PWRPLANT'
                                    and TABLE_NAME = 'BUDGET_REVIEW_TYPE'
                                    and CONSTRAINT_TYPE = 'P'))
   loop
      DBMS_OUTPUT.PUT_LINE('SQL = ' || CSR_ALTER_SQL.ALTER_SQL || ';');
      execute immediate CSR_ALTER_SQL.ALTER_SQL;
      LV_EXISTS := true;
   end loop;

   if not LV_EXISTS then
      DBMS_OUTPUT.PUT_LINE('Foreign key to be dropped, WORK_ORDER_TYPE > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE doesn''t exist.');
   end if;
end;
/

-- Drop the foreign key on WORK_ORDER_APPROVAL > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE
declare
   LV_EXISTS boolean := false;

begin
   for CSR_ALTER_SQL in (select 'alter table ' || TABLE_NAME || ' drop constraint ' ||
                                CONSTRAINT_NAME ALTER_SQL
                           from ALL_CONSTRAINTS
                          where OWNER = 'PWRPLANT'
                            and TABLE_NAME = 'WORK_ORDER_APPROVAL'
                            and R_CONSTRAINT_NAME in
                                (select CONSTRAINT_NAME
                                   from ALL_CONSTRAINTS
                                  where OWNER = 'PWRPLANT'
                                    and TABLE_NAME = 'BUDGET_REVIEW_TYPE'
                                    and CONSTRAINT_TYPE = 'P'))
   loop
      DBMS_OUTPUT.PUT_LINE('SQL = ' || CSR_ALTER_SQL.ALTER_SQL || ';');
      execute immediate CSR_ALTER_SQL.ALTER_SQL;
      LV_EXISTS := true;
   end loop;
   if not LV_EXISTS then
      DBMS_OUTPUT.PUT_LINE('Foreign key to be dropped, WORK_ORDER_TYPE > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE doesn''t exist.');
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (271, 0, 10, 4, 0, 0, 11688, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011688_budget.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
