/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032381_prov.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/25/2013 Nathan Hollis
||============================================================================
*/

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = null
 where REP_CONS_TYPE_ID in (4, 5)
   and ROW_ID = 11;

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = null
 where REP_CONS_TYPE_ID in (2, 3)
   and ROW_ID = 13;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (735, 0, 10, 4, 1, 1, 32381, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032381_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
