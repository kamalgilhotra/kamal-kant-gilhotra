SET SERVEROUTPUT ON SIZE UNLIMITED

/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044041_lease_add_new_trans_types_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 06/18/2015 Charlie Shilling New trans types for lease reserve transfers through Post.
||============================================================================
*/

DECLARE
	v_description je_trans_type.description%TYPE;
	v_need_insert BOOLEAN;
BEGIN
	--Check to see if the 3047 trans type already exists
	BEGIN
		v_need_insert := FALSE;

		SELECT description
		INTO v_description
		FROM je_trans_type
		WHERE trans_type = 3047;
	EXCEPTION
		WHEN No_Data_Found THEN
			v_need_insert := TRUE;
	END;

	IF v_need_insert THEN
		Dbms_Output.put_line('Trans Type 3047 not found - inserting now');
		INSERT INTO je_trans_type (trans_type, description)
		VALUES(3047, '3047 - Lease Reserve Transfer Debit (POST)');
	ELSE
		Dbms_Output.put_line('Trans Type 3047 already exists in the JE_TRANS_TYPE table');
	END IF;

	--Check to see if the 3048 trans type already exists
	BEGIN
		v_need_insert := FALSE;

		SELECT description
		INTO v_description
		FROM je_trans_type
		WHERE trans_type = 3048;
	EXCEPTION
		WHEN No_Data_Found THEN
			v_need_insert := TRUE;
	END;

	IF v_need_insert THEN
		Dbms_Output.put_line('Trans Type 3048 not found - inserting now');
		INSERT INTO je_trans_type (trans_type, description)
		VALUES(3048, '3048 - Lease Reserve Transfer Credit (POST)');
	ELSE
		Dbms_Output.put_line('Trans Type 3048 already exists in the JE_TRANS_TYPE table');
	END IF;
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2652, 0, 2015, 2, 0, 0, 044041, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044041_lease_add_new_trans_types_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;