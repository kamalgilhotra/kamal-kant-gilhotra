/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044520_pwrtax_notnull_fields_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   08/27/2015 Andrew Scott   Make tax_depreciation fields convention_id
||                                    and extraordinary_convention not null
||============================================================================
*/

set serveroutput on

----set convention_id to be not null
declare
    type td_detail is record (
        records_to_fix         varchar2(255)
    );
    type td_details is table of td_detail index by pls_integer;
    l_td_details td_details;
begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   select 'Tax Record ID:'||tax_record_id||', Tax Year:'||tax_year||', Tax Book ID:'||tax_book_id records_to_fix
   bulk collect into l_td_details
   from tax_depreciation
   where convention_id is null
   order by tax_record_id, tax_year, tax_book_id;

    if l_td_details.count > 0 then
        dbms_output.put_line('PPC-MSG> The following records on tax depreciation have a convention id that is not null');
        dbms_output.put_line('PPC-MSG> Please change these to have an appropriate convention id and set the column to be not null.');
        dbms_output.put_line('PPC-MSG>     ------------------------');
        for i in l_td_details.first..l_td_details.last loop
            dbms_output.put_line('PPC-MSG>     '||l_td_details(i).records_to_fix);
        end loop;
        dbms_output.put_line('PPC-MSG>     ------------------------');
        raise_application_error(-20000, 'Please read the dbms log and make the necessary changes to the system.');
    else
        dbms_output.put_line('PPC-MSG> All rows have convention ids.  Changing tax depreciation to make this a not nullable field.');
        begin
            execute immediate 'alter table tax_depreciation modify convention_id not null';
        exception
            when others then
              dbms_output.put_line('PPC-MSG> Convention id on tax depreciation is either already not null or the alter table grants need to be applied. Error:'||sqlerrm);
        end;
        dbms_output.put_line('PPC-MSG> Successfully altered tax depreciation to make convention id not null.');
    end if;
exception
    when others then
        dbms_output.put_line('PPC-MSG> Error in attempting to change tax depreciation convention id to be not null. Error:'||sqlerrm);
end;
/


----set extraordinary_convention to be not null
declare
    type td_detail is record (
        records_to_fix         varchar2(255)
    );
    type td_details is table of td_detail index by pls_integer;
    l_td_details td_details;
begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   select 'Tax Record ID:'||tax_record_id||', Tax Year:'||tax_year||', Tax Book ID:'||tax_book_id records_to_fix
   bulk collect into l_td_details
   from tax_depreciation
   where extraordinary_convention is null
   order by tax_record_id, tax_year, tax_book_id;

    if l_td_details.count > 0 then
        dbms_output.put_line('PPC-MSG> The following records on tax depreciation have a extraordinary convention that is not null');
        dbms_output.put_line('PPC-MSG> Please change these to have an appropriate extraordinary convention and set the column to be not null.');
        dbms_output.put_line('PPC-MSG>     ------------------------');
        for i in l_td_details.first..l_td_details.last loop
            dbms_output.put_line('PPC-MSG>     '||l_td_details(i).records_to_fix);
        end loop;
        dbms_output.put_line('PPC-MSG>     ------------------------');
        raise_application_error(-20000, 'Please read the dbms log and make the necessary changes to the system.');
    else
        dbms_output.put_line('PPC-MSG> All rows have extraordinary convention populated.  Changing tax depreciation to make this a not nullable field.');
        begin
            execute immediate 'alter table tax_depreciation modify extraordinary_convention not null';
        exception
            when others then
              dbms_output.put_line('PPC-MSG> extraordinary convention on tax depreciation is either already not null or the alter table grants need to be applied. Error:'||sqlerrm);
        end;
        dbms_output.put_line('PPC-MSG> Successfully altered tax depreciation to make extraordinary convention not null.');
    end if;
exception
    when others then
        dbms_output.put_line('PPC-MSG> Error in attempting to change tax depreciation extraordinary convention id to be not null. Error:'||sqlerrm);
end;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2830, 0, 2015, 2, 0, 0, 044520, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044520_pwrtax_notnull_fields_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;