/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042809_pcm_menu_items_02_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1 02/13/2015 		B.Beck			Modify Left Hand Navigation
||==========================================================================================
*/

delete
from ppbase_menu_items
where module = 'pcm'
and parent_menu_identifier in ('fp_estimate', 'fp_monitor');

delete
from ppbase_workspace
where module = 'pcm'
and workspace_identifier in
(
'fp_est_cashflows',
'fp_est_property',
'fp_est_financials',
'fp_mon_dashboards',
'fp_mon_charges',
'fp_mon_commitments'
);

delete
from ppbase_menu_items
where module = 'pcm'
and parent_menu_identifier = 'fp_maintain'
and menu_identifier = 'fp_maint_work_orders';

delete
from ppbase_workspace
where module = 'pcm'
and workspace_identifier = 'fp_maint_work_orders';

update ppbase_menu_items
set item_order = 2, label = 'Accounting'
where module = 'pcm'
and menu_identifier = 'fp_maint_accounts';

update ppbase_menu_items
set item_order = 6
where module = 'pcm'
and menu_identifier = 'fp_maint_class_codes';

update ppbase_menu_items
set item_order = 5, label = 'Tasks'
where module = 'pcm'
and menu_identifier = 'fp_maint_job_tasks';

update ppbase_menu_items
set item_order = 3
where module = 'pcm'
and menu_identifier = 'fp_maint_departments';

update ppbase_menu_items
set item_order = 7
where module = 'pcm'
and menu_identifier = 'fp_maint_tax_status';

update ppbase_menu_items
set item_order = item_order - 1
where module = 'pcm'
and menu_level = 1
;

update ppbase_menu_items
set minihelp = 'Tasks', label = 'Tasks'
where module = 'pcm'
and menu_identifier = 'job_tasks'
;

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'Extensions', 1, 6, 'Extensions', 
	'Customer Extensions', '', '', 0
);

delete
from ppbase_menu_items
where module = 'pcm'
and menu_identifier = 'fp_maint_other'
;

delete
from ppbase_workspace
where module = 'pcm'
and workspace_identifier = 'fp_maint_other'
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2280, 0, 2015, 1, 0, 0, 042809, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042809_pcm_menu_items_02_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;