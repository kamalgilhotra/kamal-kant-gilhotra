/*
||==========================================================================================
|| Application: PowerPlan
|| Module: Property Tax
|| File Name:   maint_045668_proptax_bill_instllmt_import_ddl.sql
||==========================================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2016.1    04/27/2016  Graham Miller    Add import type to import to Bills Center by installment
||==========================================================================================
*/

CREATE TABLE pt_import_stmt_installment (
  import_run_id          NUMBER(22,0)   NOT NULL,
  line_id                NUMBER(22,0)   NOT NULL,
  time_stamp             DATE           NULL,
  user_id                VARCHAR2(18)   NULL,
  description            VARCHAR2(254)  NULL,
  long_description       VARCHAR2(254)  NULL,
  account_number         VARCHAR2(254)  NULL,
  statement_xlate        VARCHAR2(254)  NULL,
  "STATEMENT_ID"           NUMBER(22,0)   NULL,
  installment_id         NUMBER(22,0)   NULL,
  statement_notes        VARCHAR2(254)  NULL,
  statement_year_xlate   VARCHAR2(254)  NULL,
  statement_year_id      NUMBER(22,0)   NULL,
  assessment_year_xlate  VARCHAR2(254)  NULL,
  assessment_year_id     NUMBER(22,0)   NULL,
  prop_tax_company_xlate VARCHAR2(254)  NULL,
  prop_tax_company_id    NUMBER(22,0)   NULL,
  statement_group_xlate  VARCHAR2(254)  NULL,
  statement_group_id     NUMBER(22,0)   NULL,
  estimated_switch_xlate VARCHAR2(254)  NULL,
  estimated_switch_yn    NUMBER(22,0)   NULL,
  year_notes             VARCHAR2(2000) NULL,
  tax_amount             VARCHAR2(35)   NULL,
  credit_amount          VARCHAR2(35)   NULL,
  penalty_amount         VARCHAR2(35)   NULL,
  interest_amount        VARCHAR2(35)   NULL,
  assessment             VARCHAR2(35)   NULL,
  taxable_value          VARCHAR2(35)   NULL,
  error_message          VARCHAR2(4000) NULL
);

ALTER TABLE pt_import_stmt_installment
  ADD CONSTRAINT pt_impt_stmt_inst_imp_run_fk FOREIGN KEY (
    import_run_id
  ) REFERENCES pp_import_run (
    import_run_id
  );

COMMENT ON TABLE pt_import_stmt_installment IS '(O and C)  [07]
The PT Import Stmt Installment table is a staging table into which data from a run of the "Update : Bills by Installment" import process is loaded and then processed.  Once the import process completes successfully, the data for the run is moved to the PT Import Stmt Installment Arc table.';
COMMENT ON COLUMN pt_import_stmt_installment.import_run_id IS 'System-assigned identifier of an import run occurrence.';
COMMENT ON COLUMN pt_import_stmt_installment.line_id IS 'Line number corresponding to the row of this data in the original import file.';
COMMENT ON COLUMN pt_import_stmt_installment.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN pt_import_stmt_installment.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN pt_import_stmt_installment.description IS 'Description of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment.long_description IS 'Long description of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment.account_number IS 'Account number of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment.statement_xlate IS 'Value from the import file translated to obtain the statement_id.';
COMMENT ON COLUMN pt_import_stmt_installment."STATEMENT_ID" IS 'System-assigned identifier of a particular statement.';
COMMENT ON COLUMN pt_import_stmt_installment.installment_id IS 'System-assigned identifier of a particular installment.';
COMMENT ON COLUMN pt_import_stmt_installment.statement_notes IS 'Notes of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment.statement_year_xlate IS 'Value from the import file translated to obtain the statement_year_id.';
COMMENT ON COLUMN pt_import_stmt_installment.statement_year_id IS 'System-assigned identifier of a particular statement year.';
COMMENT ON COLUMN pt_import_stmt_installment.assessment_year_xlate IS 'Value from the import file translated to obtain the assessment_year_id.';
COMMENT ON COLUMN pt_import_stmt_installment.assessment_year_id IS 'System-assigned identifier of a particular assessment year.';
COMMENT ON COLUMN pt_import_stmt_installment.prop_tax_company_xlate IS 'Value from the import file translated to obtain the prop_tax_company_id.';
COMMENT ON COLUMN pt_import_stmt_installment.prop_tax_company_id IS 'System-assigned identifier of a particular prop tax company.';
COMMENT ON COLUMN pt_import_stmt_installment.statement_group_xlate IS 'Value from the import file translated to obtain the statement group.';
COMMENT ON COLUMN pt_import_stmt_installment.statement_group_id IS 'System-assigned identifier of a particular statement group.';
COMMENT ON COLUMN pt_import_stmt_installment.estimated_switch_xlate IS 'Value from the import file translated to obtain the estimated_switch_yn.';
COMMENT ON COLUMN pt_import_stmt_installment.estimated_switch_yn IS 'Yes(1)/No(0) switch indicating whether or not this is an estimated bill (i.e., the final tax amount is not known yet, but a tax payment is still due based on an estimate).';
COMMENT ON COLUMN pt_import_stmt_installment.year_notes IS 'Notes of the statement year record.';
COMMENT ON COLUMN pt_import_stmt_installment.tax_amount IS 'Tax amount of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment.credit_amount IS 'Credit amount of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment.penalty_amount IS 'Penalty amount of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment.interest_amount IS 'Interest amount of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment.assessment IS 'Assessment of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment.taxable_value IS 'Taxable value of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment.error_message IS 'Error message encountered while importing/translating/loading this row of data during the import process.';

CREATE TABLE pt_import_stmt_installment_arc (
  import_run_id          NUMBER(22,0)   NOT NULL,
  line_id                NUMBER(22,0)   NOT NULL,
  time_stamp             DATE           NULL,
  user_id                VARCHAR2(18)   NULL,
  description            VARCHAR2(254)  NULL,
  long_description       VARCHAR2(254)  NULL,
  account_number         VARCHAR2(254)  NULL,
  statement_xlate        VARCHAR2(254)  NULL,
  "STATEMENT_ID"           NUMBER(22,0)   NULL,
  installment_id         NUMBER(22,0)   NULL,
  statement_notes        VARCHAR2(254)  NULL,
  statement_year_xlate   VARCHAR2(254)  NULL,
  statement_year_id      NUMBER(22,0)   NULL,
  assessment_year_xlate  VARCHAR2(254)  NULL,
  assessment_year_id     NUMBER(22,0)   NULL,
  prop_tax_company_xlate VARCHAR2(254)  NULL,
  prop_tax_company_id    NUMBER(22,0)   NULL,
  statement_group_xlate  VARCHAR2(254)  NULL,
  statement_group_id     NUMBER(22,0)   NULL,
  estimated_switch_xlate VARCHAR2(254)  NULL,
  estimated_switch_yn    NUMBER(22,0)   NULL,
  year_notes             VARCHAR2(2000) NULL,
  tax_amount             VARCHAR2(35)   NULL,
  credit_amount          VARCHAR2(35)   NULL,
  penalty_amount         VARCHAR2(35)   NULL,
  interest_amount        VARCHAR2(35)   NULL,
  assessment             VARCHAR2(35)   NULL,
  taxable_value          VARCHAR2(35)   NULL,
  error_message          VARCHAR2(4000) NULL
);

ALTER TABLE pt_import_stmt_installment_arc
  ADD CONSTRAINT pt_impt_stmt_in_arc_imp_run_fk FOREIGN KEY (
    import_run_id
  ) REFERENCES pp_import_run (
    import_run_id
  );

COMMENT ON TABLE pt_import_stmt_installment_arc IS '(O and C)  [07]
The PT Import Stmt Installment Arc table is a archive table for all successful runs of the "Update : Bills by Installment" import process';
COMMENT ON COLUMN pt_import_stmt_installment_arc.import_run_id IS 'System-assigned identifier of an import run occurrence.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.line_id IS 'Line number corresponding to the row of this data in the original import file.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.description IS 'Description of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.long_description IS 'Long description of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.account_number IS 'Account number of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.statement_xlate IS 'Value from the import file translated to obtain the statement_id.';
COMMENT ON COLUMN pt_import_stmt_installment_arc."STATEMENT_ID" IS 'System-assigned identifier of a particular statement.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.installment_id IS 'System-assigned identifier of a particular installment.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.statement_notes IS 'Notes of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.statement_year_xlate IS 'Value from the import file translated to obtain the statement_year_id.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.statement_year_id IS 'System-assigned identifier of a particular statement year.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.assessment_year_xlate IS 'Value from the import file translated to obtain the assessment_year_id.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.assessment_year_id IS 'System-assigned identifier of a particular assessment year.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.prop_tax_company_xlate IS 'Value from the import file translated to obtain the prop_tax_company_id.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.prop_tax_company_id IS 'System-assigned identifier of a particular prop tax company.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.statement_group_xlate IS 'Value from the import file translated to obtain the statement group.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.statement_group_id IS 'System-assigned identifier of a particular statement group.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.estimated_switch_xlate IS 'Value from the import file translated to obtain the estimated_switch_yn.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.estimated_switch_yn IS 'Yes(1)/No(0) switch indicating whether or not this is an estimated bill (i.e., the final tax amount is not known yet, but a tax payment is still due based on an estimate).';
COMMENT ON COLUMN pt_import_stmt_installment_arc.year_notes IS 'Notes of the statement year record.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.tax_amount IS 'Tax amount of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.credit_amount IS 'Credit amount of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.penalty_amount IS 'Penalty amount of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.interest_amount IS 'Interest amount of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.assessment IS 'Assessment of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.taxable_value IS 'Taxable value of the statement.';
COMMENT ON COLUMN pt_import_stmt_installment_arc.error_message IS 'Error message encountered while importing/translating/loading this row of data during the import process.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3171, 0, 2016, 1, 0, 0, 045668, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045668_proptax_bill_instllmt_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;