/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047037_lease_fix_vc_company_import_column_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/22/2017 Jared Watkins    fix the company ID lookup for the VC import and add the row to the template
||============================================================================
*/

--create new, non-broken lease company lookup based on company description
insert into pp_import_lookup(import_lookup_id, description, long_description, column_name, lookup_sql,
    is_derived, lookup_table_name, lookup_column_name)
values(2507, 'Lease Company.Description', 'The passed in value corresponds to ILR Number field', 'company_id', 
    '( select b.company_id from company b where upper( trim( <importfield> ) ) = upper( trim( b.description ) ) and b.is_lease_company = 1 )', 
    0, 'company', 'description')
;

--set our import to use the new, functional lookup
update pp_import_column_lookup 
set import_lookup_id = 2507
where import_type_id = 264 
and column_name = 'company_id'
;

--in the case that the company_id column was created successfully before, update the lookup
update pp_import_template_fields 
set import_lookup_id = 2507
where import_type_id = 264 
and column_name = 'company_id'
and import_template_id = (select import_template_id 
                          from pp_import_template 
                          where import_type_id = 264 
                          and description = 'VP Var Component Amounts Add')
;

--insert the company_id row correctly this time (in the case that it didn't somehow get through last time)
insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
select import_template_id, 2, 264, 'company_id', 2507
from pp_import_template a
where import_type_id = 264 
and description = 'VP Var Component Amounts Add'
and not exists (select 1 
                from pp_import_template_fields b 
                where b.import_template_id = a.import_template_id 
                and column_name = 'company_id')
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3508, 0, 2017, 1, 0, 0, 47037, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047037_lease_fix_vc_company_import_column_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;