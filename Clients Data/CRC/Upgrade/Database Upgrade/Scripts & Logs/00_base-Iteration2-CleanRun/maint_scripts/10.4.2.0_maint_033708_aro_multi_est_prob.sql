/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033708_aro_multi_est_prob.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   12/19/2013 Ryan Oliveria  Adding probability to multi est tool
||============================================================================
*/

alter table ARO_MULTI_EST_REVIEW add PROBABILITY number(22,8);

comment on column ARO_MULTI_EST_REVIEW.PROBABILITY is 'The probability for this multi estimate.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (841, 0, 10, 4, 2, 0, 33708, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033708_aro_multi_est_prob.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;