/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031624_system_basegui.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   08/14/2013 Alex P.         Point Release
||============================================================================
*/

alter table PP_REQUIRED_TABLE_COLUMN add REQUIRED_COLUMN_EXPRESSION varchar2(2000);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (511, 0, 10, 4, 1, 0, 31624, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031624_system_basegui.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
