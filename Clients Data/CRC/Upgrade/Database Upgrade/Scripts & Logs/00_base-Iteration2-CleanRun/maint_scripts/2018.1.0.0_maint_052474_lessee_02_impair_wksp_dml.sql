/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052474_lessee_02_impair_wksp_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 11/01/2018 K Powers       Impairment Workspace
||============================================================================
*/

DECLARE

  new_id number;
  new_action number;

BEGIN

  select max(id) + 1 into new_id from PPBASE_ACTIONS_WINDOWS;
  select max(action_order) + 1 into new_action from PPBASE_ACTIONS_WINDOWS;
  
  insert into PPBASE_ACTIONS_WINDOWS
  (id,module,action_identifier,action_text,action_order,action_event)
  values
  (new_id,'LESSEE','impair_ilr','Impair ILR',new_action,'ue_impairilr');

END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (11452, 0, 2018, 1, 0, 0, 52474, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052474_lessee_02_impair_wksp_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
