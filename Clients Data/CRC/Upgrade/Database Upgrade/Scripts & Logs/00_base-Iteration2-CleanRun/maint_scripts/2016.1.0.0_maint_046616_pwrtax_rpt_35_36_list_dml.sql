 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046616_pwrtax_rpt_35_36_list_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 10/05/2016 Betsy Calender Initial script for report translations
 ||============================================================================
 */ 

update pp_reports
set report_type_id = 130
where report_id in (400004, 400005);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3311, 0, 2016, 1, 0, 0, 046616, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046616_pwrtax_rpt_35_36_list_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;