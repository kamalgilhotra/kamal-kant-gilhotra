/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030381_lease_schedule_tables.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/08/2013 Brandon Beck   Point release
||============================================================================
*/

create table LS_ASSET_SCHEDULE
(
 LS_ASSET_ID          number(22,0),
 REVISION             number(4,0),
 MONTH                date,
 RESIDUAL_AMOUNT      number(22,2),
 TERM_PENALTY         number(22,2),
 BPO_PRICE            number(22,2),
 BEG_CAPITAL_COST     number(22,2),
 END_CAPITAL_COST     number(22,2),
 BEG_OBLIGATION       number(22,2),
 END_OBLIGATION       number(22,2),
 BEG_LT_OBLIGATION    number(22,2),
 END_LT_OBLIGATION    number(22,2),
 INTEREST_ACCRUAL     number(22,2),
 PRINCIPAL_ACCRUAL    number(22,2),
 INTEREST_PAID        number(22,2),
 PRINCIPAL_PAID       number(22,2),
 EXECUTORY_ACCRUAL1   number(22,2),
 EXECUTORY_ACCRUAL2   number(22,2),
 EXECUTORY_ACCRUAL3   number(22,2),
 EXECUTORY_ACCRUAL4   number(22,2),
 EXECUTORY_ACCRUAL5   number(22,2),
 EXECUTORY_ACCRUAL6   number(22,2),
 EXECUTORY_ACCRUAL7   number(22,2),
 EXECUTORY_ACCRUAL8   number(22,2),
 EXECUTORY_ACCRUAL9   number(22,2),
 EXECUTORY_ACCRUAL10  number(22,2),
 EXECUTORY_PAID1      number(22,2),
 EXECUTORY_PAID2      number(22,2),
 EXECUTORY_PAID3      number(22,2),
 EXECUTORY_PAID4      number(22,2),
 EXECUTORY_PAID5      number(22,2),
 EXECUTORY_PAID6      number(22,2),
 EXECUTORY_PAID7      number(22,2),
 EXECUTORY_PAID8      number(22,2),
 EXECUTORY_PAID9      number(22,2),
 EXECUTORY_PAID10     number(22,2),
 CONTINGENT_ACCRUAL1  number(22,2),
 CONTINGENT_ACCRUAL2  number(22,2),
 CONTINGENT_ACCRUAL3  number(22,2),
 CONTINGENT_ACCRUAL4  number(22,2),
 CONTINGENT_ACCRUAL5  number(22,2),
 CONTINGENT_ACCRUAL6  number(22,2),
 CONTINGENT_ACCRUAL7  number(22,2),
 CONTINGENT_ACCRUAL8  number(22,2),
 CONTINGENT_ACCRUAL9  number(22,2),
 CONTINGENT_ACCRUAL10 number(22,2),
 CONTINGENT_PAID1     number(22,2),
 CONTINGENT_PAID2     number(22,2),
 CONTINGENT_PAID3     number(22,2),
 CONTINGENT_PAID4     number(22,2),
 CONTINGENT_PAID5     number(22,2),
 CONTINGENT_PAID6     number(22,2),
 CONTINGENT_PAID7     number(22,2),
 CONTINGENT_PAID8     number(22,2),
 CONTINGENT_PAID9     number(22,2),
 CONTINGENT_PAID10    number(22,2),
 USER_ID              varchar2(18),
 TIME_STAMP           date
);

alter table LS_ASSET_SCHEDULE
   add constraint LS_ASSET_SCHEDULE_PK
       primary key (LS_ASSET_ID, REVISION, MONTH)
       using index tablespace PWRPLANT_IDX;

create table LS_ILR_SCHEDULE
(
 LS_ILR_ID            number(22,0),
 REVISION             number(4,0),
 MONTH                date,
 RESIDUAL_AMOUNT      number(22,2),
 TERM_PENALTY         number(22,2),
 BPO_PRICE            number(22,2),
 BEG_CAPITAL_COST     number(22,2),
 END_CAPITAL_COST     number(22,2),
 BEG_OBLIGATION       number(22,2),
 END_OBLIGATION       number(22,2),
 BEG_LT_OBLIGATION    number(22,2),
 END_LT_OBLIGATION    number(22,2),
 INTEREST_ACCRUAL     number(22,2),
 PRINCIPAL_ACCRUAL    number(22,2),
 INTEREST_PAID        number(22,2),
 PRINCIPAL_PAID       number(22,2),
 EXECUTORY_ACCRUAL1   number(22,2),
 EXECUTORY_ACCRUAL2   number(22,2),
 EXECUTORY_ACCRUAL3   number(22,2),
 EXECUTORY_ACCRUAL4   number(22,2),
 EXECUTORY_ACCRUAL5   number(22,2),
 EXECUTORY_ACCRUAL6   number(22,2),
 EXECUTORY_ACCRUAL7   number(22,2),
 EXECUTORY_ACCRUAL8   number(22,2),
 EXECUTORY_ACCRUAL9   number(22,2),
 EXECUTORY_ACCRUAL10  number(22,2),
 EXECUTORY_PAID1      number(22,2),
 EXECUTORY_PAID2      number(22,2),
 EXECUTORY_PAID3      number(22,2),
 EXECUTORY_PAID4      number(22,2),
 EXECUTORY_PAID5      number(22,2),
 EXECUTORY_PAID6      number(22,2),
 EXECUTORY_PAID7      number(22,2),
 EXECUTORY_PAID8      number(22,2),
 EXECUTORY_PAID9      number(22,2),
 EXECUTORY_PAID10     number(22,2),
 CONTINGENT_ACCRUAL1  number(22,2),
 CONTINGENT_ACCRUAL2  number(22,2),
 CONTINGENT_ACCRUAL3  number(22,2),
 CONTINGENT_ACCRUAL4  number(22,2),
 CONTINGENT_ACCRUAL5  number(22,2),
 CONTINGENT_ACCRUAL6  number(22,2),
 CONTINGENT_ACCRUAL7  number(22,2),
 CONTINGENT_ACCRUAL8  number(22,2),
 CONTINGENT_ACCRUAL9  number(22,2),
 CONTINGENT_ACCRUAL10 number(22,2),
 CONTINGENT_PAID1     number(22,2),
 CONTINGENT_PAID2     number(22,2),
 CONTINGENT_PAID3     number(22,2),
 CONTINGENT_PAID4     number(22,2),
 CONTINGENT_PAID5     number(22,2),
 CONTINGENT_PAID6     number(22,2),
 CONTINGENT_PAID7     number(22,2),
 CONTINGENT_PAID8     number(22,2),
 CONTINGENT_PAID9     number(22,2),
 CONTINGENT_PAID10    number(22,2),
 USER_ID              varchar2(18),
 TIME_STAMP           date
);

alter table LS_ILR_SCHEDULE
   add constraint LS_ILR_SCHEDULE_PK
       primary key (LS_ILR_ID, REVISION, MONTH)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (437, 0, 10, 4, 1, 0, 30381, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030381_lease_schedule_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
