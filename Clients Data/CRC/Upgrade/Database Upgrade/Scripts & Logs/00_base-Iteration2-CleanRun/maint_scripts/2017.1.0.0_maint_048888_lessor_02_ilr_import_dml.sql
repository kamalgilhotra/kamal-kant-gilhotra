/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048888_lessor_02_ilr_import_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/18/2017 Shane "C" Ward		Set up Lessor ILR Import Tables
||============================================================================
*/

--New Import Type
INSERT INTO PP_IMPORT_TYPE
            (import_type_id,
             description,
             long_description,
             import_table_name,
             archive_table_name,
             allow_updates_on_add,
             delegate_object_name,
             archive_additional_columns)
VALUES      (502,
             'Add: Lessor ILR''s',
             'Lessor ILR''s',
             'lsr_import_ilr',
             'lsr_import_ilr_archive',
             1,
             'nvo_lsr_logic_import',
             'ilr_id');

--Associate with Subsystem
INSERT INTO PP_IMPORT_TYPE_SUBSYSTEM
            (import_subsystem_id,
             import_type_id)
VALUES      (14,
             502);

--Columns
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'c_bucket_1',
             'Contingent bucket 1',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'c_bucket_10',
             'Contingent bucket 10',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'c_bucket_2',
             'Contingent bucket 2',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'c_bucket_3',
             'Contingent bucket 3',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'c_bucket_4',
             'Contingent bucket 4',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'c_bucket_5',
             'Contingent bucket 5',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'c_bucket_6',
             'Contingent bucket 6',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'c_bucket_7',
             'Contingent bucket 7',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'c_bucket_8',
             'Contingent bucket 8',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'c_bucket_9',
             'Contingent bucket 9',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'cancelable_type_id',
             'Cancelable Type Id',
             'cancelable_type_xlate',
             1,
             1,
             'number(22,0)',
             'ls_cancelable_type',
             1,
             'cancelable_type_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'cap_type_id',
             'Cap Type Id',
             'cap_type_xlate',
             1,
             1,
             'number(22,0)',
             'lsr_cap_type',
             1,
             'cap_type_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id1',
             'Class Code Id1',
             'class_code_xlate1',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id10',
             'Class Code Id10',
             'class_code_xlate10',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id11',
             'Class Code Id11',
             'class_code_xlate11',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id12',
             'Class Code Id12',
             'class_code_xlate12',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id13',
             'Class Code Id13',
             'class_code_xlate13',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id14',
             'Class Code Id14',
             'class_code_xlate14',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id15',
             'Class Code Id15',
             'class_code_xlate15',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id16',
             'Class Code Id16',
             'class_code_xlate16',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id17',
             'Class Code Id17',
             'class_code_xlate17',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id18',
             'Class Code Id18',
             'class_code_xlate18',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id19',
             'Class Code Id19',
             'class_code_xlate19',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id2',
             'Class Code Id2',
             'class_code_xlate2',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id20',
             'Class Code Id20',
             'class_code_xlate20',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id3',
             'Class Code Id3',
             'class_code_xlate3',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id4',
             'Class Code Id4',
             'class_code_xlate4',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id5',
             'Class Code Id5',
             'class_code_xlate5',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id6',
             'Class Code Id6',
             'class_code_xlate6',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id7',
             'Class Code Id7',
             'class_code_xlate7',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id8',
             'Class Code Id8',
             'class_code_xlate8',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_id9',
             'Class Code Id9',
             'class_code_xlate9',
             0,
             1,
             'number(22,0)',
             'class_code',
             1,
             'class_code_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value1',
             'Class Code Value1',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value10',
             'Class Code Value10',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value11',
             'Class Code Value11',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value12',
             'Class Code Value12',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value13',
             'Class Code Value13',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value14',
             'Class Code Value14',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value15',
             'Class Code Value15',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value16',
             'Class Code Value16',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value17',
             'Class Code Value17',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value18',
             'Class Code Value18',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value19',
             'Class Code Value19',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value2',
             'Class Code Value2',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value20',
             'Class Code Value20',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value3',
             'Class Code Value3',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value4',
             'Class Code Value4',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value5',
             'Class Code Value5',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value6',
             'Class Code Value6',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value7',
             'Class Code Value7',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value8',
             'Class Code Value8',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'class_code_value9',
             'Class Code Value9',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'company_id',
             'Company Id',
             'company_xlate',
             1,
             1,
             'number(22,0)',
             'company_setup',
             1,
             'company_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'contingent_amount',
             'Contingent Amount',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'e_bucket_1',
             'Executory bucket 1',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'e_bucket_10',
             'Executory bucket 10',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'e_bucket_2',
             'Executory bucket 2',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'e_bucket_3',
             'Executory bucket 3',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'e_bucket_4',
             'Executory bucket 4',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'e_bucket_5',
             'Executory bucket 5',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'e_bucket_6',
             'Executory bucket 6',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'e_bucket_7',
             'Executory bucket 7',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'e_bucket_8',
             'Executory bucket 8',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'e_bucket_9',
             'Executory bucket 9',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'error_message',
             'Error Message',
             NULL,
             0,
             1,
             'varchar2(4000)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'est_executory_cost',
             'Est Executory Cost',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'est_in_svc_date',
             'Est In Svc Date',
             NULL,
             1,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'external_ilr',
             'External Ilr',
             NULL,
             0,
             1,
             'varchar2(35)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'ilr_group_id',
             'Ilr Group Id',
             'ilr_group_xlate',
             1,
             1,
             'number(22,0)',
             'lsr_ilr_group',
             1,
             'ilr_group_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'ilr_number',
             'Ilr Number',
             NULL,
             1,
             1,
             'varchar2(35)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'is_modified',
             'Is Modified',
             NULL,
             0,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'itc_sw',
             'Itc Sw',
             NULL,
             1,
             1,
             'varchar2(35)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'lease_id',
             'Lease Id',
             'lease_xlate',
             1,
             2,
             'number(22,0)',
             'lsr_lease',
             1,
             'lease_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'loaded',
             'Loaded',
             NULL,
             0,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'notes',
             'Notes',
             NULL,
             0,
             1,
             'varchar2(4000)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'number_of_terms',
             'Number Of Terms',
             NULL,
             1,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'paid_amount',
             'Paid Amount',
             NULL,
             1,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'payment_freq_id',
             'Payment Freq Id',
             'payment_freq_xlate',
             1,
             1,
             'number(22,0)',
             'ls_payment_freq',
             1,
             'payment_freq_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'payment_term_date',
             'Payment Term Date',
             NULL,
             1,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'purchase_option_amt',
             'Purchase Option Amt',
             NULL,
             1,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'purchase_option_type_id',
             'Purchase Option Type Id',
             'purchase_option_type_xlate',
             1,
             1,
             'number(22,0)',
             'ls_purchase_option_type',
             1,
             'purchase_option_type_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'renewal_option_type_id',
             'Renewal Option Type Id',
             'renewal_option_type_xlate',
             1,
             1,
             'number(22,0)',
             'ls_renewal_option_type',
             1,
             'renewal_option_type_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'termination_amt',
             'Termination Amt',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'unique_ilr_identifier',
             'Unique Ilr Identifier',
             NULL,
             1,
             1,
             'varchar2(35)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'workflow_type_id',
             'Workflow Type Id',
             'workflow_type_xlate',
             0,
             1,
             'number(22,0)',
             'workflow_type',
             1,
             'workflow_type_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'sublease_flag',
             'Sublease',
             'sublease_flag_xlate',
             1,
             1,
             'number(1)',
             'yes_no',
             1,
             'yes_no_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'sublease_id',
             'Sublease Lease',
             'sublease_id_xlate',
             0,
             1,
             'number(22,0)',
             'ls_lease',
             1,
             'lease_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'likely_to_collect',
             'Likely to Collect',
             'likely_to_collect_xlate',
             1,
             1,
             'number(1)',
             'yes_no',
             1,
             'yes_no_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'intent_to_purch',
             'Intent to Purchase',
             'intent_to_purch_xlate',
             1,
             1,
             'number(1)',
             'yes_no',
             1,
             'yes_no_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'specialized_asset',
             'Specialized Asset',
             'specialized_asset_xlate',
             1,
             1,
             'number(1)',
             'yes_no',
             1,
             'yes_no_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'intercompany_lease',
             'Intercompany Lease',
             'intercompany_lease_xlate',
             1,
             1,
             'number(1)',
             'yes_no',
             1,
             'yes_no_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'intercompany_company',
             'Intercompany Lease Company',
             'intercompany_company_xlate',
             0,
             1,
             'number(22,0)',
             'company_setup',
             1,
             'company_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'init_direct_cost_amt',
             'Initial Direct Cost Amount',
             NULL,
             0,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'sales_type_disc_rate',
             'Sales Type Discount Rate',
             NULL,
             0,
             1,
             'number(22,8)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'direct_fin_disc_rate',
             'Direct Finance Discount Rate',
             NULL,
             0,
             1,
             'number(22,8)',
             NULL,
             1,
             NULL);
			 
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'payment_term_id',
             'Payment Term ID',
             NULL,
             1,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'direct_fin_fmv_rate',
             'Direct Finance FMV Comparison Rate',
             NULL,
             0,
             1,
             'number(22,8)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 502,
             'direct_fin_int_rate',
             'Direct Fin Interest on Net Inv Rate',
             NULL,
             0,
             1,
             'number(22,8)',
             NULL,
             1,
             NULL);

--New Lookups
INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      ( 1081,
             'Lessor ILR Group.Description',
             'Lessor ILR Group ID',
             'ilr_group_id',
             '( select b.ilr_group_id from lsr_ilr_group b where upper(trim( <importfield> )) = upper( trim( b.description ) ) )',
             0,
             'lsr_ilr_group',
             'description');

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      ( 1082,
             'Lessor ILR Group.Long Description',
             'Lessor ILR Group ID',
             'ilr_group_id',
             '( select b.ilr_group_id from lsr_ilr_group b where upper(trim( <importfield> )) = upper( trim( b.long_description ) ) )',
             0,
             'lsr_ilr_group',
             'long_description');

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      ( 1083,
             'Lessor Lease/MLA.Description',
             'Lessor MLA',
             'lease_id',
             '( select b.lease_id from lsr_lease b where upper(trim( <importfield> )) = upper( trim( b.description ) ) )',
             0,
             'lsr_lease',
             'description');

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      ( 1084,
             'Lessor Lease/MLA.Long Description',
             'Lessor MLA',
             'lease_id',
             '( select b.lease_id from lsr_lease b where upper(trim( <importfield> )) = upper( trim( b.long_description ) ) )',
             0,
             'lsr_lease',
             'long_description');

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      ( 1085,
             'Lessor Lease/MLA.Lease Number',
             'Lessor MLA Number',
             'lease_id',
             '( select b.lease_id from lsr_lease b where upper(trim( <importfield> ), trimg(b.description)) ) = upper( trim( b.lease_number ) ) )',
             0,
             'lsr_lease',
             'lease_number');

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      ( 1087,
             'Payment Term Description',
             'Payment Term',
             'payment_term_type_id',
             '( select b.payment_term_type_id from ls_payment_term_type b where upper(trim( <importfield> )) = upper( trim( b.description ) ) )',
             0,
             'ls_payment_term_type',
             'description');

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      ( 1088,
             'Payment Term Long Description',
             'Payment Term',
             'payment_term_type_id',
             '( select b.payment_term_type_id from ls_payment_term_type b where upper(trim( <importfield> )) = upper( trim( b.long_description ) ) )',
             0,
             'ls_payment_term_type',
             'long_description');

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      ( 1089,
             'Lessor ILR Workflow Type.Description',
             'Lessor MLA Workflow Type.Description',
             'workflow_type_id',
             '( select b.workflow_type_id from workflow_type b where subsystem like ''%lessor_ilr_approval%'' AND upper(trim( <importfield> )) = upper( trim( b.description ) ) )',
             0,
             'workflow_type',
             'description');

--Assign columns to lookups
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'cancelable_type_id',
             1044);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'cap_type_id',
             1076);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'cap_type_id',
             1077);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id1',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id1',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id10',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id10',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id11',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id11',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id12',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id12',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id13',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id13',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id14',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id14',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id15',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id15',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id16',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id16',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id17',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id17',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id18',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id18',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id19',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id19',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id2',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id2',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id20',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id20',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id3',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id3',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id4',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id4',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id5',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id5',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id6',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id6',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id7',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id7',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id8',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id8',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id9',
             199);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'class_code_id9',
             200);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'company_id',
             19);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'company_id',
             20);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'ilr_group_id',
             1081);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'ilr_group_id',
             1082);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'intent_to_purch',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'intent_to_purch',
             78);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'intercompany_company',
             19);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'intercompany_company',
             20);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'intercompany_lease',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'intercompany_lease',
             78);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'lease_id',
             1083);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'lease_id',
             1084);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'lease_id',
             1085);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'likely_to_collect',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'likely_to_collect',
             78);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'payment_freq_id',
             1047);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'purchase_option_type_id',
             1030);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'specialized_asset',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'specialized_asset',
             78);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'sublease_flag',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'sublease_flag',
             78);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'sublease_id',
             1042);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'sublease_id',
             1060);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'sublease_id',
             1061);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'workflow_type_id',
             1089);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( 502,
             'renewal_option_type_id',
             1036); 
			 
--Default Template
INSERT INTO PP_IMPORT_TEMPLATE
            (import_template_id,
             import_type_id,
             description,
             long_description,
             do_update_with_add,
             is_autocreate_template)
VALUES      (( SELECT Max(import_template_id) + 1
               FROM   PP_IMPORT_TEMPLATE ),
             502,
             'Add: Lessor ILR''s',
             'Default Template for Lessor ILR''s',
             1,
             0);

--Insert default template fields with lookups
INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             1,
             502,
             'unique_ilr_identifier',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             2,
             502,
             'ilr_number',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             3,
             502,
             'lease_id',
             1083);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             4,
             502,
             'company_id',
             19);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             5,
             502,
             'est_in_svc_date',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             6,
             502,
             'ilr_group_id',
             1081);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             7,
             502,
             'workflow_type_id',
             1089);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             8,
             502,
             'external_ilr',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             9,
             502,
             'notes',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             10,
             502,
             'sales_type_disc_rate',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             11,
             502,
             'direct_fin_disc_rate',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             12,
             502,
             'direct_fin_fmv_rate',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             13,
             502,
             'direct_fin_int_rate',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             14,
             502,
             'intent_to_purch',
             77);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             15,
             502,
             'specialized_asset',
             77);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             16,
             502,
             'likely_to_collect',
             77);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             17,
             502,
             'sublease_flag',
             77);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             18,
             502,
             'sublease_id',
             1042);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             19,
             502,
             'intercompany_lease',
             77);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             20,
             502,
             'intercompany_company',
             19);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             21,
             502,
             'init_direct_cost_amt',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             22,
             502,
             'purchase_option_type_id',
             1030);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             23,
             502,
             'purchase_option_amt',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             24,
             502,
             'renewal_option_type_id',
             1036);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             25,
             502,
             'cancelable_type_id',
             1044);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             26,
             502,
             'itc_sw',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             27,
             502,
             'cap_type_id',
             1076);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             28,
             502,
             'termination_amt',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             29,
             502,
             'payment_term_id',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             30,
             502,
             'payment_term_date',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             31,
             502,
             'payment_freq_id',
             1047);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             32,
             502,
             'number_of_terms',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 502 AND
                      description = 'Add: Lessor ILR''s' ),
             33,
             502,
             'paid_amount',
             NULL); 
			 
			 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3799, 0, 2017, 1, 0, 0, 48888, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048888_lessor_02_ilr_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
