/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036012_system_F_ALERT_TEST.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/27/2014 Chris Mardis
||============================================================================
*/

create or replace function F_ALERT_TEST return number is
   L_RET  integer;
   L_CODE integer;
begin

   L_RET := PP_ALERT_PROCESS.INITIALIZE('alert_test', 'SERVER');

   L_RET := PP_ALERT_PROCESS.MESSAGEBOX('This is a OK message.', 'OK');
   DBMS_OUTPUT.PUT_LINE('OK = ' || TO_CHAR(L_RET));

   L_RET := PP_ALERT_PROCESS.MESSAGEBOX('This is a OKCANCEL message.', 'OKCANCEL');
   DBMS_OUTPUT.PUT_LINE('OKCANCEL = ' || TO_CHAR(L_RET));

   L_RET := PP_ALERT_PROCESS.MESSAGEBOX('This is a YESNO message.', 'YESNO');
   DBMS_OUTPUT.PUT_LINE('YESNO = ' || TO_CHAR(L_RET));

   L_RET := PP_ALERT_PROCESS.MESSAGEBOX('This is a YESNOCANCEL message.', 'YESNOCANCEL');
   DBMS_OUTPUT.PUT_LINE('YESNOCANCEL = ' || TO_CHAR(L_RET));

   L_RET := PP_ALERT_PROCESS.MESSAGEBOX('This is a ABORTRETRYIGNORE message.', 'ABORTRETRYIGNORE');
   DBMS_OUTPUT.PUT_LINE('ABORTRETRYIGNORE = ' || TO_CHAR(L_RET));

   return 1;

end F_ALERT_TEST;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (919, 0, 10, 4, 2, 0, 36012, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036012_system_F_ALERT_TEST.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;