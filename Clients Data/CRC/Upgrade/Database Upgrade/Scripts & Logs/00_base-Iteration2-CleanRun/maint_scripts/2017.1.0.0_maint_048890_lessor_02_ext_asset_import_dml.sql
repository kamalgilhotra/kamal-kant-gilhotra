/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048890_lessor_02_ext_asset_import_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/26/2017 Shane "C" Ward		Set up Lessor External Asset Import Type
||============================================================================
*/

--New Import Type
INSERT INTO PP_IMPORT_TYPE
            (import_type_id,
             description,
             long_description,
             import_table_name,
             archive_table_name,
             allow_updates_on_add,
             delegate_object_name,
             archive_additional_columns)
VALUES      (504,
             'Add: External Lessor Assets to ILR',
             'Lessor External Assets to ILR',
             'lsr_import_ext_asset',
             'lsr_import_ext_asset_archive',
             1,
             'nvo_lsr_logic_import',
             null);

--Associate new Import Type
INSERT INTO PP_IMPORT_TYPE_SUBSYSTEM
            (import_type_id,
             import_subsystem_id)
VALUES      (504,
             14);

--Insert all the columns for the import type
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 504,
             'ilr_id',
             NULL,
             NULL,
             'ILR ID',
             'ilr_id_xlate',
             1,
             1,
             'number(22,0)',
             'lsr_ilr',
             NULL,
             1,
             NULL,
             'ilr_id',
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 504,
             'revision',
             NULL,
             NULL,
             'Revision',
             null,
             0,
             1,
             'number(22,0)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );
			 
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 504,
             'description',
             NULL,
             NULL,
             'Description',
             null,
             1,
             1,
             'varchar2(35)',
             null,
             NULL,
             1,
             NULL,
             null,
             NULL,
             NULL );
			 
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 504,
             'long_description',
             NULL,
             NULL,
             'Long Description',
             null,
             0,
             1,
             'varchar2(254)',
             null,
             NULL,
             1,
             NULL,
             null,
             NULL,
             NULL );
			 
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 504,
             'serial_number',
             NULL,
             NULL,
             'Serial Number',
             null,
             0,
             1,
             'varchar2(35)',
             null,
             NULL,
             1,
             NULL,
             null,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 504,
             'estimated_residual_pct',
             NULL,
             NULL,
             'Estimated Residual Percent',
             null,
             1,
             1,
             'number(22,8)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 504,
             'estimated_residual',
             NULL,
             NULL,
             'Estimated Residual Amount',
             null,
             1,
             1,
             'number(22,2)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 504,
             'fair_market_value',
             NULL,
             NULL,
             'Fair Market Value',
             null,
             1,
             1,
             'number(22,2)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 504,
             'guaranteed_residual',
             NULL,
             NULL,
             'Guaranteed Residual Amount',
             null,
             1,
             1,
             'number(22,2)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 504,
             'expected_life',
             NULL,
             NULL,
             'Expected Life',
             null,
             1,
             1,
             'number(22,0)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 504,
             'economic_life',
             NULL,
             NULL,
             'Economic Life',
             null,
             1,
             1,
             'number(22,0)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );

--Column Lookups
INSERT INTO pp_import_column_lookup
  (import_type_id, import_lookup_id, column_name)
  VALUES
  (504, 1090, 'ilr_id');

INSERT INTO pp_import_column_lookup
  (import_type_id, import_lookup_id, column_name)
  VALUES
  (504, 1091, 'ilr_id');

INSERT INTO pp_import_column_lookup
  (import_type_id, import_lookup_id, column_name)
  VALUES
  (504, 1092, 'ilr_id');

  --Default Template
INSERT INTO PP_IMPORT_TEMPLATE
            (import_template_id,
             import_type_id,
             description,
             long_description,
             do_update_with_add,
             is_autocreate_template)
VALUES      (( SELECT Max(import_template_id) + 1
               FROM   PP_IMPORT_TEMPLATE ),
             504,
             'Add: External Lessor Assets to ILR',
             'Default Template for Adding External Assets to ILR',
             1,
             0);
			 
INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 504 AND
                      description = 'Add: External Lessor Assets to ILR' ),
             1,
             504,
             'ilr_id',
             1090);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 504 AND
                      description = 'Add: External Lessor Assets to ILR' ),
             2,
             504,
             'revision',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 504 AND
                      description = 'Add: External Lessor Assets to ILR' ),
             3,
             504,
             'description',
             null);
			 
INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 504 AND
                      description = 'Add: External Lessor Assets to ILR' ),
             4,
             504,
             'long_description',
             null);
			 
INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 504 AND
                      description = 'Add: External Lessor Assets to ILR' ),
             5,
             504,
             'serial_number',
             null);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 504 AND
                      description = 'Add: External Lessor Assets to ILR' ),
             6,
             504,
             'fair_market_value',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 504 AND
                      description = 'Add: External Lessor Assets to ILR' ),
             7,
             504,
             'guaranteed_residual',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 504 AND
                      description = 'Add: External Lessor Assets to ILR' ),
             8,
             504,
             'estimated_residual',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 504 AND
                      description = 'Add: External Lessor Assets to ILR' ),
             9,
             504,
             'estimated_residual_pct',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 504 AND
                      description = 'Add: External Lessor Assets to ILR' ),
             10,
             504,
             'expected_life',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 504 AND
                      description = 'Add: External Lessor Assets to ILR' ),
             11,
             504,
             'economic_life',
             NULL); 
			 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3852, 0, 2017, 1, 0, 0, 48890, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048890_lessor_02_ext_asset_import_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
