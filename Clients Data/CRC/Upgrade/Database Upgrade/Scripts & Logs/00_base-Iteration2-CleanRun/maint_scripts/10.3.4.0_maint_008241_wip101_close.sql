/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008241_wip101_close.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   03/01/2012 Sunjin Cone    Point Release
||============================================================================
*/

alter table WIP_COMPUTATION add WIP_COMP101_ROLLUP_ID number(22,0);

alter table WIP_COMPUTATION
   add constraint WIPCOMPROLLUPID
       foreign key (WIP_COMP101_ROLLUP_ID) references WIP_COMPUTATION;

alter table WIP_COMP_PENDING_TRANS add WIP_COMP101_ROLLUP_ID number(22,0);

drop table WIP_COMP_PEND_TRANS_TEMP;

create global temporary table WIP_COMP_PEND_TRANS_TEMP
(
 ORIG_PEND_TRANS_ID    number(22,0) not null,
 WORK_ORDER_ID         number(22,0) not null,
 MONTH_NUMBER          number(22,0) not null,
 NEW_PEND_TRANS_ID     number(22,0),
 GL_ACCOUNT_ID         number(22,0) not null,
 RETIREMENT_UNIT_ID    number(22,0) not null,
 MINOR_PEND_TRAN_ID    number(22,0) not null,
 WIP_COMPUTATION_ID    number(22,0) not null,
 WIP_COMP101_ROLLUP_ID number(22,0)
) on commit preserve rows;

alter table WIP_COMP_PEND_TRANS_TEMP
   add constraint WIP_COMP_PEND_TRANS_TEMP_PK
       primary key (ORIG_PEND_TRANS_ID, WORK_ORDER_ID, MONTH_NUMBER, GL_ACCOUNT_ID,
                    RETIREMENT_UNIT_ID, MINOR_PEND_TRAN_ID, WIP_COMPUTATION_ID, WIP_COMP101_ROLLUP_ID);

update WIP_COMPUTATION
   set WIP_COMP101_ROLLUP_ID = WIP_COMPUTATION_ID
 where not exists (select 1 from COMPANY_SETUP where UPPER(DESCRIPTION) like '%ENTERGY%');
commit;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (107, 0, 10, 3, 4, 0, 8241, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_008241_wip101_close.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
