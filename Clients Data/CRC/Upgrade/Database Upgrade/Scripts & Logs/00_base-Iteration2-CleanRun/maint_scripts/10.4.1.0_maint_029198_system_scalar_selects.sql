/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029198_system_scalar_selects.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   03/14/2013 Brandon Beck    Point Release
||============================================================================
*/

create table PP_QUERY_SCALAR_SELECT
(
 DESCRIPTION   varchar2(30) not null,
 COLUMN_NAME   varchar2(30) not null,
 SELECT_CLAUSE varchar2(2000),
 GROUP_CLAUSE  varchar2(2000),
 TYPE          varchar2(35),
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 COLUMN_WIDTH  number(22,0)
);

alter table PP_QUERY_SCALAR_SELECT
   add constraint PP_QUERY_SCALAR_SELECT_PK
       primary key (DESCRIPTION, COLUMN_NAME)
       using index tablespace PWRPLANT_IDX;

create table PP_QUERY_SCALAR_KEYWORDS
(
 THE_KEYWORD varchar2(35),
 COLUMN_NAME varchar2(35),
 USER_ID     varchar2(18),
 TIME_STAMP  date
);

alter table PP_QUERY_SCALAR_KEYWORDS
   add constraint PP_QUERY_SCALAR_KEYWORDS_PK
       primary key (THE_KEYWORD, COLUMN_NAME)
       using index tablespace PWRPLANT_IDX;

insert into PP_QUERY_SCALAR_SELECT
   (DESCRIPTION, COLUMN_NAME, SELECT_CLAUSE, GROUP_CLAUSE, "TYPE", COLUMN_WIDTH)
   select DESCRIPTION, COLUMN_NAME, SELECT_CLAUSE, GROUP_CLAUSE, "TYPE", COLUMN_WIDTH
     from CR_QUERY_SCALAR_SELECT;

insert into PP_QUERY_SCALAR_KEYWORDS
   (THE_KEYWORD, COLUMN_NAME)
select '<WO>', 'work_order_number' from dual union all
select '<WO>', 'work_order' from dual union all
select '<WO>', 'wo_number' from dual union all
select '<WO>', 'wo_num' from dual union all
select '<ACCT>', 'gl_account' from dual union all
select '<ACCT>', 'account' from dual union all
select '<ACCT>', 'acct' from dual union all
select '<ACCT>', 'gl_acct' from dual union all
select '<ACCT>', 'external_account_code' from dual union all
select '<CE>', 'cost_element' from dual union all
select '<CE>', 'external_cost_element' from dual union all
select '<DEPT>', 'dept' from dual union all
select '<DEPT>', 'department' from dual union all
select '<DEPT>', 'department_code' from dual union all
select '<DEPT>', 'external_department_code' from dual union all
select '<DEPT>', 'dept_code' from dual union all
select '<CO>', 'company' from dual union all
select '<CO>', 'gl_company_no' from dual union all
select '<CO>', 'company_number' from dual union all
select '<WO_ID>', 'work_order_id' from dual union all
select '<WO_ID>', 'wo_id' from dual union all
select '<ACCT_ID>', 'gl_acct_id' from dual union all
select '<ACCT_ID>', 'gl_account_id' from dual union all
select '<CE_ID>', 'cost_element_id' from dual union all
select '<DEPT_ID>', 'dept_id' from dual union all
select '<DEPT_ID>', 'department_id' from dual union all
select '<CO_ID>', 'co_id' from dual union all
select '<CO_ID>', 'company_id' from dual union all
select '<CO_ID>', 'comp_id' from dual;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (321, 0, 10, 4, 1, 0, 29198, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029198_system_scalar_selects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;