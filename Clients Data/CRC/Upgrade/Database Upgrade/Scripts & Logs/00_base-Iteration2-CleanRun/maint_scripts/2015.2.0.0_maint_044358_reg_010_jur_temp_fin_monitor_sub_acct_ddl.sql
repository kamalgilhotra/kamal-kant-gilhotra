/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044358_reg_010_jur_temp_fin_monitor_sub_acct_ddl.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ----------------------------------------------
|| 2015.2.0.0 07/29/2015 Sarah Byers 		 	Create REG JUR FIN MONITOR SUB ACCT
||========================================================================================
*/

create table reg_jur_fin_monitor_sub_acct (
reg_jur_template_id number(22,0) not null,
reg_acct_type_id number(22,0) not null,
sub_acct_type_id number(22,0) not null,
reg_fin_monitor_summ_id number(22,0) not null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_jur_fin_monitor_sub_acct add (
constraint pk_reg_jur_fin_mon_sub_acct primary key (reg_jur_template_id, reg_acct_type_id, sub_acct_type_id) using index tablespace pwrplant_idx);

alter table reg_jur_fin_monitor_sub_acct
add constraint fk_reg_jur_fin_mon_sub_acct1
foreign key (reg_jur_template_id)
references reg_jur_template (reg_jur_template_id);

alter table reg_jur_fin_monitor_sub_acct
add constraint fk_reg_jur_fin_mon_sub_acct2
foreign key (reg_acct_type_id)
references reg_acct_type (reg_acct_type_id);

alter table reg_jur_fin_monitor_sub_acct
add constraint fk_reg_jur_fin_mon_sub_acct3
foreign key (reg_acct_type_id, sub_acct_type_id)
references reg_sub_acct_type (reg_acct_type_id, sub_acct_type_id);

alter table reg_jur_fin_monitor_sub_acct
add constraint fk_reg_jur_fin_mon_sub_acct4
foreign key (reg_fin_monitor_summ_id)
references reg_fin_monitor_summary (reg_fin_monitor_summ_id);

comment on table reg_jur_fin_monitor_sub_acct is 'The Reg Jur Fin Monitor Sub Acct table relates Regulatory Sub Account Types to Regulatory Analysis Summaries within a Jurisdictional Template.';
comment on column reg_jur_fin_monitor_sub_acct.reg_jur_template_id is 'System assigned identifier of a Regulatory Jurisdictional Template.';
comment on column reg_jur_fin_monitor_sub_acct.reg_acct_type_id is 'System assigned identifier of a Regulatory Account Type.';
comment on column reg_jur_fin_monitor_sub_acct.sub_acct_type_id is 'System assigned identifier of a Regulatory Account Type.';
comment on column reg_jur_fin_monitor_sub_acct.reg_fin_monitor_summ_id is 'System assigned identifier of a Regulatory Account Type.';
comment on column reg_jur_fin_monitor_sub_acct.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_jur_fin_monitor_sub_acct.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2740, 0, 2015, 2, 0, 0, 044358, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044358_reg_010_jur_temp_fin_monitor_sub_acct_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;